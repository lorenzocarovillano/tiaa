/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:57 PM
**        * FROM NATURAL SUBPROGRAM : Cpon110
************************************************************
**        * FILE NAME            : Cpon110.java
**        * CLASS NAME           : Cpon110
**        * INSTANCE NAME        : Cpon110
************************************************************
************************************************************************
** PROGRAM:     CPON110                                               **
** SYSTEM:      CONSOLIDATED PAYMENT SYSTEM                           **
**              OPEN INVESTMENTS ARCHITECTURE                         **
** DATE:        07/12/2002                                            **
** AUTHOR:      ALTHEA A. YOUNG - ADAPTED FROM CPSN110                **
** DESCRIPTION: PROGRAM EXTRACTS BANK INFORMATION FROM VARIOUS        **
**              BANK 'REFERENCE-TABLE' FILES.                         **
**              THIS ROUTINE CAN BE CALLED USING FOUR FUNCTIONS:      **
**              1. ' '             - FOR A GIVEN SOURCE CODE, ALL THE **
**                 RELATED FIELDS WILL BE RETURNED.                   **
**              2. 'NEXT'          - FOR A GIVEN SOURCE CODE, ALL THE **
**                 RELATED FIELDS FOR THE NEXT SOURCE CODE ARE        **
**                 RETURNED.                                          **
**                 THIS FEATURE IS USED IN BATCH, SO ALL SOURCE CODES **
**                 CAN BE REPORTED.                                   **
**              3. 'NEW SEQUENCE'  - NEXT PAYMENT NUMBER FOR A GIVEN  **
**                 SOURCE CODE IS RECEIVED, AND THE TABLE IS UPDATED. **
**              4. 'NEXT SEQUENCE' - NEW PAYMENT NUMBER AND ALL THE   **
**                 RELATED FIELDS ARE RETURNED. THE TABLE IS UPADTED. **
**                                                                    **
** ACCESS:      REFERENCE-TABLE USING INPUT PARAMETERS                **
**                                                                    **
**              RETRIEVE INFORMATION FROM THE FOLLOWING TABLES:       **
**              - BANK-ACCOUNT              (ACBNKA)                  **
**              - BANK-LOGO                 (ACLOGO)                  **
**              - BANK-NAME                 (ACBNKN)                  **
**              - COMMON-BANK-ACCOUNT-RANGE (ACBNK#)                  **
**                                                                    **
** UPDATE:      BANK-ACCOUNT (ACBNKA) :                               **
**              IF FUNCTION = 'NEW SEQUENCE' OR 'NEXT SEQUENCE'       **
**              (ET IS DONE FROM CALLING MODULE)                      **
**--------------------------------------------------------------------**
** INPUT PARAMETERS:                                                  **
**----------                                                          **
**    CPOA110-FUNCTION    - VALUES:  '               '    OR          **
**                                   'NEXT           '    OR          **
**                                   'NEW SEQUENCE   '    OR          **
**                                   'NEXT SEQUENCE  '                **
**                                                                    **
**----------                                                          **
**    CPOA110-SOURCE-CODE - VALUES:   (VALID SOURCE CODE) OR          **
**                                    ' ' (BLANK)                     **
**    (SOURCE-CODE = ' '  VALID ONLY IF CPOA110-FUNCTION = 'NEXT')    **
**                                                                    **
**----------                                                          **
**    CPOA110-NEW-SEQ-NBR - VALUES:  A NEW SEQUENCE NUM (N10)         **
**                                   WHICH IS VALIDATED BY PROGRAM    **
**                                   TO BE IN RANGE BETWEEN START-SEQ **
**                                   AND END-SEQ.                     **
**                                                                    **
**--------------------------------------------------------------------**
** OUTPUT PARAMETERS:      ALL REFERENCE-TABLE FIELDS.                **
**                                                                    **
**--------------------------------------------------------------------**
**          RETURN CODES:  (LOOK AT FUNCTION CODE BELOW ALSO)         **
** 00 - SUCCESSFUL                                                    **
**                                                                    **
** 01 - SOURCE CODE NOT SUPPLIED.                                     **
**                                                                    **
** 02 - USING SUPPLIED SOURCE CODE CAN't find matching record         **
**      IN BANK-ACCOUNT TABLE                                         **
**                                                                    **
** 03 - USING BANK-LOGO-KEY FROM ACCOUNT-TABLE CAN't find             **
**      MATCHING RECORD IN BANK-LOGO TABLE                            **
**                                                                    **
** 04 - USING BANK-ROUTING FROM ACCONT-TABLE CAN't find               **
**      MATCHING RECORD IN BANK-NAME TABLE                            **
**                                                                    **
** 05 - BANK-LOGO-KEY FROM ACCONT-TABLE IS BLANK                      **
**      SO WE CAN't get info from BANK-LOGO table                     **
**                                                                    **
** 06 - BANK-ROUTING FROM ACCONT-TABLE IS BLANK                       **
**      SO WE CAN't get info from BANK-NAME table                     **
**                                                                    **
** 11 - FUNCTION 'NEW SEQUENCE ' SUPPLIED SOURCE-CODE IS BLANK        **
**                                                                    **
** 12 - FUNCTION 'NEW SEQUENCE ' SUPPLIED NEW-SEQ-NBR IS BLANK        **
**                                                                    **
** 13 - FUNCTION 'NEW SEQUENCE ' CAN't find match on given key        **
**                                                                    **
** 14 - FUNCTION 'NEW SEQUENCE ' SUPPLIED NEW-SEQ-NBR IS NOT WITHIN   **
**      THE RANGE OF START-SEQ - END-SEQ                              **
**                                                                    **
** 21 - FUNCTION 'NEXT SEQUENCE' SUPPLIED SOURCE-CODE IS BLANK        **
**                                                                    **
** 22 - FUNCTION 'NEXT SEQUENCE' SUPPLIED NEW-SEQ-NBR NOT REQUIRED    **
**                                                                    **
** 23 - FUNCTION 'NEXT SEQUENCE' CAN't find match on given key        **
**                                                                    **
** 24 - FUNCTION 'NEXT SEQUENCE' NEW-SEQ-NBR IS NOT WITHIN THE        **
**      RANGE OF START-SEQ - END-SEQ                                  **
**                                                                    **
** 25 - FUNCTION 'NEXT SEQUENCE' CAN't find match on Common Range     **
**      TABLE                                                         **
**                                                                    **
** 26 - FUNCTION 'NEXT SEQUENCE' NEW-SEQ-NBR IS NOT WITHIN THE        **
**      RANGE OF NEXT-SEQ - END-SEQ ON COMMON RANGE TABLE             **
**                                                                    **
** 99 - INVALID FUNCTION                                              **
**                                                                    **
** *********************************************************************
**                                                                    **
**--------------------------------------------------------------------**
** RETURN CODES SPECIFIC TO EACH FUNCTION CODE:                       **
**--------------------------------------------------------------------**
**  00, 03, 04, 05, 06 CAN BE RETURNED FROM ANY FUNCTION              **
**                                                                    **
**                                                                    **
** CPOA110-FUNCTION   - VALUE = ' '                                   **
**                                                                    **
** 01 - SOURCE CODE NOT SUPPLIED                                      **
**                                                                    **
** 02 - USING SUPPLIED SOURCE CODE CAN't find matching record         **
**      IN BANK-ACCOUNT TABLE                                         **
**                                                                    **
**                                                                    **
**--------------------------------------------------------------------**
** CPOA110-FUNCTION   - VALUE = 'NEXT'                                **
**                                                                    **
** 00 AND CPOA110-FUNCTION  = 'NEXT'   -  RECORD FOUND                **
**        OR                                                          **
** 00 AND CPOA110-FUNCTION  = 'END'    -  LOGICAL END OF TABLE        **
**                                                                    **
**--------------------------------------------------------------------**
**                                                                    **
************************************************************************
**                           H I S T O R Y                            **
************************************************************************
** 10/05/02  AR - ADD COMMON RANGE BANK TABLE LOOKUP                  **
** 06/12/03  AY - RE-COMPILED TO INCLUDE NEW 'CPOA110' LAY-OUT.       **
** 06/25/05  ILSE BOHM PROJECT TESTING. FIX REFTABLE REDEFINE FOR 5/10*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpon110 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCpoa110 pdaCpoa110;
    private LdaCpol110 ldaCpol110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Bank_Super1;

    private DbsGroup pnd_Bank_Super1__R_Field_1;
    private DbsField pnd_Bank_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Bank_Super1__R_Field_2;
    private DbsField pnd_Bank_Super1_Pnd_Bank_Source_Code;
    private DbsField pnd_Bank_Super1_Pnd_Filler1;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Long_Key;
    private DbsField pnd_Logo_Super1;

    private DbsGroup pnd_Logo_Super1__R_Field_3;
    private DbsField pnd_Logo_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Logo_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Logo_Super1_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Logo_Super1__R_Field_4;
    private DbsField pnd_Logo_Super1_Pnd_Bank_Logo_Key;
    private DbsField pnd_Logo_Super1_Pnd_Filler4;
    private DbsField pnd_Logo_Super1_Pnd_Rt_Long_Key;
    private DbsField pnd_Bank_Name_Super2;

    private DbsGroup pnd_Bank_Name_Super2__R_Field_5;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_Table_Id;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_Long_Key;

    private DbsGroup pnd_Bank_Name_Super2__R_Field_6;
    private DbsField pnd_Bank_Name_Super2_Pnd_Bank_Routing;
    private DbsField pnd_Bank_Name_Super2_Pnd_Filler5;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_Short_Key;
    private DbsField pnd_Cmn_Rnge_Super2;

    private DbsGroup pnd_Cmn_Rnge_Super2__R_Field_7;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_Table_Id;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_Long_Key;

    private DbsGroup pnd_Cmn_Rnge_Super2__R_Field_8;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_Routing_Id;
    private DbsField pnd_Cmn_Rnge_Super2__Filler1;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_Account_Nbr;
    private DbsField pnd_Cmn_Rnge_Super2__Filler2;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Cmn_Rnge_Super2__R_Field_9;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_Bank_Id;
    private DbsField pnd_Cmn_Rnge_Super2_Pnd_Rt_Chk_Or_Eft;

    private DbsGroup pnd_Overide_Logo_Fields;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Name1;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Name2;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Addr1;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Addr2;
    private DbsField pnd_Isn;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCpol110 = new LdaCpol110();
        registerRecord(ldaCpol110);
        registerRecord(ldaCpol110.getVw_ra());
        registerRecord(ldaCpol110.getVw_rau());
        registerRecord(ldaCpol110.getVw_rc());
        registerRecord(ldaCpol110.getVw_rcu());
        registerRecord(ldaCpol110.getVw_rl());
        registerRecord(ldaCpol110.getVw_rn());

        // parameters
        parameters = new DbsRecord();
        pdaCpoa110 = new PdaCpoa110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Bank_Super1 = localVariables.newFieldInRecord("pnd_Bank_Super1", "#BANK-SUPER1", FieldType.STRING, 66);

        pnd_Bank_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Bank_Super1__R_Field_1", "REDEFINE", pnd_Bank_Super1);
        pnd_Bank_Super1_Pnd_Rt_A_I_Ind = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Bank_Super1_Pnd_Rt_Table_Id = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Bank_Super1_Pnd_Rt_Short_Key = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 
            20);

        pnd_Bank_Super1__R_Field_2 = pnd_Bank_Super1__R_Field_1.newGroupInGroup("pnd_Bank_Super1__R_Field_2", "REDEFINE", pnd_Bank_Super1_Pnd_Rt_Short_Key);
        pnd_Bank_Super1_Pnd_Bank_Source_Code = pnd_Bank_Super1__R_Field_2.newFieldInGroup("pnd_Bank_Super1_Pnd_Bank_Source_Code", "#BANK-SOURCE-CODE", 
            FieldType.STRING, 5);
        pnd_Bank_Super1_Pnd_Filler1 = pnd_Bank_Super1__R_Field_2.newFieldInGroup("pnd_Bank_Super1_Pnd_Filler1", "#FILLER1", FieldType.STRING, 15);
        pnd_Bank_Super1_Pnd_Rt_Long_Key = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 
            40);
        pnd_Logo_Super1 = localVariables.newFieldInRecord("pnd_Logo_Super1", "#LOGO-SUPER1", FieldType.STRING, 66);

        pnd_Logo_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Logo_Super1__R_Field_3", "REDEFINE", pnd_Logo_Super1);
        pnd_Logo_Super1_Pnd_Rt_A_I_Ind = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Logo_Super1_Pnd_Rt_Table_Id = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Logo_Super1_Pnd_Rt_Short_Key = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 
            20);

        pnd_Logo_Super1__R_Field_4 = pnd_Logo_Super1__R_Field_3.newGroupInGroup("pnd_Logo_Super1__R_Field_4", "REDEFINE", pnd_Logo_Super1_Pnd_Rt_Short_Key);
        pnd_Logo_Super1_Pnd_Bank_Logo_Key = pnd_Logo_Super1__R_Field_4.newFieldInGroup("pnd_Logo_Super1_Pnd_Bank_Logo_Key", "#BANK-LOGO-KEY", FieldType.STRING, 
            10);
        pnd_Logo_Super1_Pnd_Filler4 = pnd_Logo_Super1__R_Field_4.newFieldInGroup("pnd_Logo_Super1_Pnd_Filler4", "#FILLER4", FieldType.STRING, 10);
        pnd_Logo_Super1_Pnd_Rt_Long_Key = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 
            40);
        pnd_Bank_Name_Super2 = localVariables.newFieldInRecord("pnd_Bank_Name_Super2", "#BANK-NAME-SUPER2", FieldType.STRING, 66);

        pnd_Bank_Name_Super2__R_Field_5 = localVariables.newGroupInRecord("pnd_Bank_Name_Super2__R_Field_5", "REDEFINE", pnd_Bank_Name_Super2);
        pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Bank_Name_Super2_Pnd_Rt_Table_Id = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_Table_Id", "#RT-TABLE-ID", 
            FieldType.STRING, 5);
        pnd_Bank_Name_Super2_Pnd_Rt_Long_Key = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_Long_Key", "#RT-LONG-KEY", 
            FieldType.STRING, 40);

        pnd_Bank_Name_Super2__R_Field_6 = pnd_Bank_Name_Super2__R_Field_5.newGroupInGroup("pnd_Bank_Name_Super2__R_Field_6", "REDEFINE", pnd_Bank_Name_Super2_Pnd_Rt_Long_Key);
        pnd_Bank_Name_Super2_Pnd_Bank_Routing = pnd_Bank_Name_Super2__R_Field_6.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Bank_Routing", "#BANK-ROUTING", 
            FieldType.STRING, 9);
        pnd_Bank_Name_Super2_Pnd_Filler5 = pnd_Bank_Name_Super2__R_Field_6.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Filler5", "#FILLER5", FieldType.STRING, 
            31);
        pnd_Bank_Name_Super2_Pnd_Rt_Short_Key = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", 
            FieldType.STRING, 20);
        pnd_Cmn_Rnge_Super2 = localVariables.newFieldInRecord("pnd_Cmn_Rnge_Super2", "#CMN-RNGE-SUPER2", FieldType.STRING, 66);

        pnd_Cmn_Rnge_Super2__R_Field_7 = localVariables.newGroupInRecord("pnd_Cmn_Rnge_Super2__R_Field_7", "REDEFINE", pnd_Cmn_Rnge_Super2);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_A_I_Ind = pnd_Cmn_Rnge_Super2__R_Field_7.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Table_Id = pnd_Cmn_Rnge_Super2__R_Field_7.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Long_Key = pnd_Cmn_Rnge_Super2__R_Field_7.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 
            40);

        pnd_Cmn_Rnge_Super2__R_Field_8 = pnd_Cmn_Rnge_Super2__R_Field_7.newGroupInGroup("pnd_Cmn_Rnge_Super2__R_Field_8", "REDEFINE", pnd_Cmn_Rnge_Super2_Pnd_Rt_Long_Key);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Routing_Id = pnd_Cmn_Rnge_Super2__R_Field_8.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_Routing_Id", "#RT-ROUTING-ID", 
            FieldType.STRING, 9);
        pnd_Cmn_Rnge_Super2__Filler1 = pnd_Cmn_Rnge_Super2__R_Field_8.newFieldInGroup("pnd_Cmn_Rnge_Super2__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Account_Nbr = pnd_Cmn_Rnge_Super2__R_Field_8.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_Account_Nbr", "#RT-ACCOUNT-NBR", 
            FieldType.STRING, 21);
        pnd_Cmn_Rnge_Super2__Filler2 = pnd_Cmn_Rnge_Super2__R_Field_8.newFieldInGroup("pnd_Cmn_Rnge_Super2__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Short_Key = pnd_Cmn_Rnge_Super2__R_Field_7.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", 
            FieldType.STRING, 20);

        pnd_Cmn_Rnge_Super2__R_Field_9 = pnd_Cmn_Rnge_Super2__R_Field_7.newGroupInGroup("pnd_Cmn_Rnge_Super2__R_Field_9", "REDEFINE", pnd_Cmn_Rnge_Super2_Pnd_Rt_Short_Key);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Bank_Id = pnd_Cmn_Rnge_Super2__R_Field_9.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_Bank_Id", "#RT-BANK-ID", FieldType.STRING, 
            10);
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Chk_Or_Eft = pnd_Cmn_Rnge_Super2__R_Field_9.newFieldInGroup("pnd_Cmn_Rnge_Super2_Pnd_Rt_Chk_Or_Eft", "#RT-CHK-OR-EFT", 
            FieldType.STRING, 10);

        pnd_Overide_Logo_Fields = localVariables.newGroupInRecord("pnd_Overide_Logo_Fields", "#OVERIDE-LOGO-FIELDS");
        pnd_Overide_Logo_Fields_Over_Logo_Name1 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Name1", "OVER-LOGO-NAME1", 
            FieldType.STRING, 50);
        pnd_Overide_Logo_Fields_Over_Logo_Name2 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Name2", "OVER-LOGO-NAME2", 
            FieldType.STRING, 50);
        pnd_Overide_Logo_Fields_Over_Logo_Addr1 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Addr1", "OVER-LOGO-ADDR1", 
            FieldType.STRING, 50);
        pnd_Overide_Logo_Fields_Over_Logo_Addr2 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Addr2", "OVER-LOGO-ADDR2", 
            FieldType.STRING, 50);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCpol110.initializeValues();

        localVariables.reset();
        pnd_Bank_Super1.setInitialValue("ACBNKA");
        pnd_Logo_Super1.setInitialValue("ACLOGO");
        pnd_Bank_Name_Super2.setInitialValue("ACBNKN");
        pnd_Cmn_Rnge_Super2.setInitialValue("ACBNK#");
        pnd_Overide_Logo_Fields_Over_Logo_Name1.setInitialValue(" ");
        pnd_Overide_Logo_Fields_Over_Logo_Name2.setInitialValue(" ");
        pnd_Overide_Logo_Fields_Over_Logo_Addr1.setInitialValue(" ");
        pnd_Overide_Logo_Fields_Over_Logo_Addr2.setInitialValue(" ");
        pnd_Isn.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cpon110() throws Exception
    {
        super("Cpon110");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        //* * *ERROR-TA  :=  'INFP9000'           /* JH 7/11/2000
        //* *--------- MAIN PROCESS ---------------------------------------------**
        pdaCpoa110.getCpoa110_Cpoa110b().reset();                                                                                                                         //Natural: RESET CPOA110B #OVERIDE-LOGO-FIELDS
        pnd_Overide_Logo_Fields.reset();
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            short decideConditionsMet662 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CPOA110-FUNCTION;//Natural: VALUE ' '
            if (condition((pdaCpoa110.getCpoa110_Cpoa110_Function().equals(" "))))
            {
                decideConditionsMet662++;
                if (condition(pdaCpoa110.getCpoa110_Cpoa110_Source_Code().equals(" ")))                                                                                   //Natural: IF CPOA110.CPOA110-SOURCE-CODE = ' '
                {
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("01");                                                                                           //Natural: ASSIGN CPOA110-RETURN-CODE := '01'
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Source Code Missing");                                                                           //Natural: ASSIGN CPOA110-RETURN-MSG := 'Source Code Missing'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM A100-SETUP-BANK-ACCT-SUPER1
                sub_A100_Setup_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A150-READ-BANK-ACCT-SUPER1
                sub_A150_Read_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM B100-PROCESS-BANK-LOGO-BANK-NAME-TBLS
                sub_B100_Process_Bank_Logo_Bank_Name_Tbls();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  IF RANGE MISSING
                                                                                                                                                                          //Natural: PERFORM C100-GET-BANK-ACCOUNT-COMMON-RANGE
                sub_C100_Get_Bank_Account_Common_Range();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'NEXT'
            else if (condition((pdaCpoa110.getCpoa110_Cpoa110_Function().equals("NEXT"))))
            {
                decideConditionsMet662++;
                //*  OPTION 1
                if (condition(pdaCpoa110.getCpoa110_Cpoa110_Source_Code().equals(" ")))                                                                                   //Natural: IF CPOA110.CPOA110-SOURCE-CODE = ' '
                {
                                                                                                                                                                          //Natural: PERFORM A210-SETUP-BANK-ACCT-SUPER1-NEXT-OPT1
                    sub_A210_Setup_Bank_Acct_Super1_Next_Opt1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  OPTION 2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM A220-SETUP-BANK-ACCT-SUPER1-NEXT-OPT2
                    sub_A220_Setup_Bank_Acct_Super1_Next_Opt2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM A250-READ-BANK-ACCT-SUPER1-NEXT
                sub_A250_Read_Bank_Acct_Super1_Next();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM B100-PROCESS-BANK-LOGO-BANK-NAME-TBLS
                sub_B100_Process_Bank_Logo_Bank_Name_Tbls();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  IF RANGE MISSING
                                                                                                                                                                          //Natural: PERFORM C100-GET-BANK-ACCOUNT-COMMON-RANGE
                sub_C100_Get_Bank_Account_Common_Range();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'NEW SEQUENCE'
            else if (condition((pdaCpoa110.getCpoa110_Cpoa110_Function().equals("NEW SEQUENCE"))))
            {
                decideConditionsMet662++;
                if (condition(pdaCpoa110.getCpoa110_Cpoa110_Source_Code().equals(" ")))                                                                                   //Natural: IF CPOA110.CPOA110-SOURCE-CODE = ' '
                {
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("11");                                                                                           //Natural: ASSIGN CPOA110-RETURN-CODE := '11'
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Source Code Missing");                                                                           //Natural: ASSIGN CPOA110-RETURN-MSG := 'Source Code Missing'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaCpoa110.getCpoa110_Cpoa110_New_Seq_Nbr().equals(getZero())))                                                                             //Natural: IF CPOA110.CPOA110-NEW-SEQ-NBR = 0
                {
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("12");                                                                                           //Natural: ASSIGN CPOA110-RETURN-CODE := '12'
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Next Sequence Number Missing");                                                                  //Natural: ASSIGN CPOA110-RETURN-MSG := 'Next Sequence Number Missing'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM A300-SETUP-BANK-ACCT-SUPER1-NEW-SEQUENCE
                sub_A300_Setup_Bank_Acct_Super1_New_Sequence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A350-READ-BANK-ACCT-SUPER1-NEW-SEQUENCE
                sub_A350_Read_Bank_Acct_Super1_New_Sequence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'NEXT SEQUENCE'
            else if (condition((pdaCpoa110.getCpoa110_Cpoa110_Function().equals("NEXT SEQUENCE"))))
            {
                decideConditionsMet662++;
                if (condition(pdaCpoa110.getCpoa110_Cpoa110_Source_Code().equals(" ")))                                                                                   //Natural: IF CPOA110.CPOA110-SOURCE-CODE = ' '
                {
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("21");                                                                                           //Natural: ASSIGN CPOA110-RETURN-CODE := '21'
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Source Code Missing");                                                                           //Natural: ASSIGN CPOA110-RETURN-MSG := 'Source Code Missing'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                //*  NOT EQUAL TO ZERO
                if (condition(pdaCpoa110.getCpoa110_Cpoa110_New_Seq_Nbr().notEquals(getZero())))                                                                          //Natural: IF CPOA110.CPOA110-NEW-SEQ-NBR NE 0
                {
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("22");                                                                                           //Natural: ASSIGN CPOA110-RETURN-CODE := '22'
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Next Sequence Number Not Required");                                                             //Natural: ASSIGN CPOA110-RETURN-MSG := 'Next Sequence Number Not Required'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM A100-SETUP-BANK-ACCT-SUPER1
                sub_A100_Setup_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A150-READ-BANK-ACCT-SUPER1
                sub_A150_Read_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM B100-PROCESS-BANK-LOGO-BANK-NAME-TBLS
                sub_B100_Process_Bank_Logo_Bank_Name_Tbls();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A400-SETUP-BANK-ACCT-SUPER1-NEXT-SEQUENCE
                sub_A400_Setup_Bank_Acct_Super1_Next_Sequence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A450-READ-BANK-ACCT-SUPER1-NEXT-SEQUENCE
                sub_A450_Read_Bank_Acct_Super1_Next_Sequence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("99");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '99'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Invalid Function");                                                                                  //Natural: ASSIGN CPOA110-RETURN-MSG := 'Invalid Function'
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. )
            }                                                                                                                                                             //Natural: END-DECIDE
            if (true) break PROG;                                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
            //* *--------END OF MAIN PROCESS ---------------------------------------**
            //* **************** PERFORMED SUBROUTINES *******************************
            //*  * * * * * * * FUNCTION BLANK  * * * * * * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*   SETUP SUPER TO ACCESS BANK-ACCOUNT TBL
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A100-SETUP-BANK-ACCT-SUPER1
            //* *-------------------------------------------------------------------**
            //*  ACCESS BANK-ACCOUNT TBL
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A150-READ-BANK-ACCT-SUPER1
            //* **-----------------------------------------------------------------***
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*  *                                                                 * *
            //*  *                                                                 * *
            //*  * * * * * * * * * * * * FUNCTION 'NEXT' * * * * * * * * * * * * * * *
            //* **-----------------------------------------------------------------***
            //*  THIS SETTING USED TO READ FIRST RECORD FROM A BANK-ACCOUNT TABLE.   *
            //*                                                                      *
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A210-SETUP-BANK-ACCT-SUPER1-NEXT-OPT1
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  THIS SETINGS USED TO READ FOLOWING RECORD FROM A BANK-ACCOUNT TABLE *
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A220-SETUP-BANK-ACCT-SUPER1-NEXT-OPT2
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //* *  DEPENDING ON PREVIOUSLY SET SUPER, ACCESS BANK-ACCOUNT TABLE.
            //* *  FOR ONE OF THE TWO REASONS.
            //* *  REASON 1 TO READ FIRST RECORD FROM A BANK-ACCOUNT TABLE.
            //* *  REASON 2 TO READ FOLLOWING RECORD FROM A BANK-ACCOUNT TABLE.
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A250-READ-BANK-ACCT-SUPER1-NEXT
            //* *-------------------------------------------------------------------**
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*  * * * * * * * FUNCTION 'NEW SEQUENCE' * * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*   SETUP SUPER TO ACCESS BANK-ACCOUNT TBL
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A300-SETUP-BANK-ACCT-SUPER1-NEW-SEQUENCE
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  ACCESS BANK-ACCOUNT TBL FOR UPDATE WITH NEW SEQUENCE NBR
            //*  NO RETURN VALUE FROM RA TABLE, ONLY RETURN CODE
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A350-READ-BANK-ACCT-SUPER1-NEW-SEQUENCE
            //* *-------------------------------------------------------------------**
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*  * * * * * * * FUNCTION 'NEXT SEQUENCE'  * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*   SETUP SUPER TO ACCESS BANK-ACCOUNT TBL
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A400-SETUP-BANK-ACCT-SUPER1-NEXT-SEQUENCE
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  ACCESS BANK-ACCOUNT TBL FOR UPDATE WITH NEW SEQUENCE NBR
            //*  NO RETURN VALUE FROM RA TABLE, ONLY RETURN CODE
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A450-READ-BANK-ACCT-SUPER1-NEXT-SEQUENCE
            //* *-------------------------------------------------------------------**
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A500-SETUP-BANK-NAME-SUPER2
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A550-READ-BANK-NAME-SUPER2
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A600-SETUP-LOGO-SUPER1
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A650-READ-LOGO-SUPER1
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A900-USE-OVER-FIELDS
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  THIS SUBROUTINE PROCCESS BANK-LOGO (IF NEEDED) AND BANK-NANE TABLES
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: B100-PROCESS-BANK-LOGO-BANK-NAME-TBLS
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C100-GET-BANK-ACCOUNT-COMMON-RANGE
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*  (PROG.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_A100_Setup_Bank_Acct_Super1() throws Exception                                                                                                       //Natural: A100-SETUP-BANK-ACCT-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(pdaCpoa110.getCpoa110_Cpoa110_Source_Code());                                                                           //Natural: MOVE CPOA110.CPOA110-SOURCE-CODE TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_A150_Read_Bank_Acct_Super1() throws Exception                                                                                                        //Natural: A150-READ-BANK-ACCT-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpol110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaCpol110.getVw_ra().readNextRow("READ01")))
        {
            //*  INTENTIONALLY DONE HERE
            pdaCpoa110.getCpoa110_Cpoa110b().setValuesByName(ldaCpol110.getVw_ra());                                                                                      //Natural: MOVE BY NAME RA TO CPOA110B
            pnd_Overide_Logo_Fields.setValuesByName(ldaCpol110.getVw_ra());                                                                                               //Natural: MOVE BY NAME RA TO #OVERIDE-LOGO-FIELDS
            if (condition(pnd_Bank_Super1_Pnd_Rt_A_I_Ind.equals(ldaCpol110.getRa_Rt_A_I_Ind()) && pnd_Bank_Super1_Pnd_Rt_Table_Id.equals(ldaCpol110.getRa_Rt_Table_Id())  //Natural: IF #BANK-SUPER1.#RT-A-I-IND = RA.RT-A-I-IND AND #BANK-SUPER1.#RT-TABLE-ID = RA.RT-TABLE-ID AND #BANK-SUPER1.#BANK-SOURCE-CODE = RA.BANK-SOURCE-CODE
                && pnd_Bank_Super1_Pnd_Bank_Source_Code.equals(ldaCpol110.getRa_Bank_Source_Code())))
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '00'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("02");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '02'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Source Code Not Found               ");                                                              //Natural: ASSIGN CPOA110-RETURN-MSG := 'Source Code Not Found               '
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_A210_Setup_Bank_Acct_Super1_Next_Opt1() throws Exception                                                                                             //Natural: A210-SETUP-BANK-ACCT-SUPER1-NEXT-OPT1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(" ");                                                                                                                   //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_A220_Setup_Bank_Acct_Super1_Next_Opt2() throws Exception                                                                                             //Natural: A220-SETUP-BANK-ACCT-SUPER1-NEXT-OPT2
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bank_Super1_Pnd_Bank_Source_Code.setValue(pdaCpoa110.getCpoa110_Cpoa110_Source_Code());                                                                       //Natural: MOVE CPOA110.CPOA110-SOURCE-CODE TO #BANK-SUPER1.#BANK-SOURCE-CODE
        pnd_Bank_Super1_Pnd_Filler1.moveAll("H'FF'");                                                                                                                     //Natural: MOVE ALL H'FF' TO #BANK-SUPER1.#FILLER1
        pnd_Bank_Super1_Pnd_Rt_Long_Key.moveAll("H'FF'");                                                                                                                 //Natural: MOVE ALL H'FF' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_A250_Read_Bank_Acct_Super1_Next() throws Exception                                                                                                   //Natural: A250-READ-BANK-ACCT-SUPER1-NEXT
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpol110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "READ02",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ02:
        while (condition(ldaCpol110.getVw_ra().readNextRow("READ02")))
        {
            //*  INTENTIONALLY DONE HERE
            pdaCpoa110.getCpoa110_Cpoa110b().setValuesByName(ldaCpol110.getVw_ra());                                                                                      //Natural: MOVE BY NAME RA TO CPOA110B
            pnd_Overide_Logo_Fields.setValuesByName(ldaCpol110.getVw_ra());                                                                                               //Natural: MOVE BY NAME RA TO #OVERIDE-LOGO-FIELDS
            pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                                   //Natural: ASSIGN CPOA110-RETURN-CODE := '00'
            if (condition(((pnd_Bank_Super1_Pnd_Rt_A_I_Ind.notEquals(ldaCpol110.getRa_Rt_A_I_Ind())) || (pnd_Bank_Super1_Pnd_Rt_Table_Id.notEquals(ldaCpol110.getRa_Rt_Table_Id()))))) //Natural: IF ( ( #BANK-SUPER1.#RT-A-I-IND NE RA.RT-A-I-IND ) OR ( #BANK-SUPER1.#RT-TABLE-ID NE RA.RT-TABLE-ID ) )
            {
                pdaCpoa110.getCpoa110_Cpoa110_Function().setValue("END");                                                                                                 //Natural: MOVE 'END' TO CPOA110-FUNCTION
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_A300_Setup_Bank_Acct_Super1_New_Sequence() throws Exception                                                                                          //Natural: A300-SETUP-BANK-ACCT-SUPER1-NEW-SEQUENCE
    {
        if (BLNatReinput.isReinput()) return;

        //*   SETUP SUPER
        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(pdaCpoa110.getCpoa110_Cpoa110_Source_Code());                                                                           //Natural: MOVE CPOA110.CPOA110-SOURCE-CODE TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_A350_Read_Bank_Acct_Super1_New_Sequence() throws Exception                                                                                           //Natural: A350-READ-BANK-ACCT-SUPER1-NEW-SEQUENCE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Isn.reset();                                                                                                                                                  //Natural: RESET #ISN
        ldaCpol110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "R1",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        R1:
        while (condition(ldaCpol110.getVw_ra().readNextRow("R1")))
        {
            pnd_Isn.setValue(ldaCpol110.getVw_ra().getAstISN("R1"));                                                                                                      //Natural: MOVE *ISN TO #ISN
            if (condition(pnd_Bank_Super1_Pnd_Rt_A_I_Ind.equals(ldaCpol110.getRa_Rt_A_I_Ind()) && pnd_Bank_Super1_Pnd_Rt_Table_Id.equals(ldaCpol110.getRa_Rt_Table_Id())  //Natural: IF #BANK-SUPER1.#RT-A-I-IND = RA.RT-A-I-IND AND #BANK-SUPER1.#RT-TABLE-ID = RA.RT-TABLE-ID AND #BANK-SUPER1.#BANK-SOURCE-CODE = RA.BANK-SOURCE-CODE
                && pnd_Bank_Super1_Pnd_Bank_Source_Code.equals(ldaCpol110.getRa_Bank_Source_Code())))
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '00'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("13");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '13'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Source Code Not Found");                                                                             //Natural: ASSIGN CPOA110-RETURN-MSG := 'Source Code Not Found'
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pdaCpoa110.getCpoa110_Cpoa110_New_Seq_Nbr().greater(ldaCpol110.getRa_Start_Seq())) && (pdaCpoa110.getCpoa110_Cpoa110_New_Seq_Nbr().lessOrEqual(ldaCpol110.getRa_End_Seq()))))) //Natural: IF ( ( CPOA110.CPOA110-NEW-SEQ-NBR GT RA.START-SEQ ) AND ( CPOA110.CPOA110-NEW-SEQ-NBR LE RA.END-SEQ ) )
            {
                G1:                                                                                                                                                       //Natural: GET RAU #ISN
                ldaCpol110.getVw_rau().readByID(pnd_Isn.getLong(), "G1");
                ldaCpol110.getRau_Start_Seq().setValue(pdaCpoa110.getCpoa110_Cpoa110_New_Seq_Nbr());                                                                      //Natural: MOVE CPOA110.CPOA110-NEW-SEQ-NBR TO RAU.START-SEQ
                ldaCpol110.getVw_rau().updateDBRow("G1");                                                                                                                 //Natural: UPDATE ( G1. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("14");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '14'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Next Sequence Number is Out of Range");                                                              //Natural: ASSIGN CPOA110-RETURN-MSG := 'Next Sequence Number is Out of Range'
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_A400_Setup_Bank_Acct_Super1_Next_Sequence() throws Exception                                                                                         //Natural: A400-SETUP-BANK-ACCT-SUPER1-NEXT-SEQUENCE
    {
        if (BLNatReinput.isReinput()) return;

        //*   SETUP SUPER
        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(pdaCpoa110.getCpoa110_Cpoa110_Source_Code());                                                                           //Natural: MOVE CPOA110.CPOA110-SOURCE-CODE TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_A450_Read_Bank_Acct_Super1_Next_Sequence() throws Exception                                                                                          //Natural: A450-READ-BANK-ACCT-SUPER1-NEXT-SEQUENCE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Isn.reset();                                                                                                                                                  //Natural: RESET #ISN
        ldaCpol110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "R2",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        R2:
        while (condition(ldaCpol110.getVw_ra().readNextRow("R2")))
        {
            pnd_Isn.setValue(ldaCpol110.getVw_ra().getAstISN("R2"));                                                                                                      //Natural: MOVE *ISN TO #ISN
            if (condition(pnd_Bank_Super1_Pnd_Rt_A_I_Ind.notEquals(ldaCpol110.getRa_Rt_A_I_Ind()) || pnd_Bank_Super1_Pnd_Rt_Table_Id.notEquals(ldaCpol110.getRa_Rt_Table_Id())  //Natural: IF #BANK-SUPER1.#RT-A-I-IND NE RA.RT-A-I-IND OR #BANK-SUPER1.#RT-TABLE-ID NE RA.RT-TABLE-ID OR #BANK-SUPER1.#BANK-SOURCE-CODE NE RA.BANK-SOURCE-CODE
                || pnd_Bank_Super1_Pnd_Bank_Source_Code.notEquals(ldaCpol110.getRa_Bank_Source_Code())))
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("23");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '23'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Source Code Missing");                                                                               //Natural: ASSIGN CPOA110-RETURN-MSG := 'Source Code Missing'
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  AR
            if (condition(ldaCpol110.getRa_Start_End_Seq().notEquals(" ") && DbsUtil.maskMatches(ldaCpol110.getRa_Start_Seq(),"9999999999") && DbsUtil.maskMatches(ldaCpol110.getRa_End_Seq(),"9999999999")  //Natural: IF RA.START-END-SEQ NE ' ' AND RA.START-SEQ = MASK ( 9999999999 ) AND RA.END-SEQ = MASK ( 9999999999 ) AND RA.START-SEQ < 9999999999
                && ldaCpol110.getRa_Start_Seq().less(new DbsDecimal("9999999999"))))
            {
                G2:                                                                                                                                                       //Natural: GET RAU #ISN
                ldaCpol110.getVw_rau().readByID(pnd_Isn.getLong(), "G2");
                ldaCpol110.getRau_Start_Seq().nadd(1);                                                                                                                    //Natural: ADD 1 TO RAU.START-SEQ
                //*  AY,IB
                pdaCpoa110.getCpoa110_End_Seq().setValue(ldaCpol110.getRau_End_Seq());                                                                                    //Natural: MOVE RAU.END-SEQ TO CPOA110.END-SEQ
                //*  6/5/03
                pdaCpoa110.getCpoa110_Cpoa110_New_Seq_Nbr().setValue(ldaCpol110.getRau_Start_Seq());                                                                      //Natural: MOVE RAU.START-SEQ TO CPOA110.CPOA110-NEW-SEQ-NBR
                //*  AR
                if (condition(ldaCpol110.getRau_Start_Seq().lessOrEqual(ldaCpol110.getRau_End_Seq())))                                                                    //Natural: IF RAU.START-SEQ <= RAU.END-SEQ
                {
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                           //Natural: MOVE '00' TO CPOA110-RETURN-CODE
                    ldaCpol110.getVw_rau().updateDBRow("G2");                                                                                                             //Natural: UPDATE ( G2. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("24");                                                                                           //Natural: ASSIGN CPOA110-RETURN-CODE := '24'
                    pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Next Sequence Number is Out of Range");                                                          //Natural: ASSIGN CPOA110-RETURN-MSG := 'Next Sequence Number is Out of Range'
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. )
                    Global.setEscapeCode(EscapeType.Bottom, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  IF RANGE IS ZERO, FIND THE COMMON RANGE TABLE
        pnd_Cmn_Rnge_Super2.resetInitial();                                                                                                                               //Natural: RESET INITIAL #CMN-RNGE-SUPER2
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Routing_Id.setValue(pdaCpoa110.getCpoa110_Bank_Routing());                                                                             //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-ROUTING-ID := CPOA110.BANK-ROUTING
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Account_Nbr.setValue(pdaCpoa110.getCpoa110_Bank_Account());                                                                            //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-ACCOUNT-NBR := CPOA110.BANK-ACCOUNT
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Bank_Id.setValue(pdaCpoa110.getCpoa110_Bank_Transmission_Cde());                                                                       //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-BANK-ID := CPOA110.BANK-TRANSMISSION-CDE
        //*  AR
        if (condition(pdaCpoa110.getCpoa110_Check_Acct_Ind().equals("Y") && pdaCpoa110.getCpoa110_Eft_Acct_Ind().notEquals("Y")))                                         //Natural: IF CPOA110.CHECK-ACCT-IND = 'Y' AND CPOA110.EFT-ACCT-IND NE 'Y'
        {
            pnd_Cmn_Rnge_Super2_Pnd_Rt_Chk_Or_Eft.setValue("CHECK");                                                                                                      //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-CHK-OR-EFT := 'CHECK'
        }                                                                                                                                                                 //Natural: END-IF
        //*  AR
        if (condition(pdaCpoa110.getCpoa110_Eft_Acct_Ind().equals("Y") && pdaCpoa110.getCpoa110_Check_Acct_Ind().notEquals("Y")))                                         //Natural: IF CPOA110.EFT-ACCT-IND = 'Y' AND CPOA110.CHECK-ACCT-IND NE 'Y'
        {
            pnd_Cmn_Rnge_Super2_Pnd_Rt_Chk_Or_Eft.setValue("EFT");                                                                                                        //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-CHK-OR-EFT := 'EFT'
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpol110.getVw_rc().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RC BY RT-SUPER2 STARTING FROM #CMN-RNGE-SUPER2
        (
        "R3",
        new Wc[] { new Wc("RT_SUPER2", ">=", pnd_Cmn_Rnge_Super2, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER2", "ASC") },
        1
        );
        R3:
        while (condition(ldaCpol110.getVw_rc().readNextRow("R3")))
        {
            pnd_Isn.setValue(ldaCpol110.getVw_rc().getAstISN("R3"));                                                                                                      //Natural: MOVE *ISN TO #ISN
            if (condition(pnd_Cmn_Rnge_Super2_Pnd_Rt_A_I_Ind.notEquals(ldaCpol110.getRc_Rt_A_I_Ind()) || pnd_Cmn_Rnge_Super2_Pnd_Rt_Table_Id.notEquals(ldaCpol110.getRc_Rt_Table_Id())  //Natural: IF #CMN-RNGE-SUPER2.#RT-A-I-IND NE RC.RT-A-I-IND OR #CMN-RNGE-SUPER2.#RT-TABLE-ID NE RC.RT-TABLE-ID OR #CMN-RNGE-SUPER2.#RT-LONG-KEY NE RC.RT-LONG-KEY OR #CMN-RNGE-SUPER2.#RT-SHORT-KEY NE RC.RT-SHORT-KEY
                || pnd_Cmn_Rnge_Super2_Pnd_Rt_Long_Key.notEquals(ldaCpol110.getRc_Rt_Long_Key()) || pnd_Cmn_Rnge_Super2_Pnd_Rt_Short_Key.notEquals(ldaCpol110.getRc_Rt_Short_Key())))
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("25");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '25'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Common Bank Account Range Missing   ");                                                              //Natural: ASSIGN CPOA110-RETURN-MSG := 'Common Bank Account Range Missing   '
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            G3:                                                                                                                                                           //Natural: GET RCU #ISN
            ldaCpol110.getVw_rcu().readByID(pnd_Isn.getLong(), "G3");
            //*  AR
            //*  AR
            if (condition(DbsUtil.maskMatches(ldaCpol110.getRcu_Next_Seq_Nbr(),"9999999999") && ldaCpol110.getRcu_Next_Seq_Nbr().less(new DbsDecimal("9999999999"))))     //Natural: IF RCU.NEXT-SEQ-NBR = MASK ( 9999999999 ) AND RCU.NEXT-SEQ-NBR < 9999999999
            {
                ldaCpol110.getRcu_Next_Seq_Nbr().nadd(1);                                                                                                                 //Natural: ADD 1 TO RCU.NEXT-SEQ-NBR
            }                                                                                                                                                             //Natural: END-IF
            //* AY,IB 6/5/03
            pdaCpoa110.getCpoa110_End_Seq().setValue(ldaCpol110.getRcu_End_Seq_Nbr());                                                                                    //Natural: MOVE RCU.END-SEQ-NBR TO CPOA110.END-SEQ
            //* AY,IB 6/5/03
            pdaCpoa110.getCpoa110_Cpoa110_New_Seq_Nbr().setValue(ldaCpol110.getRcu_Next_Seq_Nbr());                                                                       //Natural: MOVE RCU.NEXT-SEQ-NBR TO CPOA110.CPOA110-NEW-SEQ-NBR
            //*  AR
            //*  AR
            if (condition(ldaCpol110.getRcu_Next_Seq_Nbr().greaterOrEqual(ldaCpol110.getRcu_Start_Seq_Nbr()) && ldaCpol110.getRcu_Next_Seq_Nbr().lessOrEqual(ldaCpol110.getRcu_End_Seq_Nbr()))) //Natural: IF RCU.NEXT-SEQ-NBR = RCU.START-SEQ-NBR THRU RCU.END-SEQ-NBR
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '00'
                ldaCpol110.getVw_rcu().updateDBRow("G3");                                                                                                                 //Natural: UPDATE ( G3. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("26");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '26'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Next Sequence Number is Out of Range");                                                              //Natural: ASSIGN CPOA110-RETURN-MSG := 'Next Sequence Number is Out of Range'
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_A500_Setup_Bank_Name_Super2() throws Exception                                                                                                       //Natural: A500-SETUP-BANK-NAME-SUPER2
    {
        if (BLNatReinput.isReinput()) return;

        //*   SETUP SUPER
        pnd_Bank_Name_Super2_Pnd_Rt_Long_Key.setValue(pdaCpoa110.getCpoa110_Bank_Routing());                                                                              //Natural: MOVE CPOA110.BANK-ROUTING TO #BANK-NAME-SUPER2.#RT-LONG-KEY
        pnd_Bank_Name_Super2_Pnd_Rt_Short_Key.setValue("  ");                                                                                                             //Natural: MOVE '  ' TO #BANK-NAME-SUPER2.#RT-SHORT-KEY
    }
    private void sub_A550_Read_Bank_Name_Super2() throws Exception                                                                                                        //Natural: A550-READ-BANK-NAME-SUPER2
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpol110.getVw_rn().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RN BY RT-SUPER2 STARTING FROM #BANK-NAME-SUPER2
        (
        "READ03",
        new Wc[] { new Wc("RT_SUPER2", ">=", pnd_Bank_Name_Super2, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER2", "ASC") },
        1
        );
        READ03:
        while (condition(ldaCpol110.getVw_rn().readNextRow("READ03")))
        {
            pdaCpoa110.getCpoa110_Cpoa110b().setValuesByName(ldaCpol110.getVw_rn());                                                                                      //Natural: MOVE BY NAME RN TO CPOA110.CPOA110B
            if (condition(pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind.equals(ldaCpol110.getRn_Rt_A_I_Ind()) && pnd_Bank_Name_Super2_Pnd_Rt_Table_Id.equals(ldaCpol110.getRn_Rt_Table_Id())  //Natural: IF #BANK-NAME-SUPER2.#RT-A-I-IND = RN.RT-A-I-IND AND #BANK-NAME-SUPER2.#RT-TABLE-ID = RN.RT-TABLE-ID AND #BANK-NAME-SUPER2.#BANK-ROUTING = RN.BANK-ROUTING
                && pnd_Bank_Name_Super2_Pnd_Bank_Routing.equals(ldaCpol110.getRn_Bank_Routing())))
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '00'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("04");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '04'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Bank Table Not Found");                                                                              //Natural: ASSIGN CPOA110-RETURN-MSG := 'Bank Table Not Found'
                //*   MOVE BACK BANK-ROUTING FROM ACCOUNT TABLE TO PDA
                pdaCpoa110.getCpoa110_Bank_Routing().setValue(pnd_Bank_Name_Super2_Pnd_Bank_Routing);                                                                     //Natural: MOVE #BANK-NAME-SUPER2.#BANK-ROUTING TO CPOA110.BANK-ROUTING
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_A600_Setup_Logo_Super1() throws Exception                                                                                                            //Natural: A600-SETUP-LOGO-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        //*   SETUP SUPER
        pnd_Logo_Super1_Pnd_Rt_Short_Key.setValue(pdaCpoa110.getCpoa110_Bank_Logo_Key());                                                                                 //Natural: MOVE CPOA110.BANK-LOGO-KEY TO #LOGO-SUPER1.#RT-SHORT-KEY
        pnd_Logo_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #LOGO-SUPER1.#RT-LONG-KEY
    }
    private void sub_A650_Read_Logo_Super1() throws Exception                                                                                                             //Natural: A650-READ-LOGO-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpol110.getVw_rl().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RL BY RT-SUPER1 STARTING FROM #LOGO-SUPER1
        (
        "READ04",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Logo_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ04:
        while (condition(ldaCpol110.getVw_rl().readNextRow("READ04")))
        {
            pdaCpoa110.getCpoa110_Cpoa110b().setValuesByName(ldaCpol110.getVw_rl());                                                                                      //Natural: MOVE BY NAME RL TO CPOA110.CPOA110B
            if (condition(pnd_Logo_Super1_Pnd_Rt_A_I_Ind.equals(ldaCpol110.getRl_Rt_A_I_Ind()) && pnd_Logo_Super1_Pnd_Rt_Table_Id.equals(ldaCpol110.getRl_Rt_Table_Id())  //Natural: IF #LOGO-SUPER1.#RT-A-I-IND = RL.RT-A-I-IND AND #LOGO-SUPER1.#RT-TABLE-ID = RL.RT-TABLE-ID AND #LOGO-SUPER1.#BANK-LOGO-KEY = RL.BANK-LOGO-KEY
                && pnd_Logo_Super1_Pnd_Bank_Logo_Key.equals(ldaCpol110.getRl_Bank_Logo_Key())))
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '00'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("03");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '03'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Bank Logo Not Found                 ");                                                              //Natural: ASSIGN CPOA110-RETURN-MSG := 'Bank Logo Not Found                 '
                //*   MOVE BACK LOGO-KEY FROM ACCOUNT TABLE TO PDA
                pdaCpoa110.getCpoa110_Bank_Logo_Key().setValue(pnd_Logo_Super1_Pnd_Bank_Logo_Key);                                                                        //Natural: MOVE #LOGO-SUPER1.#BANK-LOGO-KEY TO CPOA110.BANK-LOGO-KEY
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_A900_Use_Over_Fields() throws Exception                                                                                                              //Natural: A900-USE-OVER-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pdaCpoa110.getCpoa110_Bank_Logo_Name1().setValue(pnd_Overide_Logo_Fields_Over_Logo_Name1);                                                                        //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-NAME1 TO CPOA110.BANK-LOGO-NAME1
        pdaCpoa110.getCpoa110_Bank_Logo_Name2().setValue(pnd_Overide_Logo_Fields_Over_Logo_Name2);                                                                        //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-NAME2 TO CPOA110.BANK-LOGO-NAME2
        pdaCpoa110.getCpoa110_Bank_Logo_Address1().setValue(pnd_Overide_Logo_Fields_Over_Logo_Addr1);                                                                     //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-ADDR1 TO CPOA110.BANK-LOGO-ADDRESS1
        pdaCpoa110.getCpoa110_Bank_Logo_Address2().setValue(pnd_Overide_Logo_Fields_Over_Logo_Addr2);                                                                     //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-ADDR2 TO CPOA110.BANK-LOGO-ADDRESS2
    }
    private void sub_B100_Process_Bank_Logo_Bank_Name_Tbls() throws Exception                                                                                             //Natural: B100-PROCESS-BANK-LOGO-BANK-NAME-TBLS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaCpoa110.getCpoa110_Bank_Routing().equals(" ")))                                                                                                  //Natural: IF CPOA110.BANK-ROUTING = ' '
        {
            pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("06");                                                                                                   //Natural: ASSIGN CPOA110-RETURN-CODE := '06'
            pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Bank Table Not Found");                                                                                  //Natural: ASSIGN CPOA110-RETURN-MSG := 'Bank Table Not Found'
            Global.setEscape(true);                                                                                                                                       //Natural: ESCAPE BOTTOM ( PROG. )
            Global.setEscapeCode(EscapeType.Bottom, "PROG");
            if (true) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM A500-SETUP-BANK-NAME-SUPER2
            sub_A500_Setup_Bank_Name_Super2();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM A550-READ-BANK-NAME-SUPER2
            sub_A550_Read_Bank_Name_Super2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Overide_Logo_Fields_Over_Logo_Name1.notEquals(" ")))                                                                                            //Natural: IF #OVERIDE-LOGO-FIELDS.OVER-LOGO-NAME1 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM A900-USE-OVER-FIELDS
            sub_A900_Use_Over_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaCpoa110.getCpoa110_Bank_Logo_Key().equals(" ")))                                                                                             //Natural: IF CPOA110.BANK-LOGO-KEY = ' '
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("05");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '05'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Bank Logo Not Found");                                                                               //Natural: ASSIGN CPOA110-RETURN-MSG := 'Bank Logo Not Found'
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM A600-SETUP-LOGO-SUPER1
                sub_A600_Setup_Logo_Super1();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM A650-READ-LOGO-SUPER1
                sub_A650_Read_Logo_Super1();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_C100_Get_Bank_Account_Common_Range() throws Exception                                                                                                //Natural: C100-GET-BANK-ACCOUNT-COMMON-RANGE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaCpoa110.getCpoa110_Start_Seq().greater(getZero()) || pdaCpoa110.getCpoa110_End_Seq().greater(getZero())))                                        //Natural: IF CPOA110.START-SEQ > 0 OR CPOA110.END-SEQ > 0 THEN
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF RANGE IS ZERO, FIND THE COMMON RANGE TABLE
        pnd_Cmn_Rnge_Super2.resetInitial();                                                                                                                               //Natural: RESET INITIAL #CMN-RNGE-SUPER2
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Routing_Id.setValue(pdaCpoa110.getCpoa110_Bank_Routing());                                                                             //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-ROUTING-ID := CPOA110.BANK-ROUTING
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Account_Nbr.setValue(pdaCpoa110.getCpoa110_Bank_Account());                                                                            //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-ACCOUNT-NBR := CPOA110.BANK-ACCOUNT
        pnd_Cmn_Rnge_Super2_Pnd_Rt_Bank_Id.setValue(pdaCpoa110.getCpoa110_Bank_Transmission_Cde());                                                                       //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-BANK-ID := CPOA110.BANK-TRANSMISSION-CDE
        //*  AR
        if (condition(pdaCpoa110.getCpoa110_Check_Acct_Ind().equals("Y") && pdaCpoa110.getCpoa110_Eft_Acct_Ind().notEquals("Y")))                                         //Natural: IF CPOA110.CHECK-ACCT-IND = 'Y' AND CPOA110.EFT-ACCT-IND NE 'Y'
        {
            pnd_Cmn_Rnge_Super2_Pnd_Rt_Chk_Or_Eft.setValue("CHECK");                                                                                                      //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-CHK-OR-EFT := 'CHECK'
        }                                                                                                                                                                 //Natural: END-IF
        //*  AR
        if (condition(pdaCpoa110.getCpoa110_Eft_Acct_Ind().equals("Y") && pdaCpoa110.getCpoa110_Check_Acct_Ind().notEquals("Y")))                                         //Natural: IF CPOA110.EFT-ACCT-IND = 'Y' AND CPOA110.CHECK-ACCT-IND NE 'Y'
        {
            pnd_Cmn_Rnge_Super2_Pnd_Rt_Chk_Or_Eft.setValue("EFT");                                                                                                        //Natural: ASSIGN #CMN-RNGE-SUPER2.#RT-CHK-OR-EFT := 'EFT'
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpol110.getVw_rc().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RC BY RT-SUPER2 STARTING FROM #CMN-RNGE-SUPER2
        (
        "READ05",
        new Wc[] { new Wc("RT_SUPER2", ">=", pnd_Cmn_Rnge_Super2, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER2", "ASC") },
        1
        );
        READ05:
        while (condition(ldaCpol110.getVw_rc().readNextRow("READ05")))
        {
            if (condition(pnd_Cmn_Rnge_Super2_Pnd_Rt_A_I_Ind.notEquals(ldaCpol110.getRc_Rt_A_I_Ind()) || pnd_Cmn_Rnge_Super2_Pnd_Rt_Table_Id.notEquals(ldaCpol110.getRc_Rt_Table_Id())  //Natural: IF #CMN-RNGE-SUPER2.#RT-A-I-IND NE RC.RT-A-I-IND OR #CMN-RNGE-SUPER2.#RT-TABLE-ID NE RC.RT-TABLE-ID OR #CMN-RNGE-SUPER2.#RT-LONG-KEY NE RC.RT-LONG-KEY OR #CMN-RNGE-SUPER2.#RT-SHORT-KEY NE RC.RT-SHORT-KEY
                || pnd_Cmn_Rnge_Super2_Pnd_Rt_Long_Key.notEquals(ldaCpol110.getRc_Rt_Long_Key()) || pnd_Cmn_Rnge_Super2_Pnd_Rt_Short_Key.notEquals(ldaCpol110.getRc_Rt_Short_Key())))
            {
                pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("25");                                                                                               //Natural: ASSIGN CPOA110-RETURN-CODE := '25'
                pdaCpoa110.getCpoa110_Cpoa110_Return_Msg().setValue("Common Bank Account Range Missing   ");                                                              //Natural: ASSIGN CPOA110-RETURN-MSG := 'Common Bank Account Range Missing   '
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            pdaCpoa110.getCpoa110_Next_Seq().setValue(ldaCpol110.getRc_Next_Seq_Nbr());                                                                                   //Natural: ASSIGN CPOA110.NEXT-SEQ = RC.NEXT-SEQ-NBR
            pdaCpoa110.getCpoa110_Start_Seq().setValue(ldaCpol110.getRc_Start_Seq_Nbr());                                                                                 //Natural: ASSIGN CPOA110.START-SEQ = RC.START-SEQ-NBR
            pdaCpoa110.getCpoa110_End_Seq().setValue(ldaCpol110.getRc_End_Seq_Nbr());                                                                                     //Natural: ASSIGN CPOA110.END-SEQ = RC.END-SEQ-NBR
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
