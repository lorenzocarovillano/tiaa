/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:55 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnadd2
************************************************************
**        * FILE NAME            : Fcpnadd2.java
**        * CLASS NAME           : Fcpnadd2
**        * INSTANCE NAME        : Fcpnadd2
************************************************************
************************************************************************
* PROGRAM  : FCPNADD2
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* CREATED  : 11/29/95
* FUNCTION : FCP-CONS-ADDR FILE LOOKUP.
* : CLONED AND MODIFIED TO REPAIR C/S/R/ LOOKUP PROBLEM IN
* : FCPP835 CALL - THIS MODULE HAS BEEN SYNCED TO MIMIC THE TECHNIQUE
* : USED IN ON LINE MODULE FCPN448
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnadd2 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpaaddr pdaFcpaaddr;
    private LdaFcpladdr ldaFcpladdr;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Addr_Key;

    private DbsGroup pnd_Addr_Key__R_Field_1;
    private DbsField pnd_Addr_Key_Pnd_Addr_Ppcn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Payee;
    private DbsField pnd_Addr_Key_Pnd_Addr_Orgn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Invrs;
    private DbsField pnd_Addr_Key_Pnd_Addr_Seq;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());

        // parameters
        parameters = new DbsRecord();
        pdaFcpaaddr = new PdaFcpaaddr(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Addr_Key = localVariables.newFieldInRecord("pnd_Addr_Key", "#ADDR-KEY", FieldType.STRING, 31);

        pnd_Addr_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Addr_Key__R_Field_1", "REDEFINE", pnd_Addr_Key);
        pnd_Addr_Key_Pnd_Addr_Ppcn = pnd_Addr_Key__R_Field_1.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Ppcn", "#ADDR-PPCN", FieldType.STRING, 10);
        pnd_Addr_Key_Pnd_Addr_Payee = pnd_Addr_Key__R_Field_1.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Payee", "#ADDR-PAYEE", FieldType.STRING, 4);
        pnd_Addr_Key_Pnd_Addr_Orgn = pnd_Addr_Key__R_Field_1.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Orgn", "#ADDR-ORGN", FieldType.STRING, 2);
        pnd_Addr_Key_Pnd_Addr_Invrs = pnd_Addr_Key__R_Field_1.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Invrs", "#ADDR-INVRS", FieldType.NUMERIC, 8);
        pnd_Addr_Key_Pnd_Addr_Seq = pnd_Addr_Key__R_Field_1.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Seq", "#ADDR-SEQ", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpladdr.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnadd2() throws Exception
    {
        super("Fcpnadd2");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Addr_Key_Pnd_Addr_Seq.setValue(pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr());                                                                                    //Natural: ASSIGN #ADDR-SEQ := ADDR.PYMNT-PRCSS-SEQ-NBR
        pnd_Addr_Key_Pnd_Addr_Ppcn.setValue(pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr());                                                                           //Natural: ASSIGN #ADDR-PPCN := ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR
        pnd_Addr_Key_Pnd_Addr_Payee.setValue(pdaFcpaaddr.getAddr_Cntrct_Payee_Cde());                                                                                     //Natural: ASSIGN #ADDR-PAYEE := ADDR.CNTRCT-PAYEE-CDE
        pnd_Addr_Key_Pnd_Addr_Orgn.setValue(pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde());                                                                                       //Natural: ASSIGN #ADDR-ORGN := ADDR.CNTRCT-ORGN-CDE
        pnd_Addr_Key_Pnd_Addr_Invrs.setValue(pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte());                                                                                    //Natural: ASSIGN #ADDR-INVRS := ADDR.CNTRCT-INVRSE-DTE
        //* *
        //* *
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) FCP-CONS-ADDR WITH PPCN-PAYEE-ORGN-INVRS-SEQ = #ADDR-KEY
        (
        "RADDR",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", "=", pnd_Addr_Key, WcType.WITH) },
        1
        );
        RADDR:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("RADDR")))
        {
            ldaFcpladdr.getVw_fcp_Cons_Addr().setIfNotFoundControlFlag(false);
            pdaFcpaaddr.getAddr().setValuesByName(ldaFcpladdr.getVw_fcp_Cons_Addr());                                                                                     //Natural: MOVE BY NAME FCP-CONS-ADDR TO ADDR
            pdaFcpaaddr.getAddr_C_Pymnt_Nme_And_Addr_Grp().setValue(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp());                                     //Natural: MOVE C*PYMNT-NME-AND-ADDR-GRP TO C-PYMNT-NME-AND-ADDR-GRP
            pdaFcpaaddr.getAddr_C_Pymnt_Addr_Chg_Grp().setValue(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Addr_Chg_Grp());                                             //Natural: MOVE C*PYMNT-ADDR-CHG-GRP TO C-PYMNT-ADDR-CHG-GRP
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Addr_Key_Pnd_Addr_Invrs.nadd(1);                                                                                                                              //Natural: ASSIGN #ADDR-INVRS := #ADDR-INVRS + 1
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #ADDR-KEY
        (
        "READ01",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("READ01")))
        {
            pdaFcpaaddr.getAddr_Work_Fields_Addr_Isn().setValue(ldaFcpladdr.getVw_fcp_Cons_Addr().getAstISN("Read01"));                                                   //Natural: MOVE *ISN TO ADDR-ISN
            pdaFcpaaddr.getAddr().setValuesByName(ldaFcpladdr.getVw_fcp_Cons_Addr());                                                                                     //Natural: MOVE BY NAME FCP-CONS-ADDR TO ADDR
            pdaFcpaaddr.getAddr_C_Pymnt_Nme_And_Addr_Grp().setValue(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp());                                     //Natural: MOVE C*PYMNT-NME-AND-ADDR-GRP TO C-PYMNT-NME-AND-ADDR-GRP
            pdaFcpaaddr.getAddr_C_Pymnt_Addr_Chg_Grp().setValue(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Addr_Chg_Grp());                                             //Natural: MOVE C*PYMNT-ADDR-CHG-GRP TO C-PYMNT-ADDR-CHG-GRP
            pdaFcpaaddr.getAddr_Work_Fields_Addr_Return_Cde().reset();                                                                                                    //Natural: RESET ADDR-RETURN-CDE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().getAstCOUNTER().equals(getZero())))                                                                               //Natural: IF *COUNTER ( RADDR. ) = 0
        {
            pdaFcpaaddr.getAddr_Work_Fields_Addr_Return_Cde().setValue(91);                                                                                               //Natural: MOVE 91 TO ADDR-RETURN-CDE
            if (condition(pdaFcpaaddr.getAddr_Work_Fields_Addr_Abend_Ind().getBoolean()))                                                                                 //Natural: IF ADDR-ABEND-IND
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"ADDRESS RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIAA PPCN#  :",pnd_Addr_Key_Pnd_Addr_Ppcn,new  //Natural: WRITE '***' 25T 'ADDRESS RECORD IS NOT FOUND' 77T '***' / '***' 25T 'TIAA PPCN#  :' #ADDR-PPCN 77T '***' / '***' 25T 'PAYEE       :' #ADDR-PAYEE 77T '***' / '***' 25T 'ORIGIN      :' #ADDR-ORGN 77T '***' / '***' 25T 'INVERSE DATE:' #ADDR-INVRS 77T '***' / '***' 25T 'SEQUENCE#   :' #ADDR-SEQ 77T '***'
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PAYEE       :",pnd_Addr_Key_Pnd_Addr_Payee,new TabSetting(77),"***",NEWLINE,"***",new 
                    TabSetting(25),"ORIGIN      :",pnd_Addr_Key_Pnd_Addr_Orgn,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"INVERSE DATE:",pnd_Addr_Key_Pnd_Addr_Invrs,new 
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"SEQUENCE#   :",pnd_Addr_Key_Pnd_Addr_Seq,new TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(pdaFcpaaddr.getAddr_Work_Fields_Addr_Return_Cde());  if (true) return;                                                                  //Natural: TERMINATE ADDR-RETURN-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
