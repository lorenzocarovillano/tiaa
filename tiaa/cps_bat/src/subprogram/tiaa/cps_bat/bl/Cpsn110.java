/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:10:02 PM
**        * FROM NATURAL SUBPROGRAM : Cpsn110
************************************************************
**        * FILE NAME            : Cpsn110.java
**        * CLASS NAME           : Cpsn110
**        * INSTANCE NAME        : Cpsn110
************************************************************
***********************************************************************
** PROGRAM:     CPSN110                                              **
** SYSTEM:      ACCOUNTS PAYABLE                                     **
** DATE:        02/22/2000                                           **
** AUTHOR:      LEON GURTOVNIK                                       **
** DESCRIPTION: PROGRAM EXTRACT INFORMATION FROM 'REFERENDCE-TABLE'  **
**                                                                   **
** ACCESS:  REFERENCE-TABLE USING INPUT PARAMETERS                   **
**                                                                   **
** RETRIEVE INFORMATION FROM THIS TABLES:                            **
**   BANK-ACCOUNT (ACBNKA),  BANK-LOGO (ACLOGO),  BANK-NAME (ACBNKN) **
**                                                                   **
** UPDATE: BANK-ACCOUNT (ACBNKA)  IF FUNCTION = 'NEW SEQUENCE'       **
**  (ET IS DONE NOT HERE BUT IN CALLED PROGRAM).                     **
**------------------------------------------------------------------ **
** INPUT PARAMETERS:                                                 **
**----------                                                         **
**    CPSA110-FUNCTION    - VALUES:  '       '            OR         **
**                                   'NEXT'               OR         **
**                                   'NEW SEQUENCE'                  **
**                                                                   **
**----------                                                         **
**    CPSA110-SOURCE-CODE - VALUES:   (VALID SOURCE CODE) OR         **
**                                    '     '  (BLANK)               **
**    (SOURCE-CODE = '     '  VALID ONLY IF CPSA110-FUNCTON = 'NEXT')**
**                                                                   **
**----------                                                         **
**    CPSA110-NEW-SEQ-NBR - VALUES:  A NEW SEQUENCE NUM (N10)        **
**                                   WHICH IS VALIDATED BY PROGRAM   **
**                                   TO BE IN RANGE BETWIN START-SEQ **
**                                   AND END-SEQ.                    **
**                                                                   **
**-------------------------------------------------------------------**
** OUTPUT PARAMETERS:      ALL REFERENCE-TABLE FIELDS.               **
**                                                                   **
**------------------------------------------------------------------ **
**          RETURN CODES:  (LOOK AT FUNCTION CODE BELOW ALSO)        **
** 00 - SUCCESSFUL                                                   **
**                                                                   **
** 01 - SOURCE CODE NOT SUPLIED                                      **
**                                                                   **
** 02 - USING SUPLIED SOURCE CODE CAN't find matching record         **
**       IN BANK-ACCOUNT TABLE                                       **
**                                                                   **
** 03 - USING BANK-LOGO-KEY FROM ACCOUNT-TABLE CAN't find            **
**      MATCHING RECORD IN BANK-LOGO TABLE                           **
**                                                                   **
** 04 - USING BANK-ROUTING FROM  ACCONT-TABLE CAN't find             **
**      MATCHING RECORD IN BANK-NAME TABLE                           **
**                                                                   **
** 05 - BANK-LOGO-KEY FROM ACCONT-TABLE IS BLANK                     **
**      SO WE CAN't get info from BANK-LOGO table                    **
**                                                                   **
** 06 - BANK-ROUTING  FROM ACCONT-TABLE IS BLANK                     **
**      SO WE CAN't get info from BANK-NAME table                    **
**                                                                   **
** 11 - FUNCTION 'NEW SEQUENCE' SUPPLIED SOURCE-CODE IS BLANK        **
**                                                                   **
** 12 - FUNCTION 'NEW SEQUENCE' SUPPLIED NEW-SEQ-NBR IS BLANK        **
**                                                                   **
** 13 - FUNCTION 'NEW SEQUENCE' CANT FOUND MATCH ON GIVEN KEY        **
**                                                                   **
** 14 - FUNCTION 'NEW SEQUENCE' SUPPLIED NEW-SEQ-NBR IS NOT WITHIN   **
**      THE RANGE OFF START-SEQ - END-SEQ                            **
**                                                                   **
** 99 - INVALID FUNCTION                                             **
**                                                                   **
** ********************************************************************
**                                                                   **
**-------------------------------------------------------------------**
** RETURN CODES SPECIFIC TO EACH FUNCTION CODE:                      **
**-------------------------------------------------------------------**
**  00, 03, 04, 05, 06 CAN BE RETURNED FROM ANY FUNCTION             **
**                                                                   **
**                                                                   **
** CPSA110-FUNCTION   - VALUE = '       '                            **
**                                                                   **
** 01 - SOURCE CODE NOT SUPLIED                                      **
**                                                                   **
** 02 - USING SUPLIED SOURCE CODE CAN't find matching record         **
**       IN BANK-ACCOUNT TABLE                                       **
**                                                                   **
**                                                                   **
**-------------------------------------------------------------------**
** CPSA110-FUNCTION   - VALUE = 'NEXT'                               **
**                                                                   **
** 00 AND CPSA110-FUNCTION  = 'NEXT'   -  RECORD FOUND               **
**        OR                                                         **
** 00 AND CPSA110-FUNCTION  = 'END'    -  LOGICAL END OF TABLE       **
**                                                                   **
**                                                                   **
**-------------------------------------------------------------------**
** CPSA110-FUNCTION   - VALUE = 'NEW SEQUENCE'                       **
**                                                                   **
** 11 - FUNCTION 'NEW SEQUENCE' SUPPLIED SOURCE-CODE IS BLANK        **
**                                                                   **
** 12 - FUNCTION 'NEW SEQUENCE' SUPPLIED NEW-SEQ-NBR IS BLANK        **
**                                                                   **
** 13 - FUNCTION 'NEW SEQUENCE' CANT FOUND MATCH ON GIVEN KEY        **
**                                                                   **
** 14 - FUNCTION 'NEW SEQUENCE' SUPPLIED NEW-SEQ-NBR IS NOT WITHIN   **
**      THE RANGE OFF START-SEQ - END-SEQ                            **
**                                                                   **
**                                                                   **
**                                                                   **
**                                                                   **
**                                                                   **
**                                                                   **
**                                                                   **
***********************************************************************
**                           H I S T O R Y                           **
***********************************************************************
** 03/21/01  AR - ADD BANK-INTERNAL-POSPAY-IND                       **
**                                                                   **
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpsn110 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCpsa110 pdaCpsa110;
    private LdaCpsl110 ldaCpsl110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Bank_Super1;

    private DbsGroup pnd_Bank_Super1__R_Field_1;
    private DbsField pnd_Bank_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Bank_Super1__R_Field_2;
    private DbsField pnd_Bank_Super1_Pnd_Bank_Source_Code;
    private DbsField pnd_Bank_Super1_Pnd_Filler1;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Long_Key;
    private DbsField pnd_Logo_Super1;

    private DbsGroup pnd_Logo_Super1__R_Field_3;
    private DbsField pnd_Logo_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Logo_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Logo_Super1_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Logo_Super1__R_Field_4;
    private DbsField pnd_Logo_Super1_Pnd_Bank_Logo_Key;
    private DbsField pnd_Logo_Super1_Pnd_Filler4;
    private DbsField pnd_Logo_Super1_Pnd_Rt_Long_Key;
    private DbsField pnd_Bank_Name_Super2;

    private DbsGroup pnd_Bank_Name_Super2__R_Field_5;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_Table_Id;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_Long_Key;

    private DbsGroup pnd_Bank_Name_Super2__R_Field_6;
    private DbsField pnd_Bank_Name_Super2_Pnd_Bank_Routing;
    private DbsField pnd_Bank_Name_Super2_Pnd_Filler5;
    private DbsField pnd_Bank_Name_Super2_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Overide_Logo_Fields;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Name1;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Name2;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Addr1;
    private DbsField pnd_Overide_Logo_Fields_Over_Logo_Addr2;
    private DbsField pnd_Isn;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCpsl110 = new LdaCpsl110();
        registerRecord(ldaCpsl110);
        registerRecord(ldaCpsl110.getVw_ra());
        registerRecord(ldaCpsl110.getVw_rau());
        registerRecord(ldaCpsl110.getVw_rl());
        registerRecord(ldaCpsl110.getVw_rn());

        // parameters
        parameters = new DbsRecord();
        pdaCpsa110 = new PdaCpsa110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Bank_Super1 = localVariables.newFieldInRecord("pnd_Bank_Super1", "#BANK-SUPER1", FieldType.STRING, 66);

        pnd_Bank_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Bank_Super1__R_Field_1", "REDEFINE", pnd_Bank_Super1);
        pnd_Bank_Super1_Pnd_Rt_A_I_Ind = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Bank_Super1_Pnd_Rt_Table_Id = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Bank_Super1_Pnd_Rt_Short_Key = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 
            20);

        pnd_Bank_Super1__R_Field_2 = pnd_Bank_Super1__R_Field_1.newGroupInGroup("pnd_Bank_Super1__R_Field_2", "REDEFINE", pnd_Bank_Super1_Pnd_Rt_Short_Key);
        pnd_Bank_Super1_Pnd_Bank_Source_Code = pnd_Bank_Super1__R_Field_2.newFieldInGroup("pnd_Bank_Super1_Pnd_Bank_Source_Code", "#BANK-SOURCE-CODE", 
            FieldType.STRING, 5);
        pnd_Bank_Super1_Pnd_Filler1 = pnd_Bank_Super1__R_Field_2.newFieldInGroup("pnd_Bank_Super1_Pnd_Filler1", "#FILLER1", FieldType.STRING, 15);
        pnd_Bank_Super1_Pnd_Rt_Long_Key = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 
            40);
        pnd_Logo_Super1 = localVariables.newFieldInRecord("pnd_Logo_Super1", "#LOGO-SUPER1", FieldType.STRING, 66);

        pnd_Logo_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Logo_Super1__R_Field_3", "REDEFINE", pnd_Logo_Super1);
        pnd_Logo_Super1_Pnd_Rt_A_I_Ind = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Logo_Super1_Pnd_Rt_Table_Id = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Logo_Super1_Pnd_Rt_Short_Key = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 
            20);

        pnd_Logo_Super1__R_Field_4 = pnd_Logo_Super1__R_Field_3.newGroupInGroup("pnd_Logo_Super1__R_Field_4", "REDEFINE", pnd_Logo_Super1_Pnd_Rt_Short_Key);
        pnd_Logo_Super1_Pnd_Bank_Logo_Key = pnd_Logo_Super1__R_Field_4.newFieldInGroup("pnd_Logo_Super1_Pnd_Bank_Logo_Key", "#BANK-LOGO-KEY", FieldType.STRING, 
            10);
        pnd_Logo_Super1_Pnd_Filler4 = pnd_Logo_Super1__R_Field_4.newFieldInGroup("pnd_Logo_Super1_Pnd_Filler4", "#FILLER4", FieldType.STRING, 10);
        pnd_Logo_Super1_Pnd_Rt_Long_Key = pnd_Logo_Super1__R_Field_3.newFieldInGroup("pnd_Logo_Super1_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 
            40);
        pnd_Bank_Name_Super2 = localVariables.newFieldInRecord("pnd_Bank_Name_Super2", "#BANK-NAME-SUPER2", FieldType.STRING, 66);

        pnd_Bank_Name_Super2__R_Field_5 = localVariables.newGroupInRecord("pnd_Bank_Name_Super2__R_Field_5", "REDEFINE", pnd_Bank_Name_Super2);
        pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Bank_Name_Super2_Pnd_Rt_Table_Id = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_Table_Id", "#RT-TABLE-ID", 
            FieldType.STRING, 5);
        pnd_Bank_Name_Super2_Pnd_Rt_Long_Key = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_Long_Key", "#RT-LONG-KEY", 
            FieldType.STRING, 40);

        pnd_Bank_Name_Super2__R_Field_6 = pnd_Bank_Name_Super2__R_Field_5.newGroupInGroup("pnd_Bank_Name_Super2__R_Field_6", "REDEFINE", pnd_Bank_Name_Super2_Pnd_Rt_Long_Key);
        pnd_Bank_Name_Super2_Pnd_Bank_Routing = pnd_Bank_Name_Super2__R_Field_6.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Bank_Routing", "#BANK-ROUTING", 
            FieldType.STRING, 9);
        pnd_Bank_Name_Super2_Pnd_Filler5 = pnd_Bank_Name_Super2__R_Field_6.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Filler5", "#FILLER5", FieldType.STRING, 
            31);
        pnd_Bank_Name_Super2_Pnd_Rt_Short_Key = pnd_Bank_Name_Super2__R_Field_5.newFieldInGroup("pnd_Bank_Name_Super2_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", 
            FieldType.STRING, 20);

        pnd_Overide_Logo_Fields = localVariables.newGroupInRecord("pnd_Overide_Logo_Fields", "#OVERIDE-LOGO-FIELDS");
        pnd_Overide_Logo_Fields_Over_Logo_Name1 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Name1", "OVER-LOGO-NAME1", 
            FieldType.STRING, 50);
        pnd_Overide_Logo_Fields_Over_Logo_Name2 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Name2", "OVER-LOGO-NAME2", 
            FieldType.STRING, 50);
        pnd_Overide_Logo_Fields_Over_Logo_Addr1 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Addr1", "OVER-LOGO-ADDR1", 
            FieldType.STRING, 50);
        pnd_Overide_Logo_Fields_Over_Logo_Addr2 = pnd_Overide_Logo_Fields.newFieldInGroup("pnd_Overide_Logo_Fields_Over_Logo_Addr2", "OVER-LOGO-ADDR2", 
            FieldType.STRING, 50);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCpsl110.initializeValues();

        localVariables.reset();
        pnd_Bank_Super1.setInitialValue("ACBNKA");
        pnd_Logo_Super1.setInitialValue("ACLOGO");
        pnd_Bank_Name_Super2.setInitialValue("ACBNKN");
        pnd_Overide_Logo_Fields_Over_Logo_Name1.setInitialValue(" ");
        pnd_Overide_Logo_Fields_Over_Logo_Name2.setInitialValue(" ");
        pnd_Overide_Logo_Fields_Over_Logo_Addr1.setInitialValue(" ");
        pnd_Overide_Logo_Fields_Over_Logo_Addr2.setInitialValue(" ");
        pnd_Isn.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cpsn110() throws Exception
    {
        super("Cpsn110");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        //* * *ERROR-TA  :=  'INFP9000'           /* JH 7/11/2000
        //* *--------- MAIN PROCESS ---------------------------------------------**
        pdaCpsa110.getCpsa110_Cpsa110b().reset();                                                                                                                         //Natural: RESET CPSA110B #OVERIDE-LOGO-FIELDS
        pnd_Overide_Logo_Fields.reset();
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            short decideConditionsMet470 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CPSA110-FUNCTION;//Natural: VALUE '       '
            if (condition((pdaCpsa110.getCpsa110_Cpsa110_Function().equals("       "))))
            {
                decideConditionsMet470++;
                if (condition(pdaCpsa110.getCpsa110_Cpsa110_Source_Code().equals("     ")))                                                                               //Natural: IF CPSA110.CPSA110-SOURCE-CODE EQ '     '
                {
                    pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("01");                                                                                           //Natural: MOVE '01' TO CPSA110-RETURN-CODE
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SETUP-BANK-SUPER1
                sub_Setup_Bank_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-BANK-SUPER1
                sub_Read_Bank_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-BANK-LOGO-BANK-NAME-TBLS
                sub_Process_Bank_Logo_Bank_Name_Tbls();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'NEXT'
            else if (condition((pdaCpsa110.getCpsa110_Cpsa110_Function().equals("NEXT"))))
            {
                decideConditionsMet470++;
                //*  OPTION 1
                if (condition(pdaCpsa110.getCpsa110_Cpsa110_Source_Code().equals("     ")))                                                                               //Natural: IF CPSA110.CPSA110-SOURCE-CODE EQ '     '
                {
                                                                                                                                                                          //Natural: PERFORM SETUP-BANK-SUPER1-NEXT-OPT1
                    sub_Setup_Bank_Super1_Next_Opt1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  OPTION 2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SETUP-BANK-SUPER1-NEXT-OPT2
                    sub_Setup_Bank_Super1_Next_Opt2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-BANK-SUPER1-NEXT
                sub_Read_Bank_Super1_Next();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-BANK-LOGO-BANK-NAME-TBLS
                sub_Process_Bank_Logo_Bank_Name_Tbls();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'NEW SEQUENCE'
            else if (condition((pdaCpsa110.getCpsa110_Cpsa110_Function().equals("NEW SEQUENCE"))))
            {
                decideConditionsMet470++;
                if (condition(pdaCpsa110.getCpsa110_Cpsa110_Source_Code().equals("     ")))                                                                               //Natural: IF CPSA110.CPSA110-SOURCE-CODE EQ '     '
                {
                    pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("11");                                                                                           //Natural: MOVE '11' TO CPSA110-RETURN-CODE
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaCpsa110.getCpsa110_Cpsa110_New_Seq_Nbr().equals(getZero())))                                                                             //Natural: IF CPSA110.CPSA110-NEW-SEQ-NBR EQ 0
                {
                    pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("12");                                                                                           //Natural: MOVE '12' TO CPSA110-RETURN-CODE
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SETUP-BANK-SUPER1-NEW-SEQUENCE
                sub_Setup_Bank_Super1_New_Sequence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-BANK-SUPER1-NEW-SEQUENCE
                sub_Read_Bank_Super1_New_Sequence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("99");                                                                                               //Natural: MOVE '99' TO CPSA110-RETURN-CODE
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. )
            }                                                                                                                                                             //Natural: END-DECIDE
            if (true) break PROG;                                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
            //* *--------END OF MAIN PROCESS ---------------------------------------**
            //* **************** PERFORMED SUBROUTINES *******************************
            //*  * * * * * * * FUNCTION BLANK  * * * * * * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*   SETUP SUPER TO ACCESS BANK-ACCOUNT TBL
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-BANK-SUPER1
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  ACCESS BANK-ACCOUNT TBL
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BANK-SUPER1
            //* *-------------------------------------------------------------------**
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*  * * * * * * * FUNCTION 'NEXT' * * * * * * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*  THIS SETINGS USED TO READ FIRST RECORD FROM A BANK-ACCOUNT TABLE **
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-BANK-SUPER1-NEXT-OPT1
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  THIS SETINGS USED TO READ FOLOWING RECORD FROM A BANK-ACCOUNT TABLE *
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-BANK-SUPER1-NEXT-OPT2
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //* *  DEPENDING ON PREVIOUSLY SET SUPER, ACCESS BANK-ACCOUNT TABLE.
            //* *  FOR ONE OF THE TWO REASONS.
            //* *  REASON 1 TO READ FIRST RECORD FROM A BANK-ACCOUNT TABLE.
            //* *  REASON 2 TO READ FOLLOWING RECORD FROM A BANK-ACCOUNT TABLE.
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BANK-SUPER1-NEXT
            //* *-------------------------------------------------------------------**
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*  * * * * * * * FUNCTION 'NEW SEQUENCE' * * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*   SETUP SUPER TO ACCESS BANK-ACCOUNT TBL
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-BANK-SUPER1-NEW-SEQUENCE
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  ACCESS BANK-ACCOUNT TBL FOR UPDATE WITH NEW SEQUENCE NBR
            //*  NO RETURN VALUE FROM RA TABLE, ONLY RETURN CODE
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BANK-SUPER1-NEW-SEQUENCE
            //* *-------------------------------------------------------------------**
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //* *-------------------------------------------------------------------**
            //*  THIS SUBROUTINE PROCCESS BANK-LOGO (IF NEEDED) AND BANK-NANE TABLES
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-BANK-LOGO-BANK-NAME-TBLS
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: USE-OVER-FIELDS
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-LOGO-SUPER1
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-LOGO-SUPER1
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-BANK-NAME-SUPER2
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-BANK-NAME-SUPER2
            //* ** AND  #BANK-NAME-SUPER2.#RT-LONG-KEY   =  RN.RT-LONG-KEY
            //* *-------------------------------------------------------------------**
            //*  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
            //*  (PROG.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Setup_Bank_Super1() throws Exception                                                                                                                 //Natural: SETUP-BANK-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(pdaCpsa110.getCpsa110_Cpsa110_Source_Code());                                                                           //Natural: MOVE CPSA110.CPSA110-SOURCE-CODE TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_Read_Bank_Super1() throws Exception                                                                                                                  //Natural: READ-BANK-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpsl110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaCpsl110.getVw_ra().readNextRow("READ01")))
        {
            //*  INTENTIONALLY DONE HERE
            pdaCpsa110.getCpsa110_Cpsa110b().setValuesByName(ldaCpsl110.getVw_ra());                                                                                      //Natural: MOVE BY NAME RA TO CPSA110B
            pnd_Overide_Logo_Fields.setValuesByName(ldaCpsl110.getVw_ra());                                                                                               //Natural: MOVE BY NAME RA TO #OVERIDE-LOGO-FIELDS
            if (condition(pnd_Bank_Super1_Pnd_Rt_A_I_Ind.equals(ldaCpsl110.getRa_Rt_A_I_Ind()) && pnd_Bank_Super1_Pnd_Rt_Table_Id.equals(ldaCpsl110.getRa_Rt_Table_Id())  //Natural: IF #BANK-SUPER1.#RT-A-I-IND = RA.RT-A-I-IND AND #BANK-SUPER1.#RT-TABLE-ID = RA.RT-TABLE-ID AND #BANK-SUPER1.#RT-SHORT-KEY = RA.RT-SHORT-KEY
                && pnd_Bank_Super1_Pnd_Rt_Short_Key.equals(ldaCpsl110.getRa_Rt_Short_Key())))
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("00");                                                                                               //Natural: MOVE '00' TO CPSA110-RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("02");                                                                                               //Natural: MOVE '02' TO CPSA110-RETURN-CODE
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Setup_Bank_Super1_Next_Opt1() throws Exception                                                                                                       //Natural: SETUP-BANK-SUPER1-NEXT-OPT1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(" ");                                                                                                                   //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_Setup_Bank_Super1_Next_Opt2() throws Exception                                                                                                       //Natural: SETUP-BANK-SUPER1-NEXT-OPT2
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bank_Super1_Pnd_Bank_Source_Code.setValue(pdaCpsa110.getCpsa110_Cpsa110_Source_Code());                                                                       //Natural: MOVE CPSA110.CPSA110-SOURCE-CODE TO #BANK-SUPER1.#BANK-SOURCE-CODE
        pnd_Bank_Super1_Pnd_Filler1.moveAll("H'FF'");                                                                                                                     //Natural: MOVE ALL H'FF' TO #BANK-SUPER1.#FILLER1
        pnd_Bank_Super1_Pnd_Rt_Long_Key.moveAll("H'FF'");                                                                                                                 //Natural: MOVE ALL H'FF' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_Read_Bank_Super1_Next() throws Exception                                                                                                             //Natural: READ-BANK-SUPER1-NEXT
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpsl110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "READ02",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ02:
        while (condition(ldaCpsl110.getVw_ra().readNextRow("READ02")))
        {
            //*  INTENTIONALLY DONE HERE
            pdaCpsa110.getCpsa110_Cpsa110b().setValuesByName(ldaCpsl110.getVw_ra());                                                                                      //Natural: MOVE BY NAME RA TO CPSA110B
            pnd_Overide_Logo_Fields.setValuesByName(ldaCpsl110.getVw_ra());                                                                                               //Natural: MOVE BY NAME RA TO #OVERIDE-LOGO-FIELDS
            pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("00");                                                                                                   //Natural: MOVE '00' TO CPSA110-RETURN-CODE
            if (condition(((pnd_Bank_Super1_Pnd_Rt_A_I_Ind.notEquals(ldaCpsl110.getRa_Rt_A_I_Ind())) || (pnd_Bank_Super1_Pnd_Rt_Table_Id.notEquals(ldaCpsl110.getRa_Rt_Table_Id()))))) //Natural: IF ( ( #BANK-SUPER1.#RT-A-I-IND NE RA.RT-A-I-IND ) OR ( #BANK-SUPER1.#RT-TABLE-ID NE RA.RT-TABLE-ID ) )
            {
                pdaCpsa110.getCpsa110_Cpsa110_Function().setValue("END");                                                                                                 //Natural: MOVE 'END' TO CPSA110-FUNCTION
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Setup_Bank_Super1_New_Sequence() throws Exception                                                                                                    //Natural: SETUP-BANK-SUPER1-NEW-SEQUENCE
    {
        if (BLNatReinput.isReinput()) return;

        //*   SETUP SUPER
        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(pdaCpsa110.getCpsa110_Cpsa110_Source_Code());                                                                           //Natural: MOVE CPSA110.CPSA110-SOURCE-CODE TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_Read_Bank_Super1_New_Sequence() throws Exception                                                                                                     //Natural: READ-BANK-SUPER1-NEW-SEQUENCE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Isn.reset();                                                                                                                                                  //Natural: RESET #ISN
        ldaCpsl110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "R1",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        R1:
        while (condition(ldaCpsl110.getVw_ra().readNextRow("R1")))
        {
            pnd_Isn.setValue(ldaCpsl110.getVw_ra().getAstISN("R1"));                                                                                                      //Natural: MOVE *ISN TO #ISN
            if (condition(pnd_Bank_Super1_Pnd_Rt_A_I_Ind.equals(ldaCpsl110.getRa_Rt_A_I_Ind()) && pnd_Bank_Super1_Pnd_Rt_Table_Id.equals(ldaCpsl110.getRa_Rt_Table_Id())  //Natural: IF #BANK-SUPER1.#RT-A-I-IND = RA.RT-A-I-IND AND #BANK-SUPER1.#RT-TABLE-ID = RA.RT-TABLE-ID AND #BANK-SUPER1.#RT-SHORT-KEY = RA.RT-SHORT-KEY
                && pnd_Bank_Super1_Pnd_Rt_Short_Key.equals(ldaCpsl110.getRa_Rt_Short_Key())))
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("00");                                                                                               //Natural: MOVE '00' TO CPSA110-RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("13");                                                                                               //Natural: MOVE '13' TO CPSA110-RETURN-CODE
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pdaCpsa110.getCpsa110_Cpsa110_New_Seq_Nbr().greater(ldaCpsl110.getRa_Start_Seq())) && (pdaCpsa110.getCpsa110_Cpsa110_New_Seq_Nbr().lessOrEqual(ldaCpsl110.getRa_End_Seq()))))) //Natural: IF ( ( CPSA110.CPSA110-NEW-SEQ-NBR GT RA.START-SEQ ) AND ( CPSA110.CPSA110-NEW-SEQ-NBR LE RA.END-SEQ ) )
            {
                G1:                                                                                                                                                       //Natural: GET RAU #ISN
                ldaCpsl110.getVw_rau().readByID(pnd_Isn.getLong(), "G1");
                ldaCpsl110.getRau_Start_Seq().setValue(pdaCpsa110.getCpsa110_Cpsa110_New_Seq_Nbr());                                                                      //Natural: MOVE CPSA110.CPSA110-NEW-SEQ-NBR TO RAU.START-SEQ
                ldaCpsl110.getVw_rau().updateDBRow("G1");                                                                                                                 //Natural: UPDATE ( G1. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("14");                                                                                               //Natural: MOVE '14' TO CPSA110-RETURN-CODE
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Bank_Logo_Bank_Name_Tbls() throws Exception                                                                                                  //Natural: PROCESS-BANK-LOGO-BANK-NAME-TBLS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaCpsa110.getCpsa110_Bank_Routing().equals("  ")))                                                                                                 //Natural: IF CPSA110.BANK-ROUTING EQ '  '
        {
            pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("06");                                                                                                   //Natural: MOVE '06' TO CPSA110-RETURN-CODE
            Global.setEscape(true);                                                                                                                                       //Natural: ESCAPE BOTTOM ( PROG. )
            Global.setEscapeCode(EscapeType.Bottom, "PROG");
            if (true) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM SETUP-BANK-NAME-SUPER2
            sub_Setup_Bank_Name_Super2();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-BANK-NAME-SUPER2
            sub_Read_Bank_Name_Super2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Overide_Logo_Fields_Over_Logo_Name1.notEquals(" ")))                                                                                            //Natural: IF #OVERIDE-LOGO-FIELDS.OVER-LOGO-NAME1 NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM USE-OVER-FIELDS
            sub_Use_Over_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaCpsa110.getCpsa110_Bank_Logo_Key().equals("  ")))                                                                                            //Natural: IF CPSA110.BANK-LOGO-KEY EQ '  '
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("05");                                                                                               //Natural: MOVE '05' TO CPSA110-RETURN-CODE
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM SETUP-LOGO-SUPER1
                sub_Setup_Logo_Super1();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-LOGO-SUPER1
                sub_Read_Logo_Super1();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Use_Over_Fields() throws Exception                                                                                                                   //Natural: USE-OVER-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pdaCpsa110.getCpsa110_Bank_Logo_Name1().setValue(pnd_Overide_Logo_Fields_Over_Logo_Name1);                                                                        //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-NAME1 TO CPSA110.BANK-LOGO-NAME1
        pdaCpsa110.getCpsa110_Bank_Logo_Name2().setValue(pnd_Overide_Logo_Fields_Over_Logo_Name2);                                                                        //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-NAME2 TO CPSA110.BANK-LOGO-NAME2
        pdaCpsa110.getCpsa110_Bank_Logo_Address1().setValue(pnd_Overide_Logo_Fields_Over_Logo_Addr1);                                                                     //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-ADDR1 TO CPSA110.BANK-LOGO-ADDRESS1
        pdaCpsa110.getCpsa110_Bank_Logo_Address2().setValue(pnd_Overide_Logo_Fields_Over_Logo_Addr2);                                                                     //Natural: MOVE #OVERIDE-LOGO-FIELDS.OVER-LOGO-ADDR2 TO CPSA110.BANK-LOGO-ADDRESS2
    }
    private void sub_Setup_Logo_Super1() throws Exception                                                                                                                 //Natural: SETUP-LOGO-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        //*   SETUP SUPER
        pnd_Logo_Super1_Pnd_Rt_Short_Key.setValue(pdaCpsa110.getCpsa110_Bank_Logo_Key());                                                                                 //Natural: MOVE CPSA110.BANK-LOGO-KEY TO #LOGO-SUPER1.#RT-SHORT-KEY
        pnd_Logo_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #LOGO-SUPER1.#RT-LONG-KEY
    }
    private void sub_Read_Logo_Super1() throws Exception                                                                                                                  //Natural: READ-LOGO-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpsl110.getVw_rl().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RL BY RT-SUPER1 STARTING FROM #LOGO-SUPER1
        (
        "READ03",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Logo_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ03:
        while (condition(ldaCpsl110.getVw_rl().readNextRow("READ03")))
        {
            pdaCpsa110.getCpsa110_Cpsa110b().setValuesByName(ldaCpsl110.getVw_rl());                                                                                      //Natural: MOVE BY NAME RL TO CPSA110.CPSA110B
            if (condition(pnd_Logo_Super1_Pnd_Rt_A_I_Ind.equals(ldaCpsl110.getRl_Rt_A_I_Ind()) && pnd_Logo_Super1_Pnd_Rt_Table_Id.equals(ldaCpsl110.getRl_Rt_Table_Id())  //Natural: IF #LOGO-SUPER1.#RT-A-I-IND = RL.RT-A-I-IND AND #LOGO-SUPER1.#RT-TABLE-ID = RL.RT-TABLE-ID AND #LOGO-SUPER1.#BANK-LOGO-KEY = RL.BANK-LOGO-KEY
                && pnd_Logo_Super1_Pnd_Bank_Logo_Key.equals(ldaCpsl110.getRl_Bank_Logo_Key())))
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("00");                                                                                               //Natural: MOVE '00' TO CPSA110-RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("03");                                                                                               //Natural: MOVE '03' TO CPSA110-RETURN-CODE
                //*   MOVE BACK LOGO-KEY FROM ACCOUNT TABLE TO PDA
                pdaCpsa110.getCpsa110_Bank_Logo_Key().setValue(pnd_Logo_Super1_Pnd_Bank_Logo_Key);                                                                        //Natural: MOVE #LOGO-SUPER1.#BANK-LOGO-KEY TO CPSA110.BANK-LOGO-KEY
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Setup_Bank_Name_Super2() throws Exception                                                                                                            //Natural: SETUP-BANK-NAME-SUPER2
    {
        if (BLNatReinput.isReinput()) return;

        //*   SETUP SUPER
        pnd_Bank_Name_Super2_Pnd_Rt_Long_Key.setValue(pdaCpsa110.getCpsa110_Bank_Routing());                                                                              //Natural: MOVE CPSA110.BANK-ROUTING TO #BANK-NAME-SUPER2.#RT-LONG-KEY
        pnd_Bank_Name_Super2_Pnd_Rt_Short_Key.setValue("  ");                                                                                                             //Natural: MOVE '  ' TO #BANK-NAME-SUPER2.#RT-SHORT-KEY
    }
    private void sub_Read_Bank_Name_Super2() throws Exception                                                                                                             //Natural: READ-BANK-NAME-SUPER2
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpsl110.getVw_rn().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RN BY RT-SUPER2 STARTING FROM #BANK-NAME-SUPER2
        (
        "READ04",
        new Wc[] { new Wc("RT_SUPER2", ">=", pnd_Bank_Name_Super2, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER2", "ASC") },
        1
        );
        READ04:
        while (condition(ldaCpsl110.getVw_rn().readNextRow("READ04")))
        {
            pdaCpsa110.getCpsa110_Cpsa110b().setValuesByName(ldaCpsl110.getVw_rn());                                                                                      //Natural: MOVE BY NAME RN TO CPSA110.CPSA110B
            if (condition(pnd_Bank_Name_Super2_Pnd_Rt_A_I_Ind.equals(ldaCpsl110.getRn_Rt_A_I_Ind()) && pnd_Bank_Name_Super2_Pnd_Rt_Table_Id.equals(ldaCpsl110.getRn_Rt_Table_Id())  //Natural: IF #BANK-NAME-SUPER2.#RT-A-I-IND = RN.RT-A-I-IND AND #BANK-NAME-SUPER2.#RT-TABLE-ID = RN.RT-TABLE-ID AND #BANK-NAME-SUPER2.#BANK-ROUTING = RN.BANK-ROUTING
                && pnd_Bank_Name_Super2_Pnd_Bank_Routing.equals(ldaCpsl110.getRn_Bank_Routing())))
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("00");                                                                                               //Natural: MOVE '00' TO CPSA110-RETURN-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCpsa110.getCpsa110_Cpsa110_Return_Code().setValue("04");                                                                                               //Natural: MOVE '04' TO CPSA110-RETURN-CODE
                //*   MOVE BACK BANK-ROUTING FROM ACCOUNT TABLE TO PDA
                pdaCpsa110.getCpsa110_Bank_Routing().setValue(pnd_Bank_Name_Super2_Pnd_Bank_Routing);                                                                     //Natural: MOVE #BANK-NAME-SUPER2.#BANK-ROUTING TO CPSA110.BANK-ROUTING
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
