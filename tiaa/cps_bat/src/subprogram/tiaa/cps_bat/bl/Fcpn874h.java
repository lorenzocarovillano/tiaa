/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:52 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn874h
************************************************************
**        * FILE NAME            : Fcpn874h.java
**        * CLASS NAME           : Fcpn874h
**        * INSTANCE NAME        : Fcpn874h
************************************************************
************************************************************************
* SUBPROGRAM : FCPN874H
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "NZ" ANNUITANT STATEMENTS - HEADERS
* MODIFICATION DATE : DETAIL LOG
* 08/23/2019 : REPLACING PDQ MODULES WITH ADS MODULES I.E.
*              PDQA360,PDQA362,PDQN360,PDQN362 TO
*              ADSA360,ADSA362,ADSN360,ADSN362 RESPECTIVELY. -TAG:VIKRAM
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn874h extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa874h pdaFcpa874h;
    private PdaFcpamode pdaFcpamode;
    private PdaFcpaoptn pdaFcpaoptn;
    private LdaFcpl874h ldaFcpl874h;
    private PdaAdsa360 pdaAdsa360;
    private PdaAdsa362 pdaAdsa362;
    private PdaTbldcoda pdaTbldcoda;
    private LdaFcpltbcd ldaFcpltbcd;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Hdr_Array;
    private DbsField pnd_Ws_Pnd_Page_Num;
    private DbsField pnd_Ws_Pnd_Ia_Ppcn;
    private DbsField pnd_Ws_Pnd_Da_Ppcn;
    private DbsField pnd_Ws_Pnd_Annt_Starting_Date;
    private DbsField pnd_Ws_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Pnd_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Orgn_Chk_Dte;

    private DbsGroup pnd_Orgn_Chk_Dte__R_Field_1;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Filler;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A;
    private DbsField pnd_Orgn_Chk_Dte_A4;

    private DbsGroup pnd_Orgn_Chk_Dte_A4__R_Field_2;
    private DbsField pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D;
    private DbsField pnd_Edited_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpamode = new PdaFcpamode(localVariables);
        pdaFcpaoptn = new PdaFcpaoptn(localVariables);
        ldaFcpl874h = new LdaFcpl874h();
        registerRecord(ldaFcpl874h);
        pdaAdsa360 = new PdaAdsa360(localVariables);
        pdaAdsa362 = new PdaAdsa362(localVariables);
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        ldaFcpltbcd = new LdaFcpltbcd();
        registerRecord(ldaFcpltbcd);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa874h = new PdaFcpa874h(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Hdr_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Hdr_Array", "#HDR-ARRAY", FieldType.STRING, 143, new DbsArrayController(1, 5));
        pnd_Ws_Pnd_Page_Num = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Page_Num", "#PAGE-NUM", FieldType.STRING, 3);
        pnd_Ws_Pnd_Ia_Ppcn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ia_Ppcn", "#IA-PPCN", FieldType.STRING, 9);
        pnd_Ws_Pnd_Da_Ppcn = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Da_Ppcn", "#DA-PPCN", FieldType.STRING, 10, new DbsArrayController(1, 6));
        pnd_Ws_Pnd_Annt_Starting_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Annt_Starting_Date", "#ANNT-STARTING-DATE", FieldType.STRING, 10);
        pnd_Ws_Pnd_Pymnt_Check_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.STRING, 10);
        pnd_Ws_Pnd_Pymnt_Check_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", FieldType.STRING, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Orgn_Chk_Dte = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte", "#ORGN-CHK-DTE", FieldType.NUMERIC, 9);

        pnd_Orgn_Chk_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte__R_Field_1", "REDEFINE", pnd_Orgn_Chk_Dte);
        pnd_Orgn_Chk_Dte_Pnd_Filler = pnd_Orgn_Chk_Dte__R_Field_1.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A = pnd_Orgn_Chk_Dte__R_Field_1.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A", "#ORGN-CHK-DTE-A", FieldType.STRING, 
            8);
        pnd_Orgn_Chk_Dte_A4 = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte_A4", "#ORGN-CHK-DTE-A4", FieldType.STRING, 4);

        pnd_Orgn_Chk_Dte_A4__R_Field_2 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte_A4__R_Field_2", "REDEFINE", pnd_Orgn_Chk_Dte_A4);
        pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D = pnd_Orgn_Chk_Dte_A4__R_Field_2.newFieldInGroup("pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D", "#ORGN-CHK-DTE-D", 
            FieldType.DATE);
        pnd_Edited_Date = localVariables.newFieldInRecord("pnd_Edited_Date", "#EDITED-DATE", FieldType.STRING, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl874h.initializeValues();
        ldaFcpltbcd.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn874h() throws Exception
    {
        super("Fcpn874h");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        if (condition(pdaFcpa874h.getPnd_Fcpa874h_Pnd_Gen_Headers().getBoolean()))                                                                                        //Natural: IF #GEN-HEADERS
        {
            pdaFcpa874h.getPnd_Fcpa874h_Pnd_Gen_Headers().setValue(false);                                                                                                //Natural: ASSIGN #GEN-HEADERS := FALSE
            pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue("*").reset();                                                                                         //Natural: RESET #HEADER-ARRAY ( * ) #HDR-ARRAY ( * )
            pnd_Ws_Pnd_Hdr_Array.getValue("*").reset();
            pdaFcpamode.getPnd_Fcpamode().setValuesByName(pdaFcpa874h.getPnd_Fcpa874h());                                                                                 //Natural: MOVE BY NAME #FCPA874H TO #FCPAMODE
            pdaFcpamode.getPnd_Fcpamode_Pnd_Predict_Table().setValue(false);                                                                                              //Natural: ASSIGN #FCPAMODE.#PREDICT-TABLE := FALSE
            DbsUtil.callnat(Fcpnmode.class , getCurrentProcessState(), pdaFcpamode.getPnd_Fcpamode());                                                                    //Natural: CALLNAT 'FCPNMODE' USING #FCPAMODE
            if (condition(Global.isEscape())) return;
            pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(1).setValue(DbsUtil.compress(ldaFcpl874h.getPnd_Fcpl874h_Pnd_Hdr_Line1_1(), pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc(),  //Natural: COMPRESS #HDR-LINE1-1 #FCPAMODE.#MODE-DESC #HDR-LINE1-2 INTO #HEADER-ARRAY ( 1 )
                ldaFcpl874h.getPnd_Fcpl874h_Pnd_Hdr_Line1_2()));
            pdaAdsa360.getPnd_In_Data_Pnd_First_Name().setValue(pdaFcpa874h.getPnd_Fcpa874h_Ph_First_Name());                                                             //Natural: ASSIGN #FIRST-NAME := #FCPA874H.PH-FIRST-NAME
            pdaAdsa362.getPnd_In_Middle_Name().setValue(pdaFcpa874h.getPnd_Fcpa874h_Ph_Middle_Name());                                                                    //Natural: ASSIGN #IN-MIDDLE-NAME := #FCPA874H.PH-MIDDLE-NAME
            pdaAdsa360.getPnd_In_Data_Pnd_Last_Name().setValue(pdaFcpa874h.getPnd_Fcpa874h_Ph_Last_Name());                                                               //Natural: ASSIGN #LAST-NAME := #FCPA874H.PH-LAST-NAME
            //* *  CALLNAT 'PDQN362' /* VIKRAM
            //*  VIKRAM
            DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pdaAdsa362.getPnd_In_Middle_Name(), pdaAdsa362.getPnd_Out_Middle_Name(), pdaAdsa362.getPnd_Out_Suffix(),  //Natural: CALLNAT 'ADSN362' USING #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
                pdaAdsa362.getPnd_Out_Prefix(), pdaAdsa362.getPnd_Out_Return_Code());
            if (condition(Global.isEscape())) return;
            pdaAdsa360.getPnd_In_Data_Pnd_Prefix().setValue(pdaAdsa362.getPnd_Out_Prefix());                                                                              //Natural: ASSIGN #PREFIX := #OUT-PREFIX
            pdaAdsa360.getPnd_In_Data_Pnd_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                    //Natural: ASSIGN #MIDDLE-NAME := #OUT-MIDDLE-NAME
            pdaAdsa360.getPnd_In_Data_Pnd_Suffix().setValue(pdaAdsa362.getPnd_Out_Suffix());                                                                              //Natural: ASSIGN #SUFFIX := #OUT-SUFFIX
            pdaAdsa360.getPnd_In_Data_Pnd_Length().setValue(38);                                                                                                          //Natural: ASSIGN #LENGTH := 38
            pdaAdsa360.getPnd_In_Data_Pnd_Format().setValue("1");                                                                                                         //Natural: ASSIGN #FORMAT := '1'
            //* *  CALLNAT 'PDQN360'        USING #IN-DATA #OUT-DATA /* VIKRAM
            //*  VIKRAM
            DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pdaAdsa360.getPnd_In_Data(), pdaAdsa360.getPnd_Out_Data());                                         //Natural: CALLNAT 'ADSN360' USING #IN-DATA #OUT-DATA
            if (condition(Global.isEscape())) return;
            pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(2).setValue("+2");                                                                                    //Natural: ASSIGN #HEADER-ARRAY ( 2 ) := '+2'
            if (condition(pdaAdsa360.getPnd_Out_Data_Pnd_Length_Out().equals(getZero())))                                                                                 //Natural: IF #LENGTH-OUT = 0
            {
                pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(2).reset();                                                                                       //Natural: RESET #HEADER-ARRAY ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc_Length().add(40));                             //Natural: ASSIGN #J := #FCPAMODE.#MODE-DESC-LENGTH + 40
                setValueToSubstring(pdaAdsa360.getPnd_Out_Data_Pnd_Name_Out(),pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(2),pnd_Ws_Pnd_J.getInt(),           //Natural: MOVE #NAME-OUT TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,#J,#LENGTH-OUT )
                    pdaAdsa360.getPnd_Out_Data_Pnd_Length_Out().getInt());
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Hdr_Array.getValue(1).setValue(" 1");                                                                                                              //Natural: ASSIGN #HDR-ARRAY ( 1 ) := ' 1'
            pdaFcpaoptn.getPnd_Fcpaoptn().setValuesByName(pdaFcpa874h.getPnd_Fcpa874h());                                                                                 //Natural: MOVE BY NAME #FCPA874H TO #FCPAOPTN
            pdaFcpamode.getPnd_Fcpamode_Pnd_Predict_Table().setValue(false);                                                                                              //Natural: ASSIGN #FCPAMODE.#PREDICT-TABLE := FALSE
            DbsUtil.callnat(Fcpnoptn.class , getCurrentProcessState(), pdaFcpaoptn.getPnd_Fcpaoptn());                                                                    //Natural: CALLNAT 'FCPNOPTN' USING #FCPAOPTN
            if (condition(Global.isEscape())) return;
            setValueToSubstring(pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Optn_Desc(),pnd_Ws_Pnd_Hdr_Array.getValue(1),3,70);                                                       //Natural: MOVE #FCPAOPTN.#OPTN-DESC TO SUBSTR ( #HDR-ARRAY ( 1 ) ,3,70 )
            pnd_Ws_Pnd_Ia_Ppcn.setValueEdited(pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                             //Natural: MOVE EDITED #FCPA874H.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #IA-PPCN
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 6
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(6)); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(pnd_Ws_Pnd_I).equals(" ")))                                                            //Natural: IF #FCPA874H.CNTRCT-DA-NBR ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Da_Ppcn.getValue(pnd_Ws_Pnd_I).setValueEdited(pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(pnd_Ws_Pnd_I),new ReportEditMask("XXXXXXX-X")); //Natural: MOVE EDITED #FCPA874H.CNTRCT-DA-NBR ( #I ) ( EM = XXXXXXX-X ) TO #DA-PPCN ( #I )
                    if (condition(pnd_Ws_Pnd_I.notEquals(1)))                                                                                                             //Natural: IF #I NE 1
                    {
                        pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pnd_Ws_Pnd_I.subtract(1));                                                       //Natural: ASSIGN #J := #I - 1
                        setValueToSubstring(",",pnd_Ws_Pnd_Da_Ppcn.getValue(pnd_Ws_Pnd_J),10,1);                                                                          //Natural: MOVE ',' TO SUBSTR ( #DA-PPCN ( #J ) ,10,1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Company_Cde().equals("T")))                                                                                  //Natural: IF #FCPA874H.CNTRCT-COMPANY-CDE = 'T'
            {
                pnd_Ws_Pnd_Hdr_Array.getValue(2).setValue(DbsUtil.compress(ldaFcpl874h.getPnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_1(), pnd_Ws_Pnd_Ia_Ppcn, ldaFcpl874h.getPnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_2(),  //Natural: COMPRESS #HRD-LINE3-TIAA-1 #IA-PPCN #HRD-LINE3-TIAA-2 #WS.#DA-PPCN ( 1:3 ) INTO #HDR-ARRAY ( 2 )
                    pnd_Ws_Pnd_Da_Ppcn.getValue(1,":",3)));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Hdr_Array.getValue(2).setValue(DbsUtil.compress(ldaFcpl874h.getPnd_Fcpl874h_Pnd_Hrd_Line3_Cref_1(), pnd_Ws_Pnd_Ia_Ppcn, ldaFcpl874h.getPnd_Fcpl874h_Pnd_Hrd_Line3_Cref_2(),  //Natural: COMPRESS #HRD-LINE3-CREF-1 #IA-PPCN #HRD-LINE3-CREF-2 #WS.#DA-PPCN ( 1:3 ) INTO #HDR-ARRAY ( 2 )
                    pnd_Ws_Pnd_Da_Ppcn.getValue(1,":",3)));
            }                                                                                                                                                             //Natural: END-IF
            //*                                                           /* VF  8/99
            if (condition(pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                       //Natural: IF #FCPA874H.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                pnd_Ws_Pnd_Hdr_Array.getValue(3).setValue(" 7");                                                                                                          //Natural: ASSIGN #HDR-ARRAY ( 3 ) := ' 7'
                setValueToSubstring(pnd_Ws_Pnd_Da_Ppcn.getValue(4),pnd_Ws_Pnd_Hdr_Array.getValue(3),3,10);                                                                //Natural: MOVE #WS.#DA-PPCN ( 4 ) TO SUBSTR ( #HDR-ARRAY ( 3 ) ,3,10 )
                setValueToSubstring(pnd_Ws_Pnd_Da_Ppcn.getValue(5),pnd_Ws_Pnd_Hdr_Array.getValue(3),14,10);                                                               //Natural: MOVE #WS.#DA-PPCN ( 5 ) TO SUBSTR ( #HDR-ARRAY ( 3 ) ,14,10 )
                setValueToSubstring(pnd_Ws_Pnd_Da_Ppcn.getValue(6),pnd_Ws_Pnd_Hdr_Array.getValue(3),25,10);                                                               //Natural: MOVE #WS.#DA-PPCN ( 6 ) TO SUBSTR ( #HDR-ARRAY ( 3 ) ,25,10 )
                pnd_Ws_Pnd_Annt_Starting_Date.setValueEdited(pdaFcpa874h.getPnd_Fcpa874h_Pymnt_Ia_Issue_Dte(),new ReportEditMask("MM/DD/YYYY"));                          //Natural: MOVE EDITED #FCPA874H.PYMNT-IA-ISSUE-DTE ( EM = MM/DD/YYYY ) TO #WS.#ANNT-STARTING-DATE
                pnd_Ws_Pnd_Hdr_Array.getValue(4).setValue("13Annuity Starting Date:");                                                                                    //Natural: ASSIGN #HDR-ARRAY ( 4 ) := '13Annuity Starting Date:'
                setValueToSubstring(pnd_Ws_Pnd_Annt_Starting_Date,pnd_Ws_Pnd_Hdr_Array.getValue(4),26,10);                                                                //Natural: MOVE #WS.#ANNT-STARTING-DATE TO SUBSTR ( #HDR-ARRAY ( 4 ) ,26,10 )
                setValueToSubstring("Payment Date:",pnd_Ws_Pnd_Hdr_Array.getValue(4),40,13);                                                                              //Natural: MOVE 'Payment Date:' TO SUBSTR ( #HDR-ARRAY ( 4 ) ,40,13 )
                pnd_Ws_Pnd_Pymnt_Check_Dte.setValueEdited(pdaFcpa874h.getPnd_Fcpa874h_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                //Natural: MOVE EDITED #FCPA874H.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #WS.#PYMNT-CHECK-DTE
                setValueToSubstring(pnd_Ws_Pnd_Pymnt_Check_Dte,pnd_Ws_Pnd_Hdr_Array.getValue(4),54,10);                                                                   //Natural: MOVE #WS.#PYMNT-CHECK-DTE TO SUBSTR ( #HDR-ARRAY ( 4 ) ,54,10 )
                pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa874h.getPnd_Fcpa874h_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #FCPA874H.CNR-ORGNL-INVRSE-DTE
                getReports().display(0, pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A,"<==");                                                                                       //Natural: DISPLAY #ORGN-CHK-DTE-A '<=='
                if (Global.isEscape()) return;
                pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
                pnd_Ws_Pnd_Pymnt_Check_Dte.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YYYY"));                                       //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YYYY ) TO #WS.#PYMNT-CHECK-DTE
                setValueToSubstring("Original Check Date: ",pnd_Ws_Pnd_Hdr_Array.getValue(4),67,21);                                                                      //Natural: MOVE 'Original Check Date: ' TO SUBSTR ( #HDR-ARRAY ( 4 ) ,67,21 )
                setValueToSubstring(pnd_Ws_Pnd_Pymnt_Check_Dte,pnd_Ws_Pnd_Hdr_Array.getValue(4),88,10);                                                                   //Natural: MOVE #WS.#PYMNT-CHECK-DTE TO SUBSTR ( #HDR-ARRAY ( 4 ) ,88,10 )
                //*                                                              VF 8/99
                pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(3).setValue("17");                                                                                //Natural: ASSIGN #HEADER-ARRAY ( 3 ) := '17'
                setValueToSubstring(pnd_Ws_Pnd_Hdr_Array.getValue(4).getSubstring(40,24),pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(3),3,                    //Natural: MOVE SUBSTR ( #HDR-ARRAY ( 4 ) ,40,24 ) TO SUBSTR ( #HEADER-ARRAY ( 3 ) ,3,24 )
                    24);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Hdr_Array.getValue(3).setValue(" 7");                                                                                                          //Natural: ASSIGN #HDR-ARRAY ( 3 ) := ' 7'
                setValueToSubstring(pnd_Ws_Pnd_Da_Ppcn.getValue(4),pnd_Ws_Pnd_Hdr_Array.getValue(3),3,10);                                                                //Natural: MOVE #WS.#DA-PPCN ( 4 ) TO SUBSTR ( #HDR-ARRAY ( 3 ) ,3,10 )
                setValueToSubstring(pnd_Ws_Pnd_Da_Ppcn.getValue(5),pnd_Ws_Pnd_Hdr_Array.getValue(3),14,10);                                                               //Natural: MOVE #WS.#DA-PPCN ( 5 ) TO SUBSTR ( #HDR-ARRAY ( 3 ) ,14,10 )
                setValueToSubstring(pnd_Ws_Pnd_Da_Ppcn.getValue(6),pnd_Ws_Pnd_Hdr_Array.getValue(3),25,10);                                                               //Natural: MOVE #WS.#DA-PPCN ( 6 ) TO SUBSTR ( #HDR-ARRAY ( 3 ) ,25,10 )
                pnd_Ws_Pnd_Annt_Starting_Date.setValueEdited(pdaFcpa874h.getPnd_Fcpa874h_Pymnt_Ia_Issue_Dte(),new ReportEditMask("MM/DD/YYYY"));                          //Natural: MOVE EDITED #FCPA874H.PYMNT-IA-ISSUE-DTE ( EM = MM/DD/YYYY ) TO #WS.#ANNT-STARTING-DATE
                pnd_Ws_Pnd_Hdr_Array.getValue(4).setValue("13Annuity Starting Date:");                                                                                    //Natural: ASSIGN #HDR-ARRAY ( 4 ) := '13Annuity Starting Date:'
                setValueToSubstring(pnd_Ws_Pnd_Annt_Starting_Date,pnd_Ws_Pnd_Hdr_Array.getValue(4),26,10);                                                                //Natural: MOVE #WS.#ANNT-STARTING-DATE TO SUBSTR ( #HDR-ARRAY ( 4 ) ,26,10 )
                setValueToSubstring("Payment Date:",pnd_Ws_Pnd_Hdr_Array.getValue(4),40,13);                                                                              //Natural: MOVE 'Payment Date:' TO SUBSTR ( #HDR-ARRAY ( 4 ) ,40,13 )
                pnd_Ws_Pnd_Pymnt_Check_Dte.setValueEdited(pdaFcpa874h.getPnd_Fcpa874h_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                //Natural: MOVE EDITED #FCPA874H.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #WS.#PYMNT-CHECK-DTE
                setValueToSubstring(pnd_Ws_Pnd_Pymnt_Check_Dte,pnd_Ws_Pnd_Hdr_Array.getValue(4),54,10);                                                                   //Natural: MOVE #WS.#PYMNT-CHECK-DTE TO SUBSTR ( #HDR-ARRAY ( 4 ) ,54,10 )
                pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(3).setValue("17");                                                                                //Natural: ASSIGN #HEADER-ARRAY ( 3 ) := '17'
                setValueToSubstring(pnd_Ws_Pnd_Hdr_Array.getValue(4).getSubstring(40,24),pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(3),3,                    //Natural: MOVE SUBSTR ( #HDR-ARRAY ( 4 ) ,40,24 ) TO SUBSTR ( #HEADER-ARRAY ( 3 ) ,3,24 )
                    24);
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Hdr_Array.getValue(5).setValue(" 1");                                                                                                                  //Natural: ASSIGN #HDR-ARRAY ( 5 ) := ' 1'
        setValueToSubstring("Page:",pnd_Ws_Pnd_Hdr_Array.getValue(5),136,5);                                                                                              //Natural: MOVE 'Page:' TO SUBSTR ( #HDR-ARRAY ( 5 ) ,136,5 )
        getWorkFiles().write(8, false, pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(1));                                                                       //Natural: WRITE WORK FILE 8 #HEADER-ARRAY ( 1 )
        getWorkFiles().write(8, false, pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(2));                                                                       //Natural: WRITE WORK FILE 8 #HEADER-ARRAY ( 2 )
        if (condition(pdaFcpa874h.getPnd_Fcpa874h_Pnd_Current_Page().equals(1)))                                                                                          //Natural: IF #CURRENT-PAGE = 1
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 4
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(4)); pnd_Ws_Pnd_I.nadd(1))
            {
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Hdr_Array.getValue(pnd_Ws_Pnd_I));                                                                              //Natural: WRITE WORK FILE 8 #HDR-ARRAY ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpa874h.getPnd_Fcpa874h_Pnd_Current_Page().divide(2).add(1));                            //Natural: ASSIGN #J := #CURRENT-PAGE / 2 + 1
            pnd_Ws_Pnd_Page_Num.setValue(pnd_Ws_Pnd_J);                                                                                                                   //Natural: ASSIGN #PAGE-NUM := #J
            setValueToSubstring(pnd_Ws_Pnd_Page_Num,pnd_Ws_Pnd_Hdr_Array.getValue(5),141,3);                                                                              //Natural: MOVE #PAGE-NUM TO SUBSTR ( #HDR-ARRAY ( 5 ) ,141,3 )
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Hdr_Array.getValue(5));                                                                                             //Natural: WRITE WORK FILE 8 #HDR-ARRAY ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A,"<==");
    }
}
