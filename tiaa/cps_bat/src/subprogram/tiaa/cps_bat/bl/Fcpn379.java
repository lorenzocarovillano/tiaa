/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:23:11 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn379
************************************************************
**        * FILE NAME            : Fcpn379.java
**        * CLASS NAME           : Fcpn379
**        * INSTANCE NAME        : Fcpn379
************************************************************
*   THIS PROGRAM IS   NOT   CONSTRUCTABLE
*
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn379 extends BLNatBase
{
    // Data Areas
    private LdaFcpl200 ldaFcpl200;
    private LdaCdbatxa ldaCdbatxa;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Origin;
    private DbsField pnd_Type_Code;
    private DbsField pnd_Cnt;
    private DbsField pnd_Gross_Amt;
    private DbsField pnd_Net_Amt;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Key_For_Name_Address;

    private DbsGroup pnd_Control_Totals_Difference;
    private DbsField pnd_Control_Totals_Difference_Pnd_Ws_Type;
    private DbsField pnd_Control_Totals_Difference_Pnd_Type_Code_Diff;
    private DbsField pnd_Control_Totals_Difference_Pnd_Cnt_Diff;
    private DbsField pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff;
    private DbsField pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff;
    private DbsField pnd_I;
    private DbsField pnd_Ws_Origin_Max;
    private DbsField pnd_Ws_Start;
    private DbsField pnd_Ws_Type_X;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl200 = new LdaFcpl200();
        registerRecord(ldaFcpl200);
        registerRecord(ldaFcpl200.getVw_fcp_Cons_Cntrl_View());
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Origin = parameters.newFieldInRecord("pnd_Parm_Origin", "#PARM-ORIGIN", FieldType.STRING, 2);
        pnd_Parm_Origin.setParameterOption(ParameterOption.ByReference);
        pnd_Type_Code = parameters.newFieldArrayInRecord("pnd_Type_Code", "#TYPE-CODE", FieldType.STRING, 2, new DbsArrayController(1, 50));
        pnd_Type_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Cnt = parameters.newFieldArrayInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 50));
        pnd_Cnt.setParameterOption(ParameterOption.ByReference);
        pnd_Gross_Amt = parameters.newFieldArrayInRecord("pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 50));
        pnd_Gross_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Net_Amt = parameters.newFieldArrayInRecord("pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 50));
        pnd_Net_Amt.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Key_For_Name_Address = localVariables.newFieldInRecord("pnd_Key_For_Name_Address", "#KEY-FOR-NAME-ADDRESS", FieldType.STRING, 31);

        pnd_Control_Totals_Difference = localVariables.newGroupArrayInRecord("pnd_Control_Totals_Difference", "#CONTROL-TOTALS-DIFFERENCE", new DbsArrayController(1, 
            50));
        pnd_Control_Totals_Difference_Pnd_Ws_Type = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Ws_Type", "#WS-TYPE", 
            FieldType.STRING, 19);
        pnd_Control_Totals_Difference_Pnd_Type_Code_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Type_Code_Diff", 
            "#TYPE-CODE-DIFF", FieldType.STRING, 2);
        pnd_Control_Totals_Difference_Pnd_Cnt_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Cnt_Diff", "#CNT-DIFF", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff", 
            "#GROSS-AMT-DIFF", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff", 
            "#NET-AMT-DIFF", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Ws_Origin_Max = localVariables.newFieldInRecord("pnd_Ws_Origin_Max", "#WS-ORIGIN-MAX", FieldType.NUMERIC, 2);
        pnd_Ws_Start = localVariables.newFieldInRecord("pnd_Ws_Start", "#WS-START", FieldType.NUMERIC, 2);
        pnd_Ws_Type_X = localVariables.newFieldArrayInRecord("pnd_Ws_Type_X", "#WS-TYPE-X", FieldType.STRING, 19, new DbsArrayController(1, 50));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl200.initializeValues();
        ldaCdbatxa.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("  CANCEL/REDRAW IA BATCH EXTRACT CONTROL REPORT");
        pnd_Key_For_Name_Address.setInitialValue(0);
        pnd_Control_Totals_Difference_Pnd_Ws_Type.getValue(1).setInitialValue(" ");
        pnd_Control_Totals_Difference_Pnd_Type_Code_Diff.getValue(1).setInitialValue(" ");
        pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue(1).setInitialValue(0);
        pnd_I.setInitialValue(1);
        pnd_Ws_Origin_Max.setInitialValue(0);
        pnd_Ws_Start.setInitialValue(0);
        pnd_Ws_Type_X.getValue(1).setInitialValue("Periodic  -  cancel");
        pnd_Ws_Type_X.getValue(2).setInitialValue("Periodic  -  stop  ");
        pnd_Ws_Type_X.getValue(3).setInitialValue("Periodic  -  redraw");
        pnd_Ws_Type_X.getValue(46).setInitialValue("Total Cancels - ia ");
        pnd_Ws_Type_X.getValue(47).setInitialValue("Total Stops   - ia ");
        pnd_Ws_Type_X.getValue(48).setInitialValue("Total Checks  - ia ");
        pnd_Ws_Type_X.getValue(49).setInitialValue("Total Eft     - ia ");
        pnd_Ws_Type_X.getValue(50).setInitialValue("Total Globals - ia ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn379() throws Exception
    {
        super("Fcpn379");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        ldaFcpl200.getVw_fcp_Cons_Cntrl_View().startDatabaseRead                                                                                                          //Natural: READ ( 1 ) FCP-CONS-CNTRL-VIEW BY CNTL-ORG-CDE-INVRSE-DTE = #PARM-ORIGIN
        (
        "READ01",
        new Wc[] { new Wc("CNTL_ORG_CDE_INVRSE_DTE", ">=", pnd_Parm_Origin, WcType.BY) },
        new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcpl200.getVw_fcp_Cons_Cntrl_View().readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        short decideConditionsMet88 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE #PARM-ORIGIN;//Natural: VALUE 'CI'
        if (condition((pnd_Parm_Origin.equals("CI"))))
        {
            decideConditionsMet88++;
                                                                                                                                                                          //Natural: PERFORM IA-CONTROL-TOTALS
            sub_Ia_Control_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Origin_Max.setValue(50);                                                                                                                               //Natural: ASSIGN #WS-ORIGIN-MAX = 50
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(19),"DATABASE TOTALS", new FieldAttributes ("AD=I"),new ColumnSpacing(35),"EXTRACT TOTALS",          //Natural: WRITE ( 1 ) 19X 'DATABASE TOTALS' ( I ) 35X 'EXTRACT TOTALS' ( I ) 26X 'DIFFERENCE' ( I )
            new FieldAttributes ("AD=I"),new ColumnSpacing(26),"DIFFERENCE", new FieldAttributes ("AD=I"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"-",new RepeatItem(54), new FieldAttributes ("AD=I"),new ColumnSpacing(7),"-",new RepeatItem(32), new                  //Natural: WRITE ( 1 ) '-' ( 54 ) ( I ) 7X '-' ( 32 ) ( I ) 6X '-' ( 32 ) ( I )
            FieldAttributes ("AD=I"),new ColumnSpacing(6),"-",new RepeatItem(32), new FieldAttributes ("AD=I"));
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TYPE", new FieldAttributes ("AD=I"),new ColumnSpacing(19),"COUNT", new FieldAttributes ("AD=I"),new                   //Natural: WRITE ( 1 ) 'TYPE' ( I ) 19X 'COUNT' ( I ) 1X 'GROSS AMOUNT' ( I ) 3X 'NET AMOUNT' ( I ) 7X 'COUNT' ( I ) 2X 'GROSS AMOUNT' ( I ) 3X 'NET AMOUNT' ( I ) 6X 'COUNT' ( I ) 2X 'GROSS AMOUNT' ( I ) 3X 'NET AMOUNT' ( I )
            ColumnSpacing(1),"GROSS AMOUNT", new FieldAttributes ("AD=I"),new ColumnSpacing(3),"NET AMOUNT", new FieldAttributes ("AD=I"),new ColumnSpacing(7),"COUNT", 
            new FieldAttributes ("AD=I"),new ColumnSpacing(2),"GROSS AMOUNT", new FieldAttributes ("AD=I"),new ColumnSpacing(3),"NET AMOUNT", new FieldAttributes 
            ("AD=I"),new ColumnSpacing(6),"COUNT", new FieldAttributes ("AD=I"),new ColumnSpacing(2),"GROSS AMOUNT", new FieldAttributes ("AD=I"),new ColumnSpacing(3),"NET AMOUNT", 
            new FieldAttributes ("AD=I"));
        if (Global.isEscape()) return;
        pnd_Ws_Start.setValue(1);                                                                                                                                         //Natural: ASSIGN #WS-START := 1
        pnd_Ws_Origin_Max.setValue(3);                                                                                                                                    //Natural: ASSIGN #WS-ORIGIN-MAX := 3
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Start.setValue(46);                                                                                                                                        //Natural: ASSIGN #WS-START := 46
        pnd_Ws_Origin_Max.setValue(50);                                                                                                                                   //Natural: ASSIGN #WS-ORIGIN-MAX := 50
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue("*").notEquals(getZero()) || pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue("*").notEquals(getZero())  //Natural: IF #CNT-DIFF ( * ) NOT = 0 OR #GROSS-AMT-DIFF ( * ) NOT = 0 OR #NET-AMT-DIFF ( * ) NOT = 0
            || pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue("*").notEquals(getZero())))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)", new FieldAttributes              //Natural: WRITE ( 1 ) /// 'JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)' ( I ) 1X 'ARE MIS-MATCHED AND/OR OUT OF BALANCE' ( I )
                ("AD=I"),new ColumnSpacing(1),"ARE MIS-MATCHED AND/OR OUT OF BALANCE", new FieldAttributes ("AD=I"));
            if (Global.isEscape()) return;
            DbsUtil.terminate(98);  if (true) return;                                                                                                                     //Natural: TERMINATE 0098
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-CONTROL-TOTALS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
    }
    private void sub_Ia_Control_Totals() throws Exception                                                                                                                 //Natural: IA-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Header0_2.setValue("CANCEL/REDRAW IA BATCH EXTRACT CONTROL REPORT");                                                                                          //Natural: ASSIGN #HEADER0-2 = 'CANCEL/REDRAW IA BATCH EXTRACT CONTROL REPORT'
        pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue(1,":",3).compute(new ComputeParameters(false, pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue(1,":",3)),  //Natural: SUBTRACT FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1:3 ) FROM #CNT ( 1:3 ) GIVING #CNT-DIFF ( 1:3 )
            pnd_Cnt.getValue(1,":",3).subtract(ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(1,":",3)));
        pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue(1,":",3).compute(new ComputeParameters(false, pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue(1,":",3)),  //Natural: SUBTRACT FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1:3 ) FROM #GROSS-AMT ( 1:3 ) GIVING #GROSS-AMT-DIFF ( 1:3 )
            pnd_Gross_Amt.getValue(1,":",3).subtract(ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(1,":",3)));
        pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue(1,":",3).compute(new ComputeParameters(false, pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue(1,":",3)),  //Natural: SUBTRACT FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1:3 ) FROM #NET-AMT ( 1:3 ) GIVING #NET-AMT-DIFF ( 1:3 )
            pnd_Net_Amt.getValue(1,":",3).subtract(ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(1,":",3)));
        pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue(46,":",50).compute(new ComputeParameters(false, pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue(46,":",50)),  //Natural: SUBTRACT FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 46:50 ) FROM #CNT ( 46:50 ) GIVING #CNT-DIFF ( 46:50 )
            pnd_Cnt.getValue(46,":",50).subtract(ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(46,":",50)));
        pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue(46,":",50).compute(new ComputeParameters(false, pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue(46,":",50)),  //Natural: SUBTRACT FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 46:50 ) FROM #GROSS-AMT ( 46:50 ) GIVING #GROSS-AMT-DIFF ( 46:50 )
            pnd_Gross_Amt.getValue(46,":",50).subtract(ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(46,":",50)));
        pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue(46,":",50).compute(new ComputeParameters(false, pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue(46,":",50)),  //Natural: SUBTRACT FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 46:50 ) FROM #NET-AMT ( 46:50 ) GIVING #NET-AMT-DIFF ( 46:50 )
            pnd_Net_Amt.getValue(46,":",50).subtract(ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(46,":",50)));
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Parm_Origin.equals("CI")))                                                                                                                      //Natural: IF #PARM-ORIGIN = 'CI'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I #WS-START TO #WS-ORIGIN-MAX
            for (pnd_I.setValue(pnd_Ws_Start); condition(pnd_I.lessOrEqual(pnd_Ws_Origin_Max)); pnd_I.nadd(1))
            {
                getReports().write(1, ReportOption.NOTITLE,pnd_Ws_Type_X.getValue(pnd_I),new ColumnSpacing(2),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(pnd_I),new  //Natural: WRITE ( 1 ) #WS-TYPE-X ( #I ) 2X FCP-CONS-CNTRL-VIEW.CNTL-CNT ( #I ) 1X FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( #I ) 1X FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( #I ) 5X #CNT ( #I ) 2X #GROSS-AMT ( #I ) #NET-AMT ( #I ) 4X #CNT-DIFF ( #I ) 2X #GROSS-AMT-DIFF ( #I ) 1X #NET-AMT-DIFF ( #I )
                    ColumnSpacing(1),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(pnd_I),new ColumnSpacing(1),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(pnd_I),new 
                    ColumnSpacing(5),pnd_Cnt.getValue(pnd_I),new ColumnSpacing(2),pnd_Gross_Amt.getValue(pnd_I),pnd_Net_Amt.getValue(pnd_I),new ColumnSpacing(4),pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue(pnd_I),new 
                    ColumnSpacing(2),pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue(pnd_I),new ColumnSpacing(1),pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue(pnd_I));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 41T #HEADER0-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
