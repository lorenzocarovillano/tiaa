/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:37 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn803h
************************************************************
**        * FILE NAME            : Fcpn803h.java
**        * CLASS NAME           : Fcpn803h
**        * INSTANCE NAME        : Fcpn803h
************************************************************
************************************************************************
* SUBPROGRAM : FCPN803H
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AP" ANNUITANT STATEMENTS - HEADERS
* CHANGE HISTORY
* 12/20/05 - RAMANA ANNE - ADDED NEW PARMAMETER AREA FOR PAYEE-MATCH
*                          PROJECT.CHANGE CHECK-NUMBER N7 TO N10
*                          IN FCPA803H PARAMETER AREA
* 08/23/2019 : REPLACING PDQ MODULES WITH ADS MODULES I.E.
*              PDQA360,PDQA362,PDQN360,PDQN362 TO
*              ADSA360,ADSA362,ADSN360,ADSN362 RESPECTIVELY. -TAG:VIKRAM
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn803h extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803h pdaFcpa803h;
    private PdaFcpa803c pdaFcpa803c;
    private LdaFcpl803h ldaFcpl803h;
    private PdaAdsa360 pdaAdsa360;
    private PdaAdsa362 pdaAdsa362;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Fcpn803h;
    private DbsField pnd_Fcpn803h_Pymnt_Check_Nbr;
    private DbsField pnd_Fcpn803h_Pymnt_Check_Dte;
    private DbsField pnd_Fcpn803h_Pnd_I;
    private DbsField pnd_Fcpn803h_Pnd_J;
    private DbsField pnd_Orgn_Chk_Dte;

    private DbsGroup pnd_Orgn_Chk_Dte__R_Field_1;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Filler;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A;
    private DbsField pnd_Orgn_Chk_Dte_A4;

    private DbsGroup pnd_Orgn_Chk_Dte_A4__R_Field_2;
    private DbsField pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D;
    private DbsField pnd_Edited_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl803h = new LdaFcpl803h();
        registerRecord(ldaFcpl803h);
        localVariables = new DbsRecord();
        pdaAdsa360 = new PdaAdsa360(localVariables);
        pdaAdsa362 = new PdaAdsa362(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803h = new PdaFcpa803h(parameters);
        pdaFcpa803c = new PdaFcpa803c(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Fcpn803h = localVariables.newGroupInRecord("pnd_Fcpn803h", "#FCPN803H");
        pnd_Fcpn803h_Pymnt_Check_Nbr = pnd_Fcpn803h.newFieldInGroup("pnd_Fcpn803h_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.STRING, 10);
        pnd_Fcpn803h_Pymnt_Check_Dte = pnd_Fcpn803h.newFieldInGroup("pnd_Fcpn803h_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.STRING, 10);
        pnd_Fcpn803h_Pnd_I = pnd_Fcpn803h.newFieldInGroup("pnd_Fcpn803h_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpn803h_Pnd_J = pnd_Fcpn803h.newFieldInGroup("pnd_Fcpn803h_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Orgn_Chk_Dte = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte", "#ORGN-CHK-DTE", FieldType.NUMERIC, 9);

        pnd_Orgn_Chk_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte__R_Field_1", "REDEFINE", pnd_Orgn_Chk_Dte);
        pnd_Orgn_Chk_Dte_Pnd_Filler = pnd_Orgn_Chk_Dte__R_Field_1.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A = pnd_Orgn_Chk_Dte__R_Field_1.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A", "#ORGN-CHK-DTE-A", FieldType.STRING, 
            8);
        pnd_Orgn_Chk_Dte_A4 = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte_A4", "#ORGN-CHK-DTE-A4", FieldType.STRING, 4);

        pnd_Orgn_Chk_Dte_A4__R_Field_2 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte_A4__R_Field_2", "REDEFINE", pnd_Orgn_Chk_Dte_A4);
        pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D = pnd_Orgn_Chk_Dte_A4__R_Field_2.newFieldInGroup("pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D", "#ORGN-CHK-DTE-D", 
            FieldType.DATE);
        pnd_Edited_Date = localVariables.newFieldInRecord("pnd_Edited_Date", "#EDITED-DATE", FieldType.STRING, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803h.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn803h() throws Exception
    {
        super("Fcpn803h");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        if (condition(pdaFcpa803h.getPnd_Fcpa803h_Pnd_Gen_Headers().getBoolean()))                                                                                        //Natural: IF #GEN-HEADERS
        {
            pdaFcpa803h.getPnd_Fcpa803h_Pnd_Gen_Headers().setValue(false);                                                                                                //Natural: MOVE FALSE TO #GEN-HEADERS
            pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue("*").reset();                                                                                         //Natural: RESET #HEADER-ARRAY ( * )
            pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(1).setValue("11");                                                                                    //Natural: MOVE '11' TO #HEADER-ARRAY ( 1 )
            setValueToSubstring(ldaFcpl803h.getPnd_Fcpl803h_Pnd_Header(),pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(1),3,ldaFcpl803h.getPnd_Fcpl803h_Pnd_Header_Top().getInt()); //Natural: MOVE #HEADER TO SUBSTR ( #HEADER-ARRAY ( 1 ) ,3,#HEADER-TOP )
            pdaAdsa360.getPnd_In_Data_Pnd_First_Name().setValue(pdaFcpa803h.getPnd_Fcpa803h_Ph_First_Name());                                                             //Natural: MOVE PH-FIRST-NAME TO #FIRST-NAME
            pdaAdsa362.getPnd_In_Middle_Name().setValue(pdaFcpa803h.getPnd_Fcpa803h_Ph_Middle_Name());                                                                    //Natural: MOVE PH-MIDDLE-NAME TO #IN-MIDDLE-NAME
            pdaAdsa360.getPnd_In_Data_Pnd_Last_Name().setValue(pdaFcpa803h.getPnd_Fcpa803h_Ph_Last_Name());                                                               //Natural: MOVE PH-LAST-NAME TO #LAST-NAME
            //* *  CALLNAT 'PDQN362'     /* VIKRAM
            //*  VIKRAM
            DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pdaAdsa362.getPnd_In_Middle_Name(), pdaAdsa362.getPnd_Out_Middle_Name(), pdaAdsa362.getPnd_Out_Suffix(),  //Natural: CALLNAT 'ADSN362' USING #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
                pdaAdsa362.getPnd_Out_Prefix(), pdaAdsa362.getPnd_Out_Return_Code());
            if (condition(Global.isEscape())) return;
            pdaAdsa360.getPnd_In_Data_Pnd_Prefix().setValue(pdaAdsa362.getPnd_Out_Prefix());                                                                              //Natural: MOVE #OUT-PREFIX TO #PREFIX
            pdaAdsa360.getPnd_In_Data_Pnd_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                    //Natural: MOVE #OUT-MIDDLE-NAME TO #MIDDLE-NAME
            pdaAdsa360.getPnd_In_Data_Pnd_Suffix().setValue(pdaAdsa362.getPnd_Out_Suffix());                                                                              //Natural: MOVE #OUT-SUFFIX TO #SUFFIX
            pdaAdsa360.getPnd_In_Data_Pnd_Length().setValue(38);                                                                                                          //Natural: MOVE 38 TO #LENGTH
            pdaAdsa360.getPnd_In_Data_Pnd_Format().setValue("1");                                                                                                         //Natural: MOVE '1' TO #FORMAT
            //* *CALLNAT 'PDQN360' USING #IN-DATA #OUT-DATA /* VIKRAM
            //*  VIKRAM
            DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pdaAdsa360.getPnd_In_Data(), pdaAdsa360.getPnd_Out_Data());                                         //Natural: CALLNAT 'ADSN360' USING #IN-DATA #OUT-DATA
            if (condition(Global.isEscape())) return;
            pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(2).setValue("+2");                                                                                    //Natural: MOVE '+2' TO #HEADER-ARRAY ( 2 )
            if (condition(pdaAdsa360.getPnd_Out_Data_Pnd_Length_Out().equals(getZero())))                                                                                 //Natural: IF #LENGTH-OUT = 0
            {
                pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(2).reset();                                                                                       //Natural: RESET #HEADER-ARRAY ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring(pdaAdsa360.getPnd_Out_Data_Pnd_Name_Out(),pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(2),38,pdaAdsa360.getPnd_Out_Data_Pnd_Length_Out().getInt()); //Natural: MOVE #NAME-OUT TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,38,#LENGTH-OUT )
            }                                                                                                                                                             //Natural: END-IF
            //* ***(((((
            //*                                                           VF 8/99
            if (condition(pdaFcpa803h.getPnd_Fcpa803h_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                       //Natural: IF #FCPA803H.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                pnd_Fcpn803h_Pymnt_Check_Dte.setValueEdited(pdaFcpa803h.getPnd_Fcpa803h_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                              //Natural: MOVE EDITED #FCPA803H.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #FCPN803H.PYMNT-CHECK-DTE
                pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(3).setValue("13Check Date:");                                                                     //Natural: MOVE '13Check Date:' TO #HEADER-ARRAY ( 3 )
                setValueToSubstring(pnd_Fcpn803h_Pymnt_Check_Dte,pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(3),15,10);                                       //Natural: MOVE #FCPN803H.PYMNT-CHECK-DTE TO SUBSTR ( #HEADER-ARRAY ( 3 ) ,15,10 )
                pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa803c.getPnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #CHECK-SORT-FIELDS.CNR-ORGNL-INVRSE-DTE
                pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
                pnd_Fcpn803h_Pymnt_Check_Dte.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YYYY"));                                     //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YYYY ) TO #FCPN803H.PYMNT-CHECK-DTE
                setValueToSubstring("Installment Date: ",pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(3),27,18);                                               //Natural: MOVE 'Installment Date: ' TO SUBSTR ( #HEADER-ARRAY ( 3 ) ,27,18 )
                setValueToSubstring(pnd_Fcpn803h_Pymnt_Check_Dte,pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(3),45,10);                                       //Natural: MOVE #FCPN803H.PYMNT-CHECK-DTE TO SUBSTR ( #HEADER-ARRAY ( 3 ) ,45,10 )
                //*                                                           VF 8/99
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Fcpn803h_Pymnt_Check_Dte.setValueEdited(pdaFcpa803h.getPnd_Fcpa803h_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                              //Natural: MOVE EDITED #FCPA803H.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #FCPN803H.PYMNT-CHECK-DTE
                pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(3).setValue("13Check Date:");                                                                     //Natural: MOVE '13Check Date:' TO #HEADER-ARRAY ( 3 )
                setValueToSubstring(pnd_Fcpn803h_Pymnt_Check_Dte,pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(3),15,10);                                       //Natural: MOVE #FCPN803H.PYMNT-CHECK-DTE TO SUBSTR ( #HEADER-ARRAY ( 3 ) ,15,10 )
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(4).setValue("+3");                                                                                    //Natural: MOVE '+3' TO #HEADER-ARRAY ( 4 )
            //*  MOVE  #FCPA803H.PYMNT-CHECK-NBR TO #FCPN803H.PYMNT-CHECK-NBR /* RA
            pnd_Fcpn803h_Pymnt_Check_Nbr.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());                                                                      //Natural: MOVE #CHECK-SORT-FIELDS.PYMNT-NBR TO #FCPN803H.PYMNT-CHECK-NBR
            setValueToSubstring("Check Number:",pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(4),30,13);                                                        //Natural: MOVE 'Check Number:' TO SUBSTR ( #HEADER-ARRAY ( 4 ) ,30,13 )
            setValueToSubstring(pnd_Fcpn803h_Pymnt_Check_Nbr,pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(4),44,10);                                           //Natural: MOVE #FCPN803H.PYMNT-CHECK-NBR TO SUBSTR ( #HEADER-ARRAY ( 4 ) ,44,10 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803h.getPnd_Fcpa803h_Pnd_Stmnt().getBoolean() || pdaFcpa803h.getPnd_Fcpa803h_Pnd_Current_Page().equals(1)))                                  //Natural: IF #STMNT OR #CURRENT-PAGE = 1
        {
            pnd_Fcpn803h_Pnd_J.setValue(3);                                                                                                                               //Natural: ASSIGN #J := 3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fcpn803h_Pnd_J.setValue(4);                                                                                                                               //Natural: ASSIGN #J := 4
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #J
        for (pnd_Fcpn803h_Pnd_I.setValue(1); condition(pnd_Fcpn803h_Pnd_I.lessOrEqual(pnd_Fcpn803h_Pnd_J)); pnd_Fcpn803h_Pnd_I.nadd(1))
        {
            getWorkFiles().write(8, false, pdaFcpa803h.getPnd_Fcpa803h_Pnd_Header_Array().getValue(pnd_Fcpn803h_Pnd_I));                                                  //Natural: WRITE WORK FILE 8 #HEADER-ARRAY ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
