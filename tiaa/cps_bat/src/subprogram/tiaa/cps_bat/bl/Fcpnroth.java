/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:29 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnroth
************************************************************
**        * FILE NAME            : Fcpnroth.java
**        * CLASS NAME           : Fcpnroth
**        * INSTANCE NAME        : Fcpnroth
************************************************************
***********************************************************************
*
* SUBPROGRAM:  FCPROTH
*
* TITLE:    CPS ROTH DETERMINATION
* AUTHOR:   JOHN OSTEEN
* CREATED:  06/09/2009
*
* DESC:     THIS SUBPROGRAM IS CALLED TO DETERMINE IF THE PAYMENT BEING
*           PROCESSED FOR PRINTING CANTAINS ROTH MONIES.
*
* HISTORY:
*
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnroth extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803 pdaFcpa803;

    // Local Variables
    public DbsRecord localVariables;

    // parameters

    private DbsGroup pnd_Pymnt_Info;
    private DbsField pnd_Pymnt_Info_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_Info_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_Info_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_Info_Pymnt_Prcss_Seq_Nbr;

    private DataAccessProgramView vw_pymnt;
    private DbsField pymnt_Cntrct_Orgn_Cde;
    private DbsField pymnt_Cntrct_Invrse_Dte;
    private DbsField pymnt_Pymnt_Check_Dte;
    private DbsField pymnt_Cntrct_Ppcn_Nbr;
    private DbsField pymnt_Cntrct_Payee_Cde;
    private DbsField pymnt_Pymnt_Prcss_Seq_Nbr;
    private DbsField pymnt_Roth_Money_Source;
    private DbsField pymnt_Ppcn_Inv_Orgn_Prcss_Inst;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Detail;
    private DbsField pnd_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Pymnt_Prcss_Seq_Nbr;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        pnd_Pymnt_Info = parameters.newGroupInRecord("pnd_Pymnt_Info", "#PYMNT-INFO");
        pnd_Pymnt_Info.setParameterOption(ParameterOption.ByReference);
        pnd_Pymnt_Info_Cntrct_Ppcn_Nbr = pnd_Pymnt_Info.newFieldInGroup("pnd_Pymnt_Info_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_Info_Cntrct_Invrse_Dte = pnd_Pymnt_Info.newFieldInGroup("pnd_Pymnt_Info_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Pymnt_Info_Cntrct_Orgn_Cde = pnd_Pymnt_Info.newFieldInGroup("pnd_Pymnt_Info_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_Info_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_Info.newFieldInGroup("pnd_Pymnt_Info_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        pdaFcpa803 = new PdaFcpa803(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_pymnt = new DataAccessProgramView(new NameInfo("vw_pymnt", "PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        pymnt_Cntrct_Orgn_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pymnt_Cntrct_Invrse_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        pymnt_Pymnt_Check_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        pymnt_Cntrct_Ppcn_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        pymnt_Cntrct_Payee_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        pymnt_Pymnt_Prcss_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");
        pymnt_Roth_Money_Source = vw_pymnt.getRecord().newFieldInGroup("pymnt_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "ROTH_MONEY_SOURCE");
        pymnt_Ppcn_Inv_Orgn_Prcss_Inst = vw_pymnt.getRecord().newFieldInGroup("pymnt_Ppcn_Inv_Orgn_Prcss_Inst", "PPCN-INV-ORGN-PRCSS-INST", FieldType.STRING, 
            29, RepeatingFieldStrategy.None, "PPCN_INV_ORGN_PRCSS_INST");
        pymnt_Ppcn_Inv_Orgn_Prcss_Inst.setSuperDescriptor(true);
        registerRecord(vw_pymnt);

        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 29);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Detail = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Cntrct_Ppcn_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Key_Cntrct_Invrse_Dte = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Pymnt_Prcss_Seq_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_pymnt.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnroth() throws Exception
    {
        super("Fcpnroth");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFcpa803.getPnd_Fcpa803_Pnd_Roth_Ind().reset();                                                                                                                 //Natural: RESET #ROTH-IND
        pnd_Key_Pnd_Key_Detail.setValuesByName(pnd_Pymnt_Info);                                                                                                           //Natural: MOVE BY NAME #PYMNT-INFO TO #KEY-DETAIL
        vw_pymnt.startDatabaseRead                                                                                                                                        //Natural: READ ( 1 ) PYMNT BY PPCN-INV-ORGN-PRCSS-INST STARTING FROM #KEY ENDING AT #KEY
        (
        "READ01",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Key, "And", WcType.BY) ,
        new Wc("PPCN_INV_ORGN_PRCSS_INST", "<=", pnd_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        READ01:
        while (condition(vw_pymnt.readNextRow("READ01")))
        {
            if (condition(pymnt_Roth_Money_Source.equals("ROTHP")))                                                                                                       //Natural: IF ROTH-MONEY-SOURCE = 'ROTHP'
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Roth_Ind().setValue(true);                                                                                                  //Natural: MOVE TRUE TO #ROTH-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
