/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:20 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn753a
************************************************************
**        * FILE NAME            : Fcpn753a.java
**        * CLASS NAME           : Fcpn753a
**        * INSTANCE NAME        : Fcpn753a
************************************************************
************************************************************************
* FCPN753A FOR PAYEE MATCH
*
*
*       PERFORMS THE CHECK PRINT PORTION OF THE STATEMENTS.
* R. LANDRUM    01/05/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn753a extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Index;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_Ws_Address;
    private DbsField pnd_Ws_Address_Lines_Printed;

    private DbsGroup pnd_Ws_Postnet_Barcode;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Zip_Code;

    private DbsGroup pnd_Ws_Postnet_Barcode__R_Field_1;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_1;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_2;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_3;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_4;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_5;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_6;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_7;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_8;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_9;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_10;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_11;

    private DbsGroup pnd_Ws_Postnet_Barcode_Pnd_Ws_Delivery_Point;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_12;
    private DbsField pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_13;
    private DbsField pnd_Ws_Total;

    private DbsGroup pnd_Ws_Total__R_Field_2;
    private DbsField pnd_Ws_Total_Pnd_Ws_Total_Pos_1;
    private DbsField pnd_Ws_Total_Pnd_Ws_Total_Pos_2;
    private DbsField pnd_Ws_Check_Digit;

    private DbsGroup pnd_Ws_Check_Digit__R_Field_3;
    private DbsField pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_1;
    private DbsField pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        pnd_Ws_Index = parameters.newFieldInRecord("pnd_Ws_Index", "#WS-INDEX", FieldType.INTEGER, 2);
        pnd_Ws_Index.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_Ws_Address = localVariables.newFieldInRecord("pnd_Ws_Address", "#WS-ADDRESS", FieldType.STRING, 35);
        pnd_Ws_Address_Lines_Printed = localVariables.newFieldInRecord("pnd_Ws_Address_Lines_Printed", "#WS-ADDRESS-LINES-PRINTED", FieldType.NUMERIC, 
            1);

        pnd_Ws_Postnet_Barcode = localVariables.newGroupInRecord("pnd_Ws_Postnet_Barcode", "#WS-POSTNET-BARCODE");
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Zip_Code = pnd_Ws_Postnet_Barcode.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Zip_Code", "#WS-ZIP-CODE", FieldType.STRING, 
            13);

        pnd_Ws_Postnet_Barcode__R_Field_1 = pnd_Ws_Postnet_Barcode.newGroupInGroup("pnd_Ws_Postnet_Barcode__R_Field_1", "REDEFINE", pnd_Ws_Postnet_Barcode_Pnd_Ws_Zip_Code);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_1 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_1", "#WS-POS-1", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_2 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_2", "#WS-POS-2", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_3 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_3", "#WS-POS-3", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_4 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_4", "#WS-POS-4", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_5 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_5", "#WS-POS-5", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_6 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_6", "#WS-POS-6", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_7 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_7", "#WS-POS-7", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_8 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_8", "#WS-POS-8", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_9 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_9", "#WS-POS-9", FieldType.NUMERIC, 
            1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_10 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_10", "#WS-POS-10", 
            FieldType.STRING, 1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_11 = pnd_Ws_Postnet_Barcode__R_Field_1.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_11", "#WS-POS-11", 
            FieldType.STRING, 1);

        pnd_Ws_Postnet_Barcode_Pnd_Ws_Delivery_Point = pnd_Ws_Postnet_Barcode__R_Field_1.newGroupInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Delivery_Point", 
            "#WS-DELIVERY-POINT");
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_12 = pnd_Ws_Postnet_Barcode_Pnd_Ws_Delivery_Point.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_12", "#WS-POS-12", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_13 = pnd_Ws_Postnet_Barcode_Pnd_Ws_Delivery_Point.newFieldInGroup("pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_13", "#WS-POS-13", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Total = localVariables.newFieldInRecord("pnd_Ws_Total", "#WS-TOTAL", FieldType.NUMERIC, 2);

        pnd_Ws_Total__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Total__R_Field_2", "REDEFINE", pnd_Ws_Total);
        pnd_Ws_Total_Pnd_Ws_Total_Pos_1 = pnd_Ws_Total__R_Field_2.newFieldInGroup("pnd_Ws_Total_Pnd_Ws_Total_Pos_1", "#WS-TOTAL-POS-1", FieldType.NUMERIC, 
            1);
        pnd_Ws_Total_Pnd_Ws_Total_Pos_2 = pnd_Ws_Total__R_Field_2.newFieldInGroup("pnd_Ws_Total_Pnd_Ws_Total_Pos_2", "#WS-TOTAL-POS-2", FieldType.NUMERIC, 
            1);
        pnd_Ws_Check_Digit = localVariables.newFieldInRecord("pnd_Ws_Check_Digit", "#WS-CHECK-DIGIT", FieldType.NUMERIC, 2);

        pnd_Ws_Check_Digit__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Digit__R_Field_3", "REDEFINE", pnd_Ws_Check_Digit);
        pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_1 = pnd_Ws_Check_Digit__R_Field_3.newFieldInGroup("pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_1", "#WS-CD-POS-1", FieldType.NUMERIC, 
            1);
        pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_2 = pnd_Ws_Check_Digit__R_Field_3.newFieldInGroup("pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_2", "#WS-CD-POS-2", FieldType.NUMERIC, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn753a() throws Exception
    {
        super("Fcpn753a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        //* *ASSIGN #PROGRAM = *PROGRAM
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *
        //* *
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        //* *SAG DEFINE EXIT AFTER-PRIME-READ
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        //*  FC - CHECK POSTNET BARCODE LINE
        pnd_Ws_Total.reset();                                                                                                                                             //Natural: RESET #WS-TOTAL
        pnd_Ws_Check_Digit.reset();                                                                                                                                       //Natural: RESET #WS-CHECK-DIGIT
        pnd_Ws_Postnet_Barcode_Pnd_Ws_Zip_Code.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Postl_Data().getValue(pnd_Ws_Index));                                   //Natural: ASSIGN #WS-ZIP-CODE = PYMNT-POSTL-DATA ( #WS-INDEX )
        //*  DELIVERY-POINT
        if (condition(DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Postl_Data().getValue(pnd_Ws_Index),"...........NN") && DbsUtil.maskMatches(pnd_Ws_Postnet_Barcode_Pnd_Ws_Zip_Code, //Natural: IF PYMNT-POSTL-DATA ( #WS-INDEX ) = MASK ( ...........NN ) AND #WS-ZIP-CODE = MASK ( NNNNNNNNN )
            "NNNNNNNNN")))
        {
            pnd_Ws_Total.compute(new ComputeParameters(false, pnd_Ws_Total), pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_1.add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_2).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_3).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_4).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_5).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_6).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_7).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_8).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_9).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_12).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_13)); //Natural: ADD #WS-POS-1 #WS-POS-2 #WS-POS-3 #WS-POS-4 #WS-POS-5 #WS-POS-6 #WS-POS-7 #WS-POS-8 #WS-POS-9 #WS-POS-12 #WS-POS-13 GIVING #WS-TOTAL
            pnd_Ws_Check_Digit.compute(new ComputeParameters(false, pnd_Ws_Check_Digit), DbsField.subtract(10,pnd_Ws_Total_Pnd_Ws_Total_Pos_2));                          //Natural: SUBTRACT #WS-TOTAL-POS-2 FROM 10 GIVING #WS-CHECK-DIGIT
            //*  COMPRESS '18*B'
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "18B", pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_1, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_2,        //Natural: COMPRESS '18B' #WS-POS-1 #WS-POS-2 #WS-POS-3 #WS-POS-4 #WS-POS-5 #WS-POS-6 #WS-POS-7 #WS-POS-8 #WS-POS-9 #WS-POS-12 #WS-POS-13 #WS-CD-POS-2 'E' INTO #WS-REC-1 LEAVING NO SPACE
                pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_3, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_4, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_5, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_6, 
                pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_7, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_8, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_9, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_12, 
                pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_13, pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_2, "E"));
            //*  EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(DbsUtil.maskMatches(pnd_Ws_Postnet_Barcode_Pnd_Ws_Zip_Code,"NNNNN")))                                                                           //Natural: IF #WS-ZIP-CODE = MASK ( NNNNN )
            {
                pnd_Ws_Total.compute(new ComputeParameters(false, pnd_Ws_Total), pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_1.add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_2).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_3).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_4).add(pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_5)); //Natural: ADD #WS-POS-1 #WS-POS-2 #WS-POS-3 #WS-POS-4 #WS-POS-5 GIVING #WS-TOTAL
                pnd_Ws_Check_Digit.compute(new ComputeParameters(false, pnd_Ws_Check_Digit), DbsField.subtract(10,pnd_Ws_Total_Pnd_Ws_Total_Pos_2));                      //Natural: SUBTRACT #WS-TOTAL-POS-2 FROM 10 GIVING #WS-CHECK-DIGIT
                //*    COMPRESS '18*B'
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "18B", pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_1, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_2,    //Natural: COMPRESS '18B' #WS-POS-1 #WS-POS-2 #WS-POS-3 #WS-POS-4 #WS-POS-5 #WS-CD-POS-2 'E' INTO #WS-REC-1 LEAVING NO SPACE
                    pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_3, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_4, pnd_Ws_Postnet_Barcode_Pnd_Ws_Pos_5, pnd_Ws_Check_Digit_Pnd_Ws_Cd_Pos_2, 
                    "E"));
                //*    EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                //* *ELSE /* RL FRI FEB 10
                //* *  COMPRESS '18'
                //* *    INTO #WS-REC-1 LEAVING NO SPACE
                //* *  WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************
        //*  RL ANY FONT CHANGES FOR POS-PAY PAYEE MATCH FOR ADDRESS CODE BELOW?
        pnd_Ws_Address_Lines_Printed.reset();                                                                                                                             //Natural: RESET #WS-ADDRESS-LINES-PRINTED
        if (condition(DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(pnd_Ws_Index),"'CR '")))                                               //Natural: IF PYMNT-NME ( #WS-INDEX ) = MASK ( 'CR ' )
        {
            //* *COMPRESS '+7***' PYMNT-ADDR-LINE1-TXT (#WS-INDEX) /* RL PAYEE MATCH
            //*  RL PAYEE MATCH
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1!***", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(pnd_Ws_Index))); //Natural: COMPRESS '1!***' PYMNT-ADDR-LINE1-TXT ( #WS-INDEX ) INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            //* *EXAMINE #WS-REC-1 '+' REPLACE WITH '0'   /* RL PAYEE MATCH
            //* *EXAMINE #WS-REC-1 '+' REPLACE WITH ' ' /* RL PAYEE MATCH
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *COMPRESS '+7***' PYMNT-NME (#WS-INDEX) /* RL PAYEE MATCH
            //*  RL PAYEE MATCH
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1!***", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(pnd_Ws_Index)));      //Natural: COMPRESS '1!***' PYMNT-NME ( #WS-INDEX ) INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            //* *EXAMINE #WS-REC-1 '+' REPLACE WITH '0' /* RL PAYEE MATCH
            //* *EXAMINE #WS-REC-1 '+' REPLACE WITH ' ' /* RL PAYEE MATCH
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
            //* *ASSIGN #WS-ADDRESS = PYMNT-ADDR-LINE1-TXT (#WS-INDEX) /* RL
            //* RL
            pnd_Ws_Address.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(pnd_Ws_Index), MoveOption.LeftJustified);                         //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE1-TXT ( #WS-INDEX ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
            sub_Format_Address();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        }                                                                                                                                                                 //Natural: END-IF
        //* *ASSIGN #WS-ADDRESS = PYMNT-ADDR-LINE2-TXT (#WS-INDEX) /* RL
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(pnd_Ws_Index), MoveOption.LeftJustified);                             //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE2-TXT ( #WS-INDEX ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        //* *ASSIGN #WS-ADDRESS = PYMNT-ADDR-LINE3-TXT (#WS-INDEX) /* RL
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(pnd_Ws_Index), MoveOption.LeftJustified);                             //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE3-TXT ( #WS-INDEX ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        //* *ASSIGN #WS-ADDRESS = PYMNT-ADDR-LINE4-TXT (#WS-INDEX) /* RL
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(pnd_Ws_Index), MoveOption.LeftJustified);                             //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE4-TXT ( #WS-INDEX ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        //* *ASSIGN #WS-ADDRESS = PYMNT-ADDR-LINE5-TXT (#WS-INDEX) /* RL
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(pnd_Ws_Index), MoveOption.LeftJustified);                             //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE5-TXT ( #WS-INDEX ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        if (condition(pnd_Ws_Address_Lines_Printed.equals(6)))                                                                                                            //Natural: IF #WS-ADDRESS-LINES-PRINTED = 6
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *ASSIGN #WS-ADDRESS = PYMNT-ADDR-LINE6-TXT (#WS-INDEX) /* RL
            //* RL
            pnd_Ws_Address.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt().getValue(pnd_Ws_Index), MoveOption.LeftJustified);                         //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE6-TXT ( #WS-INDEX ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
            sub_Format_Address();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ADDRESS
        //* *******************************
    }
    private void sub_Format_Address() throws Exception                                                                                                                    //Natural: FORMAT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *COMPRESS '*7***' #WS-ADDRESS /* RL PAYEE MATCH
        //*  RL PAYEE MATCH
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*!***", pnd_Ws_Address));                                                                  //Natural: COMPRESS '*!***' #WS-ADDRESS INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
