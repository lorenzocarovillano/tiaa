/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:55 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn874t
************************************************************
**        * FILE NAME            : Fcpn874t.java
**        * CLASS NAME           : Fcpn874t
**        * INSTANCE NAME        : Fcpn874t
************************************************************
************************************************************************
* SUBPROGRAM : FCPN874T
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "NZ" ANNUITANT STATEMENTS - CONTRACT HEADERS
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn874t extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa801c pdaFcpa801c;
    private PdaFcpa873 pdaFcpa873;
    private PdaFcpa803 pdaFcpa803;
    private LdaFcpl874t ldaFcpl874t;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl874t = new LdaFcpl874t();
        registerRecord(ldaFcpl874t);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa801c = new PdaFcpa801c(parameters);
        pdaFcpa873 = new PdaFcpa873(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl874t.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn874t() throws Exception
    {
        super("Fcpn874t");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(2));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 2 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Pymnt_Hdr());                                                          //Natural: ASSIGN #OUTPUT-REC-DETAIL := #PYMNT-HDR
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(2));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 2 )
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Pymnt_Hdr_1());                                                        //Natural: ASSIGN #OUTPUT-REC-DETAIL := #PYMNT-HDR-1
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(2));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 2 )
        setValueToSubstring(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Pymnt_Hdr(),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),10,7);                                          //Natural: MOVE #PYMNT-HDR TO SUBSTR ( #OUTPUT-REC-DETAIL,10,7 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean()))                                                                                  //Natural: IF #DPI-DCI-IND
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                            //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(2));                                            //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 2 )
            setValueToSubstring(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Dpi_Dci_Hdr(),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),10,8);                                    //Natural: MOVE #DPI-DCI-HDR TO SUBSTR ( #OUTPUT-REC-DETAIL,10,8 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean()))                                                                                //Natural: IF #PYMNT-DED-IND
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                            //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(2));                                            //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 2 )
            setValueToSubstring(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Ded_Hdr(),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),5,10);                                        //Natural: MOVE #DED-HDR TO SUBSTR ( #OUTPUT-REC-DETAIL,5,10 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Net_Hdr().getValue(1));                                                //Natural: ASSIGN #OUTPUT-REC-DETAIL := #NET-HDR ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(2));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 2 )
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Net_Hdr().getValue(2));                                                //Natural: ASSIGN #OUTPUT-REC-DETAIL := #NET-HDR ( 2 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean())))                                                                              //Natural: IF NOT #DPI-DCI-IND
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 2
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(pnd_I));                                    //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( #I )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean())))                                                                            //Natural: IF NOT #PYMNT-DED-IND
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 2
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(pnd_I));                                    //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( #I )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(3));                                                //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 3 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
