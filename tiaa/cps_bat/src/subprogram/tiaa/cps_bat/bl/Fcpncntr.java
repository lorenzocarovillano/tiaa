/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:03 AM
**        * FROM NATURAL SUBPROGRAM : Fcpncntr
************************************************************
**        * FILE NAME            : Fcpncntr.java
**        * CLASS NAME           : Fcpncntr
**        * INSTANCE NAME        : Fcpncntr
************************************************************
************************************************************************
* PROGRAM  : FCPNCNTR
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* FUNCTION : FCP-CONS-CNTRL FILE LOOKUP.
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpncntr extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpacntr pdaFcpacntr;
    private LdaFcplcntr ldaFcplcntr;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Cntrl_S;
    private DbsField pnd_Cntrl_S_Cntl_Orgn_Cde;
    private DbsField pnd_Cntrl_S_Cntl_Filler;

    private DbsGroup pnd_Cntrl_S__R_Field_1;
    private DbsField pnd_Cntrl_S_Pnd_Cntrl_Superde_Start;
    private DbsField pnd_Cntrl_Superde_End;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplcntr = new LdaFcplcntr();
        registerRecord(ldaFcplcntr);
        registerRecord(ldaFcplcntr.getVw_fcp_Cons_Cntrl());

        // parameters
        parameters = new DbsRecord();
        pdaFcpacntr = new PdaFcpacntr(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Cntrl_S = localVariables.newGroupInRecord("pnd_Cntrl_S", "#CNTRL-S");
        pnd_Cntrl_S_Cntl_Orgn_Cde = pnd_Cntrl_S.newFieldInGroup("pnd_Cntrl_S_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 2);
        pnd_Cntrl_S_Cntl_Filler = pnd_Cntrl_S.newFieldInGroup("pnd_Cntrl_S_Cntl_Filler", "CNTL-FILLER", FieldType.STRING, 1);

        pnd_Cntrl_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrl_S__R_Field_1", "REDEFINE", pnd_Cntrl_S);
        pnd_Cntrl_S_Pnd_Cntrl_Superde_Start = pnd_Cntrl_S__R_Field_1.newFieldInGroup("pnd_Cntrl_S_Pnd_Cntrl_Superde_Start", "#CNTRL-SUPERDE-START", FieldType.STRING, 
            3);
        pnd_Cntrl_Superde_End = localVariables.newFieldInRecord("pnd_Cntrl_Superde_End", "#CNTRL-SUPERDE-END", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplcntr.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpncntr() throws Exception
    {
        super("Fcpncntr");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Cntrl_S.setValuesByName(pdaFcpacntr.getCntrl());                                                                                                              //Natural: MOVE BY NAME CNTRL TO #CNTRL-S
        pnd_Cntrl_S_Cntl_Filler.setValue("H'FF'");                                                                                                                        //Natural: MOVE H'FF' TO CNTL-FILLER
        pnd_Cntrl_Superde_End.setValue(pnd_Cntrl_S_Pnd_Cntrl_Superde_Start);                                                                                              //Natural: MOVE #CNTRL-SUPERDE-START TO #CNTRL-SUPERDE-END
        pnd_Cntrl_S_Cntl_Filler.setValue("H'00'");                                                                                                                        //Natural: MOVE H'00' TO CNTL-FILLER
        ldaFcplcntr.getVw_fcp_Cons_Cntrl().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-CNTRL BY CNTL-ORG-CDE-INVRSE-DTE = #CNTRL-SUPERDE-START THRU #CNTRL-SUPERDE-END
        (
        "RCNTRL",
        new Wc[] { new Wc("CNTL_ORG_CDE_INVRSE_DTE", ">=", pnd_Cntrl_S_Pnd_Cntrl_Superde_Start, "And", WcType.BY) ,
        new Wc("CNTL_ORG_CDE_INVRSE_DTE", "<=", pnd_Cntrl_Superde_End, WcType.BY) },
        new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
        1
        );
        RCNTRL:
        while (condition(ldaFcplcntr.getVw_fcp_Cons_Cntrl().readNextRow("RCNTRL")))
        {
            pdaFcpacntr.getCntrl_Work_Fields_Pnd_Cntrl_Isn().setValue(ldaFcplcntr.getVw_fcp_Cons_Cntrl().getAstISN("RCNTRL"));                                            //Natural: MOVE *ISN TO #CNTRL-ISN
            pdaFcpacntr.getCntrl().setValuesByName(ldaFcplcntr.getVw_fcp_Cons_Cntrl());                                                                                   //Natural: MOVE BY NAME FCP-CONS-CNTRL TO CNTRL
            pdaFcpacntr.getCntrl_C_Cntl_Amt_Pe_Grp().setValue(ldaFcplcntr.getFcp_Cons_Cntrl_Count_Castcntl_Amt_Pe_Grp());                                                 //Natural: MOVE C*CNTL-AMT-PE-GRP TO C-CNTL-AMT-PE-GRP
            pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Return_Cde().reset();                                                                                                  //Natural: RESET CNTRL-RETURN-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcplcntr.getVw_fcp_Cons_Cntrl().getAstCOUNTER().equals(getZero())))                                                                              //Natural: IF *COUNTER ( RCNTRL. ) = 0
        {
            pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Return_Cde().setValue(90);                                                                                             //Natural: MOVE 90 TO CNTRL-RETURN-CDE
            if (condition(pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Abend_Ind().getBoolean()))                                                                               //Natural: IF CNTRL-ABEND-IND
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"CONTROL RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN      :",pnd_Cntrl_S_Cntl_Orgn_Cde,new  //Natural: WRITE '***' 25T 'CONTROL RECORD IS NOT FOUND' 77T '***' / '***' 25T 'ORIGIN      :' #CNTRL-S.CNTL-ORGN-CDE 77T '***'
                    TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Return_Cde());  if (true) return;                                                                //Natural: TERMINATE CNTRL-RETURN-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
