/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:03:29 AM
**        * FROM NATURAL SUBPROGRAM : Efsn1205
************************************************************
**        * FILE NAME            : Efsn1205.java
**        * CLASS NAME           : Efsn1205
**        * INSTANCE NAME        : Efsn1205
************************************************************
************************************************************************
* PROGRAM  : EFSN1205
* SYSTEM   : CWFSCR
* TITLE    : SLO READ ACCESS MODULE FOR CMS DATA CAPTURE
* WRITTEN  : 07/01/00
* BY       : IRENE LOH
* FUNCTION : READ SLO AFTER BEING CALLED BY EFSN1200
*          | - RETURNS UNDUPLICATED DATA IN #PDQA652.
*          | - FOR GENERIC WARRANTS!!!
*
* MODIFIED  BY           DESCRIPTION OF CHANGES
* ========  ==   ===================================================
*
* 08/08/00  JS   CHANGED THE TAX INFORMATION DUE TO CHANGES IN
*                   EFSL3117 AND EFSA3104.  RECORD 5 AND 14 HAVE
*                   CHANGED.  ROUTINE 'READ-TAX' HAS CHANGED.
* 08/17/00  JS   ADDED THE TAX-ID-NUMBER (N9) TO  RECORD 14.
*
* 08/21/00  JS   NEW VERSION OF EFSL3117.
* 08/22/00  JS   MOVED '#PH-SOCIAL-SEC-NO := #SCR-SSN' FROM
*                   TAX-INFO ROUTINE TO REQUESTOR ROUTINE.
*                CHANGED '#TAX-ID-NUMBER := #SCR-SSN' IN TAX ROUTINE
*                   TO '#TAX-ID-NUMBER := #SCR-TAX-ID-NUMBER'.
*                ADDED '#TAX-NRA-PREFIX := #SCR-NRA-PFX' TO TAX
*                   ROUTINE.
*                ADDED '05 #TAX-NRA-PREFIX  (A1)' TO RECORD 14
* 09/15/00  JS   CHANGED THE DEFINITION OF '#PAYEE1-IVC-IND' FROM
*                   THE 'READ-PAYEE' ROUTINE TO 'READ-GROSS-TO-NET
*                   ROUTINE.
*
* 09/04/01  JS   CHANGED THE SUBSCRIPT FOR #CHECK2-SEQ-NBR AND
*                   #TOT2-GROSS-PRCDS IN READ-GROSS-TO-NET
*                   FROM #FC-P1 TO #FC-P2.
*
* 12/17/01  JS   IN PDQA650, RECORD5:
*                   REPLACED #SS-1078 (A1) AND #SS-1001 (A1)  WITH
*                            #TAX-PAYEE-CDE (A2).
*                   REPLACED #SS-1078/1001-DATE WITH #TAX-DTE-W8-W9.
*                IN PDQA650, RECORD14:
*                   ADDED   #TAX-CNTRCT-NO.
*                   REMOVED #TAX-NRA-PREFIX
*                   RECORD14 NEW SIZE = 128 BYTES (10946 - 11071)
*                NEW DISPLACEMENT IN RECORD5:
*                   #TAX-PAYEE-CDE -  58 FOR 2 BYTES
*                   #TAX-DTE-W8-W9 -  60 FOR 8 BYTES
*                NEW DISPLACEMENT IN RECORD14:
*                   #TAX-CNTRCT-NO - 121 FOR 8 BYTES
*                IN SUBROUTINE READ-TAX:
*                   REMOVED ALL REFERENCES TO #SS-1078, #SS-1001
*                       #SS-1078/1001-DATE AND #TAX-NRA-PREFIX.
*                   ADDED CODE FOR #TAX-PAYEE-CDE, #TAX-DTE-W8-W9,
*                       AND #TAX-CNTRCT-NO.
*                NEW VERSION OF EFSL3117.
*
* 03/07/02  JS   REPLACED APPC PARM AND ALL REDEFINES WITH EFSA1205
*                ADDED NEW FIELDS FOR OIA
*
* 04/12/02  JS   REPLACED SUBSCRIPTS #FC-P1 AND #FC-P2 WITH #I
*                REMOVED #CHECK1-CNT AND #CHECK1-CNT FROM
*                   EFSA1205 (PDA) AND PROGRAM
*
* 07/23/02  JS   ADDED END-OF-RECORD FIELD TO PDA
*
* 04/22/08  F.ALLIE ADDED FUNCTIONALITY FOR ROTH 403(B) RECRD ID. 11
*
* 10/23/09  R.MA  ROTH GW FOR SURVIVORS AND SIP PROJECT - SIS0904001
*                 NO CHANGES, RESTOWED DUE TO UPDATED EFSL3117.
* 12/2010  RSALGADO POPULATE NEW FIELD #PH-RESIDENCE (#SCR-PH-RESIDENCE)
* 01/2012  RSALGADO POPULATE NEW SUNYCUNY PLAN INFORMATION
* 04/2015  FENDAYA  COR/NAS SUNSET.                         FE201504
* 06/2017  FENDAYA  PIN EXPANSION.                          FE201706
*********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Efsn1205 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaEfsa1205 pdaEfsa1205;
    private PdaSpoolpda pdaSpoolpda;
    private LdaEfsl3117 ldaEfsl3117;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_J;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_1;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Nmbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Return_Parm1_Init;
    private DbsField pnd_Retry_Ctr;
    private DbsField pnd_For_Ctr;
    private DbsField pnd_Slo_Read_Key;

    private DbsGroup pnd_Slo_Read_Key__R_Field_2;
    private DbsField pnd_Slo_Read_Key_Pnd_Read_User;
    private DbsField pnd_Slo_Read_Key_Pnd_Read_Pif_Isn;
    private DbsField pnd_Dup_Slo;
    private DbsField pnd_Max_Slos;
    private DbsField pnd_I;
    private DbsField pnd_Ii;
    private DbsField pnd_Warrant;
    private DbsField pnd_Counter;
    private DbsField pnd_Kdo_Worksheet_Key;

    private DbsGroup pnd_Kdo_Worksheet_Key__R_Field_3;
    private DbsField pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mit_Work_Rqst;
    private DbsField pnd_Kdo_Worksheet_Key_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mult_Rec_Id;
    private DbsField pnd_Kdo_Cs_Key;

    private DbsGroup pnd_Kdo_Cs_Key__R_Field_4;
    private DbsField pnd_Kdo_Cs_Key_Pnd_Kdo_Mit_Work_Rqst;
    private DbsField pnd_Kdo_Cs_Key_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Kdo_Cs_Key_Pnd_Tiaa_Cntrct_No;
    private DbsField pnd_Kdo_Ledger_Key;

    private DbsGroup pnd_Kdo_Ledger_Key__R_Field_5;
    private DbsField pnd_Kdo_Ledger_Key_Pnd_Kdo_Mit_Work_Rqst;
    private DbsField pnd_Kdo_Ledger_Key_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Kdo_Ledger_Key_Pnd_Ldgr_Payee_Nbr;
    private DbsField pnd_Kdo_Ledger_Key_Pnd_Ldgr_Seq_Nbr;
    private DbsField pnd_Kdo_Fund_Key;

    private DbsGroup pnd_Kdo_Fund_Key__R_Field_6;
    private DbsField pnd_Kdo_Fund_Key_Pnd_Kdo_Mit_Work_Rqst;
    private DbsField pnd_Kdo_Fund_Key_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Kdo_Fund_Key_Pnd_Fund_Seq_Nbr;
    private DbsField pnd_Kdo_Payee_Key;

    private DbsGroup pnd_Kdo_Payee_Key__R_Field_7;
    private DbsField pnd_Kdo_Payee_Key_Pnd_Kdo_Mit_Work_Rqst;
    private DbsField pnd_Kdo_Payee_Key_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Kdo_Payee_Key_Pnd_Payee_Nbr;
    private DbsField pnd_Kdo_Check_Key;

    private DbsGroup pnd_Kdo_Check_Key__R_Field_8;
    private DbsField pnd_Kdo_Check_Key_Pnd_Kdo_Mit_Work_Rqst;
    private DbsField pnd_Kdo_Check_Key_Pnd_Doc_Entry_Dte_Tme;
    private DbsField pnd_Kdo_Check_Key_Pnd_Check_Payee_Nbr;
    private DbsField pnd_Kdo_Check_Key_Pnd_Check_Seq_Nbr;
    private DbsField pnd_Kdo_Check_Key_Pnd_F_Check_Fund_Nbr;
    private DbsField pnd_Tot_Gross_Accum1;
    private DbsField pnd_Tot_Net_Accum1;
    private DbsField pnd_Tot_Fed_Accum1;
    private DbsField pnd_Tot_Sta_Accum1;
    private DbsField pnd_Tot_Gross_Accum2;
    private DbsField pnd_Tot_Net_Accum2;
    private DbsField pnd_Tot_Fed_Accum2;
    private DbsField pnd_Tot_Sta_Accum2;
    private DbsField pnd_Tiaa_Ath_Accum1;
    private DbsField pnd_Tiaa_Ath_Accum2;
    private DbsField pnd_Cref_Ath_Accum1;
    private DbsField pnd_Cref_Ath_Accum2;
    private DbsField pnd_Date_Conv;

    private DbsGroup pnd_Date_Conv__R_Field_9;
    private DbsField pnd_Date_Conv_Pnd_Mm;
    private DbsField pnd_Date_Conv_Pnd_H1;
    private DbsField pnd_Date_Conv_Pnd_Dd;
    private DbsField pnd_Date_Conv_Pnd_H2;
    private DbsField pnd_Date_Conv_Pnd_Yy;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaSpoolpda = new PdaSpoolpda(localVariables);
        ldaEfsl3117 = new LdaEfsl3117();
        registerRecord(ldaEfsl3117);
        registerRecord(ldaEfsl3117.getVw_cwf_Efm_Wks_Tran_View());
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaEfsa1205 = new PdaEfsa1205(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Nmbr = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Nmbr", "#CNTRCT-NMBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_1.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Return_Parm1_Init = localVariables.newFieldInRecord("pnd_Return_Parm1_Init", "#RETURN-PARM1-INIT", FieldType.STRING, 186);
        pnd_Retry_Ctr = localVariables.newFieldInRecord("pnd_Retry_Ctr", "#RETRY-CTR", FieldType.NUMERIC, 3);
        pnd_For_Ctr = localVariables.newFieldInRecord("pnd_For_Ctr", "#FOR-CTR", FieldType.NUMERIC, 5);
        pnd_Slo_Read_Key = localVariables.newFieldInRecord("pnd_Slo_Read_Key", "#SLO-READ-KEY", FieldType.STRING, 18);

        pnd_Slo_Read_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Slo_Read_Key__R_Field_2", "REDEFINE", pnd_Slo_Read_Key);
        pnd_Slo_Read_Key_Pnd_Read_User = pnd_Slo_Read_Key__R_Field_2.newFieldInGroup("pnd_Slo_Read_Key_Pnd_Read_User", "#READ-USER", FieldType.STRING, 
            8);
        pnd_Slo_Read_Key_Pnd_Read_Pif_Isn = pnd_Slo_Read_Key__R_Field_2.newFieldInGroup("pnd_Slo_Read_Key_Pnd_Read_Pif_Isn", "#READ-PIF-ISN", FieldType.NUMERIC, 
            10);
        pnd_Dup_Slo = localVariables.newFieldInRecord("pnd_Dup_Slo", "#DUP-SLO", FieldType.BOOLEAN, 1);
        pnd_Max_Slos = localVariables.newFieldInRecord("pnd_Max_Slos", "#MAX-SLOS", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Ii = localVariables.newFieldInRecord("pnd_Ii", "#II", FieldType.NUMERIC, 2);
        pnd_Warrant = localVariables.newFieldInRecord("pnd_Warrant", "#WARRANT", FieldType.NUMERIC, 2);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.NUMERIC, 2);
        pnd_Kdo_Worksheet_Key = localVariables.newFieldInRecord("pnd_Kdo_Worksheet_Key", "#KDO-WORKSHEET-KEY", FieldType.STRING, 32);

        pnd_Kdo_Worksheet_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Kdo_Worksheet_Key__R_Field_3", "REDEFINE", pnd_Kdo_Worksheet_Key);
        pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mit_Work_Rqst = pnd_Kdo_Worksheet_Key__R_Field_3.newFieldInGroup("pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mit_Work_Rqst", 
            "#KDO-MIT-WORK-RQST", FieldType.STRING, 15);
        pnd_Kdo_Worksheet_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Kdo_Worksheet_Key__R_Field_3.newFieldInGroup("pnd_Kdo_Worksheet_Key_Pnd_Doc_Entry_Dte_Tme", 
            "#DOC-ENTRY-DTE-TME", FieldType.STRING, 15);
        pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mult_Rec_Id = pnd_Kdo_Worksheet_Key__R_Field_3.newFieldInGroup("pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mult_Rec_Id", "#KDO-MULT-REC-ID", 
            FieldType.NUMERIC, 2);
        pnd_Kdo_Cs_Key = localVariables.newFieldInRecord("pnd_Kdo_Cs_Key", "#KDO-CS-KEY", FieldType.STRING, 40);

        pnd_Kdo_Cs_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Kdo_Cs_Key__R_Field_4", "REDEFINE", pnd_Kdo_Cs_Key);
        pnd_Kdo_Cs_Key_Pnd_Kdo_Mit_Work_Rqst = pnd_Kdo_Cs_Key__R_Field_4.newFieldInGroup("pnd_Kdo_Cs_Key_Pnd_Kdo_Mit_Work_Rqst", "#KDO-MIT-WORK-RQST", 
            FieldType.STRING, 15);
        pnd_Kdo_Cs_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Kdo_Cs_Key__R_Field_4.newFieldInGroup("pnd_Kdo_Cs_Key_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Kdo_Cs_Key_Pnd_Tiaa_Cntrct_No = pnd_Kdo_Cs_Key__R_Field_4.newFieldInGroup("pnd_Kdo_Cs_Key_Pnd_Tiaa_Cntrct_No", "#TIAA-CNTRCT-NO", FieldType.STRING, 
            10);
        pnd_Kdo_Ledger_Key = localVariables.newFieldInRecord("pnd_Kdo_Ledger_Key", "#KDO-LEDGER-KEY", FieldType.STRING, 33);

        pnd_Kdo_Ledger_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Kdo_Ledger_Key__R_Field_5", "REDEFINE", pnd_Kdo_Ledger_Key);
        pnd_Kdo_Ledger_Key_Pnd_Kdo_Mit_Work_Rqst = pnd_Kdo_Ledger_Key__R_Field_5.newFieldInGroup("pnd_Kdo_Ledger_Key_Pnd_Kdo_Mit_Work_Rqst", "#KDO-MIT-WORK-RQST", 
            FieldType.STRING, 15);
        pnd_Kdo_Ledger_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Kdo_Ledger_Key__R_Field_5.newFieldInGroup("pnd_Kdo_Ledger_Key_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Kdo_Ledger_Key_Pnd_Ldgr_Payee_Nbr = pnd_Kdo_Ledger_Key__R_Field_5.newFieldInGroup("pnd_Kdo_Ledger_Key_Pnd_Ldgr_Payee_Nbr", "#LDGR-PAYEE-NBR", 
            FieldType.NUMERIC, 1);
        pnd_Kdo_Ledger_Key_Pnd_Ldgr_Seq_Nbr = pnd_Kdo_Ledger_Key__R_Field_5.newFieldInGroup("pnd_Kdo_Ledger_Key_Pnd_Ldgr_Seq_Nbr", "#LDGR-SEQ-NBR", FieldType.NUMERIC, 
            2);
        pnd_Kdo_Fund_Key = localVariables.newFieldInRecord("pnd_Kdo_Fund_Key", "#KDO-FUND-KEY", FieldType.STRING, 32);

        pnd_Kdo_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Kdo_Fund_Key__R_Field_6", "REDEFINE", pnd_Kdo_Fund_Key);
        pnd_Kdo_Fund_Key_Pnd_Kdo_Mit_Work_Rqst = pnd_Kdo_Fund_Key__R_Field_6.newFieldInGroup("pnd_Kdo_Fund_Key_Pnd_Kdo_Mit_Work_Rqst", "#KDO-MIT-WORK-RQST", 
            FieldType.STRING, 15);
        pnd_Kdo_Fund_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Kdo_Fund_Key__R_Field_6.newFieldInGroup("pnd_Kdo_Fund_Key_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Kdo_Fund_Key_Pnd_Fund_Seq_Nbr = pnd_Kdo_Fund_Key__R_Field_6.newFieldInGroup("pnd_Kdo_Fund_Key_Pnd_Fund_Seq_Nbr", "#FUND-SEQ-NBR", FieldType.NUMERIC, 
            2);
        pnd_Kdo_Payee_Key = localVariables.newFieldInRecord("pnd_Kdo_Payee_Key", "#KDO-PAYEE-KEY", FieldType.STRING, 31);

        pnd_Kdo_Payee_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Kdo_Payee_Key__R_Field_7", "REDEFINE", pnd_Kdo_Payee_Key);
        pnd_Kdo_Payee_Key_Pnd_Kdo_Mit_Work_Rqst = pnd_Kdo_Payee_Key__R_Field_7.newFieldInGroup("pnd_Kdo_Payee_Key_Pnd_Kdo_Mit_Work_Rqst", "#KDO-MIT-WORK-RQST", 
            FieldType.STRING, 15);
        pnd_Kdo_Payee_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Kdo_Payee_Key__R_Field_7.newFieldInGroup("pnd_Kdo_Payee_Key_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Kdo_Payee_Key_Pnd_Payee_Nbr = pnd_Kdo_Payee_Key__R_Field_7.newFieldInGroup("pnd_Kdo_Payee_Key_Pnd_Payee_Nbr", "#PAYEE-NBR", FieldType.NUMERIC, 
            1);
        pnd_Kdo_Check_Key = localVariables.newFieldInRecord("pnd_Kdo_Check_Key", "#KDO-CHECK-KEY", FieldType.STRING, 35);

        pnd_Kdo_Check_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Kdo_Check_Key__R_Field_8", "REDEFINE", pnd_Kdo_Check_Key);
        pnd_Kdo_Check_Key_Pnd_Kdo_Mit_Work_Rqst = pnd_Kdo_Check_Key__R_Field_8.newFieldInGroup("pnd_Kdo_Check_Key_Pnd_Kdo_Mit_Work_Rqst", "#KDO-MIT-WORK-RQST", 
            FieldType.STRING, 15);
        pnd_Kdo_Check_Key_Pnd_Doc_Entry_Dte_Tme = pnd_Kdo_Check_Key__R_Field_8.newFieldInGroup("pnd_Kdo_Check_Key_Pnd_Doc_Entry_Dte_Tme", "#DOC-ENTRY-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Kdo_Check_Key_Pnd_Check_Payee_Nbr = pnd_Kdo_Check_Key__R_Field_8.newFieldInGroup("pnd_Kdo_Check_Key_Pnd_Check_Payee_Nbr", "#CHECK-PAYEE-NBR", 
            FieldType.NUMERIC, 1);
        pnd_Kdo_Check_Key_Pnd_Check_Seq_Nbr = pnd_Kdo_Check_Key__R_Field_8.newFieldInGroup("pnd_Kdo_Check_Key_Pnd_Check_Seq_Nbr", "#CHECK-SEQ-NBR", FieldType.NUMERIC, 
            2);
        pnd_Kdo_Check_Key_Pnd_F_Check_Fund_Nbr = pnd_Kdo_Check_Key__R_Field_8.newFieldInGroup("pnd_Kdo_Check_Key_Pnd_F_Check_Fund_Nbr", "#F-CHECK-FUND-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Tot_Gross_Accum1 = localVariables.newFieldInRecord("pnd_Tot_Gross_Accum1", "#TOT-GROSS-ACCUM1", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Net_Accum1 = localVariables.newFieldInRecord("pnd_Tot_Net_Accum1", "#TOT-NET-ACCUM1", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Fed_Accum1 = localVariables.newFieldInRecord("pnd_Tot_Fed_Accum1", "#TOT-FED-ACCUM1", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Sta_Accum1 = localVariables.newFieldInRecord("pnd_Tot_Sta_Accum1", "#TOT-STA-ACCUM1", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Gross_Accum2 = localVariables.newFieldInRecord("pnd_Tot_Gross_Accum2", "#TOT-GROSS-ACCUM2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Net_Accum2 = localVariables.newFieldInRecord("pnd_Tot_Net_Accum2", "#TOT-NET-ACCUM2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Fed_Accum2 = localVariables.newFieldInRecord("pnd_Tot_Fed_Accum2", "#TOT-FED-ACCUM2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Sta_Accum2 = localVariables.newFieldInRecord("pnd_Tot_Sta_Accum2", "#TOT-STA-ACCUM2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tiaa_Ath_Accum1 = localVariables.newFieldInRecord("pnd_Tiaa_Ath_Accum1", "#TIAA-ATH-ACCUM1", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tiaa_Ath_Accum2 = localVariables.newFieldInRecord("pnd_Tiaa_Ath_Accum2", "#TIAA-ATH-ACCUM2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cref_Ath_Accum1 = localVariables.newFieldInRecord("pnd_Cref_Ath_Accum1", "#CREF-ATH-ACCUM1", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cref_Ath_Accum2 = localVariables.newFieldInRecord("pnd_Cref_Ath_Accum2", "#CREF-ATH-ACCUM2", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Date_Conv = localVariables.newFieldInRecord("pnd_Date_Conv", "#DATE-CONV", FieldType.STRING, 10);

        pnd_Date_Conv__R_Field_9 = localVariables.newGroupInRecord("pnd_Date_Conv__R_Field_9", "REDEFINE", pnd_Date_Conv);
        pnd_Date_Conv_Pnd_Mm = pnd_Date_Conv__R_Field_9.newFieldInGroup("pnd_Date_Conv_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Date_Conv_Pnd_H1 = pnd_Date_Conv__R_Field_9.newFieldInGroup("pnd_Date_Conv_Pnd_H1", "#H1", FieldType.STRING, 1);
        pnd_Date_Conv_Pnd_Dd = pnd_Date_Conv__R_Field_9.newFieldInGroup("pnd_Date_Conv_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Date_Conv_Pnd_H2 = pnd_Date_Conv__R_Field_9.newFieldInGroup("pnd_Date_Conv_Pnd_H2", "#H2", FieldType.STRING, 1);
        pnd_Date_Conv_Pnd_Yy = pnd_Date_Conv__R_Field_9.newFieldInGroup("pnd_Date_Conv_Pnd_Yy", "#YY", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaEfsl3117.initializeValues();

        localVariables.reset();
        pnd_Return_Parm1_Init.setInitialValue("0D        EFCMSCPT        Successful");
        pnd_Max_Slos.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Efsn1205() throws Exception
    {
        super("Efsn1205");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("EFSN1205", onError);
        setupReports();
        //*  ------------------------------------------------------------------
        //*  --------------------------------- /* INITIALIZE RETURN FIELDS                                                                                                //Natural: FORMAT LS = 132;//Natural: ON ERROR
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Return_Parm1().setValue("0D        EFSCRCAP        Successful");                                                                 //Natural: ASSIGN #RETURN-PARM1 = '0D        EFSCRCAP        Successful'
        pdaEfsa1205.getPnd_Appc_Parm_Format_Inout().setValue("CWFRETRN");                                                                                                 //Natural: ASSIGN FORMAT-INOUT = 'CWFRETRN'
        pdaEfsa1205.getPnd_Appc_Parm_Role_Msg().setValue("S");                                                                                                            //Natural: ASSIGN ROLE-MSG = 'S'
        pdaEfsa1205.getPnd_Appc_Parm_Program().setValue(Global.getPROGRAM());                                                                                             //Natural: ASSIGN PROGRAM = *PROGRAM
        //*  --------------------------------- /* MAIN PROCESS
        pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mit_Work_Rqst.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme());                                                        //Natural: MOVE #RQST-LOG-DTE-TME TO #KDO-WORKSHEET-KEY.#KDO-MIT-WORK-RQST
        pnd_Kdo_Worksheet_Key_Pnd_Doc_Entry_Dte_Tme.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme());                                                       //Natural: MOVE #MSG-ENTRY-DTE-TME TO #KDO-WORKSHEET-KEY.#DOC-ENTRY-DTE-TME
        pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mult_Rec_Id.setValue(0);                                                                                                            //Natural: MOVE 0 TO #KDO-WORKSHEET-KEY.#KDO-MULT-REC-ID
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Pdqa652().reset();                                                                                                               //Natural: RESET #PDQA652
        ldaEfsl3117.getVw_cwf_Efm_Wks_Tran_View().startDatabaseRead                                                                                                       //Natural: READ CWF-EFM-WKS-TRAN-VIEW BY KDO-WORKSHEET-KEY STARTING FROM #KDO-WORKSHEET-KEY
        (
        "READ01",
        new Wc[] { new Wc("KDO_WORKSHEET_KEY", ">=", pnd_Kdo_Worksheet_Key, WcType.BY) },
        new Oc[] { new Oc("KDO_WORKSHEET_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaEfsl3117.getVw_cwf_Efm_Wks_Tran_View().readNextRow("READ01")))
        {
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Mit_Work_Rqst().notEquals(pnd_Kdo_Worksheet_Key_Pnd_Kdo_Mit_Work_Rqst) || ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Doc_Entry_Dte_Tme().notEquals(pnd_Kdo_Worksheet_Key_Pnd_Doc_Entry_Dte_Tme))) //Natural: IF CWF-EFM-WKS-TRAN-VIEW.KDO-MIT-WORK-RQST NE #KDO-WORKSHEET-KEY.#KDO-MIT-WORK-RQST OR CWF-EFM-WKS-TRAN-VIEW.DOC-ENTRY-DTE-TME NE #KDO-WORKSHEET-KEY.#DOC-ENTRY-DTE-TME
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1221 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CWF-EFM-WKS-TRAN-VIEW.KDO-MULTI-RCRD-ID;//Natural: VALUE 99
            if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(99))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-GENERAL-INFO
                sub_Read_General_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 01
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(1))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-REQUESTOR
                sub_Read_Requestor();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 02
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(2))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-PAYEE
                sub_Read_Payee();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 03
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(3))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-TAX
                sub_Read_Tax();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    VALUE 04
                //*      PERFORM READ-REQUESTOR-INSTR
            }                                                                                                                                                             //Natural: VALUE 06
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(6))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-DIVORCE
                sub_Read_Divorce();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 07
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(7))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-PREMIUM
                sub_Read_Premium();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 08
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(8))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-LOAN-CHECK
                sub_Read_Loan_Check();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 09
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(9))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-SETTLEMENT
                sub_Read_Settlement();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(10))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-FUND-INFO
                sub_Read_Fund_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(11))))
            {
                decideConditionsMet1221++;
                if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Tran_Type().getSubstring(1,1).equals("W")))                                                    //Natural: IF SUBSTR ( #SCR-TRAN-TYPE,1,1 ) = 'W'
                {
                                                                                                                                                                          //Natural: PERFORM READ-ROTH-INFO
                    sub_Read_Roth_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaEfsa1205.getPnd_Appc_Parm_Pnd_Record19().reset();                                                                                                  //Natural: RESET #RECORD19
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 16
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(16))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-FINANCIAL-MAINTENANCE
                sub_Read_Financial_Maintenance();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 17
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(17))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-GROSS-TO-NET
                sub_Read_Gross_To_Net();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    VALUE 18
                //*      PERFORM READ-RATES
            }                                                                                                                                                             //Natural: VALUE 19
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(19))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-LEDGERS
                sub_Read_Ledgers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    VALUE 20
                //*      PERFORM READ-MDO-FINANCIAL
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Kdo_Multi_Rcrd_Id().equals(30))))
            {
                decideConditionsMet1221++;
                                                                                                                                                                          //Natural: PERFORM READ-APPROV-AUTH
                sub_Read_Approv_Auth();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Nmbr.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct());                                                                //Natural: MOVE #MSG-TIAA-CNTRCT TO #CNTRCT-NMBR
                                                                                                                                                                          //Natural: PERFORM READ-NA
        sub_Read_Na();
        if (condition(Global.isEscape())) {return;}
        //* **
        //*  FE201504
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaEfsa1205.getPnd_Appc_Parm_Length_Msg().compute(new ComputeParameters(false, pdaEfsa1205.getPnd_Appc_Parm_Length_Msg()), DbsField.add(324,29684));              //Natural: ASSIGN LENGTH-MSG = 324 + 29684
        //*  #COR-PIN
        //*  FE201504
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Pin());                                                                       //Natural: MOVE #PIN TO #MDMA101.#I-PIN-N12
        //*  ASSIGN #COR-RDCTYPE = 1
        //*  READ (1) COR-XREF-PH-VIEW BY COR-SUPER-PIN-RCDTYPE
        //*     STARTING FROM #COR-KEY
        //*   IF #PIN NE PH-UNIQUE-ID-NBR
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   MOVE PH-DOB-DTE TO #PH-DOB
        //*  END-READ
        //*  FE201504 START
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA101.#O-RETURN-CODE EQ '0000'
        {
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Dob().setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Birth());                                                          //Natural: MOVE #MDMA101.#O-DATE-OF-BIRTH TO #PH-DOB
            //*  FE201504 END
        }                                                                                                                                                                 //Natural: END-IF
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-GENERAL-INFO
        //* *******************************************
        //* ***************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-REQUESTOR
        //* ***************************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PAYEE
        //* ****************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-LOAN-CHECK
        //* ****************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-SETTLEMENT
        //* ***********************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-FUND-INFO
        //* ***********************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-FINANCIAL-MAINTENANCE
        //* **************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TAX
        //* ***************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DIVORCE
        //* ***************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PREMIUM
        //* ***************************************
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-LEDGERS
        //* ***********************************************
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-GROSS-TO-NET
        //* *******************************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ROTH-INFO
        //* *******************************
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-APPROV-AUTH
        //* **********************************************
        //* ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NA
        //*  #MDMA210.#I-TIAA-CREF-IND   := 'T'
        //* ***************************
        //*  JS 7/23/02
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_End_Of_Record().setValue("END-RECORD");                                                                                          //Natural: MOVE 'END-RECORD' TO #END-OF-RECORD
    }
    private void sub_Read_General_Info() throws Exception                                                                                                                 //Natural: READ-GENERAL-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Date_Conv.setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Effectve_Dte());                                                                              //Natural: ASSIGN #DATE-CONV := #SCR-EFFECTVE-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Effective_Date().compute(new ComputeParameters(false, pdaEfsa1205.getPnd_Appc_Parm_Pnd_Effective_Date()), (pnd_Date_Conv_Pnd_Yy.multiply(10000)).add((pnd_Date_Conv_Pnd_Mm.multiply(100))).add(pnd_Date_Conv_Pnd_Dd)); //Natural: COMPUTE #EFFECTIVE-DATE = ( #YY * 10000 ) + ( #MM * 100 ) + #DD
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Complete_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Complete());                                                //Natural: ASSIGN #COMPLETE-IND := #SCR-COMPLETE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Wpid().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Wpid());                                                            //Natural: ASSIGN #WPID := #SCR-WPID
    }
    //*  SUNYCUNY START
    //*  SUNYCUNY END
    private void sub_Read_Requestor() throws Exception                                                                                                                    //Natural: READ-REQUESTOR
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Pin().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ph_Pin());                                                           //Natural: ASSIGN #PIN := #SCR-PH-PIN
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tiaa_Cont().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cntrct());                                                     //Natural: ASSIGN #TIAA-CONT := #SCR-CNTRCT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cref_Cert().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cert());                                                       //Natural: ASSIGN #CREF-CERT := #SCR-CERT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Ssn().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ssn());                                                           //Natural: ASSIGN #PH-SSN := #SCR-SSN
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Name().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ph_Name());                                                      //Natural: ASSIGN #PH-NAME := #SCR-PH-NAME
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Dob_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Dob_Dte());                                                     //Natural: ASSIGN #PH-DOB-A := #SCR-DOB-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Dod_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Dod_Dte());                                                     //Natural: ASSIGN #PH-DOD-A := #SCR-DOD-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Type().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_Type());                                            //Natural: ASSIGN #PAYMENT-TYPE := #SCR-PAYMENT-TYPE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Desc().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_Descr());                                           //Natural: ASSIGN #PAYMENT-DESC := #SCR-PAYMENT-DESCR
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Tfslash_N().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_Tfslash_N());                                  //Natural: ASSIGN #PAYMENT-T/N := #SCR-PAYMENT-T/N
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Pfslash_N().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_Pfslash_N());                                  //Natural: ASSIGN #PAYMENT-P/N := #SCR-PAYMENT-P/N
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Lfslash_N().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_Lfslash_N());                                  //Natural: ASSIGN #PAYMENT-L/N := #SCR-PAYMENT-L/N
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_T_Code().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_T_Code());                                        //Natural: ASSIGN #PAYMENT-T-CODE := #SCR-PAYMENT-T-CODE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Nt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_Nt());                                                //Natural: ASSIGN #PAYMENT-NT := #SCR-PAYMENT-NT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Issue_Or_Settle_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Issue_Or_Settle_Dte());                           //Natural: ASSIGN #ISSUE-OR-SETTLE-DATE-A := #SCR-ISSUE-OR-SETTLE-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Oia_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Oia_Ind());                                                      //Natural: ASSIGN #OIA-IND := #SCR-OIA-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Sucu_Plan_Nbr().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Plan());                                                   //Natural: ASSIGN #SUCU-PLAN-NBR := #SCR-PLAN
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Sucu_Subplan_Nbr().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sub_Plan());                                            //Natural: ASSIGN #SUCU-SUBPLAN-NBR := #SCR-SUB-PLAN
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Sucu_Orig_Cntr_Nbr().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Orgtg_Cntrct());                                      //Natural: ASSIGN #SUCU-ORIG-CNTR-NBR := #SCR-ORGTG-CNTRCT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Sucu_Orig_Subplan_Nbr().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Orgtg_Sub_Plan());                                 //Natural: ASSIGN #SUCU-ORIG-SUBPLAN-NBR := #SCR-ORGTG-SUB-PLAN
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ppg_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ppg());                                                          //Natural: ASSIGN #PPG-IND := #SCR-PPG
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ppg_Cnt().setValue(0);                                                                                                           //Natural: ASSIGN #PPG-CNT := 0
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 15
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(15)); pnd_I.nadd(1))
        {
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ppg_Codes().getValue(pnd_I).notEquals("     ")))                                                   //Natural: IF #SCR-PPG-CODES ( #I ) NE '     '
            {
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ppgs().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ppg_Codes().getValue(pnd_I));               //Natural: MOVE #SCR-PPG-CODES ( #I ) TO #PPGS ( #I )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ppg_Cnt().nadd(1);                                                                                                       //Natural: ADD 1 TO #PPG-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Payee() throws Exception                                                                                                                        //Natural: READ-PAYEE
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            pnd_Ii.setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Nbr().getValue(pnd_I));                                                                    //Natural: ASSIGN #II := #SCR-PAYEE-NBR ( #I )
            if (condition(pnd_Ii.equals(1)))                                                                                                                              //Natural: IF #II = 1
            {
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Name1().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Name_1().getValue(pnd_Ii));                   //Natural: ASSIGN #PAYEE1-NAME1 := #SCR-PAYEE-NAME-1 ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Name2().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Name_2().getValue(pnd_Ii));                   //Natural: ASSIGN #PAYEE1-NAME2 := #SCR-PAYEE-NAME-2 ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Addr1().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,1));                   //Natural: ASSIGN #PAYEE1-ADDR1 := #SCR-PAYEE-ADDR ( #II, 1 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Addr2().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,2));                   //Natural: ASSIGN #PAYEE1-ADDR2 := #SCR-PAYEE-ADDR ( #II, 2 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Addr3().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,3));                   //Natural: ASSIGN #PAYEE1-ADDR3 := #SCR-PAYEE-ADDR ( #II, 3 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Addr4().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,4));                   //Natural: ASSIGN #PAYEE1-ADDR4 := #SCR-PAYEE-ADDR ( #II, 4 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Addr5().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,5));                   //Natural: ASSIGN #PAYEE1-ADDR5 := #SCR-PAYEE-ADDR ( #II, 5 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Addr6().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,6));                   //Natural: ASSIGN #PAYEE1-ADDR6 := #SCR-PAYEE-ADDR ( #II, 6 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_City().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_City().getValue(pnd_Ii));                      //Natural: ASSIGN #PAYEE1-CITY := #SCR-PAYEE-CITY ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_State().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_State().getValue(pnd_Ii));                    //Natural: ASSIGN #PAYEE1-STATE := #SCR-PAYEE-STATE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Zip().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Zip().getValue(pnd_Ii));                        //Natural: ASSIGN #PAYEE1-ZIP := #SCR-PAYEE-ZIP ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Aba_Num().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Aba_Nbr().getValue(pnd_Ii));                //Natural: ASSIGN #PAYEE1-ABA-NUM := #SCR-PAYEE-ABA-NBR ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Acct_Num().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Acct_Nbr().getValue(pnd_Ii));              //Natural: ASSIGN #PAYEE1-ACCT-NUM := #SCR-PAYEE-ACCT-NBR ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Acct_Type().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Acct_Type().getValue(pnd_Ii));            //Natural: ASSIGN #PAYEE1-ACCT-TYPE := #SCR-PAYEE-ACCT-TYPE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Rollover_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Rfslash_O_Ind().getValue(pnd_Ii));     //Natural: ASSIGN #PAYEE1-ROLLOVER-IND := #SCR-PAYEE-R/O-IND ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Int_Roll_Cont().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Rfslash_O_Tiaa_Cntrct().getValue(pnd_Ii));         //Natural: ASSIGN #INT-ROLL-CONT := #SCR-R/O-TIAA-CNTRCT ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Int_Roll_Cert().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Rfslash_O_Cref_Cert().getValue(pnd_Ii));           //Natural: ASSIGN #INT-ROLL-CERT := #SCR-R/O-CREF-CERT ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Hold_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Hold_Ind().getValue(pnd_Ii));              //Natural: ASSIGN #PAYEE1-HOLD-IND := #SCR-PAYEE-HOLD-IND ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Ovrpy_Rcvry_Ind().setValue("N");                                                                                  //Natural: ASSIGN #PAYEE1-OVRPY-RCVRY-IND := 'N'
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Type().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Type().getValue(pnd_Ii));                      //Natural: ASSIGN #PAYEE1-TYPE := #SCR-PAYEE-TYPE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Foreign_Code().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Frgn_Cde().getValue(pnd_Ii));          //Natural: ASSIGN #PAYEE1-FOREIGN-CODE := #SCR-PAYEE-FRGN-CDE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Method().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Method().getValue(pnd_Ii));                  //Natural: ASSIGN #PAYEE1-METHOD := #SCR-PAYEE-METHOD ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Hold_Code().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Hold_Cde().getValue(pnd_Ii));             //Natural: ASSIGN #PAYEE1-HOLD-CODE := #SCR-PAYEE-HOLD-CDE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Manu_Check().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Proline_Ind().getValue(pnd_Ii));               //Natural: ASSIGN #PAYEE1-MANU-CHECK := #SCR-PROLINE-IND ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Installment_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Inst_Date().getValue(pnd_Ii));   //Natural: ASSIGN #PAYEE1-INSTALLMENT-DATE-A := #SCR-PAYEE-INST-DATE ( #II )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Name1().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Name_1().getValue(pnd_Ii));                   //Natural: ASSIGN #PAYEE2-NAME1 := #SCR-PAYEE-NAME-1 ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Name2().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Name_2().getValue(pnd_Ii));                   //Natural: ASSIGN #PAYEE2-NAME2 := #SCR-PAYEE-NAME-2 ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Addr1().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,1));                   //Natural: ASSIGN #PAYEE2-ADDR1 := #SCR-PAYEE-ADDR ( #II, 1 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Addr2().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,2));                   //Natural: ASSIGN #PAYEE2-ADDR2 := #SCR-PAYEE-ADDR ( #II, 2 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Addr3().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,3));                   //Natural: ASSIGN #PAYEE2-ADDR3 := #SCR-PAYEE-ADDR ( #II, 3 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Addr4().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,4));                   //Natural: ASSIGN #PAYEE2-ADDR4 := #SCR-PAYEE-ADDR ( #II, 4 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Addr5().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,5));                   //Natural: ASSIGN #PAYEE2-ADDR5 := #SCR-PAYEE-ADDR ( #II, 5 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Addr6().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Addr().getValue(pnd_Ii,6));                   //Natural: ASSIGN #PAYEE2-ADDR6 := #SCR-PAYEE-ADDR ( #II, 6 )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_City().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_City().getValue(pnd_Ii));                      //Natural: ASSIGN #PAYEE2-CITY := #SCR-PAYEE-CITY ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_State().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_State().getValue(pnd_Ii));                    //Natural: ASSIGN #PAYEE2-STATE := #SCR-PAYEE-STATE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Zip().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Zip().getValue(pnd_Ii));                        //Natural: ASSIGN #PAYEE2-ZIP := #SCR-PAYEE-ZIP ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Aba_Num().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Aba_Nbr().getValue(pnd_Ii));                //Natural: ASSIGN #PAYEE2-ABA-NUM := #SCR-PAYEE-ABA-NBR ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Acct_Num().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Acct_Nbr().getValue(pnd_Ii));              //Natural: ASSIGN #PAYEE2-ACCT-NUM := #SCR-PAYEE-ACCT-NBR ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Acct_Type().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Acct_Type().getValue(pnd_Ii));            //Natural: ASSIGN #PAYEE2-ACCT-TYPE := #SCR-PAYEE-ACCT-TYPE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Rollover_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Rfslash_O_Ind().getValue(pnd_Ii));     //Natural: ASSIGN #PAYEE2-ROLLOVER-IND := #SCR-PAYEE-R/O-IND ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Roll_Cont().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Rfslash_O_Tiaa_Cntrct().getValue(pnd_Ii));      //Natural: ASSIGN #PAYEE2-ROLL-CONT := #SCR-R/O-TIAA-CNTRCT ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Roll_Cert().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Rfslash_O_Cref_Cert().getValue(pnd_Ii));        //Natural: ASSIGN #PAYEE2-ROLL-CERT := #SCR-R/O-CREF-CERT ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Ovrpy_Rcvry_Ind().setValue("N");                                                                                  //Natural: ASSIGN #PAYEE2-OVRPY-RCVRY-IND := 'N'
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Hold_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Hold_Ind().getValue(pnd_Ii));              //Natural: ASSIGN #PAYEE2-HOLD-IND := #SCR-PAYEE-HOLD-IND ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Type().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Type().getValue(pnd_Ii));                      //Natural: ASSIGN #PAYEE2-TYPE := #SCR-PAYEE-TYPE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Foreign_Code().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Frgn_Cde().getValue(pnd_Ii));          //Natural: ASSIGN #PAYEE2-FOREIGN-CODE := #SCR-PAYEE-FRGN-CDE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Method().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Method().getValue(pnd_Ii));                  //Natural: ASSIGN #PAYEE2-METHOD := #SCR-PAYEE-METHOD ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Hold_Code().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Hold_Cde().getValue(pnd_Ii));             //Natural: ASSIGN #PAYEE2-HOLD-CODE := #SCR-PAYEE-HOLD-CDE ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Manu_Check().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Proline_Ind().getValue(pnd_Ii));               //Natural: ASSIGN #PAYEE2-MANU-CHECK := #SCR-PROLINE-IND ( #II )
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Installment_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Inst_Date().getValue(pnd_Ii));   //Natural: ASSIGN #PAYEE2-INSTALLMENT-DATE-A := #SCR-PAYEE-INST-DATE ( #II )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Loan_Check() throws Exception                                                                                                                   //Natural: READ-LOAN-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Outst_Loan_Amt().getValue("*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Outst_Loan_Amt().getValue("*"));            //Natural: ASSIGN #OUTST-LOAN-AMT ( * ) := #SCR-OUTST-LOAN-AMT ( * )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Min_Dist_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Min_Distr_Ind());                                           //Natural: ASSIGN #MIN-DIST-IND := #SCR-MIN-DISTR-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Loan_Num().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Loan_Nbr());                                                    //Natural: ASSIGN #LOAN-NUM := #SCR-LOAN-NBR
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Loan_Princ_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Loan_Princ_Amt());                                        //Natural: ASSIGN #LOAN-PRINC-AMT := #SCR-LOAN-PRINC-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Loan_Intrst_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Loan_Intrst_Amt());                                      //Natural: ASSIGN #LOAN-INTRST-AMT := #SCR-LOAN-INTRST-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Late_Intrst_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Late_Int_Amt());                                         //Natural: ASSIGN #LATE-INTRST-AMT := #SCR-LATE-INT-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Total_Dflt_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Total_Default_Amt());                                     //Natural: ASSIGN #TOTAL-DFLT-AMT := #SCR-TOTAL-DEFAULT-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Funds_Dflt_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Funds_Dflt_Ind());                                        //Natural: ASSIGN #FUNDS-DFLT-IND := #SCR-FUNDS-DFLT-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Check_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Date());                                              //Natural: ASSIGN #CHECK-DATE-A := #SCR-CHECK-DATE
    }
    private void sub_Read_Settlement() throws Exception                                                                                                                   //Natural: READ-SETTLEMENT
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Ben_Dob().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ben_Dob());                                               //Natural: ASSIGN #SETTLE-BEN-DOB := #SCR-BEN-DOB
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Ben_Name().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ben_Name());                                             //Natural: ASSIGN #SETTLE-BEN-NAME := #SCR-BEN-NAME
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Ben_Sex().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ben_Sex());                                               //Natural: ASSIGN #SETTLE-BEN-SEX := #SCR-BEN-SEX
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Ben_Spouse_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ben_Spouse_Ind());                                 //Natural: ASSIGN #SETTLE-BEN-SPOUSE-IND := #SCR-BEN-SPOUSE-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Ben_Ssn().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ben_Ssn());                                               //Natural: ASSIGN #SETTLE-BEN-SSN := #SCR-BEN-SSN
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Calc_Method().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Calc_Method());                                       //Natural: ASSIGN #SETTLE-CALC-METHOD := #SCR-CALC-METHOD
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cert().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Mdo_Ben_Cert());                                     //Natural: ASSIGN #SETTLE-MDO-BEN-CERT := #SCR-MDO-BEN-CERT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cont().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Mdo_Ben_Cntr());                                     //Natural: ASSIGN #SETTLE-MDO-BEN-CONT := #SCR-MDO-BEN-CNTR
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Mdo_First_Pmnt_Date().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Mdo_First_Pmnt_Dte());                        //Natural: ASSIGN #SETTLE-MDO-FIRST-PMNT-DATE := #SCR-MDO-FIRST-PMNT-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Mdo_Mode().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Mdo_Mode());                                             //Natural: ASSIGN #SETTLE-MDO-MODE := #SCR-MDO-MODE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Mode_Change().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Mode_Change());                                       //Natural: ASSIGN #SETTLE-MODE-CHANGE := #SCR-MODE-CHANGE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Payment_Desc().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Paym_Descr());                                       //Natural: ASSIGN #SETTLE-PAYMENT-DESC := #SCR-PAYM-DESCR
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Payment_Option().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payment_Option());                                 //Natural: ASSIGN #SETTLE-PAYMENT-OPTION := #SCR-PAYMENT-OPTION
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Qualified().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Qualified());                                           //Natural: ASSIGN #SETTLE-QUALIFIED := #SCR-QUALIFIED
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Req_Beg_Date().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Req_Beg_Dte());                                      //Natural: ASSIGN #SETTLE-REQ-BEG-DATE := #SCR-REQ-BEG-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Sa_Spouse_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sa_Spouse_Ind());                                   //Natural: ASSIGN #SETTLE-SA-SPOUSE-IND := #SCR-SA-SPOUSE-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cert().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sec_Annt_Cert());                                   //Natural: ASSIGN #SETTLE-SEC-ANNT-CERT := #SCR-SEC-ANNT-CERT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cont().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sec_Annt_Cont());                                   //Natural: ASSIGN #SETTLE-SEC-ANNT-CONT := #SCR-SEC-ANNT-CONT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Dob_Date().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sec_Annt_Dob_Dte());                            //Natural: ASSIGN #SETTLE-SEC-ANNT-DOB-DATE := #SCR-SEC-ANNT-DOB-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Name().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sec_Annt_Name());                                   //Natural: ASSIGN #SETTLE-SEC-ANNT-NAME := #SCR-SEC-ANNT-NAME
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Sex().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sec_Annt_Sex());                                     //Natural: ASSIGN #SETTLE-SEC-ANNT-SEX := #SCR-SEC-ANNT-SEX
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Ssn().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Sec_Annt_Ssn());                                     //Natural: ASSIGN #SETTLE-SEC-ANNT-SSN := #SCR-SEC-ANNT-SSN
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Trb_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Trb_Ind());                                               //Natural: ASSIGN #SETTLE-TRB-IND := #SCR-TRB-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Settle_Trb_Pct().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Trb_Pcnt());                                              //Natural: ASSIGN #SETTLE-TRB-PCT := #SCR-TRB-PCNT
    }
    private void sub_Read_Fund_Info() throws Exception                                                                                                                    //Natural: READ-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Lob().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Lob());                                                              //Natural: ASSIGN #LOB := #SCR-LOB
        FOR04:                                                                                                                                                            //Natural: FOR #J = 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ticker_Symbol().getValue(pnd_J).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Smbl_Cde().getValue(pnd_J));           //Natural: MOVE #SCR-SMBL-CDE ( #J ) TO #TICKER-SYMBOL ( #J )
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Fund_Amt().getValue(pnd_J).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Smbl_Amt().getValue(pnd_J));                //Natural: MOVE #SCR-SMBL-AMT ( #J ) TO #FUND-AMT ( #J )
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Fund_Pct().getValue(pnd_J).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Smbl_Pct().getValue(pnd_J));                //Natural: MOVE #SCR-SMBL-PCT ( #J ) TO #FUND-PCT ( #J )
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Fund_Units().getValue(pnd_J).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Smbl_Units().getValue(pnd_J));            //Natural: MOVE #SCR-SMBL-UNITS ( #J ) TO #FUND-UNITS ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Financial_Maintenance() throws Exception                                                                                                        //Natural: READ-FINANCIAL-MAINTENANCE
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Dist_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Distrb_Amt());                                                  //Natural: ASSIGN #DIST-AMT := #SCR-DISTRB-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Non_Dist_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Non_Distrb_Amt());                                          //Natural: ASSIGN #NON-DIST-AMT := #SCR-NON-DISTRB-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Fund_Wthdrl_Amt().getValue("*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Fund_Withdr_Amt().getValue("*"));          //Natural: ASSIGN #FUND-WTHDRL-AMT ( * ) := #SCR-FUND-WITHDR-AMT ( * )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Accum_Before_Amt().getValue("*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Fund_Beg_Accum().getValue("*"));          //Natural: ASSIGN #ACCUM-BEFORE-AMT ( * ) := #SCR-FUND-BEG-ACCUM ( * )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Accum_After_Amt().getValue("*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Fund_End_Accum().getValue("*"));           //Natural: ASSIGN #ACCUM-AFTER-AMT ( * ) := #SCR-FUND-END-ACCUM ( * )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Net_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Net_Amt().getValue(1));                             //Natural: ASSIGN #PAYEE1-NET-AMT := #SCR-PAYEE-NET-AMT ( 1 )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Net_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Net_Amt().getValue(2));                             //Natural: ASSIGN #PAYEE2-NET-AMT := #SCR-PAYEE-NET-AMT ( 2 )
    }
    private void sub_Read_Tax() throws Exception                                                                                                                          //Natural: READ-TAX
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_Election_Ind_Ss().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Tax_Elctn_Ind());                                    //Natural: ASSIGN #TAX-ELECTION-IND-SS := #SCR-TAX-ELCTN-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Citizenship_Ss().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Citizenship());                                           //Natural: ASSIGN #CITIZENSHIP-SS := #SCR-CITIZENSHIP
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Residency_Ss().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Residency());                                               //Natural: ASSIGN #RESIDENCY-SS := #SCR-RESIDENCY
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ra_Nra_Ss().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Rafslash_Nra());                                               //Natural: ASSIGN #RA-NRA-SS := #SCR-RA/NRA
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_Date_W8_W9_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_W8fslash_W9_Date());                                    //Natural: ASSIGN #TAX-DATE-W8-W9-A := #SCR-W8/W9-DATE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_Payee_Code().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Tax_Payee_Cde());                                         //Natural: ASSIGN #TAX-PAYEE-CODE := #SCR-TAX-PAYEE-CDE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_Cont().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Tax_Contract());                                                //Natural: ASSIGN #TAX-CONT := #SCR-TAX-CONTRACT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_W8_Ben().getValue("*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_W8ben().getValue("*"));                         //Natural: ASSIGN #TAX-W8-BEN ( * ) := #SCR-W8BEN ( * )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_W9().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_W9());                                                            //Natural: ASSIGN #TAX-W9 := #SCR-W9
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_Year().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Tax_Year());                                                    //Natural: ASSIGN #TAX-YEAR := #SCR-TAX-YEAR
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ss_Tax_Id_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Tax_Id_Ind());                                             //Natural: ASSIGN #SS-TAX-ID-IND := #SCR-TAX-ID-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Taxable_Name().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Taxable_Name());                                            //Natural: ASSIGN #TAXABLE-NAME := #SCR-TAXABLE-NAME
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tax_Id_Number().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Tax_Id_Number());                                          //Natural: ASSIGN #TAX-ID-NUMBER := #SCR-TAX-ID-NUMBER
    }
    private void sub_Read_Divorce() throws Exception                                                                                                                      //Natural: READ-DIVORCE
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Court_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Court_Date());                                              //Natural: ASSIGN #COURT-DATE-A := #SCR-COURT-DATE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Dvsn_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Division_Date());                                            //Natural: ASSIGN #DVSN-DATE-A := #SCR-DIVISION-DATE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cont_From_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cntr_From_Dte());                                       //Natural: ASSIGN #CONT-FROM-DATE-A := #SCR-CNTR-FROM-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cont_To_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cntr_To_Dte());                                           //Natural: ASSIGN #CONT-TO-DATE-A := #SCR-CNTR-TO-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cont_Accum_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cntr_Accum_Amt());                                        //Natural: ASSIGN #CONT-ACCUM-AMT := #SCR-CNTR-ACCUM-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cont_Accum_Pct().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cntr_Accum_Perc());                                       //Natural: ASSIGN #CONT-ACCUM-PCT := #SCR-CNTR-ACCUM-PERC
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cert_From_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cert_From_Dte());                                       //Natural: ASSIGN #CERT-FROM-DATE-A := #SCR-CERT-FROM-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cert_To_Date_A().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cert_To_Dte());                                           //Natural: ASSIGN #CERT-TO-DATE-A := #SCR-CERT-TO-DTE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cert_Accum_Amt().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cert_Accum_Amt());                                        //Natural: ASSIGN #CERT-ACCUM-AMT := #SCR-CERT-ACCUM-AMT
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Cert_Accum_Pct().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Cert_Accum_Perc());                                       //Natural: ASSIGN #CERT-ACCUM-PCT := #SCR-CERT-ACCUM-PERC
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Spouse_Cont().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Spouse_Cntr());                                              //Natural: ASSIGN #SPOUSE-CONT := #SCR-SPOUSE-CNTR
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Spouse_Cert().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Spouse_Cert());                                              //Natural: ASSIGN #SPOUSE-CERT := #SCR-SPOUSE-CERT
    }
    private void sub_Read_Premium() throws Exception                                                                                                                      //Natural: READ-PREMIUM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ii.setValue(0);                                                                                                                                               //Natural: ASSIGN #II := 0
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 24
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(24)); pnd_I.nadd(1))
        {
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Prem_Dates().getValue(pnd_I).equals(" ")))                                                         //Natural: IF #SCR-PREM-DATES ( #I ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ii.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #II
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Prem_By_Bucket().getValue(pnd_Ii,"*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Prem_Amts().getValue(pnd_I,      //Natural: ASSIGN #PREM-BY-BUCKET ( #II, * ) := #SCR-PREM-AMTS ( #I, 1:5 )
                1,":",5));
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Prem_By_Fund().getValue(pnd_Ii,"*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Prem_Amts().getValue(pnd_I,        //Natural: ASSIGN #PREM-BY-FUND ( #II, * ) := #SCR-PREM-AMTS ( #I, 6:25 )
                6,":",25));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Prem_Totals().getValue("*").setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Prem_Totals().getValue("*"));                  //Natural: ASSIGN #PREM-TOTALS ( * ) := #SCR-PREM-TOTALS ( * )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Prem_Ftotal().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Prem_Ftotal());                                              //Natural: ASSIGN #PREM-FTOTAL := #SCR-PREM-FTOTAL
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Prem_Btotal().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Prem_Btotal());                                              //Natural: ASSIGN #PREM-BTOTAL := #SCR-PREM-BTOTAL
    }
    private void sub_Read_Ledgers() throws Exception                                                                                                                      //Natural: READ-LEDGERS
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR #II = 1 TO 90
        for (pnd_Ii.setValue(1); condition(pnd_Ii.lessOrEqual(90)); pnd_Ii.nadd(1))
        {
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Cmpny().getValue(1,pnd_Ii).notEquals("     ")))                                               //Natural: IF #SCR-LDGR-CMPNY ( 1, #II ) NE '     '
            {
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cnt().nadd(1);                                                                                                   //Natural: ADD 1 TO #LEDGER1-CNT
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Company().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Cmpny().getValue(1,        //Natural: ASSIGN #LEDGER1-COMPANY ( #II ) := #SCR-LDGR-CMPNY ( 1, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Acct().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Acct_Nmr().getValue(1,        //Natural: ASSIGN #LEDGER1-ACCT ( #II ) := #SCR-LDGR-ACCT-NMR ( 1, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Amount().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Amt().getValue(1,           //Natural: ASSIGN #LEDGER1-AMOUNT ( #II ) := #SCR-LDGR-AMT ( 1, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Ind().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Dbt_Crdt_Ind().getValue(1,          //Natural: ASSIGN #LEDGER1-IND ( #II ) := #SCR-DBT-CRDT-IND ( 1, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cont().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Contract().getValue(1,        //Natural: ASSIGN #LEDGER1-CONT ( #II ) := #SCR-LDGR-CONTRACT ( 1, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Eff_Date().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Eff_Dte().getValue(1,     //Natural: ASSIGN #LEDGER1-EFF-DATE ( #II ) := #SCR-LDGR-EFF-DTE ( 1, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Cost_Ctr().getValue(1,    //Natural: ASSIGN #LEDGER1-COST-CTR ( #II ) := #SCR-LDGR-COST-CTR ( 1, #II )
                    pnd_Ii));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #II = 1 TO 90
        for (pnd_Ii.setValue(1); condition(pnd_Ii.lessOrEqual(90)); pnd_Ii.nadd(1))
        {
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Cmpny().getValue(2,pnd_Ii).notEquals("     ")))                                               //Natural: IF #SCR-LDGR-CMPNY ( 2, #II ) NE '     '
            {
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cnt().nadd(1);                                                                                                   //Natural: ADD 1 TO #LEDGER2-CNT
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Company().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Cmpny().getValue(2,        //Natural: ASSIGN #LEDGER2-COMPANY ( #II ) := #SCR-LDGR-CMPNY ( 2, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Acct().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Acct_Nmr().getValue(2,        //Natural: ASSIGN #LEDGER2-ACCT ( #II ) := #SCR-LDGR-ACCT-NMR ( 2, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Amount().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Amt().getValue(2,           //Natural: ASSIGN #LEDGER2-AMOUNT ( #II ) := #SCR-LDGR-AMT ( 2, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Ind().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Dbt_Crdt_Ind().getValue(2,          //Natural: ASSIGN #LEDGER2-IND ( #II ) := #SCR-DBT-CRDT-IND ( 2, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cont().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Contract().getValue(2,        //Natural: ASSIGN #LEDGER2-CONT ( #II ) := #SCR-LDGR-CONTRACT ( 2, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Eff_Date().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Eff_Dte().getValue(2,     //Natural: ASSIGN #LEDGER2-EFF-DATE ( #II ) := #SCR-LDGR-EFF-DTE ( 2, #II )
                    pnd_Ii));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cost_Ctr().getValue(pnd_Ii).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ldgr_Cost_Ctr().getValue(2,    //Natural: ASSIGN #LEDGER2-COST-CTR ( #II ) := #SCR-LDGR-COST-CTR ( 2, #II )
                    pnd_Ii));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RITA201012 START
        if (condition(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cnt().greater(getZero()) || pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cnt().greater(getZero())))            //Natural: IF #LEDGER1-CNT > 0 OR #LEDGER2-CNT > 0
        {
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Residence().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Ph_Residence());                                        //Natural: ASSIGN #PH-RESIDENCE := #SCR-PH-RESIDENCE
        }                                                                                                                                                                 //Natural: END-IF
        //*  RITA201012 END
    }
    private void sub_Read_Gross_To_Net() throws Exception                                                                                                                 //Natural: READ-GROSS-TO-NET
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee1_Ivc_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Ivc_Ind().getValue(1));                             //Natural: ASSIGN #PAYEE1-IVC-IND := #SCR-PAYEE-IVC-IND ( 1 )
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payee2_Ivc_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Payee_Ivc_Ind().getValue(2));                             //Natural: ASSIGN #PAYEE2-IVC-IND := #SCR-PAYEE-IVC-IND ( 2 )
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(1,pnd_I).equals(getZero()) && ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(2, //Natural: IF #SCR-CHECK-TOTAL-PAYM ( 1, #I ) = 0 AND #SCR-CHECK-TOTAL-PAYM ( 2, #I ) = 0
                pnd_I).equals(getZero())))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(1,pnd_I).notEquals(getZero())))                                        //Natural: IF #SCR-CHECK-TOTAL-PAYM ( 1, #I ) NE 0
            {
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot1_Gross_Prcds().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(1,  //Natural: ASSIGN #TOT1-GROSS-PRCDS ( #I ) := #SCR-CHECK-TOTAL-PAYM ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Check1_Ivc().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Ivc().getValue(1,               //Natural: ASSIGN #CHECK1-IVC ( #I ) := #SCR-CHECK-IVC ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Dpi1_Amt().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Dpi().getValue(1,                 //Natural: ASSIGN #DPI1-AMT ( #I ) := #SCR-CHECK-DPI ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Int1_Ext_Ro_Amt().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Rfslash_O_Amt().getValue(1, //Natural: ASSIGN #INT1-EXT-RO-AMT ( #I ) := #SCR-CHECK-R/O-AMT ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Fed1_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Fed_Tax().getValue(1,      //Natural: ASSIGN #FED1-TAX-WTHHLD ( #I ) := #SCR-CHECK-FED-TAX ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Us1_Nra_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Nra_Tax().getValue(1,   //Natural: ASSIGN #US1-NRA-TAX-WTHHLD ( #I ) := #SCR-CHECK-NRA-TAX ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_St1_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_State_Tax().getValue(1,     //Natural: ASSIGN #ST1-TAX-WTHHLD ( #I ) := #SCR-CHECK-STATE-TAX ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Loc1_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Loc_Tax().getValue(1,      //Natural: ASSIGN #LOC1-TAX-WTHHLD ( #I ) := #SCR-CHECK-LOC-TAX ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Net1_Prcds().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Net_Proceeds().getValue(1,      //Natural: ASSIGN #NET1-PRCDS ( #I ) := #SCR-CHECK-NET-PROCEEDS ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Check1_Wthdrl_Chg().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Withdr_Chrg().getValue(1, //Natural: ASSIGN #CHECK1-WTHDRL-CHG ( #I ) := #SCR-CHECK-WITHDR-CHRG ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Gross().getValue(1).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(1,                 //Natural: ASSIGN #TOT-GROSS ( 1 ) := #TOT-GROSS ( 1 ) + #SCR-CHECK-TOTAL-PAYM ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Net().getValue(1).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Net_Proceeds().getValue(1,                 //Natural: ASSIGN #TOT-NET ( 1 ) := #TOT-NET ( 1 ) + #SCR-CHECK-NET-PROCEEDS ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Fedtax().getValue(1).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Fed_Tax().getValue(1,                   //Natural: ASSIGN #TOT-FEDTAX ( 1 ) := #TOT-FEDTAX ( 1 ) + #SCR-CHECK-FED-TAX ( 1, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Statax().getValue(1).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_State_Tax().getValue(1,                 //Natural: ASSIGN #TOT-STATAX ( 1 ) := #TOT-STATAX ( 1 ) + #SCR-CHECK-STATE-TAX ( 1, #I )
                    pnd_I));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(2,pnd_I).notEquals(getZero())))                                        //Natural: IF #SCR-CHECK-TOTAL-PAYM ( 2, #I ) NE 0
            {
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot2_Gross_Prcds().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(2,  //Natural: ASSIGN #TOT2-GROSS-PRCDS ( #I ) := #SCR-CHECK-TOTAL-PAYM ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Check2_Ivc().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Ivc().getValue(2,               //Natural: ASSIGN #CHECK2-IVC ( #I ) := #SCR-CHECK-IVC ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Dpi2_Amt().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Dpi().getValue(2,                 //Natural: ASSIGN #DPI2-AMT ( #I ) := #SCR-CHECK-DPI ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Int2_Ext_Ro_Amt().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Rfslash_O_Amt().getValue(2, //Natural: ASSIGN #INT2-EXT-RO-AMT ( #I ) := #SCR-CHECK-R/O-AMT ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Fed2_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Fed_Tax().getValue(2,      //Natural: ASSIGN #FED2-TAX-WTHHLD ( #I ) := #SCR-CHECK-FED-TAX ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Us2_Nra_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Nra_Tax().getValue(2,   //Natural: ASSIGN #US2-NRA-TAX-WTHHLD ( #I ) := #SCR-CHECK-NRA-TAX ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_St2_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_State_Tax().getValue(2,     //Natural: ASSIGN #ST2-TAX-WTHHLD ( #I ) := #SCR-CHECK-STATE-TAX ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Loc2_Tax_Wthhld().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Loc_Tax().getValue(2,      //Natural: ASSIGN #LOC2-TAX-WTHHLD ( #I ) := #SCR-CHECK-LOC-TAX ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Net2_Prcds().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Net_Proceeds().getValue(2,      //Natural: ASSIGN #NET2-PRCDS ( #I ) := #SCR-CHECK-NET-PROCEEDS ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Check2_Wthdrl_Chg().getValue(pnd_I).setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Withdr_Chrg().getValue(2, //Natural: ASSIGN #CHECK2-WTHDRL-CHG ( #I ) := #SCR-CHECK-WITHDR-CHRG ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Gross().getValue(2).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Total_Paym().getValue(2,                 //Natural: ASSIGN #TOT-GROSS ( 2 ) := #TOT-GROSS ( 2 ) + #SCR-CHECK-TOTAL-PAYM ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Net().getValue(2).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Net_Proceeds().getValue(2,                 //Natural: ASSIGN #TOT-NET ( 2 ) := #TOT-NET ( 2 ) + #SCR-CHECK-NET-PROCEEDS ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Fedtax().getValue(2).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_Fed_Tax().getValue(2,                   //Natural: ASSIGN #TOT-FEDTAX ( 2 ) := #TOT-FEDTAX ( 2 ) + #SCR-CHECK-FED-TAX ( 2, #I )
                    pnd_I));
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tot_Statax().getValue(2).nadd(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Check_State_Tax().getValue(2,                 //Natural: ASSIGN #TOT-STATAX ( 2 ) := #TOT-STATAX ( 2 ) + #SCR-CHECK-STATE-TAX ( 2, #I )
                    pnd_I));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Read_Roth_Info() throws Exception                                                                                                                    //Natural: READ-ROTH-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Roth_Contrib_Yyyy().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Roth_Contrib_Yyyy());                                  //Natural: ASSIGN #ROTH-CONTRIB-YYYY := #SCR-ROTH-CONTRIB-YYYY
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Roth_Qualified().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Roth_Qualified());                                        //Natural: ASSIGN #ROTH-QUALIFIED := #SCR-ROTH-QUALIFIED
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Dod_Dte().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Dod_Date());                                                     //Natural: ASSIGN #DOD-DTE := #SCR-DOD-DATE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Dob_Dte().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Dob_Date());                                                     //Natural: ASSIGN #DOB-DTE := #SCR-DOB-DATE
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Disabl_Dte().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Disabl_Dte());                                                //Natural: ASSIGN #DISABL-DTE := #SCR-DISABL-DTE
    }
    private void sub_Read_Approv_Auth() throws Exception                                                                                                                  //Natural: READ-APPROV-AUTH
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Approval_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Apprvl_Ind());                                              //Natural: ASSIGN #APPROVAL-IND := #SCR-APPRVL-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Auth_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Auth_Ind());                                                    //Natural: ASSIGN #AUTH-IND := #SCR-AUTH-IND
        pdaEfsa1205.getPnd_Appc_Parm_Pnd_Auth2_Ind().setValue(ldaEfsl3117.getCwf_Efm_Wks_Tran_View_Pnd_Scr_Auth2_Ind());                                                  //Natural: ASSIGN #AUTH2-IND := #SCR-AUTH2-IND
    }
    private void sub_Read_Na() throws Exception                                                                                                                           //Natural: READ-NA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************
        //*  /* FE201504 START COMMENT OUT
        //*  READ NA-VIEW BY CNTRCT-PAYEE-KEY STARTING FROM #CNTRCT-PAYEE-KEY
        //*   IF CNTRCT-NMBR �= #CNTRCT-NMBR
        //*    ESCAPE ROUTINE
        //*   END-IF
        //*   IF CORRESPONDENCE-ADDRSS-IND = 'O'
        //*    MOVE ADDRSS-LNE-1          TO #PH-ADDR1
        //*    MOVE ADDRSS-LNE-2          TO #PH-ADDR2
        //*    MOVE ADDRSS-LNE-3          TO #PH-ADDR3
        //*    MOVE ADDRSS-LNE-4          TO #PH-ADDR4
        //*    MOVE ADDRSS-LNE-5          TO #PH-ADDR5
        //*    MOVE ADDRSS-LNE-6          TO #PH-ADDR6
        //*    MOVE ADDRSS-GEOGRAPHIC-CDE TO #PH-STATE
        //*    MOVE #NA-ZIP               TO #PH-ZIP
        //*    ESCAPE ROUTINE
        //*   END-IF
        //*  END-READ                          /* FE201504 END COMMENT OUT
        //*  FE201504 START
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct());                                                   //Natural: ASSIGN #MDMA211.#I-CONTRACT-NUMBER := #MSG-TIAA-CNTRCT
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: ASSIGN #MDMA211.#I-PAYEE-CODE-A2 := '01'
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA211.#O-RETURN-CODE EQ '0000'
        {
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Number().equals(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct()) || pdaMdma211.getPnd_Mdma211_Pnd_O_Cref_Number().equals(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct()))) //Natural: IF #MDMA211.#O-CONTRACT-NUMBER = #MSG-TIAA-CNTRCT OR #MDMA211.#O-CREF-NUMBER = #MSG-TIAA-CNTRCT
            {
                //*   IF CORRESPONDENCE-ADDRSS-IND = 'O'
                //*     MOVE CNTRCT-NAME-FREE  TO #SURVIVOR-FR-FORM
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Addr1().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_1());                                                //Natural: MOVE #MDMA211.#O-CO-ADDRESS-LINE-1 TO #PH-ADDR1
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Addr2().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_2());                                                //Natural: MOVE #MDMA211.#O-CO-ADDRESS-LINE-2 TO #PH-ADDR2
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Addr3().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_3());                                                //Natural: MOVE #MDMA211.#O-CO-ADDRESS-LINE-3 TO #PH-ADDR3
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Addr4().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Line_4());                                                //Natural: MOVE #MDMA211.#O-CO-ADDRESS-LINE-4 TO #PH-ADDR4
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Addr5().reset();                                                                                                      //Natural: RESET #PH-ADDR5 #PH-ADDR6
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Addr6().reset();
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_State().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Geographic_Code());                                       //Natural: MOVE #MDMA211.#O-CO-ADDRESS-GEOGRAPHIC-CODE TO #PH-STATE
                pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Zip().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Co_Address_Zip_Code());                                                //Natural: MOVE #MDMA211.#O-CO-ADDRESS-ZIP-CODE TO #PH-ZIP
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201504 END
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        if (condition(Global.getERROR_NR().equals(3145)))                                                                                                                 //Natural: IF *ERROR-NR = 3145
        {
            if (condition(pnd_Retry_Ctr.less(25)))                                                                                                                        //Natural: IF #RETRY-CTR < 25
            {
                FOR01:                                                                                                                                                    //Natural: FOR #FOR-CTR = 1 TO 2000
                for (pnd_For_Ctr.setValue(1); condition(pnd_For_Ctr.lessOrEqual(2000)); pnd_For_Ctr.nadd(1))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Retry_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CTR
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaEfsa1205.getPnd_Appc_Parm_Cms_Rtrn_Cd().setValue("1");                                                                                                         //Natural: ASSIGN CMS-RTRN-CD = '1'
        pdaEfsa1205.getPnd_Appc_Parm_Error_Type().setValue("A");                                                                                                          //Natural: ASSIGN ERROR-TYPE = 'A'
        pdaEfsa1205.getPnd_Appc_Parm_Failing_Component().setValue("Natural");                                                                                             //Natural: ASSIGN FAILING-COMPONENT = 'Natural'
        pdaEfsa1205.getPnd_Appc_Parm_Failing_Request().setValue(Global.getPROGRAM());                                                                                     //Natural: ASSIGN FAILING-REQUEST = *PROGRAM
        pdaEfsa1205.getPnd_Appc_Parm_Failing_Date().setValue(Global.getDATU());                                                                                           //Natural: ASSIGN FAILING-DATE = *DATU
        pdaEfsa1205.getPnd_Appc_Parm_Failing_Time().setValue(Global.getTIME());                                                                                           //Natural: ASSIGN FAILING-TIME = *TIME
        pdaEfsa1205.getPnd_Appc_Parm_Rc_Failing_Component().setValue(Global.getERROR_NR());                                                                               //Natural: ASSIGN RC-FAILING-COMPONENT = *ERROR-NR
        pdaEfsa1205.getPnd_Appc_Parm_Error_Text_1().setValue("Unsuccessful");                                                                                             //Natural: ASSIGN ERROR-TEXT-1 = 'Unsuccessful'
        pdaEfsa1205.getPnd_Appc_Parm_Error_Text_2().setValue(DbsUtil.compress("NATURAL error", Global.getERROR_NR(), "in", Global.getPROGRAM(), "at line",                //Natural: COMPRESS 'NATURAL error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE INTO ERROR-TEXT-2
            Global.getERROR_LINE()));
        pdaEfsa1205.getPnd_Appc_Parm_Length_Msg().setValue(324);                                                                                                          //Natural: ASSIGN LENGTH-MSG = 324
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
}
