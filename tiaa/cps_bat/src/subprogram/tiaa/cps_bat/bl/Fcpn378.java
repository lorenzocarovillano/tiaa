/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:23:10 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn378
************************************************************
**        * FILE NAME            : Fcpn378.java
**        * CLASS NAME           : Fcpn378
**        * INSTANCE NAME        : Fcpn378
************************************************************
************************************************************************
* PROGRAM  : FCPN378
* SYSTEM   : CPS
* TITLE    : HEADINGS FOR MONTHLY SETTLEMENT PROTOTYPES (NOTIFICATION)
* FUNCTION : THIS PROGRAM GENERATES THE PROPER "HEADINGS" PORTION FOR
*            MONTHLY SETTLEMENT PROTOTYPES, IN ACCORD WITH THE
*            "XEROX" PROTOTYPES TO PRINT THE MS PAYMENT DOCUMENTS.
*            NOTIFICATION LETTER.
* ----------------------------------------------------------------------
*  NOTES:
* ----------------------------------------------------------------------
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn378 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;

    private DbsGroup msg_Info_Sub;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Nr;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data;

    private DbsGroup msg_Info_Sub__R_Field_1;

    private DbsGroup msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data_Char;
    private DbsField msg_Info_Sub_Pnd_Pnd_Return_Code;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index1;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index2;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index3;

    private DbsGroup tbldcoda;

    private DbsGroup tbldcoda_Inputs;
    private DbsField tbldcoda_Pnd_Table_Id;
    private DbsField tbldcoda_Pnd_Code;
    private DbsField tbldcoda_Pnd_Mode;

    private DbsGroup tbldcoda_Outputs;
    private DbsField tbldcoda_Pnd_Target;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_2;
    private DbsField pnd_Ws_Rec_1_Pnd_Cc;
    private DbsField pnd_Ws_Rec_1_Pnd_Text;

    private DbsGroup pnd_Ws_Rec_1__R_Field_3;
    private DbsField pnd_Ws_Rec_1__Filler1;
    private DbsField pnd_Ws_Rec_1_Pnd_Name;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Ws1;
    private DbsField pnd_Ws_Edited_Ppcn;
    private DbsField pnd_Ws_Edited_Date;
    private DbsField pnd_Start_Date_Literal;
    private DbsField pnd_I;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Blank_1;
    private DbsField pnd_Const_Whiteout_Char;
    private DbsField pnd_Const_One;
    private DbsField whiteout_All;

    private DbsGroup whiteout_All__R_Field_4;
    private DbsField whiteout_All_Whiteout_Pos;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        msg_Info_Sub = localVariables.newGroupInRecord("msg_Info_Sub", "MSG-INFO-SUB");
        msg_Info_Sub_Pnd_Pnd_Msg = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        msg_Info_Sub_Pnd_Pnd_Msg_Nr = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        msg_Info_Sub_Pnd_Pnd_Msg_Data = msg_Info_Sub.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1, 
            3));

        msg_Info_Sub__R_Field_1 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub__R_Field_1", "REDEFINE", msg_Info_Sub_Pnd_Pnd_Msg_Data);

        msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct = msg_Info_Sub__R_Field_1.newGroupArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct", "##MSG-DATA-STRUCT", 
            new DbsArrayController(1, 3));
        msg_Info_Sub_Pnd_Pnd_Msg_Data_Char = msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Char", "##MSG-DATA-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1, 32));
        msg_Info_Sub_Pnd_Pnd_Return_Code = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        msg_Info_Sub_Pnd_Pnd_Error_Field = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index1 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index2 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index3 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);

        tbldcoda = localVariables.newGroupInRecord("tbldcoda", "TBLDCODA");

        tbldcoda_Inputs = tbldcoda.newGroupInGroup("tbldcoda_Inputs", "INPUTS");
        tbldcoda_Pnd_Table_Id = tbldcoda_Inputs.newFieldInGroup("tbldcoda_Pnd_Table_Id", "#TABLE-ID", FieldType.NUMERIC, 6);
        tbldcoda_Pnd_Code = tbldcoda_Inputs.newFieldInGroup("tbldcoda_Pnd_Code", "#CODE", FieldType.STRING, 20);
        tbldcoda_Pnd_Mode = tbldcoda_Inputs.newFieldInGroup("tbldcoda_Pnd_Mode", "#MODE", FieldType.STRING, 1);

        tbldcoda_Outputs = tbldcoda.newGroupInGroup("tbldcoda_Outputs", "OUTPUTS");
        tbldcoda_Pnd_Target = tbldcoda_Outputs.newFieldInGroup("tbldcoda_Pnd_Target", "#TARGET", FieldType.STRING, 60);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_2", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Cc = pnd_Ws_Rec_1__R_Field_2.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Cc", "#CC", FieldType.STRING, 2);
        pnd_Ws_Rec_1_Pnd_Text = pnd_Ws_Rec_1__R_Field_2.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Text", "#TEXT", FieldType.STRING, 141);

        pnd_Ws_Rec_1__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_3", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1__Filler1 = pnd_Ws_Rec_1__R_Field_3.newFieldInGroup("pnd_Ws_Rec_1__Filler1", "_FILLER1", FieldType.STRING, 37);
        pnd_Ws_Rec_1_Pnd_Name = pnd_Ws_Rec_1__R_Field_3.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Name", "#NAME", FieldType.STRING, 106);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Ws1 = pnd_Ws.newFieldInGroup("pnd_Ws_Ws1", "WS1", FieldType.STRING, 120);
        pnd_Ws_Edited_Ppcn = pnd_Ws.newFieldInGroup("pnd_Ws_Edited_Ppcn", "EDITED-PPCN", FieldType.STRING, 9);
        pnd_Ws_Edited_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Edited_Date", "EDITED-DATE", FieldType.STRING, 11);
        pnd_Start_Date_Literal = localVariables.newFieldInRecord("pnd_Start_Date_Literal", "#START-DATE-LITERAL", FieldType.STRING, 22);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Blank_1 = pnd_Const.newFieldInGroup("pnd_Const_Blank_1", "BLANK-1", FieldType.STRING, 1);
        pnd_Const_Whiteout_Char = pnd_Const.newFieldInGroup("pnd_Const_Whiteout_Char", "WHITEOUT-CHAR", FieldType.STRING, 1);
        pnd_Const_One = pnd_Const.newFieldInGroup("pnd_Const_One", "ONE", FieldType.NUMERIC, 1);
        whiteout_All = localVariables.newFieldInRecord("whiteout_All", "WHITEOUT-ALL", FieldType.STRING, 60);

        whiteout_All__R_Field_4 = localVariables.newGroupInRecord("whiteout_All__R_Field_4", "REDEFINE", whiteout_All);
        whiteout_All_Whiteout_Pos = whiteout_All__R_Field_4.newFieldArrayInGroup("whiteout_All_Whiteout_Pos", "WHITEOUT-POS", FieldType.STRING, 1, new 
            DbsArrayController(1, 60));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_I.setInitialValue(0);
        pnd_Const_Blank_1.setInitialValue(" ");
        pnd_Const_Whiteout_Char.setInitialValue("�");
        pnd_Const_One.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn378() throws Exception
    {
        super("Fcpn378");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        //*    *************
        //*    * MAIN LINE *
        //*    *************
        short decideConditionsMet259 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE 'L'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L"))))
        {
            decideConditionsMet259++;
                                                                                                                                                                          //Natural: PERFORM SURVIVOR-BENEFITS-HEADINGS
            sub_Survivor_Benefits_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'PP'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("PP"))))
        {
            decideConditionsMet259++;
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals(" ")))                                                                               //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = ' '
            {
                                                                                                                                                                          //Natural: PERFORM SURVIVOR-BENEFITS-HEADINGS
                sub_Survivor_Benefits_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                           //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
                {
                                                                                                                                                                          //Natural: PERFORM MATURITY-HEADINGS
                    sub_Maturity_Headings();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet259 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM START-DATE-LINE
            sub_Start_Date_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E01 - Unexpected CNTRCT-TYPE-CDE",pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde());                           //Natural: WRITE *PROGRAM 'E01 - Unexpected CNTRCT-TYPE-CDE' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*    *****************
        //*    * SUBROUTINES   *
        //*    *****************
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATURITY-HEADINGS
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SURVIVOR-BENEFITS-HEADINGS
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START-DATE-LINE
        //* *EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REC
    }
    //*  PROTOTYPE#4
    private void sub_Maturity_Headings() throws Exception                                                                                                                 //Natural: MATURITY-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A MATURITY PROCESSING SETTLEMENT,
        //*  USING PRINT LAYOUT PROTOTYPE #4
        //*  ----------------------------------------------------------------------
        //*  .............. USE THE CNTRCT-MODE-CDE TO SEARCH TABLE#5
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        short decideConditionsMet307 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-MODE-CDE;//Natural: VALUE 100
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().equals(100))))
        {
            decideConditionsMet307++;
            tbldcoda_Pnd_Target.setValue("Monthly");                                                                                                                      //Natural: ASSIGN TBLDCODA.#TARGET := 'Monthly'
            whiteout_All_Whiteout_Pos.getValue(1,":",39).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:39 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(601) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(603)))))
        {
            decideConditionsMet307++;
            tbldcoda_Pnd_Target.setValue("Quarterly");                                                                                                                    //Natural: ASSIGN TBLDCODA.#TARGET := 'Quarterly'
            whiteout_All_Whiteout_Pos.getValue(1,":",42).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:42 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(701) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(706)))))
        {
            decideConditionsMet307++;
            tbldcoda_Pnd_Target.setValue("Semi-Annual");                                                                                                                  //Natural: ASSIGN TBLDCODA.#TARGET := 'Semi-Annual'
            whiteout_All_Whiteout_Pos.getValue(1,":",45).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:45 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(801) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(812)))))
        {
            decideConditionsMet307++;
            tbldcoda_Pnd_Target.setValue("Annual");                                                                                                                       //Natural: ASSIGN TBLDCODA.#TARGET := 'Annual'
            whiteout_All_Whiteout_Pos.getValue(1,":",39).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:39 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            tbldcoda_Pnd_Target.setValue("Unknown");                                                                                                                      //Natural: ASSIGN TBLDCODA.#TARGET := 'Unknown'
            whiteout_All_Whiteout_Pos.getValue(1,":",45).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:45 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Rec_1_Pnd_Text.setValue("Periodic Payment For:");                                                                                                      //Natural: ASSIGN #WS-REC-1.#TEXT := 'Periodic Payment For:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1_Pnd_Text.setValue(DbsUtil.compress(tbldcoda_Pnd_Target, "payment for:"));                                                                        //Natural: COMPRESS TBLDCODA.#TARGET 'payment for:' INTO #WS-REC-1.#TEXT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Ws1.setValue(whiteout_All);                                                                                                                                //Natural: ASSIGN #WS.WS1 := WHITEOUT-ALL
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("+2");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '+2'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //* * .... ANNUITY START DATE
        //* *#START-DATE-LITERAL := 'Annuity Starting Date:'
        //*  MATURITY-HEADINGS
    }
    //*  PROTOTYPE#4
    private void sub_Survivor_Benefits_Headings() throws Exception                                                                                                        //Natural: SURVIVOR-BENEFITS-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A SURVIVOR BENEFITS PROCESSING,
        //*  USING PRINT LAYOUT PROTOTYPE #4
        //*  ----------------------------------------------------------------------
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.setValue("Periodic Payment For:");                                                                                                                 //Natural: ASSIGN #WS.WS1 := 'Periodic Payment For:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Ws1.setValue("Survivor Benefit Payment for:");                                                                                                         //Natural: ASSIGN #WS.WS1 := 'Survivor Benefit Payment for:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //* * CHG 11/22/93 PER TIM: DONT SHOW SETTLMENT DATE
        //* * .... ANNUITY START DATE
        //* *#START-DATE-LITERAL := 'Annuity Starting Date:'
        //*  SURVIVOR-BENEFITS-HEADINGS /* PROTOTYPE#4
    }
    private void sub_Start_Date_Line() throws Exception                                                                                                                   //Natural: START-DATE-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE "START DATE" LINE
        //*  ----------------------------------------------------------------------
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte().notEquals(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-DTE NE 0
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Eft_Dte(),new ReportEditMask("MM/DD/YY"));                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-EFT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CLEAR LINE FROM OLD STUFF
        pnd_Ws_Ws1.reset();                                                                                                                                               //Natural: RESET #WS.WS1
        pnd_Ws_Ws1.setValue(DbsUtil.compress("Payment Date:", pnd_Ws_Edited_Date));                                                                                       //Natural: COMPRESS 'Payment Date:' #WS.EDITED-DATE INTO #WS.WS1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("17");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '17'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  START-DATE-LINE
    }
    private void sub_Print_Rec() throws Exception                                                                                                                         //Natural: PRINT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  WRITE THE PRINT LINE TO THE WORK FILE; LATER IT IS PRINTED ON THE
        //*  XEROX LASER PRINTER ACCORDING TO THE XEROX COMMANDS EMBEDDED IN THE
        //*  FILE.
        //*  ----------------------------------------------------------------------
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
    }

    //
}
