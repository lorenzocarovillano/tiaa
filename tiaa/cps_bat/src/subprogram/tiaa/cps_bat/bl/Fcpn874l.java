/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:54 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn874l
************************************************************
**        * FILE NAME            : Fcpn874l.java
**        * CLASS NAME           : Fcpn874l
**        * INSTANCE NAME        : Fcpn874l
************************************************************
************************************************************************
* SUBPROGRAM : FCPN874L
* SYSTEM     : CPS
* TITLE      : NEW ANNUITIZATION
* FUNCTION   : "NZ" ANNUITANT STATEMENTS - DEDUCTION LEDGENDS
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn874l extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa873 pdaFcpa873;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa803l pdaFcpa803l;
    private LdaFcpl803l ldaFcpl803l;
    private LdaFcpltbcd ldaFcpltbcd;
    private PdaTbldcoda pdaTbldcoda;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_State_Idx;
    private DbsField pnd_Ws_Pnd_Ded_Disp_Array;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl803l = new LdaFcpl803l();
        registerRecord(ldaFcpl803l);
        ldaFcpltbcd = new LdaFcpltbcd();
        registerRecord(ldaFcpltbcd);
        localVariables = new DbsRecord();
        pdaTbldcoda = new PdaTbldcoda(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa873 = new PdaFcpa873(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa803l = new PdaFcpa803l(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_State_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Idx", "#STATE-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ded_Disp_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Ded_Disp_Array", "#DED-DISP-ARRAY", FieldType.STRING, 36, new DbsArrayController(1, 
            10));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803l.initializeValues();
        ldaFcpltbcd.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn874l() throws Exception
    {
        super("Fcpn874l");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                                 //Natural: ASSIGN TBLDCODA.#MODE := 'T'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(1).notEquals(" ")))                                                                //Natural: IF #DED-PYMNT-TABLE ( 1 ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(1));                              //Natural: ASSIGN #OUTPUT-REC-DETAIL := #DED-PYMNT-TABLE ( 1 )
            setValueToSubstring(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Rtb_Ledgend(),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),5,31);                                    //Natural: MOVE #RTB-LEDGEND TO SUBSTR ( #OUTPUT-REC-DETAIL,5,31 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(2).notEquals(" ")))                                                                //Natural: IF #DED-PYMNT-TABLE ( 2 ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(2));                              //Natural: ASSIGN #OUTPUT-REC-DETAIL := #DED-PYMNT-TABLE ( 2 )
            if (condition(pdaFcpa803l.getPnd_Fcpa803l_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                           //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
            {
                pnd_Ws_Pnd_I.setValue(2);                                                                                                                                 //Natural: ASSIGN #I := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_I.setValue(1);                                                                                                                                 //Natural: ASSIGN #I := 1
            }                                                                                                                                                             //Natural: END-IF
            setValueToSubstring(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Federal_Ledgend().getValue(pnd_Ws_Pnd_I),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),               //Natural: MOVE #FEDERAL-LEDGEND ( #I ) TO SUBSTR ( #OUTPUT-REC-DETAIL,5,33 )
                5,33);
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(3).notEquals(" ")))                                                                //Natural: IF #DED-PYMNT-TABLE ( 3 ) NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEDGEND
            sub_Format_State_Ledgend();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(3));                              //Natural: ASSIGN #OUTPUT-REC-DETAIL := #DED-PYMNT-TABLE ( 3 )
            setValueToSubstring(pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(3),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),5,34);                  //Natural: MOVE #DED-LEDGEND-TABLE ( 3 ) TO SUBSTR ( #OUTPUT-REC-DETAIL,5,34 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(4).notEquals(" ")))                                                                //Natural: IF #DED-PYMNT-TABLE ( 4 ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(4));                              //Natural: ASSIGN #OUTPUT-REC-DETAIL := #DED-PYMNT-TABLE ( 4 )
            setValueToSubstring(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Local_Ledgend(),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),5,20);                                  //Natural: MOVE #LOCAL-LEDGEND TO SUBSTR ( #OUTPUT-REC-DETAIL,5,20 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(5).notEquals(" ")))                                                                //Natural: IF #DED-PYMNT-TABLE ( 5 ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(5));                              //Natural: ASSIGN #OUTPUT-REC-DETAIL := #DED-PYMNT-TABLE ( 5 )
            setValueToSubstring("= Canadian Tax Withheld",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),5,23);                                                        //Natural: MOVE '= Canadian Tax Withheld' TO SUBSTR ( #OUTPUT-REC-DETAIL,5,23 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEDGEND
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE
        //* **************************
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Format_State_Ledgend() throws Exception                                                                                                              //Natural: FORMAT-STATE-LEDGEND
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde().lessOrEqual("57")))             //Natural: IF ANNT-RSDNCY-CDE = '01' THRU '57'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(3).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_State_Ledgend().getValue(1));                        //Natural: ASSIGN #DED-LEDGEND-TABLE ( 3 ) := #STATE-LEDGEND ( 1 )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top().equals(getZero())))                                                                               //Natural: IF #STATE-TABLE-TOP = 0
        {
                                                                                                                                                                          //Natural: PERFORM GET-STATE
            sub_Get_State();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Cde_Table().getValue(1,":",pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top())),       //Natural: EXAMINE #STATE-CDE-TABLE ( 1:#STATE-TABLE-TOP ) FOR ANNT-RSDNCY-CDE GIVING INDEX IN #STATE-IDX
                new ExamineSearch(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde()), new ExamineGivingIndex(pnd_Ws_Pnd_State_Idx));
            if (condition(pnd_Ws_Pnd_State_Idx.equals(getZero())))                                                                                                        //Natural: IF #STATE-IDX = 0
            {
                                                                                                                                                                          //Natural: PERFORM GET-STATE
                sub_Get_State();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(3).setValue(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx));   //Natural: ASSIGN #DED-LEDGEND-TABLE ( 3 ) := #STATE-TABLE ( #STATE-IDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_State() throws Exception                                                                                                                         //Natural: GET-STATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(19);                                                                                                              //Natural: ASSIGN TBLDCODA.#TABLE-ID := 19
        pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde());                                                                       //Natural: ASSIGN TBLDCODA.#CODE := ANNT-RSDNCY-CDE
        DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), ldaFcpltbcd.getTbldcoda_Msg());                                              //Natural: CALLNAT 'TBLDCOD' USING TBLDCODA TBLDCODA-MSG
        if (condition(Global.isEscape())) return;
        if (condition(ldaFcpltbcd.getTbldcoda_Msg_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF ##MSG-DATA ( 1 ) = ' '
        {
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top().nadd(1);                                                                                                    //Natural: ASSIGN #STATE-TABLE-TOP := #STATE-TABLE-TOP + 1
            pnd_Ws_Pnd_State_Idx.setValue(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top());                                                                             //Natural: ASSIGN #STATE-IDX := #STATE-TABLE-TOP
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Cde_Table().getValue(pnd_Ws_Pnd_State_Idx).setValue(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde());                     //Natural: ASSIGN #STATE-CDE-TABLE ( #STATE-IDX ) := ANNT-RSDNCY-CDE
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx).setValue(DbsUtil.compress("=", pdaTbldcoda.getTbldcoda_Pnd_Target(),             //Natural: COMPRESS '=' TBLDCODA.#TARGET #STATE-LEDGEND ( 3 ) INTO #STATE-TABLE ( #STATE-IDX )
                ldaFcpl803l.getPnd_Fcpl803l_Pnd_State_Ledgend().getValue(3)));
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(3).setValue(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx));       //Natural: ASSIGN #DED-LEDGEND-TABLE ( 3 ) := #STATE-TABLE ( #STATE-IDX )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(3).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_State_Ledgend().getValue(2));                        //Natural: ASSIGN #DED-LEDGEND-TABLE ( 3 ) := #STATE-LEDGEND ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
