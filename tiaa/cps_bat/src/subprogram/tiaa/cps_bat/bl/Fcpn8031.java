/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:32 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn8031
************************************************************
**        * FILE NAME            : Fcpn8031.java
**        * CLASS NAME           : Fcpn8031
**        * INSTANCE NAME        : Fcpn8031
************************************************************
************************************************************************
*
* SUBPROGRAM : FCPN8031
*
************************************************************************
*
* NOTE: THIS IS A CLONE OF FCPN8032 EXCEPT THAT IT USES FCPA800D INSTEAD
*       OF FCPA800 AND MAKES A CALL TO FCPN8034 INSTEAD OF FCPN8033.
*       THIS ALLOWS US TO RUN IN THE 1400 JOBSTREAM WITHOUT THE ROTH
*       ROTH FIELDS.  AER - ROTH-MAJOR1 - 6/2/08
*
************************************************************************
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AP" ANNUITANT STATEMENTS - "Body".
*
* 01/25/99 - ROXAN CARREON
* ADDED NEW MESSAGE LINE IN #AP-PAGE-1-TEXT FOR PA SELECT
*
* 01/19/2001 - LEON G. POPULATE #CNTRCT-ANNTY-INS-TYPE FIELD
*                      FOR FCPN199A CALL
* 01/25/2001 - LEON G. RESTOW TO TAKE IN CORRECT (NEW PRODUCTION)
*                      VERSION OF FCPL803G.
* 03/19/2001 - R. CARREON
*              ADD IVC MESSAGE
* 06/11/02   - R. CARREON.  RESTOW DUE TO FCPL199A
* 04/09/03   - R. CARREON.  MODIFY DUE TO TPA EGTRRA
* 11/14/2005 - RAMANA ANNE CHANGE TO ADD ANOTHER STOP POINT FOR
*                    PAYEE-MATCH PROJECT AND ADD NEW LOCAL AND PARAMETER
*                     AREAS
* 01/05/2006   R. LANDRUM
*              POS-PAY, PAYEE MATCH. ADD STOP POINT FOR CHECK NUMBER ON
*              STATEMENT BODY & CHECK FACE, 10 DIGIT MICR LINE.
* 06/02/08 : AER - ADDED FCPA800D AND FCPN8034 - ROTH-MAJOR1 CHANGES.
* 07/02/2009 - J. OSTEEN ADD ROTH INFO TO CHECK STATEMENT
* 01/29/2010 - J. OSTEEN INCREASE NUMBER OF DEDUCTION FROM 13 TO 14
* 12/2015    - J. OSTEEN ADD CHILD SUPPORT DEDUCTION PROCESSING  /*JWO1
*
*************************** NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR THE PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn8031 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803c pdaFcpa803c;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa803a pdaFcpa803a;
    private PdaFcpa803h pdaFcpa803h;
    private PdaFcpa803l pdaFcpa803l;
    private PdaFcpabar pdaFcpabar;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl803g ldaFcpl803g;
    private LdaFcpl876b ldaFcpl876b;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Cntrct_Totals;
    private DbsField pnd_Ws_Pnd_Cntrct_Settl_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Dpi_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Ded_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Net_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Ded_Table;
    private DbsField pnd_Ws_Pnd_Amt_Alpha;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Col;
    private DbsField pnd_Ws_Pnd_Ded_Idx;
    private DbsField pnd_Ws_Pnd_Cntrct_Pnd__Ded_Temp;
    private DbsField pnd_Ws_Tiaa_Ivc;
    private DbsField pnd_Ws_Cref_Ivc;
    private DbsField pnd_R;
    private DbsField pnd_A;
    private DbsField pnd_Ws_Lit;
    private DbsField pnd_Ws_From;
    private DbsField pnd_Ws_Amt;
    private DbsField pnd_Ws_Total_Ivc;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl803g = new LdaFcpl803g();
        registerRecord(ldaFcpl803g);
        ldaFcpl876b = new LdaFcpl876b();
        registerRecord(ldaFcpl876b);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803c = new PdaFcpa803c(parameters);
        pdaFcpa110 = new PdaFcpa110(parameters);
        pdaFcpa801b = new PdaFcpa801b(parameters);
        pdaFcpa800d = new PdaFcpa800d(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa803a = new PdaFcpa803a(parameters);
        pdaFcpa803h = new PdaFcpa803h(parameters);
        pdaFcpa803l = new PdaFcpa803l(parameters);
        pdaFcpabar = new PdaFcpabar(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Cntrct_Totals = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Cntrct_Totals", "#CNTRCT-TOTALS");
        pnd_Ws_Pnd_Cntrct_Settl_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Settl_Amt", "#CNTRCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Cntrct_Dpi_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Dpi_Amt", "#CNTRCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Cntrct_Ded_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Ded_Amt", "#CNTRCT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Cntrct_Net_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Net_Amt", "#CNTRCT-NET-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Cntrct_Ded_Table = pnd_Ws_Pnd_Cntrct_Totals.newFieldArrayInGroup("pnd_Ws_Pnd_Cntrct_Ded_Table", "#CNTRCT-DED-TABLE", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Amt_Alpha = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Alpha", "#AMT-ALPHA", FieldType.STRING, 15);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Col = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Col", "#COL", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ded_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ded_Idx", "#DED-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Cntrct_Pnd__Ded_Temp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Pnd__Ded_Temp", "#CNTRCT-#-DED-TEMP", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Tiaa_Ivc = localVariables.newFieldInRecord("pnd_Ws_Tiaa_Ivc", "#WS-TIAA-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Ivc = localVariables.newFieldInRecord("pnd_Ws_Cref_Ivc", "#WS-CREF-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Lit = localVariables.newFieldArrayInRecord("pnd_Ws_Lit", "#WS-LIT", FieldType.STRING, 4, new DbsArrayController(1, 2));
        pnd_Ws_From = localVariables.newFieldArrayInRecord("pnd_Ws_From", "#WS-FROM", FieldType.STRING, 4, new DbsArrayController(1, 2));
        pnd_Ws_Amt = localVariables.newFieldArrayInRecord("pnd_Ws_Amt", "#WS-AMT", FieldType.STRING, 15, new DbsArrayController(1, 2));
        pnd_Ws_Total_Ivc = localVariables.newFieldInRecord("pnd_Ws_Total_Ivc", "#WS-TOTAL-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803g.initializeValues();
        ldaFcpl876b.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Ded_Idx.setInitialValue(1);
        pnd_Ws_Tiaa_Ivc.setInitialValue(0);
        pnd_Ws_Cref_Ivc.setInitialValue(0);
        pnd_R.setInitialValue(0);
        pnd_A.setInitialValue(0);
        pnd_Ws_Lit.getValue(1).setInitialValue(" ");
        pnd_Ws_From.getValue(1).setInitialValue(" ");
        pnd_Ws_Amt.getValue(1).setInitialValue(" ");
        pnd_Ws_Total_Ivc.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn8031() throws Exception
    {
        super("Fcpn8031");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'ANNUITANT STATEMENT DETAIL CONTROL REPORT' 120T 'REPORT: RPT1' //
        pnd_Ws_Pnd_Cntrct_Pnd__Ded_Temp.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded());                                                                  //Natural: ASSIGN #CNTRCT-#-DED-TEMP := #CNTRCT-#-DED
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().getBoolean()))                                                                                            //Natural: IF #NEW-PYMNT
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(false);                                                                                                    //Natural: ASSIGN #NEW-PYMNT := FALSE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Ppcn_Break().setValue(true);                                                                                                    //Natural: ASSIGN #PPCN-BREAK := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().setValue(true);                                                                                                 //Natural: ASSIGN #COMPANY-BREAK := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().reset();                                                                                                         //Natural: RESET #FCPA803.#CURRENT-PAGE #GRAND-TOTALS
            pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Totals().reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Ppcn_Break().getBoolean()))                                                                                           //Natural: IF #PPCN-BREAK
        {
            pnd_Ws_Pnd_Cntrct_Totals.reset();                                                                                                                             //Natural: RESET #CNTRCT-TOTALS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Cntrct_Settl_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));        //Natural: ADD INV-ACCT-SETTL-AMT ( 1:INV-ACCT-COUNT ) TO #CNTRCT-SETTL-AMT
        if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().getBoolean()))                                                                                        //Natural: IF #DPI-IND
        {
            pnd_Ws_Pnd_Cntrct_Dpi_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));        //Natural: ADD INV-ACCT-DPI-AMT ( 1:INV-ACCT-COUNT ) TO #CNTRCT-DPI-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(1).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #CNTRCT-DED-TABLE ( 1 )
        pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(2).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #CNTRCT-DED-TABLE ( 2 )
        //*  JWO1
        pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(3).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #CNTRCT-DED-TABLE ( 3 )
        //*  FOR #I = 1 TO 13
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(10)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).equals(getZero())))                                                     //Natural: IF PYMNT-DED-CDE ( #I ) = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO1
            //*  JWO1
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).greaterOrEqual(900) && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).lessOrEqual(999))) //Natural: IF PYMNT-DED-CDE ( #I ) = 900 THRU 999
            {
                pnd_Ws_Pnd_J.setValue(15);                                                                                                                                //Natural: ASSIGN #J := 15
                //*  JWO1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).add(3));         //Natural: ADD PYMNT-DED-CDE ( #I ) 3 GIVING #J
                //*  JWO1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ws_Pnd_J).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Ws_Pnd_I));                             //Natural: ADD PYMNT-DED-AMT ( #I ) TO #CNTRCT-DED-TABLE ( #J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Cntrct_Ded_Amt.nadd(pnd_Ws_Pnd_Cntrct_Ded_Table.getValue("*"));                                                                                        //Natural: ADD #CNTRCT-DED-TABLE ( * ) TO #CNTRCT-DED-AMT
        //*  LEON
        pnd_Ws_Pnd_Cntrct_Net_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));      //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) TO #CNTRCT-NET-AMT
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO INV-ACCT-COUNT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I));                          //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := INV-ACCT-CDE-N ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I));          //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := INV-ACCT-VALUAT-PERIOD ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Ins_Type().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type());                                   //Natural: ASSIGN #FUND-PDA.#CNTRCT-ANNTY-INS-TYPE := CNTRCT-ANNTY-INS-TYPE
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            //*  IF  #FUND-PDA.#CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
            //*    WRITE '=' #FUND-PDA.#CNTRCT-ANNTY-INS-TYPE '=' CNTRCT-ANNTY-INS-TYPE
            //*          '=' #FUND-PDA.#INV-ACCT-INPUT
            //*          '=' #FUND-PDA.#STMNT-LINE-1
            //*          '=' #FUND-PDA.#STMNT-LINE-2
            //*  END-IF
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(pnd_Ws_Pnd_I))))             //Natural: IF #FCPA803.#CURRENT-PAGE NE #FUND-ON-PAGE ( #I )
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().setValue(true);                                                                                               //Natural: ASSIGN #NEWPAGE-IND := TRUE
                //*  NOT START OF PYMNT
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(getZero())))                                                                         //Natural: IF #FCPA803.#CURRENT-PAGE NE 0
                {
                                                                                                                                                                          //Natural: PERFORM BOTTOM-OF-PAGE
                    sub_Bottom_Of_Page();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #FCPA803.#CURRENT-PAGE := #FUND-ON-PAGE ( #I )
                pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().divide(2).multiply(2));                     //Natural: COMPUTE #J = #FCPA803.#CURRENT-PAGE / 2 * 2
                //*  EVEN PAGE
                if (condition(pnd_Ws_Pnd_J.equals(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page())))                                                                         //Natural: IF #J = #FCPA803.#CURRENT-PAGE
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(false);                                                                                             //Natural: ASSIGN #ODD-PAGE := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(true);                                                                                              //Natural: ASSIGN #ODD-PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM GEN-BARCODE
                    sub_Gen_Barcode();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().getBoolean()))                                                                                      //Natural: IF #NEWPAGE-IND
            {
                DbsUtil.callnat(Fcpn803x.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type());        //Natural: CALLNAT 'FCPN803X' USING #FCPA803 CNTRCT-ANNTY-INS-TYPE
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM GEN-PYMNT-HEADERS
                sub_Gen_Pymnt_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().setValue(true);                                                                                             //Natural: ASSIGN #COMPANY-BREAK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().getBoolean()))                                                                                    //Natural: IF #COMPANY-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM GEN-CNTRCT-HEADERS
                sub_Gen_Cntrct_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().setValue(false);                                                                                            //Natural: ASSIGN #COMPANY-BREAK := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Ppcn_Break().getBoolean()))                                                                                       //Natural: IF #PPCN-BREAK
            {
                pnd_Ws_Pnd_Ded_Idx.setValue(1);                                                                                                                           //Natural: ASSIGN #DED-IDX := 1
                pnd_Ws_Pnd_Col.setValue(1);                                                                                                                               //Natural: ASSIGN #COL := 1
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("05");                                                                                                //Natural: ASSIGN #OUTPUT-REC := '05'
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),new                 //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #OUTPUT-COL ( #COL )
                    ReportEditMask("XXXXXXX-X"));
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Cntrct_Settl_Amt,new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                                     //Natural: MOVE EDITED #CNTRCT-SETTL-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
                sub_Convert_Amt_Dollar();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().getBoolean()))                                                                                //Natural: IF #DPI-IND
                {
                    pnd_Ws_Pnd_Col.nadd(1);                                                                                                                               //Natural: ASSIGN #COL := #COL + 1
                    pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Cntrct_Dpi_Amt,new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                                   //Natural: MOVE EDITED #CNTRCT-DPI-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
                    sub_Convert_Amt_Dollar();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean()))                                                                          //Natural: IF #PYMNT-DED-IND
                {
                    pnd_Ws_Pnd_Col.nadd(1);                                                                                                                               //Natural: ASSIGN #COL := #COL + 1
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col().setValue(pnd_Ws_Pnd_Col);                                                                                     //Natural: ASSIGN #DED-COL := #COL
                    if (condition(pnd_Ws_Pnd_Cntrct_Ded_Amt.notEquals(new DbsDecimal("0.00"))))                                                                           //Natural: IF #CNTRCT-DED-AMT NE 0.00
                    {
                        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Cntrct_Ded_Amt,new ReportEditMask("+ZZZZ,ZZ9.99"));                                                //Natural: MOVE EDITED #CNTRCT-DED-AMT ( EM = +ZZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
                        sub_Convert_Amt_Dollar();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Cntrct_Net_Amt,new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                                       //Natural: MOVE EDITED #CNTRCT-NET-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
                sub_Convert_Amt_Dollar();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
                sub_Write_Body();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpa803.getPnd_Fcpa803_Pnd_Ppcn_Break().setValue(false);                                                                                               //Natural: ASSIGN #PPCN-BREAK := FALSE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #NEWPAGE-IND := FALSE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 3");                                                                                                    //Natural: MOVE ' 3' TO #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
            sub_Format_Deductions();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                    //Natural: MOVE ' 5' TO #OUTPUT-REC
            pnd_Ws_Pnd_Col.setValue(1);                                                                                                                                   //Natural: ASSIGN #COL := 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                 //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#STMNT-LINE-1
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("+Z,ZZZ,ZZ9.99"));        //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #I ) ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
            sub_Convert_Amt_Dollar();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean())))                                                                                  //Natural: IF NOT #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_9());                            //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#VALUAT-DESC-9
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                    //Natural: MOVE '+3' TO #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
            sub_Format_Deductions();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                    //Natural: MOVE ' 5' TO #OUTPUT-REC
            pnd_Ws_Pnd_Col.setValue(1);                                                                                                                                   //Natural: ASSIGN #COL := 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                 //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#STMNT-LINE-2
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                    //Natural: MOVE '+3' TO #OUTPUT-REC
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            short decideConditionsMet959 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-PDA.#DIVIDENDS
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))
            {
                decideConditionsMet959++;
                pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("-Z,ZZZ,ZZ9.99"));   //Natural: MOVE EDITED INV-ACCT-CNTRCT-AMT ( #I ) ( EM = -Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
                sub_Convert_Amt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Contractual");                                                              //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Contractual'
            }                                                                                                                                                             //Natural: WHEN INV-ACCT-UNIT-VALUE ( #I ) NE 0.0000
            else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.0000"))))
            {
                decideConditionsMet959++;
                pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Pnd_I),new ReportEditMask("ZZZ,ZZ9.999@"));      //Natural: MOVE EDITED INV-ACCT-UNIT-QTY ( #I ) ( EM = ZZZ,ZZ9.999@ ) TO #AMT-ALPHA
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pnd_Ws_Pnd_Amt_Alpha, MoveOption.RightJustified);                            //Natural: MOVE RIGHT #AMT-ALPHA TO #OUTPUT-COL ( #COL )
                setValueToSubstring(" ",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col),15,1);                                                        //Natural: MOVE ' ' TO SUBSTR ( #OUTPUT-COL ( #COL ) ,15,1 )
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Units");                                                                    //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Units'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
            sub_Format_Deductions();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 3");                                                                                                    //Natural: MOVE ' 3' TO #OUTPUT-REC
            pnd_Ws_Pnd_Col.setValue(2);                                                                                                                                   //Natural: ASSIGN #COL := 2
            short decideConditionsMet981 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-PDA.#DIVIDENDS
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))
            {
                decideConditionsMet981++;
                pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("-Z,ZZZ,ZZ9.99"));    //Natural: MOVE EDITED INV-ACCT-DVDND-AMT ( #I ) ( EM = -Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
                sub_Convert_Amt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Dividend");                                                                 //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Dividend'
            }                                                                                                                                                             //Natural: WHEN INV-ACCT-UNIT-VALUE ( #I ) NE 0.0000
            else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.0000"))))
            {
                decideConditionsMet981++;
                pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_I),new ReportEditMask("ZZ,ZZ9.9999@"));    //Natural: MOVE EDITED INV-ACCT-UNIT-VALUE ( #I ) ( EM = ZZ,ZZ9.9999@ ) TO #AMT-ALPHA
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pnd_Ws_Pnd_Amt_Alpha, MoveOption.RightJustified);                            //Natural: MOVE RIGHT #AMT-ALPHA TO #OUTPUT-COL ( #COL )
                setValueToSubstring(" ",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col),15,1);                                                        //Natural: MOVE ' ' TO SUBSTR ( #OUTPUT-COL ( #COL ) ,15,1 )
                pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                   //Natural: ASSIGN #COL := #COL + 1
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Unit Value");                                                               //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Unit Value'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
            sub_Format_Deductions();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  EXCESS DEDUCTIONS
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 3");                                                                                                        //Natural: MOVE ' 3' TO #OUTPUT-REC
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(!(pnd_Ws_Pnd_Cntrct_Pnd__Ded_Temp.greater(getZero())))) {break;}                                                                                //Natural: WHILE #CNTRCT-#-DED-TEMP GT 0
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
            sub_Format_Deductions();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Settl_Amt().nadd(pnd_Ws_Pnd_Cntrct_Settl_Amt);                                                                                //Natural: ADD #CNTRCT-SETTL-AMT TO #GRAND-SETTL-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Dpi_Amt().nadd(pnd_Ws_Pnd_Cntrct_Dpi_Amt);                                                                                    //Natural: ADD #CNTRCT-DPI-AMT TO #GRAND-DPI-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Ded_Amt().nadd(pnd_Ws_Pnd_Cntrct_Ded_Amt);                                                                                    //Natural: ADD #CNTRCT-DED-AMT TO #GRAND-DED-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Net_Amt().nadd(pnd_Ws_Pnd_Cntrct_Net_Amt);                                                                                    //Natural: ADD #CNTRCT-NET-AMT TO #GRAND-NET-AMT
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().equals(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Records())))                                       //Natural: IF #RECORD-IN-PYMNT = #PYMNT-RECORDS
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().setValue(true);                                                                                                  //Natural: ASSIGN #END-OF-PYMNT := TRUE
                                                                                                                                                                          //Natural: PERFORM BOTTOM-OF-PAGE
            sub_Bottom_Of_Page();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BODY
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMT-$
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMT
        //* ****************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DEDUCTIONS
        //* *WRITE '$$$$$$' '=' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR '=' #DED-IDX /
        //*   '=' #CNTRCT-#-DED    /
        //*   '=' PYMNT-DED-CDE(*) /
        //*   '='PYMNT-DED-AMT(*) /
        //*   '=' #CNTRCT-DED-TABLE(*)  /
        //*   '=' #DED-PYMNT-TABLE(*)  /
        //*  FOR #DED-IDX = #DED-IDX TO 14
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BOTTOM-OF-PAGE
        //* *******************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-PYMNT-HEADERS
        //* ********************* RL END PAYEE MATCH 1.0 **********************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-CNTRCT-HEADERS
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GRAND-TOTALS
        //* ***********************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-BARCODE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BARCODE-PROCESSING
        //*      '/CHECK/NUMBER'         #FCPL876B.PYMNT-CHECK-NBR
        //*  ---------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IVC-AMT
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Write_Body() throws Exception                                                                                                                        //Natural: WRITE-BODY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        setValueToSubstring(pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().getSubstring(48,96),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),44,100);                            //Natural: MOVE SUBSTR ( #OUTPUT-REC,48,96 ) TO SUBSTR ( #OUTPUT-REC,44,100 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Convert_Amt_Dollar() throws Exception                                                                                                                //Natural: CONVERT-AMT-$
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Amt_Alpha), new ExamineSearch("+"), new ExamineReplace("$"));                                                        //Natural: EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Convert_Amt() throws Exception                                                                                                                       //Natural: CONVERT-AMT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pnd_Ws_Pnd_Amt_Alpha);                                                               //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #AMT-ALPHA
    }
    private void sub_Format_Deductions() throws Exception                                                                                                                 //Natural: FORMAT-DEDUCTIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Ws_Pnd_Cntrct_Pnd__Ded_Temp.greater(getZero())))                                                                                                //Natural: IF #CNTRCT-#-DED-TEMP GT 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  JWO1
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #DED-IDX = #DED-IDX TO 15
        for (pnd_Ws_Pnd_Ded_Idx.setValue(pnd_Ws_Pnd_Ded_Idx); condition(pnd_Ws_Pnd_Ded_Idx.lessOrEqual(15)); pnd_Ws_Pnd_Ded_Idx.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ws_Pnd_Ded_Idx).equals(new DbsDecimal("0.00"))))                                                       //Natural: IF #CNTRCT-DED-TABLE ( #DED-IDX ) = 0.00
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ws_Pnd_Ded_Idx),new ReportEditMask("Z,ZZZ,ZZ9.99'( )'"));                        //Natural: MOVE EDITED #CNTRCT-DED-TABLE ( #DED-IDX ) ( EM = Z,ZZZ,ZZ9.99'( )' ) TO #AMT-ALPHA
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col()).setValue(pnd_Ws_Pnd_Amt_Alpha);                                  //Natural: ASSIGN #OUTPUT-COL ( #DED-COL ) := #AMT-ALPHA
            setValueToSubstring(pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(pnd_Ws_Pnd_Ded_Idx),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col()), //Natural: MOVE #DED-PYMNT-TABLE ( #DED-IDX ) TO SUBSTR ( #OUTPUT-COL ( #DED-COL ) ,14,1 )
                14,1);
            pnd_Ws_Pnd_Cntrct_Pnd__Ded_Temp.nsubtract(1);                                                                                                                 //Natural: ASSIGN #CNTRCT-#-DED-TEMP := #CNTRCT-#-DED-TEMP - 1
            pnd_Ws_Pnd_Ded_Idx.nadd(1);                                                                                                                                   //Natural: ASSIGN #DED-IDX := #DED-IDX + 1
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Bottom_Of_Page() throws Exception                                                                                                                    //Natural: BOTTOM-OF-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("06");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '06'
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().getBoolean()))                                                                                         //Natural: IF #END-OF-PYMNT
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-GRAND-TOTALS
            sub_Write_Grand_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Grand_Total_Text().getValue(1));                                   //Natural: ASSIGN #OUTPUT-REC-DETAIL := #GRAND-TOTAL-TEXT ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BOTTOM OF FIRST PAGE
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("1");                                                                                                     //Natural: ASSIGN #OUTPUT-REC := '1'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde().getSubstring(3,2).equals("IV")))                                                            //Natural: IF SUBSTRING ( WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE,3,2 ) = 'IV'
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 6");                                                                                                //Natural: ASSIGN #OUTPUT-REC := ' 6'
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_From_Rtb_Rollover_Text());                                 //Natural: ASSIGN #OUTPUT-REC-DETAIL := #IVC-FROM-RTB-ROLLOVER-TEXT
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //* ***********  ADD ROTH INFO TO CHECK STATEMENT      /* JWO 07/02/2009
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Roth_Ind().getBoolean()))                                                                                         //Natural: IF #ROTH-IND
            {
                                                                                                                                                                          //Natural: PERFORM GET-IVC-AMT
                sub_Get_Ivc_Amt();
                if (condition(Global.isEscape())) {return;}
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 6");                                                                                                //Natural: ASSIGN #OUTPUT-REC := ' 6'
                if (condition(pnd_Ws_Total_Ivc.notEquals(new DbsDecimal("0.00"))))                                                                                        //Natural: IF #WS-TOTAL-IVC NE 0.00
                {
                    pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Total_Ivc,new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                                            //Natural: MOVE EDITED #WS-TOTAL-IVC ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                    pnd_Ws_Pnd_Amt_Alpha.setValue(pnd_Ws_Pnd_Amt_Alpha, MoveOption.LeftJustified);                                                                        //Natural: MOVE LEFT #AMT-ALPHA TO #AMT-ALPHA
                    if (condition(pnd_Ws_Pnd_Amt_Alpha.getSubstring(1,1).equals("+")))                                                                                    //Natural: IF SUBSTR ( #AMT-ALPHA,1,1 ) = '+'
                    {
                        setValueToSubstring("$",pnd_Ws_Pnd_Amt_Alpha,1,1);                                                                                                //Natural: MOVE '$' TO SUBSTR ( #AMT-ALPHA,1,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Roth_Text(), pnd_Ws_Pnd_Amt_Alpha));      //Natural: COMPRESS #FCPL803G.#ROTH-TEXT #AMT-ALPHA INTO #OUTPUT-REC-DETAIL
                    //*  WRITE WORK FILE 8 #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //* * END *****  ADD ROTH INFO TO CHECK STATEMENT      /* JWO 07/02/2009
            }                                                                                                                                                             //Natural: END-IF
            //*                                    /* 04/16/2003  ROXAN -TPA EGTRRA
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Egtrra_Eligibility_Ind().equals("Y")))                                                                         //Natural: IF WF-PYMNT-ADDR-GRP.EGTRRA-ELIGIBILITY-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM GET-IVC-AMT
                sub_Get_Ivc_Amt();
                if (condition(Global.isEscape())) {return;}
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 6");                                                                                                //Natural: ASSIGN #OUTPUT-REC := ' 6'
                if (condition(pnd_Ws_Tiaa_Ivc.equals(getZero()) && pnd_Ws_Cref_Ivc.equals(getZero())))                                                                    //Natural: IF #WS-TIAA-IVC = 0 AND #WS-CREF-IVC = 0
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_Zero_Text());                                          //Natural: ASSIGN #OUTPUT-REC-DETAIL := #IVC-ZERO-TEXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_Text());                                               //Natural: ASSIGN #OUTPUT-REC-DETAIL := #IVC-TEXT
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_Text(), pnd_Ws_Amt.getValue(1),       //Natural: COMPRESS #IVC-TEXT #WS-AMT ( 1 ) #WS-FROM ( 1 ) #WS-LIT ( 1 ) #WS-AMT ( 2 ) #WS-FROM ( 2 ) #WS-LIT ( 2 ) INTO #OUTPUT-REC-DETAIL
                        pnd_Ws_From.getValue(1), pnd_Ws_Lit.getValue(1), pnd_Ws_Amt.getValue(2), pnd_Ws_From.getValue(2), pnd_Ws_Lit.getValue(2)));
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))                                                                                       //Natural: IF #GLOBAL-PAY
            {
                setValueToSubstring("        ",ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ap_Page_1_Text().getValue(2),28,78);                                                       //Natural: MOVE '        ' TO SUBSTR ( #AP-PAGE-1-TEXT ( 2 ) ,28,078 )
                setValueToSubstring("        ",ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ap_Page_1_Text().getValue(3),1,100);                                                       //Natural: MOVE '        ' TO SUBSTR ( #AP-PAGE-1-TEXT ( 3 ) ,01,100 )
                setValueToSubstring(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Phone_Text().getValue(2),ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ap_Page_1_Text().getValue(2),                //Natural: MOVE #PHONE-TEXT ( 2 ) TO SUBSTR ( #AP-PAGE-1-TEXT ( 2 ) ,28,47 )
                    28,47);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* PASELECT
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M"))) //Natural: IF CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
                {
                    setValueToSubstring(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Phone_Text().getValue(3),ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ap_Page_1_Text().getValue(5),            //Natural: MOVE #PHONE-TEXT ( 3 ) TO SUBSTR ( #AP-PAGE-1-TEXT ( 5 ) ,58,47 )
                        58,47);
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 7");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := ' 7'
            //*  PA SELECT
            //*  PA SELECT - NEW LINE ADDED
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M")))  //Natural: IF CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
            {
                FOR04:                                                                                                                                                    //Natural: FOR #J = 4 TO 6
                for (pnd_Ws_Pnd_J.setValue(4); condition(pnd_Ws_Pnd_J.lessOrEqual(6)); pnd_Ws_Pnd_J.nadd(1))
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ap_Page_1_Text().getValue(pnd_Ws_Pnd_J));                  //Natural: ASSIGN #OUTPUT-REC-DETAIL := #AP-PAGE-1-TEXT ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                     //Natural: ASSIGN #CC := ' '
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR05:                                                                                                                                                    //Natural: FOR #J = 1 TO 3
                for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(3)); pnd_Ws_Pnd_J.nadd(1))
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ap_Page_1_Text().getValue(pnd_Ws_Pnd_J));                  //Natural: ASSIGN #OUTPUT-REC-DETAIL := #AP-PAGE-1-TEXT ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                     //Natural: ASSIGN #CC := ' '
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  DEDUCTION LEDGENDS
            pdaFcpa803l.getPnd_Fcpa803l().setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                            //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #FCPA803L
            DbsUtil.callnat(Fcpn803l.class , getCurrentProcessState(), pdaFcpa801b.getPnd_Check_Fields(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803l.getPnd_Fcpa803l());    //Natural: CALLNAT 'FCPN803L' USING #CHECK-FIELDS #FCPA803 #FCPA803L
            if (condition(Global.isEscape())) return;
            //*  CHECK PART
            //*  CALLNAT 'FCPN8033' USING WF-PYMNT-ADDR-GRP(*) #FCPA803 #FCPA803A /* RL
            //*                           #CHECK-SORT-FIELDS FCPA110          /* RA
            //*  AER
            //*  AER
            DbsUtil.callnat(Fcpn8034.class , getCurrentProcessState(), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*"), pdaFcpa803.getPnd_Fcpa803(),                     //Natural: CALLNAT 'FCPN8034' USING WF-PYMNT-ADDR-GRP ( * ) #FCPA803 #FCPA803A #CHECK-SORT-FIELDS FCPA110
                pdaFcpa803a.getPnd_Fcpa803a(), pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa110.getFcpa110());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  BARCODE
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Gen_Pymnt_Headers() throws Exception                                                                                                                 //Natural: GEN-PYMNT-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean())))                                                                                         //Natural: IF NOT #ODD-PAGE
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("11");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '11'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            pdaFcpa803h.getPnd_Fcpa803h_Pnd_Gen_Headers().setValue(true);                                                                                                 //Natural: ASSIGN #GEN-HEADERS := TRUE
            //*   JUST AFTER XEROX COMMANDS ARE PRINTED
            //* ******************** RL BEGIN PAYEE MATCH 1.0 *********************
            //*  RL
            if (condition(pdaFcpa110.getFcpa110_Fcpa110_Source_Code().equals("P14A1")))                                                                                   //Natural: IF FCPA110.FCPA110-SOURCE-CODE = 'P14A1'
            {
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                               //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
                pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                          //Natural: ASSIGN #CHECK-SORT-FIELDS.PYMNT-NBR := #WS-CHECK-NBR-N10
                getReports().write(0, Global.getPROGRAM(),"4020","=",pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr(),NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_Source_Code(), //Natural: WRITE *PROGRAM '4020' '=' #CHECK-SORT-FIELDS.PYMNT-NBR / '=' FCPA110-SOURCE-CODE /
                    NEWLINE);
                if (Global.isEscape()) return;
                //*  RA RIGHT CORNER TOP OF THE LETTER
                //*  RA
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("12");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '12'
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());                                                 //Natural: ASSIGN #OUTPUT-REC-DETAIL := #CHECK-SORT-FIELDS.PYMNT-NBR
            //*  RA
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803h.getPnd_Fcpa803h().setValuesByName(pdaFcpa803.getPnd_Fcpa803());                                                                                       //Natural: MOVE BY NAME #FCPA803 TO #FCPA803H
        pdaFcpa803h.getPnd_Fcpa803h().setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #FCPA803H
        DbsUtil.callnat(Fcpn803h.class , getCurrentProcessState(), pdaFcpa803h.getPnd_Fcpa803h(), pdaFcpa803c.getPnd_Check_Sort_Fields());                                //Natural: CALLNAT 'FCPN803H' USING #FCPA803H #CHECK-SORT-FIELDS
        if (condition(Global.isEscape())) return;
    }
    private void sub_Gen_Cntrct_Headers() throws Exception                                                                                                                //Natural: GEN-CNTRCT-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  PA SELECT HEADING
        //*   NEW PA SELECT
        DbsUtil.callnat(Fcpn803t.class , getCurrentProcessState(), pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa801b.getPnd_Check_Fields(), pdaFcpa803.getPnd_Fcpa803(),  //Natural: CALLNAT 'FCPN803T' USING #CHECK-SORT-FIELDS #CHECK-FIELDS #FCPA803 CNTRCT-ANNTY-INS-TYPE
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Grand_Totals() throws Exception                                                                                                                //Natural: WRITE-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Col.setValue(1);                                                                                                                                       //Natural: ASSIGN #COL := 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Grand_Total_Text().getValue(2));                     //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #GRAND-TOTAL-TEXT ( 2 )
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Settl_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                         //Natural: MOVE EDITED #GRAND-SETTL-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().getBoolean()))                                                                                        //Natural: IF #DPI-IND
        {
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Dpi_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                       //Natural: MOVE EDITED #GRAND-DPI-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
            sub_Convert_Amt_Dollar();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean()))                                                                                  //Natural: IF #PYMNT-DED-IND
        {
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Ded_Amt(),new ReportEditMask("+ZZZZ,ZZ9.99"));                                        //Natural: MOVE EDITED #GRAND-DED-AMT ( EM = +ZZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
            sub_Convert_Amt_Dollar();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Net_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                           //Natural: MOVE EDITED #GRAND-NET-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Gen_Barcode() throws Exception                                                                                                                       //Natural: GEN-BARCODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            //*  FIRST PAGE OF ENVELOPE
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Demand_Feed().setValue(true);                                                                                           //Natural: MOVE TRUE TO #BAR-DEMAND-FEED
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Id_Num().nadd(1);                                                                                                   //Natural: ADD 1 TO #BAR-ENV-ID-NUM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().equals(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page())))                                                //Natural: IF #BAR-LAST-PAGE = #FCPA803.#CURRENT-PAGE
        {
            //*  LAST  PAGE OF ENVELOPE
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Set_Last_Page().setValue(true);                                                                                         //Natural: MOVE TRUE TO #BAR-SET-LAST-PAGE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_New_Run().getBoolean()))                                                                                      //Natural: IF #BAR-NEW-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpnbar.class , getCurrentProcessState(), pdaFcpabar.getPnd_Barcode_Pda());                                                                       //Natural: CALLNAT 'FCPNBAR' #BARCODE-PDA
        if (condition(Global.isEscape())) return;
    }
    private void sub_Barcode_Processing() throws Exception                                                                                                                //Natural: BARCODE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean())))                                                                                         //Natural: IF NOT #ODD-PAGE
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("11");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '11'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("1<");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '1<'
        FOR06:                                                                                                                                                            //Natural: FOR #J = 1 TO 17
        for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(17)); pnd_Ws_Pnd_J.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Barcode().getValue(pnd_Ws_Pnd_J));                           //Natural: ASSIGN #OUTPUT-REC-DETAIL := #BAR-BARCODE ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                             //Natural: ASSIGN #CC := ' '
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Set_Last_Page().getBoolean()))                                                                                //Natural: IF #BAR-SET-LAST-PAGE
        {
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee().setValue(pdaFcpa803a.getPnd_Fcpa803a_Pnd_Stmnt_Pymnt_Nme());                                                      //Natural: ASSIGN #FCPL876B.#ADDRESSEE := #STMNT-PYMNT-NME
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Id());                                                        //Natural: ASSIGN #FCPL876B.#BAR-ENV-ID := #BARCODE-PDA.#BAR-ENV-ID
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde());                                                   //Natural: ASSIGN #FCPL876B.CNTRCT-ORGN-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr());                                                   //Natural: ASSIGN #FCPL876B.CNTRCT-PPCN-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr().getSubstring(11,4));                               //Natural: MOVE SUBSTR ( WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR,11,4 ) TO #FCPL876B.CNTRCT-PAYEE-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                                   //Natural: ASSIGN #FCPL876B.CNTRCT-HOLD-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Integrity_Counter());                          //Natural: ASSIGN #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER := #BARCODE-PDA.#BAR-ENV-INTEGRITY-COUNTER
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))                                                                                            //Natural: IF #FCPA803.#STMNT
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Pymnt_Check_Nbr().reset();                                                                                                //Natural: RESET #FCPL876B.#PYMNT-CHECK-NBR
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(false);                                                                                                  //Natural: ASSIGN #FCPL876B.#CHECK := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                               //Natural: ASSIGN #FCPL876B.PYMNT-CHECK-NBR := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(true);                                                                                                   //Natural: ASSIGN #FCPL876B.#CHECK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Envelopes());                                                  //Natural: ASSIGN #FCPL876B.#BAR-ENVELOPES := #BARCODE-PDA.#BAR-ENVELOPES
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Pages_In_Env());                                            //Natural: ASSIGN #FCPL876B.#BAR-PAGES-IN-ENV := #BARCODE-PDA.#BAR-PAGES-IN-ENV
            //* ******************** RL BEGIN PAYEE MATCH 1.0 *********************
            //*  RL
            if (condition(pdaFcpa110.getFcpa110_Fcpa110_Source_Code().equals("P14A1")))                                                                                   //Natural: IF FCPA110.FCPA110-SOURCE-CODE = 'P14A1'
            {
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                               //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
                pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                          //Natural: ASSIGN #CHECK-SORT-FIELDS.PYMNT-NBR := #WS-CHECK-NBR-N10
                getReports().write(0, Global.getPROGRAM(),"5020","=",pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr(),NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_Source_Code(), //Natural: WRITE *PROGRAM '5020' '=' #CHECK-SORT-FIELDS.PYMNT-NBR / '=' FCPA110-SOURCE-CODE /
                    NEWLINE);
                if (Global.isEscape()) return;
                //*  RL DO I NEED THIS CODE?
            }                                                                                                                                                             //Natural: END-IF
            //* ********************* RL END PAYEE MATCH 1.0 **********************
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().getBoolean()))                                                                                          //Natural: IF #CSR-RUN
            {
                getWorkFiles().write(8, false, ldaFcpl876b.getPnd_Fcpl876b());                                                                                            //Natural: WRITE WORK FILE 8 #FCPL876B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",                                                //Natural: DISPLAY ( 1 ) ( HC = R ) '//ADDRESSEE' #FCPL876B.#ADDRESSEE ( HC = L ) '/BARCODE/ID' #FCPL876B.#BAR-ENV-ID ( HC = C ) 'PACKAGE/INTEG/RITY' #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER ( EM = �������9 ) '/ENVELOPE/NUMBER' #FCPL876B.#BAR-ENVELOPES ( EM = -Z,ZZZ,ZZ9 ) '# OF/PAGES IN/ENVELOPE' #FCPL876B.#BAR-PAGES-IN-ENV ( EM = -Z,ZZZ,ZZ9 ) '//CHECK' #FCPL876B.#CHECK ( EM = NO/YES ) '/ORGN/CODE' #FCPL876B.CNTRCT-ORGN-CDE ( LC = � ) '/CONTRACT/NUMBER' #FCPL876B.CNTRCT-PPCN-NBR ( HC = L ) '/PAYEE/CODE' #FCPL876B.CNTRCT-PAYEE-CDE ( HC = L ) '/HOLD/CODE' #FCPL876B.CNTRCT-HOLD-CDE '/CHECK/NUMBER' #CHECK-SORT-FIELDS.PYMNT-NBR
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
                    
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
                		pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CODE BELLOW IS A TEMP CODE
        //*  DISPLAY
        //*   '/HOLD'      #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
        //*   '/CMB'       CNTRCT-CMBN-NBR
        //*   '/PPCN'      WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
        //*   '/SEQ'       PYMNT-PRCSS-SEQ-NBR
        //*   'TOT/PAGES'  #CHECK-SORT-FIELDS.PYMNT-TOTAL-PAGES
        //*   'N/R'        #BAR-NEW-RUN           (EM=F/T)
        //*   'S/M'        #FCPA803.#STMNT        (EM=F/T)
        //*   'E/I'        #BAR-BARCODE(1)
        //*   'B/1'        #BAR-BARCODE(2)
        //*   'E/S'        #BAR-BARCODE(3)
        //*   'B/2'        #BAR-BARCODE(4)
        //*   'B/3'        #BAR-BARCODE(5)
        //*   'B/4'        #BAR-BARCODE(6)
        //*   'B/5'        #BAR-BARCODE(7)
        //*   'P/C'        #BAR-BARCODE(8)
        //*   'E/C'        #BAR-BARCODE(9)
        //*   '/PIN'       #BARCODE-PDA.#BAR-ENV-ID
        //*   'S/I'        #BAR-BARCODE(17)
        //*   '/ENV'       #BARCODE-PDA.#BAR-ENVELOPES
        //*   'P/E'        #BARCODE-PDA.#BAR-PAGES-IN-ENV
        //*   '/NAME'      #STMNT-PYMNT-NME       (AL=33)
        //*  CODE ABOVE IS A TEMP CODE
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Barcode_Input().resetInitial();                                                                                                 //Natural: RESET INITIAL #BARCODE-INPUT
    }
    private void sub_Get_Ivc_Amt() throws Exception                                                                                                                       //Natural: GET-IVC-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------------------------------------
        pnd_Ws_Lit.getValue("*").reset();                                                                                                                                 //Natural: RESET #WS-LIT ( * ) #WS-AMT ( * ) #WS-FROM ( * ) #A
        pnd_Ws_Amt.getValue("*").reset();
        pnd_Ws_From.getValue("*").reset();
        pnd_A.reset();
        FOR07:                                                                                                                                                            //Natural: FOR #R 1 40
        for (pnd_R.setValue(1); condition(pnd_R.lessOrEqual(40)); pnd_R.nadd(1))
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(pnd_R).equals(" ")))                                                                   //Natural: IF INV-ACCT-CDE ( #R ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(pnd_R).equals("T")))                                                               //Natural: IF INV-ACCT-COMPANY ( #R ) = 'T'
            {
                pnd_Ws_Tiaa_Ivc.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(pnd_R));                                                                //Natural: ADD INV-ACCT-IVC-AMT ( #R ) TO #WS-TIAA-IVC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Cref_Ivc.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(pnd_R));                                                                //Natural: ADD INV-ACCT-IVC-AMT ( #R ) TO #WS-CREF-IVC
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO 07/02/2009
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Total_Ivc.compute(new ComputeParameters(false, pnd_Ws_Total_Ivc), pnd_Ws_Tiaa_Ivc.add(pnd_Ws_Cref_Ivc));                                                   //Natural: ASSIGN #WS-TOTAL-IVC := #WS-TIAA-IVC + #WS-CREF-IVC
        if (condition(pnd_Ws_Tiaa_Ivc.greater(getZero())))                                                                                                                //Natural: IF #WS-TIAA-IVC > 0
        {
            pnd_A.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #A
            pnd_Ws_Amt.getValue(pnd_A).setValueEdited(pnd_Ws_Tiaa_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                            //Natural: MOVE EDITED #WS-TIAA-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #A )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Amt.getValue(pnd_A),true), new ExamineSearch("X", true), new ExamineDelete());                                       //Natural: EXAMINE FULL #WS-AMT ( #A ) FOR FULL 'X' DELETE
            pnd_Ws_From.getValue(pnd_A).setValue("FROM");                                                                                                                 //Natural: ASSIGN #WS-FROM ( #A ) = 'FROM'
            pnd_Ws_Lit.getValue(pnd_A).setValue("TIAA");                                                                                                                  //Natural: ASSIGN #WS-LIT ( #A ) = 'TIAA'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Cref_Ivc.greater(getZero())))                                                                                                                //Natural: IF #WS-CREF-IVC > 0
        {
            pnd_A.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #A
            pnd_Ws_Amt.getValue(pnd_A).setValueEdited(pnd_Ws_Cref_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                            //Natural: MOVE EDITED #WS-CREF-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #A )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Amt.getValue(pnd_A),true), new ExamineSearch("X", true), new ExamineDelete());                                       //Natural: EXAMINE FULL #WS-AMT ( #A ) FOR FULL 'X' DELETE
            pnd_Ws_From.getValue(pnd_A).setValue("FROM");                                                                                                                 //Natural: ASSIGN #WS-FROM ( #A ) = 'FROM'
            pnd_Ws_Lit.getValue(pnd_A).setValue("CREF");                                                                                                                  //Natural: ASSIGN #WS-LIT ( #A ) = 'CREF'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"ANNUITANT STATEMENT DETAIL CONTROL REPORT",new TabSetting(120),
            "REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
        		pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());
    }
}
