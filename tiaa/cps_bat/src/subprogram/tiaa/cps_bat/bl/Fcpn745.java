/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:13 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn745
************************************************************
**        * FILE NAME            : Fcpn745.java
**        * CLASS NAME           : Fcpn745
**        * INSTANCE NAME        : Fcpn745
************************************************************
***********************************************************************
*  LEON SILBERSTEIN
***********************************************************************
* SYSTEM   : CPS
* TITLE    : ANNUITANT STATEMENT
* GENERATED: JUL 26,93 AT 10:04 AM
* FUNCTION : THIS PROGRAM WILL BUILD ANNUITANT STATEMENT.
* FCPN745
**08/12/98 GLORY PHILIP
**        - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
*
**9/6/2001 R. CARREON
**        - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
**03/14/02- USE TX-ELCT-TRGGR FOR NRA TEST
***********************************************************************
*
*
* HISTORY
*    02/27/97 ALTHEA A. YOUNG   - INFLATION LINKED BOND
*    - INITIALIZED LOGICAL INV. ACCT. SWITCHES TO <FALSE>.
*    - REVISED INV. ACCT. PROCESSING TO VERIFY FUNDS FOR TIAA COMPANY;
*      REAL ESTATE HAS UNITS AND UNIT VALUES.
*    10/03/96 LIN ZHENG         - INFLATION LINKED BOND
*
*    02/23/94 LEON SILBERSTEIN
*    REWORD TEXT FOR IVC MESSAGE AS PER USER REQUEST
* ----------------------------------------------------------------------
*  NOTES:
* ----------------------------------------------------------------------
* A "Group of Printed Lines" CONSISTS OF:
*  1. A SINGLE "A line" - "group Total" LINE
*  2. A SINGLE "Db" LINE - THE WORD "Units" OR "Contractual"
*  3. A SINGLE "Dc" LINE - THE WORD "Unit Value" OR "Dividend"
*  4. "Deduction Lines" - SPECIFYING THE DEDUCTION NOT SHOWN ON THE
*        "Units" LINE OR THE "Unit Value" LINE.
*      THERE MAY BE NONE OR UP TO 10 "Deduction Lines" DEPENDING ON THE
*      NUMBER OF NON-ZERO DEDUCTIONS SPECIFIED FOR A PAYMENT.
*      NOTE, THAT THE TAX DEDUCTIONS AND OHTER DEDUCTIONS ARE SHOWN
*      AT THE SAME LINE POSITION. THE "deduction" PRINT AREA IS FILLED
*      IN THE FOLLOWING SEQUENCE:
*      4.1. "Federal" TAX
*      4.2. "State" TAX
*      4.3. "Local" TAX
*      4.4. THE OTHER DEDUCTIONS.
*      I.E.,  IF THERE ARE NO TAXES THE FIRST "other" DEDUCTION IS
*        SHOWN IN THE "Units" LINE.
* ----------------------------------------------------------------------
* MONTHLY SETTLEMENT SETTLES FEW SETTLEMENTS, WHICH ARE PRINTED IN
* DIFFERENT FORMATS, I.E. DIFFERENT PROTOTYPES, AS FOLLOWS:
*
*      SETTLEMENT            CNTRCT-TYPE-CDE   PROTOTYPE#
*      TYPE
*  --  --------------------- ----------------- ---------------
*
*  1.  MATURITY                10                4
*  2.  TPA                     20                8
*  3.  MDO                     30                6
*  4.  RECURRING MDO           31                6
*  5.  LUMP SUM DEATH          40                6
*  6.  SURVIVOR BENEFITS       50                4
*
* ----------------------------------------------------------------------
*
* DATA ELEMENTS WHICH ARE COMMON TO ALL PAYMENTS AND "Groups Of
* PRINTED LINES", are normally kept in the HEADER-RECORD.
* HOWEVER, FEW OF THESE DATA ELEMENTS, MAY CHANGE FROM ONE PAYMENTS
* RECORD TO ANOTHER.  THEREFORE, THIS DATA IS KEPT AT THE OCCURS TABLE
* ATTACHED TO THE INV_ACCT DATA.
* ----------------------------------------------------------------------
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn745 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700a pdaFcpa700a;
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Cntrct_Prefix;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_1;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc;

    private DbsGroup pnd_Ws_Rec_1_Pnd_Ws_Columns;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column_Spacer;
    private DbsField pnd_Ws_Rec_2;

    private DbsGroup pnd_Ws_Rec_2__R_Field_2;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name;

    private DbsGroup pnd_Ws_Ivc_Area;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text;
    private DbsField pnd_Top_Of_Page;
    private DbsField pnd_Its_A_Cref_Fund;
    private DbsField pnd_Its_A_Tiaa_Fund;
    private DbsField pnd_Ws_First_Time;
    private DbsField pnd_C;
    private DbsField pnd_F;
    private DbsField pnd_1st_F;
    private DbsField pnd_Nbr_Of_Columns;
    private DbsField pnd_J;
    private DbsField pnd_L;
    private DbsField pnd_K;
    private DbsField pnd_Ws_Temp_Col;
    private DbsField pnd_Ws_Temp_Col2;
    private DbsField pnd_Ws_Temp_Deduction;
    private DbsField pnd_Nbr_Of_Remaining_Deductions;
    private DbsField pnd_Index_Of_Last_Used_Deduction;
    private DbsField pnd_Highest_Index_Of_Deduction;
    private DbsField pnd_Deduction_Column_Nbr;
    private DbsField pnd_I;
    private DbsField pnd_Ws_Fielda;
    private DbsField pnd_Ws_Fieldn;

    private DbsGroup pnd_Ws_Fieldn__R_Field_3;
    private DbsField pnd_Ws_Fieldn_Pnd_Ws_Int;
    private DbsField pnd_Ws_Fieldn_Pnd_Ws_Rem;
    private DbsField pnd_Ws_Masked_Amt;
    private DbsField pnd_Ws_Column_Offset;

    private DbsGroup pnd_Ws_Column_Offset__R_Field_4;
    private DbsField pnd_Ws_Column_Offset__Filler1;
    private DbsField pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data;

    private DbsGroup pnd_Ws_Column_Offset__R_Field_5;
    private DbsField pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data;
    private DbsField pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1;
    private DbsField pnd_Ws_Letter;
    private DbsField pnd_Ws_Legend;
    private DbsField pnd_Ws_Literal;
    private DbsField pnd_Ws_Printed;
    private DbsField pnd_Ws_Dpi_Hdr_Written;
    private DbsField pnd_Ws_Deduct_Hdr_Written;
    private DbsField pnd_Ws_Printed_Cref_Heading;
    private DbsField pnd_Ws_Printed_Tiaa_Heading;
    private DbsField pnd_Ws_Tot_Payment_Amt;
    private DbsField pnd_Ws_Tot_Dpi_Amt;
    private DbsField pnd_Ws_Tot_Ded_Amt;
    private DbsField pnd_Ws_Tot_Check_Amt;
    private DbsField pnd_Ws_Cref_Tot_End_Accum;
    private DbsField pnd_Ws_Cref_Tot_Payment;
    private DbsField pnd_Ws_Cref_Tot_Dpi;
    private DbsField pnd_Ws_Cref_Tot_Ded;
    private DbsField pnd_Ws_Cref_Tot_Check;
    private DbsField pnd_Ws_Grand_Tot_End_Accum;
    private DbsField pnd_Ws_Grand_Tot_Payment;
    private DbsField pnd_Ws_Grand_Tot_Dpi;
    private DbsField pnd_Ws_Grand_Tot_Ded;
    private DbsField pnd_Ws_Grand_Tot_Check;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700a = new PdaFcpa700a(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Side_Printed = parameters.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Side_Printed.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Cntrct_Prefix = localVariables.newFieldInRecord("pnd_Cntrct_Prefix", "#CNTRCT-PREFIX", FieldType.STRING, 2);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_1", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc = pnd_Ws_Rec_1__R_Field_1.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc", "#WS-REC1-CC", FieldType.STRING, 2);

        pnd_Ws_Rec_1_Pnd_Ws_Columns = pnd_Ws_Rec_1__R_Field_1.newGroupArrayInGroup("pnd_Ws_Rec_1_Pnd_Ws_Columns", "#WS-COLUMNS", new DbsArrayController(1, 
            7));
        pnd_Ws_Rec_1_Pnd_Ws_Column = pnd_Ws_Rec_1_Pnd_Ws_Columns.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column", "#WS-COLUMN", FieldType.STRING, 14);
        pnd_Ws_Rec_1_Pnd_Ws_Column_Spacer = pnd_Ws_Rec_1_Pnd_Ws_Columns.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column_Spacer", "#WS-COLUMN-SPACER", FieldType.STRING, 
            1);
        pnd_Ws_Rec_2 = localVariables.newFieldInRecord("pnd_Ws_Rec_2", "#WS-REC-2", FieldType.STRING, 143);

        pnd_Ws_Rec_2__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Rec_2__R_Field_2", "REDEFINE", pnd_Ws_Rec_2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc", "#WS-REC2-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler", "#WS-REC2-FILLER", FieldType.STRING, 
            35);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name", "#WS-REC2-NAME", FieldType.STRING, 38);

        pnd_Ws_Ivc_Area = localVariables.newGroupInRecord("pnd_Ws_Ivc_Area", "#WS-IVC-AREA");
        pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc", "#WS-TIAA-IVC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc", "#WS-CREF-IVC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc", "#WS-GOOD-IVC", FieldType.BOOLEAN, 1);
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text", "#WS-IVC-TIAA-TEXT", FieldType.STRING, 
            21);
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text", "#WS-IVC-CREF-TEXT", FieldType.STRING, 
            21);
        pnd_Top_Of_Page = localVariables.newFieldInRecord("pnd_Top_Of_Page", "#TOP-OF-PAGE", FieldType.BOOLEAN, 1);
        pnd_Its_A_Cref_Fund = localVariables.newFieldInRecord("pnd_Its_A_Cref_Fund", "#ITS-A-CREF-FUND", FieldType.BOOLEAN, 1);
        pnd_Its_A_Tiaa_Fund = localVariables.newFieldInRecord("pnd_Its_A_Tiaa_Fund", "#ITS-A-TIAA-FUND", FieldType.BOOLEAN, 1);
        pnd_Ws_First_Time = localVariables.newFieldInRecord("pnd_Ws_First_Time", "#WS-FIRST-TIME", FieldType.STRING, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.INTEGER, 2);
        pnd_1st_F = localVariables.newFieldInRecord("pnd_1st_F", "#1ST-F", FieldType.INTEGER, 2);
        pnd_Nbr_Of_Columns = localVariables.newFieldInRecord("pnd_Nbr_Of_Columns", "#NBR-OF-COLUMNS", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Ws_Temp_Col = localVariables.newFieldInRecord("pnd_Ws_Temp_Col", "#WS-TEMP-COL", FieldType.STRING, 14);
        pnd_Ws_Temp_Col2 = localVariables.newFieldInRecord("pnd_Ws_Temp_Col2", "#WS-TEMP-COL2", FieldType.STRING, 14);
        pnd_Ws_Temp_Deduction = localVariables.newFieldInRecord("pnd_Ws_Temp_Deduction", "#WS-TEMP-DEDUCTION", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Nbr_Of_Remaining_Deductions = localVariables.newFieldInRecord("pnd_Nbr_Of_Remaining_Deductions", "#NBR-OF-REMAINING-DEDUCTIONS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Index_Of_Last_Used_Deduction = localVariables.newFieldInRecord("pnd_Index_Of_Last_Used_Deduction", "#INDEX-OF-LAST-USED-DEDUCTION", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Highest_Index_Of_Deduction = localVariables.newFieldInRecord("pnd_Highest_Index_Of_Deduction", "#HIGHEST-INDEX-OF-DEDUCTION", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Deduction_Column_Nbr = localVariables.newFieldInRecord("pnd_Deduction_Column_Nbr", "#DEDUCTION-COLUMN-NBR", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Fielda = localVariables.newFieldInRecord("pnd_Ws_Fielda", "#WS-FIELDA", FieldType.STRING, 12);
        pnd_Ws_Fieldn = localVariables.newFieldInRecord("pnd_Ws_Fieldn", "#WS-FIELDN", FieldType.NUMERIC, 11, 2);

        pnd_Ws_Fieldn__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Fieldn__R_Field_3", "REDEFINE", pnd_Ws_Fieldn);
        pnd_Ws_Fieldn_Pnd_Ws_Int = pnd_Ws_Fieldn__R_Field_3.newFieldInGroup("pnd_Ws_Fieldn_Pnd_Ws_Int", "#WS-INT", FieldType.NUMERIC, 9);
        pnd_Ws_Fieldn_Pnd_Ws_Rem = pnd_Ws_Fieldn__R_Field_3.newFieldInGroup("pnd_Ws_Fieldn_Pnd_Ws_Rem", "#WS-REM", FieldType.STRING, 2);
        pnd_Ws_Masked_Amt = localVariables.newFieldInRecord("pnd_Ws_Masked_Amt", "#WS-MASKED-AMT", FieldType.STRING, 13);
        pnd_Ws_Column_Offset = localVariables.newFieldInRecord("pnd_Ws_Column_Offset", "#WS-COLUMN-OFFSET", FieldType.STRING, 14);

        pnd_Ws_Column_Offset__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Column_Offset__R_Field_4", "REDEFINE", pnd_Ws_Column_Offset);
        pnd_Ws_Column_Offset__Filler1 = pnd_Ws_Column_Offset__R_Field_4.newFieldInGroup("pnd_Ws_Column_Offset__Filler1", "_FILLER1", FieldType.STRING, 
            1);
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data = pnd_Ws_Column_Offset__R_Field_4.newFieldInGroup("pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data", 
            "#WS-COLUMN-OFFSET-LFT-1-DATA", FieldType.STRING, 13);

        pnd_Ws_Column_Offset__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Column_Offset__R_Field_5", "REDEFINE", pnd_Ws_Column_Offset);
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data = pnd_Ws_Column_Offset__R_Field_5.newFieldInGroup("pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data", 
            "#WS-COLUMN-OFFSET-RGT-1-DATA", FieldType.STRING, 13);
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1 = pnd_Ws_Column_Offset__R_Field_5.newFieldInGroup("pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1", 
            "#WS-COLUMN-OFFSET-RGT-POS1", FieldType.STRING, 1);
        pnd_Ws_Letter = localVariables.newFieldInRecord("pnd_Ws_Letter", "#WS-LETTER", FieldType.STRING, 1);
        pnd_Ws_Legend = localVariables.newFieldInRecord("pnd_Ws_Legend", "#WS-LEGEND", FieldType.STRING, 4);
        pnd_Ws_Literal = localVariables.newFieldInRecord("pnd_Ws_Literal", "#WS-LITERAL", FieldType.STRING, 12);
        pnd_Ws_Printed = localVariables.newFieldInRecord("pnd_Ws_Printed", "#WS-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Dpi_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Dpi_Hdr_Written", "#WS-DPI-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Deduct_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Deduct_Hdr_Written", "#WS-DEDUCT-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Printed_Cref_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Cref_Heading", "#WS-PRINTED-CREF-HEADING", FieldType.STRING, 1);
        pnd_Ws_Printed_Tiaa_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Tiaa_Heading", "#WS-PRINTED-TIAA-HEADING", FieldType.STRING, 1);
        pnd_Ws_Tot_Payment_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Payment_Amt", "#WS-TOT-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Dpi_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Dpi_Amt", "#WS-TOT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Ded_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Ded_Amt", "#WS-TOT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Check_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Check_Amt", "#WS-TOT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_End_Accum", "#WS-CREF-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Ws_Cref_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Payment", "#WS-CREF-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Dpi", "#WS-CREF-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Ded", "#WS-CREF-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Check", "#WS-CREF-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_End_Accum", "#WS-GRAND-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Grand_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Payment", "#WS-GRAND-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Dpi", "#WS-GRAND-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Ded", "#WS-GRAND-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Check", "#WS-GRAND-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Its_A_Cref_Fund.setInitialValue(false);
        pnd_Its_A_Tiaa_Fund.setInitialValue(false);
        pnd_Ws_First_Time.setInitialValue("Y");
        pnd_J.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_Highest_Index_Of_Deduction.setInitialValue(13);
        pnd_Ws_Printed.setInitialValue("N");
        pnd_Ws_Dpi_Hdr_Written.setInitialValue("N");
        pnd_Ws_Deduct_Hdr_Written.setInitialValue("N");
        pnd_Ws_Printed_Cref_Heading.setInitialValue("N");
        pnd_Ws_Printed_Tiaa_Heading.setInitialValue("N");
        pnd_Ws_Tot_Payment_Amt.setInitialValue(0);
        pnd_Ws_Tot_Dpi_Amt.setInitialValue(0);
        pnd_Ws_Tot_Ded_Amt.setInitialValue(0);
        pnd_Ws_Tot_Check_Amt.setInitialValue(0);
        pnd_Ws_Cref_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Cref_Tot_Payment.setInitialValue(0);
        pnd_Ws_Cref_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Cref_Tot_Ded.setInitialValue(0);
        pnd_Ws_Cref_Tot_Check.setInitialValue(0);
        pnd_Ws_Grand_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Grand_Tot_Payment.setInitialValue(0);
        pnd_Ws_Grand_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Grand_Tot_Ded.setInitialValue(0);
        pnd_Ws_Grand_Tot_Check.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn745() throws Exception
    {
        super("Fcpn745");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        //*  ----------------------------------------------------------------------
        //*  INSPECT THE DATA TO BE PRINTED AND DETERMINE THE HORIZONTAL
        //*  FORMAT OF THE PRINT. I.E., HOW MANY COLUMNS ARE THERE GOING TO
        //*  BE ON THE PAGE.
        //*  ----------------------------------------------------------------------
        //*  ...................... SETUP MAX NUMBER OF COLUMNS
        //*  MATURITY, PROTOTYPE #4
        //*  SURVIVOR BENEFITS, PROTOTYPE #4
        //*   TPA ... PROTOTYPE#9
        //*   MDO,  ... PROTOTYPE#8
        //*   RECURRING MDO, ... PROTOTYPE#6
        //*   LUMP SUM ... PROTOTYPE#6
        short decideConditionsMet390 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE '10'
        if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("10"))))
        {
            decideConditionsMet390++;
            pnd_Nbr_Of_Columns.setValue(7);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 7
        }                                                                                                                                                                 //Natural: VALUE '50'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("50"))))
        {
            decideConditionsMet390++;
            pnd_Nbr_Of_Columns.setValue(7);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 7
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("20"))))
        {
            decideConditionsMet390++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: VALUE '30'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30"))))
        {
            decideConditionsMet390++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: VALUE '31'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("31"))))
        {
            decideConditionsMet390++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: VALUE '40'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("40"))))
        {
            decideConditionsMet390++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E02 - Unexpected CNTRCT-TYPE-CDE","=",pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde());                      //Natural: WRITE *PROGRAM 'E02 - Unexpected CNTRCT-TYPE-CDE' '=' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  .................... ADJUST NUMBER OF COLUMNS BASED ON DATA
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero())))                                           //Natural: IF INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0
        {
            pnd_Ws_Dpi_Hdr_Written.setValue("Y");                                                                                                                         //Natural: ASSIGN #WS-DPI-HDR-WRITTEN := 'Y'
            //*  COMPRESS OUT THIS COULUMN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Nbr_Of_Columns.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #NBR-OF-COLUMNS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero()) || pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero())  //Natural: IF INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0 OR INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0 OR INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0 OR PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) > 0
            || pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero()) || pdaFcpa700a.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(1,
            ":",pnd_Ws_Cntr_Inv_Acct,"*").greater(getZero())))
        {
            pnd_Ws_Deduct_Hdr_Written.setValue("Y");                                                                                                                      //Natural: ASSIGN #WS-DEDUCT-HDR-WRITTEN := 'Y'
            //*  COMPRESS OUT THIS COULUMN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Nbr_Of_Columns.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #NBR-OF-COLUMNS
        }                                                                                                                                                                 //Natural: END-IF
        //*  ... DETERMINE THE "left margin" OF THE "1st column"
        //*      AS THE COLUMN LEFT TO THE 1ST ONE TO BE PRINTED. SO THAT
        //*      IF THE "1st column" TO BE PRINTED IS 1 SET THE MARGIN TO 0
        //*      IF THE "1st column" IS 3 SET THE MARGIN TO 2, ETC.
        //*  1
        //*  2
        //*  THIS SHOULD NEVER HAPPEN
        short decideConditionsMet430 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NBR-OF-COLUMNS;//Natural: VALUE 7, 6
        if (condition((pnd_Nbr_Of_Columns.equals(7) || pnd_Nbr_Of_Columns.equals(6))))
        {
            decideConditionsMet430++;
            pnd_1st_F.setValue(0);                                                                                                                                        //Natural: ASSIGN #1ST-F := 0
        }                                                                                                                                                                 //Natural: VALUE 5, 4
        else if (condition((pnd_Nbr_Of_Columns.equals(5) || pnd_Nbr_Of_Columns.equals(4))))
        {
            decideConditionsMet430++;
            pnd_1st_F.setValue(0);                                                                                                                                        //Natural: ASSIGN #1ST-F := 0
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Nbr_Of_Columns.equals(3))))
        {
            decideConditionsMet430++;
            pnd_1st_F.setValue(0);                                                                                                                                        //Natural: ASSIGN #1ST-F := 0
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E03 - Unexpected","=",pnd_Nbr_Of_Columns);                                                                         //Natural: WRITE *PROGRAM 'E03 - Unexpected' '=' #NBR-OF-COLUMNS
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  .. THE FIRST "Group of Lines" ON THE PAGE DOES NOT NEED A SPACE LINE
        //*  .. TO SEPARATE IT FROM THE PREVIOUS "Group of Lines".
        pnd_Top_Of_Page.setValue(true);                                                                                                                                   //Natural: ASSIGN #TOP-OF-PAGE := TRUE
        //*  .......... PROCESS ONLY "Groups of Lines" FOR CURRENT PAGE
        FOR01:                                                                                                                                                            //Natural: FOR #J = 1 TO #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).notEquals(pnd_Ws_Side_Printed)))                                                     //Natural: IF #WS-SIDE ( #J ) NE #WS-SIDE-PRINTED
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ENTRY
                sub_Process_Entry();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Top_Of_Page.setValue(false);                                                                                                                          //Natural: ASSIGN #TOP-OF-PAGE := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  .... CHECK THE IVC SITUATION:
        //*       1. ON PAGE 1 FOR FOOTNOTE PURPOSE
        //*       2. ON LAST PAGE FOR "footnoting" THE GRAND TOTAL
        if (condition(pnd_Ws_Side_Printed.equals(pdaFcpa700a.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex()) || pnd_Ws_Side_Printed.equals("1")))              //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX OR #WS-SIDE-PRINTED = '1'
        {
                                                                                                                                                                          //Natural: PERFORM TEST-FOR-GOOD-IVC
            sub_Test_For_Good_Ivc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... IT NOT LAST PAGE OF DOCUMENT THEN: "Next Page(s)"
        if (condition(pnd_Ws_Side_Printed.less(pdaFcpa700a.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                   //Natural: IF #WS-SIDE-PRINTED LT #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Rec_1.setValue("06This statement is continued on the next page");                                                                                      //Natural: ASSIGN #WS-REC-1 := '06This statement is continued on the next page'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  ON LAST PAGE SHOW THE GRAND TOTALS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DO-GRAND-TOTALS
            sub_Do_Grand_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... ON PAGE 1 PUT "footnote" MESSAGES (IF THERE ARE ANY)
        if (condition(pnd_Ws_Side_Printed.equals("1")))                                                                                                                   //Natural: IF #WS-SIDE-PRINTED = '1'
        {
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                                                                                                                                                                          //Natural: PERFORM SHOW-IVC-FOOTNOTE
                sub_Show_Ivc_Footnote();
                if (condition(Global.isEscape())) {return;}
                //*  EMPTY LINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue("17");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = '17'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *************************
        //*   S U B R O U T I N E S *
        //* *************************
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DO-GRAND-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ENTRY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG-PT4
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-A-LINE-PT4
        //*  ..... SETUP START COLUMN INDEX (#F)
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DB-LINE-PT4
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DC-LINE-PT4
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DX-LINE-PT4
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-E-LINE-PT4
        //*  .... NOW MOVE THE TOTAL TO THE PRINT LINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG-PT8
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-A-LINE-PT8
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DB-LINE-PT8
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //*  INSERTED 09-23-95 BY FRANK
        //*  TESTED FOR CNTRCT-TYPE-CDE OF 40 (LUMP-SUM DEATH)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DC-LINE-PT8
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DX-LINE-PT8
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-E-LINE-PT8
        //*  .... NOW MOVE THE TOTAL TO THE PRINT LINE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-AND-EDIT-NEXT-DEDUCTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-THE-DEDUCTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-FEDERAL-LEGEND
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEGEND
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-A-LINE-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-FOR-GOOD-IVC
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SHOW-IVC-FOOTNOTE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG-PT9
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-A-LINE-PT9
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DB-LINE-PT9
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DC-LINE-PT9
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //*  ZIL - 9/6/95 - TIAA: TRADITIONAL STANDARD, TRADITIONAL GRADED
        //*  ............ 2ND COLUMN
        //*  ENTER THE INVESTMENT ACCOUNT'S DESCRIPTION (USE FCPN199A'S DATA)
        //*  FOR TIAA USE THE CONTRACT PREFIX TO DETERMINE: GRADED/STANDARD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DX-LINE-PT9
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-E-LINE-PT9
        //* ******
        //*  .... NOW MOVE THE TOTAL TO THE PRINT LINE
    }
    private void sub_Do_Grand_Totals() throws Exception                                                                                                                   //Natural: DO-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  MATURITY,          PROTOTYPE #4
        short decideConditionsMet599 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE '10','50'
        if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("10") || pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("50"))))
        {
            decideConditionsMet599++;
            //*   TPA                ... PROTOTYPE#9
                                                                                                                                                                          //Natural: PERFORM BLD-E-LINE-PT4
            sub_Bld_E_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("20"))))
        {
            decideConditionsMet599++;
            //*   MDO,               ... PROTOTYPE#8
                                                                                                                                                                          //Natural: PERFORM BLD-E-LINE-PT9
            sub_Bld_E_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '30','31','40'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30") || pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("31") 
            || pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("40"))))
        {
            decideConditionsMet599++;
                                                                                                                                                                          //Natural: PERFORM BLD-E-LINE-PT8
            sub_Bld_E_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E02 - Unexpected CNTRCT-TYPE-CDE","=",pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde());                      //Natural: WRITE *PROGRAM 'E02 - Unexpected CNTRCT-TYPE-CDE' '=' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DO-GRAND-TOTALS
    }
    private void sub_Process_Entry() throws Exception                                                                                                                     //Natural: PROCESS-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  PROCESS AN ENTRY FROM THE TABLE
        //*  #J IS USED AS THE INDEX TO THE TABLE ENTRY
        //*  ----------------------------------------------------------------------
        //*  ........ FIRST LINE ON "Next Page"
        if (condition(((pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).equals("2") || pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).equals("3")) //Natural: IF #WS-SIDE ( #J ) = '2' OR = '3' AND #WS-PRINTED = 'N'
            && pnd_Ws_Printed.equals("N"))))
        {
            pnd_Ws_Printed.setValue("Y");                                                                                                                                 //Natural: ASSIGN #WS-PRINTED = 'Y'
            pnd_Ws_Rec_1.setValue("15CONTINUED FROM PREVIOUS PAGE");                                                                                                      //Natural: ASSIGN #WS-REC-1 = '15CONTINUED FROM PREVIOUS PAGE'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  LZ
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J));                                           //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = INV-ACCT-CDE ( #J )
        //*  LZ
        DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                        //Natural: CALLNAT 'FCPN199A' #FUND-PDA
        if (condition(Global.isEscape())) return;
        //*  ...... INDICATE WHETHER ITS A TIAA OR CREF INVESTMENT ACCOUNT
        pnd_Its_A_Cref_Fund.reset();                                                                                                                                      //Natural: RESET #ITS-A-CREF-FUND #ITS-A-TIAA-FUND
        pnd_Its_A_Tiaa_Fund.reset();
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            pnd_Its_A_Tiaa_Fund.setValue(true);                                                                                                                           //Natural: ASSIGN #ITS-A-TIAA-FUND := TRUE
            //*  02-27-97 : A. YOUNG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Its_A_Cref_Fund.setValue(true);                                                                                                                           //Natural: ASSIGN #ITS-A-CREF-FUND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ...... RESET POINTER OF LAST USED DEDUCTION
        pnd_Index_Of_Last_Used_Deduction.reset();                                                                                                                         //Natural: RESET #INDEX-OF-LAST-USED-DEDUCTION
        //*  ...... CALC THE NUMBER OF DEDUCTIONS
        pnd_Nbr_Of_Remaining_Deductions.reset();                                                                                                                          //Natural: RESET #NBR-OF-REMAINING-DEDUCTIONS
        short decideConditionsMet643 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN INV-ACCT-FDRL-TAX-AMT ( #J ) > 0
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet643++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN INV-ACCT-STATE-TAX-AMT ( #J ) > 0
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet643++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet643++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN #WS-OCCURS.#CNTR-DEDUCTIONS ( #J ) GT 0
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet643++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_J));                                                     //Natural: ADD #WS-OCCURS.#CNTR-DEDUCTIONS ( #J ) TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet643 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  MATURITY,          PROTOTYPE #4
        short decideConditionsMet657 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE '10','50'
        if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("10") || pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("50"))))
        {
            decideConditionsMet657++;
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG-PT4
            sub_Bld_Col_Hdng_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-A-LINE-PT4
            sub_Bld_A_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DB-LINE-PT4
            sub_Bld_Db_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DC-LINE-PT4
            sub_Bld_Dc_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
            //*   TPA                ... PROTOTYPE#9
                                                                                                                                                                          //Natural: PERFORM BLD-DX-LINE-PT4
            sub_Bld_Dx_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("20"))))
        {
            decideConditionsMet657++;
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG-PT9
            sub_Bld_Col_Hdng_Pt9();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-A-LINE-PT9
            sub_Bld_A_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DB-LINE-PT9
            sub_Bld_Db_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DC-LINE-PT9
            sub_Bld_Dc_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
            //*   MDO,               ... PROTOTYPE#8
                                                                                                                                                                          //Natural: PERFORM BLD-DX-LINE-PT9
            sub_Bld_Dx_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '30','31','40'
        else if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30") || pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("31") 
            || pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("40"))))
        {
            decideConditionsMet657++;
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG-PT8
            sub_Bld_Col_Hdng_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-A-LINE-PT8
            sub_Bld_A_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DB-LINE-PT8
            sub_Bld_Db_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DC-LINE-PT8
            sub_Bld_Dc_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DX-LINE-PT8
            sub_Bld_Dx_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E01 - Unexpected CNTRCT-TYPE-CDE","=",pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde());                      //Natural: WRITE *PROGRAM 'E01 - Unexpected CNTRCT-TYPE-CDE' '=' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PROCESS-ENTRY
    }
    private void sub_Bld_Col_Hdng_Pt4() throws Exception                                                                                                                  //Natural: BLD-COL-HDNG-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*   BUILD COLUMN HEADING LINES AS PER THE DATA TO BE PRINTED.
        //*   THERE ARE SOME COLUMNS THAT ARE ONLY CONDITIONALLY PRINTED. IN CASE
        //*   THAT A CERTAIN DATA, FOR EXAMPLE DELAYED PAYMENT INTEREST, IS NOT
        //*   PRESENT FOR THIS PRINTOUT (ON EIHTER OF ITS PAGES!), THEN IT IS NOT
        //*   PRINTED AND THE NEXT PRINTED COLUMN IS SHIFTED LEFT, SO THAT THERE
        //*   ARE NO BLANK COLUMNS.
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                                     //Natural: IF #WS-FIRST-TIME = 'Y'
        {
            pnd_Ws_First_Time.setValue("N");                                                                                                                              //Natural: ASSIGN #WS-FIRST-TIME = 'N'
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG
            sub_Bld_Col_Hdng();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("15");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG-PT4
    }
    private void sub_Bld_A_Line_Pt4() throws Exception                                                                                                                    //Natural: BLD-A-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*    "A Line": TOTALS FOR THE "group of Lines"
        //*  ----------------------------------------------------------------------
        //*   ...... ACCUM THE TOTALS FOR THE "group of Lines"
                                                                                                                                                                          //Natural: PERFORM SETUP-A-LINE-TOTALS
        sub_Setup_A_Line_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '05'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ............ COLUMN 1 - "Due"
        //*  EMPTY ON THIS LINE
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ............ COLUMN 2
        //*  DO NOT ENTER ANYTHING FOR "RTB" PAYMENT, OTHERWISE
        //*  ENTER CNTRCT-PPCN-NBR
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind().getValue(pnd_J).equals("N") && pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind().getValue(pnd_J).equals("X"))) //Natural: IF #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #J ) = 'N' AND #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #J ) = 'X'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr().getValue(pnd_J),new ReportEditMask("XXXXXXX-X"));                               //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(pnd_Ws_Temp_Col));                                                                       //Natural: COMPRESS #WS-TEMP-COL INTO #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 3
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                    //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                                       //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                        //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
        //* ***MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN (#F)
        //*  ............ COLUMN 4
        //*  FOR "RTB" PAYMENT ENTER "RTB" OTHERWISE SPACES
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind().getValue(pnd_J).equals("N") && pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind().getValue(pnd_J).equals("X"))) //Natural: IF #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #J ) = 'N' AND #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #J ) = 'X'
        {
            pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("RTB");                                                                                         //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'RTB'
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                    //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 5 - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED INV-ACCT-DPI-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
            //* *MOVE EDITED INV-ACCT-DPI-AMT (#J) (EM=X'$'Z,ZZZ,ZZ9.99'*')
            //* *  TO #WS-COLUMN (#F)
            //* *EXAMINE FULL #WS-COLUMN (#F) FOR FULL 'X' DELETE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 6 - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Deduction_Column_Nbr.setValue(pnd_F);                                                                                                                     //Natural: ASSIGN #DEDUCTION-COLUMN-NBR := #F
            if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                         //Natural: IF #WS-TOT-DED-AMT > 0
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZ,ZZ9.99"));                                                                   //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                                  //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-A-LINE-PT4
    }
    private void sub_Bld_Db_Line_Pt4() throws Exception                                                                                                                   //Natural: BLD-DB-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*   BUILD THE  "Db Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ........ 1ST COLUMN
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Due");                                                                                                       //Natural: ASSIGN #WS-COLUMN ( #F ) := 'Due'
        //*  ......... 2ND COLUMN
        //*  ENTER THE INVESTMENT ACCOUNT'S DESCRIPTION (USE FCPN199A'S DATA)
        //*  INSERTED 09-23-95 BY FRANK
        //*  TESTED FOR RTB
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-1
        //*  ............3RD COLUMN
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //*   LEAVE SPACE FOR RTB PAYMENTS
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind().getValue(pnd_J).equals("N") && pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind().getValue(pnd_J).equals("X"))) //Natural: IF #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #J ) = 'N' AND #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #J ) = 'X'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  02-27-97 : A. YOUNG
            if (condition(pnd_Its_A_Cref_Fund.getBoolean()))                                                                                                              //Natural: IF #ITS-A-CREF-FUND
            {
                pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Unit_Qty().getValue(pnd_J),new ReportEditMask("ZZZ,ZZ9.999"));                       //Natural: MOVE EDITED INV-ACCT-UNIT-QTY ( #J ) ( EM = ZZZ,ZZ9.999 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
                //*  ITS TIAA PAYMENT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 4
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //*   LEAVE SPACE FOR RTB PAYMENTS
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind().getValue(pnd_J).equals("N") && pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind().getValue(pnd_J).equals("X"))) //Natural: IF #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #J ) = 'N' AND #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #J ) = 'X'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  02-27-97 : A. YOUNG
            if (condition(pnd_Its_A_Cref_Fund.getBoolean()))                                                                                                              //Natural: IF #ITS-A-CREF-FUND
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Units");                                                                                   //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Units'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
                //*  TIAA PAYMENT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Contractual");                                                                             //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Contractual'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 5 - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 6 - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DB-LINE-PT4
    }
    private void sub_Bld_Dc_Line_Pt4() throws Exception                                                                                                                   //Natural: BLD-DC-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dc Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ........ 1ST COLUMN - "Due" DATE
        //*  ENTER THE SETTLEMENT DATE FOR THIS SPECIFIC PAYMENT
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6().getValue(pnd_J).greater(getZero())))                                                 //Natural: IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 ( #J ) GT 0
        {
            pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte().getValue(pnd_J),new ReportEditMask("MM/DD/YY"));                   //Natural: MOVE EDITED #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE ( #J ) ( EM = MM/DD/YY ) TO #WS-TEMP-COL
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "field should have a valid date:","=",pnd_J,"=",pdaFcpa700a.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4().getValue(pnd_J),         //Natural: WRITE 'field should have a valid date:' '='#J '=' #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-A4 ( #J ) ( EM = H ( 8 ) )
                new ReportEditMask ("HHHHHHHH"));
            if (Global.isEscape()) return;
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-2
        //*  ............ 3RD COLUMN
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //*   LEAVE SPACE FOR RTB PAYMENTS
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind().getValue(pnd_J).equals("N") && pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind().getValue(pnd_J).equals("X"))) //Natural: IF #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #J ) = 'N' AND #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #J ) = 'X'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  02-27-97 : A. YOUNG
            if (condition(pnd_Its_A_Cref_Fund.getBoolean()))                                                                                                              //Natural: IF #ITS-A-CREF-FUND
            {
                pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Unit_Value().getValue(pnd_J),new ReportEditMask("ZZZ,ZZ9.9999"));                    //Natural: MOVE EDITED INV-ACCT-UNIT-VALUE ( #J ) ( EM = ZZZ,ZZ9.9999 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
                //*  ITS TIAA PAYMENT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED INV-ACCT-DVDND-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ 4TH COLUMN
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //*   LEAVE SPACE FOR RTB PAYMENTS
        if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind().getValue(pnd_J).equals("N") && pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind().getValue(pnd_J).equals("X"))) //Natural: IF #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #J ) = 'N' AND #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #J ) = 'X'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  02-27-97 : A. YOUNG
            if (condition(pnd_Its_A_Cref_Fund.getBoolean()))                                                                                                              //Natural: IF #ITS-A-CREF-FUND
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Unit Value");                                                                              //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Unit Value'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
                //*  TIAA PAYMENT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Dividend");                                                                                //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Dividend'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ 5TH COLUMN - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 6 - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DC-LINE-PT4
    }
    private void sub_Bld_Dx_Line_Pt4() throws Exception                                                                                                                   //Natural: BLD-DX-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dx Line": ADDITIONAL DEDUCTIONS, AS NEEDED
        //*  ----------------------------------------------------------------------
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(!(pnd_Nbr_Of_Remaining_Deductions.greater(getZero())))) {break;}                                                                                //Natural: WHILE #NBR-OF-REMAINING-DEDUCTIONS GT 0
            pnd_Ws_Rec_1.reset();                                                                                                                                         //Natural: RESET #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 5");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '+3'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_Deduction_Column_Nbr).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #DEDUCTION-COLUMN-NBR )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  BLD-DX-LINE-PT4
    }
    private void sub_Bld_E_Line_Pt4() throws Exception                                                                                                                    //Natural: BLD-E-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD "Grand Totals" LINE
        //*  ----------------------------------------------------------------------
        //*  ............ACCUM "Grand Totals"
        if (condition(pnd_Ws_Side_Printed.equals(pdaFcpa700a.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                 //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_Payment.reset();                                                                                                                             //Natural: RESET #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Dpi.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            pnd_Ws_Grand_Tot_Payment.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-SETTL-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Payment.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-DVDND-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Dpi.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                              //Natural: ADD INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DPI
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                         //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct,"*"));                                             //Natural: ADD PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-TOT-CHECK-AMT
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC := '06'
            pnd_F.setValue(pnd_1st_F);                                                                                                                                    //Natural: ASSIGN #F := #1ST-F
            //*  ...... 1ST COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
            //*  ..... 2ND COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
            //*  ......... EDITE / BUILD THE GRAND TOTAL LINE
            //*  ..... 3RD COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
            //*  .... IF THERE IS IVC MONEY "footnote" THE GRAND TOTAL
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.setValue("#");                                                                                         //Natural: ASSIGN #WS-COLUMN-OFFSET-RGT-POS1 := '#'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                    //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            //*  ..... 4TH COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
            //*  ..... 5TH COLUMN
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... 6TH COLUMN
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99"));                                                                //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                  //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-E-LINE-PT4
    }
    private void sub_Bld_Col_Hdng_Pt8() throws Exception                                                                                                                  //Natural: BLD-COL-HDNG-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*   BUILD COLUMN HEADING LINES AS PER THE DATA TO BE PRINTED.
        //*   THERE ARE SOME COLUMNS THAT ARE ONLY CONDITIONALLY PRINTED. IN CASE
        //*   THAT A CERTAIN DATA, FOR EXAMPLE DELAYED PAYMENT INTEREST, IS NOT
        //*   PRESENT FOR THIS PRINTOUT (ON EIHTER OF ITS PAGES!), THEN IT IS NOT
        //*   PRINTED AND THE NEXT PRINTED COLUMN IS SHIFTED LEFT, SO THAT THERE
        //*   ARE NO BLANK COLUMNS.
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                                     //Natural: IF #WS-FIRST-TIME = 'Y'
        {
            pnd_Ws_First_Time.setValue("N");                                                                                                                              //Natural: ASSIGN #WS-FIRST-TIME = 'N'
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG
            sub_Bld_Col_Hdng();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("15");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG-PT8
    }
    private void sub_Bld_A_Line_Pt8() throws Exception                                                                                                                    //Natural: BLD-A-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*    "A Line": TOTALS FOR THE "group of Lines"
        //*  ----------------------------------------------------------------------
        //*   ...... ACCUM THE TOTALS FOR THE "group of Lines"
                                                                                                                                                                          //Natural: PERFORM SETUP-A-LINE-TOTALS
        sub_Setup_A_Line_Totals();
        if (condition(Global.isEscape())) {return;}
        //*   ...... NOW BUILD THE "A Line"
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '05'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN - PPCN - ALWAYS
        //*  ENTER CNTRCT-PPCN-NBR
        //*  FOR MULTIPLE CREF PRODUCTS, ONLY ENTER THE PPCN FOR
        //*  THE 1ST OCCURENCE
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //* * INSERTED 10-18-95 BY MIKE/FRANK/TIM
        //* * ADDED 'R'
        //*  LUMP SUM
        //*  NOT TIAA
        if (condition((pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("40") && ! (((pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J).equals("T") //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '40' AND NOT ( INV-ACCT-CDE ( #J ) = 'T' OR = 'G' OR = 'R' )
            || pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J).equals("G")) || pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J).equals("R"))))))
        {
            pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Cref_Nbr(),new ReportEditMask("XXXXXXX-X"));                                        //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-CREF-NBR ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr().getValue(pnd_J),new ReportEditMask("XXXXXXX-X"));                               //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(pnd_Ws_Temp_Col));                                                                           //Natural: COMPRESS #WS-TEMP-COL INTO #WS-COLUMN ( #F )
        //*  .........2ND COLUMN - TOTAL PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                    //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                                       //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                        //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
        //*  .........3RD COLUMN - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED INV-ACCT-DPI-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  .........4TH COLUMN - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Deduction_Column_Nbr.setValue(pnd_F);                                                                                                                     //Natural: ASSIGN #DEDUCTION-COLUMN-NBR := #F
            if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                         //Natural: IF #WS-TOT-DED-AMT > 0
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZ,ZZ9.99"));                                                                   //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ........ 5TH COLUMN - NET PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                                  //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-A-LINE-PT8
    }
    private void sub_Bld_Db_Line_Pt8() throws Exception                                                                                                                   //Natural: BLD-DB-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*   BUILD THE  "Db Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN
        //*  ENTER THE INVESTMENT ACCOUNT'S DESCRIPTION (USE FCPN199A'S DATA)
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-1
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ............ COLUMN 3 - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 4 - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DB-LINE-PT8
    }
    private void sub_Bld_Dc_Line_Pt8() throws Exception                                                                                                                   //Natural: BLD-DC-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dc Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ........ 1ST COLUMN - SETTLEMENT DATE
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-2
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ......... 3RD COLUMN - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ 4TH COLUMN - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 5 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DC-LINE-PT8
    }
    private void sub_Bld_Dx_Line_Pt8() throws Exception                                                                                                                   //Natural: BLD-DX-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dx Line": LOCAL TAX DEDUCTIONS, IF NEEDED
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Nbr_Of_Remaining_Deductions.greater(getZero())))                                                                                                //Natural: IF #NBR-OF-REMAINING-DEDUCTIONS GT 0
        {
            pnd_Ws_Rec_1.reset();                                                                                                                                         //Natural: RESET #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 5");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '+3'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_Deduction_Column_Nbr).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #DEDUCTION-COLUMN-NBR )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-DX-LINE-PT8
    }
    private void sub_Bld_E_Line_Pt8() throws Exception                                                                                                                    //Natural: BLD-E-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD "Grand Totals" LINE
        //*  ----------------------------------------------------------------------
        //*  ............ACCUM "Grand Totals"
        if (condition(pnd_Ws_Side_Printed.equals(pdaFcpa700a.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                 //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_Payment.reset();                                                                                                                             //Natural: RESET #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Dpi.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            pnd_Ws_Grand_Tot_Payment.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-SETTL-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Payment.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-DVDND-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Dpi.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                              //Natural: ADD INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DPI
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                         //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct,"*"));                                             //Natural: ADD PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-TOT-CHECK-AMT
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC := '06'
            pnd_F.setValue(pnd_1st_F);                                                                                                                                    //Natural: ASSIGN #F := #1ST-F
            //*  ..... 1ST COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
            //*  ......... EDITE / BUILD THE GRAND TOTAL LINE
            //*  ..... 2ND COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
            //*  .... IF THERE IS IVC MONEY "footnote" THE GRAND TOTAL
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.setValue("#");                                                                                         //Natural: ASSIGN #WS-COLUMN-OFFSET-RGT-POS1 := '#'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                    //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            //*  ..... 3RD COLUMN
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... 4TH COLUMN
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99"));                                                                //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###***
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                  //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-E-LINE-PT8
    }
    private void sub_Obtain_And_Edit_Next_Deduction() throws Exception                                                                                                    //Natural: OBTAIN-AND-EDIT-NEXT-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  THE DEDUCTION ARE PRINTED IN THE FOLLOWING ORDER:
        //*   1.    FEDERAL TAX - LEGEND "(F)"
        //*   2.    STATE   TAX - LEGEND "(S)"
        //*   3.    LOCAL   TAX - LEGEND "(L)"
        //*   4-13  ADDITIONAL DEDUCTION
        //*         IN THE SEQUENCE THAT THEY ARE LISTED IN THE DEDUCTION GROUP
        //*   NOTE: ONLY NON-ZERO DEDUCTIONS ARE PRINTED, SO USE THE FIRST
        //*         AVAILABLE NON-ZERO DEDUCTION
        //*    THE OCCURRENCE OF THE LAST USED DEDUCTION IS KEPT
        //*    IN #INDEX-OF-LAST-USED-DEDUCTION
        //*    FIELD #NBR-OF-REMAINING-DEDUCTIONS CONTAINS THE NUMBER OF DEDUCTIONS
        //*    STILL REMAIN TO BE PRINTED.
        //*  ----------------------------------------------------------------------
        pnd_Ws_Temp_Col.reset();                                                                                                                                          //Natural: RESET #WS-TEMP-COL #WS-LEGEND
        pnd_Ws_Legend.reset();
        if (condition(pnd_Nbr_Of_Remaining_Deductions.lessOrEqual(getZero())))                                                                                            //Natural: IF #NBR-OF-REMAINING-DEDUCTIONS LE 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                         //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        //*  INSPECT ALSO FEDERAL TAX
        if (condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(1)))                                                                                                   //Natural: IF #INDEX-OF-LAST-USED-DEDUCTION LE 1
        {
            //*  USE FEDERAL TAX
            if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                       //Natural: IF INV-ACCT-FDRL-TAX-AMT ( #J ) > 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J));                                                     //Natural: ASSIGN #WS-TEMP-DEDUCTION := INV-ACCT-FDRL-TAX-AMT ( #J )
                                                                                                                                                                          //Natural: PERFORM FORMAT-FEDERAL-LEGEND
                sub_Format_Federal_Legend();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                     //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  INSPECT ALSO STATE TAX
        if (condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(2)))                                                                                                   //Natural: IF #INDEX-OF-LAST-USED-DEDUCTION LE 2
        {
            //*  USE STATE TAX
            if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                      //Natural: IF INV-ACCT-STATE-TAX-AMT ( #J ) > 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J));                                                    //Natural: ASSIGN #WS-TEMP-DEDUCTION := INV-ACCT-STATE-TAX-AMT ( #J )
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEGEND
                sub_Format_State_Legend();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                     //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  INSPECT ALSO LOCAL TAX
        if (condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(3)))                                                                                                   //Natural: IF #INDEX-OF-LAST-USED-DEDUCTION LE 3
        {
            //*  USE LOACL TAX
            if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                      //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J));                                                    //Natural: ASSIGN #WS-TEMP-DEDUCTION := INV-ACCT-LOCAL-TAX-AMT ( #J )
                pnd_Ws_Legend.setValue("(L)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND = '(L)'
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                     //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #INDEX-OF-LAST-USED-DEDUCTION FROM #INDEX-OF-LAST-USED-DEDUCTION TO #HIGHEST-INDEX-OF-DEDUCTION
        for (pnd_Index_Of_Last_Used_Deduction.setValue(pnd_Index_Of_Last_Used_Deduction); condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(pnd_Highest_Index_Of_Deduction)); 
            pnd_Index_Of_Last_Used_Deduction.nadd(1))
        {
            pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Index_Of_Last_Used_Deduction.subtract(3));                                                             //Natural: ASSIGN #I := #INDEX-OF-LAST-USED-DEDUCTION - 3
            if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,pnd_I).notEquals(getZero())))                                                       //Natural: IF #WS-OCCURS.PYMNT-DED-AMT ( #J,#I ) NE 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pdaFcpa700a.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,pnd_I));                                                       //Natural: ASSIGN #WS-TEMP-DEDUCTION := #WS-OCCURS.PYMNT-DED-AMT ( #J,#I )
                pnd_Ws_Legend.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_I, ")"));                                                                 //Natural: COMPRESS '(' #I ')' INTO #WS-LEGEND LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ..... IF GOT HERE - THERE IS A PROGRAM BUG IN THE DEDUCTION LOGIC
        //*  OBTAIN-AND-EDIT-NEXT-DEDUCTION
    }
    private void sub_Edit_The_Deduction() throws Exception                                                                                                                //Natural: EDIT-THE-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Ws_Temp_Col2.setValueEdited(pnd_Ws_Temp_Deduction,new ReportEditMask("ZZZ,ZZ9.99"));                                                                          //Natural: MOVE EDITED #WS-TEMP-DEDUCTION ( EM = ZZZ,ZZ9.99 ) TO #WS-TEMP-COL2
        pnd_Ws_Temp_Col2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col2, pnd_Ws_Legend));                                                      //Natural: COMPRESS #WS-TEMP-COL2 #WS-LEGEND INTO #WS-TEMP-COL2 LEAVING NO SPACE
        pnd_Ws_Temp_Col.setValue(pnd_Ws_Temp_Col2, MoveOption.RightJustified);                                                                                            //Natural: MOVE RIGHT #WS-TEMP-COL2 TO #WS-TEMP-COL
        //*  EDIT-THE-DEDUCTION
    }
    private void sub_Format_Federal_Legend() throws Exception                                                                                                             //Natural: FORMAT-FEDERAL-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  DETERMINE THE LEGEND PRINTED NEXT TO THE FEDERAL TAX DEDUCTION
        //*  ----------------------------------------------------------------------
        //*  ROXAN 09/06/2001
        //*  CORRECT ERROR FOR NRA IND
        //*  BEGIN
        //* IF CNTRCT-CRRNCY-CDE = '1'
        //*  IF (ANNT-CTZNSHP-CDE = 01 OR= 11)
        //* *    OR    /* COMM OUT 2 LNS TO FIX TAX LETTER  G.P 8/12/98
        //* *   ( (ANNT-RSDNCY-CDE = '01' THRU '57') OR (ANNT-RSDNCY-CDE = '97') )
        //*    ASSIGN #WS-LEGEND = '(F)'
        //*  ELSE
        //*    ASSIGN #WS-LEGEND = '(N)'
        //*  END-IF
        //*  START NEW CODE       /* ROXAN 3/14/2002
        if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                               //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                   //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
            {
                pnd_Ws_Legend.setValue("(N)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND = '(N)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Legend.setValue("(F)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND = '(F)'
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF CODE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                               //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ANNT-RSDNCY-CDE = '74' THRU '86'
            {
                pnd_Ws_Legend.setValue("(C)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(C)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86")))) //Natural: IF NOT ( ANNT-RSDNCY-CDE = '74' THRU '86' )
                {
                    pnd_Ws_Legend.setValue("(C)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND = '(C)'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_State_Legend() throws Exception                                                                                                               //Natural: FORMAT-STATE-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  DETERMINE THE LEGEND PRINTED NEXT TO THE STATE TAX DEDUCTION
        //*  ----------------------------------------------------------------------
        if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                               //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))                                                                            //Natural: IF ANNT-RSDNCY-CDE = '97'
            {
                pnd_Ws_Legend.setValue("(S)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND = '(S)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11))    //Natural: IF ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( ANNT-RSDNCY-CDE = '01' THRU '57' )
                    || (pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))))
                {
                    pnd_Ws_Legend.setValue("(S)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND = '(S)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((! ((pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11)))  //Natural: IF NOT ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( ANNT-RSDNCY-CDE = '01' THRU '57' ) AND ANNT-RSDNCY-CDE NE '97'
                        && ! ((pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))) 
                        && pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().notEquals("97"))))
                    {
                        pnd_Ws_Legend.setValue("(N)");                                                                                                                    //Natural: ASSIGN #WS-LEGEND = '(N)'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                               //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ( ANNT-RSDNCY-CDE = '74' THRU '86' )
            {
                pnd_Ws_Legend.setValue("(P)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND = '(P)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa700a.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("96")))                                                                        //Natural: IF ANNT-RSDNCY-CDE = '96'
                {
                    pnd_Ws_Legend.setValue("(C)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND = '(C)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Legend.setValue("(C)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND = '(C)'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bld_Col_Hdng() throws Exception                                                                                                                      //Natural: BLD-COL-HDNG
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ......... COLUMN HEADING LINES - TIAA OR CREF
        //*  LZ
        short decideConditionsMet1548 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'T' AND #WS-PRINTED-TIAA-HEADING = 'N'
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T") && pnd_Ws_Printed_Tiaa_Heading.equals("N")))
        {
            decideConditionsMet1548++;
            pnd_Ws_Rec_1.setValue("14TIAA");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = '14TIAA'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4Contract");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4Contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  LZ
            pnd_Ws_Printed_Tiaa_Heading.setValue("Y");                                                                                                                    //Natural: ASSIGN #WS-PRINTED-TIAA-HEADING = 'Y'
        }                                                                                                                                                                 //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'C' AND #WS-PRINTED-CREF-HEADING = 'N'
        else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C") && pnd_Ws_Printed_Cref_Heading.equals("N")))
        {
            decideConditionsMet1548++;
            if (condition(pnd_Ws_Printed_Tiaa_Heading.equals("Y")))                                                                                                       //Natural: IF #WS-PRINTED-TIAA-HEADING = 'Y'
            {
                pnd_Ws_Rec_1.setValue("-4CREF");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = '-4CREF'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue("14CREF");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = '14CREF'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4Certificate");                                                                                                                       //Natural: ASSIGN #WS-REC-1 = ' 4Certificate'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Printed_Cref_Heading.setValue("Y");                                                                                                                    //Natural: ASSIGN #WS-PRINTED-CREF-HEADING = 'Y'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ............... MORE COLUMN HEADINGS
        pnd_Ws_Rec_1.setValue("14     Payment");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = '14     Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_Ws_Rec_1.setValue("14     Interest");                                                                                                                     //Natural: ASSIGN #WS-REC-1 = '14     Interest'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_Ws_Rec_1.setValue("14    Deductions");                                                                                                                    //Natural: ASSIGN #WS-REC-1 = '14    Deductions'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1.setValue("14       Net");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = '14       Net'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4       Payment");                                                                                                                        //Natural: ASSIGN #WS-REC-1 = ' 4       Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("N")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'N'
        {
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("N")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'N'
        {
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG
    }
    private void sub_Setup_A_Line_Totals() throws Exception                                                                                                               //Natural: SETUP-A-LINE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        pnd_Ws_Tot_Payment_Amt.reset();                                                                                                                                   //Natural: RESET #WS-TOT-PAYMENT-AMT #WS-TOT-DPI-AMT #WS-TOT-DED-AMT #WS-TOT-CHECK-AMT
        pnd_Ws_Tot_Dpi_Amt.reset();
        pnd_Ws_Tot_Ded_Amt.reset();
        pnd_Ws_Tot_Check_Amt.reset();
        //* *IF #ITS-A-CREF-FUND OR #ITS-A-TIAA-FUND         /* 02-27-97 : A. YOUNG
        //*  02-27-97 : A. YOUNG
        if (condition(pnd_Its_A_Cref_Fund.getBoolean()))                                                                                                                  //Natural: IF #ITS-A-CREF-FUND
        {
            pnd_Ws_Tot_Payment_Amt.setValue(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J));                                                           //Natural: ASSIGN #WS-TOT-PAYMENT-AMT := INV-ACCT-SETTL-AMT ( #J )
            //*  TIAA PAYMENT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Tot_Payment_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Payment_Amt), pnd_Ws_Tot_Payment_Amt.add(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J)).add(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J))); //Natural: ADD INV-ACCT-SETTL-AMT ( #J ) INV-ACCT-DVDND-AMT ( #J ) TO #WS-TOT-PAYMENT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Tot_Dpi_Amt.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J));                                                                         //Natural: ADD INV-ACCT-DPI-AMT ( #J ) TO #WS-TOT-DPI-AMT
        pnd_Ws_Tot_Ded_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Ded_Amt), pnd_Ws_Tot_Ded_Amt.add(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J)).add(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J))); //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #J ) INV-ACCT-STATE-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
        pnd_Ws_Tot_Ded_Amt.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J));                                                                   //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
        pnd_Ws_Tot_Ded_Amt.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,"*"));                                                                        //Natural: ADD PYMNT-DED-AMT ( #J,* ) TO #WS-TOT-DED-AMT
        pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa700a.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                                 //Natural: ADD PYMNT-CHECK-AMT TO #WS-TOT-CHECK-AMT
        //*  SETUP-A-LINE-TOTALS
    }
    private void sub_Write_Recs() throws Exception                                                                                                                        //Natural: WRITE-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }
    private void sub_Test_For_Good_Ivc() throws Exception                                                                                                                 //Natural: TEST-FOR-GOOD-IVC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.setValue(false);                                                                                                                  //Natural: ASSIGN #WS-GOOD-IVC := FALSE
        pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc.reset();                                                                                                                          //Natural: RESET #WS-TIAA-IVC #WS-CREF-IVC
        pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc.reset();
        FOR03:                                                                                                                                                            //Natural: FOR #K = 1 TO #WS-CNTR-INV-ACCT
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_K.nadd(1))
        {
            if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Ivc_Ind().getValue(pnd_K).equals("2") && pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_K).greater(getZero()))) //Natural: IF INV-ACCT-IVC-IND ( #K ) = '2' AND INV-ACCT-IVC-AMT ( #K ) > 0
            {
                pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.setValue(true);                                                                                                           //Natural: ASSIGN #WS-GOOD-IVC := TRUE
                //*  FC ADD 'R' 10-17-95
                if (condition(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_K).equals("T") || pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_K).equals("G")  //Natural: IF INV-ACCT-CDE ( #K ) = 'T' OR = 'G' OR = 'R'
                    || pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_K).equals("R")))
                {
                    pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_K));                                                //Natural: ADD INV-ACCT-IVC-AMT ( #K ) TO #WS-TIAA-IVC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_K));                                                //Natural: ADD INV-ACCT-IVC-AMT ( #K ) TO #WS-CREF-IVC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TEST-FOR-GOOD-IVC
    }
    private void sub_Show_Ivc_Footnote() throws Exception                                                                                                                 //Natural: SHOW-IVC-FOOTNOTE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text.reset();                                                                                                                     //Natural: RESET #WS-IVC-TIAA-TEXT #WS-IVC-CREF-TEXT
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text.reset();
        if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc.greater(getZero())))                                                                                                //Natural: IF #WS-TIAA-IVC > 0
        {
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text.setValueEdited(pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                  //Natural: MOVE EDITED #WS-TIAA-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-IVC-TIAA-TEXT
            DbsUtil.examine(new ExamineSource(pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text,true), new ExamineSearch("X", true), new ExamineDelete());                             //Natural: EXAMINE FULL #WS-IVC-TIAA-TEXT FOR FULL 'X' DELETE
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text.setValue(DbsUtil.compress("TIAA:", pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text));                                               //Natural: COMPRESS 'TIAA:' #WS-IVC-TIAA-TEXT INTO #WS-IVC-TIAA-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc.greater(getZero())))                                                                                                //Natural: IF #WS-CREF-IVC > 0
        {
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text.setValueEdited(pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                  //Natural: MOVE EDITED #WS-CREF-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-IVC-CREF-TEXT
            DbsUtil.examine(new ExamineSource(pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text,true), new ExamineSearch("X", true), new ExamineDelete());                             //Natural: EXAMINE FULL #WS-IVC-CREF-TEXT FOR FULL 'X' DELETE
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text.setValue(DbsUtil.compress("CREF:", pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text));                                               //Natural: COMPRESS 'CREF:' #WS-IVC-CREF-TEXT INTO #WS-IVC-CREF-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ......... CREATE IVC MESSAGE LINE.
        //*  .... NOTE: IF "TIAA" OR "CREF" AREAS ARE EMPTY
        //*             THEY WILL BE COMPRESSED OUT
        pnd_Ws_Rec_1.setValue(DbsUtil.compress("17# PAYT. INCLUDES AFTER-TAX CONTRIBUTIONS OF ", pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text, pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text)); //Natural: COMPRESS '17# PAYT. INCLUDES AFTER-TAX CONTRIBUTIONS OF ' #WS-IVC-TIAA-TEXT #WS-IVC-CREF-TEXT INTO #WS-REC-1
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  SHOW-IVC-FOOTNOTE
    }
    private void sub_Bld_Col_Hdng_Pt9() throws Exception                                                                                                                  //Natural: BLD-COL-HDNG-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*   BUILD COLUMN HEADING LINES AS PER THE DATA TO BE PRINTED.
        //*   THERE ARE SOME COLUMNS THAT ARE ONLY CONDITIONALLY PRINTED. IN CASE
        //*   THAT A CERTAIN DATA, FOR EXAMPLE DELAYED PAYMENT INTEREST, IS NOT
        //*   PRESENT FOR THIS PRINTOUT (ON ANY OF ITS PAGES!), THEN IT IS NOT
        //*   PRINTED.
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                                     //Natural: IF #WS-FIRST-TIME = 'Y'
        {
            pnd_Ws_First_Time.setValue("N");                                                                                                                              //Natural: ASSIGN #WS-FIRST-TIME = 'N'
            //*  ......... COLUMN HEADING LINES - TIAA OR CREF
            //*  LZ
            short decideConditionsMet1688 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'T' AND #WS-PRINTED-TIAA-HEADING = 'N'
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T") && pnd_Ws_Printed_Tiaa_Heading.equals("N")))
            {
                decideConditionsMet1688++;
                pnd_Ws_Rec_1.setValue("14TIAA");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = '14TIAA'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4Contract");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4Contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                //*  LZ
                pnd_Ws_Printed_Tiaa_Heading.setValue("Y");                                                                                                                //Natural: ASSIGN #WS-PRINTED-TIAA-HEADING = 'Y'
            }                                                                                                                                                             //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'C' AND #WS-PRINTED-CREF-HEADING = 'N'
            else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C") && pnd_Ws_Printed_Cref_Heading.equals("N")))
            {
                decideConditionsMet1688++;
                if (condition(pnd_Ws_Printed_Tiaa_Heading.equals("Y")))                                                                                                   //Natural: IF #WS-PRINTED-TIAA-HEADING = 'Y'
                {
                    pnd_Ws_Rec_1.setValue("-4CREF");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '-4CREF'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Rec_1.setValue("14CREF");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14CREF'
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4Certificate");                                                                                                                   //Natural: ASSIGN #WS-REC-1 = ' 4Certificate'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Printed_Cref_Heading.setValue("Y");                                                                                                                //Natural: ASSIGN #WS-PRINTED-CREF-HEADING = 'Y'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  ............... MORE COLUMN HEADINGS
            pnd_Ws_Rec_1.setValue("14     Payment");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14     Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  ...........INTEREST HEADING OR BLANK HEADINGS
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_Ws_Rec_1.setValue("14     Interest");                                                                                                                 //Natural: ASSIGN #WS-REC-1 = '14     Interest'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  NO HEADER FOR LITERAL "TTB"
                pnd_Ws_Rec_1.setValue("14");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = '14'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  ........... DEDUCTIONS
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_Ws_Rec_1.setValue("14    Deductions");                                                                                                                //Natural: ASSIGN #WS-REC-1 = '14    Deductions'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1.setValue("14       Net");                                                                                                                        //Natural: ASSIGN #WS-REC-1 = '14       Net'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4       Payment");                                                                                                                    //Natural: ASSIGN #WS-REC-1 = ' 4       Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("N")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'N'
            {
                pnd_Ws_Rec_1.setValue("14");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("15");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG-PT9
    }
    private void sub_Bld_A_Line_Pt9() throws Exception                                                                                                                    //Natural: BLD-A-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*    "A Line": TOTALS FOR THE "group of Lines"
        //*  ----------------------------------------------------------------------
        //*   ...... ACCUM THE TOTALS FOR THE "group of Lines"
                                                                                                                                                                          //Natural: PERFORM SETUP-A-LINE-TOTALS
        sub_Setup_A_Line_Totals();
        if (condition(Global.isEscape())) {return;}
        //*   ...... NOW BUILD THE "A Line"
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '05'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN - PPCN - ALWAYS
        //*  ENTER CNTRCT-PPCN-NBR
        //*  FOR MULTIPLE CREF PRODUCTS, ONLY ENTER THE PPCN FOR
        //*  THE 1ST OCCURENCE
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr().getValue(pnd_J),new ReportEditMask("XXXXXXX-X"));                                   //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(pnd_Ws_Temp_Col));                                                                           //Natural: COMPRESS #WS-TEMP-COL INTO #WS-COLUMN ( #F )
        //*  .........2ND COLUMN - TOTAL PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                    //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                                       //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                        //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
        //*  .........3RD COLUMN - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED INV-ACCT-DPI-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ...... 4TH COLUMN FOR TTB LITERAL
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("TTB");                                                                                                       //Natural: ASSIGN #WS-COLUMN ( #F ) := 'TTB'
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  .........5TH COLUMN - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Deduction_Column_Nbr.setValue(pnd_F);                                                                                                                     //Natural: ASSIGN #DEDUCTION-COLUMN-NBR := #F
            if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                         //Natural: IF #WS-TOT-DED-AMT > 0
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZ,ZZ9.99"));                                                                   //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ........ 5TH COLUMN - NET PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                     //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                                  //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-A-LINE-PT9
    }
    private void sub_Bld_Db_Line_Pt9() throws Exception                                                                                                                   //Natural: BLD-DB-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*   BUILD THE  "Db Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN - PPCN
        //*  FOR TIAA LEAVE SPACE, FOR CREF
        //*  ENTER THE INVESTMENT ACCOUNT'S DESCRIPTION (USE FCPN199A'S DATA)
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-1
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ............ COLUMN 3 - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ...... COLUMN 4 - FOR TTB LITERAL
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ............ COLUMN 5 - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO MORE COLUNMS FOR THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DB-LINE-PT9
    }
    private void sub_Bld_Dc_Line_Pt9() throws Exception                                                                                                                   //Natural: BLD-DC-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dc Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ........ 1ST COLUMN - SETTLEMENT DATE
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-2
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ......... 3RD COLUMN - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ...... 4TH COLUMN FOR TTB LITERAL
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ............ 5TH COLUMN - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO MORE COLUMNS FOR THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DC-LINE-PT9
    }
    private void sub_Bld_Dx_Line_Pt9() throws Exception                                                                                                                   //Natural: BLD-DX-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dx Line": LOCAL TAX DEDUCTIONS, IF NEEDED
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Nbr_Of_Remaining_Deductions.greater(getZero())))                                                                                                //Natural: IF #NBR-OF-REMAINING-DEDUCTIONS GT 0
        {
            pnd_Ws_Rec_1.reset();                                                                                                                                         //Natural: RESET #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 5");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '+3'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_Deduction_Column_Nbr).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #DEDUCTION-COLUMN-NBR )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-DX-LINE-PT9
    }
    private void sub_Bld_E_Line_Pt9() throws Exception                                                                                                                    //Natural: BLD-E-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ----------------------------------------------------------------------
        //*  BUILD "Grand Totals" LINE
        //*  ----------------------------------------------------------------------
        //*  ............ACCUM "Grand Totals"
        if (condition(pnd_Ws_Side_Printed.equals(pdaFcpa700a.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                 //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_Payment.reset();                                                                                                                             //Natural: RESET #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Dpi.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            //* ******
            pnd_Ws_Grand_Tot_Payment.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-SETTL-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Payment.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-DVDND-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Dpi.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                              //Natural: ADD INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DPI
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                         //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct,"*"));                                             //Natural: ADD PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa700a.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                        //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-TOT-CHECK-AMT
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC := '06'
            pnd_F.setValue(pnd_1st_F);                                                                                                                                    //Natural: ASSIGN #F := #1ST-F
            //*  ..... 1ST COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
            //*  ......... EDITE / BUILD THE GRAND TOTAL LINE
            //*  ..... 2ND COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
            //*  .... IF THERE IS IVC MONEY "footnote" THE GRAND TOTAL
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.setValue("#");                                                                                         //Natural: ASSIGN #WS-COLUMN-OFFSET-RGT-POS1 := '#'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                    //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            //*  ..... 3RD COLUMN
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //*  ...... 4TH COLUMN FOR TTB LITERAL
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###***
            //*  ..... 5TH COLUMN
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99"));                                                                //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###***
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                  //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-E-LINE-PT9
    }

    //
}
