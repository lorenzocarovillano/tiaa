/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:39 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn236a
************************************************************
**        * FILE NAME            : Fcpn236a.java
**        * CLASS NAME           : Fcpn236a
**        * INSTANCE NAME        : Fcpn236a
************************************************************
*
***********************************************************************
* LEON SILBERSTEIN - 10/30/95
***********************************************************************
* PROGRAM  : FCPN236A "HEADER" PORTION OF STATEMENT, "DS" "SS" "EW"
*
** CORRECT (INTERNALY) PROGRAM NAME FROM FCPN236 TO FCPN236A
*
* SYSTEM   : CPS
* TITLE    : ANNUITANT STATEMENT
* GENERATED: ?
* FUNCTION : THIS PROGRAM WILL BUILD THE "HEADER" PORTION OF AN
*            ANNUITANT STATEMENT. CLONE OF FCPN236.  DIFFERENT XEROX
*            COMMAND
* HISTORY  :
*
*     10/30/95: JUN F. TINIO
*
*     11/12/97: F.ORTIZ  ADD STS LOGIC FOR SINGLE SUM
*
*   01/01/2000: L.GURTOVNIK - ADD PROCESS FOR ELECTRONIC WARRANTS (EW)
*                AND  CORRECT PROGRAM NAME FROM FCPN236 TO FCPN236A
*
*   07/10/2001: J. NEUFELD - ADDED NEW VALUE FOR FREQUENCY CODE
*                            'D'=MDO
*
*     10/15/01 : R. CARREON - REMOVE SSN FROM SS STATEMENT. RESTRUCTURE
*                             TO SEPARATE SS AND EW SSN PRINTING
*                             AS PER ILSE 10/12/01 VIA EMAIL.
*
*     11/02/01 : R. CARREON - RESTORE ORIGINAL STRUCTURE AND COMMENT OUT
*                             SSN FOR EW AND SS
*                             AS PER JIM
*        11/02 : R. CARREON - STOW EGTRRA CHANGES IN PDA
*
***********************************************************************
*
* PROGRAMMER    START DTE  END DTE   DESC OF CHANGES
* ------------  ---------  --------  ----------------------------------
* F. CHIN       08/11/93     /  /    CREATED.
*     PROGRAM FOR HEADER-PORTION OF DOCUMENT
*
*
*     INSERTED 09-22-94 CODING FOR SWAT AND STS

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn236a extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa200 pdaFcpa200;
    private LdaFcpl236 ldaFcpl236;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_1;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column;
    private DbsField pnd_Ws_Rec_10;

    private DbsGroup pnd_Ws_Rec_10__R_Field_2;
    private DbsField pnd_Ws_Rec_10_Filler_A;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Lsw_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_3;
    private DbsField pnd_Ws_Rec_10_Filler_C;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Rwp_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_4;
    private DbsField pnd_Ws_Rec_10_Filler_D;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Tpa_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_5;
    private DbsField pnd_Ws_Rec_10_Filler_E;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Mdo_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_6;
    private DbsField pnd_Ws_Rec_10_Filler_F;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Ira_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_7;
    private DbsField pnd_Ws_Rec_10_Filler_G;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Gsr_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_8;
    private DbsField pnd_Ws_Rec_10_Filler_H;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Sra_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_9;
    private DbsField pnd_Ws_Rec_10_Filler_I;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Swat_Name;

    private DbsGroup pnd_Ws_Rec_10__R_Field_10;
    private DbsField pnd_Ws_Rec_10_Filler_J;
    private DbsField pnd_Ws_Rec_10_Pnd_Ws_10_Sts_Name;
    private DbsField pnd_Ws_Effect_Dt;
    private DbsField pnd_Ws_Check_Dt;
    private DbsField pnd_Ws_Ssn;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Name;
    private DbsField pnd_Ws_Pnd_Lob_Cde;
    private DbsField pnd_Ws_Pnd_Lob_Sub;
    private DbsField pnd_Ws_Pymnt_Split_Reasn_Cde;

    private DbsGroup pnd_Ws__R_Field_11;
    private DbsField pnd_Ws_Swat_Frqncy_Cde;
    private DbsField pnd_Ws_Pymnt_Init_Rqst_Ind;
    private DbsField pnd_Ind;
    private DbsField pnd_Prnt_Deceased_Name;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl236 = new LdaFcpl236();
        registerRecord(ldaFcpl236);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa200 = new PdaFcpa200(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_1", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc = pnd_Ws_Rec_1__R_Field_1.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc", "#WS-REC1-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_1_Pnd_Ws_Column = pnd_Ws_Rec_1__R_Field_1.newFieldArrayInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column", "#WS-COLUMN", FieldType.STRING, 15, new 
            DbsArrayController(1, 6));
        pnd_Ws_Rec_10 = localVariables.newFieldInRecord("pnd_Ws_Rec_10", "#WS-REC-10", FieldType.STRING, 143);

        pnd_Ws_Rec_10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_2", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_A = pnd_Ws_Rec_10__R_Field_2.newFieldInGroup("pnd_Ws_Rec_10_Filler_A", "FILLER-A", FieldType.STRING, 62);
        pnd_Ws_Rec_10_Pnd_Ws_10_Lsw_Name = pnd_Ws_Rec_10__R_Field_2.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Lsw_Name", "#WS-10-LSW-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_3", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_C = pnd_Ws_Rec_10__R_Field_3.newFieldInGroup("pnd_Ws_Rec_10_Filler_C", "FILLER-C", FieldType.STRING, 63);
        pnd_Ws_Rec_10_Pnd_Ws_10_Rwp_Name = pnd_Ws_Rec_10__R_Field_3.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Rwp_Name", "#WS-10-RWP-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_4", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_D = pnd_Ws_Rec_10__R_Field_4.newFieldInGroup("pnd_Ws_Rec_10_Filler_D", "FILLER-D", FieldType.STRING, 81);
        pnd_Ws_Rec_10_Pnd_Ws_10_Tpa_Name = pnd_Ws_Rec_10__R_Field_4.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Tpa_Name", "#WS-10-TPA-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_5", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_E = pnd_Ws_Rec_10__R_Field_5.newFieldInGroup("pnd_Ws_Rec_10_Filler_E", "FILLER-E", FieldType.STRING, 87);
        pnd_Ws_Rec_10_Pnd_Ws_10_Mdo_Name = pnd_Ws_Rec_10__R_Field_5.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Mdo_Name", "#WS-10-MDO-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_6", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_F = pnd_Ws_Rec_10__R_Field_6.newFieldInGroup("pnd_Ws_Rec_10_Filler_F", "FILLER-F", FieldType.STRING, 88);
        pnd_Ws_Rec_10_Pnd_Ws_10_Ira_Name = pnd_Ws_Rec_10__R_Field_6.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Ira_Name", "#WS-10-IRA-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_7", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_G = pnd_Ws_Rec_10__R_Field_7.newFieldInGroup("pnd_Ws_Rec_10_Filler_G", "FILLER-G", FieldType.STRING, 88);
        pnd_Ws_Rec_10_Pnd_Ws_10_Gsr_Name = pnd_Ws_Rec_10__R_Field_7.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Gsr_Name", "#WS-10-GSR-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_8 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_8", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_H = pnd_Ws_Rec_10__R_Field_8.newFieldInGroup("pnd_Ws_Rec_10_Filler_H", "FILLER-H", FieldType.STRING, 96);
        pnd_Ws_Rec_10_Pnd_Ws_10_Sra_Name = pnd_Ws_Rec_10__R_Field_8.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Sra_Name", "#WS-10-SRA-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_9 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_9", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_I = pnd_Ws_Rec_10__R_Field_9.newFieldInGroup("pnd_Ws_Rec_10_Filler_I", "FILLER-I", FieldType.STRING, 45);
        pnd_Ws_Rec_10_Pnd_Ws_10_Swat_Name = pnd_Ws_Rec_10__R_Field_9.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Swat_Name", "#WS-10-SWAT-NAME", FieldType.STRING, 
            38);

        pnd_Ws_Rec_10__R_Field_10 = localVariables.newGroupInRecord("pnd_Ws_Rec_10__R_Field_10", "REDEFINE", pnd_Ws_Rec_10);
        pnd_Ws_Rec_10_Filler_J = pnd_Ws_Rec_10__R_Field_10.newFieldInGroup("pnd_Ws_Rec_10_Filler_J", "FILLER-J", FieldType.STRING, 45);
        pnd_Ws_Rec_10_Pnd_Ws_10_Sts_Name = pnd_Ws_Rec_10__R_Field_10.newFieldInGroup("pnd_Ws_Rec_10_Pnd_Ws_10_Sts_Name", "#WS-10-STS-NAME", FieldType.STRING, 
            38);
        pnd_Ws_Effect_Dt = localVariables.newFieldInRecord("pnd_Ws_Effect_Dt", "#WS-EFFECT-DT", FieldType.STRING, 17);
        pnd_Ws_Check_Dt = localVariables.newFieldInRecord("pnd_Ws_Check_Dt", "#WS-CHECK-DT", FieldType.STRING, 11);
        pnd_Ws_Ssn = localVariables.newFieldInRecord("pnd_Ws_Ssn", "#WS-SSN", FieldType.STRING, 20);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Name", "#WS-NAME", FieldType.STRING, 38);
        pnd_Ws_Pnd_Lob_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Lob_Cde", "#LOB-CDE", FieldType.STRING, 4);
        pnd_Ws_Pnd_Lob_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Lob_Sub", "#LOB-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pymnt_Split_Reasn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);

        pnd_Ws__R_Field_11 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_11", "REDEFINE", pnd_Ws_Pymnt_Split_Reasn_Cde);
        pnd_Ws_Swat_Frqncy_Cde = pnd_Ws__R_Field_11.newFieldInGroup("pnd_Ws_Swat_Frqncy_Cde", "SWAT-FRQNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Pymnt_Init_Rqst_Ind = pnd_Ws__R_Field_11.newFieldInGroup("pnd_Ws_Pymnt_Init_Rqst_Ind", "PYMNT-INIT-RQST-IND", FieldType.STRING, 1);
        pnd_Ind = localVariables.newFieldInRecord("pnd_Ind", "#IND", FieldType.NUMERIC, 1);
        pnd_Prnt_Deceased_Name = localVariables.newFieldInRecord("pnd_Prnt_Deceased_Name", "#PRNT-DECEASED-NAME", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl236.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Ws_Rec_1.setInitialValue("11");
        pnd_Ws_Rec_10.setInitialValue("+2");
        pnd_Ind.setInitialValue(0);
        pnd_Prnt_Deceased_Name.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn236a() throws Exception
    {
        super("Fcpn236a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.resetInitial();                                                                                                                                      //Natural: RESET INITIAL #WS-REC-1 #WS-REC-10
        pnd_Ws_Rec_10.resetInitial();
        //*  SWAT CHANGES - LEON - 95/10/24
        //*  MOVE FIELD PYMNT-SPLIT-REASN-CDE FROM THE PDA
        //*  INTO #WS SO WE CAN REFER TO THE FIELD COMPONENTS BY NAME,
        //*  WITHOUT MAKING THE CHANGES TO THE PDA - FCPA200
        pnd_Ws_Pymnt_Split_Reasn_Cde.setValue(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde());                                                                //Natural: ASSIGN #WS.PYMNT-SPLIT-REASN-CDE := #WS-HEADER-RECORD.PYMNT-SPLIT-REASN-CDE
        //*  LEON
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("SS") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))            //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'SS' OR = 'EW'
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-SINGLE-SUM
            sub_Process_Single_Sum();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-OTHER-ORIGIN
            sub_Process_Other_Origin();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Effect_Dt.setValueEdited(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                          //Natural: MOVE EDITED PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #WS-EFFECT-DT
        pnd_Ws_Check_Dt.setValueEdited(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                              //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS-CHECK-DT
        pnd_Ws_Ssn.setValueEdited(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Soc_Sec_Nbr(),new ReportEditMask("999-99-9999"));                                               //Natural: MOVE EDITED ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) TO #WS-SSN
        //* *---
        //* **#WS-REC-1 := ' 7'                                          /* ORIG
        //* **WRITE WORK FILE 8 #WS-REC-1                                /* ORIG
        //* *----------------------------------------------------------- /* LEON
        pnd_Prnt_Deceased_Name.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #PRNT-DECEASED-NAME
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))                                                                                 //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'EW'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #IND 1 TO 2
            for (pnd_Ind.setValue(1); condition(pnd_Ind.lessOrEqual(2)); pnd_Ind.nadd(1))
            {
                if (condition(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Deceased_Nme().getValue(pnd_Ind).notEquals(" ")))                                                 //Natural: IF PYMNT-DECEASED-NME ( #IND ) NE ' '
                {
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress(" 7Deceased Name:", pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Deceased_Nme().getValue(pnd_Ind)));              //Natural: COMPRESS ' 7Deceased Name:' PYMNT-DECEASED-NME ( #IND ) INTO #WS-REC-1
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                    pnd_Prnt_Deceased_Name.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #PRNT-DECEASED-NAME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(! (pnd_Prnt_Deceased_Name.getBoolean())))                                                                                                       //Natural: IF NOT #PRNT-DECEASED-NAME
            {
                pnd_Ws_Rec_1.setValue(" 7");                                                                                                                              //Natural: ASSIGN #WS-REC-1 := ' 7'
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1.setValue(" 7");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 7'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------- /* LEON
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(" 7Effective Date:", pnd_Ws_Effect_Dt, "H'000000'", "Check Date:", pnd_Ws_Check_Dt, "H'000000'"));                         //Natural: COMPRESS ' 7Effective Date:' #WS-EFFECT-DT H'000000' 'Check Date:' #WS-CHECK-DT H'000000' INTO #WS-REC-1
        //* *H'000000'  'Social Security No.:' #WS-SSN
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-SINGLE-SUM
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-OTHER-ORIGIN
        //* *--> INSERTED 09-22-94 BY FRANK
        //* *--> COMMENTED-OUT/INSERTED 03-26-96 BY FRANK
        //* *-->   TEST FOR 1 THRU 4
        //* *   AND NOT(SWAT-FRQNCY-CDE = 'M' OR = ' ')
        //* *--> INSERTED 09-22-94 BY FRANK
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUTPUT-RECORDS
    }
    private void sub_Process_Single_Sum() throws Exception                                                                                                                //Natural: PROCESS-SINGLE-SUM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_Ws_Pnd_Ws_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-NAME
            pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        short decideConditionsMet377 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-TYPE-CDE;//Natural: VALUE 'L'
        if (condition((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L"))))
        {
            decideConditionsMet377++;
            //*  M=STS, D=MDO' '=NONE, ELSE=SWAT
            short decideConditionsMet380 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE SWAT-FRQNCY-CDE;//Natural: VALUE ' ','D'
            if (condition((pnd_Ws_Swat_Frqncy_Cde.equals(" ") || pnd_Ws_Swat_Frqncy_Cde.equals("D"))))
            {
                decideConditionsMet380++;
                //*  STS
                pnd_Ws_Pnd_Lob_Cde.setValue(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde());                                                                         //Natural: MOVE CNTRCT-LOB-CDE TO #LOB-CDE
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((pnd_Ws_Swat_Frqncy_Cde.equals("M"))))
            {
                decideConditionsMet380++;
                pnd_Ws_Pnd_Lob_Cde.setValue("STS");                                                                                                                       //Natural: MOVE 'STS' TO #LOB-CDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Lob_Cde.setValue("SWAT");                                                                                                                      //Natural: MOVE 'SWAT' TO #LOB-CDE
                //*  ADDED R - REPURCHASE FOR EW  LEON
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE '30','31','R'
        else if (condition((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("31") 
            || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("R"))))
        {
            decideConditionsMet377++;
            pnd_Ws_Pnd_Lob_Cde.setValue(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde());                                                                            //Natural: MOVE CNTRCT-TYPE-CDE TO #LOB-CDE
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet377 > 0))
        {
            DbsUtil.examine(new ExamineSource(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde().getValue("*"),true), new ExamineSearch(pnd_Ws_Pnd_Lob_Cde,              //Natural: EXAMINE FULL #FCPL236-LOB-CDE ( * ) FOR FULL #LOB-CDE GIVING INDEX IN #LOB-SUB
                true), new ExamineGivingIndex(pnd_Ws_Pnd_Lob_Sub));
            //*  ORIGINAL
            //*    IF #LOB-SUB NE 0
            //*      MOVE #STMNT-HDR-DATA(#LOB-SUB)     TO SUBSTR(#WS-REC-1,3,55)
            //*      MOVE #WS-NAME
            //*        TO SUBSTR(#WS-REC-10,#STMNT-HDR-CNTL-CHAR(#LOB-SUB),38)
            //*      PERFORM WRITE-OUTPUT-RECORDS
            //*  END OF ORIGINAL
            //*   DEFAULT LOB
            if (condition(pnd_Ws_Pnd_Lob_Sub.equals(getZero())))                                                                                                          //Natural: IF #LOB-SUB EQ 0
            {
                pnd_Ws_Pnd_Lob_Sub.setValue(26);                                                                                                                          //Natural: ASSIGN #LOB-SUB := 26
            }                                                                                                                                                             //Natural: END-IF
            setValueToSubstring(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data().getValue(pnd_Ws_Pnd_Lob_Sub),pnd_Ws_Rec_1,3,55);                                       //Natural: MOVE #STMNT-HDR-DATA ( #LOB-SUB ) TO SUBSTR ( #WS-REC-1,3,55 )
            setValueToSubstring(pnd_Ws_Pnd_Ws_Name,pnd_Ws_Rec_10,ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char().getValue(pnd_Ws_Pnd_Lob_Sub).getInt(),           //Natural: MOVE #WS-NAME TO SUBSTR ( #WS-REC-10,#STMNT-HDR-CNTL-CHAR ( #LOB-SUB ) ,38 )
                38);
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-RECORDS
            sub_Write_Output_Records();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Process_Other_Origin() throws Exception                                                                                                              //Natural: PROCESS-OTHER-ORIGIN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  SWAT PAYMENT
        short decideConditionsMet419 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-TYPE-CDE = 'C' OR = 'L' AND ( SWAT-FRQNCY-CDE = '1' THRU '4' )
        if (condition(((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("C") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L")) 
            && (pnd_Ws_Swat_Frqncy_Cde.greaterOrEqual("1") && pnd_Ws_Swat_Frqncy_Cde.lessOrEqual("4")))))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Systematic Withdrawal For:");                                                                                                        //Natural: ASSIGN #WS-REC-1 = '11Systematic Withdrawal For:'
            //*  STS PAYMENT
            pnd_Ws_Rec_10_Pnd_Ws_10_Swat_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-SWAT-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'C' OR = 'L' AND SWAT-FRQNCY-CDE = 'M'
        else if (condition(((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("C") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L")) 
            && pnd_Ws_Swat_Frqncy_Cde.equals("M"))))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Special Transfer Service For:");                                                                                                     //Natural: ASSIGN #WS-REC-1 = '11Special Transfer Service For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Sts_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-STS-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'C' AND CNTRCT-LOB-CDE = 'SRA '
        else if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("C") && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("SRA ")))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Supplemental Retirement Annuity Withdrawal Payment For:");                                                                           //Natural: ASSIGN #WS-REC-1 = '11Supplemental Retirement Annuity Withdrawal Payment For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Sra_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-SRA-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'C' AND CNTRCT-LOB-CDE = 'GSRA'
        else if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("C") && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("GSRA")))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Group Supplemental Retirement Annuity Payment For:");                                                                                //Natural: ASSIGN #WS-REC-1 = '11Group Supplemental Retirement Annuity Payment For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Gsr_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-GSR-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'C' AND CNTRCT-LOB-CDE = 'TPA'
        else if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("C") && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("TPA")))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Transfer Payout Annuity Withdrawal Payment For:");                                                                                   //Natural: ASSIGN #WS-REC-1 = '11Transfer Payout Annuity Withdrawal Payment For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Tpa_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-TPA-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'C' AND CNTRCT-LOB-CDE = 'IRA '
        else if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("C") && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("IRA ")))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Individual Retirement Annuity Withdrawal Payment For:");                                                                             //Natural: ASSIGN #WS-REC-1 = '11Individual Retirement Annuity Withdrawal Payment For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Ira_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-IRA-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'C' AND CNTRCT-LOB-CDE = 'MDO '
        else if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("C") && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("MDO ")))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Minimum Distribution Option Withdrawal Payment For:");                                                                               //Natural: ASSIGN #WS-REC-1 = '11Minimum Distribution Option Withdrawal Payment For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Mdo_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-MDO-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'R'
        else if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("R")))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Repurchase Withdrawal Payment For:");                                                                                                //Natural: ASSIGN #WS-REC-1 = '11Repurchase Withdrawal Payment For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Rwp_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-RWP-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'L'
        else if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L")))
        {
            decideConditionsMet419++;
            pnd_Ws_Rec_1.setValue("11Lump Sum Withdrawal Payment For:");                                                                                                  //Natural: ASSIGN #WS-REC-1 = '11Lump Sum Withdrawal Payment For:'
            pnd_Ws_Rec_10_Pnd_Ws_10_Lsw_Name.setValue(DbsUtil.compress(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS-10-LSW-NAME
                pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet419 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-RECORDS
            sub_Write_Output_Records();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_Output_Records() throws Exception                                                                                                              //Natural: WRITE-OUTPUT-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Rec_10);                                                                                                                    //Natural: WRITE WORK FILE 8 #WS-REC-10
    }

    //
}
