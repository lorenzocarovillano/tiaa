/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:34 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn230
************************************************************
**        * FILE NAME            : Fcpn230.java
**        * CLASS NAME           : Fcpn230
**        * INSTANCE NAME        : Fcpn230
************************************************************
***********************************************************************
* PROGRAM  : FCPN230                                "DS" "SS" "EW"
* SYSTEM   : CPS
* TITLE    : ANNUITANT STATEMENT
* GENERATED: ?
* FUNCTION : THIS PROGRAM WILL BUILD THE BODY PORTION OF STATEMENT
*
* HISTORY  :
*
* ------------  ---------  --------  ----------------------------------
*  01/18/2000 - LEON GURTOVNIK
*               ADD 'Electronik Warrants' PROCESS
*     11/2002 - ROXAN
*               ADDED CODES TO PRINT EGTRRA MESSAGE IN STATEMENT TO
*               ALTERNATE PAYEE/INSTITUTION.
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn230 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa200 pdaFcpa200;
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_I;

    private DbsGroup pnd_Ws_Egtrra;
    private DbsField pnd_Ws_Egtrra_Pnd_K;
    private DbsField pnd_Ws_Egtrra_Pnd_N;
    private DbsField pnd_Ws_Egtrra_Pnd_Ws_Amt;
    private DbsField pnd_Ws_Egtrra_Pnd_Ws_Ivc_Line;
    private DbsField pnd_Ws_Egtrra_Pnd_Ws_From;
    private DbsField pnd_Ws_Egtrra_Pnd_Ws_Lit;
    private DbsField pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc;
    private DbsField pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa200 = new PdaFcpa200(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws_Egtrra = localVariables.newGroupInRecord("pnd_Ws_Egtrra", "#WS-EGTRRA");
        pnd_Ws_Egtrra_Pnd_K = pnd_Ws_Egtrra.newFieldInGroup("pnd_Ws_Egtrra_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Egtrra_Pnd_N = pnd_Ws_Egtrra.newFieldInGroup("pnd_Ws_Egtrra_Pnd_N", "#N", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Egtrra_Pnd_Ws_Amt = pnd_Ws_Egtrra.newFieldArrayInGroup("pnd_Ws_Egtrra_Pnd_Ws_Amt", "#WS-AMT", FieldType.STRING, 15, new DbsArrayController(1, 
            2));
        pnd_Ws_Egtrra_Pnd_Ws_Ivc_Line = pnd_Ws_Egtrra.newFieldInGroup("pnd_Ws_Egtrra_Pnd_Ws_Ivc_Line", "#WS-IVC-LINE", FieldType.STRING, 143);
        pnd_Ws_Egtrra_Pnd_Ws_From = pnd_Ws_Egtrra.newFieldArrayInGroup("pnd_Ws_Egtrra_Pnd_Ws_From", "#WS-FROM", FieldType.STRING, 5, new DbsArrayController(1, 
            2));
        pnd_Ws_Egtrra_Pnd_Ws_Lit = pnd_Ws_Egtrra.newFieldArrayInGroup("pnd_Ws_Egtrra_Pnd_Ws_Lit", "#WS-LIT", FieldType.STRING, 5, new DbsArrayController(1, 
            2));
        pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc = pnd_Ws_Egtrra.newFieldInGroup("pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc", "#WS-TIAA-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc = pnd_Ws_Egtrra.newFieldInGroup("pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc", "#WS-CREF-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_Rec_1.setInitialValue(" ");
        pnd_I.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn230() throws Exception
    {
        super("Fcpn230");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        short decideConditionsMet247 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-PAYEE-CDE;//Natural: VALUE 'ANNT'
        if (condition((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ANNT"))))
        {
            decideConditionsMet247++;
            pnd_Ws_Rec_1.setValue("1!In accordance with your instructions, your payment has been sent to");                                                               //Natural: ASSIGN #WS-REC-1 := '1!In accordance with your instructions, your payment has been sent to'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" !this address.  A breakdown of this payment has been sent to your");                                                                  //Natural: ASSIGN #WS-REC-1 := ' !this address.  A breakdown of this payment has been sent to your'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" !residence address.");                                                                                                                //Natural: ASSIGN #WS-REC-1 := ' !residence address.'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: VALUE 'ALT1','ALT2'
        else if (condition((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ALT1") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ALT2"))))
        {
            decideConditionsMet247++;
            pnd_Ws_Rec_1.setValue("1!This individual has requested that we mail the attached check to you.");                                                             //Natural: ASSIGN #WS-REC-1 := '1!This individual has requested that we mail the attached check to you.'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            if (condition(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).notEquals(" ")))                                                           //Natural: IF PYMNT-EFT-ACCT-NBR ( 1 ) NE ' '
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "H'00'", "!It is to be credited to Account No.", "H'00'", pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1),  //Natural: COMPRESS H'00' '!It is to be credited to Account No.' H'00' PYMNT-EFT-ACCT-NBR ( 1 ) '.' INTO #WS-REC-1 LEAVING NO SPACE
                    "."));
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1.setValue(" !If you have any questions, please call our Telephone Counseling");                                                                   //Natural: ASSIGN #WS-REC-1 := ' !If you have any questions, please call our Telephone Counseling'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" !Center toll free at 1 800 842-2776.");                                                                                               //Natural: ASSIGN #WS-REC-1 := ' !Center toll free at 1 800 842-2776.'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            //*                                                    /* ROXAN 3.22.00
            if (condition(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1).getSubstring(1,2).equals("CR")))                                                    //Natural: IF SUBSTR ( #WS-NAME-N-ADDRESS.PYMNT-NME ( 1 ) ,1,2 ) = 'CR'
            {
                pnd_Ws_Rec_1.setValue("1!This individual has requested that we mail the attached check to you.");                                                         //Natural: ASSIGN #WS-REC-1 := '1!This individual has requested that we mail the attached check to you.'
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                if (condition(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).notEquals(" ")))                                                       //Natural: IF PYMNT-EFT-ACCT-NBR ( 1 ) NE ' '
                {
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "H'00'", "!It is to be credited to Account No.", "H'00'", pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1),  //Natural: COMPRESS H'00' '!It is to be credited to Account No.' H'00' PYMNT-EFT-ACCT-NBR ( 1 ) '.' INTO #WS-REC-1 LEAVING NO SPACE
                        "."));
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Rec_1.setValue(" !If you have any questions, please call our Telephone Counseling");                                                               //Natural: ASSIGN #WS-REC-1 := ' !If you have any questions, please call our Telephone Counseling'
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(" !Center toll free at 1 800 842-2776.");                                                                                           //Natural: ASSIGN #WS-REC-1 := ' !Center toll free at 1 800 842-2776.'
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                //*  END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue("1!The attached check represents the value payable to your institution");                                                           //Natural: ASSIGN #WS-REC-1 := '1!The attached check represents the value payable to your institution'
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "H'00'", "!for the repurchase of the following contracts:", "H'00'",                //Natural: COMPRESS H'00' '!for the repurchase of the following contracts:' H'00' #WS-HEADER-RECORD.CNTRCT-PPCN-NBR ',' H'00' #WS-HEADER-RECORD.CNTRCT-CREF-NBR INTO #WS-REC-1 LEAVING NO SPACE
                    pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(), ",", "H'00'", pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Cref_Nbr()));
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "H'00'", "!issued to", "H'00'", pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1),  //Natural: COMPRESS H'00' '!issued to' H'00' PH-FIRST-NAME ( 1 ) H'00' PH-MIDDLE-NAME ( 1 ) H'00' PH-LAST-NAME ( 1 ) '.' INTO #WS-REC-1 LEAVING NO SPACE
                    "H'00'", pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1), "H'00'", pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1), 
                    "."));
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Rec_1.setValue(" !");                                                                                                                                      //Natural: ASSIGN #WS-REC-1 := ' !'
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 3
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
        {
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *                                                              /* LEON
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))                                                                                 //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'EW'
        {
            pnd_Ws_Rec_1.setValue(" !Individual Services");                                                                                                               //Natural: ASSIGN #WS-REC-1 := ' !Individual Services'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" !");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' !'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *                                                              /* LEON
            if (condition((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("SS") && (pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30")        //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'SS' AND ( #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '30' OR = '31' )
                || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("31")))))
            {
                pnd_Ws_Rec_1.setValue(" !");                                                                                                                              //Natural: ASSIGN #WS-REC-1 := ' !'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(" !Single Sum Benefits");                                                                                                           //Natural: ASSIGN #WS-REC-1 := ' !Single Sum Benefits'
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" !Benefit Payment Services");                                                                                                          //Natural: ASSIGN #WS-REC-1 := ' !Benefit Payment Services'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            //*                                        EGTRRA ONLY - 11/26/02 ROXAN
            //*                                        CODE COPIED FROM FCPN246
            //*  BEGIN -------------------------------------------------------------
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Egtrra_Eligibility_Ind().equals("Y")))                                                                       //Natural: IF EGTRRA-ELIGIBILITY-IND = 'Y'
            {
                pnd_Ws_Egtrra_Pnd_Ws_From.getValue("*").reset();                                                                                                          //Natural: RESET #WS-FROM ( * ) #WS-LIT ( * )
                pnd_Ws_Egtrra_Pnd_Ws_Lit.getValue("*").reset();
                FOR02:                                                                                                                                                    //Natural: FOR #K = 1 TO #WS-CNTR-INV-ACCT
                for (pnd_Ws_Egtrra_Pnd_K.setValue(1); condition(pnd_Ws_Egtrra_Pnd_K.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_Ws_Egtrra_Pnd_K.nadd(1))
                {
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Ind().getValue(pnd_Ws_Egtrra_Pnd_K).equals("2") && pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Egtrra_Pnd_K).greater(getZero()))) //Natural: IF #WS-OCCURS.INV-ACCT-IVC-IND ( #K ) = '2' AND #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) > 0
                    {
                        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_Ws_Egtrra_Pnd_K));              //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = #WS-OCCURS.INV-ACCT-CDE ( #K )
                        //*  LZ
                        DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                        //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                        if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        //*  LZ
                        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                         //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
                        {
                            pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Egtrra_Pnd_K));                             //Natural: ADD #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) TO #WS-TIAA-IVC
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Egtrra_Pnd_K));                             //Natural: ADD #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) TO #WS-CREF-IVC
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  RE : RE-SEQUENCE COMMANDS TO WRITE IN NEXT LINE CONT. OF MSG
                //*       WHEN BOTH TIAA AND CREF IVC IS FILLED.    12/11/02  - ROXAN
                short decideConditionsMet331 = 0;                                                                                                                         //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #WS-TIAA-IVC > 0
                if (condition(pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc.greater(getZero())))
                {
                    decideConditionsMet331++;
                    pnd_Ws_Egtrra_Pnd_N.nadd(1);                                                                                                                          //Natural: ADD 1 TO #N
                    pnd_Ws_Egtrra_Pnd_Ws_Amt.getValue(pnd_Ws_Egtrra_Pnd_N).setValueEdited(pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));          //Natural: MOVE EDITED #WS-TIAA-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #N )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Egtrra_Pnd_Ws_Amt.getValue(pnd_Ws_Egtrra_Pnd_N),true), new ExamineSearch("X", true), new                     //Natural: EXAMINE FULL #WS-AMT ( #N ) FOR FULL 'X' DELETE
                        ExamineDelete());
                    pnd_Ws_Egtrra_Pnd_Ws_From.getValue(pnd_Ws_Egtrra_Pnd_N).setValue("from");                                                                             //Natural: ASSIGN #WS-FROM ( #N ) = 'from'
                    pnd_Ws_Egtrra_Pnd_Ws_Lit.getValue(pnd_Ws_Egtrra_Pnd_N).setValue("TIAA");                                                                              //Natural: ASSIGN #WS-LIT ( #N ) = 'TIAA'
                }                                                                                                                                                         //Natural: WHEN #WS-CREF-IVC > 0
                if (condition(pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc.greater(getZero())))
                {
                    decideConditionsMet331++;
                    pnd_Ws_Egtrra_Pnd_N.nadd(1);                                                                                                                          //Natural: ADD 1 TO #N
                    pnd_Ws_Egtrra_Pnd_Ws_Amt.getValue(pnd_Ws_Egtrra_Pnd_N).setValueEdited(pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));          //Natural: MOVE EDITED #WS-CREF-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #N )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Egtrra_Pnd_Ws_Amt.getValue(pnd_Ws_Egtrra_Pnd_N),true), new ExamineSearch("X", true), new                     //Natural: EXAMINE FULL #WS-AMT ( #N ) FOR FULL 'X' DELETE
                        ExamineDelete());
                    pnd_Ws_Egtrra_Pnd_Ws_From.getValue(pnd_Ws_Egtrra_Pnd_N).setValue("from");                                                                             //Natural: ASSIGN #WS-FROM ( #N ) = 'from'
                    pnd_Ws_Egtrra_Pnd_Ws_Lit.getValue(pnd_Ws_Egtrra_Pnd_N).setValue("CREF");                                                                              //Natural: ASSIGN #WS-LIT ( #N ) = 'CREF'
                }                                                                                                                                                         //Natural: WHEN #WS-TIAA-IVC = 0 AND #WS-CREF-IVC = 0
                if (condition(pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc.equals(getZero()) && pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc.equals(getZero())))
                {
                    decideConditionsMet331++;
                    pnd_Ws_Egtrra_Pnd_Ws_Ivc_Line.setValue(DbsUtil.compress("*7Payment does not have after-tax contributions"));                                          //Natural: COMPRESS '*7Payment does not have after-tax contributions' INTO #WS-IVC-LINE
                    //* ***       '*!Payment does not have after-tax contributions'
                }                                                                                                                                                         //Natural: WHEN #WS-TIAA-IVC > 0 OR #WS-CREF-IVC > 0
                if (condition(pnd_Ws_Egtrra_Pnd_Ws_Tiaa_Ivc.greater(getZero()) || pnd_Ws_Egtrra_Pnd_Ws_Cref_Ivc.greater(getZero())))
                {
                    decideConditionsMet331++;
                    pnd_Ws_Egtrra_Pnd_Ws_Ivc_Line.setValue(DbsUtil.compress("*7Payment includes after-tax contributions of:", pnd_Ws_Egtrra_Pnd_Ws_Amt.getValue(1),       //Natural: COMPRESS '*7Payment includes after-tax contributions of:' #WS-AMT ( 1 ) #WS-FROM ( 1 ) #WS-LIT ( 1 ) #WS-AMT ( 2 ) #WS-FROM ( 2 ) #WS-LIT ( 2 ) INTO #WS-IVC-LINE
                        pnd_Ws_Egtrra_Pnd_Ws_From.getValue(1), pnd_Ws_Egtrra_Pnd_Ws_Lit.getValue(1), pnd_Ws_Egtrra_Pnd_Ws_Amt.getValue(2), pnd_Ws_Egtrra_Pnd_Ws_From.getValue(2), 
                        pnd_Ws_Egtrra_Pnd_Ws_Lit.getValue(2)));
                    //* ***      '*!Payment includes after-tax contributions of:'
                    //* *        #WS-AMT(1) #WS-FROM(1) #WS-LIT(1) H'00'
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet331 > 0))
                {
                    pnd_Ws_Rec_1.setValue(" ");                                                                                                                           //Natural: ASSIGN #WS-REC-1 := ' '
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                    pnd_Ws_Rec_1.setValue(pnd_Ws_Egtrra_Pnd_Ws_Ivc_Line);                                                                                                 //Natural: ASSIGN #WS-REC-1 := #WS-IVC-LINE
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                    //Natural: EXAMINE #WS-REC-1 FOR '*' REPLACE ' '
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet331 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*     IF  #WS-TIAA-IVC > 0 AND #WS-CREF-IVC > 0
                //*         COMPRESS '*!'
                //*         #WS-AMT(2) #WS-FROM(2) #WS-LIT(2) INTO #WS-IVC-LINE
                //*         #WS-REC-1  := #WS-IVC-LINE
                //*         EXAMINE #WS-REC-1 FOR '*' REPLACE ' '
                //*         WRITE WORK FILE 8 #WS-REC-1
                //*     END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                        EGTRRA ONLY - 11/26/02 ROXAN
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
