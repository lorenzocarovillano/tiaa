/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:04 AM
**        * FROM NATURAL SUBPROGRAM : Fcpncntu
************************************************************
**        * FILE NAME            : Fcpncntu.java
**        * CLASS NAME           : Fcpncntu
**        * INSTANCE NAME        : Fcpncntu
************************************************************
************************************************************************
* PROGRAM  : FCPNCNTU
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* FUNCTION : FCP-CONS-CNTRL FILE UPDATE.
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpncntu extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpacntr pdaFcpacntr;
    private LdaFcplcntu ldaFcplcntu;

    // Local Variables
    public DbsRecord localVariables;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplcntu = new LdaFcplcntu();
        registerRecord(ldaFcplcntu);
        registerRecord(ldaFcplcntu.getVw_fcp_Cons_Cntrl());

        // parameters
        parameters = new DbsRecord();
        pdaFcpacntr = new PdaFcpacntr(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplcntu.initializeValues();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpncntu() throws Exception
    {
        super("Fcpncntu");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        GC1:                                                                                                                                                              //Natural: GET FCP-CONS-CNTRL #CNTRL-ISN
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().readByID(pdaFcpacntr.getCntrl_Work_Fields_Pnd_Cntrl_Isn().getLong(), "GC1");
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().setValuesByName(pdaFcpacntr.getCntrl());                                                                                       //Natural: MOVE BY NAME CNTRL TO FCP-CONS-CNTRL
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().updateDBRow("GC1");                                                                                                            //Natural: UPDATE ( GC1. )
    }

    //
}
