/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:41 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn803t
************************************************************
**        * FILE NAME            : Fcpn803t.java
**        * CLASS NAME           : Fcpn803t
**        * INSTANCE NAME        : Fcpn803t
************************************************************
************************************************************************
* SUBPROGRAM : FCPN803T
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AP" ANNUITANT STATEMENTS - CONTRACT HEADERS
*
* 01/27/99 - R. CARREON
*            NEW HEADER FOR PA SELECT
* 05/25/00   MCGEE - NEW HEADERS FOR SPIA PASELECT
* 12/20/05   RAMANA  ANNE - ADDED NEW PARAMETER DATA AREA FOR PAYEE
*                           MATCH PROJECT
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
* 12/2015    :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803B
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn803t extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803c pdaFcpa803c;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa803 pdaFcpa803;
    private LdaFcpl803t ldaFcpl803t;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntrct_Ins_Type;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl803t = new LdaFcpl803t();
        registerRecord(ldaFcpl803t);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803c = new PdaFcpa803c(parameters);
        pdaFcpa801b = new PdaFcpa801b(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pnd_Ws_Cntrct_Ins_Type = parameters.newFieldInRecord("pnd_Ws_Cntrct_Ins_Type", "#WS-CNTRCT-INS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Cntrct_Ins_Type.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803t.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn803t() throws Exception
    {
        super("Fcpn803t");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(2)); pnd_Ws_Pnd_I.nadd(1))
        {
            //*  NEW PA SELECT
            if (condition(pnd_Ws_Cntrct_Ins_Type.equals("S")))                                                                                                            //Natural: IF #WS-CNTRCT-INS-TYPE = 'S'
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Cntrct_Pa_Select().getValue(pnd_Ws_Pnd_I));                           //Natural: ASSIGN #OUTPUT-REC := #CNTRCT-PA-SELECT ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   NEW SPIA
                if (condition(pnd_Ws_Cntrct_Ins_Type.equals("M")))                                                                                                        //Natural: IF #WS-CNTRCT-INS-TYPE = 'M'
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Cntrct_Spia().getValue(pnd_Ws_Pnd_I));                            //Natural: ASSIGN #OUTPUT-REC := #CNTRCT-SPIA ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().equals("T")))                                                                 //Natural: IF CNTRCT-COMPANY-CDE = 'T'
                    {
                        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr().getValue(pnd_Ws_Pnd_I));                    //Natural: ASSIGN #OUTPUT-REC := #CNTRCT-TIAA-HDR ( #I )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr().getValue(pnd_Ws_Pnd_I));                    //Natural: ASSIGN #OUTPUT-REC := #CNTRCT-CREF-HDR ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_I.equals(1) && pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().getBoolean()))                                                            //Natural: IF #I = 1 AND #NEWPAGE-IND
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue("1");                                                                                                         //Natural: ASSIGN #CC := '1'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().getBoolean()))                                                                                          //Natural: IF #NEWPAGE-IND
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                            //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Pymnt_Hdr());                                                             //Natural: ASSIGN #OUTPUT-REC := #PYMNT-HDR
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().getBoolean()))                                                                                    //Natural: IF #DPI-IND
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                        //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Dpi_Hdr());                                                           //Natural: ASSIGN #OUTPUT-REC := #DPI-HDR
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean()))                                                                              //Natural: IF #PYMNT-DED-IND
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(1));                                        //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Ded_Hdr());                                                           //Natural: ASSIGN #OUTPUT-REC := #DED-HDR
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 2
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(2)); pnd_Ws_Pnd_I.nadd(1))
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Net_Hdr().getValue(pnd_Ws_Pnd_I));                                    //Natural: ASSIGN #OUTPUT-REC := #NET-HDR ( #I )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(! (pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().getBoolean())))                                                                                //Natural: IF NOT #DPI-IND
            {
                FOR03:                                                                                                                                                    //Natural: FOR #I = 1 TO 2
                for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(2)); pnd_Ws_Pnd_I.nadd(1))
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( #I )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean())))                                                                          //Natural: IF NOT #PYMNT-DED-IND
            {
                FOR04:                                                                                                                                                    //Natural: FOR #I = 1 TO 2
                for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(2)); pnd_Ws_Pnd_I.nadd(1))
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( #I )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803t.getPnd_Fcpl803t_Pnd_Blank_Hdr_Line().getValue(3));                                            //Natural: ASSIGN #OUTPUT-REC := #BLANK-HDR-LINE ( 3 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
