/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:40 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn245
************************************************************
**        * FILE NAME            : Fcpn245.java
**        * CLASS NAME           : Fcpn245
**        * INSTANCE NAME        : Fcpn245
************************************************************
***********************************************************************
* LEON SILBERSTEIN (7/95): THIS IS THE REAL-ESTATE CHANGES VERSION.
*  FCPN245
** 08/11/98 GLORY PHILIP
**        - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
** 3/14/02- USE TX-ELCT-TRGGR TO TEST FOR NRA
**   11/02- R. CARREON   STOW EGTRRA CHANGES IN PDA
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn245 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa200 pdaFcpa200;
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_1;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column;
    private DbsField pnd_Ws_Rec_2;

    private DbsGroup pnd_Ws_Rec_2__R_Field_2;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name;
    private DbsField pnd_Ws_First_Time;
    private DbsField pnd_C;
    private DbsField pnd_F;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_Cntrct_Prefix;
    private DbsField pnd_Ws_Fielda;
    private DbsField pnd_Ws_Fieldn;

    private DbsGroup pnd_Ws_Fieldn__R_Field_3;
    private DbsField pnd_Ws_Fieldn_Pnd_Ws_Int;
    private DbsField pnd_Ws_Fieldn_Pnd_Ws_Rem;
    private DbsField pnd_Ws_Masked_Amt;
    private DbsField pnd_Ws_Amount_1;
    private DbsField pnd_Ws_Amount_2;
    private DbsField pnd_Ws_Letter;
    private DbsField pnd_Ws_Legend;
    private DbsField pnd_Ws_Literal;
    private DbsField pnd_Ws_Printed;
    private DbsField pnd_Ws_Dpi_Hdr_Written;
    private DbsField pnd_Ws_Deduct_Hdr_Written;
    private DbsField pnd_Ws_Printed_Cref_Heading;
    private DbsField pnd_Ws_Printed_Tiaa_Heading;
    private DbsField pnd_Ws_Tot_Payment_Amt;
    private DbsField pnd_Ws_Tot_Dpi_Amt;
    private DbsField pnd_Ws_Tot_Ded_Amt;
    private DbsField pnd_Ws_Tot_Check_Amt;
    private DbsField pnd_Ws_Cref_Tot_End_Accum;
    private DbsField pnd_Ws_Cref_Tot_Payment;
    private DbsField pnd_Ws_Cref_Tot_Dpi;
    private DbsField pnd_Ws_Cref_Tot_Ded;
    private DbsField pnd_Ws_Cref_Tot_Check;
    private DbsField pnd_Ws_Grand_Tot_End_Accum;
    private DbsField pnd_Ws_Grand_Tot_Payment;
    private DbsField pnd_Ws_Grand_Tot_Dpi;
    private DbsField pnd_Ws_Grand_Tot_Ded;
    private DbsField pnd_Ws_Grand_Tot_Check;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa200 = new PdaFcpa200(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Side_Printed = parameters.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Side_Printed.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_1", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc = pnd_Ws_Rec_1__R_Field_1.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc", "#WS-REC1-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_1_Pnd_Ws_Column = pnd_Ws_Rec_1__R_Field_1.newFieldArrayInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column", "#WS-COLUMN", FieldType.STRING, 15, new 
            DbsArrayController(1, 6));
        pnd_Ws_Rec_2 = localVariables.newFieldInRecord("pnd_Ws_Rec_2", "#WS-REC-2", FieldType.STRING, 143);

        pnd_Ws_Rec_2__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Rec_2__R_Field_2", "REDEFINE", pnd_Ws_Rec_2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc", "#WS-REC2-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler", "#WS-REC2-FILLER", FieldType.STRING, 
            35);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name", "#WS-REC2-NAME", FieldType.STRING, 38);
        pnd_Ws_First_Time = localVariables.newFieldInRecord("pnd_Ws_First_Time", "#WS-FIRST-TIME", FieldType.STRING, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_Cntrct_Prefix = localVariables.newFieldInRecord("pnd_Cntrct_Prefix", "#CNTRCT-PREFIX", FieldType.STRING, 2);
        pnd_Ws_Fielda = localVariables.newFieldInRecord("pnd_Ws_Fielda", "#WS-FIELDA", FieldType.STRING, 12);
        pnd_Ws_Fieldn = localVariables.newFieldInRecord("pnd_Ws_Fieldn", "#WS-FIELDN", FieldType.NUMERIC, 11, 2);

        pnd_Ws_Fieldn__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Fieldn__R_Field_3", "REDEFINE", pnd_Ws_Fieldn);
        pnd_Ws_Fieldn_Pnd_Ws_Int = pnd_Ws_Fieldn__R_Field_3.newFieldInGroup("pnd_Ws_Fieldn_Pnd_Ws_Int", "#WS-INT", FieldType.NUMERIC, 9);
        pnd_Ws_Fieldn_Pnd_Ws_Rem = pnd_Ws_Fieldn__R_Field_3.newFieldInGroup("pnd_Ws_Fieldn_Pnd_Ws_Rem", "#WS-REM", FieldType.STRING, 2);
        pnd_Ws_Masked_Amt = localVariables.newFieldInRecord("pnd_Ws_Masked_Amt", "#WS-MASKED-AMT", FieldType.STRING, 13);
        pnd_Ws_Amount_1 = localVariables.newFieldInRecord("pnd_Ws_Amount_1", "#WS-AMOUNT-1", FieldType.STRING, 15);
        pnd_Ws_Amount_2 = localVariables.newFieldInRecord("pnd_Ws_Amount_2", "#WS-AMOUNT-2", FieldType.STRING, 15);
        pnd_Ws_Letter = localVariables.newFieldInRecord("pnd_Ws_Letter", "#WS-LETTER", FieldType.STRING, 1);
        pnd_Ws_Legend = localVariables.newFieldInRecord("pnd_Ws_Legend", "#WS-LEGEND", FieldType.STRING, 4);
        pnd_Ws_Literal = localVariables.newFieldInRecord("pnd_Ws_Literal", "#WS-LITERAL", FieldType.STRING, 12);
        pnd_Ws_Printed = localVariables.newFieldInRecord("pnd_Ws_Printed", "#WS-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Dpi_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Dpi_Hdr_Written", "#WS-DPI-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Deduct_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Deduct_Hdr_Written", "#WS-DEDUCT-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Printed_Cref_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Cref_Heading", "#WS-PRINTED-CREF-HEADING", FieldType.STRING, 1);
        pnd_Ws_Printed_Tiaa_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Tiaa_Heading", "#WS-PRINTED-TIAA-HEADING", FieldType.STRING, 1);
        pnd_Ws_Tot_Payment_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Payment_Amt", "#WS-TOT-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Dpi_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Dpi_Amt", "#WS-TOT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Ded_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Ded_Amt", "#WS-TOT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Check_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Check_Amt", "#WS-TOT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_End_Accum", "#WS-CREF-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Ws_Cref_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Payment", "#WS-CREF-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Dpi", "#WS-CREF-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Ded", "#WS-CREF-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Check", "#WS-CREF-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_End_Accum", "#WS-GRAND-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Grand_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Payment", "#WS-GRAND-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Dpi", "#WS-GRAND-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Ded", "#WS-GRAND-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Check", "#WS-GRAND-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_First_Time.setInitialValue("Y");
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_Ws_Printed.setInitialValue("N");
        pnd_Ws_Dpi_Hdr_Written.setInitialValue("N");
        pnd_Ws_Deduct_Hdr_Written.setInitialValue("N");
        pnd_Ws_Printed_Cref_Heading.setInitialValue("N");
        pnd_Ws_Printed_Tiaa_Heading.setInitialValue("N");
        pnd_Ws_Tot_Payment_Amt.setInitialValue(0);
        pnd_Ws_Tot_Dpi_Amt.setInitialValue(0);
        pnd_Ws_Tot_Ded_Amt.setInitialValue(0);
        pnd_Ws_Tot_Check_Amt.setInitialValue(0);
        pnd_Ws_Cref_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Cref_Tot_Payment.setInitialValue(0);
        pnd_Ws_Cref_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Cref_Tot_Ded.setInitialValue(0);
        pnd_Ws_Cref_Tot_Check.setInitialValue(0);
        pnd_Ws_Grand_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Grand_Tot_Payment.setInitialValue(0);
        pnd_Ws_Grand_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Grand_Tot_Ded.setInitialValue(0);
        pnd_Ws_Grand_Tot_Check.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn245() throws Exception
    {
        super("Fcpn245");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        FOR01:                                                                                                                                                            //Natural: FOR #J 1 #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J).greater(getZero())))                                                             //Natural: IF INV-ACCT-DPI-AMT ( #J ) > 0
            {
                pnd_Ws_Dpi_Hdr_Written.setValue("Y");                                                                                                                     //Natural: ASSIGN #WS-DPI-HDR-WRITTEN = 'Y'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J).greater(getZero()) || pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J).greater(getZero())  //Natural: IF INV-ACCT-FDRL-TAX-AMT ( #J ) > 0 OR INV-ACCT-STATE-TAX-AMT ( #J ) > 0 OR INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
                || pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J).greater(getZero())))
            {
                pnd_Ws_Deduct_Hdr_Written.setValue("Y");                                                                                                                  //Natural: ASSIGN #WS-DEDUCT-HDR-WRITTEN = 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #J 1 #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            FOR03:                                                                                                                                                        //Natural: FOR #K 1 #CNTR-DEDUCTIONS ( #J )
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_J))); pnd_K.nadd(1))
            {
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,pnd_K).greater(getZero())))                                                      //Natural: IF PYMNT-DED-AMT ( #J,#K ) > 0
                {
                    pnd_Ws_Deduct_Hdr_Written.setValue("Y");                                                                                                              //Natural: ASSIGN #WS-DEDUCT-HDR-WRITTEN = 'Y'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #J 1 #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).notEquals(pnd_Ws_Side_Printed)))                                                      //Natural: IF #WS-SIDE ( #J ) NE #WS-SIDE-PRINTED
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pdaFcpa200.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).equals("2") || pdaFcpa200.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).equals("3"))  //Natural: IF #WS-SIDE ( #J ) = '2' OR = '3' AND #WS-PRINTED = 'N'
                    && pnd_Ws_Printed.equals("N"))))
                {
                    pnd_Ws_Printed.setValue("Y");                                                                                                                         //Natural: ASSIGN #WS-PRINTED = 'Y'
                    pnd_Ws_Rec_1.setValue("15CONTINUED FROM PREVIOUS PAGE");                                                                                              //Natural: ASSIGN #WS-REC-1 = '15CONTINUED FROM PREVIOUS PAGE'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J));                                    //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := INV-ACCT-CDE ( #J )
                DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                pnd_K.reset();                                                                                                                                            //Natural: RESET #K
                //*  05-13-96 FRANK
                short decideConditionsMet319 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #COMPANY-CDE = 'T' AND #WS-PRINTED-TIAA-HEADING = 'N'
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T") && pnd_Ws_Printed_Tiaa_Heading.equals("N")))
                {
                    decideConditionsMet319++;
                    pnd_Ws_Rec_1.setValue("14             TIAA");                                                                                                         //Natural: ASSIGN #WS-REC-1 = '14             TIAA'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4             Contract");                                                                                                     //Natural: ASSIGN #WS-REC-1 = ' 4             Contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  05-13-96 FRANK
                    pnd_Ws_Printed_Tiaa_Heading.setValue("Y");                                                                                                            //Natural: ASSIGN #WS-PRINTED-TIAA-HEADING = 'Y'
                }                                                                                                                                                         //Natural: WHEN #COMPANY-CDE = 'C' AND #WS-PRINTED-CREF-HEADING = 'N'
                else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C") && pnd_Ws_Printed_Cref_Heading.equals("N")))
                {
                    decideConditionsMet319++;
                    if (condition(pnd_Ws_Printed_Tiaa_Heading.equals("Y")))                                                                                               //Natural: IF #WS-PRINTED-TIAA-HEADING = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("-4             CREF");                                                                                                     //Natural: ASSIGN #WS-REC-1 = '-4             CREF'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1.setValue("14             CREF");                                                                                                     //Natural: ASSIGN #WS-REC-1 = '14             CREF'
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4             Certificate");                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4             Certificate'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Printed_Cref_Heading.setValue("Y");                                                                                                            //Natural: ASSIGN #WS-PRINTED-CREF-HEADING = 'Y'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                             //Natural: IF #WS-FIRST-TIME = 'Y'
                {
                    pnd_Ws_First_Time.setValue("N");                                                                                                                      //Natural: ASSIGN #WS-FIRST-TIME = 'N'
                    pnd_Ws_Rec_1.setValue("14           Payment");                                                                                                        //Natural: ASSIGN #WS-REC-1 = '14           Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                    //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("14         Interest");                                                                                                     //Natural: ASSIGN #WS-REC-1 = '14         Interest'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                 //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("14       Deductions");                                                                                                     //Natural: ASSIGN #WS-REC-1 = '14       Deductions'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Rec_1.setValue("14         Net");                                                                                                              //Natural: ASSIGN #WS-REC-1 = '14         Net'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4         Payment");                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4         Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Dpi_Hdr_Written.equals("N")))                                                                                                    //Natural: IF #WS-DPI-HDR-WRITTEN = 'N'
                    {
                        pnd_Ws_Rec_1.setValue("14");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Deduct_Hdr_Written.equals("N")))                                                                                                 //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'N'
                    {
                        pnd_Ws_Rec_1.setValue("14");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Rec_1.setValue("15");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Tot_Payment_Amt.reset();                                                                                                                           //Natural: RESET #WS-TOT-PAYMENT-AMT #WS-TOT-DPI-AMT #WS-TOT-DED-AMT #WS-TOT-CHECK-AMT
                pnd_Ws_Tot_Dpi_Amt.reset();
                pnd_Ws_Tot_Ded_Amt.reset();
                pnd_Ws_Tot_Check_Amt.reset();
                pnd_Ws_Tot_Payment_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Payment_Amt), pnd_Ws_Tot_Payment_Amt.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J))); //Natural: ADD INV-ACCT-SETTL-AMT ( #J ) INV-ACCT-DVDND-AMT ( #J ) TO #WS-TOT-PAYMENT-AMT
                pnd_Ws_Tot_Dpi_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J));                                                                  //Natural: ADD INV-ACCT-DPI-AMT ( #J ) TO #WS-TOT-DPI-AMT
                pnd_Ws_Tot_Ded_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Ded_Amt), pnd_Ws_Tot_Ded_Amt.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J))); //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #J ) INV-ACCT-STATE-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
                pnd_Ws_Tot_Ded_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J));                                                            //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
                pnd_Ws_Tot_Ded_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,"*"));                                                                 //Natural: ADD PYMNT-DED-AMT ( #J,* ) TO #WS-TOT-DED-AMT
                pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                          //Natural: ADD PYMNT-CHECK-AMT TO #WS-TOT-CHECK-AMT
                pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Tot_Payment_Amt);                                                                                                    //Natural: ADD #WS-TOT-PAYMENT-AMT TO #WS-GRAND-TOT-PAYMENT
                pnd_Ws_Grand_Tot_Dpi.nadd(pnd_Ws_Tot_Dpi_Amt);                                                                                                            //Natural: ADD #WS-TOT-DPI-AMT TO #WS-GRAND-TOT-DPI
                pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Tot_Ded_Amt);                                                                                                            //Natural: ADD #WS-TOT-DED-AMT TO #WS-GRAND-TOT-DED
                pnd_Ws_Grand_Tot_Check.nadd(pnd_Ws_Tot_Check_Amt);                                                                                                        //Natural: ADD #WS-TOT-CHECK-AMT TO #WS-GRAND-TOT-CHECK
                pnd_F.reset();                                                                                                                                            //Natural: RESET #F
                pnd_Ws_Rec_1.reset();                                                                                                                                     //Natural: RESET #WS-REC-1
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                               //Natural: ASSIGN #WS-REC1-CC = '05'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr().getValue(pnd_J),new ReportEditMask("XXXXXXX-X")); //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-COLUMN ( #F )
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                              //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                 //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
                if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                        //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
                {
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'")); //Natural: MOVE EDITED INV-ACCT-DPI-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                     //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
                {
                    if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                 //Natural: IF #WS-TOT-DED-AMT > 0
                    {
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZZ,ZZ9.99'****'"));                         //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZZ,ZZ9.99'****' ) TO #WS-COLUMN ( #F )
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'")); //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                   //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("*", true), new ExamineReplace(" "));                 //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '*' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR FEDERAL TAX
                pnd_F.setValue(1);                                                                                                                                        //Natural: ASSIGN #F = 1
                pnd_Ws_Rec_1.setValue(" 5");                                                                                                                              //Natural: ASSIGN #WS-REC-1 := ' 5'
                setValueToSubstring(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1(),pnd_Ws_Rec_1,10,15);                                                                   //Natural: MOVE #STMNT-LINE-1 TO SUBSTR ( #WS-REC-1,10,15 )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                  //Natural: IF #DIVIDENDS
                {
                    pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99'@'"));               //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99'@' ) TO #WS-AMOUNT-1
                    pnd_Ws_Literal.setValue("+Contractual");                                                                                                              //Natural: ASSIGN #WS-LITERAL = '+Contractual'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Unit_Qty().getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.999'*'"));               //Natural: MOVE EDITED INV-ACCT-UNIT-QTY ( #J ) ( EM = Z,ZZZ,ZZ9.999'*' ) TO #WS-AMOUNT-1
                    pnd_Ws_Literal.setValue("+Units");                                                                                                                    //Natural: ASSIGN #WS-LITERAL = '+Units'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Amount_2.reset();                                                                                                                                  //Natural: RESET #WS-AMOUNT-2
                pnd_Ws_Legend.reset();                                                                                                                                    //Natural: RESET #WS-LEGEND
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                    //Natural: IF INV-ACCT-FDRL-TAX-AMT ( #J ) > 0
                {
                    pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J),new ReportEditMask("ZZZZ,ZZ9.99"));                //Natural: MOVE EDITED INV-ACCT-FDRL-TAX-AMT ( #J ) ( EM = ZZZZ,ZZ9.99 ) TO #WS-AMOUNT-2
                                                                                                                                                                          //Natural: PERFORM FORMAT-FEDERAL-LEGEND
                    sub_Format_Federal_Legend();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                sub_Format_Deductions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR STATE TAX
                pnd_F.setValue(1);                                                                                                                                        //Natural: ASSIGN #F = 1
                pnd_Ws_Rec_1.setValue(" 5");                                                                                                                              //Natural: ASSIGN #WS-REC-1 := ' 5'
                setValueToSubstring(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2(),pnd_Ws_Rec_1,10,15);                                                                   //Natural: MOVE #STMNT-LINE-2 TO SUBSTR ( #WS-REC-1,10,15 )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                  //Natural: IF #DIVIDENDS
                {
                    pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99'@'"));               //Natural: MOVE EDITED INV-ACCT-DVDND-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99'@' ) TO #WS-AMOUNT-1
                    pnd_Ws_Literal.setValue("+Dividend");                                                                                                                 //Natural: ASSIGN #WS-LITERAL = '+Dividend'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Unit_Value().getValue(pnd_J),new ReportEditMask("ZZ,ZZ9.9999"));                  //Natural: MOVE EDITED INV-ACCT-UNIT-VALUE ( #J ) ( EM = ZZ,ZZ9.9999 ) TO #WS-AMOUNT-1
                    pnd_Ws_Literal.setValue("+Unit Value");                                                                                                               //Natural: ASSIGN #WS-LITERAL = '+Unit Value'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Amount_2.reset();                                                                                                                                  //Natural: RESET #WS-AMOUNT-2
                pnd_Ws_Legend.reset();                                                                                                                                    //Natural: RESET #WS-LEGEND
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                   //Natural: IF INV-ACCT-STATE-TAX-AMT ( #J ) > 0
                {
                    pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J),new ReportEditMask("ZZZZ,ZZ9.99"));               //Natural: MOVE EDITED INV-ACCT-STATE-TAX-AMT ( #J ) ( EM = ZZZZ,ZZ9.99 ) TO #WS-AMOUNT-2
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEGEND
                    sub_Format_State_Legend();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                sub_Format_Deductions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR LOCAL TAXES
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                   //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
                {
                    pnd_F.setValue(1);                                                                                                                                    //Natural: ASSIGN #F = 1
                    pnd_Ws_Rec_1.reset();                                                                                                                                 //Natural: RESET #WS-REC-1
                    pnd_Ws_Rec_1.setValue(" 5");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Amount_1.setValue(" ");                                                                                                                        //Natural: ASSIGN #WS-AMOUNT-1 = ' '
                    pnd_Ws_Literal.setValue(" ");                                                                                                                         //Natural: ASSIGN #WS-LITERAL = ' '
                    pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J),new ReportEditMask("ZZZZ,ZZ9.99"));               //Natural: MOVE EDITED INV-ACCT-LOCAL-TAX-AMT ( #J ) ( EM = ZZZZ,ZZ9.99 ) TO #WS-AMOUNT-2
                    pnd_Ws_Legend.setValue("(L)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(L)*'
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                    sub_Format_Deductions();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR DEDUCTIONS
                pnd_L.reset();                                                                                                                                            //Natural: RESET #L
                FOR05:                                                                                                                                                    //Natural: FOR #C = 1 TO #CNTR-DEDUCTIONS ( #J )
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_J))); pnd_C.nadd(1))
                {
                    pnd_F.setValue(1);                                                                                                                                    //Natural: ASSIGN #F = 1
                    pnd_L.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #L
                    pnd_Ws_Rec_1.reset();                                                                                                                                 //Natural: RESET #WS-REC-1
                    pnd_Ws_Rec_1.setValue(" 5");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Amount_1.setValue(" ");                                                                                                                        //Natural: ASSIGN #WS-AMOUNT-1 = ' '
                    pnd_Ws_Literal.setValue(" ");                                                                                                                         //Natural: ASSIGN #WS-LITERAL = ' '
                    pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,pnd_C),new ReportEditMask("ZZZZ,ZZ9.99"));                  //Natural: MOVE EDITED PYMNT-DED-AMT ( #J,#C ) ( EM = ZZZZ,ZZ9.99 ) TO #WS-AMOUNT-2
                    //*      COMPRESS  '('  PYMNT-DED-CDE (#J,#C)  ')*'
                    pnd_Ws_Legend.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_L, ")*"));                                                            //Natural: COMPRESS '(' #L ')*' INTO #WS-LEGEND LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                    sub_Format_Deductions();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        if (condition(pnd_Ws_Side_Printed.notEquals(pdaFcpa200.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                               //Natural: IF #WS-SIDE-PRINTED NE #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_F.reset();                                                                                                                                                //Natural: RESET #F
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '06'
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Side_Printed.less(pdaFcpa200.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                    //Natural: IF #WS-SIDE-PRINTED LT #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Rec_1.setValue("06This statement is continued on the next page");                                                                                      //Natural: ASSIGN #WS-REC-1 = '06This statement is continued on the next page'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Side_Printed.equals(pdaFcpa200.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                  //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_Payment.reset();                                                                                                                             //Natural: RESET #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Dpi.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            FOR06:                                                                                                                                                        //Natural: FOR #J = 1 TO #WS-CNTR-INV-ACCT
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
            {
                pnd_Ws_Grand_Tot_Payment.compute(new ComputeParameters(false, pnd_Ws_Grand_Tot_Payment), pnd_Ws_Grand_Tot_Payment.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J))); //Natural: ADD INV-ACCT-SETTL-AMT ( #J ) INV-ACCT-DVDND-AMT ( #J ) TO #WS-GRAND-TOT-PAYMENT
                pnd_Ws_Grand_Tot_Dpi.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J));                                                                //Natural: ADD INV-ACCT-DPI-AMT ( #J ) TO #WS-GRAND-TOT-DPI
                pnd_Ws_Grand_Tot_Ded.compute(new ComputeParameters(false, pnd_Ws_Grand_Tot_Ded), pnd_Ws_Grand_Tot_Ded.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J))); //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #J ) INV-ACCT-STATE-TAX-AMT ( #J ) INV-ACCT-LOCAL-TAX-AMT ( #J ) TO #WS-GRAND-TOT-DED
                pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,"*"));                                                               //Natural: ADD PYMNT-DED-AMT ( #J,* ) TO #WS-GRAND-TOT-DED
                pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J));                                                          //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #J ) TO #WS-TOT-CHECK-AMT
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_F.reset();                                                                                                                                                //Natural: RESET #F
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '06'
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                     //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                //Natural: MOVE EDITED #WS-GRAND-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99'****'"));                               //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99'****' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            }                                                                                                                                                             //Natural: END-IF
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                    //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                       //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("*", true), new ExamineReplace(" "));                     //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '*' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Side_Printed.equals("1")))                                                                                                                   //Natural: IF #WS-SIDE-PRINTED = '1'
        {
            pnd_Ws_Rec_1.setValue("17");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '17'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DEDUCTIONS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-DEDUCTION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-FEDERAL-LEGEND
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEGEND
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECS
    }
    private void sub_Format_Deductions() throws Exception                                                                                                                 //Natural: FORMAT-DEDUCTIONS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC = '+3'
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Amount_1);                                                                                             //Natural: ASSIGN #WS-COLUMN ( #F ) = #WS-AMOUNT-1
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("*", true), new ExamineReplace(" "));                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL '*' REPLACE ' '
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Literal);                                                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) = #WS-LITERAL
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                     //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Amount_2, pnd_Ws_Legend));                         //Natural: COMPRESS #WS-AMOUNT-2 #WS-LEGEND INTO #WS-COLUMN ( #F ) LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(3).setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(3), MoveOption.LeftJustified);                                                //Natural: MOVE LEFT #WS-COLUMN ( 3 ) TO #WS-COLUMN ( 3 )
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("*", true), new ExamineReplace(" "));                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL '*' REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(3),true), new ExamineSearch("+", true), new ExamineReplace(" "));                           //Natural: EXAMINE FULL #WS-COLUMN ( 3 ) FOR FULL '+' REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(2),true), new ExamineSearch("@", true), new ExamineReplace(" "));                           //Natural: EXAMINE FULL #WS-COLUMN ( 2 ) FOR FULL '@' REPLACE ' '
    }
    private void sub_Translate_Deduction() throws Exception                                                                                                               //Natural: TRANSLATE-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(1)))                                                                       //Natural: IF PYMNT-DED-CDE ( #J,#C ) = 1
        {
            pnd_Ws_Letter.setValue("H");                                                                                                                                  //Natural: ASSIGN #WS-LETTER = 'H'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(2)))                                                                   //Natural: IF PYMNT-DED-CDE ( #J,#C ) = 2
            {
                pnd_Ws_Letter.setValue("L");                                                                                                                              //Natural: ASSIGN #WS-LETTER = 'L'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(3)))                                                               //Natural: IF PYMNT-DED-CDE ( #J,#C ) = 3
                {
                    pnd_Ws_Letter.setValue("M");                                                                                                                          //Natural: ASSIGN #WS-LETTER = 'M'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(4)))                                                           //Natural: IF PYMNT-DED-CDE ( #J,#C ) = 4
                    {
                        pnd_Ws_Letter.setValue("G");                                                                                                                      //Natural: ASSIGN #WS-LETTER = 'G'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Letter.setValue("?");                                                                                                                      //Natural: ASSIGN #WS-LETTER = '?'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Federal_Legend() throws Exception                                                                                                             //Natural: FORMAT-FEDERAL-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        //* * THIS IF CHNGED TO FIX THE "F" AND "N" LETERAL     G.P 8/11/98
        //*  CHANGE TESTING FOR NRA - ROXAN 3/14/2002
        //*  BEGIN
        //* IF CNTRCT-CRRNCY-CDE = '1'
        //*   IF (ANNT-CTZNSHP-CDE = 01 OR= 11)
        //* **    OR     /* G.P 8/11/98
        //* **  ( (ANNT-RSDNCY-CDE = '01' THRU '57') OR (ANNT-RSDNCY-CDE = '97') )
        //*     ASSIGN #WS-LEGEND = '(F)*'
        //*   ELSE
        //*     ASSIGN #WS-LEGEND = '(N)*'
        //*   END-IF
        //* END-IF
        //*  NEW CODE       /* ROXAN 3/14/2002
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                    //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
            {
                pnd_Ws_Legend.setValue("(N)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(N)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Legend.setValue("(F)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(F)*'
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF NEW IF STMT  3/18/98
        }                                                                                                                                                                 //Natural: END-IF
        //* END
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ANNT-RSDNCY-CDE = '74' THRU '86'
            {
                pnd_Ws_Legend.setValue("(C)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(C)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86")))) //Natural: IF NOT ( ANNT-RSDNCY-CDE = '74' THRU '86' )
                {
                    pnd_Ws_Legend.setValue("(C)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(C)*'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_State_Legend() throws Exception                                                                                                               //Natural: FORMAT-STATE-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))                                                                             //Natural: IF ANNT-RSDNCY-CDE = '97'
            {
                pnd_Ws_Legend.setValue("(S)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(S)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11))      //Natural: IF ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( ANNT-RSDNCY-CDE = '01' THRU '57' )
                    || (pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))))
                {
                    pnd_Ws_Legend.setValue("(S)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(S)*'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((! ((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11)))  //Natural: IF NOT ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( ANNT-RSDNCY-CDE = '01' THRU '57' ) AND ANNT-RSDNCY-CDE NE '97'
                        && ! ((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))) 
                        && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().notEquals("97"))))
                    {
                        pnd_Ws_Legend.setValue("(N)*");                                                                                                                   //Natural: ASSIGN #WS-LEGEND = '(N)*'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))  //Natural: IF ( ANNT-RSDNCY-CDE = '74' THRU '86' ) OR ANNT-RSDNCY-CDE = '96'
                || pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("96"))))
            {
                pnd_Ws_Legend.setValue("(P)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(P)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Legend.setValue("(C)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(C)*'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Recs() throws Exception                                                                                                                        //Natural: WRITE-RECS
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }

    //
}
