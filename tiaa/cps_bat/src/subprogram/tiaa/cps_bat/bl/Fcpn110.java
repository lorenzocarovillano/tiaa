/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:20:59 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn110
************************************************************
**        * FILE NAME            : Fcpn110.java
**        * CLASS NAME           : Fcpn110
**        * INSTANCE NAME        : Fcpn110
************************************************************
************************************************************************
** PROGRAM:     FCPN110                                               **
** SYSTEM:      CONSOLIDATED PAYMENT SYSTEM                           **
**              OPEN INVESTMENTS ARCHITECTURE                         **
** DATE:        12/14/2005                                            **
** AUTHOR:      RAMANA ANNE                                           **
** DESCRIPTION: PROGRAM EXTRACTS BANK INFORMATION FROM                **
**              BANK 'REFERENCE-TABLE' FILE.                          **
**              THIS ROUTINE CAN BE CALLED USING TWO FUNCTIONS:       **
**              1. 'NEXT'          - FOR A GIVEN SOURCE CODE, ALL THE **
**                 RELATED FIELDS WILL BE RETURNED.                   **
**              1.'update chk seq' - FOR A GIVEN SOURCE CODE, UPDATES **
**                 CHECK AND SEQUENCE NUMBER.                         **

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn110 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa110 pdaFcpa110;
    private LdaFcpl110 ldaFcpl110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Bank_Super1;

    private DbsGroup pnd_Bank_Super1__R_Field_1;
    private DbsField pnd_Bank_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Bank_Super1__R_Field_2;
    private DbsField pnd_Bank_Super1_Pnd_Bank_Source_Code;
    private DbsField pnd_Bank_Super1_Pnd_Filler1;
    private DbsField pnd_Bank_Super1_Pnd_Rt_Long_Key;
    private DbsField pnd_Isn;
    private DbsField pnd_Valid_Check;
    private DbsField pnd_Valid_Sequence;
    private DbsField pnd_Reset_Seq_No;
    private DbsField pnd_Reset_Check_No;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl110 = new LdaFcpl110();
        registerRecord(ldaFcpl110);
        registerRecord(ldaFcpl110.getVw_ra());
        registerRecord(ldaFcpl110.getVw_rau());

        // parameters
        parameters = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Bank_Super1 = localVariables.newFieldInRecord("pnd_Bank_Super1", "#BANK-SUPER1", FieldType.STRING, 66);

        pnd_Bank_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Bank_Super1__R_Field_1", "REDEFINE", pnd_Bank_Super1);
        pnd_Bank_Super1_Pnd_Rt_A_I_Ind = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 
            1);
        pnd_Bank_Super1_Pnd_Rt_Table_Id = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Bank_Super1_Pnd_Rt_Short_Key = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 
            20);

        pnd_Bank_Super1__R_Field_2 = pnd_Bank_Super1__R_Field_1.newGroupInGroup("pnd_Bank_Super1__R_Field_2", "REDEFINE", pnd_Bank_Super1_Pnd_Rt_Short_Key);
        pnd_Bank_Super1_Pnd_Bank_Source_Code = pnd_Bank_Super1__R_Field_2.newFieldInGroup("pnd_Bank_Super1_Pnd_Bank_Source_Code", "#BANK-SOURCE-CODE", 
            FieldType.STRING, 5);
        pnd_Bank_Super1_Pnd_Filler1 = pnd_Bank_Super1__R_Field_2.newFieldInGroup("pnd_Bank_Super1_Pnd_Filler1", "#FILLER1", FieldType.STRING, 15);
        pnd_Bank_Super1_Pnd_Rt_Long_Key = pnd_Bank_Super1__R_Field_1.newFieldInGroup("pnd_Bank_Super1_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 
            40);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Valid_Check = localVariables.newFieldInRecord("pnd_Valid_Check", "#VALID-CHECK", FieldType.BOOLEAN, 1);
        pnd_Valid_Sequence = localVariables.newFieldInRecord("pnd_Valid_Sequence", "#VALID-SEQUENCE", FieldType.BOOLEAN, 1);
        pnd_Reset_Seq_No = localVariables.newFieldInRecord("pnd_Reset_Seq_No", "#RESET-SEQ-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_Reset_Check_No = localVariables.newFieldInRecord("pnd_Reset_Check_No", "#RESET-CHECK-NO", FieldType.PACKED_DECIMAL, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl110.initializeValues();

        localVariables.reset();
        pnd_Bank_Super1.setInitialValue("ACIA22");
        pnd_Isn.setInitialValue(0);
        pnd_Valid_Check.setInitialValue(false);
        pnd_Valid_Sequence.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn110() throws Exception
    {
        super("Fcpn110");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPN110", onError);
        //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        //* *--------- MAIN PROCESS ---------------------------------------------**
        pdaFcpa110.getFcpa110_Fcpa110b().reset();                                                                                                                         //Natural: RESET FCPA110B
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            short decideConditionsMet302 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF FCPA110-FUNCTION;//Natural: VALUE 'NEXT'
            if (condition((pdaFcpa110.getFcpa110_Fcpa110_Function().equals("NEXT"))))
            {
                decideConditionsMet302++;
                if (condition(pdaFcpa110.getFcpa110_Fcpa110_Source_Code().equals(" ")))                                                                                   //Natural: IF FCPA110.FCPA110-SOURCE-CODE = ' '
                {
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0001");                                                                                         //Natural: ASSIGN FCPA110-RETURN-CODE := '0001'
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Source Code Missing");                                                                           //Natural: ASSIGN FCPA110-RETURN-MSG := 'Source Code Missing'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM A100-SETUP-BANK-ACCT-SUPER1
                sub_A100_Setup_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A150-READ-BANK-ACCT-SUPER1
                sub_A150_Read_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'UPDATE CHK SEQ'
            else if (condition((pdaFcpa110.getFcpa110_Fcpa110_Function().equals("UPDATE CHK SEQ"))))
            {
                decideConditionsMet302++;
                if (condition(pdaFcpa110.getFcpa110_Fcpa110_Source_Code().equals(" ")))                                                                                   //Natural: IF FCPA110.FCPA110-SOURCE-CODE = ' '
                {
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0001");                                                                                         //Natural: ASSIGN FCPA110-RETURN-CODE := '0001'
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Source Code Missing");                                                                           //Natural: ASSIGN FCPA110-RETURN-MSG := 'Source Code Missing'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa110.getFcpa110_Fcpa110_New_Check_Nbr().equals(getZero()) || ! (DbsUtil.maskMatches(pdaFcpa110.getFcpa110_Fcpa110_New_Check_Nbr(),    //Natural: IF FCPA110.FCPA110-NEW-CHECK-NBR = 0 OR FCPA110.FCPA110-NEW-CHECK-NBR NE MASK ( 9999999999 )
                    "9999999999"))))
                {
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0003");                                                                                         //Natural: ASSIGN FCPA110-RETURN-CODE := '0003'
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("New Check No Missing OR Invalid");                                                               //Natural: ASSIGN FCPA110-RETURN-MSG := 'New Check No Missing OR Invalid'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa110.getFcpa110_Fcpa110_New_Sequence_Nbr().equals(getZero()) || ! (DbsUtil.maskMatches(pdaFcpa110.getFcpa110_Fcpa110_New_Sequence_Nbr(), //Natural: IF FCPA110.FCPA110-NEW-SEQUENCE-NBR = 0 OR FCPA110.FCPA110-NEW-SEQUENCE-NBR NE MASK ( 9999999 )
                    "9999999"))))
                {
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0004");                                                                                         //Natural: ASSIGN FCPA110-RETURN-CODE := '0004'
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("New Sequence No Missing OR Invalid");                                                            //Natural: ASSIGN FCPA110-RETURN-MSG := 'New Sequence No Missing OR Invalid'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa110.getFcpa110_Fcpa110_Old_Check_Nbr().equals(getZero()) || ! (DbsUtil.maskMatches(pdaFcpa110.getFcpa110_Fcpa110_Old_Check_Nbr(),    //Natural: IF FCPA110.FCPA110-OLD-CHECK-NBR = 0 OR FCPA110.FCPA110-OLD-CHECK-NBR NE MASK ( 9999999999 )
                    "9999999999"))))
                {
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0005");                                                                                         //Natural: ASSIGN FCPA110-RETURN-CODE := '0005'
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Please Pass Old Check No Also");                                                                 //Natural: ASSIGN FCPA110-RETURN-MSG := 'Please Pass Old Check No Also'
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM A100-SETUP-BANK-ACCT-SUPER1
                sub_A100_Setup_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A150-READ-BANK-ACCT-SUPER1
                sub_A150_Read_Bank_Acct_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A450-READ-BANK-ACCT-SUPER1-UPDATE-CHECK-SEQ
                sub_A450_Read_Bank_Acct_Super1_Update_Check_Seq();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0099");                                                                                             //Natural: ASSIGN FCPA110-RETURN-CODE := '0099'
                pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Invalid Function");                                                                                  //Natural: ASSIGN FCPA110-RETURN-MSG := 'Invalid Function'
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. )
            }                                                                                                                                                             //Natural: END-DECIDE
            if (true) break PROG;                                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. )
            //* *--------END OF MAIN PROCESS ---------------------------------------**
            //*  * * * * * * * FUNCTION BLANK  * * * * * * * * * * * * * * * * * * * *
            //*   SETUP SUPER TO ACCESS BANK-ACCOUNT TBL
            //* **********************************************************************
            //*                   A100-SETUP-BANK-ACCT-SUPER1
            //* **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A100-SETUP-BANK-ACCT-SUPER1
            //* **********************************************************************
            //*  ACCESS BANK-ACCOUNT TBL
            //* **********************************************************************
            //*               A150-READ-BANK-ACCT-SUPER1
            //* **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A150-READ-BANK-ACCT-SUPER1
            //* **********************************************************************
            //*  ACCESS BANK-ACCOUNT TBL FOR UPDATE WITH NEW CHECK NBR OR SEQ NO
            //* *********************************************************************
            //*          A450-READ-BANK-ACCT-SUPER1-UPDATE-CHECK--SEQ-NO
            //* *********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A450-READ-BANK-ACCT-SUPER1-UPDATE-CHECK-SEQ
            //* ***********************************************************************
            //*  (PROG.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_A100_Setup_Bank_Acct_Super1() throws Exception                                                                                                       //Natural: A100-SETUP-BANK-ACCT-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bank_Super1_Pnd_Rt_Short_Key.setValue(pdaFcpa110.getFcpa110_Fcpa110_Source_Code());                                                                           //Natural: MOVE FCPA110.FCPA110-SOURCE-CODE TO #BANK-SUPER1.#RT-SHORT-KEY
        pnd_Bank_Super1_Pnd_Rt_Long_Key.setValue(" ");                                                                                                                    //Natural: MOVE ' ' TO #BANK-SUPER1.#RT-LONG-KEY
    }
    private void sub_A150_Read_Bank_Acct_Super1() throws Exception                                                                                                        //Natural: A150-READ-BANK-ACCT-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Isn.reset();                                                                                                                                                  //Natural: RESET #ISN
        ldaFcpl110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcpl110.getVw_ra().readNextRow("READ01")))
        {
            pnd_Isn.setValue(ldaFcpl110.getVw_ra().getAstISN("Read01"));                                                                                                  //Natural: MOVE *ISN TO #ISN
            pnd_Reset_Seq_No.compute(new ComputeParameters(false, pnd_Reset_Seq_No), ldaFcpl110.getRa_End_Seq_No().subtract(100000));                                     //Natural: COMPUTE #RESET-SEQ-NO = RA.END-SEQ-NO - 100000
            if (condition(ldaFcpl110.getRa_Start_Seq_No().greaterOrEqual(pnd_Reset_Seq_No)))                                                                              //Natural: IF RA.START-SEQ-NO GE #RESET-SEQ-NO
            {
                G2:                                                                                                                                                       //Natural: GET RAU #ISN
                ldaFcpl110.getVw_rau().readByID(pnd_Isn.getLong(), "G2");
                ldaFcpl110.getRa_Start_Seq_No().reset();                                                                                                                  //Natural: RESET RA.START-SEQ-NO
                ldaFcpl110.getRau_Start_Seq_No().setValue(ldaFcpl110.getRa_Start_Seq_No());                                                                               //Natural: MOVE RA.START-SEQ-NO TO RAU.START-SEQ-NO
                ldaFcpl110.getVw_rau().updateDBRow("G2");                                                                                                                 //Natural: UPDATE ( G2. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa110.getFcpa110_Fcpa110b().setValuesByName(ldaFcpl110.getVw_ra());                                                                                      //Natural: MOVE BY NAME RA TO FCPA110B
            if (condition(pnd_Bank_Super1_Pnd_Rt_A_I_Ind.equals(ldaFcpl110.getRa_Rt_A_I_Ind()) && pnd_Bank_Super1_Pnd_Rt_Table_Id.equals(ldaFcpl110.getRa_Rt_Table_Id())  //Natural: IF #BANK-SUPER1.#RT-A-I-IND = RA.RT-A-I-IND AND #BANK-SUPER1.#RT-TABLE-ID = RA.RT-TABLE-ID AND #BANK-SUPER1.#BANK-SOURCE-CODE = RA.BANK-SOURCE-CODE
                && pnd_Bank_Super1_Pnd_Bank_Source_Code.equals(ldaFcpl110.getRa_Bank_Source_Code())))
            {
                pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0000");                                                                                             //Natural: ASSIGN FCPA110-RETURN-CODE := '0000'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0001");                                                                                             //Natural: ASSIGN FCPA110-RETURN-CODE := '0001'
                pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Source Code Not Found               ");                                                              //Natural: ASSIGN FCPA110-RETURN-MSG := 'Source Code Not Found               '
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            //* *
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_A450_Read_Bank_Acct_Super1_Update_Check_Seq() throws Exception                                                                                       //Natural: A450-READ-BANK-ACCT-SUPER1-UPDATE-CHECK-SEQ
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Isn.reset();                                                                                                                                                  //Natural: RESET #ISN #VALID-CHECK #VALID-SEQUENCE
        pnd_Valid_Check.reset();
        pnd_Valid_Sequence.reset();
        ldaFcpl110.getVw_ra().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RA BY RT-SUPER1 STARTING FROM #BANK-SUPER1
        (
        "R2",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Bank_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        R2:
        while (condition(ldaFcpl110.getVw_ra().readNextRow("R2")))
        {
            pnd_Isn.setValue(ldaFcpl110.getVw_ra().getAstISN("R2"));                                                                                                      //Natural: MOVE *ISN TO #ISN
            if (condition(pnd_Bank_Super1_Pnd_Rt_A_I_Ind.equals(ldaFcpl110.getRa_Rt_A_I_Ind()) && pnd_Bank_Super1_Pnd_Rt_Table_Id.equals(ldaFcpl110.getRa_Rt_Table_Id())  //Natural: IF #BANK-SUPER1.#RT-A-I-IND = RA.RT-A-I-IND AND #BANK-SUPER1.#RT-TABLE-ID = RA.RT-TABLE-ID AND #BANK-SUPER1.#BANK-SOURCE-CODE = RA.BANK-SOURCE-CODE
                && pnd_Bank_Super1_Pnd_Bank_Source_Code.equals(ldaFcpl110.getRa_Bank_Source_Code())))
            {
                pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0000");                                                                                             //Natural: ASSIGN FCPA110-RETURN-CODE := '0000'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0001");                                                                                             //Natural: ASSIGN FCPA110-RETURN-CODE := '0001'
                pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Source Code Not Found");                                                                             //Natural: ASSIGN FCPA110-RETURN-MSG := 'Source Code Not Found'
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  COMPARE WITH OLD
            if (condition(pdaFcpa110.getFcpa110_Fcpa110_Old_Check_Nbr().equals(ldaFcpl110.getRa_Start_Check_No())))                                                       //Natural: IF FCPA110-OLD-CHECK-NBR EQ RA.START-CHECK-NO
            {
                G1:                                                                                                                                                       //Natural: GET RAU #ISN
                ldaFcpl110.getVw_rau().readByID(pnd_Isn.getLong(), "G1");
                if (condition(((pdaFcpa110.getFcpa110_Fcpa110_New_Check_Nbr().greater(ldaFcpl110.getRa_Start_Check_No())) && (pdaFcpa110.getFcpa110_Fcpa110_New_Check_Nbr().less(ldaFcpl110.getRa_End_Check_No()))))) //Natural: IF ( ( FCPA110-NEW-CHECK-NBR GT RA.START-CHECK-NO ) AND ( FCPA110-NEW-CHECK-NBR LT RA.END-CHECK-NO ) )
                {
                    ldaFcpl110.getRau_Start_Check_No().setValue(pdaFcpa110.getFcpa110_Fcpa110_New_Check_Nbr());                                                           //Natural: MOVE FCPA110-NEW-CHECK-NBR TO RAU.START-CHECK-NO
                    pnd_Valid_Check.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #VALID-CHECK
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0014");                                                                                         //Natural: ASSIGN FCPA110-RETURN-CODE := '0014'
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Check no GT Max OR LT Previous Check No");                                                       //Natural: ASSIGN FCPA110-RETURN-MSG := 'Check no GT Max OR LT Previous Check No'
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. )
                    Global.setEscapeCode(EscapeType.Bottom, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(((pdaFcpa110.getFcpa110_Fcpa110_New_Sequence_Nbr().greater(ldaFcpl110.getRa_Start_Seq_No())) && (pdaFcpa110.getFcpa110_Fcpa110_New_Sequence_Nbr().less(ldaFcpl110.getRa_End_Seq_No()))))) //Natural: IF ( ( FCPA110-NEW-SEQUENCE-NBR GT RA.START-SEQ-NO ) AND ( FCPA110-NEW-SEQUENCE-NBR LT RA.END-SEQ-NO ) )
                {
                    ldaFcpl110.getRau_Start_Seq_No().setValue(pdaFcpa110.getFcpa110_Fcpa110_New_Sequence_Nbr());                                                          //Natural: MOVE FCPA110-NEW-SEQUENCE-NBR TO RAU.START-SEQ-NO
                    pnd_Valid_Sequence.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #VALID-SEQUENCE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0015");                                                                                         //Natural: ASSIGN FCPA110-RETURN-CODE := '0015'
                    pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Sequence no GT Max or LT Previous number");                                                      //Natural: ASSIGN FCPA110-RETURN-MSG := 'Sequence no GT Max or LT Previous number'
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. )
                    Global.setEscapeCode(EscapeType.Bottom, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Valid_Check.getBoolean() && pnd_Valid_Sequence.getBoolean()))                                                                           //Natural: IF #VALID-CHECK AND #VALID-SEQUENCE
                {
                    ldaFcpl110.getVw_rau().updateDBRow("G1");                                                                                                             //Natural: UPDATE ( G1. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue("0016");                                                                                             //Natural: ASSIGN FCPA110-RETURN-CODE := '0016'
                pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue("Other Stream Updated check Number in REF Table");                                                    //Natural: ASSIGN FCPA110-RETURN-MSG := 'Other Stream Updated check Number in REF Table'
                Global.setEscape(true);                                                                                                                                   //Natural: ESCAPE BOTTOM ( PROG. )
                Global.setEscapeCode(EscapeType.Bottom, "PROG");
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaFcpa110.getFcpa110_Fcpa110_Return_Code().setValue(Global.getERROR_NR());                                                                                       //Natural: MOVE *ERROR-NR TO FCPA110-RETURN-CODE
        pdaFcpa110.getFcpa110_Fcpa110_Return_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "- ERROR ON LINE", Global.getERROR_LINE()));                            //Natural: COMPRESS *PROGRAM '- ERROR ON LINE' *ERROR-LINE INTO FCPA110-RETURN-MSG
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE IMMEDIATE
    };                                                                                                                                                                    //Natural: END-ERROR
}
