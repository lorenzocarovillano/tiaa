/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:36 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn235
************************************************************
**        * FILE NAME            : Fcpn235.java
**        * CLASS NAME           : Fcpn235
**        * INSTANCE NAME        : Fcpn235
************************************************************
*
* PROGRAMMER    START DTE  END DTE   DESC OF CHANGES
* ------------  ---------  --------  ----------------------------------
* ANONYMOUS     08/11/93     /  /    CREATED.
*     PROGRAM FOR HEADER-PORTION OF DOCUMENT
*       11/02   R CARREON.  STOW.  EGTRRA CHANGES IN PD
* 08/23/2019 : REPLACING PDQ MODULES WITH ADS MODULES I.E.
*              PDQA360,PDQA362,PDQN360,PDQN362 TO
*              ADSA360,ADSA362,ADSN360,ADSN362 RESPECTIVELY. -TAG:VIKRAM
*
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn235 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa200 pdaFcpa200;
    private PdaAdsa360 pdaAdsa360;
    private PdaAdsa362 pdaAdsa362;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_1;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column;
    private DbsField pnd_Ws_Rec_2;

    private DbsGroup pnd_Ws_Rec_2__R_Field_2;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name;
    private DbsField pnd_Ws_Check_Dt;
    private DbsField pnd_I;
    private DbsField pnd_Orgn_Chk_Dte;

    private DbsGroup pnd_Orgn_Chk_Dte__R_Field_3;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A;
    private DbsField pnd_Orgn_Chk_Dte_A4;

    private DbsGroup pnd_Orgn_Chk_Dte_A4__R_Field_4;
    private DbsField pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D;
    private DbsField pnd_Edited_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdsa360 = new PdaAdsa360(localVariables);
        pdaAdsa362 = new PdaAdsa362(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa200 = new PdaFcpa200(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_1", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc = pnd_Ws_Rec_1__R_Field_1.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc", "#WS-REC1-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_1_Pnd_Ws_Column = pnd_Ws_Rec_1__R_Field_1.newFieldArrayInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column", "#WS-COLUMN", FieldType.STRING, 15, new 
            DbsArrayController(1, 6));
        pnd_Ws_Rec_2 = localVariables.newFieldInRecord("pnd_Ws_Rec_2", "#WS-REC-2", FieldType.STRING, 143);

        pnd_Ws_Rec_2__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Rec_2__R_Field_2", "REDEFINE", pnd_Ws_Rec_2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc", "#WS-REC2-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler", "#WS-REC2-FILLER", FieldType.STRING, 
            35);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name", "#WS-REC2-NAME", FieldType.STRING, 38);
        pnd_Ws_Check_Dt = localVariables.newFieldInRecord("pnd_Ws_Check_Dt", "#WS-CHECK-DT", FieldType.STRING, 11);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Orgn_Chk_Dte = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte", "#ORGN-CHK-DTE", FieldType.NUMERIC, 8);

        pnd_Orgn_Chk_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte__R_Field_3", "REDEFINE", pnd_Orgn_Chk_Dte);
        pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A = pnd_Orgn_Chk_Dte__R_Field_3.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A", "#ORGN-CHK-DTE-A", FieldType.STRING, 
            8);
        pnd_Orgn_Chk_Dte_A4 = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte_A4", "#ORGN-CHK-DTE-A4", FieldType.STRING, 4);

        pnd_Orgn_Chk_Dte_A4__R_Field_4 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte_A4__R_Field_4", "REDEFINE", pnd_Orgn_Chk_Dte_A4);
        pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D = pnd_Orgn_Chk_Dte_A4__R_Field_4.newFieldInGroup("pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D", "#ORGN-CHK-DTE-D", 
            FieldType.DATE);
        pnd_Edited_Date = localVariables.newFieldInRecord("pnd_Edited_Date", "#EDITED-DATE", FieldType.STRING, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_Check_Dt.setInitialValue(" ");
        pnd_I.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn235() throws Exception
    {
        super("Fcpn235");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1.setValue("11Periodic Payment for:");                                                                                                                 //Natural: ASSIGN #WS-REC-1 = '11Periodic Payment for:'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pdaAdsa360.getPnd_In_Data_Pnd_First_Name().setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1));                                             //Natural: ASSIGN #FIRST-NAME = PH-FIRST-NAME ( 1 )
        pdaAdsa362.getPnd_In_Middle_Name().setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1));                                                    //Natural: ASSIGN #IN-MIDDLE-NAME = PH-MIDDLE-NAME ( 1 )
        pdaAdsa360.getPnd_In_Data_Pnd_Last_Name().setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1));                                               //Natural: ASSIGN #LAST-NAME = PH-LAST-NAME ( 1 )
        //* *CALLNAT 'PDQN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX/*VIKRAM
        //*  VIKRAM
        DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pdaAdsa362.getPnd_In_Middle_Name(), pdaAdsa362.getPnd_Out_Middle_Name(), pdaAdsa362.getPnd_Out_Suffix(),  //Natural: CALLNAT 'ADSN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
            pdaAdsa362.getPnd_Out_Prefix(), pdaAdsa362.getPnd_Out_Return_Code());
        if (condition(Global.isEscape())) return;
        pdaAdsa360.getPnd_In_Data_Pnd_Prefix().setValue(pdaAdsa362.getPnd_Out_Prefix());                                                                                  //Natural: ASSIGN #PREFIX = #OUT-PREFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                        //Natural: ASSIGN #MIDDLE-NAME = #OUT-MIDDLE-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_Suffix().setValue(pdaAdsa362.getPnd_Out_Suffix());                                                                                  //Natural: ASSIGN #SUFFIX = #OUT-SUFFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Length().setValue(38);                                                                                                              //Natural: ASSIGN #LENGTH = 38
        pdaAdsa360.getPnd_In_Data_Pnd_Format().setValue("1");                                                                                                             //Natural: ASSIGN #FORMAT = '1'
        //* *CALLNAT 'PDQN360' #IN-DATA #OUT-DATA   /*    VIKRAM
        //*     VIKRAM
        DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pdaAdsa360.getPnd_In_Data(), pdaAdsa360.getPnd_Out_Data());                                             //Natural: CALLNAT 'ADSN360' #IN-DATA #OUT-DATA
        if (condition(Global.isEscape())) return;
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc.setValue("+2");                                                                                                                       //Natural: ASSIGN #WS-REC2-CC = '+2'
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name.setValue(pdaAdsa360.getPnd_Out_Data_Pnd_Name_Out());                                                                                //Natural: ASSIGN #WS-REC2-NAME = #NAME-OUT
        pnd_Ws_Rec_1.setValue(pnd_Ws_Rec_2);                                                                                                                              //Natural: ASSIGN #WS-REC-1 = #WS-REC-2
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //* ***(((((
        //*                                                           VF 7/99
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                    //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            getReports().print(0, "==>",pdaFcpa200.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte());                                                                       //Natural: PRINT '==>' #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa200.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                    //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
            pnd_Edited_Date.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YY"));                                                        //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YY ) TO #EDITED-DATE
            pnd_Ws_Check_Dt.setValueEdited(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                          //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS-CHECK-DT
            pnd_Ws_Rec_1.setValue(DbsUtil.compress("13Check Date:", pnd_Ws_Check_Dt, "Installment Date:", pnd_Edited_Date));                                              //Natural: COMPRESS '13Check Date:' #WS-CHECK-DT 'Installment Date:' #EDITED-DATE INTO #WS-REC-1
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Check_Dt.setValueEdited(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                          //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS-CHECK-DT
            pnd_Ws_Rec_1.setValue(DbsUtil.compress("13Check Date:", pnd_Ws_Check_Dt));                                                                                    //Natural: COMPRESS '13Check Date:' #WS-CHECK-DT INTO #WS-REC-1
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***(((((
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECS
    }
    private void sub_Write_Recs() throws Exception                                                                                                                        //Natural: WRITE-RECS
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }

    //
}
