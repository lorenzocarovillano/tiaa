/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:57 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn878b
************************************************************
**        * FILE NAME            : Fcpn878b.java
**        * CLASS NAME           : Fcpn878b
**        * INSTANCE NAME        : Fcpn878b
************************************************************
************************************************************************
* SUBPROGRAM : FCPN878B
* SYSTEM     : CPS
* TITLE      : NEW ANNUITIZATION
* FUNCTION   : "AL" ANNUITANT STATEMENTS - "Body".
* HISTORY
*     07/98    R. CARREON
*              PRINTING OF CHECKS AND STATEMENTS
*    12/14/99  R.CARREON
*              NEW LOCAL FCPAEXT
*    03/21/01  R. CARREON
*              CHANGE TELEPHONE MESSAGE
*    03/17/03  R. CARREON
*              RESTOW. FCPAEXT WAS EXPANDED
************************************************************************
* 01/05/2006 : R. LANDRUM
*              POS-PAY, PAYEE MATCH. ADD STOP POINT FOR CHECK NUMBER ON
*              STATEMENT BODY & CHECK FACE, 10 DIGIT MICR LINE.
*
*************************** NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR THE PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn878b extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpaext pdaFcpaext;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa874a pdaFcpa874a;
    private PdaFcpa878h pdaFcpa878h;
    private PdaFcpa803l pdaFcpa803l;
    private PdaFcpabar pdaFcpabar;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl803g ldaFcpl803g;
    private PdaFcpa874c pdaFcpa874c;
    private LdaFcpl878c ldaFcpl878c;
    private LdaFcpl876b ldaFcpl876b;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Temp_Amt;
    private DbsField pnd_Ws_Pnd_Amt_Alpha;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_Comp_Idx;
    private DbsField pnd_Ws_Pnd_Col;
    private DbsField pnd_Ws_Pnd_Ded_Idx;

    private DbsGroup pnd_Ws_Pnd_Ded_Flags;
    private DbsField pnd_Ws_Pnd_Ded_Sum;
    private DbsField pnd_Ws_Pnd_Ded_Fed;
    private DbsField pnd_Ws_Pnd_Ded_State;
    private DbsField pnd_Ws_Pnd_Ded_Local;
    private DbsField pnd_Ws_Pnd_Page_Ctr;
    private DbsField pnd_Line_Ctr;
    private DbsField pnd_X;
    private DbsField pnd_Null;
    private DbsField pnd_Filler;

    private DbsGroup pnd_Ws_Check_Fields;
    private DbsField pnd_Ws_Check_Fields_Pymnt_Ivc_Amt;
    private DbsField pnd_Ws_Check_Fields_Pnd_Ded_Pymnt_Table;
    private DbsField pnd_Ws_Check_Fields_Pnd_Pymnt_Record;
    private DbsField pnd_Ws_Check_Fields_Pnd_Dpi_Dci_Ind;
    private DbsField pnd_Ws_Check_Fields_Pnd_Pymnt_Ded_Ind;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl803g = new LdaFcpl803g();
        registerRecord(ldaFcpl803g);
        pdaFcpa874c = new PdaFcpa874c(localVariables);
        ldaFcpl878c = new LdaFcpl878c();
        registerRecord(ldaFcpl878c);
        ldaFcpl876b = new LdaFcpl876b();
        registerRecord(ldaFcpl876b);

        // parameters
        parameters = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa874a = new PdaFcpa874a(parameters);
        pdaFcpa878h = new PdaFcpa878h(parameters);
        pdaFcpa803l = new PdaFcpa803l(parameters);
        pdaFcpabar = new PdaFcpabar(parameters);
        pdaFcpa110 = new PdaFcpa110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Temp_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Amt", "#TEMP-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Amt_Alpha = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Alpha", "#AMT-ALPHA", FieldType.STRING, 15);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Comp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Idx", "#COMP-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Col = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Col", "#COL", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ded_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ded_Idx", "#DED-IDX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws_Pnd_Ded_Flags = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Ded_Flags", "#DED-FLAGS");
        pnd_Ws_Pnd_Ded_Sum = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_Sum", "#DED-SUM", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ded_Fed = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_Fed", "#DED-FED", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ded_State = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_State", "#DED-STATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ded_Local = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_Local", "#DED-LOCAL", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Page_Ctr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Page_Ctr", "#PAGE-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_Line_Ctr = localVariables.newFieldInRecord("pnd_Line_Ctr", "#LINE-CTR", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Null = localVariables.newFieldInRecord("pnd_Null", "#NULL", FieldType.STRING, 1);
        pnd_Filler = localVariables.newFieldInRecord("pnd_Filler", "#FILLER", FieldType.STRING, 35);

        pnd_Ws_Check_Fields = localVariables.newGroupInRecord("pnd_Ws_Check_Fields", "#WS-CHECK-FIELDS");
        pnd_Ws_Check_Fields_Pymnt_Ivc_Amt = pnd_Ws_Check_Fields.newFieldInGroup("pnd_Ws_Check_Fields_Pymnt_Ivc_Amt", "PYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Check_Fields_Pnd_Ded_Pymnt_Table = pnd_Ws_Check_Fields.newFieldArrayInGroup("pnd_Ws_Check_Fields_Pnd_Ded_Pymnt_Table", "#DED-PYMNT-TABLE", 
            FieldType.STRING, 3, new DbsArrayController(1, 4));
        pnd_Ws_Check_Fields_Pnd_Pymnt_Record = pnd_Ws_Check_Fields.newFieldInGroup("pnd_Ws_Check_Fields_Pnd_Pymnt_Record", "#PYMNT-RECORD", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Check_Fields_Pnd_Dpi_Dci_Ind = pnd_Ws_Check_Fields.newFieldInGroup("pnd_Ws_Check_Fields_Pnd_Dpi_Dci_Ind", "#DPI-DCI-IND", FieldType.BOOLEAN, 
            1);
        pnd_Ws_Check_Fields_Pnd_Pymnt_Ded_Ind = pnd_Ws_Check_Fields.newFieldInGroup("pnd_Ws_Check_Fields_Pnd_Pymnt_Ded_Ind", "#PYMNT-DED-IND", FieldType.BOOLEAN, 
            1);
        pnd_Ws_Pymnt_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Nbr_N10", "#WS-PYMNT-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Pymnt_Check_Nbr_N10);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", 
            "#WS-CHECK-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", 
            "#WS-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803g.initializeValues();
        ldaFcpl878c.initializeValues();
        ldaFcpl876b.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn878b() throws Exception
    {
        super("Fcpn878b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'ANNUITY LOAN DETAIL CONTROL REPORT' 120T 'REPORT: RPT1' //
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().getBoolean()))                                                                                            //Natural: IF #NEW-PYMNT
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(false);                                                                                                    //Natural: ASSIGN #NEW-PYMNT := FALSE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().resetInitial();                                                                                                  //Natural: RESET INITIAL #FCPA803.#CURRENT-PAGE #DPI-COL #DED-COL #NET-COL #GRAND-TOTALS
            pdaFcpa803.getPnd_Fcpa803_Pnd_Dpi_Col().resetInitial();
            pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col().resetInitial();
            pdaFcpa803.getPnd_Fcpa803_Pnd_Net_Col().resetInitial();
            pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Totals().resetInitial();
        }                                                                                                                                                                 //Natural: END-IF
        //* *ADD INV-ACCT-IVC-AMT(*) TO  PYMNT-IVC-AMT          /* ROXAN  12/15/99
        //* TMM
        pnd_Ws_Check_Fields_Pymnt_Ivc_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue("*"));                                                                       //Natural: ADD INV-ACCT-IVC-AMT ( * ) TO #WS-CHECK-FIELDS.PYMNT-IVC-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Net_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                    //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-NET-AMT
        //*  ROXAN  7/28/98
        pnd_Line_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #LINE-CTR
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO C-INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpaext.getExt_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I));                                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := INV-ACCT-CDE-N ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpaext.getExt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := INV-ACCT-VALUAT-PERIOD ( #I )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            //* IF #FCPA803.#CURRENT-PAGE NE NZ-EXT.FUND-ON-PAGE(#I)      /* 12/14/99
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(pdaFcpaext.getExt_Funds_On_Page().getValue(pnd_Ws_Pnd_I))))                              //Natural: IF #FCPA803.#CURRENT-PAGE NE EXT.FUNDS-ON-PAGE ( #I )
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().setValue(true);                                                                                               //Natural: ASSIGN #NEWPAGE-IND := TRUE
                //*  NOT START OF PYMNT
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(getZero())))                                                                         //Natural: IF #FCPA803.#CURRENT-PAGE NE 0
                {
                                                                                                                                                                          //Natural: PERFORM BOTTOM-OF-PAGE
                    sub_Bottom_Of_Page();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().setValue(pdaFcpaext.getExt_Funds_On_Page().getValue(pnd_Ws_Pnd_I));                                          //Natural: ASSIGN #FCPA803.#CURRENT-PAGE := EXT.FUNDS-ON-PAGE ( #I )
                pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().divide(2).multiply(2));                     //Natural: COMPUTE #J = #FCPA803.#CURRENT-PAGE / 2 * 2
                //*  EVEN PAGE
                if (condition(pnd_Ws_Pnd_J.equals(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page())))                                                                         //Natural: IF #J = #FCPA803.#CURRENT-PAGE
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(false);                                                                                             //Natural: ASSIGN #ODD-PAGE := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(true);                                                                                              //Natural: ASSIGN #ODD-PAGE := TRUE
                    //*  RL DON'T THINK THIS IS POSTNET, REMOVE IF IS
                                                                                                                                                                          //Natural: PERFORM GEN-BARCODE
                    sub_Gen_Barcode();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().getBoolean()))                                                                                      //Natural: IF #NEWPAGE-IND
            {
                DbsUtil.callnat(Fcpn803x.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pnd_Null);                                                        //Natural: CALLNAT 'FCPN803X' USING #FCPA803 #NULL
                if (condition(Global.isEscape())) return;
                //* ********************** RL BEGIN PAYEE MATCH **************************
                //*  RL FRI FEB 10
                if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().getBoolean())))                                                                            //Natural: IF NOT #FCPA803.#STMNT-TO-ANNT
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("12");                                                                                            //Natural: ASSIGN #OUTPUT-REC := '12'
                    pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                         //Natural: MOVE EXT.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                    pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                               //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                               //Natural: ASSIGN #OUTPUT-REC-DETAIL := #WS-PYMNT-CHECK-NBR-N10
                    //* *COMPRESS #WS-PYMNT-CHECK-NBR-N10 *PROGRAM ' 1370' EXT.PYMNT-CHECK-NBR
                    //* *                           INTO #OUTPUT-REC-DETAIL
                    getReports().write(0, Global.getPROGRAM(),"1400","GENERATE STOP FOR STMNT",NEWLINE,"=",pdaFcpaext.getExt_Pymnt_Check_Nbr(),NEWLINE,"=",pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr(),NEWLINE,"=",ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr(),NEWLINE,"=",pnd_Ws_Pymnt_Check_Nbr_N10,NEWLINE,"=",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),  //Natural: WRITE *PROGRAM '1400' 'GENERATE STOP FOR STMNT' / '=' EXT.PYMNT-CHECK-NBR / '=' EXT.PYMNT-CHECK-SCRTY-NBR / '=' #FCPL876B.PYMNT-CHECK-NBR / '=' #WS-PYMNT-CHECK-NBR-N10 / '=' #OUTPUT-REC ( AL = 25 ) /
                        new AlphanumericLength (25),NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                           //Natural: WRITE WORK FILE 8 #OUTPUT-REC
                }                                                                                                                                                         //Natural: END-IF
                //* ********************** RL END PAYEE MATCH ****************************
                                                                                                                                                                          //Natural: PERFORM GEN-PYMNT-HEADERS
                sub_Gen_Pymnt_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GEN-CNTRCT-HEADERS
                sub_Gen_Cntrct_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #NEWPAGE-IND := FALSE
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PERIODIC
            sub_Display_Periodic();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* IF #FCPA803.#RECORD-IN-PYMNT = #NZ-CHECK-FIELDS.#PYMNT-RECORDS     /*
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().equals(pdaFcpaext.getExt_Pymnt_Record())))                                                          //Natural: IF #FCPA803.#RECORD-IN-PYMNT = EXT.PYMNT-RECORD
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().setValue(true);                                                                                                  //Natural: ASSIGN #FCPA803.#END-OF-PYMNT := TRUE
                                                                                                                                                                          //Natural: PERFORM BOTTOM-OF-PAGE
            sub_Bottom_Of_Page();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean() && ! (pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean())))                          //Natural: IF #FCPA803.#CHECK AND NOT #FCPA803.#CHECK-TO-ANNT
            {
                                                                                                                                                                          //Natural: PERFORM LETTER-CHECK
                sub_Letter_Check();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).nsubtract(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,                //Natural: ASSIGN EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) := EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) - EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT )
            ":",pdaFcpaext.getExt_C_Inv_Acct()));
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-PERIODIC
        //* *********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BODY
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMT-$
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMT
        //* ****************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BOTTOM-OF-PAGE
        //*    MOVE #PHONE-TEXT(1)     TO SUBSTR(#AL-STMNT-TO-ANNT-TEXT(3),45,47)
        //*      MOVE #PHONE-TEXT(1)     TO SUBSTR(#AL-CHECK-TEXT(3),1,47)
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BLANK-LINES
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PART
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-PYMNT-HEADERS
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-CNTRCT-HEADERS
        //* ***********************************
        //* DEFINE SUBROUTINE WRITE-GRAND-TOTALS   /* DEAD CODE ROXAN 12/15/99
        //* ***********************************
        //* #COL                                 :=  2
        //* #OUTPUT-COL(#COL)                    :=  #GRAND-TOTAL-TEXT(2)
        //* #COL                                 :=  #COL + 1
        //* MOVE EDITED #GRAND-SETTL-AMT (EM=+Z,ZZZ,ZZ9.99) TO #AMT-ALPHA
        //*  #NZ-CHECK-FIELDS.PYMNT-IVC-AMT    NE 0.00
        //*   MOVE '#'                           TO  SUBSTR(#AMT-ALPHA,14,1)
        //* END-IF
        //* *
        //* PERFORM CONVERT-AMT-$
        //* #COL                                 :=  #COL + 1
        //* *
        //* IF #DPI-DCI-IND
        //*   #COL                               :=  #COL + 1
        //*   MOVE EDITED #GRAND-DPI-AMT (EM=+Z,ZZZ,ZZ9.99) TO #AMT-ALPHA
        //*   PERFORM CONVERT-AMT-$
        //* END-IF
        //* *
        //* IF #PYMNT-DED-IND
        //*   #COL                               :=  #COL + 1
        //*   MOVE EDITED #GRAND-DED-AMT (EM=+ZZZZ,ZZ9.99)  TO #AMT-ALPHA
        //*   PERFORM CONVERT-AMT-$
        //* END-IF
        //* *
        //* #COL                                 :=  #COL + 1
        //* MOVE EDITED #GRAND-NET-AMT(EM=+Z,ZZZ,ZZ9.99)    TO #AMT-ALPHA
        //* PERFORM CONVERT-AMT-$
        //* PERFORM WRITE-BODY
        //* END-SUBROUTINE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LETTER-CHECK
        //* ********************** RL END PAYEE MATCH ****************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-BARCODE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BARCODE-PROCESSING
        //* ***********************************
        //*  IF NOT #ODD-PAGE
        //*    #OUTPUT-REC                        := '11'
        //*    PERFORM FCPC803W
        //*    ESCAPE ROUTINE
        //*  END-IF
        //* * RL NEED 10 DIGIT CHECK-NBR FOR BAR CODE ? RL CURRENTLY N7 IN FCPL876B
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    //*  DOUBLE SPACE
    private void sub_Display_Periodic() throws Exception                                                                                                                  //Natural: DISPLAY-PERIODIC
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("02   ");                                                                                                     //Natural: ASSIGN #OUTPUT-REC := '02   '
        pnd_Ws_Pnd_Col.setValue(2);                                                                                                                                       //Natural: ASSIGN #COL := 2
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValueEdited(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));          //Natural: MOVE EDITED EXT.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #OUTPUT-COL ( #COL )
        pnd_Ws_Pnd_Col.nadd(2);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 2
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
        //*  ROXAN  7/28/98
        pnd_Line_Ctr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #LINE-CTR
    }
    private void sub_Write_Body() throws Exception                                                                                                                        //Natural: WRITE-BODY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Convert_Amt_Dollar() throws Exception                                                                                                                //Natural: CONVERT-AMT-$
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Amt_Alpha), new ExamineSearch("+"), new ExamineReplace("$"));                                                        //Natural: EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Convert_Amt() throws Exception                                                                                                                       //Natural: CONVERT-AMT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pnd_Ws_Pnd_Amt_Alpha);                                                               //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #AMT-ALPHA
    }
    private void sub_Bottom_Of_Page() throws Exception                                                                                                                    //Natural: BOTTOM-OF-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
                                                                                                                                                                          //Natural: PERFORM WRITE-BLANK-LINES
        sub_Write_Blank_Lines();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().getBoolean()))                                                                                         //Natural: IF #END-OF-PYMNT
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(pdaFcpaext.getExt_Funds_On_Page().getValue(pnd_Ws_Pnd_I))))                              //Natural: IF #FCPA803.#CURRENT-PAGE NE EXT.FUNDS-ON-PAGE ( #I )
            {
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(getZero())))                                                                         //Natural: IF #FCPA803.#CURRENT-PAGE NE 0
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue(" 7");                                                                                                  //Natural: ASSIGN #FCPA803.#FONT := ' 7'
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Grand_Total_Text().getValue(1));                           //Natural: ASSIGN #OUTPUT-REC-DETAIL := #GRAND-TOTAL-TEXT ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape())) {return;}
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                   //Natural: RESET #OUTPUT-REC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().getBoolean()))                                                                                    //Natural: IF #FCPA803.#STMNT-TO-ANNT
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("7");                                                                                                       //Natural: ASSIGN #FCPA803.#FONT := '7'
                FOR02:                                                                                                                                                    //Natural: FOR #J = 1 TO 3
                for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(3)); pnd_Ws_Pnd_J.nadd(1))
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Al_Stmnt_To_Annt_Text().getValue(pnd_Ws_Pnd_J));           //Natural: ASSIGN #OUTPUT-REC-DETAIL := #AL-STMNT-TO-ANNT-TEXT ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                     //Natural: ASSIGN #CC := ' '
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 7");                                                                                            //Natural: ASSIGN #OUTPUT-REC := ' 7'
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean()))                                                                                        //Natural: IF #FCPA803.#CHECK
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("7");                                                                                                   //Natural: ASSIGN #FCPA803.#FONT := '7'
                    FOR03:                                                                                                                                                //Natural: FOR #J = 1 TO 3
                    for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(3)); pnd_Ws_Pnd_J.nadd(1))
                    {
                        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Al_Check_Text().getValue(pnd_Ws_Pnd_J));               //Natural: ASSIGN #OUTPUT-REC-DETAIL := #AL-CHECK-TEXT ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                        sub_Fcpc803w();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                 //Natural: ASSIGN #CC := ' '
                        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 7");                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 7'
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-PART
            sub_Check_Part();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BARCODE
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Blank_Lines() throws Exception                                                                                                                 //Natural: WRITE-BLANK-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  WRITE BLANK LINE UNITL ALL 15 LINES
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        FOR04:                                                                                                                                                            //Natural: FOR #X #LINE-CTR TO 15
        for (pnd_X.setValue(pnd_Line_Ctr); condition(pnd_X.lessOrEqual(15)); pnd_X.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("7");                                                                                                           //Natural: ASSIGN #FCPA803.#FONT := '7'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                           //Natural: RESET #OUTPUT-REC
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_Part() throws Exception                                                                                                                        //Natural: CHECK-PART
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        //* MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #FCPA874C         /* 12/14/99
        pdaFcpa874c.getPnd_Fcpa874c().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                          //Natural: MOVE BY NAME EXTR TO #FCPA874C
        pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp().getValue("*").setValuesByName(pdaFcpaext.getExt_Pymnt_Nme_And_Addr_Grp().getValue("*"));                     //Natural: MOVE BY NAME EXT.PYMNT-NME-AND-ADDR-GRP ( * ) TO #FCPA874A.PYMNT-NME-AND-ADDR-GRP ( * )
        //*  CHECK PRINTING DATA  /* RL
        DbsUtil.callnat(Fcpn878c.class , getCurrentProcessState(), pdaFcpa874c.getPnd_Fcpa874c(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa874a.getPnd_Fcpa874a(),             //Natural: CALLNAT 'FCPN878C' USING #FCPA874C #FCPA803 #FCPA874A FCPA110
            pdaFcpa110.getFcpa110());
        if (condition(Global.isEscape())) return;
        //* *VE '+3'      TO #OUTPUT-REC      /* RL
        //* *ITE WORK FILE 8 #OUTPUT-REC      /* RL
        //*  RL
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                        //Natural: MOVE '+3' TO #OUTPUT-REC
        //*  RL
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }
    private void sub_Gen_Pymnt_Headers() throws Exception                                                                                                                 //Natural: GEN-PYMNT-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean())))                                                                                         //Natural: IF NOT #ODD-PAGE
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("11");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '11'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            pdaFcpa878h.getPnd_Fcpa878h_Pnd_Gen_Headers().setValue(true);                                                                                                 //Natural: ASSIGN #GEN-HEADERS := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE BY NAME #CHECK-SORT-FIELDS      TO #FCPA878H      /* ROXAN 12/15
        pdaFcpa878h.getPnd_Fcpa878h().setValuesByName(pdaFcpa803.getPnd_Fcpa803());                                                                                       //Natural: MOVE BY NAME #FCPA803 TO #FCPA878H
        //* MOVE BY NAME PYMNT-ADDR-INFO         TO #FCPA878H       /*
        pdaFcpa878h.getPnd_Fcpa878h().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                          //Natural: MOVE BY NAME EXTR TO #FCPA878H
        DbsUtil.callnat(Fcpn878h.class , getCurrentProcessState(), pdaFcpa878h.getPnd_Fcpa878h(), pdaFcpa803.getPnd_Fcpa803());                                           //Natural: CALLNAT 'FCPN878H' USING #FCPA878H #FCPA803
        if (condition(Global.isEscape())) return;
    }
    private void sub_Gen_Cntrct_Headers() throws Exception                                                                                                                //Natural: GEN-CNTRCT-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //* CALLNAT 'FCPN878T' USING #CHECK-SORT-FIELDS #NZ-CHECK-FIELDS #FCPA803
        DbsUtil.callnat(Fcpn878t.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803());                                                                          //Natural: CALLNAT 'FCPN878T' USING #FCPA803
        if (condition(Global.isEscape())) return;
    }
    private void sub_Letter_Check() throws Exception                                                                                                                      //Natural: LETTER-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Type().reset();                                                                                                               //Natural: RESET #FCPA803.#STMNT-TYPE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Check().setValue(true);                                                                                                             //Natural: ASSIGN #FCPA803.#CHECK := TRUE
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean())))                                                                                          //Natural: IF NOT #SIMPLEX
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpn803x.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pnd_Null);                                                                //Natural: CALLNAT 'FCPN803X' USING #FCPA803 #NULL
        if (condition(Global.isEscape())) return;
        //* ********************** RL BEGIN PAYEE MATCH **************************
        //* * #FCPA803.#FULL-XEROX AND NOT #FCPA803.#STMNT-TO-ANNT
        //*  RL FRI FEB 10
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().getBoolean())))                                                                                    //Natural: IF NOT #FCPA803.#STMNT-TO-ANNT
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("12");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '12'
            pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                 //Natural: MOVE EXT.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
            pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                       //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                       //Natural: ASSIGN #OUTPUT-REC-DETAIL := #WS-PYMNT-CHECK-NBR-N10
            //* *COMPRESS #WS-PYMNT-CHECK-NBR-N10 *PROGRAM ' 3570' EXT.PYMNT-CHECK-NBR
            //* *INTO  #OUTPUT-REC-DETAIL
            getReports().write(0, Global.getPROGRAM(),"3560","GENERATE STOP FOR STMNT",NEWLINE,"=",pdaFcpaext.getExt_Pymnt_Check_Nbr(),NEWLINE,"=",pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr(),NEWLINE,"=",ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr(),NEWLINE,"=",pnd_Ws_Pymnt_Check_Nbr_N10,NEWLINE,"=",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),  //Natural: WRITE *PROGRAM '3560' 'GENERATE STOP FOR STMNT' / '=' EXT.PYMNT-CHECK-NBR / '=' EXT.PYMNT-CHECK-SCRTY-NBR / '=' #FCPL876B.PYMNT-CHECK-NBR / '=' #WS-PYMNT-CHECK-NBR-N10 / '=' #OUTPUT-REC ( AL = 25 ) /
                new AlphanumericLength (25),NEWLINE);
            if (Global.isEscape()) return;
            getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                   //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(2)); pnd_Ws_Pnd_I.nadd(1))
        {
            getWorkFiles().write(8, false, pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(pnd_Ws_Pnd_I));                                                        //Natural: WRITE WORK FILE 8 #HEADER-ARRAY ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE BLANK LINE UNITL ALL 15 LINES
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        FOR06:                                                                                                                                                            //Natural: FOR #X 1 TO 3
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(3)); pnd_X.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("7");                                                                                                           //Natural: ASSIGN #FCPA803.#FONT := '7'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                           //Natural: RESET #OUTPUT-REC
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        //*  12/15/99
        if (condition(pdaFcpaext.getExt_Pymnt_Alt_Addr_Ind().getBoolean()))                                                                                               //Natural: IF EXT.PYMNT-ALT-ADDR-IND
        {
            pnd_Ws_Pnd_J.setValue(2);                                                                                                                                     //Natural: ASSIGN #J := 2
            pnd_Ws_Pnd_K.setValue(3);                                                                                                                                     //Natural: ASSIGN #K := 3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_J.setValue(1);                                                                                                                                     //Natural: ASSIGN #J := 1
            pnd_Ws_Pnd_K.setValue(4);                                                                                                                                     //Natural: ASSIGN #K := 4
            //* 12/15
            setValueToSubstring(pdaFcpaext.getExt_Pymnt_Eft_Acct_Nbr(),ldaFcpl878c.getPnd_Fcpl878c_Pnd_Letter_Text().getValue(1,2),39,21);                                //Natural: MOVE EXT.PYMNT-EFT-ACCT-NBR TO SUBSTR ( #LETTER-TEXT ( 1,2 ) ,39,21 )
        }                                                                                                                                                                 //Natural: END-IF
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO #K
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Ws_Pnd_K)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl878c.getPnd_Fcpl878c_Pnd_Letter_Text().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I));                       //Natural: ASSIGN #OUTPUT-REC := #FCPL878C.#LETTER-TEXT ( #J,#I )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                                 //Natural: ASSIGN #CC := ' '
        pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("2");                                                                                                               //Natural: ASSIGN #FONT := '2'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().reset();                                                                                                        //Natural: RESET #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                                 //Natural: ASSIGN #CC := ' '
        pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("2");                                                                                                               //Natural: ASSIGN #FONT := '2'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue("Annuity Loan");                                                                                       //Natural: ASSIGN #OUTPUT-REC-DETAIL := 'Annuity Loan'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                                 //Natural: ASSIGN #CC := ' '
        pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("2");                                                                                                               //Natural: ASSIGN #FONT := '2'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue("Loan Unit");                                                                                          //Natural: ASSIGN #OUTPUT-REC-DETAIL := 'Loan Unit'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(1);                                                                                                             //Natural: ASSIGN #ADDR-IND := 1
                                                                                                                                                                          //Natural: PERFORM CHECK-PART
        sub_Check_Part();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().setValue(1);                                                                                                        //Natural: ASSIGN #BAR-LAST-PAGE := #FCPA803.#CURRENT-PAGE := 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().setValue(1);
                                                                                                                                                                          //Natural: PERFORM GEN-BARCODE
        sub_Gen_Barcode();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(true);                                                                                                          //Natural: ASSIGN #ODD-PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean())))                                                                                          //Natural: IF NOT #SIMPLEX
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Gen_Barcode() throws Exception                                                                                                                       //Natural: GEN-BARCODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  FIRST PAGE OF ENVELOPE
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Demand_Feed().setValue(true);                                                                                           //Natural: ASSIGN #BAR-DEMAND-FEED := TRUE
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Id_Num().nadd(1);                                                                                                   //Natural: ADD 1 TO #BARCODE-PDA.#BAR-ENV-ID-NUM
        }                                                                                                                                                                 //Natural: END-IF
        //*  LAST  PAGE OF ENVELOPE
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().equals(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page())))                                                //Natural: IF #BAR-LAST-PAGE = #FCPA803.#CURRENT-PAGE
        {
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Set_Last_Page().setValue(true);                                                                                         //Natural: ASSIGN #BAR-SET-LAST-PAGE := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_New_Run().getBoolean()))                                                                                      //Natural: IF #BAR-NEW-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpnbar.class , getCurrentProcessState(), pdaFcpabar.getPnd_Barcode_Pda());                                                                       //Natural: CALLNAT 'FCPNBAR' #BARCODE-PDA
        if (condition(Global.isEscape())) return;
    }
    private void sub_Barcode_Processing() throws Exception                                                                                                                //Natural: BARCODE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("1<");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '1<'
        FOR08:                                                                                                                                                            //Natural: FOR #J = 1 TO 17
        for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(17)); pnd_Ws_Pnd_J.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Barcode().getValue(pnd_Ws_Pnd_J));                           //Natural: ASSIGN #OUTPUT-REC-DETAIL := #BAR-BARCODE ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                             //Natural: ASSIGN #CC := ' '
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  12/15/99
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Set_Last_Page().getBoolean()))                                                                                //Natural: IF #BAR-SET-LAST-PAGE
        {
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee().setValue(pdaFcpa874a.getPnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme());                                                      //Natural: ASSIGN #FCPL876B.#ADDRESSEE := #STMNT-PYMNT-NME
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Id());                                                        //Natural: ASSIGN #FCPL876B.#BAR-ENV-ID := #BARCODE-PDA.#BAR-ENV-ID
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                  //Natural: ASSIGN #FCPL876B.CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr().setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                                  //Natural: ASSIGN #FCPL876B.CNTRCT-PPCN-NBR := EXT.CNTRCT-PPCN-NBR
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde().setValue(pdaFcpaext.getExt_Cntrct_Payee_Cde());                                                                //Natural: ASSIGN #FCPL876B.CNTRCT-PAYEE-CDE := EXT.CNTRCT-PAYEE-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde().setValue(pdaFcpaext.getExt_Cntrct_Hold_Cde());                                                                  //Natural: ASSIGN #FCPL876B.CNTRCT-HOLD-CDE := EXT.CNTRCT-HOLD-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Integrity_Counter());                          //Natural: ASSIGN #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER := #BARCODE-PDA.#BAR-ENV-INTEGRITY-COUNTER
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))                                                                                            //Natural: IF #FCPA803.#STMNT
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Pymnt_Check_Nbr().reset();                                                                                                //Natural: RESET #FCPL876B.#PYMNT-CHECK-NBR
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(false);                                                                                                  //Natural: ASSIGN #FCPL876B.#CHECK := FALSE
                //*  12/15/99
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                              //Natural: ASSIGN #FCPL876B.PYMNT-CHECK-NBR := EXT.PYMNT-CHECK-NBR
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(true);                                                                                                   //Natural: ASSIGN #FCPL876B.#CHECK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Envelopes());                                                  //Natural: ASSIGN #FCPL876B.#BAR-ENVELOPES := #BARCODE-PDA.#BAR-ENVELOPES
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Pages_In_Env());                                            //Natural: ASSIGN #FCPL876B.#BAR-PAGES-IN-ENV := #BARCODE-PDA.#BAR-PAGES-IN-ENV
            getWorkFiles().write(8, false, ldaFcpl876b.getPnd_Fcpl876b());                                                                                                //Natural: WRITE WORK FILE 8 #FCPL876B
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))                                                                                       //Natural: IF #FCPA803.#GLOBAL-PAY
            {
                getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",                                                //Natural: DISPLAY ( 1 ) ( HC = R ) '//ADDRESSEE' #FCPL876B.#ADDRESSEE ( HC = L ) '/BARCODE/ID' #FCPL876B.#BAR-ENV-ID ( HC = C ) 'PACKAGE/INTEG/RITY' #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER ( EM = �������9 ) '/ENVELOPE/NUMBER' #FCPL876B.#BAR-ENVELOPES ( EM = -Z,ZZZ,ZZ9 ) '# OF/PAGES IN/ENVELOPE' #FCPL876B.#BAR-PAGES-IN-ENV ( EM = -Z,ZZZ,ZZ9 ) '//CHECK' #FCPL876B.#CHECK ( EM = NO/YES ) '/ORGN/CODE' #FCPL876B.CNTRCT-ORGN-CDE ( LC = � ) '/CONTRACT/NUMBER' #FCPL876B.CNTRCT-PPCN-NBR ( HC = L ) '/PAYEE/CODE' #FCPL876B.CNTRCT-PAYEE-CDE ( HC = L ) '/HOLD/CODE' #FCPL876B.CNTRCT-HOLD-CDE '/CHECK/NUMBER' #FCPL876B.PYMNT-CHECK-NBR
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
                    
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Barcode_Input().resetInitial();                                                                                                 //Natural: RESET INITIAL #BARCODE-INPUT
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"ANNUITY LOAN DETAIL CONTROL REPORT",new TabSetting(120),
            "REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr());
    }
}
