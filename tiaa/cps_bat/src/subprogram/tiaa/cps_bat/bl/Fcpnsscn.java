/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:37 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnsscn
************************************************************
**        * FILE NAME            : Fcpnsscn.java
**        * CLASS NAME           : Fcpnsscn
**        * INSTANCE NAME        : Fcpnsscn
************************************************************
************************************************************************
* PROGRAM  : FCPNSSCN
* SYSTEM   : CPS
* TITLE    : DETERMINE ACCUMULATION SUBSCRIPTS FOR SS PROCESSING
* GENERATED: JUL 30,96
* HISTORY
*  08/25/99 - ALTHEA A. YOUNG
*             RE-COMPILED DUE TO 'CN' & 'SN' VALUES ADDED TO 'FCPLSSCN'.
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnsscn extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpasscn pdaFcpasscn;
    private PdaFcpassc1 pdaFcpassc1;
    private LdaFcplsscn ldaFcplsscn;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Idx;
    private DbsField pnd_Ws_Pnd_Error;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplsscn = new LdaFcplsscn();
        registerRecord(ldaFcplsscn);

        // parameters
        parameters = new DbsRecord();
        pdaFcpacrpt = new PdaFcpacrpt(parameters);
        pdaFcpasscn = new PdaFcpasscn(parameters);
        pdaFcpassc1 = new PdaFcpassc1(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Error = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplsscn.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnsscn() throws Exception
    {
        super("Fcpnsscn");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        DbsUtil.examine(new ExamineSource(ldaFcplsscn.getPnd_Fcplsscn_Pnd_Cntl_Table_Key().getValue("*"),true), new ExamineSearch(pdaFcpasscn.getPnd_Fcpasscn_Pnd_Search_Key(),  //Natural: EXAMINE FULL #CNTL-TABLE-KEY ( * ) FOR FULL #SEARCH-KEY GIVING INDEX IN #IDX
            true), new ExamineGivingIndex(pnd_Ws_Pnd_Idx));
        if (condition(pnd_Ws_Pnd_Idx.equals(getZero())))                                                                                                                  //Natural: IF #IDX = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
            sub_Error_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpassc1.getPnd_Fcpassc1_Pnd_Accum_Occur_1().setValue(ldaFcplsscn.getPnd_Fcplsscn_Pnd_Cntl_Table_Result().getValue(pnd_Ws_Pnd_Idx));                       //Natural: ASSIGN #ACCUM-OCCUR-1 := #CNTL-TABLE-RESULT ( #IDX )
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet54 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pdaFcpasscn.getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet54++;
            //*  LEON 'CN' 08/05/99
            //*  LEON 'SN' 08/05/99
            short decideConditionsMet58 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE ' ','R'
            if (condition((pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
            {
                decideConditionsMet58++;
                pdaFcpassc1.getPnd_Fcpassc1_Pnd_Accum_Occur_2().setValue(48);                                                                                             //Natural: ASSIGN #ACCUM-OCCUR-2 := 48
            }                                                                                                                                                             //Natural: VALUE 'C', 'CN'
            else if (condition((pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
            {
                decideConditionsMet58++;
                pdaFcpassc1.getPnd_Fcpassc1_Pnd_Accum_Occur_2().setValue(46);                                                                                             //Natural: ASSIGN #ACCUM-OCCUR-2 := 46
            }                                                                                                                                                             //Natural: VALUE 'S', 'SN'
            else if (condition((pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
            {
                decideConditionsMet58++;
                pdaFcpassc1.getPnd_Fcpassc1_Pnd_Accum_Occur_2().setValue(47);                                                                                             //Natural: ASSIGN #ACCUM-OCCUR-2 := 47
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
                sub_Error_Processing();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pdaFcpasscn.getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet54++;
            pdaFcpassc1.getPnd_Fcpassc1_Pnd_Accum_Occur_2().setValue(49);                                                                                                 //Natural: ASSIGN #ACCUM-OCCUR-2 := 49
        }                                                                                                                                                                 //Natural: VALUE 3,8
        else if (condition((pdaFcpasscn.getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpasscn.getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind().equals(8))))
        {
            decideConditionsMet54++;
            pdaFcpassc1.getPnd_Fcpassc1_Pnd_Accum_Occur_2().setValue(50);                                                                                                 //Natural: ASSIGN #ACCUM-OCCUR-2 := 50
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pdaFcpasscn.getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind().equals(6))))
        {
            decideConditionsMet54++;
            pdaFcpassc1.getPnd_Fcpassc1_Pnd_Accum_Occur_2().setValue(45);                                                                                                 //Natural: ASSIGN #ACCUM-OCCUR-2 := 45
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
            sub_Error_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Error.getBoolean() && ! (pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().getBoolean())))                                                      //Natural: IF #ERROR AND NOT #NO-ABEND
        {
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-PROCESSING
        //* *********************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* ************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* **********************************
    }
    private void sub_Error_Processing() throws Exception                                                                                                                  //Natural: ERROR-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Error.setValue(true);                                                                                                                                  //Natural: ASSIGN #ERROR := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        getReports().write(15, "***",new TabSetting(25),"ERROR OCCURED IN THE CONTROL PROCESS",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"UNKNOWN DATA FOR CONTROL PURPOSES",new  //Natural: WRITE ( 15 ) '***' 25T 'ERROR OCCURED IN THE CONTROL PROCESS' 77T '***' / '***' 25T 'UNKNOWN DATA FOR CONTROL PURPOSES' 77T '***' / '***' 25T 'ORGIN CODE..:' CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'TYPE  CODE..:' CNTRCT-TYPE-CDE 77T '***' / '***' 25T 'C & R.......:' CNTRCT-CANCEL-RDRW-ACTVTY-CDE 77T '***' / '***' 25T 'PAYMENT TYPE:' PYMNT-PAY-TYPE-REQ-IND 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORGIN CODE..:",pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Orgn_Cde(),new TabSetting(77),"***",NEWLINE,"***",new 
            TabSetting(25),"TYPE  CODE..:",pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Type_Cde(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"C & R.......:",pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde(),new 
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PAYMENT TYPE:",pdaFcpasscn.getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind(),new TabSetting(77),
            "***");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        getReports().write(15, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new //Natural: WRITE ( 15 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(15, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new         //Natural: WRITE ( 15 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
