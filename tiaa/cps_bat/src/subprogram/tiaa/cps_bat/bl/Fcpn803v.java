/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:42 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn803v
************************************************************
**        * FILE NAME            : Fcpn803v.java
**        * CLASS NAME           : Fcpn803v
**        * INSTANCE NAME        : Fcpn803v
************************************************************
************************************************************************
* SUBPROGRAM: FCPN803V
* SYSTEM    : CPS
* TITLE     : IAR RESTRUCTURE
* FUNCTION  : PRINT VOID PAGE
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn803v extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803 pdaFcpa803;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Save_Fields;
    private DbsField pnd_Save_Fields_Pnd_Stmnt;
    private DbsField pnd_Save_Fields_Pnd_Full_Xerox;
    private DbsField pnd_Save_Fields_Pnd_Simplex;
    private DbsField pnd_Save_Fields_Pnd_Check_To_Annt;
    private DbsField pnd_Save_Fields_Pnd_Stmnt_To_Annt;
    private DbsField pnd_Save_Fields_Pnd_Check;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_R;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803 = new PdaFcpa803(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Save_Fields = localVariables.newGroupInRecord("pnd_Save_Fields", "#SAVE-FIELDS");
        pnd_Save_Fields_Pnd_Stmnt = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Stmnt", "#STMNT", FieldType.BOOLEAN, 1);
        pnd_Save_Fields_Pnd_Full_Xerox = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Full_Xerox", "#FULL-XEROX", FieldType.BOOLEAN, 1);
        pnd_Save_Fields_Pnd_Simplex = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Simplex", "#SIMPLEX", FieldType.BOOLEAN, 1);
        pnd_Save_Fields_Pnd_Check_To_Annt = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Check_To_Annt", "#CHECK-TO-ANNT", FieldType.BOOLEAN, 
            1);
        pnd_Save_Fields_Pnd_Stmnt_To_Annt = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Stmnt_To_Annt", "#STMNT-TO-ANNT", FieldType.BOOLEAN, 
            1);
        pnd_Save_Fields_Pnd_Check = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Check", "#CHECK", FieldType.BOOLEAN, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_R = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_R", "#R", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn803v() throws Exception
    {
        super("Fcpn803v");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Save_Fields.setValuesByName(pdaFcpa803.getPnd_Fcpa803());                                                                                                     //Natural: MOVE BY NAME #FCPA803 TO #SAVE-FIELDS
        pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                             //Natural: ASSIGN #FCPA803.#STMNT := TRUE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                        //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().setValue(true);                                                                                                           //Natural: ASSIGN #FCPA803.#SIMPLEX := TRUE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().setValue(false);                                                                                                    //Natural: ASSIGN #FCPA803.#CHECK-TO-ANNT := FALSE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().setValue(false);                                                                                                    //Natural: ASSIGN #FCPA803.#STMNT-TO-ANNT := FALSE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Check().setValue(false);                                                                                                            //Natural: ASSIGN #FCPA803.#CHECK := FALSE
        //*  CALLNAT 'FCPN803X' USING #FCPA803                   /* ROXAN
        DbsUtil.callnat(Fcpn803x.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pnd_Ws_Pnd_R);                                                            //Natural: CALLNAT 'FCPN803X' USING #FCPA803 #R
        if (condition(Global.isEscape())) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 12
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(12)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("11VOID--------VOID--------VOID");                                                                        //Natural: MOVE '11VOID--------VOID--------VOID' TO #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().reset();                                                                                                        //Natural: RESET #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803().setValuesByName(pnd_Save_Fields);                                                                                                     //Natural: MOVE BY NAME #SAVE-FIELDS TO #FCPA803
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
