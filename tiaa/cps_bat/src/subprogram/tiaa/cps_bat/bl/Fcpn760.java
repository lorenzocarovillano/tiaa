/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:22 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn760
************************************************************
**        * FILE NAME            : Fcpn760.java
**        * CLASS NAME           : Fcpn760
**        * INSTANCE NAME        : Fcpn760
************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn760 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Addr_Lines;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_I;
    private DbsField pnd_Ws_Index;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Addr_Lines = localVariables.newFieldInRecord("pnd_Ws_Addr_Lines", "#WS-ADDR-LINES", FieldType.NUMERIC, 1);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Ws_Index = localVariables.newFieldInRecord("pnd_Ws_Index", "#WS-INDEX", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Ws_Addr_Lines.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_Ws_Index.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn760() throws Exception
    {
        super("Fcpn760");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_I.setValue(1);                                                                                                                                                //Natural: ASSIGN #I = 1
        if (condition(DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(pnd_I),"'CR '")))                                                      //Natural: IF PYMNT-NME ( #I ) = MASK ( 'CR ' )
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "17******", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(pnd_I))); //Natural: COMPRESS '17******' PYMNT-ADDR-LINE1-TXT ( #I ) INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WS-ADDR-LINES
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "17******", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(pnd_I)));          //Natural: COMPRESS '17******' PYMNT-NME ( #I ) INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(pnd_I))); //Natural: COMPRESS '*7******' PYMNT-ADDR-LINE1-TXT ( #I ) INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Addr_Lines.nadd(2);                                                                                                                                    //Natural: ADD 2 TO #WS-ADDR-LINES
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(pnd_I)));   //Natural: COMPRESS '*7******' PYMNT-ADDR-LINE2-TXT ( #I ) INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WS-ADDR-LINES
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(pnd_I)));   //Natural: COMPRESS '*7******' PYMNT-ADDR-LINE3-TXT ( #I ) INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WS-ADDR-LINES
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(pnd_I)));   //Natural: COMPRESS '*7******' PYMNT-ADDR-LINE4-TXT ( #I ) INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WS-ADDR-LINES
        if (condition(pnd_Ws_Addr_Lines.equals(4)))                                                                                                                       //Natural: IF #WS-ADDR-LINES = 4
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(pnd_I))); //Natural: COMPRESS '*7******' PYMNT-ADDR-LINE5-TXT ( #I ) INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WS-ADDR-LINES
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals("0"))  //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( #I ) = ' ' OR = '0' ) OR PYMNT-NME ( 1 ) = MASK ( 'CR ' )
            || DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1),"'CR '")))
        {
            if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals("0")))) //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( #I ) = ' ' OR = '0' )
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("+7++++++", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1), "H'0000'", "A/C", "H'00'",                //Natural: COMPRESS '+7++++++' PYMNT-NME ( 1 ) H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR ( 1 ) INTO #WS-REC-1
                    pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1)));
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("+7++++++", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1)));                                         //Natural: COMPRESS '+7++++++' PYMNT-NME ( 1 ) INTO #WS-REC-1
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1.setValue(" 7");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 7'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Index.setValue(2);                                                                                                                                         //Natural: ASSIGN #WS-INDEX = 2
        //*  FORMAT BARCODE AND ADDR LINES
        DbsUtil.callnat(Fcpn753.class , getCurrentProcessState(), pdaFcpa700.getPnd_Ws_Header_Record().getValue("*"), pdaFcpa700.getPnd_Ws_Occurs().getValue("*","*"),    //Natural: CALLNAT 'FCPN753' #WS-HEADER-RECORD ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS ( *,* ) #WS-INDEX
            pdaFcpa700.getPnd_Ws_Name_N_Address().getValue("*","*"), pnd_Ws_Index);
        if (condition(Global.isEscape())) return;
    }

    //
}
