/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:01 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn878t
************************************************************
**        * FILE NAME            : Fcpn878t.java
**        * CLASS NAME           : Fcpn878t
**        * INSTANCE NAME        : Fcpn878t
************************************************************
************************************************************************
* SUBPROGRAM : FCPN878T
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AL" ANNUITANT STATEMENTS - CONTRACT HEADERS
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn878t extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803 pdaFcpa803;
    private LdaFcpl874t ldaFcpl874t;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl874t = new LdaFcpl874t();
        registerRecord(ldaFcpl874t);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803 = new PdaFcpa803(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl874t.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn878t() throws Exception
    {
        super("Fcpn878t");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("02               TIAA");                                                                                     //Natural: ASSIGN #OUTPUT-REC := '02               TIAA'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().reset();                                                                                                        //Natural: RESET #OUTPUT-REC-DETAIL
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 2               Contract");                                                                                 //Natural: ASSIGN #OUTPUT-REC := ' 2               Contract'
        setValueToSubstring("Payment",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),52,8);                                                                                   //Natural: MOVE 'Payment' TO SUBSTR ( #OUTPUT-REC,52,8 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
