/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:38 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn803l
************************************************************
**        * FILE NAME            : Fcpn803l.java
**        * CLASS NAME           : Fcpn803l
**        * INSTANCE NAME        : Fcpn803l
************************************************************
************************************************************************
* SUBPROGRAM : FCPN803L
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AP" ANNUITANT STATEMENTS - DEDUCTION LEDGENDS
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
* 12/2015    :J.OSTEEN - ADD CHILD SUPPORT DEDUCTION PROCESSING /* JWO
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn803l extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa803l pdaFcpa803l;
    private LdaFcpl803l ldaFcpl803l;
    private PdaFcppda_M pdaFcppda_M;
    private PdaTbldcoda pdaTbldcoda;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_State_Idx;
    private DbsField pnd_Ws_Pnd_Ded_Disp_Array;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl803l = new LdaFcpl803l();
        registerRecord(ldaFcpl803l);
        localVariables = new DbsRecord();
        pdaFcppda_M = new PdaFcppda_M(localVariables);
        pdaTbldcoda = new PdaTbldcoda(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa801b = new PdaFcpa801b(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa803l = new PdaFcpa803l(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_State_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Idx", "#STATE-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ded_Disp_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Ded_Disp_Array", "#DED-DISP-ARRAY", FieldType.STRING, 36, new DbsArrayController(1, 
            10));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803l.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn803l() throws Exception
    {
        super("Fcpn803l");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                                 //Natural: ASSIGN TBLDCODA.#MODE := 'T'
        if (condition(pdaFcpa803l.getPnd_Fcpa803l_Pnd_Deductions_Loaded().getBoolean()))                                                                                  //Natural: IF #DEDUCTIONS-LOADED
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM LOAD-DED-TABLE
            sub_Load_Ded_Table();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(1).notEquals(" ")))                                                                  //Natural: IF #DED-PYMNT-TABLE ( 1 ) NE ' '
        {
            if (condition(pdaFcpa803l.getPnd_Fcpa803l_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                           //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
            {
                pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(1).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Federal_Ledgend().getValue(2));                  //Natural: ASSIGN #DED-LEDGEND-TABLE ( 1 ) := #FEDERAL-LEDGEND ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(1).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Federal_Ledgend().getValue(1));                  //Natural: ASSIGN #DED-LEDGEND-TABLE ( 1 ) := #FEDERAL-LEDGEND ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(2).notEquals(" ")))                                                                  //Natural: IF #DED-PYMNT-TABLE ( 2 ) NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEDGEND
            sub_Format_State_Ledgend();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  JWO1
        pnd_Ws_Pnd_J.reset();                                                                                                                                             //Natural: RESET #J #DED-DISP-ARRAY ( * )
        pnd_Ws_Pnd_Ded_Disp_Array.getValue("*").reset();
        //*  FOR #I = 1 TO 14
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 15
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(15)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(pnd_Ws_Pnd_I).notEquals(" ")))                                                   //Natural: IF #DED-PYMNT-TABLE ( #I ) NE ' '
            {
                if (condition(pnd_Ws_Pnd_J.notEquals(10)))                                                                                                                //Natural: IF #J NE 10
                {
                    pnd_Ws_Pnd_J.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #J
                }                                                                                                                                                         //Natural: END-IF
                setValueToSubstring(pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(pnd_Ws_Pnd_I),pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_Ws_Pnd_J),        //Natural: MOVE #DED-PYMNT-TABLE ( #I ) TO SUBSTR ( #DED-DISP-ARRAY ( #J ) ,1,1 )
                    1,1);
                if (condition(pnd_Ws_Pnd_J.equals(10) && pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Pnd().greater(10)))                                                //Natural: IF #J = 10 AND #PYMNT-DED-# GT 10
                {
                    setValueToSubstring(ldaFcpl803l.getPnd_Fcpl803l_Pnd_More_Than_10_Ded(),pnd_Ws_Pnd_Ded_Disp_Array.getValue(10),3,34);                                  //Natural: MOVE #MORE-THAN-10-DED TO SUBSTR ( #DED-DISP-ARRAY ( 10 ) ,3,34 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      IF #I NE 14                                             /* JWO1
                    //*  JWO1
                    //*  JWO1
                    short decideConditionsMet177 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #I;//Natural: VALUE 1:13
                    if (condition(((pnd_Ws_Pnd_I.greaterOrEqual(1) && pnd_Ws_Pnd_I.lessOrEqual(13)))))
                    {
                        decideConditionsMet177++;
                        //*  JWO1
                        setValueToSubstring(pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(pnd_Ws_Pnd_I),pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_Ws_Pnd_J),  //Natural: MOVE #DED-LEDGEND-TABLE ( #I ) TO SUBSTR ( #DED-DISP-ARRAY ( #J ) ,3,34 )
                            3,34);
                        //*      ELSE
                    }                                                                                                                                                     //Natural: VALUE 14
                    else if (condition((pnd_Ws_Pnd_I.equals(14))))
                    {
                        decideConditionsMet177++;
                        //*  JWO1
                        setValueToSubstring("= Dental",pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_Ws_Pnd_J),3,34);                                                            //Natural: MOVE '= Dental' TO SUBSTR ( #DED-DISP-ARRAY ( #J ) ,3,34 )
                    }                                                                                                                                                     //Natural: VALUE 15
                    else if (condition((pnd_Ws_Pnd_I.equals(15))))
                    {
                        decideConditionsMet177++;
                        //*  JWO1
                        //*  JWO1
                        setValueToSubstring("= Child Support",pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_Ws_Pnd_J),3,34);                                                     //Natural: MOVE '= Child Support' TO SUBSTR ( #DED-DISP-ARRAY ( #J ) ,3,34 )
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        ignore();
                        //*  JWO1
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*      END-IF                                                  /* JWO1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 9 STEP 2
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(9)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_Ws_Pnd_I));                                                 //Natural: MOVE #DED-DISP-ARRAY ( #I ) TO #OUTPUT-REC-DETAIL
            setValueToSubstring(pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_Ws_Pnd_I.getDec().add(1)),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),41,                    //Natural: MOVE #DED-DISP-ARRAY ( #I+1 ) TO SUBSTR ( #OUTPUT-REC-DETAIL,41,36 )
                36);
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().equals(" ")))                                                                                 //Natural: IF #OUTPUT-REC-DETAIL = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEDGEND
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE
        //* **************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DED-TABLE
        //* *******************************
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Format_State_Ledgend() throws Exception                                                                                                              //Natural: FORMAT-STATE-LEDGEND
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde().lessOrEqual("57")))             //Natural: IF ANNT-RSDNCY-CDE = '01' THRU '57'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(2).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_State_Ledgend().getValue(1));                        //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := #STATE-LEDGEND ( 1 )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top().equals(getZero())))                                                                               //Natural: IF #STATE-TABLE-TOP = 0
        {
                                                                                                                                                                          //Natural: PERFORM GET-STATE
            sub_Get_State();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Cde_Table().getValue(1,":",pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top())),       //Natural: EXAMINE #STATE-CDE-TABLE ( 1:#STATE-TABLE-TOP ) FOR ANNT-RSDNCY-CDE GIVING INDEX IN #STATE-IDX
                new ExamineSearch(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde()), new ExamineGivingIndex(pnd_Ws_Pnd_State_Idx));
            if (condition(pnd_Ws_Pnd_State_Idx.equals(getZero())))                                                                                                        //Natural: IF #STATE-IDX = 0
            {
                                                                                                                                                                          //Natural: PERFORM GET-STATE
                sub_Get_State();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(2).setValue(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx));   //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := #STATE-TABLE ( #STATE-IDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_State() throws Exception                                                                                                                         //Natural: GET-STATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(19);                                                                                                              //Natural: ASSIGN TBLDCODA.#TABLE-ID := 19
        pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde());                                                                       //Natural: ASSIGN TBLDCODA.#CODE := ANNT-RSDNCY-CDE
        DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pdaFcppda_M.getMsg_Info_Sub());                                              //Natural: CALLNAT 'TBLDCOD' USING TBLDCODA MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcppda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF ##MSG-DATA ( 1 ) = ' '
        {
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top().nadd(1);                                                                                                    //Natural: ASSIGN #STATE-TABLE-TOP := #STATE-TABLE-TOP + 1
            pnd_Ws_Pnd_State_Idx.setValue(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table_Top());                                                                             //Natural: ASSIGN #STATE-IDX := #STATE-TABLE-TOP
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Cde_Table().getValue(pnd_Ws_Pnd_State_Idx).setValue(pdaFcpa803l.getPnd_Fcpa803l_Annt_Rsdncy_Cde());                     //Natural: ASSIGN #STATE-CDE-TABLE ( #STATE-IDX ) := ANNT-RSDNCY-CDE
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx).setValue(DbsUtil.compress("=", pdaTbldcoda.getTbldcoda_Pnd_Target(),             //Natural: COMPRESS '=' TBLDCODA.#TARGET #STATE-LEDGEND ( 3 ) INTO #STATE-TABLE ( #STATE-IDX )
                ldaFcpl803l.getPnd_Fcpl803l_Pnd_State_Ledgend().getValue(3)));
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(2).setValue(pdaFcpa803l.getPnd_Fcpa803l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx));       //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := #STATE-TABLE ( #STATE-IDX )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(2).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_State_Ledgend().getValue(2));                        //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := #STATE-LEDGEND ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Ded_Table() throws Exception                                                                                                                    //Natural: LOAD-DED-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803l.getPnd_Fcpa803l_Pnd_Deductions_Loaded().setValue(true);                                                                                               //Natural: ASSIGN #DEDUCTIONS-LOADED := TRUE
        pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(3).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Local_Ledgend());                                        //Natural: ASSIGN #DED-LEDGEND-TABLE ( 3 ) := #LOCAL-LEDGEND
        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(21);                                                                                                              //Natural: ASSIGN TBLDCODA.#TABLE-ID := 21
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(10)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaTbldcoda.getTbldcoda_Pnd_Code().setValueEdited(pnd_Ws_Pnd_I,new ReportEditMask("999"));                                                                    //Natural: MOVE EDITED #I ( EM = 999 ) TO TBLDCODA.#CODE
            DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pdaFcppda_M.getMsg_Info_Sub());                                          //Natural: CALLNAT 'TBLDCOD' USING TBLDCODA MSG-INFO-SUB
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaFcppda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                        //Natural: IF ##MSG-DATA ( 1 ) = ' '
            {
                pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(pnd_Ws_Pnd_I.getDec().add(3)).setValue("=");                                                 //Natural: MOVE '=' TO #DED-LEDGEND-TABLE ( #I+3 )
                setValueToSubstring(pdaTbldcoda.getTbldcoda_Pnd_Target(),pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(pnd_Ws_Pnd_I.getDec().add(3)),      //Natural: MOVE TBLDCODA.#TARGET TO SUBSTR ( #DED-LEDGEND-TABLE ( #I+3 ) ,3,31 )
                    3,31);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803l.getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table().getValue(pnd_Ws_Pnd_I.getDec().add(3)).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Unknown_Ledgend());   //Natural: MOVE #UNKNOWN-LEDGEND TO #DED-LEDGEND-TABLE ( #I+3 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
