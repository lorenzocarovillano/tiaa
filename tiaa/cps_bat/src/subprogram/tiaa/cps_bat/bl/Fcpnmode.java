/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:20 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnmode
************************************************************
**        * FILE NAME            : Fcpnmode.java
**        * CLASS NAME           : Fcpnmode
**        * INSTANCE NAME        : Fcpnmode
************************************************************
************************************************************************
* PROGRAM  : FCPNMODE
* SYSTEM   : CPS
* FUNCTION : THIS PROGRAM DECODES CONTRACT MODE CODE.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnmode extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpamode pdaFcpamode;
    private LdaFcplmode ldaFcplmode;
    private PdaTbldcoda pdaTbldcoda;
    private LdaFcpltbcd ldaFcpltbcd;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Idx;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplmode = new LdaFcplmode();
        registerRecord(ldaFcplmode);
        localVariables = new DbsRecord();
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        ldaFcpltbcd = new LdaFcpltbcd();
        registerRecord(ldaFcpltbcd);

        // parameters
        parameters = new DbsRecord();
        pdaFcpamode = new PdaFcpamode(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplmode.initializeValues();
        ldaFcpltbcd.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnmode() throws Exception
    {
        super("Fcpnmode");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFcpamode.getPnd_Fcpamode_Pnd_Return_Cde().setValue(false);                                                                                                     //Natural: ASSIGN #RETURN-CDE := FALSE
        if (condition(pdaFcpamode.getPnd_Fcpamode_Pnd_Predict_Table().getBoolean()))                                                                                      //Natural: IF #PREDICT-TABLE
        {
                                                                                                                                                                          //Natural: PERFORM GET-MODE-DESC
            sub_Get_Mode_Desc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DECODE-MODE
            sub_Decode_Mode();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MODE-DESC
        //* ******************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECODE-MODE
        //* ****************************
    }
    private void sub_Get_Mode_Desc() throws Exception                                                                                                                     //Natural: GET-MODE-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                                 //Natural: ASSIGN TBLDCODA.#MODE := 'T'
        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(5);                                                                                                               //Natural: ASSIGN TBLDCODA.#TABLE-ID := 5
        pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde_X());                                                                     //Natural: ASSIGN TBLDCODA.#CODE := #FCPAMODE.CNTRCT-MODE-CDE-X
        DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), ldaFcpltbcd.getTbldcoda_Msg());                                              //Natural: CALLNAT 'TBLDCOD' USING TBLDCODA TBLDCODA-MSG
        if (condition(Global.isEscape())) return;
        if (condition(ldaFcpltbcd.getTbldcoda_Msg_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF ##MSG-DATA ( 1 ) = ' '
        {
            pdaFcpamode.getPnd_Fcpamode_Pnd_Return_Cde().setValue(true);                                                                                                  //Natural: ASSIGN #RETURN-CDE := TRUE
            pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc().setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                   //Natural: ASSIGN #FCPAMODE.#MODE-DESC := TBLDCODA.#TARGET
            pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc_Length().setValue(18);                                                                                              //Natural: ASSIGN #FCPAMODE.#MODE-DESC-LENGTH := 18
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc().setValue(pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde_X());                                                        //Natural: ASSIGN #FCPAMODE.#MODE-DESC := #FCPAMODE.CNTRCT-MODE-CDE-X
            pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc_Length().setValue(3);                                                                                               //Natural: ASSIGN #FCPAMODE.#MODE-DESC-LENGTH := 3
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Decode_Mode() throws Exception                                                                                                                       //Natural: DECODE-MODE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpamode.getPnd_Fcpamode_Pnd_Return_Cde().setValue(true);                                                                                                      //Natural: ASSIGN #RETURN-CDE := TRUE
        short decideConditionsMet76 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #FCPAMODE.CNTRCT-MODE-CDE;//Natural: VALUE 100
        if (condition((pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde().equals(100))))
        {
            decideConditionsMet76++;
            pnd_Idx.setValue(1);                                                                                                                                          //Natural: ASSIGN #IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde().greaterOrEqual(601) && pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde().lessOrEqual(603)))))
        {
            decideConditionsMet76++;
            pnd_Idx.setValue(2);                                                                                                                                          //Natural: ASSIGN #IDX := 2
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde().greaterOrEqual(701) && pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde().lessOrEqual(706)))))
        {
            decideConditionsMet76++;
            pnd_Idx.setValue(3);                                                                                                                                          //Natural: ASSIGN #IDX := 3
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde().greaterOrEqual(801) && pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde().lessOrEqual(812)))))
        {
            decideConditionsMet76++;
            pnd_Idx.setValue(4);                                                                                                                                          //Natural: ASSIGN #IDX := 4
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Idx.reset();                                                                                                                                              //Natural: RESET #IDX
            pdaFcpamode.getPnd_Fcpamode_Pnd_Return_Cde().setValue(false);                                                                                                 //Natural: ASSIGN #RETURN-CDE := FALSE
            ldaFcplmode.getPnd_Fcplmode_Pnd_Mode_Desc().getValue(pnd_Idx.getInt() + 1).setValue(pdaFcpamode.getPnd_Fcpamode_Cntrct_Mode_Cde_X());                         //Natural: ASSIGN #FCPLMODE.#MODE-DESC ( #IDX ) := #FCPAMODE.CNTRCT-MODE-CDE-X
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc().setValue(ldaFcplmode.getPnd_Fcplmode_Pnd_Mode_Desc().getValue(pnd_Idx.getInt() + 1));                                 //Natural: ASSIGN #FCPAMODE.#MODE-DESC := #FCPLMODE.#MODE-DESC ( #IDX )
        pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc_Length().compute(new ComputeParameters(false, pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc_Length()), ldaFcplmode.getPnd_Fcplmode_Pnd_Mode_Desc_Length().getValue(pnd_Idx.getInt()  //Natural: ASSIGN #FCPAMODE.#MODE-DESC-LENGTH := #FCPLMODE.#MODE-DESC-LENGTH ( #IDX ) + #FCPLMODE.#STMNT-INCREASE ( #IDX )
            + 1).add(ldaFcplmode.getPnd_Fcplmode_Pnd_Stmnt_Increase().getValue(pnd_Idx.getInt() + 1)));
    }

    //
}
