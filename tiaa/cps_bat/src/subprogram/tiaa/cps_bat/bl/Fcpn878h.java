/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:00 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn878h
************************************************************
**        * FILE NAME            : Fcpn878h.java
**        * CLASS NAME           : Fcpn878h
**        * INSTANCE NAME        : Fcpn878h
************************************************************
************************************************************************
* SUBPROGRAM : FCPN878H
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AL" ANNUITANT STATEMENTS - HEADERS
* HISTORY    :
*     07/98    R. CARREON
*              CHECK AND STATEMENT PRINTING
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
* 08/23/2019 : REPLACING PDQ MODULES WITH ADS MODULES I.E.
*              PDQA360,PDQA362,PDQN360,PDQN362 TO
*              ADSA360,ADSA362,ADSN360,ADSN362 RESPECTIVELY. -TAG:VIKRAM
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn878h extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa878h pdaFcpa878h;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpamode pdaFcpamode;
    private PdaAdsa360 pdaAdsa360;
    private PdaAdsa362 pdaAdsa362;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Hdr_Array;
    private DbsField pnd_Ws_Pnd_Page_Num;
    private DbsField pnd_Ws_Pnd_Ia_Ppcn;
    private DbsField pnd_Ws_Pnd_Da_Ppcn;
    private DbsField pnd_Ws_Pnd_Annt_Starting_Date;
    private DbsField pnd_Ws_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Pnd_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Orgn_Chk_Dte;

    private DbsGroup pnd_Orgn_Chk_Dte__R_Field_1;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Filler;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A;
    private DbsField pnd_Orgn_Chk_Dte_A4;

    private DbsGroup pnd_Orgn_Chk_Dte_A4__R_Field_2;
    private DbsField pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpamode = new PdaFcpamode(localVariables);
        pdaAdsa360 = new PdaAdsa360(localVariables);
        pdaAdsa362 = new PdaAdsa362(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa878h = new PdaFcpa878h(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Hdr_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Hdr_Array", "#HDR-ARRAY", FieldType.STRING, 143, new DbsArrayController(1, 5));
        pnd_Ws_Pnd_Page_Num = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Page_Num", "#PAGE-NUM", FieldType.STRING, 3);
        pnd_Ws_Pnd_Ia_Ppcn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ia_Ppcn", "#IA-PPCN", FieldType.STRING, 9);
        pnd_Ws_Pnd_Da_Ppcn = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Da_Ppcn", "#DA-PPCN", FieldType.STRING, 10, new DbsArrayController(1, 6));
        pnd_Ws_Pnd_Annt_Starting_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Annt_Starting_Date", "#ANNT-STARTING-DATE", FieldType.STRING, 10);
        pnd_Ws_Pnd_Pymnt_Check_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.STRING, 10);
        pnd_Ws_Pnd_Pymnt_Check_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", FieldType.STRING, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Orgn_Chk_Dte = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte", "#ORGN-CHK-DTE", FieldType.NUMERIC, 9);

        pnd_Orgn_Chk_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte__R_Field_1", "REDEFINE", pnd_Orgn_Chk_Dte);
        pnd_Orgn_Chk_Dte_Pnd_Filler = pnd_Orgn_Chk_Dte__R_Field_1.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A = pnd_Orgn_Chk_Dte__R_Field_1.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A", "#ORGN-CHK-DTE-A", FieldType.STRING, 
            8);
        pnd_Orgn_Chk_Dte_A4 = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte_A4", "#ORGN-CHK-DTE-A4", FieldType.STRING, 4);

        pnd_Orgn_Chk_Dte_A4__R_Field_2 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte_A4__R_Field_2", "REDEFINE", pnd_Orgn_Chk_Dte_A4);
        pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D = pnd_Orgn_Chk_Dte_A4__R_Field_2.newFieldInGroup("pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D", "#ORGN-CHK-DTE-D", 
            FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn878h() throws Exception
    {
        super("Fcpn878h");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        if (condition(pdaFcpa878h.getPnd_Fcpa878h_Pnd_Gen_Headers().getBoolean()))                                                                                        //Natural: IF #GEN-HEADERS
        {
            pdaFcpa878h.getPnd_Fcpa878h_Pnd_Gen_Headers().setValue(false);                                                                                                //Natural: ASSIGN #GEN-HEADERS := FALSE
            pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue("*").reset();                                                                                         //Natural: RESET #HEADER-ARRAY ( * ) #HDR-ARRAY ( * )
            pnd_Ws_Pnd_Hdr_Array.getValue("*").reset();
            pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(1).setValue("11Annuity Loan Payment For:");                                                           //Natural: MOVE '11Annuity Loan Payment For:' TO #HEADER-ARRAY ( 1 )
            pdaAdsa360.getPnd_In_Data_Pnd_First_Name().setValue(pdaFcpa878h.getPnd_Fcpa878h_Ph_First_Name());                                                             //Natural: ASSIGN #FIRST-NAME := #FCPA878H.PH-FIRST-NAME
            pdaAdsa362.getPnd_In_Middle_Name().setValue(pdaFcpa878h.getPnd_Fcpa878h_Ph_Middle_Name());                                                                    //Natural: ASSIGN #IN-MIDDLE-NAME := #FCPA878H.PH-MIDDLE-NAME
            pdaAdsa360.getPnd_In_Data_Pnd_Last_Name().setValue(pdaFcpa878h.getPnd_Fcpa878h_Ph_Last_Name());                                                               //Natural: ASSIGN #LAST-NAME := #FCPA878H.PH-LAST-NAME
            //* *CALLNAT 'PDQN362' /*    VIKRAM
            //*     VIKRAM
            DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pdaAdsa362.getPnd_In_Middle_Name(), pdaAdsa362.getPnd_Out_Middle_Name(), pdaAdsa362.getPnd_Out_Suffix(),  //Natural: CALLNAT 'ADSN362' USING #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
                pdaAdsa362.getPnd_Out_Prefix(), pdaAdsa362.getPnd_Out_Return_Code());
            if (condition(Global.isEscape())) return;
            pdaAdsa360.getPnd_In_Data_Pnd_Prefix().setValue(pdaAdsa362.getPnd_Out_Prefix());                                                                              //Natural: ASSIGN #PREFIX := #OUT-PREFIX
            pdaAdsa360.getPnd_In_Data_Pnd_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                    //Natural: ASSIGN #MIDDLE-NAME := #OUT-MIDDLE-NAME
            pdaAdsa360.getPnd_In_Data_Pnd_Suffix().setValue(pdaAdsa362.getPnd_Out_Suffix());                                                                              //Natural: ASSIGN #SUFFIX := #OUT-SUFFIX
            pdaAdsa360.getPnd_In_Data_Pnd_Length().setValue(38);                                                                                                          //Natural: ASSIGN #LENGTH := 38
            pdaAdsa360.getPnd_In_Data_Pnd_Format().setValue("1");                                                                                                         //Natural: ASSIGN #FORMAT := '1'
            //* *CALLNAT 'PDQN360'        USING #IN-DATA #OUT-DATA /* VIKRAM
            //*  VIKRAM
            DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pdaAdsa360.getPnd_In_Data(), pdaAdsa360.getPnd_Out_Data());                                         //Natural: CALLNAT 'ADSN360' USING #IN-DATA #OUT-DATA
            if (condition(Global.isEscape())) return;
            setValueToSubstring(pdaAdsa360.getPnd_Out_Data_Pnd_Name_Out(),pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(1),29,pdaAdsa360.getPnd_Out_Data_Pnd_Length_Out().getInt()); //Natural: MOVE #NAME-OUT TO SUBSTR ( #HEADER-ARRAY ( 1 ) ,29,#LENGTH-OUT )
            //*   #HDR-ARRAY(1)               := ' 7'
            //*  CHECK DATE ONLY FOR CHECKS TO ANNUITANT
            //*  CALLNAT 'FCPN803X' USING #FCPA803
            //*                                                    VF 8/99
            if (condition(pdaFcpa878h.getPnd_Fcpa878h_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                       //Natural: IF #FCPA878H.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                pnd_Ws_Pnd_Annt_Starting_Date.setValueEdited(pdaFcpa878h.getPnd_Fcpa878h_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YYYY"));                          //Natural: MOVE EDITED #FCPA878H.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YYYY ) TO #WS.#ANNT-STARTING-DATE
                pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2).setValue("02Effective Date:");                                                                 //Natural: ASSIGN #HEADER-ARRAY ( 2 ) := '02Effective Date:'
                setValueToSubstring(pnd_Ws_Pnd_Annt_Starting_Date,pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),19,10);                                      //Natural: MOVE #WS.#ANNT-STARTING-DATE TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,19,10 )
                pnd_Ws_Pnd_Pymnt_Check_Dte.setValueEdited(pdaFcpa878h.getPnd_Fcpa878h_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                //Natural: MOVE EDITED #FCPA878H.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #WS.#PYMNT-CHECK-DTE
                setValueToSubstring("Check Date:",pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),31,13);                                                      //Natural: MOVE 'Check Date:' TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,31,13 )
                setValueToSubstring(pnd_Ws_Pnd_Pymnt_Check_Dte,pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),43,10);                                         //Natural: MOVE #WS.#PYMNT-CHECK-DTE TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,43,10 )
                pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa878h.getPnd_Fcpa878h_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #FCPA878H.CNR-ORGNL-INVRSE-DTE
                pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
                pnd_Ws_Pnd_Pymnt_Check_Dte.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YYYY"));                                       //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YYYY ) TO #WS.#PYMNT-CHECK-DTE
                setValueToSubstring("Original Check Date: ",pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),55,21);                                            //Natural: MOVE 'Original Check Date: ' TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,55,21 )
                setValueToSubstring(pnd_Ws_Pnd_Pymnt_Check_Dte,pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),76,10);                                         //Natural: MOVE #WS.#PYMNT-CHECK-DTE TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,76,10 )
                //*    VF 8/99
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean() && pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean()))                          //Natural: IF #FCPA803.#CHECK AND #FCPA803.#CHECK-TO-ANNT
                {
                    //*  CHECK DATE IN LEFT COLUMN FOR CHECK
                    pnd_Ws_Pnd_Pymnt_Check_Dte.setValueEdited(pdaFcpa878h.getPnd_Fcpa878h_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                            //Natural: MOVE EDITED #FCPA878H.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #WS.#PYMNT-CHECK-DTE
                    pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2).setValue("02Check Date:");                                                                 //Natural: ASSIGN #HEADER-ARRAY ( 2 ) := '02Check Date:'
                    setValueToSubstring(pnd_Ws_Pnd_Pymnt_Check_Dte,pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),15,14);                                     //Natural: MOVE #WS.#PYMNT-CHECK-DTE TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,15,14 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Annt_Starting_Date.setValueEdited(pdaFcpa878h.getPnd_Fcpa878h_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YYYY"));                      //Natural: MOVE EDITED #FCPA878H.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YYYY ) TO #WS.#ANNT-STARTING-DATE
                    pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2).setValue("02Effective Date:");                                                             //Natural: ASSIGN #HEADER-ARRAY ( 2 ) := '02Effective Date:'
                    setValueToSubstring(pnd_Ws_Pnd_Annt_Starting_Date,pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),19,10);                                  //Natural: MOVE #WS.#ANNT-STARTING-DATE TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,19,10 )
                    pnd_Ws_Pnd_Pymnt_Check_Dte.setValueEdited(pdaFcpa878h.getPnd_Fcpa878h_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                            //Natural: MOVE EDITED #FCPA878H.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #WS.#PYMNT-CHECK-DTE
                    setValueToSubstring("Check Date:",pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),38,13);                                                  //Natural: MOVE 'Check Date:' TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,38,13 )
                    setValueToSubstring(pnd_Ws_Pnd_Pymnt_Check_Dte,pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2),50,10);                                     //Natural: MOVE #WS.#PYMNT-CHECK-DTE TO SUBSTR ( #HEADER-ARRAY ( 2 ) ,50,10 )
                    //*       VF 8/99
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Hdr_Array.getValue(2).setValue(" 2");                                                                                                                  //Natural: ASSIGN #HDR-ARRAY ( 2 ) := ' 2'
        setValueToSubstring("Page:",pnd_Ws_Pnd_Hdr_Array.getValue(2),136,5);                                                                                              //Natural: MOVE 'Page:' TO SUBSTR ( #HDR-ARRAY ( 2 ) ,136,5 )
        getWorkFiles().write(8, false, pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(1));                                                                       //Natural: WRITE WORK FILE 8 #HEADER-ARRAY ( 1 )
        getWorkFiles().write(8, false, pdaFcpa878h.getPnd_Fcpa878h_Pnd_Header_Array().getValue(2));                                                                       //Natural: WRITE WORK FILE 8 #HEADER-ARRAY ( 2 )
        if (condition(pdaFcpa878h.getPnd_Fcpa878h_Pnd_Current_Page().equals(1)))                                                                                          //Natural: IF #FCPA878H.#CURRENT-PAGE = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpa878h.getPnd_Fcpa878h_Pnd_Current_Page().divide(2).add(1));                            //Natural: ASSIGN #J := #FCPA878H.#CURRENT-PAGE / 2 + 1
            pnd_Ws_Pnd_Page_Num.setValue(pnd_Ws_Pnd_J);                                                                                                                   //Natural: ASSIGN #PAGE-NUM := #J
            setValueToSubstring(pnd_Ws_Pnd_Page_Num,pnd_Ws_Pnd_Hdr_Array.getValue(2),141,3);                                                                              //Natural: MOVE #PAGE-NUM TO SUBSTR ( #HDR-ARRAY ( 2 ) ,141,3 )
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Hdr_Array.getValue(2));                                                                                             //Natural: WRITE WORK FILE 8 #HDR-ARRAY ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
