/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:53 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn280
************************************************************
**        * FILE NAME            : Fcpn280.java
**        * CLASS NAME           : Fcpn280
**        * INSTANCE NAME        : Fcpn280
************************************************************
*
* PROGRAMMER    START DTE  END DTE   DESC OF CHANGES
* ------------  ---------  --------  ----------------------------------
* F. CHIN       08/09/93     /  /    CREATED.
*
** 03/18/98 GLORY PHILIP
**        - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
** 9/6/2001 R. CARREON
**        - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
** 3/14/02  R. CARREON
**        - CHANGE NRA CODE TO USE TX-ELCT-TRGGR
**   11/02  R. CARREON  STOW - EGTRRA CHANGES IN PDA
* ---------------------------------------------------------------------
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn280 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa200 pdaFcpa200;
    private PdaTbldcoda pdaTbldcoda;
    private PdaFcppda_M pdaFcppda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Deductions;
    private DbsField pnd_Ws_Deductions_Pnd_Ws_Filler_A;
    private DbsField pnd_Ws_Deductions_Pnd_Ws_Filler_B;
    private DbsField pnd_Ws_Deductions_Pnd_Ws_Filler_C;

    private DbsGroup pnd_Ws_Deductions__R_Field_1;

    private DbsGroup pnd_Ws_Deductions_Pnd_Ws_Ded_Array;
    private DbsField pnd_Ws_Deductions_Pnd_Ws_Left_Side;
    private DbsField pnd_Ws_Deductions_Pnd_Ws_Right_Side;
    private DbsField pnd_Ws_Deductions_Pnd_Ws_Filler;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_Ws_Check_Amt;
    private DbsField pnd_Ws_Hold_Amt;
    private DbsField pnd_Ws_Entry_Code;

    private DbsGroup pnd_Ws_Entry_Code__R_Field_2;
    private DbsField pnd_Ws_Entry_Code_Pnd_Ws_Entry_Code_N;
    private DbsField pnd_Ws_Letter;
    private DbsField pnd_Ws_Table_Of_Entry_Code;
    private DbsField pnd_Ws_Table_Id;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        pdaFcppda_M = new PdaFcppda_M(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa200 = new PdaFcpa200(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Deductions = localVariables.newGroupInRecord("pnd_Ws_Deductions", "#WS-DEDUCTIONS");
        pnd_Ws_Deductions_Pnd_Ws_Filler_A = pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Deductions_Pnd_Ws_Filler_A", "#WS-FILLER-A", FieldType.STRING, 250);
        pnd_Ws_Deductions_Pnd_Ws_Filler_B = pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Deductions_Pnd_Ws_Filler_B", "#WS-FILLER-B", FieldType.STRING, 250);
        pnd_Ws_Deductions_Pnd_Ws_Filler_C = pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Deductions_Pnd_Ws_Filler_C", "#WS-FILLER-C", FieldType.STRING, 215);

        pnd_Ws_Deductions__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Deductions__R_Field_1", "REDEFINE", pnd_Ws_Deductions);

        pnd_Ws_Deductions_Pnd_Ws_Ded_Array = pnd_Ws_Deductions__R_Field_1.newGroupArrayInGroup("pnd_Ws_Deductions_Pnd_Ws_Ded_Array", "#WS-DED-ARRAY", 
            new DbsArrayController(1, 5));
        pnd_Ws_Deductions_Pnd_Ws_Left_Side = pnd_Ws_Deductions_Pnd_Ws_Ded_Array.newFieldInGroup("pnd_Ws_Deductions_Pnd_Ws_Left_Side", "#WS-LEFT-SIDE", 
            FieldType.STRING, 39);
        pnd_Ws_Deductions_Pnd_Ws_Right_Side = pnd_Ws_Deductions_Pnd_Ws_Ded_Array.newFieldInGroup("pnd_Ws_Deductions_Pnd_Ws_Right_Side", "#WS-RIGHT-SIDE", 
            FieldType.STRING, 31);
        pnd_Ws_Deductions_Pnd_Ws_Filler = pnd_Ws_Deductions_Pnd_Ws_Ded_Array.newFieldInGroup("pnd_Ws_Deductions_Pnd_Ws_Filler", "#WS-FILLER", FieldType.STRING, 
            73);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_Ws_Check_Amt = localVariables.newFieldInRecord("pnd_Ws_Check_Amt", "#WS-CHECK-AMT", FieldType.STRING, 25);
        pnd_Ws_Hold_Amt = localVariables.newFieldInRecord("pnd_Ws_Hold_Amt", "#WS-HOLD-AMT", FieldType.STRING, 13);
        pnd_Ws_Entry_Code = localVariables.newFieldInRecord("pnd_Ws_Entry_Code", "#WS-ENTRY-CODE", FieldType.STRING, 3);

        pnd_Ws_Entry_Code__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Entry_Code__R_Field_2", "REDEFINE", pnd_Ws_Entry_Code);
        pnd_Ws_Entry_Code_Pnd_Ws_Entry_Code_N = pnd_Ws_Entry_Code__R_Field_2.newFieldInGroup("pnd_Ws_Entry_Code_Pnd_Ws_Entry_Code_N", "#WS-ENTRY-CODE-N", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Letter = localVariables.newFieldInRecord("pnd_Ws_Letter", "#WS-LETTER", FieldType.STRING, 1);
        pnd_Ws_Table_Of_Entry_Code = localVariables.newFieldArrayInRecord("pnd_Ws_Table_Of_Entry_Code", "#WS-TABLE-OF-ENTRY-CODE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Table_Id = localVariables.newFieldInRecord("pnd_Ws_Table_Id", "#WS-TABLE-ID", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_Rec_1.setInitialValue(" ");
        pnd_Ws_Deductions_Pnd_Ws_Filler_A.setInitialValue(" ");
        pnd_Ws_Deductions_Pnd_Ws_Filler_B.setInitialValue(" ");
        pnd_Ws_Deductions_Pnd_Ws_Filler_C.setInitialValue(" ");
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_Ws_Check_Amt.setInitialValue("15Payment Amount");
        pnd_Ws_Hold_Amt.setInitialValue(" ");
        pnd_Ws_Entry_Code.setInitialValue(" ");
        pnd_Ws_Letter.setInitialValue(" ");
        pnd_Ws_Table_Of_Entry_Code.getValue(1).setInitialValue(0);
        pnd_Ws_Table_Id.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn280() throws Exception
    {
        super("Fcpn280");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(" 7TIAA-CREF reports all taxable payments to the government for", "the year in which they are made.  If you have"));       //Natural: COMPRESS ' 7TIAA-CREF reports all taxable payments to the government for' 'the year in which they are made.  If you have' INTO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(" 7any questions, please call our Telephone Counseling Center", "toll free at 1 800 842-2776."));                          //Natural: COMPRESS ' 7any questions, please call our Telephone Counseling Center' 'toll free at 1 800 842-2776.' INTO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #WS-CNTR-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_I.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I).greater(getZero())))                                                        //Natural: IF INV-ACCT-FDRL-TAX-AMT ( #I ) > 0
            {
                                                                                                                                                                          //Natural: PERFORM FORMAT-FEDERAL-LEGEND
                sub_Format_Federal_Legend();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO #WS-CNTR-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_I.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I).greater(getZero())))                                                       //Natural: IF INV-ACCT-STATE-TAX-AMT ( #I ) > 0
            {
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEGEND
                sub_Format_State_Legend();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO #WS-CNTR-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_I.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I).greater(getZero())))                                                       //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( #I ) > 0
            {
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("L = Local Tax Withheld");                                                                    //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'L = Local Tax Withheld'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30") && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().notEquals("SS")))         //Natural: IF CNTRCT-TYPE-CDE = '30' AND #WS-HEADER-RECORD.CNTRCT-ORGN-CDE NE 'SS'
        {
            pnd_K.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #K
            pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("P = Additional TPA Payment");                                                                    //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'P = Additional TPA Payment'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_L.reset();                                                                                                                                                    //Natural: RESET #L
        FOR04:                                                                                                                                                            //Natural: FOR #I 1 TO #WS-CNTR-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_I.nadd(1))
        {
            FOR05:                                                                                                                                                        //Natural: FOR #J 1 TO #CNTR-DEDUCTIONS ( #I )
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_I))); pnd_J.nadd(1))
            {
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_I,pnd_J).greater(getZero())))                                                      //Natural: IF PYMNT-DED-AMT ( #I,#J ) > 0
                {
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_J).equals(pnd_Ws_Table_Of_Entry_Code.getValue("*"))))                    //Natural: IF PYMNT-DED-CDE ( #I,#J ) = #WS-TABLE-OF-ENTRY-CODE ( * )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_L.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #L
                        pnd_Ws_Table_Of_Entry_Code.getValue(pnd_L).setValue(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_J));                           //Natural: ASSIGN #WS-TABLE-OF-ENTRY-CODE ( #L ) = PYMNT-DED-CDE ( #I,#J )
                        pnd_Ws_Table_Id.setValue(21);                                                                                                                     //Natural: ASSIGN #WS-TABLE-ID = 21
                        pnd_Ws_Entry_Code_Pnd_Ws_Entry_Code_N.setValue(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_J));                                //Natural: ASSIGN #WS-ENTRY-CODE-N = PYMNT-DED-CDE ( #I,#J )
                                                                                                                                                                          //Natural: PERFORM GET-LEGEND
                        sub_Get_Legend();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*        PERFORM TRANSLATE-DEDUCTION
                        //*        COMPRESS PYMNT-DED-CDE (#I,#J) '*=*' TBLDCODA.#TARGET
                        //*        COMPRESS PYMNT-DED-CDE (#I,#J) '*=*' TBLDCODA.#TARGET
                        pnd_Ws_Deductions_Pnd_Ws_Right_Side.getValue(pnd_L).setValue(DbsUtil.compress(pnd_L, "*=*", pdaTbldcoda.getTbldcoda_Pnd_Target()));               //Natural: COMPRESS #L '*=*' TBLDCODA.#TARGET INTO #WS-RIGHT-SIDE ( #L )
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Deductions_Pnd_Ws_Right_Side.getValue(pnd_L),true), new ExamineSearch("*", true), new                    //Natural: EXAMINE FULL #WS-RIGHT-SIDE ( #L ) FOR FULL '*' REPLACE ' '
                            ExamineReplace(" "));
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_K.greater(4)))                                                                                                                                  //Natural: IF #K > 4
        {
            pnd_Ws_Deductions_Pnd_Ws_Ded_Array.getValue("*").reset();                                                                                                     //Natural: RESET #WS-DED-ARRAY ( * )
            pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(1).setValue(DbsUtil.compress("WARNING: ", pnd_K, "LEGEND LINES NEEDED FOR THIS PERSON"));                         //Natural: COMPRESS 'WARNING: ' #K 'LEGEND LINES NEEDED FOR THIS PERSON' INTO #WS-LEFT-SIDE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        FOR06:                                                                                                                                                            //Natural: FOR #K = 1 TO 4
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(4)); pnd_K.nadd(1))
        {
            if (condition(pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).equals(" ") && pnd_Ws_Deductions_Pnd_Ws_Right_Side.getValue(pnd_K).equals(" ")))             //Natural: IF #WS-LEFT-SIDE ( #K ) = ' ' AND #WS-RIGHT-SIDE ( #K ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_K.equals(1)))                                                                                                                               //Natural: IF #K = 1
            {
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue(DbsUtil.compress("*5", pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K)));                  //Natural: COMPRESS '*5' #WS-LEFT-SIDE ( #K ) INTO #WS-LEFT-SIDE ( #K )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K),true), new ExamineSearch("*", true), new ExamineReplace(" "));       //Natural: EXAMINE FULL #WS-LEFT-SIDE ( #K ) FOR FULL '*' REPLACE ' '
                //*    EXAMINE FULL #WS-LEFT-SIDE (#K) FOR FULL '*' REPLACE '0'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue(DbsUtil.compress("+5", pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K)));                  //Natural: COMPRESS '+5' #WS-LEFT-SIDE ( #K ) INTO #WS-LEFT-SIDE ( #K )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K),true), new ExamineSearch("+", true), new ExamineReplace(" "));       //Natural: EXAMINE FULL #WS-LEFT-SIDE ( #K ) FOR FULL '+' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(8, false, pnd_Ws_Deductions_Pnd_Ws_Ded_Array.getValue(pnd_K));                                                                           //Natural: WRITE WORK FILE 8 #WS-DED-ARRAY ( #K )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Hold_Amt.setValueEdited(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Check_Amt(),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-HOLD-AMT
        DbsUtil.examine(new ExamineSource(pnd_Ws_Hold_Amt,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-HOLD-AMT FOR FULL 'X' DELETE
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(pnd_Ws_Check_Amt, pnd_Ws_Hold_Amt));                                                                                       //Natural: COMPRESS #WS-CHECK-AMT #WS-HOLD-AMT INTO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-FEDERAL-LEGEND
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEGEND
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-DEDUCTION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-LEGEND
    }
    private void sub_Format_Federal_Legend() throws Exception                                                                                                             //Natural: FORMAT-FEDERAL-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        //* *NEXT IF STMT HAS BEEN REPLACED BY THE FOLLOWING ONE  G.P 3/18/98
        //*  IF CNTRCT-CRRNCY-CDE = '1'
        //*    IF (ANNT-CTZNSHP-CDE = 01 OR= 11)
        //*        OR
        //*        (ANNT-RSDNCY-CDE = '01' THRU '57')
        //*      ASSIGN #WS-LEFT-SIDE (#K) = 'F = Federal Tax Withheld'
        //*    ELSE
        //*      IF (ANNT-CTZNSHP-CDE = 01 OR= 11)
        //*          OR
        //*          ( ANNT-RSDNCY-CDE = '97' )
        //*        ASSIGN #WS-LEFT-SIDE (#K) = 'F = Taxes Withheld'
        //*      ELSE
        //*       ASSIGN #WS-LEFT-SIDE (#K) = 'N = Non-Resident Alien Tax Withheld'
        //*      END-IF
        //*    END-IF
        //*  END-IF
        //*  NEW IF STMT STARTS HERE G.P  3/17/98
        //*  START NEW CODE       /* ROXAN 3/14/2002
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                    //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
            {
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("N = Non-Resident Alien Tax Withheld");                                                       //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'N = Non-Resident Alien Tax Withheld'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("F = Taxes Withheld");                                                                        //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'F = Taxes Withheld'
                if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57"))) //Natural: IF ANNT-RSDNCY-CDE = '01' THRU '57'
                {
                    pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("F = Federal Tax Withheld");                                                              //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'F = Federal Tax Withheld'
                }                                                                                                                                                         //Natural: END-IF
                //*   END ROXAN 3/14/2002
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF NEW IF STMT  3/18/98
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ANNT-RSDNCY-CDE = '74' THRU '86'
            {
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("C = Canadian Tax Withheld");                                                                 //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'C = Canadian Tax Withheld'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("C = Can. Non-Resident Tax Withheld");                                                        //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'C = Can. Non-Resident Tax Withheld'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_State_Legend() throws Exception                                                                                                               //Natural: FORMAT-STATE-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))                                                                             //Natural: IF ANNT-RSDNCY-CDE = '97'
            {
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("S = Taxes Withheld");                                                                        //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'S = Taxes Withheld'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11))      //Natural: IF ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( ANNT-RSDNCY-CDE = '01' THRU '57' )
                    || (pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))))
                {
                    pnd_Ws_Table_Id.setValue(19);                                                                                                                         //Natural: ASSIGN #WS-TABLE-ID = 19
                    pnd_Ws_Entry_Code.setValue(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde());                                                                     //Natural: ASSIGN #WS-ENTRY-CODE = ANNT-RSDNCY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-LEGEND
                    sub_Get_Legend();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue(DbsUtil.compress("S*=", pdaTbldcoda.getTbldcoda_Pnd_Target(), "Tax Withheld"));           //Natural: COMPRESS 'S*=' TBLDCODA.#TARGET 'Tax Withheld' INTO #WS-LEFT-SIDE ( #K )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K),true), new ExamineSearch("*", true), new ExamineReplace(" "));   //Natural: EXAMINE FULL #WS-LEFT-SIDE ( #K ) FOR FULL '*' REPLACE ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((! ((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11)))  //Natural: IF NOT ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( ANNT-RSDNCY-CDE = '01' THRU '57' ) AND NOT ANNT-RSDNCY-CDE = '97'
                        && ! ((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))) 
                        && ! (pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))))
                    {
                        pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("N = Non-Resident Alien Tax Withheld");                                               //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'N = Non-Resident Alien Tax Withheld'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ( ANNT-RSDNCY-CDE = '74' THRU '86' )
            {
                pnd_Ws_Table_Id.setValue(19);                                                                                                                             //Natural: ASSIGN #WS-TABLE-ID = 19
                pnd_Ws_Entry_Code.setValue(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde());                                                                         //Natural: ASSIGN #WS-ENTRY-CODE = ANNT-RSDNCY-CDE
                                                                                                                                                                          //Natural: PERFORM GET-LEGEND
                sub_Get_Legend();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue(DbsUtil.compress("P*=", pdaTbldcoda.getTbldcoda_Pnd_Target(), "Tax Withheld"));               //Natural: COMPRESS 'P*=' TBLDCODA.#TARGET 'Tax Withheld' INTO #WS-LEFT-SIDE ( #K )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K),true), new ExamineSearch("*", true), new ExamineReplace(" "));       //Natural: EXAMINE FULL #WS-LEFT-SIDE ( #K ) FOR FULL '*' REPLACE ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("96")))                                                                         //Natural: IF ANNT-RSDNCY-CDE = '96'
                {
                    pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("C = Taxes Withheld");                                                                    //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'C = Taxes Withheld'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Deductions_Pnd_Ws_Left_Side.getValue(pnd_K).setValue("C = Can. Non-Resident Tax Withheld");                                                    //Natural: ASSIGN #WS-LEFT-SIDE ( #K ) = 'C = Can. Non-Resident Tax Withheld'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Translate_Deduction() throws Exception                                                                                                               //Natural: TRANSLATE-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_J).equals(1)))                                                                       //Natural: IF PYMNT-DED-CDE ( #I,#J ) = 1
        {
            pnd_Ws_Letter.setValue("H");                                                                                                                                  //Natural: ASSIGN #WS-LETTER = 'H'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_J).equals(2)))                                                                   //Natural: IF PYMNT-DED-CDE ( #I,#J ) = 2
            {
                pnd_Ws_Letter.setValue("L");                                                                                                                              //Natural: ASSIGN #WS-LETTER = 'L'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_J).equals(3)))                                                               //Natural: IF PYMNT-DED-CDE ( #I,#J ) = 3
                {
                    pnd_Ws_Letter.setValue("M");                                                                                                                          //Natural: ASSIGN #WS-LETTER = 'M'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_J).equals(4)))                                                           //Natural: IF PYMNT-DED-CDE ( #I,#J ) = 4
                    {
                        pnd_Ws_Letter.setValue("G");                                                                                                                      //Natural: ASSIGN #WS-LETTER = 'G'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Letter.setValue("?");                                                                                                                      //Natural: ASSIGN #WS-LETTER = '?'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Legend() throws Exception                                                                                                                        //Natural: GET-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                                 //Natural: ASSIGN TBLDCODA.#MODE = 'T'
        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(pnd_Ws_Table_Id);                                                                                                 //Natural: ASSIGN TBLDCODA.#TABLE-ID = #WS-TABLE-ID
        pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pnd_Ws_Entry_Code);                                                                                                   //Natural: ASSIGN TBLDCODA.#CODE = #WS-ENTRY-CODE
        DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pdaFcppda_M.getMsg_Info_Sub());                                              //Natural: CALLNAT 'TBLDCOD' TBLDCODA MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
    }

    //
}
