/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:29 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn800a
************************************************************
**        * FILE NAME            : Fcpn800a.java
**        * CLASS NAME           : Fcpn800a
**        * INSTANCE NAME        : Fcpn800a
************************************************************
************************************************************************
* PROGRAM   : FCPN800A
* SYSTEM    : CPS
* TITLE     : PRINT OF CONTROL RECORDS
* GENERATED :
* FUNCTION  : CONTROL PROCESSING FOR :
*           : 1 - IAA / CPS INTERFACE ( IF CALLED BY FCPP800)
*           :
*           : 11/25/96    RITA SALGADO
*           : - PRODUCE A VARIANCE REPORT WITH A MESSAGE EVEN IF
*           :   IAA & CPS CONTROLS BALANCE
*           :
*           : 03/15/2001  LEON GURTOVNIK
*           : - CNANGE REPORT (01) TO SPLIT INTERNAL ROLLOVERS
*           :   OCCURANCES 4 AND 13  INTO
*           : TPA  ROLLOVER          '
*           : P&I  ROLLOVER          '
*           : IPRO ROLLOVER          '
*           : INT. ROLLOVER OTHER    '
*           : AND CREATE 'Total Internal Rollover '
*           : CORRECT PROGRAM NAME FROM FCPP800A TO FCPN800A
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn800a extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa800a pdaFcpa800a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters

    private DbsGroup pnd_Leon_Calc_Report;
    private DbsField pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Net_Amt_Leon;
    private DbsField pnd_Header_Lit;
    private DbsField pnd_Total_Lit;
    private DbsField pnd_Tot_Ind;
    private DbsField pnd_Index;
    private DbsField pnd_Beg_Index;
    private DbsField pnd_End_Index;
    private DbsField pnd_Tot_Rec_Count;
    private DbsField pnd_Tot_Cntrct_Amt;
    private DbsField pnd_Tot_Dvdnd_Amt;
    private DbsField pnd_Tot_Cref_Amt;
    private DbsField pnd_Tot_Gross_Amt;
    private DbsField pnd_Tot_Ded_Amt;

    private DbsGroup pnd_Lit_Occurs_Leon;
    private DbsField pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon;
    private DbsField pnd_Index_Leon;
    private DbsField pnd_Lll;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa800a = new PdaFcpa800a(parameters);

        pnd_Leon_Calc_Report = parameters.newGroupArrayInRecord("pnd_Leon_Calc_Report", "#LEON-CALC-REPORT", new DbsArrayController(1, 4));
        pnd_Leon_Calc_Report.setParameterOption(ParameterOption.ByReference);
        pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon", "#REC-COUNT-LEON", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon", "#CNTRCT-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon", "#DVDND-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon", "#CREF-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon", "#GROSS-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon", "#DED-AMT-LEON", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon", "#TAX-AMT-LEON", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Net_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Net_Amt_Leon", "#NET-AMT-LEON", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 2));
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Header_Lit = localVariables.newFieldInRecord("pnd_Header_Lit", "#HEADER-LIT", FieldType.STRING, 20);
        pnd_Total_Lit = localVariables.newFieldInRecord("pnd_Total_Lit", "#TOTAL-LIT", FieldType.STRING, 25);
        pnd_Tot_Ind = localVariables.newFieldInRecord("pnd_Tot_Ind", "#TOT-IND", FieldType.PACKED_DECIMAL, 2);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Beg_Index = localVariables.newFieldInRecord("pnd_Beg_Index", "#BEG-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_End_Index = localVariables.newFieldInRecord("pnd_End_Index", "#END-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Tot_Rec_Count = localVariables.newFieldInRecord("pnd_Tot_Rec_Count", "#TOT-REC-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Tot_Cntrct_Amt = localVariables.newFieldInRecord("pnd_Tot_Cntrct_Amt", "#TOT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Tot_Dvdnd_Amt", "#TOT-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Cref_Amt = localVariables.newFieldInRecord("pnd_Tot_Cref_Amt", "#TOT-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Gross_Amt = localVariables.newFieldInRecord("pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Ded_Amt = localVariables.newFieldInRecord("pnd_Tot_Ded_Amt", "#TOT-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Lit_Occurs_Leon = localVariables.newGroupArrayInRecord("pnd_Lit_Occurs_Leon", "#LIT-OCCURS-LEON", new DbsArrayController(1, 4));
        pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon = pnd_Lit_Occurs_Leon.newFieldInGroup("pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon", "#TOTAL-LIT-LEON", FieldType.STRING, 
            28);
        pnd_Index_Leon = localVariables.newFieldInRecord("pnd_Index_Leon", "#INDEX-LEON", FieldType.PACKED_DECIMAL, 2);
        pnd_Lll = localVariables.newFieldInRecord("pnd_Lll", "#LLL", FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon.getValue(1).setInitialValue("                         ");
        pnd_Index_Leon.setInitialValue(1);
        pnd_Lll.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn800a() throws Exception
    {
        super("Fcpn800a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                 /* BEG LEON 03/15/01                                                                                          //Natural: FORMAT ( 01 ) PS = 58 LS = 132 ZP = OFF
        //* *ON ERROR
        //* *  WRITE
        //* *  // '**************************************************************'
        //* *  /  '**************************************************************'
        //* *  /  '***'*PROGRAM '  ERROR:' *ERROR-NR 'LINE:' *ERROR-LINE
        //* *  /  '**************************************************************'
        //* *  /  '**************************************************************'
        //* *  TERMINATE 0099
        //* *END-ERROR
        //*                                                 /* END LEON 03/15/01
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE *PROGRAM 52T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 58T 'ANNUITY PAYMENTS' 124T *TIMX ( EM = HH:II' 'AP ) / #HEADER-LIT / #CPS-TOTALS.#PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        //*   ACCUMULATE GRAND TOTALS
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(2,19)),  //Natural: ASSIGN #REC-COUNT ( 2,19 ) := #REC-COUNT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(2,19)),  //Natural: ASSIGN #PAY-COUNT ( 2,19 ) := #PAY-COUNT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(2,19)),  //Natural: ASSIGN #CNTRCT-AMT ( 2,19 ) := #CNTRCT-AMT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,19)),  //Natural: ASSIGN #DVDND-AMT ( 2,19 ) := #DVDND-AMT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(2,19)),    //Natural: ASSIGN #CREF-AMT ( 2,19 ) := #CREF-AMT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(2,19)),  //Natural: ASSIGN #GROSS-AMT ( 2,19 ) := #GROSS-AMT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(2,19)),      //Natural: ASSIGN #DED-AMT ( 2,19 ) := #DED-AMT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,19)),      //Natural: ASSIGN #TAX-AMT ( 2,19 ) := #TAX-AMT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,1,":",9).add(getZero()));
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(2,19).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(2,19)),      //Natural: ASSIGN #NET-AMT ( 2,19 ) := #NET-AMT ( 2,1:9 ) + 0
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(2,1,":",9).add(getZero()));
        //*                                                 /* BEG LEON 03/15/01
        pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon.getValue(1).setValue("     TPA  Rollover          ");                                                                      //Natural: MOVE '     TPA  Rollover          ' TO #TOTAL-LIT-LEON ( 1 )
        pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon.getValue(2).setValue("     P&I  Rollover          ");                                                                      //Natural: MOVE '     P&I  Rollover          ' TO #TOTAL-LIT-LEON ( 2 )
        pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon.getValue(3).setValue("     IPRO Rollover          ");                                                                      //Natural: MOVE '     IPRO Rollover          ' TO #TOTAL-LIT-LEON ( 3 )
        pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon.getValue(4).setValue("     INT. Rollover Other    ");                                                                      //Natural: MOVE '     INT. Rollover Other    ' TO #TOTAL-LIT-LEON ( 4 )
        FOR01:                                                                                                                                                            //Natural: FOR #TOT-IND 1 3
        for (pnd_Tot_Ind.setValue(1); condition(pnd_Tot_Ind.lessOrEqual(3)); pnd_Tot_Ind.nadd(1))
        {
            pnd_Header_Lit.setValue(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Title_Lit().getValue(pnd_Tot_Ind));                                                                 //Natural: MOVE #TITLE-LIT ( #TOT-IND ) TO #HEADER-LIT
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Tot_Ind.equals(3) && ! (pdaFcpa800a.getPnd_Cps_Totals_Pnd_Diff_In_Totals().getBoolean())))                                                  //Natural: IF #TOT-IND = 3 AND NOT #CPS-TOTALS.#DIFF-IN-TOTALS
            {
                getReports().write(1, NEWLINE,NEWLINE,"***************************************************************",NEWLINE,"***************************************************************", //Natural: WRITE ( 01 ) // '***************************************************************' / '***************************************************************' / '***                                                         ***' / '***                                                         ***' / '***  CPS control totals balance to IAA control totals       ***' / '***                                                         ***' / '***                                                         ***' / '***************************************************************' / '***************************************************************'
                    NEWLINE,"***                                                         ***",NEWLINE,"***                                                         ***",
                    NEWLINE,"***  CPS control totals balance to IAA control totals       ***",NEWLINE,"***                                                         ***",
                    NEWLINE,"***                                                         ***",NEWLINE,"***************************************************************",
                    NEWLINE,"***************************************************************");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-LINES
            sub_Write_Lines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Total_Lit.setValue("Total Pends           ");                                                                                                             //Natural: MOVE 'Total Pends           ' TO #TOTAL-LIT
            pnd_Beg_Index.setValue(14);                                                                                                                                   //Natural: MOVE 14 TO #BEG-INDEX
            pnd_End_Index.setValue(18);                                                                                                                                   //Natural: MOVE 18 TO #END-INDEX
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
            sub_Print_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-LINES
        //* **--------------------
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
        //* ***********************************************************************
    }
    private void sub_Write_Lines() throws Exception                                                                                                                       //Natural: WRITE-LINES
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #INDEX 1 19
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(19)); pnd_Index.nadd(1))
        {
            if (condition(pnd_Tot_Ind.equals(1)))                                                                                                                         //Natural: IF #TOT-IND = 1
            {
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#REC-COUNT ( 3, #INDEX ) = #CPS-TOTALS.#REC-COUNT ( 2, #INDEX ) - #CPS-TOTALS.#REC-COUNT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(1,
                    pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#PAY-COUNT ( 3, #INDEX ) = #CPS-TOTALS.#PAY-COUNT ( 2, #INDEX ) - #CPS-TOTALS.#PAY-COUNT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(1,
                    pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#CNTRCT-AMT ( 3, #INDEX ) = #CPS-TOTALS.#CNTRCT-AMT ( 2, #INDEX ) - #CPS-TOTALS.#CNTRCT-AMT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(1,
                    pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#DVDND-AMT ( 3, #INDEX ) = #CPS-TOTALS.#DVDND-AMT ( 2, #INDEX ) - #CPS-TOTALS.#DVDND-AMT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(1,
                    pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#CREF-AMT ( 3, #INDEX ) = #CPS-TOTALS.#CREF-AMT ( 2, #INDEX ) - #CPS-TOTALS.#CREF-AMT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(1,
                    pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#GROSS-AMT ( 3, #INDEX ) = #CPS-TOTALS.#GROSS-AMT ( 2, #INDEX ) - #CPS-TOTALS.#GROSS-AMT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(1,
                    pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#DED-AMT ( 3, #INDEX ) = #CPS-TOTALS.#DED-AMT ( 2, #INDEX ) - #CPS-TOTALS.#DED-AMT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(1,pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#TAX-AMT ( 3, #INDEX ) = #CPS-TOTALS.#TAX-AMT ( 2, #INDEX ) - #CPS-TOTALS.#TAX-AMT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(1,pnd_Index)));
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(3,pnd_Index).compute(new ComputeParameters(false, pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(3,pnd_Index)),  //Natural: COMPUTE #CPS-TOTALS.#NET-AMT ( 3, #INDEX ) = #CPS-TOTALS.#NET-AMT ( 2, #INDEX ) - #CPS-TOTALS.#NET-AMT ( 1, #INDEX )
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(2,pnd_Index).subtract(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(1,pnd_Index)));
                if (condition(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(3,1,":",18).notEquals(getZero()) || pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(3,1,":",18).notEquals(getZero())  //Natural: IF #CPS-TOTALS.#REC-COUNT ( 3, 1:18 ) NE 0 OR #CPS-TOTALS.#CNTRCT-AMT ( 3, 1:18 ) NE 0 OR #CPS-TOTALS.#DVDND-AMT ( 3, 1:18 ) NE 0 OR #CPS-TOTALS.#CREF-AMT ( 3, 1:18 ) NE 0 OR #CPS-TOTALS.#GROSS-AMT ( 3, 1:18 ) NE 0 OR #CPS-TOTALS.#DED-AMT ( 3, 1:18 ) NE 0
                    || pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(3,1,":",18).notEquals(getZero()) || pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(3,1,":",18).notEquals(getZero()) 
                    || pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(3,1,":",18).notEquals(getZero()) || pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(3,
                    1,":",18).notEquals(getZero())))
                {
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Diff_In_Totals().setValue(true);                                                                                    //Natural: MOVE TRUE TO #CPS-TOTALS.#DIFF-IN-TOTALS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Diff_In_Totals().setValue(false);                                                                                   //Natural: MOVE FALSE TO #CPS-TOTALS.#DIFF-IN-TOTALS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet186 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #INDEX;//Natural: VALUE 1
            if (condition((pnd_Index.equals(1))))
            {
                decideConditionsMet186++;
                getReports().write(1, "TOTAL FILE COUNT:",NEWLINE,"================ ");                                                                                   //Natural: WRITE ( 01 ) 'TOTAL FILE COUNT:' / '================ '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Lit.setValue("  Checks              ");                                                                                                         //Natural: MOVE '  Checks              ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Index.equals(2))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  EFTs                ");                                                                                                         //Natural: MOVE '  EFTs                ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Index.equals(3))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Global Pay          ");                                                                                                         //Natural: MOVE '  Global Pay          ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Index.equals(4))))
            {
                decideConditionsMet186++;
                //* ***  MOVE '  INTERNAL ROLLOVER   ' TO #TOTAL-LIT     /* ORIG
                //*  LEON 03/15/01
                pnd_Lll.setValue(1);                                                                                                                                      //Natural: MOVE 1 TO #LLL
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Index.equals(5))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("Total Pay             ");                                                                                                         //Natural: MOVE 'Total Pay             ' TO #TOTAL-LIT
                pnd_Beg_Index.setValue(1);                                                                                                                                //Natural: MOVE 1 TO #BEG-INDEX
                pnd_End_Index.setValue(4);                                                                                                                                //Natural: MOVE 4 TO #END-INDEX
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                sub_Print_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Lit.setValue("  Blocked Accounts    ");                                                                                                         //Natural: MOVE '  Blocked Accounts    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Index.equals(6))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Unclaimed Sums      ");                                                                                                         //Natural: MOVE '  Unclaimed Sums      ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_Index.equals(7))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Missing Name/Address");                                                                                                         //Natural: MOVE '  Missing Name/Address' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_Index.equals(8))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Invalid Combines    ");                                                                                                         //Natural: MOVE '  Invalid Combines    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_Index.equals(9))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Miscellaneous       ");                                                                                                         //Natural: MOVE '  Miscellaneous       ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_Index.equals(10))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("Total Pends           ");                                                                                                         //Natural: MOVE 'Total Pends           ' TO #TOTAL-LIT
                pnd_Beg_Index.setValue(5);                                                                                                                                //Natural: MOVE 5 TO #BEG-INDEX
                pnd_End_Index.setValue(9);                                                                                                                                //Natural: MOVE 9 TO #END-INDEX
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                sub_Print_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *    WRITE (01) /// 'PAYMENTS DUE:'                 /* ORIG
                //*  LEON 03/15/01
                getReports().write(1, NEWLINE,NEWLINE,"PAYMENTS DUE:",NEWLINE,"============:");                                                                           //Natural: WRITE ( 01 ) // 'PAYMENTS DUE:' / '============:'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Lit.setValue("  Checks              ");                                                                                                         //Natural: MOVE '  Checks              ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Index.equals(11))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  EFTs                ");                                                                                                         //Natural: MOVE '  EFTs                ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Index.equals(12))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Global Pay          ");                                                                                                         //Natural: MOVE '  Global Pay          ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_Index.equals(13))))
            {
                decideConditionsMet186++;
                //* *    MOVE '  INTERNAL ROLLOVER   ' TO #TOTAL-LIT     /*  ORIG
                //*  LEON 03/15/01
                pnd_Lll.setValue(2);                                                                                                                                      //Natural: MOVE 2 TO #LLL
            }                                                                                                                                                             //Natural: VALUE 14
            else if (condition((pnd_Index.equals(14))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("Total Pay             ");                                                                                                         //Natural: MOVE 'Total Pay             ' TO #TOTAL-LIT
                pnd_Beg_Index.setValue(10);                                                                                                                               //Natural: MOVE 10 TO #BEG-INDEX
                pnd_End_Index.setValue(13);                                                                                                                               //Natural: MOVE 13 TO #END-INDEX
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                sub_Print_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, NEWLINE,NEWLINE,"LEDGERIZED PENDS:");                                                                                               //Natural: WRITE ( 01 ) // 'LEDGERIZED PENDS:'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Lit.setValue("  Blocked Accounts    ");                                                                                                         //Natural: MOVE '  Blocked Accounts    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 15
            else if (condition((pnd_Index.equals(15))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Unclaimed Sums      ");                                                                                                         //Natural: MOVE '  Unclaimed Sums      ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 16
            else if (condition((pnd_Index.equals(16))))
            {
                decideConditionsMet186++;
                getReports().write(1, NEWLINE,NEWLINE,"NON-LEDGERIZED PENDS:");                                                                                           //Natural: WRITE ( 01 ) // 'NON-LEDGERIZED PENDS:'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Lit.setValue("  Missing Name/Address");                                                                                                         //Natural: MOVE '  Missing Name/Address' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 17
            else if (condition((pnd_Index.equals(17))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Invalid Combines    ");                                                                                                         //Natural: MOVE '  Invalid Combines    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 18
            else if (condition((pnd_Index.equals(18))))
            {
                decideConditionsMet186++;
                pnd_Total_Lit.setValue("  Miscellaneous       ");                                                                                                         //Natural: MOVE '  Miscellaneous       ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: ANY VALUE
            if (condition(decideConditionsMet186 > 0))
            {
                //*  LEON 03/15/01
                //*  LEON 03/15/01
                if (condition(pnd_Index.notEquals(4) && pnd_Index.notEquals(13)))                                                                                         //Natural: IF #INDEX NE 4 AND #INDEX NE 13
                {
                    getReports().display(1, " ",                                                                                                                          //Natural: DISPLAY ( 01 ) ' ' #TOTAL-LIT 'Record/Count' #CPS-TOTALS.#REC-COUNT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9- ) 'Contract/Amount' #CPS-TOTALS.#CNTRCT-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 'Dividend/Amount' #CPS-TOTALS.#DVDND-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 'REA-CREF/Amount' #CPS-TOTALS.#CREF-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 'Gross/Amount' #CPS-TOTALS.#GROSS-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 'Deduction/Amount' #CPS-TOTALS.#DED-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- )
                    		pnd_Total_Lit,"Record/Count",
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),"Contract/Amount",
                        
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"Dividend/Amount",
                        
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"REA-CREF/Amount",
                        
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"Gross/Amount",
                        
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"Deduction/Amount",
                        
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
                    getReports().setDisplayColumns(1, " ",
                    		pnd_Total_Lit,"Record/Count",
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count(), new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),"Contract/Amount",
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"Dividend/Amount",
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"REA-CREF/Amount",
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"Gross/Amount",
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"Deduction/Amount",
                    		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  LEON 03/15/01
                }                                                                                                                                                         //Natural: END-IF
                //* **--------------------                            /* BEG LEON 03/15/01
                if (condition(pnd_Index.equals(4) || pnd_Index.equals(13)))                                                                                               //Natural: IF #INDEX = 4 OR = 13
                {
                    //*  ROXAN 05/07/01
                    if (condition(pnd_Tot_Ind.equals(2)))                                                                                                                 //Natural: IF #TOT-IND = 2
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #INDEX-LEON 1 4
                        for (pnd_Index_Leon.setValue(1); condition(pnd_Index_Leon.lessOrEqual(4)); pnd_Index_Leon.nadd(1))
                        {
                            getReports().write(1, pnd_Lit_Occurs_Leon_Pnd_Total_Lit_Leon.getValue(pnd_Index_Leon),pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon.getValue(pnd_Index_Leon,pnd_Lll),  //Natural: WRITE ( 01 ) #TOTAL-LIT-LEON ( #INDEX-LEON ) #REC-COUNT-LEON ( #INDEX-LEON,#LLL ) ( EM = ZZZ,ZZZ,ZZ9- ) #CNTRCT-AMT-LEON ( #INDEX-LEON,#LLL ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #DVDND-AMT-LEON ( #INDEX-LEON,#LLL ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #CREF-AMT-LEON ( #INDEX-LEON,#LLL ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #GROSS-AMT-LEON ( #INDEX-LEON,#LLL ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #DED-AMT-LEON ( #INDEX-LEON,#LLL ) ( EM = ZZZ,ZZZ,ZZ9.99- )
                                new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon.getValue(pnd_Index_Leon,pnd_Lll), new ReportEditMask 
                                ("ZZZ,ZZZ,ZZ9.99-"),pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon.getValue(pnd_Index_Leon,pnd_Lll), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon.getValue(pnd_Index_Leon,pnd_Lll), 
                                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon.getValue(pnd_Index_Leon,pnd_Lll), new ReportEditMask 
                                ("ZZZ,ZZZ,ZZ9.99-"),pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon.getValue(pnd_Index_Leon,pnd_Lll), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Lll.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #LLL
                    getReports().write(1, "  Total Internal Rollover",pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(pnd_Tot_Ind,pnd_Index), new                  //Natural: WRITE ( 01 ) '  Total Internal Rollover' #CPS-TOTALS.#REC-COUNT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9- ) #CPS-TOTALS.#CNTRCT-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #CPS-TOTALS.#DVDND-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #CPS-TOTALS.#CREF-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #CPS-TOTALS.#GROSS-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) #CPS-TOTALS.#DED-AMT ( #TOT-IND, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- )
                        ReportEditMask ("ZZZ,ZZZ,ZZ9-"),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask 
                        ("ZZZ,ZZZ,ZZ9.99-"),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(pnd_Tot_Ind,pnd_Index), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask 
                        ("ZZZ,ZZZ,ZZ9.99-"),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(pnd_Tot_Ind,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-LINES
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tot_Rec_Count.compute(new ComputeParameters(false, pnd_Tot_Rec_Count), pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(pnd_Tot_Ind,                    //Natural: ASSIGN #TOT-REC-COUNT := #CPS-TOTALS.#REC-COUNT ( #TOT-IND, #BEG-INDEX:#END-INDEX ) + 0
            pnd_Beg_Index,":",pnd_End_Index).add(getZero()));
        pnd_Tot_Cntrct_Amt.compute(new ComputeParameters(false, pnd_Tot_Cntrct_Amt), pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(pnd_Tot_Ind,                 //Natural: ASSIGN #TOT-CNTRCT-AMT := #CPS-TOTALS.#CNTRCT-AMT ( #TOT-IND, #BEG-INDEX:#END-INDEX ) + 0
            pnd_Beg_Index,":",pnd_End_Index).add(getZero()));
        pnd_Tot_Dvdnd_Amt.compute(new ComputeParameters(false, pnd_Tot_Dvdnd_Amt), pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(pnd_Tot_Ind,                    //Natural: ASSIGN #TOT-DVDND-AMT := #CPS-TOTALS.#DVDND-AMT ( #TOT-IND, #BEG-INDEX:#END-INDEX ) + 0
            pnd_Beg_Index,":",pnd_End_Index).add(getZero()));
        pnd_Tot_Cref_Amt.compute(new ComputeParameters(false, pnd_Tot_Cref_Amt), pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(pnd_Tot_Ind,pnd_Beg_Index,         //Natural: ASSIGN #TOT-CREF-AMT := #CPS-TOTALS.#CREF-AMT ( #TOT-IND, #BEG-INDEX:#END-INDEX ) + 0
            ":",pnd_End_Index).add(getZero()));
        pnd_Tot_Gross_Amt.compute(new ComputeParameters(false, pnd_Tot_Gross_Amt), pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(pnd_Tot_Ind,                    //Natural: ASSIGN #TOT-GROSS-AMT := #CPS-TOTALS.#GROSS-AMT ( #TOT-IND, #BEG-INDEX:#END-INDEX ) + 0
            pnd_Beg_Index,":",pnd_End_Index).add(getZero()));
        pnd_Tot_Ded_Amt.compute(new ComputeParameters(false, pnd_Tot_Ded_Amt), pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(pnd_Tot_Ind,pnd_Beg_Index,            //Natural: ASSIGN #TOT-DED-AMT := #CPS-TOTALS.#DED-AMT ( #TOT-IND, #BEG-INDEX:#END-INDEX ) + 0
            ":",pnd_End_Index).add(getZero()));
        getReports().write(1, NEWLINE,pnd_Total_Lit,new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(1,1)),pnd_Tot_Rec_Count,                   //Natural: WRITE ( 01 ) / #TOTAL-LIT T*#CPS-TOTALS.#REC-COUNT ( 1,1 ) #TOT-REC-COUNT ( EM = ZZZ,ZZZ,ZZ9- ) T*#CPS-TOTALS.#CNTRCT-AMT ( 1,1 ) #TOT-CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) T*#CPS-TOTALS.#DVDND-AMT ( 1,1 ) #TOT-DVDND-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) T*#CPS-TOTALS.#CREF-AMT ( 1,1 ) #TOT-CREF-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) T*#CPS-TOTALS.#GROSS-AMT ( 1,1 ) #TOT-GROSS-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) T*#CPS-TOTALS.#DED-AMT ( 1,1 ) #TOT-DED-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) /
            new ReportEditMask ("ZZZ,ZZZ,ZZ9-"),new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(1,1)),pnd_Tot_Cntrct_Amt, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(1,1)),pnd_Tot_Dvdnd_Amt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(1,1)),pnd_Tot_Cref_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
            ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(1,1)),pnd_Tot_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(1,1)),pnd_Tot_Ded_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE);
        if (Global.isEscape()) return;
        //*  PRINT-TOTALS
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132 ZP=OFF");

        getReports().write(1, ReportOption.TITLE,Global.getPROGRAM(),new TabSetting(52),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(58),"ANNUITY PAYMENTS",new TabSetting(124),Global.getTIMX(), 
            new ReportEditMask ("HH:II' 'AP"),NEWLINE,pnd_Header_Lit,NEWLINE,pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pymnt_Check_Dte(), new ReportEditMask ("LLLLLLLLL', 'YYYY"),
            NEWLINE,NEWLINE);
    }
}
