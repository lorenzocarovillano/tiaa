/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:33 AM
**        * FROM NATURAL SUBPROGRAM : Fcpns46b
************************************************************
**        * FILE NAME            : Fcpns46b.java
**        * CLASS NAME           : Fcpns46b
**        * INSTANCE NAME        : Fcpns46b
************************************************************
************************************************************************
* PROGRAM  : FCPNS46B
*
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* FUNCTION : THIS PROGRAM WILL CONVERT THE DATE TYPE FORMAT FIELDS
*            IN S462 LAYOUT INTO NUMERIC OF 8 WHICH CAN BE
*            UNDERSTOOD BY COBOL PROGRAM
* DATE     : 01/15/2020
* CHANGES  : CTS CPS SUNSET (CHG694525)
**********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpns46b extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa800 pdaFcpa800;
    private LdaFcpls462 ldaFcpls462;
    private PdaAddrpart pdaAddrpart;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Gtn_W9_Ben_Ind;
    private DbsField pnd_Gtn_Fed_Tax_Type;
    private DbsField pnd_Gtn_Fed_Tax_Amt;
    private DbsField pnd_Gtn_Fed_Canadian_Amt;
    private DbsField pnd_Gtn_Elec_Cde;
    private DbsField pnd_Gtn_Cntrct_Money_Source;
    private DbsField pnd_Gtn_Hardship_Cde;
    private DbsField pnd_Dist;
    private DbsField pnd_Pymnt_Deceased_Nme;
    private DbsField s462_Ret_Code;
    private DbsField s462_Ret_Msg;
    private DbsField pnd_Write_Rec_Count;
    private int tiacpsdcReturnCode;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpls462 = new LdaFcpls462();
        registerRecord(ldaFcpls462);
        localVariables = new DbsRecord();
        pdaAddrpart = new PdaAddrpart(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(parameters);
        pnd_Gtn_W9_Ben_Ind = parameters.newFieldInRecord("pnd_Gtn_W9_Ben_Ind", "#GTN-W9-BEN-IND", FieldType.STRING, 1);
        pnd_Gtn_W9_Ben_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Gtn_Fed_Tax_Type = parameters.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Type", "#GTN-FED-TAX-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Tax_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Gtn_Fed_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Amt", "#GTN-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Tax_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Gtn_Fed_Canadian_Amt = parameters.newFieldArrayInRecord("pnd_Gtn_Fed_Canadian_Amt", "#GTN-FED-CANADIAN-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 2));
        pnd_Gtn_Fed_Canadian_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Gtn_Elec_Cde = parameters.newFieldInRecord("pnd_Gtn_Elec_Cde", "#GTN-ELEC-CDE", FieldType.NUMERIC, 3);
        pnd_Gtn_Elec_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Gtn_Cntrct_Money_Source = parameters.newFieldInRecord("pnd_Gtn_Cntrct_Money_Source", "#GTN-CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        pnd_Gtn_Cntrct_Money_Source.setParameterOption(ParameterOption.ByReference);
        pnd_Gtn_Hardship_Cde = parameters.newFieldInRecord("pnd_Gtn_Hardship_Cde", "#GTN-HARDSHIP-CDE", FieldType.STRING, 1);
        pnd_Gtn_Hardship_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Dist = parameters.newFieldInRecord("pnd_Dist", "#DIST", FieldType.STRING, 2);
        pnd_Dist.setParameterOption(ParameterOption.ByReference);
        pnd_Pymnt_Deceased_Nme = parameters.newFieldInRecord("pnd_Pymnt_Deceased_Nme", "#PYMNT-DECEASED-NME", FieldType.STRING, 38);
        pnd_Pymnt_Deceased_Nme.setParameterOption(ParameterOption.ByReference);
        s462_Ret_Code = parameters.newFieldInRecord("s462_Ret_Code", "S462-RET-CODE", FieldType.STRING, 4);
        s462_Ret_Code.setParameterOption(ParameterOption.ByReference);
        s462_Ret_Msg = parameters.newFieldInRecord("s462_Ret_Msg", "S462-RET-MSG", FieldType.STRING, 60);
        s462_Ret_Msg.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Write_Rec_Count = localVariables.newFieldInRecord("pnd_Write_Rec_Count", "#WRITE-REC-COUNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpls462.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Write_Rec_Count.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpns46b() throws Exception
    {
        super("Fcpns46b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPNS46B", onError);
        //*                                                                                                                                                               //Natural: ON ERROR
        ldaFcpls462.getWf_Pymnt_Addr_Grp1().getValue("*").reset();                                                                                                        //Natural: RESET WF-PYMNT-ADDR-GRP1 ( * )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Wf_Pymnt_Addr_Rec1().setValuesByName(pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                      //Natural: MOVE BY NAME WF-PYMNT-ADDR-REC TO WF-PYMNT-ADDR-REC1
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Dob().notEquals(getZero())))                                                                                  //Natural: IF PYMNT-DOB NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Dob1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Dob(),new ReportEditMask("YYYYMMDD"));                    //Natural: MOVE EDITED PYMNT-DOB ( EM = YYYYMMDD ) TO PYMNT-DOB1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().notEquals(getZero())))                                                                            //Natural: IF PYMNT-CHECK-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Check_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));        //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO PYMNT-CHECK-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte().notEquals(getZero())))                                                                            //Natural: IF PYMNT-CYCLE-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Cycle_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte(),new ReportEditMask("MMDDYYYY"));        //Natural: MOVE EDITED PYMNT-CYCLE-DTE ( EM = MMDDYYYY ) TO PYMNT-CYCLE-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Eft_Dte().notEquals(getZero())))                                                                              //Natural: IF PYMNT-EFT-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Eft_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Eft_Dte(),new ReportEditMask("MMDDYYYY"));            //Natural: MOVE EDITED PYMNT-EFT-DTE ( EM = MMDDYYYY ) TO PYMNT-EFT-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte().notEquals(getZero())))                                                                         //Natural: IF PYMNT-SETTLMNT-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Settlmnt_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));  //Natural: MOVE EDITED PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO PYMNT-SETTLMNT-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte().notEquals(getZero())))                                                                            //Natural: IF PYMNT-ACCTG-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Acctg_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte(),new ReportEditMask("MMDDYYYY"));        //Natural: MOVE EDITED PYMNT-ACCTG-DTE ( EM = MMDDYYYY ) TO PYMNT-ACCTG-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte().notEquals(getZero())))                                                                          //Natural: IF PYMNT-INTRFCE-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Intrfce_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte(),new ReportEditMask("MMDDYYYY"));    //Natural: MOVE EDITED PYMNT-INTRFCE-DTE ( EM = MMDDYYYY ) TO PYMNT-INTRFCE-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ia_Issue_Dte().notEquals(getZero())))                                                                         //Natural: IF PYMNT-IA-ISSUE-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Ia_Issue_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ia_Issue_Dte(),new ReportEditMask("MMDDYYYY"));  //Natural: MOVE EDITED PYMNT-IA-ISSUE-DTE ( EM = MMDDYYYY ) TO PYMNT-IA-ISSUE-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte().notEquals(getZero())))                                                                          //Natural: IF PYMNT-SUSPEND-DTE NE 0
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Suspend_Dte1().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte(),new ReportEditMask("MMDDYYYY"));    //Natural: MOVE EDITED PYMNT-SUSPEND-DTE ( EM = MMDDYYYY ) TO PYMNT-SUSPEND-DTE1
        }                                                                                                                                                                 //Natural: END-IF
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_1().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line1_Txt().getValue(1));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE1-TXT ( 1 ) TO ADDR-LNE-1
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_2().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line2_Txt().getValue(1));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE2-TXT ( 1 ) TO ADDR-LNE-2
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_3().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line3_Txt().getValue(1));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE3-TXT ( 1 ) TO ADDR-LNE-3
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_4().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line4_Txt().getValue(1));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE4-TXT ( 1 ) TO ADDR-LNE-4
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_5().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line5_Txt().getValue(1));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE5-TXT ( 1 ) TO ADDR-LNE-5
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_6().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line6_Txt().getValue(1));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE6-TXT ( 1 ) TO ADDR-LNE-6
        pdaAddrpart.getPnd_Twrapart_Addr_Postal_Data().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Postl_Data().getValue(1));                                        //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-POSTL-DATA ( 1 ) TO ADDR-POSTAL-DATA
        if (condition(pdaAddrpart.getPnd_Twrapart_Addr_Lne_6().getSubstring(1,2).equals("//")))                                                                           //Natural: IF SUBSTR ( ADDR-LNE-6,1,2 ) = '//'
        {
            pdaAddrpart.getPnd_Twrapart_Addr_Lne_6().reset();                                                                                                             //Natural: RESET ADDR-LNE-6
        }                                                                                                                                                                 //Natural: END-IF
        //*  TO POPULATE STATE CODE FOR FIRST ADDRESS
        DbsUtil.examine(new ExamineSource(pdaAddrpart.getPnd_Twrapart_Address_Lines().getValue("*")), new ExamineSearch(" 00000 "), new ExamineReplace("        "));      //Natural: EXAMINE ADDRESS-LINES ( * ) FOR ' 00000 ' REPLACE WITH '        '
        pdaAddrpart.getPnd_Twrapart_Pnd_Function().setValue("O");                                                                                                         //Natural: ASSIGN #TWRAPART.#FUNCTION := 'O'
        if (condition(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Foreign_Cde().getValue(1).equals("Y")))                                                                     //Natural: IF WF-PYMNT-ADDR-GRP1.PYMNT-FOREIGN-CDE ( 1 ) = 'Y'
        {
            pdaAddrpart.getPnd_Twrapart_Foreign_Addr().setValue("Y");                                                                                                     //Natural: ASSIGN #TWRAPART.FOREIGN-ADDR := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAddrpart.getPnd_Twrapart_Foreign_Addr().setValue("N");                                                                                                     //Natural: ASSIGN #TWRAPART.FOREIGN-ADDR := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR FIRST ADDRESS
        DbsUtil.callnat(Addrconv.class , getCurrentProcessState(), pdaAddrpart.getPnd_Twrapart());                                                                        //Natural: CALLNAT 'ADDRCONV' #TWRAPART
        if (condition(Global.isEscape())) return;
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Apo_Geo().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Apo_Geo());                                                       //Natural: MOVE APO-GEO TO VS-APO-GEO ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Geo().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Geo());                                                               //Natural: MOVE GEO TO VS-GEO ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Street_Addr().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Street_Addr());                                               //Natural: MOVE STREET-ADDR TO VS-STREET-ADDR ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Street_Addr_Cont_1().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Street_Addr_Cont_1());                                 //Natural: MOVE STREET-ADDR-CONT-1 TO VS-STREET-ADDR-CONT-1 ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Street_Addr_Cont_2().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Street_Addr_Cont_2());                                 //Natural: MOVE STREET-ADDR-CONT-2 TO VS-STREET-ADDR-CONT-2 ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_City().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_City());                                                             //Natural: MOVE CITY TO VS-CITY ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Province_Code().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Province_Code());                                           //Natural: MOVE PROVINCE-CODE TO VS-PROVINCE-CODE ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Country_Name().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Country_Name());                                             //Natural: MOVE COUNTRY-NAME TO VS-COUNTRY-NAME ( 1 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Zip_Code().getValue(1).setValue(pdaAddrpart.getPnd_Twrapart_Addr_Zip_Plus_4());                                              //Natural: MOVE ADDR-ZIP-PLUS-4 TO VS-ZIP-CODE ( 1 )
        if (condition(pdaAddrpart.getPnd_Twrapart_Foreign_Addr().equals("N")))                                                                                            //Natural: IF #TWRAPART.FOREIGN-ADDR = 'N'
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Country_Name().getValue(1).setValue("USA");                                                                              //Natural: MOVE 'USA' TO VS-COUNTRY-NAME ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAddrpart.getPnd_Twrapart().reset();                                                                                                                            //Natural: RESET #TWRAPART
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_1().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line1_Txt().getValue(2));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE1-TXT ( 2 ) TO ADDR-LNE-1
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_2().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line2_Txt().getValue(2));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE2-TXT ( 2 ) TO ADDR-LNE-2
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_3().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line3_Txt().getValue(2));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE3-TXT ( 2 ) TO ADDR-LNE-3
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_4().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line4_Txt().getValue(2));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE4-TXT ( 2 ) TO ADDR-LNE-4
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_5().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line5_Txt().getValue(2));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE5-TXT ( 2 ) TO ADDR-LNE-5
        pdaAddrpart.getPnd_Twrapart_Addr_Lne_6().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Addr_Line6_Txt().getValue(2));                                          //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-ADDR-LINE6-TXT ( 2 ) TO ADDR-LNE-6
        pdaAddrpart.getPnd_Twrapart_Addr_Postal_Data().setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Postl_Data().getValue(2));                                        //Natural: MOVE WF-PYMNT-ADDR-GRP1.PYMNT-POSTL-DATA ( 2 ) TO ADDR-POSTAL-DATA
        if (condition(pdaAddrpart.getPnd_Twrapart_Addr_Lne_6().getSubstring(1,2).equals("//")))                                                                           //Natural: IF SUBSTR ( ADDR-LNE-6,1,2 ) = '//'
        {
            pdaAddrpart.getPnd_Twrapart_Addr_Lne_6().reset();                                                                                                             //Natural: RESET ADDR-LNE-6
        }                                                                                                                                                                 //Natural: END-IF
        //*  TO POPULATE STATE CODE FOR SECOND ADDRESS
        DbsUtil.examine(new ExamineSource(pdaAddrpart.getPnd_Twrapart_Address_Lines().getValue("*")), new ExamineSearch(" 00000 "), new ExamineReplace("        "));      //Natural: EXAMINE ADDRESS-LINES ( * ) FOR ' 00000 ' REPLACE WITH '        '
        pdaAddrpart.getPnd_Twrapart_Pnd_Function().setValue("O");                                                                                                         //Natural: ASSIGN #TWRAPART.#FUNCTION := 'O'
        if (condition(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Foreign_Cde().getValue(2).equals("Y")))                                                                     //Natural: IF WF-PYMNT-ADDR-GRP1.PYMNT-FOREIGN-CDE ( 2 ) = 'Y'
        {
            pdaAddrpart.getPnd_Twrapart_Foreign_Addr().setValue("Y");                                                                                                     //Natural: ASSIGN #TWRAPART.FOREIGN-ADDR := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAddrpart.getPnd_Twrapart_Foreign_Addr().setValue("N");                                                                                                     //Natural: ASSIGN #TWRAPART.FOREIGN-ADDR := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR SECOND ADDRESS
        DbsUtil.callnat(Addrconv.class , getCurrentProcessState(), pdaAddrpart.getPnd_Twrapart());                                                                        //Natural: CALLNAT 'ADDRCONV' #TWRAPART
        if (condition(Global.isEscape())) return;
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Apo_Geo().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Apo_Geo());                                                       //Natural: MOVE APO-GEO TO VS-APO-GEO ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Geo().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Geo());                                                               //Natural: MOVE GEO TO VS-GEO ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Street_Addr().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Street_Addr());                                               //Natural: MOVE STREET-ADDR TO VS-STREET-ADDR ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Street_Addr_Cont_1().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Street_Addr_Cont_1());                                 //Natural: MOVE STREET-ADDR-CONT-1 TO VS-STREET-ADDR-CONT-1 ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Street_Addr_Cont_2().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Street_Addr_Cont_2());                                 //Natural: MOVE STREET-ADDR-CONT-2 TO VS-STREET-ADDR-CONT-2 ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_City().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_City());                                                             //Natural: MOVE CITY TO VS-CITY ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Province_Code().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Province_Code());                                           //Natural: MOVE PROVINCE-CODE TO VS-PROVINCE-CODE ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Country_Name().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Country_Name());                                             //Natural: MOVE COUNTRY-NAME TO VS-COUNTRY-NAME ( 2 )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Zip_Code().getValue(2).setValue(pdaAddrpart.getPnd_Twrapart_Addr_Zip_Plus_4());                                              //Natural: MOVE ADDR-ZIP-PLUS-4 TO VS-ZIP-CODE ( 2 )
        if (condition(pdaAddrpart.getPnd_Twrapart_Foreign_Addr().equals("N")))                                                                                            //Natural: IF #TWRAPART.FOREIGN-ADDR = 'N'
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Vs_Country_Name().getValue(2).setValue("USA");                                                                              //Natural: MOVE 'USA' TO VS-COUNTRY-NAME ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Off_Mode_Contract().equals(false)))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.OFF-MODE-CONTRACT = FALSE
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Off_Mode_Contract().setValue("N");                                                                                          //Natural: MOVE 'N' TO WF-PYMNT-ADDR-GRP1.OFF-MODE-CONTRACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Off_Mode_Contract().setValue("Y");                                                                                          //Natural: MOVE 'Y' TO WF-PYMNT-ADDR-GRP1.OFF-MODE-CONTRACT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde().equals("AP")))                                                                                    //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE = 'AP'
        {
            pnd_Dist.reset();                                                                                                                                             //Natural: RESET #DIST
            DbsUtil.callnat(Fcpndcm.class , getCurrentProcessState(), pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue("*"), pnd_Gtn_Cntrct_Money_Source, pnd_Gtn_Hardship_Cde, //Natural: CALLNAT 'FCPNDCM' USING WF-PYMNT-ADDR-GRP ( * ) #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE #DIST
                pnd_Dist);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Canadian_Tax_Amt().compute(new ComputeParameters(false, ldaFcpls462.getWf_Pymnt_Addr_Grp1_Canadian_Tax_Amt()),                  //Natural: ASSIGN WF-PYMNT-ADDR-GRP1.CANADIAN-TAX-AMT := #GTN-FED-CANADIAN-AMT ( 1 ) + #GTN-FED-CANADIAN-AMT ( 2 )
            pnd_Gtn_Fed_Canadian_Amt.getValue(1).add(pnd_Gtn_Fed_Canadian_Amt.getValue(2)));
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Gtn_W9_Ben_Ind().setValue(pnd_Gtn_W9_Ben_Ind);                                                                                  //Natural: MOVE #GTN-W9-BEN-IND TO GTN-W9-BEN-IND
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Gtn_Fed_Tax_Type().getValue("*").setValue(pnd_Gtn_Fed_Tax_Type.getValue("*"));                                                  //Natural: MOVE #GTN-FED-TAX-TYPE ( * ) TO GTN-FED-TAX-TYPE ( * )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Gtn_Fed_Tax_Amt().getValue("*").setValue(pnd_Gtn_Fed_Tax_Amt.getValue("*"));                                                    //Natural: MOVE #GTN-FED-TAX-AMT ( * ) TO GTN-FED-TAX-AMT ( * )
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Gtn_Elec_Cde().setValue(pnd_Gtn_Elec_Cde);                                                                                      //Natural: MOVE #GTN-ELEC-CDE TO GTN-ELEC-CDE
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Gtn_Cntrct_Money_Source().setValue(pnd_Gtn_Cntrct_Money_Source);                                                                //Natural: MOVE #GTN-CNTRCT-MONEY-SOURCE TO GTN-CNTRCT-MONEY-SOURCE
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Dist_Cde().setValue(pnd_Dist);                                                                                                  //Natural: MOVE #DIST TO DIST-CDE
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Pymnt_Deceased_Nme().setValue(pnd_Pymnt_Deceased_Nme);                                                                          //Natural: MOVE #PYMNT-DECEASED-NME TO PYMNT-DECEASED-NME
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Cntrct_Pymnt_Type_Ind().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind());                                      //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND TO WF-PYMNT-ADDR-GRP1.CNTRCT-PYMNT-TYPE-IND
        ldaFcpls462.getWf_Pymnt_Addr_Grp1_Cntrct_Sttlmnt_Type_Ind().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind());                                  //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND TO WF-PYMNT-ADDR-GRP1.CNTRCT-STTLMNT-TYPE-IND
        //*  THIS IF PART IS FOR FCPP835I, CSR DATA EXTRACT! */
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Stats_Cde().equals("D")))      //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'R' AND WF-PYMNT-ADDR-GRP.PYMNT-STATS-CDE EQ 'D'
        {
            getWorkFiles().write(2, false, ldaFcpls462.getWf_Pymnt_Addr_Grp1().getValue("*"));                                                                            //Natural: WRITE WORK FILE 2 WF-PYMNT-ADDR-GRP1 ( * )
            pnd_Write_Rec_Count.nadd(1);                                                                                                                                  //Natural: ASSIGN #WRITE-REC-COUNT := #WRITE-REC-COUNT + 1
            //*  THIS ELSE PART IS FOR NORMAL FORWARD PROCESS. */
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet1126 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE;//Natural: VALUE 'AP'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde().equals("AP"))))
            {
                decideConditionsMet1126++;
                getWorkFiles().write(2, false, ldaFcpls462.getWf_Pymnt_Addr_Grp1().getValue("*"));                                                                        //Natural: WRITE WORK FILE 2 WF-PYMNT-ADDR-GRP1 ( * )
                pnd_Write_Rec_Count.nadd(1);                                                                                                                              //Natural: ASSIGN #WRITE-REC-COUNT := #WRITE-REC-COUNT + 1
            }                                                                                                                                                             //Natural: VALUE 'NZ'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde().equals("NZ"))))
            {
                decideConditionsMet1126++;
                getWorkFiles().write(2, false, ldaFcpls462.getWf_Pymnt_Addr_Grp1().getValue("*"));                                                                        //Natural: WRITE WORK FILE 2 WF-PYMNT-ADDR-GRP1 ( * )
                pnd_Write_Rec_Count.nadd(1);                                                                                                                              //Natural: ASSIGN #WRITE-REC-COUNT := #WRITE-REC-COUNT + 1
            }                                                                                                                                                             //Natural: VALUE 'DC'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde().equals("DC"))))
            {
                decideConditionsMet1126++;
                tiacpsdcReturnCode = DbsUtil.callExternalProgram("TIACPSDC",ldaFcpls462.getWf_Pymnt_Addr_Grp1().getValue(1,":",31),ldaFcpls462.getWf_Pymnt_Addr_Grp1_Ws_Return_Msg()); //Natural: CALL 'TIACPSDC' USING WF-PYMNT-ADDR-GRP1 ( * ) WS-RETURN-MSG
                pnd_Write_Rec_Count.nadd(1);                                                                                                                              //Natural: ASSIGN #WRITE-REC-COUNT := #WRITE-REC-COUNT + 1
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        s462_Ret_Code.setValue(Global.getERROR_NR());                                                                                                                     //Natural: MOVE *ERROR-NR TO S462-RET-CODE
        s462_Ret_Msg.setValue(DbsUtil.compress(Global.getPROGRAM(), "- ERROR ON LINE", Global.getERROR_LINE(), "ERROR NBR", Global.getERROR_NR()));                       //Natural: COMPRESS *PROGRAM '- ERROR ON LINE' *ERROR-LINE 'ERROR NBR' *ERROR-NR INTO S462-RET-MSG
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE IMMEDIATE
    };                                                                                                                                                                    //Natural: END-ERROR
}
