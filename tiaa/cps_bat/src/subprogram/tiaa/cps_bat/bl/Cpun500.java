/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:10:07 PM
**        * FROM NATURAL SUBPROGRAM : Cpun500
************************************************************
**        * FILE NAME            : Cpun500.java
**        * CLASS NAME           : Cpun500
**        * INSTANCE NAME        : Cpun500
************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpun500 extends BLNatBase
{
    // Data Areas
    private LdaCpul500 ldaCpul500;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Input;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_Array;
    private DbsField pnd_I;
    private DbsField pnd_N;
    private DbsField pnd_Idx;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCpul500 = new LdaCpul500();
        registerRecord(ldaCpul500);

        // parameters
        parameters = new DbsRecord();
        pnd_Input = parameters.newFieldInRecord("pnd_Input", "#INPUT", FieldType.STRING, 200);
        pnd_Input.setParameterOption(ParameterOption.ByReference);

        pnd_Input__R_Field_1 = parameters.newGroupInRecord("pnd_Input__R_Field_1", "REDEFINE", pnd_Input);
        pnd_Input_Pnd_Array = pnd_Input__R_Field_1.newFieldArrayInGroup("pnd_Input_Pnd_Array", "#ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 
            200));
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.INTEGER, 2);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCpul500.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cpun500() throws Exception
    {
        super("Cpun500");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 200
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(200)); pnd_I.nadd(1))
        {
            if (condition(pnd_Input_Pnd_Array.getValue(pnd_I).equals(" ")))                                                                                               //Natural: IF #ARRAY ( #I ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(ldaCpul500.getPnd_Ebcidic().getValue("*")), new ExamineSearch(pnd_Input_Pnd_Array.getValue(pnd_I)), new                     //Natural: EXAMINE #EBCIDIC ( * ) FOR #ARRAY ( #I ) GIVING NUMBER #N INDEX #IDX
                ExamineGivingNumber(pnd_N), new ExamineGivingIndex(pnd_Idx));
            pnd_Input_Pnd_Array.getValue(pnd_I).setValue(ldaCpul500.getPnd_Translation().getValue(pnd_Idx));                                                              //Natural: ASSIGN #ARRAY ( #I ) := #TRANSLATION ( #IDX )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
