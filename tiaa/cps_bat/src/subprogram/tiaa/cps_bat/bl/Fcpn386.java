/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:23:15 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn386
************************************************************
**        * FILE NAME            : Fcpn386.java
**        * CLASS NAME           : Fcpn386
**        * INSTANCE NAME        : Fcpn386
************************************************************
************************************************************************
*  FCPN386
*
* R. LANDRUM    01/05/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn386 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;
    private PdaFcpa110 pdaFcpa110;
    private LdaCpol155m ldaCpol155m;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Orgn_Cde;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_Ws_Rec;

    private DbsGroup pnd_Ws_Rec__R_Field_1;
    private DbsField pnd_Ws_Rec_Pnd_Ws_Cc;
    private DbsField pnd_Ws_Rec_Pnd_Ws_Font;
    private DbsField pnd_Ws_Rec_Pnd_Ws_Detail;

    private DbsGroup pnd_Ws_Rec_14;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_14_Cc;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_14_Orgn;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1;

    private DbsGroup pnd_Ws_Rec_15;
    private DbsField pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A;
    private DbsField pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2;
    private DbsField pnd_Ws_Rec_21;
    private DbsField pnd_Ws_Index;
    private DbsField pnd_Address_Filler;

    private DbsGroup pnd_Ws_Chk_Amt;
    private DbsField pnd_Ws_Chk_Amt_Pnd_I;
    private DbsField pnd_Ws_Chk_Amt_Pnd_Work_Chk_Amt;
    private DbsField pnd_Ws_Chk_Amt_Pnd_Amt_Alpha;
    private DbsField pnd_Ws_Chk_Amt_Pnd_Amt_Text;
    private DbsField pnd_Pymnt_Seq_Nbr;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField split_Top;
    private DbsField split_Bottom;
    private DbsField pnd_Pay_Amt;
    private DbsField pnd_Pay_Amt_Edit;

    private DbsGroup pnd_Pay_Amt_Edit__R_Field_3;
    private DbsField pnd_Pay_Amt_Edit_Pnd_Remove_Stars;
    private DbsField pnd_Prt_Pymnt_Check_No;
    private DbsField pnd_Prt_Pymnt_Chck_Seq_No;
    private DbsField pnd_Ws_Address_Lines_Printed;
    private DbsField pnd_Ws_Pymnt_Check_Dte;

    private DbsGroup pnd_Ws_Pymnt_Check_Dte__R_Field_4;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Century;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd;
    private DbsField pnd_Ws_Amt_Mask;
    private DbsField pnd_Ws_Gross_A;
    private DbsField pnd_Ws_Gross_N;

    private DbsGroup pnd_Ws_Gross_N__R_Field_5;
    private DbsField pnd_Ws_Gross_N_Pnd_Ws_Amt;
    private DbsField pnd_Ws_Gross_N_Pnd_Ws_Rem;
    private DbsField pnd_Ws_Hold_Amt;
    private DbsField pnd_Ws_Amount;

    private DbsGroup pnd_Ws_Amount__R_Field_6;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Million;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Hundred_Thousand;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Thousands;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Hundred;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Dollars;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Cents;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCpol155m = new LdaCpol155m();
        registerRecord(ldaCpol155m);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        pdaFcpa110 = new PdaFcpa110(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Orgn_Cde", "#WS-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_Ws_Rec = localVariables.newFieldInRecord("pnd_Ws_Rec", "#WS-REC", FieldType.STRING, 143);

        pnd_Ws_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Rec__R_Field_1", "REDEFINE", pnd_Ws_Rec);
        pnd_Ws_Rec_Pnd_Ws_Cc = pnd_Ws_Rec__R_Field_1.newFieldInGroup("pnd_Ws_Rec_Pnd_Ws_Cc", "#WS-CC", FieldType.STRING, 1);
        pnd_Ws_Rec_Pnd_Ws_Font = pnd_Ws_Rec__R_Field_1.newFieldInGroup("pnd_Ws_Rec_Pnd_Ws_Font", "#WS-FONT", FieldType.STRING, 1);
        pnd_Ws_Rec_Pnd_Ws_Detail = pnd_Ws_Rec__R_Field_1.newFieldInGroup("pnd_Ws_Rec_Pnd_Ws_Detail", "#WS-DETAIL", FieldType.STRING, 141);

        pnd_Ws_Rec_14 = localVariables.newGroupInRecord("pnd_Ws_Rec_14", "#WS-REC-14");
        pnd_Ws_Rec_14_Pnd_Ws_14_Cc = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_14_Cc", "#WS-14-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_14_Pnd_Ws_14_Orgn = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_14_Orgn", "#WS-14-ORGN", FieldType.STRING, 9);
        pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1 = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1", "#WS-AMT-IN-WORDS-LINE-1", 
            FieldType.STRING, 132);

        pnd_Ws_Rec_15 = localVariables.newGroupInRecord("pnd_Ws_Rec_15", "#WS-REC-15");
        pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A = pnd_Ws_Rec_15.newFieldInGroup("pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A", "#WS-15-FILLER-A", FieldType.STRING, 11);
        pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2 = pnd_Ws_Rec_15.newFieldInGroup("pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2", "#WS-AMT-IN-WORDS-LINE-2", 
            FieldType.STRING, 132);
        pnd_Ws_Rec_21 = localVariables.newFieldInRecord("pnd_Ws_Rec_21", "#WS-REC-21", FieldType.STRING, 143);
        pnd_Ws_Index = localVariables.newFieldInRecord("pnd_Ws_Index", "#WS-INDEX", FieldType.INTEGER, 2);
        pnd_Address_Filler = localVariables.newFieldInRecord("pnd_Address_Filler", "#ADDRESS-FILLER", FieldType.STRING, 22);

        pnd_Ws_Chk_Amt = localVariables.newGroupInRecord("pnd_Ws_Chk_Amt", "#WS-CHK-AMT");
        pnd_Ws_Chk_Amt_Pnd_I = pnd_Ws_Chk_Amt.newFieldInGroup("pnd_Ws_Chk_Amt_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Chk_Amt_Pnd_Work_Chk_Amt = pnd_Ws_Chk_Amt.newFieldInGroup("pnd_Ws_Chk_Amt_Pnd_Work_Chk_Amt", "#WORK-CHK-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Ws_Chk_Amt_Pnd_Amt_Alpha = pnd_Ws_Chk_Amt.newFieldInGroup("pnd_Ws_Chk_Amt_Pnd_Amt_Alpha", "#AMT-ALPHA", FieldType.STRING, 15);
        pnd_Ws_Chk_Amt_Pnd_Amt_Text = pnd_Ws_Chk_Amt.newFieldArrayInGroup("pnd_Ws_Chk_Amt_Pnd_Amt_Text", "#AMT-TEXT", FieldType.STRING, 132, new DbsArrayController(1, 
            2));
        pnd_Pymnt_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Seq_Nbr", "#PYMNT-SEQ-NBR", FieldType.STRING, 7);
        pnd_Ws_Pymnt_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Nbr_N10", "#WS-PYMNT-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Ws_Pymnt_Check_Nbr_N10);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", 
            "#WS-CHECK-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", 
            "#WS-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        split_Top = localVariables.newFieldInRecord("split_Top", "SPLIT-TOP", FieldType.STRING, 6);
        split_Bottom = localVariables.newFieldInRecord("split_Bottom", "SPLIT-BOTTOM", FieldType.STRING, 5);
        pnd_Pay_Amt = localVariables.newFieldInRecord("pnd_Pay_Amt", "#PAY-AMT", FieldType.STRING, 17);
        pnd_Pay_Amt_Edit = localVariables.newFieldInRecord("pnd_Pay_Amt_Edit", "#PAY-AMT-EDIT", FieldType.STRING, 20);

        pnd_Pay_Amt_Edit__R_Field_3 = localVariables.newGroupInRecord("pnd_Pay_Amt_Edit__R_Field_3", "REDEFINE", pnd_Pay_Amt_Edit);
        pnd_Pay_Amt_Edit_Pnd_Remove_Stars = pnd_Pay_Amt_Edit__R_Field_3.newFieldInGroup("pnd_Pay_Amt_Edit_Pnd_Remove_Stars", "#REMOVE-STARS", FieldType.STRING, 
            6);
        pnd_Prt_Pymnt_Check_No = localVariables.newFieldInRecord("pnd_Prt_Pymnt_Check_No", "#PRT-PYMNT-CHECK-NO", FieldType.NUMERIC, 7);
        pnd_Prt_Pymnt_Chck_Seq_No = localVariables.newFieldInRecord("pnd_Prt_Pymnt_Chck_Seq_No", "#PRT-PYMNT-CHCK-SEQ-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Address_Lines_Printed = localVariables.newFieldInRecord("pnd_Ws_Address_Lines_Printed", "#WS-ADDRESS-LINES-PRINTED", FieldType.NUMERIC, 
            1);
        pnd_Ws_Pymnt_Check_Dte = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Dte", "#WS-PYMNT-CHECK-DTE", FieldType.STRING, 8);

        pnd_Ws_Pymnt_Check_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Pymnt_Check_Dte__R_Field_4", "REDEFINE", pnd_Ws_Pymnt_Check_Dte);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Century = pnd_Ws_Pymnt_Check_Dte__R_Field_4.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Century", "#WS-CENTURY", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy = pnd_Ws_Pymnt_Check_Dte__R_Field_4.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy", "#WS-YY", FieldType.STRING, 
            2);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm = pnd_Ws_Pymnt_Check_Dte__R_Field_4.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm", "#WS-MM", FieldType.STRING, 
            2);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd = pnd_Ws_Pymnt_Check_Dte__R_Field_4.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd", "#WS-DD", FieldType.STRING, 
            2);
        pnd_Ws_Amt_Mask = localVariables.newFieldInRecord("pnd_Ws_Amt_Mask", "#WS-AMT-MASK", FieldType.STRING, 14);
        pnd_Ws_Gross_A = localVariables.newFieldInRecord("pnd_Ws_Gross_A", "#WS-GROSS-A", FieldType.STRING, 14);
        pnd_Ws_Gross_N = localVariables.newFieldInRecord("pnd_Ws_Gross_N", "#WS-GROSS-N", FieldType.NUMERIC, 11, 2);

        pnd_Ws_Gross_N__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Gross_N__R_Field_5", "REDEFINE", pnd_Ws_Gross_N);
        pnd_Ws_Gross_N_Pnd_Ws_Amt = pnd_Ws_Gross_N__R_Field_5.newFieldInGroup("pnd_Ws_Gross_N_Pnd_Ws_Amt", "#WS-AMT", FieldType.NUMERIC, 9);
        pnd_Ws_Gross_N_Pnd_Ws_Rem = pnd_Ws_Gross_N__R_Field_5.newFieldInGroup("pnd_Ws_Gross_N_Pnd_Ws_Rem", "#WS-REM", FieldType.STRING, 2);
        pnd_Ws_Hold_Amt = localVariables.newFieldInRecord("pnd_Ws_Hold_Amt", "#WS-HOLD-AMT", FieldType.STRING, 10);
        pnd_Ws_Amount = localVariables.newFieldInRecord("pnd_Ws_Amount", "#WS-AMOUNT", FieldType.NUMERIC, 9, 2);

        pnd_Ws_Amount__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Amount__R_Field_6", "REDEFINE", pnd_Ws_Amount);
        pnd_Ws_Amount_Pnd_Ws_Million = pnd_Ws_Amount__R_Field_6.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Million", "#WS-MILLION", FieldType.NUMERIC, 1);
        pnd_Ws_Amount_Pnd_Ws_Hundred_Thousand = pnd_Ws_Amount__R_Field_6.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Hundred_Thousand", "#WS-HUNDRED-THOUSAND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Amount_Pnd_Ws_Thousands = pnd_Ws_Amount__R_Field_6.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Thousands", "#WS-THOUSANDS", FieldType.NUMERIC, 
            2);
        pnd_Ws_Amount_Pnd_Ws_Hundred = pnd_Ws_Amount__R_Field_6.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Hundred", "#WS-HUNDRED", FieldType.NUMERIC, 1);
        pnd_Ws_Amount_Pnd_Ws_Dollars = pnd_Ws_Amount__R_Field_6.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Dollars", "#WS-DOLLARS", FieldType.NUMERIC, 2);
        pnd_Ws_Amount_Pnd_Ws_Cents = pnd_Ws_Amount__R_Field_6.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Cents", "#WS-CENTS", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCpol155m.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Ws_Rec_14_Pnd_Ws_14_Cc.setInitialValue("1!");
        pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A.setInitialValue(" !");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn386() throws Exception
    {
        super("Fcpn386");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        //* *ASSIGN #PROGRAM = *PROGRAM
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *
        //* *
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        //* *SAG DEFINE EXIT AFTER-PRIME-READ
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1 #WS-REC-14 #WS-REC-15 #WS-REC-21
        pnd_Ws_Rec_14.reset();
        pnd_Ws_Rec_15.reset();
        pnd_Ws_Rec_21.reset();
        //* ***********************
        //*  RL NEW CHECK RTN FOR NEW FORMS - PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GENERATE-CHECK
        sub_Generate_Check();
        if (condition(Global.isEscape())) {return;}
        //* ****************** RL BEGIN POS-PAY PAYEE MATCH **********************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-CHECK
        //* *  ASSIGN #WS-REC-14   = '1!I.A.'
        //* *  COMPRESS '*1*********'
        //* *  COMPRESS '*1*********'
        //* *#WS-REC := '19'
        //* *#WS-REC := '+!'
        //* ********************* RL END POS PAY PAYEE MATCH *********************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ADDRESS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MICR
        //* ****************** RL END POS-PAY/PAYEE MATCH ***********************
        //* ******** OLD CODE BELOW ***********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OLD-CHECK-PRITNING-CODE
    }
    private void sub_Generate_Check() throws Exception                                                                                                                    //Natural: GENERATE-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  FIRST '1' MEANS STOP POINT
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("12");                                                                                                                                        //Natural: ASSIGN #WS-REC := '12'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pdaFcpa110.getFcpa110_Company_Name());                                                                                          //Natural: ASSIGN #WS-DETAIL := FCPA110.COMPANY-NAME
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        //*  '+' MEANS OVER PRINT
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("+8");                                                                                                                                        //Natural: ASSIGN #WS-REC := '+8'
        pnd_Address_Filler.moveAll("*");                                                                                                                                  //Natural: MOVE ALL '*' TO #ADDRESS-FILLER
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Address_Filler, pdaFcpa110.getFcpa110_Company_Address()));                  //Natural: COMPRESS #ADDRESS-FILLER FCPA110.COMPANY-ADDRESS INTO #WS-DETAIL LEAVING NO
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_Pnd_Ws_Detail,true), new ExamineSearch("*"), new ExamineReplace(" "));                                               //Natural: EXAMINE FULL #WS-DETAIL '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("12");                                                                                                                                        //Natural: ASSIGN #WS-REC := '12'
        //*  IF FCPA110-SOURCE-CODE =
        //* RL
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr());                                             //Natural: MOVE #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //* RL
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                           //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //* RL
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                                                    //Natural: MOVE #WS-PYMNT-CHECK-NBR-N10 TO #WS-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Amount.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                                     //Natural: ASSIGN #WS-AMOUNT := PYMNT-CHECK-AMT
        DbsUtil.callnat(Fcpn255.class , getCurrentProcessState(), pnd_Ws_Amount, pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1, pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2);     //Natural: CALLNAT 'FCPN255' #WS-AMOUNT #WS-AMT-IN-WORDS-LINE-1 #WS-AMT-IN-WORDS-LINE-2
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Ws_Orgn_Cde.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde());                                                                            //Natural: ASSIGN #WS-ORGN-CDE := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        short decideConditionsMet508 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #WS-HEADER-RECORD.CNTRCT-ORGN-CDE;//Natural: VALUE 'MS'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("MS"))))
        {
            decideConditionsMet508++;
            //* *  ASSIGN #WS-REC-14   = '1!M.S.'
            pnd_Ws_Rec_14_Pnd_Ws_14_Cc.setValue("1!");                                                                                                                    //Natural: ASSIGN #WS-14-CC = '1!'
            pnd_Ws_Rec_14_Pnd_Ws_14_Orgn.setValue("M.S.");                                                                                                                //Natural: ASSIGN #WS-14-ORGN = 'M.S.'
        }                                                                                                                                                                 //Natural: VALUE 'IA'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("IA"))))
        {
            decideConditionsMet508++;
            pnd_Ws_Rec_14_Pnd_Ws_14_Cc.setValue("1!");                                                                                                                    //Natural: ASSIGN #WS-14-CC = '1!'
            pnd_Ws_Rec_14_Pnd_Ws_14_Orgn.setValue("I.A.");                                                                                                                //Natural: ASSIGN #WS-14-ORGN = 'I.A.'
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet508++;
            //* *  ASSIGN #WS-REC-14   = '1!D.C.'
            pnd_Ws_Rec_14_Pnd_Ws_14_Cc.setValue("1!");                                                                                                                    //Natural: ASSIGN #WS-14-CC = '1!'
            pnd_Ws_Rec_14_Pnd_Ws_14_Orgn.setValue("D.C.");                                                                                                                //Natural: ASSIGN #WS-14-ORGN = 'D.C.'
        }                                                                                                                                                                 //Natural: VALUE 'DS'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DS"))))
        {
            decideConditionsMet508++;
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("GRA") || pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("GSRA")))       //Natural: IF CNTRCT-LOB-CDE = 'GRA' OR = 'GSRA'
            {
                //* *    ASSIGN #WS-REC-14  = '1!L.S.'
                pnd_Ws_Rec_14_Pnd_Ws_14_Cc.setValue("1!");                                                                                                                //Natural: ASSIGN #WS-14-CC = '1!'
                pnd_Ws_Rec_14_Pnd_Ws_14_Orgn.setValue("L.S.");                                                                                                            //Natural: ASSIGN #WS-14-ORGN = 'L.S.'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *    ASSIGN #WS-REC-14  = '1!D.S.'
                pnd_Ws_Rec_14_Pnd_Ws_14_Cc.setValue("1!");                                                                                                                //Natural: ASSIGN #WS-14-CC = '1!'
                pnd_Ws_Rec_14_Pnd_Ws_14_Orgn.setValue("D.S.");                                                                                                            //Natural: ASSIGN #WS-14-ORGN = 'D.S.'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet508 > 0))
        {
            getWorkFiles().write(8, false, pnd_Ws_Rec_14);                                                                                                                //Natural: WRITE WORK FILE 8 #WS-REC-14
            pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A.setValue(" !");                                                                                                              //Natural: ASSIGN #WS-15-FILLER-A = ' !'
            getWorkFiles().write(8, false, pnd_Ws_Rec_15);                                                                                                                //Natural: WRITE WORK FILE 8 #WS-REC-15
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *DECIDE ON FIRST #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        //* *  VALUE 'DS'
        //* *    IF CNTRCT-LOB-CDE = 'GRA' OR = 'GSRA'
        //* *      #WS-ORGN-CDE    := 'LS'
        //* *    END-IF
        //* *  VALUE 'SS'
        //* *    IF CNTRCT-TYPE-CDE = '30' OR = '31'
        //* *      #WS-ORGN-CDE    := 'MX'
        //* *    END-IF
        //* *  NONE
        //* *    IGNORE
        //* *END-DECIDE
        //* *MOVE EDITED #WS-ORGN-CDE(EM=X.X.) TO #WS-14-ORGN
        if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals("0"))  //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( 1 ) = ' ' OR = '0' ) OR PYMNT-NME ( 1 ) = MASK ( 'CR ' )
            || DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1),"'CR '")))
        {
            if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals("0")))) //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( 1 ) = ' ' OR = '0' )
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1), "H'0000'", "A/C", "H'00'",             //Natural: COMPRESS '*!*********' PYMNT-NME ( 1 ) H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR ( 1 ) INTO #WS-REC-1
                    pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1)));
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1)));                                      //Natural: COMPRESS '*!*********' PYMNT-NME ( 1 ) INTO #WS-REC-1
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  03   BCU
        if (condition(! (pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde().equals(" ") || pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde().equals("0"))))          //Natural: IF NOT ( CNTRCT-HOLD-CDE = ' ' OR = '0' )
        {
            pnd_Ws_Rec_1.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde(),new ReportEditMask("�0!���XXXX"));                                           //Natural: MOVE EDITED CNTRCT-HOLD-CDE ( EM = �0!���XXXX ) TO #WS-REC-1
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 3");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 3'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        //* ****************
        //* ********* COMMENTED OUT MON JAN 30 4:30 **************
        //* *RESET #WS-REC
        //* *#WS-REC := ' !'
        //* *MOVE EDITED PYMNT-CHECK-SEQ-NBR(EM=9999999) TO #PYMNT-SEQ-NBR
        //* *COMPRESS 'S' #PYMNT-SEQ-NBR INTO #WS-DETAIL LEAVING NO
        //* *WRITE WORK FILE 8 #WS-REC
        pdaFcpa110.getFcpa110_Bank_Above_Check_Amt_Nbr().separate(SeparateOption.WithAnyDelimiters, "/", split_Top, split_Bottom);                                        //Natural: SEPARATE FCPA110.BANK-ABOVE-CHECK-AMT-NBR INTO SPLIT-TOP SPLIT-BOTTOM WITH DELIMITER '/'
        split_Bottom.setValue(split_Bottom, MoveOption.RightJustified);                                                                                                   //Natural: MOVE RIGHT SPLIT-BOTTOM TO SPLIT-BOTTOM
        DbsUtil.examine(new ExamineSource(split_Top), new ExamineSearch("  ", true), new ExamineReplace(" "));                                                            //Natural: EXAMINE SPLIT-TOP FOR FULL '  ' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(split_Bottom), new ExamineSearch("  ", true), new ExamineReplace(" "));                                                         //Natural: EXAMINE SPLIT-BOTTOM FOR FULL '  ' REPLACE WITH ' '
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("15");                                                                                                                                        //Natural: ASSIGN #WS-REC := '15'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(split_Top);                                                                                                                     //Natural: ASSIGN #WS-DETAIL := SPLIT-TOP
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue(" 5");                                                                                                                                        //Natural: ASSIGN #WS-REC := ' 5'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(split_Bottom);                                                                                                                  //Natural: ASSIGN #WS-DETAIL := SPLIT-BOTTOM
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        //*  RL FEB 15
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("15");                                                                                                                                        //Natural: ASSIGN #WS-REC := '15'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue("PAY");                                                                                                                         //Natural: ASSIGN #WS-DETAIL := 'PAY'
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("+5");                                                                                                                                        //Natural: ASSIGN #WS-REC := '+5'
        pnd_Pay_Amt.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt(),new ReportEditMask("+ZZ,ZZZ,ZZZ,ZZZ.99"));                                        //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = +ZZ,ZZZ,ZZZ,ZZZ.99 ) TO #PAY-AMT
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch("+"), new ExamineReplace("$"));                                                                 //Natural: EXAMINE #PAY-AMT FOR '+' REPLACE '$'
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch(" "), new ExamineReplace("*"));                                                                 //Natural: EXAMINE #PAY-AMT FOR ' ' REPLACE '*'
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch("."), new ExamineReplace(" "));                                                                 //Natural: EXAMINE #PAY-AMT FOR '.' REPLACE ' '
        pnd_Pay_Amt_Edit.setValue(pnd_Pay_Amt, MoveOption.RightJustified);                                                                                                //Natural: MOVE RIGHT #PAY-AMT TO #PAY-AMT-EDIT
        pnd_Pay_Amt_Edit_Pnd_Remove_Stars.setValue(" ");                                                                                                                  //Natural: MOVE ' ' TO #REMOVE-STARS
        //*  MOVE EDITED PYMNT-CHECK-AMT(EM=+Z,ZZZ,ZZ9.99) TO #AMT-ALPHA
        //*  EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
        //*  MOVE ' '                             TO SUBSTR(#AMT-ALPHA,11,1)
        //*  MOVE #AMT-ALPHA                      TO SUBSTR(#WS-REC,10,15)
        //*  MOVE #PAY-AMT-EDIT                   TO SUBSTR(#WS-REC,10,20)
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pnd_Pay_Amt_Edit);                                                                                                              //Natural: MOVE #PAY-AMT-EDIT TO #WS-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("05");                                                                                                                                        //Natural: ASSIGN #WS-REC := '05'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(DbsUtil.compress("ACCOUNT:", pdaFcpa110.getFcpa110_Company_Account()));                                                         //Natural: COMPRESS 'ACCOUNT:' FCPA110.COMPANY-ACCOUNT INTO #WS-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue(" 5");                                                                                                                                        //Natural: ASSIGN #WS-REC := ' 5'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pdaFcpa110.getFcpa110_Not_Valid_Msg());                                                                                         //Natural: MOVE FCPA110.NOT-VALID-MSG TO #WS-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        //*  PERFORM POSTNET-BARCODE      /* RL
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
        //*  RL 13
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("15");                                                                                                                                        //Natural: ASSIGN #WS-REC := '15'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM���DD���YYYY"));                               //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM���DD���YYYY ) TO #WS-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("15");                                                                                                                                        //Natural: ASSIGN #WS-REC := '15'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pdaFcpa110.getFcpa110_Bank_Name());                                                                                             //Natural: ASSIGN #WS-DETAIL := FCPA110.BANK-NAME
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue(" 3");                                                                                                                                        //Natural: ASSIGN #WS-REC := ' 3'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pdaFcpa110.getFcpa110_Bank_Address1());                                                                                         //Natural: ASSIGN #WS-DETAIL := FCPA110.BANK-ADDRESS1
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1", pdaFcpa110.getFcpa110_Bank_Signature_Cde()));                                            //Natural: COMPRESS '1' FCPA110.BANK-SIGNATURE-CDE INTO #WS-REC LEAVE NO SPACE
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("+3");                                                                                                                                        //Natural: ASSIGN #WS-REC := '+3'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(" ");                                                                                                                           //Natural: ASSIGN #WS-DETAIL := ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("13");                                                                                                                                        //Natural: ASSIGN #WS-REC := '13'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pdaFcpa110.getFcpa110_Title());                                                                                                 //Natural: ASSIGN #WS-DETAIL := FCPA110.TITLE
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
                                                                                                                                                                          //Natural: PERFORM WRITE-MICR
        sub_Write_Micr();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Format_Address() throws Exception                                                                                                                    //Natural: FORMAT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //* *******************************
        pnd_Ws_Index.setValue(1);                                                                                                                                         //Natural: ASSIGN #WS-INDEX = 1
        //*  FORMAT BARCODE AND ADDR LINES
        DbsUtil.callnat(Fcpn753a.class , getCurrentProcessState(), pdaFcpa700.getPnd_Ws_Header_Record().getValue("*"), pdaFcpa700.getPnd_Ws_Occurs().getValue("*","*"),   //Natural: CALLNAT 'FCPN753A' #WS-HEADER-RECORD ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS ( *,* ) #WS-INDEX
            pdaFcpa700.getPnd_Ws_Name_N_Address().getValue("*","*"), pnd_Ws_Index);
        if (condition(Global.isEscape())) return;
        //* *******************************
        //* *SIGN #WS-INDEX = 1
        //* *CALLNAT 'FCPN253'      /* FORMAT POSTNET BARCODE AND ADDR LINES
        //* *  #WS-HEADER-RECORD(*)
        //* *  #WS-OCCURS(*,*)          /*RL (A1) (1:40,1:1500) = 20000
        //* ***#WS-OCCURS(*,*)          /*RL (A1) (1:40,1:1500) = 20000
        //* ***#WS-OCCURS(*)            /*RL (A250) (1:80) = 20000
        //* *  #WS-NAME-N-ADDRESS(*,*)  /* RL 1:4
        //* ***#WS-NAME-N-ADDRESS(*)    /* RL 1:4
        //* *  #WS-INDEX
        //* *PYMNT-ADDR-LINE4-TXT
        //* *PYMNT-ADDR-LINE5-TXT
        //* *PYMNT-ADDR-LINE6-TXT
    }
    //*  RL
    private void sub_Write_Micr() throws Exception                                                                                                                        //Natural: WRITE-MICR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        ldaCpol155m.getCpol155m().reset();                                                                                                                                //Natural: RESET CPOL155M
        ldaCpol155m.getCpol155m_Micr_Bank_Cde().setValue(pdaFcpa110.getFcpa110_Bank_Transmission_Cde());                                                                  //Natural: ASSIGN CPOL155M.MICR-BANK-CDE := FCPA110.BANK-TRANSMISSION-CDE
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr());                                             //Natural: MOVE #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                           //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        ldaCpol155m.getCpol155m_Micr_Check_Nbr().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                                    //Natural: ASSIGN CPOL155M.MICR-CHECK-NBR := #WS-PYMNT-CHECK-NBR-N10
        ldaCpol155m.getCpol155m_Micr_Bank_Cde().setValue(pdaFcpa110.getFcpa110_Bank_Transmission_Cde());                                                                  //Natural: ASSIGN CPOL155M.MICR-BANK-CDE := FCPA110.BANK-TRANSMISSION-CDE
        ldaCpol155m.getCpol155m_Micr_Bank_Routing().setValue(pdaFcpa110.getFcpa110_Routing_No());                                                                         //Natural: ASSIGN CPOL155M.MICR-BANK-ROUTING := FCPA110.ROUTING-NO
        ldaCpol155m.getCpol155m_Micr_Bank_Account().setValue(pdaFcpa110.getFcpa110_Account_No());                                                                         //Natural: ASSIGN CPOL155M.MICR-BANK-ACCOUNT := FCPA110.ACCOUNT-NO
        DbsUtil.callnat(Cpon155m.class , getCurrentProcessState(), ldaCpol155m.getCpol155m());                                                                            //Natural: CALLNAT 'CPON155M' CPOL155M
        if (condition(Global.isEscape())) return;
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("19");                                                                                                                                        //Natural: ASSIGN #WS-REC := '19'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(ldaCpol155m.getCpol155m_Micr_Micr_Line());                                                                                      //Natural: ASSIGN #WS-DETAIL := CPOL155M.MICR-MICR-LINE
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        //*  WRITE-MICR
    }
    //*  RL
    private void sub_Old_Check_Pritning_Code() throws Exception                                                                                                           //Natural: OLD-CHECK-PRITNING-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Amount.reset();                                                                                                                                            //Natural: RESET #WS-AMOUNT
        pnd_Ws_Amount.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                                     //Natural: ASSIGN #WS-AMOUNT = PYMNT-CHECK-AMT
        DbsUtil.callnat(Fcpn255.class , getCurrentProcessState(), pnd_Ws_Amount, pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1, pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2);     //Natural: CALLNAT 'FCPN255' #WS-AMOUNT #WS-AMT-IN-WORDS-LINE-1 #WS-AMT-IN-WORDS-LINE-2
        if (condition(Global.isEscape())) return;
        //* *CIDE ON FIRST #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        //* *  VALUE 'MS'
        //* *  ASSIGN #WS-14-ORIGIN   = '1!M.S.'
        //* *  VALUE 'IA'
        //* *    ASSIGN #WS-14-ORIGIN   = '1!I.A.'
        //* *  VALUE 'DC'
        //* *    ASSIGN #WS-14-ORIGIN   = '1!D.C.'
        //* *  VALUE 'DS'
        //* *    IF CNTRCT-LOB-CDE = 'GRA' OR= 'GSRA'
        //* *      ASSIGN #WS-14-ORIGIN   = '1!L.S.'
        //* *    ELSE
        //* *      ASSIGN #WS-14-ORIGIN   = '1!D.S.'
        //* *    END-IF
        //* *  ANY VALUE
        //* *    WRITE WORK FILE 8 #WS-REC-14
        //* *    ASSIGN #WS-15-FILLER-A = ' !'
        //* *    WRITE WORK FILE 8 #WS-REC-15
        //* *  NONE
        //* *    IGNORE
        //* *END-DECIDE
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*3", pdaFcpa700.getPnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr(), "H'000000'",             //Natural: COMPRESS '*3' #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR H'000000' #WS-HEADER-RECORD.#WS-PYMNT-CHECK-SEQ-NBR INTO #WS-REC-1 LEAVING NO SPACE
            pdaFcpa700.getPnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr()));
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals("0"))  //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( 1 ) = ' ' OR = '0' ) OR PYMNT-NME ( 1 ) = MASK ( 'CR ' )
            || DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1),"'CR '")))
        {
            if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals("0")))) //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( 1 ) = ' ' OR = '0' )
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1), "H'0000'", "A/C", "H'00'",             //Natural: COMPRESS '*!*********' PYMNT-NME ( 1 ) H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR ( 1 ) INTO #WS-REC-1
                    pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1)));
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1)));                                      //Natural: COMPRESS '*!*********' PYMNT-NME ( 1 ) INTO #WS-REC-1
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde().equals(" ") || pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde().equals("0"))))          //Natural: IF NOT ( CNTRCT-HOLD-CDE = ' ' OR = '0' )
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*3+++", pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde()));                        //Natural: COMPRESS '*3+++' CNTRCT-HOLD-CDE INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace("0"));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH '0'
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 3");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 3'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        //* *SIGN #WS-REC-1 = '19PAY'
        //*  RL FEB 15
        pnd_Ws_Rec_1.setValue("15PAY");                                                                                                                                   //Natural: ASSIGN #WS-REC-1 = '15PAY'
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Gross_N.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                                    //Natural: MOVE PYMNT-CHECK-AMT TO #WS-GROSS-N
        pnd_Ws_Rec_21.setValue("+5");                                                                                                                                     //Natural: MOVE '+5' TO #WS-REC-21
        pnd_Ws_Hold_Amt.setValueEdited(pnd_Ws_Gross_N_Pnd_Ws_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9"));                                                                    //Natural: MOVE EDITED #WS-AMT ( EM = X'$'Z,ZZZ,ZZ9 ) TO #WS-HOLD-AMT
        DbsUtil.examine(new ExamineSource(pnd_Ws_Hold_Amt,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-HOLD-AMT FOR FULL 'X' DELETE
        //* *MOVE LEFT #WS-HOLD-AMT TO  #WS-HOLD-AMT
        //* *COMPRESS    '$' #WS-HOLD-AMT INTO  #WS-HOLD-AMT LEAVING NO SPACE
        pnd_Ws_Hold_Amt.setValue(pnd_Ws_Hold_Amt, MoveOption.RightJustified);                                                                                             //Natural: MOVE RIGHT #WS-HOLD-AMT TO #WS-HOLD-AMT
        setValueToSubstring(pnd_Ws_Gross_N_Pnd_Ws_Rem,pnd_Ws_Rec_21,21,2);                                                                                                //Natural: MOVE #WS-REM TO SUBSTRING ( #WS-REC-21,21,2 )
        setValueToSubstring(pnd_Ws_Hold_Amt,pnd_Ws_Rec_21,10,10);                                                                                                         //Natural: MOVE #WS-HOLD-AMT TO SUBSTRING ( #WS-REC-21,10,10 )
        //* *COMPRESS '+5*******' #WS-HOLD-AMT H'00' #WS-REM
        //* *  INTO  #WS-REC-1 LEAVING NO SPACE
        //* *EXAMINE FULL #WS-REC-1 FOR FULL '*' REPLACE ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_21);                                                                                                                    //Natural: WRITE WORK FILE 8 #WS-REC-21
        pnd_Ws_Index.setValue(1);                                                                                                                                         //Natural: ASSIGN #WS-INDEX = 1
        //*  FORMAT BARCODE AND ADDR LINES
        DbsUtil.callnat(Fcpn753.class , getCurrentProcessState(), pdaFcpa700.getPnd_Ws_Header_Record().getValue("*"), pdaFcpa700.getPnd_Ws_Occurs().getValue("*","*"),    //Natural: CALLNAT 'FCPN753' #WS-HEADER-RECORD ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS ( *,* ) #WS-INDEX
            pdaFcpa700.getPnd_Ws_Name_N_Address().getValue("*","*"), pnd_Ws_Index);
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pymnt_Check_Dte.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #WS-PYMNT-CHECK-DTE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm), new ExamineSearch("0"), new ExamineReplace("*"));                                            //Natural: EXAMINE #WS-MM '0' REPLACE WITH '*'
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd), new ExamineSearch("0"), new ExamineReplace("*"));                                            //Natural: EXAMINE #WS-DD '0' REPLACE WITH '*'
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy), new ExamineSearch("0"), new ExamineReplace("*"));                                            //Natural: EXAMINE #WS-YY '0' REPLACE WITH '*'
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "13", pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm, "+++", pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd,            //Natural: COMPRESS '13' #WS-MM '+++' #WS-DD '+++' #WS-YY INTO #WS-REC-1 LEAVING NO SPACE
            "+++", pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy));
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace("0"));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH '0'
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1$AB"));                                                                                   //Natural: COMPRESS '1$AB' INTO #WS-REC-1 LEAVING NO SPACE
        //*  COMPRESS '1$XX' INTO #WS-REC-1 LEAVING NO SPACE
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        //* ******** OLD CODE ABOVE ***********************
        //*  OLD-CHECK-PRITNING-CODE /* RL
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
