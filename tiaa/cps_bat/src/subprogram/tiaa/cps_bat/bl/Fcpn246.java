/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:43 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn246
************************************************************
**        * FILE NAME            : Fcpn246.java
**        * CLASS NAME           : Fcpn246
**        * INSTANCE NAME        : Fcpn246
************************************************************
************************************************************************
* PROGRAM  : FCPN246
** 10/08/96   LIN ZHENG
**          - INFLATION LINKED BOND
** 03/18/98   GLORY PHILIP
**          - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
** 01/01/2000 LEON GURTOVNIK
**          - ADD PROCESSING FOR ELECTRONIC WARRANTS (EW)
** 01/05/2001 LEON G
**            ADD CHECK FOR  VALUES OF  'G' AND 'TG' IN
**            INV-ACCT-CDE  FIELD
**03/14/2002  R. CARREON
**            USE TX-ELCT-TRGGR TO TEST FOR NRA
**11/15/2002  R. CARREON
**            ADDED EGTRRA REQUIRMENTS.
**05/14/2008  AER - RESTOW FOR ROTH - ROTH-MAJOR1
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
*  6/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn246 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa200 pdaFcpa200;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcplpmnt ldaFcplpmnt;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Printed_Institution_Amt;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_1;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column;
    private DbsField pnd_Ws_Rec_2;

    private DbsGroup pnd_Ws_Rec_2__R_Field_2;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name;
    private DbsField pnd_Ws_Lines;
    private DbsField pnd_Ws_Side_2_Msg_Printed;
    private DbsField pnd_Ws_Side_3_Msg_Printed;
    private DbsField pnd_Ws_First_Time;
    private DbsField pnd_Ws_Lit;
    private DbsField pnd_Ws_From;
    private DbsField pnd_Ws_Amt;
    private DbsField pnd_Ws_Fonts;
    private DbsField pnd_In_Dex;
    private DbsField pnd_C;
    private DbsField pnd_F;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_N;
    private DbsField pnd_Ws_Prime_Key;

    private DbsGroup pnd_Ws_Prime_Key__R_Field_3;

    private DbsGroup pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_End_Prime_Key;

    private DbsGroup pnd_Ws_End_Prime_Key__R_Field_4;

    private DbsGroup pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2;
    private DbsField pnd_Ws_End_Prime_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_End_Prime_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_End_Prime_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_End_Prime_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_End_Prime_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Compare_Key;

    private DbsGroup pnd_Ws_Compare_Key__R_Field_5;

    private DbsGroup pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Inst_Tiaa;
    private DbsField pnd_Ws_Inst_Cref;
    private DbsField pnd_Ws_Amount_1;
    private DbsField pnd_Ws_Amount_2;
    private DbsField pnd_Ws_Exp_Amt;
    private DbsField pnd_Ws_Gra_Exp_Amt;
    private DbsField pnd_Ws_Letter;
    private DbsField pnd_Ws_Legend;
    private DbsField pnd_Ws_Literal;
    private DbsField pnd_Ws_Legend_Lump_Sum;
    private DbsField pnd_Ws_Legend_Gra;
    private DbsField pnd_Ws_Acct_Cde;
    private DbsField pnd_Ws_Good_Ivc;
    private DbsField pnd_Ws_Tiaa_Ivc;
    private DbsField pnd_Ws_Cref_Ivc;
    private DbsField pnd_First_Cref;
    private DbsField pnd_Ws_Accum_Hdr_Written;
    private DbsField pnd_Ws_Dpi_Hdr_Written;
    private DbsField pnd_Ws_Deduct_Hdr_Written;
    private DbsField pnd_Ws_Printed_Cref_Heading;
    private DbsField pnd_Ws_Printed_Tiaa_Heading;
    private DbsField pnd_Ws_Printed_Tiaa_Nbr;
    private DbsField pnd_Tiaa_Cnt;
    private DbsField pnd_Ws_Printed_Cref_Nbr;
    private DbsField pnd_Ws_Tot_Accum_Amt;
    private DbsField pnd_Ws_Tot_Payment_Amt;
    private DbsField pnd_Ws_Tot_Dpi_Amt;
    private DbsField pnd_Ws_Tot_Ded_Amt;
    private DbsField pnd_Ws_Tot_Check_Amt;
    private DbsField pnd_Ws_Cref_Tot_End_Accum;
    private DbsField pnd_Ws_Cref_Tot_Payment;
    private DbsField pnd_Ws_Cref_Tot_Dpi;
    private DbsField pnd_Ws_Cref_Tot_Ded;
    private DbsField pnd_Ws_Cref_Tot_Check;
    private DbsField pnd_Ws_Tiaa_Tot_End_Accum;
    private DbsField pnd_Ws_Tiaa_Tot_Payment;
    private DbsField pnd_Ws_Tiaa_Tot_Dpi;
    private DbsField pnd_Ws_Tiaa_Tot_Ded;
    private DbsField pnd_Ws_Tiaa_Tot_Check;
    private DbsField pnd_Ws_Grand_Tot_End_Accum;
    private DbsField pnd_Ws_Grand_Tot_Payment;
    private DbsField pnd_Ws_Grand_Tot_Dpi;
    private DbsField pnd_Ws_Grand_Tot_Ded;
    private DbsField pnd_Ws_Grand_Tot_Check;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());

        // parameters
        parameters = new DbsRecord();
        pdaFcpa200 = new PdaFcpa200(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Side_Printed = parameters.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Side_Printed.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Printed_Institution_Amt = parameters.newFieldInRecord("pnd_Ws_Printed_Institution_Amt", "#WS-PRINTED-INSTITUTION-AMT", FieldType.STRING, 
            1);
        pnd_Ws_Printed_Institution_Amt.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_1", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc = pnd_Ws_Rec_1__R_Field_1.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc", "#WS-REC1-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_1_Pnd_Ws_Column = pnd_Ws_Rec_1__R_Field_1.newFieldArrayInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column", "#WS-COLUMN", FieldType.STRING, 15, new 
            DbsArrayController(1, 7));
        pnd_Ws_Rec_2 = localVariables.newFieldInRecord("pnd_Ws_Rec_2", "#WS-REC-2", FieldType.STRING, 143);

        pnd_Ws_Rec_2__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Rec_2__R_Field_2", "REDEFINE", pnd_Ws_Rec_2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc", "#WS-REC2-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler", "#WS-REC2-FILLER", FieldType.STRING, 
            35);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name = pnd_Ws_Rec_2__R_Field_2.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name", "#WS-REC2-NAME", FieldType.STRING, 38);
        pnd_Ws_Lines = localVariables.newFieldArrayInRecord("pnd_Ws_Lines", "#WS-LINES", FieldType.STRING, 143, new DbsArrayController(1, 3));
        pnd_Ws_Side_2_Msg_Printed = localVariables.newFieldInRecord("pnd_Ws_Side_2_Msg_Printed", "#WS-SIDE-2-MSG-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Side_3_Msg_Printed = localVariables.newFieldInRecord("pnd_Ws_Side_3_Msg_Printed", "#WS-SIDE-3-MSG-PRINTED", FieldType.STRING, 1);
        pnd_Ws_First_Time = localVariables.newFieldInRecord("pnd_Ws_First_Time", "#WS-FIRST-TIME", FieldType.STRING, 1);
        pnd_Ws_Lit = localVariables.newFieldArrayInRecord("pnd_Ws_Lit", "#WS-LIT", FieldType.STRING, 4, new DbsArrayController(1, 2));
        pnd_Ws_From = localVariables.newFieldArrayInRecord("pnd_Ws_From", "#WS-FROM", FieldType.STRING, 4, new DbsArrayController(1, 2));
        pnd_Ws_Amt = localVariables.newFieldArrayInRecord("pnd_Ws_Amt", "#WS-AMT", FieldType.STRING, 15, new DbsArrayController(1, 2));
        pnd_Ws_Fonts = localVariables.newFieldInRecord("pnd_Ws_Fonts", "#WS-FONTS", FieldType.STRING, 3);
        pnd_In_Dex = localVariables.newFieldInRecord("pnd_In_Dex", "#IN-DEX", FieldType.INTEGER, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.INTEGER, 2);
        pnd_Ws_Prime_Key = localVariables.newFieldInRecord("pnd_Ws_Prime_Key", "#WS-PRIME-KEY", FieldType.STRING, 29);

        pnd_Ws_Prime_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Prime_Key__R_Field_3", "REDEFINE", pnd_Ws_Prime_Key);

        pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2 = pnd_Ws_Prime_Key__R_Field_3.newGroupInGroup("pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2", "#WS-PRIME-KEY-LEVEL2");
        pnd_Ws_Prime_Key_Cntrct_Ppcn_Nbr = pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Prime_Key_Cntrct_Invrse_Dte = pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Prime_Key_Cntrct_Orgn_Cde = pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_End_Prime_Key = localVariables.newFieldInRecord("pnd_Ws_End_Prime_Key", "#WS-END-PRIME-KEY", FieldType.STRING, 29);

        pnd_Ws_End_Prime_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_End_Prime_Key__R_Field_4", "REDEFINE", pnd_Ws_End_Prime_Key);

        pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2 = pnd_Ws_End_Prime_Key__R_Field_4.newGroupInGroup("pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2", 
            "#WS-END-PRIME-KEY-LEVEL2");
        pnd_Ws_End_Prime_Key_Cntrct_Ppcn_Nbr = pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2.newFieldInGroup("pnd_Ws_End_Prime_Key_Cntrct_Ppcn_Nbr", 
            "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Ws_End_Prime_Key_Cntrct_Invrse_Dte = pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2.newFieldInGroup("pnd_Ws_End_Prime_Key_Cntrct_Invrse_Dte", 
            "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ws_End_Prime_Key_Cntrct_Orgn_Cde = pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2.newFieldInGroup("pnd_Ws_End_Prime_Key_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_End_Prime_Key_Pymnt_Prcss_Seq_Num = pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2.newFieldInGroup("pnd_Ws_End_Prime_Key_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_End_Prime_Key_Pymnt_Instmt_Nbr = pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2.newFieldInGroup("pnd_Ws_End_Prime_Key_Pymnt_Instmt_Nbr", 
            "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Ws_Compare_Key = localVariables.newFieldInRecord("pnd_Ws_Compare_Key", "#WS-COMPARE-KEY", FieldType.STRING, 29);

        pnd_Ws_Compare_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Compare_Key__R_Field_5", "REDEFINE", pnd_Ws_Compare_Key);

        pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2 = pnd_Ws_Compare_Key__R_Field_5.newGroupInGroup("pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2", "#WS-COMPARE-KEY-LEVEL2");
        pnd_Ws_Compare_Key_Cntrct_Ppcn_Nbr = pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Compare_Key_Cntrct_Invrse_Dte = pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Compare_Key_Cntrct_Orgn_Cde = pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Num = pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr = pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Inst_Tiaa = localVariables.newFieldInRecord("pnd_Ws_Inst_Tiaa", "#WS-INST-TIAA", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Inst_Cref = localVariables.newFieldInRecord("pnd_Ws_Inst_Cref", "#WS-INST-CREF", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Amount_1 = localVariables.newFieldInRecord("pnd_Ws_Amount_1", "#WS-AMOUNT-1", FieldType.STRING, 15);
        pnd_Ws_Amount_2 = localVariables.newFieldInRecord("pnd_Ws_Amount_2", "#WS-AMOUNT-2", FieldType.STRING, 15);
        pnd_Ws_Exp_Amt = localVariables.newFieldInRecord("pnd_Ws_Exp_Amt", "#WS-EXP-AMT", FieldType.STRING, 15);
        pnd_Ws_Gra_Exp_Amt = localVariables.newFieldInRecord("pnd_Ws_Gra_Exp_Amt", "#WS-GRA-EXP-AMT", FieldType.STRING, 15);
        pnd_Ws_Letter = localVariables.newFieldInRecord("pnd_Ws_Letter", "#WS-LETTER", FieldType.STRING, 1);
        pnd_Ws_Legend = localVariables.newFieldInRecord("pnd_Ws_Legend", "#WS-LEGEND", FieldType.STRING, 4);
        pnd_Ws_Literal = localVariables.newFieldInRecord("pnd_Ws_Literal", "#WS-LITERAL", FieldType.STRING, 11);
        pnd_Ws_Legend_Lump_Sum = localVariables.newFieldInRecord("pnd_Ws_Legend_Lump_Sum", "#WS-LEGEND-LUMP-SUM", FieldType.STRING, 1);
        pnd_Ws_Legend_Gra = localVariables.newFieldInRecord("pnd_Ws_Legend_Gra", "#WS-LEGEND-GRA", FieldType.STRING, 1);
        pnd_Ws_Acct_Cde = localVariables.newFieldInRecord("pnd_Ws_Acct_Cde", "#WS-ACCT-CDE", FieldType.STRING, 2);
        pnd_Ws_Good_Ivc = localVariables.newFieldInRecord("pnd_Ws_Good_Ivc", "#WS-GOOD-IVC", FieldType.STRING, 1);
        pnd_Ws_Tiaa_Ivc = localVariables.newFieldInRecord("pnd_Ws_Tiaa_Ivc", "#WS-TIAA-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Ivc = localVariables.newFieldInRecord("pnd_Ws_Cref_Ivc", "#WS-CREF-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_First_Cref = localVariables.newFieldInRecord("pnd_First_Cref", "#FIRST-CREF", FieldType.BOOLEAN, 1);
        pnd_Ws_Accum_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Accum_Hdr_Written", "#WS-ACCUM-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Dpi_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Dpi_Hdr_Written", "#WS-DPI-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Deduct_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Deduct_Hdr_Written", "#WS-DEDUCT-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Printed_Cref_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Cref_Heading", "#WS-PRINTED-CREF-HEADING", FieldType.STRING, 1);
        pnd_Ws_Printed_Tiaa_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Tiaa_Heading", "#WS-PRINTED-TIAA-HEADING", FieldType.STRING, 1);
        pnd_Ws_Printed_Tiaa_Nbr = localVariables.newFieldInRecord("pnd_Ws_Printed_Tiaa_Nbr", "#WS-PRINTED-TIAA-NBR", FieldType.STRING, 1);
        pnd_Tiaa_Cnt = localVariables.newFieldInRecord("pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 1);
        pnd_Ws_Printed_Cref_Nbr = localVariables.newFieldInRecord("pnd_Ws_Printed_Cref_Nbr", "#WS-PRINTED-CREF-NBR", FieldType.STRING, 1);
        pnd_Ws_Tot_Accum_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Accum_Amt", "#WS-TOT-ACCUM-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Payment_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Payment_Amt", "#WS-TOT-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Dpi_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Dpi_Amt", "#WS-TOT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Ded_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Ded_Amt", "#WS-TOT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Check_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Check_Amt", "#WS-TOT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_End_Accum", "#WS-CREF-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Ws_Cref_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Payment", "#WS-CREF-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Dpi", "#WS-CREF-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Ded", "#WS-CREF-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Cref_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Cref_Tot_Check", "#WS-CREF-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tiaa_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Tiaa_Tot_End_Accum", "#WS-TIAA-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Ws_Tiaa_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Tiaa_Tot_Payment", "#WS-TIAA-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tiaa_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Tiaa_Tot_Dpi", "#WS-TIAA-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tiaa_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Tiaa_Tot_Ded", "#WS-TIAA-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tiaa_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Tiaa_Tot_Check", "#WS-TIAA-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_End_Accum", "#WS-GRAND-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Grand_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Payment", "#WS-GRAND-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Dpi", "#WS-GRAND-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Ded", "#WS-GRAND-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Check", "#WS-GRAND-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmnt.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Ws_Side_2_Msg_Printed.setInitialValue("N");
        pnd_Ws_Side_3_Msg_Printed.setInitialValue("N");
        pnd_Ws_First_Time.setInitialValue("Y");
        pnd_Ws_Lit.getValue(1).setInitialValue(" ");
        pnd_Ws_From.getValue(1).setInitialValue(" ");
        pnd_Ws_Amt.getValue(1).setInitialValue(" ");
        pnd_Ws_Fonts.setInitialValue(" ");
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_N.setInitialValue(0);
        pnd_Ws_Inst_Tiaa.setInitialValue(0);
        pnd_Ws_Inst_Cref.setInitialValue(0);
        pnd_Ws_Legend_Lump_Sum.setInitialValue("N");
        pnd_Ws_Legend_Gra.setInitialValue("N");
        pnd_Ws_Good_Ivc.setInitialValue("N");
        pnd_Ws_Tiaa_Ivc.setInitialValue(0);
        pnd_Ws_Cref_Ivc.setInitialValue(0);
        pnd_Ws_Accum_Hdr_Written.setInitialValue("N");
        pnd_Ws_Dpi_Hdr_Written.setInitialValue("N");
        pnd_Ws_Deduct_Hdr_Written.setInitialValue("N");
        pnd_Ws_Printed_Cref_Heading.setInitialValue("N");
        pnd_Ws_Printed_Tiaa_Heading.setInitialValue("N");
        pnd_Ws_Printed_Tiaa_Nbr.setInitialValue("N");
        pnd_Ws_Printed_Cref_Nbr.setInitialValue("N");
        pnd_Ws_Tot_Accum_Amt.setInitialValue(0);
        pnd_Ws_Tot_Payment_Amt.setInitialValue(0);
        pnd_Ws_Tot_Dpi_Amt.setInitialValue(0);
        pnd_Ws_Tot_Ded_Amt.setInitialValue(0);
        pnd_Ws_Tot_Check_Amt.setInitialValue(0);
        pnd_Ws_Cref_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Cref_Tot_Payment.setInitialValue(0);
        pnd_Ws_Cref_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Cref_Tot_Ded.setInitialValue(0);
        pnd_Ws_Cref_Tot_Check.setInitialValue(0);
        pnd_Ws_Tiaa_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Tiaa_Tot_Payment.setInitialValue(0);
        pnd_Ws_Tiaa_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Tiaa_Tot_Ded.setInitialValue(0);
        pnd_Ws_Tiaa_Tot_Check.setInitialValue(0);
        pnd_Ws_Grand_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Grand_Tot_Payment.setInitialValue(0);
        pnd_Ws_Grand_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Grand_Tot_Ded.setInitialValue(0);
        pnd_Ws_Grand_Tot_Check.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn246() throws Exception
    {
        super("Fcpn246");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        //* *IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DS'
        pnd_Ws_Accum_Hdr_Written.setValue("Y");                                                                                                                           //Natural: ASSIGN #WS-ACCUM-HDR-WRITTEN = 'Y'
        //* *END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #J 1 #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J).greater(getZero())))                                                             //Natural: IF #WS-OCCURS.INV-ACCT-DPI-AMT ( #J ) > 0
            {
                pnd_Ws_Dpi_Hdr_Written.setValue("Y");                                                                                                                     //Natural: ASSIGN #WS-DPI-HDR-WRITTEN = 'Y'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J).greater(getZero()) || pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J).greater(getZero())  //Natural: IF #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #J ) > 0 OR #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #J ) > 0 OR #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
                || pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J).greater(getZero())))
            {
                pnd_Ws_Deduct_Hdr_Written.setValue("Y");                                                                                                                  //Natural: ASSIGN #WS-DEDUCT-HDR-WRITTEN = 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #J 1 #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            FOR03:                                                                                                                                                        //Natural: FOR #K 1 #CNTR-DEDUCTIONS ( #J )
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_J))); pnd_K.nadd(1))
            {
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,pnd_K).greater(getZero())))                                                      //Natural: IF #WS-OCCURS.PYMNT-DED-AMT ( #J,#K ) > 0
                {
                    pnd_Ws_Deduct_Hdr_Written.setValue("Y");                                                                                                              //Natural: ASSIGN #WS-DEDUCT-HDR-WRITTEN = 'Y'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #J 1 #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).notEquals(pnd_Ws_Side_Printed)))                                                      //Natural: IF #WS-SIDE ( #J ) NE #WS-SIDE-PRINTED
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).equals("2") && pnd_Ws_Side_2_Msg_Printed.equals("N")))                            //Natural: IF #WS-SIDE ( #J ) = '2' AND #WS-SIDE-2-MSG-PRINTED = 'N'
                {
                    pnd_Ws_Side_2_Msg_Printed.setValue("Y");                                                                                                              //Natural: ASSIGN #WS-SIDE-2-MSG-PRINTED = 'Y'
                    pnd_Ws_Rec_1.setValue("15CONTINUED FROM PREVIOUS PAGE");                                                                                              //Natural: ASSIGN #WS-REC-1 = '15CONTINUED FROM PREVIOUS PAGE'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  JFT - TO FORMAT MULTIPLEX PAGE
                //*        CREATE HEADER FOR MULTIPLEX PAGE
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).equals("3") && pnd_Ws_Side_3_Msg_Printed.equals("N")))                            //Natural: IF #WS-SIDE ( #J ) = '3' AND #WS-SIDE-3-MSG-PRINTED = 'N'
                {
                    pnd_Ws_Side_3_Msg_Printed.setValue("Y");                                                                                                              //Natural: ASSIGN #WS-SIDE-3-MSG-PRINTED := 'Y'
                    DbsUtil.callnat(Fcpn236a.class , getCurrentProcessState(), pdaFcpa200.getPnd_Ws_Header_Record().getValue("*"), pdaFcpa200.getPnd_Ws_Occurs().getValue("*"),  //Natural: CALLNAT 'FCPN236A' #WS-HEADER-RECORD ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
                        pdaFcpa200.getPnd_Ws_Name_N_Address().getValue("*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    pnd_Ws_Printed_Cref_Heading.resetInitial();                                                                                                           //Natural: RESET INITIAL #WS-PRINTED-CREF-HEADING #WS-PRINTED-TIAA-HEADING #WS-PRINTED-TIAA-NBR #WS-PRINTED-CREF-NBR #WS-FIRST-TIME
                    pnd_Ws_Printed_Tiaa_Heading.resetInitial();
                    pnd_Ws_Printed_Tiaa_Nbr.resetInitial();
                    pnd_Ws_Printed_Cref_Nbr.resetInitial();
                    pnd_Ws_First_Time.resetInitial();
                    //*  LZ
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF JFT CODE
                //* *  ASSIGN #PDA-INV-ACCT = INV-ACCT-CDE (#J)
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J));                                    //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := #WS-OCCURS.INV-ACCT-CDE ( #J )
                //* *  CALLNAT 'FCPN199' #PDA-INV-ACCT #PDA-INV-ACCT-INFO     /* LZ
                //*  LZ
                DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                pnd_K.reset();                                                                                                                                            //Natural: RESET #K
                //*  LZ
                //* *    WHEN #PDA-INV-ACCT-COMPANY   =  'T'                                                                                                              //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet699 = 0;                                                                                                                         //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'T' AND #WS-PRINTED-TIAA-HEADING = 'N'
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T") && pnd_Ws_Printed_Tiaa_Heading.equals("N")))
                {
                    decideConditionsMet699++;
                    pnd_Ws_Rec_1.setValue("14           TIAA");                                                                                                           //Natural: ASSIGN #WS-REC-1 = '14           TIAA'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4           Contract");                                                                                                       //Natural: ASSIGN #WS-REC-1 = ' 4           Contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  LZ
                    pnd_Ws_Printed_Tiaa_Heading.setValue("Y");                                                                                                            //Natural: ASSIGN #WS-PRINTED-TIAA-HEADING = 'Y'
                    //* *    WHEN #PDA-INV-ACCT-COMPANY = 'C'
                }                                                                                                                                                         //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'C' AND #WS-PRINTED-CREF-HEADING = 'N'
                else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C") && pnd_Ws_Printed_Cref_Heading.equals("N")))
                {
                    decideConditionsMet699++;
                    if (condition(pnd_Ws_Printed_Tiaa_Heading.equals("Y")))                                                                                               //Natural: IF #WS-PRINTED-TIAA-HEADING = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("-4           CREF");                                                                                                       //Natural: ASSIGN #WS-REC-1 = '-4           CREF'
                        //*  TO IDENTIFY FIRST CREF CONTRACT
                        pnd_First_Cref.setValue(true);                                                                                                                    //Natural: ASSIGN #FIRST-CREF = TRUE
                        //*  AFTER TIAA CONTRACT(S) - JFT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1.setValue("14           CREF");                                                                                                       //Natural: ASSIGN #WS-REC-1 = '14           CREF'
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4           Certificate");                                                                                                    //Natural: ASSIGN #WS-REC-1 = ' 4           Certificate'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_J).equals("3")))                                                                 //Natural: IF #WS-SIDE ( #J ) = '3'
                    {
                        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Printed_Cref_Heading.setValue("Y");                                                                                                            //Natural: ASSIGN #WS-PRINTED-CREF-HEADING = 'Y'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                             //Natural: IF #WS-FIRST-TIME = 'Y'
                {
                    pnd_Ws_First_Time.setValue("N");                                                                                                                      //Natural: ASSIGN #WS-FIRST-TIME = 'N'
                    //*  LEON
                    if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))                                                                     //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE EQ 'EW'
                    {
                        pnd_Ws_Rec_1.setValue("14             ");                                                                                                         //Natural: ASSIGN #WS-REC-1 = '14             '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4                ");                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4                '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  LEON
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1.setValue("14    Remaining");                                                                                                         //Natural: ASSIGN #WS-REC-1 = '14    Remaining'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4    Accumulation");                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4    Accumulation'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  LEON
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue("14          Payment");                                                                                                         //Natural: ASSIGN #WS-REC-1 = '14          Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      ASSIGN #WS-REC-1    = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                    //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("14            Interest");                                                                                                  //Natural: ASSIGN #WS-REC-1 = '14            Interest'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*        ASSIGN #WS-REC-1    = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                 //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("14    Deductions");                                                                                                        //Natural: ASSIGN #WS-REC-1 = '14    Deductions'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*        ASSIGN #WS-REC-1    = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Rec_1.setValue("14          Net");                                                                                                             //Natural: ASSIGN #WS-REC-1 = '14          Net'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4          Payment");                                                                                                         //Natural: ASSIGN #WS-REC-1 = ' 4          Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Rec_1.setValue(" 4");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Dpi_Hdr_Written.equals("N")))                                                                                                    //Natural: IF #WS-DPI-HDR-WRITTEN = 'N'
                    {
                        pnd_Ws_Rec_1.setValue("14");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Deduct_Hdr_Written.equals("N")))                                                                                                 //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'N'
                    {
                        pnd_Ws_Rec_1.setValue("14");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                        sub_Write_Recs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Rec_1.setValue("15");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Tot_Accum_Amt.reset();                                                                                                                             //Natural: RESET #WS-TOT-ACCUM-AMT #WS-TOT-PAYMENT-AMT #WS-TOT-DPI-AMT #WS-TOT-DED-AMT #WS-TOT-CHECK-AMT
                pnd_Ws_Tot_Payment_Amt.reset();
                pnd_Ws_Tot_Dpi_Amt.reset();
                pnd_Ws_Tot_Ded_Amt.reset();
                pnd_Ws_Tot_Check_Amt.reset();
                //* *  IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DS'
                pnd_Ws_Tot_Accum_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J));                                                          //Natural: ADD #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) TO #WS-TOT-ACCUM-AMT
                //* *  END-IF
                pnd_Ws_Tot_Payment_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Payment_Amt), pnd_Ws_Tot_Payment_Amt.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J))); //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT ( #J ) #WS-OCCURS.INV-ACCT-DVDND-AMT ( #J ) TO #WS-TOT-PAYMENT-AMT
                pnd_Ws_Tot_Dpi_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J));                                                                  //Natural: ADD #WS-OCCURS.INV-ACCT-DPI-AMT ( #J ) TO #WS-TOT-DPI-AMT
                pnd_Ws_Tot_Ded_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Ded_Amt), pnd_Ws_Tot_Ded_Amt.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J))); //Natural: ADD #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #J ) #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
                pnd_Ws_Tot_Ded_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J));                                                            //Natural: ADD #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
                pnd_Ws_Tot_Ded_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,"*"));                                                                 //Natural: ADD #WS-OCCURS.PYMNT-DED-AMT ( #J,* ) TO #WS-TOT-DED-AMT
                pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                          //Natural: ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO #WS-TOT-CHECK-AMT
                //* *  IF #PDA-INV-ACCT-COMPANY = 'C'                         /* LZ
                //*  LZ
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C")))                                                                                 //Natural: IF #FUND-PDA.#COMPANY-CDE = 'C'
                {
                    pnd_Ws_Cref_Tot_Payment.nadd(pnd_Ws_Tot_Payment_Amt);                                                                                                 //Natural: ADD #WS-TOT-PAYMENT-AMT TO #WS-CREF-TOT-PAYMENT
                    pnd_Ws_Cref_Tot_Dpi.nadd(pnd_Ws_Tot_Dpi_Amt);                                                                                                         //Natural: ADD #WS-TOT-DPI-AMT TO #WS-CREF-TOT-DPI
                    pnd_Ws_Cref_Tot_Ded.nadd(pnd_Ws_Tot_Ded_Amt);                                                                                                         //Natural: ADD #WS-TOT-DED-AMT TO #WS-CREF-TOT-DED
                    pnd_Ws_Cref_Tot_Check.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J));                                                     //Natural: ADD #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #J ) TO #WS-CREF-TOT-CHECK
                    //* *    IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DS'
                    pnd_Ws_Cref_Tot_End_Accum.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J));                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) TO #WS-CREF-TOT-END-ACCUM
                    //* *    END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Tiaa_Tot_Payment.nadd(pnd_Ws_Tot_Payment_Amt);                                                                                                 //Natural: ADD #WS-TOT-PAYMENT-AMT TO #WS-TIAA-TOT-PAYMENT
                    pnd_Ws_Tiaa_Tot_Dpi.nadd(pnd_Ws_Tot_Dpi_Amt);                                                                                                         //Natural: ADD #WS-TOT-DPI-AMT TO #WS-TIAA-TOT-DPI
                    pnd_Ws_Tiaa_Tot_Ded.nadd(pnd_Ws_Tot_Ded_Amt);                                                                                                         //Natural: ADD #WS-TOT-DED-AMT TO #WS-TIAA-TOT-DED
                    pnd_Ws_Tiaa_Tot_Check.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J));                                                     //Natural: ADD #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #J ) TO #WS-TIAA-TOT-CHECK
                    pnd_Ws_Tiaa_Tot_End_Accum.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J));                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) TO #WS-TIAA-TOT-END-ACCUM
                }                                                                                                                                                         //Natural: END-IF
                //* *  IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DS'
                pnd_Ws_Grand_Tot_End_Accum.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J));                                                    //Natural: ADD #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) TO #WS-GRAND-TOT-END-ACCUM
                //* *  END-IF
                pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Tot_Payment_Amt);                                                                                                    //Natural: ADD #WS-TOT-PAYMENT-AMT TO #WS-GRAND-TOT-PAYMENT
                pnd_Ws_Grand_Tot_Dpi.nadd(pnd_Ws_Tot_Dpi_Amt);                                                                                                            //Natural: ADD #WS-TOT-DPI-AMT TO #WS-GRAND-TOT-DPI
                pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Tot_Ded_Amt);                                                                                                            //Natural: ADD #WS-TOT-DED-AMT TO #WS-GRAND-TOT-DED
                pnd_Ws_Grand_Tot_Check.nadd(pnd_Ws_Tot_Check_Amt);                                                                                                        //Natural: ADD #WS-TOT-CHECK-AMT TO #WS-GRAND-TOT-CHECK
                pnd_F.reset();                                                                                                                                            //Natural: RESET #F #WS-REC-1
                pnd_Ws_Rec_1.reset();
                //*  BY JFT TO AVOID DOUBLE SPACE AFTER
                if (condition(pnd_First_Cref.getBoolean()))                                                                                                               //Natural: IF #FIRST-CREF
                {
                    //*  CREF HEADER
                    pnd_First_Cref.reset();                                                                                                                               //Natural: RESET #FIRST-CREF
                    pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                           //Natural: ASSIGN #WS-REC1-CC = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                           //Natural: ASSIGN #WS-REC1-CC = '05'
                }                                                                                                                                                         //Natural: END-IF
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                if (condition(pnd_Ws_Printed_Cref_Heading.equals("Y")))                                                                                                   //Natural: IF #WS-PRINTED-CREF-HEADING = 'Y'
                {
                    if (condition(pnd_Ws_Printed_Cref_Nbr.equals("N")))                                                                                                   //Natural: IF #WS-PRINTED-CREF-NBR = 'N'
                    {
                        pnd_Ws_Printed_Cref_Nbr.setValue("Y");                                                                                                            //Natural: ASSIGN #WS-PRINTED-CREF-NBR = 'Y'
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Cref_Nbr(),new ReportEditMask("XXXXXXX-X"));  //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-CREF-NBR ( EM = XXXXXXX-X ) TO #WS-COLUMN ( #F )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Printed_Cref_Heading.equals("N")))                                                                                                   //Natural: IF #WS-PRINTED-CREF-HEADING = 'N'
                {
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr().getValue(pnd_J),new ReportEditMask("XXXXXXX-X")); //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-COLUMN ( #F )
                    //* *--> INSERTED 09-14-95 BY FRANK
                    //* *--> TEST FOR PDA-INV-ACCT OF 'R'; IF THERE ARE TRADITIONAL
                    //* *-->  AND REAL ESTATE, WE PRINT ONLY 1 PPCN.
                    pnd_Tiaa_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TIAA-CNT
                    //* *    IF #PDA-INV-ACCT = 'R' AND #TIAA-CNT GT 1              /* LZ
                    //*  LZ
                    if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("R") && pnd_Tiaa_Cnt.greater(1)))                                               //Natural: IF #FUND-PDA.#INV-ACCT-ALPHA = 'R' AND #TIAA-CNT GT 1
                    {
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := ' '
                        pnd_Tiaa_Cnt.reset();                                                                                                                             //Natural: RESET #TIAA-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* *-----                                                       /*  LEON
                if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))                                                                         //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE EQ 'EW'
                {
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                             //Natural: MOVE ' ' TO #WS-COLUMN ( #F )
                    //*   LEON
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* *-----
                    if (condition(((((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ANNT") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ALT1"))  //Natural: IF #WS-HEADER-RECORD.CNTRCT-PAYEE-CDE = 'ANNT' OR = 'ALT1' OR = 'ALT2' AND #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'R' AND #WS-HEADER-RECORD.PYMNT-INST-REP-CDE = 'Y'
                        || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ALT2")) && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("R")) 
                        && pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde().equals("Y"))))
                    {
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J),new                //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99'@' ) TO #WS-COLUMN ( #F )
                            ReportEditMask("X'$'Z,ZZZ,ZZ9.99'@'"));
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J),new                //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                            ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  LEON
                }                                                                                                                                                         //Natural: END-IF
                //* *
                //*  INSERTED 04-12-94 BY FRANK
                //*  TESTED FOR GRA REPURCHASES TO DISPLAY LEGEND
                //* *  IF CNTRCT-TYPE-CDE = 'R' AND #PDA-INV-ACCT-COMPANY = 'T'  /* LZ
                //*  LZ
                if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("R") && pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")               //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'R' AND #FUND-PDA.#COMPANY-CDE = 'T' AND #WS-HEADER-RECORD.CNTRCT-PPCN-NBR = MASK ( '2' ) AND #WS-OCCURS.INV-ACCT-EXP-AMT ( #J ) > 0
                    && DbsUtil.maskMatches(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(),"'2'") && pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Exp_Amt().getValue(pnd_J).greater(getZero())))
                {
                    pnd_Ws_Legend_Gra.setValue("Y");                                                                                                                      //Natural: ASSIGN #WS-LEGEND-GRA = 'Y'
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Ws_Gra_Exp_Amt.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Exp_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));             //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-EXP-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-GRA-EXP-AMT
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Gra_Exp_Amt,true), new ExamineSearch("X", true), new ExamineDelete());                                       //Natural: EXAMINE FULL #WS-GRA-EXP-AMT FOR FULL 'X' DELETE
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'!'"));                          //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99'!' ) TO #WS-COLUMN ( #F )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  LZ
                    if (condition((((pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("GRA") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("GRAD"))  //Natural: IF ( #WS-HEADER-RECORD.CNTRCT-LOB-CDE = 'GRA' OR = 'GRAD' ) AND #FUND-PDA.#COMPANY-CDE = 'T' AND #WS-OCCURS.INV-ACCT-EXP-AMT ( #J ) > 0
                        && pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")) && pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Exp_Amt().getValue(pnd_J).greater(getZero()))))
                    {
                        //* *        AND #PDA-INV-ACCT-COMPANY = 'T'
                        //*  ADDED BY JFT TO RESOLVE
                        //*  0.00 EXPENSE CHARGE BEING
                        //*  PRINTED ON THE STATEMENT.
                        pnd_Ws_Legend_Lump_Sum.setValue("Y");                                                                                                             //Natural: ASSIGN #WS-LEGEND-LUMP-SUM = 'Y'
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Exp_Amt.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Exp_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));             //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-EXP-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-EXP-AMT
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Exp_Amt,true), new ExamineSearch("X", true), new ExamineDelete());                                       //Natural: EXAMINE FULL #WS-EXP-AMT FOR FULL 'X' DELETE
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'@'"));                      //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99'@' ) TO #WS-COLUMN ( #F )
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                      //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                 //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
                if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                        //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
                {
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'")); //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-DPI-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                     //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
                {
                    if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                 //Natural: IF #WS-TOT-DED-AMT > 0
                    {
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZZ,ZZ9.99'****'"));                         //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZZ,ZZ9.99'****' ) TO #WS-COLUMN ( #F )
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_F.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #F
                        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'")); //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                   //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("*", true), new ExamineReplace(" "));                 //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '*' REPLACE ' '
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("@", true), new ExamineReplace("*"));                 //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '@' REPLACE '*'
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("!", true), new ExamineReplace("@"));                 //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '!' REPLACE '@'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR FEDERAL TAX
                pnd_F.setValue(1);                                                                                                                                        //Natural: ASSIGN #F = 1
                pnd_Ws_Rec_1.reset();                                                                                                                                     //Natural: RESET #WS-REC-1
                //*  LZ
                //* *    WHEN #PDA-INV-ACCT-COMPANY = 'T'                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet969 = 0;                                                                                                                         //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'T'
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))
                {
                    decideConditionsMet969++;
                    //* *      IF #PDA-INV-ACCT = 'R'                             /* LZ
                    //*  LZ
                    if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("R")))                                                                          //Natural: IF #FUND-PDA.#INV-ACCT-ALPHA = 'R'
                    {
                        //* *        COMPRESS '*5******' #PDA-INV-ACCT-FUND-DESC1 INTO #WS-REC-1
                        pnd_Ws_Rec_1.setValue(DbsUtil.compress("*5******", pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1()));                                              //Natural: COMPRESS '*5******' #FUND-PDA.#STMNT-LINE-1 INTO #WS-REC-1
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                //Natural: EXAMINE #WS-REC-1 FOR '*' REPLACE ' '
                        pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Unit_Qty().getValue(pnd_J),new ReportEditMask("ZZZZZZZ9.999'*'"));            //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-UNIT-QTY ( #J ) ( EM = ZZZZZZZ9.999'*' ) TO #WS-AMOUNT-1
                        pnd_Ws_Literal.setValue("+Units");                                                                                                                //Natural: ASSIGN #WS-LITERAL = '+Units'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1.setValue(" 5       Traditional");                                                                                                    //Natural: ASSIGN #WS-REC-1 = ' 5       Traditional'
                        pnd_Ws_Amount_1.reset();                                                                                                                          //Natural: RESET #WS-AMOUNT-1 #WS-LITERAL
                        pnd_Ws_Literal.reset();
                    }                                                                                                                                                     //Natural: END-IF
                    //*  LZ
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *    WHEN #PDA-INV-ACCT-COMPANY = 'C'
                }                                                                                                                                                         //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'C'
                else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C")))
                {
                    decideConditionsMet969++;
                    //* *      COMPRESS '*5******' #PDA-INV-ACCT-FUND-DESC1      /* LZ
                    //*  LZ
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress("*5******", pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1()));                                                  //Natural: COMPRESS '*5******' #FUND-PDA.#STMNT-LINE-1 INTO #WS-REC-1
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                    //Natural: EXAMINE #WS-REC-1 FOR '*' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Unit_Qty().getValue(pnd_J),new ReportEditMask("ZZZZZZZ9.999'*'"));                //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-UNIT-QTY ( #J ) ( EM = ZZZZZZZ9.999'*' ) TO #WS-AMOUNT-1
                    pnd_Ws_Literal.setValue("+Units");                                                                                                                    //Natural: ASSIGN #WS-LITERAL = '+Units'
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet969 > 0))
                {
                    pnd_Ws_Amount_2.reset();                                                                                                                              //Natural: RESET #WS-AMOUNT-2 #WS-LEGEND
                    pnd_Ws_Legend.reset();
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                //Natural: IF #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #J ) > 0
                    {
                        pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J),new ReportEditMask("ZZZZ,ZZ9.99"));            //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #J ) ( EM = ZZZZ,ZZ9.99 ) TO #WS-AMOUNT-2
                                                                                                                                                                          //Natural: PERFORM FORMAT-FEDERAL-LEGEND
                        sub_Format_Federal_Legend();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                    sub_Format_Deductions();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR STATE TAX
                pnd_F.setValue(1);                                                                                                                                        //Natural: ASSIGN #F = 1
                pnd_Ws_Rec_1.reset();                                                                                                                                     //Natural: RESET #WS-REC-1
                //*  LZ
                //* *    WHEN #PDA-INV-ACCT-COMPANY = 'T'                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet1012 = 0;                                                                                                                        //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'T'
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))
                {
                    decideConditionsMet1012++;
                    //* *      IF #PDA-INV-ACCT = 'R'                            /* LZ
                    //*  LZ
                    if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("R")))                                                                          //Natural: IF #FUND-PDA.#INV-ACCT-ALPHA = 'R'
                    {
                        //* *        COMPRESS '*5******' #PDA-INV-ACCT-FUND-DESC2    /* LZ
                        //*  LZ
                        pnd_Ws_Rec_1.setValue(DbsUtil.compress("*5******", pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2()));                                              //Natural: COMPRESS '*5******' #FUND-PDA.#STMNT-LINE-2 INTO #WS-REC-1
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                //Natural: EXAMINE #WS-REC-1 FOR '*' REPLACE ' '
                        pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Unit_Value().getValue(pnd_J),new ReportEditMask("ZZZZZZ9.9999'*'"));          //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #J ) ( EM = ZZZZZZ9.9999'*' ) TO #WS-AMOUNT-1
                        pnd_Ws_Literal.setValue("+Unit Value");                                                                                                           //Natural: ASSIGN #WS-LITERAL = '+Unit Value'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1.setValue(" 5");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 5'
                        //*          ASSIGN #WS-REC-1     = ' 5       Traditional'
                        pnd_Ws_Amount_1.reset();                                                                                                                          //Natural: RESET #WS-AMOUNT-1 #WS-LITERAL
                        pnd_Ws_Literal.reset();
                    }                                                                                                                                                     //Natural: END-IF
                    //*  LZ
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *    WHEN #PDA-INV-ACCT-COMPANY = 'C'
                }                                                                                                                                                         //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'C'
                else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C")))
                {
                    decideConditionsMet1012++;
                    //* *      COMPRESS '*5******' #PDA-INV-ACCT-FUND-DESC2      /* LZ
                    //*  LZ
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress("*5******", pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2()));                                                  //Natural: COMPRESS '*5******' #FUND-PDA.#STMNT-LINE-2 INTO #WS-REC-1
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                    //Natural: EXAMINE #WS-REC-1 FOR '*' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Amount_1.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Unit_Value().getValue(pnd_J),new ReportEditMask("ZZZZZZ9.9999'*'"));              //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #J ) ( EM = ZZZZZZ9.9999'*' ) TO #WS-AMOUNT-1
                    pnd_Ws_Literal.setValue("+Unit Value");                                                                                                               //Natural: ASSIGN #WS-LITERAL = '+Unit Value'
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet1012 > 0))
                {
                    pnd_Ws_Amount_2.reset();                                                                                                                              //Natural: RESET #WS-AMOUNT-2 #WS-LEGEND
                    pnd_Ws_Legend.reset();
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J).greater(getZero())))                                               //Natural: IF #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #J ) > 0
                    {
                        pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J),new ReportEditMask("ZZZZ,ZZ9.99"));           //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #J ) ( EM = ZZZZ,ZZ9.99 ) TO #WS-AMOUNT-2
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEGEND
                        sub_Format_State_Legend();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                    sub_Format_Deductions();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  INSERTED BY JFT TO PRINT TIAA SUB TOTALS
                //* *  IF #PDA-INV-ACCT-COMPANY = 'T'                        /* LZ
                //*  LZ
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                                 //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
                {
                    //*  LEON 01/05/01
                    if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J.getDec().add(1)).equals("R") || pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J.getDec().add(1)).equals("T")  //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( #J +1 ) = 'R' OR = 'T' OR = 'G' OR = 'TG'
                        || pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J.getDec().add(1)).equals("G") || pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J.getDec().add(1)).equals("TG")))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM FORMAT-TIAA-TOTALS
                        sub_Format_Tiaa_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR LOCAL TAX
                if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J).greater(getZero())))                                                   //Natural: IF #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
                {
                    pnd_F.setValue(1);                                                                                                                                    //Natural: ASSIGN #F = 1
                    //*      RESET #WS-REC-1
                    pnd_Ws_Rec_1.setValue(" 5");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Amount_1.reset();                                                                                                                              //Natural: RESET #WS-AMOUNT-1 #WS-LITERAL
                    pnd_Ws_Literal.reset();
                    pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J),new ReportEditMask("ZZZZ,ZZ9.99"));               //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #J ) ( EM = ZZZZ,ZZ9.99 ) TO #WS-AMOUNT-2
                    pnd_Ws_Legend.setValue("(L)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(L)*'
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                    sub_Format_Deductions();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  THE FOLLOWING CODE FOR PART-D OF SPECS FOR DEDUCTIONS
                pnd_L.reset();                                                                                                                                            //Natural: RESET #L
                FOR05:                                                                                                                                                    //Natural: FOR #C = 1 TO #CNTR-DEDUCTIONS ( #J )
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pdaFcpa200.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_J))); pnd_C.nadd(1))
                {
                    pnd_F.setValue(1);                                                                                                                                    //Natural: ASSIGN #F = 1
                    pnd_L.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #L
                    //*      RESET #WS-REC-1
                    pnd_Ws_Rec_1.setValue(" 5");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Amount_1.setValue(" ");                                                                                                                        //Natural: ASSIGN #WS-AMOUNT-1 = ' '
                    pnd_Ws_Literal.setValue(" ");                                                                                                                         //Natural: ASSIGN #WS-LITERAL = ' '
                    pnd_Ws_Amount_2.setValueEdited(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,pnd_C),new ReportEditMask("ZZZZZZ9.99"));                   //Natural: MOVE EDITED #WS-OCCURS.PYMNT-DED-AMT ( #J,#C ) ( EM = ZZZZZZ9.99 ) TO #WS-AMOUNT-2
                    //*      COMPRESS  '('  PYMNT-DED-CDE (#J,#C)  ')*'
                    pnd_Ws_Legend.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_L, ")*"));                                                            //Natural: COMPRESS '(' #L ')*' INTO #WS-LEGEND LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM FORMAT-DEDUCTIONS
                    sub_Format_Deductions();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        if (condition(pnd_Ws_Printed_Cref_Heading.equals("Y")))                                                                                                           //Natural: IF #WS-PRINTED-CREF-HEADING = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM FORMAT-CREF-TOTALS
            sub_Format_Cref_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Side_Printed.less(pdaFcpa200.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                    //Natural: IF #WS-SIDE-PRINTED LT #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Rec_1.setValue("06This statement is continued on the next page");                                                                                      //Natural: ASSIGN #WS-REC-1 = '06This statement is continued on the next page'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TEST-FOR-GOOD-IVC
        sub_Test_For_Good_Ivc();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Side_Printed.equals(pdaFcpa200.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                  //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_End_Accum.reset();                                                                                                                           //Natural: RESET #WS-GRAND-TOT-END-ACCUM #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Payment.reset();
            pnd_Ws_Grand_Tot_Dpi.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            FOR06:                                                                                                                                                        //Natural: FOR #J = 1 TO #WS-CNTR-INV-ACCT
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
            {
                pnd_Ws_Grand_Tot_End_Accum.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J));                                                    //Natural: ADD #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) TO #WS-GRAND-TOT-END-ACCUM
                pnd_Ws_Grand_Tot_Payment.compute(new ComputeParameters(false, pnd_Ws_Grand_Tot_Payment), pnd_Ws_Grand_Tot_Payment.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J))); //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT ( #J ) #WS-OCCURS.INV-ACCT-DVDND-AMT ( #J ) TO #WS-GRAND-TOT-PAYMENT
                pnd_Ws_Grand_Tot_Dpi.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J));                                                                //Natural: ADD #WS-OCCURS.INV-ACCT-DPI-AMT ( #J ) TO #WS-GRAND-TOT-DPI
                pnd_Ws_Grand_Tot_Ded.compute(new ComputeParameters(false, pnd_Ws_Grand_Tot_Ded), pnd_Ws_Grand_Tot_Ded.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J))); //Natural: ADD #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #J ) #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #J ) #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #J ) TO #WS-GRAND-TOT-DED
                pnd_Ws_Grand_Tot_Ded.nadd(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,"*"));                                                               //Natural: ADD #WS-OCCURS.PYMNT-DED-AMT ( #J,* ) TO #WS-GRAND-TOT-DED
                pnd_Ws_Tot_Check_Amt.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J));                                                          //Natural: ADD #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #J ) TO #WS-TOT-CHECK-AMT
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_F.reset();                                                                                                                                                //Natural: RESET #F
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '06'
            //*  ASSIGN #WS-REC1-CC     = ' 6'
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            //* *--------                                                      /* LEON
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))                                                                             //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE EQ 'EW'
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                 //Natural: MOVE ' ' TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *--------                                                      /* LEON
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_End_Accum,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                          //Natural: MOVE EDITED #WS-GRAND-TOT-END-ACCUM ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                //*  LEON
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("R")))          //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L' OR = 'R'
            {
                if (condition(pnd_Ws_Good_Ivc.equals("Y")))                                                                                                               //Natural: IF #WS-GOOD-IVC = 'Y'
                {
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'#'"));                        //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99'#' ) TO #WS-COLUMN ( #F )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_F.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #F
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                        //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                            //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            }                                                                                                                                                             //Natural: END-IF
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                     //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                //Natural: MOVE EDITED #WS-GRAND-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99'****'"));                               //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99'****' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            }                                                                                                                                                             //Natural: END-IF
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                    //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                       //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("*", true), new ExamineReplace(" "));                     //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '*' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Side_Printed.equals("1")))                                                                                                                   //Natural: IF #WS-SIDE-PRINTED = '1'
        {
            pnd_I.reset();                                                                                                                                                //Natural: RESET #I #WS-LINES ( * )
            pnd_Ws_Lines.getValue("*").reset();
            if (condition(pnd_Ws_Legend_Lump_Sum.equals("Y")))                                                                                                            //Natural: IF #WS-LEGEND-LUMP-SUM = 'Y'
            {
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pnd_Ws_Lines.getValue(pnd_I).setValue(DbsUtil.compress("17* INCLUDES TIAA EXPENSE CHARGE OF ", pnd_Ws_Exp_Amt));                                          //Natural: COMPRESS '17* INCLUDES TIAA EXPENSE CHARGE OF ' #WS-EXP-AMT INTO #WS-LINES ( #I )
                pnd_Ws_Rec_1.setValue(pnd_Ws_Lines.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #WS-REC-1 = #WS-LINES ( #I )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Side_Printed.equals("1")))                                                                                                                   //Natural: IF #WS-SIDE-PRINTED = '1'
        {
            //*  RCC 12/06/02
            if (condition(pnd_Ws_Good_Ivc.equals("Y") || pdaFcpa200.getPnd_Ws_Header_Record_Egtrra_Eligibility_Ind().equals("Y")))                                        //Natural: IF #WS-GOOD-IVC = 'Y' OR #WS-HEADER-RECORD.EGTRRA-ELIGIBILITY-IND = 'Y'
            {
                pnd_Ws_Lit.getValue("*").reset();                                                                                                                         //Natural: RESET #WS-LIT ( * ) #WS-AMT ( * ) #N #WS-FROM ( * )
                pnd_Ws_Amt.getValue("*").reset();
                pnd_N.reset();
                pnd_Ws_From.getValue("*").reset();
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    pnd_Ws_Fonts.setValue("17#");                                                                                                                         //Natural: ASSIGN #WS-FONTS = '17#'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Fonts.setValue(" 7#");                                                                                                                         //Natural: ASSIGN #WS-FONTS = ' 7#'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Tiaa_Ivc.greater(getZero())))                                                                                                        //Natural: IF #WS-TIAA-IVC > 0
                {
                    pnd_N.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #N
                    pnd_Ws_Amt.getValue(pnd_N).setValueEdited(pnd_Ws_Tiaa_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                    //Natural: MOVE EDITED #WS-TIAA-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #N )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Amt.getValue(pnd_N),true), new ExamineSearch("X", true), new ExamineDelete());                               //Natural: EXAMINE FULL #WS-AMT ( #N ) FOR FULL 'X' DELETE
                    pnd_Ws_From.getValue(pnd_N).setValue("FROM");                                                                                                         //Natural: ASSIGN #WS-FROM ( #N ) = 'FROM'
                    pnd_Ws_Lit.getValue(pnd_N).setValue("TIAA");                                                                                                          //Natural: ASSIGN #WS-LIT ( #N ) = 'TIAA'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Cref_Ivc.greater(getZero())))                                                                                                        //Natural: IF #WS-CREF-IVC > 0
                {
                    pnd_N.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #N
                    pnd_Ws_Amt.getValue(pnd_N).setValueEdited(pnd_Ws_Cref_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                    //Natural: MOVE EDITED #WS-CREF-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #N )
                    DbsUtil.examine(new ExamineSource(pnd_Ws_Amt.getValue(pnd_N),true), new ExamineSearch("X", true), new ExamineDelete());                               //Natural: EXAMINE FULL #WS-AMT ( #N ) FOR FULL 'X' DELETE
                    pnd_Ws_From.getValue(pnd_N).setValue("FROM");                                                                                                         //Natural: ASSIGN #WS-FROM ( #N ) = 'FROM'
                    pnd_Ws_Lit.getValue(pnd_N).setValue("CREF");                                                                                                          //Natural: ASSIGN #WS-LIT ( #N ) = 'CREF'
                }                                                                                                                                                         //Natural: END-IF
                //*      EGTRRA REQUIREMENT                         /* ROXAN 11/15/02
                //*   BEGIN
                if (condition(pnd_Ws_Tiaa_Ivc.equals(getZero()) && pnd_Ws_Cref_Ivc.equals(getZero()) && pdaFcpa200.getPnd_Ws_Header_Record_Egtrra_Eligibility_Ind().equals("Y"))) //Natural: IF #WS-TIAA-IVC = 0 AND #WS-CREF-IVC = 0 AND #WS-HEADER-RECORD.EGTRRA-ELIGIBILITY-IND = 'Y'
                {
                    pnd_Ws_Lines.getValue(pnd_I).setValue(DbsUtil.compress(pnd_Ws_Fonts, " PAYMENT DOES NOT HAVE AFTER-TAX CONTRIBUTIONS"));                              //Natural: COMPRESS #WS-FONTS ' PAYMENT DOES NOT HAVE AFTER-TAX CONTRIBUTIONS' INTO #WS-LINES ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ROXAN 2/13/03
                    pnd_Ws_Lines.getValue(pnd_I).setValue(DbsUtil.compress(pnd_Ws_Fonts, " PAYMENT INCLUDES AFTER-TAX CONTRIBUTIONS OF:", pnd_Ws_Amt.getValue(1),         //Natural: COMPRESS #WS-FONTS ' PAYMENT INCLUDES AFTER-TAX CONTRIBUTIONS OF:' #WS-AMT ( 1 ) #WS-FROM ( 1 ) #WS-LIT ( 1 ) #WS-AMT ( 2 ) #WS-FROM ( 2 ) #WS-LIT ( 2 ) INTO #WS-LINES ( #I )
                        pnd_Ws_From.getValue(1), pnd_Ws_Lit.getValue(1), pnd_Ws_Amt.getValue(2), pnd_Ws_From.getValue(2), pnd_Ws_Lit.getValue(2)));
                    //* *       #WS-AMT(1) #WS-FROM(1) #WS-LIT(1) H'00'
                }                                                                                                                                                         //Natural: END-IF
                //*    COMPRESS #WS-FONTS '# PAYT. INCLUDES AFTER-TAX CONTRIBUTIONS OF:'
                //*      #WS-AMT(1) #WS-LIT(1) H'00' #WS-AMT(2) #WS-LIT(2)
                //*      INTO #WS-LINES(#I)
                //*   END                                           /* ROXAN 11/15/01
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 FOR '*' REPLACE ' '
                pnd_Ws_Rec_1.setValue(pnd_Ws_Lines.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #WS-REC-1 = #WS-LINES ( #I )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  INSERTED 04-12-94 BY FRANK
        if (condition(pnd_Ws_Side_Printed.equals("1")))                                                                                                                   //Natural: IF #WS-SIDE-PRINTED = '1'
        {
            if (condition(pnd_Ws_Legend_Gra.equals("Y")))                                                                                                                 //Natural: IF #WS-LEGEND-GRA = 'Y'
            {
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pnd_Ws_Lit.getValue("*").reset();                                                                                                                         //Natural: RESET #WS-LIT ( * ) #WS-AMT ( * ) #N
                pnd_Ws_Amt.getValue("*").reset();
                pnd_N.reset();
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    pnd_Ws_Fonts.setValue("17@");                                                                                                                         //Natural: ASSIGN #WS-FONTS = '17@'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Fonts.setValue(" 7@");                                                                                                                         //Natural: ASSIGN #WS-FONTS = ' 7@'
                }                                                                                                                                                         //Natural: END-IF
                //*    ASSIGN #WS-FONTS = ' 7@'
                pnd_Ws_Lines.getValue(pnd_I).setValue(DbsUtil.compress(pnd_Ws_Fonts, "INCLUDES TIAA EXPENSE CHARGE OF ", pnd_Ws_Gra_Exp_Amt));                            //Natural: COMPRESS #WS-FONTS 'INCLUDES TIAA EXPENSE CHARGE OF ' #WS-GRA-EXP-AMT INTO #WS-LINES ( #I )
                pnd_Ws_Rec_1.setValue(pnd_Ws_Lines.getValue(pnd_I));                                                                                                      //Natural: ASSIGN #WS-REC-1 = #WS-LINES ( #I )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ANNT") || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ALT1")        //Natural: IF ( #WS-HEADER-RECORD.CNTRCT-PAYEE-CDE = 'ANNT' OR = 'ALT1' OR = 'ALT2' ) OR SUBSTR ( #WS-HEADER-RECORD.CNTRCT-PAYEE-CDE,1,2 ) = MASK ( 99 )
            || pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().equals("ALT2") || DbsUtil.maskMatches(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Payee_Cde().getSubstring(1,
            2),"99")))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("R")))                                                                              //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'R'
            {
                if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde().equals("Y")))                                                                       //Natural: IF #WS-HEADER-RECORD.PYMNT-INST-REP-CDE = 'Y'
                {
                    pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2.setValuesByName(pdaFcpa200.getPnd_Ws_Header_Record_Pnd_Ws_Header_Level2());                                  //Natural: MOVE BY NAME #WS-HEADER-LEVEL2 TO #WS-PRIME-KEY-LEVEL2
                    pnd_Ws_End_Prime_Key_Pnd_Ws_End_Prime_Key_Level2.setValuesByName(pnd_Ws_Prime_Key_Pnd_Ws_Prime_Key_Level2);                                           //Natural: MOVE BY NAME #WS-PRIME-KEY-LEVEL2 TO #WS-END-PRIME-KEY-LEVEL2
                    pnd_Ws_End_Prime_Key_Pymnt_Prcss_Seq_Num.setValue(9999999);                                                                                           //Natural: ASSIGN #WS-END-PRIME-KEY.PYMNT-PRCSS-SEQ-NUM = 9999999
                    pnd_Ws_End_Prime_Key_Pymnt_Instmt_Nbr.setValue(99);                                                                                                   //Natural: ASSIGN #WS-END-PRIME-KEY.PYMNT-INSTMT-NBR = 99
                                                                                                                                                                          //Natural: PERFORM READ-PAYMENT-FILE
                    sub_Read_Payment_File();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Side_Printed.equals("1")))                                                                                                                   //Natural: IF #WS-SIDE-PRINTED = '1'
        {
            if (condition(pnd_Ws_Lines.getValue(1).equals(" ") && pnd_Ws_Lines.getValue(2).equals(" ")))                                                                  //Natural: IF #WS-LINES ( 1 ) = ' ' AND #WS-LINES ( 2 ) = ' '
            {
                pnd_Ws_Rec_1.setValue("17");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = '17'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pnd_Ws_Rec_1.setValue(" 7");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' 7'
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_I.equals(1)))                                                                                                                                   //Natural: IF #I = 1
        {
            pnd_Ws_Rec_1.setValue(" 7");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 7'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PAYMENT-FILE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-FOR-GOOD-IVC
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DEDUCTIONS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-DEDUCTION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-CREF-TOTALS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-FEDERAL-LEGEND
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEGEND
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-TIAA-TOTALS
    }
    private void sub_Read_Payment_File() throws Exception                                                                                                                 //Natural: READ-PAYMENT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY PPCN-INV-ORGN-PRCSS-INST STARTING FROM #WS-PRIME-KEY
        (
        "READ_PRIME",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Ws_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") }
        );
        READ_PRIME:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ_PRIME")))
        {
            pnd_Ws_Compare_Key_Pnd_Ws_Compare_Key_Level2.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                             //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #WS-COMPARE-KEY-LEVEL2
            if (condition(pnd_Ws_Compare_Key.greater(pnd_Ws_End_Prime_Key)))                                                                                              //Natural: IF #WS-COMPARE-KEY GT #WS-END-PRIME-KEY
            {
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. )
            }                                                                                                                                                             //Natural: END-IF
            //*  3.28
            if (condition(((((! ((((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde().equals("ANNT") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde().equals("ALT1"))   //Natural: IF NOT ( ( FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE = 'ANNT' OR = 'ALT1' OR = 'ALT2' ) OR SUBSTR ( FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE,1,2 ) = MASK ( 99 ) ) AND ( #WS-HEADER-RECORD.CNTRCT-PPCN-NBR = FCP-CONS-PYMNT.CNTRCT-PPCN-NBR ) AND ( #WS-HEADER-RECORD.CNTRCT-UNQ-ID-NBR = FCP-CONS-PYMNT.CNTRCT-UNQ-ID-NBR ) AND ( #WS-HEADER-RECORD.CNTRCT-INVRSE-DTE = FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE ) AND ( #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = FCP-CONS-PYMNT.CNTRCT-ORGN-CDE )
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde().equals("ALT2")) || DbsUtil.maskMatches(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde().getSubstring(1,2),"99"))) 
                && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr())) && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr())) 
                && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Invrse_Dte().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte())) && pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde()))))
            {
                FOR07:                                                                                                                                                    //Natural: FOR #IN-DEX 1 TO C*INV-ACCT
                for (pnd_In_Dex.setValue(1); condition(pnd_In_Dex.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); pnd_In_Dex.nadd(1))
                {
                    //*  LZ
                    pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_In_Dex));                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = FCP-CONS-PYMNT.INV-ACCT-CDE ( #IN-DEX )
                    DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                            //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    //*  LZ
                    if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                             //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
                    {
                        pnd_Ws_Inst_Tiaa.nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_In_Dex));                                               //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( #IN-DEX ) TO #WS-INST-TIAA
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Inst_Cref.nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_In_Dex));                                               //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( #IN-DEX ) TO #WS-INST-CREF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Ws_Printed_Institution_Amt.equals("N") && pnd_Ws_Side_Printed.equals("1")))                                                             //Natural: IF #WS-PRINTED-INSTITUTION-AMT = 'N' AND #WS-SIDE-PRINTED = '1'
                {
                    pnd_Ws_Printed_Institution_Amt.setValue("Y");                                                                                                         //Natural: ASSIGN #WS-PRINTED-INSTITUTION-AMT = 'Y'
                    pnd_Ws_Lit.getValue("*").reset();                                                                                                                     //Natural: RESET #WS-LIT ( * ) #WS-AMT ( * ) #N
                    pnd_Ws_Amt.getValue("*").reset();
                    pnd_N.reset();
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    if (condition(pnd_I.equals(1)))                                                                                                                       //Natural: IF #I = 1
                    {
                        pnd_Ws_Fonts.setValue("17*");                                                                                                                     //Natural: ASSIGN #WS-FONTS = '17*'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Fonts.setValue(" 7*");                                                                                                                     //Natural: ASSIGN #WS-FONTS = ' 7*'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Inst_Tiaa.greater(getZero())))                                                                                                   //Natural: IF #WS-INST-TIAA > 0
                    {
                        pnd_N.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #N
                        pnd_Ws_Amt.getValue(pnd_N).setValueEdited(pnd_Ws_Inst_Tiaa,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                               //Natural: MOVE EDITED #WS-INST-TIAA ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #N )
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Amt.getValue(pnd_N),true), new ExamineSearch("X", true), new ExamineDelete());                           //Natural: EXAMINE FULL #WS-AMT ( #N ) FOR FULL 'X' DELETE
                        pnd_Ws_Lit.getValue(pnd_N).setValue("TIAA: ");                                                                                                    //Natural: ASSIGN #WS-LIT ( #N ) = 'TIAA: '
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Inst_Cref.greater(getZero())))                                                                                                   //Natural: IF #WS-INST-CREF > 0
                    {
                        pnd_N.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #N
                        pnd_Ws_Amt.getValue(pnd_N).setValueEdited(pnd_Ws_Inst_Cref,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                               //Natural: MOVE EDITED #WS-INST-CREF ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-AMT ( #N )
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Amt.getValue(pnd_N),true), new ExamineSearch("X", true), new ExamineDelete());                           //Natural: EXAMINE FULL #WS-AMT ( #N ) FOR FULL 'X' DELETE
                        pnd_Ws_Lit.getValue(pnd_N).setValue("CREF: ");                                                                                                    //Natural: ASSIGN #WS-LIT ( #N ) = 'CREF: '
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Lines.getValue(pnd_I).setValue(DbsUtil.compress(pnd_Ws_Fonts, "AMOUNT WILL BE REDUCED BY INSTITUTION ", "PAYMENT OF ", pnd_Ws_Lit.getValue(1), //Natural: COMPRESS #WS-FONTS 'AMOUNT WILL BE REDUCED BY INSTITUTION ' 'PAYMENT OF ' #WS-LIT ( 1 ) #WS-AMT ( 1 ) H'0000' #WS-LIT ( 2 ) #WS-AMT ( 2 ) INTO #WS-LINES ( #I )
                        pnd_Ws_Amt.getValue(1), "H'0000'", pnd_Ws_Lit.getValue(2), pnd_Ws_Amt.getValue(2)));
                    pnd_Ws_Rec_1.setValue(pnd_Ws_Lines.getValue(pnd_I));                                                                                                  //Natural: ASSIGN #WS-REC-1 = #WS-LINES ( #I )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Test_For_Good_Ivc() throws Exception                                                                                                                 //Natural: TEST-FOR-GOOD-IVC
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #K = 1 TO #WS-CNTR-INV-ACCT
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_K.nadd(1))
        {
            if (condition(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Ind().getValue(pnd_K).equals("2") && pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_K).greater(getZero()))) //Natural: IF #WS-OCCURS.INV-ACCT-IVC-IND ( #K ) = '2' AND #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) > 0
            {
                pnd_Ws_Good_Ivc.setValue("Y");                                                                                                                            //Natural: ASSIGN #WS-GOOD-IVC = 'Y'
                //* *  ASSIGN #WS-ACCT-CDE = INV-ACCT-CDE (#K)                /* LZ
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_K));                                    //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = #WS-OCCURS.INV-ACCT-CDE ( #K )
                //* *  CALLNAT 'FCPN199' #WS-ACCT-CDE  #PDA-INV-ACCT-INFO     /* LZ
                //*  LZ
                DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //* *  IF #PDA-INV-ACCT-COMPANY   =  'T'
                //*  LZ
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                                 //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
                {
                    pnd_Ws_Tiaa_Ivc.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_K));                                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) TO #WS-TIAA-IVC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Cref_Ivc.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_K));                                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) TO #WS-CREF-IVC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Format_Deductions() throws Exception                                                                                                                 //Natural: FORMAT-DEDUCTIONS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC = '+3'
        //* *IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DS'
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
        //* *END-IF
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Amount_1);                                                                                             //Natural: ASSIGN #WS-COLUMN ( #F ) = #WS-AMOUNT-1
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("*", true), new ExamineReplace(" "));                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL '*' REPLACE ' '
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Literal);                                                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) = #WS-LITERAL
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                     //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Amount_2, pnd_Ws_Legend));                         //Natural: COMPRESS #WS-AMOUNT-2 #WS-LEGEND INTO #WS-COLUMN ( #F ) LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(4).setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(4), MoveOption.LeftJustified);                                                //Natural: MOVE LEFT #WS-COLUMN ( 4 ) TO #WS-COLUMN ( 4 )
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("*", true), new ExamineReplace(" "));                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL '*' REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(4),true), new ExamineSearch("+", true), new ExamineReplace(" "));                           //Natural: EXAMINE FULL #WS-COLUMN ( 4 ) FOR FULL '+' REPLACE ' '
    }
    private void sub_Translate_Deduction() throws Exception                                                                                                               //Natural: TRANSLATE-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1412 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WS-OCCURS.PYMNT-DED-CDE ( #J,#C );//Natural: VALUE 1
        if (condition((pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(1))))
        {
            decideConditionsMet1412++;
            pnd_Ws_Letter.setValue("H");                                                                                                                                  //Natural: ASSIGN #WS-LETTER = 'H'
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(2))))
        {
            decideConditionsMet1412++;
            pnd_Ws_Letter.setValue("L");                                                                                                                                  //Natural: ASSIGN #WS-LETTER = 'L'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(3))))
        {
            decideConditionsMet1412++;
            pnd_Ws_Letter.setValue("M");                                                                                                                                  //Natural: ASSIGN #WS-LETTER = 'M'
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_J,pnd_C).equals(4))))
        {
            decideConditionsMet1412++;
            pnd_Ws_Letter.setValue("G");                                                                                                                                  //Natural: ASSIGN #WS-LETTER = 'G'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Format_Cref_Totals() throws Exception                                                                                                                //Natural: FORMAT-CREF-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Ws_Side_Printed.equals(pdaFcpa200.getPnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex())))                                                  //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Cref_Tot_End_Accum.reset();                                                                                                                            //Natural: RESET #WS-CREF-TOT-END-ACCUM #WS-CREF-TOT-PAYMENT #WS-CREF-TOT-DPI #WS-CREF-TOT-DED #WS-CREF-TOT-CHECK
            pnd_Ws_Cref_Tot_Payment.reset();
            pnd_Ws_Cref_Tot_Dpi.reset();
            pnd_Ws_Cref_Tot_Ded.reset();
            pnd_Ws_Cref_Tot_Check.reset();
            FOR09:                                                                                                                                                        //Natural: FOR #J = 1 TO #WS-CNTR-INV-ACCT
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
            {
                //* *  ASSIGN #WS-ACCT-CDE = INV-ACCT-CDE (#J)                /* LZ
                //*  LZ
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_J));                                    //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = #WS-OCCURS.INV-ACCT-CDE ( #J )
                //* *  CALLNAT 'FCPN199' #WS-ACCT-CDE  #PDA-INV-ACCT-INFO     /* LZ
                //*  LZ
                DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //* *  IF #PDA-INV-ACCT-COMPANY   =  'C'                      /* LZ
                //*  LZ
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C")))                                                                                 //Natural: IF #FUND-PDA.#COMPANY-CDE = 'C'
                {
                    pnd_Ws_Cref_Tot_End_Accum.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt().getValue(pnd_J));                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #J ) TO #WS-CREF-TOT-END-ACCUM
                    pnd_Ws_Cref_Tot_Payment.compute(new ComputeParameters(false, pnd_Ws_Cref_Tot_Payment), pnd_Ws_Cref_Tot_Payment.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_J))); //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT ( #J ) #WS-OCCURS.INV-ACCT-DVDND-AMT ( #J ) TO #WS-CREF-TOT-PAYMENT
                    pnd_Ws_Cref_Tot_Dpi.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_J));                                                             //Natural: ADD #WS-OCCURS.INV-ACCT-DPI-AMT ( #J ) TO #WS-CREF-TOT-DPI
                    pnd_Ws_Cref_Tot_Ded.compute(new ComputeParameters(false, pnd_Ws_Cref_Tot_Ded), pnd_Ws_Cref_Tot_Ded.add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_J)).add(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_J))); //Natural: ADD #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #J ) #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #J ) #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #J ) TO #WS-CREF-TOT-DED
                    pnd_Ws_Cref_Tot_Ded.nadd(pdaFcpa200.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_J,"*"));                                                            //Natural: ADD #WS-OCCURS.PYMNT-DED-AMT ( #J,* ) TO #WS-CREF-TOT-DED
                    pnd_Ws_Cref_Tot_Check.nadd(pdaFcpa200.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_J));                                                     //Natural: ADD #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #J ) TO #WS-CREF-TOT-CHECK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_F.reset();                                                                                                                                                //Natural: RESET #F
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '05'
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("CREF Total");                                                                                            //Natural: ASSIGN #WS-COLUMN ( #F ) = 'CREF Total'
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            //* *--------                                                      /* LEON
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))                                                                             //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE EQ 'EW'
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                 //Natural: MOVE ' ' TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *--------                                                      /* LEON
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Cref_Tot_End_Accum,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                           //Natural: MOVE EDITED #WS-CREF-TOT-END-ACCUM ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
                //*  LEON
            }                                                                                                                                                             //Natural: END-IF
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Cref_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                 //Natural: MOVE EDITED #WS-CREF-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                     //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Cref_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                 //Natural: MOVE EDITED #WS-CREF-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Cref_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99'****'"));                                //Natural: MOVE EDITED #WS-CREF-TOT-DED ( EM = X'$'ZZZ,ZZ9.99'****' ) TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                   //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            }                                                                                                                                                             //Natural: END-IF
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Cref_Tot_Check,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                   //Natural: MOVE EDITED #WS-CREF-TOT-CHECK ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                       //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("*", true), new ExamineReplace(" "));                     //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '*' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Federal_Legend() throws Exception                                                                                                             //Natural: FORMAT-FEDERAL-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        //* *
        //*  USE TX-ELCT-TRGGR TO TEST FOR NRA - ROXAN 3/14/2002
        //*  BEGIN
        //* IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
        //*   IF (#WS-HEADER-RECORD.ANNT-CTZNSHP-CDE = 01 OR= 11)
        //* **OR ( (#WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57') /* COMM 2
        //* **  OR (#WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '97') ) /* LNS G.P 11/3/98
        //*     ASSIGN #WS-LEGEND = '(F)*'
        //*   ELSE
        //*     ASSIGN #WS-LEGEND = '(N)*'
        //*   END-IF
        //* END-IF
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                                //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                    //Natural: IF #WS-HEADER-RECORD.PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
            {
                pnd_Ws_Legend.setValue("(N)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(N)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Legend.setValue("(F)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(F)*'
            }                                                                                                                                                             //Natural: END-IF
            //*   END OF NEW IF STMT  3/18/98
        }                                                                                                                                                                 //Natural: END-IF
        //*  END
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                                //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86'
            {
                pnd_Ws_Legend.setValue("(C)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(C)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86")))) //Natural: IF NOT ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86' )
                {
                    pnd_Ws_Legend.setValue("(C)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(C)*'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_State_Legend() throws Exception                                                                                                               //Natural: FORMAT-STATE-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                                //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))                                                                             //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '97'
            {
                pnd_Ws_Legend.setValue("(S)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(S)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11))      //Natural: IF ( #WS-HEADER-RECORD.ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' )
                    || (pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))))
                {
                    pnd_Ws_Legend.setValue("(S)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(S)*'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((! ((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa200.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11)))  //Natural: IF NOT ( #WS-HEADER-RECORD.ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' ) AND #WS-HEADER-RECORD.ANNT-RSDNCY-CDE NE '97'
                        && ! ((pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))) 
                        && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().notEquals("97"))))
                    {
                        pnd_Ws_Legend.setValue("(N)*");                                                                                                                   //Natural: ASSIGN #WS-LEGEND = '(N)*'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                                //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86' )
            {
                pnd_Ws_Legend.setValue("(P)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND = '(P)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("96")))                                                                         //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '96'
                {
                    pnd_Ws_Legend.setValue("(C)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(C)*'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Legend.setValue("(C)*");                                                                                                                       //Natural: ASSIGN #WS-LEGEND = '(C)*'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Recs() throws Exception                                                                                                                        //Natural: WRITE-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        //*  FCPN246 IS USED ONLY BY DS (FCPP221) - BATCH STATUS 'W' AND
        //*  BY SS(ALL) AND DS(REDRAW) DURING THE DAY -   STATUS 'D'
        //*  FOR SS & DS(REDRAW) RUN, COLUMN 4 OF 7 SHOULD BE 11 BYTES INSTEAD 15
        //*  THIS CHANGE IS NEEDED, BECAUSE A NEW FORM WITH BARCODE IS USED AND
        //*  IT HAS NO SPACE FOR 7 COLUMNS 15 CHARACTERS EACH.
        //*  THE CHANGE IS ONLY FOR THE 1ST PAGE OF THE PAYMENT (PAGE WITH BARCODE
        //* *IF      #WS-HEADER-RECORD.PYMNT-STATS-CDE = 'D' /* JFT
        //* *  AND #WS-SIDE-PRINTED = '1'                    /*
        if (condition(((pnd_Ws_Side_Printed.equals("1") || pnd_Ws_Side_Printed.equals("3")) && (pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(4).equals(" ") ||                     //Natural: IF #WS-SIDE-PRINTED = '1' OR = '3' AND ( #WS-COLUMN ( 4 ) = ' ' OR SUBSTR ( #WS-COLUMN ( 4 ) ,1,5 ) = ' Unit' )
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(4).getSubstring(1,5).equals(" Unit")))))
        {
            setValueToSubstring(pnd_Ws_Rec_1.getSubstring(63,81),pnd_Ws_Rec_1,59,85);                                                                                     //Natural: MOVE SUBSTR ( #WS-REC-1,63,81 ) TO SUBSTR ( #WS-REC-1,59,85 )
            //*  MOVE THE REST OF THE LINE AFTER COLUMN 4(15 CHARS)
            //*  TO THE REST OF THE LINE   AFTER COLUMN 4(11 CHARS)
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }
    private void sub_Format_Tiaa_Totals() throws Exception                                                                                                                //Natural: FORMAT-TIAA-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_F.reset();                                                                                                                                                    //Natural: RESET #F
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC = '05'
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("TIAA Total");                                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) = 'TIAA Total'
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //* *--------                                                      /* LEON
        if (condition(pdaFcpa200.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("EW")))                                                                                 //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE EQ 'EW'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                     //Natural: MOVE ' ' TO #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *--------                                                      /* LEON
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tiaa_Tot_End_Accum,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                               //Natural: MOVE EDITED #WS-TIAA-TOT-END-ACCUM ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
            //*  LEON
        }                                                                                                                                                                 //Natural: END-IF
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tiaa_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                     //Natural: MOVE EDITED #WS-TIAA-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(" ");                                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) = ' '
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tiaa_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                     //Natural: MOVE EDITED #WS-TIAA-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tiaa_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99'****'"));                                    //Natural: MOVE EDITED #WS-TIAA-TOT-DED ( EM = X'$'ZZZ,ZZ9.99'****' ) TO #WS-COLUMN ( #F )
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                       //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValueEdited(pnd_Ws_Tiaa_Tot_Check,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99'*'"));                                       //Natural: MOVE EDITED #WS-TIAA-TOT-CHECK ( EM = X'$'Z,ZZZ,ZZ9.99'*' ) TO #WS-COLUMN ( #F )
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineDelete());                           //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' DELETE
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*").setValue(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"), MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-COLUMN ( * ) TO #WS-COLUMN ( * )
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue("*"),true), new ExamineSearch("*", true), new ExamineReplace(" "));                         //Natural: EXAMINE FULL #WS-COLUMN ( * ) FOR FULL '*' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_F.reset();                                                                                                                                                    //Natural: RESET #F #WS-TIAA-TOT-END-ACCUM #WS-TIAA-TOT-PAYMENT #WS-TIAA-TOT-DPI #WS-TIAA-TOT-DED #WS-TIAA-TOT-CHECK
        pnd_Ws_Tiaa_Tot_End_Accum.reset();
        pnd_Ws_Tiaa_Tot_Payment.reset();
        pnd_Ws_Tiaa_Tot_Dpi.reset();
        pnd_Ws_Tiaa_Tot_Ded.reset();
        pnd_Ws_Tiaa_Tot_Check.reset();
    }

    //
}
