/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:55 PM
**        * FROM NATURAL SUBPROGRAM : Cpon100
************************************************************
**        * FILE NAME            : Cpon100.java
**        * CLASS NAME           : Cpon100
**        * INSTANCE NAME        : Cpon100
************************************************************
***********************************************************************
** PROGRAM:     CPON100 CLONE FROM CPSN100                           **
** SYSTEM:      CONSOLIDATED PAYMENT SYSTEM                          **
** DATE:        02/16/2000                                           **
** AUTHOR:      LEON GURTOVNIK                                       **
** DESCRIPTION: THIS PROGRAM WILL ACCESS THE REFERENCE-TABLE USING   **
**              INPUT PARAMETERS:                                    **
**                    TABLE ID,                                      **
**                    SHORT KEY,                                     **
**                    AND/OR LONG KEY.                               **
**              OUTPUT PARAMETERS:                                   **
**                    RETURN CODE = 00    (SUCCESSFUL)               **
**                                = 02    (PARTIAL MATCH)            **
**                                = 03-99 (ERROR CODES)              **
**                    ALL REFERENCE-TABLE FIELDS.                    **
**              THE CALLING PROGRAM MUST REDEFINE THE FIELD          **
**              FOR ITS OWN DEFINITIONS.                             **
***********************************************************************
**                           H I S T O R Y                           **
***********************************************************************
** 03/27/00  ILSE BOHM  ADD LONG-KEY PROCESSING.                     **
**                                                                   **
** 11/08/01  M.ACLAN    ADD RF-SUPER2 (LONG KEY) DATA RETRIEVAL,     **
**                      "NEXT" FUNCTION AND SUPERS OPTION.           **
**                                                                   **
** 07/24/02  A.REYHANI  MOVED THE ORDER OF DATA RETRIEVAL            **
**                      CHANGED LOGIC FOR PARTIAL MATCH              **
**                                                                   **
** 08/02/02 A.REYHANI/I.BOHM MOVE THE NEXT FUNCTION                  **
**                                                                   **
** 02/03/03 A.REYHANIAN IF A-I-IND IS BLANK, DEFAULT IT TO 'A'       **
**                                                                   **
** 02/20/03 A.REYHANIAN ADD SUPER-IND FOR NEXT FUNCTION              **
**                      SUPER-IND = ' '/'S' FIND NEXT ON SHORT KEY   **
**                      SUPER-IND =     'L' FIND NEXT ON LONG  KEY   **
**                                                                   **
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpon100 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCpoa100 pdaCpoa100;
    private LdaCpsl100 ldaCpsl100;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rt_Super1;

    private DbsGroup pnd_Rt_Super1__R_Field_1;
    private DbsField pnd_Rt_Super1_Pnd_Rt1_A_I_Ind;
    private DbsField pnd_Rt_Super1_Pnd_Rt1_Table_Id;
    private DbsField pnd_Rt_Super1_Pnd_Rt1_Short_Key;

    private DbsGroup pnd_Rt_Super1__R_Field_2;
    private DbsField pnd_Rt_Super1_Pnd_Rt1_Short_Key_1;
    private DbsField pnd_Rt_Super1_Pnd_Rt1_Short_Key_2;
    private DbsField pnd_Rt_Super1_Pnd_Rt1_Long_Key;

    private DbsGroup pnd_Rt_Super1__R_Field_3;
    private DbsField pnd_Rt_Super1_Pnd_Rt1_Long_Key_Arr;
    private DbsField pnd_Rt_Super2;

    private DbsGroup pnd_Rt_Super2__R_Field_4;
    private DbsField pnd_Rt_Super2_Pnd_Rt2_A_I_Ind;
    private DbsField pnd_Rt_Super2_Pnd_Rt2_Table_Id;
    private DbsField pnd_Rt_Super2_Pnd_Rt2_Long_Key;
    private DbsField pnd_Rt_Super2_Pnd_Rt2_Short_Key;
    private DbsField pnd_A;
    private DbsField pnd_Partial_Match;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCpsl100 = new LdaCpsl100();
        registerRecord(ldaCpsl100);
        registerRecord(ldaCpsl100.getVw_rt());

        // parameters
        parameters = new DbsRecord();
        pdaCpoa100 = new PdaCpoa100(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Rt_Super1 = localVariables.newFieldInRecord("pnd_Rt_Super1", "#RT-SUPER1", FieldType.STRING, 66);

        pnd_Rt_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Rt_Super1__R_Field_1", "REDEFINE", pnd_Rt_Super1);
        pnd_Rt_Super1_Pnd_Rt1_A_I_Ind = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt1_A_I_Ind", "#RT1-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super1_Pnd_Rt1_Table_Id = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt1_Table_Id", "#RT1-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Rt_Super1_Pnd_Rt1_Short_Key = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt1_Short_Key", "#RT1-SHORT-KEY", FieldType.STRING, 
            20);

        pnd_Rt_Super1__R_Field_2 = pnd_Rt_Super1__R_Field_1.newGroupInGroup("pnd_Rt_Super1__R_Field_2", "REDEFINE", pnd_Rt_Super1_Pnd_Rt1_Short_Key);
        pnd_Rt_Super1_Pnd_Rt1_Short_Key_1 = pnd_Rt_Super1__R_Field_2.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt1_Short_Key_1", "#RT1-SHORT-KEY-1", FieldType.STRING, 
            18);
        pnd_Rt_Super1_Pnd_Rt1_Short_Key_2 = pnd_Rt_Super1__R_Field_2.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt1_Short_Key_2", "#RT1-SHORT-KEY-2", FieldType.STRING, 
            2);
        pnd_Rt_Super1_Pnd_Rt1_Long_Key = pnd_Rt_Super1__R_Field_1.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt1_Long_Key", "#RT1-LONG-KEY", FieldType.STRING, 
            40);

        pnd_Rt_Super1__R_Field_3 = pnd_Rt_Super1__R_Field_1.newGroupInGroup("pnd_Rt_Super1__R_Field_3", "REDEFINE", pnd_Rt_Super1_Pnd_Rt1_Long_Key);
        pnd_Rt_Super1_Pnd_Rt1_Long_Key_Arr = pnd_Rt_Super1__R_Field_3.newFieldArrayInGroup("pnd_Rt_Super1_Pnd_Rt1_Long_Key_Arr", "#RT1-LONG-KEY-ARR", 
            FieldType.STRING, 1, new DbsArrayController(1, 40));
        pnd_Rt_Super2 = localVariables.newFieldInRecord("pnd_Rt_Super2", "#RT-SUPER2", FieldType.STRING, 66);

        pnd_Rt_Super2__R_Field_4 = localVariables.newGroupInRecord("pnd_Rt_Super2__R_Field_4", "REDEFINE", pnd_Rt_Super2);
        pnd_Rt_Super2_Pnd_Rt2_A_I_Ind = pnd_Rt_Super2__R_Field_4.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt2_A_I_Ind", "#RT2-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super2_Pnd_Rt2_Table_Id = pnd_Rt_Super2__R_Field_4.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt2_Table_Id", "#RT2-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Rt_Super2_Pnd_Rt2_Long_Key = pnd_Rt_Super2__R_Field_4.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt2_Long_Key", "#RT2-LONG-KEY", FieldType.STRING, 
            40);
        pnd_Rt_Super2_Pnd_Rt2_Short_Key = pnd_Rt_Super2__R_Field_4.newFieldInGroup("pnd_Rt_Super2_Pnd_Rt2_Short_Key", "#RT2-SHORT-KEY", FieldType.STRING, 
            20);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_Partial_Match = localVariables.newFieldInRecord("pnd_Partial_Match", "#PARTIAL-MATCH", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCpsl100.initializeValues();

        localVariables.reset();
        pnd_Rt_Super1.setInitialValue("  ");
        pnd_Rt_Super2.setInitialValue("  ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cpon100() throws Exception
    {
        super("Cpon100");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 58 LS = 132 ZP = ON
        //* * *ERROR-TA  :=  'INFP9000'           /* JH 7/11/2000
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pdaCpoa100.getCpoa100_Cpoa100b().reset();                                                                                                                         //Natural: RESET CPOA100B
        //* *----------MAIN PROCESS ---------------------------------------------**
        //*   SETUP SUPER
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*                                       /* CHANGED THE ORDER
            //*                                       /* OF DATA RETRIEVAL  07/24/02 AR
            //*  MUST HAVE AT LEAST A TABLE ID
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_Table_Id().equals(" ")))                                                                                          //Natural: IF CPOA100-TABLE-ID = ' '
            {
                pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("99");                                                                                               //Natural: MOVE '99' TO CPOA100-RETURN-CODE
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*  DEFAULT TO 'A'     /* 03/02/03
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().equals(" ")))                                                                                           //Natural: IF CPOA100-A-I-IND = ' '
            {
                //*  03/02/03
                pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().setValue("A");                                                                                                    //Natural: MOVE 'A' TO CPOA100-A-I-IND
                //*  03/02/03
            }                                                                                                                                                             //Natural: END-IF
            //*  8/2/02
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_Function().equals("NEXT")))                                                                                       //Natural: IF CPOA100-FUNCTION EQ 'NEXT'
            {
                                                                                                                                                                          //Natural: PERFORM N100-SETUP-NEXT-SUPER1
                sub_N100_Setup_Next_Super1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  SAME TABLE ID
                //*                                                                                                                                                       //Natural: DECIDE ON FIRST VALUE OF CPOA100-SUPER-IND
                short decideConditionsMet171 = 0;                                                                                                                         //Natural: VALUE ' ', 'S'
                if (condition((pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().equals(" ") || pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().equals("S"))))
                {
                    decideConditionsMet171++;
                    //*  SAME SHORT KEY
                                                                                                                                                                          //Natural: PERFORM N110-GET-NEXT-ON-SHORT-KEY
                    sub_N110_Get_Next_On_Short_Key();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 'L'
                else if (condition((pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().equals("L"))))
                {
                    decideConditionsMet171++;
                    if (condition(pdaCpoa100.getCpoa100_Cpoa100_Short_Key().equals(" ")))                                                                                 //Natural: IF CPOA100-SHORT-KEY EQ ' ' THEN
                    {
                        pdaCpoa100.getCpoa100_Cpoa100_Function().setValue("END");                                                                                         //Natural: MOVE 'END' TO CPOA100-FUNCTION
                        pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("98");                                                                                       //Natural: MOVE '98' TO CPOA100-RETURN-CODE
                        if (true) break PROG;                                                                                                                             //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FUNCTION 'NEXT'
                                                                                                                                                                          //Natural: PERFORM N120-GET-NEXT-ON-LONG-KEY
                    sub_N120_Get_Next_On_Long_Key();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pdaCpoa100.getCpoa100_Cpoa100_Function().setValue("END");                                                                                             //Natural: MOVE 'END' TO CPOA100-FUNCTION
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("98");                                                                                           //Natural: MOVE '98' TO CPOA100-RETURN-CODE
                    if (true) break PROG;                                                                                                                                 //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().notEquals(" ")))                                                                                      //Natural: IF CPOA100-SUPER-IND NE ' '
            {
                short decideConditionsMet194 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF CPOA100-SUPER-IND;//Natural: VALUE '1'
                if (condition((pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().equals("1"))))
                {
                    decideConditionsMet194++;
                                                                                                                                                                          //Natural: PERFORM S100-SETUP-SUPER1
                    sub_S100_Setup_Super1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM S110-READ-SUPER1
                    sub_S110_Read_Super1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE '2'
                else if (condition((pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().equals("2"))))
                {
                    decideConditionsMet194++;
                                                                                                                                                                          //Natural: PERFORM S200-SETUP-SUPER2
                    sub_S200_Setup_Super2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM S210-READ-SUPER2
                    sub_S210_Read_Super2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  MUST HAVE ONE OF THE KEYS
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(" ") && pdaCpoa100.getCpoa100_Cpoa100_Short_Key().equals(" ")))                                 //Natural: IF CPOA100-LONG-KEY EQ ' ' AND CPOA100-SHORT-KEY EQ ' '
            {
                pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("98");                                                                                               //Natural: MOVE '98' TO CPOA100-RETURN-CODE
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF CPOA100-FUNCTION
            short decideConditionsMet212 = 0;                                                                                                                             //Natural: VALUE ' '
            if (condition((pdaCpoa100.getCpoa100_Cpoa100_Function().equals(" "))))
            {
                decideConditionsMet212++;
                short decideConditionsMet215 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CPOA100-A-I-IND NE ' ' AND CPOA100-TABLE-ID NE '     ' AND CPOA100-SHORT-KEY NE '     '
                if (condition(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().notEquals(" ") && pdaCpoa100.getCpoa100_Cpoa100_Table_Id().notEquals("     ") && 
                    pdaCpoa100.getCpoa100_Cpoa100_Short_Key().notEquals("     ")))
                {
                    decideConditionsMet215++;
                                                                                                                                                                          //Natural: PERFORM S100-SETUP-SUPER1
                    sub_S100_Setup_Super1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM S110-READ-SUPER1
                    sub_S110_Read_Super1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN CPOA100-A-I-IND NE ' ' AND CPOA100-TABLE-ID NE ' ' AND CPOA100-LONG-KEY NE ' '
                else if (condition(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().notEquals(" ") && pdaCpoa100.getCpoa100_Cpoa100_Table_Id().notEquals(" ") && 
                    pdaCpoa100.getCpoa100_Cpoa100_Long_Key().notEquals(" ")))
                {
                    decideConditionsMet215++;
                                                                                                                                                                          //Natural: PERFORM S200-SETUP-SUPER2
                    sub_S200_Setup_Super2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM S210-READ-SUPER2
                    sub_S210_Read_Super2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *-------------------------------------------------------------------**
            //* **************** PERFORMED SUBROUTINES *******************************
            //* *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: N100-SETUP-NEXT-SUPER1
            //* *-------------------------------------------------------------------**
            //* *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: N110-GET-NEXT-ON-SHORT-KEY
            //* *-------------------------------------------------------------------**
            //* *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: N120-GET-NEXT-ON-LONG-KEY
            //* *-------------------------------------------------------------------**
            //* *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S100-SETUP-SUPER1
            //* *-------------------------------------------------------------------**
            //* *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S110-READ-SUPER1
            //* *-------------------------------------------------------------------**
            //* *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S200-SETUP-SUPER2
            //* *-------------------------------------------------------------------**
            //* *
            //* *-------------------------------------------------------------------**
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S210-READ-SUPER2
            //* *-------------------------------------------------------------------**
            //* *-------------------------------------------------------------------**
            if (true) break PROG;                                                                                                                                         //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
            //*  (PROG.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* *-------------------------------------------------------------------**
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_N100_Setup_Next_Super1() throws Exception                                                                                                            //Natural: N100-SETUP-NEXT-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rt_Super1_Pnd_Rt1_A_I_Ind.setValue(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind());                                                                                  //Natural: MOVE CPOA100-A-I-IND TO #RT1-A-I-IND
        pnd_Rt_Super1_Pnd_Rt1_Table_Id.setValue(pdaCpoa100.getCpoa100_Cpoa100_Table_Id());                                                                                //Natural: MOVE CPOA100-TABLE-ID TO #RT1-TABLE-ID
        pnd_Rt_Super1_Pnd_Rt1_Short_Key.setValue(pdaCpoa100.getCpoa100_Cpoa100_Short_Key());                                                                              //Natural: MOVE CPOA100-SHORT-KEY TO #RT1-SHORT-KEY
        //*  FIND FIRST NON-BLANK CHAR.
        pnd_Rt_Super1_Pnd_Rt1_Long_Key.setValue(pdaCpoa100.getCpoa100_Cpoa100_Long_Key());                                                                                //Natural: MOVE CPOA100-LONG-KEY TO #RT1-LONG-KEY
        FOR01:                                                                                                                                                            //Natural: FOR #A 40 TO 1 STEP -1
        for (pnd_A.setValue(40); condition(pnd_A.greaterOrEqual(1)); pnd_A.nsubtract(1))
        {
            if (condition(pnd_Rt_Super1_Pnd_Rt1_Long_Key_Arr.getValue(pnd_A).notEquals(" ")))                                                                             //Natural: IF #RT1-LONG-KEY-ARR ( #A ) NE ' ' THEN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FORCED TO HIGH VALUES
        if (condition(pnd_A.equals(40)))                                                                                                                                  //Natural: IF #A = 40
        {
            pnd_Rt_Super1_Pnd_Rt1_Long_Key_Arr.getValue(pnd_A).setValue("H'FF'");                                                                                         //Natural: MOVE H'FF' TO #RT1-LONG-KEY-ARR ( #A )
            //*  SET TO HIGHER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  THAN BLANK.
            pnd_Rt_Super1_Pnd_Rt1_Long_Key_Arr.getValue(pnd_A.getDec().add(1)).setValue("H'41'");                                                                         //Natural: MOVE H'41' TO #RT1-LONG-KEY-ARR ( #A + 1 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_N110_Get_Next_On_Short_Key() throws Exception                                                                                                        //Natural: N110-GET-NEXT-ON-SHORT-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //*                  LOOP THRU ENTRIES WITHIN SAME TABLE.
        ldaCpsl100.getVw_rt().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaCpsl100.getVw_rt().readNextRow("READ01")))
        {
            pdaCpoa100.getCpoa100_Cpoa100b().setValuesByName(ldaCpsl100.getVw_rt());                                                                                      //Natural: MOVE BY NAME RT TO CPOA100B
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().equals(ldaCpsl100.getRt_Rt_A_I_Ind()) && pdaCpoa100.getCpoa100_Cpoa100_Table_Id().equals(ldaCpsl100.getRt_Rt_Table_Id()))) //Natural: IF CPOA100-A-I-IND = RT.RT-A-I-IND AND CPOA100-TABLE-ID = RT.RT-TABLE-ID
            {
                short decideConditionsMet313 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CPOA100-LONG-KEY = ' '
                if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(" ")))
                {
                    decideConditionsMet313++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN CPOA100-LONG-KEY > ' ' AND CPOA100-LONG-KEY = RT.RT-LONG-KEY
                else if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().greater(" ") && pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(ldaCpsl100.getRt_Rt_Long_Key())))
                {
                    decideConditionsMet313++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet313 > 0))
                {
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("00");                                                                                           //Natural: MOVE '00' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("02");                                                                                           //Natural: MOVE '02' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  READ-REF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(" ")))                                                                                              //Natural: IF CPOA100-LONG-KEY = ' '
        {
            //*  NO RECORDS OR NO MATCH
            pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("03");                                                                                                   //Natural: MOVE '03' TO CPOA100-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        pdaCpoa100.getCpoa100_Cpoa100b().reset();                                                                                                                         //Natural: RESET CPOA100B
        pdaCpoa100.getCpoa100_Cpoa100_Function().setValue("END");                                                                                                         //Natural: MOVE 'END' TO CPOA100-FUNCTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
    }
    private void sub_N120_Get_Next_On_Long_Key() throws Exception                                                                                                         //Natural: N120-GET-NEXT-ON-LONG-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //*                  LOOP THRU ENTRIES WITHIN SAME TABLE & SHORT KEY.
        ldaCpsl100.getVw_rt().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
        (
        "READ02",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ02:
        while (condition(ldaCpsl100.getVw_rt().readNextRow("READ02")))
        {
            pdaCpoa100.getCpoa100_Cpoa100b().setValuesByName(ldaCpsl100.getVw_rt());                                                                                      //Natural: MOVE BY NAME RT TO CPOA100B
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().equals(ldaCpsl100.getRt_Rt_A_I_Ind()) && pdaCpoa100.getCpoa100_Cpoa100_Table_Id().equals(ldaCpsl100.getRt_Rt_Table_Id())  //Natural: IF CPOA100-A-I-IND = RT.RT-A-I-IND AND CPOA100-TABLE-ID = RT.RT-TABLE-ID AND CPOA100-SHORT-KEY = RT.RT-SHORT-KEY
                && pdaCpoa100.getCpoa100_Cpoa100_Short_Key().equals(ldaCpsl100.getRt_Rt_Short_Key())))
            {
                short decideConditionsMet345 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CPOA100-LONG-KEY = ' '
                if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(" ")))
                {
                    decideConditionsMet345++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN CPOA100-LONG-KEY > ' ' AND CPOA100-LONG-KEY = RT.RT-LONG-KEY
                else if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().greater(" ") && pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(ldaCpsl100.getRt_Rt_Long_Key())))
                {
                    decideConditionsMet345++;
                    ignore();
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet345 > 0))
                {
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("00");                                                                                           //Natural: MOVE '00' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("02");                                                                                           //Natural: MOVE '02' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  READ-REF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(" ")))                                                                                              //Natural: IF CPOA100-LONG-KEY = ' '
        {
            //*  NO RECORDS OR NO MATCH
            pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("03");                                                                                                   //Natural: MOVE '03' TO CPOA100-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        pdaCpoa100.getCpoa100_Cpoa100b().reset();                                                                                                                         //Natural: RESET CPOA100B
        pdaCpoa100.getCpoa100_Cpoa100_Function().setValue("END");                                                                                                         //Natural: MOVE 'END' TO CPOA100-FUNCTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
    }
    private void sub_S100_Setup_Super1() throws Exception                                                                                                                 //Natural: S100-SETUP-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rt_Super1_Pnd_Rt1_A_I_Ind.setValue(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind());                                                                                  //Natural: MOVE CPOA100-A-I-IND TO #RT1-A-I-IND
        pnd_Rt_Super1_Pnd_Rt1_Table_Id.setValue(pdaCpoa100.getCpoa100_Cpoa100_Table_Id());                                                                                //Natural: MOVE CPOA100-TABLE-ID TO #RT1-TABLE-ID
        pnd_Rt_Super1_Pnd_Rt1_Short_Key.setValue(pdaCpoa100.getCpoa100_Cpoa100_Short_Key());                                                                              //Natural: MOVE CPOA100-SHORT-KEY TO #RT1-SHORT-KEY
        pnd_Rt_Super1_Pnd_Rt1_Long_Key.setValue(pdaCpoa100.getCpoa100_Cpoa100_Long_Key());                                                                                //Natural: MOVE CPOA100-LONG-KEY TO #RT1-LONG-KEY
    }
    private void sub_S110_Read_Super1() throws Exception                                                                                                                  //Natural: S110-READ-SUPER1
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpsl100.getVw_rt().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
        (
        "READ03",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ03:
        while (condition(ldaCpsl100.getVw_rt().readNextRow("READ03")))
        {
            pdaCpoa100.getCpoa100_Cpoa100b().setValuesByName(ldaCpsl100.getVw_rt());                                                                                      //Natural: MOVE BY NAME RT TO CPOA100B
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().equals(ldaCpsl100.getRt_Rt_A_I_Ind()) && pdaCpoa100.getCpoa100_Cpoa100_Table_Id().equals(ldaCpsl100.getRt_Rt_Table_Id())  //Natural: IF CPOA100-A-I-IND = RT.RT-A-I-IND AND CPOA100-TABLE-ID = RT.RT-TABLE-ID AND CPOA100-SHORT-KEY = RT.RT-SHORT-KEY
                && pdaCpoa100.getCpoa100_Cpoa100_Short_Key().equals(ldaCpsl100.getRt_Rt_Short_Key())))
            {
                //*  07/24/02 AR
                //*  07/24/02 AR
                DbsUtil.examine(new ExamineSource(ldaCpsl100.getRt_Rt_Long_Key()), new ExamineSearch(pdaCpoa100.getCpoa100_Cpoa100_Long_Key()), new ExamineGivingPosition(pnd_Partial_Match)); //Natural: EXAMINE RT.RT-LONG-KEY FOR CPOA100-LONG-KEY POSITION #PARTIAL-MATCH
                short decideConditionsMet386 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CPOA100-LONG-KEY = ' '
                if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(" ")))
                {
                    decideConditionsMet386++;
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("00");                                                                                           //Natural: MOVE '00' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN CPOA100-LONG-KEY > ' ' AND CPOA100-LONG-KEY = RT.RT-LONG-KEY
                else if (condition(pdaCpoa100.getCpoa100_Cpoa100_Long_Key().greater(" ") && pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(ldaCpsl100.getRt_Rt_Long_Key())))
                {
                    decideConditionsMet386++;
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("00");                                                                                           //Natural: MOVE '00' TO CPOA100-RETURN-CODE
                    //*  07/24/02 AR
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN #PARTIAL-MATCH = 1
                else if (condition(pnd_Partial_Match.equals(1)))
                {
                    decideConditionsMet386++;
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("02");                                                                                           //Natural: MOVE '02' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  READ-REF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  NO RECORDS OR NO MATCH
        pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("03");                                                                                                       //Natural: MOVE '03' TO CPOA100-RETURN-CODE
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
    }
    //*  MLA 11/08/01
    private void sub_S200_Setup_Super2() throws Exception                                                                                                                 //Natural: S200-SETUP-SUPER2
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rt_Super2_Pnd_Rt2_A_I_Ind.setValue(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind());                                                                                  //Natural: MOVE CPOA100-A-I-IND TO #RT2-A-I-IND
        pnd_Rt_Super2_Pnd_Rt2_Table_Id.setValue(pdaCpoa100.getCpoa100_Cpoa100_Table_Id());                                                                                //Natural: MOVE CPOA100-TABLE-ID TO #RT2-TABLE-ID
        pnd_Rt_Super2_Pnd_Rt2_Long_Key.setValue(pdaCpoa100.getCpoa100_Cpoa100_Long_Key());                                                                                //Natural: MOVE CPOA100-LONG-KEY TO #RT2-LONG-KEY
        pnd_Rt_Super2_Pnd_Rt2_Short_Key.setValue(pdaCpoa100.getCpoa100_Cpoa100_Short_Key());                                                                              //Natural: MOVE CPOA100-SHORT-KEY TO #RT2-SHORT-KEY
    }
    //*  MLA 11/08/01
    private void sub_S210_Read_Super2() throws Exception                                                                                                                  //Natural: S210-READ-SUPER2
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpsl100.getVw_rt().startDatabaseRead                                                                                                                           //Natural: READ ( 1 ) RT BY RT-SUPER2 STARTING FROM #RT-SUPER2
        (
        "READ04",
        new Wc[] { new Wc("RT_SUPER2", ">=", pnd_Rt_Super2, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER2", "ASC") },
        1
        );
        READ04:
        while (condition(ldaCpsl100.getVw_rt().readNextRow("READ04")))
        {
            pdaCpoa100.getCpoa100_Cpoa100b().setValuesByName(ldaCpsl100.getVw_rt());                                                                                      //Natural: MOVE BY NAME RT TO CPOA100B
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().equals(ldaCpsl100.getRt_Rt_A_I_Ind()) && pdaCpoa100.getCpoa100_Cpoa100_Table_Id().equals(ldaCpsl100.getRt_Rt_Table_Id())  //Natural: IF CPOA100-A-I-IND = RT.RT-A-I-IND AND CPOA100-TABLE-ID = RT.RT-TABLE-ID AND CPOA100-LONG-KEY = RT.RT-LONG-KEY
                && pdaCpoa100.getCpoa100_Cpoa100_Long_Key().equals(ldaCpsl100.getRt_Rt_Long_Key())))
            {
                //*  07/24/02 AR
                //*  07/24/02 AR
                DbsUtil.examine(new ExamineSource(ldaCpsl100.getRt_Rt_Short_Key()), new ExamineSearch(pdaCpoa100.getCpoa100_Cpoa100_Short_Key()), new                     //Natural: EXAMINE RT.RT-SHORT-KEY FOR CPOA100-SHORT-KEY POSITION #PARTIAL-MATCH
                    ExamineGivingPosition(pnd_Partial_Match));
                short decideConditionsMet428 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CPOA100-SHORT-KEY = ' '
                if (condition(pdaCpoa100.getCpoa100_Cpoa100_Short_Key().equals(" ")))
                {
                    decideConditionsMet428++;
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("00");                                                                                           //Natural: MOVE '00' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN CPOA100-SHORT-KEY > ' ' AND CPOA100-SHORT-KEY = RT.RT-SHORT-KEY
                else if (condition(pdaCpoa100.getCpoa100_Cpoa100_Short_Key().greater(" ") && pdaCpoa100.getCpoa100_Cpoa100_Short_Key().equals(ldaCpsl100.getRt_Rt_Short_Key())))
                {
                    decideConditionsMet428++;
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("00");                                                                                           //Natural: MOVE '00' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN #PARTIAL-MATCH = 1
                else if (condition(pnd_Partial_Match.equals(1)))
                {
                    decideConditionsMet428++;
                    pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("02");                                                                                           //Natural: MOVE '02' TO CPOA100-RETURN-CODE
                    Global.setEscape(true);                                                                                                                               //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
                    if (true) return;
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  READ-REF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  NO RECORDS OR NO MATCH
        pdaCpoa100.getCpoa100_Cpoa100_Return_Code().setValue("03");                                                                                                       //Natural: MOVE '03' TO CPOA100-RETURN-CODE
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
    }
}
