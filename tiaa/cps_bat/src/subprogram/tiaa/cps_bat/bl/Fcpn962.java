/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:15 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn962
************************************************************
**        * FILE NAME            : Fcpn962.java
**        * CLASS NAME           : Fcpn962
**        * INSTANCE NAME        : Fcpn962
************************************************************
************************************************************************
* SUBPROGRAM : FCPN962  (CLONE OF FCPNNZC2)
* SYSTEM     : CPS
* FUNCTION   : CONTROL REPORT FOR NEW ANNUITIZATION FOR XEROX/DCS TO
*            : CCP ELIMINATION.
************************************************************************
*  NAME         DATE            DESCRIPTION
*  FENDAYA      2016 MARCH      NEW
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn962 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpanzc2 pdaFcpanzc2;
    private PdaFcpacrpt pdaFcpacrpt;
    private LdaFcplnzc3 ldaFcplnzc3;
    private LdaFcplcrp1 ldaFcplcrp1;
    private PdaFcpacntr pdaFcpacntr;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Record_Cnt_Cv;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplnzc3 = new LdaFcplnzc3();
        registerRecord(ldaFcplnzc3);
        ldaFcplcrp1 = new LdaFcplcrp1();
        registerRecord(ldaFcplcrp1);
        localVariables = new DbsRecord();
        pdaFcpacntr = new PdaFcpacntr(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpanzc2 = new PdaFcpanzc2(parameters);
        pdaFcpacrpt = new PdaFcpacrpt(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Record_Cnt_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Record_Cnt_Cv", "#RECORD-CNT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplnzc3.initializeValues();
        ldaFcplcrp1.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Record_Cnt_Cv.setInitialAttributeValue("AD=D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn962() throws Exception
    {
        super("Fcpn962");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 15 ) PS = 58 LS = 132 ZP = ON
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Cntl_Table().getValue(1).setValuesByName(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Table());                                          //Natural: MOVE BY NAME #ACCUM-TABLE TO #CNTL-TABLE ( 1 )
        pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Abend_Ind().setValue(false);                                                                                               //Natural: ASSIGN CNTRL-ABEND-IND := FALSE
        pdaFcpacntr.getCntrl_Cntl_Orgn_Cde().setValue("NZ");                                                                                                              //Natural: ASSIGN CNTRL.CNTL-ORGN-CDE := 'NZ'
        DbsUtil.callnat(Fcpncntr.class , getCurrentProcessState(), pdaFcpacntr.getCntrl_Work_Fields(), pdaFcpacntr.getCntrl());                                           //Natural: CALLNAT 'FCPNCNTR' CNTRL-WORK-FIELDS CNTRL
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Return_Cde().equals(90)))                                                                                    //Natural: IF CNTRL-RETURN-CDE = 90
        {
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Diff().setValue(true);                                                                                                        //Natural: ASSIGN #FCPACRPT.#DIFF := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(2,"*").reset();                                                                                                //Natural: RESET #FCPLCRP1.#REC-CNT ( 2,* )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(2,"*").setValue(pdaFcpacntr.getCntrl_Cntl_Cnt().getValue("*"));                                              //Natural: ASSIGN #FCPLCRP1.#PYMNT-CNT ( 2,* ) := CNTRL.CNTL-CNT ( * )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(2,"*").setValue(pdaFcpacntr.getCntrl_Cntl_Gross_Amt().getValue("*"));                                        //Natural: ASSIGN #FCPLCRP1.#SETTL-AMT ( 2,* ) := CNTRL.CNTL-GROSS-AMT ( * )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(2,"*").setValue(pdaFcpacntr.getCntrl_Cntl_Net_Amt().getValue("*"));                                      //Natural: ASSIGN #FCPLCRP1.#NET-PYMNT-AMT ( 2,* ) := CNTRL.CNTL-NET-AMT ( * )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(3,"*").compute(new ComputeParameters(false, ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(3,"*")),    //Natural: COMPUTE #FCPLCRP1.#PYMNT-CNT ( 3,* ) = #FCPLCRP1.#PYMNT-CNT ( 2,* ) - #FCPLCRP1.#PYMNT-CNT ( 1,* )
            ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(2,"*").subtract(ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(1,"*")));
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(3,"*").compute(new ComputeParameters(false, ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(3,"*")),    //Natural: COMPUTE #FCPLCRP1.#SETTL-AMT ( 3,* ) = #FCPLCRP1.#SETTL-AMT ( 2,* ) - #FCPLCRP1.#SETTL-AMT ( 1,* )
            ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(2,"*").subtract(ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(1,"*")));
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(3,"*").compute(new ComputeParameters(false, ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(3,"*")),  //Natural: COMPUTE #FCPLCRP1.#NET-PYMNT-AMT ( 3,* ) = #FCPLCRP1.#NET-PYMNT-AMT ( 2,* ) - #FCPLCRP1.#NET-PYMNT-AMT ( 1,* )
            ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(2,"*").subtract(ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(1,"*")));
        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        FOR01:                                                                                                                                                            //Natural: FOR #J = 1 TO 3
        for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(3)); pnd_Ws_Pnd_J.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_J.greater(1)))                                                                                                                       //Natural: IF #J > 1
            {
                pnd_Ws_Pnd_Record_Cnt_Cv.setValue("AD=N");                                                                                                                //Natural: ASSIGN #RECORD-CNT-CV := ( AD = N )
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(15, NEWLINE,new TabSetting(5),ldaFcplnzc3.getPnd_Fcplnzc3_Pnd_Cntl_Type().getValue(pnd_Ws_Pnd_J));                                         //Natural: WRITE ( 15 ) / 5T #CNTL-TYPE ( #J )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO 50
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(50)); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(pnd_Ws_Pnd_I).getBoolean() && ldaFcplnzc3.getPnd_Fcplnzc3_Pnd_Pymnt_Type_Desc().getValue(pnd_Ws_Pnd_I).notEquals(" "))) //Natural: IF #FCPACRPT.#TRUTH-TABLE ( #I ) AND #PYMNT-TYPE-DESC ( #I ) NE ' '
                {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CNTL-REPORT
                    sub_Display_Cntl_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Pnd_J.equals(3) && (ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(3,pnd_Ws_Pnd_I).notEquals(getZero())                    //Natural: IF #J = 3 AND ( #FCPLCRP1.#PYMNT-CNT ( 3,#I ) NE 0 OR #FCPLCRP1.#SETTL-AMT ( 3,#I ) NE 0.00 OR #FCPLCRP1.#NET-PYMNT-AMT ( 3,#I ) NE 0.00 )
                        || ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(3,pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")) || ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(3,pnd_Ws_Pnd_I).notEquals(new 
                        DbsDecimal("0.00")))))
                    {
                        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Diff().setValue(true);                                                                                            //Natural: ASSIGN #FCPACRPT.#DIFF := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  IF #FCPACRPT.#DIFF
        //*    PERFORM  ERROR-DISPLAY-START
        //*    WRITE (15)
        //*          '***' 25T 'ERROR OCCURED IN THE CONTROL PROCESS' 77T '***'
        //*    /     '***' 25T 'PROGRAM....:' *PROGRAM                77T '***'
        //*    /     '***' 25T 'CALLED FROM:' #PROGRAM                77T '***'
        //*    PERFORM  ERROR-DISPLAY-END
        //*    IF #FCPACRPT.#NO-ABEND
        //*       IGNORE
        //*    ELSE
        //*       TERMINATE 90
        //*    END-IF
        //*  END-IF
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-CNTL-REPORT
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *----------------------------------
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 15 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Consolidated Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' #PROGRAM 47T #TITLE 119T 'REPORT: RPT15' / 58T 'Control Report' //
    }
    private void sub_Display_Cntl_Report() throws Exception                                                                                                               //Natural: DISPLAY-CNTL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().display(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",                                   //Natural: DISPLAY ( 15 ) ( HC = R ) 10T '/DESCRIPTION' #PYMNT-TYPE-DESC ( #I ) ( HC = L ) 'PAYMNT/COUNT' #FCPLCRP1.#PYMNT-CNT ( #J,#I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 'RECORD/COUNT' #FCPLCRP1.#REC-CNT ( #J,#I ) ( EM = -ZZZ,ZZZ,ZZ9 CV = #RECORD-CNT-CV ) 'GROSS/AMOUNT' #FCPLCRP1.#SETTL-AMT ( #J,#I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ ) 'NET/AMOUNT' #FCPLCRP1.#NET-PYMNT-AMT ( #J,#I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ )
        		ldaFcplnzc3.getPnd_Fcplnzc3_Pnd_Pymnt_Type_Desc().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
            "PAYMNT/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"), pnd_Ws_Pnd_Record_Cnt_Cv,
            "GROSS/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),
            "NET/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        getReports().write(15, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new //Natural: WRITE ( 15 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(15, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new         //Natural: WRITE ( 15 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(15, "PS=58 LS=132 ZP=ON");

        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"Consolidated Payment System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program(),new 
            TabSetting(47),pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title(),new TabSetting(119),"REPORT: RPT15",NEWLINE,new TabSetting(58),"Control Report",NEWLINE,
            NEWLINE);

        getReports().setDisplayColumns(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",
        		ldaFcplnzc3.getPnd_Fcplnzc3_Pnd_Pymnt_Type_Desc(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"PAYMNT/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"), pnd_Ws_Pnd_Record_Cnt_Cv,"GROSS/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),"NET/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
    }
}
