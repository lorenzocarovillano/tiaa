/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:23:04 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn376
************************************************************
**        * FILE NAME            : Fcpn376.java
**        * CLASS NAME           : Fcpn376
**        * INSTANCE NAME        : Fcpn376
************************************************************
************************************************************************
* PROGRAM  : FCPN376
* SYSTEM   : CPS
* TITLE    : CONTROLS: VERIFY AND REPORT
* GENERATED: DEC 1993
* FUNCTION : THIS SUBPROGRAM IS CALLED BY VARIOUS PROGRAMS PROCESSING
*            THE VARIOUS SETTLEMENTS AND VARIOUS PAY TYPES: CHECKS, EFT,
*            GLOBAL. IT COMPARES CONTROL ELEMENTS OF COUNTS AND
*            ACCUMULATED AMOUNTS WHICH ARE STORED ON THE CONTROL DB
*            WITH DATA COLLECTED DURING EACH OF THE INVOKING PROGRAMS.
*            IF AN IMBALANCE CONDITION IS DETECTED THE PROGRAM ABEND
*            THE EXECUTION WITH A PROPER MESSAGE AND SETS THE MVS
*            CONDITION CODE.
* HISTORY:
*
*  FEB 28, 1994  LEON SILBERSTEIN
*      MODIFY THE SUBPROGRAM TO COMPARE THE CONTROLS DEPENDING
*      ON THE INVOKING PROGRAM.
*
************************************************************************
* NOTE:
*  THIS SUBPROGRAM IS CALLED BY VARIOUS PROGRAMS. IT ACTS DIFFERENTLY
*  DEPENDING ON WHICH PROGRAM INVOKS IT:
*  1. THE EXTRACT PROGRAM (FCPP710), EXTRACTS ALL THE VARIOUS RECORDS
*     WHICH NEED TO BE PROCESSED INCLUDING CHECKS, EFT, GLOBALS. THUS,
*     THE COUNTERS ACCUMULATED BY FCPP710 ARE ALL ICLUSIVE FOR ALL
*     PAY TYPES AND ALL SETTLEMENTS.
*     THEREFORE, IF THIS SUBPROGRAM WAS CALLED BY FCPP710 THEN WE ARE
*     LOOKING FOR A PERFECT MATCH ON ALL COUNTERS.
*  2. THE UPDATE PROGRAM (FCPP765) PROCESSES EITHER THE CHECKS, OR THE
*     EFTS, OR THE GLOBALS THAT HAVE BEEN PROCESSED. (EACH OF THESE
*     PAYTYPES IS SEPARATED INTO ITS OWN INPUT AND FED INTO THE
*     STATEMENT PROCESSING BY PROGRAM FCPP720).
*     THEREFORE, WHEN THIS SUBPROGRAM IS INVOKED FROM THE UPDATE
*     PROGRAM THEN WE DO NOT EXPECT A MATCH ON ALL THE PAYTYPES, BUT
*     ONLY FOR THE PAYTYPE THAT IS INPUT TO THE PROCESS.
*  THIS SUBPROGRAM RECEIVES A PARAMETER INDICATING WHICH IS THE
*  CALLING PROGRAM, BUT IT RECEIVES NO PARAMETER INDICATING IF THE
*  INPUT FOR THE UPDATE PROCESSING WAS CHECKS, EFT, GLOBAL.
*  THUS THIS SUBPROGRAM MAKES THE FOLLOWING ASSUMPTION:
*  - IF THE CHECKS COUNT IS NOT 0, THEN CHECKS WERE PROCESSED.
*  - IF THE EFT    COUNT IS NOT 0, THEN EFT    WERE PROCESSED.
*  - IF THE GLOBAL COUNT IS NOT 0, THEN GLOBAL WERE PROCESSED.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn376 extends BLNatBase
{
    // Data Areas
    private LdaFcpl200 ldaFcpl200;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Via_Program;
    private DbsField pnd_Cntl_Isn;

    private DbsGroup pnd_Ct_Cntrct_Typ;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Type_Code;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Cnt;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Net_Amt;

    private DbsGroup pnd_Ct_Pay_Typ;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Pay_Code;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Cnt;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Gross_Amt;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Net_Amt;
    private DbsField pnd_Program;
    private DbsField pnd_Via_Program_Title;

    private DbsGroup pnd_Cntl_Dffrnc;
    private DbsField pnd_Cntl_Dffrnc_Pnd_Cnt;
    private DbsField pnd_Cntl_Dffrnc_Pnd_Gross_Amt;
    private DbsField pnd_Cntl_Dffrnc_Pnd_Net_Amt;
    private DbsField pnd_I;
    private DbsField pnd_Pymt_Type_Desc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl200 = new LdaFcpl200();
        registerRecord(ldaFcpl200);
        registerRecord(ldaFcpl200.getVw_fcp_Cons_Cntrl_View());

        // parameters
        parameters = new DbsRecord();
        pnd_Via_Program = parameters.newFieldInRecord("pnd_Via_Program", "#VIA-PROGRAM", FieldType.STRING, 8);
        pnd_Via_Program.setParameterOption(ParameterOption.ByReference);
        pnd_Cntl_Isn = parameters.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.NUMERIC, 10);
        pnd_Cntl_Isn.setParameterOption(ParameterOption.ByReference);

        pnd_Ct_Cntrct_Typ = parameters.newGroupArrayInRecord("pnd_Ct_Cntrct_Typ", "#CT-CNTRCT-TYP", new DbsArrayController(1, 50));
        pnd_Ct_Cntrct_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Type_Code", "#TYPE-CODE", FieldType.STRING, 2);
        pnd_Ct_Cntrct_Typ_Pnd_Cnt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ct_Cntrct_Typ_Pnd_Net_Amt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Ct_Pay_Typ = parameters.newGroupArrayInRecord("pnd_Ct_Pay_Typ", "#CT-PAY-TYP", new DbsArrayController(1, 50));
        pnd_Ct_Pay_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_Ct_Pay_Typ_Pnd_Pay_Code = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Pay_Code", "#PAY-CODE", FieldType.STRING, 1);
        pnd_Ct_Pay_Typ_Pnd_Cnt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Pay_Typ_Pnd_Gross_Amt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ct_Pay_Typ_Pnd_Net_Amt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Via_Program_Title = localVariables.newFieldInRecord("pnd_Via_Program_Title", "#VIA-PROGRAM-TITLE", FieldType.STRING, 20);

        pnd_Cntl_Dffrnc = localVariables.newGroupArrayInRecord("pnd_Cntl_Dffrnc", "#CNTL-DFFRNC", new DbsArrayController(1, 50));
        pnd_Cntl_Dffrnc_Pnd_Cnt = pnd_Cntl_Dffrnc.newFieldInGroup("pnd_Cntl_Dffrnc_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt = pnd_Cntl_Dffrnc.newFieldInGroup("pnd_Cntl_Dffrnc_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Dffrnc_Pnd_Net_Amt = pnd_Cntl_Dffrnc.newFieldInGroup("pnd_Cntl_Dffrnc_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Pymt_Type_Desc = localVariables.newFieldArrayInRecord("pnd_Pymt_Type_Desc", "#PYMT-TYPE-DESC", FieldType.STRING, 25, new DbsArrayController(1, 
            50));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl200.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Pymt_Type_Desc.getValue(1).setInitialValue("Lump Sum       - ia death");
        pnd_Pymt_Type_Desc.getValue(2).setInitialValue("Pended Current - ia death");
        pnd_Pymt_Type_Desc.getValue(3).setInitialValue("Pended Future  - ia death");
        pnd_Pymt_Type_Desc.getValue(12).setInitialValue("Cancels        - ia death");
        pnd_Pymt_Type_Desc.getValue(13).setInitialValue("Stops          - ia death");
        pnd_Pymt_Type_Desc.getValue(14).setInitialValue("Lump Sum       - iad:rdrw");
        pnd_Pymt_Type_Desc.getValue(15).setInitialValue("Pended Current - iad:rdrw");
        pnd_Pymt_Type_Desc.getValue(16).setInitialValue("Pended Future  - iad:rdrw");
        pnd_Pymt_Type_Desc.getValue(25).setInitialValue("Cancels        - m.s.    ");
        pnd_Pymt_Type_Desc.getValue(26).setInitialValue("Stops          - m.s.    ");
        pnd_Pymt_Type_Desc.getValue(27).setInitialValue("Maturity       - ms:redrw");
        pnd_Pymt_Type_Desc.getValue(28).setInitialValue("TPA            - ms:redrw");
        pnd_Pymt_Type_Desc.getValue(29).setInitialValue("Initial MDO    - ms:redrw");
        pnd_Pymt_Type_Desc.getValue(30).setInitialValue("Recuring MDO   - ms:redrw");
        pnd_Pymt_Type_Desc.getValue(31).setInitialValue("Lump Sum Death - ms:redrw");
        pnd_Pymt_Type_Desc.getValue(32).setInitialValue("Survivor Bnfts - ms:redrw");
        pnd_Pymt_Type_Desc.getValue(46).setInitialValue("Total Cancels  - iadth:ms");
        pnd_Pymt_Type_Desc.getValue(47).setInitialValue("Total Stops    - iadth:ms");
        pnd_Pymt_Type_Desc.getValue(48).setInitialValue("Total Checks ");
        pnd_Pymt_Type_Desc.getValue(49).setInitialValue("Total EFT    ");
        pnd_Pymt_Type_Desc.getValue(50).setInitialValue("Total Globals");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn376() throws Exception
    {
        super("Fcpn376");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        getReports().display(0, pnd_Cntl_Isn,"<==");                                                                                                                      //Natural: DISPLAY #CNTL-ISN '<=='
        if (Global.isEscape()) return;
        GET01:                                                                                                                                                            //Natural: GET FCP-CONS-CNTRL-VIEW #CNTL-ISN
        ldaFcpl200.getVw_fcp_Cons_Cntrl_View().readByID(pnd_Cntl_Isn.getLong(), "GET01");
        //* *--> INSERTED 07-26-94 BY FRANK
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(1,":",3).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(1,":",3)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(1, //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 1:3 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1:3 ) - #CT-CNTRCT-TYP.#CNT ( 1:3 )
            ":",3).subtract(pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(1,":",3)));
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(1,":",3).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(1,":",3)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(1, //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 1:3 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1:3 ) - #CT-CNTRCT-TYP.#GROSS-AMT ( 1:3 )
            ":",3).subtract(pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(1,":",3)));
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(1,":",3).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(1,":",3)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(1, //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 1:3 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1:3 ) - #CT-CNTRCT-TYP.#NET-AMT ( 1:3 )
            ":",3).subtract(pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(1,":",3)));
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(12,":",16).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(12,":",16)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(12, //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 12:16 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 12:16 ) - #CT-CNTRCT-TYP.#CNT ( 12:16 )
            ":",16).subtract(pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(12,":",16)));
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(12,":",16).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(12,":",16)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(12, //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 12:16 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 12:16 ) - #CT-CNTRCT-TYP.#GROSS-AMT ( 12:16 )
            ":",16).subtract(pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(12,":",16)));
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(12,":",16).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(12,":",16)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(12, //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 12:16 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 12:16 ) - #CT-CNTRCT-TYP.#NET-AMT ( 12:16 )
            ":",16).subtract(pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(12,":",16)));
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(25,":",32).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(25,":",32)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(25, //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 25:32 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 25:32 ) - #CT-CNTRCT-TYP.#CNT ( 25:32 )
            ":",32).subtract(pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(25,":",32)));
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(25,":",32).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(25,":",32)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(25, //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 25:32 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 25:32 ) - #CT-CNTRCT-TYP.#GROSS-AMT ( 25:32 )
            ":",32).subtract(pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(25,":",32)));
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(25,":",32).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(25,":",32)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(25, //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 25:32 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 25:32 ) - #CT-CNTRCT-TYP.#NET-AMT ( 25:32 )
            ":",32).subtract(pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(25,":",32)));
        //* ** CANCELS
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(46).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(46)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(46).subtract(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(46))); //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 46 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 46 ) - #CT-PAY-TYP.#CNT ( 46 )
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(46).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(46)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(46).subtract(pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(46))); //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 46 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 46 ) - #CT-PAY-TYP.#GROSS-AMT ( 46 )
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(46).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(46)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(46).subtract(pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(46))); //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 46 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 46 ) - #CT-PAY-TYP.#NET-AMT ( 46 )
        //* ** STOPS
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(47).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(47)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(47).subtract(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(47))); //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 47 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 47 ) - #CT-PAY-TYP.#CNT ( 47 )
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(47).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(47)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(47).subtract(pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(47))); //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 47 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 47 ) - #CT-PAY-TYP.#GROSS-AMT ( 47 )
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(47).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(47)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(47).subtract(pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(47))); //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 47 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 47 ) - #CT-PAY-TYP.#NET-AMT ( 47 )
        //* ** CHECKS
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(48).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(48)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(48).subtract(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(48))); //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 48 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 48 ) - #CT-PAY-TYP.#CNT ( 48 )
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(48).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(48)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(48).subtract(pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(48))); //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 48 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 48 ) - #CT-PAY-TYP.#GROSS-AMT ( 48 )
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(48).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(48)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(48).subtract(pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(48))); //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 48 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 48 ) - #CT-PAY-TYP.#NET-AMT ( 48 )
        //*  EFT
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(49).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(49)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(49).subtract(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(49))); //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 49 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 49 ) - #CT-PAY-TYP.#CNT ( 49 )
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(49).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(49)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(49).subtract(pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(49))); //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 49 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 49 ) - #CT-PAY-TYP.#GROSS-AMT ( 49 )
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(49).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(49)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(49).subtract(pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(49))); //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 49 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 49 ) - #CT-PAY-TYP.#NET-AMT ( 49 )
        //*  GLOBALS
        pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(50).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(50)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(50).subtract(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(50))); //Natural: ASSIGN #CNTL-DFFRNC.#CNT ( 50 ) := FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 50 ) - #CT-PAY-TYP.#CNT ( 50 )
        pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(50).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(50)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(50).subtract(pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(50))); //Natural: ASSIGN #CNTL-DFFRNC.#GROSS-AMT ( 50 ) := FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 50 ) - #CT-PAY-TYP.#GROSS-AMT ( 50 )
        pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(50).compute(new ComputeParameters(false, pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(50)), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(50).subtract(pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(50))); //Natural: ASSIGN #CNTL-DFFRNC.#NET-AMT ( 50 ) := FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 50 ) - #CT-PAY-TYP.#NET-AMT ( 50 )
        pnd_Via_Program_Title.reset();                                                                                                                                    //Natural: RESET #VIA-PROGRAM-TITLE
        //* *--> COMMENTED/INSERTED 07-26-94 BY FRANK
        //* *VALUE 'FCPP710'                                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF #VIA-PROGRAM
        short decideConditionsMet161 = 0;                                                                                                                                 //Natural: VALUE 'FCPP370'
        if (condition((pnd_Via_Program.equals("FCPP370"))))
        {
            decideConditionsMet161++;
            pnd_Via_Program_Title.setValue("via Check Extract");                                                                                                          //Natural: ASSIGN #VIA-PROGRAM-TITLE := 'via Check Extract'
            //* *VALUE 'FCPP721'
        }                                                                                                                                                                 //Natural: VALUE 'FCPP374'
        else if (condition((pnd_Via_Program.equals("FCPP374"))))
        {
            decideConditionsMet161++;
            pnd_Via_Program_Title.setValue("via Check Creation");                                                                                                         //Natural: ASSIGN #VIA-PROGRAM-TITLE := 'via Check Creation'
            //* *VALUE 'FHCP765'
        }                                                                                                                                                                 //Natural: VALUE 'FCPP376'
        else if (condition((pnd_Via_Program.equals("FCPP376"))))
        {
            decideConditionsMet161++;
            pnd_Via_Program_Title.setValue("via Check Update");                                                                                                           //Natural: ASSIGN #VIA-PROGRAM-TITLE := 'via Check Update'
        }                                                                                                                                                                 //Natural: VALUE 'FCPP385'
        else if (condition((pnd_Via_Program.equals("FCPP385"))))
        {
            decideConditionsMet161++;
            pnd_Via_Program_Title.setValue("via Check Update");                                                                                                           //Natural: ASSIGN #VIA-PROGRAM-TITLE := 'via Check Update'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Via_Program_Title.setValue("via Unknown");                                                                                                                //Natural: ASSIGN #VIA-PROGRAM-TITLE := 'via Unknown'
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* .01S005 P05.0.                                                                                                                                                //Natural: FORMAT ( 1 ) PS = 050 LS = 132 ZP = ON SG = OFF
        //* .01S008 A008 .
        //* .01S010 A010 .
        //* .01D025 A025 .
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(1),pnd_Program, new FieldAttributes ("AD=ODL"), Color.white,new ColumnSpacing(3),pnd_Via_Program_Title,  //Natural: WRITE ( 1 ) NOTITLE NOHDR 001T #PROGRAM ( AD = ODL CD = NE ) 003X #VIA-PROGRAM-TITLE ( AD = ODL CD = NE ) 051T 'CONSOLIDATED PAYMENT SYSTEM' ( BL ) 120T 'Page:' ( BL ) 126T *PAGE-NUMBER ( 1 ) ( AD = OD CD = NE ) / 001T *DATU ( AD = OD CD = NE ) 041T 'MS / IA DEATH BATCH EXTRACT CONTROL REPORT     ' ( BL ) 121T *TIME ( AD = OD CD = NE ) / / / 001T '*-------- SETTLEMENT -------* *------ DATABASE TOTALS' ( BL ) 055T '--------* *------- EXTRACT TOTALS --------* *---------' ( BL ) 110T 'DIFFERENCE ----------*' ( BL ) / / 001T 'Typ Description' ( BL ) 031T 'Count' ( BL ) 038T 'Gross Amount' ( BL ) 054T 'Net Amount Count' ( BL ) 072T 'Gross Amount' ( BL ) 088T 'Net Amount Count' ( BL ) 106T 'Gross Amount' ( BL ) 122T 'Net Amount' ( BL ) / 001T '---' ( BL ) 005T '-' ( 025 ) ( BL ) 031T '----- ------------- ------------- ----- -------------' ( BL ) 085T '------------- ----- ------------- -------------' ( BL ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+000 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+000 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+000 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+000 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+000 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+000 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+000 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+000 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+000 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+000 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+000 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+001 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+001 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+001 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+001 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+001 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+001 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+001 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+001 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+001 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+001 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+001 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+002 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+002 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+002 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+002 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+002 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+002 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+002 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+002 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+002 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+002 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+002 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+011 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+011 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+011 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+011 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+011 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+011 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+011 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+011 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+011 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+011 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+011 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+012 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+012 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+012 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+012 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+012 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+012 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+012 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+012 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+012 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+012 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+012 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+013 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+013 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+013 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+013 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+013 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+013 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+013 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+013 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+013 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+013 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+013 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+014 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+014 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+014 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+014 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+014 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+014 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+014 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+014 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+014 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+014 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+014 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+015 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+015 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+015 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+015 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+015 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+015 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+015 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+015 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+015 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+015 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+015 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+024 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+024 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+024 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+024 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+024 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+024 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+024 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+024 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+024 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+024 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+024 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+025 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+025 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+025 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+025 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+025 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+025 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+025 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+025 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+025 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+025 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+025 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+026 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+026 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+026 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+026 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+026 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+026 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+026 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+026 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+026 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+026 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+026 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+027 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+027 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+027 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+027 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+027 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+027 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+027 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+027 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+027 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+027 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+027 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+028 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+028 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+028 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+028 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+028 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+028 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+028 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+028 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+028 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+028 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+028 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+029 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+029 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+029 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+029 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+029 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+029 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+029 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+029 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+029 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+029 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+029 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+030 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+030 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+030 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+030 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+030 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+030 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+030 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+030 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+030 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+030 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+030 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 002T #CT-CNTRCT-TYP.#TYPE-CODE ( 1+031 ) ( AD = ODL CD = NE ) 005T #PYMT-TYPE-DESC ( 1+031 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 1+031 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 1+031 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 1+031 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-CNTRCT-TYP.#CNT ( 1+031 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-CNTRCT-TYP.#GROSS-AMT ( 1+031 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-CNTRCT-TYP.#NET-AMT ( 1+031 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 1+031 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 1+031 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 1+031 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 005T #PYMT-TYPE-DESC ( 46 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 46 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 46 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 46 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-PAY-TYP.#CNT ( 46 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-PAY-TYP.#GROSS-AMT ( 46 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-PAY-TYP.#NET-AMT ( 46 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 46 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 46 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 46 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / / 005T #PYMT-TYPE-DESC ( 47 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 47 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 47 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 47 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-PAY-TYP.#CNT ( 47 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-PAY-TYP.#GROSS-AMT ( 47 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-PAY-TYP.#NET-AMT ( 47 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 47 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 47 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 47 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 005T #PYMT-TYPE-DESC ( 48 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 48 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 48 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 48 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-PAY-TYP.#CNT ( 48 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-PAY-TYP.#GROSS-AMT ( 48 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-PAY-TYP.#NET-AMT ( 48 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 48 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 48 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 48 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 005T #PYMT-TYPE-DESC ( 49 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 49 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 49 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 49 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-PAY-TYP.#CNT ( 49 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-PAY-TYP.#GROSS-AMT ( 49 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-PAY-TYP.#NET-AMT ( 49 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 49 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 49 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 49 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) / 005T #PYMT-TYPE-DESC ( 50 ) ( AD = ODL CD = NE ) 032T FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 50 ) ( AD = ODL EM = ZZZ9 CD = NE ) 037T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT ( 50 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 051T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 50 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 066T #CT-PAY-TYP.#CNT ( 50 ) ( AD = ODL EM = ZZZ9 CD = NE ) 071T #CT-PAY-TYP.#GROSS-AMT ( 50 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 085T #CT-PAY-TYP.#NET-AMT ( 50 ) ( AD = ODL EM = ZZ,ZZZ,ZZZ.99 CD = NE ) 100T #CNTL-DFFRNC.#CNT ( 50 ) ( AD = ODL EM = ZZZ9 CD = NE ) 105T #CNTL-DFFRNC.#GROSS-AMT ( 50 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE ) 119T #CNTL-DFFRNC.#NET-AMT ( 50 ) ( AD = ODL EM = ZZZZZZ,ZZZ.99 CD = NE )
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM", Color.blue,new TabSetting(120),"Page:", Color.blue,new 
            TabSetting(126),getReports().getPageNumberDbs(1), new FieldAttributes ("AD=OD"), Color.white,NEWLINE,new TabSetting(1),Global.getDATU(), new 
            FieldAttributes ("AD=OD"), Color.white,new TabSetting(41),"MS / IA DEATH BATCH EXTRACT CONTROL REPORT     ", Color.blue,new TabSetting(121),Global.getTIME(), 
            new FieldAttributes ("AD=OD"), Color.white,NEWLINE,NEWLINE,NEWLINE,new TabSetting(1),"*-------- SETTLEMENT -------* *------ DATABASE TOTALS", 
            Color.blue,new TabSetting(55),"--------* *------- EXTRACT TOTALS --------* *---------", Color.blue,new TabSetting(110),"DIFFERENCE ----------*", 
            Color.blue,NEWLINE,NEWLINE,new TabSetting(1),"Typ Description", Color.blue,new TabSetting(31),"Count", Color.blue,new TabSetting(38),"Gross Amount", 
            Color.blue,new TabSetting(54),"Net Amount Count", Color.blue,new TabSetting(72),"Gross Amount", Color.blue,new TabSetting(88),"Net Amount Count", 
            Color.blue,new TabSetting(106),"Gross Amount", Color.blue,new TabSetting(122),"Net Amount", Color.blue,NEWLINE,new TabSetting(1),"---", Color.blue,new 
            TabSetting(5),"-",new RepeatItem(25), Color.blue,new TabSetting(31),"----- ------------- ------------- ----- -------------", Color.blue,new 
            TabSetting(85),"------------- ----- ------------- -------------", Color.blue,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,0)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,0)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,0)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,0)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,1)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,1)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,1)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,1)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,2)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,2)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,2)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,2)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,11)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,11)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,11)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,11)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,12)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,12)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,12)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,12)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,13)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,13)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,13)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,13)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,14)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,14)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,14)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,14)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,15)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,15)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,15)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,15)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,24)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,24)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,24)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,24)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,25)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,25)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,25)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,25)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,26)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,26)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,26)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,26)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,27)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,27)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,27)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,27)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,28)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,28)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,28)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,28)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,29)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,29)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,29)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,29)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,30)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,30)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,30)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,30)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(2),pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(DbsField.add(1,31)), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(DbsField.add(1,31)), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(DbsField.add(1,31)), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(DbsField.add(1,31)), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(46), 
            new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(46), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(46), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(46), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(46), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(46), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(46), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(46), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(46), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(46), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,NEWLINE,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(47), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(47), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(47), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(47), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(47), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(47), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(47), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(47), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(47), new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), 
            Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(47), new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), 
            Color.white,NEWLINE,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(48), new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(48), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(48), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(48), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(48), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(48), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(48), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(48), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(48), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(48), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,NEWLINE,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(49), new FieldAttributes ("AD=ODL"), 
            Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(49), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(49), new FieldAttributes ("AD=ODL"), new 
            ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(49), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(49), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(49), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(49), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(49), new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(49), new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), 
            Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(49), new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), 
            Color.white,NEWLINE,new TabSetting(5),pnd_Pymt_Type_Desc.getValue(50), new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(32),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(50), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(37),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Gross_Amt().getValue(50), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(51),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(50), 
            new FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(66),pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(50), new 
            FieldAttributes ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(71),pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(50), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(85),pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(50), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99"), Color.white,new TabSetting(100),pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(50), new FieldAttributes 
            ("AD=ODL"), new ReportEditMask ("ZZZ9"), Color.white,new TabSetting(105),pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(50), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white,new TabSetting(119),pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(50), new FieldAttributes ("AD=ODL"), 
            new ReportEditMask ("ZZZZZZ,ZZZ.99"), Color.white);
        if (Global.isEscape()) return;
        //* *--> INSERTED 07-26-94 BY FRANK
        getReports().skip(1, 5);                                                                                                                                          //Natural: SKIP ( 1 ) 5
        //*  ..... CHECK IF CONTROL ELEMENTS ARE IN BALANCE
        //* *--> COMMENTED/INSERTED 07-26-94 BY FRANK
        //* *IF #VIA-PROGRAM  = 'FCPP765'
        //* *IF #VIA-PROGRAM  = 'FCPP376'
        if (condition(pnd_Via_Program.equals("FCPP385") || pnd_Via_Program.equals("FCPP376")))                                                                            //Natural: IF #VIA-PROGRAM = 'FCPP385' OR = 'FCPP376'
        {
                                                                                                                                                                          //Natural: PERFORM CHK-CNTRL-FOR-UPD
            sub_Chk_Cntrl_For_Upd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Cntl_Dffrnc_Pnd_Cnt.getValue("*").notEquals(getZero()) || pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue("*").notEquals(getZero())                  //Natural: IF #CNTL-DFFRNC.#CNT ( * ) NOT = 0 OR #CNTL-DFFRNC.#GROSS-AMT ( * ) NOT = 0 OR #CNTL-DFFRNC.#NET-AMT ( * ) NOT = 0
                || pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue("*").notEquals(getZero())))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)", new FieldAttributes          //Natural: WRITE ( 1 ) /// 'JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)' ( I ) 1X 'ARE MIS-MATCHED AND/OR OUT OF BALANCE' ( I )
                    ("AD=I"),new ColumnSpacing(1),"ARE MIS-MATCHED AND/OR OUT OF BALANCE", new FieldAttributes ("AD=I"));
                if (Global.isEscape()) return;
                DbsUtil.terminate(98);  if (true) return;                                                                                                                 //Natural: TERMINATE 0098
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  .......
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHK-CNTRL-FOR-UPD
        //*  ... IF CANCELS ARE PROCESSED - VERIFY CANCELS CONTROLS
        //*  ... IF STOPS ARE PROCESSED - VERIFY STOPS CONTROLS
        //*  ... IF CHECKS ARE PROCESSED - VERIFY CHECK CONTROLS
        //*  ... IF EFTS ARE PROCESSED - VERIFY EFTS CONTROLS
        //*  ... IF GLOBALS ARE PROCESSED - VERIFY GLOBALS CONTROLS
    }
    private void sub_Chk_Cntrl_For_Upd() throws Exception                                                                                                                 //Natural: CHK-CNTRL-FOR-UPD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  THE INVOKER IS THE UPDATE PROGRAM.  COMPARE THE PROPER CONTROLS
        //*  ......................................................................
        //*  NOTE:
        //*   #CT-PAY-TYP(1)   : CHECKS
        //*   #CT-PAY-TYP(2)   : EFT
        //*   #CT-PAY-TYP(3)   : GLOBAL
        //*   #CNTL-DFFRNC(08) : CHECKS
        //*   #CNTL-DFFRNC(09) : EFTS
        //*   #CNTL-DFFRNC(10) : GLOBALS
        //*  ----------------------------------------------------------------------
        //*  CANCELS PROCESSED
        short decideConditionsMet230 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #CT-PAY-TYP.#CNT ( 46 ) NE 0
        if (condition(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(46).notEquals(getZero())))
        {
            decideConditionsMet230++;
            //*  IMBALANCE IN CANCELS
            if (condition(pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(46).notEquals(getZero()) || pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(46).notEquals(getZero())                    //Natural: IF #CNTL-DFFRNC.#CNT ( 46 ) NOT = 0 OR #CNTL-DFFRNC.#GROSS-AMT ( 46 ) NOT = 0 OR #CNTL-DFFRNC.#NET-AMT ( 46 ) NOT = 0
                || pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(46).notEquals(getZero())))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)", new FieldAttributes          //Natural: WRITE ( 1 ) /// 'JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)' ( I ) 1X 'ARE MIS-MATCHED AND/OR OUT OF BALANCE (cancels)' ( I )
                    ("AD=I"),new ColumnSpacing(1),"ARE MIS-MATCHED AND/OR OUT OF BALANCE (cancels)", new FieldAttributes ("AD=I"));
                if (Global.isEscape()) return;
                DbsUtil.terminate(98);  if (true) return;                                                                                                                 //Natural: TERMINATE 0098
                //*  STOPS PROCESSED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #CT-PAY-TYP.#CNT ( 47 ) NE 0
        if (condition(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(47).notEquals(getZero())))
        {
            decideConditionsMet230++;
            //*  IMBALANCE IN STOPS
            if (condition(pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(47).notEquals(getZero()) || pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(47).notEquals(getZero())                    //Natural: IF #CNTL-DFFRNC.#CNT ( 47 ) NOT = 0 OR #CNTL-DFFRNC.#GROSS-AMT ( 47 ) NOT = 0 OR #CNTL-DFFRNC.#NET-AMT ( 47 ) NOT = 0
                || pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(47).notEquals(getZero())))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)", new FieldAttributes          //Natural: WRITE ( 1 ) /// 'JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)' ( I ) 1X 'ARE MIS-MATCHED AND/OR OUT OF BALANCE (stops)' ( I )
                    ("AD=I"),new ColumnSpacing(1),"ARE MIS-MATCHED AND/OR OUT OF BALANCE (stops)", new FieldAttributes ("AD=I"));
                if (Global.isEscape()) return;
                DbsUtil.terminate(98);  if (true) return;                                                                                                                 //Natural: TERMINATE 0098
                //*  CHECKS PROCESSED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #CT-PAY-TYP.#CNT ( 48 ) NE 0
        if (condition(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(48).notEquals(getZero())))
        {
            decideConditionsMet230++;
            //*  IMBALANCE IN CHECKS
            if (condition(pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(48).notEquals(getZero()) || pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(48).notEquals(getZero())                    //Natural: IF #CNTL-DFFRNC.#CNT ( 48 ) NOT = 0 OR #CNTL-DFFRNC.#GROSS-AMT ( 48 ) NOT = 0 OR #CNTL-DFFRNC.#NET-AMT ( 48 ) NOT = 0
                || pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(48).notEquals(getZero())))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)", new FieldAttributes          //Natural: WRITE ( 1 ) /// 'JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)' ( I ) 1X 'ARE MIS-MATCHED AND/OR OUT OF BALANCE (checks)' ( I )
                    ("AD=I"),new ColumnSpacing(1),"ARE MIS-MATCHED AND/OR OUT OF BALANCE (checks)", new FieldAttributes ("AD=I"));
                if (Global.isEscape()) return;
                DbsUtil.terminate(98);  if (true) return;                                                                                                                 //Natural: TERMINATE 0098
                //*  EFT PROCESSED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #CT-PAY-TYP.#CNT ( 49 ) NE 0
        if (condition(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(49).notEquals(getZero())))
        {
            decideConditionsMet230++;
            //*  IMBALANCE IN EFTS
            if (condition(pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(49).notEquals(getZero()) || pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(49).notEquals(getZero())                    //Natural: IF #CNTL-DFFRNC.#CNT ( 49 ) NOT = 0 OR #CNTL-DFFRNC.#GROSS-AMT ( 49 ) NOT = 0 OR #CNTL-DFFRNC.#NET-AMT ( 49 ) NOT = 0
                || pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(49).notEquals(getZero())))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)", new FieldAttributes          //Natural: WRITE ( 1 ) /// 'JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)' ( I ) 1X 'ARE MIS-MATCHED AND/OR OUT OF BALANCE (EFT)' ( I )
                    ("AD=I"),new ColumnSpacing(1),"ARE MIS-MATCHED AND/OR OUT OF BALANCE (EFT)", new FieldAttributes ("AD=I"));
                if (Global.isEscape()) return;
                DbsUtil.terminate(98);  if (true) return;                                                                                                                 //Natural: TERMINATE 0098
                //*  GLOBALS PROCESSED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #CT-PAY-TYP.#CNT ( 50 ) NE 0
        if (condition(pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(50).notEquals(getZero())))
        {
            decideConditionsMet230++;
            //*  IMBALANCE IN GLOBALS
            if (condition(pnd_Cntl_Dffrnc_Pnd_Cnt.getValue(50).notEquals(getZero()) || pnd_Cntl_Dffrnc_Pnd_Gross_Amt.getValue(50).notEquals(getZero())                    //Natural: IF #CNTL-DFFRNC.#CNT ( 50 ) NOT = 0 OR #CNTL-DFFRNC.#GROSS-AMT ( 50 ) NOT = 0 OR #CNTL-DFFRNC.#NET-AMT ( 50 ) NOT = 0
                || pnd_Cntl_Dffrnc_Pnd_Net_Amt.getValue(50).notEquals(getZero())))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)", new FieldAttributes          //Natural: WRITE ( 1 ) /// 'JOB TERMINATED: CONTROL RECORDS (DATABASE AND PROGRAM)' ( I ) 1X 'ARE MIS-MATCHED AND/OR OUT OF BALANCE (Globals)' ( I )
                    ("AD=I"),new ColumnSpacing(1),"ARE MIS-MATCHED AND/OR OUT OF BALANCE (Globals)", new FieldAttributes ("AD=I"));
                if (Global.isEscape()) return;
                DbsUtil.terminate(98);  if (true) return;                                                                                                                 //Natural: TERMINATE 0098
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet230 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CHK-CNTRL-FOR-UPD
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "PS=050 LS=132 ZP=ON SG=OFF");

        getReports().setDisplayColumns(0, pnd_Cntl_Isn,"<==");
    }
}
