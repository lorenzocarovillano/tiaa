/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:10:09 PM
**        * FROM NATURAL SUBPROGRAM : Cpvn101
************************************************************
**        * FILE NAME            : Cpvn101.java
**        * CLASS NAME           : Cpvn101
**        * INSTANCE NAME        : Cpvn101
************************************************************
************************************************************************
** PROGRAM    :  CPON101                                              **
** APPLICATION:  CONSOLIDATED PAYMENT SYSTEM                          **
**            :  OPEN INVESTMENTS ARCHITECTURE                        **
** DATE       :  11/28/2001                                           **
** DESCRIPTION:  BATCH CONTROL PROCESSING MODULE.                     **
**            :                                                       **
** AUTHOR     :  ALTHEA A. YOUNG - ADAPTED FROM CPSN101.              **
**            :                                                       **
** IMPORTANT  :  PLEASE NOTE THAT CHANGES TO THE REPORT HEADINGS MAY  **
**            :  AFFECT THE ARMD / MOBIUS REPORT BREAK-OUT!           **
**            :                                                       **
** HISTORY    :                                                       **
**            :                                                       **
** 11/28/2001 :  ALTHEA A. YOUNG - REVISED ERROR PROCESSING.          **
**            :                  - ADDED #TOTAL-PROCESSED.            **
**            :                  - INCREASED #STATUS TO 36 BYTES.     **
**            :                  - POPULATED #CPS-RESTART ON 'PROCESS'**
**            :                    RECORD FOR RERUN FUNCTION.         **
**            :                  - ADDED #OTHER-STATUS-1, #OTHER-     **
**            :                    STATUS-2 TO PRINT MESSAGES FOR     **
**            :                    PCP1605D ONLY.                     **
** 11-08-2002 :  ALTHEA A. YOUNG - RESET 'RESTART' PARAMETER ON NEW   **
**            :                    'RECEIVE' RECORD.                  **
**            :                  - MOVE 'RESTART' PARAMETER TO THE    **
**            :                    END OF THE 'PROCESS' RECORD.  ALSO **
**            :                    DECREASED TO A8 FROM A10 TO        **
**            :                    OPTIMIZE SPACE USAGE FOR THREE MORE**
**            :                    SETS OF UNUSED CONTROL TOTALS.     **
** 12-16-2002 :  A. REYHANIAN    - ADDED CS (CANCEL/STOP) AND REDRAW  **
**            :                    TALLIES FOR CONTROL PROCESSING.    **
**            :                  - FOR END OF DAY, MOVE TERMINATE     **
**            :                    AFTER ET.                          **
** 01-21-2004 :  ALTHEA YOUNG    - REPLACED REF.CPS-DATE WITH         **
**            :                    #WS-DATE-D FOR CONTROL REPORTS.    **
**            :                    FORMAT ERRORS RESULTED (IN PROD.   **
**            :                    ONLY) WHEN IT WAS WRITTEN.         **
**            :                                                       **
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpvn101 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCpoa101 pdaCpoa101;
    private PdaCpoaorgn pdaCpoaorgn;
    private PdaCpoatotl pdaCpoatotl;
    private PdaCpoa100 pdaCpoa100;
    private LdaCpol101 ldaCpol101;
    private LdaCpol101a ldaCpol101a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_rt;
    private DbsGroup rt_Count_Castrt_DescriptionMuGroup;
    private DbsField rt_Count_Castrt_Description;

    private DbsGroup rt_Rt_Record;
    private DbsField rt_Rt_A_I_Ind;
    private DbsField rt_Rt_Table_Id;
    private DbsField rt_Rt_Short_Key;

    private DbsGroup rt__R_Field_1;
    private DbsField rt_Cps_Key;
    private DbsField rt__Filler1;
    private DbsField rt_Cps_Inverse_Dte;
    private DbsField rt_Rt_Long_Key;
    private DbsField rt_Rt_Desc1;

    private DbsGroup rt__R_Field_2;
    private DbsField rt_Cps_Ccyymmdd;
    private DbsField rt_Cps_Filler1;
    private DbsField rt_Cps_Date;
    private DbsGroup rt_Rt_DescriptionMuGroup;
    private DbsField rt_Rt_Description;

    private DbsGroup rt__R_Field_3;

    private DbsGroup rt_Gr_Desc;
    private DbsField rt_Cps_Time;
    private DbsField rt_Cps_Cycle;
    private DbsField rt_Cps_Status;
    private DbsField rt_Cps_Check_Cnt;
    private DbsField rt_Cps_Check_Amt;
    private DbsField rt_Cps_Eft_Cnt;
    private DbsField rt_Cps_Eft_Amt;
    private DbsField rt_Cps_Other_Cnt;
    private DbsField rt_Cps_Other_Amt;
    private DbsField rt_Cps_Cs_Cnt;
    private DbsField rt_Cps_Cs_Amt;
    private DbsField rt_Cps_Redraw_Cnt;
    private DbsField rt_Cps_Redraw_Amt;
    private DbsField rt_Cps_Unused_Cnt;
    private DbsField rt_Cps_Unused_Amt;
    private DbsField rt_Rt_Upd_Source;
    private DbsField rt_Rt_Upd_User;
    private DbsField rt_Rt_Upd_Ccyymmdd;
    private DbsField rt_Rt_Upd_Timn;
    private DbsField pnd_Rt_Super;

    private DbsGroup pnd_Rt_Super__R_Field_4;
    private DbsField pnd_Rt_Super_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Rt_Super_Pnd_Rt_Table_Id;
    private DbsField pnd_Rt_Super_Pnd_Rt_Short_Key;

    private DbsGroup pnd_Rt_Super__R_Field_5;
    private DbsField pnd_Rt_Super_Pnd_Rt_Key;
    private DbsField pnd_Rt_Super_Pnd_Rt_F1;
    private DbsField pnd_Rt_Super_Pnd_Rt_Invrse_Dte;
    private DbsField pnd_Rt_Super_Pnd_Rt_Long_Key;

    private DbsGroup pnd_Rt_Super__R_Field_6;
    private DbsField pnd_Rt_Super_Pnd_Rt_Sequence_Nbr;

    private DbsGroup pnd_Rt_Super__R_Field_7;
    private DbsField pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd;

    private DbsGroup pnd_Rt_Super__R_Field_8;
    private DbsField pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd_A;
    private DbsField pnd_Desc;
    private DbsField pnd_Pymnt_Type;
    private DbsField pnd_Cycle;

    private DbsGroup pnd_Cycle__R_Field_9;
    private DbsField pnd_Cycle__Filler2;
    private DbsField pnd_Cycle_Pnd_Cycle_N;
    private DbsField pnd_Time;
    private DbsField pnd_Stage;
    private DbsField pnd_Status;
    private DbsField pnd_Other_Status_1;
    private DbsField pnd_Other_Status_2;
    private DbsField pnd_Cps_Receive_Date;
    private DbsField pnd_Date_A8;

    private DbsGroup pnd_Date_A8__R_Field_10;
    private DbsField pnd_Date_A8_Pnd_Date_N8;
    private DbsField pnd_Date_X;
    private DbsField pnd_Status_After_Upd;
    private DbsField pnd_Terminate;

    private DbsGroup pnd_Terminate__R_Field_11;
    private DbsField pnd_Terminate_Pnd_Terminate_A;
    private DbsField pnd_Prog_Message;

    private DbsGroup pnd_Error_Ws;
    private DbsField pnd_Error_Ws_Pnd_Call_Progs;
    private DbsField pnd_Error_Ws_Pnd_Invalid_Prev_Funct;
    private DbsField pnd_Error_Ws_Pnd_Expect_Prev_Stat;
    private DbsField pnd_Error_Ws_Pnd_Function;
    private DbsField pnd_Error_Ws_Pnd_Error_Description;

    private DbsGroup pnd_Error_Ws__R_Field_12;
    private DbsField pnd_Error_Ws_Pnd_Error_Desc_Line_1;
    private DbsField pnd_Error_Ws_Pnd_Error_Desc_Line_2;
    private DbsField pnd_Error_Ws_Pnd_Error_Desc_Line_3;
    private DbsField pnd_Error_Ws_Pnd_Error_Desc_Line_4;

    private DbsGroup pnd_Prev_Stat_Ws;
    private DbsField pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Blank;
    private DbsField pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Match;
    private DbsField pnd_First;
    private DbsField pnd_Out_Of_Balance;
    private DbsField pnd_Ws_Last;
    private DbsField pnd_I;
    private DbsField pnd_P;
    private DbsField pnd_Sum_Check_Cnt;
    private DbsField pnd_Sum_Check_Amt;
    private DbsField pnd_Sum_Eft_Cnt;
    private DbsField pnd_Sum_Eft_Amt;
    private DbsField pnd_Sum_Other_Cnt;
    private DbsField pnd_Sum_Other_Amt;
    private DbsField pnd_Sum_Cs_Cnt;
    private DbsField pnd_Sum_Cs_Amt;
    private DbsField pnd_Sum_Redraw_Cnt;
    private DbsField pnd_Sum_Redraw_Amt;
    private DbsField pnd_Tot_Check_Cnt;
    private DbsField pnd_Tot_Check_Amt;
    private DbsField pnd_Tot_Eft_Cnt;
    private DbsField pnd_Tot_Eft_Amt;
    private DbsField pnd_Tot_Other_Cnt;
    private DbsField pnd_Tot_Other_Amt;
    private DbsField pnd_Tot_Cs_Cnt;
    private DbsField pnd_Tot_Cs_Amt;
    private DbsField pnd_Tot_Redraw_Cnt;
    private DbsField pnd_Tot_Redraw_Amt;
    private DbsField pnd_Tot_Unused_Cnt;
    private DbsField pnd_Tot_Unused_Amt;

    private DbsGroup pnd_Total_Processed;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Check_Cnt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Check_Amt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Eft_Cnt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Eft_Amt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Other_Cnt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Other_Amt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Cs_Cnt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Cs_Amt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Redraw_Cnt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Redraw_Amt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Unused_Cnt;
    private DbsField pnd_Total_Processed_Pnd_Prcss_Unused_Amt;
    private DbsField pnd_Orgn;
    private DbsField pnd_First_Cntl;
    private DbsField pnd_Ws_Date_A;

    private DbsGroup pnd_Ws_Date_A__R_Field_13;
    private DbsField pnd_Ws_Date_A_Pnd_Ws_Date_N;
    private DbsField pnd_Ws_Date_D;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpoaorgn = new PdaCpoaorgn(localVariables);
        pdaCpoatotl = new PdaCpoatotl(localVariables);
        pdaCpoa100 = new PdaCpoa100(localVariables);
        ldaCpol101 = new LdaCpol101();
        registerRecord(ldaCpol101);
        registerRecord(ldaCpol101.getVw_ref());
        registerRecord(ldaCpol101.getVw_rtu());
        ldaCpol101a = new LdaCpol101a();
        registerRecord(ldaCpol101a);

        // parameters
        parameters = new DbsRecord();
        pdaCpoa101 = new PdaCpoa101(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_rt = new DataAccessProgramView(new NameInfo("vw_rt", "RT"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        rt_Count_Castrt_Description = vw_rt.getRecord().newFieldInGroup("rt_Count_Castrt_Description", "C*RT-DESCRIPTION", RepeatingFieldStrategy.CAsteriskVariable, 
            "REFERNCE_TABLE_RT_DESCRIPTION");

        rt_Rt_Record = vw_rt.getRecord().newGroupInGroup("RT_RT_RECORD", "RT-RECORD");
        rt_Rt_A_I_Ind = rt_Rt_Record.newFieldInGroup("rt_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt_Rt_Table_Id = rt_Rt_Record.newFieldInGroup("rt_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rt_Rt_Short_Key = rt_Rt_Record.newFieldInGroup("rt_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");

        rt__R_Field_1 = rt_Rt_Record.newGroupInGroup("rt__R_Field_1", "REDEFINE", rt_Rt_Short_Key);
        rt_Cps_Key = rt__R_Field_1.newFieldInGroup("rt_Cps_Key", "CPS-KEY", FieldType.STRING, 11);
        rt__Filler1 = rt__R_Field_1.newFieldInGroup("rt__Filler1", "_FILLER1", FieldType.STRING, 1);
        rt_Cps_Inverse_Dte = rt__R_Field_1.newFieldInGroup("rt_Cps_Inverse_Dte", "CPS-INVERSE-DTE", FieldType.STRING, 8);
        rt_Rt_Long_Key = rt_Rt_Record.newFieldInGroup("rt_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        rt_Rt_Desc1 = rt_Rt_Record.newFieldInGroup("rt_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");

        rt__R_Field_2 = rt_Rt_Record.newGroupInGroup("rt__R_Field_2", "REDEFINE", rt_Rt_Desc1);
        rt_Cps_Ccyymmdd = rt__R_Field_2.newFieldInGroup("rt_Cps_Ccyymmdd", "CPS-CCYYMMDD", FieldType.NUMERIC, 8);
        rt_Cps_Filler1 = rt__R_Field_2.newFieldInGroup("rt_Cps_Filler1", "CPS-FILLER1", FieldType.STRING, 1);
        rt_Cps_Date = rt__R_Field_2.newFieldInGroup("rt_Cps_Date", "CPS-DATE", FieldType.DATE);
        rt_Rt_DescriptionMuGroup = rt_Rt_Record.newGroupInGroup("RT_RT_DESCRIPTIONMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        rt_Rt_Description = rt_Rt_DescriptionMuGroup.newFieldArrayInGroup("rt_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1, 
            2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");

        rt__R_Field_3 = rt_Rt_Record.newGroupInGroup("rt__R_Field_3", "REDEFINE", rt_Rt_Description);

        rt_Gr_Desc = rt__R_Field_3.newGroupArrayInGroup("rt_Gr_Desc", "GR-DESC", new DbsArrayController(1, 2));
        rt_Cps_Time = rt_Gr_Desc.newFieldInGroup("rt_Cps_Time", "CPS-TIME", FieldType.TIME);
        rt_Cps_Cycle = rt_Gr_Desc.newFieldInGroup("rt_Cps_Cycle", "CPS-CYCLE", FieldType.NUMERIC, 4);
        rt_Cps_Status = rt_Gr_Desc.newFieldInGroup("rt_Cps_Status", "CPS-STATUS", FieldType.STRING, 20);
        rt_Cps_Check_Cnt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Check_Cnt", "CPS-CHECK-CNT", FieldType.NUMERIC, 10);
        rt_Cps_Check_Amt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Check_Amt", "CPS-CHECK-AMT", FieldType.NUMERIC, 25, 2);
        rt_Cps_Eft_Cnt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Eft_Cnt", "CPS-EFT-CNT", FieldType.NUMERIC, 10);
        rt_Cps_Eft_Amt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Eft_Amt", "CPS-EFT-AMT", FieldType.NUMERIC, 25, 2);
        rt_Cps_Other_Cnt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Other_Cnt", "CPS-OTHER-CNT", FieldType.NUMERIC, 10);
        rt_Cps_Other_Amt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Other_Amt", "CPS-OTHER-AMT", FieldType.NUMERIC, 25, 2);
        rt_Cps_Cs_Cnt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Cs_Cnt", "CPS-CS-CNT", FieldType.NUMERIC, 10);
        rt_Cps_Cs_Amt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Cs_Amt", "CPS-CS-AMT", FieldType.NUMERIC, 25, 2);
        rt_Cps_Redraw_Cnt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Redraw_Cnt", "CPS-REDRAW-CNT", FieldType.NUMERIC, 10);
        rt_Cps_Redraw_Amt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Redraw_Amt", "CPS-REDRAW-AMT", FieldType.NUMERIC, 25, 2);
        rt_Cps_Unused_Cnt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Unused_Cnt", "CPS-UNUSED-CNT", FieldType.NUMERIC, 10);
        rt_Cps_Unused_Amt = rt_Gr_Desc.newFieldInGroup("rt_Cps_Unused_Amt", "CPS-UNUSED-AMT", FieldType.NUMERIC, 25, 2);
        rt_Rt_Upd_Source = rt_Rt_Record.newFieldInGroup("rt_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        rt_Rt_Upd_User = rt_Rt_Record.newFieldInGroup("rt_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        rt_Rt_Upd_Ccyymmdd = rt_Rt_Record.newFieldInGroup("rt_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_CCYYMMDD");
        rt_Rt_Upd_Timn = rt_Rt_Record.newFieldInGroup("rt_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");
        registerRecord(vw_rt);

        pnd_Rt_Super = localVariables.newFieldInRecord("pnd_Rt_Super", "#RT-SUPER", FieldType.STRING, 66);

        pnd_Rt_Super__R_Field_4 = localVariables.newGroupInRecord("pnd_Rt_Super__R_Field_4", "REDEFINE", pnd_Rt_Super);
        pnd_Rt_Super_Pnd_Rt_A_I_Ind = pnd_Rt_Super__R_Field_4.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super_Pnd_Rt_Table_Id = pnd_Rt_Super__R_Field_4.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 5);
        pnd_Rt_Super_Pnd_Rt_Short_Key = pnd_Rt_Super__R_Field_4.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 20);

        pnd_Rt_Super__R_Field_5 = pnd_Rt_Super__R_Field_4.newGroupInGroup("pnd_Rt_Super__R_Field_5", "REDEFINE", pnd_Rt_Super_Pnd_Rt_Short_Key);
        pnd_Rt_Super_Pnd_Rt_Key = pnd_Rt_Super__R_Field_5.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_Key", "#RT-KEY", FieldType.STRING, 11);
        pnd_Rt_Super_Pnd_Rt_F1 = pnd_Rt_Super__R_Field_5.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_F1", "#RT-F1", FieldType.STRING, 1);
        pnd_Rt_Super_Pnd_Rt_Invrse_Dte = pnd_Rt_Super__R_Field_5.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_Invrse_Dte", "#RT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Rt_Super_Pnd_Rt_Long_Key = pnd_Rt_Super__R_Field_4.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 40);

        pnd_Rt_Super__R_Field_6 = pnd_Rt_Super__R_Field_4.newGroupInGroup("pnd_Rt_Super__R_Field_6", "REDEFINE", pnd_Rt_Super_Pnd_Rt_Long_Key);
        pnd_Rt_Super_Pnd_Rt_Sequence_Nbr = pnd_Rt_Super__R_Field_6.newFieldInGroup("pnd_Rt_Super_Pnd_Rt_Sequence_Nbr", "#RT-SEQUENCE-NBR", FieldType.NUMERIC, 
            7);

        pnd_Rt_Super__R_Field_7 = pnd_Rt_Super__R_Field_4.newGroupInGroup("pnd_Rt_Super__R_Field_7", "REDEFINE", pnd_Rt_Super_Pnd_Rt_Long_Key);
        pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd = pnd_Rt_Super__R_Field_7.newFieldInGroup("pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd", "#NEW-REC-CCYYMMDD", FieldType.NUMERIC, 
            8);

        pnd_Rt_Super__R_Field_8 = pnd_Rt_Super__R_Field_7.newGroupInGroup("pnd_Rt_Super__R_Field_8", "REDEFINE", pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd);
        pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd_A = pnd_Rt_Super__R_Field_8.newFieldInGroup("pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd_A", "#NEW-REC-CCYYMMDD-A", FieldType.STRING, 
            8);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 18);
        pnd_Pymnt_Type = localVariables.newFieldInRecord("pnd_Pymnt_Type", "#PYMNT-TYPE", FieldType.STRING, 10);
        pnd_Cycle = localVariables.newFieldInRecord("pnd_Cycle", "#CYCLE", FieldType.STRING, 5);

        pnd_Cycle__R_Field_9 = localVariables.newGroupInRecord("pnd_Cycle__R_Field_9", "REDEFINE", pnd_Cycle);
        pnd_Cycle__Filler2 = pnd_Cycle__R_Field_9.newFieldInGroup("pnd_Cycle__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Cycle_Pnd_Cycle_N = pnd_Cycle__R_Field_9.newFieldInGroup("pnd_Cycle_Pnd_Cycle_N", "#CYCLE-N", FieldType.NUMERIC, 4);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.STRING, 11);
        pnd_Stage = localVariables.newFieldInRecord("pnd_Stage", "#STAGE", FieldType.STRING, 8);
        pnd_Status = localVariables.newFieldInRecord("pnd_Status", "#STATUS", FieldType.STRING, 36);
        pnd_Other_Status_1 = localVariables.newFieldInRecord("pnd_Other_Status_1", "#OTHER-STATUS-1", FieldType.STRING, 36);
        pnd_Other_Status_2 = localVariables.newFieldInRecord("pnd_Other_Status_2", "#OTHER-STATUS-2", FieldType.STRING, 36);
        pnd_Cps_Receive_Date = localVariables.newFieldInRecord("pnd_Cps_Receive_Date", "#CPS-RECEIVE-DATE", FieldType.STRING, 8);
        pnd_Date_A8 = localVariables.newFieldInRecord("pnd_Date_A8", "#DATE-A8", FieldType.STRING, 8);

        pnd_Date_A8__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_A8__R_Field_10", "REDEFINE", pnd_Date_A8);
        pnd_Date_A8_Pnd_Date_N8 = pnd_Date_A8__R_Field_10.newFieldInGroup("pnd_Date_A8_Pnd_Date_N8", "#DATE-N8", FieldType.NUMERIC, 8);
        pnd_Date_X = localVariables.newFieldInRecord("pnd_Date_X", "#DATE-X", FieldType.DATE);
        pnd_Status_After_Upd = localVariables.newFieldInRecord("pnd_Status_After_Upd", "#STATUS-AFTER-UPD", FieldType.STRING, 20);
        pnd_Terminate = localVariables.newFieldInRecord("pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 4);

        pnd_Terminate__R_Field_11 = localVariables.newGroupInRecord("pnd_Terminate__R_Field_11", "REDEFINE", pnd_Terminate);
        pnd_Terminate_Pnd_Terminate_A = pnd_Terminate__R_Field_11.newFieldInGroup("pnd_Terminate_Pnd_Terminate_A", "#TERMINATE-A", FieldType.STRING, 4);
        pnd_Prog_Message = localVariables.newFieldInRecord("pnd_Prog_Message", "#PROG-MESSAGE", FieldType.STRING, 108);

        pnd_Error_Ws = localVariables.newGroupInRecord("pnd_Error_Ws", "#ERROR-WS");
        pnd_Error_Ws_Pnd_Call_Progs = pnd_Error_Ws.newFieldInGroup("pnd_Error_Ws_Pnd_Call_Progs", "#CALL-PROGS", FieldType.STRING, 40);
        pnd_Error_Ws_Pnd_Invalid_Prev_Funct = pnd_Error_Ws.newFieldInGroup("pnd_Error_Ws_Pnd_Invalid_Prev_Funct", "#INVALID-PREV-FUNCT", FieldType.STRING, 
            40);
        pnd_Error_Ws_Pnd_Expect_Prev_Stat = pnd_Error_Ws.newFieldInGroup("pnd_Error_Ws_Pnd_Expect_Prev_Stat", "#EXPECT-PREV-STAT", FieldType.STRING, 40);
        pnd_Error_Ws_Pnd_Function = pnd_Error_Ws.newFieldInGroup("pnd_Error_Ws_Pnd_Function", "#FUNCTION", FieldType.STRING, 21);
        pnd_Error_Ws_Pnd_Error_Description = pnd_Error_Ws.newFieldInGroup("pnd_Error_Ws_Pnd_Error_Description", "#ERROR-DESCRIPTION", FieldType.STRING, 
            250);

        pnd_Error_Ws__R_Field_12 = pnd_Error_Ws.newGroupInGroup("pnd_Error_Ws__R_Field_12", "REDEFINE", pnd_Error_Ws_Pnd_Error_Description);
        pnd_Error_Ws_Pnd_Error_Desc_Line_1 = pnd_Error_Ws__R_Field_12.newFieldInGroup("pnd_Error_Ws_Pnd_Error_Desc_Line_1", "#ERROR-DESC-LINE-1", FieldType.STRING, 
            75);
        pnd_Error_Ws_Pnd_Error_Desc_Line_2 = pnd_Error_Ws__R_Field_12.newFieldInGroup("pnd_Error_Ws_Pnd_Error_Desc_Line_2", "#ERROR-DESC-LINE-2", FieldType.STRING, 
            75);
        pnd_Error_Ws_Pnd_Error_Desc_Line_3 = pnd_Error_Ws__R_Field_12.newFieldInGroup("pnd_Error_Ws_Pnd_Error_Desc_Line_3", "#ERROR-DESC-LINE-3", FieldType.STRING, 
            75);
        pnd_Error_Ws_Pnd_Error_Desc_Line_4 = pnd_Error_Ws__R_Field_12.newFieldInGroup("pnd_Error_Ws_Pnd_Error_Desc_Line_4", "#ERROR-DESC-LINE-4", FieldType.STRING, 
            25);

        pnd_Prev_Stat_Ws = localVariables.newGroupInRecord("pnd_Prev_Stat_Ws", "#PREV-STAT-WS");
        pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Blank = pnd_Prev_Stat_Ws.newFieldInGroup("pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Blank", "#PREV-FUNCT-BLANK", FieldType.NUMERIC, 
            2);
        pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Match = pnd_Prev_Stat_Ws.newFieldInGroup("pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Match", "#PREV-FUNCT-MATCH", FieldType.BOOLEAN, 
            1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Out_Of_Balance = localVariables.newFieldInRecord("pnd_Out_Of_Balance", "#OUT-OF-BALANCE", FieldType.BOOLEAN, 1);
        pnd_Ws_Last = localVariables.newFieldInRecord("pnd_Ws_Last", "#WS-LAST", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 2);
        pnd_Sum_Check_Cnt = localVariables.newFieldInRecord("pnd_Sum_Check_Cnt", "#SUM-CHECK-CNT", FieldType.NUMERIC, 11);
        pnd_Sum_Check_Amt = localVariables.newFieldInRecord("pnd_Sum_Check_Amt", "#SUM-CHECK-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Sum_Eft_Cnt = localVariables.newFieldInRecord("pnd_Sum_Eft_Cnt", "#SUM-EFT-CNT", FieldType.NUMERIC, 11);
        pnd_Sum_Eft_Amt = localVariables.newFieldInRecord("pnd_Sum_Eft_Amt", "#SUM-EFT-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Sum_Other_Cnt = localVariables.newFieldInRecord("pnd_Sum_Other_Cnt", "#SUM-OTHER-CNT", FieldType.NUMERIC, 11);
        pnd_Sum_Other_Amt = localVariables.newFieldInRecord("pnd_Sum_Other_Amt", "#SUM-OTHER-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Sum_Cs_Cnt = localVariables.newFieldInRecord("pnd_Sum_Cs_Cnt", "#SUM-CS-CNT", FieldType.NUMERIC, 11);
        pnd_Sum_Cs_Amt = localVariables.newFieldInRecord("pnd_Sum_Cs_Amt", "#SUM-CS-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Sum_Redraw_Cnt = localVariables.newFieldInRecord("pnd_Sum_Redraw_Cnt", "#SUM-REDRAW-CNT", FieldType.NUMERIC, 11);
        pnd_Sum_Redraw_Amt = localVariables.newFieldInRecord("pnd_Sum_Redraw_Amt", "#SUM-REDRAW-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Tot_Check_Cnt = localVariables.newFieldInRecord("pnd_Tot_Check_Cnt", "#TOT-CHECK-CNT", FieldType.NUMERIC, 11);
        pnd_Tot_Check_Amt = localVariables.newFieldInRecord("pnd_Tot_Check_Amt", "#TOT-CHECK-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Tot_Eft_Cnt = localVariables.newFieldInRecord("pnd_Tot_Eft_Cnt", "#TOT-EFT-CNT", FieldType.NUMERIC, 11);
        pnd_Tot_Eft_Amt = localVariables.newFieldInRecord("pnd_Tot_Eft_Amt", "#TOT-EFT-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Tot_Other_Cnt = localVariables.newFieldInRecord("pnd_Tot_Other_Cnt", "#TOT-OTHER-CNT", FieldType.NUMERIC, 11);
        pnd_Tot_Other_Amt = localVariables.newFieldInRecord("pnd_Tot_Other_Amt", "#TOT-OTHER-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Tot_Cs_Cnt = localVariables.newFieldInRecord("pnd_Tot_Cs_Cnt", "#TOT-CS-CNT", FieldType.NUMERIC, 11);
        pnd_Tot_Cs_Amt = localVariables.newFieldInRecord("pnd_Tot_Cs_Amt", "#TOT-CS-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Tot_Redraw_Cnt = localVariables.newFieldInRecord("pnd_Tot_Redraw_Cnt", "#TOT-REDRAW-CNT", FieldType.NUMERIC, 11);
        pnd_Tot_Redraw_Amt = localVariables.newFieldInRecord("pnd_Tot_Redraw_Amt", "#TOT-REDRAW-AMT", FieldType.NUMERIC, 25, 2);
        pnd_Tot_Unused_Cnt = localVariables.newFieldInRecord("pnd_Tot_Unused_Cnt", "#TOT-UNUSED-CNT", FieldType.NUMERIC, 11);
        pnd_Tot_Unused_Amt = localVariables.newFieldInRecord("pnd_Tot_Unused_Amt", "#TOT-UNUSED-AMT", FieldType.NUMERIC, 25, 2);

        pnd_Total_Processed = localVariables.newGroupInRecord("pnd_Total_Processed", "#TOTAL-PROCESSED");
        pnd_Total_Processed_Pnd_Prcss_Check_Cnt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Check_Cnt", "#PRCSS-CHECK-CNT", FieldType.NUMERIC, 
            11);
        pnd_Total_Processed_Pnd_Prcss_Check_Amt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Check_Amt", "#PRCSS-CHECK-AMT", FieldType.NUMERIC, 
            25, 2);
        pnd_Total_Processed_Pnd_Prcss_Eft_Cnt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Eft_Cnt", "#PRCSS-EFT-CNT", FieldType.NUMERIC, 
            11);
        pnd_Total_Processed_Pnd_Prcss_Eft_Amt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Eft_Amt", "#PRCSS-EFT-AMT", FieldType.NUMERIC, 
            25, 2);
        pnd_Total_Processed_Pnd_Prcss_Other_Cnt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Other_Cnt", "#PRCSS-OTHER-CNT", FieldType.NUMERIC, 
            11);
        pnd_Total_Processed_Pnd_Prcss_Other_Amt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Other_Amt", "#PRCSS-OTHER-AMT", FieldType.NUMERIC, 
            25, 2);
        pnd_Total_Processed_Pnd_Prcss_Cs_Cnt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Cs_Cnt", "#PRCSS-CS-CNT", FieldType.NUMERIC, 
            11);
        pnd_Total_Processed_Pnd_Prcss_Cs_Amt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Cs_Amt", "#PRCSS-CS-AMT", FieldType.NUMERIC, 
            25, 2);
        pnd_Total_Processed_Pnd_Prcss_Redraw_Cnt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Redraw_Cnt", "#PRCSS-REDRAW-CNT", 
            FieldType.NUMERIC, 11);
        pnd_Total_Processed_Pnd_Prcss_Redraw_Amt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Redraw_Amt", "#PRCSS-REDRAW-AMT", 
            FieldType.NUMERIC, 25, 2);
        pnd_Total_Processed_Pnd_Prcss_Unused_Cnt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Unused_Cnt", "#PRCSS-UNUSED-CNT", 
            FieldType.NUMERIC, 11);
        pnd_Total_Processed_Pnd_Prcss_Unused_Amt = pnd_Total_Processed.newFieldInGroup("pnd_Total_Processed_Pnd_Prcss_Unused_Amt", "#PRCSS-UNUSED-AMT", 
            FieldType.NUMERIC, 25, 2);
        pnd_Orgn = localVariables.newFieldInRecord("pnd_Orgn", "#ORGN", FieldType.STRING, 4);
        pnd_First_Cntl = localVariables.newFieldInRecord("pnd_First_Cntl", "#FIRST-CNTL", FieldType.BOOLEAN, 1);
        pnd_Ws_Date_A = localVariables.newFieldInRecord("pnd_Ws_Date_A", "#WS-DATE-A", FieldType.STRING, 8);

        pnd_Ws_Date_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Ws_Date_A__R_Field_13", "REDEFINE", pnd_Ws_Date_A);
        pnd_Ws_Date_A_Pnd_Ws_Date_N = pnd_Ws_Date_A__R_Field_13.newFieldInGroup("pnd_Ws_Date_A_Pnd_Ws_Date_N", "#WS-DATE-N", FieldType.NUMERIC, 8);
        pnd_Ws_Date_D = localVariables.newFieldInRecord("pnd_Ws_Date_D", "#WS-DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rt.reset();

        ldaCpol101.initializeValues();
        ldaCpol101a.initializeValues();

        localVariables.reset();
        pnd_Rt_Super.setInitialValue("ACCNTV ");
        pnd_Pymnt_Type.setInitialValue(" ");
        pnd_Other_Status_1.setInitialValue(" ");
        pnd_Other_Status_2.setInitialValue(" ");
        pnd_Terminate.setInitialValue(0);
        pnd_Prog_Message.setInitialValue(" ");
        pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Blank.setInitialValue(0);
        pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Match.setInitialValue(false);
        pnd_First.setInitialValue(true);
        pnd_First_Cntl.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cpvn101() throws Exception
    {
        super("Cpvn101");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt10, 10);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 58 LS = 160 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 10 ) PS = 58 LS = 134 ZP = ON SF = 2
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 10 )
        //* *====================================================================**
        //* *                      M A I N   P R O G R A M                       **
        //* *====================================================================**
        //* *                                                                    **
        //* * ------------------------ VERIFY BATCH CALL ----------------------- **
        if (condition(Global.getDEVICE().notEquals("BATCH")))                                                                                                             //Natural: IF *DEVICE NE 'BATCH'
        {
            pnd_Terminate.setValue(70);                                                                                                                                   //Natural: ASSIGN #TERMINATE := 70
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
            sub_Z99_Common_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pdaCpoa101.getCpoa101_Procedure(),pdaCpoa101.getCpoa101_Module(),pdaCpoa101.getCpoa101_Function(),pdaCpoa101.getCpoa101_Report(),       //Natural: WRITE '=' CPOA101 /
            pdaCpoa101.getCpoa101_Sequence_Nbr(),pdaCpoa101.getCpoa101_Control_Key(),pdaCpoa101.getCpoa101_Update_Time(),pdaCpoa101.getCpoa101_Cycle_Nbr(),
            pdaCpoa101.getCpoa101_Current_Status(),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(12),pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(15),pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(18),pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),pdaCpoa101.getCpoa101_Check_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),pdaCpoa101.getCpoa101_Check_Amt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),pdaCpoa101.getCpoa101_Check_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(12),pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(15),pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(18),pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),pdaCpoa101.getCpoa101_Check_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),pdaCpoa101.getCpoa101_Check_Amt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),pdaCpoa101.getCpoa101_Check_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(1),pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(5),pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(9),pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(12),pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(15),pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(18),pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(1),pdaCpoa101.getCpoa101_Other_Amt().getValue(2),pdaCpoa101.getCpoa101_Other_Amt().getValue(3),pdaCpoa101.getCpoa101_Other_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(5),pdaCpoa101.getCpoa101_Other_Amt().getValue(6),pdaCpoa101.getCpoa101_Other_Amt().getValue(7),pdaCpoa101.getCpoa101_Other_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(9),pdaCpoa101.getCpoa101_Other_Amt().getValue(10),pdaCpoa101.getCpoa101_Other_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(12),pdaCpoa101.getCpoa101_Other_Amt().getValue(13),pdaCpoa101.getCpoa101_Other_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(15),pdaCpoa101.getCpoa101_Other_Amt().getValue(16),pdaCpoa101.getCpoa101_Other_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(18),pdaCpoa101.getCpoa101_Other_Amt().getValue(19),pdaCpoa101.getCpoa101_Other_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),pdaCpoa101.getCpoa101_Check_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),pdaCpoa101.getCpoa101_Check_Amt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),pdaCpoa101.getCpoa101_Check_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),pdaCpoa101.getCpoa101_Other_Amt().getValue(1),pdaCpoa101.getCpoa101_Other_Amt().getValue(2),pdaCpoa101.getCpoa101_Other_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(4),pdaCpoa101.getCpoa101_Other_Amt().getValue(5),pdaCpoa101.getCpoa101_Other_Amt().getValue(6),pdaCpoa101.getCpoa101_Other_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(8),pdaCpoa101.getCpoa101_Other_Amt().getValue(9),pdaCpoa101.getCpoa101_Other_Amt().getValue(10),pdaCpoa101.getCpoa101_Other_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(12),pdaCpoa101.getCpoa101_Other_Amt().getValue(13),pdaCpoa101.getCpoa101_Other_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(15),pdaCpoa101.getCpoa101_Other_Amt().getValue(16),pdaCpoa101.getCpoa101_Other_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(18),pdaCpoa101.getCpoa101_Other_Amt().getValue(19),pdaCpoa101.getCpoa101_Other_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(1),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(2),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(3),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(5),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(6),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(7),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(9),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(10),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(11),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(13),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(14),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(15),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(17),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(18),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(19),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),pdaCpoa101.getCpoa101_Check_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),pdaCpoa101.getCpoa101_Check_Amt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),pdaCpoa101.getCpoa101_Check_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),pdaCpoa101.getCpoa101_Other_Amt().getValue(1),pdaCpoa101.getCpoa101_Other_Amt().getValue(2),pdaCpoa101.getCpoa101_Other_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(4),pdaCpoa101.getCpoa101_Other_Amt().getValue(5),pdaCpoa101.getCpoa101_Other_Amt().getValue(6),pdaCpoa101.getCpoa101_Other_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(8),pdaCpoa101.getCpoa101_Other_Amt().getValue(9),pdaCpoa101.getCpoa101_Other_Amt().getValue(10),pdaCpoa101.getCpoa101_Other_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(12),pdaCpoa101.getCpoa101_Other_Amt().getValue(13),pdaCpoa101.getCpoa101_Other_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(15),pdaCpoa101.getCpoa101_Other_Amt().getValue(16),pdaCpoa101.getCpoa101_Other_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(18),pdaCpoa101.getCpoa101_Other_Amt().getValue(19),pdaCpoa101.getCpoa101_Other_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(1),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(2),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(3),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(5),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(6),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(7),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(9),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(10),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(11),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(13),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(14),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(15),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(17),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(18),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(19),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(1),pdaCpoa101.getCpoa101_Cs_Amt().getValue(2),pdaCpoa101.getCpoa101_Cs_Amt().getValue(3),pdaCpoa101.getCpoa101_Cs_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(5),pdaCpoa101.getCpoa101_Cs_Amt().getValue(6),pdaCpoa101.getCpoa101_Cs_Amt().getValue(7),pdaCpoa101.getCpoa101_Cs_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(9),pdaCpoa101.getCpoa101_Cs_Amt().getValue(10),pdaCpoa101.getCpoa101_Cs_Amt().getValue(11),pdaCpoa101.getCpoa101_Cs_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(13),pdaCpoa101.getCpoa101_Cs_Amt().getValue(14),pdaCpoa101.getCpoa101_Cs_Amt().getValue(15),pdaCpoa101.getCpoa101_Cs_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(17),pdaCpoa101.getCpoa101_Cs_Amt().getValue(18),pdaCpoa101.getCpoa101_Cs_Amt().getValue(19),pdaCpoa101.getCpoa101_Cs_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),pdaCpoa101.getCpoa101_Check_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),pdaCpoa101.getCpoa101_Check_Amt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),pdaCpoa101.getCpoa101_Check_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),pdaCpoa101.getCpoa101_Other_Amt().getValue(1),pdaCpoa101.getCpoa101_Other_Amt().getValue(2),pdaCpoa101.getCpoa101_Other_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(4),pdaCpoa101.getCpoa101_Other_Amt().getValue(5),pdaCpoa101.getCpoa101_Other_Amt().getValue(6),pdaCpoa101.getCpoa101_Other_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(8),pdaCpoa101.getCpoa101_Other_Amt().getValue(9),pdaCpoa101.getCpoa101_Other_Amt().getValue(10),pdaCpoa101.getCpoa101_Other_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(12),pdaCpoa101.getCpoa101_Other_Amt().getValue(13),pdaCpoa101.getCpoa101_Other_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(15),pdaCpoa101.getCpoa101_Other_Amt().getValue(16),pdaCpoa101.getCpoa101_Other_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(18),pdaCpoa101.getCpoa101_Other_Amt().getValue(19),pdaCpoa101.getCpoa101_Other_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(1),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(2),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(3),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(5),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(6),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(7),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(9),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(10),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(11),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(13),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(14),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(15),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(17),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(18),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(19),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(1),pdaCpoa101.getCpoa101_Cs_Amt().getValue(2),pdaCpoa101.getCpoa101_Cs_Amt().getValue(3),pdaCpoa101.getCpoa101_Cs_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(5),pdaCpoa101.getCpoa101_Cs_Amt().getValue(6),pdaCpoa101.getCpoa101_Cs_Amt().getValue(7),pdaCpoa101.getCpoa101_Cs_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(9),pdaCpoa101.getCpoa101_Cs_Amt().getValue(10),pdaCpoa101.getCpoa101_Cs_Amt().getValue(11),pdaCpoa101.getCpoa101_Cs_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(13),pdaCpoa101.getCpoa101_Cs_Amt().getValue(14),pdaCpoa101.getCpoa101_Cs_Amt().getValue(15),pdaCpoa101.getCpoa101_Cs_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(17),pdaCpoa101.getCpoa101_Cs_Amt().getValue(18),pdaCpoa101.getCpoa101_Cs_Amt().getValue(19),pdaCpoa101.getCpoa101_Cs_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(1),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(2),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(3),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(4),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(5),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(7),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(8),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(10),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(11),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(13),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(14),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(16),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(17),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(19),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(20),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(12),pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(15),pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(18),pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(1),pdaCpoa101.getCpoa101_Other_Amt().getValue(2),pdaCpoa101.getCpoa101_Other_Amt().getValue(3),pdaCpoa101.getCpoa101_Other_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(5),pdaCpoa101.getCpoa101_Other_Amt().getValue(6),pdaCpoa101.getCpoa101_Other_Amt().getValue(7),pdaCpoa101.getCpoa101_Other_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(9),pdaCpoa101.getCpoa101_Other_Amt().getValue(10),pdaCpoa101.getCpoa101_Other_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(12),pdaCpoa101.getCpoa101_Other_Amt().getValue(13),pdaCpoa101.getCpoa101_Other_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(15),pdaCpoa101.getCpoa101_Other_Amt().getValue(16),pdaCpoa101.getCpoa101_Other_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(18),pdaCpoa101.getCpoa101_Other_Amt().getValue(19),pdaCpoa101.getCpoa101_Other_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(1),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(2),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(3),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(5),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(6),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(7),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(9),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(10),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(11),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(13),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(14),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(15),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(17),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(18),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(19),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(1),pdaCpoa101.getCpoa101_Cs_Amt().getValue(2),pdaCpoa101.getCpoa101_Cs_Amt().getValue(3),pdaCpoa101.getCpoa101_Cs_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(5),pdaCpoa101.getCpoa101_Cs_Amt().getValue(6),pdaCpoa101.getCpoa101_Cs_Amt().getValue(7),pdaCpoa101.getCpoa101_Cs_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(9),pdaCpoa101.getCpoa101_Cs_Amt().getValue(10),pdaCpoa101.getCpoa101_Cs_Amt().getValue(11),pdaCpoa101.getCpoa101_Cs_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(13),pdaCpoa101.getCpoa101_Cs_Amt().getValue(14),pdaCpoa101.getCpoa101_Cs_Amt().getValue(15),pdaCpoa101.getCpoa101_Cs_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(17),pdaCpoa101.getCpoa101_Cs_Amt().getValue(18),pdaCpoa101.getCpoa101_Cs_Amt().getValue(19),pdaCpoa101.getCpoa101_Cs_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(1),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(2),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(3),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(4),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(5),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(7),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(8),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(10),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(11),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(13),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(14),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(16),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(17),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(19),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(20),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(2),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(3),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(5),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(6),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(8),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(9),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(11),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(12),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(13),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(14),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(15),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(17),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(18),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(19),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(20),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(1),pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(5),pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(9),pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(12),pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(15),pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(18),pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(1),pdaCpoa101.getCpoa101_Other_Amt().getValue(2),pdaCpoa101.getCpoa101_Other_Amt().getValue(3),pdaCpoa101.getCpoa101_Other_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(5),pdaCpoa101.getCpoa101_Other_Amt().getValue(6),pdaCpoa101.getCpoa101_Other_Amt().getValue(7),pdaCpoa101.getCpoa101_Other_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(9),pdaCpoa101.getCpoa101_Other_Amt().getValue(10),pdaCpoa101.getCpoa101_Other_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(12),pdaCpoa101.getCpoa101_Other_Amt().getValue(13),pdaCpoa101.getCpoa101_Other_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(15),pdaCpoa101.getCpoa101_Other_Amt().getValue(16),pdaCpoa101.getCpoa101_Other_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(18),pdaCpoa101.getCpoa101_Other_Amt().getValue(19),pdaCpoa101.getCpoa101_Other_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(1),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(2),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(3),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(5),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(6),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(7),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(9),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(10),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(11),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(13),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(14),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(15),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(17),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(18),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(19),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(1),pdaCpoa101.getCpoa101_Cs_Amt().getValue(2),pdaCpoa101.getCpoa101_Cs_Amt().getValue(3),pdaCpoa101.getCpoa101_Cs_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(5),pdaCpoa101.getCpoa101_Cs_Amt().getValue(6),pdaCpoa101.getCpoa101_Cs_Amt().getValue(7),pdaCpoa101.getCpoa101_Cs_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(9),pdaCpoa101.getCpoa101_Cs_Amt().getValue(10),pdaCpoa101.getCpoa101_Cs_Amt().getValue(11),pdaCpoa101.getCpoa101_Cs_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(13),pdaCpoa101.getCpoa101_Cs_Amt().getValue(14),pdaCpoa101.getCpoa101_Cs_Amt().getValue(15),pdaCpoa101.getCpoa101_Cs_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(17),pdaCpoa101.getCpoa101_Cs_Amt().getValue(18),pdaCpoa101.getCpoa101_Cs_Amt().getValue(19),pdaCpoa101.getCpoa101_Cs_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(1),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(2),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(3),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(4),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(5),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(7),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(8),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(10),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(11),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(13),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(14),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(16),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(17),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(19),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(20),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(2),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(3),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(5),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(6),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(8),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(9),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(11),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(12),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(13),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(14),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(15),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(17),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(18),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(19),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(20),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(1),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(3),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(4),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(6),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(7),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(9),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(10),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(11),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(12),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(13),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(15),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(16),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(17),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(18),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(19),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(2),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(3),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(4),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(5),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(6),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(7),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(8),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(9),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(10),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(11),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(12),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(13),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(14),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(15),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(16),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(17),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(18),
            pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(19),pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(20),pdaCpoa101.getCpoa101_Check_Cnt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(2),pdaCpoa101.getCpoa101_Check_Cnt().getValue(3),pdaCpoa101.getCpoa101_Check_Cnt().getValue(4),pdaCpoa101.getCpoa101_Check_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(6),pdaCpoa101.getCpoa101_Check_Cnt().getValue(7),pdaCpoa101.getCpoa101_Check_Cnt().getValue(8),pdaCpoa101.getCpoa101_Check_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(10),pdaCpoa101.getCpoa101_Check_Cnt().getValue(11),pdaCpoa101.getCpoa101_Check_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(13),pdaCpoa101.getCpoa101_Check_Cnt().getValue(14),pdaCpoa101.getCpoa101_Check_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(16),pdaCpoa101.getCpoa101_Check_Cnt().getValue(17),pdaCpoa101.getCpoa101_Check_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Cnt().getValue(19),pdaCpoa101.getCpoa101_Check_Cnt().getValue(20),pdaCpoa101.getCpoa101_Check_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(2),pdaCpoa101.getCpoa101_Check_Amt().getValue(3),pdaCpoa101.getCpoa101_Check_Amt().getValue(4),pdaCpoa101.getCpoa101_Check_Amt().getValue(5),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(6),pdaCpoa101.getCpoa101_Check_Amt().getValue(7),pdaCpoa101.getCpoa101_Check_Amt().getValue(8),pdaCpoa101.getCpoa101_Check_Amt().getValue(9),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(10),pdaCpoa101.getCpoa101_Check_Amt().getValue(11),pdaCpoa101.getCpoa101_Check_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(13),pdaCpoa101.getCpoa101_Check_Amt().getValue(14),pdaCpoa101.getCpoa101_Check_Amt().getValue(15),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(16),pdaCpoa101.getCpoa101_Check_Amt().getValue(17),pdaCpoa101.getCpoa101_Check_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Check_Amt().getValue(19),pdaCpoa101.getCpoa101_Check_Amt().getValue(20),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(3),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(4),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(5),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(7),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(8),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(9),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(11),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(12),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(13),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(15),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(16),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(17),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Cnt().getValue(19),pdaCpoa101.getCpoa101_Eft_Cnt().getValue(20),pdaCpoa101.getCpoa101_Eft_Amt().getValue(1),pdaCpoa101.getCpoa101_Eft_Amt().getValue(2),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(3),pdaCpoa101.getCpoa101_Eft_Amt().getValue(4),pdaCpoa101.getCpoa101_Eft_Amt().getValue(5),pdaCpoa101.getCpoa101_Eft_Amt().getValue(6),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(7),pdaCpoa101.getCpoa101_Eft_Amt().getValue(8),pdaCpoa101.getCpoa101_Eft_Amt().getValue(9),pdaCpoa101.getCpoa101_Eft_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(11),pdaCpoa101.getCpoa101_Eft_Amt().getValue(12),pdaCpoa101.getCpoa101_Eft_Amt().getValue(13),pdaCpoa101.getCpoa101_Eft_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(15),pdaCpoa101.getCpoa101_Eft_Amt().getValue(16),pdaCpoa101.getCpoa101_Eft_Amt().getValue(17),pdaCpoa101.getCpoa101_Eft_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Eft_Amt().getValue(19),pdaCpoa101.getCpoa101_Eft_Amt().getValue(20),pdaCpoa101.getCpoa101_Other_Cnt().getValue(1),pdaCpoa101.getCpoa101_Other_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(3),pdaCpoa101.getCpoa101_Other_Cnt().getValue(4),pdaCpoa101.getCpoa101_Other_Cnt().getValue(5),pdaCpoa101.getCpoa101_Other_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(7),pdaCpoa101.getCpoa101_Other_Cnt().getValue(8),pdaCpoa101.getCpoa101_Other_Cnt().getValue(9),pdaCpoa101.getCpoa101_Other_Cnt().getValue(10),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(11),pdaCpoa101.getCpoa101_Other_Cnt().getValue(12),pdaCpoa101.getCpoa101_Other_Cnt().getValue(13),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(14),pdaCpoa101.getCpoa101_Other_Cnt().getValue(15),pdaCpoa101.getCpoa101_Other_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(17),pdaCpoa101.getCpoa101_Other_Cnt().getValue(18),pdaCpoa101.getCpoa101_Other_Cnt().getValue(19),
            pdaCpoa101.getCpoa101_Other_Cnt().getValue(20),pdaCpoa101.getCpoa101_Other_Amt().getValue(1),pdaCpoa101.getCpoa101_Other_Amt().getValue(2),pdaCpoa101.getCpoa101_Other_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(4),pdaCpoa101.getCpoa101_Other_Amt().getValue(5),pdaCpoa101.getCpoa101_Other_Amt().getValue(6),pdaCpoa101.getCpoa101_Other_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(8),pdaCpoa101.getCpoa101_Other_Amt().getValue(9),pdaCpoa101.getCpoa101_Other_Amt().getValue(10),pdaCpoa101.getCpoa101_Other_Amt().getValue(11),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(12),pdaCpoa101.getCpoa101_Other_Amt().getValue(13),pdaCpoa101.getCpoa101_Other_Amt().getValue(14),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(15),pdaCpoa101.getCpoa101_Other_Amt().getValue(16),pdaCpoa101.getCpoa101_Other_Amt().getValue(17),
            pdaCpoa101.getCpoa101_Other_Amt().getValue(18),pdaCpoa101.getCpoa101_Other_Amt().getValue(19),pdaCpoa101.getCpoa101_Other_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(1),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(2),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(3),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(5),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(6),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(7),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(9),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(10),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(11),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(13),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(14),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(15),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Cnt().getValue(17),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(18),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(19),pdaCpoa101.getCpoa101_Cs_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(1),pdaCpoa101.getCpoa101_Cs_Amt().getValue(2),pdaCpoa101.getCpoa101_Cs_Amt().getValue(3),pdaCpoa101.getCpoa101_Cs_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(5),pdaCpoa101.getCpoa101_Cs_Amt().getValue(6),pdaCpoa101.getCpoa101_Cs_Amt().getValue(7),pdaCpoa101.getCpoa101_Cs_Amt().getValue(8),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(9),pdaCpoa101.getCpoa101_Cs_Amt().getValue(10),pdaCpoa101.getCpoa101_Cs_Amt().getValue(11),pdaCpoa101.getCpoa101_Cs_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(13),pdaCpoa101.getCpoa101_Cs_Amt().getValue(14),pdaCpoa101.getCpoa101_Cs_Amt().getValue(15),pdaCpoa101.getCpoa101_Cs_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Cs_Amt().getValue(17),pdaCpoa101.getCpoa101_Cs_Amt().getValue(18),pdaCpoa101.getCpoa101_Cs_Amt().getValue(19),pdaCpoa101.getCpoa101_Cs_Amt().getValue(20),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(1),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(2),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(3),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(4),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(5),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(6),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(7),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(8),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(9),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(10),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(11),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(12),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(13),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(14),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(15),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(16),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(17),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(18),
            pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(19),pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(20),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(1),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(2),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(3),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(4),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(5),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(6),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(7),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(8),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(9),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(10),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(11),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(12),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(13),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(14),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(15),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(16),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(17),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(18),pdaCpoa101.getCpoa101_Redraw_Amt().getValue(19),
            pdaCpoa101.getCpoa101_Redraw_Amt().getValue(20),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(1),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(2),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(3),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(4),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(5),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(6),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(7),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(8),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(9),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(10),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(11),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(12),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(13),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(14),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(15),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(16),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(17),
            pdaCpoa101.getCpoa101_Unused_Cnt().getValue(18),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(19),pdaCpoa101.getCpoa101_Unused_Cnt().getValue(20),
            pdaCpoa101.getCpoa101_Unused_Amt().getValue(1),pdaCpoa101.getCpoa101_Unused_Amt().getValue(2),pdaCpoa101.getCpoa101_Unused_Amt().getValue(3),
            pdaCpoa101.getCpoa101_Unused_Amt().getValue(4),pdaCpoa101.getCpoa101_Unused_Amt().getValue(5),pdaCpoa101.getCpoa101_Unused_Amt().getValue(6),
            pdaCpoa101.getCpoa101_Unused_Amt().getValue(7),pdaCpoa101.getCpoa101_Unused_Amt().getValue(8),pdaCpoa101.getCpoa101_Unused_Amt().getValue(9),
            pdaCpoa101.getCpoa101_Unused_Amt().getValue(10),pdaCpoa101.getCpoa101_Unused_Amt().getValue(11),pdaCpoa101.getCpoa101_Unused_Amt().getValue(12),
            pdaCpoa101.getCpoa101_Unused_Amt().getValue(13),pdaCpoa101.getCpoa101_Unused_Amt().getValue(14),pdaCpoa101.getCpoa101_Unused_Amt().getValue(15),
            pdaCpoa101.getCpoa101_Unused_Amt().getValue(16),pdaCpoa101.getCpoa101_Unused_Amt().getValue(17),pdaCpoa101.getCpoa101_Unused_Amt().getValue(18),
            pdaCpoa101.getCpoa101_Unused_Amt().getValue(19),pdaCpoa101.getCpoa101_Unused_Amt().getValue(20),pdaCpoa101.getCpoa101_Restart(),NEWLINE);
        if (Global.isEscape()) return;
        //* * ------------------------ START OF PROCESS ------------------------ **
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Out_Of_Balance.setValue(false);                                                                                                                               //Natural: ASSIGN #OUT-OF-BALANCE := FALSE
        //* * ------------------------ INIT. ORIGIN CODE ----------------------- **
        //* * ---------------------- READ PROCESSING RULES --------------------- **
        //* * --------------------- PROGRAM CALL VALIDATION -------------------- **
        //* * --------------------- 'RECEIVE' CONTROL TOTALS ------------------- **
        //*  11-28-2001
                                                                                                                                                                          //Natural: PERFORM A10-INITIALIZATION
        sub_A10_Initialization();
        if (condition(Global.isEscape())) {return;}
        //* * --------------------- SEQUENCE OR PROCESS REC -------------------- **
        ldaCpol101.getRef_Cps_Key().setValue(ldaCpol101a.getPnd_Rules_Record_Key());                                                                                      //Natural: ASSIGN REF.CPS-KEY := #RT-KEY := #RULES.RECORD-KEY
        pnd_Rt_Super_Pnd_Rt_Key.setValue(ldaCpol101a.getPnd_Rules_Record_Key());
        //*                                                                                                                                                               //Natural: DECIDE ON FIRST VALUE #RULES.RECORD-KEY
        short decideConditionsMet652 = 0;                                                                                                                                 //Natural: VALUE 'SEQUENCE'
        if (condition((ldaCpol101a.getPnd_Rules_Record_Key().equals("SEQUENCE"))))
        {
            decideConditionsMet652++;
                                                                                                                                                                          //Natural: PERFORM B10-SEQ-MAINLINE
            sub_B10_Seq_Mainline();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'PROCESS'
        else if (condition((ldaCpol101a.getPnd_Rules_Record_Key().equals("PROCESS"))))
        {
            decideConditionsMet652++;
                                                                                                                                                                          //Natural: PERFORM C10-PROC-MAINLINE
            sub_C10_Proc_Mainline();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Terminate.setValue(71);                                                                                                                                   //Natural: ASSIGN #TERMINATE := 71
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Terminate.greater(getZero())))                                                                                                                  //Natural: IF #TERMINATE > 0
        {
            //*  11-28-2001
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
            sub_Z99_Common_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *====================================================================**
        //* *                      S U B - R O U T I N E S                       **
        //* *====================================================================**
        //* *                                                                    **
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A00-DUMMY-DISPLAY
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A10-INITIALIZATION
        //*  SOMETIMES, NO VALUE IN #RULES.CALLING-PROGRAM (*)...BUT THIS IS OK.
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A20-READ-PROCESSING-RULES
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A30-ACCUM-GIVEN-CYCLE-TOTALS
        //* *--------------------------------------------------------------------**
        //*  ACCUM. TOTALS FROM 'RECEIVE' RECORD.
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: B10-SEQ-MAINLINE
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C10-PROC-MAINLINE
        //* *--------------------------------------------------------------------**
        //*  READING 'PROCESS' RECORD.
        //*  WRITE '='   #WS-LAST
        //*        '='   #CPS-TIME
        //*        '='   #RT-SUPER
        //*  WRITE '='   #RT-DESCRIPTION (AL=150)
        //*  WRITE '- RT-DESC1'  REF.CPS-CCYYMMDD  REF.CPS-DATE
        //*        '=' REF.CPS-INVERSE-DTE
        //*  -----------------
        //*  -----------------
        //* *        PRINT (01) 'START function processing'
        //*            RTU.RT-LONG-KEY       := #RT-SUPER.#RT-LONG-KEY
        //* *        PRINT (01) 'EXTRACT function processing'
        //*  11-28-2001 : PROCESS FOR 'START PRINT', 'PRINTED', 'DATABASE START',
        //*               'END OF CYCLE', AND 'END OF DAY' FUNCTIONS.
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C20-CHECK-PREVIOUS-STAT
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C30-CHECK-INVALID-PREV-STAT
        //* *--------------------------------------------------------------------**
        //*  CALLED BY 'RERUN' FUNCTION ONLY.
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C40-UPD-PROCESS-RECORD
        //* *MOVE EDITED REF.CPS-TIME (#WS-LAST) (EM=MM'/'DD' 'HH':'II)
        //* *  TO #TIME
        //*  WRITE 'Update'   #CPS-TIME
        //*  WRITE '='  REF.RT-DESCRIPTION(#WS-LAST) (AL=150)
        //*  WRITE '-'  #RT-DESCRIPTION (AL=150) /
        //*  WRITE '- RT-DESC1'  REF.CPS-CCYYMMDD  REF.CPS-DATE
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: D10-PRINT-REPORT
        //*      RT.RT-SHORT-KEY =  #RT-SHORT-KEY
        //* *
        //* *
        //* *MOVE EDITED #CPS-TIME (EM=MM'/'DD' 'HH':'II) TO #TIME
        //*  ----------------------------------    PREPARE NEW 'RECEIVE' RECORD
        //*  ---------------------------------------------
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: E10-MOVE-FROM-WS-REPORT
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: Z80-SYMBOLIC-TRANSLATION
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: Z90-TRANSLATE-TABLEPROG
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: Z92-TRANSLATE-INVALID-STATUS
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: Z95-TRANSLATE-PREVIOUS-STATUS
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: Z99-COMMON-ERROR
    }
    //*  11-28-2001
    private void sub_A00_Dummy_Display() throws Exception                                                                                                                 //Natural: A00-DUMMY-DISPLAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        getReports().display(10, "    ",                                                                                                                                  //Natural: DISPLAY ( 10 ) '    ' #DESC '/Cycle' #CYCLE '/Date & Time' #TIME ( HC = C ) '/Orgn' #ORGN 'Payment/Type' #PYMNT-TYPE ( HC = R ) 'Number of/Payments' #CPS-CHECK-CNT ( EM = Z,ZZZ,ZZ9- HC = R ) '/Payment Amount' #CPS-CHECK-AMT ( EM = ZZZ,ZZZ,ZZ9.99- HC = R ) '/Restart' #STAGE ( HC = L ) '/Status' #STATUS ( HC = L )
        		pnd_Desc,"/Cycle",
        		pnd_Cycle,"/Date & Time",
        		pnd_Time, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"/Orgn",
        		pnd_Orgn,"Payment/Type",
        		pnd_Pymnt_Type, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Number of/Payments",
        		ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt(), new ReportEditMask ("Z,ZZZ,ZZ9-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "/Payment Amount",
        		ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "/Restart",
        		pnd_Stage, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Status",
        		pnd_Status, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left));
        if (Global.isEscape()) return;
        //*  A00-DUMMY-DISPLAY
    }
    //*  11-28-2001
    private void sub_A10_Initialization() throws Exception                                                                                                                //Natural: A10-INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  11-28-2001
        DbsUtil.callnat(Cpvnorgn.class , getCurrentProcessState(), pdaCpoaorgn.getCpoaorgn());                                                                            //Natural: CALLNAT 'CPVNORGN' CPOAORGN
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM A20-READ-PROCESSING-RULES
        sub_A20_Read_Processing_Rules();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (ldaCpol101a.getPnd_Rules_Calling_Program().getValue("*").equals(pdaCpoa101.getCpoa101_Module()))))                                               //Natural: IF NOT ( #RULES.CALLING-PROGRAM ( * ) = CPOA101.MODULE )
        {
            pnd_Terminate.setValue(ldaCpol101a.getPnd_Rules_Invalid_Calling_Terminate_Code());                                                                            //Natural: ASSIGN #TERMINATE := #RULES.INVALID-CALLING-TERMINATE-CODE
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
            sub_Z99_Common_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  SETTL. SYSTEM TOTALS
                                                                                                                                                                          //Natural: PERFORM A30-ACCUM-GIVEN-CYCLE-TOTALS
        sub_A30_Accum_Given_Cycle_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  A10-INITIALIZATION
    }
    //*  11-28-2001
    //*  11-28-2001
    private void sub_A20_Read_Processing_Rules() throws Exception                                                                                                         //Natural: A20-READ-PROCESSING-RULES
    {
        if (BLNatReinput.isReinput()) return;

        pdaCpoa100.getCpoa100_Cpoa100_Table_Id().setValue("CPARM");                                                                                                       //Natural: ASSIGN CPOA100-TABLE-ID := 'CPARM'
        pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().setValue("A");                                                                                                            //Natural: ASSIGN CPOA100-A-I-IND := 'A'
        pdaCpoa100.getCpoa100_Cpoa100_Short_Key().setValue("PRO CONTROLS");                                                                                               //Natural: ASSIGN CPOA100-SHORT-KEY := 'PRO CONTROLS'
        pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().setValue("2 ");                                                                                                         //Natural: ASSIGN CPOA100-SUPER-IND := '2 '
        //*  11-28-2001 : FOR 'PRINT', CALLING MODULE PASSES CPOA101.FUNCTION ONLY.
        if (condition(pdaCpoa101.getCpoa101_Function().equals("PRINT")))                                                                                                  //Natural: IF CPOA101.FUNCTION = 'PRINT'
        {
            pdaCpoa100.getCpoa100_Cpoa100_Long_Key().setValue(DbsUtil.compress("********", pdaCpoa101.getCpoa101_Function()));                                            //Natural: COMPRESS '********' CPOA101.FUNCTION INTO CPOA100-LONG-KEY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCpoa100.getCpoa100_Cpoa100_Long_Key().setValue(DbsUtil.compress(pdaCpoa101.getCpoa101_Procedure(), pdaCpoa101.getCpoa101_Function()));                     //Natural: COMPRESS CPOA101.PROCEDURE CPOA101.FUNCTION INTO CPOA100-LONG-KEY
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cpon100.class , getCurrentProcessState(), pdaCpoa100.getCpoa100().getValue("*"));                                                                 //Natural: CALLNAT 'CPON100' CPOA100 ( * )
        if (condition(Global.isEscape())) return;
        //*  11-28-2001 : 'PRINT' FUNCTION ONLY, WILL HAVE AN EXACT MATCH.
        //*  EXACT MATCH
        //*  PARTIAL MATCH
        if (condition(pdaCpoa100.getCpoa100_Cpoa100_Return_Code().equals("00") || pdaCpoa100.getCpoa100_Cpoa100_Return_Code().equals("02")))                              //Natural: IF CPOA100-RETURN-CODE = '00' OR = '02'
        {
            ldaCpol101a.getPnd_Rules_Rt_Desc1().setValue(pdaCpoa100.getCpoa100_Rt_Desc1());                                                                               //Natural: ASSIGN #RULES.RT-DESC1 := CPOA100.RT-DESC1
            ldaCpol101a.getPnd_Rules_Rt_Desc2().setValue(pdaCpoa100.getCpoa100_Rt_Desc2());                                                                               //Natural: ASSIGN #RULES.RT-DESC2 := CPOA100.RT-DESC2
            ldaCpol101a.getPnd_Rules_Rt_Desc3().setValue(pdaCpoa100.getCpoa100_Rt_Desc3());                                                                               //Natural: ASSIGN #RULES.RT-DESC3 := CPOA100.RT-DESC3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Terminate.compute(new ComputeParameters(false, pnd_Terminate), pdaCpoa100.getCpoa100_Cpoa100_Return_Code().val());                                        //Natural: ASSIGN #TERMINATE := VAL ( CPOA100-RETURN-CODE )
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
            sub_Z99_Common_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  A20-READ-PROCESSING-RULES
    }
    //*  11-28-2001
    private void sub_A30_Accum_Given_Cycle_Totals() throws Exception                                                                                                      //Natural: A30-ACCUM-GIVEN-CYCLE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #O 1 #ORGN-USED
        for (pdaCpoaorgn.getCpoaorgn_Pnd_O().setValue(1); condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().lessOrEqual(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used())); 
            pdaCpoaorgn.getCpoaorgn_Pnd_O().nadd(1))
        {
            pnd_Tot_Check_Cnt.nadd(pdaCpoa101.getCpoa101_Check_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                          //Natural: ADD CPOA101.CHECK-CNT ( #O ) TO #TOT-CHECK-CNT
            pnd_Tot_Check_Amt.nadd(pdaCpoa101.getCpoa101_Check_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                          //Natural: ADD CPOA101.CHECK-AMT ( #O ) TO #TOT-CHECK-AMT
            pnd_Tot_Eft_Cnt.nadd(pdaCpoa101.getCpoa101_Eft_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                              //Natural: ADD CPOA101.EFT-CNT ( #O ) TO #TOT-EFT-CNT
            pnd_Tot_Eft_Amt.nadd(pdaCpoa101.getCpoa101_Eft_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                              //Natural: ADD CPOA101.EFT-AMT ( #O ) TO #TOT-EFT-AMT
            //*  11-28-2001
            pnd_Tot_Other_Cnt.nadd(pdaCpoa101.getCpoa101_Other_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                          //Natural: ADD CPOA101.OTHER-CNT ( #O ) TO #TOT-OTHER-CNT
            //*  11-28-2001
            pnd_Tot_Other_Amt.nadd(pdaCpoa101.getCpoa101_Other_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                          //Natural: ADD CPOA101.OTHER-AMT ( #O ) TO #TOT-OTHER-AMT
            //*  12-16-2002
            pnd_Tot_Cs_Cnt.nadd(pdaCpoa101.getCpoa101_Cs_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                                //Natural: ADD CPOA101.CS-CNT ( #O ) TO #TOT-CS-CNT
            //*  12-16-2002
            pnd_Tot_Cs_Amt.nadd(pdaCpoa101.getCpoa101_Cs_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                                //Natural: ADD CPOA101.CS-AMT ( #O ) TO #TOT-CS-AMT
            //*  12-16-2002
            pnd_Tot_Redraw_Cnt.nadd(pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                        //Natural: ADD CPOA101.REDRAW-CNT ( #O ) TO #TOT-REDRAW-CNT
            //*  12-16-2002
            pnd_Tot_Redraw_Amt.nadd(pdaCpoa101.getCpoa101_Redraw_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                        //Natural: ADD CPOA101.REDRAW-AMT ( #O ) TO #TOT-REDRAW-AMT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  A30-ACCUM-GIVEN-CYCLE-TOTALS
    }
    //*  11-28-2001
    private void sub_B10_Seq_Mainline() throws Exception                                                                                                                  //Natural: B10-SEQ-MAINLINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  READING 'SEQUENCE' RECORD.
        ldaCpol101.getVw_ref().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) REF BY RT-SUPER1 STARTING FROM #RT-SUPER
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaCpol101.getVw_ref().readNextRow("READ01")))
        {
            if (condition(pnd_Rt_Super_Pnd_Rt_A_I_Ind.notEquals(ldaCpol101.getRef_Rt_A_I_Ind()) || pnd_Rt_Super_Pnd_Rt_Table_Id.notEquals(ldaCpol101.getRef_Rt_Table_Id())  //Natural: IF #RT-A-I-IND NE REF.RT-A-I-IND OR #RT-TABLE-ID NE REF.RT-TABLE-ID OR #RT-KEY NE REF.CPS-KEY
                || pnd_Rt_Super_Pnd_Rt_Key.notEquals(ldaCpol101.getRef_Cps_Key())))
            {
                pnd_Terminate.setValue(80);                                                                                                                               //Natural: ASSIGN #TERMINATE := 80
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
                sub_Z99_Common_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCpol101a.getPnd_Rules_New_Update_Code().equals(" ")))                                                                                        //Natural: IF #RULES.NEW-UPDATE-CODE = ' '
            {
                pdaCpoa101.getCpoa101_Sequence_Nbr().setValue(ldaCpol101.getRef_Sequence_Nbr());                                                                          //Natural: ASSIGN CPOA101.SEQUENCE-NBR := REF.SEQUENCE-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaCpol101a.getPnd_Rules_New_Update_Code().equals("UPDATE")))                                                                               //Natural: IF #RULES.NEW-UPDATE-CODE = 'UPDATE'
                {
                    ldaCpol101.getRef_Sequence_Nbr().setValue(pdaCpoa101.getCpoa101_Sequence_Nbr());                                                                      //Natural: ASSIGN REF.SEQUENCE-NBR := CPOA101.SEQUENCE-NBR
                    ldaCpol101.getVw_ref().updateDBRow("READ01");                                                                                                         //Natural: UPDATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  B10-SEQ-MAINLINE
    }
    //*  11-28-2001
    private void sub_C10_Proc_Mainline() throws Exception                                                                                                                 //Natural: C10-PROC-MAINLINE
    {
        if (BLNatReinput.isReinput()) return;

        ldaCpol101.getVw_ref().startDatabaseRead                                                                                                                          //Natural: READ ( 1 ) REF BY RT-SUPER1 STARTING FROM #RT-SUPER
        (
        "R1",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") },
        1
        );
        R1:
        while (condition(ldaCpol101.getVw_ref().readNextRow("R1")))
        {
            if (condition(pnd_Rt_Super_Pnd_Rt_A_I_Ind.notEquals(ldaCpol101.getRef_Rt_A_I_Ind()) || pnd_Rt_Super_Pnd_Rt_Table_Id.notEquals(ldaCpol101.getRef_Rt_Table_Id())  //Natural: IF #RT-A-I-IND NE REF.RT-A-I-IND OR #RT-TABLE-ID NE REF.RT-TABLE-ID OR #RT-KEY NE REF.CPS-KEY
                || pnd_Rt_Super_Pnd_Rt_Key.notEquals(ldaCpol101.getRef_Cps_Key())))
            {
                pnd_Terminate.setValue(80);                                                                                                                               //Natural: ASSIGN #TERMINATE := 80
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
                sub_Z99_Common_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PREVIOUS STATUS
            }                                                                                                                                                             //Natural: END-IF
            ldaCpol101.getRef_Rt_Upd_Source().setValue(Global.getPROGRAM());                                                                                              //Natural: ASSIGN REF.RT-UPD-SOURCE := *PROGRAM
            ldaCpol101.getRef_Rt_Upd_User().setValue(Global.getINIT_USER());                                                                                              //Natural: ASSIGN REF.RT-UPD-USER := *INIT-USER
            ldaCpol101.getRef_Rt_Upd_Ccyymmdd().setValue(Global.getDATN());                                                                                               //Natural: ASSIGN REF.RT-UPD-CCYYMMDD := *DATN
            ldaCpol101.getRef_Rt_Upd_Timn().setValue(Global.getTIMN());                                                                                                   //Natural: ASSIGN REF.RT-UPD-TIMN := *TIMN
            pnd_Ws_Last.setValue(ldaCpol101.getRef_Count_Castrt_Description());                                                                                           //Natural: ASSIGN #WS-LAST := REF.C*RT-DESCRIPTION
            ldaCpol101.getPnd_Rt_Description().setValue(ldaCpol101.getRef_Rt_Description().getValue(pnd_Ws_Last));                                                        //Natural: ASSIGN #RT-DESCRIPTION := REF.RT-DESCRIPTION ( #WS-LAST )
            pdaCpoa101.getCpoa101_Key_Inverse().setValue(ldaCpol101.getRef_Cps_Inverse_Dte());                                                                            //Natural: ASSIGN CPOA101.KEY-INVERSE := REF.CPS-INVERSE-DTE
            //*  11-28-2001
            if (condition(pdaCpoa101.getCpoa101_Function().notEquals("RERUN")))                                                                                           //Natural: IF CPOA101.FUNCTION NE 'RERUN'
            {
                                                                                                                                                                          //Natural: PERFORM C20-CHECK-PREVIOUS-STAT
                sub_C20_Check_Previous_Stat();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM C30-CHECK-INVALID-PREV-STAT
                sub_C30_Check_Invalid_Prev_Stat();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet962 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CPOA101.FUNCTION;//Natural: VALUE 'RERUN'
            if (condition((pdaCpoa101.getCpoa101_Function().equals("RERUN"))))
            {
                decideConditionsMet962++;
                //*          PRINT (01) 'RERUN function processing'           /* TEMPORARY
                //*  11-28-2001
                //*  12-16-2002
                //*  12-16-2002
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().reset();                                                                                             //Natural: RESET #CPS-CHECK-CNT #CPS-CHECK-AMT #CPS-EFT-CNT #CPS-EFT-AMT #CPS-OTHER-CNT #CPS-OTHER-AMT #CPS-CS-CNT #CPS-CS-AMT #CPS-REDRAW-CNT #CPS-REDRAW-AMT
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Time().setValue(Global.getTIMX());                                                                               //Natural: ASSIGN #CPS-TIME := *TIMX
                //*  START NEW CYCLE
                //*  11-28-2001
                                                                                                                                                                          //Natural: PERFORM C40-UPD-PROCESS-RECORD
                sub_C40_Upd_Process_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaCpoa101.getCpoa101_Cycle_Nbr().setValue(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cycle());                                                             //Natural: ASSIGN CPOA101.CYCLE-NBR := #CPS-CYCLE
            }                                                                                                                                                             //Natural: VALUE 'START'
            else if (condition((pdaCpoa101.getCpoa101_Function().equals("START"))))
            {
                decideConditionsMet962++;
                pnd_Rt_Super_Pnd_Rt_Invrse_Dte.compute(new ComputeParameters(false, pnd_Rt_Super_Pnd_Rt_Invrse_Dte), DbsField.subtract(100000000,Global.getDATN()));      //Natural: ASSIGN #RT-INVRSE-DTE := 100000000 - *DATN
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Time().setValue(Global.getTIMX());                                                                               //Natural: ASSIGN #CPS-TIME := *TIMX
                //*  11-28-2001
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().reset();                                                                                             //Natural: RESET #CPS-CHECK-CNT #CPS-CHECK-AMT #CPS-EFT-CNT #CPS-EFT-AMT #CPS-OTHER-CNT #CPS-OTHER-AMT #CPS-CS-CNT #CPS-CS-AMT #CPS-REDRAW-CNT #CPS-REDRAW-AMT #CPS-UNUSED-CNT #CPS-UNUSED-AMT #CPS-RESTART
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Unused_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Unused_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Restart().reset();
                //*  ---
                //*  IS IT TODAY?
                if (condition(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status().equals("END OF DAY")))                                                                    //Natural: IF #CPS-STATUS = 'END OF DAY'
                {
                    //*  - YES; NO GOOD!!
                    if (condition(pnd_Rt_Super_Pnd_Rt_Short_Key.equals(ldaCpol101.getRef_Rt_Short_Key())))                                                                //Natural: IF #RT-SHORT-KEY = REF.RT-SHORT-KEY
                    {
                        pnd_Terminate.setValue(83);                                                                                                                       //Natural: ASSIGN #TERMINATE := 83
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
                        sub_Z99_Common_Error();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  11-28-2001
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCpol101.getRtu_Rt_A_I_Ind().setValue(pnd_Rt_Super_Pnd_Rt_A_I_Ind);                                                                             //Natural: ASSIGN RTU.RT-A-I-IND := #RT-A-I-IND
                        ldaCpol101.getRtu_Rt_Table_Id().setValue(pnd_Rt_Super_Pnd_Rt_Table_Id);                                                                           //Natural: ASSIGN RTU.RT-TABLE-ID := #RT-TABLE-ID
                        ldaCpol101.getRtu_Rt_Short_Key().setValue(pnd_Rt_Super_Pnd_Rt_Short_Key);                                                                         //Natural: ASSIGN RTU.RT-SHORT-KEY := #RT-SHORT-KEY
                        ldaCpol101.getRtu_Rt_Long_Key().setValue(Global.getDATN());                                                                                       //Natural: ASSIGN RTU.RT-LONG-KEY := *DATN
                        ldaCpol101.getRtu_Cps_Ccyymmdd().setValue(Global.getDATN());                                                                                      //Natural: ASSIGN RTU.CPS-CCYYMMDD := *DATN
                        ldaCpol101.getRtu_Cps_Date().setValue(Global.getDATX());                                                                                          //Natural: ASSIGN RTU.CPS-DATE := *DATX
                        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cycle().setValue(1);                                                                                     //Natural: ASSIGN #CPS-CYCLE := 1
                        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status().setValue(ldaCpol101a.getPnd_Rules_New_Function());                                              //Natural: ASSIGN #CPS-STATUS := #RULES.NEW-FUNCTION
                        ldaCpol101.getRtu_Rt_Description().getValue(1).setValue(ldaCpol101.getPnd_Rt_Description());                                                      //Natural: ASSIGN RTU.RT-DESCRIPTION ( 1 ) := #RT-DESCRIPTION
                        ldaCpol101.getRtu_Rt_Upd_Source().setValue(Global.getPROGRAM());                                                                                  //Natural: ASSIGN RTU.RT-UPD-SOURCE := *PROGRAM
                        ldaCpol101.getRtu_Rt_Upd_User().setValue(Global.getINIT_USER());                                                                                  //Natural: ASSIGN RTU.RT-UPD-USER := *INIT-USER
                        ldaCpol101.getRtu_Rt_Upd_Ccyymmdd().setValue(Global.getDATN());                                                                                   //Natural: ASSIGN RTU.RT-UPD-CCYYMMDD := *DATN
                        ldaCpol101.getRtu_Rt_Upd_Timn().setValue(Global.getTIMN());                                                                                       //Natural: ASSIGN RTU.RT-UPD-TIMN := *TIMN
                        getReports().write(0, "Store",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Time());                                                                   //Natural: WRITE 'Store' #CPS-TIME
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  JH
                        getReports().write(0, "=",ldaCpol101.getPnd_Rt_Description(), new AlphanumericLength (158));                                                      //Natural: WRITE '=' #RT-DESCRIPTION ( AL = 158 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "- RT-DESC1",ldaCpol101.getRtu_Cps_Ccyymmdd(),ldaCpol101.getRtu_Cps_Date());                                                //Natural: WRITE '- RT-DESC1' RTU.CPS-CCYYMMDD RTU.CPS-DATE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  NEW DAY CREATED
                        ldaCpol101.getVw_rtu().insertDBRow();                                                                                                             //Natural: STORE RTU
                        //* *   PRINT (01) 'new day record created'            /* TEMPORARY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Last.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WS-LAST
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cycle().setValue(pnd_Ws_Last);                                                                               //Natural: ASSIGN #CPS-CYCLE := #WS-LAST
                    //*  START NEW CYCLE
                                                                                                                                                                          //Natural: PERFORM C40-UPD-PROCESS-RECORD
                    sub_C40_Upd_Process_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'XML CREATED'
            else if (condition((pdaCpoa101.getCpoa101_Function().equals("XML CREATED"))))
            {
                decideConditionsMet962++;
                FOR02:                                                                                                                                                    //Natural: FOR #O FROM 1 TO #ORGN-USED
                for (pdaCpoaorgn.getCpoaorgn_Pnd_O().setValue(1); condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().lessOrEqual(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used())); 
                    pdaCpoaorgn.getCpoaorgn_Pnd_O().nadd(1))
                {
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().nadd(pdaCpoa101.getCpoa101_Check_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ADD CPOA101.CHECK-CNT ( #O ) TO #CPS-CHECK-CNT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().nadd(pdaCpoa101.getCpoa101_Check_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ADD CPOA101.CHECK-AMT ( #O ) TO #CPS-CHECK-AMT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().nadd(pdaCpoa101.getCpoa101_Eft_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ADD CPOA101.EFT-CNT ( #O ) TO #CPS-EFT-CNT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().nadd(pdaCpoa101.getCpoa101_Eft_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ADD CPOA101.EFT-AMT ( #O ) TO #CPS-EFT-AMT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().nadd(pdaCpoa101.getCpoa101_Other_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ADD CPOA101.OTHER-CNT ( #O ) TO #CPS-OTHER-CNT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().nadd(pdaCpoa101.getCpoa101_Other_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ADD CPOA101.OTHER-AMT ( #O ) TO #CPS-OTHER-AMT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().nadd(pdaCpoa101.getCpoa101_Cs_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                     //Natural: ADD CPOA101.CS-CNT ( #O ) TO #CPS-CS-CNT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().nadd(pdaCpoa101.getCpoa101_Cs_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                     //Natural: ADD CPOA101.CS-AMT ( #O ) TO #CPS-CS-AMT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().nadd(pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));             //Natural: ADD CPOA101.REDRAW-CNT ( #O ) TO #CPS-REDRAW-CNT
                    ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().nadd(pdaCpoa101.getCpoa101_Redraw_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));             //Natural: ADD CPOA101.REDRAW-AMT ( #O ) TO #CPS-REDRAW-AMT
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM C40-UPD-PROCESS-RECORD
                sub_C40_Upd_Process_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'CHECK CONTROLS'
            else if (condition((pdaCpoa101.getCpoa101_Function().equals("CHECK CONTROLS"))))
            {
                decideConditionsMet962++;
                //* *      PRINT (01) CPOA101.FUNCTION 'processing'         /* TEMPORARY
                //* *
                if (condition(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().equals(pnd_Tot_Check_Cnt) && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().equals(pnd_Tot_Check_Amt)  //Natural: IF #CPS-CHECK-CNT = #TOT-CHECK-CNT AND #CPS-CHECK-AMT = #TOT-CHECK-AMT AND #CPS-EFT-CNT = #TOT-EFT-CNT AND #CPS-EFT-AMT = #TOT-EFT-AMT
                    && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().equals(pnd_Tot_Eft_Cnt) && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().equals(pnd_Tot_Eft_Amt)))
                {
                    //*  11-28-2001 : A. YOUNG - ONLY CHECKS AND EFTS ARE BALANCED HERE.
                    //*                          DO NOT TRY TO BALANCE OTHER, CS OR REDRAW
                    //*                          TOTALS HERE.
                                                                                                                                                                          //Natural: PERFORM C40-UPD-PROCESS-RECORD
                    sub_C40_Upd_Process_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Out_Of_Balance.setValue(true);                                                                                                                    //Natural: ASSIGN #OUT-OF-BALANCE := TRUE
                    pnd_Terminate.setValue(84);                                                                                                                           //Natural: ASSIGN #TERMINATE := 84
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
                    sub_Z99_Common_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'DATABASE UPDATE'
            else if (condition((pdaCpoa101.getCpoa101_Function().equals("DATABASE UPDATE"))))
            {
                decideConditionsMet962++;
                //* *        PRINT (01) CPOA101.FUNCTION 'processing'         /* TEMPORARY
                //*  11-28-2001
                //*  11-28-2001
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                if (condition(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().equals(pnd_Tot_Check_Cnt) && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().equals(pnd_Tot_Check_Amt)  //Natural: IF #CPS-CHECK-CNT = #TOT-CHECK-CNT AND #CPS-CHECK-AMT = #TOT-CHECK-AMT AND #CPS-EFT-CNT = #TOT-EFT-CNT AND #CPS-EFT-AMT = #TOT-EFT-AMT AND #CPS-OTHER-CNT = #TOT-OTHER-CNT AND #CPS-OTHER-AMT = #TOT-OTHER-AMT AND #CPS-CS-CNT = #TOT-CS-CNT AND #CPS-CS-AMT = #TOT-CS-AMT AND #CPS-REDRAW-CNT = #TOT-REDRAW-CNT AND #CPS-REDRAW-AMT = #TOT-REDRAW-AMT
                    && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().equals(pnd_Tot_Eft_Cnt) && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().equals(pnd_Tot_Eft_Amt) 
                    && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().equals(pnd_Tot_Other_Cnt) && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().equals(pnd_Tot_Other_Amt) 
                    && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().equals(pnd_Tot_Cs_Cnt) && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().equals(pnd_Tot_Cs_Amt) 
                    && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().equals(pnd_Tot_Redraw_Cnt) && ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().equals(pnd_Tot_Redraw_Amt)))
                {
                                                                                                                                                                          //Natural: PERFORM C40-UPD-PROCESS-RECORD
                    sub_C40_Upd_Process_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    pnd_Out_Of_Balance.setValue(true);                                                                                                                    //Natural: ASSIGN #OUT-OF-BALANCE := TRUE
                    getReports().write(0, "out of balance- Checks",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt(),pnd_Tot_Check_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt(), //Natural: WRITE 'out of balance- Checks' #CPS-CHECK-CNT #TOT-CHECK-CNT #CPS-CHECK-AMT #TOT-CHECK-AMT
                        pnd_Tot_Check_Amt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "out of balance- EFTs  ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt(),pnd_Tot_Eft_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt(), //Natural: WRITE 'out of balance- EFTs  ' #CPS-EFT-CNT #TOT-EFT-CNT #CPS-EFT-AMT #TOT-EFT-AMT
                        pnd_Tot_Eft_Amt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "out of balance- Other ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt(),pnd_Tot_Other_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt(), //Natural: WRITE 'out of balance- Other ' #CPS-OTHER-CNT #TOT-OTHER-CNT #CPS-OTHER-AMT #TOT-OTHER-AMT
                        pnd_Tot_Other_Amt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "out of balance- C/S   ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt(),pnd_Tot_Cs_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt(), //Natural: WRITE 'out of balance- C/S   ' #CPS-CS-CNT #TOT-CS-CNT #CPS-CS-AMT #TOT-CS-AMT
                        pnd_Tot_Cs_Amt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "out of balance- Redraw",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt(),pnd_Tot_Redraw_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt(), //Natural: WRITE 'out of balance- Redraw' #CPS-REDRAW-CNT #TOT-REDRAW-CNT #CPS-REDRAW-AMT #TOT-REDRAW-AMT
                        pnd_Tot_Redraw_Amt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Terminate.setValue(84);                                                                                                                           //Natural: ASSIGN #TERMINATE := 84
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
                    sub_Z99_Common_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'PRINT'
            else if (condition((pdaCpoa101.getCpoa101_Function().equals("PRINT"))))
            {
                decideConditionsMet962++;
                getReports().write(0, "'PRINT' Function processing performed...");                                                                                        //Natural: WRITE '"PRINT" Function processing performed...'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                getReports().print(0, "C10-PROC-MAINLINE NONE value of DECIDE");                                                                                          //Natural: PRINT 'C10-PROC-MAINLINE NONE value of DECIDE'
                                                                                                                                                                          //Natural: PERFORM C40-UPD-PROCESS-RECORD
                sub_C40_Upd_Process_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  11-28-2001
            if (condition(pnd_Terminate.greater(getZero()) || ldaCpol101a.getPnd_Rules_Print_Report().equals("YES")))                                                     //Natural: IF #TERMINATE > 0 OR #RULES.PRINT-REPORT = 'YES'
            {
                                                                                                                                                                          //Natural: PERFORM D10-PRINT-REPORT
                sub_D10_Print_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  C10-PROC-MAINLINE
    }
    //*  11-28-2001
    private void sub_C20_Check_Previous_Stat() throws Exception                                                                                                           //Natural: C20-CHECK-PREVIOUS-STAT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  CALLED BY ALL FUNCTIONS EXCEPT 'RERUN'.
        pnd_Prev_Stat_Ws.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #PREV-STAT-WS #PREV-FUNCT-BLANK #PREV-FUNCT-MATCH
        pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Blank.resetInitial();
        pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Match.resetInitial();
        FOR03:                                                                                                                                                            //Natural: FOR #P 1 10
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(10)); pnd_P.nadd(1))
        {
            if (condition(ldaCpol101a.getPnd_Rules_Expected_Previous_Function().getValue(pnd_P).equals(" ")))                                                             //Natural: IF #RULES.EXPECTED-PREVIOUS-FUNCTION ( #P ) = ' '
            {
                pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Blank.nadd(1);                                                                                                            //Natural: ASSIGN #PREV-FUNCT-BLANK := #PREV-FUNCT-BLANK + 1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCpol101a.getPnd_Rules_Expected_Previous_Function().getValue(pnd_P).equals(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status())))               //Natural: IF #RULES.EXPECTED-PREVIOUS-FUNCTION ( #P ) = #CPS-STATUS
            {
                pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Match.setValue(true);                                                                                                     //Natural: ASSIGN #PREV-FUNCT-MATCH := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ALL OCCURS. BLANK
        //*  MATCH FOUND
        if (condition(pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Blank.equals(10) || pnd_Prev_Stat_Ws_Pnd_Prev_Funct_Match.getBoolean()))                                            //Natural: IF #PREV-FUNCT-BLANK = 10 OR #PREV-FUNCT-MATCH
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Terminate.setValue(ldaCpol101a.getPnd_Rules_Prev_Function_Terminate_Code());                                                                              //Natural: ASSIGN #TERMINATE := #RULES.PREV-FUNCTION-TERMINATE-CODE
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
            sub_Z99_Common_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  C20-CHECK-PREVIOUS-STAT
    }
    //*  11-28-2001
    private void sub_C30_Check_Invalid_Prev_Stat() throws Exception                                                                                                       //Natural: C30-CHECK-INVALID-PREV-STAT
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #P 1 3
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(3)); pnd_P.nadd(1))
        {
            if (condition(ldaCpol101a.getPnd_Rules_Invalid_Previous_Functions().getValue(pnd_P).equals(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status())))               //Natural: IF #RULES.INVALID-PREVIOUS-FUNCTIONS ( #P ) = #CPS-STATUS
            {
                pnd_Terminate.setValue(ldaCpol101a.getPnd_Rules_Invalid_Previous_Terminate_Code());                                                                       //Natural: ASSIGN #TERMINATE := #RULES.INVALID-PREVIOUS-TERMINATE-CODE
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
                sub_Z99_Common_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  C30-CHECK-INVALID-PREV-STAT
    }
    //*  11-28-2001
    private void sub_C40_Upd_Process_Record() throws Exception                                                                                                            //Natural: C40-UPD-PROCESS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  11-28-2001
        //*  11-28-2001
        //*  11-28-2001
        if (condition(ldaCpol101a.getPnd_Rules_New_Update_Code().equals("NEW") || ldaCpol101a.getPnd_Rules_New_Update_Code().equals("UPDATE")))                           //Natural: IF #RULES.NEW-UPDATE-CODE = 'NEW' OR = 'UPDATE'
        {
            pdaCpoa101.getCpoa101_Current_Status().setValue(ldaCpol101a.getPnd_Rules_New_Function());                                                                     //Natural: ASSIGN CPOA101.CURRENT-STATUS := #RULES.NEW-FUNCTION
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status().setValue(ldaCpol101a.getPnd_Rules_New_Function());                                                          //Natural: ASSIGN #CPS-STATUS := #RULES.NEW-FUNCTION
            G1:                                                                                                                                                           //Natural: GET REF *ISN ( R1. )
            ldaCpol101.getVw_ref().readByID(ldaCpol101.getVw_ref().getAstISN("R1"), "G1");
            ldaCpol101.getRef_Rt_Upd_Source().setValue(Global.getPROGRAM());                                                                                              //Natural: ASSIGN REF.RT-UPD-SOURCE := *PROGRAM
            ldaCpol101.getRef_Rt_Upd_User().setValue(Global.getINIT_USER());                                                                                              //Natural: ASSIGN REF.RT-UPD-USER := *INIT-USER
            ldaCpol101.getRef_Rt_Upd_Ccyymmdd().setValue(Global.getDATN());                                                                                               //Natural: ASSIGN REF.RT-UPD-CCYYMMDD := *DATN
            ldaCpol101.getRef_Rt_Upd_Timn().setValue(Global.getTIMN());                                                                                                   //Natural: ASSIGN REF.RT-UPD-TIMN := *TIMN
            //*  11-28-2001
            if (condition(ldaCpol101a.getPnd_Rules_Restart_Value().notEquals(" ")))                                                                                       //Natural: IF #RULES.RESTART-VALUE NE ' '
            {
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Restart().setValue(ldaCpol101a.getPnd_Rules_Restart_Value());                                                    //Natural: ASSIGN #CPS-RESTART := #RULES.RESTART-VALUE
                //*  ELSE
                //*    #CPS-RESTART               := REF.CPS-RESTART (#WS-LAST)
                //*  11-28-2001
                //*  NOT NEEDED FOR NEXT CYCLE.
            }                                                                                                                                                             //Natural: END-IF
            ldaCpol101.getRef_Rt_Description().getValue(pnd_Ws_Last).setValue(ldaCpol101.getPnd_Rt_Description());                                                        //Natural: ASSIGN REF.RT-DESCRIPTION ( #WS-LAST ) := #RT-DESCRIPTION
            ldaCpol101.getRef_Cps_Unused_Cnt().getValue(pnd_Ws_Last).setValue(0);                                                                                         //Natural: ASSIGN REF.CPS-UNUSED-CNT ( #WS-LAST ) := 0
            ldaCpol101.getRef_Cps_Unused_Amt().getValue(pnd_Ws_Last).setValue(0);                                                                                         //Natural: ASSIGN REF.CPS-UNUSED-AMT ( #WS-LAST ) := 0
            ldaCpol101.getRef_Pnd_Filler().getValue(pnd_Ws_Last).setValue(" ");                                                                                           //Natural: ASSIGN REF.#FILLER ( #WS-LAST ) := ' '
            //* *INT '=' REF.RT-DESCRIPTION(#WS-LAST)
            //* *INT '=' #RT-DESCRIPTION
            //* *ITE '=' #TIME
            //* *ITE '=' REF.CPS-TIME      (#WS-LAST)
            //* *ITE '=' REF.CPS-CYCLE     (#WS-LAST)
            //*      '=' REF.CPS-STATUS    (#WS-LAST)
            //*      '=' REF.CPS-CHECK-CNT (#WS-LAST)
            //*      '=' REF.CPS-CHECK-AMT (#WS-LAST)
            //*      '=' REF.CPS-EFT-CNT   (#WS-LAST)
            //*      '=' REF.CPS-EFT-AMT   (#WS-LAST)
            //*      '=' REF.CPS-OTHER-CNT (#WS-LAST)
            //*      '=' REF.CPS-OTHER-AMT (#WS-LAST)
            //*      '=' REF.CPS-CS-CNT    (#WS-LAST)
            //*      '=' REF.CPS-CS-AMT    (#WS-LAST)
            //*      '=' REF.CPS-REDRAW-CNT(#WS-LAST)
            //*      '=' REF.CPS-REDRAW-AMT(#WS-LAST)
            //*      '=' REF.CPS-RESTART   (#WS-LAST)
            //*      '=' REF.CPS-FILLER    (#WS-LAST)
            //*      '=' REF.RT-EFF-FROM-CCYYMMDD
            //*      '=' REF.RT-EFF-TO-CCYYMMDD
            //*      '=' REF.RT-UPD-SOURCE
            //*      '=' REF.RT-UPD-USER
            //*      '=' REF.RT-UPD-CCYYMMDD
            //*      '=' REF.RT-UPD-TIMN
            //*  11-28-2001 : MOVED UPDATE (G1.) BEFORE DISPLAYS.
            ldaCpol101.getVw_ref().updateDBRow("G1");                                                                                                                     //Natural: UPDATE ( G1. )
            pnd_Status_After_Upd.setValue(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status());                                                                             //Natural: ASSIGN #STATUS-AFTER-UPD := #CPS-STATUS
        }                                                                                                                                                                 //Natural: END-IF
        //*  C40-UPD-PROCESS-RECORD
    }
    private void sub_D10_Print_Report() throws Exception                                                                                                                  //Natural: D10-PRINT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  11-28-2001
        //*  11-28-2001
        getReports().write(10, ReportOption.NOTITLE,new ReportTAsterisk(pnd_Desc),"RECEIVED:");                                                                           //Natural: WRITE ( 10 ) T*#DESC 'RECEIVED:'
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #O FROM 1 TO #ORGN-USED
        for (pdaCpoaorgn.getCpoaorgn_Pnd_O().setValue(1); condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().lessOrEqual(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used())); 
            pdaCpoaorgn.getCpoaorgn_Pnd_O().nadd(1))
        {
            //*  IF       #O-ORGN(#O)  =  ' ' THEN  ESCAPE TOP  END-IF
            //*  MOVE     #O-ORGN(#O)  TO #ORGN
            //*  COMPRESS #O-ORGN(#O) 'RECEIVE'     /* GET 'RECEIVE' RECORD
            //*      INTO #RT-KEY
            if (condition(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).equals(" ")))                                                //Natural: IF CPOAORGN.#ORGN-CDE-4 ( #O ) = ' ' THEN
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Orgn.setValue(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                        //Natural: MOVE CPOAORGN.#ORGN-CDE-4 ( #O ) TO #ORGN
            //*  GET 'RECEIVE' RECORD
            pnd_Rt_Super_Pnd_Rt_Key.setValue(DbsUtil.compress(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()), "RECEIVE"));            //Natural: COMPRESS CPOAORGN.#ORGN-CDE-4 ( #O ) 'RECEIVE' INTO #RT-KEY
            vw_rt.startDatabaseRead                                                                                                                                       //Natural: READ ( 1 ) RT BY RT-SUPER1 STARTING FROM #RT-SUPER
            (
            "RECEIVE",
            new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super, WcType.BY) },
            new Oc[] { new Oc("RT_SUPER1", "ASC") },
            1
            );
            RECEIVE:
            while (condition(vw_rt.readNextRow("RECEIVE")))
            {
                //*  CHECK INRVSE-DTE 3/15/2000
                //*  03/02/01 AR
                if (condition(rt_Rt_A_I_Ind.equals(pnd_Rt_Super_Pnd_Rt_A_I_Ind) && rt_Rt_Table_Id.equals(pnd_Rt_Super_Pnd_Rt_Table_Id) && rt_Cps_Key.equals(pnd_Rt_Super_Pnd_Rt_Key))) //Natural: IF RT.RT-A-I-IND = #RT-A-I-IND AND RT.RT-TABLE-ID = #RT-TABLE-ID AND RT.CPS-KEY = #RT-KEY
                {
                    pnd_Cps_Receive_Date.setValue(rt_Cps_Inverse_Dte);                                                                                                    //Natural: ASSIGN #CPS-RECEIVE-DATE := RT.CPS-INVERSE-DTE
                    FOR06:                                                                                                                                                //Natural: FOR #I FROM 1 TO 2
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_I.equals(1)))                                                                                                                   //Natural: IF #I = 1 THEN
                        {
                            pnd_Desc.setValue("BROUGHT FORWARD");                                                                                                         //Natural: ASSIGN #DESC := 'BROUGHT FORWARD'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Desc.setValue("CURRENT");                                                                                                                 //Natural: ASSIGN #DESC := 'CURRENT'
                        }                                                                                                                                                 //Natural: END-IF
                        ldaCpol101.getPnd_Rt_Description().setValue(rt_Rt_Description.getValue(pnd_I));                                                                   //Natural: ASSIGN #RT-DESCRIPTION := RT.RT-DESCRIPTION ( #I )
                        //*  BUILD 'TOTAL OUTSTANDING'
                        pnd_Sum_Check_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt());                                                                     //Natural: ADD #CPS-CHECK-CNT TO #SUM-CHECK-CNT
                        pnd_Sum_Check_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt());                                                                     //Natural: ADD #CPS-CHECK-AMT TO #SUM-CHECK-AMT
                        pnd_Sum_Eft_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt());                                                                         //Natural: ADD #CPS-EFT-CNT TO #SUM-EFT-CNT
                        pnd_Sum_Eft_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt());                                                                         //Natural: ADD #CPS-EFT-AMT TO #SUM-EFT-AMT
                        //*  11-28-2001
                        pnd_Sum_Other_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt());                                                                     //Natural: ADD #CPS-OTHER-CNT TO #SUM-OTHER-CNT
                        //*  11-28-2001
                        pnd_Sum_Other_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt());                                                                     //Natural: ADD #CPS-OTHER-AMT TO #SUM-OTHER-AMT
                        //*  12-16-2002
                        pnd_Sum_Cs_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt());                                                                           //Natural: ADD #CPS-CS-CNT TO #SUM-CS-CNT
                        //*  12-16-2002
                        pnd_Sum_Cs_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt());                                                                           //Natural: ADD #CPS-CS-AMT TO #SUM-CS-AMT
                        //*  12-16-2002
                        pnd_Sum_Redraw_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt());                                                                   //Natural: ADD #CPS-REDRAW-CNT TO #SUM-REDRAW-CNT
                        //*  12-16-2002
                        pnd_Sum_Redraw_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt());                                                                   //Natural: ADD #CPS-REDRAW-AMT TO #SUM-REDRAW-AMT
                        //*  PRINT 'RECEIVE' RECORD
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
                        sub_E10_Move_From_Ws_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RECEIVE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RECEIVE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  11-28-2001
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  TERMINATING...'
                    //*  ORIGINALLY 93
                    pnd_Prog_Message.setValue(DbsUtil.compress("93 - 'RECEIVE' Control Record:", pnd_Orgn, "for", pnd_Rt_Super.getSubstring(1,26), "not in file"));       //Natural: COMPRESS '93 - "RECEIVE" Control Record:' #ORGN 'for' SUBSTR ( #RT-SUPER,1,26 ) 'not in file' INTO #PROG-MESSAGE
                    pnd_Terminate.setValue(0);                                                                                                                            //Natural: ASSIGN #TERMINATE := 00
                    //*  11-28-2001
                                                                                                                                                                          //Natural: PERFORM Z99-COMMON-ERROR
                    sub_Z99_Common_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RECEIVE"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RECEIVE"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  RT.CPS-TIME(1)
                getReports().write(0, "READ RECEIVE:",pnd_Orgn);                                                                                                          //Natural: WRITE 'READ RECEIVE:' #ORGN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RECEIVE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RECEIVE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  JHHHHHH
                getReports().write(0, "-",rt_Rt_Description.getValue(1), new AlphanumericLength (150),NEWLINE);                                                           //Natural: WRITE '-' RT.RT-DESCRIPTION ( 1 ) ( AL = 150 ) /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RECEIVE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RECEIVE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  JHHHHHH
                getReports().write(0, "-",rt_Rt_Description.getValue(2), new AlphanumericLength (150),NEWLINE);                                                           //Natural: WRITE '-' RT.RT-DESCRIPTION ( 2 ) ( AL = 150 ) /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RECEIVE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RECEIVE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  11-28-2001
        pnd_Orgn.reset();                                                                                                                                                 //Natural: RESET #ORGN
        //*  11-28-2001
        //*  11-28-2001
        //*  11-28-2001
        //*  12-16-2002
        //*  12-16-2002
        //*  12-16-2002
        //*  12-16-2002
        getReports().write(10, ReportOption.NOTITLE,new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),"-",new RepeatItem(10),new                  //Natural: WRITE ( 10 ) T*#CPS-CHECK-CNT '-' ( 10 ) T*#CPS-CHECK-AMT '-' ( 15 )
            ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),"-",new RepeatItem(15));
        if (Global.isEscape()) return;
        pnd_Desc.setValue("TOTAL OUTSTANDING");                                                                                                                           //Natural: ASSIGN #DESC := 'TOTAL OUTSTANDING'
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().setValue(pnd_Sum_Check_Cnt);                                                                                 //Natural: ASSIGN #CPS-CHECK-CNT := #SUM-CHECK-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().setValue(pnd_Sum_Check_Amt);                                                                                 //Natural: ASSIGN #CPS-CHECK-AMT := #SUM-CHECK-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().setValue(pnd_Sum_Eft_Cnt);                                                                                     //Natural: ASSIGN #CPS-EFT-CNT := #SUM-EFT-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().setValue(pnd_Sum_Eft_Amt);                                                                                     //Natural: ASSIGN #CPS-EFT-AMT := #SUM-EFT-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().setValue(pnd_Sum_Other_Cnt);                                                                                 //Natural: ASSIGN #CPS-OTHER-CNT := #SUM-OTHER-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().setValue(pnd_Sum_Other_Amt);                                                                                 //Natural: ASSIGN #CPS-OTHER-AMT := #SUM-OTHER-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().setValue(pnd_Sum_Cs_Cnt);                                                                                       //Natural: ASSIGN #CPS-CS-CNT := #SUM-CS-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().setValue(pnd_Sum_Cs_Amt);                                                                                       //Natural: ASSIGN #CPS-CS-AMT := #SUM-CS-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().setValue(pnd_Sum_Redraw_Cnt);                                                                               //Natural: ASSIGN #CPS-REDRAW-CNT := #SUM-REDRAW-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().setValue(pnd_Sum_Redraw_Amt);                                                                               //Natural: ASSIGN #CPS-REDRAW-AMT := #SUM-REDRAW-AMT
        //*  PRINT 'TOTAL OUTSTANDING'
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
        sub_E10_Move_From_Ws_Report();
        if (condition(Global.isEscape())) {return;}
        //*  11-28-01
        getReports().write(10, ReportOption.NOTITLE,new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),"=",new RepeatItem(10),new                  //Natural: WRITE ( 10 ) T*#CPS-CHECK-CNT '=' ( 10 ) T*#CPS-CHECK-AMT '=' ( 15 )
            ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),"=",new RepeatItem(15));
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #I 1 #WS-LAST
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ws_Last)); pnd_I.nadd(1))
        {
            ldaCpol101.getPnd_Rt_Description().setValue(ldaCpol101.getRef_Rt_Description().getValue(pnd_I));                                                              //Natural: ASSIGN #RT-DESCRIPTION := REF.RT-DESCRIPTION ( #I )
            pnd_Desc.setValue("PROCESSED");                                                                                                                               //Natural: ASSIGN #DESC := 'PROCESSED'
            pnd_Cycle_Pnd_Cycle_N.setValue(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cycle());                                                                             //Natural: ASSIGN #CYCLE-N := #CPS-CYCLE
            //*  11-28-2001
            if (condition(pnd_I.equals(pnd_Ws_Last)))                                                                                                                     //Natural: IF #I = #WS-LAST
            {
                pdaCpoa101.getCpoa101_Cycle_Nbr().setValue(ldaCpol101.getRef_Cps_Cycle().getValue(pnd_Ws_Last));                                                          //Natural: ASSIGN CPOA101.CYCLE-NBR := REF.CPS-CYCLE ( #WS-LAST )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Status.setValue(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status());                                                                                       //Natural: ASSIGN #STATUS := #CPS-STATUS
            pnd_Stage.setValue(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Restart());                                                                                       //Natural: ASSIGN #STAGE := #CPS-RESTART
            //*  PRINT 'PROCESS' RECORD(S)
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
            sub_E10_Move_From_Ws_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                          AND BUILD 'CARRIED FORWARD'
            pnd_Sum_Check_Cnt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt());                                                                            //Natural: SUBTRACT #CPS-CHECK-CNT FROM #SUM-CHECK-CNT
            pnd_Sum_Check_Amt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt());                                                                            //Natural: SUBTRACT #CPS-CHECK-AMT FROM #SUM-CHECK-AMT
            pnd_Sum_Eft_Cnt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt());                                                                                //Natural: SUBTRACT #CPS-EFT-CNT FROM #SUM-EFT-CNT
            pnd_Sum_Eft_Amt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt());                                                                                //Natural: SUBTRACT #CPS-EFT-AMT FROM #SUM-EFT-AMT
            //*  11-28-2001
            pnd_Sum_Other_Cnt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt());                                                                            //Natural: SUBTRACT #CPS-OTHER-CNT FROM #SUM-OTHER-CNT
            //*  11-28-2001
            pnd_Sum_Other_Amt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt());                                                                            //Natural: SUBTRACT #CPS-OTHER-AMT FROM #SUM-OTHER-AMT
            //*  12-16-2002
            pnd_Sum_Cs_Cnt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt());                                                                                  //Natural: SUBTRACT #CPS-CS-CNT FROM #SUM-CS-CNT
            //*  12-16-2002
            pnd_Sum_Cs_Amt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt());                                                                                  //Natural: SUBTRACT #CPS-CS-AMT FROM #SUM-CS-AMT
            //*  12-16-2002
            pnd_Sum_Redraw_Cnt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt());                                                                          //Natural: SUBTRACT #CPS-REDRAW-CNT FROM #SUM-REDRAW-CNT
            //*  12-16-2002
            pnd_Sum_Redraw_Amt.nsubtract(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt());                                                                          //Natural: SUBTRACT #CPS-REDRAW-AMT FROM #SUM-REDRAW-AMT
            //*  11-28-2001
            pnd_Total_Processed_Pnd_Prcss_Check_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt());                                                           //Natural: ADD #CPS-CHECK-CNT TO #PRCSS-CHECK-CNT
            //*  11-28-2001
            pnd_Total_Processed_Pnd_Prcss_Check_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt());                                                           //Natural: ADD #CPS-CHECK-AMT TO #PRCSS-CHECK-AMT
            //*  11-28-2001
            pnd_Total_Processed_Pnd_Prcss_Eft_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt());                                                               //Natural: ADD #CPS-EFT-CNT TO #PRCSS-EFT-CNT
            //*  11-28-2001
            pnd_Total_Processed_Pnd_Prcss_Eft_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt());                                                               //Natural: ADD #CPS-EFT-AMT TO #PRCSS-EFT-AMT
            //*  11-28-2001
            pnd_Total_Processed_Pnd_Prcss_Other_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt());                                                           //Natural: ADD #CPS-OTHER-CNT TO #PRCSS-OTHER-CNT
            //*  11-28-2001
            pnd_Total_Processed_Pnd_Prcss_Other_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt());                                                           //Natural: ADD #CPS-OTHER-AMT TO #PRCSS-OTHER-AMT
            //*  12-16-2002
            pnd_Total_Processed_Pnd_Prcss_Cs_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt());                                                                 //Natural: ADD #CPS-CS-CNT TO #PRCSS-CS-CNT
            //*  12-16-2002
            pnd_Total_Processed_Pnd_Prcss_Cs_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt());                                                                 //Natural: ADD #CPS-CS-AMT TO #PRCSS-CS-AMT
            //*  12-16-2002
            pnd_Total_Processed_Pnd_Prcss_Redraw_Cnt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt());                                                         //Natural: ADD #CPS-REDRAW-CNT TO #PRCSS-REDRAW-CNT
            //*  12-16-2002
            pnd_Total_Processed_Pnd_Prcss_Redraw_Amt.nadd(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt());                                                         //Natural: ADD #CPS-REDRAW-AMT TO #PRCSS-REDRAW-AMT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ---------------------
        //*  11-28-2001
        getReports().write(10, ReportOption.NOTITLE,new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),"-",new RepeatItem(10),new                  //Natural: WRITE ( 10 ) T*#CPS-CHECK-CNT '-' ( 10 ) T*#CPS-CHECK-AMT '-' ( 15 )
            ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),"-",new RepeatItem(15));
        if (Global.isEscape()) return;
        //*  11-28-2001
        //*  12-16-2002
        //*  12-16-2002
        //*  11-28-2001
        //*  11-28-2001
        //*  11-28-2001
        //*  11-28-2001
        //*  11-28-2001
        //*  11-28-2001
        //*  11-28-2001
        //*  12-16-2002
        //*  12-16-2002
        //*  12-16-2002
        //*  12-16-2002
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().reset();                                                                                                     //Natural: RESET #CPS-CHECK-CNT #CPS-CHECK-AMT #CPS-EFT-CNT #CPS-EFT-AMT #CPS-OTHER-CNT #CPS-OTHER-AMT #CPS-CS-CNT #CPS-CS-AMT #CPS-REDRAW-CNT #CPS-REDRAW-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().reset();
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().reset();
        pnd_Desc.setValue("TOTAL PROCESSED");                                                                                                                             //Natural: ASSIGN #DESC := 'TOTAL PROCESSED'
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().setValue(pnd_Total_Processed_Pnd_Prcss_Check_Cnt);                                                           //Natural: ASSIGN #CPS-CHECK-CNT := #PRCSS-CHECK-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().setValue(pnd_Total_Processed_Pnd_Prcss_Check_Amt);                                                           //Natural: ASSIGN #CPS-CHECK-AMT := #PRCSS-CHECK-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().setValue(pnd_Total_Processed_Pnd_Prcss_Eft_Cnt);                                                               //Natural: ASSIGN #CPS-EFT-CNT := #PRCSS-EFT-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().setValue(pnd_Total_Processed_Pnd_Prcss_Eft_Amt);                                                               //Natural: ASSIGN #CPS-EFT-AMT := #PRCSS-EFT-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().setValue(pnd_Total_Processed_Pnd_Prcss_Other_Cnt);                                                           //Natural: ASSIGN #CPS-OTHER-CNT := #PRCSS-OTHER-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().setValue(pnd_Total_Processed_Pnd_Prcss_Other_Amt);                                                           //Natural: ASSIGN #CPS-OTHER-AMT := #PRCSS-OTHER-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().setValue(pnd_Total_Processed_Pnd_Prcss_Cs_Cnt);                                                                 //Natural: ASSIGN #CPS-CS-CNT := #PRCSS-CS-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().setValue(pnd_Total_Processed_Pnd_Prcss_Cs_Amt);                                                                 //Natural: ASSIGN #CPS-CS-AMT := #PRCSS-CS-AMT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().setValue(pnd_Total_Processed_Pnd_Prcss_Redraw_Cnt);                                                         //Natural: ASSIGN #CPS-REDRAW-CNT := #PRCSS-REDRAW-CNT
        ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().setValue(pnd_Total_Processed_Pnd_Prcss_Redraw_Amt);                                                         //Natural: ASSIGN #CPS-REDRAW-AMT := #PRCSS-REDRAW-AMT
        //*  11-28-2001
        pnd_Total_Processed.reset();                                                                                                                                      //Natural: RESET #TOTAL-PROCESSED
        //*  PRINT 'TOTAL PROCESSED'/* 11-28-2001
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
        sub_E10_Move_From_Ws_Report();
        if (condition(Global.isEscape())) {return;}
        //*  11-28-01
        //*  11-28-2001
        getReports().write(10, ReportOption.NOTITLE,new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),"=",new RepeatItem(10),new                  //Natural: WRITE ( 10 ) T*#CPS-CHECK-CNT '=' ( 10 ) T*#CPS-CHECK-AMT '=' ( 15 ) T*#STATUS #OTHER-STATUS-2
            ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),"=",new RepeatItem(15),new ReportTAsterisk(pnd_Status),pnd_Other_Status_2);
        if (Global.isEscape()) return;
        //*  11-28-2001
        if (condition(pnd_Other_Status_2.notEquals(" ")))                                                                                                                 //Natural: IF #OTHER-STATUS-2 NE ' '
        {
            pnd_Other_Status_2.reset();                                                                                                                                   //Natural: RESET #OTHER-STATUS-2
        }                                                                                                                                                                 //Natural: END-IF
        //*  11-28-2001
        //*  11-28-2001
        getReports().write(10, ReportOption.NOTITLE,NEWLINE,NEWLINE,"CURRENT PROCESS");                                                                                   //Natural: WRITE ( 10 ) // 'CURRENT PROCESS'
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #O FROM 1 TO #ORGN-USED
        for (pdaCpoaorgn.getCpoaorgn_Pnd_O().setValue(1); condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().lessOrEqual(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used())); 
            pdaCpoaorgn.getCpoaorgn_Pnd_O().nadd(1))
        {
            //*  11-28-2001
            //*  11-28-2001
            //*  BUILD 'ACTUAL' FROM PDA
            //*  11-28-2001
            //*  11-28-2001
            //*  12-16-2002
            //*  12-16-2002
            //*  12-16-2002
            //*  12-16-2002
            if (condition(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).equals(" ")))                                                //Natural: IF CPOAORGN.#ORGN-CDE-4 ( #O ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Orgn.setValue(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                        //Natural: ASSIGN #ORGN := CPOAORGN.#ORGN-CDE-4 ( #O )
            pnd_Desc.setValue("ACTUAL");                                                                                                                                  //Natural: ASSIGN #DESC := 'ACTUAL'
            pnd_Cycle_Pnd_Cycle_N.setValue(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cycle());                                                                             //Natural: ASSIGN #CYCLE-N := #CPS-CYCLE
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().setValue(pdaCpoa101.getCpoa101_Check_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ASSIGN #CPS-CHECK-CNT := CPOA101.CHECK-CNT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().setValue(pdaCpoa101.getCpoa101_Check_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ASSIGN #CPS-CHECK-AMT := CPOA101.CHECK-AMT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().setValue(pdaCpoa101.getCpoa101_Eft_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                       //Natural: ASSIGN #CPS-EFT-CNT := CPOA101.EFT-CNT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().setValue(pdaCpoa101.getCpoa101_Eft_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                       //Natural: ASSIGN #CPS-EFT-AMT := CPOA101.EFT-AMT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().setValue(pdaCpoa101.getCpoa101_Other_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ASSIGN #CPS-OTHER-CNT := CPOA101.OTHER-CNT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().setValue(pdaCpoa101.getCpoa101_Other_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ASSIGN #CPS-OTHER-AMT := CPOA101.OTHER-AMT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().setValue(pdaCpoa101.getCpoa101_Cs_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                         //Natural: ASSIGN #CPS-CS-CNT := CPOA101.CS-CNT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().setValue(pdaCpoa101.getCpoa101_Cs_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                         //Natural: ASSIGN #CPS-CS-AMT := CPOA101.CS-AMT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().setValue(pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                 //Natural: ASSIGN #CPS-REDRAW-CNT := CPOA101.REDRAW-CNT ( #O )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().setValue(pdaCpoa101.getCpoa101_Redraw_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                 //Natural: ASSIGN #CPS-REDRAW-AMT := CPOA101.REDRAW-AMT ( #O )
            pnd_Status.setValue(pnd_Status_After_Upd);                                                                                                                    //Natural: ASSIGN #STATUS := #STATUS-AFTER-UPD
            //*  PRINT PDA AMOUNTS
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
            sub_E10_Move_From_Ws_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  --------------------
        //*  PDA NOT EQUAL LAST 'PROCESS'
        if (condition(pnd_Out_Of_Balance.getBoolean()))                                                                                                                   //Natural: IF #OUT-OF-BALANCE
        {
            ldaCpol101.getPnd_Rt_Description().setValue(ldaCpol101.getRef_Rt_Description().getValue(pnd_Ws_Last));                                                        //Natural: ASSIGN #RT-DESCRIPTION := REF.RT-DESCRIPTION ( #WS-LAST )
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().nsubtract(pnd_Tot_Check_Cnt);                                                                            //Natural: SUBTRACT #TOT-CHECK-CNT FROM #CPS-CHECK-CNT
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().nsubtract(pnd_Tot_Check_Amt);                                                                            //Natural: SUBTRACT #TOT-CHECK-AMT FROM #CPS-CHECK-AMT
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().nsubtract(pnd_Tot_Eft_Cnt);                                                                                //Natural: SUBTRACT #TOT-EFT-CNT FROM #CPS-EFT-CNT
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().nsubtract(pnd_Tot_Eft_Amt);                                                                                //Natural: SUBTRACT #TOT-EFT-AMT FROM #CPS-EFT-AMT
            //*  11-28-2001
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().nsubtract(pnd_Tot_Other_Cnt);                                                                            //Natural: SUBTRACT #TOT-OTHER-CNT FROM #CPS-OTHER-CNT
            //*  11-28-2001
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().nsubtract(pnd_Tot_Other_Amt);                                                                            //Natural: SUBTRACT #TOT-OTHER-AMT FROM #CPS-OTHER-AMT
            //*  12-16-2002
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().nsubtract(pnd_Tot_Cs_Cnt);                                                                                  //Natural: SUBTRACT #TOT-CS-CNT FROM #CPS-CS-CNT
            //*  12-16-2002
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().nsubtract(pnd_Tot_Cs_Amt);                                                                                  //Natural: SUBTRACT #TOT-CS-AMT FROM #CPS-CS-AMT
            //*  12-16-2002
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().nsubtract(pnd_Tot_Redraw_Cnt);                                                                          //Natural: SUBTRACT #TOT-REDRAW-CNT FROM #CPS-REDRAW-CNT
            //*  12-16-2002
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().nsubtract(pnd_Tot_Redraw_Amt);                                                                          //Natural: SUBTRACT #TOT-REDRAW-AMT FROM #CPS-REDRAW-AMT
            pnd_Desc.setValue("DIFFERENCE");                                                                                                                              //Natural: ASSIGN #DESC := 'DIFFERENCE'
            pnd_Cycle_Pnd_Cycle_N.setValue(pdaCpoa101.getCpoa101_Cycle_Nbr());                                                                                            //Natural: ASSIGN #CYCLE-N := CPOA101.CYCLE-NBR
            pnd_Status.setValue("TERMINATED");                                                                                                                            //Natural: ASSIGN #STATUS := 'TERMINATED'
            pnd_Terminate.setValue(94);                                                                                                                                   //Natural: ASSIGN #TERMINATE := 94
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
            sub_E10_Move_From_Ws_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  ============================================
        if (condition(pdaCpoa101.getCpoa101_Function().equals("END OF DAY")))                                                                                             //Natural: IF CPOA101.FUNCTION = 'END OF DAY'
        {
            getReports().write(10, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(52),"CARRIED FORWARD FOR NEXT PROCESS DAY");                                       //Natural: WRITE ( 10 ) // 052T 'CARRIED FORWARD FOR NEXT PROCESS DAY'
            if (Global.isEscape()) return;
            pnd_Date_A8.setValue(pnd_Cps_Receive_Date);                                                                                                                   //Natural: ASSIGN #DATE-A8 := #CPS-RECEIVE-DATE
            pnd_Date_A8_Pnd_Date_N8.compute(new ComputeParameters(false, pnd_Date_A8_Pnd_Date_N8), DbsField.subtract(100000000,pnd_Date_A8_Pnd_Date_N8));                 //Natural: ASSIGN #DATE-N8 := 100000000 - #DATE-N8
            pnd_Date_X.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_A8);                                                                                        //Natural: MOVE EDITED #DATE-A8 TO #DATE-X ( EM = YYYYMMDD )
            //*                                     TO BRING THE RECEIVE UP-TO-DATE
            //*  03/02/01 AR
            //*  03/02/01 AR
            if (condition(Global.getDATX().greater(pnd_Date_X)))                                                                                                          //Natural: IF *DATX > #DATE-X THEN
            {
                pnd_Date_X.setValue(Global.getDATX());                                                                                                                    //Natural: MOVE *DATX TO #DATE-X
                //*  03/02/01 AR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Date_X.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #DATE-X
                //*  03/02/01 AR
            }                                                                                                                                                             //Natural: END-IF
            //*  TODAY's Date
            //*  11-28-2001
            pnd_Date_A8.setValueEdited(pnd_Date_X,new ReportEditMask("YYYYMMDD"));                                                                                        //Natural: MOVE EDITED #DATE-X ( EM = YYYYMMDD ) TO #DATE-A8
            FOR09:                                                                                                                                                        //Natural: FOR #O FROM 1 TO #ORGN-USED
            for (pdaCpoaorgn.getCpoaorgn_Pnd_O().setValue(1); condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().lessOrEqual(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used())); 
                pdaCpoaorgn.getCpoaorgn_Pnd_O().nadd(1))
            {
                //*  11-28-2001
                if (condition(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).equals(" ")))                                            //Natural: IF CPOAORGN.#ORGN-CDE-4 ( #O ) = ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  11-28-2001
                //*  11-28-2001
                //*  11-28-2001
                //*  11-28-2001
                //*  'CARRIED FORWARD'
                //*  11-28-2001
                //*  11-28-2001
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                pnd_Rt_Super_Pnd_Rt_Key.setValue(DbsUtil.compress(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()),                     //Natural: COMPRESS CPOAORGN.#ORGN-CDE-4 ( #O ) 'RECEIVE' INTO #RT-KEY
                    "RECEIVE"));
                pnd_Rt_Super_Pnd_Rt_Invrse_Dte.compute(new ComputeParameters(false, pnd_Rt_Super_Pnd_Rt_Invrse_Dte), DbsField.subtract(100000000,pnd_Date_A8_Pnd_Date_N8)); //Natural: ASSIGN #RT-INVRSE-DTE := 100000000 - #DATE-N8
                rt_Rt_A_I_Ind.setValue(pnd_Rt_Super_Pnd_Rt_A_I_Ind);                                                                                                      //Natural: ASSIGN RT.RT-A-I-IND := #RT-A-I-IND
                rt_Rt_Table_Id.setValue(pnd_Rt_Super_Pnd_Rt_Table_Id);                                                                                                    //Natural: ASSIGN RT.RT-TABLE-ID := #RT-TABLE-ID
                rt_Rt_Short_Key.setValue(pnd_Rt_Super_Pnd_Rt_Short_Key);                                                                                                  //Natural: ASSIGN RT.RT-SHORT-KEY := #RT-SHORT-KEY
                pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd.setValue(pnd_Date_A8_Pnd_Date_N8);                                                                                      //Natural: ASSIGN #NEW-REC-CCYYMMDD := #DATE-N8
                rt_Rt_Long_Key.setValue(pnd_Rt_Super_Pnd_Rt_Long_Key);                                                                                                    //Natural: ASSIGN RT.RT-LONG-KEY := #RT-SUPER.#RT-LONG-KEY
                rt_Cps_Ccyymmdd.setValue(pnd_Date_A8_Pnd_Date_N8);                                                                                                        //Natural: ASSIGN RT.CPS-CCYYMMDD := #DATE-N8
                rt_Cps_Date.setValue(pnd_Date_X);                                                                                                                         //Natural: ASSIGN RT.CPS-DATE := #DATE-X
                rt_Rt_Upd_Source.setValue(Global.getPROGRAM());                                                                                                           //Natural: ASSIGN RT.RT-UPD-SOURCE := *PROGRAM
                rt_Rt_Upd_User.setValue(Global.getINIT_USER());                                                                                                           //Natural: ASSIGN RT.RT-UPD-USER := *INIT-USER
                rt_Rt_Upd_Ccyymmdd.setValue(Global.getDATN());                                                                                                            //Natural: ASSIGN RT.RT-UPD-CCYYMMDD := *DATN
                rt_Rt_Upd_Timn.setValue(Global.getTIMN());                                                                                                                //Natural: ASSIGN RT.RT-UPD-TIMN := *TIMN
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cycle().setValue(1);                                                                                             //Natural: ASSIGN #CPS-CYCLE := 0001
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status().setValue("BROUGHT FORWARD");                                                                            //Natural: ASSIGN #CPS-STATUS := 'BROUGHT FORWARD'
                pnd_Orgn.setValue(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_4().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                                    //Natural: ASSIGN #ORGN := CPOAORGN.#ORGN-CDE-4 ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().setValue(pdaCpoa101.getCpoa101_Check_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ASSIGN #CPS-CHECK-CNT := CPOA101.CHECK-CNT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().setValue(pdaCpoa101.getCpoa101_Check_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ASSIGN #CPS-CHECK-AMT := CPOA101.CHECK-AMT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().setValue(pdaCpoa101.getCpoa101_Eft_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ASSIGN #CPS-EFT-CNT := CPOA101.EFT-CNT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().setValue(pdaCpoa101.getCpoa101_Eft_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                   //Natural: ASSIGN #CPS-EFT-AMT := CPOA101.EFT-AMT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().setValue(pdaCpoa101.getCpoa101_Other_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ASSIGN #CPS-OTHER-CNT := CPOA101.OTHER-CNT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().setValue(pdaCpoa101.getCpoa101_Other_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));               //Natural: ASSIGN #CPS-OTHER-AMT := CPOA101.OTHER-AMT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().setValue(pdaCpoa101.getCpoa101_Cs_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                     //Natural: ASSIGN #CPS-CS-CNT := CPOA101.CS-CNT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().setValue(pdaCpoa101.getCpoa101_Cs_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                     //Natural: ASSIGN #CPS-CS-AMT := CPOA101.CS-AMT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().setValue(pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));             //Natural: ASSIGN #CPS-REDRAW-CNT := CPOA101.REDRAW-CNT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().setValue(pdaCpoa101.getCpoa101_Redraw_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));             //Natural: ASSIGN #CPS-REDRAW-AMT := CPOA101.REDRAW-AMT ( #O )
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Unused_Cnt().setValue(0);                                                                                        //Natural: ASSIGN #CPS-UNUSED-CNT := 0
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Unused_Amt().setValue(0);                                                                                        //Natural: ASSIGN #CPS-UNUSED-AMT := 0
                //*  11-08-2002
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Restart().reset();                                                                                               //Natural: RESET #CPS-RESTART
                rt_Rt_Description.getValue(1).setValue(ldaCpol101.getPnd_Rt_Description());                                                                               //Natural: ASSIGN RT.RT-DESCRIPTION ( 1 ) := #RT-DESCRIPTION
                pnd_Desc.setValue("CARRIED FORWARD");                                                                                                                     //Natural: ASSIGN #DESC := 'CARRIED FORWARD'
                //*  PRINT NEW 'RECEIVE' RECORD(1)
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
                sub_E10_Move_From_Ws_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cycle().setValue(2);                                                                                             //Natural: ASSIGN #CPS-CYCLE := 0002
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status().setValue("CURRENT");                                                                                    //Natural: ASSIGN #CPS-STATUS := 'CURRENT'
                //*  11-28-2001
                //*  12-16-2002
                //*  12-16-2002
                //*  12-16-2002
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().reset();                                                                                             //Natural: RESET #CPS-CHECK-CNT #CPS-CHECK-AMT #CPS-EFT-CNT #CPS-EFT-AMT #CPS-OTHER-CNT #CPS-OTHER-AMT #CPS-CS-CNT #CPS-CS-AMT #CPS-REDRAW-CNT #CPS-REDRAW-AMT #CPS-UNUSED-CNT #CPS-UNUSED-AMT
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Unused_Cnt().reset();
                ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Unused_Amt().reset();
                rt_Rt_Description.getValue(2).setValue(ldaCpol101.getPnd_Rt_Description());                                                                               //Natural: ASSIGN RT.RT-DESCRIPTION ( 2 ) := #RT-DESCRIPTION
                //*  WRITE NEW 'RECEIVE' RECORD
                vw_rt.insertDBRow();                                                                                                                                      //Natural: STORE RT
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            //*  NOTE: ET IS DONE INTENTIONALLY HERE. THE ET SHOULD TAKE PLACE AFTER
            //*  THE CONTROL RECORDS ARE CREATED. THEN THE BALANCE CHECK MUST BE DONE.
            //*  THEN WE TERMINATE, IF THERE IS AN IMBALANCE.
            //*  THE IDEA IS THAT THE CONTROLS FOR THE NEXT DAY IS SET UP AND NEXT DAY
            //*  CYCLES CAN CONTINUE.  CPS CAN THEN TAKE IT's time to research and
            //*  CORRECT THE IMBALANCE. THE BATCH RUNS AND CYCLES ARE NOT HELD UP
            //*  BECAUSE OF A JOB DOWN
            //*  11-28-2001
            //*  11-28-2001
            //*  11-28-2001
            //*  12-16-2002
            //*  12-16-2002
            //*  12-16-2002
            //*  12-16-2002
            pnd_Rt_Super_Pnd_New_Rec_Ccyymmdd_A.reset();                                                                                                                  //Natural: RESET #NEW-REC-CCYYMMDD-A
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),              //Natural: ASSIGN #CPS-CHECK-CNT := #SUM-CHECK-CNT - #TOT-CHECK-CNT
                pnd_Sum_Check_Cnt.subtract(pnd_Tot_Check_Cnt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),              //Natural: ASSIGN #CPS-CHECK-AMT := #SUM-CHECK-AMT - #TOT-CHECK-AMT
                pnd_Sum_Check_Amt.subtract(pnd_Tot_Check_Amt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt()),                  //Natural: ASSIGN #CPS-EFT-CNT := #SUM-EFT-CNT - #TOT-EFT-CNT
                pnd_Sum_Eft_Cnt.subtract(pnd_Tot_Eft_Cnt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt()),                  //Natural: ASSIGN #CPS-EFT-AMT := #SUM-EFT-AMT - #TOT-EFT-AMT
                pnd_Sum_Eft_Amt.subtract(pnd_Tot_Eft_Amt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt()),              //Natural: ASSIGN #CPS-OTHER-CNT := #SUM-OTHER-CNT - #TOT-OTHER-CNT
                pnd_Sum_Other_Cnt.subtract(pnd_Tot_Other_Cnt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt()),              //Natural: ASSIGN #CPS-OTHER-AMT := #SUM-OTHER-AMT - #TOT-OTHER-AMT
                pnd_Sum_Other_Amt.subtract(pnd_Tot_Other_Amt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt()),                    //Natural: ASSIGN #CPS-CS-CNT := #SUM-CS-CNT - #TOT-CS-CNT
                pnd_Sum_Cs_Cnt.subtract(pnd_Tot_Cs_Cnt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt()),                    //Natural: ASSIGN #CPS-CS-AMT := #SUM-CS-AMT - #TOT-CS-AMT
                pnd_Sum_Cs_Amt.subtract(pnd_Tot_Cs_Amt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt()),            //Natural: ASSIGN #CPS-REDRAW-CNT := #SUM-REDRAW-CNT - #TOT-REDRAW-CNT
                pnd_Sum_Redraw_Cnt.subtract(pnd_Tot_Redraw_Cnt));
            ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().compute(new ComputeParameters(false, ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt()),            //Natural: ASSIGN #CPS-REDRAW-AMT := #SUM-REDRAW-AMT - #TOT-REDRAW-AMT
                pnd_Sum_Redraw_Amt.subtract(pnd_Tot_Redraw_Amt));
            getReports().write(0, "chk   ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt(),pnd_Sum_Check_Cnt,pnd_Tot_Check_Cnt);                                     //Natural: WRITE 'chk   ' #CPS-CHECK-CNT #SUM-CHECK-CNT #TOT-CHECK-CNT
            if (Global.isEscape()) return;
            getReports().write(0, "      ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt(),pnd_Sum_Check_Amt,pnd_Tot_Check_Amt);                                     //Natural: WRITE '      ' #CPS-CHECK-AMT #SUM-CHECK-AMT #TOT-CHECK-AMT
            if (Global.isEscape()) return;
            getReports().write(0, "eft   ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt(),pnd_Sum_Eft_Cnt,pnd_Tot_Eft_Cnt);                                           //Natural: WRITE 'eft   ' #CPS-EFT-CNT #SUM-EFT-CNT #TOT-EFT-CNT
            if (Global.isEscape()) return;
            getReports().write(0, "      ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt(),pnd_Sum_Eft_Amt,pnd_Tot_Eft_Amt);                                           //Natural: WRITE '      ' #CPS-EFT-AMT #SUM-EFT-AMT #TOT-EFT-AMT
            if (Global.isEscape()) return;
            getReports().write(0, "oth   ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt(),pnd_Sum_Other_Cnt,pnd_Tot_Other_Cnt);                                     //Natural: WRITE 'oth   ' #CPS-OTHER-CNT #SUM-OTHER-CNT #TOT-OTHER-CNT
            if (Global.isEscape()) return;
            getReports().write(0, "      ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt(),pnd_Sum_Other_Amt,pnd_Tot_Other_Amt);                                     //Natural: WRITE '      ' #CPS-OTHER-AMT #SUM-OTHER-AMT #TOT-OTHER-AMT
            if (Global.isEscape()) return;
            getReports().write(0, "cs    ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt(),pnd_Sum_Cs_Cnt,pnd_Tot_Cs_Cnt);                                              //Natural: WRITE 'cs    ' #CPS-CS-CNT #SUM-CS-CNT #TOT-CS-CNT
            if (Global.isEscape()) return;
            getReports().write(0, "      ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt(),pnd_Sum_Cs_Amt,pnd_Tot_Cs_Amt);                                              //Natural: WRITE '      ' #CPS-CS-AMT #SUM-CS-AMT #TOT-CS-AMT
            if (Global.isEscape()) return;
            getReports().write(0, "redrw ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt(),pnd_Sum_Redraw_Cnt,pnd_Tot_Redraw_Cnt);                                  //Natural: WRITE 'redrw ' #CPS-REDRAW-CNT #SUM-REDRAW-CNT #TOT-REDRAW-CNT
            if (Global.isEscape()) return;
            getReports().write(0, "      ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt(),pnd_Sum_Redraw_Amt,pnd_Tot_Redraw_Amt);                                  //Natural: WRITE '      ' #CPS-REDRAW-AMT #SUM-REDRAW-AMT #TOT-REDRAW-AMT
            if (Global.isEscape()) return;
            //*  ---------------------------------------------
            //*  11-28-2001
            //*  12-16-2002
            //*  12-16-2002
            if (condition(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt().notEquals(getZero()) || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt().notEquals(getZero())  //Natural: IF #CPS-CHECK-CNT NE 0 OR #CPS-CHECK-AMT NE 0 OR #CPS-EFT-CNT NE 0 OR #CPS-EFT-AMT NE 0 OR #CPS-OTHER-CNT NE 0 OR #CPS-OTHER-AMT NE 0 OR #CPS-CS-CNT NE 0 OR #CPS-CS-AMT NE 0 OR #CPS-REDRAW-CNT NE 0 OR #CPS-REDRAW-AMT NE 0
                || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt().notEquals(getZero()) || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt().notEquals(getZero()) 
                || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt().notEquals(getZero()) || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt().notEquals(getZero()) 
                || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt().notEquals(getZero()) || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt().notEquals(getZero()) 
                || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt().notEquals(getZero()) || ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt().notEquals(getZero())))
            {
                pnd_Desc.setValue("DAY END DIFFERENCE");                                                                                                                  //Natural: ASSIGN #DESC := 'DAY END DIFFERENCE'
                pnd_Cycle_Pnd_Cycle_N.setValue(pdaCpoa101.getCpoa101_Cycle_Nbr());                                                                                        //Natural: ASSIGN #CYCLE-N := CPOA101.CYCLE-NBR
                pnd_Status.setValue("TERMINATED");                                                                                                                        //Natural: ASSIGN #STATUS := 'TERMINATED'
                getReports().write(0, "Out of Balance- Checks ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt(),pnd_Tot_Check_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt(), //Natural: WRITE 'Out of Balance- Checks ' #CPS-CHECK-CNT #TOT-CHECK-CNT #CPS-CHECK-AMT #TOT-CHECK-AMT
                    pnd_Tot_Check_Amt);
                if (Global.isEscape()) return;
                getReports().write(0, "Out of Balance- EFTs   ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt(),pnd_Tot_Eft_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt(), //Natural: WRITE 'Out of Balance- EFTs   ' #CPS-EFT-CNT #TOT-EFT-CNT #CPS-EFT-AMT #TOT-EFT-AMT
                    pnd_Tot_Eft_Amt);
                if (Global.isEscape()) return;
                getReports().write(0, "Out of Balance- Other  ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt(),pnd_Tot_Other_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt(), //Natural: WRITE 'Out of Balance- Other  ' #CPS-OTHER-CNT #TOT-OTHER-CNT #CPS-OTHER-AMT #TOT-OTHER-AMT
                    pnd_Tot_Other_Amt);
                if (Global.isEscape()) return;
                getReports().write(0, "Out of Balance- C/S    ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Cnt(),pnd_Tot_Cs_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Cs_Amt(), //Natural: WRITE 'Out of Balance- C/S    ' #CPS-CS-CNT #TOT-CS-CNT #CPS-CS-AMT #TOT-CS-AMT
                    pnd_Tot_Cs_Amt);
                if (Global.isEscape()) return;
                //*  FORMERLY 95      /* 11-28-2001
                getReports().write(0, "Out of Balance- Redraw ",ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt(),pnd_Tot_Redraw_Cnt,ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Redraw_Amt(), //Natural: WRITE 'Out of Balance- Redraw ' #CPS-REDRAW-CNT #TOT-REDRAW-CNT #CPS-REDRAW-AMT #TOT-REDRAW-AMT
                    pnd_Tot_Redraw_Amt);
                if (Global.isEscape()) return;
                pnd_Terminate.setValue(84);                                                                                                                               //Natural: ASSIGN #TERMINATE := 84
                                                                                                                                                                          //Natural: PERFORM E10-MOVE-FROM-WS-REPORT
                sub_E10_Move_From_Ws_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  ----------------------------------------
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(10, ReportOption.NOTITLE,NEWLINE,"=",new RepeatItem(132),NEWLINE);                                                                             //Natural: WRITE ( 10 ) / '=' ( 132 ) /
        if (Global.isEscape()) return;
        //*  D10-PRINT-REPORT
    }
    //*  11-28-2001
    private void sub_E10_Move_From_Ws_Report() throws Exception                                                                                                           //Natural: E10-MOVE-FROM-WS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  11-28-2001
        if (condition(getReports().getAstLinesLeft(0).less(5)))                                                                                                           //Natural: NEWPAGE IF LESS THAN 5
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        if (condition((pnd_Desc.equals("TOTAL PROCESSED")) && (pdaCpoa101.getCpoa101_Procedure().equals("PCP1605D"))))                                                    //Natural: IF ( #DESC = 'TOTAL PROCESSED' ) AND ( CPOA101.PROCEDURE = 'PCP1605D' )
        {
            pnd_Other_Status_1.setValue("'OTHER' CHKS/STMNTS ARE NOT PRINTED");                                                                                           //Natural: ASSIGN #OTHER-STATUS-1 := '"OTHER" CHKS/STMNTS ARE NOT PRINTED'
            pnd_Other_Status_2.setValue("****   This is not an error!   ****");                                                                                           //Natural: ASSIGN #OTHER-STATUS-2 := '****   This is not an error!   ****'
        }                                                                                                                                                                 //Natural: END-IF
        //*  11-28-2001
        if (condition(pnd_Desc.equals("DIFFERENCE") || pnd_Desc.equals("DAY END DIFFERENCE")))                                                                            //Natural: IF #DESC = 'DIFFERENCE' OR = 'DAY END DIFFERENCE'
        {
            pnd_Cycle.reset();                                                                                                                                            //Natural: RESET #CYCLE #ORGN
            pnd_Orgn.reset();
            //* *MOVE EDITED #CPS-TIME (EM=MM'/'DD' 'HH':'II) TO #TIME
        }                                                                                                                                                                 //Natural: END-IF
        //*  11-28-2001
        //*  11-28-2001
        getReports().write(10, ReportOption.NOTITLE,NEWLINE,new ReportTAsterisk(pnd_Desc),pnd_Desc, new AlphanumericLength (18),new ReportTAsterisk(pnd_Cycle),pnd_Cycle,new  //Natural: WRITE ( 10 ) / T*#DESC #DESC ( AL = 18 ) T*#CYCLE #CYCLE T*#TIME #TIME T*#ORGN #ORGN T*#PYMNT-TYPE '    Checks' T*#CPS-CHECK-CNT #CPS-CHECK-CNT ( EM = Z,ZZZ,ZZ9- ) T*#CPS-CHECK-AMT #CPS-CHECK-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) T*#STAGE #STAGE T*#STATUS #STATUS / T*#ORGN #ORGN T*#PYMNT-TYPE '      EFTs' T*#CPS-CHECK-CNT #CPS-EFT-CNT ( EM = Z,ZZZ,ZZ9- ) T*#CPS-CHECK-AMT #CPS-EFT-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) / T*#ORGN #ORGN T*#PYMNT-TYPE '    Others' T*#CPS-CHECK-CNT #CPS-OTHER-CNT ( EM = Z,ZZZ,ZZ9- ) T*#CPS-CHECK-AMT #CPS-OTHER-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) T*#STATUS #OTHER-STATUS-1
            ReportTAsterisk(pnd_Time),pnd_Time,new ReportTAsterisk(pnd_Orgn),pnd_Orgn,new ReportTAsterisk(pnd_Pymnt_Type),"    Checks",new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9-"),new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Stage),pnd_Stage,new ReportTAsterisk(pnd_Status),pnd_Status,NEWLINE,new ReportTAsterisk(pnd_Orgn),pnd_Orgn,new 
            ReportTAsterisk(pnd_Pymnt_Type),"      EFTs",new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Cnt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9-"),new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Eft_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new ReportTAsterisk(pnd_Orgn),pnd_Orgn,new ReportTAsterisk(pnd_Pymnt_Type),"    Others",new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt()),ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Cnt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9-"),new ReportTAsterisk(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt()),ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Other_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ReportTAsterisk(pnd_Status),pnd_Other_Status_1);
        if (Global.isEscape()) return;
        //*  11-28-2001
        pnd_Cycle.reset();                                                                                                                                                //Natural: RESET #CYCLE #TIME #STAGE #STATUS #OTHER-STATUS-1
        pnd_Time.reset();
        pnd_Stage.reset();
        pnd_Status.reset();
        pnd_Other_Status_1.reset();
        //*  E10-MOVE-FROM-WS-REPORT
    }
    //*  11-28-2001
    private void sub_Z80_Symbolic_Translation() throws Exception                                                                                                          //Natural: Z80-SYMBOLIC-TRANSLATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  '&SYMBOLICS&' MUST ALWAYS BE IN UPPER CASE.
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&CALLPROG&"), new ExamineReplace(pdaCpoa101.getCpoa101_Module()));      //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&CALLPROG&' REPLACE CPOA101.MODULE
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&RECORD-KEY&"), new ExamineReplace(ldaCpol101a.getPnd_Rules_Record_Key())); //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&RECORD-KEY&' REPLACE #RULES.RECORD-KEY
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&FUNCTION&"), new ExamineReplace(pdaCpoa101.getCpoa101_Function()));    //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&FUNCTION&' REPLACE CPOA101.FUNCTION
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&LAST-STATUS&"), new ExamineReplace(ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Status())); //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&LAST-STATUS&' REPLACE #CPS-STATUS
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&TRY-FUNCTION&"), new ExamineReplace(ldaCpol101a.getPnd_Rules_Try_Function())); //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&TRY-FUNCTION&' REPLACE #RULES.TRY-FUNCTION
        //*  11-28-2001
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&RT-LONG-KEY&"), new ExamineReplace(pdaCpoa100.getCpoa100_Cpoa100_Long_Key())); //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&RT-LONG-KEY&' REPLACE CPOA100.CPOA100-LONG-KEY
                                                                                                                                                                          //Natural: PERFORM Z90-TRANSLATE-TABLEPROG
        sub_Z90_Translate_Tableprog();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM Z92-TRANSLATE-INVALID-STATUS
        sub_Z92_Translate_Invalid_Status();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM Z95-TRANSLATE-PREVIOUS-STATUS
        sub_Z95_Translate_Previous_Status();
        if (condition(Global.isEscape())) {return;}
        //*  Z80-SYMBOLIC-TRANSLATION
    }
    //*  11-28-2001
    private void sub_Z90_Translate_Tableprog() throws Exception                                                                                                           //Natural: Z90-TRANSLATE-TABLEPROG
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //* *INT (01) 'Inside Z90-TRANSLATE-TABLEPROG sub-routine.'   /* TEMPORARY
        pnd_Error_Ws_Pnd_Call_Progs.reset();                                                                                                                              //Natural: RESET #CALL-PROGS
        FOR10:                                                                                                                                                            //Natural: FOR #P 1 3
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(3)); pnd_P.nadd(1))
        {
            if (condition(ldaCpol101a.getPnd_Rules_Calling_Program().getValue(pnd_P).equals(" ")))                                                                        //Natural: IF #RULES.CALLING-PROGRAM ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_P.greater(1)))                                                                                                                              //Natural: IF #P > 1
            {
                pnd_Error_Ws_Pnd_Call_Progs.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Error_Ws_Pnd_Call_Progs, ",@"));                                 //Natural: COMPRESS #CALL-PROGS ',@' INTO #CALL-PROGS LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Error_Ws_Pnd_Call_Progs.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Error_Ws_Pnd_Call_Progs, ldaCpol101a.getPnd_Rules_Calling_Program().getValue(pnd_P))); //Natural: COMPRESS #CALL-PROGS #RULES.CALLING-PROGRAM ( #P ) INTO #CALL-PROGS LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Call_Progs,true), new ExamineSearch("@"), new ExamineReplace(" "));                                            //Natural: EXAMINE FULL #CALL-PROGS FOR '@' REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&TABLEPROG&"), new ExamineReplace(pnd_Error_Ws_Pnd_Call_Progs));        //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&TABLEPROG&' REPLACE #CALL-PROGS
        //*  Z90-TRANSLATE-TABLEPROG
    }
    //*  11-28-2001
    private void sub_Z92_Translate_Invalid_Status() throws Exception                                                                                                      //Natural: Z92-TRANSLATE-INVALID-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //* *INT (01) 'Inside Z92-TRANSLATE-INVALID-STATUS routine.'  /* TEMPORARY
        pnd_Error_Ws_Pnd_Invalid_Prev_Funct.reset();                                                                                                                      //Natural: RESET #INVALID-PREV-FUNCT
        FOR11:                                                                                                                                                            //Natural: FOR #P 1 3
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(3)); pnd_P.nadd(1))
        {
            if (condition(ldaCpol101a.getPnd_Rules_Invalid_Previous_Functions().getValue(pnd_P).equals(" ")))                                                             //Natural: IF #RULES.INVALID-PREVIOUS-FUNCTIONS ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_P.greater(1)))                                                                                                                              //Natural: IF #P > 1
            {
                pnd_Error_Ws_Pnd_Invalid_Prev_Funct.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Error_Ws_Pnd_Invalid_Prev_Funct, ",@"));                 //Natural: COMPRESS #INVALID-PREV-FUNCT ',@' INTO #INVALID-PREV-FUNCT LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Error_Ws_Pnd_Invalid_Prev_Funct.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Error_Ws_Pnd_Invalid_Prev_Funct, ldaCpol101a.getPnd_Rules_Invalid_Previous_Functions().getValue(pnd_P))); //Natural: COMPRESS #INVALID-PREV-FUNCT #RULES.INVALID-PREVIOUS-FUNCTIONS ( #P ) INTO #INVALID-PREV-FUNCT LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Invalid_Prev_Funct,true), new ExamineSearch("@"), new ExamineReplace(" "));                                    //Natural: EXAMINE FULL #INVALID-PREV-FUNCT FOR '@' REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&INVALID-STATUS&"), new ExamineReplace(pnd_Error_Ws_Pnd_Invalid_Prev_Funct)); //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&INVALID-STATUS&' REPLACE #INVALID-PREV-FUNCT
        //*  Z92-TRANSLATE-INVALID-STATUS
    }
    //*  11-28-2001
    private void sub_Z95_Translate_Previous_Status() throws Exception                                                                                                     //Natural: Z95-TRANSLATE-PREVIOUS-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //* *INT (01) 'Inside Z95-TRANSLATE-PREVIOUS-STATUS routine.' /* TEMPORARY
        pnd_Error_Ws_Pnd_Expect_Prev_Stat.reset();                                                                                                                        //Natural: RESET #EXPECT-PREV-STAT
        FOR12:                                                                                                                                                            //Natural: FOR #P 1 10
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(10)); pnd_P.nadd(1))
        {
            if (condition(ldaCpol101a.getPnd_Rules_Expected_Previous_Function().getValue(pnd_P).equals(" ")))                                                             //Natural: IF #RULES.EXPECTED-PREVIOUS-FUNCTION ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_P.greater(1)))                                                                                                                              //Natural: IF #P > 1
            {
                pnd_Error_Ws_Pnd_Expect_Prev_Stat.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Error_Ws_Pnd_Expect_Prev_Stat, ",@"));                     //Natural: COMPRESS #EXPECT-PREV-STAT ',@' INTO #EXPECT-PREV-STAT LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Error_Ws_Pnd_Expect_Prev_Stat.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Error_Ws_Pnd_Expect_Prev_Stat, ldaCpol101a.getPnd_Rules_Expected_Previous_Function().getValue(pnd_P))); //Natural: COMPRESS #EXPECT-PREV-STAT #RULES.EXPECTED-PREVIOUS-FUNCTION ( #P ) INTO #EXPECT-PREV-STAT LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Expect_Prev_Stat,true), new ExamineSearch("@"), new ExamineReplace(" "));                                      //Natural: EXAMINE FULL #EXPECT-PREV-STAT FOR '@' REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Error_Ws_Pnd_Error_Description), new ExamineSearch("&PREVIOUS-STATUS&"), new ExamineReplace(pnd_Error_Ws_Pnd_Expect_Prev_Stat)); //Natural: EXAMINE #ERROR-DESCRIPTION FOR '&PREVIOUS-STATUS&' REPLACE #EXPECT-PREV-STAT
        //*  Z95-TRANSLATE-PREVIOUS-STATUS
    }
    //*  11-28-2001
    private void sub_Z99_Common_Error() throws Exception                                                                                                                  //Natural: Z99-COMMON-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //* *INT (01) 'Inside Z99-COMMON-ERROR sub-routine.'          /* TEMPORARY
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(132),NEWLINE,"*",new RepeatItem(132),NEWLINE,"**",new TabSetting(130),"**",NEWLINE,"**",new              //Natural: WRITE // '*' ( 132 ) / '*' ( 132 ) / '**' 130T '**' / '**' 004T 'INIT-USER :' *INIT-USER 002X 'PROGRAM:' *PROGRAM 002X 'LIBRARY:' *LIBRARY-ID 002X 'DATE:' *DATX ( EM = MM/DD/YYYY ) 002X 'TIME:' *TIMX ( EM = HH:IIAP ) 130T '**' / '**' 004T 'INVALID CALL / PROCESS INITIATED FROM CALLING' 'PROCEDURE:' CPOA101.PROCEDURE 002X 'CALLING PROGRAM:' CPOA101.MODULE 130T '**'
            TabSetting(4),"INIT-USER :",Global.getINIT_USER(),new ColumnSpacing(2),"PROGRAM:",Global.getPROGRAM(),new ColumnSpacing(2),"LIBRARY:",Global.getLIBRARY_ID(),new 
            ColumnSpacing(2),"DATE:",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(2),"TIME:",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(130),"**",NEWLINE,"**",new TabSetting(4),"INVALID CALL / PROCESS INITIATED FROM CALLING","PROCEDURE:",pdaCpoa101.getCpoa101_Procedure(),new 
            ColumnSpacing(2),"CALLING PROGRAM:",pdaCpoa101.getCpoa101_Module(),new TabSetting(130),"**");
        if (Global.isEscape()) return;
        pnd_Error_Ws.reset();                                                                                                                                             //Natural: RESET #ERROR-WS
        if (condition(pnd_Terminate.greater(getZero())))                                                                                                                  //Natural: IF #TERMINATE > 0
        {
            pdaCpoa100.getCpoa100_Cpoa100_Table_Id().setValue("CPARM");                                                                                                   //Natural: ASSIGN CPOA100-TABLE-ID := 'CPARM'
            pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().setValue("A");                                                                                                        //Natural: ASSIGN CPOA100-A-I-IND := 'A'
            pdaCpoa100.getCpoa100_Cpoa100_Short_Key().setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN CPOA100-SHORT-KEY := *PROGRAM
            pdaCpoa100.getCpoa100_Cpoa100_Long_Key().setValue(pnd_Terminate_Pnd_Terminate_A);                                                                             //Natural: ASSIGN CPOA100-LONG-KEY := #TERMINATE-A
            DbsUtil.callnat(Cpon100.class , getCurrentProcessState(), pdaCpoa100.getCpoa100().getValue("*"));                                                             //Natural: CALLNAT 'CPON100' CPOA100 ( * )
            if (condition(Global.isEscape())) return;
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_Return_Code().equals("00")))                                                                                      //Natural: IF CPOA100-RETURN-CODE = '00'
            {
                pnd_Error_Ws_Pnd_Error_Description.setValue(pdaCpoa100.getCpoa100_Rt_Desc1());                                                                            //Natural: ASSIGN #ERROR-DESCRIPTION := CPOA100.RT-DESC1
                                                                                                                                                                          //Natural: PERFORM Z80-SYMBOLIC-TRANSLATION
                sub_Z80_Symbolic_Translation();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Error_Ws_Pnd_Error_Description.setValue(DbsUtil.compress("Terminate Code", pnd_Terminate, "Invalid.  Please add entry to 'CPARM' Reference Table.")); //Natural: COMPRESS 'Terminate Code' #TERMINATE 'Invalid.  Please add entry to "CPARM" Reference Table.' INTO #ERROR-DESCRIPTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"**",new TabSetting(4),"FUNCTION  :",pdaCpoa101.getCpoa101_Function(),new TabSetting(130),"**",NEWLINE,"**",new                     //Natural: WRITE / '**' 004T 'FUNCTION  :' CPOA101.FUNCTION 130T '**' / '**' 004T 'ERROR     :' #TERMINATE ( EM = 9999 ) 130T '**' / '**' 130T '**' / '**' 004T 'MESSAGE(S):' #PROG-MESSAGE 130T '**' / '**' 016T #ERROR-DESC-LINE-1 130T '**'
            TabSetting(4),"ERROR     :",pnd_Terminate, new ReportEditMask ("9999"),new TabSetting(130),"**",NEWLINE,"**",new TabSetting(130),"**",NEWLINE,"**",new 
            TabSetting(4),"MESSAGE(S):",pnd_Prog_Message,new TabSetting(130),"**",NEWLINE,"**",new TabSetting(16),pnd_Error_Ws_Pnd_Error_Desc_Line_1,new 
            TabSetting(130),"**");
        if (Global.isEscape()) return;
        if (condition(pnd_Error_Ws_Pnd_Error_Desc_Line_2.notEquals(" ")))                                                                                                 //Natural: IF #ERROR-DESC-LINE-2 NE ' '
        {
            getReports().write(0, NEWLINE,"**",new TabSetting(16),pnd_Error_Ws_Pnd_Error_Desc_Line_2,new TabSetting(130),"**");                                           //Natural: WRITE / '**' 016T #ERROR-DESC-LINE-2 130T '**'
            if (Global.isEscape()) return;
            pnd_Error_Ws_Pnd_Error_Desc_Line_2.reset();                                                                                                                   //Natural: RESET #ERROR-DESC-LINE-2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Error_Ws_Pnd_Error_Desc_Line_3.notEquals(" ")))                                                                                                 //Natural: IF #ERROR-DESC-LINE-3 NE ' '
        {
            getReports().write(0, NEWLINE,"**",new TabSetting(16),pnd_Error_Ws_Pnd_Error_Desc_Line_3,new TabSetting(130),"**");                                           //Natural: WRITE / '**' 016T #ERROR-DESC-LINE-3 130T '**'
            if (Global.isEscape()) return;
            pnd_Error_Ws_Pnd_Error_Desc_Line_3.reset();                                                                                                                   //Natural: RESET #ERROR-DESC-LINE-3
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Error_Ws_Pnd_Error_Desc_Line_4.notEquals(" ")))                                                                                                 //Natural: IF #ERROR-DESC-LINE-4 NE ' '
        {
            getReports().write(0, NEWLINE,"**",new TabSetting(16),pnd_Error_Ws_Pnd_Error_Desc_Line_4,new TabSetting(130),"**");                                           //Natural: WRITE / '**' 016T #ERROR-DESC-LINE-4 130T '**'
            if (Global.isEscape()) return;
            pnd_Error_Ws_Pnd_Error_Desc_Line_4.reset();                                                                                                                   //Natural: RESET #ERROR-DESC-LINE-4
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"**",new TabSetting(130),"**",NEWLINE,"*",new RepeatItem(132),NEWLINE,"*",new RepeatItem(132));                                     //Natural: WRITE / '**' 130T '**' / '*' ( 132 ) / '*' ( 132 )
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        if (condition(pnd_Terminate.greater(getZero())))                                                                                                                  //Natural: IF #TERMINATE > 0
        {
            //*  DEVELOPMENT RUN
            if (condition(ldaCpol101a.getPnd_Rules_Test().equals("TEST")))                                                                                                //Natural: IF #RULES.TEST EQ 'TEST'
            {
                getReports().write(0, NEWLINE,"Termination by-passed... Check",ldaCpol101a.getPnd_Rules_Test(),"parameter on OMNI CONTROLS table.");                      //Natural: WRITE / 'Termination by-passed... Check' #RULES.TEST 'parameter on OMNI CONTROLS table.'
                if (Global.isEscape()) return;
                //*  PRODUCTION RUN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    PRINT (01) 'About to print report from Z99-COMMON-ERROR...'  /* TEMP
                                                                                                                                                                          //Natural: PERFORM D10-PRINT-REPORT
                sub_D10_Print_Report();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(pnd_Terminate);  if (true) return;                                                                                                      //Natural: TERMINATE #TERMINATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Prog_Message.reset();                                                                                                                                     //Natural: RESET #PROG-MESSAGE
        }                                                                                                                                                                 //Natural: END-IF
        //*  Z99-COMMON-ERROR
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt10 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  01-21-2004
                    if (condition(pnd_First_Cntl.getBoolean()))                                                                                                           //Natural: IF #FIRST-CNTL
                    {
                        pnd_First_Cntl.setValue(false);                                                                                                                   //Natural: ASSIGN #FIRST-CNTL := FALSE
                        pnd_Ws_Date_A_Pnd_Ws_Date_N.setValue(ldaCpol101.getRef_Cps_Ccyymmdd());                                                                           //Natural: MOVE REF.CPS-CCYYMMDD TO #WS-DATE-N
                        pnd_Ws_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Date_A);                                                                       //Natural: MOVE EDITED #WS-DATE-A TO #WS-DATE-D ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(10, ReportOption.NOTITLE,new TabSetting(1),Global.getDATU(),"-",new TabSetting(12),Global.getTIMX(), new ReportEditMask            //Natural: WRITE ( 10 ) NOTITLE 001T *DATU '-' 012T *TIMX ( EM = HH':'IIAP ) 048T 'CONSOLIDATED PAYMENT SYSTEM' 110T 'Page:' *PAGE-NUMBER ( 10 ) ( AD = L ) / 001T *INIT-USER '-' 012T *PROGRAM 040T 'PROCESS CONTROL REPORT for' #WS-DATE-D ( EM = LLLLLLLLL' 'DD', 'YYYY ) / 001T 'Called from :' CPOA101.PROCEDURE '/' CPOA101.MODULE '/' CPOA101.FUNCTION /
                        ("HH':'IIAP"),new TabSetting(48),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(110),"Page:",getReports().getPageNumberDbs(10), new 
                        FieldAttributes ("AD=L"),NEWLINE,new TabSetting(1),Global.getINIT_USER(),"-",new TabSetting(12),Global.getPROGRAM(),new TabSetting(40),"PROCESS CONTROL REPORT for",pnd_Ws_Date_D, 
                        new ReportEditMask ("LLLLLLLLL' 'DD', 'YYYY"),NEWLINE,new TabSetting(1),"Called from :",pdaCpoa101.getCpoa101_Procedure(),"/",pdaCpoa101.getCpoa101_Module(),
                        "/",pdaCpoa101.getCpoa101_Function(),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=160 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(10, "PS=58 LS=134 ZP=ON SF=2");

        getReports().setDisplayColumns(10, "    ",
        		pnd_Desc,"/Cycle",
        		pnd_Cycle,"/Date & Time",
        		pnd_Time, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"/Orgn",
        		pnd_Orgn,"Payment/Type",
        		pnd_Pymnt_Type, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Number of/Payments",
        		ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Cnt(), new ReportEditMask ("Z,ZZZ,ZZ9-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "/Payment Amount",
        		ldaCpol101.getPnd_Rt_Description_Pnd_Cps_Check_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
            "/Restart",
        		pnd_Stage, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/Status",
        		pnd_Status, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left));
    }
}
