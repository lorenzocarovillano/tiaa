/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:56 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn372
************************************************************
**        * FILE NAME            : Fcpn372.java
**        * CLASS NAME           : Fcpn372
**        * INSTANCE NAME        : Fcpn372
************************************************************
***********************************************************************
*  LEON SILBERSTEIN
***********************************************************************
* SYSTEM   : CPS
* TITLE    : ANNUITANT STATEMENT
* GENERATED: JUL 26,93 AT 10:04 AM
* FUNCTION : THIS PROGRAM WILL BUILD IA_DEATH STATEMENT.
*
***********************************************************************
*
* HISTORY
*    03/02/99 LEON GURTOVNIK    - MULTIPLE PAGES, BARCODE
*    10/03/96 LIN ZHENG         - INFLATION LINKED BOND
*    10/23/94 LEON SILBERSTEIN
*    04/01/98 GLORY PHILIP
*           - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
*    05/00    MCGEE             -ADD PA-SELECT SPIA
*    09/06/2001 R. CARREON
*           - FIXED THE LOGIC FOR TAX CODES 'F' AND 'N' (US OR NRA)
*    03/14/02 USE TX-ELCT-TRGGR TO TEST FOR NRA
*    05/05/2014 CTS  -  PRB62858 - P1405CPD ABEND FIX - CORRECTION OF
*                    #NBR-OF-REMAINING-DEDUCTIONS POPULATION LOGIC FOR
*                    PYMNT-DED-AMT NE ZERO CASES  /* TAG - CTS *
* ----------------------------------------------------------------------
*  NOTES:
* ----------------------------------------------------------------------
* A "Group of Printed Lines" CONSISTS OF:
*  1. A SINGLE "A line" - "group Total" LINE
*  2. A SINGLE "Db" LINE - THE WORD "Units" OR "Contractual"
*  3. A SINGLE "Dc" LINE - THE WORD "Unit Value" OR "Dividend"
*  4. "Deduction Lines" - SPECIFYING THE DEDUCTION NOT SHOWN ON THE
*        "Units" LINE OR THE "Unit Value" LINE.
*      THERE MAY BE NONE OR UP TO 10 "Deduction Lines" DEPENDING ON THE
*      NUMBER OF NON-ZERO DEDUCTIONS SPECIFIED FOR A PAYMENT.
*      NOTE, THAT THE TAX DEDUCTIONS AND OHTER DEDUCTIONS ARE SHOWN
*      AT THE SAME LINE POSITION. THE "deduction" PRINT AREA IS FILLED
*      IN THE FOLLOWING SEQUENCE:
*      4.1. "Federal" TAX
*      4.2. "State" TAX
*      4.3. "Local" TAX
*      4.4. THE OTHER DEDUCTIONS.
*      I.E.,  IF THERE ARE NO TAXES THE FIRST "other" DEDUCTION IS
*        SHOWN IN THE "Units" LINE.
* ----------------------------------------------------------------------
* IA-DEATH SETTLES FEW SETTLEMENTS, WHICH ARE PRINTED USING
* THE FOLLOWING PROTOTYPES:
*
*      SETTLEMENT                   CNTRCT-TYPE-CDE   PROTOTYPE#
*      TYPE
*  --  ---------------------        ----------------- ---------------
*
*  1.  PERIODIC PAYMENT - PAST      'PP'              4
*  2.  PERIODIC PAYMENT - FUTURE    'PP'              4
*  3.  LUMP SUM DEATH  (CLOSEOUT)   'L'               4
*
* ----------------------------------------------------------------------
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn372 extends BLNatBase
{
    // Data Areas
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ws_Header_Record;

    private DbsGroup pnd_Ws_Header_Record__R_Field_1;

    private DbsGroup pnd_Ws_Header_Record_Pnd_Ws_Header_Level2;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_2;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;
    private DbsField pnd_Ws_Occurs;

    private DbsGroup pnd_Ws_Occurs__R_Field_3;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;

    private DbsGroup pnd_Ws_Occurs_Pnd_Pyhdr_Data;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte;

    private DbsGroup pnd_Ws_Occurs__R_Field_4;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4;

    private DbsGroup pnd_Ws_Occurs__R_Field_5;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6;

    private DbsGroup pnd_Ws_Occurs__R_Field_6;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End;

    private DbsGroup pnd_Ws_Occurs__R_Field_7;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde;

    private DbsGroup pnd_Ws_Occurs__R_Field_8;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6;

    private DbsGroup pnd_Ws_Occurs__R_Field_9;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler;
    private DbsField pnd_Ws_Name_N_Address;

    private DbsGroup pnd_Ws_Name_N_Address__R_Field_10;

    private DbsGroup pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Name_N_Address_Rcrd_Typ;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Name_N_Address_Ph_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Last_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_First_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Middle_Name;

    private DbsGroup pnd_Ws_Name_N_Address_Nme_N_Addr;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Nme;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Postl_Data;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Address_Filler;
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ws_Cntrct_Insurance_Option;
    private DbsField pnd_Ws_Cntrct_Life_Contingency;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_11;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc;

    private DbsGroup pnd_Ws_Rec_1_Pnd_Ws_Columns;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column;
    private DbsField pnd_Ws_Rec_1_Pnd_Ws_Column_Spacer;
    private DbsField pnd_Ws_Rec_2;

    private DbsGroup pnd_Ws_Rec_2__R_Field_12;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler;
    private DbsField pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name;

    private DbsGroup pnd_Ws_Ivc_Area;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text;
    private DbsField pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text;
    private DbsField pnd_Top_Of_Page;
    private DbsField pnd_Ws_First_Time;
    private DbsField pnd_C;
    private DbsField pnd_F;
    private DbsField pnd_1st_F;
    private DbsField pnd_Nbr_Of_Columns;
    private DbsField pnd_J;
    private DbsField pnd_L;
    private DbsField pnd_K;
    private DbsField pnd_Ws_Temp_Col;
    private DbsField pnd_Ws_Temp_Col2;
    private DbsField pnd_Ws_Temp_Deduction;
    private DbsField pnd_Nbr_Of_Remaining_Deductions;
    private DbsField pnd_Index_Of_Last_Used_Deduction;
    private DbsField pnd_Highest_Index_Of_Deduction;
    private DbsField pnd_Deduction_Column_Nbr;
    private DbsField pnd_Ws_Dpi_Plus_Dci_Amt;
    private DbsField pnd_I;
    private DbsField pnd_Ws_Fielda;
    private DbsField pnd_Ws_Fieldn;

    private DbsGroup pnd_Ws_Fieldn__R_Field_13;
    private DbsField pnd_Ws_Fieldn_Pnd_Ws_Int;
    private DbsField pnd_Ws_Fieldn_Pnd_Ws_Rem;
    private DbsField pnd_Ws_Masked_Amt;
    private DbsField pnd_Ws_Column_Offset;

    private DbsGroup pnd_Ws_Column_Offset__R_Field_14;
    private DbsField pnd_Ws_Column_Offset__Filler1;
    private DbsField pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data;

    private DbsGroup pnd_Ws_Column_Offset__R_Field_15;
    private DbsField pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data;
    private DbsField pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1;
    private DbsField pnd_Ws_Letter;
    private DbsField pnd_Ws_Legend;
    private DbsField pnd_Ws_Literal;
    private DbsField pnd_Ws_Printed;
    private DbsField pnd_Ws_Dpi_Dci_Hdr_Written;
    private DbsField pnd_Ws_Deduct_Hdr_Written;
    private DbsField pnd_Ws_Dpi_Hdr_Written;
    private DbsField pnd_Ws_Printed_Cref_Heading;
    private DbsField pnd_Ws_Printed_Tiaa_Heading;
    private DbsField pnd_Ws_Printed_Spia_Heading;
    private DbsField pnd_Ws_Printed_Select_Heading;
    private DbsField pnd_Ws_Tot_Payment_Amt;
    private DbsField pnd_Ws_Tot_Dpi_Plus_Dci_Amt;
    private DbsField pnd_Ws_Tot_Ded_Amt;
    private DbsField pnd_Ws_Tot_Check_Amt;
    private DbsField pnd_Ws_Grand_Tot_End_Accum;
    private DbsField pnd_Ws_Grand_Tot_Payment;
    private DbsField pnd_Ws_Grand_Tot_Dpi_Plus_Dci;
    private DbsField pnd_Ws_Grand_Tot_Ded;
    private DbsField pnd_Ws_Grand_Tot_Check;
    private DbsField pnd_Ws_Grand_Tot_Dpi;
    private DbsField pnd_Ws_Leon_Rememb_Setl_Dt;
    private DbsField pnd_First_Time_Sw;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Ws_Header_Record = parameters.newFieldArrayInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 
            500));
        pnd_Ws_Header_Record.setParameterOption(ParameterOption.ByReference);

        pnd_Ws_Header_Record__R_Field_1 = parameters.newGroupInRecord("pnd_Ws_Header_Record__R_Field_1", "REDEFINE", pnd_Ws_Header_Record);

        pnd_Ws_Header_Record_Pnd_Ws_Header_Level2 = pnd_Ws_Header_Record__R_Field_1.newGroupInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Level2", "#WS-HEADER-LEVEL2");
        pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", 
            "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", 
            "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", 
            "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", 
            "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", 
            "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", 
            "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", 
            "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", 
            "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", 
            "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", 
            "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", 
            "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", 
            "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", 
            "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", 
            "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_2 = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newGroupInGroup("pnd_Ws_Header_Record__R_Field_2", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", 
            "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", 
            "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", 
            "#WS-HEADER-FILLER", FieldType.STRING, 154);
        pnd_Ws_Occurs = parameters.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 1, new DbsArrayController(1, 40, 1, 500));
        pnd_Ws_Occurs.setParameterOption(ParameterOption.ByReference);

        pnd_Ws_Occurs__R_Field_3 = parameters.newGroupInRecord("pnd_Ws_Occurs__R_Field_3", "REDEFINE", pnd_Ws_Occurs);

        pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2 = pnd_Ws_Occurs__R_Field_3.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2", "#WS-OCCURS-LEVEL2", 
            new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 4);
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));

        pnd_Ws_Occurs_Pnd_Pyhdr_Data = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newGroupInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Data", "#PYHDR-DATA");
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte", "#PYHDR-PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);

        pnd_Ws_Occurs__R_Field_4 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_4", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4 = pnd_Ws_Occurs__R_Field_4.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4", "#PYHDR-PYMNT-SETTLMNT-DTE-A4", 
            FieldType.STRING, 4);

        pnd_Ws_Occurs__R_Field_5 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_5", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6 = pnd_Ws_Occurs__R_Field_5.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6", "#PYHDR-PYMNT-SETTLMNT-DTE-P6", 
            FieldType.PACKED_DECIMAL, 6);

        pnd_Ws_Occurs__R_Field_6 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_6", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start = pnd_Ws_Occurs__R_Field_6.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-START", FieldType.DATE);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-END", FieldType.DATE);

        pnd_Ws_Occurs__R_Field_7 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_7", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde = pnd_Ws_Occurs__R_Field_7.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde", "#PYHDR-ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde = pnd_Ws_Occurs__R_Field_7.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde", "#PYHDR-ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);

        pnd_Ws_Occurs__R_Field_8 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_8", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6 = pnd_Ws_Occurs__R_Field_8.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-END-P6", FieldType.PACKED_DECIMAL, 6);

        pnd_Ws_Occurs__R_Field_9 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_9", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4 = pnd_Ws_Occurs__R_Field_9.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-END-A4", FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", 
            FieldType.STRING, 180);
        pnd_Ws_Name_N_Address = parameters.newFieldArrayInRecord("pnd_Ws_Name_N_Address", "#WS-NAME-N-ADDRESS", FieldType.STRING, 1, new DbsArrayController(1, 
            2, 1, 500));
        pnd_Ws_Name_N_Address.setParameterOption(ParameterOption.ByReference);

        pnd_Ws_Name_N_Address__R_Field_10 = parameters.newGroupInRecord("pnd_Ws_Name_N_Address__R_Field_10", "REDEFINE", pnd_Ws_Name_N_Address);

        pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2 = pnd_Ws_Name_N_Address__R_Field_10.newGroupArrayInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2", 
            "#WS-NAME-N-ADDR-LEVEL2", new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Name_N_Address_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Name_N_Address_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Name_N_Address_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Name_N_Address_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Name_N_Address_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Name_N_Address_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Name_N_Address_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Name_N_Address_Rcrd_Typ = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Rcrd_Typ", "RCRD-TYP", 
            FieldType.STRING, 1);
        pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr", 
            "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Ws_Name_N_Address_Cntrct_Payee_Cde = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte", 
            "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7);

        pnd_Ws_Name_N_Address_Ph_Name = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newGroupInGroup("pnd_Ws_Name_N_Address_Ph_Name", "PH-NAME");
        pnd_Ws_Name_N_Address_Ph_Last_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Ws_Name_N_Address_Ph_First_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Ws_Name_N_Address_Ph_Middle_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Middle_Name", "PH-MIDDLE-NAME", 
            FieldType.STRING, 12);

        pnd_Ws_Name_N_Address_Nme_N_Addr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newGroupInGroup("pnd_Ws_Name_N_Address_Nme_N_Addr", "NME-N-ADDR");
        pnd_Ws_Name_N_Address_Pymnt_Nme = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", 
            FieldType.STRING, 9);
        pnd_Ws_Name_N_Address_Pymnt_Postl_Data = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", 
            FieldType.STRING, 32);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte", 
            "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 8);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme", 
            "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 7);
        pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id", 
            "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr", 
            "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind", 
            "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme", 
            "PYMNT-DECEASED-NME", FieldType.STRING, 38);
        pnd_Ws_Name_N_Address_Cntrct_Hold_Tme = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Hold_Tme", 
            "CNTRCT-HOLD-TME", FieldType.TIME);
        pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Address_Filler = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Address_Filler", 
            "#WS-NAME-N-ADDRESS-FILLER", FieldType.STRING, 8);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Side_Printed = parameters.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Side_Printed.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Annty_Ins_Type = parameters.newFieldInRecord("pnd_Ws_Cntrct_Annty_Ins_Type", "#WS-CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Cntrct_Annty_Ins_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Annty_Type_Cde = parameters.newFieldInRecord("pnd_Ws_Cntrct_Annty_Type_Cde", "#WS-CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        pnd_Ws_Cntrct_Annty_Type_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Insurance_Option = parameters.newFieldInRecord("pnd_Ws_Cntrct_Insurance_Option", "#WS-CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1);
        pnd_Ws_Cntrct_Insurance_Option.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Life_Contingency = parameters.newFieldInRecord("pnd_Ws_Cntrct_Life_Contingency", "#WS-CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1);
        pnd_Ws_Cntrct_Life_Contingency.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_11 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_11", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc = pnd_Ws_Rec_1__R_Field_11.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc", "#WS-REC1-CC", FieldType.STRING, 2);

        pnd_Ws_Rec_1_Pnd_Ws_Columns = pnd_Ws_Rec_1__R_Field_11.newGroupArrayInGroup("pnd_Ws_Rec_1_Pnd_Ws_Columns", "#WS-COLUMNS", new DbsArrayController(1, 
            7));
        pnd_Ws_Rec_1_Pnd_Ws_Column = pnd_Ws_Rec_1_Pnd_Ws_Columns.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column", "#WS-COLUMN", FieldType.STRING, 13);
        pnd_Ws_Rec_1_Pnd_Ws_Column_Spacer = pnd_Ws_Rec_1_Pnd_Ws_Columns.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Ws_Column_Spacer", "#WS-COLUMN-SPACER", FieldType.STRING, 
            1);
        pnd_Ws_Rec_2 = localVariables.newFieldInRecord("pnd_Ws_Rec_2", "#WS-REC-2", FieldType.STRING, 143);

        pnd_Ws_Rec_2__R_Field_12 = localVariables.newGroupInRecord("pnd_Ws_Rec_2__R_Field_12", "REDEFINE", pnd_Ws_Rec_2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc = pnd_Ws_Rec_2__R_Field_12.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Cc", "#WS-REC2-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler = pnd_Ws_Rec_2__R_Field_12.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Filler", "#WS-REC2-FILLER", FieldType.STRING, 
            35);
        pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name = pnd_Ws_Rec_2__R_Field_12.newFieldInGroup("pnd_Ws_Rec_2_Pnd_Ws_Rec2_Name", "#WS-REC2-NAME", FieldType.STRING, 38);

        pnd_Ws_Ivc_Area = localVariables.newGroupInRecord("pnd_Ws_Ivc_Area", "#WS-IVC-AREA");
        pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc", "#WS-TIAA-IVC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc", "#WS-CREF-IVC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc", "#WS-GOOD-IVC", FieldType.BOOLEAN, 1);
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text", "#WS-IVC-TIAA-TEXT", FieldType.STRING, 
            21);
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text = pnd_Ws_Ivc_Area.newFieldInGroup("pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text", "#WS-IVC-CREF-TEXT", FieldType.STRING, 
            21);
        pnd_Top_Of_Page = localVariables.newFieldInRecord("pnd_Top_Of_Page", "#TOP-OF-PAGE", FieldType.BOOLEAN, 1);
        pnd_Ws_First_Time = localVariables.newFieldInRecord("pnd_Ws_First_Time", "#WS-FIRST-TIME", FieldType.STRING, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.INTEGER, 2);
        pnd_1st_F = localVariables.newFieldInRecord("pnd_1st_F", "#1ST-F", FieldType.INTEGER, 2);
        pnd_Nbr_Of_Columns = localVariables.newFieldInRecord("pnd_Nbr_Of_Columns", "#NBR-OF-COLUMNS", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_Ws_Temp_Col = localVariables.newFieldInRecord("pnd_Ws_Temp_Col", "#WS-TEMP-COL", FieldType.STRING, 14);
        pnd_Ws_Temp_Col2 = localVariables.newFieldInRecord("pnd_Ws_Temp_Col2", "#WS-TEMP-COL2", FieldType.STRING, 14);
        pnd_Ws_Temp_Deduction = localVariables.newFieldInRecord("pnd_Ws_Temp_Deduction", "#WS-TEMP-DEDUCTION", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Nbr_Of_Remaining_Deductions = localVariables.newFieldInRecord("pnd_Nbr_Of_Remaining_Deductions", "#NBR-OF-REMAINING-DEDUCTIONS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Index_Of_Last_Used_Deduction = localVariables.newFieldInRecord("pnd_Index_Of_Last_Used_Deduction", "#INDEX-OF-LAST-USED-DEDUCTION", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Highest_Index_Of_Deduction = localVariables.newFieldInRecord("pnd_Highest_Index_Of_Deduction", "#HIGHEST-INDEX-OF-DEDUCTION", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Deduction_Column_Nbr = localVariables.newFieldInRecord("pnd_Deduction_Column_Nbr", "#DEDUCTION-COLUMN-NBR", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Dpi_Plus_Dci_Amt = localVariables.newFieldInRecord("pnd_Ws_Dpi_Plus_Dci_Amt", "#WS-DPI-PLUS-DCI-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Fielda = localVariables.newFieldInRecord("pnd_Ws_Fielda", "#WS-FIELDA", FieldType.STRING, 12);
        pnd_Ws_Fieldn = localVariables.newFieldInRecord("pnd_Ws_Fieldn", "#WS-FIELDN", FieldType.NUMERIC, 11, 2);

        pnd_Ws_Fieldn__R_Field_13 = localVariables.newGroupInRecord("pnd_Ws_Fieldn__R_Field_13", "REDEFINE", pnd_Ws_Fieldn);
        pnd_Ws_Fieldn_Pnd_Ws_Int = pnd_Ws_Fieldn__R_Field_13.newFieldInGroup("pnd_Ws_Fieldn_Pnd_Ws_Int", "#WS-INT", FieldType.NUMERIC, 9);
        pnd_Ws_Fieldn_Pnd_Ws_Rem = pnd_Ws_Fieldn__R_Field_13.newFieldInGroup("pnd_Ws_Fieldn_Pnd_Ws_Rem", "#WS-REM", FieldType.STRING, 2);
        pnd_Ws_Masked_Amt = localVariables.newFieldInRecord("pnd_Ws_Masked_Amt", "#WS-MASKED-AMT", FieldType.STRING, 13);
        pnd_Ws_Column_Offset = localVariables.newFieldInRecord("pnd_Ws_Column_Offset", "#WS-COLUMN-OFFSET", FieldType.STRING, 14);

        pnd_Ws_Column_Offset__R_Field_14 = localVariables.newGroupInRecord("pnd_Ws_Column_Offset__R_Field_14", "REDEFINE", pnd_Ws_Column_Offset);
        pnd_Ws_Column_Offset__Filler1 = pnd_Ws_Column_Offset__R_Field_14.newFieldInGroup("pnd_Ws_Column_Offset__Filler1", "_FILLER1", FieldType.STRING, 
            1);
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data = pnd_Ws_Column_Offset__R_Field_14.newFieldInGroup("pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data", 
            "#WS-COLUMN-OFFSET-LFT-1-DATA", FieldType.STRING, 13);

        pnd_Ws_Column_Offset__R_Field_15 = localVariables.newGroupInRecord("pnd_Ws_Column_Offset__R_Field_15", "REDEFINE", pnd_Ws_Column_Offset);
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data = pnd_Ws_Column_Offset__R_Field_15.newFieldInGroup("pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data", 
            "#WS-COLUMN-OFFSET-RGT-1-DATA", FieldType.STRING, 13);
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1 = pnd_Ws_Column_Offset__R_Field_15.newFieldInGroup("pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1", 
            "#WS-COLUMN-OFFSET-RGT-POS1", FieldType.STRING, 1);
        pnd_Ws_Letter = localVariables.newFieldInRecord("pnd_Ws_Letter", "#WS-LETTER", FieldType.STRING, 1);
        pnd_Ws_Legend = localVariables.newFieldInRecord("pnd_Ws_Legend", "#WS-LEGEND", FieldType.STRING, 4);
        pnd_Ws_Literal = localVariables.newFieldInRecord("pnd_Ws_Literal", "#WS-LITERAL", FieldType.STRING, 12);
        pnd_Ws_Printed = localVariables.newFieldInRecord("pnd_Ws_Printed", "#WS-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Dpi_Dci_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Dpi_Dci_Hdr_Written", "#WS-DPI-DCI-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Deduct_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Deduct_Hdr_Written", "#WS-DEDUCT-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Dpi_Hdr_Written = localVariables.newFieldInRecord("pnd_Ws_Dpi_Hdr_Written", "#WS-DPI-HDR-WRITTEN", FieldType.STRING, 1);
        pnd_Ws_Printed_Cref_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Cref_Heading", "#WS-PRINTED-CREF-HEADING", FieldType.STRING, 1);
        pnd_Ws_Printed_Tiaa_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Tiaa_Heading", "#WS-PRINTED-TIAA-HEADING", FieldType.STRING, 1);
        pnd_Ws_Printed_Spia_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Spia_Heading", "#WS-PRINTED-SPIA-HEADING", FieldType.STRING, 1);
        pnd_Ws_Printed_Select_Heading = localVariables.newFieldInRecord("pnd_Ws_Printed_Select_Heading", "#WS-PRINTED-SELECT-HEADING", FieldType.STRING, 
            1);
        pnd_Ws_Tot_Payment_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Payment_Amt", "#WS-TOT-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Dpi_Plus_Dci_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Dpi_Plus_Dci_Amt", "#WS-TOT-DPI-PLUS-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Tot_Ded_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Ded_Amt", "#WS-TOT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Tot_Check_Amt = localVariables.newFieldInRecord("pnd_Ws_Tot_Check_Amt", "#WS-TOT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_End_Accum = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_End_Accum", "#WS-GRAND-TOT-END-ACCUM", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Grand_Tot_Payment = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Payment", "#WS-GRAND-TOT-PAYMENT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Dpi_Plus_Dci = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Dpi_Plus_Dci", "#WS-GRAND-TOT-DPI-PLUS-DCI", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Grand_Tot_Ded = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Ded", "#WS-GRAND-TOT-DED", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Check = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Check", "#WS-GRAND-TOT-CHECK", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Grand_Tot_Dpi = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Dpi", "#WS-GRAND-TOT-DPI", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Leon_Rememb_Setl_Dt = localVariables.newFieldInRecord("pnd_Ws_Leon_Rememb_Setl_Dt", "#WS-LEON-REMEMB-SETL-DT", FieldType.PACKED_DECIMAL, 
            6);
        pnd_First_Time_Sw = localVariables.newFieldInRecord("pnd_First_Time_Sw", "#FIRST-TIME-SW", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_First_Time.setInitialValue("Y");
        pnd_J.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_Highest_Index_Of_Deduction.setInitialValue(13);
        pnd_Ws_Printed.setInitialValue("N");
        pnd_Ws_Dpi_Dci_Hdr_Written.setInitialValue("N");
        pnd_Ws_Deduct_Hdr_Written.setInitialValue("N");
        pnd_Ws_Dpi_Hdr_Written.setInitialValue("N");
        pnd_Ws_Printed_Cref_Heading.setInitialValue("N");
        pnd_Ws_Printed_Tiaa_Heading.setInitialValue("N");
        pnd_Ws_Printed_Spia_Heading.setInitialValue("N");
        pnd_Ws_Printed_Select_Heading.setInitialValue("N");
        pnd_Ws_Tot_Payment_Amt.setInitialValue(0);
        pnd_Ws_Tot_Dpi_Plus_Dci_Amt.setInitialValue(0);
        pnd_Ws_Tot_Ded_Amt.setInitialValue(0);
        pnd_Ws_Tot_Check_Amt.setInitialValue(0);
        pnd_Ws_Grand_Tot_End_Accum.setInitialValue(0);
        pnd_Ws_Grand_Tot_Payment.setInitialValue(0);
        pnd_Ws_Grand_Tot_Dpi_Plus_Dci.setInitialValue(0);
        pnd_Ws_Grand_Tot_Ded.setInitialValue(0);
        pnd_Ws_Grand_Tot_Check.setInitialValue(0);
        pnd_Ws_Grand_Tot_Dpi.setInitialValue(0);
        pnd_Ws_Leon_Rememb_Setl_Dt.setInitialValue(0);
        pnd_First_Time_Sw.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn372() throws Exception
    {
        super("Fcpn372");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        //*  ----------------------------------------------------------------------
        //*  INSPECT THE DATA TO BE PRINTED AND DETERMINE THE HORIZONTAL
        //*  FORMAT OF THE PRINT. I.E., HOW MANY COLUMNS ARE THERE GOING TO
        //*  BE ON THE PAGE.
        //*  ----------------------------------------------------------------------
        //*  ...................... SETUP MAX NUMBER OF COLUMNS
        //*  PERIODIC PAYMNETS PROTOTYPE #4
        //*   LUMP SUM ... PROTOTYPE#4
        //*  MATURITY, PROTOTYPE #4
        //*  SURVIVOR BENEFITS, PROTOTYPE #4
        //*   TPA ... PROTOTYPE#9
        //*   MDO,  ... PROTOTYPE#8
        //*   RECURRING MDO, ... PROTOTYPE#6
        //*   LUMP SUM ... PROTOTYPE#6
        short decideConditionsMet398 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE 'PP'
        if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("PP"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(7);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 7
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("L"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(7);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 7
        }                                                                                                                                                                 //Natural: VALUE '10'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("10"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(7);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 7
        }                                                                                                                                                                 //Natural: VALUE '50'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("50"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(7);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 7
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("20"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: VALUE '30'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("30"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: VALUE '31'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("31"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: VALUE '40'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("40"))))
        {
            decideConditionsMet398++;
            pnd_Nbr_Of_Columns.setValue(5);                                                                                                                               //Natural: ASSIGN #NBR-OF-COLUMNS := 5
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E02 - Unexpected CNTRCT-TYPE-CDE","=",pnd_Ws_Header_Record_Cntrct_Type_Cde);                                       //Natural: WRITE *PROGRAM 'E02 - Unexpected CNTRCT-TYPE-CDE' '=' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  .................... ADJUST NUMBER OF COLUMNS BASED ON DATA
        //*  ......... INTEREST IS THE SUM OF DPI AND DCI
        //*  LS
        if (condition(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero()) || pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(1,                //Natural: IF INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0 OR INV-ACCT-DCI-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0
            ":",pnd_Ws_Cntr_Inv_Acct).greater(getZero())))
        {
            pnd_Ws_Dpi_Dci_Hdr_Written.setValue("Y");                                                                                                                     //Natural: ASSIGN #WS-DPI-DCI-HDR-WRITTEN := 'Y'
            //*  COMPRESS OUT THIS COULUMN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Nbr_Of_Columns.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #NBR-OF-COLUMNS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero()) || pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero())  //Natural: IF INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0 OR INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0 OR INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) > 0 OR PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) > 0
            || pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero()) || pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(1,":",
            pnd_Ws_Cntr_Inv_Acct,"*").greater(getZero())))
        {
            pnd_Ws_Deduct_Hdr_Written.setValue("Y");                                                                                                                      //Natural: ASSIGN #WS-DEDUCT-HDR-WRITTEN := 'Y'
            //*  COMPRESS OUT THIS COULUMN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Nbr_Of_Columns.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #NBR-OF-COLUMNS
        }                                                                                                                                                                 //Natural: END-IF
        //*   DETERMINE THE "left margin" OF THE "1st column" AS THE COLUMN
        //*   LEFT TO THE 1ST ONE TO BE PRINTED. SO THAT IF THE "1st column"
        //*   TO BE PRINTED IS 1 SET THE MARGIN TO 0 IF THE "1st column"
        //*   IS 3 SET THE MARGIN TO 2, ETC.
        //*  1
        //*  THIS SHOULD NEVER HAPPEN
        short decideConditionsMet443 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NBR-OF-COLUMNS;//Natural: VALUE 7, 6
        if (condition((pnd_Nbr_Of_Columns.equals(7) || pnd_Nbr_Of_Columns.equals(6))))
        {
            decideConditionsMet443++;
            pnd_1st_F.setValue(0);                                                                                                                                        //Natural: ASSIGN #1ST-F := 0
        }                                                                                                                                                                 //Natural: VALUE 5, 4
        else if (condition((pnd_Nbr_Of_Columns.equals(5) || pnd_Nbr_Of_Columns.equals(4))))
        {
            decideConditionsMet443++;
            pnd_1st_F.setValue(0);                                                                                                                                        //Natural: ASSIGN #1ST-F := 0
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E03 - Unexpected","=",pnd_Nbr_Of_Columns);                                                                         //Natural: WRITE *PROGRAM 'E03 - Unexpected' '=' #NBR-OF-COLUMNS
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  .. THE FIRST "Group of Lines" ON THE PAGE DOES NOT NEED A SPACE LINE
        //*  .. TO SEPARATE IT FROM THE PREVIOUS "Group of Lines".
        pnd_Top_Of_Page.setValue(true);                                                                                                                                   //Natural: ASSIGN #TOP-OF-PAGE := TRUE
        //*  .......... PROCESS ONLY "Groups of Lines" FOR CURRENT PAGE
        FOR01:                                                                                                                                                            //Natural: FOR #J 1 #WS-CNTR-INV-ACCT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_J.nadd(1))
        {
            if (condition(pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).notEquals(pnd_Ws_Side_Printed)))                                                                      //Natural: IF #WS-SIDE ( #J ) NE #WS-SIDE-PRINTED
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ENTRY
                sub_Process_Entry();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Top_Of_Page.setValue(false);                                                                                                                          //Natural: ASSIGN #TOP-OF-PAGE := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  .... CHECK THE IVC SITUATION:
        //*       1. ON PAGE 1 FOR FOOTNOTE PURPOSE
        //*       2. ON LAST PAGE FOR "footnoting" THE GRAND TOTAL
        if (condition(pnd_Ws_Side_Printed.equals(pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex) || pnd_Ws_Side_Printed.equals("1")))                               //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX OR #WS-SIDE-PRINTED = '1'
        {
                                                                                                                                                                          //Natural: PERFORM TEST-FOR-GOOD-IVC
            sub_Test_For_Good_Ivc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... IT NOT LAST PAGE OF DOCUMENT THEN: "Next Page(s)"
        if (condition(pnd_Ws_Side_Printed.less(pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex)))                                                                    //Natural: IF #WS-SIDE-PRINTED LT #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Rec_1.setValue("06  This statement is continued on the next page");                                                                                    //Natural: ASSIGN #WS-REC-1 := '06  This statement is continued on the next page'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  ON LAST PAGE SHOW THE GRAND TOTALS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DO-GRAND-TOTALS
            sub_Do_Grand_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... ON PAGE 1 PUT "footnote" MESSAGES (IF THERE ARE ANY)
        if (condition(pnd_Ws_Side_Printed.equals("1")))                                                                                                                   //Natural: IF #WS-SIDE-PRINTED = '1'
        {
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                                                                                                                                                                          //Natural: PERFORM SHOW-IVC-FOOTNOTE
                sub_Show_Ivc_Footnote();
                if (condition(Global.isEscape())) {return;}
                //*  EMPTY LINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue("17");                                                                                                                              //Natural: ASSIGN #WS-REC-1 := '17'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *************************
        //*   S U B R O U T I N E S *
        //* *************************
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DO-GRAND-TOTALS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ENTRY
        //*  ...... OBTAIN INVESTMENT ACCOUNT DESCRIPTION FROM TABLE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG-PT4
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-A-LINE-PT4
        //*  ..... SETUP START COLUMN INDEX (#F)
        //*  ......... COMPUTE INTEREST AS THE SUM OF DPI AND DCI
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DB-LINE-PT4
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //* * * *
        //* **��� HERE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DC-LINE-PT4
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DX-LINE-PT4
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-E-LINE-PT4
        //* ******
        //*  .... NOW MOVE THE TOTAL TO THE PRINT LINE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-AND-EDIT-NEXT-DEDUCTION
        //* *------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-THE-DEDUCTION
        //* ***--------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-FEDERAL-LEGEND
        //* ***--------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEGEND
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG
        //* *WHEN #PDA-INV-ACCT-COMPANY   =  'T'
        //* *WHEN #PDA-INV-ACCT-COMPANY = 'C'
        //* ****ORIG  #WS-REC-1   := '14     Interest'
        //* *****ORIG  #WS-REC-1   := '14    Deductions'
        //* *****ORIG  #WS-REC-1     := '14       Net'
        //* *****ORIG  #WS-REC-1     := ' 4       Payment'
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-A-LINE-TOTALS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-FOR-GOOD-IVC
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SHOW-IVC-FOOTNOTE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG-PT8
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-A-LINE-PT8
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DB-LINE-PT8
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //* *#WS-COLUMN (#F) := #PDA-INV-ACCT-FUND-DESC1
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DC-LINE-PT8
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //*  ...... FUND DESC LINE 2 LIKE "market" IN "money market"
        //* *#WS-COLUMN (#F) := #PDA-INV-ACCT-FUND-DESC2
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DX-LINE-PT8
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-E-LINE-PT8
        //* ******
        //*  .... NOW MOVE THE TOTAL TO THE PRINT LINE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-COL-HDNG-PT9
        //* *  WHEN #PDA-INV-ACCT-COMPANY   =  'T'
        //* *  WHEN #PDA-INV-ACCT-COMPANY = 'C'
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-A-LINE-PT9
        //* **END-IF
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DB-LINE-PT9
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //* *#WS-COLUMN (#F) := #PDA-INV-ACCT-FUND-DESC1
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DC-LINE-PT9
        //*  ....... POSITION TO NEW LINE
        //*  ....... SET FONTS TO NON-BOLD
        //*  ....... SET LEFT MARGIN
        //*  ...... FUND DESC LINE 2 LIKE "market" IN "money market"
        //* *#WS-COLUMN (#F) := #PDA-INV-ACCT-FUND-DESC2
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-DX-LINE-PT9
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-E-LINE-PT9
        //* ******
        //*  .... NOW MOVE THE TOTAL TO THE PRINT LINE
    }
    private void sub_Do_Grand_Totals() throws Exception                                                                                                                   //Natural: DO-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  PERIODIC PAYMENTS, PROTOTYPE #4
        short decideConditionsMet638 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE 'PP', 'L'
        if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("PP") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("L"))))
        {
            decideConditionsMet638++;
            //*  MATURITY,          PROTOTYPE #4
                                                                                                                                                                          //Natural: PERFORM BLD-E-LINE-PT4
            sub_Bld_E_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '10','50'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("10") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("50"))))
        {
            decideConditionsMet638++;
            //*   TPA                ... PROTOTYPE#9
                                                                                                                                                                          //Natural: PERFORM BLD-E-LINE-PT4
            sub_Bld_E_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("20"))))
        {
            decideConditionsMet638++;
            //*   MDO,               ... PROTOTYPE#8
                                                                                                                                                                          //Natural: PERFORM BLD-E-LINE-PT9
            sub_Bld_E_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '30','31','40'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("30") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("31") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("40"))))
        {
            decideConditionsMet638++;
                                                                                                                                                                          //Natural: PERFORM BLD-E-LINE-PT8
            sub_Bld_E_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E02 - Unexpected CNTRCT-TYPE-CDE","=",pnd_Ws_Header_Record_Cntrct_Type_Cde);                                       //Natural: WRITE *PROGRAM 'E02 - Unexpected CNTRCT-TYPE-CDE' '=' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DO-GRAND-TOTALS
    }
    private void sub_Process_Entry() throws Exception                                                                                                                     //Natural: PROCESS-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  PROCESS AN ENTRY FROM THE TABLE
        //*  #J IS USED AS THE INDEX TO THE TABLE ENTRY
        //*  ----------------------------------------------------------------------
        //*  ........ FIRST LINE ON "Next Page"
        if (condition(((((((pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("2") || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("3")) || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("4"))  //Natural: IF #WS-SIDE ( #J ) = '2' OR = '3' OR = '4' OR = '5' OR = '6' OR = '7' AND #WS-PRINTED = 'N'
            || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("5")) || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("6")) || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("7")) 
            && pnd_Ws_Printed.equals("N"))))
        {
            pnd_Ws_Printed.setValue("Y");                                                                                                                                 //Natural: ASSIGN #WS-PRINTED := 'Y'
            pnd_Ws_Rec_1.setValue("15                            ");                                                                                                      //Natural: ASSIGN #WS-REC-1 := '15                            '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_J));                                                            //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := INV-ACCT-CDE ( #J )
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pnd_Ws_Occurs_Inv_Acct_Valuat_Period.getValue(pnd_J));                                          //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := INV-ACCT-VALUAT-PERIOD ( #J )
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Ins_Type().setValue(pnd_Ws_Cntrct_Annty_Ins_Type);                                                                   //Natural: ASSIGN #FUND-PDA.#CNTRCT-ANNTY-INS-TYPE := #WS-CNTRCT-ANNTY-INS-TYPE
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Type_Cde().setValue(pnd_Ws_Cntrct_Annty_Type_Cde);                                                                   //Natural: ASSIGN #FUND-PDA.#CNTRCT-ANNTY-TYPE-CDE := #WS-CNTRCT-ANNTY-TYPE-CDE
        DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                        //Natural: CALLNAT 'FCPN199A' #FUND-PDA
        if (condition(Global.isEscape())) return;
        //*  ...... RESET POINTER OF LAST USED DEDUCTION
        pnd_Index_Of_Last_Used_Deduction.reset();                                                                                                                         //Natural: RESET #INDEX-OF-LAST-USED-DEDUCTION
        //*  ...... CALC THE NUMBER OF DEDUCTIONS
        pnd_Nbr_Of_Remaining_Deductions.reset();                                                                                                                          //Natural: RESET #NBR-OF-REMAINING-DEDUCTIONS
        short decideConditionsMet677 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN INV-ACCT-FDRL-TAX-AMT ( #J ) > 0
        if (condition(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet677++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN INV-ACCT-STATE-TAX-AMT ( #J ) > 0
        if (condition(pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet677++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
        if (condition(pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet677++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(1);                                                                                                                      //Natural: ADD 1 TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN #WS-OCCURS.#CNTR-DEDUCTIONS ( #J ) GT 0
        if (condition(pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_J).greater(getZero())))
        {
            decideConditionsMet677++;
            pnd_Nbr_Of_Remaining_Deductions.nadd(pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_J));                                                                      //Natural: ADD #WS-OCCURS.#CNTR-DEDUCTIONS ( #J ) TO #NBR-OF-REMAINING-DEDUCTIONS
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet677 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PERIODIC PAYMENTS, PROTOTYPE #4
        short decideConditionsMet691 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE 'PP','L'
        if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("PP") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("L"))))
        {
            decideConditionsMet691++;
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG-PT4
            sub_Bld_Col_Hdng_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-A-LINE-PT4
            sub_Bld_A_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DB-LINE-PT4
            sub_Bld_Db_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DC-LINE-PT4
            sub_Bld_Dc_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
            //*  MATURITY,          PROTOTYPE #4
                                                                                                                                                                          //Natural: PERFORM BLD-DX-LINE-PT4
            sub_Bld_Dx_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '10','50'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("10") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("50"))))
        {
            decideConditionsMet691++;
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG-PT4
            sub_Bld_Col_Hdng_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-A-LINE-PT4
            sub_Bld_A_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DB-LINE-PT4
            sub_Bld_Db_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DC-LINE-PT4
            sub_Bld_Dc_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
            //*   TPA                ... PROTOTYPE#9
                                                                                                                                                                          //Natural: PERFORM BLD-DX-LINE-PT4
            sub_Bld_Dx_Line_Pt4();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("20"))))
        {
            decideConditionsMet691++;
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG-PT9
            sub_Bld_Col_Hdng_Pt9();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-A-LINE-PT9
            sub_Bld_A_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DB-LINE-PT9
            sub_Bld_Db_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DC-LINE-PT9
            sub_Bld_Dc_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
            //*   MDO,               ... PROTOTYPE#8
                                                                                                                                                                          //Natural: PERFORM BLD-DX-LINE-PT9
            sub_Bld_Dx_Line_Pt9();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '30','31','40'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("30") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("31") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("40"))))
        {
            decideConditionsMet691++;
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG-PT8
            sub_Bld_Col_Hdng_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-A-LINE-PT8
            sub_Bld_A_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DB-LINE-PT8
            sub_Bld_Db_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DC-LINE-PT8
            sub_Bld_Dc_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BLD-DX-LINE-PT8
            sub_Bld_Dx_Line_Pt8();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E01 - Unexpected CNTRCT-TYPE-CDE","=",pnd_Ws_Header_Record_Cntrct_Type_Cde);                                       //Natural: WRITE *PROGRAM 'E01 - Unexpected CNTRCT-TYPE-CDE' '=' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PROCESS-ENTRY
    }
    private void sub_Bld_Col_Hdng_Pt4() throws Exception                                                                                                                  //Natural: BLD-COL-HDNG-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*   BUILD COLUMN HEADING LINES AS PER THE DATA TO BE PRINTED.
        //*   THERE ARE SOME COLUMNS THAT ARE ONLY CONDITIONALLY PRINTED. IN CASE
        //*   THAT A CERTAIN DATA, FOR EXAMPLE DELAYED PAYMENT INTEREST, IS NOT
        //*   PRESENT FOR THIS PRINTOUT (ON EIHTER OF ITS PAGES!), THEN IT IS NOT
        //*   PRINTED AND THE NEXT PRINTED COLUMN IS SHIFTED LEFT, SO THAT THERE
        //*   ARE NO BLANK COLUMNS.
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                                     //Natural: IF #WS-FIRST-TIME = 'Y'
        {
            pnd_Ws_First_Time.setValue("N");                                                                                                                              //Natural: ASSIGN #WS-FIRST-TIME := 'N'
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG
            sub_Bld_Col_Hdng();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("15");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG-PT4
    }
    private void sub_Bld_A_Line_Pt4() throws Exception                                                                                                                    //Natural: BLD-A-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*    "A Line": TOTALS FOR THE "group of Lines"
        //*  ----------------------------------------------------------------------
        //*   ...... ACCUM THE TOTALS FOR THE "group of Lines"
                                                                                                                                                                          //Natural: PERFORM SETUP-A-LINE-TOTALS
        sub_Setup_A_Line_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '05'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  **
        //*  ............ COLUMN 1
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //*  ... EMPTY FOR FUTURE PERIODIC PAYMENTS
        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.equals("F")))                                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
            //*  ... START-DATE FOR PAST PERIODIC PAYMENTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *** LEON *
            if (condition(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6.getValue(pnd_J).equals(pnd_Ws_Leon_Rememb_Setl_Dt)))                                              //Natural: IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 ( #J ) EQ #WS-LEON-REMEMB-SETL-DT
            {
                pnd_First_Time_Sw.setValue(" ");                                                                                                                          //Natural: MOVE ' ' TO #FIRST-TIME-SW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* * * *
                if (condition(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6.getValue(pnd_J).greater(getZero())))                                                          //Natural: IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 ( #J ) GT 0
                {
                    pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start.getValue(pnd_J),new ReportEditMask("MM/DD/YY"));                      //Natural: MOVE EDITED #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-START ( #J ) ( EM = MM/DD/YY ) TO #WS-TEMP-COL
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                 //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
                    //*  LEON
                    //*  LEON
                    pnd_Ws_Leon_Rememb_Setl_Dt.setValue(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6.getValue(pnd_J));                                                   //Natural: MOVE #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 ( #J ) TO #WS-LEON-REMEMB-SETL-DT
                    //*  LEON
                    pnd_First_Time_Sw.setValue("Y");                                                                                                                      //Natural: MOVE 'Y' TO #FIRST-TIME-SW
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "field should have a valid date:","=",pnd_J,"=",pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4.getValue(pnd_J),                  //Natural: WRITE 'field should have a valid date:' '='#J '=' #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-A4 ( #J ) ( EM = H ( 8 ) )
                        new ReportEditMask ("HHHHHHHH"));
                    if (Global.isEscape()) return;
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                   //Natural: RESET #WS-COLUMN ( #F )
                }                                                                                                                                                         //Natural: END-IF
                //*  L
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ****************************************************************
        //* *----------------------------------------------------------------
        //*  ............ COLUMN 2 - ENTER CNTRCT-PPCN-NBR
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Cntrct_Ppcn_Nbr.getValue(pnd_J),new ReportEditMask("XXXXXXX-X"));                                                    //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(pnd_Ws_Temp_Col));                                                                           //Natural: COMPRESS #WS-TEMP-COL INTO #WS-COLUMN ( #F )
        //* *-------------------------------------------------------------
        //*  ............ COLUMN 3
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                    //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                                       //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                        //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
        //*  ............ COLUMN 4 - EMPTY
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean())))                                                                                      //Natural: IF NOT #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_9());                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#VALUAT-DESC-9
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 5 - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF THERE IS DATA
        if (condition(pnd_Ws_Dpi_Dci_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-DCI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Dpi_Plus_Dci_Amt.compute(new ComputeParameters(false, pnd_Ws_Dpi_Plus_Dci_Amt), pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(pnd_J).add(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_J))); //Natural: ASSIGN #WS-DPI-PLUS-DCI-AMT := INV-ACCT-DCI-AMT ( #J ) + INV-ACCT-DPI-AMT ( #J )
            //*   MOVE EDITED INV-ACCT-DPI-AMT (#J) (EM=X'$'Z,ZZZ,ZZ9.99)
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Dpi_Plus_Dci_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                               //Natural: MOVE EDITED #WS-DPI-PLUS-DCI-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 6 - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR ANY OF THE
        //*  INSTALLMENTS FOR THIS SETTLMENT, THEN THE COLUMN AND IT's header
        //*  WILL NOT BE PRINTED AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Deduction_Column_Nbr.setValue(pnd_F);                                                                                                                     //Natural: ASSIGN #DEDUCTION-COLUMN-NBR := #F
            if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                         //Natural: IF #WS-TOT-DED-AMT > 0
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZ,ZZ9.99"));                                                                   //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                                  //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-A-LINE-PT4
    }
    private void sub_Bld_Db_Line_Pt4() throws Exception                                                                                                                   //Natural: BLD-DB-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*   BUILD THE  "Db Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //* *----------------------------------------------------------
        //*  ........ 1ST COLUMN
        //*  ... 'Due' FOR FUTURE PAYMENTS, 'THRU' FOR PAST PAYMENTS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.equals("F")))                                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Due");                                                                                                   //Natural: ASSIGN #WS-COLUMN ( #F ) := 'Due'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *** LEON *
            if (condition(((pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6.getValue(pnd_J).equals(pnd_Ws_Leon_Rememb_Setl_Dt)) && (pnd_First_Time_Sw.equals(" ")))))       //Natural: IF ( ( #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 ( #J ) EQ #WS-LEON-REMEMB-SETL-DT ) AND ( #FIRST-TIME-SW EQ ' ' ) )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("thru");                                                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := 'thru'
                //*  LEON
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------
        //*  ......... 2ND COLUMN
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-1
        //*  ............3RD COLUMN
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //*   SHOW FOR FUTURE PAYMENTS; EMPTY FOR PAST PAYMENTS
        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.equals("F")))                                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Unit_Qty.getValue(pnd_J),new ReportEditMask("ZZZ,ZZ9.999"));                                        //Natural: MOVE EDITED INV-ACCT-UNIT-QTY ( #J ) ( EM = ZZZ,ZZ9.999 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 4
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //*   LEAVE SPACE FOR PAST PAYMENTS
        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.equals("F")))                                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Contractual");                                                                             //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Contractual'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Units");                                                                                   //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Units'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Contractual");                                                                             //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Contractual'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 5 - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF THERE IS AN
        //*  AMOUNT IN INV-ACCT-DPI-AMT(*)ORDCI(*)FOR ANY OF THE INSTALLMENTS FOR
        //*  THE SETTLMENT. OTHERWISE, THE COLUMN AND IT's header will not be
        //*  PRINTED AND THE NEXT COLUMN IS SHIFTED OVER. EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Dci_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-DCI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 6 - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR ANY OF THE INSTALLMENTS
        //*  FOR THIS SETTLMENT, THEN THE COLUMN AND IT's header will not be
        //*  PRINTED AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DB-LINE-PT4
    }
    private void sub_Bld_Dc_Line_Pt4() throws Exception                                                                                                                   //Natural: BLD-DC-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dc Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ........ 1ST COLUMN - "Due" DATE
        //*  FOR FUTURE PAYMENTS: THE SETTLEMENT DATE FOR THIS SPECIFIC PAYMENT
        //*  FOR PAST PAYMENTS - DATE OF LAST PERIODIC PAYMENT
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.equals("F")))                                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        {
            //*  FUTUTE PAYMENT
            if (condition(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6.getValue(pnd_J).greater(getZero())))                                                              //Natural: IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 ( #J ) GT 0
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte.getValue(pnd_J),new ReportEditMask("MM/DD/YY"));                                //Natural: MOVE EDITED #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE ( #J ) ( EM = MM/DD/YY ) TO #WS-TEMP-COL
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                     //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "field should have a valid date:","=",pnd_J,"=",pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4.getValue(pnd_J), new                  //Natural: WRITE 'field should have a valid date:' '='#J '=' #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-A4 ( #J ) ( EM = H ( 8 ) )
                    ReportEditMask ("HHHHHHHH"));
                if (Global.isEscape()) return;
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //*  PAST PAYMENTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* ***   NEW LEON * *
            if (condition(((pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6.getValue(pnd_J).equals(pnd_Ws_Leon_Rememb_Setl_Dt)) && (pnd_First_Time_Sw.equals(" ")))))       //Natural: IF ( ( #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 ( #J ) EQ #WS-LEON-REMEMB-SETL-DT ) AND ( #FIRST-TIME-SW EQ ' ' ) )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *** * * *
                if (condition(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6.getValue(pnd_J).greater(getZero())))                                                      //Natural: IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END-P6 ( #J ) GT 0
                {
                    pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End.getValue(pnd_J),new ReportEditMask("MM/DD/YY"));                        //Natural: MOVE EDITED #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END ( #J ) ( EM = MM/DD/YY ) TO #WS-TEMP-COL
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                 //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "end period date should have a valid date:","=",pnd_J,"=",pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4.getValue(pnd_J),    //Natural: WRITE 'end period date should have a valid date:' '='#J '=' #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END-A4 ( #J ) ( EM = H ( 8 ) )
                        new ReportEditMask ("HHHHHHHH"));
                    if (Global.isEscape()) return;
                    pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                   //Natural: RESET #WS-COLUMN ( #F )
                }                                                                                                                                                         //Natural: END-IF
                //*   LEON
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *------------------------------------------------------------------
        //*  ............ 2ND COLUMN
        //*  FOR TIAA LEAVE SPACE, FOR CREF
        //*  ENTER THE INVESTMENT ACCOUNT's description (use FCPN199A'S DATA)
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            //* *IF #WS-OCCURS.CNTRCT-PPCN-NBR(#J) = MASK('GA') OR = MASK('GW')
            //*   LEON
            if (condition(pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_J).equals("TG")))                                                                                       //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( #J ) = 'TG'
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Graded");                                                                                            //Natural: ASSIGN #WS-COLUMN ( #F ) := 'Graded'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Standard");                                                                                          //Natural: ASSIGN #WS-COLUMN ( #F ) := 'Standard'
            }                                                                                                                                                             //Natural: END-IF
            //*  LZ
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                                          //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-2
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ 3RD COLUMN
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.equals("F")))                                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        {
            //*   FUTURE PAYMENT
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED INV-ACCT-DVDND-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Unit_Value.getValue(pnd_J),new ReportEditMask("ZZZ,ZZ9.9999"));                                     //Natural: MOVE EDITED INV-ACCT-UNIT-VALUE ( #J ) ( EM = ZZZ,ZZ9.9999 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
            //*   LEAVE SPACE FOR PAST PAYMENTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(pnd_J),new ReportEditMask("Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED INV-ACCT-DVDND-AMT ( #J ) ( EM = Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ 4TH COLUMN
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //* ****
        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.equals("F")))                                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        {
            //*   FUTURE PAYMENTS
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Dividend");                                                                                //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Dividend'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Unit Value");                                                                              //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Unit Value'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: END-IF
            //*   LEAVE SPACE FOR PAST PAYMENTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Lft_1_Data.setValue("Dividend");                                                                                //Natural: ASSIGN #WS-COLUMN-OFFSET-LFT-1-DATA := 'Dividend'
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ 5TH COLUMN - INTEREST  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Dci_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-DCI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 6 - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DC-LINE-PT4
    }
    private void sub_Bld_Dx_Line_Pt4() throws Exception                                                                                                                   //Natural: BLD-DX-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dx Line": ADDITIONAL DEDUCTIONS, AS NEEDED
        //*  ----------------------------------------------------------------------
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(!(pnd_Nbr_Of_Remaining_Deductions.greater(getZero())))) {break;}                                                                                //Natural: WHILE #NBR-OF-REMAINING-DEDUCTIONS GT 0
            pnd_Ws_Rec_1.reset();                                                                                                                                         //Natural: RESET #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 5");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC := '+3'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_Deduction_Column_Nbr).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #DEDUCTION-COLUMN-NBR )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  BLD-DX-LINE-PT4
    }
    private void sub_Bld_E_Line_Pt4() throws Exception                                                                                                                    //Natural: BLD-E-LINE-PT4
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD "Grand Totals" LINE
        //*  ----------------------------------------------------------------------
        //*  ............ACCUM "Grand Totals"
        if (condition(pnd_Ws_Side_Printed.equals(pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex)))                                                                  //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_Payment.reset();                                                                                                                             //Natural: RESET #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI-PLUS-DCI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Dpi_Plus_Dci.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            //* ******
            pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-SETTL-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-DVDND-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Dpi_Plus_Dci.nadd(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                      //Natural: ADD INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DPI-PLUS-DCI
            pnd_Ws_Grand_Tot_Dpi_Plus_Dci.nadd(pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                      //Natural: ADD INV-ACCT-DCI-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DPI-PLUS-DCI
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                          //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct,"*"));                                                              //Natural: ADD PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Tot_Check_Amt.nadd(pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-TOT-CHECK-AMT
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC := '06'
            pnd_F.setValue(pnd_1st_F);                                                                                                                                    //Natural: ASSIGN #F := #1ST-F
            //*  ...... 1ST COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
            //*  ..... 2ND COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) := 'Grand Total'
            //*  ......... EDITE / BUILD THE GRAND TOTAL LINE
            //*  ..... 3RD COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
            //*  .... IF THERE IS IVC MONEY "footnote" THE GRAND TOTAL
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.setValue("#");                                                                                         //Natural: ASSIGN #WS-COLUMN-OFFSET-RGT-POS1 := '#'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                    //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            //*  ..... 4TH COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
            //*  ..... 5TH COLUMN
            if (condition(pnd_Ws_Dpi_Dci_Hdr_Written.equals("Y")))                                                                                                        //Natural: IF #WS-DPI-DCI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Dpi_Plus_Dci,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                     //Natural: MOVE EDITED #WS-GRAND-TOT-DPI-PLUS-DCI ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... 6TH COLUMN
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99"));                                                                //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                  //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-E-LINE-PT4
    }
    private void sub_Obtain_And_Edit_Next_Deduction() throws Exception                                                                                                    //Natural: OBTAIN-AND-EDIT-NEXT-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  THE DEDUCTION ARE PRINTED IN THE FOLLOWING ORDER:
        //*   1.    FEDERAL TAX - LEGEND "(F)"
        //*   2.    STATE   TAX - LEGEND "(S)"
        //*   3.    LOCAL   TAX - LEGEND "(L)"
        //*   4-13  ADDITIONAL DEDUCTION
        //*         IN THE SEQUENCE THAT THEY ARE LISTED IN THE DEDUCTION GROUP
        //*   NOTE: ONLY NON-ZERO DEDUCTIONS ARE PRINTED, SO USE THE FIRST
        //*         AVAILABLE NON-ZERO DEDUCTION
        //*    THE OCCURENCE OF THE LAST USED DEDUCTION IS KEPT
        //*    IN #INDEX-OF-LAST-USED-DEDUCTION
        //*    FIELD #NBR-OF-REMAINING-DEDUCTIONS CONTAINS THE NUMBER OF DEDUCTIONS
        //*    STILL REMAIN TO BE PRINTED.
        //*  ----------------------------------------------------------------------
        pnd_Ws_Temp_Col.reset();                                                                                                                                          //Natural: RESET #WS-TEMP-COL #WS-LEGEND
        pnd_Ws_Legend.reset();
        if (condition(pnd_Nbr_Of_Remaining_Deductions.lessOrEqual(getZero())))                                                                                            //Natural: IF #NBR-OF-REMAINING-DEDUCTIONS LE 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '='  #INDEX-OF-LAST-USED-DEDUCTION   /* TEMPORARY  ROXAN
        pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                         //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        //*  INSPECT ALSO FEDERAL TAX
        if (condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(1)))                                                                                                   //Natural: IF #INDEX-OF-LAST-USED-DEDUCTION LE 1
        {
            //*  USE FEDERAL TAX
            if (condition(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_J).greater(getZero())))                                                                        //Natural: IF INV-ACCT-FDRL-TAX-AMT ( #J ) > 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_J));                                                                      //Natural: ASSIGN #WS-TEMP-DEDUCTION := INV-ACCT-FDRL-TAX-AMT ( #J )
                                                                                                                                                                          //Natural: PERFORM FORMAT-FEDERAL-LEGEND
                sub_Format_Federal_Legend();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                     //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  INSPECT ALSO STATE TAX
        if (condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(2)))                                                                                                   //Natural: IF #INDEX-OF-LAST-USED-DEDUCTION LE 2
        {
            //*  USE STATE TAX
            if (condition(pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_J).greater(getZero())))                                                                       //Natural: IF INV-ACCT-STATE-TAX-AMT ( #J ) > 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_J));                                                                     //Natural: ASSIGN #WS-TEMP-DEDUCTION := INV-ACCT-STATE-TAX-AMT ( #J )
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEGEND
                sub_Format_State_Legend();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                     //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  INSPECT ALSO LOCAL TAX
        if (condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(3)))                                                                                                   //Natural: IF #INDEX-OF-LAST-USED-DEDUCTION LE 3
        {
            //*  USE LOACL TAX
            if (condition(pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_J).greater(getZero())))                                                                       //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( #J ) > 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_J));                                                                     //Natural: ASSIGN #WS-TEMP-DEDUCTION := INV-ACCT-LOCAL-TAX-AMT ( #J )
                pnd_Ws_Legend.setValue("(L)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND := '(L)'
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Index_Of_Last_Used_Deduction.nadd(1);                                                                                                                     //Natural: ADD 1 TO #INDEX-OF-LAST-USED-DEDUCTION
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #INDEX-OF-LAST-USED-DEDUCTION FROM #INDEX-OF-LAST-USED-DEDUCTION TO #HIGHEST-INDEX-OF-DEDUCTION
        for (pnd_Index_Of_Last_Used_Deduction.setValue(pnd_Index_Of_Last_Used_Deduction); condition(pnd_Index_Of_Last_Used_Deduction.lessOrEqual(pnd_Highest_Index_Of_Deduction)); 
            pnd_Index_Of_Last_Used_Deduction.nadd(1))
        {
            pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Index_Of_Last_Used_Deduction.subtract(3));                                                             //Natural: ASSIGN #I := #INDEX-OF-LAST-USED-DEDUCTION - 3
            if (condition(pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_J,pnd_I).notEquals(getZero())))                                                                        //Natural: IF #WS-OCCURS.PYMNT-DED-AMT ( #J,#I ) NE 0
            {
                pnd_Nbr_Of_Remaining_Deductions.nsubtract(1);                                                                                                             //Natural: SUBTRACT 1 FROM #NBR-OF-REMAINING-DEDUCTIONS
                pnd_Ws_Temp_Deduction.setValue(pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_J,pnd_I));                                                                        //Natural: ASSIGN #WS-TEMP-DEDUCTION := #WS-OCCURS.PYMNT-DED-AMT ( #J,#I )
                pnd_Ws_Legend.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_I, ")"));                                                                 //Natural: COMPRESS '(' #I ')' INTO #WS-LEGEND LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM EDIT-THE-DEDUCTION
                sub_Edit_The_Deduction();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* CTS
        if (condition(pnd_Index_Of_Last_Used_Deduction.greaterOrEqual(pnd_Highest_Index_Of_Deduction)))                                                                   //Natural: IF #INDEX-OF-LAST-USED-DEDUCTION GE #HIGHEST-INDEX-OF-DEDUCTION
        {
            //* CTS
            pnd_Index_Of_Last_Used_Deduction.reset();                                                                                                                     //Natural: RESET #INDEX-OF-LAST-USED-DEDUCTION #NBR-OF-REMAINING-DEDUCTIONS
            pnd_Nbr_Of_Remaining_Deductions.reset();
            //* CTS
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //* CTS
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... IF GOT HERE - THERE IS A PROGRAM BUG IN THE DEDUCTION LOGIC
        //*  OBTAIN-AND-EDIT-NEXT-DEDUCTION
    }
    private void sub_Edit_The_Deduction() throws Exception                                                                                                                //Natural: EDIT-THE-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Temp_Col2.setValueEdited(pnd_Ws_Temp_Deduction,new ReportEditMask("ZZZ,ZZ9.99"));                                                                          //Natural: MOVE EDITED #WS-TEMP-DEDUCTION ( EM = ZZZ,ZZ9.99 ) TO #WS-TEMP-COL2
        pnd_Ws_Temp_Col2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col2, pnd_Ws_Legend));                                                      //Natural: COMPRESS #WS-TEMP-COL2 #WS-LEGEND INTO #WS-TEMP-COL2 LEAVING NO SPACE
        //* ***ORIG ** MOVE RIGHT #WS-TEMP-COL2 TO #WS-TEMP-COL
        //* ************* LEON BARCODE CHANGES ******************************
        //* ***  BOTH AREAS IS (A14) ,SO MOVING FROM ONE TO OTHER WITH RIGHT STT
        //* ***  WILL MAKE RIGNT MOST ')' TO BE LAST CH IN (A14) B AREA
        //* *** SO FAR SO GOOD, BUT LETER THIS AREA WILL BE MOVED TO ANOTHER
        //* *** WICH IS USE TO BE ALSO (A14) BUT WITH BARCODE CHANGES WE MADE
        //* *** (A13). AND THAT ALPHA MOVE WILL TRANCATE LAST BYTE WICH CONTAIN
        //* ***  ')' SO HELP ME GOT.
        //* * * * * * ** * * * * * * * * * * * * ** * * * * * * * * * * * * *
        //*    SAVE ')'  LEON
        pnd_Ws_Temp_Col.setValue(pnd_Ws_Temp_Col2);                                                                                                                       //Natural: MOVE #WS-TEMP-COL2 TO #WS-TEMP-COL
        //*  EDIT-THE-DEDUCTION
    }
    private void sub_Format_Federal_Legend() throws Exception                                                                                                             //Natural: FORMAT-FEDERAL-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  DETERMINE THE LEGEND PRINTED NEXT TO THE FEDERAL TAX DEDUCTION
        //*  ----------------------------------------------------------------------
        //*  ROXAN 09/06/2001
        //*  RESTRUCTURE THESE CODES AND AT THE SAME TIME CORRECT THE NRA IND
        //*  BEGIN
        //*  IF CNTRCT-CRRNCY-CDE = '1'
        //*   IF (ANNT-CTZNSHP-CDE = 01 OR= 11)
        //* *    OR   /* COMM OUT 2 LINES TO FIX TAX CODES  G.P 4/1/98
        //* *  ( (ANNT-RSDNCY-CDE = '01' THRU '57') OR (ANNT-RSDNCY-CDE = '97') )
        //*     #WS-LEGEND := '(F)'
        //*   ELSE
        //*     #WS-LEGEND := '(N)'
        //*   END-IF
        //*  CHANGE NEXT LINES TO USE TX-ELCT-TRGGR - ROXAN 3/14/2002
        //*  START NEW CODE
        if (condition(pnd_Ws_Header_Record_Cntrct_Crrncy_Cde.equals("1")))                                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr.equals("N")))                                                                                    //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
            {
                pnd_Ws_Legend.setValue("(N)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND := '(N)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Legend.setValue("(F)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND := '(F)'
            }                                                                                                                                                             //Natural: END-IF
            //*   END  - ROXAN 3/14/2002
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Header_Record_Cntrct_Crrncy_Cde.equals("2")))                                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pnd_Ws_Header_Record_Annt_Rsdncy_Cde.greaterOrEqual("74") && pnd_Ws_Header_Record_Annt_Rsdncy_Cde.lessOrEqual("86")))                           //Natural: IF ANNT-RSDNCY-CDE = '74' THRU '86'
            {
                pnd_Ws_Legend.setValue("(C)*");                                                                                                                           //Natural: ASSIGN #WS-LEGEND := '(C)*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Ws_Header_Record_Annt_Rsdncy_Cde.greaterOrEqual("74") && pnd_Ws_Header_Record_Annt_Rsdncy_Cde.lessOrEqual("86"))))                   //Natural: IF NOT ( ANNT-RSDNCY-CDE = '74' THRU '86' )
                {
                    pnd_Ws_Legend.setValue("(C)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND := '(C)'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_State_Legend() throws Exception                                                                                                               //Natural: FORMAT-STATE-LEGEND
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  DETERMINE THE LEGEND PRINTED NEXT TO THE STATE TAX DEDUCTION
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Ws_Header_Record_Cntrct_Crrncy_Cde.equals("1")))                                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pnd_Ws_Header_Record_Annt_Rsdncy_Cde.equals("97")))                                                                                             //Natural: IF ANNT-RSDNCY-CDE = '97'
            {
                pnd_Ws_Legend.setValue("(S)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND := '(S)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pnd_Ws_Header_Record_Annt_Ctznshp_Cde.equals(1) || pnd_Ws_Header_Record_Annt_Ctznshp_Cde.equals(11)) || (pnd_Ws_Header_Record_Annt_Rsdncy_Cde.greaterOrEqual("01")  //Natural: IF ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( ANNT-RSDNCY-CDE = '01' THRU '57' )
                    && pnd_Ws_Header_Record_Annt_Rsdncy_Cde.lessOrEqual("57")))))
                {
                    pnd_Ws_Legend.setValue("(S)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND := '(S)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((! ((pnd_Ws_Header_Record_Annt_Ctznshp_Cde.equals(1) || pnd_Ws_Header_Record_Annt_Ctznshp_Cde.equals(11))) && ! ((pnd_Ws_Header_Record_Annt_Rsdncy_Cde.greaterOrEqual("01")  //Natural: IF NOT ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( ANNT-RSDNCY-CDE = '01' THRU '57' ) AND ANNT-RSDNCY-CDE NE '97'
                        && pnd_Ws_Header_Record_Annt_Rsdncy_Cde.lessOrEqual("57")))) && pnd_Ws_Header_Record_Annt_Rsdncy_Cde.notEquals("97"))))
                    {
                        pnd_Ws_Legend.setValue("(N)");                                                                                                                    //Natural: ASSIGN #WS-LEGEND := '(N)'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Header_Record_Cntrct_Crrncy_Cde.equals("2")))                                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pnd_Ws_Header_Record_Annt_Rsdncy_Cde.greaterOrEqual("74") && pnd_Ws_Header_Record_Annt_Rsdncy_Cde.lessOrEqual("86")))                           //Natural: IF ( ANNT-RSDNCY-CDE = '74' THRU '86' )
            {
                pnd_Ws_Legend.setValue("(P)");                                                                                                                            //Natural: ASSIGN #WS-LEGEND := '(P)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_Header_Record_Annt_Rsdncy_Cde.equals("96")))                                                                                         //Natural: IF ANNT-RSDNCY-CDE = '96'
                {
                    pnd_Ws_Legend.setValue("(C)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND := '(C)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Legend.setValue("(C)");                                                                                                                        //Natural: ASSIGN #WS-LEGEND := '(C)'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bld_Col_Hdng() throws Exception                                                                                                                      //Natural: BLD-COL-HDNG
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  ......... COLUMN HEADING LINES - TIAA OR CREF
        //* ****** LEON BAR 14 ***** DONT HELP ??? ***
        if (condition(pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("2") || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("4") || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("6"))) //Natural: IF #WS-SIDE ( #J ) = '2' OR = '4' OR = '6'
        {
            pnd_Ws_Rec_1.setValue("   ");                                                                                                                                 //Natural: ASSIGN #WS-REC-1 := '   '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("   ");                                                                                                                                 //Natural: ASSIGN #WS-REC-1 := '   '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("3") || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("5") || pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_J).equals("7"))) //Natural: IF #WS-SIDE ( #J ) = '3' OR = '5' OR = '7'
        {
            pnd_Ws_Rec_1.setValue("   ");                                                                                                                                 //Natural: ASSIGN #WS-REC-1 := '   '
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *************************************
        //*  LZ
        short decideConditionsMet1375 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'T' AND #WS-PRINTED-TIAA-HEADING = 'N'
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T") && pnd_Ws_Printed_Tiaa_Heading.equals("N")))
        {
            decideConditionsMet1375++;
            pnd_Ws_Rec_1.setValue("14TIAA");                                                                                                                              //Natural: ASSIGN #WS-REC-1 := '14TIAA'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4Contract");                                                                                                                          //Natural: ASSIGN #WS-REC-1 := ' 4Contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 4'
            //*  LZ
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Printed_Tiaa_Heading.setValue("Y");                                                                                                                    //Natural: ASSIGN #WS-PRINTED-TIAA-HEADING := 'Y'
        }                                                                                                                                                                 //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'C' AND #WS-PRINTED-CREF-HEADING = 'N'
        else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C") && pnd_Ws_Printed_Cref_Heading.equals("N")))
        {
            decideConditionsMet1375++;
            if (condition(pnd_Ws_Printed_Tiaa_Heading.equals("Y")))                                                                                                       //Natural: IF #WS-PRINTED-TIAA-HEADING = 'Y'
            {
                pnd_Ws_Rec_1.setValue("-4CREF");                                                                                                                          //Natural: ASSIGN #WS-REC-1 := '-4CREF'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue("14CREF");                                                                                                                          //Natural: ASSIGN #WS-REC-1 := '14CREF'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4Certificate");                                                                                                                       //Natural: ASSIGN #WS-REC-1 := ' 4Certificate'
            //*  SPIA PA-SELECT
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Printed_Cref_Heading.setValue("Y");                                                                                                                    //Natural: ASSIGN #WS-PRINTED-CREF-HEADING := 'Y'
        }                                                                                                                                                                 //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'L'
        else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("L")))
        {
            decideConditionsMet1375++;
            //*  PA-SELECT
            if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("S") && pnd_Ws_Printed_Select_Heading.equals("N")))                                                         //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'S' AND #WS-PRINTED-SELECT-HEADING = 'N'
            {
                if (condition(pnd_Ws_Printed_Spia_Heading.equals("Y")))                                                                                                   //Natural: IF #WS-PRINTED-SPIA-HEADING = 'Y'
                {
                    pnd_Ws_Rec_1.setValue("-4PA Select");                                                                                                                 //Natural: ASSIGN #WS-REC-1 := '-4PA Select'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Rec_1.setValue("14PA Select");                                                                                                                 //Natural: ASSIGN #WS-REC-1 := '14PA Select'
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4Number");                                                                                                                        //Natural: ASSIGN #WS-REC-1 := ' 4Number'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Printed_Select_Heading.setValue("Y");                                                                                                              //Natural: ASSIGN #WS-PRINTED-SELECT-HEADING := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  SPIA
            if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("M") && pnd_Ws_Printed_Spia_Heading.equals("N")))                                                           //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'M' AND #WS-PRINTED-SPIA-HEADING = 'N'
            {
                if (condition(pnd_Ws_Printed_Select_Heading.equals("Y")))                                                                                                 //Natural: IF #WS-PRINTED-SELECT-HEADING = 'Y'
                {
                    pnd_Ws_Rec_1.setValue("-4SPIA");                                                                                                                      //Natural: ASSIGN #WS-REC-1 := '-4SPIA'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Rec_1.setValue("14SPIA");                                                                                                                      //Natural: ASSIGN #WS-REC-1 := '14SPIA'
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4contract");                                                                                                                      //Natural: ASSIGN #WS-REC-1 := ' 4contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Printed_Spia_Heading.setValue("Y");                                                                                                                //Natural: ASSIGN #WS-PRINTED-SPIA-HEADING := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Rec_1.setValue("14     Payment");                                                                                                                          //Natural: ASSIGN #WS-REC-1 := '14     Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                      //Natural: ASSIGN #WS-REC-1 := ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                      //Natural: ASSIGN #WS-REC-1 := ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*     LEON BAR
        if (condition(pnd_Ws_Dpi_Dci_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-DCI-HDR-WRITTEN = 'Y'
        {
            pnd_Ws_Rec_1.setValue("14      Interest");                                                                                                                    //Natural: ASSIGN #WS-REC-1 := '14      Interest'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*    LEON  BAR
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_Ws_Rec_1.setValue("14  Deductions");                                                                                                                      //Natural: ASSIGN #WS-REC-1 := '14  Deductions'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  LEON BAR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1.setValue("14   Net");                                                                                                                                //Natural: ASSIGN #WS-REC-1 := '14   Net'
        //*   LEON BAR
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4   Payment");                                                                                                                            //Natural: ASSIGN #WS-REC-1 := ' 4   Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                      //Natural: ASSIGN #WS-REC-1 := ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Dpi_Dci_Hdr_Written.equals("N")))                                                                                                            //Natural: IF #WS-DPI-DCI-HDR-WRITTEN = 'N'
        {
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("N")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'N'
        {
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 := '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG
    }
    private void sub_Setup_A_Line_Totals() throws Exception                                                                                                               //Natural: SETUP-A-LINE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_Ws_Tot_Payment_Amt.reset();                                                                                                                                   //Natural: RESET #WS-TOT-PAYMENT-AMT #WS-TOT-DPI-PLUS-DCI-AMT #WS-TOT-DED-AMT #WS-TOT-CHECK-AMT
        pnd_Ws_Tot_Dpi_Plus_Dci_Amt.reset();
        pnd_Ws_Tot_Ded_Amt.reset();
        pnd_Ws_Tot_Check_Amt.reset();
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Tot_Payment_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Payment_Amt), pnd_Ws_Tot_Payment_Amt.add(pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_J)).add(pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(pnd_J))); //Natural: ADD INV-ACCT-SETTL-AMT ( #J ) INV-ACCT-DVDND-AMT ( #J ) TO #WS-TOT-PAYMENT-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Tot_Payment_Amt.setValue(pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_J));                                                                            //Natural: ASSIGN #WS-TOT-PAYMENT-AMT := INV-ACCT-SETTL-AMT ( #J )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Tot_Dpi_Plus_Dci_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Dpi_Plus_Dci_Amt), pnd_Ws_Tot_Dpi_Plus_Dci_Amt.add(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_J)).add(pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(pnd_J))); //Natural: ADD INV-ACCT-DPI-AMT ( #J ) INV-ACCT-DCI-AMT ( #J ) TO #WS-TOT-DPI-PLUS-DCI-AMT
        pnd_Ws_Tot_Ded_Amt.compute(new ComputeParameters(false, pnd_Ws_Tot_Ded_Amt), pnd_Ws_Tot_Ded_Amt.add(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_J)).add(pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_J))); //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #J ) INV-ACCT-STATE-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
        pnd_Ws_Tot_Ded_Amt.nadd(pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_J));                                                                                    //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( #J ) TO #WS-TOT-DED-AMT
        pnd_Ws_Tot_Ded_Amt.nadd(pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_J,"*"));                                                                                         //Natural: ADD PYMNT-DED-AMT ( #J,* ) TO #WS-TOT-DED-AMT
        pnd_Ws_Tot_Check_Amt.nadd(pnd_Ws_Header_Record_Pymnt_Check_Amt);                                                                                                  //Natural: ADD PYMNT-CHECK-AMT TO #WS-TOT-CHECK-AMT
        //*  SETUP-A-LINE-TOTALS
    }
    private void sub_Write_Recs() throws Exception                                                                                                                        //Natural: WRITE-RECS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }
    private void sub_Test_For_Good_Ivc() throws Exception                                                                                                                 //Natural: TEST-FOR-GOOD-IVC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.setValue(false);                                                                                                                  //Natural: ASSIGN #WS-GOOD-IVC := FALSE
        pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc.reset();                                                                                                                          //Natural: RESET #WS-TIAA-IVC #WS-CREF-IVC
        pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc.reset();
        FOR03:                                                                                                                                                            //Natural: FOR #K = 1 TO #WS-CNTR-INV-ACCT
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_K.nadd(1))
        {
            if (condition(pnd_Ws_Occurs_Inv_Acct_Ivc_Ind.getValue(pnd_K).equals("2") && pnd_Ws_Occurs_Inv_Acct_Ivc_Amt.getValue(pnd_K).greater(getZero())))               //Natural: IF INV-ACCT-IVC-IND ( #K ) = '2' AND INV-ACCT-IVC-AMT ( #K ) > 0
            {
                pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.setValue(true);                                                                                                           //Natural: ASSIGN #WS-GOOD-IVC := TRUE
                //*  FRANK ADDED 'R' 10-18
                if (condition(pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_K).equals("T") || pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_K).equals("G") || pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_K).equals("R"))) //Natural: IF INV-ACCT-CDE ( #K ) = 'T' OR = 'G' OR = 'R'
                {
                    pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc.nadd(pnd_Ws_Occurs_Inv_Acct_Ivc_Amt.getValue(pnd_K));                                                                 //Natural: ADD INV-ACCT-IVC-AMT ( #K ) TO #WS-TIAA-IVC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc.nadd(pnd_Ws_Occurs_Inv_Acct_Ivc_Amt.getValue(pnd_K));                                                                 //Natural: ADD INV-ACCT-IVC-AMT ( #K ) TO #WS-CREF-IVC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TEST-FOR-GOOD-IVC
    }
    private void sub_Show_Ivc_Footnote() throws Exception                                                                                                                 //Natural: SHOW-IVC-FOOTNOTE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text.reset();                                                                                                                     //Natural: RESET #WS-IVC-TIAA-TEXT #WS-IVC-CREF-TEXT
        pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text.reset();
        if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc.greater(getZero())))                                                                                                //Natural: IF #WS-TIAA-IVC > 0
        {
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text.setValueEdited(pnd_Ws_Ivc_Area_Pnd_Ws_Tiaa_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                  //Natural: MOVE EDITED #WS-TIAA-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-IVC-TIAA-TEXT
            DbsUtil.examine(new ExamineSource(pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text,true), new ExamineSearch("X", true), new ExamineDelete());                             //Natural: EXAMINE FULL #WS-IVC-TIAA-TEXT FOR FULL 'X' DELETE
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text.setValue(DbsUtil.compress("TIAA:", pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text));                                               //Natural: COMPRESS 'TIAA:' #WS-IVC-TIAA-TEXT INTO #WS-IVC-TIAA-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc.greater(getZero())))                                                                                                //Natural: IF #WS-CREF-IVC > 0
        {
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text.setValueEdited(pnd_Ws_Ivc_Area_Pnd_Ws_Cref_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                  //Natural: MOVE EDITED #WS-CREF-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-IVC-CREF-TEXT
            DbsUtil.examine(new ExamineSource(pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text,true), new ExamineSearch("X", true), new ExamineDelete());                             //Natural: EXAMINE FULL #WS-IVC-CREF-TEXT FOR FULL 'X' DELETE
            pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text.setValue(DbsUtil.compress("CREF:", pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text));                                               //Natural: COMPRESS 'CREF:' #WS-IVC-CREF-TEXT INTO #WS-IVC-CREF-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ......... CREATE IVC MESSAGE LINE.
        //*  .... NOTE: IF "TIAA" OR "CREF" AREAS ARE EMPTY
        //*             THEY WILL BE COMPRESSED OUT
        pnd_Ws_Rec_1.setValue(DbsUtil.compress("17  # PAYT. INCLUDES AFTER-TAX CONTRIBUTIONS OF ", pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Tiaa_Text, pnd_Ws_Ivc_Area_Pnd_Ws_Ivc_Cref_Text)); //Natural: COMPRESS '17  # PAYT. INCLUDES AFTER-TAX CONTRIBUTIONS OF ' #WS-IVC-TIAA-TEXT #WS-IVC-CREF-TEXT INTO #WS-REC-1
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  SHOW-IVC-FOOTNOTE
    }
    private void sub_Bld_Col_Hdng_Pt8() throws Exception                                                                                                                  //Natural: BLD-COL-HDNG-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*   BUILD COLUMN HEADING LINES AS PER THE DATA TO BE PRINTED.
        //*   THERE ARE SOME COLUMNS THAT ARE ONLY CONDITIONALLY PRINTED. IN CASE
        //*   THAT A CERTAIN DATA, FOR EXAMPLE DELAYED PAYMENT INTEREST, IS NOT
        //*   PRESENT FOR THIS PRINTOUT (ON EIHTER OF ITS PAGES!), THEN IT IS NOT
        //*   PRINTED AND THE NEXT PRINTED COLUMN IS SHIFTED LEFT, SO THAT THERE
        //*   ARE NO BLANK COLUMNS.
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                                     //Natural: IF #WS-FIRST-TIME = 'Y'
        {
            pnd_Ws_First_Time.setValue("N");                                                                                                                              //Natural: ASSIGN #WS-FIRST-TIME = 'N'
                                                                                                                                                                          //Natural: PERFORM BLD-COL-HDNG
            sub_Bld_Col_Hdng();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("15");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG-PT8
    }
    private void sub_Bld_A_Line_Pt8() throws Exception                                                                                                                    //Natural: BLD-A-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*    "A Line": TOTALS FOR THE "group of Lines"
        //*  ----------------------------------------------------------------------
        //*   ...... ACCUM THE TOTALS FOR THE "group of Lines"
                                                                                                                                                                          //Natural: PERFORM SETUP-A-LINE-TOTALS
        sub_Setup_A_Line_Totals();
        if (condition(Global.isEscape())) {return;}
        //*   ...... NOW BUILD THE "A Line"
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '05'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN - PPCN - ALWAYS
        //*  ENTER CNTRCT-PPCN-NBR
        //*  FOR MULTIPLE CREF PRODUCTS, ONLY ENTER THE PPCN FOR
        //*  THE 1ST OCCURENCE
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        //* * INSERTED 10-18-95 BY FRANK  ADDED 'R'
        //*  LUMP SUM
        if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("40") && ! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean())))                                 //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '40' AND NOT #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Header_Record_Cntrct_Cref_Nbr,new ReportEditMask("XXXXXXX-X"));                                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-CREF-NBR ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Cntrct_Ppcn_Nbr.getValue(pnd_J),new ReportEditMask("XXXXXXX-X"));                                                //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(pnd_Ws_Temp_Col));                                                                           //Natural: COMPRESS #WS-TEMP-COL INTO #WS-COLUMN ( #F )
        //*  .........2ND COLUMN - TOTAL PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                    //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                                       //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                        //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
        //*  .........3RD COLUMN - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                        //Natural: MOVE EDITED INV-ACCT-DPI-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  .........4TH COLUMN - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Deduction_Column_Nbr.setValue(pnd_F);                                                                                                                     //Natural: ASSIGN #DEDUCTION-COLUMN-NBR := #F
            if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                         //Natural: IF #WS-TOT-DED-AMT > 0
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZ,ZZ9.99"));                                                                   //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ........ 5TH COLUMN - NET PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                                  //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-A-LINE-PT8
    }
    private void sub_Bld_Db_Line_Pt8() throws Exception                                                                                                                   //Natural: BLD-DB-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*   BUILD THE  "Db Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN - PPCN
        //*  FOR TIAA LEAVE SPACE, FOR CREF
        //*  ENTER THE INVESTMENT ACCOUNT's description (use FCPN199A'S DATA)
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-1
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ............ COLUMN 3 - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ............ COLUMN 4 - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 7 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DB-LINE-PT8
    }
    private void sub_Bld_Dc_Line_Pt8() throws Exception                                                                                                                   //Natural: BLD-DC-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dc Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ........ 1ST COLUMN - SETTLEMENT DATE
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-2
        //* ** ENTER THE SETTLEMENT DATE FOR THIS SPECIFIC PAYMENT
        //* *IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 (#J) GT 0
        //* *  MOVE EDITED #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE (#J) (EM=MM/DD/YY)
        //* *    TO #WS-TEMP-COL
        //* *  #WS-COLUMN (#F) := #WS-TEMP-COL
        //* *ELSE
        //* *  WRITE *PROGRAM 'field should have a valid date:'
        //* *    '='#J
        //* *    '=' #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-A4 (#J) (EM=H(8))
        //* *  RESET #WS-COLUMN (#F)
        //* *END-IF
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ......... 3RD COLUMN - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ............ 4TH COLUMN - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  ............ COLUMN 5 - NET PAYMENT
        //*  EMPTY ON THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DC-LINE-PT8
    }
    private void sub_Bld_Dx_Line_Pt8() throws Exception                                                                                                                   //Natural: BLD-DX-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dx Line": LOCAL TAX DEDUCTIONS, IF NEEDED
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Nbr_Of_Remaining_Deductions.greater(getZero())))                                                                                                //Natural: IF #NBR-OF-REMAINING-DEDUCTIONS GT 0
        {
            pnd_Ws_Rec_1.reset();                                                                                                                                         //Natural: RESET #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 5");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '+3'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_Deduction_Column_Nbr).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #DEDUCTION-COLUMN-NBR )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-DX-LINE-PT8
    }
    private void sub_Bld_E_Line_Pt8() throws Exception                                                                                                                    //Natural: BLD-E-LINE-PT8
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD "Grand Totals" LINE
        //*  ----------------------------------------------------------------------
        //*  ............ACCUM "Grand Totals"
        if (condition(pnd_Ws_Side_Printed.equals(pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex)))                                                                  //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_Payment.reset();                                                                                                                             //Natural: RESET #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Dpi.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            //* ******
            pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-SETTL-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-DVDND-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Dpi.nadd(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                               //Natural: ADD INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DPI
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                          //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct,"*"));                                                              //Natural: ADD PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Tot_Check_Amt.nadd(pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-TOT-CHECK-AMT
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC := '06'
            pnd_F.setValue(pnd_1st_F);                                                                                                                                    //Natural: ASSIGN #F := #1ST-F
            //*  ..... 1ST COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
            //*  ......... EDITE / BUILD THE GRAND TOTAL LINE
            //*  ..... 2ND COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
            //*  .... IF THERE IS IVC MONEY "footnote" THE GRAND TOTAL
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.setValue("#");                                                                                         //Natural: ASSIGN #WS-COLUMN-OFFSET-RGT-POS1 := '#'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                    //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            //*  ..... 3RD COLUMN
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###***
            //*  ..... 4TH COLUMN
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99"));                                                                //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###***
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                  //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-E-LINE-PT8
    }
    private void sub_Bld_Col_Hdng_Pt9() throws Exception                                                                                                                  //Natural: BLD-COL-HDNG-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*   BUILD COLUMN HEADING LINES AS PER THE DATA TO BE PRINTED.
        //*   THERE ARE SOME COLUMNS THAT ARE ONLY CONDITIONALLY PRINTED. IN CASE
        //*   THAT A CERTAIN DATA, FOR EXAMPLE DELAYED PAYMENT INTEREST, IS NOT
        //*   PRESENT FOR THIS PRINTOUT (ON ANY OF ITS PAGES!), THEN IT IS NOT
        //*   PRINTED.
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                                     //Natural: IF #WS-FIRST-TIME = 'Y'
        {
            pnd_Ws_First_Time.setValue("N");                                                                                                                              //Natural: ASSIGN #WS-FIRST-TIME = 'N'
            //*  ......... COLUMN HEADING LINES - TIAA OR CREF
            //*  ......... COLUMN HEADING LINES - TIAA OR CREF
            //*  LZ
            short decideConditionsMet1889 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'T' AND #WS-PRINTED-TIAA-HEADING = 'N'
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T") && pnd_Ws_Printed_Tiaa_Heading.equals("N")))
            {
                decideConditionsMet1889++;
                pnd_Ws_Rec_1.setValue("14TIAA");                                                                                                                          //Natural: ASSIGN #WS-REC-1 = '14TIAA'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4Contract");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = ' 4Contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                //*  LZ
                pnd_Ws_Printed_Tiaa_Heading.setValue("Y");                                                                                                                //Natural: ASSIGN #WS-PRINTED-TIAA-HEADING = 'Y'
            }                                                                                                                                                             //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'C' AND #WS-PRINTED-CREF-HEADING = 'N'
            else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("C") && pnd_Ws_Printed_Cref_Heading.equals("N")))
            {
                decideConditionsMet1889++;
                if (condition(pnd_Ws_Printed_Tiaa_Heading.equals("Y")))                                                                                                   //Natural: IF #WS-PRINTED-TIAA-HEADING = 'Y'
                {
                    pnd_Ws_Rec_1.setValue("-4CREF");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '-4CREF'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Rec_1.setValue("14CREF");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14CREF'
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4Certificate");                                                                                                                   //Natural: ASSIGN #WS-REC-1 = ' 4Certificate'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                //*  LZ
                pnd_Ws_Printed_Cref_Heading.setValue("Y");                                                                                                                //Natural: ASSIGN #WS-PRINTED-CREF-HEADING = 'Y'
            }                                                                                                                                                             //Natural: WHEN #FUND-PDA.#COMPANY-CDE = 'L'
            else if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("L")))
            {
                decideConditionsMet1889++;
                //*  PA-SELECT
                if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("S") && pnd_Ws_Printed_Select_Heading.equals("N")))                                                     //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'S' AND #WS-PRINTED-SELECT-HEADING = 'N'
                {
                    if (condition(pnd_Ws_Printed_Spia_Heading.equals("Y")))                                                                                               //Natural: IF #WS-PRINTED-SPIA-HEADING = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("-4PA Select");                                                                                                             //Natural: ASSIGN #WS-REC-1 := '-4PA Select'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1.setValue("14PA Select");                                                                                                             //Natural: ASSIGN #WS-REC-1 := '14PA Select'
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ws_Rec_1.setValue(" 4Number");                                                                                                                    //Natural: ASSIGN #WS-REC-1 := ' 4Number'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ws_Printed_Select_Heading.setValue("Y");                                                                                                          //Natural: ASSIGN #WS-PRINTED-SELECT-HEADING := 'Y'
                }                                                                                                                                                         //Natural: END-IF
                //*  SPIA
                if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("M") && pnd_Ws_Printed_Spia_Heading.equals("N")))                                                       //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'M' AND #WS-PRINTED-SPIA-HEADING = 'N'
                {
                    if (condition(pnd_Ws_Printed_Select_Heading.equals("Y")))                                                                                             //Natural: IF #WS-PRINTED-SELECT-HEADING = 'Y'
                    {
                        pnd_Ws_Rec_1.setValue("-4SPIA");                                                                                                                  //Natural: ASSIGN #WS-REC-1 := '-4SPIA'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Rec_1.setValue("14SPIA");                                                                                                                  //Natural: ASSIGN #WS-REC-1 := '14SPIA'
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ws_Rec_1.setValue(" 4contract");                                                                                                                  //Natural: ASSIGN #WS-REC-1 := ' 4contract'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                    sub_Write_Recs();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ws_Printed_Spia_Heading.setValue("Y");                                                                                                            //Natural: ASSIGN #WS-PRINTED-SPIA-HEADING := 'Y'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  ............... MORE COLUMN HEADINGS
            pnd_Ws_Rec_1.setValue("14     Payment");                                                                                                                      //Natural: ASSIGN #WS-REC-1 = '14     Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  ...........INTEREST HEADING OR BLANK HEADINGS
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_Ws_Rec_1.setValue("14     Interest");                                                                                                                 //Natural: ASSIGN #WS-REC-1 = '14     Interest'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  NO HEADER FOR LITERAL "TTB"
                pnd_Ws_Rec_1.setValue("14");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = '14'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            //*  ........... DEDUCTIONS
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_Ws_Rec_1.setValue("14    Deductions");                                                                                                                //Natural: ASSIGN #WS-REC-1 = '14    Deductions'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Rec_1.setValue(" 4");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1.setValue("14       Net");                                                                                                                        //Natural: ASSIGN #WS-REC-1 = '14       Net'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4       Payment");                                                                                                                    //Natural: ASSIGN #WS-REC-1 = ' 4       Payment'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue(" 4");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 4'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("N")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'N'
            {
                pnd_Ws_Rec_1.setValue("14");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
                sub_Write_Recs();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1.setValue("14");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '14'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1.setValue("15");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = '15'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-COL-HDNG-PT9
    }
    private void sub_Bld_A_Line_Pt9() throws Exception                                                                                                                    //Natural: BLD-A-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*    "A Line": TOTALS FOR THE "group of Lines"
        //*  ----------------------------------------------------------------------
        //*   ...... ACCUM THE TOTALS FOR THE "group of Lines"
                                                                                                                                                                          //Natural: PERFORM SETUP-A-LINE-TOTALS
        sub_Setup_A_Line_Totals();
        if (condition(Global.isEscape())) {return;}
        //*   ...... NOW BUILD THE "A Line"
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("05");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '05'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN - PPCN - ALWAYS
        //*  ENTER CNTRCT-PPCN-NBR
        //*  FOR MULTIPLE CREF PRODUCTS, ONLY ENTER THE PPCN FOR
        //*  THE 1ST OCCURENCE
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Cntrct_Ppcn_Nbr.getValue(pnd_J),new ReportEditMask("XXXXXXX-X"));                                                    //Natural: MOVE EDITED #WS-OCCURS.CNTRCT-PPCN-NBR ( #J ) ( EM = XXXXXXX-X ) TO #WS-TEMP-COL
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(DbsUtil.compress(pnd_Ws_Temp_Col));                                                                           //Natural: COMPRESS #WS-TEMP-COL INTO #WS-COLUMN ( #F )
        //*  .........2ND COLUMN - TOTAL PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Payment_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                    //Natural: MOVE EDITED #WS-TOT-PAYMENT-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
        //* **IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '20'
        //* **  #WS-COLUMN-OFFSET-RGT-POS1 := 'T'
        //* **ELSE
        pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                                       //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                        //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
        //*  .........3RD COLUMN - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                        //Natural: MOVE EDITED INV-ACCT-DPI-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ...... 4TH COLUMN FOR TTB LITERAL
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("TTB");                                                                                                       //Natural: ASSIGN #WS-COLUMN ( #F ) := 'TTB'
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  .........5TH COLUMN - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Deduction_Column_Nbr.setValue(pnd_F);                                                                                                                     //Natural: ASSIGN #DEDUCTION-COLUMN-NBR := #F
            if (condition(pnd_Ws_Tot_Ded_Amt.greater(getZero())))                                                                                                         //Natural: IF #WS-TOT-DED-AMT > 0
            {
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Ded_Amt,new ReportEditMask("X'$'ZZ,ZZ9.99"));                                                                   //Natural: MOVE EDITED #WS-TOT-DED-AMT ( EM = X'$'ZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ........ 5TH COLUMN - NET PAYMENT - ALWAYS
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_J),new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED INV-ACCT-NET-PYMNT-AMT ( #J ) ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
        DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                                  //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-A-LINE-PT9
    }
    private void sub_Bld_Db_Line_Pt9() throws Exception                                                                                                                   //Natural: BLD-DB-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*   BUILD THE  "Db Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  .........1ST COLUMN - PPCN
        //*  FOR TIAA LEAVE SPACE, FOR CREF
        //*  ENTER THE INVESTMENT ACCOUNT's description (use FCPN199A'S DATA)
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-1
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ............ COLUMN 3 - INTEREST - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ...... COLUMN 4 - FOR TTB LITERAL
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ............ COLUMN 5 - DEDUCTIONS - CONDITIONAL
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO MORE COLUNMS FOR THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DB-LINE-PT9
    }
    private void sub_Bld_Dc_Line_Pt9() throws Exception                                                                                                                   //Natural: BLD-DC-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dc Line"
        //*  ----------------------------------------------------------------------
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue(" 5");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                       //Natural: ASSIGN #WS-REC1-CC := '+3'
        pnd_F.setValue(pnd_1st_F);                                                                                                                                        //Natural: ASSIGN #F := #1ST-F
        //*  ........ 1ST COLUMN - SETTLEMENT DATE
        //*  LZ
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                                              //Natural: ASSIGN #WS-COLUMN ( #F ) := #FUND-PDA.#STMNT-LINE-2
        //* ** ENTER THE SETTLEMENT DATE FOR THIS SPECIFIC PAYMENT
        //* *IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-P6 (#J) GT 0
        //* *  MOVE EDITED #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE (#J) (EM=MM/DD/YY)
        //* *    TO #WS-TEMP-COL
        //* *  #WS-COLUMN (#F) := #WS-TEMP-COL
        //* *ELSE
        //* *  WRITE *PROGRAM 'field should have a valid date:'
        //* *    '='#J
        //* *    '=' #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-A4 (#J) (EM=H(8))
        //* *  RESET #WS-COLUMN (#F)
        //* *END-IF
        //*  ........ 2ND COLUMN
        //*    THERE WILL BE NO CONTRACTUAL, DIVIDEND, UNITS AND UNIT VALUES
        //*    ENTERED FOR MDO SETTLEMENTS.
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //*  ......... 3RD COLUMN - INTEREST
        //*  THIS COLUMN DISPLAYS THE INTEREST DATA BUT ONLY IF
        //*  THERE IS AN AMOUNT IN THE FIRST INV-ACCT-DPI-AMT FOR
        //*  ANY OF THE INSTALLMENTS FOR THE SETTLMENT.
        //*  OTHERWISE, THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        //*  EMPTY ON THIS LINE.
        if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                                //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ...... 4TH COLUMN FOR TTB LITERAL
        pnd_F.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #F
        pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                               //Natural: RESET #WS-COLUMN ( #F )
        //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
        if (condition(pnd_F.equals(3)))                                                                                                                                   //Natural: IF #F = 3
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
        }                                                                                                                                                                 //Natural: END-IF
        //* **###***
        //*  ............ 5TH COLUMN - DEDUCTIONS
        //*  THIS COLUMN DISPLAYS THE TAX DEDUCTIONS AND/OR THE ADDITIONAL
        //*  DEDUCTIONS.  IF THERE ARE NO DEDUCTION FOR
        //*  ANY OF THE INSTALLMENTS FOR THIS SETTLMENT, THEN
        //*  THE COLUMN AND IT's header will not be printed
        //*  AND THE NEXT COLUMN IS SHIFTED OVER.
        if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                             //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
        {
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col);                                                                                         //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-TEMP-COL
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO MORE COLUMNS FOR THIS LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
        sub_Write_Recs();
        if (condition(Global.isEscape())) {return;}
        //*  BLD-DC-LINE-PT9
    }
    private void sub_Bld_Dx_Line_Pt9() throws Exception                                                                                                                   //Natural: BLD-DX-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD THE "Dx Line": LOCAL TAX DEDUCTIONS, IF NEEDED
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Nbr_Of_Remaining_Deductions.greater(getZero())))                                                                                                //Natural: IF #NBR-OF-REMAINING-DEDUCTIONS GT 0
        {
            pnd_Ws_Rec_1.reset();                                                                                                                                         //Natural: RESET #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 5");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 5'
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("+3");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC = '+3'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AND-EDIT-NEXT-DEDUCTION
            sub_Obtain_And_Edit_Next_Deduction();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_Deduction_Column_Nbr).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #DEDUCTION-COLUMN-NBR )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-DX-LINE-PT9
    }
    private void sub_Bld_E_Line_Pt9() throws Exception                                                                                                                    //Natural: BLD-E-LINE-PT9
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        //*  BUILD "Grand Totals" LINE
        //*  ----------------------------------------------------------------------
        //*  ............ACCUM "Grand Totals"
        if (condition(pnd_Ws_Side_Printed.equals(pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex)))                                                                  //Natural: IF #WS-SIDE-PRINTED = #WS-HEADER-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
        {
            pnd_Ws_Grand_Tot_Payment.reset();                                                                                                                             //Natural: RESET #WS-GRAND-TOT-PAYMENT #WS-GRAND-TOT-DPI #WS-GRAND-TOT-DED #WS-TOT-CHECK-AMT
            pnd_Ws_Grand_Tot_Dpi.reset();
            pnd_Ws_Grand_Tot_Ded.reset();
            pnd_Ws_Tot_Check_Amt.reset();
            //* ******
            pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-SETTL-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Payment.nadd(pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-DVDND-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-PAYMENT
            pnd_Ws_Grand_Tot_Dpi.nadd(pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                               //Natural: ADD INV-ACCT-DPI-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DPI
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                          //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Grand_Tot_Ded.nadd(pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct,"*"));                                                              //Natural: ADD PYMNT-DED-AMT ( 1:#WS-CNTR-INV-ACCT,* ) TO #WS-GRAND-TOT-DED
            pnd_Ws_Tot_Check_Amt.nadd(pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                                                         //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #WS-TOT-CHECK-AMT
            pnd_Ws_Rec_1_Pnd_Ws_Rec1_Cc.setValue("06");                                                                                                                   //Natural: ASSIGN #WS-REC1-CC := '06'
            pnd_F.setValue(pnd_1st_F);                                                                                                                                    //Natural: ASSIGN #F := #1ST-F
            //*  ..... 1ST COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue("Grand Total");                                                                                           //Natural: ASSIGN #WS-COLUMN ( #F ) = 'Grand Total'
            //*  ......... EDITE / BUILD THE GRAND TOTAL LINE
            //*  ..... 2ND COLUMN
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Payment,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-PAYMENT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_1_Data.setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN-OFFSET-RGT-1-DATA
            //*  .... IF THERE IS IVC MONEY "footnote" THE GRAND TOTAL
            if (condition(pnd_Ws_Ivc_Area_Pnd_Ws_Good_Ivc.getBoolean()))                                                                                                  //Natural: IF #WS-GOOD-IVC
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.setValue("#");                                                                                         //Natural: ASSIGN #WS-COLUMN-OFFSET-RGT-POS1 := '#'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Column_Offset_Pnd_Ws_Column_Offset_Rgt_Pos1.reset();                                                                                               //Natural: RESET #WS-COLUMN-OFFSET-RGT-POS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Column_Offset);                                                                                    //Natural: ASSIGN #WS-COLUMN ( #F ) := #WS-COLUMN-OFFSET
            //*  ..... 3RD COLUMN
            if (condition(pnd_Ws_Dpi_Hdr_Written.equals("Y")))                                                                                                            //Natural: IF #WS-DPI-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Dpi,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                              //Natural: MOVE EDITED #WS-GRAND-TOT-DPI ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //*  ...... 4TH COLUMN FOR TTB LITERAL
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                           //Natural: RESET #WS-COLUMN ( #F )
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###***
            //*  ..... 5TH COLUMN
            if (condition(pnd_Ws_Deduct_Hdr_Written.equals("Y")))                                                                                                         //Natural: IF #WS-DEDUCT-HDR-WRITTEN = 'Y'
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Grand_Tot_Ded,new ReportEditMask("X'$'ZZZ,ZZ9.99"));                                                                //Natural: MOVE EDITED #WS-GRAND-TOT-DED ( EM = X'$'ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
                DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                              //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
                //*  OFFSET 3 BYTES TO THE LEFT TO ALLIGH WITH THE LEGEND
                //*  ON THE DETAIL LINES
                pnd_Ws_Temp_Col.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Temp_Col, "XXX"));                                                        //Natural: COMPRESS #WS-TEMP-COL 'XXX' INTO #WS-TEMP-COL LEAVING NO
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                          //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F),true), new ExamineSearch("X", true), new ExamineReplace(" "));               //Natural: EXAMINE FULL #WS-COLUMN ( #F ) FOR FULL 'X' REPLACE ' '
            }                                                                                                                                                             //Natural: END-IF
            //* **###*** /* COMPENSATE FOR LACK OF HEADINGS FOR COLUMN 4
            if (condition(pnd_F.equals(3)))                                                                                                                               //Natural: IF #F = 3
            {
                pnd_F.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #F
                pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).reset();                                                                                                       //Natural: RESET #WS-COLUMN ( #F )
            }                                                                                                                                                             //Natural: END-IF
            //* **###***
            pnd_F.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #F
            pnd_Ws_Temp_Col.setValueEdited(pnd_Ws_Tot_Check_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                                                  //Natural: MOVE EDITED #WS-TOT-CHECK-AMT ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #WS-TEMP-COL
            DbsUtil.examine(new ExamineSource(pnd_Ws_Temp_Col,true), new ExamineSearch("X", true), new ExamineDelete());                                                  //Natural: EXAMINE FULL #WS-TEMP-COL FOR FULL 'X' DELETE
            pnd_Ws_Rec_1_Pnd_Ws_Column.getValue(pnd_F).setValue(pnd_Ws_Temp_Col, MoveOption.RightJustified);                                                              //Natural: MOVE RIGHT #WS-TEMP-COL TO #WS-COLUMN ( #F )
                                                                                                                                                                          //Natural: PERFORM WRITE-RECS
            sub_Write_Recs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLD-E-LINE-PT9
    }

    //
}
