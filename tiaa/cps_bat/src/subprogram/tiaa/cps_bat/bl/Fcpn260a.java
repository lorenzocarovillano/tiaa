/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:52 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn260a
************************************************************
**        * FILE NAME            : Fcpn260a.java
**        * CLASS NAME           : Fcpn260a
**        * INSTANCE NAME        : Fcpn260a
************************************************************
** COPY OF FCPN260 - FOR FORMATS UNCHANGED BY PAYEE MATCH
*
* R. LANDRUM    01/05/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn260a extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa200 pdaFcpa200;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Addr_Lines;
    private DbsField pnd_Ws_Justify_Addr;
    private DbsField pnd_Ws_Justify_Name;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_I;
    private DbsField pnd_Ws_Index;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa200 = new PdaFcpa200(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Addr_Lines = localVariables.newFieldInRecord("pnd_Ws_Addr_Lines", "#WS-ADDR-LINES", FieldType.NUMERIC, 1);
        pnd_Ws_Justify_Addr = localVariables.newFieldInRecord("pnd_Ws_Justify_Addr", "#WS-JUSTIFY-ADDR", FieldType.STRING, 35);
        pnd_Ws_Justify_Name = localVariables.newFieldInRecord("pnd_Ws_Justify_Name", "#WS-JUSTIFY-NAME", FieldType.STRING, 38);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Ws_Index = localVariables.newFieldInRecord("pnd_Ws_Index", "#WS-INDEX", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Ws_Addr_Lines.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_Ws_Index.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn260a() throws Exception
    {
        super("Fcpn260a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* RL
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1 #WS-JUSTIFY-ADDR
        pnd_Ws_Justify_Addr.reset();
        pnd_I.setValue(1);                                                                                                                                                //Natural: ASSIGN #I = 1
        if (condition(DbsUtil.maskMatches(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(pnd_I),"'CR '")))                                                      //Natural: IF PYMNT-NME ( #I ) = MASK ( 'CR ' )
        {
            //* RL
            pnd_Ws_Justify_Addr.setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(pnd_I), MoveOption.LeftJustified);                           //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE1-TXT ( #I ) TO #WS-JUSTIFY-ADDR
            //* RL
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "17******", pnd_Ws_Justify_Addr));                                                      //Natural: COMPRESS '17******' #WS-JUSTIFY-ADDR INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WS-ADDR-LINES
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *COMPRESS '17******'  PYMNT-NME (#I)
            //* RL
            pnd_Ws_Justify_Name.setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(pnd_I), MoveOption.LeftJustified);                                      //Natural: MOVE LEFT JUSTIFIED PYMNT-NME ( #I ) TO #WS-JUSTIFY-NAME
            //* RL
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "17******", pnd_Ws_Justify_Name));                                                      //Natural: COMPRESS '17******' #WS-JUSTIFY-NAME INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            //* *COMPRESS '*7******' PYMNT-ADDR-LINE1-TXT (#I) /* RL
            //* RL
            pnd_Ws_Justify_Addr.setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(pnd_I), MoveOption.LeftJustified);                           //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE1-TXT ( #I ) TO #WS-JUSTIFY-ADDR
            //* RL
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pnd_Ws_Justify_Addr));                                                      //Natural: COMPRESS '*7******' #WS-JUSTIFY-ADDR INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Addr_Lines.nadd(2);                                                                                                                                    //Natural: ADD 2 TO #WS-ADDR-LINES
        }                                                                                                                                                                 //Natural: END-IF
        //* RL
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1 #WS-JUSTIFY-ADDR
        pnd_Ws_Justify_Addr.reset();
        //* *COMPRESS '*7******' PYMNT-ADDR-LINE2-TXT (#I)
        //* RL
        pnd_Ws_Justify_Addr.setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(pnd_I), MoveOption.LeftJustified);                               //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE2-TXT ( #I ) TO #WS-JUSTIFY-ADDR
        //* RL
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pnd_Ws_Justify_Addr));                                                          //Natural: COMPRESS '*7******' #WS-JUSTIFY-ADDR INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WS-ADDR-LINES
        //* RL
        pnd_Ws_Justify_Addr.reset();                                                                                                                                      //Natural: RESET #WS-JUSTIFY-ADDR
        //* *COMPRESS '*7******' PYMNT-ADDR-LINE3-TXT (#I)
        //* RL
        pnd_Ws_Justify_Addr.setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(pnd_I), MoveOption.LeftJustified);                               //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE3-TXT ( #I ) TO #WS-JUSTIFY-ADDR
        //* RL
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pnd_Ws_Justify_Addr));                                                          //Natural: COMPRESS '*7******' #WS-JUSTIFY-ADDR INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WS-ADDR-LINES
        //* RL
        pnd_Ws_Justify_Addr.reset();                                                                                                                                      //Natural: RESET #WS-JUSTIFY-ADDR
        //* *COMPRESS '*7******' PYMNT-ADDR-LINE4-TXT (#I) /* RL
        //* RL
        pnd_Ws_Justify_Addr.setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(pnd_I), MoveOption.LeftJustified);                               //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE4-TXT ( #I ) TO #WS-JUSTIFY-ADDR
        //* RL
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pnd_Ws_Justify_Addr));                                                          //Natural: COMPRESS '*7******' #WS-JUSTIFY-ADDR INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WS-ADDR-LINES
        //* RL
        pnd_Ws_Justify_Addr.reset();                                                                                                                                      //Natural: RESET #WS-JUSTIFY-ADDR
        if (condition(pnd_Ws_Addr_Lines.equals(4)))                                                                                                                       //Natural: IF #WS-ADDR-LINES = 4
        {
            //* *COMPRESS '*7******' PYMNT-ADDR-LINE5-TXT (#I) /* RL
            //* RL
            pnd_Ws_Justify_Addr.setValue(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(pnd_I), MoveOption.LeftJustified);                           //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE5-TXT ( #I ) TO #WS-JUSTIFY-ADDR
            //* RL
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*7******", pnd_Ws_Justify_Addr));                                                      //Natural: COMPRESS '*7******' #WS-JUSTIFY-ADDR INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Addr_Lines.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WS-ADDR-LINES
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals(" ") || pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals("0"))  //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( #I ) = ' ' OR = '0' ) OR PYMNT-NME ( 1 ) = MASK ( 'CR ' )
            || DbsUtil.maskMatches(pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1),"'CR '")))
        {
            if (condition(! (pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals(" ") || pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(pnd_I).equals("0")))) //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( #I ) = ' ' OR = '0' )
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("+7++++++", pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1), "H'0000'", "A/C", "H'00'",                //Natural: COMPRESS '+7++++++' PYMNT-NME ( 1 ) H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR ( 1 ) INTO #WS-REC-1
                    pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1)));
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("+7++++++", pdaFcpa200.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1)));                                         //Natural: COMPRESS '+7++++++' PYMNT-NME ( 1 ) INTO #WS-REC-1
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *ASSIGN #WS-REC-1 = ' 7'
            //*  RL
            pnd_Ws_Rec_1.setValue(" 7");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 7'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Index.setValue(2);                                                                                                                                         //Natural: ASSIGN #WS-INDEX = 2
        //*  FORMAT BARCODE AND ADDR LINES
        DbsUtil.callnat(Fcpn253.class , getCurrentProcessState(), pdaFcpa200.getPnd_Ws_Header_Record().getValue("*"), pdaFcpa200.getPnd_Ws_Occurs().getValue("*"),        //Natural: CALLNAT 'FCPN253' #WS-HEADER-RECORD ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-INDEX
            pdaFcpa200.getPnd_Ws_Name_N_Address().getValue("*"), pnd_Ws_Index);
        if (condition(Global.isEscape())) return;
    }

    //
}
