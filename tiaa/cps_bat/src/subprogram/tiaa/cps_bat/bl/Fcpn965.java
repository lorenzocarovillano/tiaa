/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:29 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn965
************************************************************
**        * FILE NAME            : Fcpn965.java
**        * CLASS NAME           : Fcpn965
**        * INSTANCE NAME        : Fcpn965
************************************************************
***********************************************************************
* PROGRAM:  FCPN965
*
* TITLE:    CPS DC AND NZ XML STATEMENT GENERATION
* AUTHOR:   FRANCIS ENDAYA
* DESC:     THE PROGRAM READS XML FILE AND REPLACE BATCH TOTALS WITH
*           PROCESSING.
******************************** FCPN874B AND C
* MODIFICATION LOG
* 2016/11/01 F.ENDAYA  NEW
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn965 extends BLNatBase
{
    // Data Areas
    private LdaFcpl961 ldaFcpl961;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Batch_Total;
    private DbsField pnd_Xml_Line2;

    private DbsGroup pnd_Xml_Line2__R_Field_1;
    private DbsField pnd_Xml_Line2_Pnd_Xml_Line_A;
    private DbsField pnd_Tot_Ndx;
    private DbsField pnd_Xml_Line;
    private DbsField pnd_Xml_Line_Length;
    private DbsField pnd_Out_Area14;

    private DbsGroup pnd_Out_Area14__R_Field_2;
    private DbsField pnd_Out_Area14_Pnd_Out_Batch_Cnt;
    private DbsField pnd_Out_Area14_Pnd_Out_Filler;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl961 = new LdaFcpl961();
        registerRecord(ldaFcpl961);

        // parameters
        parameters = new DbsRecord();
        pnd_Batch_Total = parameters.newFieldInRecord("pnd_Batch_Total", "#BATCH-TOTAL", FieldType.NUMERIC, 7);
        pnd_Batch_Total.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Xml_Line2 = localVariables.newFieldInRecord("pnd_Xml_Line2", "#XML-LINE2", FieldType.STRING, 250);

        pnd_Xml_Line2__R_Field_1 = localVariables.newGroupInRecord("pnd_Xml_Line2__R_Field_1", "REDEFINE", pnd_Xml_Line2);
        pnd_Xml_Line2_Pnd_Xml_Line_A = pnd_Xml_Line2__R_Field_1.newFieldArrayInGroup("pnd_Xml_Line2_Pnd_Xml_Line_A", "#XML-LINE-A", FieldType.STRING, 
            1, new DbsArrayController(1, 250));
        pnd_Tot_Ndx = localVariables.newFieldInRecord("pnd_Tot_Ndx", "#TOT-NDX", FieldType.PACKED_DECIMAL, 5);
        pnd_Xml_Line = localVariables.newFieldInRecord("pnd_Xml_Line", "#XML-LINE", FieldType.STRING, 250);
        pnd_Xml_Line_Length = localVariables.newFieldInRecord("pnd_Xml_Line_Length", "#XML-LINE-LENGTH", FieldType.NUMERIC, 7);
        pnd_Out_Area14 = localVariables.newFieldInRecord("pnd_Out_Area14", "#OUT-AREA14", FieldType.STRING, 80);

        pnd_Out_Area14__R_Field_2 = localVariables.newGroupInRecord("pnd_Out_Area14__R_Field_2", "REDEFINE", pnd_Out_Area14);
        pnd_Out_Area14_Pnd_Out_Batch_Cnt = pnd_Out_Area14__R_Field_2.newFieldInGroup("pnd_Out_Area14_Pnd_Out_Batch_Cnt", "#OUT-BATCH-CNT", FieldType.STRING, 
            7);
        pnd_Out_Area14_Pnd_Out_Filler = pnd_Out_Area14__R_Field_2.newFieldInGroup("pnd_Out_Area14_Pnd_Out_Filler", "#OUT-FILLER", FieldType.STRING, 73);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl961.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn965() throws Exception
    {
        super("Fcpn965");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* ***************                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON
        //*  MAIN PROGRAM *
        //* ***************
        pnd_Out_Area14.reset();                                                                                                                                           //Natural: RESET #OUT-AREA14
        ldaFcpl961.getCommon_Xml_Batchtotal_Data().setValue(pnd_Batch_Total);                                                                                             //Natural: MOVE #BATCH-TOTAL TO BATCHTOTAL-DATA
        getWorkFiles().close(8);                                                                                                                                          //Natural: CLOSE WORK FILE 8
        RD1:                                                                                                                                                              //Natural: READ WORK FILE 8 #XML-LINE
        while (condition(getWorkFiles().read(8, pnd_Xml_Line)))
        {
            DbsUtil.examine(new ExamineSource(pnd_Xml_Line), new ExamineSearch(ldaFcpl961.getCommon_Xml_Batchtotal_Open()), new ExamineGivingNumber(pnd_Tot_Ndx));        //Natural: EXAMINE #XML-LINE BATCHTOTAL-OPEN GIVING NUMBER #TOT-NDX
            if (condition(pnd_Tot_Ndx.greater(getZero())))                                                                                                                //Natural: IF #TOT-NDX GT 0
            {
                pnd_Xml_Line2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Batchtotal_Open(), ldaFcpl961.getCommon_Xml_Batchtotal_Data(),  //Natural: COMPRESS BATCHTOTAL-OPEN BATCHTOTAL-DATA BATCHTOTAL-CLOSE INTO #XML-LINE2 LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Batchtotal_Close()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Xml_Line2.setValue(pnd_Xml_Line);                                                                                                                     //Natural: ASSIGN #XML-LINE2 := #XML-LINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Xml_Line2.contains ("&#174;") ||pnd_Xml_Line.contains ("&#xA")))                                                                            //Natural: IF #XML-LINE2 = SCAN '&#174;' OR #XML-LINE = SCAN '&#xA'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.examine(new ExamineSource(pnd_Xml_Line2), new ExamineSearch("&"), new ExamineReplace("&amp;"));                                                   //Natural: EXAMINE #XML-LINE2 FOR '&' REPLACE WITH '&amp;'
                DbsUtil.examine(new ExamineSource(pnd_Xml_Line2), new ExamineSearch("#"), new ExamineReplace(" "));                                                       //Natural: EXAMINE #XML-LINE2 FOR '#' REPLACE WITH ' '
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #XML-LINE-LENGTH 250 TO 1 STEP -1
            for (pnd_Xml_Line_Length.setValue(250); condition(pnd_Xml_Line_Length.greaterOrEqual(1)); pnd_Xml_Line_Length.nsubtract(1))
            {
                if (condition(pnd_Xml_Line2_Pnd_Xml_Line_A.getValue(pnd_Xml_Line_Length).notEquals(" ")))                                                                 //Natural: IF #XML-LINE-A ( #XML-LINE-LENGTH ) NE ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        RD1_Exit:
        if (Global.isEscape()) return;
        //*  OUT-BATCH-CNT := #BATCH-TOTAL
        pnd_Out_Area14_Pnd_Out_Batch_Cnt.setValueEdited(pnd_Batch_Total,new ReportEditMask("ZZZZZZ9"));                                                                   //Natural: MOVE EDITED #BATCH-TOTAL ( EM = ZZZZZZ9 ) TO #OUT-BATCH-CNT
        pnd_Out_Area14_Pnd_Out_Batch_Cnt.setValue(pnd_Out_Area14_Pnd_Out_Batch_Cnt, MoveOption.LeftJustified);                                                            //Natural: MOVE LEFT #OUT-BATCH-CNT TO #OUT-BATCH-CNT
        getWorkFiles().write(14, false, pnd_Out_Area14);                                                                                                                  //Natural: WRITE WORK FILE 14 #OUT-AREA14
        //*  ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-3
    }
    private void sub_Write_Work_3() throws Exception                                                                                                                      //Natural: WRITE-WORK-3
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(12, true, pnd_Xml_Line2_Pnd_Xml_Line_A.getValue(1,":",pnd_Xml_Line_Length));                                                                 //Natural: WRITE WORK FILE 12 VARIABLE #XML-LINE-A ( 1:#XML-LINE-LENGTH )
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
    }
}
