/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:24 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnoptn
************************************************************
**        * FILE NAME            : Fcpnoptn.java
**        * CLASS NAME           : Fcpnoptn
**        * INSTANCE NAME        : Fcpnoptn
************************************************************
************************************************************************
*
* PROGRAM  : FCPNOPTN
*
* SYSTEM   : CPS
* FUNCTION : THIS PROGRAM DECODES CONTRACT OPTION CODE
*
* CHANGES  :
* 09/13/07 LCW  CHANGED CNTRCT-OPTION-CDE RANGE TO INCLUDE 50S.
*
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnoptn extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpaoptn pdaFcpaoptn;
    private LdaFcploptn ldaFcploptn;
    private PdaTbldcoda pdaTbldcoda;
    private LdaFcpltbcd ldaFcpltbcd;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Idx;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcploptn = new LdaFcploptn();
        registerRecord(ldaFcploptn);
        localVariables = new DbsRecord();
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        ldaFcpltbcd = new LdaFcpltbcd();
        registerRecord(ldaFcpltbcd);

        // parameters
        parameters = new DbsRecord();
        pdaFcpaoptn = new PdaFcpaoptn(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcploptn.initializeValues();
        ldaFcpltbcd.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnoptn() throws Exception
    {
        super("Fcpnoptn");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Return_Cde().setValue(false);                                                                                                     //Natural: ASSIGN #RETURN-CDE := FALSE
        if (condition(pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Predict_Table().getBoolean()))                                                                                      //Natural: IF #PREDICT-TABLE
        {
                                                                                                                                                                          //Natural: PERFORM GET-OPTN-DESC
            sub_Get_Optn_Desc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DECODE-OPTN
            sub_Decode_Optn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OPTN-DESC
        //* ******************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECODE-OPTN
    }
    private void sub_Get_Optn_Desc() throws Exception                                                                                                                     //Natural: GET-OPTN-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                                 //Natural: ASSIGN TBLDCODA.#MODE := 'T'
        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(4);                                                                                                               //Natural: ASSIGN TBLDCODA.#TABLE-ID := 4
        pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pdaFcpaoptn.getPnd_Fcpaoptn_Cntrct_Option_Cde_X());                                                                   //Natural: ASSIGN TBLDCODA.#CODE := #FCPAOPTN.CNTRCT-OPTION-CDE-X
        DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), ldaFcpltbcd.getTbldcoda_Msg());                                              //Natural: CALLNAT 'TBLDCOD' USING TBLDCODA TBLDCODA-MSG
        if (condition(Global.isEscape())) return;
        if (condition(ldaFcpltbcd.getTbldcoda_Msg_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF ##MSG-DATA ( 1 ) = ' '
        {
            pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Return_Cde().setValue(true);                                                                                                  //Natural: ASSIGN #RETURN-CDE := TRUE
            pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Optn_Desc().setValue(pdaTbldcoda.getTbldcoda_Pnd_Target().getSubstring(6,35));                                                //Natural: MOVE SUBSTR ( TBLDCODA.#TARGET,6,35 ) TO #FCPAOPTN.#OPTN-DESC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Optn_Desc().setValue(pdaFcpaoptn.getPnd_Fcpaoptn_Cntrct_Option_Cde_X());                                                      //Natural: ASSIGN #FCPAOPTN.#OPTN-DESC := #FCPAOPTN.CNTRCT-OPTION-CDE-X
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Decode_Optn() throws Exception                                                                                                                       //Natural: DECODE-OPTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //* * #FCPAOPTN.CNTRCT-OPTION-CDE  = 1 THRU 31
        //*  LCW 09/13/07
        if (condition(pdaFcpaoptn.getPnd_Fcpaoptn_Cntrct_Option_Cde().greaterOrEqual(1) && pdaFcpaoptn.getPnd_Fcpaoptn_Cntrct_Option_Cde().lessOrEqual(59)))              //Natural: IF #FCPAOPTN.CNTRCT-OPTION-CDE = 1 THRU 59
        {
            pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Return_Cde().setValue(true);                                                                                                  //Natural: ASSIGN #RETURN-CDE := TRUE
            pnd_Idx.setValue(pdaFcpaoptn.getPnd_Fcpaoptn_Cntrct_Option_Cde());                                                                                            //Natural: ASSIGN #IDX := #FCPAOPTN.CNTRCT-OPTION-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Idx.reset();                                                                                                                                              //Natural: RESET #IDX
            ldaFcploptn.getPnd_Fcploptn_Pnd_Optn_Desc().getValue(pnd_Idx.getInt() + 1).setValue(pdaFcpaoptn.getPnd_Fcpaoptn_Cntrct_Option_Cde_X());                       //Natural: ASSIGN #FCPLOPTN.#OPTN-DESC ( #IDX ) := #FCPAOPTN.CNTRCT-OPTION-CDE-X
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Optn_Desc().setValue(ldaFcploptn.getPnd_Fcploptn_Pnd_Optn_Desc().getValue(pnd_Idx.getInt() + 1));                                 //Natural: ASSIGN #FCPAOPTN.#OPTN-DESC := #FCPLOPTN.#OPTN-DESC ( #IDX )
    }

    //
}
