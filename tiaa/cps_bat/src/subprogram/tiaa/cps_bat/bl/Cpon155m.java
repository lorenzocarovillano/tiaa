/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:59 PM
**        * FROM NATURAL SUBPROGRAM : Cpon155m
************************************************************
**        * FILE NAME            : Cpon155m.java
**        * CLASS NAME           : Cpon155m
**        * INSTANCE NAME        : Cpon155m
************************************************************
************************************************************************
****     SET UP MICR LINE FOR CHECKS
************************************************************************
* PROGRAM   : CPSN155M
* SYSTEM    : CPS
* TITLE     : SET UP MICR LINE FOR CPS/AP CHECKS FOR
*           : CONSOLIDATED PAYMENT SYSTEM
* GENERATED : 11/01/00 ABRAHAM REYHANIAN
* FUNCTION  : PRODUCE THE CHECK MICR LINE
*           : UPDATE -   NONE
* HISTORY   : 01/23/02 AR   MODIFY CITIBANK MICR LINE FORMATTING
*           : 03/26/03 TMM  MODIFY WACHOVIA ADD TO FUB LOGIC
*            : 09/13/04 KMG  ADD TRUST LOGIC. COPY FUB LOGIC
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpon155m extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Bank_Cde;
    private DbsField pnd_Check_Nbr;
    private DbsField pnd_Bank_Routing;
    private DbsField pnd_Bank_Account;
    private DbsField pnd_Micr_Line;

    private DbsGroup pnd_Micr_Line__R_Field_1;
    private DbsField pnd_Micr_Line_Pnd_Micr_Line1;
    private DbsField pnd_Micr_Line_Pnd_Micr_Line2;

    private DbsGroup pnd_Micr_Line__R_Field_2;
    private DbsField pnd_Micr_Line_Pnd_Micr_Line3;
    private DbsField pnd_Micr_Line_Pnd_Micr_Line4;
    private DbsField pnd_Return_Code;
    private DbsField pnd_Temp_Check;
    private DbsField pnd_Temp_Routing;
    private DbsField pnd_Temp_Account;
    private DbsField pnd_Line_Info;

    private DbsGroup pnd_Line_Info__R_Field_3;
    private DbsField pnd_Line_Info_Pnd_Citi_Filler1;
    private DbsField pnd_Line_Info_Pnd_Citi_Check;
    private DbsField pnd_Line_Info_Pnd_Citi_Filler2;
    private DbsField pnd_Line_Info_Pnd_Citi_Routing;
    private DbsField pnd_Line_Info_Pnd_Citi_Filler3;
    private DbsField pnd_Line_Info_Pnd_Citi_Account;

    private DbsGroup pnd_Line_Info__R_Field_4;
    private DbsField pnd_Line_Info_Pnd_Citi_Line1;
    private DbsField pnd_Line_Info_Pnd_Citi_Line2;

    private DbsGroup pnd_Line_Info__R_Field_5;
    private DbsField pnd_Line_Info_Pnd_Bony_Filler1;
    private DbsField pnd_Line_Info_Pnd_Bony_Check;
    private DbsField pnd_Line_Info_Pnd_Bony_Filler2;
    private DbsField pnd_Line_Info_Pnd_Bony_Routing;
    private DbsField pnd_Line_Info_Pnd_Bony_Filler3;
    private DbsField pnd_Line_Info_Pnd_Bony_Account;

    private DbsGroup pnd_Line_Info__R_Field_6;
    private DbsField pnd_Line_Info_Pnd_Bony_Line1;
    private DbsField pnd_Line_Info_Pnd_Bony_Line2;

    private DbsGroup pnd_Line_Info__R_Field_7;
    private DbsField pnd_Line_Info_Pnd_Fubx_Filler1;
    private DbsField pnd_Line_Info_Pnd_Fubx_Check;
    private DbsField pnd_Line_Info_Pnd_Fubx_Filler2;
    private DbsField pnd_Line_Info_Pnd_Fubx_Routing;
    private DbsField pnd_Line_Info_Pnd_Fubx_Account;

    private DbsGroup pnd_Line_Info__R_Field_8;
    private DbsField pnd_Line_Info_Pnd_Fubx_Line1;
    private DbsField pnd_Line_Info_Pnd_Fubx_Line2;

    private DbsGroup pnd_Line_Info__R_Field_9;
    private DbsField pnd_Line_Info_Pnd_Jpmct_Filler1;
    private DbsField pnd_Line_Info_Pnd_Jpmct_Check;
    private DbsField pnd_Line_Info_Pnd_Jpmct_Filler2;
    private DbsField pnd_Line_Info_Pnd_Jpmct_Routing;
    private DbsField pnd_Line_Info_Pnd_Jpmct_Account;

    private DbsGroup pnd_Line_Info__R_Field_10;
    private DbsField pnd_Line_Info_Pnd_Jpmct_Line1;
    private DbsField pnd_Line_Info_Pnd_Jpmct_Line2;
    private DbsField pnd_Testing;
    private DbsField pnd_I;
    private DbsField pnd_Dash_Line1;
    private DbsField pnd_Dash_Line2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Bank_Cde = parameters.newFieldInRecord("pnd_Bank_Cde", "#BANK-CDE", FieldType.STRING, 5);
        pnd_Bank_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Nbr = parameters.newFieldInRecord("pnd_Check_Nbr", "#CHECK-NBR", FieldType.NUMERIC, 10);
        pnd_Check_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Bank_Routing = parameters.newFieldInRecord("pnd_Bank_Routing", "#BANK-ROUTING", FieldType.STRING, 9);
        pnd_Bank_Routing.setParameterOption(ParameterOption.ByReference);
        pnd_Bank_Account = parameters.newFieldInRecord("pnd_Bank_Account", "#BANK-ACCOUNT", FieldType.STRING, 21);
        pnd_Bank_Account.setParameterOption(ParameterOption.ByReference);
        pnd_Micr_Line = parameters.newFieldInRecord("pnd_Micr_Line", "#MICR-LINE", FieldType.STRING, 100);
        pnd_Micr_Line.setParameterOption(ParameterOption.ByReference);

        pnd_Micr_Line__R_Field_1 = parameters.newGroupInRecord("pnd_Micr_Line__R_Field_1", "REDEFINE", pnd_Micr_Line);
        pnd_Micr_Line_Pnd_Micr_Line1 = pnd_Micr_Line__R_Field_1.newFieldInGroup("pnd_Micr_Line_Pnd_Micr_Line1", "#MICR-LINE1", FieldType.STRING, 51);
        pnd_Micr_Line_Pnd_Micr_Line2 = pnd_Micr_Line__R_Field_1.newFieldInGroup("pnd_Micr_Line_Pnd_Micr_Line2", "#MICR-LINE2", FieldType.STRING, 49);

        pnd_Micr_Line__R_Field_2 = parameters.newGroupInRecord("pnd_Micr_Line__R_Field_2", "REDEFINE", pnd_Micr_Line);
        pnd_Micr_Line_Pnd_Micr_Line3 = pnd_Micr_Line__R_Field_2.newFieldInGroup("pnd_Micr_Line_Pnd_Micr_Line3", "#MICR-LINE3", FieldType.STRING, 54);
        pnd_Micr_Line_Pnd_Micr_Line4 = pnd_Micr_Line__R_Field_2.newFieldInGroup("pnd_Micr_Line_Pnd_Micr_Line4", "#MICR-LINE4", FieldType.STRING, 46);
        pnd_Return_Code = parameters.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 2);
        pnd_Return_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Temp_Check = localVariables.newFieldInRecord("pnd_Temp_Check", "#TEMP-CHECK", FieldType.STRING, 10);
        pnd_Temp_Routing = localVariables.newFieldInRecord("pnd_Temp_Routing", "#TEMP-ROUTING", FieldType.STRING, 9);
        pnd_Temp_Account = localVariables.newFieldInRecord("pnd_Temp_Account", "#TEMP-ACCOUNT", FieldType.STRING, 23);
        pnd_Line_Info = localVariables.newFieldInRecord("pnd_Line_Info", "#LINE-INFO", FieldType.STRING, 72);

        pnd_Line_Info__R_Field_3 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_3", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Citi_Filler1 = pnd_Line_Info__R_Field_3.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Filler1", "#CITI-FILLER1", FieldType.STRING, 
            8);
        pnd_Line_Info_Pnd_Citi_Check = pnd_Line_Info__R_Field_3.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Check", "#CITI-CHECK", FieldType.STRING, 13);
        pnd_Line_Info_Pnd_Citi_Filler2 = pnd_Line_Info__R_Field_3.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Filler2", "#CITI-FILLER2", FieldType.STRING, 
            1);
        pnd_Line_Info_Pnd_Citi_Routing = pnd_Line_Info__R_Field_3.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Routing", "#CITI-ROUTING", FieldType.STRING, 
            11);
        pnd_Line_Info_Pnd_Citi_Filler3 = pnd_Line_Info__R_Field_3.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Filler3", "#CITI-FILLER3", FieldType.STRING, 
            3);
        pnd_Line_Info_Pnd_Citi_Account = pnd_Line_Info__R_Field_3.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Account", "#CITI-ACCOUNT", FieldType.STRING, 
            23);

        pnd_Line_Info__R_Field_4 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_4", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Citi_Line1 = pnd_Line_Info__R_Field_4.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Line1", "#CITI-LINE1", FieldType.STRING, 34);
        pnd_Line_Info_Pnd_Citi_Line2 = pnd_Line_Info__R_Field_4.newFieldInGroup("pnd_Line_Info_Pnd_Citi_Line2", "#CITI-LINE2", FieldType.STRING, 38);

        pnd_Line_Info__R_Field_5 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_5", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Bony_Filler1 = pnd_Line_Info__R_Field_5.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Filler1", "#BONY-FILLER1", FieldType.STRING, 
            9);
        pnd_Line_Info_Pnd_Bony_Check = pnd_Line_Info__R_Field_5.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Check", "#BONY-CHECK", FieldType.STRING, 12);
        pnd_Line_Info_Pnd_Bony_Filler2 = pnd_Line_Info__R_Field_5.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Filler2", "#BONY-FILLER2", FieldType.STRING, 
            1);
        pnd_Line_Info_Pnd_Bony_Routing = pnd_Line_Info__R_Field_5.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Routing", "#BONY-ROUTING", FieldType.STRING, 
            11);
        pnd_Line_Info_Pnd_Bony_Filler3 = pnd_Line_Info__R_Field_5.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Filler3", "#BONY-FILLER3", FieldType.STRING, 
            1);
        pnd_Line_Info_Pnd_Bony_Account = pnd_Line_Info__R_Field_5.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Account", "#BONY-ACCOUNT", FieldType.STRING, 
            23);

        pnd_Line_Info__R_Field_6 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_6", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Bony_Line1 = pnd_Line_Info__R_Field_6.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Line1", "#BONY-LINE1", FieldType.STRING, 34);
        pnd_Line_Info_Pnd_Bony_Line2 = pnd_Line_Info__R_Field_6.newFieldInGroup("pnd_Line_Info_Pnd_Bony_Line2", "#BONY-LINE2", FieldType.STRING, 36);

        pnd_Line_Info__R_Field_7 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_7", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Fubx_Filler1 = pnd_Line_Info__R_Field_7.newFieldInGroup("pnd_Line_Info_Pnd_Fubx_Filler1", "#FUBX-FILLER1", FieldType.STRING, 
            8);
        pnd_Line_Info_Pnd_Fubx_Check = pnd_Line_Info__R_Field_7.newFieldInGroup("pnd_Line_Info_Pnd_Fubx_Check", "#FUBX-CHECK", FieldType.STRING, 14);
        pnd_Line_Info_Pnd_Fubx_Filler2 = pnd_Line_Info__R_Field_7.newFieldInGroup("pnd_Line_Info_Pnd_Fubx_Filler2", "#FUBX-FILLER2", FieldType.STRING, 
            1);
        pnd_Line_Info_Pnd_Fubx_Routing = pnd_Line_Info__R_Field_7.newFieldInGroup("pnd_Line_Info_Pnd_Fubx_Routing", "#FUBX-ROUTING", FieldType.STRING, 
            11);
        pnd_Line_Info_Pnd_Fubx_Account = pnd_Line_Info__R_Field_7.newFieldInGroup("pnd_Line_Info_Pnd_Fubx_Account", "#FUBX-ACCOUNT", FieldType.STRING, 
            23);

        pnd_Line_Info__R_Field_8 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_8", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Fubx_Line1 = pnd_Line_Info__R_Field_8.newFieldInGroup("pnd_Line_Info_Pnd_Fubx_Line1", "#FUBX-LINE1", FieldType.STRING, 37);
        pnd_Line_Info_Pnd_Fubx_Line2 = pnd_Line_Info__R_Field_8.newFieldInGroup("pnd_Line_Info_Pnd_Fubx_Line2", "#FUBX-LINE2", FieldType.STRING, 33);

        pnd_Line_Info__R_Field_9 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_9", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Jpmct_Filler1 = pnd_Line_Info__R_Field_9.newFieldInGroup("pnd_Line_Info_Pnd_Jpmct_Filler1", "#JPMCT-FILLER1", FieldType.STRING, 
            8);
        pnd_Line_Info_Pnd_Jpmct_Check = pnd_Line_Info__R_Field_9.newFieldInGroup("pnd_Line_Info_Pnd_Jpmct_Check", "#JPMCT-CHECK", FieldType.STRING, 14);
        pnd_Line_Info_Pnd_Jpmct_Filler2 = pnd_Line_Info__R_Field_9.newFieldInGroup("pnd_Line_Info_Pnd_Jpmct_Filler2", "#JPMCT-FILLER2", FieldType.STRING, 
            1);
        pnd_Line_Info_Pnd_Jpmct_Routing = pnd_Line_Info__R_Field_9.newFieldInGroup("pnd_Line_Info_Pnd_Jpmct_Routing", "#JPMCT-ROUTING", FieldType.STRING, 
            11);
        pnd_Line_Info_Pnd_Jpmct_Account = pnd_Line_Info__R_Field_9.newFieldInGroup("pnd_Line_Info_Pnd_Jpmct_Account", "#JPMCT-ACCOUNT", FieldType.STRING, 
            23);

        pnd_Line_Info__R_Field_10 = localVariables.newGroupInRecord("pnd_Line_Info__R_Field_10", "REDEFINE", pnd_Line_Info);
        pnd_Line_Info_Pnd_Jpmct_Line1 = pnd_Line_Info__R_Field_10.newFieldInGroup("pnd_Line_Info_Pnd_Jpmct_Line1", "#JPMCT-LINE1", FieldType.STRING, 37);
        pnd_Line_Info_Pnd_Jpmct_Line2 = pnd_Line_Info__R_Field_10.newFieldInGroup("pnd_Line_Info_Pnd_Jpmct_Line2", "#JPMCT-LINE2", FieldType.STRING, 33);
        pnd_Testing = localVariables.newFieldInRecord("pnd_Testing", "#TESTING", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Dash_Line1 = localVariables.newFieldInRecord("pnd_Dash_Line1", "#DASH-LINE1", FieldType.STRING, 100);
        pnd_Dash_Line2 = localVariables.newFieldInRecord("pnd_Dash_Line2", "#DASH-LINE2", FieldType.STRING, 100);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Dash_Line1.setInitialValue("----+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0");
        pnd_Dash_Line2.setInitialValue("1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cpon155m() throws Exception
    {
        super("Cpon155m");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 22 LS = 133 ZP = ON
        //*  RESET ALL OUTPUT FIELDS
        pnd_Micr_Line.reset();                                                                                                                                            //Natural: RESET #MICR-LINE
        pnd_Return_Code.setValue("00");                                                                                                                                   //Natural: ASSIGN #RETURN-CODE = '00'
        //*  USED FOR ONLINE TESTING ONLY
        //*  IF *LEVEL = 1 THEN           /* TO MIMIC INPUT PARAMETERS
        //*    ASSIGN    #TESTING         =  TRUE
        //*    ASSIGN    #BANK-CDE        = 'BONY'
        //*    ASSIGN    #BANK-CDE        = 'FUB '
        //*    ASSIGN    #BANK-CDE        = 'CITI'
        //*    ASSIGN    #CHECK-NBR       =  9200000016
        //*    ASSIGN    #BANK-ROUTING    = '031100209'
        //*    ASSIGN    #BANK-ACCOUNT    = '38725277             '
        //*  END-IF
        pnd_Temp_Check.setValue(pnd_Check_Nbr, MoveOption.RightJustified);                                                                                                //Natural: MOVE RIGHT #CHECK-NBR TO #TEMP-CHECK
        pnd_Temp_Routing.setValue(pnd_Bank_Routing, MoveOption.LeftJustified);                                                                                            //Natural: MOVE LEFT #BANK-ROUTING TO #TEMP-ROUTING
        pnd_Temp_Account.setValue(pnd_Bank_Account, MoveOption.LeftJustified);                                                                                            //Natural: MOVE LEFT #BANK-ACCOUNT TO #TEMP-ACCOUNT
        DbsUtil.examine(new ExamineSource(pnd_Temp_Check), new ExamineSearch(" "), new ExamineReplace("0"));                                                              //Natural: EXAMINE #TEMP-CHECK FOR ' ' REPLACE WITH '0'
        DbsUtil.examine(new ExamineSource(pnd_Temp_Routing,true), new ExamineSearch(" "), new ExamineReplace("0"));                                                       //Natural: EXAMINE FULL #TEMP-ROUTING FOR ' ' REPLACE WITH '0'
        short decideConditionsMet129 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #BANK-CDE = 'CITI'
        if (condition(pnd_Bank_Cde.equals("CITI")))
        {
            decideConditionsMet129++;
            pnd_Line_Info_Pnd_Citi_Check.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "<", pnd_Temp_Check, "<"));                                             //Natural: COMPRESS '<' #TEMP-CHECK '<' TO #CITI-CHECK LEAVING NO
            pnd_Line_Info_Pnd_Citi_Routing.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ":", pnd_Temp_Routing, ":"));                                         //Natural: COMPRESS ':' #TEMP-ROUTING ':' TO #CITI-ROUTING LEAVING NO
            pnd_Line_Info_Pnd_Citi_Account.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Temp_Account, "<"));                                              //Natural: COMPRESS #TEMP-ACCOUNT '<' TO #CITI-ACCOUNT LEAVING NO
            //*      --------------------------->   /* ORIGINAL FORMATTING /* 01/23/02
            //*      MOVE EDITED #CITI-LINE1
            //*           ----+----1----+----2----+----3----+----4----+----5
            //*       (EM=XXXXXX!XXXX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!)
            //*              TO #MICR-LINE1
            //*      MOVE EDITED #CITI-LINE2
            //*            ---+----1----+----2----+----3----+----4----+----5
            //*        (EM=XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!X)
            //*              TO #MICR-LINE2
            pnd_Micr_Line_Pnd_Micr_Line1.setValueEdited(pnd_Line_Info_Pnd_Citi_Line1,new ReportEditMask("XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!"));          //Natural: MOVE EDITED #CITI-LINE1 ( EM = XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX! ) TO #MICR-LINE1
            //*           ----+----1----+----2----+----3----+----4----+----5
            pnd_Micr_Line_Pnd_Micr_Line2.setValueEdited(pnd_Line_Info_Pnd_Citi_Line2,new ReportEditMask("XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!X"));            //Natural: MOVE EDITED #CITI-LINE2 ( EM = XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!X ) TO #MICR-LINE2
            //*            ---+----1----+----2----+----3----+----4----+----5
        }                                                                                                                                                                 //Natural: WHEN #BANK-CDE = 'BONY'
        else if (condition(pnd_Bank_Cde.equals("BONY")))
        {
            decideConditionsMet129++;
            pnd_Line_Info_Pnd_Bony_Check.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "<", pnd_Temp_Check, "<"));                                             //Natural: COMPRESS '<' #TEMP-CHECK '<' TO #BONY-CHECK LEAVING NO
            pnd_Line_Info_Pnd_Bony_Routing.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ":", pnd_Temp_Routing, ":"));                                         //Natural: COMPRESS ':' #TEMP-ROUTING ':' TO #BONY-ROUTING LEAVING NO
            pnd_Line_Info_Pnd_Bony_Account.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "<", pnd_Temp_Account, "<"));                                         //Natural: COMPRESS '<' #TEMP-ACCOUNT '<' TO #BONY-ACCOUNT LEAVING NO
            pnd_Micr_Line_Pnd_Micr_Line1.setValueEdited(pnd_Line_Info_Pnd_Bony_Line1,new ReportEditMask("XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!"));          //Natural: MOVE EDITED #BONY-LINE1 ( EM = XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX! ) TO #MICR-LINE1
            //*           ----+----1----+----2----+----3----+----4----+----5
            //* TMM OIA
            pnd_Micr_Line_Pnd_Micr_Line2.setValueEdited(pnd_Line_Info_Pnd_Bony_Line2,new ReportEditMask("XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!X"));            //Natural: MOVE EDITED #BONY-LINE2 ( EM = XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!X ) TO #MICR-LINE2
            //*            ---+----1----+----2----+----3----+----4----+----5
        }                                                                                                                                                                 //Natural: WHEN #BANK-CDE = 'FUB' OR #BANK-CDE = 'WACHO'
        else if (condition(pnd_Bank_Cde.equals("FUB") || pnd_Bank_Cde.equals("WACHO")))
        {
            decideConditionsMet129++;
            pnd_Line_Info_Pnd_Fubx_Check.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "FB<", pnd_Temp_Check, "<"));                                           //Natural: COMPRESS 'FB<' #TEMP-CHECK '<' TO #FUBX-CHECK LEAVING NO
            pnd_Line_Info_Pnd_Fubx_Routing.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ":", pnd_Temp_Routing, ":"));                                         //Natural: COMPRESS ':' #TEMP-ROUTING ':' TO #FUBX-ROUTING LEAVING NO
            pnd_Line_Info_Pnd_Fubx_Account.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Temp_Account, "<"));                                              //Natural: COMPRESS #TEMP-ACCOUNT '<' TO #FUBX-ACCOUNT LEAVING NO
            //*           ----+----1----+----2----+----3----+----4----+----5
            //* * TMM 11/2003 (EM=XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XXX!XX!XX!XX!XX!XX!XX)
            //* * ADD TO FILLER WITH EDIT TO FIRST 2 POSITIONS TO SHIFT RIGHT 2
            pnd_Micr_Line_Pnd_Micr_Line3.setValueEdited(pnd_Line_Info_Pnd_Fubx_Line1,new ReportEditMask("XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XXX!XX!XX!XX!XX!XX!XX"));       //Natural: MOVE EDITED #FUBX-LINE1 ( EM = XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XXX!XX!XX!XX!XX!XX!XX ) TO #MICR-LINE3
            //* * KMG 09/13/04 START
            pnd_Micr_Line_Pnd_Micr_Line4.setValueEdited(pnd_Line_Info_Pnd_Fubx_Line2,new ReportEditMask("!!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX"));               //Natural: MOVE EDITED #FUBX-LINE2 ( EM = !!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX ) TO #MICR-LINE4
            //*            ---+----1----+----2----+----3----+----4----+----5
            //* * TMM 11/2003  (EM=!!XX!X=!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX)
            //* * TMM 11/2003  REMOVE DASH
            //*    WRITE  '='    #FUBX-LINE1 '***'
            //*    WRITE  '='    #FUBX-LINE2 '***'
            //*    WRITE  '='    #MICR-LINE1 '***'
            //*    WRITE  '='    #MICR-LINE2 '***'
        }                                                                                                                                                                 //Natural: WHEN #BANK-CDE = 'JPMCT'
        else if (condition(pnd_Bank_Cde.equals("JPMCT")))
        {
            decideConditionsMet129++;
            pnd_Line_Info_Pnd_Jpmct_Check.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "FB<", pnd_Temp_Check, "<"));                                          //Natural: COMPRESS 'FB<' #TEMP-CHECK '<' TO #JPMCT-CHECK LEAVING NO
            pnd_Line_Info_Pnd_Jpmct_Routing.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ":", pnd_Temp_Routing, ":"));                                        //Natural: COMPRESS ':' #TEMP-ROUTING ':' TO #JPMCT-ROUTING LEAVING NO
            pnd_Line_Info_Pnd_Jpmct_Account.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Temp_Account, "<"));                                             //Natural: COMPRESS #TEMP-ACCOUNT '<' TO #JPMCT-ACCOUNT LEAVING NO
            pnd_Micr_Line_Pnd_Micr_Line3.setValueEdited(pnd_Line_Info_Pnd_Jpmct_Line1,new ReportEditMask("XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XXX!XX!XX!XX!XX!XX!XX"));      //Natural: MOVE EDITED #JPMCT-LINE1 ( EM = XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XXX!XX!XX!XX!XX!XX!XX ) TO #MICR-LINE3
            //* * KMG 09/13/04 END
            pnd_Micr_Line_Pnd_Micr_Line4.setValueEdited(pnd_Line_Info_Pnd_Jpmct_Line2,new ReportEditMask("!!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX"));              //Natural: MOVE EDITED #JPMCT-LINE2 ( EM = !!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX!XX ) TO #MICR-LINE4
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Return_Code.setValue("99");                                                                                                                               //Natural: ASSIGN #RETURN-CODE = '99'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ------  TO MATCH WITH THE OLD WAY MICR LINE WAS PRODUCED
        DbsUtil.examine(new ExamineSource(pnd_Micr_Line), new ExamineSearch("!  !  !  !  !  !  !  !  !  !  !  !  !"), new ExamineReplace("!"));                           //Natural: EXAMINE #MICR-LINE FOR '!  !  !  !  !  !  !  !  !  !  !  !  !' REPLACE WITH '!'
        DbsUtil.examine(new ExamineSource(pnd_Micr_Line), new ExamineSearch("!  !  !  !  !  !  !  !  !  !  !  !"), new ExamineReplace("!"));                              //Natural: EXAMINE #MICR-LINE FOR '!  !  !  !  !  !  !  !  !  !  !  !' REPLACE WITH '!'
        DbsUtil.examine(new ExamineSource(pnd_Micr_Line), new ExamineSearch("!  !  !  !  !  !  !  !  !  !  !"), new ExamineReplace("!"));                                 //Natural: EXAMINE #MICR-LINE FOR '!  !  !  !  !  !  !  !  !  !  !' REPLACE WITH '!'
        DbsUtil.examine(new ExamineSource(pnd_Micr_Line), new ExamineSearch("!  !  !  !  !  !  !  !  !  !"), new ExamineReplace("!"));                                    //Natural: EXAMINE #MICR-LINE FOR '!  !  !  !  !  !  !  !  !  !' REPLACE WITH '!'
        DbsUtil.examine(new ExamineSource(pnd_Micr_Line), new ExamineSearch("!  !  !  !  !  !  !  !  !"), new ExamineReplace("!"));                                       //Natural: EXAMINE #MICR-LINE FOR '!  !  !  !  !  !  !  !  !' REPLACE WITH '!'
        DbsUtil.examine(new ExamineSource(pnd_Micr_Line), new ExamineSearch("!  !  !  !  !  !  !  !"), new ExamineReplace("!"));                                          //Natural: EXAMINE #MICR-LINE FOR '!  !  !  !  !  !  !  !' REPLACE WITH '!'
        if (condition(pnd_Testing.getBoolean()))                                                                                                                          //Natural: IF #TESTING THEN
        {
            getReports().write(0, "=",pnd_Bank_Cde);                                                                                                                      //Natural: WRITE '=' #BANK-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Check_Nbr);                                                                                                                     //Natural: WRITE '=' #CHECK-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Bank_Routing);                                                                                                                  //Natural: WRITE '=' #BANK-ROUTING
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Bank_Account);                                                                                                                  //Natural: WRITE '=' #BANK-ACCOUNT
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Dash_Line1);                                                                                                                        //Natural: WRITE #DASH-LINE1
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Dash_Line2);                                                                                                                        //Natural: WRITE #DASH-LINE2
            if (Global.isEscape()) return;
            getReports().write(0, new ColumnSpacing(2),pnd_Micr_Line);                                                                                                    //Natural: WRITE 2X #MICR-LINE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Return_Code);                                                                                                                   //Natural: WRITE '=' #RETURN-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=22 LS=133 ZP=ON");
    }
}
