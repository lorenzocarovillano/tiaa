/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:21:06 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn127
************************************************************
**        * FILE NAME            : Fcpn127.java
**        * CLASS NAME           : Fcpn127
**        * INSTANCE NAME        : Fcpn127
************************************************************
************************************************************************
* SUBPROGRAM: FCPN127
* SYSTEM    : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE     : MIT  INTERFACE
* GENERATED : 12/05/94
* FUNCTION  : RETRIEVE MIT INFORMATION FROM THE MIT INDEX MASTER FILE.
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* ______________________________________________
* 1996/10/08 LEON SILBERSTEIN:
*            REMOVE DIRECT READ TO MIT FILE AND REPLACE WITH CALLNAT
*            TO "CWFN6300". (IN EFFECT A COMPLETE RE-WRITE).
*
* 1997/11/18 F.ORTIZ
*            GET ORGNL-UNIT-CDE FROM CWF.
* 2018/08/05 ARIVU
*            RE-COMPILED FOR FCPA124
*
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn127 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa124 pdaFcpa124;
    private PdaFcpa127 pdaFcpa127;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfa6300 pdaCwfa6300;

    // Local Variables
    public DbsRecord localVariables;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfa6300 = new PdaCwfa6300(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa124 = new PdaFcpa124(parameters);
        pdaFcpa127 = new PdaFcpa127(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn127() throws Exception
    {
        super("Fcpn127");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaCwfa6300.getCwfa6300_Rqst_Log_Dte_Tme().setValue(pdaFcpa124.getPnd_Mit_Read_Pda_Rqst_Log_Dte_Tme());                                                           //Natural: ASSIGN CWFA6300.RQST-LOG-DTE-TME := #MIT-READ-PDA.RQST-LOG-DTE-TME
        pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code().setValue(false);                                                                                            //Natural: ASSIGN #OK-RETURN-CODE := FALSE
        DbsUtil.callnat(Cwfn6300.class , getCurrentProcessState(), pdaCwfa6300.getCwfa6300(), pdaCwfpda_M.getMsg_Info_Sub());                                             //Natural: CALLNAT 'CWFN6300' CWFA6300 MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        //*  SUCCESSFUL
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(getZero())))                                                                                    //Natural: IF MSG-INFO-SUB.##MSG-NR EQ 0
        {
            pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Return_Message().reset();                                                                                                //Natural: RESET #RETURN-MESSAGE
            pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code().setValue(true);                                                                                         //Natural: ASSIGN #OK-RETURN-CODE := TRUE
            pdaFcpa124.getPnd_Mit_Read_Pda_Admin_Unit_Cde().setValue(pdaCwfa6300.getCwfa6300_Admin_Unit_Cde());                                                           //Natural: ASSIGN #MIT-READ-PDA.ADMIN-UNIT-CDE := CWFA6300.ADMIN-UNIT-CDE
            pdaFcpa124.getPnd_Mit_Read_Pda_Rqst_Orgn_Cde().setValue(pdaCwfa6300.getCwfa6300_Rqst_Orgn_Cde());                                                             //Natural: ASSIGN #MIT-READ-PDA.RQST-ORGN-CDE := CWFA6300.RQST-ORGN-CDE
            pdaFcpa124.getPnd_Mit_Read_Pda_Orgnl_Unit_Cde().setValue(pdaCwfa6300.getCwfa6300_Orgnl_Unit_Cde());                                                           //Natural: ASSIGN #MIT-READ-PDA.ORGNL-UNIT-CDE := CWFA6300.ORGNL-UNIT-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Return_Message().setValue(DbsUtil.compress("Return code from MIT:", pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr()));      //Natural: COMPRESS 'Return code from MIT:' MSG-INFO-SUB.##MSG-NR INTO #RETURN-MESSAGE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
