/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:47 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn874b
************************************************************
**        * FILE NAME            : Fcpn874b.java
**        * CLASS NAME           : Fcpn874b
**        * INSTANCE NAME        : Fcpn874b
************************************************************
************************************************************************
* SUBPROGRAM : FCPN874B
* SYSTEM     : CPS
* TITLE      : NEW ANNUITIZATION
* FUNCTION   : "NZ" ANNUITANT STATEMENTS - "Body".
*  12/15/99  : NEW LOCAL
*  07/25/00  : A. YOUNG - REVISED TO PASS DA NUMBERS TO FCPN874H FOR
*            :            PRINTING ON STATEMENTS.
*            :          - REVISED CONTRACTUAL AMT. PROCESSING BY
*            :            CREATING 40 OCCURS. TO STORE ALL ORIGINAL
*            :            EXT.INV-ACCT-SETTL-AMT VALUES.
*            :          - REVISED PROCESSING TO POPULATE #DPI-DCI-IND
*            :            AND #PYMNT-DED-IND.
* 08/01/00   : MCGEE      ADDED PYMNT-IVC-AMT TO FCPAEXT TO HOLD
*            :            IVC AMOUNTS ACROSS MULTIPLE INSTALLMENTS
* 03/15/01   : RCC      - PRINT MESSAGE FOR IVC CHECK
* 04/17/03   : ROXAN    - FCPAEXT WAS EXPANDED
*            :          - TPA EGTRRA MESSAGE FOR IVC
* 01/05/2006 : R. LANDRUM
*              POS-PAY, PAYEE MATCH. ADD STOP POINT FOR CHECK NUMBER ON
*              STATEMENT BODY & CHECK FACE, 10 DIGIT MICR LINE.
* 08/11/2006 : R. LANDRUM
*              DO NOT PRINT CHECK NUMBER ON NZ "OVERFLOW" PAGES
* 07/02/2009 - J. OSTEEN ADD ROTH INFO TO CHECK STATEMENT
*
* 02/29/2016 - RAHUL DAS - WRITE CHECK NUMBER IF THE SETTLEMENT PAGE
*              CONTINUES TO GO BEYOND 1ST PAGE AND CHECK NUMBER IS NOT
*              PRINTED SO FAR - TAG: RAHUL
* 4/2017     - JJG - PIN EXPANSION RESTOW
* ************************* NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR THE PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn874b extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa873 pdaFcpa873;
    private PdaFcpaext pdaFcpaext;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa874a pdaFcpa874a;
    private PdaFcpa874h pdaFcpa874h;
    private PdaFcpa803l pdaFcpa803l;
    private PdaFcpabar pdaFcpabar;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl803g ldaFcpl803g;
    private PdaFcpa874c pdaFcpa874c;
    private LdaFcpl874c ldaFcpl874c;
    private LdaFcpl876b ldaFcpl876b;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_can_Pymnt;

    private DbsGroup can_Pymnt_Inv_Acct_Part_2;
    private DbsField can_Pymnt_Inv_Acct_Can_Tax_Amt;
    private DbsField can_Pymnt_Cntrct_Can_Tax_Amt;
    private DbsField pnd_Chk_Printed;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Pymnt_S__R_Field_1;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Temp_Amt;
    private DbsField pnd_Ws_Pnd_Amt_Alpha;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_K;
    private DbsField pnd_Ws_Pnd_Comp_Idx;
    private DbsField pnd_Ws_Pnd_Col;
    private DbsField pnd_Ws_Pnd_Ded_Idx;

    private DbsGroup pnd_Ws_Pnd_Ded_Flags;
    private DbsField pnd_Ws_Pnd_Ded_Sum;
    private DbsField pnd_Ws_Pnd_Ded_Fed;
    private DbsField pnd_Ws_Pnd_Ded_State;
    private DbsField pnd_Ws_Pnd_Ded_Local;
    private DbsField pnd_Ws_Pnd_Ded_Can;
    private DbsField pnd_Ws_Pnd_Null;

    private DbsGroup pnd_Chk_Fields;
    private DbsField pnd_Chk_Fields_Rtb;
    private DbsField pnd_Chk_Fields_Ivc_From_Rtb_Rollover;
    private DbsField pnd_Chk_Fields_Pnd_Cntrct_Amt;
    private DbsField pnd_Chk_Fields_Pnd_Filler;

    private DbsGroup pnd_Ws_Tpa_Egtrra;
    private DbsField pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc;
    private DbsField pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc;
    private DbsField pnd_Ws_Tpa_Egtrra_Pnd_Ws_From;
    private DbsField pnd_Ws_Tpa_Egtrra_Pnd_Ws_From_2;
    private DbsField pnd_Ws_Tpa_Egtrra_Pnd_Amt_Alpha_2;
    private DbsField pnd_Ws_Tpa_Egtrra_Pnd_X;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl803g = new LdaFcpl803g();
        registerRecord(ldaFcpl803g);
        pdaFcpa874c = new PdaFcpa874c(localVariables);
        ldaFcpl874c = new LdaFcpl874c();
        registerRecord(ldaFcpl874c);
        ldaFcpl876b = new LdaFcpl876b();
        registerRecord(ldaFcpl876b);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa873 = new PdaFcpa873(parameters);
        pdaFcpaext = new PdaFcpaext(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa874a = new PdaFcpa874a(parameters);
        pdaFcpa874h = new PdaFcpa874h(parameters);
        pdaFcpa803l = new PdaFcpa803l(parameters);
        pdaFcpabar = new PdaFcpabar(parameters);
        pdaFcpa110 = new PdaFcpa110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_can_Pymnt = new DataAccessProgramView(new NameInfo("vw_can_Pymnt", "CAN-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));

        can_Pymnt_Inv_Acct_Part_2 = vw_can_Pymnt.getRecord().newGroupInGroup("can_Pymnt_Inv_Acct_Part_2", "INV-ACCT-PART-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT_PART_2");
        can_Pymnt_Inv_Acct_Can_Tax_Amt = can_Pymnt_Inv_Acct_Part_2.newFieldArrayInGroup("can_Pymnt_Inv_Acct_Can_Tax_Amt", "INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CAN_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT_PART_2");
        can_Pymnt_Cntrct_Can_Tax_Amt = vw_can_Pymnt.getRecord().newFieldInGroup("can_Pymnt_Cntrct_Can_Tax_Amt", "CNTRCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_CAN_TAX_AMT");
        registerRecord(vw_can_Pymnt);

        pnd_Chk_Printed = localVariables.newFieldInRecord("pnd_Chk_Printed", "#CHK-PRINTED", FieldType.BOOLEAN, 1);

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Num = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);

        pnd_Pymnt_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_1", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_1.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 27);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Temp_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Amt", "#TEMP-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Amt_Alpha = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Alpha", "#AMT-ALPHA", FieldType.STRING, 15);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Comp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Comp_Idx", "#COMP-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Col = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Col", "#COL", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ded_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ded_Idx", "#DED-IDX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws_Pnd_Ded_Flags = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Ded_Flags", "#DED-FLAGS");
        pnd_Ws_Pnd_Ded_Sum = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_Sum", "#DED-SUM", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ded_Fed = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_Fed", "#DED-FED", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ded_State = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_State", "#DED-STATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ded_Local = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_Local", "#DED-LOCAL", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ded_Can = pnd_Ws_Pnd_Ded_Flags.newFieldInGroup("pnd_Ws_Pnd_Ded_Can", "#DED-CAN", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Null = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Null", "#NULL", FieldType.STRING, 1);

        pnd_Chk_Fields = localVariables.newGroupInRecord("pnd_Chk_Fields", "#CHK-FIELDS");
        pnd_Chk_Fields_Rtb = pnd_Chk_Fields.newFieldInGroup("pnd_Chk_Fields_Rtb", "RTB", FieldType.BOOLEAN, 1);
        pnd_Chk_Fields_Ivc_From_Rtb_Rollover = pnd_Chk_Fields.newFieldInGroup("pnd_Chk_Fields_Ivc_From_Rtb_Rollover", "IVC-FROM-RTB-ROLLOVER", FieldType.BOOLEAN, 
            1);
        pnd_Chk_Fields_Pnd_Cntrct_Amt = pnd_Chk_Fields.newFieldArrayInGroup("pnd_Chk_Fields_Pnd_Cntrct_Amt", "#CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 40));
        pnd_Chk_Fields_Pnd_Filler = pnd_Chk_Fields.newFieldInGroup("pnd_Chk_Fields_Pnd_Filler", "#FILLER", FieldType.STRING, 35);

        pnd_Ws_Tpa_Egtrra = localVariables.newGroupInRecord("pnd_Ws_Tpa_Egtrra", "#WS-TPA-EGTRRA");
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc = pnd_Ws_Tpa_Egtrra.newFieldInGroup("pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc", "#WS-TIAA-IVC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc = pnd_Ws_Tpa_Egtrra.newFieldInGroup("pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc", "#WS-CREF-IVC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_From = pnd_Ws_Tpa_Egtrra.newFieldInGroup("pnd_Ws_Tpa_Egtrra_Pnd_Ws_From", "#WS-FROM", FieldType.STRING, 10);
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_From_2 = pnd_Ws_Tpa_Egtrra.newFieldInGroup("pnd_Ws_Tpa_Egtrra_Pnd_Ws_From_2", "#WS-FROM-2", FieldType.STRING, 10);
        pnd_Ws_Tpa_Egtrra_Pnd_Amt_Alpha_2 = pnd_Ws_Tpa_Egtrra.newFieldInGroup("pnd_Ws_Tpa_Egtrra_Pnd_Amt_Alpha_2", "#AMT-ALPHA-2", FieldType.STRING, 15);
        pnd_Ws_Tpa_Egtrra_Pnd_X = pnd_Ws_Tpa_Egtrra.newFieldInGroup("pnd_Ws_Tpa_Egtrra_Pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Ws_Pymnt_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Nbr_N10", "#WS-PYMNT-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Ws_Pymnt_Check_Nbr_N10);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", 
            "#WS-CHECK-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", 
            "#WS-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_can_Pymnt.reset();

        ldaFcpl803g.initializeValues();
        ldaFcpl874c.initializeValues();
        ldaFcpl876b.initializeValues();

        localVariables.reset();
        pnd_Chk_Printed.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn874b() throws Exception
    {
        super("Fcpn874b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 45T 'ANNUITANT STATEMENT DETAIL CONTROL REPORT' 120T 'REPORT: RPT1' //
        //*  CHECK FOR RTB/DED-IND/DPI-DCI     /* 12/15/99
        short decideConditionsMet852 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EXT.CNTRCT-PYMNT-TYPE-IND = 'N' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'X'
        if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("N") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("X")))
        {
            decideConditionsMet852++;
            pnd_Chk_Fields_Rtb.setValue(true);                                                                                                                            //Natural: ASSIGN RTB := TRUE
        }                                                                                                                                                                 //Natural: WHEN EXT.CNTRCT-PYMNT-TYPE-IND = 'I' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'R'
        else if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("I") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("R")))
        {
            decideConditionsMet852++;
            pnd_Chk_Fields_Ivc_From_Rtb_Rollover.setValue(true);                                                                                                          //Natural: ASSIGN IVC-FROM-RTB-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Chk_Fields_Rtb.getBoolean()))                                                                                                                   //Natural: IF RTB
        {
            pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(1).setValue("RTB");                                                                          //Natural: ASSIGN #DED-PYMNT-TABLE ( 1 ) := 'RTB'
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "**************","=",pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind(),NEWLINE,"=",pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind());                    //Natural: WRITE '**************' '=' EXT.CNTRCT-PYMNT-TYPE-IND / '=' EXT.CNTRCT-STTLMNT-TYPE-IND
        if (Global.isEscape()) return;
        if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("C") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("C")))                                  //Natural: IF EXT.CNTRCT-PYMNT-TYPE-IND = 'C' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'C'
        {
            pnd_Pymnt_S_Cntrct_Ppcn_Nbr.setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                                                    //Natural: ASSIGN #PYMNT-S.CNTRCT-PPCN-NBR := EXT.CNTRCT-PPCN-NBR
            pnd_Pymnt_S_Cntrct_Invrse_Dte.setValue(pdaFcpaext.getExt_Cntrct_Invrse_Dte());                                                                                //Natural: ASSIGN #PYMNT-S.CNTRCT-INVRSE-DTE := EXT.CNTRCT-INVRSE-DTE
            pnd_Pymnt_S_Cntrct_Orgn_Cde.setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                                    //Natural: ASSIGN #PYMNT-S.CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
            pnd_Pymnt_S_Pymnt_Prcss_Seq_Num.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Num());                                                                            //Natural: ASSIGN #PYMNT-S.PYMNT-PRCSS-SEQ-NUM := EXT.PYMNT-PRCSS-SEQ-NUM
            vw_can_Pymnt.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) CAN-PYMNT WITH PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE
            (
            "READ01",
            new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
            new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
            1
            );
            READ01:
            while (condition(vw_can_Pymnt.readNextRow("READ01")))
            {
                getReports().write(0, "*********** CANADIAN CONVERTED *************");                                                                                    //Natural: WRITE '*********** CANADIAN CONVERTED *************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(1,":",40),"=",can_Pymnt_Cntrct_Can_Tax_Amt);                                            //Natural: WRITE '=' INV-ACCT-CAN-TAX-AMT ( 1:40 ) '=' CNTRCT-CAN-TAX-AMT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 EXT.C-INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
            //*  07-25-2000
            if (condition(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(getZero())))                                                         //Natural: IF EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) NE 0
            {
                //*      #PYMNT-DED-IND := TRUE                               /* 07-25-2000
                if (condition(pdaFcpaext.getExt_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                                 //Natural: IF EXT.PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
                {
                    pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(2).setValue("  N");                                                                  //Natural: ASSIGN #DED-PYMNT-TABLE ( 2 ) := '  N'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(2).setValue("  F");                                                                  //Natural: ASSIGN #DED-PYMNT-TABLE ( 2 ) := '  F'
                }                                                                                                                                                         //Natural: END-IF
                //*  07-25-2000
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(getZero())))                                                        //Natural: IF EXT.INV-ACCT-STATE-TAX-AMT ( #I ) NE 0
            {
                //*      #PYMNT-DED-IND := TRUE
                pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(3).setValue("  S");                                                                      //Natural: ASSIGN #DED-PYMNT-TABLE ( 3 ) := '  S'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(getZero())))                                                        //Natural: IF EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) NE 0
            {
                //*      #PYMNT-DED-IND := TRUE
                pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(4).setValue("  L");                                                                      //Natural: ASSIGN #DED-PYMNT-TABLE ( 4 ) := '  L'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(pnd_Ws_Pnd_I).notEquals(getZero())))                                                                    //Natural: IF CAN-PYMNT.INV-ACCT-CAN-TAX-AMT ( #I ) NE 0
            {
                //*      #PYMNT-DED-IND := TRUE
                pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(5).setValue("  C");                                                                      //Natural: ASSIGN #DED-PYMNT-TABLE ( 5 ) := '  C'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  07-25-2000 : REVISED CONTRACTUAL AMT. PROCESSING.
        //*  ADD EXT.INV-ACCT-SETTL-AMT  (1:C-INV-ACCT) TO #CNTRCT-AMT
        pnd_Chk_Fields_Pnd_Cntrct_Amt.getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).setValue(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct())); //Natural: ASSIGN #CNTRCT-AMT ( 1:C-INV-ACCT ) := EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT )
        pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).nadd(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,":",                 //Natural: ASSIGN EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) := EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) + EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT )
            pdaFcpaext.getExt_C_Inv_Acct()));
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().getBoolean()))                                                                                            //Natural: IF #NEW-PYMNT
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(false);                                                                                                    //Natural: ASSIGN #NEW-PYMNT := FALSE
            //*  07-25-2000
            pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().reset();                                                                                                  //Natural: RESET #DPI-DCI-IND #PYMNT-DED-IND
            pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().reset();
            //*  07-25-2000
            if (condition(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).notEquals(getZero()) || pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(1, //Natural: IF EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT ) NE 0 OR EXT.INV-ACCT-DCI-AMT ( 1:C-INV-ACCT ) NE 0
                ":",pdaFcpaext.getExt_C_Inv_Acct()).notEquals(getZero())))
            {
                pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().setValue(true);                                                                                       //Natural: ASSIGN #DPI-DCI-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  07-25-2000
            if (condition((pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).notEquals(getZero())) || (pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).notEquals(getZero()))  //Natural: IF ( EXT.INV-ACCT-FDRL-TAX-AMT ( 1:C-INV-ACCT ) NE 0 ) OR ( EXT.INV-ACCT-STATE-TAX-AMT ( 1:C-INV-ACCT ) NE 0 ) OR ( EXT.INV-ACCT-LOCAL-TAX-AMT ( 1:C-INV-ACCT ) NE 0 ) OR ( CAN-PYMNT.INV-ACCT-CAN-TAX-AMT ( 1:C-INV-ACCT ) NE 0 )
                || (pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).notEquals(getZero())) || (can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(1,
                ":",pdaFcpaext.getExt_C_Inv_Acct()).notEquals(getZero()))))
            {
                pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().setValue(true);                                                                                     //Natural: ASSIGN #PYMNT-DED-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().resetInitial();                                                                                                  //Natural: RESET INITIAL #FCPA803.#CURRENT-PAGE #DPI-COL #DED-COL #NET-COL #GRAND-TOTALS
            pdaFcpa803.getPnd_Fcpa803_Pnd_Dpi_Col().resetInitial();
            pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col().resetInitial();
            pdaFcpa803.getPnd_Fcpa803_Pnd_Net_Col().resetInitial();
            pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Totals().resetInitial();
            pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col().setValue(5);                                                                                                          //Natural: ASSIGN #DED-COL := #DPI-COL := #NET-COL := 5
            pdaFcpa803.getPnd_Fcpa803_Pnd_Dpi_Col().setValue(5);
            pdaFcpa803.getPnd_Fcpa803_Pnd_Net_Col().setValue(5);
            if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean()))                                                                              //Natural: IF #DPI-DCI-IND
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col().nadd(1);                                                                                                          //Natural: ADD 1 TO #DED-COL
                pdaFcpa803.getPnd_Fcpa803_Pnd_Net_Col().nadd(1);                                                                                                          //Natural: ADD 1 TO #NET-COL
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean()))                                                                            //Natural: IF #PYMNT-DED-IND
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Net_Col().nadd(1);                                                                                                          //Natural: ADD 1 TO #NET-COL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Settl_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                      //Natural: ADD EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-SETTL-AMT
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean()))                                                                                  //Natural: IF #DPI-DCI-IND
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Dpi_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                      //Natural: ADD EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-DPI-AMT
            pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Dpi_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                      //Natural: ADD EXT.INV-ACCT-DCI-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-DPI-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Ded_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                     //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-DED-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Ded_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                    //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-DED-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Ded_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                    //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-DED-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Ded_Amt().nadd(can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                //Natural: ADD CAN-PYMNT.INV-ACCT-CAN-TAX-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-DED-AMT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Net_Amt().nadd(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                    //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( 1:C-INV-ACCT ) TO #FCPA803.#GRAND-NET-AMT
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO C-INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Ded_Flags.reset();                                                                                                                                 //Natural: RESET #DED-FLAGS
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpaext.getExt_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I));                                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := INV-ACCT-CDE-N ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpaext.getExt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := INV-ACCT-VALUAT-PERIOD ( #I )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(pdaFcpaext.getExt_Funds_On_Page().getValue(pnd_Ws_Pnd_I))))                              //Natural: IF #FCPA803.#CURRENT-PAGE NE EXT.FUNDS-ON-PAGE ( #I )
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().setValue(true);                                                                                               //Natural: ASSIGN #NEWPAGE-IND := TRUE
                //*  NOT START OF PYMNT
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().notEquals(getZero())))                                                                         //Natural: IF #FCPA803.#CURRENT-PAGE NE 0
                {
                                                                                                                                                                          //Natural: PERFORM BOTTOM-OF-PAGE
                    sub_Bottom_Of_Page();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().setValue(pdaFcpaext.getExt_Funds_On_Page().getValue(pnd_Ws_Pnd_I));                                          //Natural: ASSIGN #FCPA803.#CURRENT-PAGE := EXT.FUNDS-ON-PAGE ( #I )
                pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().divide(2).multiply(2));                     //Natural: COMPUTE #J = #FCPA803.#CURRENT-PAGE / 2 * 2
                //*  EVEN PAGE
                if (condition(pnd_Ws_Pnd_J.equals(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page())))                                                                         //Natural: IF #J = #FCPA803.#CURRENT-PAGE
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(false);                                                                                             //Natural: ASSIGN #ODD-PAGE := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(true);                                                                                              //Natural: ASSIGN #ODD-PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM GEN-BARCODE
                    sub_Gen_Barcode();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().getBoolean()))                                                                                      //Natural: IF #NEWPAGE-IND
            {
                DbsUtil.callnat(Fcpn803x.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pnd_Ws_Pnd_Null);                                                 //Natural: CALLNAT 'FCPN803X' USING #FCPA803 #NULL
                if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM GEN-PYMNT-HEADERS
                sub_Gen_Pymnt_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GEN-CNTRCT-HEADERS
                sub_Gen_Cntrct_Headers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Newpage_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #NEWPAGE-IND := FALSE
            if (condition(pnd_Chk_Fields_Rtb.getBoolean() || pnd_Chk_Fields_Ivc_From_Rtb_Rollover.getBoolean()))                                                          //Natural: IF RTB OR IVC-FROM-RTB-ROLLOVER
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-RTB
                sub_Display_Rtb();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PERIODIC
                sub_Display_Periodic();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(!(pnd_Ws_Pnd_Ded_Sum.getBoolean()))) {break;}                                                                                               //Natural: WHILE #DED-SUM
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 3");                                                                                                //Natural: ASSIGN #OUTPUT-REC := ' 3'
                                                                                                                                                                          //Natural: PERFORM PRINT-DED-DETAIL
                sub_Print_Ded_Detail();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* IF #FCPA803.#RECORD-IN-PYMNT = #NZ-CHECK-FIELDS.#PYMNT-RECORDS    /*
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().equals(pdaFcpaext.getExt_Pymnt_Record())))                                                          //Natural: IF #FCPA803.#RECORD-IN-PYMNT = EXT.PYMNT-RECORD
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().setValue(true);                                                                                                  //Natural: ASSIGN #FCPA803.#END-OF-PYMNT := TRUE
                                                                                                                                                                          //Natural: PERFORM BOTTOM-OF-PAGE
            sub_Bottom_Of_Page();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean() && ! (pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean())))                          //Natural: IF #FCPA803.#CHECK AND NOT #FCPA803.#CHECK-TO-ANNT
            {
                                                                                                                                                                          //Natural: PERFORM LETTER-CHECK
                sub_Letter_Check();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).nsubtract(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,                //Natural: ASSIGN EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) := EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) - EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT )
            ":",pdaFcpaext.getExt_C_Inv_Acct()));
        //*  07-25-2000
        pnd_Chk_Fields_Pnd_Cntrct_Amt.getValue("*").reset();                                                                                                              //Natural: RESET #CNTRCT-AMT ( * )
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-RTB
        //* ****************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-PERIODIC
        //* *********************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-NET
        //* **************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DED-SUM
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DED-DETAIL
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DPI-DCI
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BODY
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMT-$
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMT
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BOTTOM-OF-PAGE
        //* *******************************
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PART
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-PYMNT-HEADERS
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-CNTRCT-HEADERS
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GRAND-TOTALS
        //* ***********************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LETTER-CHECK
        //* *********************** RL END PAYEE MATCH ***************************
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-BARCODE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BARCODE-PROCESSING
        //*  ---------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IVC-AMT
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Display_Rtb() throws Exception                                                                                                                       //Natural: DISPLAY-RTB
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("05Due");                                                                                                     //Natural: ASSIGN #OUTPUT-REC := '05Due'
        pnd_Ws_Pnd_Col.setValue(2);                                                                                                                                       //Natural: ASSIGN #COL := 2
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                     //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#STMNT-LINE-1
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                           //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #I ) ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        if (condition(pnd_Chk_Fields_Ivc_From_Rtb_Rollover.getBoolean()))                                                                                                 //Natural: IF IVC-FROM-RTB-ROLLOVER
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("*");                                                                            //Natural: ASSIGN #OUTPUT-COL ( #COL ) := '*'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("RTB");                                                                          //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'RTB'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean()))                                                                                  //Natural: IF #DPI-DCI-IND
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DPI-DCI
            sub_Print_Dpi_Dci();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DED-SUM
        sub_Print_Ded_Sum();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-NET
        sub_Print_Net();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        pnd_Ws_Pnd_Col.setValue(1);                                                                                                                                       //Natural: ASSIGN #COL := 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValueEdited(pdaFcpaext.getExt_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YYYY"));      //Natural: MOVE EDITED EXT.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YYYY ) TO #OUTPUT-COL ( #COL )
        if (condition(! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean())))                                                                                      //Natural: IF NOT #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                 //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#STMNT-LINE-2
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Pnd_Ded_Sum.getBoolean()))                                                                                                                   //Natural: IF #DED-SUM
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '+3'
                                                                                                                                                                          //Natural: PERFORM PRINT-DED-DETAIL
            sub_Print_Ded_Detail();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Periodic() throws Exception                                                                                                                  //Natural: DISPLAY-PERIODIC
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("05Due");                                                                                                     //Natural: ASSIGN #OUTPUT-REC := '05Due'
        pnd_Ws_Pnd_Col.setValue(2);                                                                                                                                       //Natural: ASSIGN #COL := 2
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValueEdited(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));          //Natural: MOVE EDITED EXT.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #OUTPUT-COL ( #COL )
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                           //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #I ) ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean())))                                                                                      //Natural: IF NOT #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_9());                                //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#VALUAT-DESC-9
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean()))                                                                                  //Natural: IF #DPI-DCI-IND
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DPI-DCI
            sub_Print_Dpi_Dci();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DED-SUM
        sub_Print_Ded_Sum();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-NET
        sub_Print_Net();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        pnd_Ws_Pnd_Col.setValue(1);                                                                                                                                       //Natural: ASSIGN #COL := 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValueEdited(pdaFcpaext.getExt_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YYYY"));      //Natural: MOVE EDITED EXT.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YYYY ) TO #OUTPUT-COL ( #COL )
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                     //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#STMNT-LINE-1
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+3'
        pnd_Ws_Pnd_Col.setValue(3);                                                                                                                                       //Natural: ASSIGN #COL := 3
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            //* MOVE EDITED NZ-EXT.INV-ACCT-CNTRCT-AMT(#I)(EM=-Z,ZZZ,ZZ9.99)      /*
            //*  MOVE EDITED #CNTRCT-AMT (EM=-Z,ZZZ,ZZ9.99)               /* 07-25-2000
            //*  07-25-2000
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Chk_Fields_Pnd_Cntrct_Amt.getValue(pnd_Ws_Pnd_I),new ReportEditMask("-Z,ZZZ,ZZ9.99"));                                //Natural: MOVE EDITED #CNTRCT-AMT ( #I ) ( EM = -Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
            sub_Convert_Amt();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Contractual");                                                                  //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Contractual'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Pnd_I),new ReportEditMask("ZZZ,ZZ9.999@"));                         //Natural: MOVE EDITED EXT.INV-ACCT-UNIT-QTY ( #I ) ( EM = ZZZ,ZZ9.999@ ) TO #AMT-ALPHA
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pnd_Ws_Pnd_Amt_Alpha, MoveOption.RightJustified);                                //Natural: MOVE RIGHT #AMT-ALPHA TO #OUTPUT-COL ( #COL )
            setValueToSubstring(" ",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col),15,1);                                                            //Natural: MOVE ' ' TO SUBSTR ( #OUTPUT-COL ( #COL ) ,15,1 )
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Units");                                                                        //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Units'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Ded_Sum.getBoolean()))                                                                                                                   //Natural: IF #DED-SUM
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DED-DETAIL
            sub_Print_Ded_Detail();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Ws_Pnd_Ded_Sum.getBoolean())))                                                                                                               //Natural: IF NOT #DED-SUM
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        pnd_Ws_Pnd_Col.setValue(2);                                                                                                                                       //Natural: ASSIGN #COL := 2
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                     //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #FUND-PDA.#STMNT-LINE-2
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+3'
        pnd_Ws_Pnd_Col.setValue(3);                                                                                                                                       //Natural: ASSIGN #COL := 3
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("-Z,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED EXT.INV-ACCT-DVDND-AMT ( #I ) ( EM = -Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
            sub_Convert_Amt();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Dividend");                                                                     //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Dividend'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_I),new ReportEditMask("ZZ,ZZ9.9999@"));                       //Natural: MOVE EDITED EXT.INV-ACCT-UNIT-VALUE ( #I ) ( EM = ZZ,ZZ9.9999@ ) TO #AMT-ALPHA
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pnd_Ws_Pnd_Amt_Alpha, MoveOption.RightJustified);                                //Natural: MOVE RIGHT #AMT-ALPHA TO #OUTPUT-COL ( #COL )
            setValueToSubstring(" ",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col),15,1);                                                            //Natural: MOVE ' ' TO SUBSTR ( #OUTPUT-COL ( #COL ) ,15,1 )
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue("Unit value");                                                                   //Natural: ASSIGN #OUTPUT-COL ( #COL ) := 'Unit value'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Ded_Sum.getBoolean()))                                                                                                                   //Natural: IF #DED-SUM
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DED-DETAIL
            sub_Print_Ded_Detail();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Ws_Pnd_Ded_Sum.getBoolean())))                                                                                                               //Natural: IF NOT #DED-SUM
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
            sub_Write_Body();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Net() throws Exception                                                                                                                         //Natural: PRINT-NET
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Col.setValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Net_Col());                                                                                                 //Natural: ASSIGN #COL := #NET-COL
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED EXT.INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Print_Ded_Sum() throws Exception                                                                                                                     //Natural: PRINT-DED-SUM
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Ws_Pnd_Temp_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Temp_Amt), pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I).add(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I)).add(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I)).add(can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(pnd_Ws_Pnd_I))); //Natural: COMPUTE #WS.#TEMP-AMT = EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) + EXT.INV-ACCT-STATE-TAX-AMT ( #I ) + EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) + CAN-PYMNT.INV-ACCT-CAN-TAX-AMT ( #I )
        if (condition(pnd_Ws_Pnd_Temp_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                             //Natural: IF #WS.#TEMP-AMT NE 0.00
        {
            pnd_Ws_Pnd_Ded_Sum.setValue(true);                                                                                                                            //Natural: ASSIGN #DED-SUM := TRUE
            pnd_Ws_Pnd_Col.setValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col());                                                                                             //Natural: ASSIGN #COL := #DED-COL
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Temp_Amt,new ReportEditMask("+ZZZZ,ZZ9.99"));                                                                  //Natural: MOVE EDITED #WS.#TEMP-AMT ( EM = +ZZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
            sub_Convert_Amt_Dollar();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Ded_Detail() throws Exception                                                                                                                  //Natural: PRINT-DED-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        short decideConditionsMet1220 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) NE 0.00 AND NOT #DED-FED
        if (condition(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")) && ! (pnd_Ws_Pnd_Ded_Fed.getBoolean())))
        {
            decideConditionsMet1220++;
            pnd_Ws_Pnd_Temp_Amt.setValue(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I));                                                               //Natural: ASSIGN #WS.#TEMP-AMT := EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
            pnd_Ws_Pnd_Ded_Idx.setValue(2);                                                                                                                               //Natural: ASSIGN #DED-IDX := 2
            pnd_Ws_Pnd_Ded_Fed.setValue(true);                                                                                                                            //Natural: ASSIGN #DED-FED := TRUE
        }                                                                                                                                                                 //Natural: WHEN EXT.INV-ACCT-STATE-TAX-AMT ( #I ) NE 0.00 AND NOT #DED-STATE
        else if (condition(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")) && ! (pnd_Ws_Pnd_Ded_State.getBoolean())))
        {
            decideConditionsMet1220++;
            pnd_Ws_Pnd_Temp_Amt.setValue(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I));                                                              //Natural: ASSIGN #WS.#TEMP-AMT := EXT.INV-ACCT-STATE-TAX-AMT ( #I )
            pnd_Ws_Pnd_Ded_Idx.setValue(3);                                                                                                                               //Natural: ASSIGN #DED-IDX := 3
            pnd_Ws_Pnd_Ded_State.setValue(true);                                                                                                                          //Natural: ASSIGN #DED-STATE := TRUE
        }                                                                                                                                                                 //Natural: WHEN EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) NE 0.00 AND NOT #DED-LOCAL
        else if (condition(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")) && ! (pnd_Ws_Pnd_Ded_Local.getBoolean())))
        {
            decideConditionsMet1220++;
            pnd_Ws_Pnd_Temp_Amt.setValue(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I));                                                              //Natural: ASSIGN #WS.#TEMP-AMT := EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
            pnd_Ws_Pnd_Ded_Idx.setValue(4);                                                                                                                               //Natural: ASSIGN #DED-IDX := 4
            pnd_Ws_Pnd_Ded_Local.setValue(true);                                                                                                                          //Natural: ASSIGN #DED-LOCAL := TRUE
        }                                                                                                                                                                 //Natural: WHEN CAN-PYMNT.INV-ACCT-CAN-TAX-AMT ( #I ) NE 0.00 AND NOT #DED-CAN
        else if (condition(can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")) && ! (pnd_Ws_Pnd_Ded_Can.getBoolean())))
        {
            decideConditionsMet1220++;
            pnd_Ws_Pnd_Temp_Amt.setValue(can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(pnd_Ws_Pnd_I));                                                                          //Natural: ASSIGN #WS.#TEMP-AMT := CAN-PYMNT.INV-ACCT-CAN-TAX-AMT ( #I )
            pnd_Ws_Pnd_Ded_Idx.setValue(5);                                                                                                                               //Natural: ASSIGN #DED-IDX := 5
            pnd_Ws_Pnd_Ded_Can.setValue(true);                                                                                                                            //Natural: ASSIGN #DED-CAN := TRUE
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1220 > 0))
        {
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Temp_Amt,new ReportEditMask("Z,ZZZ,ZZ9.99'( )'"));                                                             //Natural: MOVE EDITED #WS.#TEMP-AMT ( EM = Z,ZZZ,ZZ9.99'( )' ) TO #AMT-ALPHA
            //*  RCC
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col().greater(getZero()) && pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col().less(8)))                                 //Natural: IF #DED-COL > 0 AND #DED-COL LT 8
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col()).setValue(pnd_Ws_Pnd_Amt_Alpha);                              //Natural: ASSIGN #OUTPUT-COL ( #DED-COL ) := #AMT-ALPHA
                setValueToSubstring(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(pnd_Ws_Pnd_Ded_Idx).getSubstring(3,1),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Ded_Col()), //Natural: MOVE SUBSTR ( #DED-PYMNT-TABLE ( #DED-IDX ) ,3,1 ) TO SUBSTR ( #OUTPUT-COL ( #DED-COL ) ,14,1 )
                    14,1);
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
                sub_Write_Body();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Ded_Sum.setValue(false);                                                                                                                           //Natural: ASSIGN #DED-SUM := FALSE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Dpi_Dci() throws Exception                                                                                                                     //Natural: PRINT-DPI-DCI
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Ws_Pnd_Temp_Amt.compute(new ComputeParameters(false, pnd_Ws_Pnd_Temp_Amt), pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Pnd_I).add(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_I))); //Natural: COMPUTE #WS.#TEMP-AMT = EXT.INV-ACCT-DCI-AMT ( #I ) + EXT.INV-ACCT-DPI-AMT ( #I )
        if (condition(pnd_Ws_Pnd_Temp_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                             //Natural: IF #WS.#TEMP-AMT NE 0.00
        {
            pnd_Ws_Pnd_Col.setValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Dpi_Col());                                                                                             //Natural: ASSIGN #COL := #DPI-COL
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Pnd_Temp_Amt,new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                                                 //Natural: MOVE EDITED #WS.#TEMP-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
            sub_Convert_Amt_Dollar();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Body() throws Exception                                                                                                                        //Natural: WRITE-BODY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        setValueToSubstring(pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().getSubstring(63,78),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),59,82);                             //Natural: MOVE SUBSTR ( #OUTPUT-REC,63,78 ) TO SUBSTR ( #OUTPUT-REC,59,82 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Convert_Amt_Dollar() throws Exception                                                                                                                //Natural: CONVERT-AMT-$
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Amt_Alpha), new ExamineSearch("+"), new ExamineReplace("$"));                                                        //Natural: EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Convert_Amt() throws Exception                                                                                                                       //Natural: CONVERT-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        if (condition(pnd_Ws_Pnd_Col.notEquals(getZero())))                                                                                                               //Natural: IF #COL NE 0
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(pnd_Ws_Pnd_Amt_Alpha);                                                           //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #AMT-ALPHA
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bottom_Of_Page() throws Exception                                                                                                                    //Natural: BOTTOM-OF-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("06");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '06'
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().getBoolean()))                                                                                         //Natural: IF #END-OF-PYMNT
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-GRAND-TOTALS
            sub_Write_Grand_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Grand_Total_Text().getValue(1));                                   //Natural: ASSIGN #OUTPUT-REC-DETAIL := #GRAND-TOTAL-TEXT ( 1 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            //*  ROXAN 4/17/03
            short decideConditionsMet1285 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EXT.EGTRRA-ELIGIBILITY-IND = 'Y'
            if (condition(pdaFcpaext.getExt_Egtrra_Eligibility_Ind().equals("Y")))
            {
                decideConditionsMet1285++;
                                                                                                                                                                          //Natural: PERFORM GET-IVC-AMT
                sub_Get_Ivc_Amt();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc.equals(getZero()) && pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc.equals(getZero())))                                //Natural: IF #WS-TIAA-IVC = 0 AND #WS-CREF-IVC = 0
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_Zero_Text());                                          //Natural: ASSIGN #OUTPUT-REC-DETAIL := #FCPL803G.#IVC-ZERO-TEXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc.greater(getZero())))                                                                                  //Natural: IF #WS-TIAA-IVC > 0
                    {
                        pnd_Ws_Tpa_Egtrra_Pnd_Ws_From.setValue("FROM TIAA");                                                                                              //Natural: ASSIGN #WS-FROM := 'FROM TIAA'
                        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                                    //Natural: MOVE EDITED #WS-TIAA-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Amt_Alpha,true), new ExamineSearch("X", true), new ExamineDelete());                                 //Natural: EXAMINE FULL #AMT-ALPHA FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc.greater(getZero())))                                                                                  //Natural: IF #WS-CREF-IVC > 0
                    {
                        pnd_Ws_Tpa_Egtrra_Pnd_Ws_From_2.setValue("FROM CREF");                                                                                            //Natural: ASSIGN #WS-FROM-2 := 'FROM CREF'
                        pnd_Ws_Tpa_Egtrra_Pnd_Amt_Alpha_2.setValueEdited(pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc,new ReportEditMask("X'$'Z,ZZZ,ZZ9.99"));                       //Natural: MOVE EDITED #WS-CREF-IVC ( EM = X'$'Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-2
                        DbsUtil.examine(new ExamineSource(pnd_Ws_Tpa_Egtrra_Pnd_Amt_Alpha_2,true), new ExamineSearch("X", true), new ExamineDelete());                    //Natural: EXAMINE FULL #AMT-ALPHA-2 FOR FULL 'X' DELETE
                    }                                                                                                                                                     //Natural: END-IF
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_Text(), pnd_Ws_Pnd_Amt_Alpha,         //Natural: COMPRESS #FCPL803G.#IVC-TEXT #AMT-ALPHA #WS-FROM #AMT-ALPHA-2 #WS-FROM-2 INTO #OUTPUT-REC-DETAIL
                        pnd_Ws_Tpa_Egtrra_Pnd_Ws_From, pnd_Ws_Tpa_Egtrra_Pnd_Amt_Alpha_2, pnd_Ws_Tpa_Egtrra_Pnd_Ws_From_2));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTRING ( EXT.CNTRCT-PAYEE-CDE,3,2 ) = 'IV'
            else if (condition(pdaFcpaext.getExt_Cntrct_Payee_Cde().getSubstring(3,2).equals("IV")))
            {
                decideConditionsMet1285++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_From_Rtb_Rollover_Text());                                 //Natural: ASSIGN #OUTPUT-REC-DETAIL := #FCPL803G.#IVC-FROM-RTB-ROLLOVER-TEXT
                //*  07-25-2000
                setValueToSubstring("#",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),1,1);                                                                           //Natural: MOVE '#' TO SUBSTR ( #OUTPUT-REC-DETAIL,1,1 )
            }                                                                                                                                                             //Natural: WHEN IVC-FROM-RTB-ROLLOVER
            else if (condition(pnd_Chk_Fields_Ivc_From_Rtb_Rollover.getBoolean()))
            {
                decideConditionsMet1285++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_From_Rtb_Rollover_Text());                                 //Natural: ASSIGN #OUTPUT-REC-DETAIL := #FCPL803G.#IVC-FROM-RTB-ROLLOVER-TEXT
            }                                                                                                                                                             //Natural: WHEN EXT.PYMNT-IVC-AMT NE 0.00
            else if (condition(pdaFcpaext.getExt_Pymnt_Ivc_Amt().notEquals(new DbsDecimal("0.00"))))
            {
                decideConditionsMet1285++;
                pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpaext.getExt_Pymnt_Ivc_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                               //Natural: MOVE EDITED EXT.PYMNT-IVC-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                pnd_Ws_Pnd_Amt_Alpha.setValue(pnd_Ws_Pnd_Amt_Alpha, MoveOption.LeftJustified);                                                                            //Natural: MOVE LEFT #AMT-ALPHA TO #AMT-ALPHA
                if (condition(pnd_Ws_Pnd_Amt_Alpha.getSubstring(1,1).equals("+")))                                                                                        //Natural: IF SUBSTR ( #AMT-ALPHA,1,1 ) = '+'
                {
                    setValueToSubstring("$",pnd_Ws_Pnd_Amt_Alpha,1,1);                                                                                                    //Natural: MOVE '$' TO SUBSTR ( #AMT-ALPHA,1,1 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpaext.getExt_Inv_Acct_Company().getValue(1).equals("T")))                                                                              //Natural: IF EXT.INV-ACCT-COMPANY ( 1 ) = 'T'
                {
                    pnd_Ws_Pnd_Comp_Idx.setValue(1);                                                                                                                      //Natural: ASSIGN #COMP-IDX := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Comp_Idx.setValue(2);                                                                                                                      //Natural: ASSIGN #COMP-IDX := 2
                }                                                                                                                                                         //Natural: END-IF
                //* ***********  ADD ROTH INFO TO CHECK STATEMENT      /* JWO 07/02/2009
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Roth_Ind().getBoolean()))                                                                                     //Natural: IF #ROTH-IND
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Roth_Text(), pnd_Ws_Pnd_Amt_Alpha));      //Natural: COMPRESS #FCPL803G.#ROTH-TEXT #AMT-ALPHA INTO #OUTPUT-REC-DETAIL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Ivc_Text(), ldaFcpl803g.getPnd_Fcpl803g_Pnd_Company_Text().getValue(pnd_Ws_Pnd_Comp_Idx),  //Natural: COMPRESS #FCPL803G.#IVC-TEXT #FCPL803G.#COMPANY-TEXT ( #COMP-IDX ) #AMT-ALPHA INTO #OUTPUT-REC-DETAIL
                        pnd_Ws_Pnd_Amt_Alpha));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1285 > 0))
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue("1");                                                                                                         //Natural: ASSIGN #FCPA803.#CC := '1'
                pdaFcpa803.getPnd_Fcpa803_Pnd_Font().setValue("6");                                                                                                       //Natural: ASSIGN #FCPA803.#FONT := '6'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 7");                                                                                                //Natural: ASSIGN #OUTPUT-REC := ' 7'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("17");                                                                                                //Natural: ASSIGN #OUTPUT-REC := '17'
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))                                                                                       //Natural: IF #FCPA803.#GLOBAL-PAY
            {
                setValueToSubstring("          ",ldaFcpl803g.getPnd_Fcpl803g_Pnd_Nz_Page_1_Text().getValue(2),95,10);                                                     //Natural: MOVE '          ' TO SUBSTR ( #NZ-PAGE-1-TEXT ( 2 ) ,95,10 )
                setValueToSubstring("          ",ldaFcpl803g.getPnd_Fcpl803g_Pnd_Nz_Page_1_Text().getValue(3),1,100);                                                     //Natural: MOVE '          ' TO SUBSTR ( #NZ-PAGE-1-TEXT ( 3 ) ,01,100 )
                setValueToSubstring(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Phone_Text().getValue(2),ldaFcpl803g.getPnd_Fcpl803g_Pnd_Nz_Page_1_Text().getValue(3),                //Natural: MOVE #PHONE-TEXT ( 2 ) TO SUBSTR ( #NZ-PAGE-1-TEXT ( 3 ) ,01,47 )
                    1,47);
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #J = 1 TO 3
            for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(3)); pnd_Ws_Pnd_J.nadd(1))
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Nz_Page_1_Text().getValue(pnd_Ws_Pnd_J));                      //Natural: ASSIGN #OUTPUT-REC-DETAIL := #NZ-PAGE-1-TEXT ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                         //Natural: ASSIGN #CC := ' '
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  DEDUCTION LEDGENDS
            //* MOVE BY NAME PYMNT-ADDR-INFO       TO #FCPA803L                 /*
            pdaFcpa803l.getPnd_Fcpa803l().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                      //Natural: MOVE BY NAME EXTR TO #FCPA803L
            //* CALLNAT 'FCPN874L' USING #NZ-CHECK-FIELDS #FCPA803 #FCPA803L    /*
            //* *CALLNAT 'FCPN874L' USING #CHK-FIELDS.#PYMNT-IVC-AMT      /* 07-25-2000
            //* TMM
            DbsUtil.callnat(Fcpn874l.class , getCurrentProcessState(), pdaFcpaext.getExt_Pymnt_Ivc_Amt(), pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue("*"),  //Natural: CALLNAT 'FCPN874L' USING EXT.PYMNT-IVC-AMT #DED-PYMNT-TABLE ( * ) EXT.PYMNT-RECORD #DPI-DCI-IND #PYMNT-DED-IND #FCPA803 #FCPA803L
                pdaFcpaext.getExt_Pymnt_Record(), pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind(), pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind(), 
                pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803l.getPnd_Fcpa803l());
            if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM CHECK-PART
            sub_Check_Part();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BARCODE
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
    }
    //*  CHECK FORMATTED HERE RL
    private void sub_Check_Part() throws Exception                                                                                                                        //Natural: CHECK-PART
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        //* MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #FCPA874C                 /*
        pdaFcpa874c.getPnd_Fcpa874c().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                          //Natural: MOVE BY NAME EXTR TO #FCPA874C
        pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp().getValue("*").setValuesByName(pdaFcpaext.getExt_Pymnt_Nme_And_Addr_Grp().getValue("*"));                     //Natural: MOVE BY NAME EXT.PYMNT-NME-AND-ADDR-GRP ( * ) TO #FCPA874A.PYMNT-NME-AND-ADDR-GRP ( * )
        //*  CHECK PRINTING DATA  /* RL
        DbsUtil.callnat(Fcpn874c.class , getCurrentProcessState(), pdaFcpa874c.getPnd_Fcpa874c(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa874a.getPnd_Fcpa874a(),             //Natural: CALLNAT 'FCPN874C' USING #FCPA874C #FCPA803 #FCPA874A FCPA110
            pdaFcpa110.getFcpa110());
        if (condition(Global.isEscape())) return;
        //*  RL
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                        //Natural: MOVE '+3' TO #OUTPUT-REC
        //*  RL
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }
    private void sub_Gen_Pymnt_Headers() throws Exception                                                                                                                 //Natural: GEN-PYMNT-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //* ********************** RL BEGIN PAYEE MATCH **************************
        //*  RL
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().getBoolean()))                                                                                        //Natural: IF #FCPA803.#STMNT-TO-ANNT
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  RL FRI FEB 10
            //*  RL AUG 11, 2006
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean() || pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean() && pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean())) //Natural: IF #FCPA803.#CHECK OR #FCPA803.#CHECK-TO-ANNT AND #FCPA803.#FULL-XEROX
            {
                //*  RL AUG 11, 2006
                //*  RL AUG 11, 2006
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                    //Natural: IF #FCPA803.#CURRENT-PAGE = 1
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("12");                                                                                            //Natural: ASSIGN #OUTPUT-REC := '12'
                    pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                         //Natural: MOVE EXT.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                    pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                               //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                               //Natural: ASSIGN #OUTPUT-REC-DETAIL := #WS-PYMNT-CHECK-NBR-N10
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                    sub_Fcpc803w();
                    if (condition(Global.isEscape())) {return;}
                    //*  RL AUG 11, 2006
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************** RL END PAYEE MATCH ***************************
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean())))                                                                                         //Natural: IF NOT #ODD-PAGE
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("11");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '11'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            pdaFcpa874h.getPnd_Fcpa874h_Pnd_Gen_Headers().setValue(true);                                                                                                 //Natural: ASSIGN #GEN-HEADERS := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Company_Cde().setValue(pdaFcpaext.getExt_Inv_Acct_Company().getValue(1));                                                      //Natural: ASSIGN #FCPA874H.CNTRCT-COMPANY-CDE := EXT.INV-ACCT-COMPANY ( 1 )
        pdaFcpa874h.getPnd_Fcpa874h_Cnr_Orgnl_Invrse_Dte().setValue(pdaFcpaext.getExt_Cnr_Orgnl_Invrse_Dte());                                                            //Natural: ASSIGN #FCPA874H.CNR-ORGNL-INVRSE-DTE := EXT.CNR-ORGNL-INVRSE-DTE
        pdaFcpa874h.getPnd_Fcpa874h().setValuesByName(pdaFcpa803.getPnd_Fcpa803());                                                                                       //Natural: MOVE BY NAME #FCPA803 TO #FCPA874H
        //* MOVE BY NAME PYMNT-ADDR-INFO       TO #FCPA874H                 /*
        //*  07-25-2000
        //*  07-25-2000
        //*  07-25-2000
        //*  07-25-2000
        //*  07-25-2000
        //*  07-25-2000
        pdaFcpa874h.getPnd_Fcpa874h().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                          //Natural: MOVE BY NAME EXTR TO #FCPA874H
        pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(1).setValue(pdaFcpaext.getExt_Cntrct_Da_Tiaa_1_Nbr());                                                       //Natural: ASSIGN #FCPA874H.CNTRCT-DA-NBR ( 1 ) := EXT.CNTRCT-DA-TIAA-1-NBR
        pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(2).setValue(pdaFcpaext.getExt_Cntrct_Da_Tiaa_2_Nbr());                                                       //Natural: ASSIGN #FCPA874H.CNTRCT-DA-NBR ( 2 ) := EXT.CNTRCT-DA-TIAA-2-NBR
        pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(3).setValue(pdaFcpaext.getExt_Cntrct_Da_Tiaa_3_Nbr());                                                       //Natural: ASSIGN #FCPA874H.CNTRCT-DA-NBR ( 3 ) := EXT.CNTRCT-DA-TIAA-3-NBR
        pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(4).setValue(pdaFcpaext.getExt_Cntrct_Da_Tiaa_4_Nbr());                                                       //Natural: ASSIGN #FCPA874H.CNTRCT-DA-NBR ( 4 ) := EXT.CNTRCT-DA-TIAA-4-NBR
        pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(5).setValue(pdaFcpaext.getExt_Cntrct_Da_Tiaa_5_Nbr());                                                       //Natural: ASSIGN #FCPA874H.CNTRCT-DA-NBR ( 5 ) := EXT.CNTRCT-DA-TIAA-5-NBR
        pdaFcpa874h.getPnd_Fcpa874h_Cntrct_Da_Nbr().getValue(6).setValue(pdaFcpaext.getExt_Cntrct_Da_Cref_1_Nbr());                                                       //Natural: ASSIGN #FCPA874H.CNTRCT-DA-NBR ( 6 ) := EXT.CNTRCT-DA-CREF-1-NBR
        DbsUtil.callnat(Fcpn874h.class , getCurrentProcessState(), pdaFcpa874h.getPnd_Fcpa874h());                                                                        //Natural: CALLNAT 'FCPN874H' USING #FCPA874H
        if (condition(Global.isEscape())) return;
    }
    private void sub_Gen_Cntrct_Headers() throws Exception                                                                                                                //Natural: GEN-CNTRCT-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //* TMM
        DbsUtil.callnat(Fcpn874t.class , getCurrentProcessState(), pdaFcpaext.getExt_Cntrct_Hold_Cde(), pdaFcpaext.getExt_Inv_Acct_Company().getValue(1),                 //Natural: CALLNAT 'FCPN874T' USING EXT.CNTRCT-HOLD-CDE EXT.INV-ACCT-COMPANY ( 1 ) EXT.PYMNT-TOTAL-PAGES EXT.CNR-ORGNL-INVRSE-DTE #FILLER EXT.PYMNT-IVC-AMT #DED-PYMNT-TABLE ( * ) EXT.PYMNT-RECORD #DPI-DCI-IND #PYMNT-DED-IND #FCPA803
            pdaFcpaext.getExt_Pymnt_Total_Pages(), pdaFcpaext.getExt_Cnr_Orgnl_Invrse_Dte(), pnd_Chk_Fields_Pnd_Filler, pdaFcpaext.getExt_Pymnt_Ivc_Amt(), 
            pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue("*"), pdaFcpaext.getExt_Pymnt_Record(), pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind(), 
            pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind(), pdaFcpa803.getPnd_Fcpa803());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Grand_Totals() throws Exception                                                                                                                //Natural: WRITE-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Col.setValue(2);                                                                                                                                       //Natural: ASSIGN #COL := 2
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Col().getValue(pnd_Ws_Pnd_Col).setValue(ldaFcpl803g.getPnd_Fcpl803g_Pnd_Grand_Total_Text().getValue(2));                     //Natural: ASSIGN #OUTPUT-COL ( #COL ) := #GRAND-TOTAL-TEXT ( 2 )
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Settl_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                         //Natural: MOVE EDITED #GRAND-SETTL-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
        //*  #NZ-CHECK-FIELDS.PYMNT-IVC-AMT    NE 0.00                      /*
        //*  07-25-2000
        if (condition(pdaFcpaext.getExt_Pymnt_Ivc_Amt().notEquals(new DbsDecimal("0.00"))))                                                                               //Natural: IF EXT.PYMNT-IVC-AMT NE 0.00
        {
            setValueToSubstring("#",pnd_Ws_Pnd_Amt_Alpha,14,1);                                                                                                           //Natural: MOVE '#' TO SUBSTR ( #AMT-ALPHA,14,1 )
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean()))                                                                                  //Natural: IF #DPI-DCI-IND
        {
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Dpi_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                       //Natural: MOVE EDITED #GRAND-DPI-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
            sub_Convert_Amt_Dollar();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean()))                                                                                //Natural: IF #PYMNT-DED-IND
        {
            pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                       //Natural: ASSIGN #COL := #COL + 1
            pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Ded_Amt(),new ReportEditMask("+ZZZZ,ZZ9.99"));                                        //Natural: MOVE EDITED #GRAND-DED-AMT ( EM = +ZZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
            sub_Convert_Amt_Dollar();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Col.nadd(1);                                                                                                                                           //Natural: ASSIGN #COL := #COL + 1
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa803.getPnd_Fcpa803_Pnd_Grand_Net_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                           //Natural: MOVE EDITED #GRAND-NET-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT-$
        sub_Convert_Amt_Dollar();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-BODY
        sub_Write_Body();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Letter_Check() throws Exception                                                                                                                      //Natural: LETTER-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Type().reset();                                                                                                               //Natural: RESET #FCPA803.#STMNT-TYPE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Check().setValue(true);                                                                                                             //Natural: ASSIGN #FCPA803.#CHECK := TRUE
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean())))                                                                                          //Natural: IF NOT #SIMPLEX
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpn803x.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pnd_Ws_Pnd_Null);                                                         //Natural: CALLNAT 'FCPN803X' USING #FCPA803 #NULL
        if (condition(Global.isEscape())) return;
        //* ********************** RL BEGIN PAYEE MATCH **************************
        //* * FORMAT=SSLETX,FORMS=MCMLET
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean() || pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean() && pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean())) //Natural: IF #FCPA803.#CHECK OR #FCPA803.#CHECK-TO-ANNT AND #FCPA803.#FULL-XEROX
        {
            //*  RL AUG 11, 2006
            //*  RAHUL
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1) || pnd_Chk_Printed.equals(false)))                                                       //Natural: IF #FCPA803.#CURRENT-PAGE = 1 OR #CHK-PRINTED = FALSE
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("12");                                                                                                //Natural: ASSIGN #OUTPUT-REC := '12'
                pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                             //Natural: MOVE EXT.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                   //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                   //Natural: ASSIGN #OUTPUT-REC-DETAIL := #WS-PYMNT-CHECK-NBR-N10
                //*  RAHUL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
                pnd_Chk_Printed.setValue(true);                                                                                                                           //Natural: ASSIGN #CHK-PRINTED := TRUE
                //*  RL AUG 11, 2006
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            getWorkFiles().write(8, false, pdaFcpa874h.getPnd_Fcpa874h_Pnd_Header_Array().getValue(pnd_Ws_Pnd_I));                                                        //Natural: WRITE WORK FILE 8 #HEADER-ARRAY ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpaext.getExt_Pymnt_Alt_Addr_Ind().getBoolean()))                                                                                               //Natural: IF EXT.PYMNT-ALT-ADDR-IND
        {
            pnd_Ws_Pnd_J.setValue(2);                                                                                                                                     //Natural: ASSIGN #J := 2
            pnd_Ws_Pnd_K.setValue(3);                                                                                                                                     //Natural: ASSIGN #K := 3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_J.setValue(1);                                                                                                                                     //Natural: ASSIGN #J := 1
            pnd_Ws_Pnd_K.setValue(4);                                                                                                                                     //Natural: ASSIGN #K := 4
            setValueToSubstring(pdaFcpaext.getExt_Pymnt_Eft_Acct_Nbr(),ldaFcpl874c.getPnd_Fcpl874c_Pnd_Letter_Text().getValue(1,2),39,21);                                //Natural: MOVE EXT.PYMNT-EFT-ACCT-NBR TO SUBSTR ( #LETTER-TEXT ( 1,2 ) ,39,21 )
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO #K
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Ws_Pnd_K)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl874c.getPnd_Fcpl874c_Pnd_Letter_Text().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I));                       //Natural: ASSIGN #OUTPUT-REC := #FCPL874C.#LETTER-TEXT ( #J,#I )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(1);                                                                                                             //Natural: ASSIGN #ADDR-IND := 1
                                                                                                                                                                          //Natural: PERFORM CHECK-PART
        sub_Check_Part();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().setValue(1);                                                                                                        //Natural: ASSIGN #BAR-LAST-PAGE := #FCPA803.#CURRENT-PAGE := 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().setValue(1);
                                                                                                                                                                          //Natural: PERFORM GEN-BARCODE
        sub_Gen_Barcode();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().setValue(true);                                                                                                          //Natural: ASSIGN #ODD-PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean())))                                                                                          //Natural: IF NOT #SIMPLEX
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Gen_Barcode() throws Exception                                                                                                                       //Natural: GEN-BARCODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  FIRST PAGE OF ENVELOPE
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #FCPA803.#CURRENT-PAGE = 1
        {
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Demand_Feed().setValue(true);                                                                                           //Natural: ASSIGN #BAR-DEMAND-FEED := TRUE
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Id_Num().nadd(1);                                                                                                   //Natural: ADD 1 TO #BARCODE-PDA.#BAR-ENV-ID-NUM
        }                                                                                                                                                                 //Natural: END-IF
        //*  LAST  PAGE OF ENVELOPE
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().equals(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page())))                                                //Natural: IF #BAR-LAST-PAGE = #FCPA803.#CURRENT-PAGE
        {
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Set_Last_Page().setValue(true);                                                                                         //Natural: ASSIGN #BAR-SET-LAST-PAGE := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_New_Run().getBoolean()))                                                                                      //Natural: IF #BAR-NEW-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpnbar.class , getCurrentProcessState(), pdaFcpabar.getPnd_Barcode_Pda());                                                                       //Natural: CALLNAT 'FCPNBAR' #BARCODE-PDA
        if (condition(Global.isEscape())) return;
    }
    private void sub_Barcode_Processing() throws Exception                                                                                                                //Natural: BARCODE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(! (pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean())))                                                                                         //Natural: IF NOT #ODD-PAGE
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("11");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := '11'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("1<");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '1<'
        FOR06:                                                                                                                                                            //Natural: FOR #J = 1 TO 17
        for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(17)); pnd_Ws_Pnd_J.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Barcode().getValue(pnd_Ws_Pnd_J));                           //Natural: ASSIGN #OUTPUT-REC-DETAIL := #BAR-BARCODE ( #J )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa803.getPnd_Fcpa803_Pnd_Cc().setValue(" ");                                                                                                             //Natural: ASSIGN #CC := ' '
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Set_Last_Page().getBoolean()))                                                                                //Natural: IF #BAR-SET-LAST-PAGE
        {
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee().setValue(pdaFcpa874a.getPnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme());                                                      //Natural: ASSIGN #FCPL876B.#ADDRESSEE := #STMNT-PYMNT-NME
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Id());                                                        //Natural: ASSIGN #FCPL876B.#BAR-ENV-ID := #BARCODE-PDA.#BAR-ENV-ID
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                  //Natural: ASSIGN #FCPL876B.CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr().setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                                  //Natural: ASSIGN #FCPL876B.CNTRCT-PPCN-NBR := EXT.CNTRCT-PPCN-NBR
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde().setValue(pdaFcpaext.getExt_Cntrct_Payee_Cde());                                                                //Natural: ASSIGN #FCPL876B.CNTRCT-PAYEE-CDE := EXT.CNTRCT-PAYEE-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde().setValue(pdaFcpaext.getExt_Cntrct_Hold_Cde());                                                                  //Natural: ASSIGN #FCPL876B.CNTRCT-HOLD-CDE := EXT.CNTRCT-HOLD-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Integrity_Counter());                          //Natural: ASSIGN #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER := #BARCODE-PDA.#BAR-ENV-INTEGRITY-COUNTER
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))                                                                                            //Natural: IF #FCPA803.#STMNT
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Pymnt_Check_Nbr().reset();                                                                                                //Natural: RESET #FCPL876B.#PYMNT-CHECK-NBR
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(false);                                                                                                  //Natural: ASSIGN #FCPL876B.#CHECK := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                              //Natural: ASSIGN #FCPL876B.PYMNT-CHECK-NBR := EXT.PYMNT-CHECK-NBR
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(true);                                                                                                   //Natural: ASSIGN #FCPL876B.#CHECK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Envelopes());                                                  //Natural: ASSIGN #FCPL876B.#BAR-ENVELOPES := #BARCODE-PDA.#BAR-ENVELOPES
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env().setValue(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Pages_In_Env());                                            //Natural: ASSIGN #FCPL876B.#BAR-PAGES-IN-ENV := #BARCODE-PDA.#BAR-PAGES-IN-ENV
            getWorkFiles().write(8, false, ldaFcpl876b.getPnd_Fcpl876b());                                                                                                //Natural: WRITE WORK FILE 8 #FCPL876B
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))                                                                                       //Natural: IF #FCPA803.#GLOBAL-PAY
            {
                getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",                                                //Natural: DISPLAY ( 1 ) ( HC = R ) '//ADDRESSEE' #FCPL876B.#ADDRESSEE ( HC = L ) '/BARCODE/ID' #FCPL876B.#BAR-ENV-ID ( HC = C ) 'PACKAGE/INTEG/RITY' #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER ( EM = �������9 ) '/ENVELOPE/NUMBER' #FCPL876B.#BAR-ENVELOPES ( EM = -Z,ZZZ,ZZ9 ) '# OF/PAGES IN/ENVELOPE' #FCPL876B.#BAR-PAGES-IN-ENV ( EM = -Z,ZZZ,ZZ9 ) '//CHECK' #FCPL876B.#CHECK ( EM = NO/YES ) '/ORGN/CODE' #FCPL876B.CNTRCT-ORGN-CDE ( LC = � ) '/CONTRACT/NUMBER' #FCPL876B.CNTRCT-PPCN-NBR ( HC = L ) '/PAYEE/CODE' #FCPL876B.CNTRCT-PAYEE-CDE ( HC = L ) '/HOLD/CODE' #FCPL876B.CNTRCT-HOLD-CDE '/CHECK/NUMBER' #FCPL876B.PYMNT-CHECK-NBR
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
                    
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
                		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
                		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
                		ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CODE BELLOW IS A TEMP CODE
        //* *DISPLAY
        //*  '/HOLD'      #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
        //*  '/PPCN'      NZ-EXT.CNTRCT-PPCN-NBR
        //*  '/SEQ'       PYMNT-PRCSS-SEQ-NBR
        //*  'TOT/PAGES'  #CHECK-SORT-FIELDS.PYMNT-TOTAL-PAGES
        //*  'N/R'        #BAR-NEW-RUN           (EM=F/T)
        //*  'S/M'        #FCPA803.#STMNT        (EM=F/T)
        //*  'E/I'        #BAR-BARCODE(1)
        //*  'B/1'        #BAR-BARCODE(2)
        //*  'E/S'        #BAR-BARCODE(3)
        //*  'B/2'        #BAR-BARCODE(4)
        //*  'B/3'        #BAR-BARCODE(5)
        //*  'B/4'        #BAR-BARCODE(6)
        //*  'B/5'        #BAR-BARCODE(7)
        //*  'P/C'        #BAR-BARCODE(8)
        //*  'E/C'        #BAR-BARCODE(9)
        //*  '/PIN'       #BARCODE-PDA.#BAR-ENV-ID
        //*  'S/I'        #BAR-BARCODE(17)
        //*  '/ENV'       #BARCODE-PDA.#BAR-ENVELOPES
        //*  'P/E'        #BARCODE-PDA.#BAR-PAGES-IN-ENV
        //*  '/NAME'      #STMNT-PYMNT-NME       (AL=33)
        //*  CODE ABOVE IS A TEMP CODE
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Barcode_Input().resetInitial();                                                                                                 //Natural: RESET INITIAL #BARCODE-INPUT
    }
    private void sub_Get_Ivc_Amt() throws Exception                                                                                                                       //Natural: GET-IVC-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc.reset();                                                                                                                        //Natural: RESET #WS-TIAA-IVC #WS-CREF-IVC #WS-FROM #WS-FROM-2 #AMT-ALPHA-2 #AMT-ALPHA
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc.reset();
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_From.reset();
        pnd_Ws_Tpa_Egtrra_Pnd_Ws_From_2.reset();
        pnd_Ws_Tpa_Egtrra_Pnd_Amt_Alpha_2.reset();
        pnd_Ws_Pnd_Amt_Alpha.reset();
        FOR07:                                                                                                                                                            //Natural: FOR #X 1 40
        for (pnd_Ws_Tpa_Egtrra_Pnd_X.setValue(1); condition(pnd_Ws_Tpa_Egtrra_Pnd_X.lessOrEqual(40)); pnd_Ws_Tpa_Egtrra_Pnd_X.nadd(1))
        {
            if (condition(pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_Ws_Tpa_Egtrra_Pnd_X).equals(" ")))                                                                //Natural: IF INV-ACCT-CDE ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_Company().getValue(pnd_Ws_Tpa_Egtrra_Pnd_X).equals("T")))                                                            //Natural: IF INV-ACCT-COMPANY ( #X ) = 'T'
            {
                pnd_Ws_Tpa_Egtrra_Pnd_Ws_Tiaa_Ivc.nadd(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Tpa_Egtrra_Pnd_X));                                           //Natural: ASSIGN #WS-TIAA-IVC := #WS-TIAA-IVC + INV-ACCT-IVC-AMT ( #X )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Tpa_Egtrra_Pnd_Ws_Cref_Ivc.nadd(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Tpa_Egtrra_Pnd_X));                                           //Natural: ASSIGN #WS-CREF-IVC := #WS-CREF-IVC + INV-ACCT-IVC-AMT ( #X )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(45),"ANNUITANT STATEMENT DETAIL CONTROL REPORT",new TabSetting(120),
            "REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr());
    }
}
