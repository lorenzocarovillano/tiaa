/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:00 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnbar
************************************************************
**        * FILE NAME            : Fcpnbar.java
**        * CLASS NAME           : Fcpnbar
**        * INSTANCE NAME        : Fcpnbar
************************************************************
************************************************************************
* PROGRAM  : FCPNBAR
* SYSTEM   : CPS
* TITLE    : FORMAT BARCODE
* FUNCTION : THIS PROGRAM WILL FORMAT BARCODES.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnbar extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpabar pdaFcpabar;
    private LdaFcplbar ldaFcplbar;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Idx;
    private DbsField pnd_Ws_Pnd_5_Bits;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_5_Bits_Array;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplbar = new LdaFcplbar();
        registerRecord(ldaFcplbar);

        // parameters
        parameters = new DbsRecord();
        pdaFcpabar = new PdaFcpabar(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_5_Bits = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_5_Bits", "#5-BITS", FieldType.STRING, 5);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_5_Bits);
        pnd_Ws_Pnd_5_Bits_Array = pnd_Ws__R_Field_1.newFieldArrayInGroup("pnd_Ws_Pnd_5_Bits_Array", "#5-BITS-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 
            5));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplbar.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_5_Bits.setInitialValue("00000");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnbar() throws Exception
    {
        super("Fcpnbar");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Ws_Pnd_5_Bits.resetInitial();                                                                                                                                 //Natural: RESET INITIAL #5-BITS
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Tiaa_Cover_Pull().getBoolean()))                                                                              //Natural: IF #BAR-TIAA-COVER-PULL
        {
            pnd_Ws_Pnd_5_Bits_Array.getValue(1).setValue("1");                                                                                                            //Natural: MOVE '1' TO #5-BITS-ARRAY ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Cref_Cover_Pull().getBoolean()))                                                                              //Natural: IF #BAR-CREF-COVER-PULL
        {
            pnd_Ws_Pnd_5_Bits_Array.getValue(2).setValue("1");                                                                                                            //Natural: MOVE '1' TO #5-BITS-ARRAY ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Stitch_Ind().getBoolean()))                                                                                   //Natural: IF #BAR-STITCH-IND
        {
            pnd_Ws_Pnd_5_Bits_Array.getValue(3).setValue("1");                                                                                                            //Natural: MOVE '1' TO #5-BITS-ARRAY ( 3 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Demand_Feed().getBoolean()))                                                                                  //Natural: IF #BAR-DEMAND-FEED
        {
            pnd_Ws_Pnd_5_Bits_Array.getValue(4).setValue("1");                                                                                                            //Natural: MOVE '1' TO #5-BITS-ARRAY ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Search_Key().getValue("*")), new ExamineSearch(pnd_Ws_Pnd_5_Bits),                    //Natural: EXAMINE #BAR-TABLE-SEARCH-KEY ( * ) FOR #5-BITS GIVING INDEX IN #IDX
            new ExamineGivingIndex(pnd_Ws_Pnd_Idx));
        if (condition(pnd_Ws_Pnd_Idx.equals(getZero())))                                                                                                                  //Natural: IF #IDX = 0
        {
            getReports().write(0, "ERROR IN BARCODE GENERATION. FUNCTION BYTE 1:",pnd_Ws_Pnd_5_Bits);                                                                     //Natural: WRITE 'ERROR IN BARCODE GENERATION. FUNCTION BYTE 1:' #5-BITS
            if (Global.isEscape()) return;
            DbsUtil.terminate(60);  if (true) return;                                                                                                                     //Natural: TERMINATE 60
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Machine_Function_Byte_1().setValue(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Value().getValue(pnd_Ws_Pnd_Idx));             //Natural: MOVE #BAR-TABLE-VALUE ( #IDX ) TO #BAR-MACHINE-FUNCTION-BYTE-1
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Set_Last_Page().getBoolean()))                                                                                //Natural: IF #BAR-SET-LAST-PAGE
        {
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_End_Of_Set_Ind().setValue("E");                                                                                         //Natural: MOVE 'E' TO #BAR-END-OF-SET-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_End_Of_Set_Ind().setValue("0");                                                                                         //Natural: MOVE '0' TO #BAR-END-OF-SET-IND
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_5_Bits.resetInitial();                                                                                                                                 //Natural: RESET INITIAL #5-BITS
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Alert_Clear().getBoolean()))                                                                                  //Natural: IF #BAR-ALERT-CLEAR
        {
            pnd_Ws_Pnd_5_Bits_Array.getValue(1).setValue("1");                                                                                                            //Natural: MOVE '1' TO #5-BITS-ARRAY ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Divert().getBoolean()))                                                                                       //Natural: IF #BAR-DIVERT
        {
            pnd_Ws_Pnd_5_Bits_Array.getValue(2).setValue("1");                                                                                                            //Natural: MOVE '1' TO #5-BITS-ARRAY ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Zip_Break().getBoolean()))                                                                                    //Natural: IF #BAR-ZIP-BREAK
        {
            pnd_Ws_Pnd_5_Bits_Array.getValue(3).setValue("1");                                                                                                            //Natural: MOVE '1' TO #5-BITS-ARRAY ( 3 )
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Search_Key().getValue("*")), new ExamineSearch(pnd_Ws_Pnd_5_Bits),                    //Natural: EXAMINE #BAR-TABLE-SEARCH-KEY ( * ) FOR #5-BITS GIVING INDEX IN #IDX
            new ExamineGivingIndex(pnd_Ws_Pnd_Idx));
        if (condition(pnd_Ws_Pnd_Idx.equals(getZero())))                                                                                                                  //Natural: IF #IDX = 0
        {
            getReports().write(0, "ERROR IN BARCODE GENERATION. FUNCTION BYTE 2:",pnd_Ws_Pnd_5_Bits);                                                                     //Natural: WRITE 'ERROR IN BARCODE GENERATION. FUNCTION BYTE 2:' #5-BITS
            if (Global.isEscape()) return;
            DbsUtil.terminate(61);  if (true) return;                                                                                                                     //Natural: TERMINATE 61
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Machine_Function_Byte_2().setValue(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Value().getValue(pnd_Ws_Pnd_Idx));             //Natural: MOVE #BAR-TABLE-VALUE ( #IDX ) TO #BAR-MACHINE-FUNCTION-BYTE-2
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Machine_Function_Byte_3().setValue("0");                                                                                    //Natural: MOVE '0' TO #BAR-MACHINE-FUNCTION-BYTE-3
        pnd_Ws_Pnd_5_Bits.resetInitial();                                                                                                                                 //Natural: RESET INITIAL #5-BITS
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(5)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Function_Byte_4_5().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                 //Natural: IF #BAR-FUNCTION-BYTE-4-5 ( #I )
            {
                pnd_Ws_Pnd_5_Bits_Array.getValue(pnd_Ws_Pnd_I).setValue("1");                                                                                             //Natural: MOVE '1' TO #5-BITS-ARRAY ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Search_Key().getValue("*")), new ExamineSearch(pnd_Ws_Pnd_5_Bits),                    //Natural: EXAMINE #BAR-TABLE-SEARCH-KEY ( * ) FOR #5-BITS GIVING INDEX IN #IDX
            new ExamineGivingIndex(pnd_Ws_Pnd_Idx));
        if (condition(pnd_Ws_Pnd_Idx.equals(getZero())))                                                                                                                  //Natural: IF #IDX = 0
        {
            getReports().write(0, "ERROR IN BARCODE GENERATION. FUNCTION BYTE 4:",pnd_Ws_Pnd_5_Bits);                                                                     //Natural: WRITE 'ERROR IN BARCODE GENERATION. FUNCTION BYTE 4:' #5-BITS
            if (Global.isEscape()) return;
            DbsUtil.terminate(62);  if (true) return;                                                                                                                     //Natural: TERMINATE 62
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Machine_Function_Byte_4().setValue(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Value().getValue(pnd_Ws_Pnd_Idx));             //Natural: MOVE #BAR-TABLE-VALUE ( #IDX ) TO #BAR-MACHINE-FUNCTION-BYTE-4
        pnd_Ws_Pnd_5_Bits.resetInitial();                                                                                                                                 //Natural: RESET INITIAL #5-BITS
        FOR02:                                                                                                                                                            //Natural: FOR #I = 6 TO 10
        for (pnd_Ws_Pnd_I.setValue(6); condition(pnd_Ws_Pnd_I.lessOrEqual(10)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Function_Byte_4_5().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                 //Natural: IF #BAR-FUNCTION-BYTE-4-5 ( #I )
            {
                pnd_Ws_Pnd_5_Bits_Array.getValue(pnd_Ws_Pnd_I).setValue("1");                                                                                             //Natural: MOVE '1' TO #5-BITS-ARRAY ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Search_Key().getValue("*")), new ExamineSearch(pnd_Ws_Pnd_5_Bits),                    //Natural: EXAMINE #BAR-TABLE-SEARCH-KEY ( * ) FOR #5-BITS GIVING INDEX IN #IDX
            new ExamineGivingIndex(pnd_Ws_Pnd_Idx));
        if (condition(pnd_Ws_Pnd_Idx.equals(getZero())))                                                                                                                  //Natural: IF #IDX = 0
        {
            getReports().write(0, "ERROR IN BARCODE GENERATION. FUNCTION BYTE 5:",pnd_Ws_Pnd_5_Bits);                                                                     //Natural: WRITE 'ERROR IN BARCODE GENERATION. FUNCTION BYTE 5:' #5-BITS
            if (Global.isEscape()) return;
            DbsUtil.terminate(63);  if (true) return;                                                                                                                     //Natural: TERMINATE 63
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Machine_Function_Byte_5().setValue(ldaFcplbar.getPnd_Bar_Table_Pnd_Bar_Table_Value().getValue(pnd_Ws_Pnd_Idx));             //Natural: MOVE #BAR-TABLE-VALUE ( #IDX ) TO #BAR-MACHINE-FUNCTION-BYTE-5
        //*  NEW RUN (OR HOLD BREAK)
        short decideConditionsMet139 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #BAR-NEW-RUN
        if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_New_Run().getBoolean()))
        {
            decideConditionsMet139++;
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Integrity_Counter().setValue(0);                                                                                    //Natural: MOVE 0 TO #BAR-ENV-INTEGRITY-COUNTER
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Page_Integrity_Counter().setValue(0);                                                                                   //Natural: MOVE 0 TO #BAR-PAGE-INTEGRITY-COUNTER
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Envelopes().setValue(1);                                                                                                //Natural: MOVE 1 TO #BAR-ENVELOPES #BAR-PAGES-IN-ENV
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Pages_In_Env().setValue(1);
        }                                                                                                                                                                 //Natural: WHEN #BAR-DEMAND-FEED
        else if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Demand_Feed().getBoolean()))
        {
            decideConditionsMet139++;
            if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Integrity_Counter().equals(9)))                                                                       //Natural: IF #BAR-ENV-INTEGRITY-COUNTER = 9
            {
                pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Integrity_Counter().setValue(0);                                                                                //Natural: MOVE 0 TO #BAR-ENV-INTEGRITY-COUNTER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Env_Integrity_Counter().nadd(1);                                                                                    //Natural: ADD 1 TO #BAR-ENV-INTEGRITY-COUNTER
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Page_Integrity_Counter().setValue(0);                                                                                   //Natural: MOVE 0 TO #BAR-PAGE-INTEGRITY-COUNTER
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Pages_In_Env().setValue(1);                                                                                             //Natural: MOVE 1 TO #BAR-PAGES-IN-ENV
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Envelopes().nadd(1);                                                                                                    //Natural: ADD 1 TO #BAR-ENVELOPES
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            if (condition(pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Page_Integrity_Counter().equals(7)))                                                                      //Natural: IF #BAR-PAGE-INTEGRITY-COUNTER = 7
            {
                pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Page_Integrity_Counter().setValue(0);                                                                               //Natural: MOVE 0 TO #BAR-PAGE-INTEGRITY-COUNTER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Page_Integrity_Counter().nadd(1);                                                                                   //Natural: ADD 1 TO #BAR-PAGE-INTEGRITY-COUNTER
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpabar.getPnd_Barcode_Pda_Pnd_Bar_Pages_In_Env().nadd(1);                                                                                                 //Natural: ADD 1 TO #BAR-PAGES-IN-ENV
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
