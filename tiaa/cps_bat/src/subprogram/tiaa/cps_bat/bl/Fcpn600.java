/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:07 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn600
************************************************************
**        * FILE NAME            : Fcpn600.java
**        * CLASS NAME           : Fcpn600
**        * INSTANCE NAME        : Fcpn600
************************************************************
**********************************************************************
*
* PROGRAM: FCPN600
*
* DATE   : 10/29/93
*
* PURPOSE: THIS SUBPROGRAM WILL RETURN THE CODE DESCRIPTION
*
* AUTHOR : JUN F. TINIO
*
* HISTORY:
* -------
* 091807   LCW  CHANGED VALUE RANGE FOR CNTRCT-OPTION-CDES.
*
**********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn600 extends BLNatBase
{
    // Data Areas
    private LdaFcpl600 ldaFcpl600;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Parm_Code;
    private DbsField pnd_Parm_Desc;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl600 = new LdaFcpl600();
        registerRecord(ldaFcpl600);

        // parameters
        parameters = new DbsRecord();
        pnd_Parm_Code = parameters.newFieldInRecord("pnd_Parm_Code", "#PARM-CODE", FieldType.NUMERIC, 2);
        pnd_Parm_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Desc = parameters.newFieldInRecord("pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 70);
        pnd_Parm_Desc.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl600.initializeValues();

        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn600() throws Exception
    {
        super("Fcpn600");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Parm_Desc.reset();                                                                                                                                            //Natural: RESET #PARM-DESC
        if (condition((((pnd_Parm_Code.greaterOrEqual(1) && pnd_Parm_Code.lessOrEqual(28)) || (pnd_Parm_Code.greaterOrEqual(30) && pnd_Parm_Code.lessOrEqual(31)))        //Natural: IF #PARM-CODE = 1 THRU 28 OR #PARM-CODE = 30 THRU 31 OR #PARM-CODE = 53 THRU 57
            || (pnd_Parm_Code.greaterOrEqual(53) && pnd_Parm_Code.lessOrEqual(57)))))
        {
            pnd_Parm_Desc.setValue(ldaFcpl600.getPnd_Code_Desc().getValue(pnd_Parm_Code));                                                                                //Natural: ASSIGN #PARM-DESC = #CODE-DESC ( #PARM-CODE )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
