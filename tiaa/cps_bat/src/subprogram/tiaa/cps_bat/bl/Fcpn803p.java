/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:39 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn803p
************************************************************
**        * FILE NAME            : Fcpn803p.java
**        * CLASS NAME           : Fcpn803p
**        * INSTANCE NAME        : Fcpn803p
************************************************************
************************************************************************
* SUBPROGRAM : FCPN803P
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AP" ANNUITANT STATEMENTS - POSTNET BARCODE.
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn803p extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa803p pdaFcpa803p;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Total;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws__Filler1;
    private DbsField pnd_Ws_Pnd_Total_Pos_2;
    private DbsField pnd_Ws_Pnd_Check_Digit;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws__Filler2;
    private DbsField pnd_Ws_Pnd_Check_Digit_Pos_2;
    private DbsField pnd_Ws_Pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa803p = new PdaFcpa803p(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Total = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Total", "#TOTAL", FieldType.NUMERIC, 2);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Total);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Ws_Pnd_Total_Pos_2 = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Total_Pos_2", "#TOTAL-POS-2", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Check_Digit = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Check_Digit", "#CHECK-DIGIT", FieldType.NUMERIC, 2);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Check_Digit);
        pnd_Ws__Filler2 = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Ws_Pnd_Check_Digit_Pos_2 = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Check_Digit_Pos_2", "#CHECK-DIGIT-POS-2", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn803p() throws Exception
    {
        super("Fcpn803p");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("18");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '18'
        short decideConditionsMet77 = 0;                                                                                                                                  //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PYMNT-POSTL-DATA = MASK ( NNNNNNNNN..NN )
        if (condition(DbsUtil.maskMatches(pdaFcpa803p.getPnd_Fcpa803p_Pymnt_Postl_Data(),"NNNNNNNNN..NN")))
        {
            decideConditionsMet77++;
            pnd_Ws_Pnd_Total.nadd(pdaFcpa803p.getPnd_Fcpa803p_Pnd_Zip_Pos().getValue(1,":",9));                                                                           //Natural: COMPUTE #TOTAL = #TOTAL + #ZIP-POS ( 1:9 )
            pnd_Ws_Pnd_Total.nadd(pdaFcpa803p.getPnd_Fcpa803p_Pnd_Zip_Pos().getValue(12,":",13));                                                                         //Natural: COMPUTE #TOTAL = #TOTAL + #ZIP-POS ( 12:13 )
            pnd_Ws_Pnd_Check_Digit.compute(new ComputeParameters(false, pnd_Ws_Pnd_Check_Digit), DbsField.subtract(10,pnd_Ws_Pnd_Total_Pos_2));                           //Natural: ASSIGN #CHECK-DIGIT := 10 - #TOTAL-POS-2
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "B", pdaFcpa803p.getPnd_Fcpa803p_Pnd_Zip_Pos().getValue(1,":",9),  //Natural: COMPRESS 'B' #ZIP-POS ( 1:9 ) #ZIP-POS ( 12:13 ) #CHECK-DIGIT-POS-2 'E' INTO #OUTPUT-REC-DETAIL LEAVING NO SPACE
                pdaFcpa803p.getPnd_Fcpa803p_Pnd_Zip_Pos().getValue(12,":",13), pnd_Ws_Pnd_Check_Digit_Pos_2, "E"));
        }                                                                                                                                                                 //Natural: WHEN PYMNT-POSTL-DATA = MASK ( NNNNN )
        else if (condition(DbsUtil.maskMatches(pdaFcpa803p.getPnd_Fcpa803p_Pymnt_Postl_Data(),"NNNNN")))
        {
            decideConditionsMet77++;
            pnd_Ws_Pnd_Total.nadd(pdaFcpa803p.getPnd_Fcpa803p_Pnd_Zip_Pos().getValue(1,":",5));                                                                           //Natural: COMPUTE #TOTAL = #TOTAL + #ZIP-POS ( 1:5 )
            pnd_Ws_Pnd_Check_Digit.compute(new ComputeParameters(false, pnd_Ws_Pnd_Check_Digit), DbsField.subtract(10,pnd_Ws_Pnd_Total_Pos_2));                           //Natural: ASSIGN #CHECK-DIGIT := 10 - #TOTAL-POS-2
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "B", pdaFcpa803p.getPnd_Fcpa803p_Pnd_Zip_Pos().getValue(1,":",5),  //Natural: COMPRESS 'B' #ZIP-POS ( 1:5 ) #CHECK-DIGIT-POS-2 'E' INTO #OUTPUT-REC-DETAIL LEAVING NO SPACE
                pnd_Ws_Pnd_Check_Digit_Pos_2, "E"));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
