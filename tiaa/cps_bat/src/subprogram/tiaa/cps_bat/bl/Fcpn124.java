/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:21:05 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn124
************************************************************
**        * FILE NAME            : Fcpn124.java
**        * CLASS NAME           : Fcpn124
**        * INSTANCE NAME        : Fcpn124
************************************************************
************************************************************************
* SUBPROGRAM: FCPN124
* SYSTEM    : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE     : MIT  INTERFACE
* GENERATED : 11/30/94
* FUNCTION  : UPDATE STATUS CODE AND PAYMENT DATE IN MIT FOR CPS
*           : !!! NO ET IS ISSUED !!! CALLING PROGRAM MUST ISSUE ET.
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* ______________________________________________
* 05/08/2018: ARIVU - RE-COMPILED FOR FCPA124
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn124 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa124 pdaFcpa124;
    private PdaFcpa127 pdaFcpa127;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfa8000 pdaCwfa8000;

    // Local Variables
    public DbsRecord localVariables;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfa8000 = new PdaCwfa8000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa124 = new PdaFcpa124(parameters);
        pdaFcpa127 = new PdaFcpa127(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn124() throws Exception
    {
        super("Fcpn124");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  NOT BATCH
        if (condition(Global.getDEVICE().notEquals("BATCH")))                                                                                                             //Natural: IF *DEVICE NE 'BATCH'
        {
            pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code().setValue(false);                                                                                        //Natural: MOVE FALSE TO #OK-RETURN-CODE
            pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Return_Message().setValue("SUBPROGRAM FCPN124 CAN RUN IN BATCH ONLY");                                                   //Natural: MOVE 'SUBPROGRAM FCPN124 CAN RUN IN BATCH ONLY' TO #RETURN-MESSAGE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa8000.getCwfa8000().reset();                                                                                                                                //Natural: RESET CWFA8000
        pdaCwfa8000.getCwfa8000_Action().setValue("MO");                                                                                                                  //Natural: MOVE 'MO' TO CWFA8000.ACTION
        pdaCwfa8000.getCwfa8000_System().setValue("BATCHCPS");                                                                                                            //Natural: MOVE 'BATCHCPS' TO CWFA8000.SYSTEM
        pdaCwfa8000.getCwfa8000_Wpid_Vldte_Ind().setValue("Y");                                                                                                           //Natural: MOVE 'Y' TO CWFA8000.WPID-VLDTE-IND
        pdaCwfa8000.getCwfa8000().setValuesByName(pdaFcpa124.getPnd_Mit_Read_Pda());                                                                                      //Natural: MOVE BY NAME #MIT-READ-PDA TO CWFA8000
        pdaCwfa8000.getCwfa8000_Orgnl_Unit_Cde().setValue(pdaFcpa124.getPnd_Mit_Read_Pda_Admin_Unit_Cde());                                                               //Natural: MOVE #MIT-READ-PDA.ADMIN-UNIT-CDE TO CWFA8000.ORGNL-UNIT-CDE
        pdaCwfa8000.getCwfa8000().setValuesByName(pdaFcpa127.getPnd_Mit_Update_Pda());                                                                                    //Natural: MOVE BY NAME #MIT-UPDATE-PDA TO CWFA8000
        DbsUtil.callnat(Cwfn8000.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),            //Natural: CALLNAT 'CWFN8000' USING MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB CWFA8000
            pdaCwfa8000.getCwfa8000());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero()) || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                //Natural: IF MSG-INFO-SUB.##MSG-NR NE 0 OR MSG-INFO-SUB.##RETURN-CODE = 'E'
        {
            pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code().setValue(false);                                                                                        //Natural: MOVE FALSE TO #OK-RETURN-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code().setValue(true);                                                                                         //Natural: MOVE TRUE TO #OK-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Return_Message().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                        //Natural: MOVE MSG-INFO-SUB.##MSG TO #RETURN-MESSAGE
    }

    //
}
