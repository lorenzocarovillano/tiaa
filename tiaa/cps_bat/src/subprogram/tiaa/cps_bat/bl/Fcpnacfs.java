/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:53 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnacfs
************************************************************
**        * FILE NAME            : Fcpnacfs.java
**        * CLASS NAME           : Fcpnacfs
**        * INSTANCE NAME        : Fcpnacfs
************************************************************
************************************************************************
* PROGRAM  : FCPNACFS
* SYSTEM   : CPS
* TITLE    : ALTERNATE CARRIER FACT SHEET
* FUNCTION : THIS PROGRAM WILL BUILD ALTERNATE CARRIER FACT SHEET.
*
*
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnacfs extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa378 pdaFcpa378;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Inv_Acct_Dpi_Amt;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Ws_Rec_1_Cc;
    private DbsField pnd_Ws_Pnd_Ws_Rec_1_Font;
    private DbsField pnd_Ws_Pnd_Ws_Rec_1_Detail;
    private DbsField pnd_Ws_Pnd_Ws_Xerox_Statement;
    private DbsField pnd_Ws_Pnd_Accounts;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Ppcn;
    private DbsField pnd_Ws_Pnd_Cref;
    private DbsField pnd_Ws_Pnd_Amt_Alpha;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws__Filler1;
    private DbsField pnd_Ws_Pnd_Amt_Alpha_Detail;
    private DbsField pnd_Ws_Pnd_Amt_Alpha_1;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa378 = new PdaFcpa378(parameters);
        pnd_Ws_Inv_Acct_Dpi_Amt = parameters.newFieldInRecord("pnd_Ws_Inv_Acct_Dpi_Amt", "#WS-INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Inv_Acct_Dpi_Amt.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Rec_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Ws_Rec_1);
        pnd_Ws_Pnd_Ws_Rec_1_Cc = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Ws_Rec_1_Cc", "#WS-REC-1-CC", FieldType.STRING, 1);
        pnd_Ws_Pnd_Ws_Rec_1_Font = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Ws_Rec_1_Font", "#WS-REC-1-FONT", FieldType.STRING, 1);
        pnd_Ws_Pnd_Ws_Rec_1_Detail = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Ws_Rec_1_Detail", "#WS-REC-1-DETAIL", FieldType.STRING, 141);
        pnd_Ws_Pnd_Ws_Xerox_Statement = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Xerox_Statement", "#WS-XEROX-STATEMENT", FieldType.STRING, 72);
        pnd_Ws_Pnd_Accounts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accounts", "#ACCOUNTS", FieldType.STRING, 19);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Accounts);
        pnd_Ws_Pnd_Ppcn = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Ppcn", "#PPCN", FieldType.STRING, 9);
        pnd_Ws_Pnd_Cref = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Cref", "#CREF", FieldType.STRING, 10);
        pnd_Ws_Pnd_Amt_Alpha = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Alpha", "#AMT-ALPHA", FieldType.STRING, 14);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Amt_Alpha);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Ws_Pnd_Amt_Alpha_Detail = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Amt_Alpha_Detail", "#AMT-ALPHA-DETAIL", FieldType.STRING, 13);
        pnd_Ws_Pnd_Amt_Alpha_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Alpha_1", "#AMT-ALPHA-1", FieldType.STRING, 14);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_Pnd_Ws_Xerox_Statement.setInitialValue(" $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=ACF00?,FORMS=ACF00?,FEED=BOND,END;");
        pnd_Ws_Pnd_Amt_Alpha.setInitialValue("$");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnacfs() throws Exception
    {
        super("Fcpnacfs");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pdaFcpa378.getPnd_Record_Type_40_Pnd_Good_Letter().getBoolean()))                                                                                   //Natural: IF #GOOD-LETTER
        {
                                                                                                                                                                          //Natural: PERFORM GENERATE-GOOD-ACFS
            sub_Generate_Good_Acfs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM GENERATE-BAD-ACFS
            sub_Generate_Bad_Acfs();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-GOOD-ACFS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-BAD-ACFS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-COMMON
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-AMT
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-PLAN-TYPE
    }
    private void sub_Generate_Good_Acfs() throws Exception                                                                                                                //Natural: GENERATE-GOOD-ACFS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        setValueToSubstring("2",pnd_Ws_Pnd_Ws_Xerox_Statement,43,1);                                                                                                      //Natural: MOVE '2' TO SUBSTR ( #WS-XEROX-STATEMENT,43,1 )
        setValueToSubstring("2",pnd_Ws_Pnd_Ws_Xerox_Statement,56,1);                                                                                                      //Natural: MOVE '2' TO SUBSTR ( #WS-XEROX-STATEMENT,56,1 )
                                                                                                                                                                          //Natural: PERFORM GENERATE-COMMON
        sub_Generate_Common();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_40_Plan_Employer_Name());                                                                       //Natural: MOVE PLAN-EMPLOYER-NAME TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
                                                                                                                                                                          //Natural: PERFORM GENERATE-PLAN-TYPE
        sub_Generate_Plan_Type();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_40_Plan_Cash_Avail());                                                                          //Natural: MOVE PLAN-CASH-AVAIL TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Cntrct_Ivc_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                     //Natural: MOVE EDITED CNTRCT-IVC-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Ws_Rec_1_Cc.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO #WS-REC-1-CC
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pnd_Ws_Inv_Acct_Dpi_Amt,new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                                               //Natural: MOVE EDITED #WS-INV-ACCT-DPI-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Employer_Cntrb(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                //Natural: MOVE EDITED PLAN-EMPLOYER-CNTRB ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Employer_Cntrb_Earn(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                           //Natural: MOVE EDITED PLAN-EMPLOYER-CNTRB-EARN ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Employee_Rdctn(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                //Natural: MOVE EDITED PLAN-EMPLOYEE-RDCTN ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Employee_Rdctn_Earn(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                           //Natural: MOVE EDITED PLAN-EMPLOYEE-RDCTN-EARN ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Employee_Ddctn(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                //Natural: MOVE EDITED PLAN-EMPLOYEE-DDCTN ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Employee_Ddctn_Earn(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                           //Natural: MOVE EDITED PLAN-EMPLOYEE-DDCTN-EARN ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Acc_123186(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                    //Natural: MOVE EDITED PLAN-ACC-123186 ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Acc_123188(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                    //Natural: MOVE EDITED PLAN-ACC-123188 ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Acc_123188_Rdctn(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                              //Natural: MOVE EDITED PLAN-ACC-123188-RDCTN ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Plan_Acc_123188_Rdctn_Earn(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                         //Natural: MOVE EDITED PLAN-ACC-123188-RDCTN-EARN ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
                                                                                                                                                                          //Natural: PERFORM CONVERT-AMT
        sub_Convert_Amt();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Generate_Bad_Acfs() throws Exception                                                                                                                 //Natural: GENERATE-BAD-ACFS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        setValueToSubstring("1",pnd_Ws_Pnd_Ws_Xerox_Statement,43,1);                                                                                                      //Natural: MOVE '1' TO SUBSTR ( #WS-XEROX-STATEMENT,43,1 )
        setValueToSubstring("1",pnd_Ws_Pnd_Ws_Xerox_Statement,56,1);                                                                                                      //Natural: MOVE '1' TO SUBSTR ( #WS-XEROX-STATEMENT,56,1 )
                                                                                                                                                                          //Natural: PERFORM GENERATE-COMMON
        sub_Generate_Common();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GENERATE-PLAN-TYPE
        sub_Generate_Plan_Type();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Generate_Common() throws Exception                                                                                                                   //Natural: GENERATE-COMMON
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Ws_Pnd_Ws_Rec_1.setValue(pnd_Ws_Pnd_Ws_Xerox_Statement);                                                                                                      //Natural: MOVE #WS-XEROX-STATEMENT TO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Cc.setValue("1");                                                                                                                             //Natural: MOVE '1' TO #WS-REC-1-CC
        pnd_Ws_Pnd_Ws_Rec_1_Font.setValue("1");                                                                                                                           //Natural: MOVE '1' TO #WS-REC-1-FONT
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValueEdited(pdaFcpa378.getPnd_Record_Type_40_Pymnt_Check_Dte(),new ReportEditMask("LLLLLLLLL�ZD,�YYYY"));                           //Natural: MOVE EDITED #RECORD-TYPE-40.PYMNT-CHECK-DTE ( EM = L ( 9 ) �ZD,�YYYY ) TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Addr_Line1_Txt());                                                                     //Natural: MOVE PYMNT-ADDR-LINE1-TXT TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Cc.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO #WS-REC-1-CC
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Addr_Line2_Txt());                                                                     //Natural: MOVE PYMNT-ADDR-LINE2-TXT TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Addr_Line3_Txt());                                                                     //Natural: MOVE PYMNT-ADDR-LINE3-TXT TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Addr_Line4_Txt());                                                                     //Natural: MOVE PYMNT-ADDR-LINE4-TXT TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Addr_Line5_Txt());                                                                     //Natural: MOVE PYMNT-ADDR-LINE5-TXT TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Addr_Line6_Txt());                                                                     //Natural: MOVE PYMNT-ADDR-LINE6-TXT TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Cc.setValue("1");                                                                                                                             //Natural: MOVE '1' TO #WS-REC-1-CC
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Nme());                                                                                //Natural: MOVE PYMNT-NME TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Accounts.setValueEdited(pdaFcpa378.getPnd_Record_Type_10_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                           //Natural: MOVE EDITED #RECORD-TYPE-10.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #ACCOUNTS
        if (condition(pdaFcpa378.getPnd_Record_Type_10_Cntrct_Cref_Nbr().notEquals(" ")))                                                                                 //Natural: IF #RECORD-TYPE-10.CNTRCT-CREF-NBR NE ' '
        {
            pnd_Ws_Pnd_Cref.setValueEdited(pdaFcpa378.getPnd_Record_Type_10_Cntrct_Cref_Nbr(),new ReportEditMask("//XXXXXXX-X"));                                         //Natural: MOVE EDITED #RECORD-TYPE-10.CNTRCT-CREF-NBR ( EM = //XXXXXXX-X ) TO #CREF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pnd_Ws_Pnd_Accounts);                                                                                                         //Natural: MOVE #ACCOUNTS TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Amt_Alpha_1.setValueEdited(pdaFcpa378.getPnd_Record_Type_10_Pymnt_Check_Amt(),new ReportEditMask("-Z,ZZZ,ZZ9.99"));                                    //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = -Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA-1
        pnd_Ws_Pnd_Amt_Alpha_Detail.setValue(pnd_Ws_Pnd_Amt_Alpha_1, MoveOption.LeftJustified);                                                                           //Natural: MOVE LEFT #AMT-ALPHA-1 TO #AMT-ALPHA-DETAIL
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pnd_Ws_Pnd_Amt_Alpha);                                                                                                        //Natural: MOVE #AMT-ALPHA TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_30_Pymnt_Eft_Acct_Nbr());                                                                       //Natural: MOVE PYMNT-EFT-ACCT-NBR TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
    }
    private void sub_Convert_Amt() throws Exception                                                                                                                       //Natural: CONVERT-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Amt_Alpha_1), new ExamineSearch("+"), new ExamineReplace("$"));                                                      //Natural: EXAMINE #AMT-ALPHA-1 FOR '+' REPLACE '$'
        pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pnd_Ws_Pnd_Amt_Alpha_1);                                                                                                      //Natural: MOVE #AMT-ALPHA-1 TO #WS-REC-1-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
    }
    private void sub_Generate_Plan_Type() throws Exception                                                                                                                //Natural: GENERATE-PLAN-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaFcpa378.getPnd_Record_Type_40_Pnd_Plan_Type_Display().getBoolean() && pdaFcpa378.getPnd_Record_Type_40_Plan_Type().notEquals(" ")))              //Natural: IF #PLAN-TYPE-DISPLAY AND PLAN-TYPE NE ' '
        {
            pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue(pdaFcpa378.getPnd_Record_Type_40_Plan_Type());                                                                            //Natural: MOVE PLAN-TYPE TO #WS-REC-1-DETAIL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Ws_Rec_1_Detail.setValue("UNKNOWN");                                                                                                               //Natural: MOVE 'UNKNOWN' TO #WS-REC-1-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
    }

    //
}
