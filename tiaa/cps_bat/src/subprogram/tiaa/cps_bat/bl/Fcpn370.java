/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:54 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn370
************************************************************
**        * FILE NAME            : Fcpn370.java
**        * CLASS NAME           : Fcpn370
**        * INSTANCE NAME        : Fcpn370
************************************************************
************************************************************************
* PROGRAM  : FCPN370
* SYSTEM   : CPS
* TITLE    : NOTIFICATION TEXT
* DATE     : FEB 1994
* FUNCTION : THIS PROGRAM PRINTS THE NOTIFICATION TEXT TO A RECEIPIANT
*            OF A MONTHLY SETTLEMENT CHECK.
*            THE SUBPROGRAM IS PART OF THE CHECK AND SETTLEMENT PRINTING
*            PROCESS AND IS INVOKED BY THE PRINTING DRIVER AS NEEDED.
**----------------------------------------------------------------------
* HISTORY
*
* >
* NOTES: MCGEE 05/00 ADD SPIA PA-SELECT
* ----------------------------------------------------------------------
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn370 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ws_Cntrct_Insurance_Option;
    private DbsField pnd_Ws_Cntrct_Life_Contingency;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_It_Is_An_Rtb_Payment;
    private DbsField pnd_J;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Annty_Ins_Type = parameters.newFieldInRecord("pnd_Ws_Cntrct_Annty_Ins_Type", "#WS-CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Cntrct_Annty_Ins_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Annty_Type_Cde = parameters.newFieldInRecord("pnd_Ws_Cntrct_Annty_Type_Cde", "#WS-CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        pnd_Ws_Cntrct_Annty_Type_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Insurance_Option = parameters.newFieldInRecord("pnd_Ws_Cntrct_Insurance_Option", "#WS-CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1);
        pnd_Ws_Cntrct_Insurance_Option.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Life_Contingency = parameters.newFieldInRecord("pnd_Ws_Cntrct_Life_Contingency", "#WS-CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1);
        pnd_Ws_Cntrct_Life_Contingency.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_It_Is_An_Rtb_Payment = localVariables.newFieldInRecord("pnd_It_Is_An_Rtb_Payment", "#IT-IS-AN-RTB-PAYMENT", FieldType.BOOLEAN, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_Rec_1.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn370() throws Exception
    {
        super("Fcpn370");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        //*  SURVIVOR BENEFITS, PROTOTYPE #4
        short decideConditionsMet228 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE 'PP'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("PP"))))
        {
            decideConditionsMet228++;
            //*  VACATION ADDR
            if (condition(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals(" ")))                                                              //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR ( 1 ) EQ ' '
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("1!In accordance with your instructions, your payment has", "been sent to"));                                      //Natural: COMPRESS '1!In accordance with your instructions, your payment has' 'been sent to' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !this address.  A breakdown of this payment has been sent", "to your"));                                         //Natural: COMPRESS ' !this address.  A breakdown of this payment has been sent' 'to your' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !residence address."));                                                                                          //Natural: COMPRESS ' !residence address.' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                //*  BANK ADDRESS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("1!The enclosed check(s) includes all annuity payments due", "through the current"));                              //Natural: COMPRESS '1!The enclosed check(s) includes all annuity payments due' 'through the current' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !month. Each future annuity check will be mailed in time to be", "deposited to"));                               //Natural: COMPRESS ' !month. Each future annuity check will be mailed in time to be' 'deposited to' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !our annuitant's account on or before the first day of the", "month for which"));                                //Natural: COMPRESS ' !our annuitant"s account on or before the first day of the' 'month for which' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !it is due."));                                                                                                  //Natural: COMPRESS ' !it is due.' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                //*   LUMP SUM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L"))))
        {
            decideConditionsMet228++;
            //* *  IF #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR(1) EQ ' ' /* VACATION ADDR
            //*  VAC. ADDR
            if (condition(! (DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1),"'CR '"))))                                                  //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-NME ( 1 ) NE MASK ( 'CR ' )
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("1!In accordance with your instructions, your payment has", "been sent to"));                                      //Natural: COMPRESS '1!In accordance with your instructions, your payment has' 'been sent to' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !this address.  A breakdown of this payment has been sent", "to your"));                                         //Natural: COMPRESS ' !this address.  A breakdown of this payment has been sent' 'to your' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !residence address."));                                                                                          //Natural: COMPRESS ' !residence address.' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                //*  BANK ADDRESS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("1!This individual has requested that we mail the attached", "check to you."));                                    //Natural: COMPRESS '1!This individual has requested that we mail the attached' 'check to you.' INTO #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " !It is to be credited to Account No. ", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1),  //Natural: COMPRESS ' !It is to be credited to Account No. ' PYMNT-EFT-ACCT-NBR ( 1 ) '.' INTO #WS-REC-1 LEAVING NO SPACE
                    "."));
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("S") || pnd_Ws_Cntrct_Annty_Ins_Type.equals("M")))                                                      //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
                {
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !any questions, please call our Planning & Service Center", "toll free at 1 800 223-1200 M-F 8am-8pm ET.")); //Natural: COMPRESS ' !any questions, please call our Planning & Service Center' 'toll free at 1 800 223-1200 M-F 8am-8pm ET.' INTO #WS-REC-1
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                    pnd_Ws_Rec_1.reset();                                                                                                                                 //Natural: RESET #WS-REC-1
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !This contract has been issued by", "TIAA-CREF Life Insurance Company."));                                   //Natural: COMPRESS ' !This contract has been issued by' 'TIAA-CREF Life Insurance Company.' INTO #WS-REC-1
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !If you have any questions, please call our Telephone", "Counseling Center"));                               //Natural: COMPRESS ' !If you have any questions, please call our Telephone' 'Counseling Center' INTO #WS-REC-1
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                    pnd_Ws_Rec_1.setValue(DbsUtil.compress(" !toll free at 1 800 842-2776."));                                                                            //Natural: COMPRESS ' !toll free at 1 800 842-2776.' INTO #WS-REC-1
                    getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                         //Natural: WRITE WORK FILE 8 #WS-REC-1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Rec_1.setValue(" !");                                                                                                                              //Natural: ASSIGN #WS-REC-1 = ' !'
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet228 > 0))
        {
            pnd_Ws_Rec_1.setValue(" !");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' !'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" !");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' !'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" !Benefit Payment Services");                                                                                                          //Natural: ASSIGN #WS-REC-1 = ' !Benefit Payment Services'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E02 - Unexpected CNTRCT-TYPE-CDE","=",pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde());                       //Natural: WRITE *PROGRAM 'E02 - Unexpected CNTRCT-TYPE-CDE' '=' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
