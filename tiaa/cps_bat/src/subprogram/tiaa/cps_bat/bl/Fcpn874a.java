/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:45 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn874a
************************************************************
**        * FILE NAME            : Fcpn874a.java
**        * CLASS NAME           : Fcpn874a
**        * INSTANCE NAME        : Fcpn874a
************************************************************
************************************************************************
* SUBPROGRAM : FCPN874A
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "NZ" ANNUITANT STATEMENTS - FORMAT ADDRESS LINES
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn874a extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa874a pdaFcpa874a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Start;
    private DbsField pnd_Ws_Pnd_End;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa874a = new PdaFcpa874a(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Start", "#START", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_End", "#END", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn874a() throws Exception
    {
        super("Fcpn874a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()).getSubstring(1,3).equals("CR ")))                        //Natural: IF SUBSTR ( PYMNT-NME ( #ADDR-IND ) ,1,3 ) = 'CR '
        {
            pdaFcpa874a.getPnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme().setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind(), //Natural: MOVE PYMNT-ADDR-LINE-TXT ( #ADDR-IND,1 ) TO #STMNT-PYMNT-NME
                1));
            pnd_Ws_Pnd_Start.setValue(2);                                                                                                                                 //Natural: ASSIGN #START := 2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa874a.getPnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme().setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()));       //Natural: MOVE PYMNT-NME ( #ADDR-IND ) TO #STMNT-PYMNT-NME
            pnd_Ws_Pnd_Start.setValue(1);                                                                                                                                 //Natural: ASSIGN #START := 1
        }                                                                                                                                                                 //Natural: END-IF
        setValueToSubstring(pdaFcpa874a.getPnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme(),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),7,38);                                           //Natural: MOVE #STMNT-PYMNT-NME TO SUBSTR ( #OUTPUT-REC,7,38 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 7");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 7'
        pnd_Ws_Pnd_End.compute(new ComputeParameters(false, pnd_Ws_Pnd_End), pnd_Ws_Pnd_Start.add(4));                                                                    //Natural: ASSIGN #END := #START + 4
        FOR01:                                                                                                                                                            //Natural: FOR #I = #START TO #END
        for (pnd_Ws_Pnd_I.setValue(pnd_Ws_Pnd_Start); condition(pnd_Ws_Pnd_I.lessOrEqual(pnd_Ws_Pnd_End)); pnd_Ws_Pnd_I.nadd(1))
        {
            setValueToSubstring(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind(),pnd_Ws_Pnd_I),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(), //Natural: MOVE PYMNT-ADDR-LINE-TXT ( #ADDR-IND,#I ) TO SUBSTR ( #OUTPUT-REC,7,35 )
                7,35);
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().notEquals(" ")))                                                                              //Natural: IF #OUTPUT-REC-DETAIL NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
