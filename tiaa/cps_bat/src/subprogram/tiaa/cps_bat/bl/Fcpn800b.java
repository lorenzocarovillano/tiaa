/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:30 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn800b
************************************************************
**        * FILE NAME            : Fcpn800b.java
**        * CLASS NAME           : Fcpn800b
**        * INSTANCE NAME        : Fcpn800b
************************************************************
************************************************************************
* PROGRAM   : FCPP800B
* SYSTEM    : CPS
* TITLE     : CPS CONTROL RECORD REPORT
* GENERATED :
* FUNCTION  :
*           :
* HISTORY   : 06/06/96
*           : REMOVE THE COUNTS OF CONVERTED EFTS AND GLOBAL PAYS
*           : (TO CHECKS). CONVERSION IS NOW DONE BY IA.
*           : 07/07/96
*           : PRINT TOTALS OF DIVIDEND TO COLLEGES
*           :
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn800b extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa800a pdaFcpa800a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Total_Lit;
    private DbsField pnd_Index;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa800a = new PdaFcpa800a(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Total_Lit = localVariables.newFieldInRecord("pnd_Total_Lit", "#TOTAL-LIT", FieldType.STRING, 20);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn800b() throws Exception
    {
        super("Fcpn800b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************                                                                                       //Natural: FORMAT ( 02 ) PS = 58 LS = 132 ZP = OFF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE *PROGRAM 52T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 58T 'ANNUITY PAYMENTS' 124T *TIMX ( EM = HH:II' 'AP ) / #PROGRAM-LIT / #CPS-TOTALS.#PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        FOR01:                                                                                                                                                            //Natural: FOR #INDEX 10 13
        for (pnd_Index.setValue(10); condition(pnd_Index.lessOrEqual(13)); pnd_Index.nadd(1))
        {
            short decideConditionsMet81 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF #INDEX;//Natural: VALUE 10
            if (condition((pnd_Index.equals(10))))
            {
                decideConditionsMet81++;
                pnd_Total_Lit.setValue("Checks          ");                                                                                                               //Natural: MOVE 'Checks          ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 11
            else if (condition((pnd_Index.equals(11))))
            {
                decideConditionsMet81++;
                pnd_Total_Lit.setValue("EFTs            ");                                                                                                               //Natural: MOVE 'EFTs            ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 12
            else if (condition((pnd_Index.equals(12))))
            {
                decideConditionsMet81++;
                pnd_Total_Lit.setValue("Global Pay      ");                                                                                                               //Natural: MOVE 'Global Pay      ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 13
            else if (condition((pnd_Index.equals(13))))
            {
                decideConditionsMet81++;
                pnd_Total_Lit.setValue("Int. Rollover   ");                                                                                                               //Natural: MOVE 'Int. Rollover   ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(2, " ",                                                                                                                                  //Natural: DISPLAY ( 02 ) ' ' #TOTAL-LIT ( AL = 16 ) 'Record/Count' #CPS-TOTALS.#REC-COUNT ( 2, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9 ) 'Contract/Amount' #CPS-TOTALS.#CNTRCT-AMT ( 2, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 'Dividend/Amount' #CPS-TOTALS.#DVDND-AMT ( 2, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 'REA-CREF/AMOUNT' #CPS-TOTALS.#CREF-AMT ( 2, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 'Gross/Amount' #CPS-TOTALS.#GROSS-AMT ( 2, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 'Deduction/Amount' #CPS-TOTALS.#DED-AMT ( 2, #INDEX ) ( EM = ZZ,ZZZ,ZZ9.99 ) 'Tax/Amount' #CPS-TOTALS.#TAX-AMT ( 2, #INDEX ) ( EM = ZZ,ZZZ,ZZ9.99 ) 'Net/Amount' #CPS-TOTALS.#NET-AMT ( 2, #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            		pnd_Total_Lit, new AlphanumericLength (16),"Record/Count",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(2,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),"Contract/Amount",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(2,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"Dividend/Amount",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"REA-CREF/AMOUNT",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(2,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"Gross/Amount",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(2,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"Deduction/Amount",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(2,pnd_Index), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"Tax/Amount",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,pnd_Index), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"Net/Amount",
            		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(2,pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"Dividend to colleges:",NEWLINE,NEWLINE,"Checks",new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,10)),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Chk_Coll_Dvdnd_Amt(),  //Natural: WRITE ( 02 ) /// 'Dividend to colleges:' // 'Checks' T*#CPS-TOTALS.#DVDND-AMT ( 2, 10 ) #CPS-TOTALS.#CHK-COLL-DVDND-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) / 'EFTs' T*#CPS-TOTALS.#DVDND-AMT ( 2, 10 ) #CPS-TOTALS.#EFT-COLL-DVDND-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) / 'Global Pay' T*#CPS-TOTALS.#DVDND-AMT ( 2, 10 ) #CPS-TOTALS.#GP-COLL-DVDND-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) // 'Internal Rollovers' T*#CPS-TOTALS.#DVDND-AMT ( 2, 10 ) #CPS-TOTALS.#IR-COLL-DVDND-AMT ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"EFTs",new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,10)),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Eft_Coll_Dvdnd_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"Global Pay",new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,10)),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gp_Coll_Dvdnd_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"Internal Rollovers",new ReportTAsterisk(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,10)),pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ir_Coll_Dvdnd_Amt(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(2, "PS=58 LS=132 ZP=OFF");

        getReports().write(2, ReportOption.TITLE,Global.getPROGRAM(),new TabSetting(52),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(58),"ANNUITY PAYMENTS",new TabSetting(124),Global.getTIMX(), 
            new ReportEditMask ("HH:II' 'AP"),NEWLINE,pdaFcpa800a.getPnd_Cps_Totals_Pnd_Program_Lit(),NEWLINE,pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, " ",
        		pnd_Total_Lit, new AlphanumericLength (16),"Record/Count",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),"Contract/Amount",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"Dividend/Amount",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"REA-CREF/AMOUNT",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"Gross/Amount",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"Deduction/Amount",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"Tax/Amount",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt(), new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),"Net/Amount",
        		pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
    }
}
