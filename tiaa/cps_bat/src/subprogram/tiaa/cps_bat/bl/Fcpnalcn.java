/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:59 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnalcn
************************************************************
**        * FILE NAME            : Fcpnalcn.java
**        * CLASS NAME           : Fcpnalcn
**        * INSTANCE NAME        : Fcpnalcn
************************************************************
***********************************************************************
* PROGRAM  : FCPNALCN
* SYSTEM   : CPS
* DATE     : 07/17/98
* PROG.    : RIAD LOUTFI
* TITLE    : DETERMINE ACCUMULATION SUBSCRIPTS FOR ANNUITY LOAN "AL".
* HISTORY
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*  11/11/99 - LEON GURTOVNIK
*             CHANGE  ERROR REPORT WRITING FROM 15 TO 16
*  11/29/99 - USE ELEMENT 43 OF THE CONTROL RECORD FOR EW SUSPENSE
*
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnalcn extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpanzcn pdaFcpanzcn;
    private PdaFcpanzc1 pdaFcpanzc1;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Error;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpacrpt = new PdaFcpacrpt(parameters);
        pdaFcpanzcn = new PdaFcpanzcn(parameters);
        pdaFcpanzc1 = new PdaFcpanzc1(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Error = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnalcn() throws Exception
    {
        super("Fcpnalcn");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: DECIDE ON FIRST VALUE OF PYMNT-PAY-TYPE-REQ-IND
        short decideConditionsMet50 = 0;                                                                                                                                  //Natural: VALUE 1
        if (condition((pdaFcpanzcn.getPnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet50++;
            //*  LEON 'CN' 08-05-99
            //*  LEON 'SN' 08-05-99
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            short decideConditionsMet56 = 0;                                                                                                                              //Natural: VALUE ' ','R'
            if (condition((pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
            {
                decideConditionsMet56++;
                pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().setValue(48);                                                                                             //Natural: ASSIGN #ACCUM-OCCUR-2 := 48
            }                                                                                                                                                             //Natural: VALUE 'C', 'CN'
            else if (condition((pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
            {
                decideConditionsMet56++;
                pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().setValue(44);                                                                                             //Natural: ASSIGN #ACCUM-OCCUR-2 := 44
            }                                                                                                                                                             //Natural: VALUE 'S', 'SN'
            else if (condition((pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
            {
                decideConditionsMet56++;
                pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().setValue(45);                                                                                             //Natural: ASSIGN #ACCUM-OCCUR-2 := 45
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
                sub_Error_Processing();
                if (condition(Global.isEscape())) {return;}
                //*  EW SUSPENSE ONLY
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pdaFcpanzcn.getPnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet50++;
            pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().setValue(49);                                                                                                 //Natural: ASSIGN #ACCUM-OCCUR-2 := 49
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pdaFcpanzcn.getPnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind().equals(6))))
        {
            decideConditionsMet50++;
            if (condition(pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Orgn_Cde().equals("EW")))                                                                                    //Natural: IF CNTRCT-ORGN-CDE = 'EW'
            {
                pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().setValue(43);                                                                                             //Natural: ASSIGN #ACCUM-OCCUR-2 := 43
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
                sub_Error_Processing();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pdaFcpanzcn.getPnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind().equals(8))))
        {
            decideConditionsMet50++;
            pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().setValue(50);                                                                                                 //Natural: ASSIGN #ACCUM-OCCUR-2 := 50
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-PROCESSING
            sub_Error_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Error.getBoolean() && ! (pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().getBoolean())))                                                      //Natural: IF #ERROR AND NOT #NO-ABEND
        {
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-PROCESSING
        //* *---------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
    }
    private void sub_Error_Processing() throws Exception                                                                                                                  //Natural: ERROR-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Error.setValue(true);                                                                                                                                  //Natural: ASSIGN #ERROR := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        getReports().write(16, "***",new TabSetting(25),"ERROR OCCURED IN THE CONTROL PROCESS",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"UNKNOWN DATA FOR CONTROL PURPOSES",new  //Natural: WRITE ( 16 ) '***' 25T 'ERROR OCCURED IN THE CONTROL PROCESS' 77T '***' / '***' 25T 'UNKNOWN DATA FOR CONTROL PURPOSES' 77T '***' / '***' 25T 'ORGIN CODE..:' CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'C & R.......:' CNTRCT-CANCEL-RDRW-ACTVTY-CDE 77T '***' / '***' 25T 'PAYMENT TYPE:' PYMNT-PAY-TYPE-REQ-IND 77T '***' / '***' 25T 'ROLLOVER    :' CNTRCT-ROLL-DEST-CDE 77T '***' / '***' 25T 'PAYMENT AMT :' PYMNT-CHECK-AMT ( EM = -Z,ZZZ,ZZ9.99 ) 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORGIN CODE..:",pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Orgn_Cde(),new TabSetting(77),"***",NEWLINE,"***",new 
            TabSetting(25),"C & R.......:",pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PAYMENT TYPE:",pdaFcpanzcn.getPnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind(),new 
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ROLLOVER    :",pdaFcpanzcn.getPnd_Fcpanzcn_Cntrct_Roll_Dest_Cde(),new TabSetting(77),"***",NEWLINE,"***",new 
            TabSetting(25),"PAYMENT AMT :",pdaFcpanzcn.getPnd_Fcpanzcn_Pymnt_Check_Amt(), new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new TabSetting(77),"***");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(16));                                                                                                                //Natural: NEWPAGE ( 16 )
        if (condition(Global.isEscape())){return;}
        getReports().write(16, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new //Natural: WRITE ( 16 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(16, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new         //Natural: WRITE ( 16 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
