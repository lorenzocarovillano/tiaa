/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:32 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn970
************************************************************
**        * FILE NAME            : Fcpn970.java
**        * CLASS NAME           : Fcpn970
**        * INSTANCE NAME        : Fcpn970
************************************************************
***********************************************************************
* PROGRAM:  FCPN970
*
* TITLE:    CPS DEATH CLAIMS STATEMENT GENERATION
* AUTHOR:   FRANCIS ENDAYA
* DESC:     THE PROGRAM READS CHECK FILE AND CREATE XML FILE TO BE SENT
*           OVER TO CCP FOR CHECK CREATION FOR DC CHECKS AND EFT
******************************** FCPN372
* MODIFICATION LOG
* 2016/11/01 F.ENDAYA  NEW
*
* 2017/07/15 F.ENDAYA  HOLD CODE FOR LOCAL PRINTING TO REMOVE HARD
*              CODING AND USE REFERENCE TABLE INSTEAD. FE201707
* 2017/08/12 F.ENDAYA  PIN EXPANSION. FE201708
* 2017/10/11 J.GHOSH   FOR IA PAYMENTS THE CANADIAN ADDRESS POPULATING
*                      AS 'U' INSTEAD OF 'F' WHEN STATEMENTS GETTING
*                      PRINTED VIA CCP. INC3925506          /* JG171011
*
* 01/2018  J.OSTEEN : FIX TO ELIMINATE BAD CHARACTERS FROM ADDRESS JWO1
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn970 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;
    private PdaFcpa110 pdaFcpa110;
    private LdaFcpl961 ldaFcpl961;
    private PdaCpwapytp pdaCpwapytp;
    private PdaCpoaorgn pdaCpoaorgn;
    private PdaTbldcoda pdaTbldcoda;
    private LdaFcpltbcd ldaFcpltbcd;
    private PdaFcpamode pdaFcpamode;
    private PdaFcpaoptn pdaFcpaoptn;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl893g ldaFcpl893g;
    private LdaFcpl803l ldaFcpl803l;
    private LdaFcpl893l ldaFcpl893l;
    private PdaFcppda_M pdaFcppda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ws_Cntrct_Insurance_Option;
    private DbsField pnd_Ws_Cntrct_Life_Contingency;
    private DbsField pnd_Batch_Cnt;
    private DbsField pnd_Save_First_Twenty;
    private DbsField pnd_First_Record;
    private DbsField pnd_Last_Record;
    private DbsField pnd_Invalid_Rec;
    private DbsField pnd_Hold_Seq;
    private DbsField pnd_I;
    private DbsField pnd_I3;
    private DbsField pnd_J;
    private DbsField pnd_D;
    private DbsField pnd_Record_Cnt;
    private DbsField pnd_Check_Cnt;
    private DbsField pnd_Check_Amt;
    private DbsField pnd_Batch_Counter;
    private DbsField pnd_Batch_Total;
    private DbsField pnd_Current_Check;
    private DbsField pnd_Payment_Cnt;
    private DbsField pnd_Eft_Cnt;
    private DbsField pnd_Eft_Amt;
    private DbsField pnd_Other_Cnt;
    private DbsField pnd_Other_Amt;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Ded_Amt;
    private DbsField pnd_Blank_Rerun;
    private DbsField pnd_Xml_Line;
    private DbsField pnd_Bank_Source_Code;
    private DbsField pnd_Charlotte_Printer;
    private DbsField pnd_Denver_Printer;
    private DbsField pnd_Dbl_Quote;
    private DbsField pnd_Total_Tax_Amt;
    private DbsField pnd_Tax_Amt;
    private DbsField pnd_Payment_Amt;
    private DbsField pnd_Total_Payment_Amt;
    private DbsField pnd_Total_Net_Amt;
    private DbsField pnd_Total_Net_Amt2;
    private DbsField pnd_Total_Fund_Net_Amt;
    private DbsField pnd_Total_Gross_Amt;
    private DbsField pnd_Federal_Tax_Amt;
    private DbsField pnd_State_Tax_Amt;
    private DbsField pnd_Local_Tax_Amt;
    private DbsField pnd_Can_Tax_Amt;
    private DbsField pnd_Federal_Tax_Amt2;
    private DbsField pnd_State_Tax_Amt2;
    private DbsField pnd_Local_Tax_Amt2;
    private DbsField pnd_Can_Tax_Amt2;
    private DbsField pnd_Total_Net_Amt_Trunc;
    private DbsField pnd_Total_Fund_Net_Amt_Trunc;
    private DbsField pnd_Total_Ded;
    private DbsField pnd_Inv_Id;
    private DbsField pnd_Ndx;
    private DbsField pnd_Origin_Description;
    private DbsField pnd_Omni_Check_Date;
    private DbsField pnd_Debug;
    private DbsField pnd_Formatted_Zip;
    private DbsField pnd_Post_Num;

    private DbsGroup pnd_Post_Num__R_Field_1;
    private DbsField pnd_Post_Num_Pnd_Post_Num_A;
    private DbsField pnd_Hold_Post_Num;

    private DbsGroup pnd_Hold_Post_Num__R_Field_2;
    private DbsField pnd_Hold_Post_Num_Pnd_Hold_Post_Num_A;
    private DbsField pnd_Last_Name;
    private DbsField pnd_First_Name;
    private DbsField pnd_Middle_Name;
    private DbsField pnd_Rest_Of_Name;
    private DbsField pnd_Other;
    private DbsField pnd_Other2;
    private DbsField pnd_Other3;
    private DbsField pnd_Cnt;
    private DbsField pnd_Inv_Acct_Ticker_Hold;

    private DbsGroup pnd_Inv_Acct_Ticker_Hold__R_Field_3;
    private DbsField pnd_Inv_Acct_Ticker_Hold_Alpha;
    private DbsField pnd_Inv_Acct_Ticker_Hold_Pnd_Seq_Nbr;
    private DbsField pnd_Seq_Number;
    private DbsField pnd_Insndx;
    private DbsField pnd_Ndx_Fund;
    private DbsField pnd_Ndx_Ded;
    private DbsField pnd_Pos_Ded;
    private DbsField pnd_Wk_Acct_Cde_A;

    private DbsGroup pnd_Wk_Acct_Cde_A__R_Field_4;
    private DbsField pnd_Wk_Acct_Cde_A_Pnd_Wk_Acct_Cde;
    private DbsField pnd_Ded_Desc_Tab;

    private DbsGroup pnd_Hold_Addr;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Nme;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Addr_Line1_Txt;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Addr_Line2_Txt;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Addr_Line3_Txt;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Addr_Line4_Txt;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Addr_Line5_Txt;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Addr_Line6_Txt;
    private DbsField pnd_Hold_Addr_Pnd_Hold_Addr_Zip_Cde;
    private DbsField pnd_Hold_Addr_Pnd_Hold_For_Dom;
    private DbsField hold_Cntrct_Orgn_Cde;
    private DbsField pnd_Time;

    private DataAccessProgramView vw_rt;
    private DbsField rt_Rt_A_I_Ind;
    private DbsField rt_Rt_Table_Id;
    private DbsField rt_Rt_Short_Key;
    private DbsField rt_Rt_Long_Key;
    private DbsField rt_Rt_Desc1;
    private DbsField rt_Rt_Desc2;
    private DbsField rt_Rt_Desc3;
    private DbsField pnd_Rt_Super1;

    private DbsGroup pnd_Rt_Super1__R_Field_5;
    private DbsField pnd_Rt_Super1_Pnd_Rt_A_I_Ind;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Table_Id;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Short_Key;
    private DbsField pnd_Rt_Super1_Pnd_Rt_Long_Key;

    private DataAccessProgramView vw_ia_View;
    private DbsField ia_View_Cntrct_Ppcn_Nbr;
    private DbsField ia_View_Cntrct_Orgn_Cde;
    private DbsField ia_View_Cntrct_Mtch_Ppcn;
    private DbsField ia_View_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Wk_Settlementsystem;
    private DbsField pnd_Ws_Acct_Code;
    private DbsField pnd_Ws_Valuat_Period;
    private DbsField pnd_Save_Eft_Acct;
    private DbsField pnd_Rec_Type01;
    private DbsField pnd_First_Ded;
    private DbsField pnd_Tiaa_Ppcn;
    private DbsField pnd_Netzero;
    private DbsField pnd_Check_Eft;
    private DbsField pnd_Check_Rollover;
    private DbsField pnd_Check_Efthold;
    private DbsField pnd_Ws_Da_Cntrct;
    private DbsField pnd_Ws_Ia_Cntrct;
    private DbsField deductionsdescription_Open;
    private DbsField deductionsdescription_Close;
    private DbsField pnd_Ws_Interest_Amt;
    private DbsField pnd_Interest_Amt;
    private DbsField pnd_Nzdc;
    private DbsField commentlinenetzero_Data;
    private DbsField deductiondesc_Tab;
    private DbsField deductionamount_Tab;
    private DbsField pnd_Sv_Pymnt_Check_Nbr_A;
    private DbsField pnd_Pymnt_Ded_Tab_A;

    private DbsGroup pnd_Pymnt_Ded_Tab_A__R_Field_6;
    private DbsField pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab;
    private DbsField pnd_Pymnt_Ded_Cde_A;

    private DbsGroup pnd_Pymnt_Ded_Cde_A__R_Field_7;
    private DbsField pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde;
    private DbsField pnd_Netzero_Sw;
    private DbsField pnd_Xndx;
    private DbsField pnd_Yndx;
    private DbsField pnd_Bndx;
    private DbsField pnd_Cndx;
    private DbsField pnd_Bc_Line;
    private DbsField pnd_Univ_Line;
    private DbsField pnd_Univ_Switch;
    private DbsField pnd_Dr_Line;
    private DbsField pnd_Adr_Line;
    private DbsField pnd_Fn_Line;
    private DbsField pnd_Ins_Line;
    private DbsField addressline2_Data;
    private DbsField pnd_Xndx_Rollover;
    private DbsField pnd_Addr_Isn;
    private DbsField pnd_Addr;

    private DbsGroup pnd_Addr__R_Field_8;
    private DbsField pnd_Addr_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Addr_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Addr_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Addr_Pnd_Cntrct_Invrse_Dte;
    private DbsField pnd_Addr_Pnd_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Cntrct_Cmbn_Nbr;

    private DataAccessProgramView vw_addr_View;
    private DbsField addr_View_Rcrd_Typ;
    private DbsField addr_View_Cntrct_Orgn_Cde;
    private DbsField addr_View_Cntrct_Ppcn_Nbr;
    private DbsField addr_View_Cntrct_Payee_Cde;
    private DbsField addr_View_Cntrct_Invrse_Dte;
    private DbsField addr_View_Pymnt_Prcss_Seq_Nbr;

    private DataAccessProgramView vw_addr_Upd;

    private DbsGroup addr_Upd_Pymnt_Nme_And_Addr_Grp;
    private DbsField addr_Upd_Pymnt_Addr_Line1_Txt;
    private DbsField addr_Upd_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_9;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Proline_Local_Print;
    private DbsField pnd_Pymnt_Settlmnt_Dte_A;
    private DbsField pnd_I_Fund;
    private DbsField pnd_Next_Pymnt_Due_Dte_A;

    private DbsGroup pnd_Next_Pymnt_Due_Dte_A__R_Field_10;
    private DbsField pnd_Next_Pymnt_Due_Dte_A_Pnd_Next_Pymnt_Due_Dte_N;
    private DbsField pnd_Next_Pymnt_Due_Dtef_A;

    private DbsGroup pnd_Next_Pymnt_Due_Dtef_A__R_Field_11;
    private DbsField pnd_Next_Pymnt_Due_Dtef_A_Pnd_Next_Pymnt_Due_Dtef_N;
    private DbsField pnd_Ws_Frequency;
    private DbsField pnd_Pyhdr_Pymnt_Settlmnt_Dte_End;

    private DbsGroup pnd_Pyhdr_Pymnt_Settlmnt_Dte_End__R_Field_12;
    private DbsField pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Ctznshp_Cder;
    private DbsField pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Rsdncy_Cder;
    private DbsField pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start_A;
    private DbsField pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A;

    private DataAccessProgramView vw_can_Pymnt;

    private DbsGroup can_Pymnt_Inv_Acct_Part_2;
    private DbsField can_Pymnt_Inv_Acct_Can_Tax_Amt;
    private DbsField can_Pymnt_Cntrct_Can_Tax_Amt;
    private DbsField can_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField can_Pymnt_Cntrct_Invrse_Dte;
    private DbsField can_Pymnt_Cntrct_Orgn_Cde;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Pymnt_S__R_Field_13;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;
    private DbsField pnd_Inv_Acct_Can_Tax_Amt;
    private DbsField pnd_Ded_Amt2;

    private DbsGroup pnd_Chk_Fields;
    private DbsField pnd_Chk_Fields_Rtb;
    private DbsField pnd_Chk_Fields_Ivc_From_Rtb_Rollover;
    private DbsField pnd_Chk_Fields_Pnd_Cntrct_Amt;
    private DbsField pnd_Chk_Fields_Pnd_Filler;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_State_Idx;
    private DbsField pnd_Ws_Pnd_Ded_Disp_Array;

    private DbsGroup pnd_Ws_Pnd_Cntrct_Totals;
    private DbsField pnd_Ws_Pnd_Cntrct_Settl_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Dpi_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Ded_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Net_Amt;
    private DbsField pnd_Ws_Pnd_Cntrct_Ded_Table;
    private DbsField pnd_Pymnt_Ded_Pnd;
    private DbsField pnd_Ded_Pymnt_Table;

    private DbsGroup pnd_Ded_Pymnt_Table__R_Field_14;
    private DbsField pnd_Ded_Pymnt_Table_Pnd_Ded_Pymnt_Table_N;
    private DbsField pnd_Ws_Legend;
    private DbsField pnd_Ded_Count;
    private DbsField pnd_Ded_Curr;
    private DbsField pnd_Ded_Save;
    private DbsField pnd_Ded_Ndx;
    private DbsField pnd_Ded_Ndx2;
    private DbsField pnd_Ded_Last;
    private DbsField pnd_Sve_Documentrequestid_Data;

    private DbsGroup pnd_Fcpn600;
    private DbsField pnd_Fcpn600_Pnd_Parm_Code;
    private DbsField pnd_Fcpn600_Pnd_Parm_Desc;
    private DbsField pnd_Ws_Legend2;

    private DbsGroup pnd_Ws_Legend2__R_Field_15;
    private DbsField pnd_Ws_Legend2_Pnd_Ws_Legend2_A;
    private DbsField pnd_Ws_Legend2_Pnd_Ws_Legend2_Fil;
    private DbsField pnd_Ws_Fund_Description;
    private DbsField pnd_First_Addr;
    private DbsField pnd_Close_Addr;

    private DataAccessProgramView vw_rt2;

    private DbsGroup rt2_Rt_Record;
    private DbsField rt2_Rt_A_I_Ind;
    private DbsField rt2_Rt_Table_Id;
    private DbsField rt2_Rt_Short_Key;
    private DbsField rt2_Rt_Long_Key;
    private DbsField pnd_Rt2_Super1;

    private DbsGroup pnd_Rt2_Super1__R_Field_16;
    private DbsField pnd_Rt2_Super1_Pnd_Rt2_A_I_Ind;
    private DbsField pnd_Rt2_Super1_Pnd_Rt2_Table_Id;
    private DbsField pnd_Lp_Holdcode;
    private DbsField pnd_Ws_Holdcode;
    private DbsField pnd_Pymnt_Ded2;
    private DbsField pnd_Pymnt_Ded3;
    private DbsField pnd_Ndx2;
    private DbsField pnd_Universalid_Seven;
    private DbsField pnd_Wk_Universalid;
    private DbsField pnd_Cpun500_Parm;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl961 = new LdaFcpl961();
        registerRecord(ldaFcpl961);
        localVariables = new DbsRecord();
        pdaCpwapytp = new PdaCpwapytp(localVariables);
        pdaCpoaorgn = new PdaCpoaorgn(localVariables);
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        ldaFcpltbcd = new LdaFcpltbcd();
        registerRecord(ldaFcpltbcd);
        pdaFcpamode = new PdaFcpamode(localVariables);
        pdaFcpaoptn = new PdaFcpaoptn(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl893g = new LdaFcpl893g();
        registerRecord(ldaFcpl893g);
        ldaFcpl803l = new LdaFcpl803l();
        registerRecord(ldaFcpl803l);
        ldaFcpl893l = new LdaFcpl893l();
        registerRecord(ldaFcpl893l);
        pdaFcppda_M = new PdaFcppda_M(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Annty_Ins_Type = parameters.newFieldInRecord("pnd_Ws_Cntrct_Annty_Ins_Type", "#WS-CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Cntrct_Annty_Ins_Type.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Annty_Type_Cde = parameters.newFieldInRecord("pnd_Ws_Cntrct_Annty_Type_Cde", "#WS-CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        pnd_Ws_Cntrct_Annty_Type_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Insurance_Option = parameters.newFieldInRecord("pnd_Ws_Cntrct_Insurance_Option", "#WS-CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1);
        pnd_Ws_Cntrct_Insurance_Option.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Cntrct_Life_Contingency = parameters.newFieldInRecord("pnd_Ws_Cntrct_Life_Contingency", "#WS-CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1);
        pnd_Ws_Cntrct_Life_Contingency.setParameterOption(ParameterOption.ByReference);
        pdaFcpa110 = new PdaFcpa110(parameters);
        pnd_Batch_Cnt = parameters.newFieldInRecord("pnd_Batch_Cnt", "#BATCH-CNT", FieldType.NUMERIC, 7);
        pnd_Batch_Cnt.setParameterOption(ParameterOption.ByReference);
        pnd_Save_First_Twenty = parameters.newFieldInRecord("pnd_Save_First_Twenty", "#SAVE-FIRST-TWENTY", FieldType.STRING, 20);
        pnd_Save_First_Twenty.setParameterOption(ParameterOption.ByReference);
        pnd_First_Record = parameters.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_First_Record.setParameterOption(ParameterOption.ByReference);
        pnd_Last_Record = parameters.newFieldInRecord("pnd_Last_Record", "#LAST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Last_Record.setParameterOption(ParameterOption.ByReference);
        pnd_Invalid_Rec = parameters.newFieldInRecord("pnd_Invalid_Rec", "#INVALID-REC", FieldType.BOOLEAN, 1);
        pnd_Invalid_Rec.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Hold_Seq = localVariables.newFieldArrayInRecord("pnd_Hold_Seq", "#HOLD-SEQ", FieldType.NUMERIC, 5, new DbsArrayController(1, 1300));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 3);
        pnd_Record_Cnt = localVariables.newFieldInRecord("pnd_Record_Cnt", "#RECORD-CNT", FieldType.NUMERIC, 10);
        pnd_Check_Cnt = localVariables.newFieldInRecord("pnd_Check_Cnt", "#CHECK-CNT", FieldType.NUMERIC, 10);
        pnd_Check_Amt = localVariables.newFieldInRecord("pnd_Check_Amt", "#CHECK-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Batch_Counter = localVariables.newFieldInRecord("pnd_Batch_Counter", "#BATCH-COUNTER", FieldType.NUMERIC, 7);
        pnd_Batch_Total = localVariables.newFieldInRecord("pnd_Batch_Total", "#BATCH-TOTAL", FieldType.NUMERIC, 7);
        pnd_Current_Check = localVariables.newFieldInRecord("pnd_Current_Check", "#CURRENT-CHECK", FieldType.NUMERIC, 10);
        pnd_Payment_Cnt = localVariables.newFieldInRecord("pnd_Payment_Cnt", "#PAYMENT-CNT", FieldType.NUMERIC, 10);
        pnd_Eft_Cnt = localVariables.newFieldInRecord("pnd_Eft_Cnt", "#EFT-CNT", FieldType.NUMERIC, 10);
        pnd_Eft_Amt = localVariables.newFieldInRecord("pnd_Eft_Amt", "#EFT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Other_Cnt = localVariables.newFieldInRecord("pnd_Other_Cnt", "#OTHER-CNT", FieldType.NUMERIC, 10);
        pnd_Other_Amt = localVariables.newFieldInRecord("pnd_Other_Amt", "#OTHER-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.NUMERIC, 10);
        pnd_Ded_Amt = localVariables.newFieldInRecord("pnd_Ded_Amt", "#DED-AMT", FieldType.NUMERIC, 13, 2);
        pnd_Blank_Rerun = localVariables.newFieldInRecord("pnd_Blank_Rerun", "#BLANK-RERUN", FieldType.STRING, 5);
        pnd_Xml_Line = localVariables.newFieldInRecord("pnd_Xml_Line", "#XML-LINE", FieldType.STRING, 250);
        pnd_Bank_Source_Code = localVariables.newFieldInRecord("pnd_Bank_Source_Code", "#BANK-SOURCE-CODE", FieldType.STRING, 50);
        pnd_Charlotte_Printer = localVariables.newFieldInRecord("pnd_Charlotte_Printer", "#CHARLOTTE-PRINTER", FieldType.STRING, 8);
        pnd_Denver_Printer = localVariables.newFieldInRecord("pnd_Denver_Printer", "#DENVER-PRINTER", FieldType.STRING, 8);
        pnd_Dbl_Quote = localVariables.newFieldInRecord("pnd_Dbl_Quote", "#DBL-QUOTE", FieldType.STRING, 1);
        pnd_Total_Tax_Amt = localVariables.newFieldInRecord("pnd_Total_Tax_Amt", "#TOTAL-TAX-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Payment_Amt = localVariables.newFieldInRecord("pnd_Payment_Amt", "#PAYMENT-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Total_Payment_Amt = localVariables.newFieldInRecord("pnd_Total_Payment_Amt", "#TOTAL-PAYMENT-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Total_Net_Amt = localVariables.newFieldInRecord("pnd_Total_Net_Amt", "#TOTAL-NET-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Total_Net_Amt2 = localVariables.newFieldInRecord("pnd_Total_Net_Amt2", "#TOTAL-NET-AMT2", FieldType.NUMERIC, 17, 2);
        pnd_Total_Fund_Net_Amt = localVariables.newFieldInRecord("pnd_Total_Fund_Net_Amt", "#TOTAL-FUND-NET-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Total_Gross_Amt = localVariables.newFieldInRecord("pnd_Total_Gross_Amt", "#TOTAL-GROSS-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Federal_Tax_Amt = localVariables.newFieldInRecord("pnd_Federal_Tax_Amt", "#FEDERAL-TAX-AMT", FieldType.NUMERIC, 17, 2);
        pnd_State_Tax_Amt = localVariables.newFieldInRecord("pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Local_Tax_Amt = localVariables.newFieldInRecord("pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Can_Tax_Amt = localVariables.newFieldInRecord("pnd_Can_Tax_Amt", "#CAN-TAX-AMT", FieldType.NUMERIC, 17, 2);
        pnd_Federal_Tax_Amt2 = localVariables.newFieldInRecord("pnd_Federal_Tax_Amt2", "#FEDERAL-TAX-AMT2", FieldType.NUMERIC, 17, 2);
        pnd_State_Tax_Amt2 = localVariables.newFieldInRecord("pnd_State_Tax_Amt2", "#STATE-TAX-AMT2", FieldType.NUMERIC, 17, 2);
        pnd_Local_Tax_Amt2 = localVariables.newFieldInRecord("pnd_Local_Tax_Amt2", "#LOCAL-TAX-AMT2", FieldType.NUMERIC, 17, 2);
        pnd_Can_Tax_Amt2 = localVariables.newFieldInRecord("pnd_Can_Tax_Amt2", "#CAN-TAX-AMT2", FieldType.NUMERIC, 17, 2);
        pnd_Total_Net_Amt_Trunc = localVariables.newFieldInRecord("pnd_Total_Net_Amt_Trunc", "#TOTAL-NET-AMT-TRUNC", FieldType.NUMERIC, 9);
        pnd_Total_Fund_Net_Amt_Trunc = localVariables.newFieldInRecord("pnd_Total_Fund_Net_Amt_Trunc", "#TOTAL-FUND-NET-AMT-TRUNC", FieldType.NUMERIC, 
            9);
        pnd_Total_Ded = localVariables.newFieldInRecord("pnd_Total_Ded", "#TOTAL-DED", FieldType.NUMERIC, 11, 2);
        pnd_Inv_Id = localVariables.newFieldInRecord("pnd_Inv_Id", "#INV-ID", FieldType.STRING, 6);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Origin_Description = localVariables.newFieldInRecord("pnd_Origin_Description", "#ORIGIN-DESCRIPTION", FieldType.STRING, 20);
        pnd_Omni_Check_Date = localVariables.newFieldInRecord("pnd_Omni_Check_Date", "#OMNI-CHECK-DATE", FieldType.DATE);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Formatted_Zip = localVariables.newFieldInRecord("pnd_Formatted_Zip", "#FORMATTED-ZIP", FieldType.STRING, 10);
        pnd_Post_Num = localVariables.newFieldInRecord("pnd_Post_Num", "#POST-NUM", FieldType.NUMERIC, 5);

        pnd_Post_Num__R_Field_1 = localVariables.newGroupInRecord("pnd_Post_Num__R_Field_1", "REDEFINE", pnd_Post_Num);
        pnd_Post_Num_Pnd_Post_Num_A = pnd_Post_Num__R_Field_1.newFieldInGroup("pnd_Post_Num_Pnd_Post_Num_A", "#POST-NUM-A", FieldType.STRING, 5);
        pnd_Hold_Post_Num = localVariables.newFieldInRecord("pnd_Hold_Post_Num", "#HOLD-POST-NUM", FieldType.NUMERIC, 5);

        pnd_Hold_Post_Num__R_Field_2 = localVariables.newGroupInRecord("pnd_Hold_Post_Num__R_Field_2", "REDEFINE", pnd_Hold_Post_Num);
        pnd_Hold_Post_Num_Pnd_Hold_Post_Num_A = pnd_Hold_Post_Num__R_Field_2.newFieldInGroup("pnd_Hold_Post_Num_Pnd_Hold_Post_Num_A", "#HOLD-POST-NUM-A", 
            FieldType.STRING, 5);
        pnd_Last_Name = localVariables.newFieldInRecord("pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        pnd_First_Name = localVariables.newFieldInRecord("pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);
        pnd_Middle_Name = localVariables.newFieldInRecord("pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 30);
        pnd_Rest_Of_Name = localVariables.newFieldInRecord("pnd_Rest_Of_Name", "#REST-OF-NAME", FieldType.STRING, 30);
        pnd_Other = localVariables.newFieldInRecord("pnd_Other", "#OTHER", FieldType.STRING, 30);
        pnd_Other2 = localVariables.newFieldInRecord("pnd_Other2", "#OTHER2", FieldType.STRING, 30);
        pnd_Other3 = localVariables.newFieldInRecord("pnd_Other3", "#OTHER3", FieldType.STRING, 30);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Inv_Acct_Ticker_Hold = localVariables.newFieldInRecord("pnd_Inv_Acct_Ticker_Hold", "#INV-ACCT-TICKER-HOLD", FieldType.STRING, 10);

        pnd_Inv_Acct_Ticker_Hold__R_Field_3 = localVariables.newGroupInRecord("pnd_Inv_Acct_Ticker_Hold__R_Field_3", "REDEFINE", pnd_Inv_Acct_Ticker_Hold);
        pnd_Inv_Acct_Ticker_Hold_Alpha = pnd_Inv_Acct_Ticker_Hold__R_Field_3.newFieldInGroup("pnd_Inv_Acct_Ticker_Hold_Alpha", "ALPHA", FieldType.STRING, 
            3);
        pnd_Inv_Acct_Ticker_Hold_Pnd_Seq_Nbr = pnd_Inv_Acct_Ticker_Hold__R_Field_3.newFieldInGroup("pnd_Inv_Acct_Ticker_Hold_Pnd_Seq_Nbr", "#SEQ-NBR", 
            FieldType.NUMERIC, 4);
        pnd_Seq_Number = localVariables.newFieldInRecord("pnd_Seq_Number", "#SEQ-NUMBER", FieldType.NUMERIC, 5);
        pnd_Insndx = localVariables.newFieldInRecord("pnd_Insndx", "#INSNDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ndx_Fund = localVariables.newFieldInRecord("pnd_Ndx_Fund", "#NDX-FUND", FieldType.PACKED_DECIMAL, 3);
        pnd_Ndx_Ded = localVariables.newFieldInRecord("pnd_Ndx_Ded", "#NDX-DED", FieldType.PACKED_DECIMAL, 3);
        pnd_Pos_Ded = localVariables.newFieldInRecord("pnd_Pos_Ded", "#POS-DED", FieldType.PACKED_DECIMAL, 3);
        pnd_Wk_Acct_Cde_A = localVariables.newFieldInRecord("pnd_Wk_Acct_Cde_A", "#WK-ACCT-CDE-A", FieldType.STRING, 2);

        pnd_Wk_Acct_Cde_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Wk_Acct_Cde_A__R_Field_4", "REDEFINE", pnd_Wk_Acct_Cde_A);
        pnd_Wk_Acct_Cde_A_Pnd_Wk_Acct_Cde = pnd_Wk_Acct_Cde_A__R_Field_4.newFieldInGroup("pnd_Wk_Acct_Cde_A_Pnd_Wk_Acct_Cde", "#WK-ACCT-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ded_Desc_Tab = localVariables.newFieldArrayInRecord("pnd_Ded_Desc_Tab", "#DED-DESC-TAB", FieldType.STRING, 20, new DbsArrayController(1, 10));

        pnd_Hold_Addr = localVariables.newGroupInRecord("pnd_Hold_Addr", "#HOLD-ADDR");
        pnd_Hold_Addr_Pnd_Hold_Nme = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Nme", "#HOLD-NME", FieldType.STRING, 38);
        pnd_Hold_Addr_Pnd_Hold_Addr_Line1_Txt = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Addr_Line1_Txt", "#HOLD-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        pnd_Hold_Addr_Pnd_Hold_Addr_Line2_Txt = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Addr_Line2_Txt", "#HOLD-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        pnd_Hold_Addr_Pnd_Hold_Addr_Line3_Txt = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Addr_Line3_Txt", "#HOLD-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        pnd_Hold_Addr_Pnd_Hold_Addr_Line4_Txt = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Addr_Line4_Txt", "#HOLD-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        pnd_Hold_Addr_Pnd_Hold_Addr_Line5_Txt = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Addr_Line5_Txt", "#HOLD-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        pnd_Hold_Addr_Pnd_Hold_Addr_Line6_Txt = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Addr_Line6_Txt", "#HOLD-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        pnd_Hold_Addr_Pnd_Hold_Addr_Zip_Cde = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_Addr_Zip_Cde", "#HOLD-ADDR-ZIP-CDE", FieldType.STRING, 
            9);
        pnd_Hold_Addr_Pnd_Hold_For_Dom = pnd_Hold_Addr.newFieldInGroup("pnd_Hold_Addr_Pnd_Hold_For_Dom", "#HOLD-FOR-DOM", FieldType.STRING, 1);
        hold_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("hold_Cntrct_Orgn_Cde", "HOLD-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);

        vw_rt = new DataAccessProgramView(new NameInfo("vw_rt", "RT"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        rt_Rt_A_I_Ind = vw_rt.getRecord().newFieldInGroup("rt_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt_Rt_Table_Id = vw_rt.getRecord().newFieldInGroup("rt_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rt_Rt_Short_Key = vw_rt.getRecord().newFieldInGroup("rt_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        rt_Rt_Long_Key = vw_rt.getRecord().newFieldInGroup("rt_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        rt_Rt_Desc1 = vw_rt.getRecord().newFieldInGroup("rt_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rt_Rt_Desc2 = vw_rt.getRecord().newFieldInGroup("rt_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rt_Rt_Desc3 = vw_rt.getRecord().newFieldInGroup("rt_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        registerRecord(vw_rt);

        pnd_Rt_Super1 = localVariables.newFieldInRecord("pnd_Rt_Super1", "#RT-SUPER1", FieldType.STRING, 66);

        pnd_Rt_Super1__R_Field_5 = localVariables.newGroupInRecord("pnd_Rt_Super1__R_Field_5", "REDEFINE", pnd_Rt_Super1);
        pnd_Rt_Super1_Pnd_Rt_A_I_Ind = pnd_Rt_Super1__R_Field_5.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_A_I_Ind", "#RT-A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super1_Pnd_Rt_Table_Id = pnd_Rt_Super1__R_Field_5.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Table_Id", "#RT-TABLE-ID", FieldType.STRING, 5);
        pnd_Rt_Super1_Pnd_Rt_Short_Key = pnd_Rt_Super1__R_Field_5.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Short_Key", "#RT-SHORT-KEY", FieldType.STRING, 
            20);
        pnd_Rt_Super1_Pnd_Rt_Long_Key = pnd_Rt_Super1__R_Field_5.newFieldInGroup("pnd_Rt_Super1_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 40);

        vw_ia_View = new DataAccessProgramView(new NameInfo("vw_ia_View", "IA-VIEW"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        ia_View_Cntrct_Ppcn_Nbr = vw_ia_View.getRecord().newFieldInGroup("ia_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        ia_View_Cntrct_Orgn_Cde = vw_ia_View.getRecord().newFieldInGroup("ia_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        ia_View_Cntrct_Mtch_Ppcn = vw_ia_View.getRecord().newFieldInGroup("ia_View_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_MTCH_PPCN");
        ia_View_Cntrct_Orig_Da_Cntrct_Nbr = vw_ia_View.getRecord().newFieldInGroup("ia_View_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        registerRecord(vw_ia_View);

        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 132);
        pnd_Wk_Settlementsystem = localVariables.newFieldInRecord("pnd_Wk_Settlementsystem", "#WK-SETTLEMENTSYSTEM", FieldType.STRING, 6);
        pnd_Ws_Acct_Code = localVariables.newFieldInRecord("pnd_Ws_Acct_Code", "#WS-ACCT-CODE", FieldType.STRING, 2);
        pnd_Ws_Valuat_Period = localVariables.newFieldInRecord("pnd_Ws_Valuat_Period", "#WS-VALUAT-PERIOD", FieldType.STRING, 1);
        pnd_Save_Eft_Acct = localVariables.newFieldInRecord("pnd_Save_Eft_Acct", "#SAVE-EFT-ACCT", FieldType.STRING, 21);
        pnd_Rec_Type01 = localVariables.newFieldInRecord("pnd_Rec_Type01", "#REC-TYPE01", FieldType.BOOLEAN, 1);
        pnd_First_Ded = localVariables.newFieldInRecord("pnd_First_Ded", "#FIRST-DED", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Ppcn = localVariables.newFieldInRecord("pnd_Tiaa_Ppcn", "#TIAA-PPCN", FieldType.BOOLEAN, 1);
        pnd_Netzero = localVariables.newFieldInRecord("pnd_Netzero", "#NETZERO", FieldType.BOOLEAN, 1);
        pnd_Check_Eft = localVariables.newFieldInRecord("pnd_Check_Eft", "#CHECK-EFT", FieldType.BOOLEAN, 1);
        pnd_Check_Rollover = localVariables.newFieldInRecord("pnd_Check_Rollover", "#CHECK-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Check_Efthold = localVariables.newFieldInRecord("pnd_Check_Efthold", "#CHECK-EFTHOLD", FieldType.BOOLEAN, 1);
        pnd_Ws_Da_Cntrct = localVariables.newFieldInRecord("pnd_Ws_Da_Cntrct", "#WS-DA-CNTRCT", FieldType.STRING, 8);
        pnd_Ws_Ia_Cntrct = localVariables.newFieldInRecord("pnd_Ws_Ia_Cntrct", "#WS-IA-CNTRCT", FieldType.STRING, 10);
        deductionsdescription_Open = localVariables.newFieldInRecord("deductionsdescription_Open", "DEDUCTIONSDESCRIPTION-OPEN", FieldType.STRING, 25);
        deductionsdescription_Close = localVariables.newFieldInRecord("deductionsdescription_Close", "DEDUCTIONSDESCRIPTION-CLOSE", FieldType.STRING, 
            25);
        pnd_Ws_Interest_Amt = localVariables.newFieldInRecord("pnd_Ws_Interest_Amt", "#WS-INTEREST-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Interest_Amt = localVariables.newFieldInRecord("pnd_Interest_Amt", "#INTEREST-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Nzdc = localVariables.newFieldInRecord("pnd_Nzdc", "#NZDC", FieldType.BOOLEAN, 1);
        commentlinenetzero_Data = localVariables.newFieldInRecord("commentlinenetzero_Data", "COMMENTLINENETZERO-DATA", FieldType.STRING, 250);
        deductiondesc_Tab = localVariables.newFieldArrayInRecord("deductiondesc_Tab", "DEDUCTIONDESC-TAB", FieldType.STRING, 30, new DbsArrayController(1, 
            10));
        deductionamount_Tab = localVariables.newFieldArrayInRecord("deductionamount_Tab", "DEDUCTIONAMOUNT-TAB", FieldType.STRING, 15, new DbsArrayController(1, 
            10));
        pnd_Sv_Pymnt_Check_Nbr_A = localVariables.newFieldInRecord("pnd_Sv_Pymnt_Check_Nbr_A", "#SV-PYMNT-CHECK-NBR-A", FieldType.STRING, 11);
        pnd_Pymnt_Ded_Tab_A = localVariables.newFieldArrayInRecord("pnd_Pymnt_Ded_Tab_A", "#PYMNT-DED-TAB-A", FieldType.STRING, 9, new DbsArrayController(1, 
            10));

        pnd_Pymnt_Ded_Tab_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Pymnt_Ded_Tab_A__R_Field_6", "REDEFINE", pnd_Pymnt_Ded_Tab_A);
        pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab = pnd_Pymnt_Ded_Tab_A__R_Field_6.newFieldArrayInGroup("pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab", "#PYMNT-DED-TAB", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 10));
        pnd_Pymnt_Ded_Cde_A = localVariables.newFieldArrayInRecord("pnd_Pymnt_Ded_Cde_A", "#PYMNT-DED-CDE-A", FieldType.STRING, 3, new DbsArrayController(1, 
            10));

        pnd_Pymnt_Ded_Cde_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Pymnt_Ded_Cde_A__R_Field_7", "REDEFINE", pnd_Pymnt_Ded_Cde_A);
        pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde = pnd_Pymnt_Ded_Cde_A__R_Field_7.newFieldArrayInGroup("pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde", "#PYMNT-DED-CDE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 10));
        pnd_Netzero_Sw = localVariables.newFieldInRecord("pnd_Netzero_Sw", "#NETZERO-SW", FieldType.BOOLEAN, 1);
        pnd_Xndx = localVariables.newFieldInRecord("pnd_Xndx", "#XNDX", FieldType.PACKED_DECIMAL, 5);
        pnd_Yndx = localVariables.newFieldInRecord("pnd_Yndx", "#YNDX", FieldType.PACKED_DECIMAL, 5);
        pnd_Bndx = localVariables.newFieldInRecord("pnd_Bndx", "#BNDX", FieldType.PACKED_DECIMAL, 5);
        pnd_Cndx = localVariables.newFieldInRecord("pnd_Cndx", "#CNDX", FieldType.PACKED_DECIMAL, 5);
        pnd_Bc_Line = localVariables.newFieldInRecord("pnd_Bc_Line", "#BC-LINE", FieldType.BOOLEAN, 1);
        pnd_Univ_Line = localVariables.newFieldInRecord("pnd_Univ_Line", "#UNIV-LINE", FieldType.BOOLEAN, 1);
        pnd_Univ_Switch = localVariables.newFieldInRecord("pnd_Univ_Switch", "#UNIV-SWITCH", FieldType.BOOLEAN, 1);
        pnd_Dr_Line = localVariables.newFieldInRecord("pnd_Dr_Line", "#DR-LINE", FieldType.BOOLEAN, 1);
        pnd_Adr_Line = localVariables.newFieldInRecord("pnd_Adr_Line", "#ADR-LINE", FieldType.BOOLEAN, 1);
        pnd_Fn_Line = localVariables.newFieldInRecord("pnd_Fn_Line", "#FN-LINE", FieldType.BOOLEAN, 1);
        pnd_Ins_Line = localVariables.newFieldInRecord("pnd_Ins_Line", "#INS-LINE", FieldType.BOOLEAN, 1);
        addressline2_Data = localVariables.newFieldArrayInRecord("addressline2_Data", "ADDRESSLINE2-DATA", FieldType.STRING, 35, new DbsArrayController(1, 
            6));
        pnd_Xndx_Rollover = localVariables.newFieldArrayInRecord("pnd_Xndx_Rollover", "#XNDX-ROLLOVER", FieldType.STRING, 250, new DbsArrayController(1, 
            250));
        pnd_Addr_Isn = localVariables.newFieldInRecord("pnd_Addr_Isn", "#ADDR-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Addr = localVariables.newFieldInRecord("pnd_Addr", "#ADDR", FieldType.STRING, 32);

        pnd_Addr__R_Field_8 = localVariables.newGroupInRecord("pnd_Addr__R_Field_8", "REDEFINE", pnd_Addr);
        pnd_Addr_Pnd_Cntrct_Ppcn_Nbr = pnd_Addr__R_Field_8.newFieldInGroup("pnd_Addr_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Addr_Pnd_Cntrct_Payee_Cde = pnd_Addr__R_Field_8.newFieldInGroup("pnd_Addr_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Addr_Pnd_Cntrct_Orgn_Cde = pnd_Addr__R_Field_8.newFieldInGroup("pnd_Addr_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Addr_Pnd_Cntrct_Invrse_Dte = pnd_Addr__R_Field_8.newFieldInGroup("pnd_Addr_Pnd_Cntrct_Invrse_Dte", "#CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Addr_Pnd_Pymnt_Prcss_Seq_Nbr = pnd_Addr__R_Field_8.newFieldInGroup("pnd_Addr_Pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            7);
        pnd_Cntrct_Cmbn_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Cmbn_Nbr", "#CNTRCT-CMBN-NBR", FieldType.STRING, 10);

        vw_addr_View = new DataAccessProgramView(new NameInfo("vw_addr_View", "ADDR-VIEW"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR");
        addr_View_Rcrd_Typ = vw_addr_View.getRecord().newFieldInGroup("addr_View_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYP");
        addr_View_Cntrct_Orgn_Cde = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        addr_View_Cntrct_Ppcn_Nbr = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        addr_View_Cntrct_Payee_Cde = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        addr_View_Cntrct_Invrse_Dte = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        addr_View_Pymnt_Prcss_Seq_Nbr = vw_addr_View.getRecord().newFieldInGroup("addr_View_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        registerRecord(vw_addr_View);

        vw_addr_Upd = new DataAccessProgramView(new NameInfo("vw_addr_Upd", "ADDR-UPD"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ADDR"));

        addr_Upd_Pymnt_Nme_And_Addr_Grp = vw_addr_Upd.getRecord().newGroupInGroup("addr_Upd_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Upd_Pymnt_Addr_Line1_Txt = addr_Upd_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Upd_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35, new DbsArrayController(10, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE1_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Upd_Pymnt_Addr_Line2_Txt = addr_Upd_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Upd_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35, new DbsArrayController(10, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE2_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        registerRecord(vw_addr_Upd);

        pnd_Ws_Pymnt_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Nbr_N10", "#WS-PYMNT-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_9 = localVariables.newGroupInRecord("pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_9", "REDEFINE", pnd_Ws_Pymnt_Check_Nbr_N10);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_9.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", 
            "#WS-CHECK-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_9.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", 
            "#WS-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        pnd_Cntrct_Ivc_Amt = localVariables.newFieldInRecord("pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Proline_Local_Print = localVariables.newFieldInRecord("pnd_Proline_Local_Print", "#PROLINE-LOCAL-PRINT", FieldType.STRING, 1);
        pnd_Pymnt_Settlmnt_Dte_A = localVariables.newFieldInRecord("pnd_Pymnt_Settlmnt_Dte_A", "#PYMNT-SETTLMNT-DTE-A", FieldType.STRING, 8);
        pnd_I_Fund = localVariables.newFieldInRecord("pnd_I_Fund", "#I-FUND", FieldType.PACKED_DECIMAL, 3);
        pnd_Next_Pymnt_Due_Dte_A = localVariables.newFieldInRecord("pnd_Next_Pymnt_Due_Dte_A", "#NEXT-PYMNT-DUE-DTE-A", FieldType.STRING, 8);

        pnd_Next_Pymnt_Due_Dte_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Next_Pymnt_Due_Dte_A__R_Field_10", "REDEFINE", pnd_Next_Pymnt_Due_Dte_A);
        pnd_Next_Pymnt_Due_Dte_A_Pnd_Next_Pymnt_Due_Dte_N = pnd_Next_Pymnt_Due_Dte_A__R_Field_10.newFieldInGroup("pnd_Next_Pymnt_Due_Dte_A_Pnd_Next_Pymnt_Due_Dte_N", 
            "#NEXT-PYMNT-DUE-DTE-N", FieldType.NUMERIC, 8);
        pnd_Next_Pymnt_Due_Dtef_A = localVariables.newFieldInRecord("pnd_Next_Pymnt_Due_Dtef_A", "#NEXT-PYMNT-DUE-DTEF-A", FieldType.STRING, 8);

        pnd_Next_Pymnt_Due_Dtef_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Next_Pymnt_Due_Dtef_A__R_Field_11", "REDEFINE", pnd_Next_Pymnt_Due_Dtef_A);
        pnd_Next_Pymnt_Due_Dtef_A_Pnd_Next_Pymnt_Due_Dtef_N = pnd_Next_Pymnt_Due_Dtef_A__R_Field_11.newFieldInGroup("pnd_Next_Pymnt_Due_Dtef_A_Pnd_Next_Pymnt_Due_Dtef_N", 
            "#NEXT-PYMNT-DUE-DTEF-N", FieldType.NUMERIC, 8);
        pnd_Ws_Frequency = localVariables.newFieldInRecord("pnd_Ws_Frequency", "#WS-FREQUENCY", FieldType.STRING, 12);
        pnd_Pyhdr_Pymnt_Settlmnt_Dte_End = localVariables.newFieldInRecord("pnd_Pyhdr_Pymnt_Settlmnt_Dte_End", "#PYHDR-PYMNT-SETTLMNT-DTE-END", FieldType.DATE);

        pnd_Pyhdr_Pymnt_Settlmnt_Dte_End__R_Field_12 = localVariables.newGroupInRecord("pnd_Pyhdr_Pymnt_Settlmnt_Dte_End__R_Field_12", "REDEFINE", pnd_Pyhdr_Pymnt_Settlmnt_Dte_End);
        pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Ctznshp_Cder = pnd_Pyhdr_Pymnt_Settlmnt_Dte_End__R_Field_12.newFieldInGroup("pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Ctznshp_Cder", 
            "#PYHDR-ANNT-CTZNSHP-CDER", FieldType.NUMERIC, 2);
        pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Rsdncy_Cder = pnd_Pyhdr_Pymnt_Settlmnt_Dte_End__R_Field_12.newFieldInGroup("pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Rsdncy_Cder", 
            "#PYHDR-ANNT-RSDNCY-CDER", FieldType.STRING, 2);
        pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start_A = localVariables.newFieldInRecord("pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start_A", "#PYHDR-PYMNT-SETTLMNT-DTE-START-A", 
            FieldType.STRING, 10);
        pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A = localVariables.newFieldInRecord("pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A", "#PYHDR-PYMNT-SETTLMNT-DTE-END-A", 
            FieldType.STRING, 10);

        vw_can_Pymnt = new DataAccessProgramView(new NameInfo("vw_can_Pymnt", "CAN-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));

        can_Pymnt_Inv_Acct_Part_2 = vw_can_Pymnt.getRecord().newGroupInGroup("can_Pymnt_Inv_Acct_Part_2", "INV-ACCT-PART-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT_PART_2");
        can_Pymnt_Inv_Acct_Can_Tax_Amt = can_Pymnt_Inv_Acct_Part_2.newFieldArrayInGroup("can_Pymnt_Inv_Acct_Can_Tax_Amt", "INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CAN_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT_PART_2");
        can_Pymnt_Cntrct_Can_Tax_Amt = vw_can_Pymnt.getRecord().newFieldInGroup("can_Pymnt_Cntrct_Can_Tax_Amt", "CNTRCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_CAN_TAX_AMT");
        can_Pymnt_Cntrct_Ppcn_Nbr = vw_can_Pymnt.getRecord().newFieldInGroup("can_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        can_Pymnt_Cntrct_Invrse_Dte = vw_can_Pymnt.getRecord().newFieldInGroup("can_Pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        can_Pymnt_Cntrct_Orgn_Cde = vw_can_Pymnt.getRecord().newFieldInGroup("can_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        registerRecord(vw_can_Pymnt);

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Num = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);

        pnd_Pymnt_S__R_Field_13 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_13", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_13.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 27);
        pnd_Inv_Acct_Can_Tax_Amt = localVariables.newFieldArrayInRecord("pnd_Inv_Acct_Can_Tax_Amt", "#INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ded_Amt2 = localVariables.newFieldInRecord("pnd_Ded_Amt2", "#DED-AMT2", FieldType.NUMERIC, 9, 2);

        pnd_Chk_Fields = localVariables.newGroupInRecord("pnd_Chk_Fields", "#CHK-FIELDS");
        pnd_Chk_Fields_Rtb = pnd_Chk_Fields.newFieldInGroup("pnd_Chk_Fields_Rtb", "RTB", FieldType.BOOLEAN, 1);
        pnd_Chk_Fields_Ivc_From_Rtb_Rollover = pnd_Chk_Fields.newFieldInGroup("pnd_Chk_Fields_Ivc_From_Rtb_Rollover", "IVC-FROM-RTB-ROLLOVER", FieldType.BOOLEAN, 
            1);
        pnd_Chk_Fields_Pnd_Cntrct_Amt = pnd_Chk_Fields.newFieldArrayInGroup("pnd_Chk_Fields_Pnd_Cntrct_Amt", "#CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1, 40));
        pnd_Chk_Fields_Pnd_Filler = pnd_Chk_Fields.newFieldInGroup("pnd_Chk_Fields_Pnd_Filler", "#FILLER", FieldType.STRING, 35);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_State_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Idx", "#STATE-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ded_Disp_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Ded_Disp_Array", "#DED-DISP-ARRAY", FieldType.STRING, 36, new DbsArrayController(1, 
            10));

        pnd_Ws_Pnd_Cntrct_Totals = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Cntrct_Totals", "#CNTRCT-TOTALS");
        pnd_Ws_Pnd_Cntrct_Settl_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Settl_Amt", "#CNTRCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Cntrct_Dpi_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Dpi_Amt", "#CNTRCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Cntrct_Ded_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Ded_Amt", "#CNTRCT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Cntrct_Net_Amt = pnd_Ws_Pnd_Cntrct_Totals.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Net_Amt", "#CNTRCT-NET-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Cntrct_Ded_Table = pnd_Ws_Pnd_Cntrct_Totals.newFieldArrayInGroup("pnd_Ws_Pnd_Cntrct_Ded_Table", "#CNTRCT-DED-TABLE", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 15));
        pnd_Pymnt_Ded_Pnd = localVariables.newFieldInRecord("pnd_Pymnt_Ded_Pnd", "#PYMNT-DED-#", FieldType.PACKED_DECIMAL, 3);
        pnd_Ded_Pymnt_Table = localVariables.newFieldArrayInRecord("pnd_Ded_Pymnt_Table", "#DED-PYMNT-TABLE", FieldType.STRING, 3, new DbsArrayController(1, 
            15));

        pnd_Ded_Pymnt_Table__R_Field_14 = localVariables.newGroupInRecord("pnd_Ded_Pymnt_Table__R_Field_14", "REDEFINE", pnd_Ded_Pymnt_Table);
        pnd_Ded_Pymnt_Table_Pnd_Ded_Pymnt_Table_N = pnd_Ded_Pymnt_Table__R_Field_14.newFieldArrayInGroup("pnd_Ded_Pymnt_Table_Pnd_Ded_Pymnt_Table_N", 
            "#DED-PYMNT-TABLE-N", FieldType.NUMERIC, 3, new DbsArrayController(1, 15));
        pnd_Ws_Legend = localVariables.newFieldArrayInRecord("pnd_Ws_Legend", "#WS-LEGEND", FieldType.STRING, 4, new DbsArrayController(1, 15));
        pnd_Ded_Count = localVariables.newFieldInRecord("pnd_Ded_Count", "#DED-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Ded_Curr = localVariables.newFieldInRecord("pnd_Ded_Curr", "#DED-CURR", FieldType.PACKED_DECIMAL, 5);
        pnd_Ded_Save = localVariables.newFieldInRecord("pnd_Ded_Save", "#DED-SAVE", FieldType.PACKED_DECIMAL, 5);
        pnd_Ded_Ndx = localVariables.newFieldInRecord("pnd_Ded_Ndx", "#DED-NDX", FieldType.PACKED_DECIMAL, 5);
        pnd_Ded_Ndx2 = localVariables.newFieldInRecord("pnd_Ded_Ndx2", "#DED-NDX2", FieldType.PACKED_DECIMAL, 5);
        pnd_Ded_Last = localVariables.newFieldInRecord("pnd_Ded_Last", "#DED-LAST", FieldType.BOOLEAN, 1);
        pnd_Sve_Documentrequestid_Data = localVariables.newFieldInRecord("pnd_Sve_Documentrequestid_Data", "#SVE-DOCUMENTREQUESTID-DATA", FieldType.STRING, 
            40);

        pnd_Fcpn600 = localVariables.newGroupInRecord("pnd_Fcpn600", "#FCPN600");
        pnd_Fcpn600_Pnd_Parm_Code = pnd_Fcpn600.newFieldInGroup("pnd_Fcpn600_Pnd_Parm_Code", "#PARM-CODE", FieldType.NUMERIC, 2);
        pnd_Fcpn600_Pnd_Parm_Desc = pnd_Fcpn600.newFieldInGroup("pnd_Fcpn600_Pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 70);
        pnd_Ws_Legend2 = localVariables.newFieldInRecord("pnd_Ws_Legend2", "#WS-LEGEND2", FieldType.STRING, 3);

        pnd_Ws_Legend2__R_Field_15 = localVariables.newGroupInRecord("pnd_Ws_Legend2__R_Field_15", "REDEFINE", pnd_Ws_Legend2);
        pnd_Ws_Legend2_Pnd_Ws_Legend2_A = pnd_Ws_Legend2__R_Field_15.newFieldInGroup("pnd_Ws_Legend2_Pnd_Ws_Legend2_A", "#WS-LEGEND2-A", FieldType.STRING, 
            1);
        pnd_Ws_Legend2_Pnd_Ws_Legend2_Fil = pnd_Ws_Legend2__R_Field_15.newFieldInGroup("pnd_Ws_Legend2_Pnd_Ws_Legend2_Fil", "#WS-LEGEND2-FIL", FieldType.STRING, 
            2);
        pnd_Ws_Fund_Description = localVariables.newFieldInRecord("pnd_Ws_Fund_Description", "#WS-FUND-DESCRIPTION", FieldType.STRING, 20);
        pnd_First_Addr = localVariables.newFieldInRecord("pnd_First_Addr", "#FIRST-ADDR", FieldType.BOOLEAN, 1);
        pnd_Close_Addr = localVariables.newFieldInRecord("pnd_Close_Addr", "#CLOSE-ADDR", FieldType.BOOLEAN, 1);

        vw_rt2 = new DataAccessProgramView(new NameInfo("vw_rt2", "RT2"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        rt2_Rt_Record = vw_rt2.getRecord().newGroupInGroup("RT2_RT_RECORD", "RT-RECORD");
        rt2_Rt_A_I_Ind = rt2_Rt_Record.newFieldInGroup("rt2_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rt2_Rt_Table_Id = rt2_Rt_Record.newFieldInGroup("rt2_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rt2_Rt_Short_Key = rt2_Rt_Record.newFieldInGroup("rt2_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        rt2_Rt_Long_Key = rt2_Rt_Record.newFieldInGroup("rt2_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        registerRecord(vw_rt2);

        pnd_Rt2_Super1 = localVariables.newFieldInRecord("pnd_Rt2_Super1", "#RT2-SUPER1", FieldType.STRING, 66);

        pnd_Rt2_Super1__R_Field_16 = localVariables.newGroupInRecord("pnd_Rt2_Super1__R_Field_16", "REDEFINE", pnd_Rt2_Super1);
        pnd_Rt2_Super1_Pnd_Rt2_A_I_Ind = pnd_Rt2_Super1__R_Field_16.newFieldInGroup("pnd_Rt2_Super1_Pnd_Rt2_A_I_Ind", "#RT2-A-I-IND", FieldType.STRING, 
            1);
        pnd_Rt2_Super1_Pnd_Rt2_Table_Id = pnd_Rt2_Super1__R_Field_16.newFieldInGroup("pnd_Rt2_Super1_Pnd_Rt2_Table_Id", "#RT2-TABLE-ID", FieldType.STRING, 
            5);
        pnd_Lp_Holdcode = localVariables.newFieldInRecord("pnd_Lp_Holdcode", "#LP-HOLDCODE", FieldType.BOOLEAN, 1);
        pnd_Ws_Holdcode = localVariables.newFieldInRecord("pnd_Ws_Holdcode", "#WS-HOLDCODE", FieldType.STRING, 4);
        pnd_Pymnt_Ded2 = localVariables.newFieldInRecord("pnd_Pymnt_Ded2", "#PYMNT-DED2", FieldType.PACKED_DECIMAL, 3);
        pnd_Pymnt_Ded3 = localVariables.newFieldArrayInRecord("pnd_Pymnt_Ded3", "#PYMNT-DED3", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 15));
        pnd_Ndx2 = localVariables.newFieldInRecord("pnd_Ndx2", "#NDX2", FieldType.PACKED_DECIMAL, 3);
        pnd_Universalid_Seven = localVariables.newFieldInRecord("pnd_Universalid_Seven", "#UNIVERSALID-SEVEN", FieldType.STRING, 7);
        pnd_Wk_Universalid = localVariables.newFieldInRecord("pnd_Wk_Universalid", "#WK-UNIVERSALID", FieldType.STRING, 12);
        pnd_Cpun500_Parm = localVariables.newFieldInRecord("pnd_Cpun500_Parm", "#CPUN500-PARM", FieldType.STRING, 200);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rt.reset();
        vw_ia_View.reset();
        vw_addr_View.reset();
        vw_addr_Upd.reset();
        vw_can_Pymnt.reset();
        vw_rt2.reset();

        ldaFcpl961.initializeValues();
        ldaFcpltbcd.initializeValues();
        ldaFcpl893g.initializeValues();
        ldaFcpl803l.initializeValues();
        ldaFcpl893l.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Dbl_Quote.setInitialValue("H'7F'");
        pnd_Debug.setInitialValue(false);
        pnd_Tiaa_Ppcn.setInitialValue(false);
        pnd_Netzero.setInitialValue(false);
        pnd_Check_Eft.setInitialValue(false);
        pnd_Check_Rollover.setInitialValue(false);
        pnd_Check_Efthold.setInitialValue(false);
        pnd_Rt2_Super1.setInitialValue("ACHLLP");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn970() throws Exception
    {
        super("Fcpn970");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: FORMAT ( 0 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON;//Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pnd_Ded_Desc_Tab.getValue(1).setValue("Blue cross");                                                                                                              //Natural: ASSIGN #DED-DESC-TAB ( 01 ) := 'Blue cross'
        pnd_Ded_Desc_Tab.getValue(2).setValue("Long term care");                                                                                                          //Natural: ASSIGN #DED-DESC-TAB ( 02 ) := 'Long term care'
        pnd_Ded_Desc_Tab.getValue(3).setValue("Major medical");                                                                                                           //Natural: ASSIGN #DED-DESC-TAB ( 03 ) := 'Major medical'
        pnd_Ded_Desc_Tab.getValue(4).setValue("Group life");                                                                                                              //Natural: ASSIGN #DED-DESC-TAB ( 04 ) := 'Group life'
        pnd_Ded_Desc_Tab.getValue(5).setValue("Overpayment");                                                                                                             //Natural: ASSIGN #DED-DESC-TAB ( 05 ) := 'Overpayment'
        pnd_Ded_Desc_Tab.getValue(6).setValue("Canadian Deduction");                                                                                                      //Natural: ASSIGN #DED-DESC-TAB ( 06 ) := 'Canadian Deduction'
        pnd_Ded_Desc_Tab.getValue(7).setValue("Personal annuity");                                                                                                        //Natural: ASSIGN #DED-DESC-TAB ( 07 ) := 'Personal annuity'
        pnd_Ded_Desc_Tab.getValue(8).setValue("Mutual funds");                                                                                                            //Natural: ASSIGN #DED-DESC-TAB ( 08 ) := 'Mutual funds'
        pnd_Ded_Desc_Tab.getValue(9).setValue("PA Select");                                                                                                               //Natural: ASSIGN #DED-DESC-TAB ( 09 ) := 'PA Select'
        pnd_Ded_Desc_Tab.getValue(10).setValue("Universal Life");                                                                                                         //Natural: ASSIGN #DED-DESC-TAB ( 10 ) := 'Universal Life'
        deductionsdescription_Open.setValue("<DeductionsDescription>");                                                                                                   //Natural: ASSIGN DEDUCTIONSDESCRIPTION-OPEN := '<DeductionsDescription>'
        deductionsdescription_Close.setValue("</DeductionsDescription>");                                                                                                 //Natural: ASSIGN DEDUCTIONSDESCRIPTION-CLOSE := '</DeductionsDescription>'
        ldaFcpl961.getCommon_Xml_Interest_Open().setValue("<Interest>");                                                                                                  //Natural: ASSIGN INTEREST-OPEN := '<Interest>'
        ldaFcpl961.getCommon_Xml_Interest_Close().setValue("</Interest>");                                                                                                //Natural: ASSIGN INTEREST-CLOSE := '</Interest>'
        //* * MOVE BY NAME #WS-HEADER-LEVEL2   TO EXTR
        //* * MOVE BY NAME #WS-OCCURS-LEVEL2(*) TO INV-ACCT(*)
        //*  MOVE BY NAME #WS-NAME-N-ADDR-LEVEL2(*) TO EXTR
        //*  #RT-A-I-IND := 'A'
        //*  #RT-TABLE-ID := 'CLCLP'
        //*  #RT-SHORT-KEY := 'D'
        //*  READ (1) RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
        //*    IF RT.RT-SHORT-KEY NE #RT-SHORT-KEY
        //*      TERMINATE 90
        //*   END-IF
        //*   MOVE RT.RT-LONG-KEY TO #DENVER-PRINTER
        //*   WRITE 'Charlotte Printer ID:' #DENVER-PRINTER
        //*  END-READ
        //* **********************************************************************
        //*  ON ERROR
        //*  MOVE *ERROR-NR TO #GL-ERROR
        //*   COMPRESS *PROGRAM '- LINE ' *ERROR-LINE 'ERROR # ' *ERROR-NR
        //*     INTO #ERROR-MSG
        //*   WRITE  #ERROR-MSG
        //*   TERMINATE 0004
        //*  END-ERROR
        //*  *********************************************
        //*  MAIN ROUTINE
        ldaFcpl961.getCommon_Xml().resetInitial();                                                                                                                        //Natural: RESET INITIAL COMMON-XML #DED-AMT
        pnd_Ded_Amt.resetInitial();
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(1) || pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(2)      //Natural: IF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND = 1 OR = 2 OR = 5 OR = 8
            || pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(5) || pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(8)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "will escape pay type invalid",pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr());                                                   //Natural: WRITE 'will escape pay type invalid' #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
            if (Global.isEscape()) return;
            pnd_Invalid_Rec.setValue(true);                                                                                                                               //Natural: ASSIGN #INVALID-REC := TRUE
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Netzero_Sw.setValue(false);                                                                                                                                   //Natural: ASSIGN #NETZERO-SW := FALSE
        //*  IF #WS-HEADER-RECORD.PYMNT-CHECK-AMT LE 0
        //*   #NETZERO-SW := TRUE
        //*   WRITE 'Will escape, Net Pay 0' #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
        //*   #INVALID-REC := TRUE
        //*   ESCAPE ROUTINE
        //*  END-IF
        pnd_Record_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #RECORD-CNT
        pnd_Payment_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #PAYMENT-CNT
        pnd_Total_Net_Amt.nadd(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                                     //Natural: ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO #TOTAL-NET-AMT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                             //Natural: IF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND = 8
        {
            pnd_Batch_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #BATCH-CNT
        }                                                                                                                                                                 //Natural: END-IF
        //*  DECIDE ON FIRST VALUE #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
        //*   VALUE 1
        //*     ADD 1 TO #BATCH-TOTAL
        //*     ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO CPOL155B.CHECK-AMT(1)
        //*   VALUE 2
        //*     ADD 1 TO #BATCH-TOTAL
        //*   VALUE 5
        //*     ADD 1 TO #BATCH-TOTAL
        //*   VALUE 8
        //*     ADD 1 TO #BATCH-TOTAL
        //*   NONE VALUE
        //*     IGNORE
        //*  END-DECIDE
        pnd_Batch_Counter.setValue(pnd_Batch_Cnt);                                                                                                                        //Natural: ASSIGN #BATCH-COUNTER := #BATCH-CNT
        //*  MOVE '****FIRST TWENTY2***' TO #FIRST-TWENTY
        ldaFcpl961.getCommon_Xml_Pnd_First_Twenty().setValue(pnd_Save_First_Twenty);                                                                                      //Natural: ASSIGN #FIRST-TWENTY := #SAVE-FIRST-TWENTY
        pnd_Total_Net_Amt2.reset();                                                                                                                                       //Natural: RESET #TOTAL-NET-AMT2 #TOTAL-GROSS-AMT #TOTAL-PAYMENT-AMT
        pnd_Total_Gross_Amt.reset();
        pnd_Total_Payment_Amt.reset();
                                                                                                                                                                          //Natural: PERFORM C0000-MOVE-PAYMENT-RECORD
        sub_C0000_Move_Payment_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-XML
        sub_Write_Xml();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM C1000-PROCESS-FUND-RECORD
        sub_C1000_Process_Fund_Record();
        if (condition(Global.isEscape())) {return;}
        //*  IF #BATCH-COUNTER GT 0
                                                                                                                                                                          //Natural: PERFORM LOAD-DEDUCTION-LEGENDS
        sub_Load_Deduction_Legends();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-XML2
        sub_Write_Xml2();
        if (condition(Global.isEscape())) {return;}
        //*  END-IF
        //*  PERFORM WRITE-CONTROL-REPORT
                                                                                                                                                                          //Natural: PERFORM GET-ADDR-RECORD
        sub_Get_Addr_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Addr_Isn.greater(getZero())))                                                                                                                   //Natural: IF #ADDR-ISN GT 0
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-DRI
            sub_Update_Dri();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C0000-MOVE-PAYMENT-RECORD
        //*        #WS-HEADER-RECORD.CNTRCT-HOLD-CDE NE '    '        AND
        //*        #WS-HEADER-RECORD.CNTRCT-HOLD-CDE NE '0000'
        //*  ADD 1 TO #BATCH-COUNTER
        //*  MOVE EDITED #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR (EM=9999999)
        //*    'in Settlement of Contract(s)'
        //*    TIAACREFNUMBER-DATA
        //*        'in Settlement of Certificate(s)'
        //*        TIAACREFNUMBER-DATA
        //*  IF #WS-HEADER-RECORD.CNTRCT-HOLD-CDE = 'PU00'
        //*  #FCPAOPTN.CNTRCT-OPTION-CDE := #WS-HEADER-RECORD.CNTRCT-OPTION-CDE
        //*  #FCPAOPTN.#PREDICT-TABLE  :=  TRUE
        //*  CALLNAT  'FCPNOPTN'  #FCPAOPTN
        //*  TYPEOFANNUITY-DATA :=  #FCPAOPTN.#OPTN-DESC
        //*      COMPRESS 'Survivor Benefit Payment for:'  INTO
        //*        PAYMENTLABEL-DATA
        //*  DECEDENTNAME-DATA := PYMNT-DECEASED-NME(1)
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-XML
        //*      WHEN #CHECK-EFT
        //*       COMPRESS DELIVERYTYPE-OPEN 'P'
        //*         DELIVERYTYPE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*  ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-XML2
        //*  MOVE 'This payment includes after-tax contributions of ..'
        //*    TO AFTERTAXDESC-DATA
        //*      'TIAA reports all taxable payments to the government for'
        //*      'the year in which they are made.  Contracts'
        //*      'and other material have been sent under separate cover.'
        //*      'If you have any questions, please call us at'
        //*      '!800 842-2776, Monday to Friday from 8 a.m. to 10 p.m. '
        //*      'ET and Saturday from 9 a.m. to 6 p.m. ET.'
        //*      'TIAA reports all taxable payments to the government for'
        //*      'the year in which they are made.  Contracts'
        //*      'and other material have been sent under separate cover.'
        //*      'If you have any questions, please call us at'
        //*      '&800 842-2776, Monday to Friday from 8 a.m. to 10 p.m. '
        //*      'ET and Saturday from 9 a.m. to 6 p.m. ET.'
        //*  YOUR PAYMENT HAS BEEN SENT TO YOUR FINANCIAL INSTITUTION AS DIRECTED.'
        //*      'As instructed, we have sent the payment to the address below:'
        //*  END-IF
        //*  ****************************************************** FE201303 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C1000-PROCESS-FUND-RECORD
        //*  IF #TAX-AMT GT 0
        //*    ADD #TAX-AMT TO #TOTAL-TAX-AMT
        //*  END-IF
        //*  ****************************************************** FE201307 END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INSERT-ZIP-INTO-ADDRESS
        //*  ****************************************************** FE201303 END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-3
        //*  ****************************************************** FE201303 END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ROLLOVER
        //*  ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUBPAYMENT-INFO
        //*  NAME="SubDate"
        //*  2ND LINE SUBPAYMENTDETAIL
        //*  ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-DED-LINE
        //*  ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AFTER-TAX-ROUTINE
        //*  *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DEDUCTION-LEGENDS
        //*  **********************************************
        //*  IF CAN-PYMNT.INV-ACCT-CAN-TAX-AMT(1:C-INV-ACCT) GT 0
        //*   #DED-PYMNT-TABLE (4) := '  C'
        //*  END-IF
        //*  ADD #PYMNT-DED-TAB(*) TO #DED-AMT
        //*  *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DEDUCTION-LEGENDS-BY-FUND
        //*  **********************************************
        //*  IF CAN-PYMNT.INV-ACCT-CAN-TAX-AMT(1:C-INV-ACCT) GT 0
        //*   #DED-PYMNT-TABLE (4) := '  C'
        //*  END-IF
        //*  ADD  #WS-OCCURS.PYMNT-DED-AMT (#I-FUND,#NDX)
        //*  ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-LINE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATE-LEDGEND
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE
        //* **************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-DED-TABLE
        //* *******************************
        //*  ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDR-RECORD
        //* *--------------------------------
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-DRI
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SURVIVOR-BENEFITS-HEADINGS
        //*   DECEDENT NAME WILL BE PRINTED
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-HEADINGS
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LUMP-SUM-DEATH-HEADINGS
        //*   DECEDENT NAME WILL BE PRINTED
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATURITY-HEADINGS
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-HEADINGS
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RECURRING-MDO-HEADINGS
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CANADIAN-TAX
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-ACCOUNT-MOVE-SETTLE
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-ACCOUNT-MOVE-UNIT
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-FUND-DESCRIPTION
        //*  **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C9100-GET-SPECIAL-HANDLING
        //*  **********************************************
        //*  INCLUDE FCPC803W
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_C0000_Move_Payment_Record() throws Exception                                                                                                         //Natural: C0000-MOVE-PAYMENT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Save_Eft_Acct.reset();                                                                                                                                        //Natural: RESET #SAVE-EFT-ACCT
        pnd_Check_Eft.setValue(false);                                                                                                                                    //Natural: ASSIGN #CHECK-EFT := FALSE
        pnd_Check_Rollover.setValue(false);                                                                                                                               //Natural: ASSIGN #CHECK-ROLLOVER := FALSE
        pnd_Check_Efthold.setValue(false);                                                                                                                                //Natural: ASSIGN #CHECK-EFTHOLD := FALSE
        short decideConditionsMet1545 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1545++;
            ldaFcpl961.getCommon_Xml_Checkeftind_Data().setValue("CHECK");                                                                                                //Natural: MOVE 'CHECK' TO CHECKEFTIND-DATA
            //*    IF #WS-NAME-N-ADDRESS.PYMNT-EFT-TRANSIT-ID(1:2) GT 0    AND
            if (condition(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1,":",2).notEquals(" ") && (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind().getValue(1,":",2).equals("C")  //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR ( 1:2 ) NE ' ' AND ( #WS-NAME-N-ADDRESS.PYMNT-CHK-SAV-IND ( 1:2 ) EQ 'C' OR #WS-NAME-N-ADDRESS.PYMNT-CHK-SAV-IND ( 1:2 ) EQ 'S' OR #WS-NAME-N-ADDRESS.PYMNT-CHK-SAV-IND ( 1:2 ) EQ ' ' )
                || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind().getValue(1,":",2).equals("S") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind().getValue(1,
                ":",2).equals(" "))))
            {
                pnd_Check_Efthold.setValue(true);                                                                                                                         //Natural: ASSIGN #CHECK-EFTHOLD := TRUE
                pnd_Save_Eft_Acct.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1));                                                         //Natural: ASSIGN #SAVE-EFT-ACCT := #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1545++;
            ldaFcpl961.getCommon_Xml_Checkeftind_Data().setValue("EFT");                                                                                                  //Natural: MOVE 'EFT' TO CHECKEFTIND-DATA
            pnd_Check_Eft.setValue(true);                                                                                                                                 //Natural: ASSIGN #CHECK-EFT := TRUE
            pnd_Save_Eft_Acct.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1));                                                             //Natural: ASSIGN #SAVE-EFT-ACCT := #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(5))))
        {
            decideConditionsMet1545++;
            ldaFcpl961.getCommon_Xml_Checkeftind_Data().setValue("CheckFedEx");                                                                                           //Natural: MOVE 'CheckFedEx' TO CHECKEFTIND-DATA
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(8))))
        {
            decideConditionsMet1545++;
            ldaFcpl961.getCommon_Xml_Checkeftind_Data().setValue("EFT");                                                                                                  //Natural: MOVE 'EFT' TO CHECKEFTIND-DATA
            pnd_Check_Eft.setValue(true);                                                                                                                                 //Natural: ASSIGN #CHECK-EFT := TRUE
            pnd_Check_Rollover.setValue(true);                                                                                                                            //Natural: ASSIGN #CHECK-ROLLOVER := TRUE
            pnd_Save_Eft_Acct.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1));                                                             //Natural: ASSIGN #SAVE-EFT-ACCT := #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR ( 1 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Check_Rollover.getBoolean()))                                                                                                                   //Natural: IF #CHECK-ROLLOVER
        {
                                                                                                                                                                          //Natural: PERFORM INSERT-ZIP-INTO-ADDRESS
            sub_Insert_Zip_Into_Address();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Univ_Switch.setValue(false);                                                                                                                                  //Natural: ASSIGN #UNIV-SWITCH := FALSE
        pnd_Wk_Universalid.reset();                                                                                                                                       //Natural: RESET #WK-UNIVERSALID
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr().greater(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.CNTRCT-UNQ-ID-NBR GT 0
        {
            ldaFcpl961.getCommon_Xml_Universalid_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr(),new ReportEditMask("999999999999"));        //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) TO UNIVERSALID-DATA
            pnd_Wk_Universalid.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr());                                                                          //Natural: ASSIGN #WK-UNIVERSALID := #WS-HEADER-RECORD.CNTRCT-UNQ-ID-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            setValueToSubstring("00000",ldaFcpl961.getCommon_Xml_Universalid_Data(),1,5);                                                                                 //Natural: MOVE '00000' TO SUBSTR ( UNIVERSALID-DATA,1,5 )
            pnd_Universalid_Seven.setValueEdited(pnd_Batch_Counter,new ReportEditMask("9999999"));                                                                        //Natural: MOVE EDITED #BATCH-COUNTER ( EM = 9999999 ) TO #UNIVERSALID-SEVEN
            setValueToSubstring(pnd_Universalid_Seven,ldaFcpl961.getCommon_Xml_Universalid_Data(),6,7);                                                                   //Natural: MOVE #UNIVERSALID-SEVEN TO SUBSTR ( UNIVERSALID-DATA,6,7 )
        }                                                                                                                                                                 //Natural: END-IF
        //*   IAMONTHLYCHECKS:   005009  005017
        //*   DEATHCLAIMCHECKS:  005010  005018
        //*   ADASCHECKS:        005011  005019
        short decideConditionsMet1583 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WS-HEADER-RECORD.CNTRCT-ORGN-CDE;//Natural: VALUE 'AP'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("AP"))))
        {
            decideConditionsMet1583++;
            //*  IA MONTHLY EFT
            if (condition(pnd_Check_Eft.getBoolean()))                                                                                                                    //Natural: IF #CHECK-EFT
            {
                ldaFcpl961.getCommon_Xml_Documentrequestid_Sub_Sys_Id().setValue("009");                                                                                  //Natural: ASSIGN DOCUMENTREQUESTID-SUB-SYS-ID := '009'
                //*  IA MONTHLY CHECK
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Documentrequestid_Sub_Sys_Id().setValue("017");                                                                                  //Natural: ASSIGN DOCUMENTREQUESTID-SUB-SYS-ID := '017'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet1583++;
            //*  DEATH CLAIMS EFT
            if (condition(pnd_Check_Eft.getBoolean()))                                                                                                                    //Natural: IF #CHECK-EFT
            {
                ldaFcpl961.getCommon_Xml_Documentrequestid_Sub_Sys_Id().setValue("010");                                                                                  //Natural: ASSIGN DOCUMENTREQUESTID-SUB-SYS-ID := '010'
                //*  DEATH CLAIMS CHECKS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Documentrequestid_Sub_Sys_Id().setValue("018");                                                                                  //Natural: ASSIGN DOCUMENTREQUESTID-SUB-SYS-ID := '018'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'NZ'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("NZ"))))
        {
            decideConditionsMet1583++;
            //*  ADAS EFT
            if (condition(pnd_Check_Eft.getBoolean()))                                                                                                                    //Natural: IF #CHECK-EFT
            {
                ldaFcpl961.getCommon_Xml_Documentrequestid_Sub_Sys_Id().setValue("011");                                                                                  //Natural: ASSIGN DOCUMENTREQUESTID-SUB-SYS-ID := '011'
                //*  ADAS CHECKS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Documentrequestid_Sub_Sys_Id().setValue("019");                                                                                  //Natural: ASSIGN DOCUMENTREQUESTID-SUB-SYS-ID := '019'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcpl961.getCommon_Xml_Universaltype_Data().setValue("P");                                                                                                      //Natural: MOVE 'P' TO UNIVERSALTYPE-DATA
        //*  MOVE #SAVE-FIRST-TWENTY TO #FIRST-TWENTY
        if (condition(ldaFcpl961.getCommon_Xml_Universalid_Data().notEquals(" ")))                                                                                        //Natural: IF UNIVERSALID-DATA NE ' '
        {
            //*  PIN EXPANSION
            setValueToSubstring("0",ldaFcpl961.getCommon_Xml_Documentrequestid_Uid(),1,1);                                                                                //Natural: MOVE '0' TO SUBSTRING ( DOCUMENTREQUESTID-UID,1,1 )
            setValueToSubstring(ldaFcpl961.getCommon_Xml_Universalid_Data(),ldaFcpl961.getCommon_Xml_Documentrequestid_Uid(),2,12);                                       //Natural: MOVE UNIVERSALID-DATA TO SUBSTR ( DOCUMENTREQUESTID-UID,2,12 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Documentrequestid_Uid().setValue("0000000000000");                                                                                   //Natural: MOVE '0000000000000' TO DOCUMENTREQUESTID-UID
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Documentrequestid_Cnt().setValueEdited(pnd_Batch_Counter,new ReportEditMask("9999999"));                                                 //Natural: MOVE EDITED #BATCH-COUNTER ( EM = 9999999 ) TO DOCUMENTREQUESTID-CNT
        //*  * REMOVE LEADING ZEROS IN UNIVERSALID   /* PIN EXPANSION
        if (condition(pnd_Wk_Universalid.notEquals(" ")))                                                                                                                 //Natural: IF #WK-UNIVERSALID NE ' '
        {
            ldaFcpl961.getCommon_Xml_Universalid_Data().setValue(pnd_Wk_Universalid, MoveOption.LeftJustified);                                                           //Natural: MOVE LEFT #WK-UNIVERSALID TO UNIVERSALID-DATA
        }                                                                                                                                                                 //Natural: END-IF
        //*  *
        //*  #WS-NAME-N-ADDRESS-RECORD
        //*  PYMNT-NME
        //*  PYMNT-ADDR-LINE1-TXT
        if (condition(pnd_Check_Eft.getBoolean() || pnd_Check_Rollover.getBoolean() || pnd_Check_Efthold.getBoolean()))                                                   //Natural: IF #CHECK-EFT OR #CHECK-ROLLOVER OR #CHECK-EFTHOLD
        {
            ldaFcpl961.getCommon_Xml_Fullname_Data().setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(2));                                               //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( 2 ) TO FULLNAME-DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Fullname_Data().setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1));                                               //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( 1 ) TO FULLNAME-DATA
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue("*").reset();                                                                                        //Natural: RESET INSTITUTIONINFOLINE-DATA ( * )
        if (condition(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(1).getSubstring(1,3).equals("CR ")))                                            //Natural: IF SUBSTR ( #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 1 ) ,1,3 ) = 'CR '
        {
            ldaFcpl961.getCommon_Xml_Participantname_Data().setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(1));                             //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 1 ) TO PARTICIPANTNAME-DATA
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Participantname_Data(),1,3), new ExamineSearch("CR "), new ExamineReplace("   "));                 //Natural: EXAMINE SUBSTR ( PARTICIPANTNAME-DATA,1,3 ) FOR 'CR ' REPLACE '   '
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Participantname_Data(),1,3), new ExamineSearch("CR:"), new ExamineReplace("   "));                 //Natural: EXAMINE SUBSTR ( PARTICIPANTNAME-DATA,1,3 ) FOR 'CR:' REPLACE '   '
            ldaFcpl961.getCommon_Xml_Participantname_Data().setValue(ldaFcpl961.getCommon_Xml_Participantname_Data(), MoveOption.LeftJustified);                          //Natural: MOVE LEFT PARTICIPANTNAME-DATA TO PARTICIPANTNAME-DATA
            if (condition(pnd_Check_Eft.getBoolean() || pnd_Check_Rollover.getBoolean() || pnd_Check_Efthold.getBoolean()))                                               //Natural: IF #CHECK-EFT OR #CHECK-ROLLOVER OR #CHECK-EFTHOLD
            {
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 8 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(2).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 2 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(3).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 3 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(4).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 4 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(5).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 5 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(6).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 6 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Participantname_Data().setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1));                                        //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( 1 ) TO PARTICIPANTNAME-DATA
            if (condition(pnd_Check_Eft.getBoolean() || pnd_Check_Rollover.getBoolean() || pnd_Check_Efthold.getBoolean()))                                               //Natural: IF #CHECK-EFT OR #CHECK-ROLLOVER OR #CHECK-EFTHOLD
            {
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1));                    //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 8 )
                if (condition(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).getSubstring(1,3).equals("CR ")))                                           //Natural: IF SUBSTR ( INSTITUTIONINFOLINE-DATA ( 8 ) ,1,3 ) = 'CR '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).setValue(DbsUtil.compress("CR ", ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8))); //Natural: COMPRESS 'CR ' INSTITUTIONINFOLINE-DATA ( 8 ) INTO INSTITUTIONINFOLINE-DATA ( 8 )
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(2).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 2 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(3).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 3 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(4).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 4 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(5).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 5 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(6).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 6 )
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(7).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt().getValue(1));         //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 1 ) TO INSTITUTIONINFOLINE-DATA ( 7 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 7
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(7)); pnd_I.nadd(1))
        {
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(pnd_I)), new ExamineSearch("*"), new ExamineReplace(" "));     //Natural: EXAMINE INSTITUTIONINFOLINE-DATA ( #I ) '*' REPLACE ' '
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  IF #WS-HEADER-RECORD.PYMNT-FOR-DOM (1) = 'F'
        //*  IF #WS-NAME-N-ADDRESS.PYMNT-ADDR-TYPE-IND(1) = 'F'        /* JG171011
        //*  JG171011
        if (condition(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind().getValue(1).equals("F") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind().getValue(1).equals("C"))) //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-ADDR-TYPE-IND ( 1 ) = 'F' OR = 'C'
        {
            ldaFcpl961.getCommon_Xml_Addresstypecode_Data().setValue("F");                                                                                                //Natural: MOVE 'F' TO ADDRESSTYPECODE-DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Addresstypecode_Data().setValue("U");                                                                                                //Natural: MOVE 'U' TO ADDRESSTYPECODE-DATA
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Documentnumber_Data().setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr());                                                    //Natural: MOVE #WS-HEADER-RECORD.CNTRCT-PPCN-NBR TO DOCUMENTNUMBER-DATA
        //*  MOVE PLAN-EMPLOYER-NAME (3) TO PLANID-DATA
        ldaFcpl961.getCommon_Xml_Letterdate_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("YYYY-MM-DD"));                 //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = YYYY-MM-DD ) TO LETTERDATE-DATA
        //* *MOVE 'I' TO ARCHIVALIND-DATA
        ldaFcpl961.getCommon_Xml_Archivalind_Data().setValue("M");                                                                                                        //Natural: MOVE 'M' TO ARCHIVALIND-DATA
        ldaFcpl961.getCommon_Xml_Businessdate_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("YYYY-MM-DD"));               //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = YYYY-MM-DD ) TO BUSINESSDATE-DATA
        ldaFcpl961.getCommon_Xml_Checkdate_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("YYYY-MM-DD"));                  //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = YYYY-MM-DD ) TO CHECKDATE-DATA
        pnd_Ws_Pymnt_Check_Nbr_N10.reset();                                                                                                                               //Natural: RESET #WS-PYMNT-CHECK-NBR-N10
        if (condition(pnd_Check_Eft.getBoolean()))                                                                                                                        //Natural: IF #CHECK-EFT
        {
            pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr());                                          //Natural: MOVE #WS-HEADER-RECORD.PYMNT-CHECK-SCRTY-NBR TO #WS-CHECK-NBR-N7
            ldaFcpl961.getCommon_Xml_Checknumber_Data().setValueEdited(pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7,new ReportEditMask("9999999"));                     //Natural: MOVE EDITED #WS-CHECK-NBR-N7 ( EM = 9999999 ) TO CHECKNUMBER-DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr());                                         //Natural: MOVE #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
            pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                       //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
            ldaFcpl961.getCommon_Xml_Checknumber_Data().setValueEdited(pnd_Ws_Pymnt_Check_Nbr_N10,new ReportEditMask("9999999999"));                                      //Natural: MOVE EDITED #WS-PYMNT-CHECK-NBR-N10 ( EM = 9999999999 ) TO CHECKNUMBER-DATA
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sv_Pymnt_Check_Nbr_A.setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                                                    //Natural: ASSIGN #SV-PYMNT-CHECK-NBR-A := #WS-PYMNT-CHECK-NBR-N10
        ldaFcpl961.getCommon_Xml_Checkamount_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt(),new ReportEditMask("ZZZZZZZZ9.99"));              //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-AMT ( EM = ZZZZZZZZ9.99 ) TO CHECKAMOUNT-DATA
        ldaFcpl961.getCommon_Xml_Checkamount_Data().setValue(ldaFcpl961.getCommon_Xml_Checkamount_Data(), MoveOption.LeftJustified);                                      //Natural: MOVE LEFT CHECKAMOUNT-DATA TO CHECKAMOUNT-DATA
        pnd_Netzero.setValue(false);                                                                                                                                      //Natural: ASSIGN #NETZERO := FALSE
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt().equals(getZero())))                                                                            //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-AMT = 0
        {
            pnd_Netzero.setValue(true);                                                                                                                                   //Natural: ASSIGN #NETZERO := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Netamount_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt(),new ReportEditMask("ZZZZZZZZZZZ9.99"));             //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-AMT ( EM = ZZZZZZZZZZZ9.99 ) TO NETAMOUNT-DATA
        ldaFcpl961.getCommon_Xml_Netamount_Data().setValue(ldaFcpl961.getCommon_Xml_Netamount_Data(), MoveOption.LeftJustified);                                          //Natural: MOVE LEFT NETAMOUNT-DATA TO NETAMOUNT-DATA
        pnd_Nzdc.setValue(false);                                                                                                                                         //Natural: ASSIGN #NZDC := FALSE
        ldaFcpl961.getCommon_Xml_Expagind_Data().setValue("N");                                                                                                           //Natural: MOVE 'N' TO EXPAGIND-DATA
        //*  #CHECK-EFTHOLD WILL USE INSTITUTION
        if (condition(pnd_Check_Eft.getBoolean() || pnd_Check_Rollover.getBoolean()))                                                                                     //Natural: IF #CHECK-EFT OR #CHECK-ROLLOVER
        {
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(1).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(2));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 2 ) TO ADDRESSLINE-DATA ( 1 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(2).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(2));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 2 ) TO ADDRESSLINE-DATA ( 2 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(3).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(2));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 2 ) TO ADDRESSLINE-DATA ( 3 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(4).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(2));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 ) TO ADDRESSLINE-DATA ( 4 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(5).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(2));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 2 ) TO ADDRESSLINE-DATA ( 5 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(6).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt().getValue(2));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 2 ) TO ADDRESSLINE-DATA ( 6 )
            //*  #CHECK-EFTHOLD USE INST ADDRESS(1),CHECK PARTICITANT ALSO (1)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(1).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(1));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 1 ) TO ADDRESSLINE-DATA ( 1 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(2).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(1));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 1 ) TO ADDRESSLINE-DATA ( 2 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(3).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(1));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 1 ) TO ADDRESSLINE-DATA ( 3 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(4).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(1));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 1 ) TO ADDRESSLINE-DATA ( 4 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(5).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(1));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 1 ) TO ADDRESSLINE-DATA ( 5 )
            ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(6).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt().getValue(1));                     //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 1 ) TO ADDRESSLINE-DATA ( 6 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wk_Settlementsystem.reset();                                                                                                                                  //Natural: RESET #WK-SETTLEMENTSYSTEM
        //*  MOVE #WS-HEADER-RECORD.PYMNT-BANK-SOURCE-CODE TO #BANK-SOURCE-CODE
        //*  DECIDE ON FIRST #BANK-SOURCE-CODE
        //*   VALUE '00586'
        //*     MOVE 'IDS' TO SETTLEMENTSYSTEM-DATA
        //*     MOVE 'IDS' TO ACCOUNTNAME-DATA
        //*   VALUE '19992'
        //*     MOVE 'IDS' TO SETTLEMENTSYSTEM-DATA
        //*     MOVE 'TCLIFE' TO ACCOUNTNAME-DATA
        //*   VALUE '42721'
        //*     MOVE 'IDS' TO SETTLEMENTSYSTEM-DATA
        //*     MOVE 'PA' TO ACCOUNTNAME-DATA
        //*   VALUE  '90915'
        //*     MOVE 'IDS' TO SETTLEMENTSYSTEM-DATA
        //*     MOVE 'PS' TO ACCOUNTNAME-DATA
        //*   VALUE '55011','57224'                    /* EFT 57224
        //*     MOVE 'NONIDS' TO SETTLEMENTSYSTEM-DATA
        //*     MOVE 'OMNI' TO ACCOUNTNAME-DATA
        //*     #TIAA-PPCN   := FALSE
        pnd_Ws_Da_Cntrct.reset();                                                                                                                                         //Natural: RESET #WS-DA-CNTRCT
        ldaFcpl961.getCommon_Xml_Settlementsystem_Data().setValue("NONIDS");                                                                                              //Natural: MOVE 'NONIDS' TO SETTLEMENTSYSTEM-DATA
        //* *MOVE 'OMNI' TO ACCOUNTNAME-DATA
        ldaFcpl961.getCommon_Xml_Accountname_Data().setValue("CPS");                                                                                                      //Natural: MOVE 'CPS' TO ACCOUNTNAME-DATA
        pnd_Tiaa_Ppcn.setValue(false);                                                                                                                                    //Natural: ASSIGN #TIAA-PPCN := FALSE
        //*  TIAA
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")            //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R' OR = 'G' OR = 'TG'
            || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("G") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("TG")))
        {
            ldaFcpl961.getCommon_Xml_Tiaacontract_Data().setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr());                                                  //Natural: MOVE #WS-HEADER-RECORD.CNTRCT-PPCN-NBR TO TIAACONTRACT-DATA
            ldaFcpl961.getCommon_Xml_Crefcontract_Data().reset();                                                                                                         //Natural: RESET CREFCONTRACT-DATA
            pnd_Tiaa_Ppcn.setValue(true);                                                                                                                                 //Natural: ASSIGN #TIAA-PPCN := TRUE
            setValueToSubstring(ldaFcpl961.getCommon_Xml_Tiaacontract_Data().getSubstring(8,3),ldaFcpl961.getCommon_Xml_Tiaacontract_Data(),9,3);                         //Natural: MOVE SUBSTR ( TIAACONTRACT-DATA,8,3 ) TO SUBSTR ( TIAACONTRACT-DATA,9,3 )
            setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Tiaacontract_Data(),8,1);                                                                                    //Natural: MOVE '-' TO SUBSTR ( TIAACONTRACT-DATA,8,1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IFSUBSTR(#WS-HEADER-RECORD.CNTRCT-PPCN-NBR,1,1) EQ 'Z' OR = '0' OR = '6'
            ldaFcpl961.getCommon_Xml_Crefcontract_Data().setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr());                                                  //Natural: MOVE #WS-HEADER-RECORD.CNTRCT-PPCN-NBR TO CREFCONTRACT-DATA
            ldaFcpl961.getCommon_Xml_Tiaacontract_Data().reset();                                                                                                         //Natural: RESET TIAACONTRACT-DATA
            pnd_Tiaa_Ppcn.setValue(false);                                                                                                                                //Natural: ASSIGN #TIAA-PPCN := FALSE
            setValueToSubstring(ldaFcpl961.getCommon_Xml_Crefcontract_Data().getSubstring(8,3),ldaFcpl961.getCommon_Xml_Crefcontract_Data(),9,3);                         //Natural: MOVE SUBSTR ( CREFCONTRACT-DATA,8,3 ) TO SUBSTR ( CREFCONTRACT-DATA,9,3 )
            setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Crefcontract_Data(),8,1);                                                                                    //Natural: MOVE '-' TO SUBSTR ( CREFCONTRACT-DATA,8,1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr().equals(" ")))                                                                             //Natural: IF #WS-HEADER-RECORD.CNTRCT-DA-TIAA-1-NBR = ' '
        {
            vw_ia_View.startDatabaseFind                                                                                                                                  //Natural: FIND ( 1 ) IA-VIEW WITH IA-VIEW.CNTRCT-PPCN-NBR = #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
            (
            "F1",
            new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(), WcType.WITH) },
            1
            );
            F1:
            while (condition(vw_ia_View.readNextRow("F1", true)))
            {
                vw_ia_View.setIfNotFoundControlFlag(false);
                if (condition(vw_ia_View.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO RECORDS FOUND
                {
                    //*  TEST
                    getReports().write(0, "No records found",pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr());                                                       //Natural: WRITE 'No records found' #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Ws_Da_Cntrct.setValue(ia_View_Cntrct_Orig_Da_Cntrct_Nbr);                                                                                             //Natural: ASSIGN #WS-DA-CNTRCT := IA-VIEW.CNTRCT-ORIG-DA-CNTRCT-NBR
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Da_Cntrct.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr());                                                                         //Natural: ASSIGN #WS-DA-CNTRCT := #WS-HEADER-RECORD.CNTRCT-DA-TIAA-1-NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Da_Cntrct.notEquals(" ")))                                                                                                                   //Natural: IF #WS-DA-CNTRCT NE ' '
        {
            ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Data().setValue(pnd_Ws_Da_Cntrct);                                                                                    //Natural: ASSIGN TIAACREFNUMBER-DATA := #WS-DA-CNTRCT
            setValueToSubstring(ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Data().getSubstring(8,3),ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Data(),9,3);                     //Natural: MOVE SUBSTR ( TIAACREFNUMBER-DATA,8,3 ) TO SUBSTR ( TIAACREFNUMBER-DATA,9,3 )
            setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Data(),8,1);                                                                                  //Natural: MOVE '-' TO SUBSTR ( TIAACREFNUMBER-DATA,8,1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Tiaa_Ppcn.getBoolean() && ! ((pnd_Ws_Cntrct_Annty_Ins_Type.equals("S") || pnd_Ws_Cntrct_Annty_Ins_Type.equals("M"))))))                        //Natural: IF #TIAA-PPCN AND NOT ( #WS-CNTRCT-ANNTY-INS-TYPE = 'S' OR #WS-CNTRCT-ANNTY-INS-TYPE = 'M' )
        {
            ldaFcpl961.getCommon_Xml_Contractlabel_Data().setValue(DbsUtil.compress("For TIAA Contract", ldaFcpl961.getCommon_Xml_Tiaacontract_Data()));                  //Natural: COMPRESS 'For TIAA Contract' TIAACONTRACT-DATA INTO CONTRACTLABEL-DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  PA-SELECT
            if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("S")))                                                                                                      //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'S'
            {
                if (condition(pnd_Tiaa_Ppcn.getBoolean()))                                                                                                                //Natural: IF #TIAA-PPCN
                {
                    ldaFcpl961.getCommon_Xml_Contractlabel_Data().setValue(DbsUtil.compress("For PA Select Number", ldaFcpl961.getCommon_Xml_Tiaacontract_Data()));       //Natural: COMPRESS 'For PA Select Number' TIAACONTRACT-DATA INTO CONTRACTLABEL-DATA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl961.getCommon_Xml_Contractlabel_Data().setValue(DbsUtil.compress("For PA Select Number", ldaFcpl961.getCommon_Xml_Crefcontract_Data()));       //Natural: COMPRESS 'For PA Select Number' CREFCONTRACT-DATA INTO CONTRACTLABEL-DATA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  SPIA
                if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("M")))                                                                                                  //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'M'
                {
                    if (condition(pnd_Tiaa_Ppcn.getBoolean()))                                                                                                            //Natural: IF #TIAA-PPCN
                    {
                        ldaFcpl961.getCommon_Xml_Contractlabel_Data().setValue(DbsUtil.compress("For SPIA Contract", ldaFcpl961.getCommon_Xml_Tiaacontract_Data()));      //Natural: COMPRESS 'For SPIA Contract' TIAACONTRACT-DATA INTO CONTRACTLABEL-DATA
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaFcpl961.getCommon_Xml_Contractlabel_Data().setValue(DbsUtil.compress("For SPIA Contract", ldaFcpl961.getCommon_Xml_Crefcontract_Data()));      //Natural: COMPRESS 'For SPIA Contract' CREFCONTRACT-DATA INTO CONTRACTLABEL-DATA
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-FUND-DESCRIPTION
                    sub_Obtain_Fund_Description();
                    if (condition(Global.isEscape())) {return;}
                    ldaFcpl961.getCommon_Xml_Contractlabel_Data().setValue(DbsUtil.compress("For CREF", pnd_Ws_Fund_Description, "Certificate", ldaFcpl961.getCommon_Xml_Crefcontract_Data())); //Natural: COMPRESS 'For CREF' #WS-FUND-DESCRIPTION 'Certificate' CREFCONTRACT-DATA INTO CONTRACTLABEL-DATA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Settlementsystem_Data().notEquals(" ")))                                                                                   //Natural: IF SETTLEMENTSYSTEM-DATA NE ' '
        {
            pnd_Wk_Settlementsystem.setValue(ldaFcpl961.getCommon_Xml_Settlementsystem_Data());                                                                           //Natural: ASSIGN #WK-SETTLEMENTSYSTEM := SETTLEMENTSYSTEM-DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Settlementsystem_Data().setValue("NONIDS");                                                                                          //Natural: ASSIGN SETTLEMENTSYSTEM-DATA := 'NONIDS'
            pnd_Wk_Settlementsystem.setValue("NONIDS");                                                                                                                   //Natural: ASSIGN #WK-SETTLEMENTSYSTEM := 'NONIDS'
            //*  FE201707 START
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Lp_Holdcode.setValue(false);                                                                                                                                  //Natural: ASSIGN #LP-HOLDCODE := FALSE
        pnd_Ws_Holdcode.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde());                                                                                   //Natural: ASSIGN #WS-HOLDCODE := #WS-HEADER-RECORD.CNTRCT-HOLD-CDE
                                                                                                                                                                          //Natural: PERFORM C9100-GET-SPECIAL-HANDLING
        sub_C9100_Get_Special_Handling();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Lp_Holdcode.getBoolean()))                                                                                                                      //Natural: IF #LP-HOLDCODE
        {
            pnd_Rt_Super1_Pnd_Rt_A_I_Ind.setValue("A");                                                                                                                   //Natural: ASSIGN #RT-A-I-IND := 'A'
            pnd_Rt_Super1_Pnd_Rt_Table_Id.setValue("CLCLP");                                                                                                              //Natural: ASSIGN #RT-TABLE-ID := 'CLCLP'
            pnd_Rt_Super1_Pnd_Rt_Short_Key.setValue("C");                                                                                                                 //Natural: ASSIGN #RT-SHORT-KEY := 'C'
            vw_rt.startDatabaseRead                                                                                                                                       //Natural: READ ( 1 ) RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
            (
            "READ01",
            new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
            new Oc[] { new Oc("RT_SUPER1", "ASC") },
            1
            );
            READ01:
            while (condition(vw_rt.readNextRow("READ01")))
            {
                if (condition(rt_Rt_Short_Key.notEquals(pnd_Rt_Super1_Pnd_Rt_Short_Key)))                                                                                 //Natural: IF RT.RT-SHORT-KEY NE #RT-SHORT-KEY
                {
                    DbsUtil.terminate(90);  if (true) return;                                                                                                             //Natural: TERMINATE 90
                }                                                                                                                                                         //Natural: END-IF
                pnd_Charlotte_Printer.setValue(rt_Rt_Long_Key);                                                                                                           //Natural: MOVE RT.RT-LONG-KEY TO #CHARLOTTE-PRINTER
                ldaFcpl961.getCommon_Xml_Printerid_Data().setValue(rt_Rt_Long_Key);                                                                                       //Natural: MOVE RT.RT-LONG-KEY TO PRINTERID-DATA
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Holdcode_Data().setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde());                                                          //Natural: MOVE #WS-HEADER-RECORD.CNTRCT-HOLD-CDE TO HOLDCODE-DATA
        //*  IF HOLDCODE-DATA GT ' '
        //*  DECIDE ON FIRST VALUE HOLDCODE-DATA
        //*    VALUE 'C'
        //*      IF PROLINE-LOCAL-PRINT = 'Y' /* HERE HERE
        //*        HOLDCODE-DATA := 'LCLC'    /* LOCAL PRINT CHARLOTTE
        //*        PRINTERID-DATA := #CHARLOTTE-PRINTER  /* LOCAL PRINT CHARLOTTE
        //*      END-IF
        //*    VALUE 'D'
        //*      IF PROLINE-LOCAL-PRINT = 'Y' /* HERE HERE
        //*        HOLDCODE-DATA := 'LCLD'    /* LOCAL PRINT DENVER
        //*        PRINTERID-DATA := #DENVER-PRINTER  /* LOCAL PRINT DENVER
        //*      END-IF
        //*    VALUE 'O'
        //*      HOLDCODE-DATA := 'OV00' /* OVERNIGHT
        //*    VALUE 'P'
        //*      HOLDCODE-DATA := 'PU00' /* PICKUP
        //*    VALUE 'F'
        //*      HOLDCODE-DATA := 'MAPP' /* FOREIGN ADDRESS
        //*    VALUE 'I'
        //*      HOLDCODE-DATA := 'IROP' /* INTERNAL ROLLOVER OMNIPAY
        //*    VALUE 'G'
        //*      HOLDCODE-DATA := 'GLPY' /* GLOBAL PAY/SWIFT PAYMENTS
        //*    VALUE 'A'
        //*      HOLDCODE-DATA := 'CASS' /* ADDRESS CHANGE
        //*                                          (CANZ, CASS, OOTO, OOXO)
        //*    VALUE 'B'
        //*      HOLDCODE-DATA := 'BCU'  /* BENEFIT CONTROL UNIT
        //*    VALUE 'U'
        //*      HOLDCODE-DATA := 'USPS' /* POSTAL SERVICE
        //*    VALUE ' '
        //*      IGNORE                              /* DO NOT HOLD
        //*    NONE
        //*      IF #WS-HEADER-RECORD.CNTRCT-HOLD-CDE NE ' '
        //*        COMPRESS #WS-HEADER-RECORD.CNTRCT-HOLD-CDE 'OHLD'
        //*          INTO HOLDCODE-DATA  /* UNKNOWN HOLD CODE
        //*      END-IF
        //*  END-DECIDE
        //*  ELSE
        //*   HOLD LOGIC FOR:
        //*         1) ZERO CHECK
        //*         2) INTERNAL ROLLOVER
        //*         3) INVALID PAYMENT TYPE
        //*  DECIDE FOR FIRST CONDITION
        //*    WHEN    #PROLINE-LOCAL-PRINT = 'Y'
        //*      HOLDCODE-DATA := 'LCLC'    /* LOCAL PRINT CHARLOTTE
        //*      PRINTERID-DATA := #CHARLOTTE-PRINTER  /* LOCAL PRINT CHARLOTTE
        //*      WHEN #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND = 8   /* 5/1/2012 JWO
        //*        HOLDCODE-DATA := 'IROP' /* INTERNAL ROLLOVER OMNIPAY
        //*    WHEN NONE
        //*      IGNORE
        //*  END-DECIDE
        //*  END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DC") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L")))             //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DC' AND #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L'
        {
            ldaFcpl961.getCommon_Xml_Typeofannuity_Data().setValue(" ");                                                                                                  //Natural: ASSIGN TYPEOFANNUITY-DATA := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Fcpn600_Pnd_Parm_Code.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Option_Cde());                                                                   //Natural: ASSIGN #FCPN600.#PARM-CODE := #WS-HEADER-RECORD.CNTRCT-OPTION-CDE
            pnd_Fcpn600_Pnd_Parm_Desc.reset();                                                                                                                            //Natural: RESET #PARM-DESC
            DbsUtil.callnat(Fcpn600.class , getCurrentProcessState(), pnd_Fcpn600_Pnd_Parm_Code, pnd_Fcpn600_Pnd_Parm_Desc);                                              //Natural: CALLNAT 'FCPN600' #FCPN600.#PARM-CODE #FCPN600.#PARM-DESC
            if (condition(Global.isEscape())) return;
            ldaFcpl961.getCommon_Xml_Typeofannuity_Data().setValue(pnd_Fcpn600_Pnd_Parm_Desc);                                                                            //Natural: ASSIGN TYPEOFANNUITY-DATA := #FCPN600.#PARM-DESC
        }                                                                                                                                                                 //Natural: END-IF
        //*  * 2016/11/12                              START
        short decideConditionsMet1892 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-MODE-CDE;//Natural: VALUE 100
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().equals(100))))
        {
            decideConditionsMet1892++;
            pnd_Ws_Frequency.setValue("Monthly");                                                                                                                         //Natural: ASSIGN #WS-FREQUENCY := 'Monthly'
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(601) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(603)))))
        {
            decideConditionsMet1892++;
            pnd_Ws_Frequency.setValue("Quarterly");                                                                                                                       //Natural: ASSIGN #WS-FREQUENCY := 'Quarterly'
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(701) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(706)))))
        {
            decideConditionsMet1892++;
            pnd_Ws_Frequency.setValue("Semi-Annual");                                                                                                                     //Natural: ASSIGN #WS-FREQUENCY := 'Semi-Annual'
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(801) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(812)))))
        {
            decideConditionsMet1892++;
            pnd_Ws_Frequency.setValue("Annual");                                                                                                                          //Natural: ASSIGN #WS-FREQUENCY := 'Annual'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Ws_Frequency.setValue("Unknown");                                                                                                                         //Natural: ASSIGN #WS-FREQUENCY := 'Unknown'
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet1904 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WS-HEADER-RECORD.CNTRCT-ORGN-CDE;//Natural: VALUE 'NZ'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("NZ"))))
        {
            decideConditionsMet1904++;
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress("Initial", pnd_Ws_Frequency, "Payment for:"));                                         //Natural: COMPRESS 'Initial' #WS-FREQUENCY 'Payment for:' INTO PAYMENTLABEL-DATA
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet1904++;
            short decideConditionsMet1908 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE 'L'
            if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L"))))
            {
                decideConditionsMet1908++;
                                                                                                                                                                          //Natural: PERFORM SURVIVOR-BENEFITS-HEADINGS
                sub_Survivor_Benefits_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'PP'
            else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("PP"))))
            {
                decideConditionsMet1908++;
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals(" ")))                                                                           //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = ' '
                {
                                                                                                                                                                          //Natural: PERFORM SURVIVOR-BENEFITS-HEADINGS
                    sub_Survivor_Benefits_Headings();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                       //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
                    {
                                                                                                                                                                          //Natural: PERFORM MATURITY-HEADINGS
                        sub_Maturity_Headings();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '10'
            else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("10"))))
            {
                decideConditionsMet1908++;
                //*  PA-SELECT SPIA ADDED
                                                                                                                                                                          //Natural: PERFORM MATURITY-HEADINGS
                sub_Maturity_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE '20'
            else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("20"))))
            {
                decideConditionsMet1908++;
                                                                                                                                                                          //Natural: PERFORM TPA-HEADINGS
                sub_Tpa_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE '30'
            else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30"))))
            {
                decideConditionsMet1908++;
                                                                                                                                                                          //Natural: PERFORM MDO-HEADINGS
                sub_Mdo_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE '31'
            else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("31"))))
            {
                decideConditionsMet1908++;
                                                                                                                                                                          //Natural: PERFORM RECURRING-MDO-HEADINGS
                sub_Recurring_Mdo_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE '40'
            else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("40"))))
            {
                decideConditionsMet1908++;
                                                                                                                                                                          //Natural: PERFORM LUMP-SUM-DEATH-HEADINGS
                sub_Lump_Sum_Death_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE '50'
            else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("50"))))
            {
                decideConditionsMet1908++;
                //*  PA SELECT SPIA ADDED
                                                                                                                                                                          //Natural: PERFORM SURVIVOR-BENEFITS-HEADINGS
                sub_Survivor_Benefits_Headings();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'AP'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("AP"))))
        {
            decideConditionsMet1904++;
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress("Periodic Payment For:"));                                                             //Natural: COMPRESS 'Periodic Payment For:' INTO PAYMENTLABEL-DATA
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcpl961.getCommon_Xml_Paymentdate_Data().setValue(ldaFcpl961.getCommon_Xml_Checkdate_Data());                                                                  //Natural: ASSIGN PAYMENTDATE-DATA := CHECKDATE-DATA
                                                                                                                                                                          //Natural: PERFORM GET-CANADIAN-TAX
        sub_Get_Canadian_Tax();
        if (condition(Global.isEscape())) {return;}
        //*   *****************************  END 2016/11/13
        //*  C0000-MOVE-PAYMENT-RECORD
    }
    private void sub_Write_Xml() throws Exception                                                                                                                         //Natural: WRITE-XML
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        if (condition(pnd_First_Record.getBoolean()))                                                                                                                     //Natural: IF #FIRST-RECORD
        {
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Xml_Header());                                                                                                 //Natural: MOVE XML-HEADER TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Documentrequests_Open());                                                                                      //Natural: MOVE DOCUMENTREQUESTS-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Documentrequest_Open());                                                                                           //Natural: MOVE DOCUMENTREQUEST-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl961.getCommon_Xml_Requestdatetime_Data().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYY-MM-DD'T'HH:II:SS"));                                     //Natural: MOVE EDITED *TIMX ( EM = YYYY-MM-DD'T'HH:II:SS ) TO REQUESTDATETIME-DATA
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Requestdatetime_Open(), ldaFcpl961.getCommon_Xml_Requestdatetime_Data(),  //Natural: COMPRESS REQUESTDATETIME-OPEN REQUESTDATETIME-DATA REQUESTDATETIME-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Requestdatetime_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Batchind_Open(), ldaFcpl961.getCommon_Xml_Batchind_Close()));      //Natural: COMPRESS BATCHIND-OPEN BATCHIND-CLOSE INTO #XML-LINE LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Bc_Line.setValue(false);                                                                                                                                      //Natural: ASSIGN #BC-LINE := FALSE
        ldaFcpl961.getCommon_Xml_Batchcounter_Data().setValue(pnd_Batch_Counter);                                                                                         //Natural: MOVE #BATCH-COUNTER TO BATCHCOUNTER-DATA
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Batchcounter_Open(), ldaFcpl961.getCommon_Xml_Batchcounter_Data(), //Natural: COMPRESS BATCHCOUNTER-OPEN BATCHCOUNTER-DATA BATCHCOUNTER-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Batchcounter_Close()));
        pnd_Bc_Line.setValue(true);                                                                                                                                       //Natural: ASSIGN #BC-LINE := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Bc_Line.setValue(false);                                                                                                                                      //Natural: ASSIGN #BC-LINE := FALSE
        ldaFcpl961.getCommon_Xml_Batchtotal_Data().setValue(pnd_Batch_Total);                                                                                             //Natural: MOVE #BATCH-TOTAL TO BATCHTOTAL-DATA
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Batchtotal_Open(), ldaFcpl961.getCommon_Xml_Batchtotal_Data(),     //Natural: COMPRESS BATCHTOTAL-OPEN BATCHTOTAL-DATA BATCHTOTAL-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Batchtotal_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Mailiteminfo_Open());                                                                                              //Natural: MOVE MAILITEMINFO-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*   IAMONTHLYCHECKS:
        //*  DEATHCLAIMCHECKS
        //*  COMPRESS APPLICATIONID2-OPEN 'LGDAILYCHECKS' APPLICATIONID2-CLOSE
        short decideConditionsMet1983 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WS-HEADER-RECORD.CNTRCT-ORGN-CDE;//Natural: VALUE 'AP'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("AP"))))
        {
            decideConditionsMet1983++;
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Applicationid2_Open(), "IAMONTHLYCHECKS", ldaFcpl961.getCommon_Xml_Applicationid2_Close())); //Natural: COMPRESS APPLICATIONID2-OPEN 'IAMONTHLYCHECKS' APPLICATIONID2-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Planid_Data().setValue("IAAP01");                                                                                                    //Natural: MOVE 'IAAP01' TO PLANID-DATA
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet1983++;
            if (condition(pnd_Check_Eft.getBoolean()))                                                                                                                    //Natural: IF #CHECK-EFT
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Applicationid2_Open(), "DEATHCLAIMCHECKS",                 //Natural: COMPRESS APPLICATIONID2-OPEN 'DEATHCLAIMCHECKS' APPLICATIONID2-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Applicationid2_Close()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Applicationid2_Open(), "DEATHCLAIMCHECKS_PKG",             //Natural: COMPRESS APPLICATIONID2-OPEN 'DEATHCLAIMCHECKS_PKG' APPLICATIONID2-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Applicationid2_Close()));
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl961.getCommon_Xml_Planid_Data().setValue("IADC01");                                                                                                    //Natural: MOVE 'IADC01' TO PLANID-DATA
        }                                                                                                                                                                 //Natural: VALUE 'NZ'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("NZ"))))
        {
            decideConditionsMet1983++;
            if (condition(pnd_Check_Eft.getBoolean()))                                                                                                                    //Natural: IF #CHECK-EFT
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Applicationid2_Open(), "ADASCHECKS", ldaFcpl961.getCommon_Xml_Applicationid2_Close())); //Natural: COMPRESS APPLICATIONID2-OPEN 'ADASCHECKS' APPLICATIONID2-CLOSE INTO #XML-LINE LEAVING NO SPACE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Applicationid2_Open(), "ADASCHECKS_PKG",                   //Natural: COMPRESS APPLICATIONID2-OPEN 'ADASCHECKS_PKG' APPLICATIONID2-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Applicationid2_Close()));
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl961.getCommon_Xml_Planid_Data().setValue("ADAS01");                                                                                                    //Natural: MOVE 'ADAS01' TO PLANID-DATA
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Univ_Line.setValue(false);                                                                                                                                    //Natural: ASSIGN #UNIV-LINE := FALSE
        pnd_Dr_Line.setValue(false);                                                                                                                                      //Natural: ASSIGN #DR-LINE := FALSE
        if (condition(ldaFcpl961.getCommon_Xml_Universalid_Data().notEquals(" ")))                                                                                        //Natural: IF UNIVERSALID-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Universalid_Open(), ldaFcpl961.getCommon_Xml_Universalid_Data(),  //Natural: COMPRESS UNIVERSALID-OPEN UNIVERSALID-DATA UNIVERSALID-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Universalid_Close()));
            pnd_Univ_Line.setValue(true);                                                                                                                                 //Natural: ASSIGN #UNIV-LINE := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            pnd_Univ_Line.setValue(false);                                                                                                                                //Natural: ASSIGN #UNIV-LINE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Universaltype_Data().notEquals(" ")))                                                                                      //Natural: IF UNIVERSALTYPE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Universaltype_Open(), ldaFcpl961.getCommon_Xml_Universaltype_Data(),  //Natural: COMPRESS UNIVERSALTYPE-OPEN UNIVERSALTYPE-DATA UNIVERSALTYPE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Universaltype_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sve_Documentrequestid_Data.setValue(ldaFcpl961.getCommon_Xml_Documentrequestid_Data());                                                                       //Natural: ASSIGN #SVE-DOCUMENTREQUESTID-DATA := DOCUMENTREQUESTID-DATA
        if (condition(ldaFcpl961.getCommon_Xml_Documentrequestid_Data().notEquals(" ")))                                                                                  //Natural: IF DOCUMENTREQUESTID-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Documentrequestid_Open(), ldaFcpl961.getCommon_Xml_Documentrequestid_Data(),  //Natural: COMPRESS DOCUMENTREQUESTID-OPEN DOCUMENTREQUESTID-DATA DOCUMENTREQUESTID-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Documentrequestid_Close()));
            pnd_Dr_Line.setValue(true);                                                                                                                                   //Natural: ASSIGN #DR-LINE := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            pnd_Dr_Line.setValue(false);                                                                                                                                  //Natural: ASSIGN #DR-LINE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Printerid_Data().notEquals(" ") && pnd_Lp_Holdcode.getBoolean()))                                                          //Natural: IF PRINTERID-DATA NE ' ' AND #LP-HOLDCODE
        {
            //*    #WS-HEADER-RECORD.CNTRCT-HOLD-CDE = 'PU00'
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Printerid_Open(), ldaFcpl961.getCommon_Xml_Printerid_Data(),   //Natural: COMPRESS PRINTERID-OPEN PRINTERID-DATA PRINTERID-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Printerid_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Fullname_Data().notEquals(" ")))                                                                                           //Natural: IF FULLNAME-DATA NE ' '
        {
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Fullname_Data()), new ExamineSearch("&"), new ExamineReplace(" "));                                //Natural: EXAMINE FULLNAME-DATA FOR '&' REPLACE WITH ' '
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Fullname_Open(), ldaFcpl961.getCommon_Xml_Fullname_Data(),     //Natural: COMPRESS FULLNAME-OPEN FULLNAME-DATA FULLNAME-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Fullname_Close()));
            pnd_Fn_Line.setValue(true);                                                                                                                                   //Natural: ASSIGN #FN-LINE := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            pnd_Fn_Line.setValue(false);                                                                                                                                  //Natural: ASSIGN #FN-LINE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Addresstypecode_Data().notEquals(" ")))                                                                                    //Natural: IF ADDRESSTYPECODE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Addresstypecode_Open(), ldaFcpl961.getCommon_Xml_Addresstypecode_Data(),  //Natural: COMPRESS ADDRESSTYPECODE-OPEN ADDRESSTYPECODE-DATA ADDRESSTYPECODE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Addresstypecode_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_First_Addr.setValue(true);                                                                                                                                    //Natural: ASSIGN #FIRST-ADDR := TRUE
        pnd_Close_Addr.setValue(false);                                                                                                                                   //Natural: ASSIGN #CLOSE-ADDR := FALSE
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 6
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                pnd_Adr_Line.setValue(true);                                                                                                                              //Natural: ASSIGN #ADR-LINE := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Adr_Line.setValue(false);                                                                                                                             //Natural: ASSIGN #ADR-LINE := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(pnd_I).notEquals(" ")))                                                                    //Natural: IF ADDRESSLINE-DATA ( #I ) NE ' '
            {
                if (condition(pnd_First_Addr.getBoolean()))                                                                                                               //Natural: IF #FIRST-ADDR
                {
                    pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Addresslines_Open());                                                                                  //Natural: MOVE ADDRESSLINES-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_First_Addr.setValue(false);                                                                                                                       //Natural: ASSIGN #FIRST-ADDR := FALSE
                    pnd_Close_Addr.setValue(true);                                                                                                                        //Natural: ASSIGN #CLOSE-ADDR := TRUE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(pnd_I)), new ExamineSearch("&"), new ExamineReplace(" "));         //Natural: EXAMINE ADDRESSLINE-DATA ( #I ) FOR '&' REPLACE WITH ' '
                //*  JWO1
                pnd_Cpun500_Parm.setValue(ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(pnd_I));                                                                   //Natural: MOVE ADDRESSLINE-DATA ( #I ) TO #CPUN500-PARM
                //*  JWO1
                DbsUtil.callnat(Cpun500.class , getCurrentProcessState(), pnd_Cpun500_Parm);                                                                              //Natural: CALLNAT 'CPUN500' #CPUN500-PARM
                if (condition(Global.isEscape())) return;
                //*  JWO1
                ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(pnd_I).setValue(pnd_Cpun500_Parm);                                                                   //Natural: MOVE #CPUN500-PARM TO ADDRESSLINE-DATA ( #I )
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Addressline_Open(), ldaFcpl961.getCommon_Xml_Addressline_Data().getValue(pnd_I),  //Natural: COMPRESS ADDRESSLINE-OPEN ADDRESSLINE-DATA ( #I ) ADDRESSLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Addressline_Close()));
                pnd_Ins_Line.setValue(true);                                                                                                                              //Natural: ASSIGN #INS-LINE := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ins_Line.setValue(false);                                                                                                                             //Natural: ASSIGN #INS-LINE := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Close_Addr.getBoolean()))                                                                                                                       //Natural: IF #CLOSE-ADDR
        {
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Addresslines_Close());                                                                                         //Natural: MOVE ADDRESSLINES-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Letterdate_Data().notEquals(" ")))                                                                                         //Natural: IF LETTERDATE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Letterdate_Open(), ldaFcpl961.getCommon_Xml_Letterdate_Data(), //Natural: COMPRESS LETTERDATE-OPEN LETTERDATE-DATA LETTERDATE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Letterdate_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE 'S' TO DELIVERYTYPE-DATA          /*  ALWAYS "S"
        //*  COMPRESS DELIVERYTYPE-OPEN DELIVERYTYPE-DATA
        //*   DELIVERYTYPE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*  FE201707
        short decideConditionsMet2090 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #LP-HOLDCODE
        if (condition(pnd_Lp_Holdcode.getBoolean()))
        {
            decideConditionsMet2090++;
            //*  WHEN #WS-HEADER-RECORD.CNTRCT-HOLD-CDE = 'PU00' /* FE201707
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Deliverytype_Open(), "L", ldaFcpl961.getCommon_Xml_Deliverytype_Close())); //Natural: COMPRESS DELIVERYTYPE-OPEN 'L' DELIVERYTYPE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2090 == 0))
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Deliverytype_Open(), "S", ldaFcpl961.getCommon_Xml_Deliverytype_Close())); //Natural: COMPRESS DELIVERYTYPE-OPEN 'S' DELIVERYTYPE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpl961.getCommon_Xml_Expagind_Data().notEquals(" ")))                                                                                           //Natural: IF EXPAGIND-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Expagind_Open(), ldaFcpl961.getCommon_Xml_Expagind_Data(),     //Natural: COMPRESS EXPAGIND-OPEN EXPAGIND-DATA EXPAGIND-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Expagind_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Archivalind_Data().notEquals(" ")))                                                                                        //Natural: IF ARCHIVALIND-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Archivalind_Open(), ldaFcpl961.getCommon_Xml_Archivalind_Data(),  //Natural: COMPRESS ARCHIVALIND-OPEN ARCHIVALIND-DATA ARCHIVALIND-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Archivalind_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Planid_Data().notEquals(" ")))                                                                                             //Natural: IF PLANID-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Planid_Open(), ldaFcpl961.getCommon_Xml_Planid_Data(),         //Natural: COMPRESS PLANID-OPEN PLANID-DATA PLANID-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Planid_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Businessdate_Data().notEquals(" ")))                                                                                       //Natural: IF BUSINESSDATE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Businessdate_Open(), ldaFcpl961.getCommon_Xml_Businessdate_Data(),  //Natural: COMPRESS BUSINESSDATE-OPEN BUSINESSDATE-DATA BUSINESSDATE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Businessdate_Close()));
            //*  JWO 08/2012
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Mailiteminfo_Close());                                                                                             //Natural: MOVE MAILITEMINFO-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Documentinfo_Open());                                                                                              //Natural: MOVE DOCUMENTINFO-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Prolinechecks_Open());                                                                                             //Natural: MOVE PROLINECHECKS-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Headerinfo_Open());                                                                                                //Natural: MOVE HEADERINFO-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpl961.getCommon_Xml_Settlementsystem_Data().notEquals(" ")))                                                                                   //Natural: IF SETTLEMENTSYSTEM-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Settlementsystem_Open(), ldaFcpl961.getCommon_Xml_Settlementsystem_Data(),  //Natural: COMPRESS SETTLEMENTSYSTEM-OPEN SETTLEMENTSYSTEM-DATA SETTLEMENTSYSTEM-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Settlementsystem_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Checkeftind_Data().notEquals(" ")))                                                                                        //Natural: IF CHECKEFTIND-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Checkeftind_Open(), ldaFcpl961.getCommon_Xml_Checkeftind_Data(),  //Natural: COMPRESS CHECKEFTIND-OPEN CHECKEFTIND-DATA CHECKEFTIND-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Checkeftind_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Holdcode_Data().notEquals(" ")))                                                                                           //Natural: IF HOLDCODE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Holdcode_Open(), ldaFcpl961.getCommon_Xml_Holdcode_Data(),     //Natural: COMPRESS HOLDCODE-OPEN HOLDCODE-DATA HOLDCODE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Holdcode_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            ldaFcpl961.getCommon_Xml_Holdcode_Data().reset();                                                                                                             //Natural: RESET HOLDCODE-DATA
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Tiaaaddress_Open());                                                                                               //Natural: MOVE TIAAADDRESS-OPEN TO #XML-LINE
        //*  IDS START
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Tiaaaddressline_Open(), ldaFcpl961.getCommon_Xml_Tiaaaddressline_Data().getValue(ldaFcpl961.getCommon_Xml_Nonids_Index(),pnd_I),  //Natural: COMPRESS TIAAADDRESSLINE-OPEN TIAAADDRESSLINE-DATA ( NONIDS-INDEX,#I ) TIAAADDRESSLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Tiaaaddressline_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Tiaaaddress_Close());                                                                                              //Natural: MOVE TIAAADDRESS-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Tiaaphonenumber_Open(), ldaFcpl961.getCommon_Xml_Tiaaphonenumber_Data().getValue(ldaFcpl961.getCommon_Xml_Nonids_Index()),  //Natural: COMPRESS TIAAPHONENUMBER-OPEN TIAAPHONENUMBER-DATA ( NONIDS-INDEX ) TIAAPHONENUMBER-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Tiaaphonenumber_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*  MOVE #BANK-SOURCE-CODE TO CPOA110.CPOA110-SOURCE-CODE
        //*  CALLNAT 'CPON110' CPOA110
        //*  IF CPOA110-RETURN-CODE NE '00'
        //*   WRITE 'Could not find Bank Information'
        //*   TERMINATE 50
        //*  END-IF
        ldaFcpl961.getCommon_Xml_Bankname_Data().setValue(pdaFcpa110.getFcpa110_Bank_Name());                                                                             //Natural: MOVE FCPA110.BANK-NAME TO BANKNAME-DATA
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Bankname_Open(), ldaFcpl961.getCommon_Xml_Bankname_Data(),         //Natural: COMPRESS BANKNAME-OPEN BANKNAME-DATA BANKNAME-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Bankname_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Bankaddress_Open());                                                                                               //Natural: MOVE BANKADDRESS-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl961.getCommon_Xml_Bankaddressline_Data().setValue(pdaFcpa110.getFcpa110_Bank_Address1());                                                                  //Natural: MOVE FCPA110.BANK-ADDRESS1 TO BANKADDRESSLINE-DATA
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Bankaddressline_Open(), ldaFcpl961.getCommon_Xml_Bankaddressline_Data(),  //Natural: COMPRESS BANKADDRESSLINE-OPEN BANKADDRESSLINE-DATA BANKADDRESSLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Bankaddressline_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*  IF FCPA110.BANK-ADDRESS2 NE ' '
        //*   MOVE FCPA110.BANK-ADDRESS2 TO BANKADDRESSLINE-DATA
        //*   COMPRESS BANKADDRESSLINE-OPEN BANKADDRESSLINE-DATA
        //*     BANKADDRESSLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*   PERFORM WRITE-WORK-3
        //*  END-IF
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Bankaddress_Close());                                                                                              //Natural: MOVE BANKADDRESS-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa110.getFcpa110_Bank_Above_Check_Amt_Nbr().separate(SeparateOption.WithAnyDelimiters, "/", ldaFcpl961.getCommon_Xml_Bankfractional1_Data(),                 //Natural: SEPARATE FCPA110.BANK-ABOVE-CHECK-AMT-NBR INTO BANKFRACTIONAL1-DATA BANKFRACTIONAL2-DATA WITH DELIMITER '/'
            ldaFcpl961.getCommon_Xml_Bankfractional2_Data());
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Bankfractional1_Open(), ldaFcpl961.getCommon_Xml_Bankfractional1_Data(),  //Natural: COMPRESS BANKFRACTIONAL1-OPEN BANKFRACTIONAL1-DATA BANKFRACTIONAL1-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Bankfractional1_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Bankfractional2_Open(), ldaFcpl961.getCommon_Xml_Bankfractional2_Data(),  //Natural: COMPRESS BANKFRACTIONAL2-OPEN BANKFRACTIONAL2-DATA BANKFRACTIONAL2-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Bankfractional2_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*  MOVE FCPA110.BANK-ROUTING TO BANKROUTINGNUMBER-DATA
        ldaFcpl961.getCommon_Xml_Bankroutingnumber_Data().setValue(pdaFcpa110.getFcpa110_Routing_No());                                                                   //Natural: MOVE FCPA110.ROUTING-NO TO BANKROUTINGNUMBER-DATA
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Bankroutingnumber_Open(), ldaFcpl961.getCommon_Xml_Bankroutingnumber_Data(),  //Natural: COMPRESS BANKROUTINGNUMBER-OPEN BANKROUTINGNUMBER-DATA BANKROUTINGNUMBER-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Bankroutingnumber_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl961.getCommon_Xml_Bankaccountnumber_Data().setValue(pdaFcpa110.getFcpa110_Account_No());                                                                   //Natural: MOVE FCPA110.ACCOUNT-NO TO BANKACCOUNTNUMBER-DATA
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Bankaccountnumber_Open(), ldaFcpl961.getCommon_Xml_Bankaccountnumber_Data(),  //Natural: COMPRESS BANKACCOUNTNUMBER-OPEN BANKACCOUNTNUMBER-DATA BANKACCOUNTNUMBER-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Bankaccountnumber_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpl961.getCommon_Xml_Checknumber_Data().notEquals(" ")))                                                                                        //Natural: IF CHECKNUMBER-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Checknumber_Open(), ldaFcpl961.getCommon_Xml_Checknumber_Data(),  //Natural: COMPRESS CHECKNUMBER-OPEN CHECKNUMBER-DATA CHECKNUMBER-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Checknumber_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Checkdate_Data().notEquals(" ")))                                                                                          //Natural: IF CHECKDATE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Checkdate_Open(), ldaFcpl961.getCommon_Xml_Checkdate_Data(),   //Natural: COMPRESS CHECKDATE-OPEN CHECKDATE-DATA CHECKDATE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Checkdate_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Checkamount_Data().notEquals(" ")))                                                                                        //Natural: IF CHECKAMOUNT-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Checkamount_Open(), ldaFcpl961.getCommon_Xml_Checkamount_Data(),  //Natural: COMPRESS CHECKAMOUNT-OPEN CHECKAMOUNT-DATA CHECKAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Checkamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Accountname_Data().notEquals(" ")))                                                                                        //Natural: IF ACCOUNTNAME-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Accountname_Open(), ldaFcpl961.getCommon_Xml_Accountname_Data(),  //Natural: COMPRESS ACCOUNTNAME-OPEN ACCOUNTNAME-DATA ACCOUNTNAME-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Accountname_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Signatorytitle_Data().setValue(pdaFcpa110.getFcpa110_Title());                                                                           //Natural: MOVE FCPA110.TITLE TO SIGNATORYTITLE-DATA
        if (condition(ldaFcpl961.getCommon_Xml_Signatorytitle_Data().notEquals(" ")))                                                                                     //Natural: IF SIGNATORYTITLE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Signatorytitle_Open(), ldaFcpl961.getCommon_Xml_Signatorytitle_Data(),  //Natural: COMPRESS SIGNATORYTITLE-OPEN SIGNATORYTITLE-DATA SIGNATORYTITLE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Signatorytitle_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Settlementsystemcodes_Data().setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde());                                             //Natural: ASSIGN SETTLEMENTSYSTEMCODES-DATA := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Settlementsystemcodes_Open(), ldaFcpl961.getCommon_Xml_Settlementsystemcodes_Data(),  //Natural: COMPRESS SETTLEMENTSYSTEMCODES-OPEN SETTLEMENTSYSTEMCODES-DATA SETTLEMENTSYSTEMCODES-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Settlementsystemcodes_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Headerinfo_Close());                                                                                               //Natural: MOVE HEADERINFO-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*  DECIDE ON FIRST SETTLEMENTSYSTEM-DATA
        //*   VALUE 'IDS'
        //*     MOVE IDSINFO-OPEN TO #XML-LINE
        //*     PERFORM WRITE-WORK-3
        //*                                                       /*     MB  07/19
        //*    EXAMINE DISBURSEMENTDESCRIPTION-DATA FOR '&' REPLACE WITH ' '
        //*    COMPRESS DISBURSEMENTDESCRIPTION-OPEN DISBURSEMENTDESCRIPTION-DATA
        //*      DISBURSEMENTDESCRIPTION-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*    PERFORM WRITE-WORK-3
        //*                                                        /*     MB  07/19
        //*    EXAMINE NAMELABEL-DATA FOR '&' REPLACE WITH ' '
        //*    COMPRESS NAMELABEL-OPEN NAMELABEL-DATA NAMELABEL-CLOSE
        //*      INTO #XML-LINE LEAVING NO SPACE
        //*    PERFORM WRITE-WORK-3
        //*                                                        /*     MB  07/19
        //*    EXAMINE INSUREDNAME-DATA FOR '&' REPLACE WITH ' '
        //*    COMPRESS INSUREDNAME-OPEN INSUREDNAME-DATA INSUREDNAME-CLOSE
        //*      INTO #XML-LINE LEAVING NO SPACE
        //*    PERFORM WRITE-WORK-3
        //*                                                        /*     MB  07/19
        //*    EXAMINE DOCUMENTLABEL1-DATA FOR '&' REPLACE WITH ' '
        //*    COMPRESS DOCUMENTLABEL1-OPEN DOCUMENTLABEL1-DATA
        //*      DOCUMENTLABEL1-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*    PERFORM WRITE-WORK-3
        //*                                                        /*     MB  07/19
        //*    EXAMINE DOCUMENTLABEL2-DATA FOR '&' REPLACE WITH ' '
        //*    COMPRESS DOCUMENTLABEL2-OPEN DOCUMENTLABEL2-DATA
        //*      DOCUMENTLABEL2-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*    PERFORM WRITE-WORK-3
        //*    COMPRESS DOCUMENTNUMBER-OPEN DOCUMENTNUMBER-DATA
        //*      DOCUMENTNUMBER-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*    PERFORM WRITE-WORK-3
        //*    MOVE  COMMENTS-OPEN TO #XML-LINE
        //*    PERFORM WRITE-WORK-3
        //*    #RT-A-I-IND := 'A'
        //*    #RT-TABLE-ID := 'CIDSC'
        //*    #RT-SHORT-KEY := #BANK-SOURCE-CODE
        //*    READ (1) RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
        //*      IF RT.RT-SHORT-KEY NE #BANK-SOURCE-CODE
        //*        TERMINATE 90
        //*      END-IF
        //*      DECIDE FOR EVERY CONDITION
        //*        WHEN RT-DESC1 NE ' '
        //*          EXAMINE RT-DESC1 FOR '&' REPLACE WITH ' '     /*     MB  07/19
        //*          MOVE RT-DESC1 TO COMMENTLINE-DATA
        //*          COMPRESS COMMENTLINE-OPEN COMMENTLINE-DATA
        //*            COMMENTLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*          PERFORM WRITE-WORK-3
        //*        WHEN RT-DESC2 NE ' '
        //*          EXAMINE RT-DESC2 FOR '&' REPLACE WITH ' '     /*     MB  07/19
        //*          MOVE RT-DESC2 TO COMMENTLINE-DATA
        //*          COMPRESS COMMENTLINE-OPEN COMMENTLINE-DATA
        //*            COMMENTLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*          PERFORM WRITE-WORK-3
        //*        WHEN RT-DESC3 NE ' '
        //*          EXAMINE RT-DESC3 FOR '&' REPLACE WITH ' '     /*     MB  07/19
        //*          MOVE RT-DESC3 TO COMMENTLINE-DATA
        //*          COMPRESS COMMENTLINE-OPEN COMMENTLINE-DATA
        //*            COMMENTLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*          PERFORM WRITE-WORK-3
        //*        WHEN NONE
        //*          IGNORE
        //*      END-DECIDE
        //*    END-READ
        //*    MOVE  COMMENTS-CLOSE TO #XML-LINE
        //*    PERFORM WRITE-WORK-3
        //*    MOVE  IDSINFO-CLOSE TO #XML-LINE
        //*    PERFORM WRITE-WORK-3
        //*  VALUE 'NONIDS'
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Nonidsinfo_Open());                                                                                                //Natural: MOVE NONIDSINFO-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpl961.getCommon_Xml_Participantname_Data().notEquals(" ")))                                                                                    //Natural: IF PARTICIPANTNAME-DATA NE ' '
        {
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Participantname_Data()), new ExamineSearch("&"), new ExamineReplace(" "));                         //Natural: EXAMINE PARTICIPANTNAME-DATA FOR '&' REPLACE WITH ' '
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Participantname_Open(), ldaFcpl961.getCommon_Xml_Participantname_Data(),  //Natural: COMPRESS PARTICIPANTNAME-OPEN PARTICIPANTNAME-DATA PARTICIPANTNAME-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Participantname_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*    INITIALPAYMENTFREQUENCY
        short decideConditionsMet2320 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-MODE-CDE;//Natural: VALUE 100
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().equals(100))))
        {
            decideConditionsMet2320++;
            pnd_Ws_Frequency.setValue("Monthly");                                                                                                                         //Natural: ASSIGN #WS-FREQUENCY := 'Monthly'
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(601) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(603)))))
        {
            decideConditionsMet2320++;
            pnd_Ws_Frequency.setValue("Quarterly");                                                                                                                       //Natural: ASSIGN #WS-FREQUENCY := 'Quarterly'
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(701) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(706)))))
        {
            decideConditionsMet2320++;
            pnd_Ws_Frequency.setValue("Semi-Annual");                                                                                                                     //Natural: ASSIGN #WS-FREQUENCY := 'Semi-Annual'
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(801) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(812)))))
        {
            decideConditionsMet2320++;
            pnd_Ws_Frequency.setValue("Annual");                                                                                                                          //Natural: ASSIGN #WS-FREQUENCY := 'Annual'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Ws_Frequency.setValue("Unknown");                                                                                                                         //Natural: ASSIGN #WS-FREQUENCY := 'Unknown'
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcpl961.getCommon_Xml_Initialpaymentfrequency_Data().setValue(pnd_Ws_Frequency);                                                                               //Natural: ASSIGN INITIALPAYMENTFREQUENCY-DATA := #WS-FREQUENCY
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Initialpaymentfrequency_Open(), ldaFcpl961.getCommon_Xml_Initialpaymentfrequency_Data(),  //Natural: COMPRESS INITIALPAYMENTFREQUENCY-OPEN INITIALPAYMENTFREQUENCY-DATA INITIALPAYMENTFREQUENCY-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Initialpaymentfrequency_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpl961.getCommon_Xml_Typeofannuity_Data().notEquals(" ")))                                                                                      //Natural: IF TYPEOFANNUITY-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Typeofannuity_Open(), ldaFcpl961.getCommon_Xml_Typeofannuity_Data(),  //Natural: COMPRESS TYPEOFANNUITY-OPEN TYPEOFANNUITY-DATA TYPEOFANNUITY-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Typeofannuity_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Tiaacontract_Data().notEquals(" ")))                                                                                       //Natural: IF TIAACONTRACT-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Tiaacontract_Open(), ldaFcpl961.getCommon_Xml_Tiaacontract_Data(),  //Natural: COMPRESS TIAACONTRACT-OPEN TIAACONTRACT-DATA TIAACONTRACT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Tiaacontract_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaFcpl961.getCommon_Xml_Crefcontract_Data().notEquals(" ")))                                                                                   //Natural: IF CREFCONTRACT-DATA NE ' '
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Crefcontract_Open(), ldaFcpl961.getCommon_Xml_Crefcontract_Data(),  //Natural: COMPRESS CREFCONTRACT-OPEN CREFCONTRACT-DATA CREFCONTRACT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Crefcontract_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR DC PAYMENTS  2016/11/14 START
        if (condition(ldaFcpl961.getCommon_Xml_Decedentname_Data().notEquals(" ")))                                                                                       //Natural: IF DECEDENTNAME-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Decedentname_Open(), ldaFcpl961.getCommon_Xml_Decedentname_Data(),  //Natural: COMPRESS DECEDENTNAME-OPEN DECEDENTNAME-DATA DECEDENTNAME-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Decedentname_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Paymentlabel_Open(), ldaFcpl961.getCommon_Xml_Paymentlabel_Data(), //Natural: COMPRESS PAYMENTLABEL-OPEN PAYMENTLABEL-DATA PAYMENTLABEL-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Paymentlabel_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpl961.getCommon_Xml_Contractlabel_Data().notEquals(" ")))                                                                                      //Natural: IF CONTRACTLABEL-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Contractlabel_Open(), ldaFcpl961.getCommon_Xml_Contractlabel_Data(),  //Natural: COMPRESS CONTRACTLABEL-OPEN CONTRACTLABEL-DATA CONTRACTLABEL-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Contractlabel_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Paymentdate_Data().notEquals(" ")))                                                                                        //Natural: IF PAYMENTDATE-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Paymentdate_Open(), ldaFcpl961.getCommon_Xml_Paymentdate_Data(),  //Natural: COMPRESS PAYMENTDATE-OPEN PAYMENTDATE-DATA PAYMENTDATE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Paymentdate_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  2016/11/14                  END
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Tiaacreflist_Open());                                                                                              //Natural: MOVE TIAACREFLIST-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Data().notEquals(" ")))                                                                                     //Natural: IF TIAACREFNUMBER-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Open(), ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Data(),  //Natural: COMPRESS TIAACREFNUMBER-OPEN TIAACREFNUMBER-DATA TIAACREFNUMBER-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Tiaacrefnumber_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Tiaacreflist_Close());                                                                                             //Natural: MOVE TIAACREFLIST-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*  ANNUITYSTARTDATE
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DC") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("L")))             //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DC' AND #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Settlmnt_Dte_A.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO #PYMNT-SETTLMNT-DTE-A
            if (condition(DbsUtil.maskMatches(pnd_Pymnt_Settlmnt_Dte_A,"YYYYMMDD")))                                                                                      //Natural: IF #PYMNT-SETTLMNT-DTE-A EQ MASK ( YYYYMMDD )
            {
                setValueToSubstring(pnd_Pymnt_Settlmnt_Dte_A.getSubstring(1,4),ldaFcpl961.getCommon_Xml_Annuitystartdate_Data(),1,4);                                     //Natural: MOVE SUBSTR ( #PYMNT-SETTLMNT-DTE-A,1,4 ) TO SUBSTR ( ANNUITYSTARTDATE-DATA,1,4 )
                setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Annuitystartdate_Data(),5,1);                                                                            //Natural: MOVE '-' TO SUBSTR ( ANNUITYSTARTDATE-DATA,5,1 )
                setValueToSubstring(pnd_Pymnt_Settlmnt_Dte_A.getSubstring(5,2),ldaFcpl961.getCommon_Xml_Annuitystartdate_Data(),6,2);                                     //Natural: MOVE SUBSTR ( #PYMNT-SETTLMNT-DTE-A,5,2 ) TO SUBSTR ( ANNUITYSTARTDATE-DATA,6,2 )
                setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Annuitystartdate_Data(),8,1);                                                                            //Natural: MOVE '-' TO SUBSTR ( ANNUITYSTARTDATE-DATA,8,1 )
                setValueToSubstring(pnd_Pymnt_Settlmnt_Dte_A.getSubstring(7,2),ldaFcpl961.getCommon_Xml_Annuitystartdate_Data(),9,2);                                     //Natural: MOVE SUBSTR ( #PYMNT-SETTLMNT-DTE-A,7,2 ) TO SUBSTR ( ANNUITYSTARTDATE-DATA,9,2 )
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Annuitystartdate_Open(), ldaFcpl961.getCommon_Xml_Annuitystartdate_Data(),  //Natural: COMPRESS ANNUITYSTARTDATE-OPEN ANNUITYSTARTDATE-DATA ANNUITYSTARTDATE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Annuitystartdate_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NONE
        //*    IGNORE
        //*  END-DECIDE
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Paymentdetail_Open());                                                                                             //Natural: MOVE PAYMENTDETAIL-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE-XML
    }
    private void sub_Write_Xml2() throws Exception                                                                                                                        //Natural: WRITE-XML2
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Paymentdetail_Close());                                                                                            //Natural: MOVE PAYMENTDETAIL-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Interest_Amt.greater(getZero())))                                                                                                            //Natural: IF #WS-INTEREST-AMT GT 0
        {
            ldaFcpl961.getCommon_Xml_Totalinterest_Data().setValueEdited(pnd_Ws_Interest_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                         //Natural: MOVE EDITED #WS-INTEREST-AMT ( EM = ZZZZZZZZ9.99 ) TO TOTALINTEREST-DATA
            ldaFcpl961.getCommon_Xml_Totalinterest_Data().setValue(ldaFcpl961.getCommon_Xml_Totalinterest_Data(), MoveOption.LeftJustified);                              //Natural: MOVE LEFT TOTALINTEREST-DATA TO TOTALINTEREST-DATA
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Totalinterest_Open(), ldaFcpl961.getCommon_Xml_Totalinterest_Data(),  //Natural: COMPRESS TOTALINTEREST-OPEN TOTALINTEREST-DATA TOTALINTEREST-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Totalinterest_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Totalnetpaymentamount_Data().setValueEdited(pnd_Total_Net_Amt2,new ReportEditMask("ZZZZZZZZZZZ9.99"));                                   //Natural: MOVE EDITED #TOTAL-NET-AMT2 ( EM = ZZZZZZZZZZZ9.99 ) TO TOTALNETPAYMENTAMOUNT-DATA
        ldaFcpl961.getCommon_Xml_Totalnetpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Totalnetpaymentamount_Data(), MoveOption.LeftJustified);                  //Natural: MOVE LEFT TOTALNETPAYMENTAMOUNT-DATA TO TOTALNETPAYMENTAMOUNT-DATA
        //*  IF TOTALNETPAYMENTAMOUNT-DATA =  '0.00' OR
        if (condition(ldaFcpl961.getCommon_Xml_Totalnetpaymentamount_Data().notEquals(" ")))                                                                              //Natural: IF TOTALNETPAYMENTAMOUNT-DATA NE ' '
        {
            //*    IGNORE
            //*  ELSE
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Totalnetpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Totalnetpaymentamount_Data(),  //Natural: COMPRESS TOTALNETPAYMENTAMOUNT-OPEN TOTALNETPAYMENTAMOUNT-DATA TOTALNETPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Totalnetpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-AMT (EM=ZZZZZZZZZZZ9.99)
        //*    TO NETAMOUNT-DATA
        //*  MOVE LEFT NETAMOUNT-DATA TO NETAMOUNT-DATA
        ldaFcpl961.getCommon_Xml_Totalpaymentamount_Data().setValueEdited(pnd_Total_Payment_Amt,new ReportEditMask("ZZZZZZZZZZZZ.99"));                                   //Natural: MOVE EDITED #TOTAL-PAYMENT-AMT ( EM = ZZZZZZZZZZZZ.99 ) TO TOTALPAYMENTAMOUNT-DATA
        ldaFcpl961.getCommon_Xml_Totalpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Totalpaymentamount_Data(), MoveOption.LeftJustified);                        //Natural: MOVE LEFT TOTALPAYMENTAMOUNT-DATA TO TOTALPAYMENTAMOUNT-DATA
        if (condition(ldaFcpl961.getCommon_Xml_Totalpaymentamount_Data().equals("0.00") || ldaFcpl961.getCommon_Xml_Totalpaymentamount_Data().equals(" ")))               //Natural: IF TOTALPAYMENTAMOUNT-DATA = '0.00' OR TOTALPAYMENTAMOUNT-DATA = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Totalpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Totalpaymentamount_Data(),  //Natural: COMPRESS TOTALPAYMENTAMOUNT-OPEN TOTALPAYMENTAMOUNT-DATA TOTALPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Totalpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Interest_Amt.greater(getZero())))                                                                                                            //Natural: IF #WS-INTEREST-AMT GT 0
        {
            ldaFcpl961.getCommon_Xml_Interestind_Data().setValue("Y");                                                                                                    //Natural: ASSIGN INTERESTIND-DATA := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Interestind_Data().setValue("N");                                                                                                    //Natural: ASSIGN INTERESTIND-DATA := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Interestind_Open(), ldaFcpl961.getCommon_Xml_Interestind_Data(),   //Natural: COMPRESS INTERESTIND-OPEN INTERESTIND-DATA INTERESTIND-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Interestind_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Total_Tax_Amt.compute(new ComputeParameters(false, pnd_Total_Tax_Amt), pnd_Federal_Tax_Amt.add(pnd_State_Tax_Amt).add(pnd_Local_Tax_Amt).add(pnd_Can_Tax_Amt).add(pnd_Ded_Amt)); //Natural: COMPUTE #TOTAL-TAX-AMT = #FEDERAL-TAX-AMT + #STATE-TAX-AMT + #LOCAL-TAX-AMT + #CAN-TAX-AMT + #DED-AMT
        if (condition(pnd_Total_Tax_Amt.greater(getZero())))                                                                                                              //Natural: IF #TOTAL-TAX-AMT GT 0
        {
            ldaFcpl961.getCommon_Xml_Deductionamountind_Data().setValue("Y");                                                                                             //Natural: ASSIGN DEDUCTIONAMOUNTIND-DATA := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Deductionamountind_Data().setValue("N");                                                                                             //Natural: ASSIGN DEDUCTIONAMOUNTIND-DATA := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Deductionamountind_Open(), ldaFcpl961.getCommon_Xml_Deductionamountind_Data(),  //Natural: COMPRESS DEDUCTIONAMOUNTIND-OPEN DEDUCTIONAMOUNTIND-DATA DEDUCTIONAMOUNTIND-CLOSE INTO #XML-LINE LEAVING NO SPACE
            ldaFcpl961.getCommon_Xml_Deductionamountind_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl961.getCommon_Xml_Grossamount_Data().setValueEdited(pnd_Total_Gross_Amt,new ReportEditMask("ZZZZZZZZZZZ9.99"));                                            //Natural: MOVE EDITED #TOTAL-GROSS-AMT ( EM = ZZZZZZZZZZZ9.99 ) TO GROSSAMOUNT-DATA
        ldaFcpl961.getCommon_Xml_Grossamount_Data().setValue(ldaFcpl961.getCommon_Xml_Grossamount_Data(), MoveOption.LeftJustified);                                      //Natural: MOVE LEFT GROSSAMOUNT-DATA TO GROSSAMOUNT-DATA
        if (condition(ldaFcpl961.getCommon_Xml_Grossamount_Data().notEquals(" ")))                                                                                        //Natural: IF GROSSAMOUNT-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Grossamount_Open(), ldaFcpl961.getCommon_Xml_Grossamount_Data(),  //Natural: COMPRESS GROSSAMOUNT-OPEN GROSSAMOUNT-DATA GROSSAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Grossamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Total_Tax_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                               //Natural: IF #TOTAL-TAX-AMT NE 0.00
        {
            ldaFcpl961.getCommon_Xml_Totaltaxwitheld_Data().setValueEdited(pnd_Total_Tax_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                         //Natural: MOVE EDITED #TOTAL-TAX-AMT ( EM = ZZZZZZZZ9.99 ) TO TOTALTAXWITHELD-DATA
            ldaFcpl961.getCommon_Xml_Totaltaxwitheld_Data().setValue(ldaFcpl961.getCommon_Xml_Totaltaxwitheld_Data(), MoveOption.LeftJustified);                          //Natural: MOVE LEFT TOTALTAXWITHELD-DATA TO TOTALTAXWITHELD-DATA
            pnd_Total_Ded.nadd(pnd_Total_Tax_Amt);                                                                                                                        //Natural: COMPUTE #TOTAL-DED = #TOTAL-DED + #TOTAL-TAX-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Federal_Tax_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                             //Natural: IF #FEDERAL-TAX-AMT NE 0.00
        {
            ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(1).setValueEdited(pnd_Federal_Tax_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                 //Natural: MOVE EDITED #FEDERAL-TAX-AMT ( EM = ZZZZZZZZ9.99 ) TO TAXAMOUNT-DATA ( 1 )
            ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(1).setValue(ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(1), MoveOption.LeftJustified);              //Natural: MOVE LEFT TAXAMOUNT-DATA ( 1 ) TO TAXAMOUNT-DATA ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_State_Tax_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                               //Natural: IF #STATE-TAX-AMT NE 0.00
        {
            ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(3).setValueEdited(pnd_State_Tax_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                   //Natural: MOVE EDITED #STATE-TAX-AMT ( EM = ZZZZZZZZ9.99 ) TO TAXAMOUNT-DATA ( 3 )
            ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(3).setValue(ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(3), MoveOption.LeftJustified);              //Natural: MOVE LEFT TAXAMOUNT-DATA ( 3 ) TO TAXAMOUNT-DATA ( 3 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Local_Tax_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                               //Natural: IF #LOCAL-TAX-AMT NE 0.00
        {
            ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(4).setValueEdited(pnd_Local_Tax_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                   //Natural: MOVE EDITED #LOCAL-TAX-AMT ( EM = ZZZZZZZZ9.99 ) TO TAXAMOUNT-DATA ( 4 )
            ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(4).setValue(ldaFcpl961.getCommon_Xml_Taxamount_Data().getValue(4), MoveOption.LeftJustified);              //Natural: MOVE LEFT TAXAMOUNT-DATA ( 4 ) TO TAXAMOUNT-DATA ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  *********************************** TAX WITHOLDING
        //*  DEDUCTION DESCRIPTIONS
                                                                                                                                                                          //Natural: PERFORM GET-STATE-LINE
        sub_Get_State_Line();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Total_Ded.greater(getZero())))                                                                                                                  //Natural: IF #TOTAL-DED GT 0
        {
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Taxeswithheld_Open());                                                                                         //Natural: MOVE TAXESWITHHELD-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO 6
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
            {
                if (condition(ldaFcpl961.getCommon_Xml_Taxdescription_Data().getValue(pnd_I).notEquals(" ")))                                                             //Natural: IF TAXDESCRIPTION-DATA ( #I ) NE ' '
                {
                    pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Taxeswithheldinfo_Open());                                                                             //Natural: MOVE TAXESWITHHELDINFO-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Taxdescription_Open(), ldaFcpl961.getCommon_Xml_Taxdescription_Data().getValue(pnd_I),  //Natural: COMPRESS TAXDESCRIPTION-OPEN TAXDESCRIPTION-DATA ( #I ) TAXDESCRIPTION-CLOSE INTO #XML-LINE LEAVING NO SPACE
                        ldaFcpl961.getCommon_Xml_Taxdescription_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      COMPRESS TAXAMOUNT-OPEN
                    //*        TAXAMOUNT-DATA (#I)
                    //*        TAXAMOUNT-CLOSE
                    //*        INTO #XML-LINE LEAVING NO SPACE
                    //*      PERFORM WRITE-WORK-3
                    pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Taxeswithheldinfo_Close());                                                                            //Natural: MOVE TAXESWITHHELDINFO-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Taxeswithheld_Close());                                                                                        //Natural: MOVE TAXESWITHHELD-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Totaltaxwitheld_Data().notEquals(" ")))                                                                                    //Natural: IF TOTALTAXWITHELD-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Totaltaxwitheld_Open(), ldaFcpl961.getCommon_Xml_Totaltaxwitheld_Data(),  //Natural: COMPRESS TOTALTAXWITHELD-OPEN TOTALTAXWITHELD-DATA TOTALTAXWITHELD-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Totaltaxwitheld_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  ***********************************  DEDUCTIONS
        if (condition(pnd_Ded_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                                     //Natural: IF #DED-AMT NE 0.00
        {
            ldaFcpl961.getCommon_Xml_Otherdeductionamount_Data().setValueEdited(pnd_Ded_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                          //Natural: MOVE EDITED #DED-AMT ( EM = ZZZZZZZZ9.99 ) TO OTHERDEDUCTIONAMOUNT-DATA
            ldaFcpl961.getCommon_Xml_Otherdeductionamount_Data().setValue(ldaFcpl961.getCommon_Xml_Otherdeductionamount_Data(), MoveOption.LeftJustified);                //Natural: MOVE LEFT OTHERDEDUCTIONAMOUNT-DATA TO OTHERDEDUCTIONAMOUNT-DATA
            //*  COMPUTE #TOTAL-DED = #TOTAL-DED + #DED-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Total_Tax_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                               //Natural: IF #TOTAL-TAX-AMT NE 0.00
        {
            ldaFcpl961.getCommon_Xml_Totaldeductionamount_Data().setValueEdited(pnd_Total_Tax_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                    //Natural: MOVE EDITED #TOTAL-TAX-AMT ( EM = ZZZZZZZZ9.99 ) TO TOTALDEDUCTIONAMOUNT-DATA
            ldaFcpl961.getCommon_Xml_Totaldeductionamount_Data().setValue(ldaFcpl961.getCommon_Xml_Totaldeductionamount_Data(), MoveOption.LeftJustified);                //Natural: MOVE LEFT TOTALDEDUCTIONAMOUNT-DATA TO TOTALDEDUCTIONAMOUNT-DATA
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Totaldeductionamount_Data().notEquals(" ")))                                                                               //Natural: IF TOTALDEDUCTIONAMOUNT-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Totaldeductionamount_Open(), ldaFcpl961.getCommon_Xml_Totaldeductionamount_Data(),  //Natural: COMPRESS TOTALDEDUCTIONAMOUNT-OPEN TOTALDEDUCTIONAMOUNT-DATA TOTALDEDUCTIONAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Totaldeductionamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Otherdeductionamount_Data().notEquals(" ")))                                                                               //Natural: IF OTHERDEDUCTIONAMOUNT-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Otherdeductiondesc_Open(), ldaFcpl961.getCommon_Xml_Otherdeductiondesc_Data(),  //Natural: COMPRESS OTHERDEDUCTIONDESC-OPEN OTHERDEDUCTIONDESC-DATA OTHERDEDUCTIONDESC-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Otherdeductiondesc_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Otherdeductionamount_Data().notEquals(" ")))                                                                               //Natural: IF OTHERDEDUCTIONAMOUNT-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Otherdeductionamount_Open(), ldaFcpl961.getCommon_Xml_Otherdeductionamount_Data(),  //Natural: COMPRESS OTHERDEDUCTIONAMOUNT-OPEN OTHERDEDUCTIONAMOUNT-DATA OTHERDEDUCTIONAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Otherdeductionamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Netamount_Data().notEquals(" ")))                                                                                          //Natural: IF NETAMOUNT-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Netamount_Open(), ldaFcpl961.getCommon_Xml_Netamount_Data(),   //Natural: COMPRESS NETAMOUNT-OPEN NETAMOUNT-DATA NETAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Netamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM AFTER-TAX-ROUTINE
        sub_After_Tax_Routine();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Cntrct_Ivc_Amt.greater(getZero())))                                                                                                             //Natural: IF #CNTRCT-IVC-AMT GT 0
        {
            ldaFcpl961.getCommon_Xml_Aftertaxamount_Data().setValueEdited(pnd_Cntrct_Ivc_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                         //Natural: MOVE EDITED #CNTRCT-IVC-AMT ( EM = ZZZZZZZZ9.99 ) TO AFTERTAXAMOUNT-DATA
            ldaFcpl961.getCommon_Xml_Aftertaxamount_Data().setValue(ldaFcpl961.getCommon_Xml_Aftertaxamount_Data(), MoveOption.LeftJustified);                            //Natural: MOVE LEFT AFTERTAXAMOUNT-DATA TO AFTERTAXAMOUNT-DATA
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Aftertaxamount_Data().notEquals(" ") && ldaFcpl961.getCommon_Xml_Aftertaxdesc_Data().notEquals(" ")))                      //Natural: IF AFTERTAXAMOUNT-DATA NE ' ' AND AFTERTAXDESC-DATA NE ' '
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Aftertaxdesc_Open(), ldaFcpl961.getCommon_Xml_Aftertaxdesc_Data(),  //Natural: COMPRESS AFTERTAXDESC-OPEN AFTERTAXDESC-DATA AFTERTAXDESC-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Aftertaxdesc_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Aftertaxamount_Open(), ldaFcpl961.getCommon_Xml_Aftertaxamount_Data(),  //Natural: COMPRESS AFTERTAXAMOUNT-OPEN AFTERTAXAMOUNT-DATA AFTERTAXAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Aftertaxamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMPRESS PROLINE-NOTE1 PROLINE-NOTE2 SUBSTR(PROLINE-NOTE3,1,20)
        //*   INTO MEMOFIELD-DATA
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("10") || pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("50")))            //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '10' OR = '50'
        {
            //*  PASELECT SPIA
            if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("S") || pnd_Ws_Cntrct_Annty_Ins_Type.equals("M")))                                                          //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
            {
                ldaFcpl961.getCommon_Xml_Memofield_Data().setValue(DbsUtil.compress("TIAA Life reports all taxable payments to the", "government for the year in which they are made. Contracts",  //Natural: COMPRESS 'TIAA Life reports all taxable payments to the' 'government for the year in which they are made. Contracts' 'and other material have been sent under separate cover.' 'If you have any questions, please call' INTO MEMOFIELD-DATA
                    "and other material have been sent under separate cover.", "If you have any questions, please call"));
                ldaFcpl961.getCommon_Xml_Memofieldext_Data().setValue(DbsUtil.compress("our Planning and Service Center", "at 800 223-1200 M-F 8am-8pm ET.",              //Natural: COMPRESS 'our Planning and Service Center' 'at 800 223-1200 M-F 8am-8pm ET.' ' This contract has been issued by' 'TIAA Life Insurance Company.' INTO MEMOFIELDEXT-DATA
                    " This contract has been issued by", "TIAA Life Insurance Company."));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Memofield_Data().setValue(DbsUtil.compress("TIAA reports all taxable payments to the IRS for the year", "in which they are made.{cr}",  //Natural: COMPRESS 'TIAA reports all taxable payments to the IRS for the year' 'in which they are made.{cr}' 'If you have any questions, please call us at' '{b}800-842-2252{/b} weekdays from 8 a.m. to 10 p.m. and' INTO MEMOFIELD-DATA
                    "If you have any questions, please call us at", "{b}800-842-2252{/b} weekdays from 8 a.m. to 10 p.m. and"));
                ldaFcpl961.getCommon_Xml_Memofieldext_Data().setValue(DbsUtil.compress("!Saturday from 9 a.m. to 6 p.m.(ET). You can also visit", "{b}TIAA.org{/b} for more information.")); //Natural: COMPRESS '!Saturday from 9 a.m. to 6 p.m.(ET). You can also visit' '{b}TIAA.org{/b} for more information.' INTO MEMOFIELDEXT-DATA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  PASELECT SPIA
            if (condition(pnd_Ws_Cntrct_Annty_Ins_Type.equals("S") || pnd_Ws_Cntrct_Annty_Ins_Type.equals("M")))                                                          //Natural: IF #WS-CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
            {
                ldaFcpl961.getCommon_Xml_Memofield_Data().setValue(DbsUtil.compress("TIAA Life report all taxable payments to the", "government for the year in which they are made. If you have",  //Natural: COMPRESS 'TIAA Life report all taxable payments to the' 'government for the year in which they are made. If you have' 'any questions, please call our Planning and Service Center' 'at 800 223-1200 M-F 8am-8pm ET.' INTO MEMOFIELD-DATA
                    "any questions, please call our Planning and Service Center", "at 800 223-1200 M-F 8am-8pm ET."));
                ldaFcpl961.getCommon_Xml_Memofieldext_Data().setValue(DbsUtil.compress("!This contract has been issued by", "TIAA Life Insurance Company."));             //Natural: COMPRESS '!This contract has been issued by' 'TIAA Life Insurance Company.' INTO MEMOFIELDEXT-DATA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Memofield_Data().setValue(DbsUtil.compress("TIAA reports all taxable payments to the IRS for the year", "in which they are made.{cr}",  //Natural: COMPRESS 'TIAA reports all taxable payments to the IRS for the year' 'in which they are made.{cr}' 'If you have any questions, please call us at' '{b}800-842-2252{/b} weekdays from 8 a.m. to 10 p.m. and' INTO MEMOFIELD-DATA
                    "If you have any questions, please call us at", "{b}800-842-2252{/b} weekdays from 8 a.m. to 10 p.m. and"));
                ldaFcpl961.getCommon_Xml_Memofieldext_Data().setValue(DbsUtil.compress("!Saturday from 9 a.m. to 6 p.m.(ET). You can also visit", "{b}TIAA.org{/b} for more information.")); //Natural: COMPRESS '!Saturday from 9 a.m. to 6 p.m.(ET). You can also visit' '{b}TIAA.org{/b} for more information.' INTO MEMOFIELDEXT-DATA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Memofield_Data().notEquals(" ")))                                                                                          //Natural: IF MEMOFIELD-DATA NE ' '
        {
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Memofield_Data()), new ExamineSearch("&"), new ExamineReplace(" "));                               //Natural: EXAMINE MEMOFIELD-DATA FOR '&' REPLACE WITH ' '
            //*  IF #CNTRCT-IVC-AMT NE 0
            //*    COMPRESS MEMOFIELD-OPEN '{cr}' MEMOFIELD-DATA MEMOFIELD-CLOSE
            //*      INTO #XML-LINE LEAVING NO SPACE
            //*  ELSE
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Memofield_Open(), ldaFcpl961.getCommon_Xml_Memofield_Data(),   //Natural: COMPRESS MEMOFIELD-OPEN MEMOFIELD-DATA MEMOFIELD-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Memofield_Close()));
            //*  END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl961.getCommon_Xml_Memofieldext_Data().notEquals(" ")))                                                                                       //Natural: IF MEMOFIELDEXT-DATA NE ' '
        {
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Memofield_Data()), new ExamineSearch("&"), new ExamineReplace(" "));                               //Natural: EXAMINE MEMOFIELD-DATA FOR '&' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Memofieldext_Data()), new ExamineSearch("!"), new ExamineReplace(" "));                            //Natural: EXAMINE MEMOFIELDEXT-DATA FOR '!' REPLACE WITH ' '
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Memofieldext_Open(), ldaFcpl961.getCommon_Xml_Memofieldext_Data(),  //Natural: COMPRESS MEMOFIELDEXT-OPEN MEMOFIELDEXT-DATA MEMOFIELDEXT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Memofieldext_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR EFTS AND ROLLOVERS, SUPPLY INSTITUTION INFORMATION
        if (condition((pnd_Check_Eft.getBoolean() || pnd_Check_Rollover.getBoolean() || pnd_Check_Efthold.getBoolean()) && ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).notEquals(" "))) //Natural: IF ( #CHECK-EFT OR #CHECK-ROLLOVER OR #CHECK-EFTHOLD ) AND INSTITUTIONINFOLINE-DATA ( 8 ) NOT = ' '
        {
            ldaFcpl961.getCommon_Xml_Institutioninfo_Open().setValue("<InstitutionInfo>");                                                                                //Natural: MOVE '<InstitutionInfo>' TO INSTITUTIONINFO-OPEN
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Institutioninfo_Open());                                                                                       //Natural: MOVE INSTITUTIONINFO-OPEN TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            //*  DO NOT INCLUDE EFT WITH HOLD CODE START
            if (condition(! (pnd_Check_Efthold.getBoolean())))                                                                                                            //Natural: IF NOT #CHECK-EFTHOLD
            {
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(1).setValue("Per your request, we have sent the payment to the address below:");             //Natural: MOVE 'Per your request, we have sent the payment to the address below:' TO INSTITUTIONINFOLINE-DATA ( 1 )
                FOR05:                                                                                                                                                    //Natural: FOR #INSNDX = 1 TO 7
                for (pnd_Insndx.setValue(1); condition(pnd_Insndx.lessOrEqual(7)); pnd_Insndx.nadd(1))
                {
                    if (condition(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(pnd_Insndx).equals(" ")))                                                  //Natural: IF INSTITUTIONINFOLINE-DATA ( #INSNDX ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(pnd_Insndx)), new ExamineSearch("&"),                  //Natural: EXAMINE INSTITUTIONINFOLINE-DATA ( #INSNDX ) FOR '&' REPLACE WITH ' '
                        new ExamineReplace(" "));
                    ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(pnd_Insndx).setValue(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(pnd_Insndx),  //Natural: MOVE LEFT INSTITUTIONINFOLINE-DATA ( #INSNDX ) TO INSTITUTIONINFOLINE-DATA ( #INSNDX )
                        MoveOption.LeftJustified);
                    pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Institutioninfoline_Open(), ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(pnd_Insndx),  //Natural: COMPRESS INSTITUTIONINFOLINE-OPEN INSTITUTIONINFOLINE-DATA ( #INSNDX ) INSTITUTIONINFOLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                        ldaFcpl961.getCommon_Xml_Institutioninfoline_Close()));
                    pnd_Ins_Line.setValue(true);                                                                                                                          //Natural: ASSIGN #INS-LINE := TRUE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ins_Line.setValue(false);                                                                                                                         //Natural: ASSIGN #INS-LINE := FALSE
                    //*      IF (#CHECK-EFT OR #CHECK-EFTHOLD) AND #INSNDX EQ 1
                    //*        ESCAPE BOTTOM
                    //*      END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  ELSE /* EFT WITH HOLD CONVERTED INTO CHECK
                //*    FOR #INSNDX = 2 TO 7
                //*      IF INSTITUTIONINFOLINE-DATA(#INSNDX) = ' '
                //*        ESCAPE BOTTOM
                //*      END-IF
                //*      EXAMINE INSTITUTIONINFOLINE-DATA(#INSNDX) FOR '&'
                //*        REPLACE WITH ' '
                //*      MOVE LEFT INSTITUTIONINFOLINE-DATA(#INSNDX)
                //*        TO INSTITUTIONINFOLINE-DATA(#INSNDX)
                //*      COMPRESS INSTITUTIONINFOLINE-OPEN
                //*        INSTITUTIONINFOLINE-DATA(#INSNDX)
                //*        INSTITUTIONINFOLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                //*      #INS-LINE := TRUE
                //*      PERFORM WRITE-WORK-3
                //*      #INS-LINE := FALSE
                //*      IF (#CHECK-EFT OR #CHECK-EFTHOLD) AND #INSNDX EQ 1
                //*        ESCAPE BOTTOM
                //*      END-IF
                //*    END-FOR
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8)), new ExamineSearch("&"), new ExamineReplace(" "));         //Natural: EXAMINE INSTITUTIONINFOLINE-DATA ( 8 ) FOR '&' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8)), new ExamineSearch("*"), new ExamineReplace(" "));         //Natural: EXAMINE INSTITUTIONINFOLINE-DATA ( 8 ) FOR '*' REPLACE WITH ' '
            if (condition(pnd_Save_Eft_Acct.notEquals(" ")))                                                                                                              //Natural: IF #SAVE-EFT-ACCT NE ' '
            {
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).setValue(DbsUtil.compress(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8),  //Natural: COMPRESS INSTITUTIONINFOLINE-DATA ( 8 ) 'A/C' #SAVE-EFT-ACCT TO INSTITUTIONINFOLINE-DATA ( 8 )
                    "A/C", pnd_Save_Eft_Acct));
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).setValue(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8),                 //Natural: MOVE LEFT INSTITUTIONINFOLINE-DATA ( 8 ) TO INSTITUTIONINFOLINE-DATA ( 8 )
                    MoveOption.LeftJustified);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8).setValue(ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8),                 //Natural: MOVE LEFT INSTITUTIONINFOLINE-DATA ( 8 ) TO INSTITUTIONINFOLINE-DATA ( 8 )
                    MoveOption.LeftJustified);
            }                                                                                                                                                             //Natural: END-IF
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Institutioninfoline_Open(), ldaFcpl961.getCommon_Xml_Institutioninfoline_Data().getValue(8),  //Natural: COMPRESS INSTITUTIONINFOLINE-OPEN INSTITUTIONINFOLINE-DATA ( 8 ) INSTITUTIONINFOLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Institutioninfoline_Close()));
            DbsUtil.examine(new ExamineSource(pnd_Xml_Line), new ExamineSearch("A/C"), new ExamineReplace("$$$A/C$$$"));                                                  //Natural: EXAMINE #XML-LINE 'A/C' REPLACE '$$$A/C$$$'
            DbsUtil.examine(new ExamineSource(pnd_Xml_Line), new ExamineSearch("$"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #XML-LINE '$' REPLACE ' '
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            ldaFcpl961.getCommon_Xml_Institutioninfo_Close().setValue("</InstitutionInfo>");                                                                              //Natural: MOVE '</InstitutionInfo>' TO INSTITUTIONINFO-CLOSE
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Institutioninfo_Close());                                                                                      //Natural: MOVE INSTITUTIONINFO-CLOSE TO #XML-LINE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            //*    RESET PARTICIPANTINSTITUTIONFLAG-DATA
            //*    IF #CHECK-EFT
            //*       MOVE 'P' TO PARTICIPANTINSTITUTIONFLAG-DATA
            //*    ELSE
            //*       MOVE 'I' TO PARTICIPANTINSTITUTIONFLAG-DATA
            //*    END-IF
            //*    COMPRESS PARTICIPANTINSTITUTIONFLAG-OPEN
            //*      PARTICIPANTINSTITUTIONFLAG-DATA
            //*      PARTICIPANTINSTITUTIONFLAG-CLOS
            //*      INTO #XML-LINE LEAVING NO SPACE
            //*      PERFORM WRITE-WORK-3
        }                                                                                                                                                                 //Natural: END-IF
        //*  #FIRST-DED  := TRUE
        //*  FOR #NDX-DED = 1 TO 10
        //*   IF DEDUCTIONDESC-TAB (#NDX-DED) = ' '
        //*     ESCAPE BOTTOM
        //*   ELSE
        //*     IF #FIRST-DED
        //*       MOVE DEDUCTIONS-OPEN TO #XML-LINE
        //*       PERFORM WRITE-WORK-3
        //*       #FIRST-DED  := FALSE
        //*     END-IF
        //*     #XML-LINE :=       DEDUCTIONSINFO-OPEN
        //*     PERFORM WRITE-WORK-3
        //*     MOVE DEDUCTIONDESC-TAB(#NDX-DED)   TO DEDUCTIONDESCRIPTION-DATA
        //*     MOVE DEDUCTIONAMOUNT-TAB(#NDX-DED)
        //*       TO DEDUCTIONAMOUNT-DATA
        //*     MOVE LEFT DEDUCTIONAMOUNT-DATA TO DEDUCTIONAMOUNT-DATA
        //*     COMPRESS DEDUCTIONSDESCRIPTION-OPEN DEDUCTIONDESCRIPTION-DATA
        //*       DEDUCTIONSDESCRIPTION-CLOSE INTO #XML-LINE LEAVING NO
        //*     PERFORM WRITE-WORK-3
        //*     COMPRESS DEDUCTIONAMOUNT-OPEN DEDUCTIONAMOUNT-DATA
        //*       DEDUCTIONAMOUNT-CLOSE INTO #XML-LINE LEAVING NO
        //*     PERFORM WRITE-WORK-3
        //*     #XML-LINE := DEDUCTIONSINFO-CLOSE
        //*     PERFORM WRITE-WORK-3
        //*   END-IF
        //*  END-FOR
        //*  IF NOT #FIRST-DED
        //*   #XML-LINE := DEDUCTIONS-CLOSE
        //*   PERFORM WRITE-WORK-3
        //*  END-IF
        if (condition(pnd_Check_Rollover.getBoolean() && pnd_Save_Eft_Acct.notEquals(" ")))                                                                               //Natural: IF #CHECK-ROLLOVER AND #SAVE-EFT-ACCT NE ' '
        {
            pnd_Xndx.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #XNDX
            pnd_Xndx_Rollover.getValue(pnd_Xndx).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "<AccountNumber>", pnd_Save_Eft_Acct, "</AccountNumber>"));     //Natural: COMPRESS '<AccountNumber>' #SAVE-EFT-ACCT '</AccountNumber>' INTO #XNDX-ROLLOVER ( #XNDX ) LEAVING NO SPACE
            //*      PERFORM WRITE-WORK-3
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Nonidsinfo_Close());                                                                                               //Natural: ASSIGN #XML-LINE := NONIDSINFO-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Prolinechecks_Close());                                                                                            //Natural: ASSIGN #XML-LINE := PROLINECHECKS-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Documentinfo_Close());                                                                                             //Natural: ASSIGN #XML-LINE := DOCUMENTINFO-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Documentrequest_Close());                                                                                          //Natural: ASSIGN #XML-LINE := DOCUMENTREQUEST-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Check_Rollover.getBoolean()))                                                                                                                   //Natural: IF #CHECK-ROLLOVER
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-ROLLOVER
            sub_Write_Rollover();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Last_Record.getBoolean()))                                                                                                                      //Natural: IF #LAST-RECORD
        {
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Documentrequests_Close());                                                                                     //Natural: ASSIGN #XML-LINE := DOCUMENTREQUESTS-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-XML2
    }
    private void sub_C1000_Process_Fund_Record() throws Exception                                                                                                         //Natural: C1000-PROCESS-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  READ WORK 2 ONCE #WS-HEADER-RECORD.COMMON-FIELDS #WS-HEADER-RECORD.RECORD-DETAIL(*)
        pnd_Total_Gross_Amt.reset();                                                                                                                                      //Natural: RESET #TOTAL-GROSS-AMT #CNTRCT-IVC-AMT #WS-INTEREST-AMT
        pnd_Cntrct_Ivc_Amt.reset();
        pnd_Ws_Interest_Amt.reset();
        pnd_Federal_Tax_Amt.reset();                                                                                                                                      //Natural: RESET #FEDERAL-TAX-AMT #STATE-TAX-AMT #LOCAL-TAX-AMT #CAN-TAX-AMT #TOTAL-TAX-AMT
        pnd_State_Tax_Amt.reset();
        pnd_Local_Tax_Amt.reset();
        pnd_Can_Tax_Amt.reset();
        pnd_Total_Tax_Amt.reset();
        FOR06:                                                                                                                                                            //Natural: FOR #I-FUND 1 TO 40
        for (pnd_I_Fund.setValue(1); condition(pnd_I_Fund.lessOrEqual(40)); pnd_I_Fund.nadd(1))
        {
            pnd_Interest_Amt.reset();                                                                                                                                     //Natural: RESET #INTEREST-AMT
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_I_Fund).equals(" ")))                                                                   //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( #I-FUND ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #PROLINE-FUTURE-DATED = 'Y' /* HERE CHECK  START
            //*   HOLDCODE-DATA := 'FUTR'    /* HOLD UNTIL THE 1ST OF THE MONTH
            //*  END-IF
            //*  MOVE PROLINE-IDS-DISB-DESC TO DISBURSEMENTDESCRIPTION-DATA
            //*  MOVE PROLINE-IDS-NAME-LBL TO NAMELABEL-DATA
            //*  MOVE PROLINE-IDS-INS-NAME TO INSUREDNAME-DATA
            //*  MOVE PROLINE-IDS-DOC-NUM-LBL TO DOCUMENTLABEL1-DATA
            //*  MOVE PROLINE-IDS-DOC-NUM-LBL TO DOCUMENTLABEL2-DATA /* CHECK END
            //*  ********* FUND CODE TRANSLATION
            ldaFcpl961.getCommon_Xml_Tiaacrefind_Data().reset();                                                                                                          //Natural: RESET TIAACREFIND-DATA #WS-ACCT-CODE #WS-VALUAT-PERIOD
            pnd_Ws_Acct_Code.reset();
            pnd_Ws_Valuat_Period.reset();
            pnd_Ws_Acct_Code.setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_I_Fund));                                                                   //Natural: MOVE #WS-OCCURS.INV-ACCT-CDE ( #I-FUND ) TO #WS-ACCT-CODE
            pnd_Ws_Valuat_Period.setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Valuat_Period().getValue(pnd_I_Fund));                                                     //Natural: MOVE #WS-OCCURS.INV-ACCT-VALUAT-PERIOD ( #I-FUND ) TO #WS-VALUAT-PERIOD
            //*  MOVE #WS-OCCURS.INV-ACCT-CDE TO #FUND-PDA.#INV-ACCT-INPUT
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pnd_Ws_Acct_Code);                                                                                  //Natural: MOVE #WS-ACCT-CODE TO #FUND-PDA.#INV-ACCT-INPUT
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Valuat_Period().getValue(pnd_I_Fund));                 //Natural: MOVE #WS-OCCURS.INV-ACCT-VALUAT-PERIOD ( #I-FUND ) TO #FUND-PDA.#INV-ACCT-VALUAT-PERIOD
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Type_Cde().setValue(pnd_Ws_Cntrct_Annty_Type_Cde);                                                               //Natural: MOVE #WS-CNTRCT-ANNTY-TYPE-CDE TO #FUND-PDA.#CNTRCT-ANNTY-TYPE-CDE
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Ins_Type().setValue(pnd_Ws_Cntrct_Annty_Ins_Type);                                                               //Natural: MOVE #WS-CNTRCT-ANNTY-INS-TYPE TO #FUND-PDA.#CNTRCT-ANNTY-INS-TYPE
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            //*  TIAA ACCOUNTS
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                                                                                                                                                                          //Natural: PERFORM TIAA-ACCOUNT-MOVE-SETTLE
                sub_Tiaa_Account_Move_Settle();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CREF ACCOUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CREF-ACCOUNT-MOVE-UNIT
                sub_Cref_Account_Move_Unit();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Payment_Amt.compute(new ComputeParameters(false, pnd_Payment_Amt), pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_I_Fund).add(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_I_Fund))); //Natural: COMPUTE #PAYMENT-AMT = #WS-OCCURS.INV-ACCT-SETTL-AMT ( #I-FUND ) + #WS-OCCURS.INV-ACCT-DVDND-AMT ( #I-FUND )
            if (condition(pnd_Payment_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                             //Natural: IF #PAYMENT-AMT NE 0.00
            {
                pnd_Total_Payment_Amt.nadd(pnd_Payment_Amt);                                                                                                              //Natural: ASSIGN #TOTAL-PAYMENT-AMT := #TOTAL-PAYMENT-AMT + #PAYMENT-AMT
                ldaFcpl961.getCommon_Xml_Paymentamount_Data().setValueEdited(pnd_Payment_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                         //Natural: MOVE EDITED #PAYMENT-AMT ( EM = ZZZZZZZZ9.99 ) TO PAYMENTAMOUNT-DATA
                ldaFcpl961.getCommon_Xml_Paymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Paymentamount_Data(), MoveOption.LeftJustified);                          //Natural: MOVE LEFT PAYMENTAMOUNT-DATA TO PAYMENTAMOUNT-DATA
                //*  END-IF /* NETZERO
                //*  IF #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT(#I-FUND)  NE 0.00 /* NETZERO ACTI
                pnd_Total_Net_Amt2.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I_Fund));                                                       //Natural: ADD #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #I-FUND ) TO #TOTAL-NET-AMT2
                ldaFcpl961.getCommon_Xml_Netpaymentamount_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I_Fund),new             //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #I-FUND ) ( EM = ZZZZZZZZ9.99 ) TO NETPAYMENTAMOUNT-DATA
                    ReportEditMask("ZZZZZZZZ9.99"));
                ldaFcpl961.getCommon_Xml_Netpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Netpaymentamount_Data(), MoveOption.LeftJustified);                    //Natural: MOVE LEFT NETPAYMENTAMOUNT-DATA TO NETPAYMENTAMOUNT-DATA
                //*  NETZERO ACTIVATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Interest_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_I_Fund));                                                                   //Natural: ADD #WS-OCCURS.INV-ACCT-DPI-AMT ( #I-FUND ) TO #INTEREST-AMT
            pnd_Interest_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Dci_Amt().getValue(pnd_I_Fund));                                                                   //Natural: ADD #WS-OCCURS.INV-ACCT-DCI-AMT ( #I-FUND ) TO #INTEREST-AMT
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Paymentinfo_Open());                                                                                           //Natural: ASSIGN #XML-LINE := PAYMENTINFO-OPEN
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Next_Pymnt_Due_Dte_A.setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte().getValue(pnd_I_Fund),new ReportEditMask("YYYYMMDD"));      //Natural: MOVE EDITED #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE ( #I-FUND ) ( EM = YYYYMMDD ) TO #NEXT-PYMNT-DUE-DTE-A
            if (condition(DbsUtil.maskMatches(pnd_Next_Pymnt_Due_Dte_A,"YYYYMMDD")))                                                                                      //Natural: IF #NEXT-PYMNT-DUE-DTE-A EQ MASK ( YYYYMMDD )
            {
                setValueToSubstring(pnd_Next_Pymnt_Due_Dte_A.getSubstring(1,4),ldaFcpl961.getCommon_Xml_Duedate_Data(),1,4);                                              //Natural: MOVE SUBSTR ( #NEXT-PYMNT-DUE-DTE-A,1,4 ) TO SUBSTR ( DUEDATE-DATA,1,4 )
                setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Duedate_Data(),5,1);                                                                                     //Natural: MOVE '-' TO SUBSTR ( DUEDATE-DATA,5,1 )
                setValueToSubstring(pnd_Next_Pymnt_Due_Dte_A.getSubstring(5,2),ldaFcpl961.getCommon_Xml_Duedate_Data(),6,2);                                              //Natural: MOVE SUBSTR ( #NEXT-PYMNT-DUE-DTE-A,5,2 ) TO SUBSTR ( DUEDATE-DATA,6,2 )
                setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Duedate_Data(),8,1);                                                                                     //Natural: MOVE '-' TO SUBSTR ( DUEDATE-DATA,8,1 )
                setValueToSubstring(pnd_Next_Pymnt_Due_Dte_A.getSubstring(7,2),ldaFcpl961.getCommon_Xml_Duedate_Data(),9,2);                                              //Natural: MOVE SUBSTR ( #NEXT-PYMNT-DUE-DTE-A,7,2 ) TO SUBSTR ( DUEDATE-DATA,9,2 )
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Duedate_Open(), ldaFcpl961.getCommon_Xml_Duedate_Data(),   //Natural: COMPRESS DUEDATE-OPEN DUEDATE-DATA DUEDATE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Duedate_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tax_Amt.reset();                                                                                                                                          //Natural: RESET #TAX-AMT
            pnd_Tax_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I_Fund));                                                                   //Natural: ADD #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #I-FUND ) TO #TAX-AMT
            pnd_Tax_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I_Fund));                                                                  //Natural: ADD #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #I-FUND ) TO #TAX-AMT
            pnd_Tax_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I_Fund));                                                                  //Natural: ADD #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #I-FUND ) TO #TAX-AMT
            pnd_Tax_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_I_Fund,1,":",10));                                                                  //Natural: ADD #WS-OCCURS.PYMNT-DED-AMT ( #I-FUND,1:10 ) TO #TAX-AMT
            pnd_Tax_Amt.nadd(pnd_Inv_Acct_Can_Tax_Amt.getValue(pnd_I_Fund));                                                                                              //Natural: ADD #INV-ACCT-CAN-TAX-AMT ( #I-FUND ) TO #TAX-AMT
            if (condition(pnd_Tax_Amt.notEquals(new DbsDecimal("0.00"))))                                                                                                 //Natural: IF #TAX-AMT NE 0.00
            {
                ldaFcpl961.getCommon_Xml_Deductionamount_Data().setValueEdited(pnd_Tax_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                           //Natural: MOVE EDITED #TAX-AMT ( EM = ZZZZZZZZ9.99 ) TO DEDUCTIONAMOUNT-DATA
                ldaFcpl961.getCommon_Xml_Deductionamount_Data().setValue(ldaFcpl961.getCommon_Xml_Deductionamount_Data(), MoveOption.LeftJustified);                      //Natural: MOVE LEFT DEDUCTIONAMOUNT-DATA TO DEDUCTIONAMOUNT-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Deductionamount_Open(), ldaFcpl961.getCommon_Xml_Deductionamount_Data(),  //Natural: COMPRESS DEDUCTIONAMOUNT-OPEN DEDUCTIONAMOUNT-DATA DEDUCTIONAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Deductionamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Federal_Tax_Amt2.setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I_Fund));                                                      //Natural: ASSIGN #FEDERAL-TAX-AMT2 := #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #I-FUND )
            pnd_Can_Tax_Amt2.setValue(pnd_Inv_Acct_Can_Tax_Amt.getValue(pnd_I_Fund));                                                                                     //Natural: ASSIGN #CAN-TAX-AMT2 := #INV-ACCT-CAN-TAX-AMT ( #I-FUND )
            pnd_State_Tax_Amt2.setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I_Fund));                                                       //Natural: ASSIGN #STATE-TAX-AMT2 := #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #I-FUND )
            pnd_Local_Tax_Amt2.setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I_Fund));                                                       //Natural: ASSIGN #LOCAL-TAX-AMT2 := #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #I-FUND )
            //*   ****** ADD AMOUNTS FOR TOTAL TAXES
            pnd_Federal_Tax_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I_Fund));                                                           //Natural: ADD #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #I-FUND ) TO #FEDERAL-TAX-AMT
            pnd_Can_Tax_Amt.nadd(pnd_Inv_Acct_Can_Tax_Amt.getValue(pnd_I_Fund));                                                                                          //Natural: ADD #INV-ACCT-CAN-TAX-AMT ( #I-FUND ) TO #CAN-TAX-AMT
            pnd_State_Tax_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I_Fund));                                                            //Natural: ADD #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #I-FUND ) TO #STATE-TAX-AMT
            pnd_Local_Tax_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I_Fund));                                                            //Natural: ADD #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #I-FUND ) TO #LOCAL-TAX-AMT
            if (condition(ldaFcpl961.getCommon_Xml_Paymentamount_Data().notEquals(" ")))                                                                                  //Natural: IF PAYMENTAMOUNT-DATA NE ' '
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Paymentamount_Open(), ldaFcpl961.getCommon_Xml_Paymentamount_Data(),  //Natural: COMPRESS PAYMENTAMOUNT-OPEN PAYMENTAMOUNT-DATA PAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Paymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FE201305
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl961.getCommon_Xml_Paymentfrequency_Data().reset();                                                                                                     //Natural: RESET PAYMENTFREQUENCY-DATA
            if (condition(! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean())))                                                                                  //Natural: IF NOT #FUND-PDA.#DIVIDENDS
            {
                ldaFcpl961.getCommon_Xml_Paymentfrequency_Data().setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_9());                                               //Natural: ASSIGN PAYMENTFREQUENCY-DATA := #FUND-PDA.#VALUAT-DESC-9
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201305
            if (condition(ldaFcpl961.getCommon_Xml_Paymentfrequency_Data().notEquals(" ")))                                                                               //Natural: IF PAYMENTFREQUENCY-DATA NE ' '
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Paymentfrequency_Open(), ldaFcpl961.getCommon_Xml_Paymentfrequency_Data(),  //Natural: COMPRESS PAYMENTFREQUENCY-OPEN PAYMENTFREQUENCY-DATA PAYMENTFREQUENCY-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Paymentfrequency_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl961.getCommon_Xml_Tiaacontract_Data().notEquals(" ")))                                                                                   //Natural: IF TIAACONTRACT-DATA NE ' '
            {
                ldaFcpl961.getCommon_Xml_Tiaacontractpayment_Data().setValue(ldaFcpl961.getCommon_Xml_Tiaacontract_Data(), MoveOption.LeftJustified);                     //Natural: MOVE LEFT TIAACONTRACT-DATA TO TIAACONTRACTPAYMENT-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Tiaacontractpayment_Open(), ldaFcpl961.getCommon_Xml_Tiaacontractpayment_Data(),  //Natural: COMPRESS TIAACONTRACTPAYMENT-OPEN TIAACONTRACTPAYMENT-DATA TIAACONTRACTPAYMENT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Tiaacontractpayment_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                               //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pyhdr_Pymnt_Settlmnt_Dte_End.reset();                                                                                                                 //Natural: RESET #PYHDR-PYMNT-SETTLMNT-DTE-END
                pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Ctznshp_Cder.setValue(pdaFcpa700.getPnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde().getValue(pnd_I_Fund));     //Natural: ASSIGN #PYHDR-ANNT-CTZNSHP-CDER := #WS-OCCURS.#PYHDR-ANNT-CTZNSHP-CDE ( #I-FUND )
                pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_Pnd_Pyhdr_Annt_Rsdncy_Cder.setValue(pdaFcpa700.getPnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde().getValue(pnd_I_Fund));       //Natural: ASSIGN #PYHDR-ANNT-RSDNCY-CDER := #WS-OCCURS.#PYHDR-ANNT-RSDNCY-CDE ( #I-FUND )
                if (condition(pnd_Pyhdr_Pymnt_Settlmnt_Dte_End.greater(getZero())))                                                                                       //Natural: IF #PYHDR-PYMNT-SETTLMNT-DTE-END GT 0
                {
                    pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A.setValueEdited(pnd_Pyhdr_Pymnt_Settlmnt_Dte_End,new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED #PYHDR-PYMNT-SETTLMNT-DTE-END ( EM = YYYYMMDD ) TO #PYHDR-PYMNT-SETTLMNT-DTE-END-A
                    if (condition(DbsUtil.maskMatches(pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A,"YYYYMMDD")))                                                                    //Natural: IF #PYHDR-PYMNT-SETTLMNT-DTE-END-A EQ MASK ( YYYYMMDD )
                    {
                        setValueToSubstring(pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A.getSubstring(1,4),ldaFcpl961.getCommon_Xml_Duedatefuture_Data(),1,4);                      //Natural: MOVE SUBSTR ( #PYHDR-PYMNT-SETTLMNT-DTE-END-A,1,4 ) TO SUBSTR ( DUEDATEFUTURE-DATA,1,4 )
                        setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Duedatefuture_Data(),5,1);                                                                       //Natural: MOVE '-' TO SUBSTR ( DUEDATEFUTURE-DATA,5,1 )
                        setValueToSubstring(pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A.getSubstring(5,2),ldaFcpl961.getCommon_Xml_Duedatefuture_Data(),6,2);                      //Natural: MOVE SUBSTR ( #PYHDR-PYMNT-SETTLMNT-DTE-END-A,5,2 ) TO SUBSTR ( DUEDATEFUTURE-DATA,6,2 )
                        setValueToSubstring("-",ldaFcpl961.getCommon_Xml_Duedatefuture_Data(),8,1);                                                                       //Natural: MOVE '-' TO SUBSTR ( DUEDATEFUTURE-DATA,8,1 )
                        setValueToSubstring(pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A.getSubstring(7,2),ldaFcpl961.getCommon_Xml_Duedatefuture_Data(),9,2);                      //Natural: MOVE SUBSTR ( #PYHDR-PYMNT-SETTLMNT-DTE-END-A,7,2 ) TO SUBSTR ( DUEDATEFUTURE-DATA,9,2 )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Duedatefuture_Open(), ldaFcpl961.getCommon_Xml_Duedatefuture_Data(),  //Natural: COMPRESS DUEDATEFUTURE-OPEN DUEDATEFUTURE-DATA DUEDATEFUTURE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                        ldaFcpl961.getCommon_Xml_Duedatefuture_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl961.getCommon_Xml_Crefcontract_Data().notEquals(" ")))                                                                                   //Natural: IF CREFCONTRACT-DATA NE ' '
            {
                ldaFcpl961.getCommon_Xml_Crefcertificate_Data().setValue(ldaFcpl961.getCommon_Xml_Crefcontract_Data(), MoveOption.LeftJustified);                         //Natural: MOVE LEFT CREFCONTRACT-DATA TO CREFCERTIFICATE-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Crefcertificate_Open(), ldaFcpl961.getCommon_Xml_Crefcertificate_Data(),  //Natural: COMPRESS CREFCERTIFICATE-OPEN CREFCERTIFICATE-DATA CREFCERTIFICATE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Crefcertificate_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Interest_Amt.greater(getZero())))                                                                                                           //Natural: IF #INTEREST-AMT GT 0
            {
                ldaFcpl961.getCommon_Xml_Interestpayment_Data().setValueEdited(pnd_Interest_Amt,new ReportEditMask("ZZZZZZZZ9.99"));                                      //Natural: MOVE EDITED #INTEREST-AMT ( EM = ZZZZZZZZ9.99 ) TO INTERESTPAYMENT-DATA
                ldaFcpl961.getCommon_Xml_Interestpayment_Data().setValue(ldaFcpl961.getCommon_Xml_Interestpayment_Data(), MoveOption.LeftJustified);                      //Natural: MOVE LEFT INTERESTPAYMENT-DATA TO INTERESTPAYMENT-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Interestpayment_Open(), ldaFcpl961.getCommon_Xml_Interestpayment_Data(),  //Natural: COMPRESS INTERESTPAYMENT-OPEN INTERESTPAYMENT-DATA INTERESTPAYMENT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Interestpayment_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl961.getCommon_Xml_Netpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Netpaymentamount_Data(), MoveOption.LeftJustified);                        //Natural: MOVE LEFT NETPAYMENTAMOUNT-DATA TO NETPAYMENTAMOUNT-DATA
            if (condition(ldaFcpl961.getCommon_Xml_Netpaymentamount_Data().notEquals(" ")))                                                                               //Natural: IF NETPAYMENTAMOUNT-DATA NE ' '
            {
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Netpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Netpaymentamount_Data(),  //Natural: COMPRESS NETPAYMENTAMOUNT-OPEN NETPAYMENTAMOUNT-DATA NETPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Netpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*                             /* POPULATE SUBDETAIL TAGS
            //*  FUND LEVEL DEDUCTIONS
                                                                                                                                                                          //Natural: PERFORM LOAD-DEDUCTION-LEGENDS-BY-FUND
            sub_Load_Deduction_Legends_By_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SUBPAYMENT-INFO
            sub_Subpayment_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    ADDED 2016/11/13      START
            pnd_Total_Gross_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Dci_Amt().getValue(pnd_I_Fund));                                                                //Natural: ADD #WS-OCCURS.INV-ACCT-DCI-AMT ( #I-FUND ) TO #TOTAL-GROSS-AMT
            pnd_Total_Gross_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_I_Fund));                                                                //Natural: ADD #WS-OCCURS.INV-ACCT-DPI-AMT ( #I-FUND ) TO #TOTAL-GROSS-AMT
            pnd_Total_Gross_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_I_Fund));                                                              //Natural: ADD #WS-OCCURS.INV-ACCT-DVDND-AMT ( #I-FUND ) TO #TOTAL-GROSS-AMT
            pnd_Total_Gross_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_I_Fund));                                                              //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT ( #I-FUND ) TO #TOTAL-GROSS-AMT
            pnd_Cntrct_Ivc_Amt.nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_I_Fund));                                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-IVC-AMT ( #I-FUND ) TO #CNTRCT-IVC-AMT
            if (condition(pnd_Interest_Amt.greater(getZero())))                                                                                                           //Natural: IF #INTEREST-AMT GT 0
            {
                pnd_Ws_Interest_Amt.nadd(pnd_Interest_Amt);                                                                                                               //Natural: ADD #INTEREST-AMT TO #WS-INTEREST-AMT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Paymentinfo_Close());                                                                                          //Natural: ASSIGN #XML-LINE := PAYMENTINFO-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    ADDED 2016/11/13      END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*   C1000-PROCESS-FUND-RECORD
    }
    private void sub_Insert_Zip_Into_Address() throws Exception                                                                                                           //Natural: INSERT-ZIP-INTO-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        addressline2_Data.getValue("*").reset();                                                                                                                          //Natural: RESET ADDRESSLINE2-DATA ( * )
        short decideConditionsMet2875 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WS-NAME-N-ADDRESS.PYMNT-ADDR-ZIP-CDE ( 2 ) = MASK ( NNNNNNNNN )
        if (condition(DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde().getValue(2),"NNNNNNNNN")))
        {
            decideConditionsMet2875++;
            pnd_Formatted_Zip.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde().getValue(2).getSubstring(1,5),  //Natural: COMPRESS SUBSTR ( #WS-NAME-N-ADDRESS.PYMNT-ADDR-ZIP-CDE ( 2 ) ,1,5 ) '-' SUBSTR ( #WS-NAME-N-ADDRESS.PYMNT-ADDR-ZIP-CDE ( 2 ) ,6,4 ) INTO #FORMATTED-ZIP LEAVING NO SPACE
                "-", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde().getValue(2).getSubstring(6,4)));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Formatted_Zip.setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde().getValue(2));                                                             //Natural: ASSIGN #FORMATTED-ZIP := #WS-NAME-N-ADDRESS.PYMNT-ADDR-ZIP-CDE ( 2 )
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(2).setValue(DbsUtil.compress(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(2),  //Natural: COMPRESS #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 ) #FORMATTED-ZIP INTO #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 )
            pnd_Formatted_Zip));
        short decideConditionsMet2882 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 2 ) = ' '
        if (condition(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(2).equals(" ")))
        {
            decideConditionsMet2882++;
            pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(2).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(2));      //Natural: ASSIGN #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 2 ) := #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 )
        }                                                                                                                                                                 //Natural: WHEN #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 2 ) = ' '
        else if (condition(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(2).equals(" ")))
        {
            decideConditionsMet2882++;
            pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(2).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(2));      //Natural: ASSIGN #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 2 ) := #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 )
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet2882 > 0))
        {
            pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(2).reset();                                                                               //Natural: RESET #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 )
            pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(2).reset();                                                                               //Natural: RESET #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 2 )
            pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt().getValue(2).reset();                                                                               //Natural: RESET #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 2 )
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        addressline2_Data.getValue(1).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt().getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 2 ) TO ADDRESSLINE2-DATA ( 1 )
        addressline2_Data.getValue(2).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt().getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 2 ) TO ADDRESSLINE2-DATA ( 2 )
        addressline2_Data.getValue(3).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt().getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 2 ) TO ADDRESSLINE2-DATA ( 3 )
        addressline2_Data.getValue(4).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt().getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 ) TO ADDRESSLINE2-DATA ( 4 )
        addressline2_Data.getValue(5).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt().getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 2 ) TO ADDRESSLINE2-DATA ( 5 )
        addressline2_Data.getValue(6).setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt().getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 2 ) TO ADDRESSLINE2-DATA ( 6 )
    }
    //*   FE201307 START
    private void sub_Write_Work_3() throws Exception                                                                                                                      //Natural: WRITE-WORK-3
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().write(8, true, pnd_Xml_Line);                                                                                                                      //Natural: WRITE WORK FILE 8 VARIABLE #XML-LINE
        if (condition(pnd_Check_Rollover.getBoolean() && pnd_Xndx.lessOrEqual(250)))                                                                                      //Natural: IF #CHECK-ROLLOVER AND #XNDX LE 250
        {
            pnd_Xndx.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #XNDX
            if (condition(pnd_Bc_Line.getBoolean()))                                                                                                                      //Natural: IF #BC-LINE
            {
                pnd_Batch_Counter.nadd(1);                                                                                                                                //Natural: ADD 1 TO #BATCH-COUNTER
                ldaFcpl961.getCommon_Xml_Batchcounter_Data().setValue(pnd_Batch_Counter);                                                                                 //Natural: MOVE #BATCH-COUNTER TO BATCHCOUNTER-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Batchcounter_Open(), ldaFcpl961.getCommon_Xml_Batchcounter_Data(),  //Natural: COMPRESS BATCHCOUNTER-OPEN BATCHCOUNTER-DATA BATCHCOUNTER-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Batchcounter_Close()));
                //*       #BC-LINE := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Univ_Line.getBoolean()))                                                                                                                    //Natural: IF #UNIV-LINE
            {
                if (condition(pnd_Univ_Switch.getBoolean()))                                                                                                              //Natural: IF #UNIV-SWITCH
                {
                    ldaFcpl961.getCommon_Xml_Universalid_Data().setValueEdited(pnd_Batch_Counter,new ReportEditMask("9999999"));                                          //Natural: MOVE EDITED #BATCH-COUNTER ( EM = 9999999 ) TO UNIVERSALID-DATA
                    pnd_Univ_Switch.setValue(false);                                                                                                                      //Natural: ASSIGN #UNIV-SWITCH := FALSE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Universalid_Open(), ldaFcpl961.getCommon_Xml_Universalid_Data(),  //Natural: COMPRESS UNIVERSALID-OPEN UNIVERSALID-DATA UNIVERSALID-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Universalid_Close()));
                //*       #UNIV-LINE := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Dr_Line.getBoolean()))                                                                                                                      //Natural: IF #DR-LINE
            {
                setValueToSubstring(ldaFcpl961.getCommon_Xml_Universalid_Data(),ldaFcpl961.getCommon_Xml_Documentrequestid_Uid(),7,7);                                    //Natural: MOVE UNIVERSALID-DATA TO SUBSTRING ( DOCUMENTREQUESTID-UID,7,7 )
                ldaFcpl961.getCommon_Xml_Documentrequestid_Cnt().setValueEdited(pnd_Batch_Counter,new ReportEditMask("9999999"));                                         //Natural: MOVE EDITED #BATCH-COUNTER ( EM = 9999999 ) TO DOCUMENTREQUESTID-CNT
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Documentrequestid_Open(), ldaFcpl961.getCommon_Xml_Documentrequestid_Data(),  //Natural: COMPRESS DOCUMENTREQUESTID-OPEN DOCUMENTREQUESTID-DATA DOCUMENTREQUESTID-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Documentrequestid_Close()));
                //*       #DR-LINE := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Fn_Line.getBoolean()))                                                                                                                      //Natural: IF #FN-LINE
            {
                ldaFcpl961.getCommon_Xml_Fullname_Data().setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(2));                                           //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( 2 ) TO FULLNAME-DATA
                //*        IF FULLNAME-DATA                  = SCAN('CR:')
                //*           IGNORE
                //*        ELSE
                //*          COMPRESS 'CR ' FULLNAME-DATA  INTO        FULLNAME-DATA
                //*        END-IF
                if (condition(ldaFcpl961.getCommon_Xml_Fullname_Data().notEquals(" ")))                                                                                   //Natural: IF FULLNAME-DATA NE ' '
                {
                    DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Fullname_Data()), new ExamineSearch("&"), new ExamineReplace(" "));                        //Natural: EXAMINE FULLNAME-DATA FOR '&' REPLACE WITH ' '
                    pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Fullname_Open(), ldaFcpl961.getCommon_Xml_Fullname_Data(),  //Natural: COMPRESS FULLNAME-OPEN FULLNAME-DATA FULLNAME-CLOSE INTO #XML-LINE LEAVING NO SPACE
                        ldaFcpl961.getCommon_Xml_Fullname_Close()));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Adr_Line.getBoolean()))                                                                                                                     //Natural: IF #ADR-LINE
            {
                FOR07:                                                                                                                                                    //Natural: FOR #J 1 TO 6
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(6)); pnd_J.nadd(1))
                {
                    if (condition(addressline2_Data.getValue(pnd_J).notEquals(" ")))                                                                                      //Natural: IF ADDRESSLINE2-DATA ( #J ) NE ' '
                    {
                        DbsUtil.examine(new ExamineSource(addressline2_Data.getValue(pnd_J)), new ExamineSearch("&"), new ExamineReplace(" "));                           //Natural: EXAMINE ADDRESSLINE2-DATA ( #J ) FOR '&' REPLACE WITH ' '
                        pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Addressline_Open(), addressline2_Data.getValue(pnd_J),  //Natural: COMPRESS ADDRESSLINE-OPEN ADDRESSLINE2-DATA ( #J ) ADDRESSLINE-CLOSE INTO #XML-LINE LEAVING NO SPACE
                            ldaFcpl961.getCommon_Xml_Addressline_Close()));
                        pnd_Xndx_Rollover.getValue(pnd_Xndx).setValue(pnd_Xml_Line);                                                                                      //Natural: MOVE #XML-LINE TO #XNDX-ROLLOVER ( #XNDX )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Xndx.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #XNDX
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ins_Line.getBoolean()))                                                                                                                     //Natural: IF #INS-LINE
            {
                pnd_Xndx.nsubtract(1);                                                                                                                                    //Natural: ASSIGN #XNDX := #XNDX - 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Xndx_Rollover.getValue(pnd_Xndx).setValue(pnd_Xml_Line);                                                                                              //Natural: MOVE #XML-LINE TO #XNDX-ROLLOVER ( #XNDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   WRITE-WORK-FILE-3
    }
    //*   FE201307 START
    private void sub_Write_Rollover() throws Exception                                                                                                                    //Natural: WRITE-ROLLOVER
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #YNDX 1 TO #XNDX
        for (pnd_Yndx.setValue(1); condition(pnd_Yndx.lessOrEqual(pnd_Xndx)); pnd_Yndx.nadd(1))
        {
            pnd_Bndx.reset();                                                                                                                                             //Natural: RESET #BNDX
            DbsUtil.examine(new ExamineSource("<CheckEFTInd>EFT<(CheckEFTInd>"), new ExamineSearch(pnd_Xndx_Rollover.getValue(pnd_Yndx)), new ExamineGivingNumber(pnd_Bndx)); //Natural: EXAMINE '<CheckEFTInd>EFT</CheckEFTInd>' #XNDX-ROLLOVER ( #YNDX ) GIVING NUMBER #BNDX
            if (condition(pnd_Bndx.greater(getZero())))                                                                                                                   //Natural: IF #BNDX GT 0
            {
                pnd_Xndx_Rollover.getValue(pnd_Yndx).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "<CheckEFTInd>Rollover</CheckEFTInd>"));                    //Natural: COMPRESS '<CheckEFTInd>Rollover</CheckEFTInd>' INTO #XNDX-ROLLOVER ( #YNDX ) LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(8, true, pnd_Xndx_Rollover.getValue(pnd_Yndx));                                                                                          //Natural: WRITE WORK FILE 8 VARIABLE #XNDX-ROLLOVER ( #YNDX )
            //*   END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Xndx.reset();                                                                                                                                                 //Natural: RESET #XNDX
        pnd_Xndx_Rollover.getValue("*").reset();                                                                                                                          //Natural: RESET #XNDX-ROLLOVER ( * )
        //*   WRITE-ROLLOVER
    }
    private void sub_Subpayment_Info() throws Exception                                                                                                                   //Natural: SUBPAYMENT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Subpaymentdetail_Open());                                                                                          //Natural: ASSIGN #XML-LINE := SUBPAYMENTDETAIL-OPEN
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1().notEquals(" ")))                                                                                     //Natural: IF #FUND-PDA.#STMNT-LINE-1 NE ' '
        {
            ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data().setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_1());                                                      //Natural: ASSIGN SUBPAYMENTFROM-DATA := #FUND-PDA.#STMNT-LINE-1
            ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data(), MoveOption.LeftJustified);                            //Natural: MOVE LEFT SUBPAYMENTFROM-DATA TO SUBPAYMENTFROM-DATA
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentfrom_Open(), ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data(),  //Natural: COMPRESS SUBPAYMENTFROM-OPEN SUBPAYMENTFROM-DATA SUBPAYMENTFROM-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Subpaymentfrom_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  RESET  SUBPAYMENTFROMFREQUENCY-DATA
        //*  IF NOT #FUND-PDA.#DIVIDENDS
        //*   SUBPAYMENTFROMFREQUENCY-DATA    :=  #FUND-PDA.#VALUAT-DESC-9
        //*  MOVE LEFT SUBPAYMENTFROMFREQUENCY-DATA TO SUBPAYMENTFROMFREQUENCY-DATA
        //*   COMPRESS SUBPAYMENTFROMFREQUENCY-OPEN
        //*     SUBPAYMENTFROMFREQUENCY-DATA
        //*     SUBPAYMENTFROMFREQUENCY-CLOSE INTO #XML-LINE LEAVING NO SPACE
        //*   PERFORM WRITE-WORK-3
        //*  END-IF
        //*  IF #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Contractual_Data());                                                       //Natural: ASSIGN SUBPAYMENTAMOUNT-DATA := CONTRACTUAL-DATA
            ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(), MoveOption.LeftJustified);                        //Natural: MOVE LEFT SUBPAYMENTAMOUNT-DATA TO SUBPAYMENTAMOUNT-DATA
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNT-OPEN SUBPAYMENTAMOUNT-DATA SUBPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue("Contractual");                                                                            //Natural: ASSIGN SUBPAYMENTAMOUNTFREQUENCY-DATA := 'Contractual'
            ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(), MoveOption.LeftJustified);      //Natural: MOVE LEFT SUBPAYMENTAMOUNTFREQUENCY-DATA TO SUBPAYMENTAMOUNTFREQUENCY-DATA
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNTFREQUENCY-OPEN SUBPAYMENTAMOUNTFREQUENCY-DATA SUBPAYMENTAMOUNTFREQUENCY-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Num().notEquals(40)))                                                                                  //Natural: IF #FUND-PDA.#INV-ACCT-NUM NE 40
            {
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Unit_Data());                                                          //Natural: ASSIGN SUBPAYMENTAMOUNT-DATA := UNIT-DATA
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(), MoveOption.LeftJustified);                    //Natural: MOVE LEFT SUBPAYMENTAMOUNT-DATA TO SUBPAYMENTAMOUNT-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNT-OPEN SUBPAYMENTAMOUNT-DATA SUBPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Subpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape())) {return;}
                ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue("Units");                                                                              //Natural: ASSIGN SUBPAYMENTAMOUNTFREQUENCY-DATA := 'Units'
                ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(), MoveOption.LeftJustified);  //Natural: MOVE LEFT SUBPAYMENTAMOUNTFREQUENCY-DATA TO SUBPAYMENTAMOUNTFREQUENCY-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNTFREQUENCY-OPEN SUBPAYMENTAMOUNTFREQUENCY-DATA SUBPAYMENTAMOUNTFREQUENCY-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape())) {return;}
                //*  FIXED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_I_Fund),new                 //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-SETTL-AMT ( #I-FUND ) ( EM = ZZZZZZZZ9.99 ) TO SUBPAYMENTAMOUNT-DATA
                    ReportEditMask("ZZZZZZZZ9.99"));
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(), MoveOption.LeftJustified);                    //Natural: MOVE LEFT SUBPAYMENTAMOUNT-DATA TO SUBPAYMENTAMOUNT-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNT-OPEN SUBPAYMENTAMOUNT-DATA SUBPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Subpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Subdeductionamount_Data().reset();                                                                                                       //Natural: RESET SUBDEDUCTIONAMOUNT-DATA SUBDEDUCTIONAMOUNTFREQUENCY-DATA #DED-COUNT #DED-LAST #DED-CURR
        ldaFcpl961.getCommon_Xml_Subdeductionamountfrequency_Data().reset();
        pnd_Ded_Count.reset();
        pnd_Ded_Last.reset();
        pnd_Ded_Curr.reset();
                                                                                                                                                                          //Natural: PERFORM GENERATE-DED-LINE
        sub_Generate_Ded_Line();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Subpaymentdetail_Close());                                                                                         //Natural: ASSIGN #XML-LINE := SUBPAYMENTDETAIL-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Subpaymentdetail_Open());                                                                                          //Natural: ASSIGN #XML-LINE := SUBPAYMENTDETAIL-OPEN
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2().notEquals(" ")))                                                                                     //Natural: IF #FUND-PDA.#STMNT-LINE-2 NE ' '
        {
            ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data().setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Stmnt_Line_2());                                                      //Natural: ASSIGN SUBPAYMENTFROM-DATA := #FUND-PDA.#STMNT-LINE-2
            ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data(), MoveOption.LeftJustified);                            //Natural: MOVE LEFT SUBPAYMENTFROM-DATA TO SUBPAYMENTFROM-DATA
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentfrom_Open(), ldaFcpl961.getCommon_Xml_Subpaymentfrom_Data(),  //Natural: COMPRESS SUBPAYMENTFROM-OPEN SUBPAYMENTFROM-DATA SUBPAYMENTFROM-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Subpaymentfrom_Close()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentfrom_Open(), "!", ldaFcpl961.getCommon_Xml_Subpaymentfrom_Close())); //Natural: COMPRESS SUBPAYMENTFROM-OPEN '!' SUBPAYMENTFROM-CLOSE INTO #XML-LINE LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Xml_Line), new ExamineSearch("!"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #XML-LINE '!' REPLACE ' '
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        //*   #WS-HEADER-RECORD.PYMNT-FTRE-IND EQ 'F'
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Dividend_Data());                                                          //Natural: ASSIGN SUBPAYMENTAMOUNT-DATA := DIVIDEND-DATA
            ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(), MoveOption.LeftJustified);                        //Natural: MOVE LEFT SUBPAYMENTAMOUNT-DATA TO SUBPAYMENTAMOUNT-DATA
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNT-OPEN SUBPAYMENTAMOUNT-DATA SUBPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
            ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue("Dividend");                                                                               //Natural: ASSIGN SUBPAYMENTAMOUNTFREQUENCY-DATA := 'Dividend'
            ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(), MoveOption.LeftJustified);      //Natural: MOVE LEFT SUBPAYMENTAMOUNTFREQUENCY-DATA TO SUBPAYMENTAMOUNTFREQUENCY-DATA
            pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNTFREQUENCY-OPEN SUBPAYMENTAMOUNTFREQUENCY-DATA SUBPAYMENTAMOUNTFREQUENCY-CLOSE INTO #XML-LINE LEAVING NO SPACE
                ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
            sub_Write_Work_3();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Num().notEquals(40)))                                                                                  //Natural: IF #FUND-PDA.#INV-ACCT-NUM NE 40
            {
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Unitvalue_Data());                                                     //Natural: ASSIGN SUBPAYMENTAMOUNT-DATA := UNITVALUE-DATA
                ldaFcpl961.getCommon_Xml_Subpaymentamount_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(), MoveOption.LeftJustified);                    //Natural: MOVE LEFT SUBPAYMENTAMOUNT-DATA TO SUBPAYMENTAMOUNT-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamount_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamount_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNT-OPEN SUBPAYMENTAMOUNT-DATA SUBPAYMENTAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Subpaymentamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape())) {return;}
                ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue("Unit Value");                                                                         //Natural: ASSIGN SUBPAYMENTAMOUNTFREQUENCY-DATA := 'Unit Value'
                ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().setValue(ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(), MoveOption.LeftJustified);  //Natural: MOVE LEFT SUBPAYMENTAMOUNTFREQUENCY-DATA TO SUBPAYMENTAMOUNTFREQUENCY-DATA
                pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Open(), ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data(),  //Natural: COMPRESS SUBPAYMENTAMOUNTFREQUENCY-OPEN SUBPAYMENTAMOUNTFREQUENCY-DATA SUBPAYMENTAMOUNTFREQUENCY-CLOSE INTO #XML-LINE LEAVING NO SPACE
                    ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                sub_Write_Work_3();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl961.getCommon_Xml_Subpaymentamountfrequency_Data().reset();                                                                                        //Natural: RESET SUBPAYMENTAMOUNTFREQUENCY-DATA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Ded_Last.getBoolean())))                                                                                                                     //Natural: IF NOT #DED-LAST
        {
                                                                                                                                                                          //Natural: PERFORM GENERATE-DED-LINE
            sub_Generate_Ded_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Subpaymentdetail_Close());                                                                                         //Natural: ASSIGN #XML-LINE := SUBPAYMENTDETAIL-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
        sub_Write_Work_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ded_Last.getBoolean() || (pnd_Ded_Curr.greater(15)) || pnd_Ded_Count.less(2)))                                                                  //Natural: IF #DED-LAST OR ( #DED-CURR GT 15 ) OR #DED-COUNT LT 2
        {
            ignore();
            //*  SUBDETAIL LINE 3 TO 5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR09:                                                                                                                                                        //Natural: FOR #DED-NDX2 #DED-CURR 15
            for (pnd_Ded_Ndx2.setValue(pnd_Ded_Curr); condition(pnd_Ded_Ndx2.lessOrEqual(15)); pnd_Ded_Ndx2.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM GENERATE-DED-LINE
                sub_Generate_Ded_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUBPAYMENT-INFO
    }
    private void sub_Generate_Ded_Line() throws Exception                                                                                                                 //Natural: GENERATE-DED-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ded_Curr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #DED-CURR
        pnd_Ded_Save.setValue(pnd_Ded_Curr);                                                                                                                              //Natural: ASSIGN #DED-SAVE := #DED-CURR
        if (condition(pnd_Ded_Curr.greater(15) || pnd_Ded_Last.getBoolean()))                                                                                             //Natural: IF #DED-CURR GT 15 OR #DED-LAST
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR10:                                                                                                                                                            //Natural: FOR #DED-NDX #DED-SAVE 15
        for (pnd_Ded_Ndx.setValue(pnd_Ded_Save); condition(pnd_Ded_Ndx.lessOrEqual(15)); pnd_Ded_Ndx.nadd(1))
        {
            if (condition(pnd_Ded_Curr.equals(15)))                                                                                                                       //Natural: IF #DED-CURR = 15
            {
                pnd_Ded_Last.setValue(true);                                                                                                                              //Natural: ASSIGN #DED-LAST := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ded_Ndx.greater(15)))                                                                                                                       //Natural: IF #DED-NDX GT 15
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ded_Pymnt_Table.getValue(pnd_Ded_Ndx).equals(" ")))                                                                                         //Natural: IF #DED-PYMNT-TABLE ( #DED-NDX ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ded_Ndx).greater(getZero())))                                                                      //Natural: IF #CNTRCT-DED-TABLE ( #DED-NDX ) GT 0
                {
                    ldaFcpl961.getCommon_Xml_Subdeductionamount_Data().setValueEdited(pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ded_Ndx),new ReportEditMask("ZZZZZZ9.99")); //Natural: MOVE EDITED #CNTRCT-DED-TABLE ( #DED-NDX ) ( EM = ZZZZZZ9.99 ) TO SUBDEDUCTIONAMOUNT-DATA
                    ldaFcpl961.getCommon_Xml_Subdeductionamount_Data().setValue(ldaFcpl961.getCommon_Xml_Subdeductionamount_Data(), MoveOption.LeftJustified);            //Natural: MOVE LEFT SUBDEDUCTIONAMOUNT-DATA TO SUBDEDUCTIONAMOUNT-DATA
                    if (condition(pnd_Ded_Pymnt_Table.getValue(pnd_Ded_Ndx).equals("  F") || pnd_Ded_Pymnt_Table.getValue(pnd_Ded_Ndx).equals("  S") ||                   //Natural: IF #DED-PYMNT-TABLE ( #DED-NDX ) = '  F' OR = '  S' OR = '  L'
                        pnd_Ded_Pymnt_Table.getValue(pnd_Ded_Ndx).equals("  L")))
                    {
                        ldaFcpl961.getCommon_Xml_Subdeductionamountfrequency_Data().setValue(pnd_Ws_Legend.getValue(pnd_Ded_Ndx));                                        //Natural: MOVE #WS-LEGEND ( #DED-NDX ) TO SUBDEDUCTIONAMOUNTFREQUENCY-DATA
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*        COMPRESS '(' #DED-PYMNT-TABLE(#DED-NDX) ')' /* DEFECT 181070
                        //*  DEFECT 181070
                        ldaFcpl961.getCommon_Xml_Subdeductionamountfrequency_Data().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Pymnt_Ded3.getValue(pnd_Ded_Ndx),  //Natural: COMPRESS '(' #PYMNT-DED3 ( #DED-NDX ) ')' INTO SUBDEDUCTIONAMOUNTFREQUENCY-DATA LEAVING NO
                            ")"));
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ded_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #DED-COUNT
                    if (condition(pnd_Ded_Count.greater(2)))                                                                                                              //Natural: IF #DED-COUNT GT 2
                    {
                        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Subpaymentdetail_Open());                                                                          //Natural: ASSIGN #XML-LINE := SUBPAYMENTDETAIL-OPEN
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                        sub_Write_Work_3();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subdeductionamount_Open(), ldaFcpl961.getCommon_Xml_Subdeductionamount_Data(),  //Natural: COMPRESS SUBDEDUCTIONAMOUNT-OPEN SUBDEDUCTIONAMOUNT-DATA SUBDEDUCTIONAMOUNT-CLOSE INTO #XML-LINE LEAVING NO SPACE
                        ldaFcpl961.getCommon_Xml_Subdeductionamount_Close()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Xml_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl961.getCommon_Xml_Subdeductionamountfrequency_Open(),                    //Natural: COMPRESS SUBDEDUCTIONAMOUNTFREQUENCY-OPEN SUBDEDUCTIONAMOUNTFREQUENCY-DATA SUBDEDUCTIONAMOUNTFREQUENCY-CLOS INTO #XML-LINE LEAVING NO SPACE
                        ldaFcpl961.getCommon_Xml_Subdeductionamountfrequency_Data(), ldaFcpl961.getCommon_Xml_Subdeductionamountfrequency_Clos()));
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                    sub_Write_Work_3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ded_Count.greater(2)))                                                                                                              //Natural: IF #DED-COUNT GT 2
                    {
                        pnd_Xml_Line.setValue(ldaFcpl961.getCommon_Xml_Subpaymentdetail_Close());                                                                         //Natural: ASSIGN #XML-LINE := SUBPAYMENTDETAIL-CLOSE
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-3
                        sub_Write_Work_3();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ded_Ndx.greaterOrEqual(15)))                                                                                                                //Natural: IF #DED-NDX GE 15
            {
                pnd_Ded_Last.setValue(true);                                                                                                                              //Natural: ASSIGN #DED-LAST := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ded_Curr.setValue(pnd_Ded_Ndx);                                                                                                                               //Natural: ASSIGN #DED-CURR := #DED-NDX
        //*  GENERATE-DED-LINE
    }
    private void sub_After_Tax_Routine() throws Exception                                                                                                                 //Natural: AFTER-TAX-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet3133 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CNTRCT-IVC-AMT GT 0
        if (condition(pnd_Cntrct_Ivc_Amt.greater(getZero())))
        {
            decideConditionsMet3133++;
            ldaFcpl961.getCommon_Xml_Aftertaxamount_Data().setValueEdited(pnd_Cntrct_Ivc_Amt,new ReportEditMask("ZZZZZZ9.99"));                                           //Natural: MOVE EDITED #CNTRCT-IVC-AMT ( EM = ZZZZZZ9.99 ) TO AFTERTAXAMOUNT-DATA
            ldaFcpl961.getCommon_Xml_Aftertaxamount_Data().setValue(ldaFcpl961.getCommon_Xml_Aftertaxamount_Data(), MoveOption.LeftJustified);                            //Natural: MOVE LEFT AFTERTAXAMOUNT-DATA TO AFTERTAXAMOUNT-DATA
            ldaFcpl961.getCommon_Xml_Aftertaxdesc_Data().setValue(DbsUtil.compress(ldaFcpl893g.getPnd_Fcpl893g_Pnd_Ivc_Text()));                                          //Natural: COMPRESS #FCPL893G.#IVC-TEXT INTO AFTERTAXDESC-DATA
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Tiaa_Ppcn.getBoolean()))                                                                                                                        //Natural: IF #TIAA-PPCN
        {
            //*         COMPRESS #FCPL893G.#IVC-TEXT #FCPL893G.#COMPANY-TEXT(1)
            ldaFcpl961.getCommon_Xml_Aftertaxdesc_Data().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "This payment includes after-tax contributions of :!",  //Natural: COMPRESS 'This payment includes after-tax contributions of :!' #FCPL893G.#COMPANY-TEXT ( 1 ) INTO AFTERTAXDESC-DATA LEAVING NO
                ldaFcpl893g.getPnd_Fcpl893g_Pnd_Company_Text().getValue(1)));
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Aftertaxdesc_Data()), new ExamineSearch("!"), new ExamineDelete());                                //Natural: EXAMINE AFTERTAXDESC-DATA '!' DELETE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Aftertaxdesc_Data().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "This payment includes after-tax contributions of :!",  //Natural: COMPRESS 'This payment includes after-tax contributions of :!' #FCPL893G.#COMPANY-TEXT ( 2 ) INTO AFTERTAXDESC-DATA LEAVING NO
                ldaFcpl893g.getPnd_Fcpl893g_Pnd_Company_Text().getValue(2)));
            DbsUtil.examine(new ExamineSource(ldaFcpl961.getCommon_Xml_Aftertaxdesc_Data()), new ExamineSearch("!"), new ExamineDelete());                                //Natural: EXAMINE AFTERTAXDESC-DATA '!' DELETE
        }                                                                                                                                                                 //Natural: END-IF
        //*  AFTER-TAX-ROUTINE
    }
    private void sub_Load_Deduction_Legends() throws Exception                                                                                                            //Natural: LOAD-DEDUCTION-LEGENDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ded_Pymnt_Table.getValue("*").reset();                                                                                                                        //Natural: RESET #DED-PYMNT-TABLE ( * ) #CNTRCT-DED-TABLE ( * ) #PYMNT-DED-# #PYMNT-DED2 #PYMNT-DED3 ( * )
        pnd_Ws_Pnd_Cntrct_Ded_Table.getValue("*").reset();
        pnd_Pymnt_Ded_Pnd.reset();
        pnd_Pymnt_Ded2.reset();
        pnd_Pymnt_Ded3.getValue("*").reset();
        //*  **************************** FEDERAL TAX
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero())))                                       //Natural: IF #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) GT 0
        {
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                //Natural: IF #WS-HEADER-RECORD.PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
                {
                    pnd_Ws_Legend.getValue(1).setValue("(N)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(N)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Legend.getValue(1).setValue("(F)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(F)'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '2'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86'
                {
                    pnd_Ws_Legend.getValue(1).setValue("(C)*");                                                                                                           //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(C)*'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86")))) //Natural: IF NOT ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86' )
                    {
                        pnd_Ws_Legend.getValue(1).setValue("(C)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(C)'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ded_Pymnt_Table.getValue(1).setValue("  F");                                                                                                              //Natural: ASSIGN #DED-PYMNT-TABLE ( 1 ) := '  F'
            pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(1).nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                       //Natural: ADD #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #CNTRCT-DED-TABLE ( 1 )
            pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PYMNT-DED-#
        }                                                                                                                                                                 //Natural: END-IF
        //*  ******************** STATE TAX
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero())))                                      //Natural: IF #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) GT 0
        {
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))                                                                         //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '97'
                {
                    pnd_Ws_Legend.getValue(2).setValue("(S)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(S)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11))  //Natural: IF ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' )
                        || (pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))))
                    {
                        pnd_Ws_Legend.getValue(2).setValue("(S)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(S)'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(((! ((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11)))  //Natural: IF NOT ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' ) AND #WS-HEADER-RECORD.ANNT-RSDNCY-CDE NE '97'
                            && ! ((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))) 
                            && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().notEquals("97"))))
                        {
                            pnd_Ws_Legend.getValue(2).setValue("(N)");                                                                                                    //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(N)'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '2'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86' )
                {
                    pnd_Ws_Legend.getValue(2).setValue("(P)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(P)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("96")))                                                                     //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '96'
                    {
                        pnd_Ws_Legend.getValue(2).setValue("(C)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(C)'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Legend.getValue(2).setValue("(C)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(C)'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ded_Pymnt_Table.getValue(2).setValue("  S");                                                                                                              //Natural: ASSIGN #DED-PYMNT-TABLE ( 2 ) := '  S'
            pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(2).nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                      //Natural: ADD #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #CNTRCT-DED-TABLE ( 2 )
            pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PYMNT-DED-#
        }                                                                                                                                                                 //Natural: END-IF
        //*  ******************** LOCAL TAX
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct).greater(getZero())))                                      //Natural: IF #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) GT 0
        {
            pnd_Ws_Legend.getValue(3).setValue("(L)");                                                                                                                    //Natural: ASSIGN #WS-LEGEND ( 3 ) := '(L)'
            pnd_Ded_Pymnt_Table.getValue(3).setValue("  L");                                                                                                              //Natural: ASSIGN #DED-PYMNT-TABLE ( 3 ) := '  L'
            pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(3).nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Cntr_Inv_Acct));                      //Natural: ADD #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( 1:#WS-CNTR-INV-ACCT ) TO #CNTRCT-DED-TABLE ( 3 )
            pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PYMNT-DED-#
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Chk_Fields_Pnd_Cntrct_Amt.getValue("*").setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue("*"));                                             //Natural: ASSIGN #CNTRCT-AMT ( * ) := #WS-OCCURS.INV-ACCT-SETTL-AMT ( * )
        FOR11:                                                                                                                                                            //Natural: FOR #NDX 1 TO #WS-CNTR-INV-ACCT
        for (pnd_Ndx.setValue(1); condition(pnd_Ndx.lessOrEqual(pnd_Ws_Cntr_Inv_Acct)); pnd_Ndx.nadd(1))
        {
            FOR12:                                                                                                                                                        //Natural: FOR #NDX2 1 TO 10
            for (pnd_Ndx2.setValue(1); condition(pnd_Ndx2.lessOrEqual(10)); pnd_Ndx2.nadd(1))
            {
                pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx2).nadd(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_Ndx,pnd_Ndx2));                    //Natural: ADD #WS-OCCURS.PYMNT-DED-AMT ( #NDX,#NDX2 ) TO #PYMNT-DED-TAB ( #NDX2 )
                //*   MOVE  #WS-OCCURS.PYMNT-DED-AMT (#NDX,#NDX2) TO #PYMNT-DED-TAB (#NDX2)
                if (condition(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_Ndx,pnd_Ndx2).equals(getZero())))                                                  //Natural: IF #WS-OCCURS.PYMNT-DED-CDE ( #NDX,#NDX2 ) = 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx2).setValue(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_Ndx,pnd_Ndx2));            //Natural: MOVE #WS-OCCURS.PYMNT-DED-CDE ( #NDX,#NDX2 ) TO #PYMNT-DED-CDE ( #NDX2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR13:                                                                                                                                                            //Natural: FOR #NDX = 1 TO 10
        for (pnd_Ndx.setValue(1); condition(pnd_Ndx.lessOrEqual(10)); pnd_Ndx.nadd(1))
        {
            if (condition(DbsUtil.maskMatches(pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx),"NNNNNNNNN")))                                                      //Natural: IF #PYMNT-DED-TAB ( #NDX ) = MASK ( NNNNNNNNN )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Ded_Tab_A.getValue(pnd_Ndx).setValue("000000000");                                                                                              //Natural: MOVE '000000000' TO #PYMNT-DED-TAB-A ( #NDX )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Pymnt_Ded_Cde_A.getValue(pnd_Ndx),"NNN")))                                                                              //Natural: IF #PYMNT-DED-CDE-A ( #NDX ) = MASK ( NNN )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Ded_Cde_A.getValue(pnd_Ndx).setValue("000");                                                                                                    //Natural: MOVE '000' TO #PYMNT-DED-CDE-A ( #NDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR14:                                                                                                                                                            //Natural: FOR #NDX-DED = 1 TO 10
        for (pnd_Ndx_Ded.setValue(1); condition(pnd_Ndx_Ded.lessOrEqual(10)); pnd_Ndx_Ded.nadd(1))
        {
            if (condition(pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded).greaterOrEqual(1) && pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded).lessOrEqual(10))) //Natural: IF #PYMNT-DED-CDE ( #NDX-DED ) = 1 THRU 10
            {
                pnd_Pos_Ded.setValue(pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded));                                                                        //Natural: ASSIGN #POS-DED := #PYMNT-DED-CDE ( #NDX-DED )
                deductiondesc_Tab.getValue(pnd_Ndx_Ded).setValue(pnd_Ded_Desc_Tab.getValue(pnd_Pos_Ded));                                                                 //Natural: MOVE #DED-DESC-TAB ( #POS-DED ) TO DEDUCTIONDESC-TAB ( #NDX-DED )
                pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                //Natural: ADD 1 TO #PYMNT-DED-#
                pnd_Pymnt_Ded2.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PYMNT-DED2
                pnd_Ded_Pymnt_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).setValue(pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded));                          //Natural: ASSIGN #DED-PYMNT-TABLE ( #NDX-DED+3 ) := #PYMNT-DED-CDE ( #NDX-DED )
                pnd_Pymnt_Ded3.getValue(pnd_Ndx_Ded.getDec().add(3)).setValue(pnd_Pymnt_Ded2);                                                                            //Natural: ASSIGN #PYMNT-DED3 ( #NDX-DED+3 ) := #PYMNT-DED2
                pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).setValue(pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx_Ded));                  //Natural: ASSIGN #CNTRCT-DED-TABLE ( #NDX-DED+3 ) := #PYMNT-DED-TAB ( #NDX-DED )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                deductiondesc_Tab.getValue(pnd_Ndx_Ded).reset();                                                                                                          //Natural: RESET DEDUCTIONDESC-TAB ( #NDX-DED ) #DED-PYMNT-TABLE ( #NDX-DED+3 ) #CNTRCT-DED-TABLE ( #NDX-DED+3 )
                pnd_Ded_Pymnt_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).reset();
                pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).reset();
            }                                                                                                                                                             //Natural: END-IF
            deductionamount_Tab.getValue(pnd_Ndx_Ded).setValueEdited(pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx_Ded),new ReportEditMask("ZZZZZZ9.99"));       //Natural: MOVE EDITED #PYMNT-DED-TAB ( #NDX-DED ) ( EM = ZZZZZZ9.99 ) TO DEDUCTIONAMOUNT-TAB ( #NDX-DED )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #PYMNT-DED-#
        //*  LOAD-DEDUCTION-LEGENDS
    }
    private void sub_Load_Deduction_Legends_By_Fund() throws Exception                                                                                                    //Natural: LOAD-DEDUCTION-LEGENDS-BY-FUND
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ded_Pymnt_Table.getValue("*").reset();                                                                                                                        //Natural: RESET #DED-PYMNT-TABLE ( * ) #CNTRCT-DED-TABLE ( * ) #PYMNT-DED-# #PYMNT-DED2 #PYMNT-DED3 ( * )
        pnd_Ws_Pnd_Cntrct_Ded_Table.getValue("*").reset();
        pnd_Pymnt_Ded_Pnd.reset();
        pnd_Pymnt_Ded2.reset();
        pnd_Pymnt_Ded3.getValue("*").reset();
        //*  **************************** FEDERAL TAX
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I_Fund).greater(getZero())))                                                       //Natural: IF #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #I-FUND ) GT 0
        {
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                //Natural: IF #WS-HEADER-RECORD.PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
                {
                    pnd_Ws_Legend.getValue(1).setValue("(N)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(N)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Legend.getValue(1).setValue("(F)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(F)'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '2'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86'
                {
                    pnd_Ws_Legend.getValue(1).setValue("(C)*");                                                                                                           //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(C)*'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86")))) //Natural: IF NOT ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86' )
                    {
                        pnd_Ws_Legend.getValue(1).setValue("(C)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 1 ) := '(C)'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ded_Pymnt_Table.getValue(1).setValue("  F");                                                                                                              //Natural: ASSIGN #DED-PYMNT-TABLE ( 1 ) := '  F'
            pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(1).setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I_Fund));                                   //Natural: ASSIGN #CNTRCT-DED-TABLE ( 1 ) := #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #I-FUND )
            pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PYMNT-DED-#
        }                                                                                                                                                                 //Natural: END-IF
        //*  ******************** STATE TAX
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I_Fund).greater(getZero())))                                                      //Natural: IF #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #I-FUND ) GT 0
        {
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))                                                                         //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '97'
                {
                    pnd_Ws_Legend.getValue(2).setValue("(S)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(S)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11))  //Natural: IF ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' )
                        || (pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))))
                    {
                        pnd_Ws_Legend.getValue(2).setValue("(S)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(S)'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(((! ((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11)))  //Natural: IF NOT ( ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' ) AND #WS-HEADER-RECORD.ANNT-RSDNCY-CDE NE '97'
                            && ! ((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))) 
                            && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().notEquals("97"))))
                        {
                            pnd_Ws_Legend.getValue(2).setValue("(N)");                                                                                                    //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(N)'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '2'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86' )
                {
                    pnd_Ws_Legend.getValue(2).setValue("(P)");                                                                                                            //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(P)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("96")))                                                                     //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '96'
                    {
                        pnd_Ws_Legend.getValue(2).setValue("(C)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(C)'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Legend.getValue(2).setValue("(C)");                                                                                                        //Natural: ASSIGN #WS-LEGEND ( 2 ) := '(C)'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ded_Pymnt_Table.getValue(2).setValue("  S");                                                                                                              //Natural: ASSIGN #DED-PYMNT-TABLE ( 2 ) := '  S'
            pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(2).setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I_Fund));                                  //Natural: ASSIGN #CNTRCT-DED-TABLE ( 2 ) := #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #I-FUND )
            pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PYMNT-DED-#
        }                                                                                                                                                                 //Natural: END-IF
        //*  ******************** LOCAL TAX
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I_Fund).greater(getZero())))                                                      //Natural: IF #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #I-FUND ) GT 0
        {
            pnd_Ws_Legend.getValue(3).setValue("(L)");                                                                                                                    //Natural: ASSIGN #WS-LEGEND ( 3 ) := '(L)'
            pnd_Ded_Pymnt_Table.getValue(3).setValue("  L");                                                                                                              //Natural: ASSIGN #DED-PYMNT-TABLE ( 3 ) := '  L'
            pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(3).nadd(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I_Fund));                                      //Natural: ADD #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #I-FUND ) TO #CNTRCT-DED-TABLE ( 3 )
            pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PYMNT-DED-#
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Chk_Fields_Pnd_Cntrct_Amt.getValue("*").setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue("*"));                                             //Natural: ASSIGN #CNTRCT-AMT ( * ) := #WS-OCCURS.INV-ACCT-SETTL-AMT ( * )
        FOR15:                                                                                                                                                            //Natural: FOR #NDX 1 TO 10
        for (pnd_Ndx.setValue(1); condition(pnd_Ndx.lessOrEqual(10)); pnd_Ndx.nadd(1))
        {
            pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx).setValue(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_I_Fund,pnd_Ndx));                   //Natural: MOVE #WS-OCCURS.PYMNT-DED-AMT ( #I-FUND,#NDX ) TO #PYMNT-DED-TAB ( #NDX )
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I_Fund,pnd_Ndx).equals(getZero())))                                                    //Natural: IF #WS-OCCURS.PYMNT-DED-CDE ( #I-FUND,#NDX ) EQ 0
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx).setValue(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I_Fund,pnd_Ndx));               //Natural: MOVE #WS-OCCURS.PYMNT-DED-CDE ( #I-FUND,#NDX ) TO #PYMNT-DED-CDE ( #NDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR16:                                                                                                                                                            //Natural: FOR #NDX = 1 TO 10
        for (pnd_Ndx.setValue(1); condition(pnd_Ndx.lessOrEqual(10)); pnd_Ndx.nadd(1))
        {
            if (condition(DbsUtil.maskMatches(pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx),"NNNNNNNNN")))                                                      //Natural: IF #PYMNT-DED-TAB ( #NDX ) = MASK ( NNNNNNNNN )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Ded_Tab_A.getValue(pnd_Ndx).setValue("000000000");                                                                                              //Natural: MOVE '000000000' TO #PYMNT-DED-TAB-A ( #NDX )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.maskMatches(pnd_Pymnt_Ded_Cde_A.getValue(pnd_Ndx),"NNN")))                                                                              //Natural: IF #PYMNT-DED-CDE-A ( #NDX ) = MASK ( NNN )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Ded_Cde_A.getValue(pnd_Ndx).setValue("000");                                                                                                    //Natural: MOVE '000' TO #PYMNT-DED-CDE-A ( #NDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ded_Amt.nadd(pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue("*"));                                                                                            //Natural: ADD #PYMNT-DED-TAB ( * ) TO #DED-AMT
        FOR17:                                                                                                                                                            //Natural: FOR #NDX-DED = 1 TO 10
        for (pnd_Ndx_Ded.setValue(1); condition(pnd_Ndx_Ded.lessOrEqual(10)); pnd_Ndx_Ded.nadd(1))
        {
            if (condition(pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded).greaterOrEqual(1) && pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded).lessOrEqual(10))) //Natural: IF #PYMNT-DED-CDE ( #NDX-DED ) = 1 THRU 10
            {
                pnd_Pos_Ded.setValue(pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded));                                                                        //Natural: ASSIGN #POS-DED := #PYMNT-DED-CDE ( #NDX-DED )
                deductiondesc_Tab.getValue(pnd_Ndx_Ded).setValue(pnd_Ded_Desc_Tab.getValue(pnd_Pos_Ded));                                                                 //Natural: MOVE #DED-DESC-TAB ( #POS-DED ) TO DEDUCTIONDESC-TAB ( #NDX-DED )
                pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                //Natural: ADD 1 TO #PYMNT-DED-#
                pnd_Pymnt_Ded2.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PYMNT-DED2
                pnd_Ded_Pymnt_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).setValue(pnd_Pymnt_Ded_Cde_A_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ndx_Ded));                          //Natural: ASSIGN #DED-PYMNT-TABLE ( #NDX-DED+3 ) := #PYMNT-DED-CDE ( #NDX-DED )
                pnd_Pymnt_Ded3.getValue(pnd_Ndx_Ded.getDec().add(3)).setValue(pnd_Pymnt_Ded2);                                                                            //Natural: ASSIGN #PYMNT-DED3 ( #NDX-DED+3 ) := #PYMNT-DED2
                pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).setValue(pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx_Ded));                  //Natural: ASSIGN #CNTRCT-DED-TABLE ( #NDX-DED+3 ) := #PYMNT-DED-TAB ( #NDX-DED )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                deductiondesc_Tab.getValue(pnd_Ndx_Ded).reset();                                                                                                          //Natural: RESET DEDUCTIONDESC-TAB ( #NDX-DED ) #DED-PYMNT-TABLE ( #NDX-DED+3 ) #CNTRCT-DED-TABLE ( #NDX-DED+3 )
                pnd_Ded_Pymnt_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).reset();
                pnd_Ws_Pnd_Cntrct_Ded_Table.getValue(pnd_Ndx_Ded.getDec().add(3)).reset();
            }                                                                                                                                                             //Natural: END-IF
            deductionamount_Tab.getValue(pnd_Ndx_Ded).setValueEdited(pnd_Pymnt_Ded_Tab_A_Pnd_Pymnt_Ded_Tab.getValue(pnd_Ndx_Ded),new ReportEditMask("ZZZZZZ9.99"));       //Natural: MOVE EDITED #PYMNT-DED-TAB ( #NDX-DED ) ( EM = ZZZZZZ9.99 ) TO DEDUCTIONAMOUNT-TAB ( #NDX-DED )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #PYMNT-DED-#
        //*  LOAD-DEDUCTION-LEGENDS-BY-FUND
    }
    private void sub_Get_State_Line() throws Exception                                                                                                                    //Natural: GET-STATE-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                                 //Natural: ASSIGN TBLDCODA.#MODE := 'T'
        if (condition(pnd_Total_Ded.greater(getZero())))                                                                                                                  //Natural: IF #TOTAL-DED GT 0
        {
                                                                                                                                                                          //Natural: PERFORM LOAD-DED-TABLE
            sub_Load_Ded_Table();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ded_Pymnt_Table.getValue(1).notEquals(" ")))                                                                                                    //Natural: IF #DED-PYMNT-TABLE ( 1 ) NE ' '
        {
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                            //Natural: IF #WS-HEADER-RECORD.CNTRCT-CRRNCY-CDE = '1'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                //Natural: IF #WS-HEADER-RECORD.PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(1).setValue(DbsUtil.compress("N", ldaFcpl803l.getPnd_Fcpl803l_Pnd_Federal_Ledgend().getValue(2))); //Natural: COMPRESS 'N' #FEDERAL-LEDGEND ( 2 ) INTO #DED-LEDGEND-TABLE ( 1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(1).setValue("F = Taxes Withheld");                                                       //Natural: ASSIGN #DED-LEDGEND-TABLE ( 1 ) := 'F = Taxes Withheld'
                    if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57"))) //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57'
                    {
                        ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(1).setValue(DbsUtil.compress("F", ldaFcpl803l.getPnd_Fcpl803l_Pnd_Federal_Ledgend().getValue(1))); //Natural: COMPRESS 'F' #FEDERAL-LEDGEND ( 1 ) INTO #DED-LEDGEND-TABLE ( 1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                            //Natural: IF CNTRCT-CRRNCY-CDE = '2'
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86'
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(1).setValue("C = Canadian Tax Withheld");                                                //Natural: ASSIGN #DED-LEDGEND-TABLE ( 1 ) := 'C = Canadian Tax Withheld'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(1).setValue("C = Can. Non-Resident Tax Withheld");                                       //Natural: ASSIGN #DED-LEDGEND-TABLE ( 1 ) := 'C = Can. Non-Resident Tax Withheld'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  **************************************** STATE
        if (condition(pnd_Ded_Pymnt_Table.getValue(2).notEquals(" ")))                                                                                                    //Natural: IF #DED-PYMNT-TABLE ( 2 ) NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATE-LEDGEND
            sub_Format_State_Ledgend();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J #DED-DISP-ARRAY ( * )
        pnd_Ws_Pnd_Ded_Disp_Array.getValue("*").reset();
        FOR18:                                                                                                                                                            //Natural: FOR #I = 1 TO 15
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(15)); pnd_I.nadd(1))
        {
            if (condition(pnd_Ded_Pymnt_Table.getValue(pnd_I).notEquals(" ")))                                                                                            //Natural: IF #DED-PYMNT-TABLE ( #I ) NE ' '
            {
                if (condition(pnd_J.notEquals(10)))                                                                                                                       //Natural: IF #J NE 10
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_J.equals(10) && pnd_Pymnt_Ded_Pnd.greater(10)))                                                                                         //Natural: IF #J = 10 AND #PYMNT-DED-# GT 10
                {
                    setValueToSubstring(ldaFcpl803l.getPnd_Fcpl803l_Pnd_More_Than_10_Ded(),pnd_Ws_Pnd_Ded_Disp_Array.getValue(10),3,34);                                  //Natural: MOVE #MORE-THAN-10-DED TO SUBSTR ( #DED-DISP-ARRAY ( 10 ) ,3,34 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    short decideConditionsMet3399 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #I;//Natural: VALUE 1:13
                    if (condition(((pnd_I.greaterOrEqual(1) && pnd_I.lessOrEqual(13)))))
                    {
                        decideConditionsMet3399++;
                        if (condition(pnd_I.equals(1) || pnd_I.equals(2) || pnd_I.equals(3)))                                                                             //Natural: IF #I = 1 OR = 2 OR = 3
                        {
                            setValueToSubstring(ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(pnd_I),pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_J),            //Natural: MOVE #DED-LEDGEND-TABLE ( #I ) TO SUBSTR ( #DED-DISP-ARRAY ( #J ) ,1,36 )
                                1,36);
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ws_Legend2.setValue(pnd_Pymnt_Ded3.getValue(pnd_I), MoveOption.LeftJustified);                                                            //Natural: MOVE LEFT #PYMNT-DED3 ( #I ) TO #WS-LEGEND2
                            setValueToSubstring(pnd_Ws_Legend2_Pnd_Ws_Legend2_A,pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_J),1,1);                                           //Natural: MOVE #WS-LEGEND2-A TO SUBSTR ( #DED-DISP-ARRAY ( #J ) ,1,1 )
                            setValueToSubstring(ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(pnd_I),pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_J),            //Natural: MOVE #DED-LEDGEND-TABLE ( #I ) TO SUBSTR ( #DED-DISP-ARRAY ( #J ) ,3,34 )
                                3,34);
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_I3.setValue(1);                                                                                                                                               //Natural: ASSIGN #I3 := 1
        FOR19:                                                                                                                                                            //Natural: FOR #I = 1 TO 9 STEP 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            ldaFcpl961.getCommon_Xml_Taxdescription_Data().getValue(pnd_I3).setValue(pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_I));                                          //Natural: MOVE #DED-DISP-ARRAY ( #I ) TO TAXDESCRIPTION-DATA ( #I3 )
            setValueToSubstring(pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_I.getDec().add(1)),ldaFcpl961.getCommon_Xml_Taxdescription_Data().getValue(pnd_I3),                //Natural: MOVE #DED-DISP-ARRAY ( #I+1 ) TO SUBSTR ( TAXDESCRIPTION-DATA ( #I3 ) ,41,36 )
                41,36);
            if (condition(pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_I).equals(" ") || pnd_Ws_Pnd_Ded_Disp_Array.getValue(pnd_I.getDec().add(1)).equals(" ")))                //Natural: IF #DED-DISP-ARRAY ( #I ) = ' ' OR #DED-DISP-ARRAY ( #I+1 ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_I3.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I3
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  GET-STATE-LINE
    }
    private void sub_Format_State_Ledgend() throws Exception                                                                                                              //Natural: FORMAT-STATE-LEDGEND
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("1")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '1'
        {
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))                                                                             //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '97'
            {
                ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(2).setValue("S = Taxes Withheld");                                                           //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := 'S = Taxes Withheld'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11))      //Natural: IF ( #WS-HEADER-RECORD.ANNT-CTZNSHP-CDE = 01 OR = 11 ) OR ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' )
                    || (pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))))
                {
                                                                                                                                                                          //Natural: PERFORM GET-STATE
                    sub_Get_State();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(((! ((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(1) || pdaFcpa700.getPnd_Ws_Header_Record_Annt_Ctznshp_Cde().equals(11)))  //Natural: IF NOT ( #WS-HEADER-RECORD.ANNT-CTZNSHP-CDE = 01 OR = 11 ) AND NOT ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '01' THRU '57' ) AND NOT #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '97'
                        && ! ((pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("01") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("57")))) 
                        && ! (pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("97")))))
                    {
                        ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(2).setValue("N = Non-Resident Alien Tax Withheld");                                  //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := 'N = Non-Resident Alien Tax Withheld'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde().equals("2")))                                                                                //Natural: IF CNTRCT-CRRNCY-CDE = '2'
        {
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().greaterOrEqual("74") && pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().lessOrEqual("86"))) //Natural: IF ( #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '74' THRU '86' )
            {
                                                                                                                                                                          //Natural: PERFORM GET-STATE
                sub_Get_State();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde().equals("96")))                                                                         //Natural: IF #WS-HEADER-RECORD.ANNT-RSDNCY-CDE = '96'
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(2).setValue("C = Taxes Withheld");                                                       //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := 'C = Taxes Withheld'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(2).setValue("C = Can. Non-Resident Tax Withheld");                                       //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := 'C = Can. Non-Resident Tax Withheld'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FORMAT-STATE-LEDGEND
    }
    private void sub_Get_State() throws Exception                                                                                                                         //Natural: GET-STATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(19);                                                                                                              //Natural: ASSIGN TBLDCODA.#TABLE-ID := 19
        pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde());                                                                //Natural: ASSIGN TBLDCODA.#CODE := #WS-HEADER-RECORD.ANNT-RSDNCY-CDE
        DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pdaFcppda_M.getMsg_Info_Sub());                                              //Natural: CALLNAT 'TBLDCOD' USING TBLDCODA MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcppda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 1 ) = ' '
        {
            ldaFcpl893l.getPnd_Fcpl893l_Pnd_State_Table_Top().nadd(1);                                                                                                    //Natural: ASSIGN #STATE-TABLE-TOP := #STATE-TABLE-TOP + 1
            pnd_Ws_Pnd_State_Idx.setValue(ldaFcpl893l.getPnd_Fcpl893l_Pnd_State_Table_Top());                                                                             //Natural: ASSIGN #STATE-IDX := #STATE-TABLE-TOP
            ldaFcpl893l.getPnd_Fcpl893l_Pnd_State_Cde_Table().getValue(pnd_Ws_Pnd_State_Idx).setValue(pdaFcpa700.getPnd_Ws_Header_Record_Annt_Rsdncy_Cde());              //Natural: ASSIGN #STATE-CDE-TABLE ( #STATE-IDX ) := #WS-HEADER-RECORD.ANNT-RSDNCY-CDE
            //*  COMPRESS '=' TBLDCODA.#TARGET #STATE-LEDGEND(3)
            ldaFcpl893l.getPnd_Fcpl893l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx).setValue(DbsUtil.compress("S =", pdaTbldcoda.getTbldcoda_Pnd_Target(),           //Natural: COMPRESS 'S =' TBLDCODA.#TARGET 'Tax Withheld' INTO #STATE-TABLE ( #STATE-IDX )
                "Tax Withheld"));
            ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(2).setValue(ldaFcpl893l.getPnd_Fcpl893l_Pnd_State_Table().getValue(pnd_Ws_Pnd_State_Idx));       //Natural: ASSIGN #DED-LEDGEND-TABLE ( 2 ) := #STATE-TABLE ( #STATE-IDX )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(2).setValue(DbsUtil.compress("S =", ldaFcpl803l.getPnd_Fcpl803l_Pnd_State_Ledgend().getValue(2))); //Natural: COMPRESS 'S =' #STATE-LEDGEND ( 2 ) INTO #DED-LEDGEND-TABLE ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Ded_Table() throws Exception                                                                                                                    //Natural: LOAD-DED-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl893l.getPnd_Fcpl893l_Pnd_Deductions_Loaded().setValue(true);                                                                                               //Natural: ASSIGN #DEDUCTIONS-LOADED := TRUE
        ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(3).setValue(DbsUtil.compress("L", ldaFcpl803l.getPnd_Fcpl803l_Pnd_Local_Ledgend()));                 //Natural: COMPRESS 'L' #LOCAL-LEDGEND INTO #DED-LEDGEND-TABLE ( 3 )
        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(21);                                                                                                              //Natural: ASSIGN TBLDCODA.#TABLE-ID := 21
        FOR20:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(1,pnd_I).greaterOrEqual(1) && pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(1,      //Natural: IF #WS-OCCURS.PYMNT-DED-CDE ( 1,#I ) EQ 1 THRU 10
                pnd_I).lessOrEqual(10)))
            {
                //*  MOVE EDITED #DED-PYMNT-TABLE-N(#I+ 3) (EM=999) TO TBLDCODA.#CODE
                pdaTbldcoda.getTbldcoda_Pnd_Code().setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(1,pnd_I),new ReportEditMask("999"));               //Natural: MOVE EDITED #WS-OCCURS.PYMNT-DED-CDE ( 1,#I ) ( EM = 999 ) TO TBLDCODA.#CODE
                DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pdaFcppda_M.getMsg_Info_Sub());                                      //Natural: CALLNAT 'TBLDCOD' USING TBLDCODA MSG-INFO-SUB
                if (condition(Global.isEscape())) return;
                if (condition(pdaFcppda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                    //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 1 ) = ' '
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(pnd_I.getDec().add(3)).setValue("=");                                                    //Natural: MOVE '=' TO #DED-LEDGEND-TABLE ( #I+3 )
                    setValueToSubstring(pdaTbldcoda.getTbldcoda_Pnd_Target(),ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(pnd_I.getDec().add(3)),         //Natural: MOVE TBLDCODA.#TARGET TO SUBSTR ( #DED-LEDGEND-TABLE ( #I+3 ) ,3,31 )
                        3,31);
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl893l.getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table().getValue(pnd_I.getDec().add(3)).setValue(ldaFcpl803l.getPnd_Fcpl803l_Pnd_Unknown_Ledgend());      //Natural: MOVE #UNKNOWN-LEDGEND TO #DED-LEDGEND-TABLE ( #I+3 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Addr_Record() throws Exception                                                                                                                   //Natural: GET-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Addr_Pnd_Cntrct_Ppcn_Nbr.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr());                                                                      //Natural: ASSIGN #CNTRCT-PPCN-NBR := #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
        pnd_Addr_Pnd_Cntrct_Payee_Cde.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Payee_Cde());                                                                    //Natural: ASSIGN #CNTRCT-PAYEE-CDE := #WS-HEADER-RECORD.CNTRCT-PAYEE-CDE
        pnd_Addr_Pnd_Cntrct_Orgn_Cde.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde());                                                                      //Natural: ASSIGN #CNTRCT-ORGN-CDE := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        pnd_Addr_Pnd_Cntrct_Invrse_Dte.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Invrse_Dte());                                                                  //Natural: ASSIGN #CNTRCT-INVRSE-DTE := #WS-HEADER-RECORD.CNTRCT-INVRSE-DTE
        pnd_Addr_Pnd_Pymnt_Prcss_Seq_Nbr.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num());                                                              //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #WS-HEADER-RECORD.PYMNT-PRCSS-SEQ-NUM
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("AP")))                                                                                 //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'AP'
        {
            pnd_Addr_Pnd_Cntrct_Ppcn_Nbr.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Cmbn_Nbr());                                                                  //Natural: ASSIGN #CNTRCT-PPCN-NBR := #WS-HEADER-RECORD.CNTRCT-CMBN-NBR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Addr_Isn.setValue(0);                                                                                                                                         //Natural: ASSIGN #ADDR-ISN := 0
        vw_addr_View.startDatabaseFind                                                                                                                                    //Natural: FIND ( 1 ) ADDR-VIEW WITH PPCN-PAYEE-ORGN-INVRS-SEQ = #ADDR
        (
        "FN1",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", "=", pnd_Addr, WcType.WITH) },
        1
        );
        FN1:
        while (condition(vw_addr_View.readNextRow("FN1", true)))
        {
            vw_addr_View.setIfNotFoundControlFlag(false);
            if (condition(vw_addr_View.getAstCOUNTER().equals(0)))                                                                                                        //Natural: IF NO RECORDS FOUND
            {
                pnd_Addr_Isn.setValue(0);                                                                                                                                 //Natural: ASSIGN #ADDR-ISN := 0
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Addr_Isn.setValue(vw_addr_View.getAstISN("FN1"));                                                                                                         //Natural: ASSIGN #ADDR-ISN := *ISN ( FN1. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  GET-ADDR-RECORD
    }
    private void sub_Update_Dri() throws Exception                                                                                                                        //Natural: UPDATE-DRI
    {
        if (BLNatReinput.isReinput()) return;

        GT1:                                                                                                                                                              //Natural: GET ADDR-UPD #ADDR-ISN
        vw_addr_Upd.readByID(pnd_Addr_Isn.getLong(), "GT1");
        setValueToSubstring(pnd_Sve_Documentrequestid_Data.getSubstring(1,20),addr_Upd_Pymnt_Addr_Line1_Txt.getValue(10),1,20);                                           //Natural: MOVE SUBSTR ( #SVE-DOCUMENTREQUESTID-DATA,01,20 ) TO SUBSTR ( ADDR-UPD.PYMNT-ADDR-LINE1-TXT ( 10 ) ,1,20 )
        setValueToSubstring(pnd_Sve_Documentrequestid_Data.getSubstring(21,20),addr_Upd_Pymnt_Addr_Line2_Txt.getValue(10),1,20);                                          //Natural: MOVE SUBSTR ( #SVE-DOCUMENTREQUESTID-DATA,21,20 ) TO SUBSTR ( ADDR-UPD.PYMNT-ADDR-LINE2-TXT ( 10 ) ,1,20 )
        vw_addr_Upd.updateDBRow("GT1");                                                                                                                                   //Natural: UPDATE ( GT1. )
        //*  UPDATE-DRI
    }
    private void sub_Survivor_Benefits_Headings() throws Exception                                                                                                        //Natural: SURVIVOR-BENEFITS-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("PP")))                                                                                 //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'PP'
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Survivor Benefit Payment for:");                                                                       //Natural: ASSIGN PAYMENTLABEL-DATA := 'Survivor Benefit Payment for:'
            //*  LUMP SUM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Lump Sum Payment for:");                                                                               //Natural: ASSIGN PAYMENTLABEL-DATA := 'Lump Sum Payment for:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress(ldaFcpl961.getCommon_Xml_Paymentlabel_Data(), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1),  //Natural: COMPRESS PAYMENTLABEL-DATA #WS-NAME-N-ADDRESS.PH-FIRST-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-LAST-NAME ( 1 ) INTO PAYMENTLABEL-DATA
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        ldaFcpl961.getCommon_Xml_Decedentname_Data().setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Deceased_Nme().getValue(1));                                      //Natural: ASSIGN DECEDENTNAME-DATA := #WS-NAME-N-ADDRESS.PYMNT-DECEASED-NME ( 1 )
        //*  SURVIVOR-BENEFITS-HEADINGS
    }
    private void sub_Tpa_Headings() throws Exception                                                                                                                      //Natural: TPA-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Periodic Payment for:");                                                                               //Natural: ASSIGN PAYMENTLABEL-DATA := 'Periodic Payment for:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Transfer Payout Annuity Payment for");                                                                 //Natural: ASSIGN PAYMENTLABEL-DATA := 'Transfer Payout Annuity Payment for'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress(ldaFcpl961.getCommon_Xml_Paymentlabel_Data(), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1),  //Natural: COMPRESS PAYMENTLABEL-DATA #WS-NAME-N-ADDRESS.PH-FIRST-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-LAST-NAME ( 1 ) INTO PAYMENTLABEL-DATA
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        //*  IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F' /* FUTURE PAYMENT
        //*    RESET ANNUITYSTARTDESC-DATA
        //*  ELSE
        //*    MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE  (EM=MM/DD/YY)
        //*      TO ANNUITYSTARTDATE-DATA
        //*    COMPRESS
        //*      'Annuity Starting Date:'
        //*      ANNUITYSTARTDATE-DATA
        //*      INTO ANNUITYSTARTLABEL /* COMBINE WITH PAYMENT DATE
        //*  TPA-HEADINGS
    }
    private void sub_Lump_Sum_Death_Headings() throws Exception                                                                                                           //Natural: LUMP-SUM-DEATH-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Periodic Payment For:");                                                                               //Natural: ASSIGN PAYMENTLABEL-DATA := 'Periodic Payment For:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Lump Sum Payment for:");                                                                               //Natural: ASSIGN PAYMENTLABEL-DATA := 'Lump Sum Payment for:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress(ldaFcpl961.getCommon_Xml_Paymentlabel_Data(), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1),  //Natural: COMPRESS PAYMENTLABEL-DATA #WS-NAME-N-ADDRESS.PH-FIRST-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-LAST-NAME ( 1 ) INTO PAYMENTLABEL-DATA
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        ldaFcpl961.getCommon_Xml_Decedentname_Data().setValue(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Deceased_Nme().getValue(1));                                      //Natural: ASSIGN DECEDENTNAME-DATA := #WS-NAME-N-ADDRESS.PYMNT-DECEASED-NME ( 1 )
        //*  LUMP-SUM-DEATH-HEADINGS
    }
    private void sub_Maturity_Headings() throws Exception                                                                                                                 //Natural: MATURITY-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet3563 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-MODE-CDE;//Natural: VALUE 100
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().equals(100))))
        {
            decideConditionsMet3563++;
            pnd_Ws_Frequency.setValue("Monthly");                                                                                                                         //Natural: ASSIGN #WS-FREQUENCY := 'Monthly'
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(601) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(603)))))
        {
            decideConditionsMet3563++;
            pnd_Ws_Frequency.setValue("Quarterly");                                                                                                                       //Natural: ASSIGN #WS-FREQUENCY := 'Quarterly'
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(701) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(706)))))
        {
            decideConditionsMet3563++;
            pnd_Ws_Frequency.setValue("Semi-Annual");                                                                                                                     //Natural: ASSIGN #WS-FREQUENCY := 'Semi-Annual'
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(801) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(812)))))
        {
            decideConditionsMet3563++;
            pnd_Ws_Frequency.setValue("Annual");                                                                                                                          //Natural: ASSIGN #WS-FREQUENCY := 'Annual'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Ws_Frequency.setValue("Unknown");                                                                                                                         //Natural: ASSIGN #WS-FREQUENCY := 'Unknown'
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Periodic Payment For:");                                                                               //Natural: ASSIGN PAYMENTLABEL-DATA := 'Periodic Payment For:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress("Initial", pnd_Ws_Frequency, "payment for:"));                                         //Natural: COMPRESS 'Initial' #WS-FREQUENCY 'payment for:' INTO PAYMENTLABEL-DATA
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress(ldaFcpl961.getCommon_Xml_Paymentlabel_Data(), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1),  //Natural: COMPRESS PAYMENTLABEL-DATA #WS-NAME-N-ADDRESS.PH-FIRST-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-LAST-NAME ( 1 ) INTO PAYMENTLABEL-DATA
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        //*  MATURITY-HEADINGS
    }
    private void sub_Mdo_Headings() throws Exception                                                                                                                      //Natural: MDO-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Periodic Payment for:");                                                                               //Natural: ASSIGN PAYMENTLABEL-DATA := 'Periodic Payment for:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Initial Minimum Distribution payment for:");                                                           //Natural: ASSIGN PAYMENTLABEL-DATA := 'Initial Minimum Distribution payment for:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress(ldaFcpl961.getCommon_Xml_Paymentlabel_Data(), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1),  //Natural: COMPRESS PAYMENTLABEL-DATA #WS-NAME-N-ADDRESS.PH-FIRST-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-LAST-NAME ( 1 ) INTO PAYMENTLABEL-DATA
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        //*  MDO-HEADINGS
    }
    private void sub_Recurring_Mdo_Headings() throws Exception                                                                                                            //Natural: RECURRING-MDO-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue("Recurring Minimum Distribution payment for:");                                                             //Natural: ASSIGN PAYMENTLABEL-DATA := 'Recurring Minimum Distribution payment for:'
        //*  * ..... COMPOSE THE NAME FOR PRINTING
        ldaFcpl961.getCommon_Xml_Paymentlabel_Data().setValue(DbsUtil.compress(ldaFcpl961.getCommon_Xml_Paymentlabel_Data(), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1),  //Natural: COMPRESS PAYMENTLABEL-DATA #WS-NAME-N-ADDRESS.PH-FIRST-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME ( 1 ) #WS-NAME-N-ADDRESS.PH-LAST-NAME ( 1 ) INTO PAYMENTLABEL-DATA
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        //*  USE   'Effective Date:'  INSTEAD OF "Annuity Starting Date:"
        //*  RECURRING-MDO-HEADINGS
    }
    private void sub_Get_Canadian_Tax() throws Exception                                                                                                                  //Natural: GET-CANADIAN-TAX
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind().equals("C") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind().equals("C"))) //Natural: IF #WS-HEADER-RECORD.CNTRCT-PYMNT-TYPE-IND = 'C' AND #WS-HEADER-RECORD.CNTRCT-STTLMNT-TYPE-IND = 'C'
        {
            pnd_Pymnt_S_Cntrct_Ppcn_Nbr.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr());                                                                   //Natural: ASSIGN #PYMNT-S.CNTRCT-PPCN-NBR := #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
            pnd_Pymnt_S_Cntrct_Invrse_Dte.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Invrse_Dte());                                                               //Natural: ASSIGN #PYMNT-S.CNTRCT-INVRSE-DTE := #WS-HEADER-RECORD.CNTRCT-INVRSE-DTE
            pnd_Pymnt_S_Cntrct_Orgn_Cde.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde());                                                                   //Natural: ASSIGN #PYMNT-S.CNTRCT-ORGN-CDE := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
            pnd_Pymnt_S_Pymnt_Prcss_Seq_Num.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num());                                                           //Natural: ASSIGN #PYMNT-S.PYMNT-PRCSS-SEQ-NUM := #WS-HEADER-RECORD.PYMNT-PRCSS-SEQ-NUM
            vw_can_Pymnt.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) CAN-PYMNT WITH PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE
            (
            "R1",
            new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
            new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
            1
            );
            R1:
            while (condition(vw_can_Pymnt.readNextRow("R1")))
            {
                getReports().write(0, "*********** CANADIAN CONVERTED *************");                                                                                    //Natural: WRITE '*********** CANADIAN CONVERTED *************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue(1,":",40),"=",can_Pymnt_Cntrct_Can_Tax_Amt);                                            //Natural: WRITE '=' INV-ACCT-CAN-TAX-AMT ( 1:40 ) '=' CNTRCT-CAN-TAX-AMT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr().equals(can_Pymnt_Cntrct_Ppcn_Nbr) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Invrse_Dte().equals(can_Pymnt_Cntrct_Invrse_Dte)  //Natural: IF #WS-HEADER-RECORD.CNTRCT-PPCN-NBR = CAN-PYMNT.CNTRCT-PPCN-NBR AND #WS-HEADER-RECORD.CNTRCT-INVRSE-DTE = CAN-PYMNT.CNTRCT-INVRSE-DTE AND #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = CAN-PYMNT.CNTRCT-ORGN-CDE
                    && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals(can_Pymnt_Cntrct_Orgn_Cde)))
                {
                    //*     ADD  R1.INV-ACCT-CAN-TAX-AMT (1:C-INV-ACCT) TO #CAN-TAX-AMT
                    pnd_Inv_Acct_Can_Tax_Amt.getValue("*").setValue(can_Pymnt_Inv_Acct_Can_Tax_Amt.getValue("*"));                                                        //Natural: MOVE CAN-PYMNT.INV-ACCT-CAN-TAX-AMT ( * ) TO #INV-ACCT-CAN-TAX-AMT ( * )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CANADIAN-TAX
    }
    private void sub_Tiaa_Account_Move_Settle() throws Exception                                                                                                          //Natural: TIAA-ACCOUNT-MOVE-SETTLE
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl961.getCommon_Xml_Tiaacrefind_Data().setValue("T");                                                                                                        //Natural: ASSIGN TIAACREFIND-DATA := 'T'
        //*  IF #WS-OCCURS.INV-ACCT-SETTL-AMT (#I-FUND) NE 0
        ldaFcpl961.getCommon_Xml_Contractual_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_I_Fund),new ReportEditMask("ZZZZZZZZ9.99")); //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-SETTL-AMT ( #I-FUND ) ( EM = ZZZZZZZZ9.99 ) TO CONTRACTUAL-DATA
        ldaFcpl961.getCommon_Xml_Contractual_Data().setValue(ldaFcpl961.getCommon_Xml_Contractual_Data(), MoveOption.LeftJustified);                                      //Natural: MOVE LEFT CONTRACTUAL-DATA TO CONTRACTUAL-DATA
        //*  END-IF
        //*  IF #WS-OCCURS.INV-ACCT-DVDND-AMT(#I-FUND)         NE 0
        ldaFcpl961.getCommon_Xml_Dividend_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_I_Fund),new ReportEditMask("ZZZZZZZZ9.99")); //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-DVDND-AMT ( #I-FUND ) ( EM = ZZZZZZZZ9.99 ) TO DIVIDEND-DATA
        ldaFcpl961.getCommon_Xml_Dividend_Data().setValue(ldaFcpl961.getCommon_Xml_Dividend_Data(), MoveOption.LeftJustified);                                            //Natural: MOVE LEFT DIVIDEND-DATA TO DIVIDEND-DATA
        //*  END-IF
        //*  TIAA-ACCOUNT-MOVE-SETTLE
    }
    private void sub_Cref_Account_Move_Unit() throws Exception                                                                                                            //Natural: CREF-ACCOUNT-MOVE-UNIT
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl961.getCommon_Xml_Tiaacrefind_Data().setValue("C");                                                                                                        //Natural: ASSIGN TIAACREFIND-DATA := 'C'
        //*  IF #WS-OCCURS.INV-ACCT-UNIT-VALUE(#I-FUND) NE 0.00
        ldaFcpl961.getCommon_Xml_Unitvalue_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Unit_Value().getValue(pnd_I_Fund),new ReportEditMask("ZZZZ9.9999")); //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #I-FUND ) ( EM = ZZZZ9.9999 ) TO UNITVALUE-DATA
        ldaFcpl961.getCommon_Xml_Unitvalue_Data().setValue(ldaFcpl961.getCommon_Xml_Unitvalue_Data(), MoveOption.LeftJustified);                                          //Natural: MOVE LEFT UNITVALUE-DATA TO UNITVALUE-DATA
        //*  END-IF
        //*  IF #WS-OCCURS.INV-ACCT-UNIT-QTY(#I-FUND) NE 0.00
        ldaFcpl961.getCommon_Xml_Unit_Data().setValueEdited(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Unit_Qty().getValue(pnd_I_Fund),new ReportEditMask("ZZZZZ9.999"));       //Natural: MOVE EDITED #WS-OCCURS.INV-ACCT-UNIT-QTY ( #I-FUND ) ( EM = ZZZZZ9.999 ) TO UNIT-DATA
        ldaFcpl961.getCommon_Xml_Unit_Data().setValue(ldaFcpl961.getCommon_Xml_Unit_Data(), MoveOption.LeftJustified);                                                    //Natural: MOVE LEFT UNIT-DATA TO UNIT-DATA
        //*  END-IF
        //*  CREF-ACCOUNT-MOVE-UNIT
    }
    private void sub_Obtain_Fund_Description() throws Exception                                                                                                           //Natural: OBTAIN-FUND-DESCRIPTION
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GET THE DESCRIPTION FOR A CREF FUND. I.E. "Money Market", "Stock", ETC
        //*  ----------------------------------------------------------------------
        //*  ...... OBTAIN INVESTMENT ACCOUNT DESCRIPTION FROM TABLE
        //* *ASSIGN #PDA-INV-ACCT = #WS-OCCURS.INV-ACCT-CDE (1)
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1));                                                //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = #WS-OCCURS.INV-ACCT-CDE ( 1 )
        //* *CALLNAT 'FCPN199' #PDA-INV-ACCT #PDA-INV-ACCT-INFO
        DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                        //Natural: CALLNAT 'FCPN199A' #FUND-PDA
        if (condition(Global.isEscape())) return;
        //*  ...... INDICATE WHETHER ITS A TIAA OR CREF INVESTMENT ACCOUNT
        pnd_Ws_Fund_Description.setValue(DbsUtil.compress(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Hrd_Line_1(), pdaFcpa199a.getPnd_Fund_Pda_Pnd_Hrd_Line_2()));                   //Natural: COMPRESS #FUND-PDA.#HRD-LINE-1 #FUND-PDA.#HRD-LINE-2 INTO #WS-FUND-DESCRIPTION
        //*  OBTAIN-FUND-DESCRIPTION
    }
    private void sub_C9100_Get_Special_Handling() throws Exception                                                                                                        //Natural: C9100-GET-SPECIAL-HANDLING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Lp_Holdcode.setValue(false);                                                                                                                                  //Natural: ASSIGN #LP-HOLDCODE := FALSE
        vw_rt2.startDatabaseRead                                                                                                                                          //Natural: READ RT2 WITH RT-SUPER1 STARTING FROM #RT2-SUPER1
        (
        "RDRT",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt2_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        RDRT:
        while (condition(vw_rt2.readNextRow("RDRT")))
        {
            if (condition(rt2_Rt_Table_Id.notEquals(pnd_Rt2_Super1_Pnd_Rt2_Table_Id)))                                                                                    //Natural: IF RT2.RT-TABLE-ID NE #RT2-TABLE-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rt2_Rt_Short_Key.equals(pnd_Ws_Holdcode)))                                                                                                      //Natural: IF RT2.RT-SHORT-KEY = #WS-HOLDCODE
            {
                pnd_Lp_Holdcode.setValue(true);                                                                                                                           //Natural: ASSIGN #LP-HOLDCODE := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
    }
}
