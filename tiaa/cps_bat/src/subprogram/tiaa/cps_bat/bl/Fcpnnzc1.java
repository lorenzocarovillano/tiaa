/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:22 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnnzc1
************************************************************
**        * FILE NAME            : Fcpnnzc1.java
**        * CLASS NAME           : Fcpnnzc1
**        * INSTANCE NAME        : Fcpnnzc1
************************************************************
************************************************************************
* PROGRAM  : FCPNNZC1
* SYSTEM   : CPS
* TITLE    : ACCUMULATE CONTROL DATA.
* CREATED  : 08/01/96
* HISTORY
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnnzc1 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpanzc1 pdaFcpanzc1;
    private PdaFcpanzc2 pdaFcpanzc2;
    private PdaFcpanzc3 pdaFcpanzc3;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Accum_Occur;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpanzc1 = new PdaFcpanzc1(parameters);
        pdaFcpanzc2 = new PdaFcpanzc2(parameters);
        pdaFcpanzc3 = new PdaFcpanzc3(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Accum_Occur = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accum_Occur", "#ACCUM-OCCUR", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnnzc1() throws Exception
    {
        super("Fcpnnzc1");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Ws_Pnd_Accum_Occur.setValue(pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_1());                                                                                 //Natural: ASSIGN #ACCUM-OCCUR := #ACCUM-OCCUR-1
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
        sub_Accum_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Accum_Occur.setValue(pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2());                                                                                 //Natural: ASSIGN #ACCUM-OCCUR := #ACCUM-OCCUR-2
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
        sub_Accum_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pnd_Ws_Pnd_Accum_Occur.greaterOrEqual(1) && pnd_Ws_Pnd_Accum_Occur.lessOrEqual(50)))                                                                //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet62 = 0;                                                                                                                                  //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPANZC2.#NEW-PYMNT-IND
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(1);                                                                         //Natural: ADD 1 TO #FCPANZC2.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Settl_Amt().getValue(1,                //Natural: ADD #FCPANZC3.INV-ACCT-SETTL-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Dvdnd_Amt().getValue(1,                //Natural: ADD #FCPANZC3.INV-ACCT-DVDND-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dvdnd_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Dvdnd_Amt().getValue(1,                //Natural: ADD #FCPANZC3.INV-ACCT-DVDND-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dci_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Dci_Amt().getValue(1,                    //Natural: ADD #FCPANZC3.INV-ACCT-DCI-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dpi_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Dpi_Amt().getValue(1,                    //Natural: ADD #FCPANZC3.INV-ACCT-DPI-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Net_Pymnt_Amt().getValue(1,        //Natural: ADD #FCPANZC3.INV-ACCT-NET-PYMNT-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Fdrl_Tax_Amt().getValue(1,          //Natural: ADD #FCPANZC3.INV-ACCT-FDRL-TAX-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_State_Tax_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_State_Tax_Amt().getValue(1,        //Natural: ADD #FCPANZC3.INV-ACCT-STATE-TAX-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPANZC2.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet62++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Local_Tax_Amt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(pdaFcpanzc3.getPnd_Fcpanzc3_Inv_Acct_Local_Tax_Amt().getValue(1,        //Natural: ADD #FCPANZC3.INV-ACCT-LOCAL-TAX-AMT ( 1:#MAX-FUND ) TO #FCPANZC2.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpanzc3.getPnd_Fcpanzc3_Pnd_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet62 > 0))
        {
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_Accum_Occur).nadd(1);                                                                           //Natural: ADD 1 TO #FCPANZC2.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet62 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
