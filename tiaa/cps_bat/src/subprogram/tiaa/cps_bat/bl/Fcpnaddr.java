/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:26:56 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnaddr
************************************************************
**        * FILE NAME            : Fcpnaddr.java
**        * CLASS NAME           : Fcpnaddr
**        * INSTANCE NAME        : Fcpnaddr
************************************************************
************************************************************************
* PROGRAM  : FCPNADDR
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* CREATED  : 11/29/95
* FUNCTION : FCP-CONS-ADDR FILE LOOKUP.
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnaddr extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpaaddr pdaFcpaaddr;
    private LdaFcpladdr ldaFcpladdr;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Addr_S;
    private DbsField pnd_Addr_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Addr_S_Cntrct_Payee_Cde;
    private DbsField pnd_Addr_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Addr_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Addr_S_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Addr_S__R_Field_1;
    private DbsField pnd_Addr_S_Pnd_Addr_Superde_Start;
    private DbsField pnd_Addr_Superde_End;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());

        // parameters
        parameters = new DbsRecord();
        pdaFcpaaddr = new PdaFcpaaddr(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Addr_S = localVariables.newGroupInRecord("pnd_Addr_S", "#ADDR-S");
        pnd_Addr_S_Cntrct_Ppcn_Nbr = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Addr_S_Cntrct_Payee_Cde = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Addr_S_Cntrct_Orgn_Cde = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Addr_S_Cntrct_Invrse_Dte = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Addr_S_Pymnt_Prcss_Seq_Nbr = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7);

        pnd_Addr_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Addr_S__R_Field_1", "REDEFINE", pnd_Addr_S);
        pnd_Addr_S_Pnd_Addr_Superde_Start = pnd_Addr_S__R_Field_1.newFieldInGroup("pnd_Addr_S_Pnd_Addr_Superde_Start", "#ADDR-SUPERDE-START", FieldType.STRING, 
            31);
        pnd_Addr_Superde_End = localVariables.newFieldInRecord("pnd_Addr_Superde_End", "#ADDR-SUPERDE-END", FieldType.STRING, 31);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpladdr.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnaddr() throws Exception
    {
        super("Fcpnaddr");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Addr_S.setValuesByName(pdaFcpaaddr.getAddr());                                                                                                                //Natural: MOVE BY NAME ADDR TO #ADDR-S
        if (condition(pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().equals("AP") || pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().equals("IA")))                                          //Natural: IF ADDR.CNTRCT-ORGN-CDE = 'AP' OR = 'IA'
        {
            pnd_Addr_S_Cntrct_Ppcn_Nbr.setValue(pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().getSubstring(1,10));                                                    //Natural: MOVE SUBSTRING ( ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR,1,10 ) TO #ADDR-S.CNTRCT-PPCN-NBR
            pnd_Addr_S_Cntrct_Payee_Cde.setValue(pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().getSubstring(11,4));                                                   //Natural: MOVE SUBSTRING ( ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR,11,4 ) TO #ADDR-S.CNTRCT-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Addr_Superde_End.setValue(pnd_Addr_S_Pnd_Addr_Superde_Start);                                                                                                 //Natural: MOVE #ADDR-SUPERDE-START TO #ADDR-SUPERDE-END
        setValueToSubstring("H'FF'",pnd_Addr_Superde_End,17,1);                                                                                                           //Natural: MOVE H'FF' TO SUBSTRING ( #ADDR-SUPERDE-END,17,1 )
        if (condition(((pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().equals("AP") || pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().equals("IA")) && pdaFcpaaddr.getAddr_Work_Fields_Current_Addr().getBoolean()))) //Natural: IF ADDR.CNTRCT-ORGN-CDE = 'AP' OR = 'IA' AND CURRENT-ADDR
        {
            setValueToSubstring("H'00'",pnd_Addr_S_Pnd_Addr_Superde_Start,17,1);                                                                                          //Natural: MOVE H'00' TO SUBSTRING ( #ADDR-SUPERDE-START,17,1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ = #ADDR-SUPERDE-START THRU #ADDR-SUPERDE-END
        (
        "RADDR",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_S_Pnd_Addr_Superde_Start, "And", WcType.BY) ,
        new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", "<=", pnd_Addr_Superde_End, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        RADDR:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("RADDR")))
        {
            pdaFcpaaddr.getAddr_Work_Fields_Addr_Isn().setValue(ldaFcpladdr.getVw_fcp_Cons_Addr().getAstISN("RADDR"));                                                    //Natural: MOVE *ISN TO ADDR-ISN
            pdaFcpaaddr.getAddr().setValuesByName(ldaFcpladdr.getVw_fcp_Cons_Addr());                                                                                     //Natural: MOVE BY NAME FCP-CONS-ADDR TO ADDR
            pdaFcpaaddr.getAddr_C_Pymnt_Nme_And_Addr_Grp().setValue(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp());                                     //Natural: MOVE C*PYMNT-NME-AND-ADDR-GRP TO C-PYMNT-NME-AND-ADDR-GRP
            pdaFcpaaddr.getAddr_C_Pymnt_Addr_Chg_Grp().setValue(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Addr_Chg_Grp());                                             //Natural: MOVE C*PYMNT-ADDR-CHG-GRP TO C-PYMNT-ADDR-CHG-GRP
            pdaFcpaaddr.getAddr_Work_Fields_Addr_Return_Cde().reset();                                                                                                    //Natural: RESET ADDR-RETURN-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().getAstCOUNTER().equals(getZero())))                                                                               //Natural: IF *COUNTER ( RADDR. ) = 0
        {
            pdaFcpaaddr.getAddr_Work_Fields_Addr_Return_Cde().setValue(91);                                                                                               //Natural: MOVE 91 TO ADDR-RETURN-CDE
            if (condition(pdaFcpaaddr.getAddr_Work_Fields_Addr_Abend_Ind().getBoolean()))                                                                                 //Natural: IF ADDR-ABEND-IND
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"ADDRESS RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIAA PPCN#  :",pnd_Addr_S_Cntrct_Ppcn_Nbr,new  //Natural: WRITE '***' 25T 'ADDRESS RECORD IS NOT FOUND' 77T '***' / '***' 25T 'TIAA PPCN#  :' #ADDR-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'PAYEE       :' #ADDR-S.CNTRCT-PAYEE-CDE 77T '***' / '***' 25T 'ORIGIN      :' #ADDR-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'INVERSE DATE:' #ADDR-S.CNTRCT-INVRSE-DTE 77T '***' / '***' 25T 'SEQUENCE#   :' #ADDR-S.PYMNT-PRCSS-SEQ-NBR 77T '***'
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PAYEE       :",pnd_Addr_S_Cntrct_Payee_Cde,new TabSetting(77),"***",NEWLINE,"***",new 
                    TabSetting(25),"ORIGIN      :",pnd_Addr_S_Cntrct_Orgn_Cde,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"INVERSE DATE:",pnd_Addr_S_Cntrct_Invrse_Dte,new 
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"SEQUENCE#   :",pnd_Addr_S_Pymnt_Prcss_Seq_Nbr,new TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(pdaFcpaaddr.getAddr_Work_Fields_Addr_Return_Cde());  if (true) return;                                                                  //Natural: TERMINATE ADDR-RETURN-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
