/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:21:03 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn121
************************************************************
**        * FILE NAME            : Fcpn121.java
**        * CLASS NAME           : Fcpn121
**        * INSTANCE NAME        : Fcpn121
************************************************************
************************************************************************
* PROGRAM  : FCPN121
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS LEDGER DESCRIPTION
* CREATED  : 08/25/94
* FUNCTION : THIS PROGRAM GETS THE LEDGER ACCOUNT DESCRIPTION.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn121 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa121 pdaFcpa121;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_sttl_Isa;
    private DbsField sttl_Isa_Sttl_Isa_Delete_Ind;
    private DbsField sttl_Isa_Sttl_Isa_Key;
    private DbsField sttl_Isa_Sttl_Isa_Data;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Key;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Company;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Table_Type;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Account;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Table_Level;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Filler;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Table_Sequence;
    private DbsField pnd_Ws_Pnd_Lgr_Isa_Data;
    private DbsField pnd_Ws_Pnd_Idx;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_sttl_Isa = new DataAccessProgramView(new NameInfo("vw_sttl_Isa", "STTL-ISA"), "STTL_ISA", "STTL_ISA");
        sttl_Isa_Sttl_Isa_Delete_Ind = vw_sttl_Isa.getRecord().newFieldInGroup("sttl_Isa_Sttl_Isa_Delete_Ind", "STTL-ISA-DELETE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "STTL_ISA_DELETE_IND");
        sttl_Isa_Sttl_Isa_Key = vw_sttl_Isa.getRecord().newFieldInGroup("sttl_Isa_Sttl_Isa_Key", "STTL-ISA-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "STTL_ISA_KEY");
        sttl_Isa_Sttl_Isa_Data = vw_sttl_Isa.getRecord().newFieldInGroup("sttl_Isa_Sttl_Isa_Data", "STTL-ISA-DATA", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "STTL_ISA_DATA");
        registerRecord(vw_sttl_Isa);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Lgr_Isa_Key = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Key", "#LGR-ISA-KEY", FieldType.STRING, 40);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Lgr_Isa_Key);
        pnd_Ws_Pnd_Lgr_Isa_Company = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Company", "#LGR-ISA-COMPANY", FieldType.STRING, 5);
        pnd_Ws_Pnd_Lgr_Isa_Table_Type = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Table_Type", "#LGR-ISA-TABLE-TYPE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Lgr_Isa_Account = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Account", "#LGR-ISA-ACCOUNT", FieldType.STRING, 15);
        pnd_Ws_Pnd_Lgr_Isa_Table_Level = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Table_Level", "#LGR-ISA-TABLE-LEVEL", FieldType.STRING, 
            2);
        pnd_Ws_Pnd_Lgr_Isa_Filler = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Filler", "#LGR-ISA-FILLER", FieldType.STRING, 15);
        pnd_Ws_Pnd_Lgr_Isa_Table_Sequence = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Table_Sequence", "#LGR-ISA-TABLE-SEQUENCE", FieldType.STRING, 
            2);
        pnd_Ws_Pnd_Lgr_Isa_Data = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Lgr_Isa_Data", "#LGR-ISA-DATA", FieldType.STRING, 40);
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_sttl_Isa.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn121() throws Exception
    {
        super("Fcpn121");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Ws_Pnd_Lgr_Isa_Company.setValue(pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa());                                                                                    //Natural: ASSIGN #LGR-ISA-COMPANY := #FCPA121.INV-ACCT-ISA
        pnd_Ws_Pnd_Lgr_Isa_Table_Type.setValue("H");                                                                                                                      //Natural: ASSIGN #LGR-ISA-TABLE-TYPE := 'H'
        pnd_Ws_Pnd_Lgr_Isa_Account.setValue(pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr());                                                                              //Natural: ASSIGN #LGR-ISA-ACCOUNT := #FCPA121.INV-ACCT-LEDGR-NBR
        pnd_Ws_Pnd_Lgr_Isa_Table_Sequence.setValue("20");                                                                                                                 //Natural: ASSIGN #LGR-ISA-TABLE-SEQUENCE := '20'
        vw_sttl_Isa.startDatabaseRead                                                                                                                                     //Natural: READ ( 1 ) STTL-ISA BY STTL-ISA-KEY = #LGR-ISA-KEY THRU #LGR-ISA-KEY
        (
        "R1",
        new Wc[] { new Wc("STTL_ISA_KEY", ">=", pnd_Ws_Pnd_Lgr_Isa_Key, "And", WcType.BY) ,
        new Wc("STTL_ISA_KEY", "<=", pnd_Ws_Pnd_Lgr_Isa_Key, WcType.BY) },
        new Oc[] { new Oc("STTL_ISA_KEY", "ASC") },
        1
        );
        R1:
        while (condition(vw_sttl_Isa.readNextRow("R1")))
        {
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc().setValue(sttl_Isa_Sttl_Isa_Data);                                                                             //Natural: ASSIGN #FCPA121.INV-ACCT-LEDGR-DESC := STTL-ISA-DATA
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(vw_sttl_Isa.getAstCOUNTER().equals(getZero())))                                                                                                     //Natural: IF *COUNTER ( R1. ) = 0
        {
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc().setValue("DESCRIPTION NOT AVAILABLE");                                                                        //Natural: ASSIGN #FCPA121.INV-ACCT-LEDGR-DESC := 'DESCRIPTION NOT AVAILABLE'
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
