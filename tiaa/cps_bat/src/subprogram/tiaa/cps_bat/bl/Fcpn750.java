/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:18 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn750
************************************************************
**        * FILE NAME            : Fcpn750.java
**        * CLASS NAME           : Fcpn750
**        * INSTANCE NAME        : Fcpn750
************************************************************
************************************************************************
*  FCPN750
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn750 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_14;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_14_Origin;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1;

    private DbsGroup pnd_Ws_Rec_15;
    private DbsField pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A;
    private DbsField pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2;
    private DbsField pnd_Ws_Rec_21;
    private DbsField pnd_Ws_Index;
    private DbsField pnd_Prt_Pymnt_Check_No;
    private DbsField pnd_Prt_Pymnt_Chck_Seq_No;
    private DbsField pnd_Ws_Address_Lines_Printed;
    private DbsField pnd_Ws_Pymnt_Check_Dte;

    private DbsGroup pnd_Ws_Pymnt_Check_Dte__R_Field_1;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Century;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm;
    private DbsField pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd;
    private DbsField pnd_Ws_Amt_Mask;
    private DbsField pnd_Ws_Gross_A;
    private DbsField pnd_Ws_Gross_N;

    private DbsGroup pnd_Ws_Gross_N__R_Field_2;
    private DbsField pnd_Ws_Gross_N_Pnd_Ws_Amt;
    private DbsField pnd_Ws_Gross_N_Pnd_Ws_Rem;
    private DbsField pnd_Ws_Hold_Amt;
    private DbsField pnd_Ws_Amount;

    private DbsGroup pnd_Ws_Amount__R_Field_3;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Million;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Hundred_Thousand;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Thousands;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Hundred;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Dollars;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Cents;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_14 = localVariables.newGroupInRecord("pnd_Ws_Rec_14", "#WS-REC-14");
        pnd_Ws_Rec_14_Pnd_Ws_14_Origin = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_14_Origin", "#WS-14-ORIGIN", FieldType.STRING, 11);
        pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1 = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1", "#WS-AMT-IN-WORDS-LINE-1", 
            FieldType.STRING, 132);

        pnd_Ws_Rec_15 = localVariables.newGroupInRecord("pnd_Ws_Rec_15", "#WS-REC-15");
        pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A = pnd_Ws_Rec_15.newFieldInGroup("pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A", "#WS-15-FILLER-A", FieldType.STRING, 11);
        pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2 = pnd_Ws_Rec_15.newFieldInGroup("pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2", "#WS-AMT-IN-WORDS-LINE-2", 
            FieldType.STRING, 132);
        pnd_Ws_Rec_21 = localVariables.newFieldInRecord("pnd_Ws_Rec_21", "#WS-REC-21", FieldType.STRING, 143);
        pnd_Ws_Index = localVariables.newFieldInRecord("pnd_Ws_Index", "#WS-INDEX", FieldType.INTEGER, 2);
        pnd_Prt_Pymnt_Check_No = localVariables.newFieldInRecord("pnd_Prt_Pymnt_Check_No", "#PRT-PYMNT-CHECK-NO", FieldType.NUMERIC, 7);
        pnd_Prt_Pymnt_Chck_Seq_No = localVariables.newFieldInRecord("pnd_Prt_Pymnt_Chck_Seq_No", "#PRT-PYMNT-CHCK-SEQ-NO", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Address_Lines_Printed = localVariables.newFieldInRecord("pnd_Ws_Address_Lines_Printed", "#WS-ADDRESS-LINES-PRINTED", FieldType.NUMERIC, 
            1);
        pnd_Ws_Pymnt_Check_Dte = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Dte", "#WS-PYMNT-CHECK-DTE", FieldType.STRING, 8);

        pnd_Ws_Pymnt_Check_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Pymnt_Check_Dte__R_Field_1", "REDEFINE", pnd_Ws_Pymnt_Check_Dte);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Century = pnd_Ws_Pymnt_Check_Dte__R_Field_1.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Century", "#WS-CENTURY", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy = pnd_Ws_Pymnt_Check_Dte__R_Field_1.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy", "#WS-YY", FieldType.STRING, 
            2);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm = pnd_Ws_Pymnt_Check_Dte__R_Field_1.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm", "#WS-MM", FieldType.STRING, 
            2);
        pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd = pnd_Ws_Pymnt_Check_Dte__R_Field_1.newFieldInGroup("pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd", "#WS-DD", FieldType.STRING, 
            2);
        pnd_Ws_Amt_Mask = localVariables.newFieldInRecord("pnd_Ws_Amt_Mask", "#WS-AMT-MASK", FieldType.STRING, 14);
        pnd_Ws_Gross_A = localVariables.newFieldInRecord("pnd_Ws_Gross_A", "#WS-GROSS-A", FieldType.STRING, 14);
        pnd_Ws_Gross_N = localVariables.newFieldInRecord("pnd_Ws_Gross_N", "#WS-GROSS-N", FieldType.NUMERIC, 11, 2);

        pnd_Ws_Gross_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Gross_N__R_Field_2", "REDEFINE", pnd_Ws_Gross_N);
        pnd_Ws_Gross_N_Pnd_Ws_Amt = pnd_Ws_Gross_N__R_Field_2.newFieldInGroup("pnd_Ws_Gross_N_Pnd_Ws_Amt", "#WS-AMT", FieldType.NUMERIC, 9);
        pnd_Ws_Gross_N_Pnd_Ws_Rem = pnd_Ws_Gross_N__R_Field_2.newFieldInGroup("pnd_Ws_Gross_N_Pnd_Ws_Rem", "#WS-REM", FieldType.STRING, 2);
        pnd_Ws_Hold_Amt = localVariables.newFieldInRecord("pnd_Ws_Hold_Amt", "#WS-HOLD-AMT", FieldType.STRING, 10);
        pnd_Ws_Amount = localVariables.newFieldInRecord("pnd_Ws_Amount", "#WS-AMOUNT", FieldType.NUMERIC, 9, 2);

        pnd_Ws_Amount__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Amount__R_Field_3", "REDEFINE", pnd_Ws_Amount);
        pnd_Ws_Amount_Pnd_Ws_Million = pnd_Ws_Amount__R_Field_3.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Million", "#WS-MILLION", FieldType.NUMERIC, 1);
        pnd_Ws_Amount_Pnd_Ws_Hundred_Thousand = pnd_Ws_Amount__R_Field_3.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Hundred_Thousand", "#WS-HUNDRED-THOUSAND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Amount_Pnd_Ws_Thousands = pnd_Ws_Amount__R_Field_3.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Thousands", "#WS-THOUSANDS", FieldType.NUMERIC, 
            2);
        pnd_Ws_Amount_Pnd_Ws_Hundred = pnd_Ws_Amount__R_Field_3.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Hundred", "#WS-HUNDRED", FieldType.NUMERIC, 1);
        pnd_Ws_Amount_Pnd_Ws_Dollars = pnd_Ws_Amount__R_Field_3.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Dollars", "#WS-DOLLARS", FieldType.NUMERIC, 2);
        pnd_Ws_Amount_Pnd_Ws_Cents = pnd_Ws_Amount__R_Field_3.newFieldInGroup("pnd_Ws_Amount_Pnd_Ws_Cents", "#WS-CENTS", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn750() throws Exception
    {
        super("Fcpn750");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        //* *ASSIGN #PROGRAM = *PROGRAM
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* *SAG DEFINE EXIT BEFORE-READ
        //* *
        //* *
        //* *SAG END-EXIT
        //*  PRIMARY FILE
        //* *SAG DEFINE EXIT AFTER-PRIME-READ
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1 #WS-REC-14 #WS-REC-15 #WS-REC-21
        pnd_Ws_Rec_14.reset();
        pnd_Ws_Rec_15.reset();
        pnd_Ws_Rec_21.reset();
        pnd_Ws_Amount.reset();                                                                                                                                            //Natural: RESET #WS-AMOUNT
        pnd_Ws_Amount.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                                     //Natural: ASSIGN #WS-AMOUNT = PYMNT-CHECK-AMT
        DbsUtil.callnat(Fcpn255.class , getCurrentProcessState(), pnd_Ws_Amount, pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1, pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2);     //Natural: CALLNAT 'FCPN255' #WS-AMOUNT #WS-AMT-IN-WORDS-LINE-1 #WS-AMT-IN-WORDS-LINE-2
        if (condition(Global.isEscape())) return;
        short decideConditionsMet265 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #WS-HEADER-RECORD.CNTRCT-ORGN-CDE;//Natural: VALUE 'MS'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("MS"))))
        {
            decideConditionsMet265++;
            pnd_Ws_Rec_14_Pnd_Ws_14_Origin.setValue("1!M.S.");                                                                                                            //Natural: ASSIGN #WS-14-ORIGIN = '1!M.S.'
        }                                                                                                                                                                 //Natural: VALUE 'IA'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("IA"))))
        {
            decideConditionsMet265++;
            pnd_Ws_Rec_14_Pnd_Ws_14_Origin.setValue("1!I.A.");                                                                                                            //Natural: ASSIGN #WS-14-ORIGIN = '1!I.A.'
        }                                                                                                                                                                 //Natural: VALUE 'DS'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Orgn_Cde().equals("DS"))))
        {
            decideConditionsMet265++;
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("GRA") || pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Lob_Cde().equals("GSRA")))       //Natural: IF CNTRCT-LOB-CDE = 'GRA' OR = 'GSRA'
            {
                pnd_Ws_Rec_14_Pnd_Ws_14_Origin.setValue("1!L.S.");                                                                                                        //Natural: ASSIGN #WS-14-ORIGIN = '1!L.S.'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_14_Pnd_Ws_14_Origin.setValue("1!D.S.");                                                                                                        //Natural: ASSIGN #WS-14-ORIGIN = '1!D.S.'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet265 > 0))
        {
            getWorkFiles().write(8, false, pnd_Ws_Rec_14);                                                                                                                //Natural: WRITE WORK FILE 8 #WS-REC-14
            pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A.setValue(" !");                                                                                                              //Natural: ASSIGN #WS-15-FILLER-A = ' !'
            getWorkFiles().write(8, false, pnd_Ws_Rec_15);                                                                                                                //Natural: WRITE WORK FILE 8 #WS-REC-15
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*3", pdaFcpa700.getPnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr(), "H'000000'",             //Natural: COMPRESS '*3' #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR H'000000' #WS-HEADER-RECORD.#WS-PYMNT-CHECK-SEQ-NBR INTO #WS-REC-1 LEAVING NO SPACE
            pdaFcpa700.getPnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr()));
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals("0"))  //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( 1 ) = ' ' OR = '0' ) OR PYMNT-NME ( 1 ) = MASK ( 'CR ' )
            || DbsUtil.maskMatches(pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1),"'CR '")))
        {
            if (condition(! (pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals(" ") || pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1).equals("0")))) //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR ( 1 ) = ' ' OR = '0' )
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1), "H'0000'", "A/C", "H'00'",             //Natural: COMPRESS '*!*********' PYMNT-NME ( 1 ) H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR ( 1 ) INTO #WS-REC-1
                    pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr().getValue(1)));
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Nme().getValue(1)));                                      //Natural: COMPRESS '*!*********' PYMNT-NME ( 1 ) INTO #WS-REC-1
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde().equals(" ") || pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde().equals("0"))))          //Natural: IF NOT ( CNTRCT-HOLD-CDE = ' ' OR = '0' )
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*3+++", pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Hold_Cde()));                        //Natural: COMPRESS '*3+++' CNTRCT-HOLD-CDE INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace("0"));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH '0'
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 3");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 3'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1.setValue("19PAY");                                                                                                                                   //Natural: ASSIGN #WS-REC-1 = '19PAY'
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Gross_N.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Amt());                                                                                    //Natural: MOVE PYMNT-CHECK-AMT TO #WS-GROSS-N
        pnd_Ws_Rec_21.setValue("+5");                                                                                                                                     //Natural: MOVE '+5' TO #WS-REC-21
        pnd_Ws_Hold_Amt.setValueEdited(pnd_Ws_Gross_N_Pnd_Ws_Amt,new ReportEditMask("X'$'Z,ZZZ,ZZ9"));                                                                    //Natural: MOVE EDITED #WS-AMT ( EM = X'$'Z,ZZZ,ZZ9 ) TO #WS-HOLD-AMT
        DbsUtil.examine(new ExamineSource(pnd_Ws_Hold_Amt,true), new ExamineSearch("X", true), new ExamineDelete());                                                      //Natural: EXAMINE FULL #WS-HOLD-AMT FOR FULL 'X' DELETE
        //* *MOVE LEFT #WS-HOLD-AMT TO  #WS-HOLD-AMT
        //* *COMPRESS    '$' #WS-HOLD-AMT INTO  #WS-HOLD-AMT LEAVING NO SPACE
        pnd_Ws_Hold_Amt.setValue(pnd_Ws_Hold_Amt, MoveOption.RightJustified);                                                                                             //Natural: MOVE RIGHT #WS-HOLD-AMT TO #WS-HOLD-AMT
        setValueToSubstring(pnd_Ws_Gross_N_Pnd_Ws_Rem,pnd_Ws_Rec_21,21,2);                                                                                                //Natural: MOVE #WS-REM TO SUBSTRING ( #WS-REC-21,21,2 )
        setValueToSubstring(pnd_Ws_Hold_Amt,pnd_Ws_Rec_21,10,10);                                                                                                         //Natural: MOVE #WS-HOLD-AMT TO SUBSTRING ( #WS-REC-21,10,10 )
        //* *COMPRESS '+5*******' #WS-HOLD-AMT H'00' #WS-REM
        //* *  INTO  #WS-REC-1 LEAVING NO SPACE
        //* *EXAMINE FULL #WS-REC-1 FOR FULL '*' REPLACE ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_21);                                                                                                                    //Natural: WRITE WORK FILE 8 #WS-REC-21
        pnd_Ws_Index.setValue(1);                                                                                                                                         //Natural: ASSIGN #WS-INDEX = 1
        //*  FORMAT BARCODE AND ADDR LINES
        DbsUtil.callnat(Fcpn753.class , getCurrentProcessState(), pdaFcpa700.getPnd_Ws_Header_Record().getValue("*"), pdaFcpa700.getPnd_Ws_Occurs().getValue("*","*"),    //Natural: CALLNAT 'FCPN753' #WS-HEADER-RECORD ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS ( *,* ) #WS-INDEX
            pdaFcpa700.getPnd_Ws_Name_N_Address().getValue("*","*"), pnd_Ws_Index);
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pymnt_Check_Dte.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #WS-PYMNT-CHECK-DTE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm), new ExamineSearch("0"), new ExamineReplace("*"));                                            //Natural: EXAMINE #WS-MM '0' REPLACE WITH '*'
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd), new ExamineSearch("0"), new ExamineReplace("*"));                                            //Natural: EXAMINE #WS-DD '0' REPLACE WITH '*'
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy), new ExamineSearch("0"), new ExamineReplace("*"));                                            //Natural: EXAMINE #WS-YY '0' REPLACE WITH '*'
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "13", pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Mm, "+++", pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Dd,            //Natural: COMPRESS '13' #WS-MM '+++' #WS-DD '+++' #WS-YY INTO #WS-REC-1 LEAVING NO SPACE
            "+++", pnd_Ws_Pymnt_Check_Dte_Pnd_Ws_Yy));
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace("0"));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH '0'
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("+"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '+' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1$AB"));                                                                                   //Natural: COMPRESS '1$AB' INTO #WS-REC-1 LEAVING NO SPACE
        //*  COMPRESS '1$XX' INTO #WS-REC-1 LEAVING NO SPACE
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
