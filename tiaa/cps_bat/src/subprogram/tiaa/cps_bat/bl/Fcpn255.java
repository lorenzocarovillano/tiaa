/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:22:49 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn255
************************************************************
**        * FILE NAME            : Fcpn255.java
**        * CLASS NAME           : Fcpn255
**        * INSTANCE NAME        : Fcpn255
************************************************************
************************************************************************
* PROGRAM  : FCPN255
* SYSTEM   : CPS
* FUNCTION : THIS PROGRAM TESTS THE SUBPROGRAM THAT CONVERTS AMOUNT
*            INTO WORDS.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn255 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Amt;

    private DbsGroup pnd_Amt__R_Field_1;
    private DbsField pnd_Amt_Pnd_Million;
    private DbsField pnd_Amt_Pnd_Hundred_Thousand;
    private DbsField pnd_Amt_Pnd_Thousands;

    private DbsGroup pnd_Amt__R_Field_2;
    private DbsField pnd_Amt__Filler1;
    private DbsField pnd_Amt_Pnd_Thousands_2;
    private DbsField pnd_Amt_Pnd_Hundred;
    private DbsField pnd_Amt_Pnd_Dollars;

    private DbsGroup pnd_Amt__R_Field_3;
    private DbsField pnd_Amt__Filler2;
    private DbsField pnd_Amt_Pnd_Dollars_2;
    private DbsField pnd_Amt_Pnd_Cents;
    private DbsField pnd_Amt_In_Words_Line_1;
    private DbsField pnd_Amt_In_Words_Line_2;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_One_Digit;
    private DbsField pnd_Ws_Pnd_Two_Digits;
    private DbsField pnd_Ws_Pnd_Wrd;

    private DbsGroup pnd_Ws_Pnd_Literal;
    private DbsField pnd_Ws_Pnd_Literal_1;
    private DbsField pnd_Ws_Pnd_Literal_2;

    private DbsGroup pnd_Ws__R_Field_4;
    private DbsField pnd_Ws_Pnd_Word;
    private DbsField pnd_Ws_Pnd_Word_Table;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Amt = parameters.newFieldInRecord("pnd_Amt", "#AMT", FieldType.NUMERIC, 9, 2);
        pnd_Amt.setParameterOption(ParameterOption.ByReference);

        pnd_Amt__R_Field_1 = parameters.newGroupInRecord("pnd_Amt__R_Field_1", "REDEFINE", pnd_Amt);
        pnd_Amt_Pnd_Million = pnd_Amt__R_Field_1.newFieldInGroup("pnd_Amt_Pnd_Million", "#MILLION", FieldType.NUMERIC, 1);
        pnd_Amt_Pnd_Hundred_Thousand = pnd_Amt__R_Field_1.newFieldInGroup("pnd_Amt_Pnd_Hundred_Thousand", "#HUNDRED-THOUSAND", FieldType.NUMERIC, 1);
        pnd_Amt_Pnd_Thousands = pnd_Amt__R_Field_1.newFieldInGroup("pnd_Amt_Pnd_Thousands", "#THOUSANDS", FieldType.NUMERIC, 2);

        pnd_Amt__R_Field_2 = pnd_Amt__R_Field_1.newGroupInGroup("pnd_Amt__R_Field_2", "REDEFINE", pnd_Amt_Pnd_Thousands);
        pnd_Amt__Filler1 = pnd_Amt__R_Field_2.newFieldInGroup("pnd_Amt__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Amt_Pnd_Thousands_2 = pnd_Amt__R_Field_2.newFieldInGroup("pnd_Amt_Pnd_Thousands_2", "#THOUSANDS-2", FieldType.NUMERIC, 1);
        pnd_Amt_Pnd_Hundred = pnd_Amt__R_Field_1.newFieldInGroup("pnd_Amt_Pnd_Hundred", "#HUNDRED", FieldType.NUMERIC, 1);
        pnd_Amt_Pnd_Dollars = pnd_Amt__R_Field_1.newFieldInGroup("pnd_Amt_Pnd_Dollars", "#DOLLARS", FieldType.NUMERIC, 2);

        pnd_Amt__R_Field_3 = pnd_Amt__R_Field_1.newGroupInGroup("pnd_Amt__R_Field_3", "REDEFINE", pnd_Amt_Pnd_Dollars);
        pnd_Amt__Filler2 = pnd_Amt__R_Field_3.newFieldInGroup("pnd_Amt__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Amt_Pnd_Dollars_2 = pnd_Amt__R_Field_3.newFieldInGroup("pnd_Amt_Pnd_Dollars_2", "#DOLLARS-2", FieldType.NUMERIC, 1);
        pnd_Amt_Pnd_Cents = pnd_Amt__R_Field_1.newFieldInGroup("pnd_Amt_Pnd_Cents", "#CENTS", FieldType.STRING, 2);
        pnd_Amt_In_Words_Line_1 = parameters.newFieldInRecord("pnd_Amt_In_Words_Line_1", "#AMT-IN-WORDS-LINE-1", FieldType.STRING, 132);
        pnd_Amt_In_Words_Line_1.setParameterOption(ParameterOption.ByReference);
        pnd_Amt_In_Words_Line_2 = parameters.newFieldInRecord("pnd_Amt_In_Words_Line_2", "#AMT-IN-WORDS-LINE-2", FieldType.STRING, 132);
        pnd_Amt_In_Words_Line_2.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_One_Digit = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_One_Digit", "#ONE-DIGIT", FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Two_Digits = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Two_Digits", "#TWO-DIGITS", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Wrd = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Wrd", "#WRD", FieldType.PACKED_DECIMAL, 7);

        pnd_Ws_Pnd_Literal = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Literal", "#LITERAL");
        pnd_Ws_Pnd_Literal_1 = pnd_Ws_Pnd_Literal.newFieldInGroup("pnd_Ws_Pnd_Literal_1", "#LITERAL-1", FieldType.STRING, 100);
        pnd_Ws_Pnd_Literal_2 = pnd_Ws_Pnd_Literal.newFieldInGroup("pnd_Ws_Pnd_Literal_2", "#LITERAL-2", FieldType.STRING, 100);

        pnd_Ws__R_Field_4 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_4", "REDEFINE", pnd_Ws_Pnd_Literal);
        pnd_Ws_Pnd_Word = pnd_Ws__R_Field_4.newFieldArrayInGroup("pnd_Ws_Pnd_Word", "#WORD", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Ws_Pnd_Word_Table = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Word_Table", "#WORD-TABLE", FieldType.STRING, 10, new DbsArrayController(1, 30));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Ws_Pnd_Wrd.setInitialValue(0);
        pnd_Ws_Pnd_Word_Table.getValue(1).setInitialValue("ONE");
        pnd_Ws_Pnd_Word_Table.getValue(2).setInitialValue("TWO");
        pnd_Ws_Pnd_Word_Table.getValue(3).setInitialValue("THREE");
        pnd_Ws_Pnd_Word_Table.getValue(4).setInitialValue("FOUR");
        pnd_Ws_Pnd_Word_Table.getValue(5).setInitialValue("FIVE");
        pnd_Ws_Pnd_Word_Table.getValue(6).setInitialValue("SIX");
        pnd_Ws_Pnd_Word_Table.getValue(7).setInitialValue("SEVEN");
        pnd_Ws_Pnd_Word_Table.getValue(8).setInitialValue("EIGHT");
        pnd_Ws_Pnd_Word_Table.getValue(9).setInitialValue("NINE");
        pnd_Ws_Pnd_Word_Table.getValue(10).setInitialValue("TEN");
        pnd_Ws_Pnd_Word_Table.getValue(11).setInitialValue("ELEVEN");
        pnd_Ws_Pnd_Word_Table.getValue(12).setInitialValue("TWELVE");
        pnd_Ws_Pnd_Word_Table.getValue(13).setInitialValue("THIRTEEN");
        pnd_Ws_Pnd_Word_Table.getValue(14).setInitialValue("FOURTEEN");
        pnd_Ws_Pnd_Word_Table.getValue(15).setInitialValue("FIFTEEN");
        pnd_Ws_Pnd_Word_Table.getValue(16).setInitialValue("SIXTEEN");
        pnd_Ws_Pnd_Word_Table.getValue(17).setInitialValue("SEVENTEEN");
        pnd_Ws_Pnd_Word_Table.getValue(18).setInitialValue("EIGHTEEN");
        pnd_Ws_Pnd_Word_Table.getValue(19).setInitialValue("NINETEEN");
        pnd_Ws_Pnd_Word_Table.getValue(20).setInitialValue("TWENTY");
        pnd_Ws_Pnd_Word_Table.getValue(21).setInitialValue("THIRTY");
        pnd_Ws_Pnd_Word_Table.getValue(22).setInitialValue("FORTY");
        pnd_Ws_Pnd_Word_Table.getValue(23).setInitialValue("FIFTY");
        pnd_Ws_Pnd_Word_Table.getValue(24).setInitialValue("SIXTY");
        pnd_Ws_Pnd_Word_Table.getValue(25).setInitialValue("SEVENTY");
        pnd_Ws_Pnd_Word_Table.getValue(26).setInitialValue("EIGHTY");
        pnd_Ws_Pnd_Word_Table.getValue(27).setInitialValue("NINETY");
        pnd_Ws_Pnd_Word_Table.getValue(28).setInitialValue("HUNDRED");
        pnd_Ws_Pnd_Word_Table.getValue(29).setInitialValue("THOUSAND");
        pnd_Ws_Pnd_Word_Table.getValue(30).setInitialValue("MILLION");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn255() throws Exception
    {
        super("Fcpn255");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Amt_In_Words_Line_2.reset();                                                                                                                                  //Natural: RESET #AMT-IN-WORDS-LINE-2
        if (condition(pnd_Amt.less(getZero())))                                                                                                                           //Natural: IF #AMT < 0
        {
            pnd_Amt_In_Words_Line_1.setValue("** AMOUNT IS LESS THAN ZERO. !!! E R R O R !!! **");                                                                        //Natural: MOVE '** AMOUNT IS LESS THAN ZERO. !!! E R R O R !!! **' TO #AMT-IN-WORDS-LINE-1
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Amt.equals(getZero())))                                                                                                                         //Natural: IF #AMT = 0
        {
            pnd_Amt_In_Words_Line_1.setValue("** 00 CENTS **");                                                                                                           //Natural: MOVE '** 00 CENTS **' TO #AMT-IN-WORDS-LINE-1
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WRD
        pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue("**");                                                                                                          //Natural: MOVE '**' TO #WORD ( #WRD )
        if (condition(pnd_Amt_Pnd_Million.greater(getZero())))                                                                                                            //Natural: IF #MILLION GT 0
        {
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(pnd_Amt_Pnd_Million));                                                       //Natural: MOVE #WORD-TABLE ( #MILLION ) TO #WORD ( #WRD )
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(30));                                                                        //Natural: MOVE #WORD-TABLE ( 30 ) TO #WORD ( #WRD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Amt_Pnd_Hundred_Thousand.greater(getZero())))                                                                                                   //Natural: IF #HUNDRED-THOUSAND GT 0
        {
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(pnd_Amt_Pnd_Hundred_Thousand));                                              //Natural: MOVE #WORD-TABLE ( #HUNDRED-THOUSAND ) TO #WORD ( #WRD )
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(28));                                                                        //Natural: MOVE #WORD-TABLE ( 28 ) TO #WORD ( #WRD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Amt_Pnd_Thousands.greater(getZero())))                                                                                                          //Natural: IF #THOUSANDS GT 0
        {
            pnd_Ws_Pnd_Two_Digits.setValue(pnd_Amt_Pnd_Thousands);                                                                                                        //Natural: MOVE #THOUSANDS TO #TWO-DIGITS
            pnd_Ws_Pnd_One_Digit.setValue(pnd_Amt_Pnd_Thousands_2);                                                                                                       //Natural: MOVE #THOUSANDS-2 TO #ONE-DIGIT
                                                                                                                                                                          //Natural: PERFORM TEST-TWO-DIGITS
            sub_Test_Two_Digits();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Amt_Pnd_Hundred_Thousand.notEquals(getZero()) || pnd_Amt_Pnd_Thousands.notEquals(getZero())))                                                   //Natural: IF #HUNDRED-THOUSAND NE 0 OR #THOUSANDS NE 0
        {
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(29));                                                                        //Natural: MOVE #WORD-TABLE ( 29 ) TO #WORD ( #WRD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Amt_Pnd_Hundred.greater(getZero())))                                                                                                            //Natural: IF #HUNDRED GT 0
        {
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(pnd_Amt_Pnd_Hundred));                                                       //Natural: MOVE #WORD-TABLE ( #HUNDRED ) TO #WORD ( #WRD )
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(28));                                                                        //Natural: MOVE #WORD-TABLE ( 28 ) TO #WORD ( #WRD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Amt_Pnd_Dollars.greater(getZero())))                                                                                                            //Natural: IF #DOLLARS GT 0
        {
            pnd_Ws_Pnd_Two_Digits.setValue(pnd_Amt_Pnd_Dollars);                                                                                                          //Natural: MOVE #DOLLARS TO #TWO-DIGITS
            pnd_Ws_Pnd_One_Digit.setValue(pnd_Amt_Pnd_Dollars_2);                                                                                                         //Natural: MOVE #DOLLARS-2 TO #ONE-DIGIT
                                                                                                                                                                          //Natural: PERFORM TEST-TWO-DIGITS
            sub_Test_Two_Digits();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Amt.greaterOrEqual(new DbsDecimal("1.00"))))                                                                                                    //Natural: IF #AMT GE 1.00
        {
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue("DOLLARS");                                                                                                 //Natural: MOVE 'DOLLARS' TO #WORD ( #WRD )
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue("AND");                                                                                                     //Natural: MOVE 'AND' TO #WORD ( #WRD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Wrd.equals(9)))                                                                                                                          //Natural: IF #WRD = 9
        {
            pnd_Ws_Pnd_Wrd.nadd(2);                                                                                                                                       //Natural: ADD 2 TO #WRD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Amt_Pnd_Cents);                                                                                             //Natural: MOVE #CENTS TO #WORD ( #WRD )
        pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WRD
        pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue("CENTS **");                                                                                                    //Natural: MOVE 'CENTS **' TO #WORD ( #WRD )
        pnd_Amt_In_Words_Line_1.setValue(DbsUtil.compress(pnd_Ws_Pnd_Word.getValue(1,":",10)));                                                                           //Natural: COMPRESS #WORD ( 1:10 ) INTO #AMT-IN-WORDS-LINE-1
        if (condition(pnd_Ws_Pnd_Literal_2.notEquals(" ")))                                                                                                               //Natural: IF #LITERAL-2 NE ' '
        {
            pnd_Amt_In_Words_Line_2.setValue(DbsUtil.compress(pnd_Ws_Pnd_Word.getValue(11,":",20)));                                                                      //Natural: COMPRESS #WORD ( 11:20 ) INTO #AMT-IN-WORDS-LINE-2
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-TWO-DIGITS
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-ONE-DIGIT
    }
    private void sub_Test_Two_Digits() throws Exception                                                                                                                   //Natural: TEST-TWO-DIGITS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #WRD
        short decideConditionsMet111 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TWO-DIGITS;//Natural: VALUE 1 : 20
        if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(1) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(20)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(pnd_Ws_Pnd_Two_Digits));                                                     //Natural: MOVE #WORD-TABLE ( #TWO-DIGITS ) TO #WORD ( #WRD )
        }                                                                                                                                                                 //Natural: VALUE 21 : 29
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(21) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(29)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(20));                                                                        //Natural: MOVE #WORD-TABLE ( 20 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 30 : 39
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(30) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(39)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(21));                                                                        //Natural: MOVE #WORD-TABLE ( 21 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 40 : 49
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(40) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(49)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(22));                                                                        //Natural: MOVE #WORD-TABLE ( 22 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 50 : 59
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(50) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(59)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(23));                                                                        //Natural: MOVE #WORD-TABLE ( 23 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 60 : 69
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(60) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(69)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(24));                                                                        //Natural: MOVE #WORD-TABLE ( 24 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 70 : 79
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(70) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(79)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(25));                                                                        //Natural: MOVE #WORD-TABLE ( 25 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 80 : 89
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(80) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(89)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(26));                                                                        //Natural: MOVE #WORD-TABLE ( 26 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 90 : 99
        else if (condition(((pnd_Ws_Pnd_Two_Digits.greaterOrEqual(90) && pnd_Ws_Pnd_Two_Digits.lessOrEqual(99)))))
        {
            decideConditionsMet111++;
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(27));                                                                        //Natural: MOVE #WORD-TABLE ( 27 ) TO #WORD ( #WRD )
                                                                                                                                                                          //Natural: PERFORM TEST-ONE-DIGIT
            sub_Test_One_Digit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Test_One_Digit() throws Exception                                                                                                                    //Natural: TEST-ONE-DIGIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(pnd_Ws_Pnd_One_Digit.notEquals(getZero())))                                                                                                         //Natural: IF #ONE-DIGIT NE 0
        {
            pnd_Ws_Pnd_Wrd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #WRD
            pnd_Ws_Pnd_Word.getValue(pnd_Ws_Pnd_Wrd).setValue(pnd_Ws_Pnd_Word_Table.getValue(pnd_Ws_Pnd_One_Digit));                                                      //Natural: MOVE #WORD-TABLE ( #ONE-DIGIT ) TO #WORD ( #WRD )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
