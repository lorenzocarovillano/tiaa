/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:49 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn874c
************************************************************
**        * FILE NAME            : Fcpn874c.java
**        * CLASS NAME           : Fcpn874c
**        * INSTANCE NAME        : Fcpn874c
************************************************************
************************************************************************
* SUBPROGRAM : FCPN874C
* SYSTEM     : CPS
* TITLE      : NEW ANNUITIZATION
* FUNCTION   : "NZ" ANNUITANT STATEMENTS - CHECK PART.
************************************************************************
* 01/05/2006 : R. LANDRUM
*              POS-PAY, PAYEE MATCH. ADD STOP POINT FOR CHECK NUMBER ON
*              STATEMENT BODY & CHECK FACE, 10 DIGIT MICR LINE.
*
*************************** NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR THE PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn874c extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa874c pdaFcpa874c;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa874a pdaFcpa874a;
    private PdaFcpa110 pdaFcpa110;
    private LdaFcpl803c ldaFcpl803c;
    private PdaFcpa803p pdaFcpa803p;
    private LdaCpol155m ldaCpol155m;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Orgn_Cde;
    private DbsField pnd_Ws_Rec;

    private DbsGroup pnd_Ws_Rec__R_Field_1;
    private DbsField pnd_Ws_Rec_Pnd_Ws_Cc;
    private DbsField pnd_Ws_Rec_Pnd_Ws_Font;
    private DbsField pnd_Ws_Rec_Pnd_Ws_Detail;
    private DbsField pnd_Ws_Address;
    private DbsField pnd_Ws_Address_Lines_Printed;
    private DbsField pnd_Ws_Index;
    private DbsField pnd_I;
    private DbsField pnd_Start;
    private DbsField pnd_End;
    private DbsField pnd_Address_Filler;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_A10;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;

    private DbsGroup pnd_Ws_Chk_Amt;
    private DbsField pnd_Ws_Chk_Amt_Pnd_Work_Chk_Amt;
    private DbsField pnd_Ws_Chk_Amt_Pnd_Amt_Alpha;
    private DbsField pnd_Ws_Chk_Amt_Pnd_Amt_Text;
    private DbsField pnd_Ws_Amount;

    private DbsGroup pnd_Ws_Rec_14;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_14_Cc;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_14_Orgn;
    private DbsField pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1;

    private DbsGroup pnd_Ws_Rec_15;
    private DbsField pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A;
    private DbsField pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2;
    private DbsField pnd_Ws_Rec_21;
    private DbsField pnd_Pymnt_Seq_Nbr;
    private DbsField split_Top;
    private DbsField split_Bottom;
    private DbsField pnd_Pay_Amt;
    private DbsField pnd_Pay_Amt_Edit;

    private DbsGroup pnd_Pay_Amt_Edit__R_Field_3;
    private DbsField pnd_Pay_Amt_Edit_Pnd_Remove_Stars;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl803c = new LdaFcpl803c();
        registerRecord(ldaFcpl803c);
        localVariables = new DbsRecord();
        pdaFcpa803p = new PdaFcpa803p(localVariables);
        ldaCpol155m = new LdaCpol155m();
        registerRecord(ldaCpol155m);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa874c = new PdaFcpa874c(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa874a = new PdaFcpa874a(parameters);
        pdaFcpa110 = new PdaFcpa110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Orgn_Cde", "#WS-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Rec = localVariables.newFieldInRecord("pnd_Ws_Rec", "#WS-REC", FieldType.STRING, 143);

        pnd_Ws_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Rec__R_Field_1", "REDEFINE", pnd_Ws_Rec);
        pnd_Ws_Rec_Pnd_Ws_Cc = pnd_Ws_Rec__R_Field_1.newFieldInGroup("pnd_Ws_Rec_Pnd_Ws_Cc", "#WS-CC", FieldType.STRING, 1);
        pnd_Ws_Rec_Pnd_Ws_Font = pnd_Ws_Rec__R_Field_1.newFieldInGroup("pnd_Ws_Rec_Pnd_Ws_Font", "#WS-FONT", FieldType.STRING, 1);
        pnd_Ws_Rec_Pnd_Ws_Detail = pnd_Ws_Rec__R_Field_1.newFieldInGroup("pnd_Ws_Rec_Pnd_Ws_Detail", "#WS-DETAIL", FieldType.STRING, 141);
        pnd_Ws_Address = localVariables.newFieldInRecord("pnd_Ws_Address", "#WS-ADDRESS", FieldType.STRING, 35);
        pnd_Ws_Address_Lines_Printed = localVariables.newFieldInRecord("pnd_Ws_Address_Lines_Printed", "#WS-ADDRESS-LINES-PRINTED", FieldType.NUMERIC, 
            1);
        pnd_Ws_Index = localVariables.newFieldInRecord("pnd_Ws_Index", "#WS-INDEX", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Start = localVariables.newFieldInRecord("pnd_Start", "#START", FieldType.PACKED_DECIMAL, 3);
        pnd_End = localVariables.newFieldInRecord("pnd_End", "#END", FieldType.PACKED_DECIMAL, 3);
        pnd_Address_Filler = localVariables.newFieldInRecord("pnd_Address_Filler", "#ADDRESS-FILLER", FieldType.STRING, 22);
        pnd_Ws_Pymnt_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Nbr_A10", "#WS-PYMNT-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Pymnt_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Check_Nbr_N10", "#WS-PYMNT-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Ws_Pymnt_Check_Nbr_N10);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", 
            "#WS-CHECK-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", 
            "#WS-CHECK-NBR-N7", FieldType.NUMERIC, 7);

        pnd_Ws_Chk_Amt = localVariables.newGroupInRecord("pnd_Ws_Chk_Amt", "#WS-CHK-AMT");
        pnd_Ws_Chk_Amt_Pnd_Work_Chk_Amt = pnd_Ws_Chk_Amt.newFieldInGroup("pnd_Ws_Chk_Amt_Pnd_Work_Chk_Amt", "#WORK-CHK-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Ws_Chk_Amt_Pnd_Amt_Alpha = pnd_Ws_Chk_Amt.newFieldInGroup("pnd_Ws_Chk_Amt_Pnd_Amt_Alpha", "#AMT-ALPHA", FieldType.STRING, 15);
        pnd_Ws_Chk_Amt_Pnd_Amt_Text = pnd_Ws_Chk_Amt.newFieldArrayInGroup("pnd_Ws_Chk_Amt_Pnd_Amt_Text", "#AMT-TEXT", FieldType.STRING, 132, new DbsArrayController(1, 
            2));
        pnd_Ws_Amount = localVariables.newFieldInRecord("pnd_Ws_Amount", "#WS-AMOUNT", FieldType.NUMERIC, 9, 2);

        pnd_Ws_Rec_14 = localVariables.newGroupInRecord("pnd_Ws_Rec_14", "#WS-REC-14");
        pnd_Ws_Rec_14_Pnd_Ws_14_Cc = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_14_Cc", "#WS-14-CC", FieldType.STRING, 2);
        pnd_Ws_Rec_14_Pnd_Ws_14_Orgn = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_14_Orgn", "#WS-14-ORGN", FieldType.STRING, 9);
        pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1 = pnd_Ws_Rec_14.newFieldInGroup("pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1", "#WS-AMT-IN-WORDS-LINE-1", 
            FieldType.STRING, 132);

        pnd_Ws_Rec_15 = localVariables.newGroupInRecord("pnd_Ws_Rec_15", "#WS-REC-15");
        pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A = pnd_Ws_Rec_15.newFieldInGroup("pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A", "#WS-15-FILLER-A", FieldType.STRING, 11);
        pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2 = pnd_Ws_Rec_15.newFieldInGroup("pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2", "#WS-AMT-IN-WORDS-LINE-2", 
            FieldType.STRING, 132);
        pnd_Ws_Rec_21 = localVariables.newFieldInRecord("pnd_Ws_Rec_21", "#WS-REC-21", FieldType.STRING, 143);
        pnd_Pymnt_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Seq_Nbr", "#PYMNT-SEQ-NBR", FieldType.STRING, 7);
        split_Top = localVariables.newFieldInRecord("split_Top", "SPLIT-TOP", FieldType.STRING, 6);
        split_Bottom = localVariables.newFieldInRecord("split_Bottom", "SPLIT-BOTTOM", FieldType.STRING, 5);
        pnd_Pay_Amt = localVariables.newFieldInRecord("pnd_Pay_Amt", "#PAY-AMT", FieldType.STRING, 17);
        pnd_Pay_Amt_Edit = localVariables.newFieldInRecord("pnd_Pay_Amt_Edit", "#PAY-AMT-EDIT", FieldType.STRING, 20);

        pnd_Pay_Amt_Edit__R_Field_3 = localVariables.newGroupInRecord("pnd_Pay_Amt_Edit__R_Field_3", "REDEFINE", pnd_Pay_Amt_Edit);
        pnd_Pay_Amt_Edit_Pnd_Remove_Stars = pnd_Pay_Amt_Edit__R_Field_3.newFieldInGroup("pnd_Pay_Amt_Edit_Pnd_Remove_Stars", "#REMOVE-STARS", FieldType.STRING, 
            6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803c.initializeValues();
        ldaCpol155m.initializeValues();

        localVariables.reset();
        pnd_Ws_Rec_14_Pnd_Ws_14_Cc.setInitialValue("1!");
        pnd_Ws_Rec_15_Pnd_Ws_15_Filler_A.setInitialValue(" !");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn874c() throws Exception
    {
        super("Fcpn874c");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        short decideConditionsMet298 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FCPA803.#CHECK-TO-ANNT
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean()))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM FORMAT-PYMNT-AMT
            sub_Format_Pymnt_Amt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GENERATE-CHECK
            sub_Generate_Check();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#STMNT-TO-ANNT
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().getBoolean()))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM FORMAT-PYMNT-AMT
            sub_Format_Pymnt_Amt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GENERATE-STMNT-TO-ANNT
            sub_Generate_Stmnt_To_Annt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#ZERO-CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().getBoolean()))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM FORMAT-PYMNT-AMT
            sub_Format_Pymnt_Amt();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Text_Ind().setValue(1);                                                                                                   //Natural: ASSIGN #STMNT-TEXT-IND := 1
                                                                                                                                                                          //Natural: PERFORM GENERATE-STMNT
            sub_Generate_Stmnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#GLOBAL-PAY
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM FORMAT-PYMNT-AMT
            sub_Format_Pymnt_Amt();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Text_Ind().setValue(2);                                                                                                   //Natural: ASSIGN #STMNT-TEXT-IND := 2
                                                                                                                                                                          //Natural: PERFORM GENERATE-STMNT
            sub_Generate_Stmnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean()))
        {
            decideConditionsMet298++;
                                                                                                                                                                          //Natural: PERFORM GENERATE-CHECK
            sub_Generate_Check();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ****************** RL BEGIN POS-PAY PAYEE MATCH **********************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-CHECK
        //* *  COMPRESS '*1*********'
        //* *    PYMNT-NME (1)
        //* *    H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR (1)
        //* *  COMPRESS '*1*********'
        //* *#OUTPUT-REC := '19'
        //* *#OUTPUT-REC := '+!'
        //* ********************* RL END POS PAY PAYEE MATCH *********************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MICR
        //* ****************** RL END POS-PAY/PAYEE MATCH ***********************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-STMNT-TO-ANNT
        //* ***************************************
        //* *#OUTPUT-REC                         := '1!'
        //* *UTPUT-REC                         := '0!'
        //* *RFORM POSTNET-BARCODE
        //* *#OUTPUT-REC                       := '0!'
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-PYMNT-AMT
        //* *********************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-STMNT
        //* *******************************
        //* *PERFORM POSTNET-BARCODE
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-NAME-ACCT
        //* *TO SUBSTR(#OUTPUT-REC-DETAIL,#I,38)
        //* *TO SUBSTR(#WS-DETAIL,38)
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POSTNET-BARCODE
        //* ********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ADDR-CR
        //* ****************** RL BEGIN PAYEE MATCH ******************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ADDRESS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ADDRESS-LINE
        //* *******************************
        //* ********************* RL END PAYEE MATCH ******************************
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Generate_Check() throws Exception                                                                                                                    //Natural: GENERATE-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  FIRST '1' MEANS STOP POINT
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("12");                                                                                                                                        //Natural: ASSIGN #WS-REC := '12'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pdaFcpa110.getFcpa110_Company_Name());                                                                                          //Natural: ASSIGN #WS-DETAIL := FCPA110.COMPANY-NAME
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        //*  '+' MEANS OVER PRINT
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("+8");                                                                                                                                        //Natural: ASSIGN #WS-REC := '+8'
        pnd_Address_Filler.moveAll("*");                                                                                                                                  //Natural: MOVE ALL '*' TO #ADDRESS-FILLER
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Address_Filler, pdaFcpa110.getFcpa110_Company_Address()));                  //Natural: COMPRESS #ADDRESS-FILLER FCPA110.COMPANY-ADDRESS INTO #WS-DETAIL LEAVING NO
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_Pnd_Ws_Detail,true), new ExamineSearch("*"), new ExamineReplace(" "));                                               //Natural: EXAMINE FULL #WS-DETAIL '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("12");                                                                                                                                        //Natural: ASSIGN #WS-REC := '12'
        //*  RL
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Check_Nbr());                                                           //Natural: MOVE PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //* RL
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                           //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //* RL
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                                                    //Natural: MOVE #WS-PYMNT-CHECK-NBR-N10 TO #WS-DETAIL
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        pnd_Ws_Amount.setValue(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Check_Amt());                                                                                            //Natural: ASSIGN #WS-AMOUNT := PYMNT-CHECK-AMT
        DbsUtil.callnat(Fcpn255.class , getCurrentProcessState(), pnd_Ws_Amount, pnd_Ws_Rec_14_Pnd_Ws_Amt_In_Words_Line_1, pnd_Ws_Rec_15_Pnd_Ws_Amt_In_Words_Line_2);     //Natural: CALLNAT 'FCPN255' #WS-AMOUNT #WS-AMT-IN-WORDS-LINE-1 #WS-AMT-IN-WORDS-LINE-2
        if (condition(Global.isEscape())) return;
        //* *#WS-ORGN-CDE          := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        //* *DECIDE ON FIRST #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        //* *DECIDE ON FIRST #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        //* *  VALUE 'DS'
        //* *    IF CNTRCT-LOB-CDE = 'GRA' OR = 'GSRA'
        //* ***      #WS-ORGN-CDE    := 'LS'
        //* *    END-IF
        //* *  VALUE 'SS'
        //* *    IF CNTRCT-TYPE-CDE = '30' OR = '31'
        //* *      #WS-ORGN-CDE    := 'MX'
        //* *    END-IF
        //* *  NONE
        //* *    IGNORE
        //* *END-DECIDE
        //* *VE EDITED #WS-ORGN-CDE(EM=X.X.) TO #WS-14-ORGN
        pnd_Ws_Rec_14_Pnd_Ws_14_Orgn.setValueEdited(pdaFcpa874c.getPnd_Fcpa874c_Cntrct_Orgn_Cde(),new ReportEditMask("X.X."));                                            //Natural: MOVE EDITED #FCPA874C.CNTRCT-ORGN-CDE ( EM = X.X. ) TO #WS-14-ORGN
        //*  AMOUNT IN WORDS LINE-1
        getWorkFiles().write(8, false, pnd_Ws_Rec_14);                                                                                                                    //Natural: WRITE WORK FILE 8 #WS-REC-14
        //*  AMOUNT IN WORDS LINE-2
        getWorkFiles().write(8, false, pnd_Ws_Rec_15);                                                                                                                    //Natural: WRITE WORK FILE 8 #WS-REC-15
        //* *IF  NOT ( PYMNT-EFT-ACCT-NBR (1) = ' ' OR= '0' )
        if (condition(! (pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr().equals(" ") || pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr().equals("0"))                    //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR = ' ' OR = '0' ) OR PYMNT-NME ( 1 ) = MASK ( 'CR ' )
            || DbsUtil.maskMatches(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(1),"'CR '")))
        {
            //* *  IF  NOT ( PYMNT-EFT-ACCT-NBR (1) = ' ' OR= '0' )
            if (condition(! (pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr().equals(" ") || pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr().equals("0"))))              //Natural: IF NOT ( PYMNT-EFT-ACCT-NBR = ' ' OR = '0' )
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(1), "H'0000'", "A/C", "H'00'",                     //Natural: COMPRESS '*!*********' #FCPA874A.PYMNT-NME ( 1 ) H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR INTO #WS-REC-1
                    pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr()));
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(DbsUtil.compress("*!*********", pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(1)));                                              //Natural: COMPRESS '*!*********' PYMNT-NME ( 1 ) INTO #WS-REC-1
                DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                        //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  03   BCU
        if (condition(! (pdaFcpa874c.getPnd_Fcpa874c_Cntrct_Hold_Cde().equals(" ") || pdaFcpa874c.getPnd_Fcpa874c_Cntrct_Hold_Cde().equals("0"))))                        //Natural: IF NOT ( CNTRCT-HOLD-CDE = ' ' OR = '0' )
        {
            pnd_Ws_Rec_1.setValueEdited(pdaFcpa874c.getPnd_Fcpa874c_Cntrct_Hold_Cde(),new ReportEditMask("�0!���XXXX"));                                                  //Natural: MOVE EDITED CNTRCT-HOLD-CDE ( EM = �0!���XXXX ) TO #WS-REC-1
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" 3");                                                                                                                                  //Natural: ASSIGN #WS-REC-1 = ' 3'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        //* ********* COMMENTED OUT MON JAN 30 4:30 **************
        //* *RESET #OUTPUT-REC
        //* *#OUTPUT-REC := ' !'
        //* *MOVE EDITED PYMNT-CHECK-SEQ-NBR(EM=9999999) TO #PYMNT-SEQ-NBR
        //* *COMPRESS 'S' #PYMNT-SEQ-NBR INTO #OUTPUT-REC-DETAIL LEAVING NO
        //* *WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa110.getFcpa110_Bank_Above_Check_Amt_Nbr().separate(SeparateOption.WithAnyDelimiters, "/", split_Top, split_Bottom);                                        //Natural: SEPARATE FCPA110.BANK-ABOVE-CHECK-AMT-NBR INTO SPLIT-TOP SPLIT-BOTTOM WITH DELIMITER '/'
        split_Bottom.setValue(split_Bottom, MoveOption.RightJustified);                                                                                                   //Natural: MOVE RIGHT SPLIT-BOTTOM TO SPLIT-BOTTOM
        DbsUtil.examine(new ExamineSource(split_Top), new ExamineSearch("  ", true), new ExamineReplace(" "));                                                            //Natural: EXAMINE SPLIT-TOP FOR FULL '  ' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(split_Bottom), new ExamineSearch("  ", true), new ExamineReplace(" "));                                                         //Natural: EXAMINE SPLIT-BOTTOM FOR FULL '  ' REPLACE WITH ' '
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(split_Top);                                                                                            //Natural: ASSIGN #OUTPUT-REC-DETAIL := SPLIT-TOP
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(split_Bottom);                                                                                         //Natural: ASSIGN #OUTPUT-REC-DETAIL := SPLIT-BOTTOM
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        //*  RL FEB 15
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue("PAY");                                                                                                //Natural: ASSIGN #OUTPUT-REC-DETAIL := 'PAY'
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+5'
        pnd_Pay_Amt.setValueEdited(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Check_Amt(),new ReportEditMask("+ZZ,ZZZ,ZZZ,ZZZ.99"));                                               //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = +ZZ,ZZZ,ZZZ,ZZZ.99 ) TO #PAY-AMT
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch("+"), new ExamineReplace("$"));                                                                 //Natural: EXAMINE #PAY-AMT FOR '+' REPLACE '$'
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch(" "), new ExamineReplace("*"));                                                                 //Natural: EXAMINE #PAY-AMT FOR ' ' REPLACE '*'
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch("."), new ExamineReplace(" "));                                                                 //Natural: EXAMINE #PAY-AMT FOR '.' REPLACE ' '
        pnd_Pay_Amt_Edit.setValue(pnd_Pay_Amt, MoveOption.RightJustified);                                                                                                //Natural: MOVE RIGHT #PAY-AMT TO #PAY-AMT-EDIT
        pnd_Pay_Amt_Edit_Pnd_Remove_Stars.setValue(" ");                                                                                                                  //Natural: MOVE ' ' TO #REMOVE-STARS
        //*  MOVE EDITED PYMNT-CHECK-AMT(EM=+Z,ZZZ,ZZ9.99) TO #AMT-ALPHA
        //*  EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
        //*  MOVE ' '                             TO SUBSTR(#AMT-ALPHA,11,1)
        //*  MOVE #AMT-ALPHA                      TO SUBSTR(#OUTPUT-REC,10,15)
        //*  MOVE #PAY-AMT-EDIT                   TO SUBSTR(#OUTPUT-REC,10,20)
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Pay_Amt_Edit);                                                                                     //Natural: MOVE #PAY-AMT-EDIT TO #OUTPUT-REC-DETAIL
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("05");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '05'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress("ACCOUNT:", pdaFcpa110.getFcpa110_Company_Account()));                                //Natural: COMPRESS 'ACCOUNT:' FCPA110.COMPANY-ACCOUNT INTO #OUTPUT-REC-DETAIL
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Not_Valid_Msg());                                                                //Natural: MOVE FCPA110.NOT-VALID-MSG TO #OUTPUT-REC-DETAIL
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        //* *PERFORM POSTNET-BARCODE      /* RL
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
        //*  RL 13
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValueEdited(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Check_Dte(),new ReportEditMask("MM���DD���YYYY"));             //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM���DD���YYYY ) TO #OUTPUT-REC-DETAIL
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Bank_Name());                                                                    //Natural: ASSIGN #OUTPUT-REC-DETAIL := FCPA110.BANK-NAME
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 3");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 3'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Bank_Address1());                                                                //Natural: ASSIGN #OUTPUT-REC-DETAIL := FCPA110.BANK-ADDRESS1
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1", pdaFcpa110.getFcpa110_Bank_Signature_Cde()));            //Natural: COMPRESS '1' FCPA110.BANK-SIGNATURE-CDE INTO #OUTPUT-REC LEAVE NO SPACE
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+3'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(" ");                                                                                                  //Natural: ASSIGN #OUTPUT-REC-DETAIL := ' '
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("13");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '13'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Title());                                                                        //Natural: ASSIGN #OUTPUT-REC-DETAIL := FCPA110.TITLE
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM WRITE-MICR
        sub_Write_Micr();
        if (condition(Global.isEscape())) {return;}
    }
    //*  RL
    private void sub_Write_Micr() throws Exception                                                                                                                        //Natural: WRITE-MICR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        ldaCpol155m.getCpol155m().reset();                                                                                                                                //Natural: RESET CPOL155M
        ldaCpol155m.getCpol155m_Micr_Bank_Cde().setValue(pdaFcpa110.getFcpa110_Bank_Transmission_Cde());                                                                  //Natural: ASSIGN CPOL155M.MICR-BANK-CDE := FCPA110.BANK-TRANSMISSION-CDE
        //*  RL
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Check_Nbr());                                                           //Natural: MOVE PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //* RL
        pnd_Ws_Pymnt_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                           //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        ldaCpol155m.getCpol155m_Micr_Check_Nbr().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                                    //Natural: ASSIGN CPOL155M.MICR-CHECK-NBR := #WS-PYMNT-CHECK-NBR-N10
        ldaCpol155m.getCpol155m_Micr_Bank_Cde().setValue(pdaFcpa110.getFcpa110_Bank_Transmission_Cde());                                                                  //Natural: ASSIGN CPOL155M.MICR-BANK-CDE := FCPA110.BANK-TRANSMISSION-CDE
        ldaCpol155m.getCpol155m_Micr_Bank_Routing().setValue(pdaFcpa110.getFcpa110_Routing_No());                                                                         //Natural: ASSIGN CPOL155M.MICR-BANK-ROUTING := FCPA110.ROUTING-NO
        ldaCpol155m.getCpol155m_Micr_Bank_Account().setValue(pdaFcpa110.getFcpa110_Account_No());                                                                         //Natural: ASSIGN CPOL155M.MICR-BANK-ACCOUNT := FCPA110.ACCOUNT-NO
        DbsUtil.callnat(Cpon155m.class , getCurrentProcessState(), ldaCpol155m.getCpol155m());                                                                            //Natural: CALLNAT 'CPON155M' CPOL155M
        if (condition(Global.isEscape())) return;
        pnd_Ws_Rec.reset();                                                                                                                                               //Natural: RESET #WS-REC
        pnd_Ws_Rec.setValue("19");                                                                                                                                        //Natural: ASSIGN #WS-REC := '19'
        pnd_Ws_Rec_Pnd_Ws_Detail.setValue(ldaCpol155m.getCpol155m_Micr_Micr_Line());                                                                                      //Natural: ASSIGN #WS-DETAIL := CPOL155M.MICR-MICR-LINE
        getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                       //Natural: WRITE WORK FILE 8 #WS-REC
        //*  WRITE-MICR
    }
    //*  RL
    //*  RL
    private void sub_Generate_Stmnt_To_Annt() throws Exception                                                                                                            //Natural: GENERATE-STMNT-TO-ANNT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(1);                                                                                                             //Natural: ASSIGN #ADDR-IND := 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("17");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '17'
        //*  RL
        //*  RL
        //*  RL  FEB 23
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDR-CR
        sub_Format_Addr_Cr();
        if (condition(Global.isEscape())) {return;}
        pnd_I.setValue(5);                                                                                                                                                //Natural: ASSIGN #I := 5
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("07");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '07'
        //* RL
        //*  RL  FEB 23
                                                                                                                                                                          //Natural: PERFORM FORMAT-NAME-ACCT
        sub_Format_Name_Acct();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(2);                                                                                                             //Natural: ASSIGN #ADDR-IND := 2
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("07");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '07'
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDR-CR
        sub_Format_Addr_Cr();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Format_Pymnt_Amt() throws Exception                                                                                                                  //Natural: FORMAT-PYMNT-AMT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803c.getPnd_Fcpl803c_Pnd_Check_Amt_Text());                                                            //Natural: ASSIGN #OUTPUT-REC := #CHECK-AMT-TEXT
        pnd_Ws_Chk_Amt_Pnd_Amt_Alpha.setValueEdited(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Check_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                   //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
        DbsUtil.examine(new ExamineSource(pnd_Ws_Chk_Amt_Pnd_Amt_Alpha), new ExamineSearch("+"), new ExamineReplace("$"));                                                //Natural: EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
        pnd_Ws_Chk_Amt_Pnd_Amt_Alpha.setValue(pnd_Ws_Chk_Amt_Pnd_Amt_Alpha, MoveOption.LeftJustified);                                                                    //Natural: MOVE LEFT #AMT-ALPHA TO #AMT-ALPHA
        setValueToSubstring(pnd_Ws_Chk_Amt_Pnd_Amt_Alpha,pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),18,15);                                                               //Natural: MOVE #AMT-ALPHA TO SUBSTR ( #OUTPUT-REC,18,15 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Generate_Stmnt() throws Exception                                                                                                                    //Natural: GENERATE-STMNT
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803c.getPnd_Fcpl803c_Pnd_Stmnt_Text().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Text_Ind(),     //Natural: ASSIGN #OUTPUT-REC := #STMNT-TEXT ( #STMNT-TEXT-IND,#I )
                pnd_I));
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  RL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("0!");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '0!'
        //* *#OUTPUT-REC                         := '0!'  /* ?
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Format_Name_Acct() throws Exception                                                                                                                  //Natural: FORMAT-NAME-ACCT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr().notEquals(" ") || pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()).getSubstring(1, //Natural: IF PYMNT-EFT-ACCT-NBR NE ' ' OR SUBSTR ( #FCPA874A.PYMNT-NME ( #ADDR-IND ) ,1,3 ) = 'CR '
            3).equals("CR ")))
        {
            //*  RL
            getReports().write(0, Global.getPROGRAM(),"=",pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()),NEWLINE,"=",pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()),NEWLINE,"=",pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),  //Natural: WRITE *PROGRAM '=' #FCPA874A.PYMNT-NME ( #ADDR-IND ) / '=' #FCPA874A.PYMNT-NME ( #ADDR-IND ) / '='#OUTPUT-REC-DETAIL ( AL = 41 ) /
                new AlphanumericLength (41),NEWLINE);
            if (Global.isEscape()) return;
            //* *MOVE '0!' TO #WS-REC
            pnd_Ws_Rec.setValue("0!");                                                                                                                                    //Natural: MOVE '0!' TO #WS-REC
            //*  RL FEB 23
            setValueToSubstring(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()),pnd_Ws_Rec_Pnd_Ws_Detail,5);                   //Natural: MOVE #FCPA874A.PYMNT-NME ( #ADDR-IND ) TO SUBSTR ( #WS-DETAIL,5 )
            if (condition(pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr().notEquals(" ")))                                                                               //Natural: IF PYMNT-EFT-ACCT-NBR NE ' '
            {
                //*  RL
                //*  RL FEB13
                pnd_Ws_Rec_Pnd_Ws_Detail.setValue(DbsUtil.compress(pnd_Ws_Rec_Pnd_Ws_Detail, "H'0000'", "A/C", "H'00'", pdaFcpa874c.getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr())); //Natural: COMPRESS #WS-DETAIL H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR INTO #WS-DETAIL
                //* *  COMPRESS #OUTPUT-REC-DETAIL H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR
                //* *      INTO #OUTPUT-REC-DETAIL  /* RL FEB13
            }                                                                                                                                                             //Natural: END-IF
            //*  RL FEB14
            getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                   //Natural: WRITE WORK FILE 8 #WS-REC
            pnd_Ws_Rec.reset();                                                                                                                                           //Natural: RESET #WS-REC
            //*  RL FEB 14
            pnd_Ws_Rec.setValue("18");                                                                                                                                    //Natural: MOVE '18' TO #WS-REC
            //*  RL FEB14
            getWorkFiles().write(8, false, pnd_Ws_Rec);                                                                                                                   //Natural: WRITE WORK FILE 8 #WS-REC
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Postnet_Barcode() throws Exception                                                                                                                   //Natural: POSTNET-BARCODE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803p.getPnd_Fcpa803p_Pymnt_Postl_Data().setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Postl_Data().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()));       //Natural: ASSIGN #FCPA803P.PYMNT-POSTL-DATA := #FCPA874A.PYMNT-POSTL-DATA ( #ADDR-IND )
        DbsUtil.callnat(Fcpn803p.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803p.getPnd_Fcpa803p());                                           //Natural: CALLNAT 'FCPN803P' USING #FCPA803 #FCPA803P
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
    }
    //*  RL
    private void sub_Format_Addr_Cr() throws Exception                                                                                                                    //Natural: FORMAT-ADDR-CR
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  RL
        DbsUtil.callnat(Fcpn874a.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa874a.getPnd_Fcpa874a());                                           //Natural: CALLNAT 'FCPN874A' USING #FCPA803 #FCPA874A
        if (condition(Global.isEscape())) return;
        //*  FORMAT-ADDR-CR
    }
    private void sub_Format_Address() throws Exception                                                                                                                    //Natural: FORMAT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //* *CALLNAT 'FCPN874A' USING #FCPA803 #FCPA874A /* RL
        //* ****************** RL BEGIN PAYEE MATCH ******************************
        pnd_Ws_Address_Lines_Printed.reset();                                                                                                                             //Natural: RESET #WS-ADDRESS-LINES-PRINTED
        pnd_Ws_Index.setValue(1);                                                                                                                                         //Natural: ASSIGN #WS-INDEX = 1
        if (condition(DbsUtil.maskMatches(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(pnd_Ws_Index),"'CR '")))                                                       //Natural: IF PYMNT-NME ( #WS-INDEX ) = MASK ( 'CR ' )
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1!***", pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(1,                  //Natural: COMPRESS '1!***' PYMNT-ADDR-LINE-TXT ( 1,1 ) INTO #WS-REC-1 LEAVING NO SPACE
                1)));
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  RL PAYEE MATCH
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1!***", pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Nme().getValue(1)));                         //Natural: COMPRESS '1!***' PYMNT-NME ( 1 ) INTO #WS-REC-1 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
            //* RL
            pnd_Ws_Address.setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(1,1), MoveOption.LeftJustified);                                           //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE-TXT ( 1,1 ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS-LINE
            sub_Format_Address_Line();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        }                                                                                                                                                                 //Natural: END-IF
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(1,2), MoveOption.LeftJustified);                                               //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE-TXT ( 1,2 ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS-LINE
        sub_Format_Address_Line();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(1,3), MoveOption.LeftJustified);                                               //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE-TXT ( 1,3 ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS-LINE
        sub_Format_Address_Line();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(1,4), MoveOption.LeftJustified);                                               //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE-TXT ( 1,4 ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS-LINE
        sub_Format_Address_Line();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        //* RL
        pnd_Ws_Address.setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(1,5), MoveOption.LeftJustified);                                               //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE-TXT ( 1,5 ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS-LINE
        sub_Format_Address_Line();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Address_Lines_Printed.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-ADDRESS-LINES-PRINTED
        if (condition(pnd_Ws_Address_Lines_Printed.equals(6)))                                                                                                            //Natural: IF #WS-ADDRESS-LINES-PRINTED = 6
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* RL
            pnd_Ws_Address.setValue(pdaFcpa874a.getPnd_Fcpa874a_Pymnt_Addr_Line_Txt().getValue(1,6), MoveOption.LeftJustified);                                           //Natural: MOVE LEFT JUSTIFIED PYMNT-ADDR-LINE-TXT ( 1,6 ) TO #WS-ADDRESS
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS-LINE
            sub_Format_Address_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Address_Line() throws Exception                                                                                                               //Natural: FORMAT-ADDRESS-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  RL PAYEE MATCH
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "*!***", pnd_Ws_Address));                                                                  //Natural: COMPRESS '*!***' #WS-ADDRESS INTO #WS-REC-1 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Rec_1), new ExamineSearch("*"), new ExamineReplace(" "));                                                                //Natural: EXAMINE #WS-REC-1 '*' REPLACE WITH ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
