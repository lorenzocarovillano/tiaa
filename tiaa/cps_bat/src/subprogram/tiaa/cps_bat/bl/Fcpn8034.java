/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:34 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn8034
************************************************************
**        * FILE NAME            : Fcpn8034.java
**        * CLASS NAME           : Fcpn8034
**        * INSTANCE NAME        : Fcpn8034
************************************************************
**********************************************************************
*
* SUBPROGRAM : FCPN8034
*
**********************************************************************
*
* NOTE: THIS PROGRAM HAS BEEN CLONED FROM FCPN8033 TO RUN IN THE 1400
*       JOBSTREAM.  PLEASE REVIEW BOTH MODULES WHEN MAKING MODIFICATIONS
*       TO EITHER ONE OF THEM.  AER 6/2/08
*
**********************************************************************
*
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
*
* FUNCTION   : "AP" ANNUITANT STATEMENTS - CHECK PART.
* HISTORY    :
*  04/19/03   R. CARREON - RESTOW. FCPA800 EXPANDED FCPL803L WAS CHANGED
*
* 11/14/2005 RAMANA ANNE - CHANGE CODING FOR CHECK PART AND COMMENTED
*                    POSTNET BARCODE ROUTINE FOR PAYEE-MATCH PROJECT
*                    ADDED NEW PARAMETER AREAS FOR N10 CHECK NO
*                    ADDED NEW CALLNAT FOR MICA LINE ROUTINE CPOP155M
*
*  6/2/08 : AER - ROTH-MAJOR1 - CLONED FROM FCPN8033.  CHANGED FCPA800
*                 TO FCPA800D
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
**********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn8034 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa803a pdaFcpa803a;
    private PdaFcpa803c pdaFcpa803c;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpa803p pdaFcpa803p;
    private LdaFcpl803c ldaFcpl803c;
    private LdaCpol155m ldaCpol155m;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Work_Amt;
    private DbsField pnd_Ws_Pnd_Amt_Alpha;
    private DbsField pnd_Ws_Pnd_Amt_Text;
    private DbsField pnd_Pay_Amt;
    private DbsField pnd_Pay_Amt_Edit;

    private DbsGroup pnd_Pay_Amt_Edit__R_Field_1;
    private DbsField pnd_Pay_Amt_Edit_Pnd_Remove_Stars;
    private DbsField pnd_Err_Msg;
    private DbsField split_Top;
    private DbsField split_Bottom;
    private DbsField pnd_Address_Filler;
    private DbsField pnd_Pymnt_Seq_Nbr;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa803p = new PdaFcpa803p(localVariables);
        ldaFcpl803c = new LdaFcpl803c();
        registerRecord(ldaFcpl803c);
        ldaCpol155m = new LdaCpol155m();
        registerRecord(ldaCpol155m);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(parameters);
        pdaFcpa803 = new PdaFcpa803(parameters);
        pdaFcpa803a = new PdaFcpa803a(parameters);
        pdaFcpa803c = new PdaFcpa803c(parameters);
        pdaFcpa110 = new PdaFcpa110(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Work_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Work_Amt", "#WORK-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Ws_Pnd_Amt_Alpha = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Alpha", "#AMT-ALPHA", FieldType.STRING, 15);
        pnd_Ws_Pnd_Amt_Text = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Amt_Text", "#AMT-TEXT", FieldType.STRING, 132, new DbsArrayController(1, 2));
        pnd_Pay_Amt = localVariables.newFieldInRecord("pnd_Pay_Amt", "#PAY-AMT", FieldType.STRING, 17);
        pnd_Pay_Amt_Edit = localVariables.newFieldInRecord("pnd_Pay_Amt_Edit", "#PAY-AMT-EDIT", FieldType.STRING, 20);

        pnd_Pay_Amt_Edit__R_Field_1 = localVariables.newGroupInRecord("pnd_Pay_Amt_Edit__R_Field_1", "REDEFINE", pnd_Pay_Amt_Edit);
        pnd_Pay_Amt_Edit_Pnd_Remove_Stars = pnd_Pay_Amt_Edit__R_Field_1.newFieldInGroup("pnd_Pay_Amt_Edit_Pnd_Remove_Stars", "#REMOVE-STARS", FieldType.STRING, 
            6);
        pnd_Err_Msg = localVariables.newFieldInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 70);
        split_Top = localVariables.newFieldInRecord("split_Top", "SPLIT-TOP", FieldType.STRING, 6);
        split_Bottom = localVariables.newFieldInRecord("split_Bottom", "SPLIT-BOTTOM", FieldType.STRING, 5);
        pnd_Address_Filler = localVariables.newFieldInRecord("pnd_Address_Filler", "#ADDRESS-FILLER", FieldType.STRING, 22);
        pnd_Pymnt_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Seq_Nbr", "#PYMNT-SEQ-NBR", FieldType.STRING, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803c.initializeValues();
        ldaCpol155m.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn8034() throws Exception
    {
        super("Fcpn8034");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803c.getPnd_Fcpl803c_Pnd_Check_Amt_Text());                                                            //Natural: ASSIGN #OUTPUT-REC := #CHECK-AMT-TEXT
        pnd_Ws_Pnd_Amt_Alpha.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(),new ReportEditMask("+Z,ZZZ,ZZ9.99"));                                      //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = +Z,ZZZ,ZZ9.99 ) TO #AMT-ALPHA
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Amt_Alpha), new ExamineSearch("+"), new ExamineReplace("$"));                                                        //Natural: EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
        pnd_Ws_Pnd_Amt_Alpha.setValue(pnd_Ws_Pnd_Amt_Alpha, MoveOption.LeftJustified);                                                                                    //Natural: MOVE LEFT #AMT-ALPHA TO #AMT-ALPHA
        setValueToSubstring(pnd_Ws_Pnd_Amt_Alpha,pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),18,15);                                                                       //Natural: MOVE #AMT-ALPHA TO SUBSTR ( #OUTPUT-REC,18,15 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))                                                                                                //Natural: IF #STMNT
        {
                                                                                                                                                                          //Natural: PERFORM GENERATE-STMNT
            sub_Generate_Stmnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM GENERATE-CHECK
            sub_Generate_Check();
            if (condition(Global.isEscape())) {return;}
            //*  RL
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+3'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-CHECK
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MICR
        //* ***********************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-STMNT
        //* *******************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POSTNET-BARCODE
        //* ********************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-ADDRESS
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Generate_Check() throws Exception                                                                                                                    //Natural: GENERATE-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  RA START
        //*  FIRST '1' MEANS STOP POINT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("12");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '12'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Company_Name());                                                                 //Natural: ASSIGN #OUTPUT-REC-DETAIL := FCPA110.COMPANY-NAME
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        //*  '+' MEANS OVER PRINT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+8");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+8'
        pnd_Address_Filler.moveAll("*");                                                                                                                                  //Natural: MOVE ALL '*' TO #ADDRESS-FILLER
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Address_Filler, pdaFcpa110.getFcpa110_Company_Address())); //Natural: COMPRESS #ADDRESS-FILLER FCPA110.COMPANY-ADDRESS INTO #OUTPUT-REC-DETAIL LEAVING NO
        DbsUtil.examine(new ExamineSource(pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),true), new ExamineSearch("*"), new ExamineReplace(" "));                      //Natural: EXAMINE FULL #OUTPUT-REC-DETAIL '*' REPLACE WITH ' '
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("12");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '12'
        //*  IF FCPA110-SOURCE-CODE =
        //*  RL POPULATED W 1400 CHK NBR FROM 876 FILE IN FCPN803B/8033 TEST
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());                                                     //Natural: MOVE #CHECK-SORT-FIELDS.PYMNT-NBR TO #OUTPUT-REC-DETAIL
        getReports().write(0, Global.getPROGRAM(),"=",pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());                                                                  //Natural: WRITE *PROGRAM '=' #CHECK-SORT-FIELDS.PYMNT-NBR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Work_Amt.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt());                                                                                 //Natural: ASSIGN #WORK-AMT := PYMNT-CHECK-AMT
        DbsUtil.callnat(Fcpn255.class , getCurrentProcessState(), pnd_Ws_Pnd_Work_Amt, pnd_Ws_Pnd_Amt_Text.getValue(1), pnd_Ws_Pnd_Amt_Text.getValue(2));                 //Natural: CALLNAT 'FCPN255' USING #WORK-AMT #AMT-TEXT ( 1 ) #AMT-TEXT ( 2 )
        if (condition(Global.isEscape())) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("1!");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '1!'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Ws_Pnd_Amt_Text.getValue(1));                                                                      //Natural: MOVE #AMT-TEXT ( 1 ) TO #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        //*  SPACE MEANS ONE BLANK
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" !");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' !'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Ws_Pnd_Amt_Text.getValue(2));                                                                      //Natural: MOVE #AMT-TEXT ( 2 ) TO #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr().notEquals(" ") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1).getSubstring(1,    //Natural: IF PYMNT-EFT-ACCT-NBR NE ' ' OR SUBSTR ( WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ,1,3 ) = 'CR '
            3).equals("CR ")))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                           //Natural: RESET #OUTPUT-REC
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" !");                                                                                                    //Natural: ASSIGN #OUTPUT-REC := ' !'
            setValueToSubstring(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),13,38);                        //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) TO SUBSTR ( #OUTPUT-REC-DETAIL,13,38 )
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr().notEquals(" ")))                                                                          //Natural: IF PYMNT-EFT-ACCT-NBR NE ' '
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(), "H'0000'",                 //Natural: COMPRESS #OUTPUT-REC-DETAIL H'0000' 'A/C' H'00' PYMNT-EFT-ACCT-NBR INTO #OUTPUT-REC-DETAIL
                    "A/C", "H'00'", pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr()));
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().reset();                                                                                                //Natural: RESET #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail(),13,38);                    //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) TO SUBSTR ( #OUTPUT-REC-DETAIL,13,38 )
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
                pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().reset();                                                                                                //Natural: RESET #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
                sub_Fcpc803w();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().reset();                                                                                                    //Natural: RESET #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" !");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' !'
        pnd_Pymnt_Seq_Nbr.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr(),new ReportEditMask("9999999"));                                           //Natural: MOVE EDITED PYMNT-CHECK-SEQ-NBR ( EM = 9999999 ) TO #PYMNT-SEQ-NBR
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "S", pnd_Pymnt_Seq_Nbr));                              //Natural: COMPRESS 'S' #PYMNT-SEQ-NBR INTO #OUTPUT-REC-DETAIL LEAVING NO
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" !");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' !'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde(),new ReportEditMask("X.X."));                  //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE ( EM = X.X. ) TO #OUTPUT-REC-DETAIL
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().notEquals(" ")))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE NE ' '
        {
            setValueToSubstring(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde(),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(),7,4);                                       //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE TO SUBSTR ( #OUTPUT-REC,7,4 )
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa110.getFcpa110_Bank_Above_Check_Amt_Nbr().separate(SeparateOption.WithAnyDelimiters, "/", split_Top, split_Bottom);                                        //Natural: SEPARATE FCPA110.BANK-ABOVE-CHECK-AMT-NBR INTO SPLIT-TOP SPLIT-BOTTOM WITH DELIMITER '/'
        split_Bottom.setValue(split_Bottom, MoveOption.RightJustified);                                                                                                   //Natural: MOVE RIGHT SPLIT-BOTTOM TO SPLIT-BOTTOM
        DbsUtil.examine(new ExamineSource(split_Top), new ExamineSearch("  ", true), new ExamineReplace(" "));                                                            //Natural: EXAMINE SPLIT-TOP FOR FULL '  ' REPLACE WITH ' '
        DbsUtil.examine(new ExamineSource(split_Bottom), new ExamineSearch("  ", true), new ExamineReplace(" "));                                                         //Natural: EXAMINE SPLIT-BOTTOM FOR FULL '  ' REPLACE WITH ' '
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(split_Top);                                                                                            //Natural: ASSIGN #OUTPUT-REC-DETAIL := SPLIT-TOP
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(split_Bottom);                                                                                         //Natural: ASSIGN #OUTPUT-REC-DETAIL := SPLIT-BOTTOM
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue("PAY");                                                                                                //Natural: ASSIGN #OUTPUT-REC-DETAIL := 'PAY'
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+5'
        pnd_Pay_Amt.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(),new ReportEditMask("+ZZ,ZZZ,ZZZ,ZZZ.99"));                                          //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = +ZZ,ZZZ,ZZZ,ZZZ.99 ) TO #PAY-AMT
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch("+"), new ExamineReplace("$"));                                                                 //Natural: EXAMINE #PAY-AMT FOR '+' REPLACE '$'
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch(" "), new ExamineReplace("*"));                                                                 //Natural: EXAMINE #PAY-AMT FOR ' ' REPLACE '*'
        DbsUtil.examine(new ExamineSource(pnd_Pay_Amt), new ExamineSearch("."), new ExamineReplace(" "));                                                                 //Natural: EXAMINE #PAY-AMT FOR '.' REPLACE ' '
        pnd_Pay_Amt_Edit.setValue(pnd_Pay_Amt, MoveOption.RightJustified);                                                                                                //Natural: MOVE RIGHT #PAY-AMT TO #PAY-AMT-EDIT
        pnd_Pay_Amt_Edit_Pnd_Remove_Stars.setValue(" ");                                                                                                                  //Natural: MOVE ' ' TO #REMOVE-STARS
        //*  MOVE EDITED PYMNT-CHECK-AMT(EM=+Z,ZZZ,ZZ9.99) TO #AMT-ALPHA
        //*  EXAMINE #AMT-ALPHA FOR '+' REPLACE '$'
        //*  MOVE ' '                             TO SUBSTR(#AMT-ALPHA,11,1)
        //*  MOVE #AMT-ALPHA                      TO SUBSTR(#OUTPUT-REC,10,15)
        //*  MOVE #PAY-AMT-EDIT                   TO SUBSTR(#OUTPUT-REC,10,20)
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pnd_Pay_Amt_Edit);                                                                                     //Natural: MOVE #PAY-AMT-EDIT TO #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("05");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '05'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(DbsUtil.compress("ACCOUNT:", pdaFcpa110.getFcpa110_Company_Account()));                                //Natural: COMPRESS 'ACCOUNT:' FCPA110.COMPANY-ACCOUNT INTO #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 5");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 5'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Not_Valid_Msg());                                                                //Natural: MOVE FCPA110.NOT-VALID-MSG TO #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        //*  PERFORM POSTNET-BARCODE      /* RA
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("MM���DD���YYYY"));        //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM���DD���YYYY ) TO #OUTPUT-REC-DETAIL
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("15");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '15'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Bank_Name());                                                                    //Natural: ASSIGN #OUTPUT-REC-DETAIL := FCPA110.BANK-NAME
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" 3");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := ' 3'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Bank_Address1());                                                                //Natural: ASSIGN #OUTPUT-REC-DETAIL := FCPA110.BANK-ADDRESS1
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1", pdaFcpa110.getFcpa110_Bank_Signature_Cde()));            //Natural: COMPRESS '1' FCPA110.BANK-SIGNATURE-CDE INTO #OUTPUT-REC LEAVE NO SPACE
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("+3");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '+3'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(" ");                                                                                                  //Natural: ASSIGN #OUTPUT-REC-DETAIL := ' '
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("13");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '13'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(pdaFcpa110.getFcpa110_Title());                                                                        //Natural: ASSIGN #OUTPUT-REC-DETAIL := FCPA110.TITLE
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-MICR
        sub_Write_Micr();
        if (condition(Global.isEscape())) {return;}
        //*                                   /* END RA
    }
    private void sub_Write_Micr() throws Exception                                                                                                                        //Natural: WRITE-MICR
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Err_Msg.setValue("CPOP155 - CPSN155M MICR FORMAT ABEND");                                                                                                     //Natural: ASSIGN #ERR-MSG := 'CPOP155 - CPSN155M MICR FORMAT ABEND'
        //*  CHECK NO
        ldaCpol155m.getCpol155m().reset();                                                                                                                                //Natural: RESET CPOL155M
        ldaCpol155m.getCpol155m_Micr_Bank_Cde().setValue(pdaFcpa110.getFcpa110_Bank_Transmission_Cde());                                                                  //Natural: ASSIGN CPOL155M.MICR-BANK-CDE := FCPA110.BANK-TRANSMISSION-CDE
        ldaCpol155m.getCpol155m_Micr_Check_Nbr().setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());                                                              //Natural: ASSIGN CPOL155M.MICR-CHECK-NBR := #CHECK-SORT-FIELDS.PYMNT-NBR
        ldaCpol155m.getCpol155m_Micr_Bank_Routing().setValue(pdaFcpa110.getFcpa110_Routing_No());                                                                         //Natural: ASSIGN CPOL155M.MICR-BANK-ROUTING := FCPA110.ROUTING-NO
        ldaCpol155m.getCpol155m_Micr_Bank_Account().setValue(pdaFcpa110.getFcpa110_Account_No());                                                                         //Natural: ASSIGN CPOL155M.MICR-BANK-ACCOUNT := FCPA110.ACCOUNT-NO
        DbsUtil.callnat(Cpon155m.class , getCurrentProcessState(), ldaCpol155m.getCpol155m());                                                                            //Natural: CALLNAT 'CPON155M' CPOL155M
        if (condition(Global.isEscape())) return;
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue("19");                                                                                                        //Natural: ASSIGN #OUTPUT-REC := '19'
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec_Detail().setValue(ldaCpol155m.getCpol155m_Micr_Micr_Line());                                                             //Natural: ASSIGN #OUTPUT-REC-DETAIL := CPOL155M.MICR-MICR-LINE
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        pnd_Err_Msg.setValue(" ");                                                                                                                                        //Natural: ASSIGN #ERR-MSG := ' '
        //*  WRITE-MICR
    }
    private void sub_Generate_Stmnt() throws Exception                                                                                                                    //Natural: GENERATE-STMNT
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(5)); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(ldaFcpl803c.getPnd_Fcpl803c_Pnd_Stmnt_Text().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Text_Ind(),     //Natural: ASSIGN #OUTPUT-REC := #STMNT-TEXT ( #STMNT-TEXT-IND,#I )
                pnd_Ws_Pnd_I));
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  PERFORM POSTNET-BARCODE  /* RA
                                                                                                                                                                          //Natural: PERFORM FORMAT-ADDRESS
        sub_Format_Address();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Postnet_Barcode() throws Exception                                                                                                                   //Natural: POSTNET-BARCODE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa803p.getPnd_Fcpa803p_Pymnt_Postl_Data().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Postl_Data().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind()));  //Natural: ASSIGN #FCPA803P.PYMNT-POSTL-DATA := WF-PYMNT-ADDR-GRP.PYMNT-POSTL-DATA ( #ADDR-IND )
        DbsUtil.callnat(Fcpn803p.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803p.getPnd_Fcpa803p());                                           //Natural: CALLNAT 'FCPN803P' USING #FCPA803 #FCPA803P
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Format_Address() throws Exception                                                                                                                    //Natural: FORMAT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pdaFcpa803a.getPnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp().setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind())); //Natural: MOVE BY NAME WF-PYMNT-ADDR-GRP.PYMNT-NME-AND-ADDR-GRP ( #ADDR-IND ) TO #FCPA803A.PYMNT-NME-AND-ADDR-GRP
        DbsUtil.callnat(Fcpn803a.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803a.getPnd_Fcpa803a());                                           //Natural: CALLNAT 'FCPN803A' USING #FCPA803 #FCPA803A
        if (condition(Global.isEscape())) return;
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
