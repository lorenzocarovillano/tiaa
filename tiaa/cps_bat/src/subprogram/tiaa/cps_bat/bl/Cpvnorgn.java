/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:10:15 PM
**        * FROM NATURAL SUBPROGRAM : Cpvnorgn
************************************************************
**        * FILE NAME            : Cpvnorgn.java
**        * CLASS NAME           : Cpvnorgn
**        * INSTANCE NAME        : Cpvnorgn
************************************************************
************************************************************************
** MODULE      :  CPONORGN                                            **
** SYSTEM      :  CONSOLIDATED PAYMENT SYSTEM                         **
**             :  OPEN INVESTMENTS ARCHITECTURE                       **
**             :                                                      **
** DESCRIPTION :  THIS SUB-PROGRAM WILL PREPARE THE ORIGIN CODE       **
**             :  WORKING STORAGE FOR THE CALLING MODULE.             **
**             :                                                      **
** IMPORTANT   :  IF ORIGIN TOTALS WILL BE CALCULATED AND/OR PASSED,  **
**             :  PDA 'CPOATOTL' SHOULD BE USED.                      **
**             :                                                      **
** HISTORY     :                                                      **
** 02/28/2002  :  ALTHEA A. YOUNG - CREATED.                          **
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpvnorgn extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCpoaorgn pdaCpoaorgn;
    private PdaCpoa100 pdaCpoa100;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Max_Codes;
    private DbsField pnd_Terminate;

    private DbsGroup pnd_Terminate__R_Field_1;
    private DbsField pnd_Terminate_Pnd_Terminate_A;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpoa100 = new PdaCpoa100(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaCpoaorgn = new PdaCpoaorgn(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Max_Codes = localVariables.newFieldInRecord("pnd_Max_Codes", "#MAX-CODES", FieldType.NUMERIC, 4);
        pnd_Terminate = localVariables.newFieldInRecord("pnd_Terminate", "#TERMINATE", FieldType.NUMERIC, 4);

        pnd_Terminate__R_Field_1 = localVariables.newGroupInRecord("pnd_Terminate__R_Field_1", "REDEFINE", pnd_Terminate);
        pnd_Terminate_Pnd_Terminate_A = pnd_Terminate__R_Field_1.newFieldInGroup("pnd_Terminate_Pnd_Terminate_A", "#TERMINATE-A", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max_Codes.setInitialValue(20);
        pnd_Terminate.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cpvnorgn() throws Exception
    {
        super("Cpvnorgn");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  --------------------- M A I N   P R O C E S S ---------------------- *
                                                                                                                                                                          //Natural: PERFORM A10-INITIALIZATION
        sub_A10_Initialization();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM A20-ORIGIN-CODE-PROCESSING
        sub_A20_Origin_Code_Processing();
        if (condition(Global.isEscape())) {return;}
        pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used().setValue(pdaCpoaorgn.getCpoaorgn_Pnd_O());                                                                                //Natural: ASSIGN #ORGN-USED := #O
        //*  --------------------- S U B - R O U T I N E S ---------------------- *
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A10-INITIALIZATION
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A20-ORIGIN-CODE-PROCESSING
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: B10-ERROR-PROCESSING
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_A10_Initialization() throws Exception                                                                                                                //Natural: A10-INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------- *
        pdaCpoaorgn.getCpoaorgn().reset();                                                                                                                                //Natural: RESET CPOAORGN CPOA100 ( * )
        pdaCpoa100.getCpoa100().getValue("*").reset();
        pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Max().setValue(pnd_Max_Codes);                                                                                                   //Natural: ASSIGN CPOAORGN.#ORGN-MAX := #MAX-CODES
        pdaCpoa100.getCpoa100_Cpoa100_Function().setValue("NEXT");                                                                                                        //Natural: ASSIGN CPOA100-FUNCTION := 'NEXT'
        pdaCpoa100.getCpoa100_Cpoa100_Table_Id().setValue("CPARM");                                                                                                       //Natural: ASSIGN CPOA100-TABLE-ID := 'CPARM'
        pdaCpoa100.getCpoa100_Cpoa100_Short_Key().setValue("PRO ORIGIN CODES");                                                                                           //Natural: ASSIGN CPOA100-SHORT-KEY := 'PRO ORIGIN CODES'
        pdaCpoa100.getCpoa100_Cpoa100_Long_Key().setValue("A");                                                                                                           //Natural: ASSIGN CPOA100-LONG-KEY := 'A'
        pdaCpoa100.getCpoa100_Cpoa100_Super_Ind().setValue("L");                                                                                                          //Natural: ASSIGN CPOA100-SUPER-IND := 'L'
        //*  A10-INITIALIZATION
    }
    private void sub_A20_Origin_Code_Processing() throws Exception                                                                                                        //Natural: A20-ORIGIN-CODE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------- *
        //*  GET ORIGIN CODES
        ORIGIN:                                                                                                                                                           //Natural: REPEAT
        while (condition(whileTrue))
        {
            DbsUtil.callnat(Cpon100.class , getCurrentProcessState(), pdaCpoa100.getCpoa100().getValue("*"));                                                             //Natural: CALLNAT 'CPON100' CPOA100 ( * )
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            //*  END OF TABLE
            if (condition(pdaCpoa100.getCpoa100_Cpoa100_Function().equals("END")))                                                                                        //Natural: IF CPOA100-FUNCTION EQ 'END'
            {
                if (true) break ORIGIN;                                                                                                                                   //Natural: ESCAPE BOTTOM ( ORIGIN. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*  PARTIAL MATCH
            //*  #O MUST NOT OVERFLOW
            //*  NUM. OF ORIGIN CODES
            if (condition((pdaCpoa100.getCpoa100_Cpoa100_Return_Code().equals("02")) && (pdaCpoaorgn.getCpoaorgn_Pnd_O().less(pnd_Max_Codes))))                           //Natural: IF ( CPOA100-RETURN-CODE = '02' ) AND ( #O < #MAX-CODES )
            {
                pdaCpoaorgn.getCpoaorgn_Pnd_O().nadd(1);                                                                                                                  //Natural: ASSIGN #O := #O + 1
                pdaCpoaorgn.getCpoaorgn_Pnd_Rt_Long_Key().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).setValue(pdaCpoa100.getCpoa100_Rt_Long_Key());                        //Natural: ASSIGN CPOAORGN.#RT-LONG-KEY ( #O ) := CPOA100-LONG-KEY := CPOA100.RT-LONG-KEY
                pdaCpoa100.getCpoa100_Cpoa100_Long_Key().setValue(pdaCpoa100.getCpoa100_Rt_Long_Key());
                pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_Desc_Mc().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).setValue(pdaCpoa100.getCpoa100_Rt_Desc1());                      //Natural: ASSIGN #ORGN-CDE-DESC-MC ( #O ) := CPOA100.RT-DESC1
                pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_Bus_Unit().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).setValue(pdaCpoa100.getCpoa100_Rt_Desc2());                     //Natural: ASSIGN #ORGN-CDE-BUS-UNIT ( #O ) := CPOA100.RT-DESC2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM B10-ERROR-PROCESSING
                sub_B10_Error_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("ORIGIN"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("ORIGIN"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Terminate_Pnd_Terminate_A.setValue(pdaCpoa100.getCpoa100_Cpoa100_Return_Code());                                                                      //Natural: ASSIGN #TERMINATE-A := CPOA100-RETURN-CODE
                DbsUtil.terminate(pnd_Terminate);  if (true) return;                                                                                                      //Natural: TERMINATE #TERMINATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  A20-ORIGIN-CODE-PROCESSING
    }
    private void sub_B10_Error_Processing() throws Exception                                                                                                              //Natural: B10-ERROR-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------- *
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        //*  NO MATCH
        if (condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().equals(getZero())))                                                                                                 //Natural: IF #O = 0
        {
            pnd_Terminate_Pnd_Terminate_A.setValue(pdaCpoa100.getCpoa100_Cpoa100_Return_Code());                                                                          //Natural: ASSIGN #TERMINATE-A := CPOA100-RETURN-CODE
            getReports().write(0, "***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(5),pnd_Terminate_Pnd_Terminate_A,"- NO MATCH FOUND ON 'CPARM'","REFERENCE TABLE FOR KEY:",new  //Natural: WRITE '***' 77T '***' / '***' 5T #TERMINATE-A '- NO MATCH FOUND ON "CPARM"' 'REFERENCE TABLE FOR KEY:' 77T '***' / '***' 12T CPOA100-LONG-KEY 77T '***' / '***' 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(12),pdaCpoa100.getCpoa100_Cpoa100_Long_Key(),new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  OVERFLOW
            if (condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().greaterOrEqual(pnd_Max_Codes)))                                                                                 //Natural: IF #O GE #MAX-CODES
            {
                pnd_Terminate.setValue(100);                                                                                                                              //Natural: ASSIGN #TERMINATE := 100
                getReports().write(0, "***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(5),pnd_Terminate_Pnd_Terminate_A,"- ORIGIN CODES ON 'CPARM' REFERENCE -","TABLE GREATER THAN CURRENT MAXIMUM.",new  //Natural: WRITE '***' 77T '***' / '***' 5T #TERMINATE-A '- ORIGIN CODES ON "CPARM" REFERENCE -' 'TABLE GREATER THAN CURRENT MAXIMUM.' 83T '***' / '***' 5T 'CANNOT PROCESS ORIGIN CODE: -' CPOA100.RT-LONG-KEY ( AL = 2 ) 77T '***' / '***' 77T '***' / '***' 5T 'INCREASE #MAX-CODES VALUE IN' *PROGRAM 77T '***' / '***' 77T '***'
                    TabSetting(83),"***",NEWLINE,"***",new TabSetting(5),"CANNOT PROCESS ORIGIN CODE: -",pdaCpoa100.getCpoa100_Rt_Long_Key(), new AlphanumericLength 
                    (2),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(5),"INCREASE #MAX-CODES VALUE IN",Global.getPROGRAM(),new 
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  EXACT MATCH
        //*  PARTIAL MATCH
        if (condition((pdaCpoa100.getCpoa100_Cpoa100_Return_Code().notEquals("01")) && (pdaCpoa100.getCpoa100_Cpoa100_Return_Code().notEquals("02"))))                    //Natural: IF ( CPOA100-RETURN-CODE NE '01' ) AND ( CPOA100-RETURN-CODE NE '02' )
        {
            pnd_Terminate_Pnd_Terminate_A.setValue(pdaCpoa100.getCpoa100_Cpoa100_Return_Code());                                                                          //Natural: ASSIGN #TERMINATE-A := CPOA100-RETURN-CODE
            getReports().write(0, "***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(5),Global.getPROGRAM(),"- ERROR CODE",pdaCpoa100.getCpoa100_Cpoa100_Return_Code(),"RECEIVED FROM CPON100.",new  //Natural: WRITE '***' 77T '***' / '***' 5T *PROGRAM '- ERROR CODE' CPOA100-RETURN-CODE 'RECEIVED FROM CPON100.' 77T '***' / '***' 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
        //*  B10-ERROR-PROCESSING
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
