/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:19 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnldgr
************************************************************
**        * FILE NAME            : Fcpnldgr.java
**        * CLASS NAME           : Fcpnldgr
**        * INSTANCE NAME        : Fcpnldgr
************************************************************
************************************************************************
* PROGRAM  : FCPNLDGR
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* CREATED  : 11/30/95
* FUNCTION : FCP-CONS-LEDGER FILE LOOKUP.
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnldgr extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpaldgr pdaFcpaldgr;
    private LdaFcplldgr ldaFcplldgr;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ledger_S;
    private DbsField pnd_Ledger_S_Cntrct_Rcrd_Typ;
    private DbsField pnd_Ledger_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Ledger_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ledger_S_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Ledger_S_Cntl_Check_Dte;

    private DbsGroup pnd_Ledger_S__R_Field_1;
    private DbsField pnd_Ledger_S_Pnd_Ledger_Superde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplldgr = new LdaFcplldgr();
        registerRecord(ldaFcplldgr);
        registerRecord(ldaFcplldgr.getVw_fcp_Cons_Ledger());

        // parameters
        parameters = new DbsRecord();
        pdaFcpaldgr = new PdaFcpaldgr(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ledger_S = localVariables.newGroupInRecord("pnd_Ledger_S", "#LEDGER-S");
        pnd_Ledger_S_Cntrct_Rcrd_Typ = pnd_Ledger_S.newFieldInGroup("pnd_Ledger_S_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_Ledger_S_Cntrct_Orgn_Cde = pnd_Ledger_S.newFieldInGroup("pnd_Ledger_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ledger_S_Cntrct_Ppcn_Nbr = pnd_Ledger_S.newFieldInGroup("pnd_Ledger_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Ledger_S_Pymnt_Prcss_Seq_Nbr = pnd_Ledger_S.newFieldInGroup("pnd_Ledger_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        pnd_Ledger_S_Cntl_Check_Dte = pnd_Ledger_S.newFieldInGroup("pnd_Ledger_S_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);

        pnd_Ledger_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Ledger_S__R_Field_1", "REDEFINE", pnd_Ledger_S);
        pnd_Ledger_S_Pnd_Ledger_Superde = pnd_Ledger_S__R_Field_1.newFieldInGroup("pnd_Ledger_S_Pnd_Ledger_Superde", "#LEDGER-SUPERDE", FieldType.STRING, 
            26);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplldgr.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnldgr() throws Exception
    {
        super("Fcpnldgr");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Ledger_S.setValuesByName(pdaFcpaldgr.getLedger());                                                                                                            //Natural: MOVE BY NAME LEDGER TO #LEDGER-S
        pnd_Ledger_S_Cntrct_Rcrd_Typ.setValue("5");                                                                                                                       //Natural: MOVE '5' TO #LEDGER-S.CNTRCT-RCRD-TYP
        ldaFcplldgr.getVw_fcp_Cons_Ledger().startDatabaseRead                                                                                                             //Natural: READ FCP-CONS-LEDGER BY RCRD-ORGN-PPCN-PRCSS-CHKDT = #LEDGER-SUPERDE THRU #LEDGER-SUPERDE WHERE LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = FCP-CONS-LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        (
        "RLEDGER",
        new Wc[] { new Wc("CNTRCT_CANCEL_RDRW_ACTVTY_CDE", "=", pdaFcpaldgr.getLedger_Cntrct_Cancel_Rdrw_Actvty_Cde(), WcType.WHERE) ,
        new Wc("RCRD_ORGN_PPCN_PRCSS_CHKDT", ">=", pnd_Ledger_S_Pnd_Ledger_Superde.getBinary(), "And", WcType.BY) ,
        new Wc("RCRD_ORGN_PPCN_PRCSS_CHKDT", "<=", pnd_Ledger_S_Pnd_Ledger_Superde.getBinary(), WcType.BY) },
        new Oc[] { new Oc("RCRD_ORGN_PPCN_PRCSS_CHKDT", "ASC") }
        );
        RLEDGER:
        while (condition(ldaFcplldgr.getVw_fcp_Cons_Ledger().readNextRow("RLEDGER")))
        {
            pdaFcpaldgr.getLedger().setValuesByName(ldaFcplldgr.getVw_fcp_Cons_Ledger());                                                                                 //Natural: MOVE BY NAME FCP-CONS-LEDGER TO LEDGER
            pdaFcpaldgr.getLedger_C_Inv_Ledgr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Count_Castinv_Ledgr());                                                           //Natural: MOVE C*INV-LEDGR TO C-INV-LEDGR
            pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl());                                               //Natural: MOVE C*INV-LEDGR-OVRFL TO C-INV-LEDGR-OVRFL
            pdaFcpaldgr.getLedger_Work_Fields_Ledger_Return_Cde().reset();                                                                                                //Natural: RESET LEDGER-RETURN-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcplldgr.getVw_fcp_Cons_Ledger().getAstCOUNTER().equals(getZero())))                                                                             //Natural: IF *COUNTER ( RLEDGER. ) = 0
        {
            pdaFcpaldgr.getLedger_Work_Fields_Ledger_Return_Cde().setValue(92);                                                                                           //Natural: MOVE 92 TO LEDGER-RETURN-CDE
            if (condition(pdaFcpaldgr.getLedger_Work_Fields_Ledger_Abend_Ind().getBoolean()))                                                                             //Natural: IF LEDGER-ABEND-IND
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"LEDGER RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN    :",pnd_Ledger_S_Cntrct_Orgn_Cde,new  //Natural: WRITE '***' 25T 'LEDGER RECORD IS NOT FOUND' 77T '***' / '***' 25T 'ORIGIN    :' #LEDGER-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'TIAA PPCN#:' #LEDGER-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'CHECK DATE:' #LEDGER-S.CNTL-CHECK-DTE ( EM = MM/DD/YYYY ) 77T '***' / '***' 25T 'SEQUENCE# :' #LEDGER-S.PYMNT-PRCSS-SEQ-NBR 77T '***'
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIAA PPCN#:",pnd_Ledger_S_Cntrct_Ppcn_Nbr,new TabSetting(77),"***",NEWLINE,"***",new 
                    TabSetting(25),"CHECK DATE:",pnd_Ledger_S_Cntl_Check_Dte, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***",NEWLINE,"***",new 
                    TabSetting(25),"SEQUENCE# :",pnd_Ledger_S_Pymnt_Prcss_Seq_Nbr,new TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(pdaFcpaldgr.getLedger_Work_Fields_Ledger_Return_Cde());  if (true) return;                                                              //Natural: TERMINATE LEDGER-RETURN-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
