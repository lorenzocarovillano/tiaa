/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:10 AM
**        * FROM NATURAL SUBPROGRAM : Fcpncrpt
************************************************************
**        * FILE NAME            : Fcpncrpt.java
**        * CLASS NAME           : Fcpncrpt
**        * INSTANCE NAME        : Fcpncrpt
************************************************************
************************************************************************
* SUBPROGRAM : FCPNCRPT
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : "AP" ANNUITANT STATEMENTS - "Body".
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpncrpt extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpacrpt pdaFcpacrpt;
    private LdaFcplcrpt ldaFcplcrpt;
    private LdaFcplcrp1 ldaFcplcrp1;
    private PdaFcpacntl pdaFcpacntl;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplcrpt = new LdaFcplcrpt();
        registerRecord(ldaFcplcrpt);
        ldaFcplcrp1 = new LdaFcplcrp1();
        registerRecord(ldaFcplcrp1);
        localVariables = new DbsRecord();
        pdaFcpacntl = new PdaFcpacntl(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpaacum = new PdaFcpaacum(parameters);
        pdaFcpacrpt = new PdaFcpacrpt(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplcrpt.initializeValues();
        ldaFcplcrp1.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpncrpt() throws Exception
    {
        super("Fcpncrpt");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 15 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 15 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Consolidated Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' #PROGRAM 47T #TITLE 119T 'REPORT: RPT15' / 58T 'Control Report' //
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Cntl_Table().getValue(1).setValuesByName(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Table());                                          //Natural: MOVE BY NAME #ACCUM-TABLE TO #CNTL-TABLE ( 1 )
        getWorkFiles().read(15, pdaFcpacntl.getCntl());                                                                                                                   //Natural: READ WORK FILE 15 ONCE CNTL
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(15, "***",new TabSetting(25),"CONTROL RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PROGRAM....:",Global.getPROGRAM(),new  //Natural: WRITE ( 15 ) '***' 25T 'CONTROL RECORD IS NOT FOUND' 77T '***' / '***' 25T 'PROGRAM....:' *PROGRAM 77T '***' / '***' 25T 'CALLED FROM:' #PROGRAM 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"CALLED FROM:",pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Diff().setValue(true);                                                                                                        //Natural: MOVE TRUE TO #FCPACRPT.#DIFF
        }                                                                                                                                                                 //Natural: END-ENDFILE
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(2,"*").setValue(pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue("*"));                                         //Natural: MOVE CNTL-PYMNT-CNT ( * ) TO #FCPLCRP1.#PYMNT-CNT ( 2,* )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(2,"*").setValue(pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue("*"));                                             //Natural: MOVE CNTL-REC-CNT ( * ) TO #FCPLCRP1.#REC-CNT ( 2,* )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(2,"*").setValue(pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue("*"));                                         //Natural: MOVE CNTL-GROSS-AMT ( * ) TO #FCPLCRP1.#SETTL-AMT ( 2,* )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(2,"*").setValue(pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue("*"));                                       //Natural: MOVE CNTL-NET-AMT ( * ) TO #FCPLCRP1.#NET-PYMNT-AMT ( 2,* )
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(3,"*").compute(new ComputeParameters(false, ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(3,"*")),    //Natural: COMPUTE #FCPLCRP1.#PYMNT-CNT ( 3,* ) = #FCPLCRP1.#PYMNT-CNT ( 2,* ) - #FCPLCRP1.#PYMNT-CNT ( 1,* )
            ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(2,"*").subtract(ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(1,"*")));
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(3,"*").compute(new ComputeParameters(false, ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(3,"*")),        //Natural: COMPUTE #FCPLCRP1.#REC-CNT ( 3,* ) = #FCPLCRP1.#REC-CNT ( 2,* ) - #FCPLCRP1.#REC-CNT ( 1,* )
            ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(2,"*").subtract(ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(1,"*")));
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(3,"*").compute(new ComputeParameters(false, ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(3,"*")),    //Natural: COMPUTE #FCPLCRP1.#SETTL-AMT ( 3,* ) = #FCPLCRP1.#SETTL-AMT ( 2,* ) - #FCPLCRP1.#SETTL-AMT ( 1,* )
            ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(2,"*").subtract(ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(1,"*")));
        ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(3,"*").compute(new ComputeParameters(false, ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(3,"*")),  //Natural: COMPUTE #FCPLCRP1.#NET-PYMNT-AMT ( 3,* ) = #FCPLCRP1.#NET-PYMNT-AMT ( 2,* ) - #FCPLCRP1.#NET-PYMNT-AMT ( 1,* )
            ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(2,"*").subtract(ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(1,"*")));
        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        FOR01:                                                                                                                                                            //Natural: FOR #J = 1 TO 3
        for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(3)); pnd_Ws_Pnd_J.nadd(1))
        {
            getReports().write(15, NEWLINE,new TabSetting(5),ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Cntl_Type().getValue(pnd_Ws_Pnd_J));                                         //Natural: WRITE ( 15 ) / 5T #CNTL-TYPE ( #J )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO #MAX-OCCUR
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur())); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                         //Natural: IF #FCPACRPT.#TRUTH-TABLE ( #I )
                {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CNTL-REPORT
                    sub_Display_Cntl_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Ws_Pnd_J.equals(3) && (ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(3,pnd_Ws_Pnd_I).notEquals(getZero())                    //Natural: IF #J = 3 AND ( #FCPLCRP1.#PYMNT-CNT ( 3,#I ) NE 0 OR #FCPLCRP1.#REC-CNT ( 3,#I ) NE 0 OR #FCPLCRP1.#SETTL-AMT ( 3,#I ) NE 0.00 OR #FCPLCRP1.#NET-PYMNT-AMT ( 3,#I ) NE 0.00 )
                        || ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(3,pnd_Ws_Pnd_I).notEquals(getZero()) || ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(3,pnd_Ws_Pnd_I).notEquals(new 
                        DbsDecimal("0.00")) || ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(3,pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00")))))
                    {
                        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Diff().setValue(true);                                                                                            //Natural: MOVE TRUE TO #FCPACRPT.#DIFF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Diff().getBoolean()))                                                                                               //Natural: IF #FCPACRPT.#DIFF
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(15, "***",new TabSetting(25),"ERROR OCCURED IN THE CONTROL PROCESS",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PROGRAM....:",Global.getPROGRAM(),new  //Natural: WRITE ( 15 ) '***' 25T 'ERROR OCCURED IN THE CONTROL PROCESS' 77T '***' / '***' 25T 'PROGRAM....:' *PROGRAM 77T '***' / '***' 25T 'CALLED FROM:' #PROGRAM 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"CALLED FROM:",pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().getBoolean()))                                                                                       //Natural: IF #FCPACRPT.#NO-ABEND
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                DbsUtil.terminate(90);  if (true) return;                                                                                                                 //Natural: TERMINATE 90
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-CNTL-REPORT
        //* ************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* ************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* **********************************
    }
    private void sub_Display_Cntl_Report() throws Exception                                                                                                               //Natural: DISPLAY-CNTL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",                                   //Natural: DISPLAY ( 15 ) ( HC = R ) 10T '/DESCRIPTION' #PYMNT-TYPE-DESC ( #I ) ( HC = L ) 'PYMNT/COUNT' #FCPLCRP1.#PYMNT-CNT ( #J,#I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 'RECORD/COUNT' #FCPLCRP1.#REC-CNT ( #J,#I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 'GROSS/AMOUNT' #FCPLCRP1.#SETTL-AMT ( #J,#I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ ) 'NET/AMOUNT' #FCPLCRP1.#NET-PYMNT-AMT ( #J,#I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ )
        		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
            "PYMNT/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"GROSS/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),
            "NET/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_J,pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        getReports().write(15, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new //Natural: WRITE ( 15 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(15, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new         //Natural: WRITE ( 15 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(15, "PS=58 LS=132 ZP=ON");

        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"Consolidated Payment System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program(),new 
            TabSetting(47),pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title(),new TabSetting(119),"REPORT: RPT15",NEWLINE,new TabSetting(58),"Control Report",NEWLINE,
            NEWLINE);

        getReports().setDisplayColumns(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",
        		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"PYMNT/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Pymnt_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Rec_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"GROSS/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Settl_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),"NET/AMOUNT",
        		ldaFcplcrp1.getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
    }
}
