/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:43 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn803x
************************************************************
**        * FILE NAME            : Fcpn803x.java
**        * CLASS NAME           : Fcpn803x
**        * INSTANCE NAME        : Fcpn803x
************************************************************
************************************************************************
* SUBPROGRAM : FCPS803X
* SYSTEM     : CPS
* TITLE      : IAR RESTRUCTURE
* FUNCTION   : XEROX STATEMENTS
* HISTORY    :
* 01/19/2001 LEON G. CORRECT TRANCATION OF XEROX COMMAND
*                    BY MOVING RIGHT HEADER LINE
* 11/14/2005 RAMANA ANNE CHANGE XEROX COMMANDS TO USE NEW FORMS IN LOCAL
*               AREA AND VARIABLE SIZE ALSO FOR PAYEE-MATCH PROJECT
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn803x extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa803 pdaFcpa803;
    private LdaFcpl803x ldaFcpl803x;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField cntrct_Annty_Ins_Type;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl803x = new LdaFcpl803x();
        registerRecord(ldaFcpl803x);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa803 = new PdaFcpa803(parameters);
        cntrct_Annty_Ins_Type = parameters.newFieldInRecord("cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        cntrct_Annty_Ins_Type.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl803x.initializeValues();

        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn803x() throws Exception
    {
        super("Fcpn803x");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        short decideConditionsMet72 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ORGN-CDE;//Natural: VALUE 'AP'
        if (condition((pdaFcpa803.getPnd_Fcpa803_Cntrct_Orgn_Cde().equals("AP"))))
        {
            decideConditionsMet72++;
                                                                                                                                                                          //Natural: PERFORM GEN-XEROX-FOR-AP
            sub_Gen_Xerox_For_Ap();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'NZ'
        else if (condition((pdaFcpa803.getPnd_Fcpa803_Cntrct_Orgn_Cde().equals("NZ"))))
        {
            decideConditionsMet72++;
            //*  INCLUDE AL PRINTING    -   ROXAN
                                                                                                                                                                          //Natural: PERFORM GEN-XEROX-FOR-NZ
            sub_Gen_Xerox_For_Nz();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pdaFcpa803.getPnd_Fcpa803_Cntrct_Orgn_Cde().equals("AL"))))
        {
            decideConditionsMet72++;
                                                                                                                                                                          //Natural: PERFORM GEN-XEROX-FOR-AL
            sub_Gen_Xerox_For_Al();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-XEROX-FOR-AP
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-XEROX-FOR-NZ
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-XEROX-FOR-AL
        //*      WHEN #ODD-PAGE
        //*        #XEROX-SUB         := 28
        //*      WHEN NOT #ODD-PAGE
        //*        #XEROX-SUB         := 27
        //*      WHEN #ODD-PAGE
        //*        #XEROX-SUB         := 28
        //*      WHEN NOT #ODD-PAGE
        //*        #XEROX-SUB         := 27
        //*      WHEN #CURRENT-PAGE = 1
        //*        #XEROX-SUB         := 36
        //*      WHEN #ODD-PAGE
        //*        #XEROX-SUB         := 28
        //*      WHEN NOT #ODD-PAGE
        //*        #XEROX-SUB         := 27
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-XEROX
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
    }
    private void sub_Gen_Xerox_For_Ap() throws Exception                                                                                                                  //Natural: GEN-XEROX-FOR-AP
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        short decideConditionsMet124 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #STMNT
        if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))))
        {
            decideConditionsMet124++;
            short decideConditionsMet126 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet126++;
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean()))                                                                                      //Natural: IF #SIMPLEX
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(4);                                                                                                //Natural: ASSIGN #XEROX-SUB := 4
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(5);                                                                                                //Natural: ASSIGN #XEROX-SUB := 5
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CURRENT-PAGE = 1
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))
            {
                decideConditionsMet126++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(7);                                                                                                    //Natural: ASSIGN #XEROX-SUB := 7
            }                                                                                                                                                             //Natural: WHEN #ODD-PAGE
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))
            {
                decideConditionsMet126++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(9);                                                                                                    //Natural: ASSIGN #XEROX-SUB := 9
            }                                                                                                                                                             //Natural: WHEN NOT #ODD-PAGE
            else if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))))
            {
                decideConditionsMet126++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(8);                                                                                                    //Natural: ASSIGN #XEROX-SUB := 8
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet126 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #STMNT
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))
        {
            decideConditionsMet124++;
            short decideConditionsMet145 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet145++;
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean()))                                                                                      //Natural: IF #SIMPLEX
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(1);                                                                                                //Natural: ASSIGN #XEROX-SUB := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(2);                                                                                                //Natural: ASSIGN #XEROX-SUB := 2
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CURRENT-PAGE = 1
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))
            {
                decideConditionsMet145++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(3);                                                                                                    //Natural: ASSIGN #XEROX-SUB := 3
            }                                                                                                                                                             //Natural: WHEN #ODD-PAGE
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))
            {
                decideConditionsMet145++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(9);                                                                                                    //Natural: ASSIGN #XEROX-SUB := 9
            }                                                                                                                                                             //Natural: WHEN NOT #ODD-PAGE
            else if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))))
            {
                decideConditionsMet145++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(8);                                                                                                    //Natural: ASSIGN #XEROX-SUB := 8
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet145 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Gen_Xerox_For_Nz() throws Exception                                                                                                                  //Natural: GEN-XEROX-FOR-NZ
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        short decideConditionsMet169 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CHECK-TO-ANNT
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean()))
        {
            decideConditionsMet169++;
            short decideConditionsMet171 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet171++;
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean()))                                                                                      //Natural: IF #SIMPLEX
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(12);                                                                                               //Natural: ASSIGN #XEROX-SUB := 12
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(13);                                                                                               //Natural: ASSIGN #XEROX-SUB := 13
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CURRENT-PAGE = 1
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))
            {
                decideConditionsMet171++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(14);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 14
            }                                                                                                                                                             //Natural: WHEN #ODD-PAGE
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))
            {
                decideConditionsMet171++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(16);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 16
            }                                                                                                                                                             //Natural: WHEN NOT #ODD-PAGE
            else if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))))
            {
                decideConditionsMet171++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(15);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 15
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet171 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #STMNT-TO-ANNT
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().getBoolean()))
        {
            decideConditionsMet169++;
            short decideConditionsMet190 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet190++;
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean()))                                                                                      //Natural: IF #SIMPLEX
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(17);                                                                                               //Natural: ASSIGN #XEROX-SUB := 17
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(18);                                                                                               //Natural: ASSIGN #XEROX-SUB := 18
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CURRENT-PAGE = 1
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))
            {
                decideConditionsMet190++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(19);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 19
            }                                                                                                                                                             //Natural: WHEN #ODD-PAGE
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))
            {
                decideConditionsMet190++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(16);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 16
            }                                                                                                                                                             //Natural: WHEN NOT #ODD-PAGE
            else if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))))
            {
                decideConditionsMet190++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(15);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 15
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet190 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #STMNT OR #GLOBAL-PAY OR #ZERO-CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean() || pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean() || pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().getBoolean()))
        {
            decideConditionsMet169++;
            short decideConditionsMet209 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet209++;
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().getBoolean()))                                                                                      //Natural: IF #SIMPLEX
                {
                    if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Separator_Page().getBoolean()))                                                                           //Natural: IF #SEPARATOR-PAGE
                    {
                        pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(11);                                                                                           //Natural: ASSIGN #XEROX-SUB := 11
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(10);                                                                                           //Natural: ASSIGN #XEROX-SUB := 10
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(20);                                                                                               //Natural: ASSIGN #XEROX-SUB := 20
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CURRENT-PAGE = 1
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))
            {
                decideConditionsMet209++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(21);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 21
            }                                                                                                                                                             //Natural: WHEN #ODD-PAGE
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))
            {
                decideConditionsMet209++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(16);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 16
            }                                                                                                                                                             //Natural: WHEN NOT #ODD-PAGE
            else if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Odd_Page().getBoolean()))))
            {
                decideConditionsMet209++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(15);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 15
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet209 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean()))
        {
            decideConditionsMet169++;
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))                                                                                       //Natural: IF #FULL-XEROX
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(22);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 22
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(23);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 23
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
            sub_Write_Xerox();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Gen_Xerox_For_Al() throws Exception                                                                                                                  //Natural: GEN-XEROX-FOR-AL
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        short decideConditionsMet244 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CHECK-TO-ANNT
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().getBoolean()))
        {
            decideConditionsMet244++;
            short decideConditionsMet246 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet246++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(24);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 24
            }                                                                                                                                                             //Natural: WHEN #CURRENT-PAGE = 1
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))
            {
                decideConditionsMet246++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(26);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 26
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet246 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #STMNT-TO-ANNT
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().getBoolean()))
        {
            decideConditionsMet244++;
            short decideConditionsMet257 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet257++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(29);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 29
            }                                                                                                                                                             //Natural: WHEN #CURRENT-PAGE = 1
            else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Current_Page().equals(1)))
            {
                decideConditionsMet257++;
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(31);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 31
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet257 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #STMNT OR #GLOBAL-PAY
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean() || pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))
        {
            decideConditionsMet244++;
            short decideConditionsMet268 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FULL-XEROX
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))
            {
                decideConditionsMet268++;
                if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Separator_Page().getBoolean()))                                                                               //Natural: IF #SEPARATOR-PAGE
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(33);                                                                                               //Natural: ASSIGN #XEROX-SUB := 33
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(32);                                                                                               //Natural: ASSIGN #XEROX-SUB := 32
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet268 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
                sub_Write_Xerox();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean()))
        {
            decideConditionsMet244++;
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().getBoolean()))                                                                                       //Natural: IF #FULL-XEROX
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(24);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 24
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub().setValue(26);                                                                                                   //Natural: ASSIGN #XEROX-SUB := 26
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-XEROX
            sub_Write_Xerox();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_Xerox() throws Exception                                                                                                                       //Natural: WRITE-XEROX
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                               //Natural: RESET #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FCPC803W
        sub_Fcpc803w();
        if (condition(Global.isEscape())) {return;}
        if (condition(cntrct_Annty_Ins_Type.equals("S") || cntrct_Annty_Ins_Type.equals("M")))                                                                            //Natural: IF CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" $$XEROX");                                                                                              //Natural: ASSIGN #OUTPUT-REC := ' $$XEROX'
            //*  LEON 01/19/2001
            setValueToSubstring(ldaFcpl803x.getPnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub()),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(), //Natural: MOVE #XEROX-STMNTS-PA ( #XEROX-SUB ) TO SUBSTR ( #OUTPUT-REC,10,#XEROX-STMNTS-TOP-PA )
                10,ldaFcpl803x.getPnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top_Pa().getInt());
            //* ***TO SUBSTR(#OUTPUT-REC,10,#XEROX-STMNTS-TOP)     /*
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                           //Natural: RESET #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().setValue(" $$XEROX");                                                                                              //Natural: ASSIGN #OUTPUT-REC := ' $$XEROX'
            setValueToSubstring(ldaFcpl803x.getPnd_Xerox_Fields_Pnd_Xerox_Stmnts().getValue(pdaFcpa803.getPnd_Fcpa803_Pnd_Xerox_Sub()),pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec(), //Natural: MOVE #XEROX-STMNTS ( #XEROX-SUB ) TO SUBSTR ( #OUTPUT-REC,10,#XEROX-STMNTS-TOP )
                10,ldaFcpl803x.getPnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top().getInt());
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec().reset();                                                                                                           //Natural: RESET #OUTPUT-REC
                                                                                                                                                                          //Natural: PERFORM FCPC803W
            sub_Fcpc803w();
            if (condition(Global.isEscape())) {return;}
            //*  #PREV-XEROX-SUB    := #XEROX-SUB                /* ROXAN 08/11/00
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(false);                                                                                                       //Natural: ASSIGN #FULL-XEROX := FALSE
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }

    //
}
