/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:27:27 AM
**        * FROM NATURAL SUBPROGRAM : Fcpnpmnt
************************************************************
**        * FILE NAME            : Fcpnpmnt.java
**        * CLASS NAME           : Fcpnpmnt
**        * INSTANCE NAME        : Fcpnpmnt
************************************************************
************************************************************************
*
* PROGRAM  : FCPNPMNT
*
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* CREATED  : 11/29/95
*
* FUNCTION : FCP-CONS-PYMNT FILE LOOKUP.
*
**  12/18/01 - TEMP FIX TO BYPASS - JS
**     11/02 - R. CARREON
**             STOW. EGTRRA CHANGES IN LDA.
*
* 05/06/08 : LCW - RE-STOWED FOR FCPLPMNT ROTH-MAJOR1 CHANGES.
* 09/16/10 : CMM - GET CANADIAN TAX AMOUNT TO STORE ON TAX PAYMENT FILE
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPAPMNT
*  4/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpnpmnt extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpapmnt pdaFcpapmnt;
    private LdaFcplpmnt ldaFcplpmnt;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_S__R_Field_1;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;

    private DataAccessProgramView vw_payment;

    private DbsGroup payment_Inv_Acct_Part_2;
    private DbsField payment_Inv_Acct_Can_Tax_Amt;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());

        // parameters
        parameters = new DbsRecord();
        pdaFcpapmnt = new PdaFcpapmnt(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_1", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_1.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 29);

        vw_payment = new DataAccessProgramView(new NameInfo("vw_payment", "PAYMENT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));

        payment_Inv_Acct_Part_2 = vw_payment.getRecord().newGroupInGroup("payment_Inv_Acct_Part_2", "INV-ACCT-PART-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT_PART_2");
        payment_Inv_Acct_Can_Tax_Amt = payment_Inv_Acct_Part_2.newFieldArrayInGroup("payment_Inv_Acct_Can_Tax_Amt", "INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CAN_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT_PART_2");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_payment.reset();

        ldaFcplpmnt.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpnpmnt() throws Exception
    {
        super("Fcpnpmnt");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Pymnt_S.setValuesByName(pdaFcpapmnt.getPymnt());                                                                                                              //Natural: MOVE BY NAME PYMNT TO #PYMNT-S
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-PYMNT BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE THRU #PYMNT-SUPERDE
        (
        "RPYMNT",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, "And", WcType.BY) ,
        new Wc("PPCN_INV_ORGN_PRCSS_INST", "<=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        RPYMNT:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("RPYMNT")))
        {
            pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Isn().setValue(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstISN("RPYMNT"));                                                //Natural: MOVE *ISN TO PYMNT-ISN
            pdaFcpapmnt.getPymnt().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                   //Natural: MOVE BY NAME FCP-CONS-PYMNT TO PYMNT
            pdaFcpapmnt.getPymnt_C_Inv_Acct().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                               //Natural: MOVE C*INV-ACCT TO C-INV-ACCT
            pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                                     //Natural: MOVE C*PYMNT-DED-GRP TO C-PYMNT-DED-GRP
            pdaFcpapmnt.getPymnt_C_Inv_Rtb_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Rtb_Grp());                                                         //Natural: MOVE C*INV-RTB-GRP TO C-INV-RTB-GRP
            pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Return_Cde().reset();                                                                                                  //Natural: RESET PYMNT-RETURN-CDE
            //*  CM - GET CANADIAN TAX
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind().equals("C") && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind().equals("C")))      //Natural: IF FCP-CONS-PYMNT.CNTRCT-PYMNT-TYPE-IND = 'C' AND FCP-CONS-PYMNT.CNTRCT-STTLMNT-TYPE-IND = 'C'
            {
                GET01:                                                                                                                                                    //Natural: GET PAYMENT *ISN
                vw_payment.readByID(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstISN("RPYMNT"), "GET01");
                pdaFcpapmnt.getPymnt().setValuesByName(vw_payment);                                                                                                       //Natural: MOVE BY NAME PAYMENT TO PYMNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpapmnt.getPymnt_Inv_Acct_Can_Tax_Amt().getValue("*").reset();                                                                                        //Natural: RESET PYMNT.INV-ACCT-CAN-TAX-AMT ( * )
            }                                                                                                                                                             //Natural: END-IF
            //*  CM - END
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().equals(getZero())))                                                                              //Natural: IF *COUNTER ( RPYMNT. ) = 0
        {
            pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Return_Cde().setValue(90);                                                                                             //Natural: MOVE 90 TO PYMNT-RETURN-CDE
            if (condition(pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Abend_Ind().getBoolean()))                                                                               //Natural: IF PYMNT-ABEND-IND
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"PAYMENT RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIAA PPCN#  :",pnd_Pymnt_S_Cntrct_Ppcn_Nbr,new  //Natural: WRITE '***' 25T 'PAYMENT RECORD IS NOT FOUND' 77T '***' / '***' 25T 'TIAA PPCN#  :' #PYMNT-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'INVERSE DATE:' #PYMNT-S.CNTRCT-INVRSE-DTE 77T '***' / '***' 25T 'ORIGIN      :' #PYMNT-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'SEQUENCE#   :' #PYMNT-S.PYMNT-PRCSS-SEQ-NBR 77T '***'
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"INVERSE DATE:",pnd_Pymnt_S_Cntrct_Invrse_Dte,new TabSetting(77),"***",NEWLINE,"***",new 
                    TabSetting(25),"ORIGIN      :",pnd_Pymnt_S_Cntrct_Orgn_Cde,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"SEQUENCE#   :",pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr,new 
                    TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                //* *
                //* ** TEMP STOP OF TERMINATE - JS  12/18/01
                //* *  TERMINATE PYMNT-RETURN-CDE
                //* *
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //
}
