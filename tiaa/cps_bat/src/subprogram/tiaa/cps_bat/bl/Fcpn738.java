/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 12:25:09 AM
**        * FROM NATURAL SUBPROGRAM : Fcpn738
************************************************************
**        * FILE NAME            : Fcpn738.java
**        * CLASS NAME           : Fcpn738
**        * INSTANCE NAME        : Fcpn738
************************************************************
************************************************************************
* PROGRAM  : FCPN738
* SYSTEM   : CPS
* TITLE    : HEADINGS FOR MONTHLY SETTLEMENT PROTOTYPES
* FUNCTION : THIS PROGRAM GENERATES THE PROPER "HEADINGS" PORTION FOR
*            MONTHLY SETTLEMENT PROTOTYPES, IN ACCORD WITH THE
*            "XEROX" PROTOTYPES TO PRINT THE MS PAYMENT DOCUMENTS.
* ----------------------------------------------------------------------
* HISTORY:
* ----------------------------------------------------------------------
*
* 10/03/96 LIN ZHENG         - INFLATION LINKED BOND
*
* 2/15/94 LEON SILBERSTEIN
*  CHANGE TPA HEADING TO LIST THE CONTRACT NUMBERS IN THE SAME MANNER
*  AS A MATTURITY.
* ----------------------------------------------------------------------
*  NOTES:
* ----------------------------------------------------------------------
*
* FEW DATA ELEMENTS WHICH ARE NORMALLY KEPT IN THE HEADER-RECORD
* AND ARE COMMON TO ALL PAYMENTS AND "groups of printed lines",
* DATA ELEMENTS WHICH ARE COMMON TO ALL PAYMENTS AND "Groups Of
* PRINTED LINES", are normally kept in the HEADER-RECORD.
* HOWEVER, FEW OF THESE DATA ELEMENTS, MAY CHANGE FROM ONE PAYMENTS
* RECORD TO ANOTHER.  THEREFORE, THIS DATA IS KEPT AT THE OCCURS TABLE
* ATTACHED TO THE INV_ACCT DATA.
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpn738 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaFcpa700 pdaFcpa700;
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;

    private DbsGroup msg_Info_Sub;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Nr;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data;

    private DbsGroup msg_Info_Sub__R_Field_1;

    private DbsGroup msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data_Char;
    private DbsField msg_Info_Sub_Pnd_Pnd_Return_Code;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index1;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index2;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index3;

    private DbsGroup pnd_Fcpn600;
    private DbsField pnd_Fcpn600_Pnd_Parm_Code;
    private DbsField pnd_Fcpn600_Pnd_Parm_Desc;

    private DbsGroup tbldcoda;

    private DbsGroup tbldcoda_Inputs;
    private DbsField tbldcoda_Pnd_Table_Id;
    private DbsField tbldcoda_Pnd_Code;
    private DbsField tbldcoda_Pnd_Mode;

    private DbsGroup tbldcoda_Outputs;
    private DbsField tbldcoda_Pnd_Target;
    private DbsField pnd_Ws_Rec_1;

    private DbsGroup pnd_Ws_Rec_1__R_Field_2;
    private DbsField pnd_Ws_Rec_1_Pnd_Cc;
    private DbsField pnd_Ws_Rec_1_Pnd_Text;

    private DbsGroup pnd_Ws_Rec_1__R_Field_3;
    private DbsField pnd_Ws_Rec_1__Filler1;
    private DbsField pnd_Ws_Rec_1_Pnd_Name;
    private DbsField pnd_I;

    private DbsGroup pnd_Const;
    private DbsField pnd_Const_Blank_1;
    private DbsField pnd_Const_Whiteout_Char;
    private DbsField pnd_Const_One;
    private DbsField whiteout_All;

    private DbsGroup whiteout_All__R_Field_4;
    private DbsField whiteout_All_Whiteout_Pos;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Ws1;
    private DbsField pnd_Ws_Edited_Ppcn;
    private DbsField pnd_Ws_Edited_Date;
    private DbsField pnd_Ws_Fund_Description;
    private DbsField pnd_Orgn_Chk_Dte;

    private DbsGroup pnd_Orgn_Chk_Dte__R_Field_5;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Filler;
    private DbsField pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A;
    private DbsField pnd_Orgn_Chk_Dte_A4;

    private DbsGroup pnd_Orgn_Chk_Dte_A4__R_Field_6;
    private DbsField pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D;
    private DbsField pnd_Edited_Date;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaFcpa700 = new PdaFcpa700(parameters);
        pnd_Ws_Cntr_Inv_Acct = parameters.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Cntr_Inv_Acct.setParameterOption(ParameterOption.ByReference);
        pnd_Ws_Sim_Dup_Multiplex_Written = parameters.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Sim_Dup_Multiplex_Written.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        msg_Info_Sub = localVariables.newGroupInRecord("msg_Info_Sub", "MSG-INFO-SUB");
        msg_Info_Sub_Pnd_Pnd_Msg = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        msg_Info_Sub_Pnd_Pnd_Msg_Nr = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        msg_Info_Sub_Pnd_Pnd_Msg_Data = msg_Info_Sub.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1, 
            3));

        msg_Info_Sub__R_Field_1 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub__R_Field_1", "REDEFINE", msg_Info_Sub_Pnd_Pnd_Msg_Data);

        msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct = msg_Info_Sub__R_Field_1.newGroupArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct", "##MSG-DATA-STRUCT", 
            new DbsArrayController(1, 3));
        msg_Info_Sub_Pnd_Pnd_Msg_Data_Char = msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Char", "##MSG-DATA-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1, 32));
        msg_Info_Sub_Pnd_Pnd_Return_Code = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        msg_Info_Sub_Pnd_Pnd_Error_Field = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index1 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index2 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index3 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);

        pnd_Fcpn600 = localVariables.newGroupInRecord("pnd_Fcpn600", "#FCPN600");
        pnd_Fcpn600_Pnd_Parm_Code = pnd_Fcpn600.newFieldInGroup("pnd_Fcpn600_Pnd_Parm_Code", "#PARM-CODE", FieldType.NUMERIC, 2);
        pnd_Fcpn600_Pnd_Parm_Desc = pnd_Fcpn600.newFieldInGroup("pnd_Fcpn600_Pnd_Parm_Desc", "#PARM-DESC", FieldType.STRING, 70);

        tbldcoda = localVariables.newGroupInRecord("tbldcoda", "TBLDCODA");

        tbldcoda_Inputs = tbldcoda.newGroupInGroup("tbldcoda_Inputs", "INPUTS");
        tbldcoda_Pnd_Table_Id = tbldcoda_Inputs.newFieldInGroup("tbldcoda_Pnd_Table_Id", "#TABLE-ID", FieldType.NUMERIC, 6);
        tbldcoda_Pnd_Code = tbldcoda_Inputs.newFieldInGroup("tbldcoda_Pnd_Code", "#CODE", FieldType.STRING, 20);
        tbldcoda_Pnd_Mode = tbldcoda_Inputs.newFieldInGroup("tbldcoda_Pnd_Mode", "#MODE", FieldType.STRING, 1);

        tbldcoda_Outputs = tbldcoda.newGroupInGroup("tbldcoda_Outputs", "OUTPUTS");
        tbldcoda_Pnd_Target = tbldcoda_Outputs.newFieldInGroup("tbldcoda_Pnd_Target", "#TARGET", FieldType.STRING, 60);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);

        pnd_Ws_Rec_1__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_2", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1_Pnd_Cc = pnd_Ws_Rec_1__R_Field_2.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Cc", "#CC", FieldType.STRING, 2);
        pnd_Ws_Rec_1_Pnd_Text = pnd_Ws_Rec_1__R_Field_2.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Text", "#TEXT", FieldType.STRING, 141);

        pnd_Ws_Rec_1__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Rec_1__R_Field_3", "REDEFINE", pnd_Ws_Rec_1);
        pnd_Ws_Rec_1__Filler1 = pnd_Ws_Rec_1__R_Field_3.newFieldInGroup("pnd_Ws_Rec_1__Filler1", "_FILLER1", FieldType.STRING, 37);
        pnd_Ws_Rec_1_Pnd_Name = pnd_Ws_Rec_1__R_Field_3.newFieldInGroup("pnd_Ws_Rec_1_Pnd_Name", "#NAME", FieldType.STRING, 106);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);

        pnd_Const = localVariables.newGroupInRecord("pnd_Const", "#CONST");
        pnd_Const_Blank_1 = pnd_Const.newFieldInGroup("pnd_Const_Blank_1", "BLANK-1", FieldType.STRING, 1);
        pnd_Const_Whiteout_Char = pnd_Const.newFieldInGroup("pnd_Const_Whiteout_Char", "WHITEOUT-CHAR", FieldType.STRING, 1);
        pnd_Const_One = pnd_Const.newFieldInGroup("pnd_Const_One", "ONE", FieldType.NUMERIC, 1);
        whiteout_All = localVariables.newFieldInRecord("whiteout_All", "WHITEOUT-ALL", FieldType.STRING, 60);

        whiteout_All__R_Field_4 = localVariables.newGroupInRecord("whiteout_All__R_Field_4", "REDEFINE", whiteout_All);
        whiteout_All_Whiteout_Pos = whiteout_All__R_Field_4.newFieldArrayInGroup("whiteout_All_Whiteout_Pos", "WHITEOUT-POS", FieldType.STRING, 1, new 
            DbsArrayController(1, 60));

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Ws1 = pnd_Ws.newFieldInGroup("pnd_Ws_Ws1", "WS1", FieldType.STRING, 120);
        pnd_Ws_Edited_Ppcn = pnd_Ws.newFieldInGroup("pnd_Ws_Edited_Ppcn", "EDITED-PPCN", FieldType.STRING, 9);
        pnd_Ws_Edited_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Edited_Date", "EDITED-DATE", FieldType.STRING, 11);
        pnd_Ws_Fund_Description = localVariables.newFieldInRecord("pnd_Ws_Fund_Description", "#WS-FUND-DESCRIPTION", FieldType.STRING, 22);
        pnd_Orgn_Chk_Dte = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte", "#ORGN-CHK-DTE", FieldType.NUMERIC, 9);

        pnd_Orgn_Chk_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte__R_Field_5", "REDEFINE", pnd_Orgn_Chk_Dte);
        pnd_Orgn_Chk_Dte_Pnd_Filler = pnd_Orgn_Chk_Dte__R_Field_5.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A = pnd_Orgn_Chk_Dte__R_Field_5.newFieldInGroup("pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A", "#ORGN-CHK-DTE-A", FieldType.STRING, 
            8);
        pnd_Orgn_Chk_Dte_A4 = localVariables.newFieldInRecord("pnd_Orgn_Chk_Dte_A4", "#ORGN-CHK-DTE-A4", FieldType.STRING, 4);

        pnd_Orgn_Chk_Dte_A4__R_Field_6 = localVariables.newGroupInRecord("pnd_Orgn_Chk_Dte_A4__R_Field_6", "REDEFINE", pnd_Orgn_Chk_Dte_A4);
        pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D = pnd_Orgn_Chk_Dte_A4__R_Field_6.newFieldInGroup("pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D", "#ORGN-CHK-DTE-D", 
            FieldType.DATE);
        pnd_Edited_Date = localVariables.newFieldInRecord("pnd_Edited_Date", "#EDITED-DATE", FieldType.STRING, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_I.setInitialValue(0);
        pnd_Const_Blank_1.setInitialValue(" ");
        pnd_Const_Whiteout_Char.setInitialValue("�");
        pnd_Const_One.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Fcpn738() throws Exception
    {
        super("Fcpn738");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
        //*    *************
        //*    * MAIN LINE *
        //*    *************
        short decideConditionsMet332 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE;//Natural: VALUE '10'
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("10"))))
        {
            decideConditionsMet332++;
                                                                                                                                                                          //Natural: PERFORM MATURITY-HEADINGS
            sub_Maturity_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '20'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("20"))))
        {
            decideConditionsMet332++;
                                                                                                                                                                          //Natural: PERFORM TPA-HEADINGS
            sub_Tpa_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '30'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("30"))))
        {
            decideConditionsMet332++;
                                                                                                                                                                          //Natural: PERFORM MDO-HEADINGS
            sub_Mdo_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '31'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("31"))))
        {
            decideConditionsMet332++;
                                                                                                                                                                          //Natural: PERFORM RECURRING-MDO-HEADINGS
            sub_Recurring_Mdo_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '40'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("40"))))
        {
            decideConditionsMet332++;
                                                                                                                                                                          //Natural: PERFORM LUMP-SUM-DEATH-HEADINGS
            sub_Lump_Sum_Death_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '50'
        else if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde().equals("50"))))
        {
            decideConditionsMet332++;
                                                                                                                                                                          //Natural: PERFORM SURVIVOR-BENEFITS-HEADINGS
            sub_Survivor_Benefits_Headings();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"E01 - Unexpected CNTRCT-TYPE-CDE",pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Type_Cde());                           //Natural: WRITE *PROGRAM 'E01 - Unexpected CNTRCT-TYPE-CDE' #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            if (Global.isEscape()) return;
            //*  ABEND?
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*    *****************
        //*    * SUBROUTINES   *
        //*    *****************
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATURITY-HEADINGS
        //*  ..... CORRESPONDING DESCRIPTION
        //* *TBLDCODA.#MODE     := 'T'
        //* *TBLDCODA.#TABLE-ID := 4
        //* *TBLDCODA.#CODE     := #WS-HEADER-RECORD.CNTRCT-OPTION-CDE
        //* *CALLNAT 'TBLDCOD' TBLDCODA MSG-INFO-SUB
        //* *#WS-REC-1.#TEXT := TBLDCODA.#TARGET
        //*  'For TIAA Contract'
        //*                                                                 VF 8/99
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-HEADINGS
        //*                                                                 VF 8/99
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-HEADINGS
        //* *  #WS-FUND-DESCRIPTION
        //*                                                                 VF 8/99
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RECURRING-MDO-HEADINGS
        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A RECCURING MIMIMUM DISTRIBUTION PROCESSING
        //*  USING PRINT LAYOUT PROTOTYPE #6 (SEE PROTOTYPE #8)
        //*  ----------------------------------------------------------------------
        //*  ....... 'Recurring Minimum Distribution Payout For ..name....'
        //* *Z IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        //* *Z  #WS.WS1 := 'PERIODIC PAYMENT FOR:'
        //* *Z ELSE
        //* *  #WS-FUND-DESCRIPTION
        //*                                                                 VF 8/99
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LUMP-SUM-DEATH-HEADINGS
        //* **  #CONST.WHITEOUT-CHAR
        //* **  #CONST.WHITEOUT-CHAR
        //* **EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        //* *  #WS-FUND-DESCRIPTION
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SURVIVOR-BENEFITS-HEADINGS
        //*  ..... CORRESPONDING DESCRIPTION
        //* **TBLDCODA.#MODE     := 'T'
        //* **TBLDCODA.#TABLE-ID := 4
        //* **TBLDCODA.#CODE     := #WS-HEADER-RECORD.CNTRCT-OPTION-CDE
        //* **CALLNAT 'TBLDCOD' TBLDCODA MSG-INFO-SUB
        //* **#WS-REC-1.#TEXT := TBLDCODA.#TARGET
        //* **  #CONST.WHITEOUT-CHAR
        //* **  #CONST.WHITEOUT-CHAR
        //* **EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        //* *  #WS-FUND-DESCRIPTION
        //*                                                                 VF 8/99
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REC
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-FUND-DESCRIPTION
        //* *#PDA-INV-ACCT-FUND-DESC1
        //* *#PDA-INV-ACCT-FUND-DESC2
    }
    //*  PROTOTYPE#4
    private void sub_Maturity_Headings() throws Exception                                                                                                                 //Natural: MATURITY-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A MATURITY PROCESSING SETTLEMENT,
        //*  USING PRINT LAYOUT PROTOTYPE #4
        //*  ----------------------------------------------------------------------
        //*  ........ USE THE CNTRCT-MODE-CDE TO DETERMINE THE SETTLEMENT WORDING
        //*  ..... SETUP THE START THE NAME FOR PRINTING IN THE PROPER POSITION
        //*  ..... SEE SPECS FOR LINE 25 IN THE INTEGRATED PUBLISHING PROTOTYPE4
        //* **TBLDCODA.#MODE     := 'T'
        //* **TBLDCODA.#TABLE-ID := 5
        //* **TBLDCODA.#CODE     := #WS-HEADER-RECORD.CNTRCT-MODE-CDE
        //* **CALLNAT 'TBLDCOD' TBLDCODA MSG-INFO-SUB
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        short decideConditionsMet432 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-MODE-CDE;//Natural: VALUE 100
        if (condition((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().equals(100))))
        {
            decideConditionsMet432++;
            tbldcoda_Pnd_Target.setValue("Monthly");                                                                                                                      //Natural: ASSIGN TBLDCODA.#TARGET := 'Monthly'
            whiteout_All_Whiteout_Pos.getValue(1,":",39).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:39 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(601) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(603)))))
        {
            decideConditionsMet432++;
            tbldcoda_Pnd_Target.setValue("Quarterly");                                                                                                                    //Natural: ASSIGN TBLDCODA.#TARGET := 'Quarterly'
            whiteout_All_Whiteout_Pos.getValue(1,":",42).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:42 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(701) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(706)))))
        {
            decideConditionsMet432++;
            tbldcoda_Pnd_Target.setValue("Semi-Annual");                                                                                                                  //Natural: ASSIGN TBLDCODA.#TARGET := 'Semi-Annual'
            whiteout_All_Whiteout_Pos.getValue(1,":",45).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:45 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().greaterOrEqual(801) && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Mode_Cde().lessOrEqual(812)))))
        {
            decideConditionsMet432++;
            tbldcoda_Pnd_Target.setValue("Annual");                                                                                                                       //Natural: ASSIGN TBLDCODA.#TARGET := 'Annual'
            whiteout_All_Whiteout_Pos.getValue(1,":",39).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:39 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            tbldcoda_Pnd_Target.setValue("Unknown");                                                                                                                      //Natural: ASSIGN TBLDCODA.#TARGET := 'Unknown'
            whiteout_All_Whiteout_Pos.getValue(1,":",45).setValue(pnd_Const_Whiteout_Char);                                                                               //Natural: ASSIGN WHITEOUT-POS ( 1:45 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ... FOR "futures" SAY "Periodic Payment"
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Rec_1_Pnd_Text.setValue("Periodic Payment For:");                                                                                                      //Natural: ASSIGN #WS-REC-1.#TEXT := 'Periodic Payment For:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1_Pnd_Text.setValue(DbsUtil.compress(tbldcoda_Pnd_Target, "payment for:"));                                                                        //Natural: COMPRESS TBLDCODA.#TARGET 'payment for:' INTO #WS-REC-1.#TEXT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Ws1.setValue(whiteout_All);                                                                                                                                //Natural: ASSIGN #WS.WS1 := WHITEOUT-ALL
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("+2");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '+2'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Fcpn600_Pnd_Parm_Code.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Option_Cde());                                                                       //Natural: ASSIGN #FCPN600.#PARM-CODE := #WS-HEADER-RECORD.CNTRCT-OPTION-CDE
        pnd_Fcpn600_Pnd_Parm_Desc.reset();                                                                                                                                //Natural: RESET #PARM-DESC
        DbsUtil.callnat(Fcpn600.class , getCurrentProcessState(), pnd_Fcpn600_Pnd_Parm_Code, pnd_Fcpn600_Pnd_Parm_Desc);                                                  //Natural: CALLNAT 'FCPN600' #FCPN600.#PARM-CODE #FCPN600.#PARM-DESC
        if (condition(Global.isEscape())) return;
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Fcpn600_Pnd_Parm_Desc);                                                                                                        //Natural: ASSIGN #WS-REC-1.#TEXT := #FCPN600.#PARM-DESC
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" 1");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' 1'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  ....  "for TIAA" OR "for CREF" AND THE CONTRACT NUMBER(S)
        pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                          //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
        //*  ......... TIAA CONTRACT OR CREF CERTIFICATE NUMBERS
        //*  TIAA
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))          //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
        {
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T")))                                                                            //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T'
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Traditional Contract", pnd_Ws_Edited_Ppcn, "in settlement of contract(s)"));                               //Natural: COMPRESS 'For TIAA Traditional Contract' #WS.EDITED-PPCN 'in settlement of contract(s)' INTO #WS.WS1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Real Estate Contract", pnd_Ws_Edited_Ppcn, "in settlement of contract(s)"));                               //Natural: COMPRESS 'For TIAA Real Estate Contract' #WS.EDITED-PPCN 'in settlement of contract(s)' INTO #WS.WS1
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... ADD TO THE TEXT THE ADDITIONAL PPCN's
            short decideConditionsMet482 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CNTRCT-DA-TIAA-1-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")))
            {
                decideConditionsMet482++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-1-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-2-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr().notEquals(" ")))
            {
                decideConditionsMet482++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-2-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-3-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr().notEquals(" ")))
            {
                decideConditionsMet482++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-3-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet482++;
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet482++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet482++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-4-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Edited_Ppcn));                                                                                                //Natural: COMPRESS #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-5-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr().notEquals(" ")))
            {
                decideConditionsMet482++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-5-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet482++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet482 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-FUND-DESCRIPTION
            sub_Obtain_Fund_Description();
            if (condition(Global.isEscape())) {return;}
            //*  PRODUCT NAME
            pnd_Ws_Ws1.setValue(DbsUtil.compress("For CREF", pnd_Ws_Fund_Description, "Certificate", pnd_Ws_Edited_Ppcn, "in settlement of Certificate(s)"));             //Natural: COMPRESS 'For CREF' #WS-FUND-DESCRIPTION 'Certificate' #WS.EDITED-PPCN 'in settlement of Certificate(s)' INTO #WS.WS1
            //*  ..... ADD TO THE TEXT THE ADDITIONAL PPCN's
            short decideConditionsMet524 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CNTRCT-DA-CREF-1-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr().notEquals(" ")))
            {
                decideConditionsMet524++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-1-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-2-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr().notEquals(" ")))
            {
                decideConditionsMet524++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-2-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-3-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr().notEquals(" ")))
            {
                decideConditionsMet524++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-3-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet524++;
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet524++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet524++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-4-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Edited_Ppcn));                                                                                                //Natural: COMPRESS #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-5-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr().notEquals(" ")))
            {
                decideConditionsMet524++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-5-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet524++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet524 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  TIAA CONTRACT OR CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... ANNUITY START DATE
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        //*  .... FOR "Futures" AVOID ANNUITY START DATE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress("Annuity Starting Date:", pnd_Ws_Edited_Date));                                                                          //Natural: COMPRESS 'Annuity Starting Date:' #WS.EDITED-DATE INTO #WS.WS1
            whiteout_All_Whiteout_Pos.getValue(1,":",3).setValue(pnd_Const_Whiteout_Char);                                                                                //Natural: ASSIGN WHITEOUT-POS ( 1:03 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte().notEquals(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-DTE NE 0
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Eft_Dte(),new ReportEditMask("MM/DD/YY"));                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-EFT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Payment Date:", pnd_Ws_Edited_Date));                                                             //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Payment Date:' #WS.EDITED-DATE INTO #WS.WS1
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().notEquals("F") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND NE 'F' AND #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa700.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            getReports().display(0, pdaFcpa700.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte());                                                                           //Natural: DISPLAY #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            if (Global.isEscape()) return;
            pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                    //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
            pnd_Edited_Date.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YY"));                                                        //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YY ) TO #EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Original Check Date: ", pnd_Edited_Date));                                                    //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Original Check Date: ' #EDITED-DATE INTO #WS.WS1
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("13");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '13'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  MATURITY-HEADINGS
    }
    //*  PROTOTYPE#8
    private void sub_Tpa_Headings() throws Exception                                                                                                                      //Natural: TPA-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A TRANSFER PAYOUT PROCESSING
        //*  USING PRINT LAYOUT PROTOTYPE #8
        //*  ----------------------------------------------------------------------
        //*  .............. 'Transfer Payout For ..name....'
        //*  ... FOR "futures" SAY "Periodic Payment"
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.setValue("Periodic Payment for:");                                                                                                                 //Natural: ASSIGN #WS.WS1 := 'Periodic Payment for:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Ws1.setValue("Transfer Payout Annuity Payment for");                                                                                                   //Natural: ASSIGN #WS.WS1 := 'Transfer Payout Annuity Payment for'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  ..... EMPTY LINE IN PLACE OF 'Decedent name....'
        pnd_Ws_Rec_1_Pnd_Text.reset();                                                                                                                                    //Natural: RESET #WS-REC-1.#TEXT
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" !");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' !'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  ....  "for TIAA"  PPCN
        pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                          //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
        pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Contract", pnd_Ws_Edited_Ppcn, "in settlement of contract(s)"));                                                   //Natural: COMPRESS 'For TIAA Contract' #WS.EDITED-PPCN 'in settlement of contract(s)' INTO #WS.WS1
        //*  ..... ADD TO THE TEXT THE ADDITIONAL PPCN's
        short decideConditionsMet623 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CNTRCT-DA-TIAA-1-NBR NE ' '
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")))
        {
            decideConditionsMet623++;
            pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr(),new ReportEditMask("XXXXXXX-X"));                                 //Natural: MOVE EDITED CNTRCT-DA-TIAA-1-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                        //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-DA-TIAA-2-NBR NE ' '
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr().notEquals(" ")))
        {
            decideConditionsMet623++;
            pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr(),new ReportEditMask("XXXXXXX-X"));                                 //Natural: MOVE EDITED CNTRCT-DA-TIAA-2-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
            pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                        //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                        //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-DA-TIAA-3-NBR NE ' '
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr().notEquals(" ")))
        {
            decideConditionsMet623++;
            pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr(),new ReportEditMask("XXXXXXX-X"));                                 //Natural: MOVE EDITED CNTRCT-DA-TIAA-3-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
            pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                        //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                        //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
        {
            decideConditionsMet623++;
            //*  ALWAYS
            pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                        //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: WHEN #CONST.ONE = #CONST.ONE
        if (condition(pnd_Const_One.equals(pnd_Const_One)))
        {
            decideConditionsMet623++;
            pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                   //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
            pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                           //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
            sub_Print_Rec();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
        {
            decideConditionsMet623++;
            pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr(),new ReportEditMask("XXXXXXX-X"));                                 //Natural: MOVE EDITED CNTRCT-DA-TIAA-4-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Edited_Ppcn));                                                                                                    //Natural: COMPRESS #WS.EDITED-PPCN INTO #WS.WS1
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-DA-TIAA-5-NBR NE ' '
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr().notEquals(" ")))
        {
            decideConditionsMet623++;
            pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr(),new ReportEditMask("XXXXXXX-X"));                                 //Natural: MOVE EDITED CNTRCT-DA-TIAA-5-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
            pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                        //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            //*  ALWAYS
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                        //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
        }                                                                                                                                                                 //Natural: WHEN #CONST.ONE = #CONST.ONE
        if (condition(pnd_Const_One.equals(pnd_Const_One)))
        {
            decideConditionsMet623++;
            pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                   //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
            pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                           //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
            sub_Print_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet623 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  .... LINE FOR 'Effective Date......'
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        //*  .... FOR "Futures" AVOID ANNUITY START DATE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress("Annuity Starting Date:", pnd_Ws_Edited_Date));                                                                          //Natural: COMPRESS 'Annuity Starting Date:' #WS.EDITED-DATE INTO #WS.WS1
            whiteout_All_Whiteout_Pos.getValue(1,":",3).setValue(pnd_Const_Whiteout_Char);                                                                                //Natural: ASSIGN WHITEOUT-POS ( 1:03 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte().notEquals(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-DTE NE 0
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Eft_Dte(),new ReportEditMask("MM/DD/YY"));                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-EFT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Payment Date:", pnd_Ws_Edited_Date));                                                             //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Payment Date:' #WS.EDITED-DATE INTO #WS.WS1
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().notEquals("F") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND NE 'F' AND #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa700.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                    //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
            pnd_Edited_Date.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YY"));                                                        //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YY ) TO #EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Original Check Date: ", pnd_Edited_Date));                                                    //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Original Check Date: ' #EDITED-DATE INTO #WS.WS1
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("13");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '13'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  TPA-HEADINGS
    }
    //*  PROTOTYPE#6
    private void sub_Mdo_Headings() throws Exception                                                                                                                      //Natural: MDO-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A MIMIMUM DISTRIBUTION PROCESSING
        //*  USING PRINT LAYOUT PROTOTYPE #6 (SEE PROTOTYPE #8)
        //*  ----------------------------------------------------------------------
        //*  .............. 'Initial Minimum Distribution Payout For ..name....'
        //*  ... FOR "futures" SAY "Periodic Payment"
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.setValue("Periodic Payment for:");                                                                                                                 //Natural: ASSIGN #WS.WS1 := 'Periodic Payment for:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Ws1.setValue("Initial Minimum Distribution payment for:");                                                                                             //Natural: ASSIGN #WS.WS1 := 'Initial Minimum Distribution payment for:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  ..... EMPTY LINE IN PLACE OF 'Decedent name....'
        pnd_Ws_Rec_1_Pnd_Text.reset();                                                                                                                                    //Natural: RESET #WS-REC-1.#TEXT
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" !");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' !'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  .... USE THE 2 LINES "For TIAA...' and the next line "FOR CREF.."
        //*  .... AS FREE FORM FOR EITHER TIAA AND UP TO 5 PPCN's, or for CREF
        //*  .... AND UP TO 5 PPCN's.
        //*  ....  "for TIAA"  PPCN
        //*  ....  "for TIAA" OR "for CREF" AND THE CONTRACT NUMBER(S)
        pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                          //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
        //*  ......... TIAA CONTRACT OR CREF CERTIFICATE NUMBERS
        //*  TIAA
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))          //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
        {
            //*  11-10-95 FRANK
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))      //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Contract", pnd_Ws_Edited_Ppcn, "in settlement of contract(s)"));                                           //Natural: COMPRESS 'For TIAA Contract' #WS.EDITED-PPCN 'in settlement of contract(s)' INTO #WS.WS1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Real Estate Contract", pnd_Ws_Edited_Ppcn, "in settlement of contract(s)"));                               //Natural: COMPRESS 'For TIAA Real Estate Contract' #WS.EDITED-PPCN 'in settlement of contract(s)' INTO #WS.WS1
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... ADD TO THE TEXT THE ADDITIONAL PPCN's
            short decideConditionsMet733 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CNTRCT-DA-TIAA-1-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")))
            {
                decideConditionsMet733++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-1-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-2-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr().notEquals(" ")))
            {
                decideConditionsMet733++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-2-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-3-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr().notEquals(" ")))
            {
                decideConditionsMet733++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-3-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet733++;
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet733++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet733++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-4-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Edited_Ppcn));                                                                                                //Natural: COMPRESS #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-5-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr().notEquals(" ")))
            {
                decideConditionsMet733++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-5-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet733++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet733 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-FUND-DESCRIPTION
            sub_Obtain_Fund_Description();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Ws1.setValue(DbsUtil.compress("For CREF", "Certificate", pnd_Ws_Edited_Ppcn, "in settlement of Certificate(s)"));                                      //Natural: COMPRESS 'For CREF' 'Certificate' #WS.EDITED-PPCN 'in settlement of Certificate(s)' INTO #WS.WS1
            //*  ..... ADD TO THE TEXT THE ADDITIONAL PPCN's
            short decideConditionsMet774 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CNTRCT-DA-CREF-1-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr().notEquals(" ")))
            {
                decideConditionsMet774++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-1-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-2-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr().notEquals(" ")))
            {
                decideConditionsMet774++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-2-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-3-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr().notEquals(" ")))
            {
                decideConditionsMet774++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-3-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet774++;
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet774++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet774++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-4-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Edited_Ppcn));                                                                                                //Natural: COMPRESS #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-5-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr().notEquals(" ")))
            {
                decideConditionsMet774++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-5-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet774++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet774 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  TIAA CONTRACT OR CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... EFFECTIVE DATE.....
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        //*  .... FOR "Futures" AVOID ANNUITY START DATE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress("Effective Date:", pnd_Ws_Edited_Date));                                                                                 //Natural: COMPRESS 'Effective Date:' #WS.EDITED-DATE INTO #WS.WS1
            whiteout_All_Whiteout_Pos.getValue(1,":",3).setValue(pnd_Const_Whiteout_Char);                                                                                //Natural: ASSIGN WHITEOUT-POS ( 1:03 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte().notEquals(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-DTE NE 0
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Eft_Dte(),new ReportEditMask("MM/DD/YY"));                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-EFT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Payment Date:", pnd_Ws_Edited_Date));                                                             //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Payment Date:' #WS.EDITED-DATE INTO #WS.WS1
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().notEquals("F") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND NE 'F' AND #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa700.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                    //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
            pnd_Edited_Date.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YY"));                                                        //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YY ) TO #EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Original Check Date: ", pnd_Edited_Date));                                                    //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Original Check Date: ' #EDITED-DATE INTO #WS.WS1
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("13");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '13'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  MDO-HEADINGS
    }
    //*  PROTOTYPE#6
    private void sub_Recurring_Mdo_Headings() throws Exception                                                                                                            //Natural: RECURRING-MDO-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Ws1.setValue("Recurring Minimum Distribution payment for:");                                                                                               //Natural: ASSIGN #WS.WS1 := 'Recurring Minimum Distribution payment for:'
        //* *Z END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  ..... EMPTY LINE IN PLACE OF 'Decedent name....'
        pnd_Ws_Rec_1_Pnd_Text.reset();                                                                                                                                    //Natural: RESET #WS-REC-1.#TEXT
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" !");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' !'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  .... USE THE 2 LINES "For TIAA...' and the next line "FOR CREF.."
        //*  .... AS FREE FORM FOR EITHER TIAA AND UP TO 5 PPCN's, or for CREF
        //*  .... AND UP TO 5 PPCN's.
        //*  ....  "for TIAA"  PPCN
        //*  ....  "for TIAA" OR "for CREF" AND THE CONTRACT NUMBER(S)
        pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                          //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
        //*  ......... TIAA CONTRACT OR CREF CERTIFICATE NUMBERS
        //*  TIAA
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))          //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
        {
            //*  11-10-95 FRANK
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))      //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Contract", pnd_Ws_Edited_Ppcn));                                                                           //Natural: COMPRESS 'For TIAA Contract' #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Real Estate Contract", pnd_Ws_Edited_Ppcn));                                                               //Natural: COMPRESS 'For TIAA Real Estate Contract' #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                   //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
            pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                           //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
            sub_Print_Rec();
            if (condition(Global.isEscape())) {return;}
            //*  CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-FUND-DESCRIPTION
            sub_Obtain_Fund_Description();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Ws1.setValue(DbsUtil.compress("For CREF", "Certificate", pnd_Ws_Edited_Ppcn));                                                                         //Natural: COMPRESS 'For CREF' 'Certificate' #WS.EDITED-PPCN INTO #WS.WS1
            pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                   //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
            pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                           //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
            sub_Print_Rec();
            if (condition(Global.isEscape())) {return;}
            //*  TIAA CONTRACT OR CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... EMPTY LINE FOR 'Cref certificate....'
        pnd_Ws_Rec_1_Pnd_Text.reset();                                                                                                                                    //Natural: RESET #WS-REC-1.#TEXT
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  .... EFFECTIVE DATE.....
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        //*  .... FOR "Futures" AVOID ANNUITY START DATE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress("Effective Date:", pnd_Ws_Edited_Date));                                                                                 //Natural: COMPRESS 'Effective Date:' #WS.EDITED-DATE INTO #WS.WS1
            whiteout_All_Whiteout_Pos.getValue(1,":",3).setValue(pnd_Const_Whiteout_Char);                                                                                //Natural: ASSIGN WHITEOUT-POS ( 1:03 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte().notEquals(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-DTE NE 0
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Eft_Dte(),new ReportEditMask("MM/DD/YY"));                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-EFT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Payment Date:", pnd_Ws_Edited_Date));                                                             //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Payment Date:' #WS.EDITED-DATE INTO #WS.WS1
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().notEquals("F") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND NE 'F' AND #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa700.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                    //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
            pnd_Edited_Date.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YY"));                                                        //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YY ) TO #EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Original Check Date: ", pnd_Edited_Date));                                                    //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Original Check Date: ' #EDITED-DATE INTO #WS.WS1
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("13");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '13'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  RECURRING-MDO-HEADINGS
    }
    //*  PROTOTYPE#6
    private void sub_Lump_Sum_Death_Headings() throws Exception                                                                                                           //Natural: LUMP-SUM-DEATH-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A LUMP SUM DEATH CLAIM PROCESSING
        //*  USING PRINT LAYOUT PROTOTYPE #6 (SEE PROTOTYPE #8)
        //*  ----------------------------------------------------------------------
        //*  ....... 'Lump Sum.. Payout For ..name....'
        //*  ... FOR "futures" SAY "Periodic Payment"
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.setValue("Periodic Payment For:");                                                                                                                 //Natural: ASSIGN #WS.WS1 := 'Periodic Payment For:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Ws1.setValue("Lump Sum Payment for:");                                                                                                                 //Natural: ASSIGN #WS.WS1 := 'Lump Sum Payment for:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  .....  LINE FOR 'Decedent name....'
        //*  ??
        //*  ' !'
        pnd_Ws_Ws1.setValue(DbsUtil.compress("Decedent Name:", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Deceased_Nme().getValue(1)));                                    //Natural: COMPRESS 'Decedent Name:' #WS-NAME-N-ADDRESS.PYMNT-DECEASED-NME ( 1 ) INTO #WS.WS1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  ......... TIAA CONTRACT OR CREF CERTIFICATE NUMBERS
        //*  TIAA
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))          //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
        {
            pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                      //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
            //* 11-10-95 FRANK
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))      //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("In settlement of TIAA Contract", pnd_Ws_Edited_Ppcn));                                                              //Natural: COMPRESS 'In settlement of TIAA Contract' #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("In settlement of TIAA Real Estate Contract", pnd_Ws_Edited_Ppcn));                                                  //Natural: COMPRESS 'In settlement of TIAA Real Estate Contract' #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                   //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
            pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                           //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
            sub_Print_Rec();
            if (condition(Global.isEscape())) {return;}
            //*  CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Cref_Nbr(),new ReportEditMask("XXXXXXX-X"));                                      //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-CREF-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                                                                                                                                                                          //Natural: PERFORM OBTAIN-FUND-DESCRIPTION
            sub_Obtain_Fund_Description();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Ws1.setValue(DbsUtil.compress("In settlement of CREF", "Certificate", pnd_Ws_Edited_Ppcn));                                                            //Natural: COMPRESS 'In settlement of CREF' 'Certificate' #WS.EDITED-PPCN INTO #WS.WS1
            pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                   //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
            pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                           //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
            sub_Print_Rec();
            if (condition(Global.isEscape())) {return;}
            //*  TIAA CONTRACT OR CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... EMPTY LINE FOR 'Cref certificate....'
        pnd_Ws_Rec_1_Pnd_Text.reset();                                                                                                                                    //Natural: RESET #WS-REC-1.#TEXT
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  .... EFFECTIVE DATE.....
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        //*  .... FOR "Futures" AVOID ANNUITY START DATE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress("Effective Date:", pnd_Ws_Edited_Date));                                                                                 //Natural: COMPRESS 'Effective Date:' #WS.EDITED-DATE INTO #WS.WS1
            whiteout_All_Whiteout_Pos.getValue(1,":",3).setValue(pnd_Const_Whiteout_Char);                                                                                //Natural: ASSIGN WHITEOUT-POS ( 1:03 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte().notEquals(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-DTE NE 0
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Eft_Dte(),new ReportEditMask("MM/DD/YY"));                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-EFT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Payment Date:", pnd_Ws_Edited_Date));                                                             //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Payment Date:' #WS.EDITED-DATE INTO #WS.WS1
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("13");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '13'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  LUMP-SUM-DEATH-HEADINGS /* PROTOTYPE#6
    }
    //*  PROTOTYPE#4
    private void sub_Survivor_Benefits_Headings() throws Exception                                                                                                        //Natural: SURVIVOR-BENEFITS-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE THE HEADINGS FOR A SURVIVOR BENEFITS PROCESSING,
        //*  USING PRINT LAYOUT PROTOTYPE #4
        //*  ----------------------------------------------------------------------
        //*  ... FOR "futures" SAY "Periodic Payment"
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.setValue("Periodic Payment For:");                                                                                                                 //Natural: ASSIGN #WS.WS1 := 'Periodic Payment For:'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Ws1.setValue("Survivor Benefit Payment for:");                                                                                                         //Natural: ASSIGN #WS.WS1 := 'Survivor Benefit Payment for:'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... COMPOSE THE NAME FOR PRINTING
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_First_Name().getValue(1), pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Middle_Name().getValue(1),  //Natural: COMPRESS #WS.WS1 PH-FIRST-NAME ( 1 ) PH-MIDDLE-NAME ( 1 ) PH-LAST-NAME ( 1 ) INTO #WS.WS1
            pdaFcpa700.getPnd_Ws_Name_N_Address_Ph_Last_Name().getValue(1)));
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("11");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '11'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Fcpn600_Pnd_Parm_Code.setValue(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Option_Cde());                                                                       //Natural: ASSIGN #FCPN600.#PARM-CODE := #WS-HEADER-RECORD.CNTRCT-OPTION-CDE
        pnd_Fcpn600_Pnd_Parm_Desc.reset();                                                                                                                                //Natural: RESET #PARM-DESC
        DbsUtil.callnat(Fcpn600.class , getCurrentProcessState(), pnd_Fcpn600_Pnd_Parm_Code, pnd_Fcpn600_Pnd_Parm_Desc);                                                  //Natural: CALLNAT 'FCPN600' #FCPN600.#PARM-CODE #FCPN600.#PARM-DESC
        if (condition(Global.isEscape())) return;
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Fcpn600_Pnd_Parm_Desc);                                                                                                        //Natural: ASSIGN #WS-REC-1.#TEXT := #FCPN600.#PARM-DESC
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" 1");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' 1'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  .....  LINE FOR 'Decedent name....'
        //*  ??
        //* ' !'
        pnd_Ws_Ws1.setValue(DbsUtil.compress("Decedent Name:", pdaFcpa700.getPnd_Ws_Name_N_Address_Pymnt_Deceased_Nme().getValue(1)));                                    //Natural: COMPRESS 'Decedent Name:' #WS-NAME-N-ADDRESS.PYMNT-DECEASED-NME ( 1 ) INTO #WS.WS1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  ....  "for TIAA" OR "for CREF" AND THE CONTRACT NUMBER(S)
        pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                          //Natural: MOVE EDITED #WS-HEADER-RECORD.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
        //*  ......... TIAA CONTRACT OR CREF CERTIFICATE NUMBERS
        //*  TIAA
        if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))          //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
        {
            if (condition(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("T") || pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1).equals("R")))      //Natural: IF #WS-OCCURS.INV-ACCT-CDE ( 1 ) = 'T' OR = 'R'
            {
                //* 11-10-95 FRANK
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Contract", pnd_Ws_Edited_Ppcn, "in settlement of contract(s)"));                                           //Natural: COMPRESS 'For TIAA Contract' #WS.EDITED-PPCN 'in settlement of contract(s)' INTO #WS.WS1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Ws1.setValue(DbsUtil.compress("For TIAA Real Estate Contract", pnd_Ws_Edited_Ppcn, "in settlement of contract(s)"));                               //Natural: COMPRESS 'For TIAA Real Estate Contract' #WS.EDITED-PPCN 'in settlement of contract(s)' INTO #WS.WS1
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... ADD TO THE TEXT THE ADDITIONAL PPCN's
            short decideConditionsMet1043 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CNTRCT-DA-TIAA-1-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")))
            {
                decideConditionsMet1043++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-1-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-2-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr().notEquals(" ")))
            {
                decideConditionsMet1043++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-2-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-3-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr().notEquals(" ")))
            {
                decideConditionsMet1043++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-3-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet1043++;
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet1043++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet1043++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-4-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Edited_Ppcn));                                                                                                //Natural: COMPRESS #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-TIAA-5-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr().notEquals(" ")))
            {
                decideConditionsMet1043++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-TIAA-5-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet1043++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet1043 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-FUND-DESCRIPTION
            sub_Obtain_Fund_Description();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Ws1.setValue(DbsUtil.compress("For CREF", "Certificate", pnd_Ws_Edited_Ppcn, "in settlement of Certificate(s)"));                                      //Natural: COMPRESS 'For CREF' 'Certificate' #WS.EDITED-PPCN 'in settlement of Certificate(s)' INTO #WS.WS1
            //*  ..... ADD TO THE TEXT THE ADDITIONAL PPCN's
            short decideConditionsMet1084 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CNTRCT-DA-CREF-1-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr().notEquals(" ")))
            {
                decideConditionsMet1084++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-1-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-2-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr().notEquals(" ")))
            {
                decideConditionsMet1084++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-2-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-3-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr().notEquals(" ")))
            {
                decideConditionsMet1084++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-3-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet1084++;
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet1084++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-4-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr().notEquals(" ")))
            {
                decideConditionsMet1084++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-4-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Edited_Ppcn));                                                                                                //Natural: COMPRESS #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN CNTRCT-DA-CREF-5-NBR NE ' '
            if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr().notEquals(" ")))
            {
                decideConditionsMet1084++;
                pnd_Ws_Edited_Ppcn.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr(),new ReportEditMask("XXXXXXX-X"));                             //Natural: MOVE EDITED CNTRCT-DA-CREF-5-NBR ( EM = XXXXXXX-X ) TO #WS.EDITED-PPCN
                pnd_Ws_Ws1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ws_Ws1, ","));                                                                    //Natural: COMPRESS #WS.WS1 ',' INTO #WS.WS1 LEAVING NO SPACE
                //*  ALWAYS
                pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, pnd_Ws_Edited_Ppcn));                                                                                    //Natural: COMPRESS #WS.WS1 #WS.EDITED-PPCN INTO #WS.WS1
            }                                                                                                                                                             //Natural: WHEN #CONST.ONE = #CONST.ONE
            if (condition(pnd_Const_One.equals(pnd_Const_One)))
            {
                decideConditionsMet1084++;
                pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                               //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
                pnd_Ws_Rec_1_Pnd_Cc.setValue(" 7");                                                                                                                       //Natural: ASSIGN #WS-REC-1.#CC := ' 7'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
                sub_Print_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Ws1.reset();                                                                                                                                       //Natural: RESET #WS.WS1
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet1084 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  TIAA CONTRACT OR CREF CERTIFICATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... ANNUITY START DATE
        whiteout_All.reset();                                                                                                                                             //Natural: RESET WHITEOUT-ALL
        //*  .... FOR "Futures" AVOID ANNUITY START DATE
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND = 'F'
        {
            pnd_Ws_Ws1.reset();                                                                                                                                           //Natural: RESET #WS.WS1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress("Annuity Starting Date:", pnd_Ws_Edited_Date));                                                                          //Natural: COMPRESS 'Annuity Starting Date:' #WS.EDITED-DATE INTO #WS.WS1
            whiteout_All_Whiteout_Pos.getValue(1,":",3).setValue(pnd_Const_Whiteout_Char);                                                                                //Natural: ASSIGN WHITEOUT-POS ( 1:03 ) := #CONST.WHITEOUT-CHAR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte().notEquals(getZero())))                                                                         //Natural: IF #WS-HEADER-RECORD.PYMNT-CHECK-DTE NE 0
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YY"));                                       //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Edited_Date.setValueEdited(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Eft_Dte(),new ReportEditMask("MM/DD/YY"));                                         //Natural: MOVE EDITED #WS-HEADER-RECORD.PYMNT-EFT-DTE ( EM = MM/DD/YY ) TO #WS.EDITED-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Payment Date:", pnd_Ws_Edited_Date));                                                             //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Payment Date:' #WS.EDITED-DATE INTO #WS.WS1
        //*  FUTURE PAYMENT
        if (condition(pdaFcpa700.getPnd_Ws_Header_Record_Pymnt_Ftre_Ind().notEquals("F") && pdaFcpa700.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND NE 'F' AND #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            pnd_Orgn_Chk_Dte.compute(new ComputeParameters(false, pnd_Orgn_Chk_Dte), DbsField.subtract(100000000,pdaFcpa700.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte())); //Natural: ASSIGN #ORGN-CHK-DTE := 100000000 - #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
            pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Orgn_Chk_Dte_Pnd_Orgn_Chk_Dte_A);                                    //Natural: MOVE EDITED #ORGN-CHK-DTE-A TO #ORGN-CHK-DTE-D ( EM = YYYYMMDD )
            pnd_Edited_Date.setValueEdited(pnd_Orgn_Chk_Dte_A4_Pnd_Orgn_Chk_Dte_D,new ReportEditMask("MM/DD/YY"));                                                        //Natural: MOVE EDITED #ORGN-CHK-DTE-D ( EM = MM/DD/YY ) TO #EDITED-DATE
            pnd_Ws_Ws1.setValue(DbsUtil.compress(pnd_Ws_Ws1, whiteout_All, "Original Check Date: ", pnd_Edited_Date));                                                    //Natural: COMPRESS #WS.WS1 WHITEOUT-ALL 'Original Check Date: ' #EDITED-DATE INTO #WS.WS1
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ws_Ws1), new ExamineSearch(pnd_Const_Whiteout_Char), new ExamineReplace(pnd_Const_Blank_1));                                //Natural: EXAMINE #WS.WS1 FOR #CONST.WHITEOUT-CHAR REPLACE #CONST.BLANK-1
        pnd_Ws_Rec_1_Pnd_Text.setValue(pnd_Ws_Ws1);                                                                                                                       //Natural: ASSIGN #WS-REC-1.#TEXT := #WS.WS1
        pnd_Ws_Rec_1_Pnd_Cc.setValue("13");                                                                                                                               //Natural: ASSIGN #WS-REC-1.#CC := '13'
                                                                                                                                                                          //Natural: PERFORM PRINT-REC
        sub_Print_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  SURVIVOR-BENEFITS-HEADINGS /* PROTOTYPE#4
    }
    private void sub_Print_Rec() throws Exception                                                                                                                         //Natural: PRINT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  WRITE THE "PRINT" RECORD TO A WORK FILE
        //*  ----------------------------------------------------------------------
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Rec_1.reset();                                                                                                                                             //Natural: RESET #WS-REC-1
    }
    private void sub_Obtain_Fund_Description() throws Exception                                                                                                           //Natural: OBTAIN-FUND-DESCRIPTION
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GET THE DESCRIPTION FOR A CREF FUND. I.E. "Money Market", "Stock", ETC
        //*  ----------------------------------------------------------------------
        //*  ...... OBTAIN INVESTMENT ACCOUNT DESCRIPTION FROM TABLE
        //* *ASSIGN #PDA-INV-ACCT = #WS-OCCURS.INV-ACCT-CDE (1)           /* LZ
        //*  LZ
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa700.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(1));                                                //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = #WS-OCCURS.INV-ACCT-CDE ( 1 )
        //* * CALLNAT 'FCPN199' #PDA-INV-ACCT #PDA-INV-ACCT-INFO          /* LZ
        //*  LZ
        DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                        //Natural: CALLNAT 'FCPN199A' #FUND-PDA
        if (condition(Global.isEscape())) return;
        //*  ...... INDICATE WHETHER ITS A TIAA OR CREF INVESTMENT ACCOUNT
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Hrd_Line_1().equals("Inflation-")))                                                                                 //Natural: IF #FUND-PDA.#HRD-LINE-1 = 'Inflation-'
        {
            pnd_Ws_Fund_Description.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaFcpa199a.getPnd_Fund_Pda_Pnd_Hrd_Line_1(), pdaFcpa199a.getPnd_Fund_Pda_Pnd_Hrd_Line_2())); //Natural: COMPRESS #FUND-PDA.#HRD-LINE-1 #FUND-PDA.#HRD-LINE-2 INTO #WS-FUND-DESCRIPTION LEAVE NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Fund_Description.setValue(DbsUtil.compress(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Hrd_Line_1(), pdaFcpa199a.getPnd_Fund_Pda_Pnd_Hrd_Line_2()));               //Natural: COMPRESS #FUND-PDA.#HRD-LINE-1 #FUND-PDA.#HRD-LINE-2 INTO #WS-FUND-DESCRIPTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  OBTAIN-FUND-DESCRIPTION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, pdaFcpa700.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte());
    }
}
