/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:43 PM
**        *   FROM NATURAL MAP   :  Appm223
************************************************************
**        * FILE NAME               : Appm223.java
**        * CLASS NAME              : Appm223
**        * INSTANCE NAME           : Appm223
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PRAP-APPL-CNT #PRAP-CNT #PRAP-NEW-APPL-CNT                                                                              *     #PRAP-NEW-APPL-DEL-CNT 
    #PRAP-NEW-PREM-CNT #PRAP-OLD-APPL-CNT                                                             *     #PRAP-OLD-PREM-CNT #PRAP-OTHR-CNT #PRAP-PREM-CNT 
    #PRAP-UPDT-CNT                                                          *     DISPLAY-MAP-DATE REGION-WRONG-COUNT                                   
    *     ROMAN-GLOBAL.#CNT-PRODUCT-CODE(*)                                                                                        *     ROMAN-GLOBAL.DISPLAY-CONTRACTS-TOT 
    *     ROMAN-GLOBAL.DISPLAY-CURR-CONTRACT(*)                                                                                    *     ROMAN-GLOBAL.DISPLAY-NUMS-CONTRACT(*) 
    *     ROMAN-GLOBAL.DISPLAY-OUT-BALANCE                                                                                         *     ROMAN-GLOBAL.DISPLAY-PREV-CONTRACT(*) 
    TOTAL-ASSIGNED-PREMIUMS                                                            *     TOTAL-ISSUED-APPLICATIONS
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appm223 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Prap_Appl_Cnt;
    private DbsField pnd_Prap_Cnt;
    private DbsField pnd_Prap_New_Appl_Cnt;
    private DbsField pnd_Prap_New_Appl_Del_Cnt;
    private DbsField pnd_Prap_New_Prem_Cnt;
    private DbsField pnd_Prap_Old_Appl_Cnt;
    private DbsField pnd_Prap_Old_Prem_Cnt;
    private DbsField pnd_Prap_Othr_Cnt;
    private DbsField pnd_Prap_Prem_Cnt;
    private DbsField pnd_Prap_Updt_Cnt;
    private DbsField display_Map_Date;
    private DbsField region_Wrong_Count;
    private DbsField roman_Global_Pnd_Cnt_Product_Code;
    private DbsField roman_Global_Display_Contracts_Tot;
    private DbsField roman_Global_Display_Curr_Contract;
    private DbsField roman_Global_Display_Nums_Contract;
    private DbsField roman_Global_Display_Out_Balance;
    private DbsField roman_Global_Display_Prev_Contract;
    private DbsField total_Assigned_Premiums;
    private DbsField total_Issued_Applications;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Prap_Appl_Cnt = parameters.newFieldInRecord("pnd_Prap_Appl_Cnt", "#PRAP-APPL-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_Cnt = parameters.newFieldInRecord("pnd_Prap_Cnt", "#PRAP-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_New_Appl_Cnt = parameters.newFieldInRecord("pnd_Prap_New_Appl_Cnt", "#PRAP-NEW-APPL-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_New_Appl_Del_Cnt = parameters.newFieldInRecord("pnd_Prap_New_Appl_Del_Cnt", "#PRAP-NEW-APPL-DEL-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_New_Prem_Cnt = parameters.newFieldInRecord("pnd_Prap_New_Prem_Cnt", "#PRAP-NEW-PREM-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_Old_Appl_Cnt = parameters.newFieldInRecord("pnd_Prap_Old_Appl_Cnt", "#PRAP-OLD-APPL-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_Old_Prem_Cnt = parameters.newFieldInRecord("pnd_Prap_Old_Prem_Cnt", "#PRAP-OLD-PREM-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_Othr_Cnt = parameters.newFieldInRecord("pnd_Prap_Othr_Cnt", "#PRAP-OTHR-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_Prem_Cnt = parameters.newFieldInRecord("pnd_Prap_Prem_Cnt", "#PRAP-PREM-CNT", FieldType.NUMERIC, 10);
        pnd_Prap_Updt_Cnt = parameters.newFieldInRecord("pnd_Prap_Updt_Cnt", "#PRAP-UPDT-CNT", FieldType.NUMERIC, 10);
        display_Map_Date = parameters.newFieldInRecord("display_Map_Date", "DISPLAY-MAP-DATE", FieldType.DATE);
        region_Wrong_Count = parameters.newFieldInRecord("region_Wrong_Count", "REGION-WRONG-COUNT", FieldType.NUMERIC, 10);
        roman_Global_Pnd_Cnt_Product_Code = parameters.newFieldArrayInRecord("roman_Global_Pnd_Cnt_Product_Code", "ROMAN-GLOBAL.#CNT-PRODUCT-CODE", FieldType.STRING, 
            4, new DbsArrayController(1, 30));
        roman_Global_Display_Contracts_Tot = parameters.newFieldInRecord("roman_Global_Display_Contracts_Tot", "ROMAN-GLOBAL.DISPLAY-CONTRACTS-TOT", FieldType.NUMERIC, 
            10);
        roman_Global_Display_Curr_Contract = parameters.newFieldArrayInRecord("roman_Global_Display_Curr_Contract", "ROMAN-GLOBAL.DISPLAY-CURR-CONTRACT", 
            FieldType.STRING, 8, new DbsArrayController(1, 30));
        roman_Global_Display_Nums_Contract = parameters.newFieldArrayInRecord("roman_Global_Display_Nums_Contract", "ROMAN-GLOBAL.DISPLAY-NUMS-CONTRACT", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 30));
        roman_Global_Display_Out_Balance = parameters.newFieldInRecord("roman_Global_Display_Out_Balance", "ROMAN-GLOBAL.DISPLAY-OUT-BALANCE", FieldType.NUMERIC, 
            10);
        roman_Global_Display_Prev_Contract = parameters.newFieldArrayInRecord("roman_Global_Display_Prev_Contract", "ROMAN-GLOBAL.DISPLAY-PREV-CONTRACT", 
            FieldType.STRING, 8, new DbsArrayController(1, 30));
        total_Assigned_Premiums = parameters.newFieldInRecord("total_Assigned_Premiums", "TOTAL-ASSIGNED-PREMIUMS", FieldType.NUMERIC, 10);
        total_Issued_Applications = parameters.newFieldInRecord("total_Issued_Applications", "TOTAL-ISSUED-APPLICATIONS", FieldType.NUMERIC, 10);
        parameters.reset();
    }

    public Appm223() throws Exception
    {
        super("Appm223");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=040 LS=133 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Appm223", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Appm223"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "PROGRAM", "BLUE", 1, 1, 7);
            uiForm.setUiControl("astPROGRAM", Global.getPROGRAM(), true, 1, 10, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "TEACHERS INSURANCE AND ANNUITY OF AMERICA", "BLUE", 1, 31, 41);
            uiForm.setUiLabel("label_3", "RUN DATE:", "BLUE", 1, 84, 9);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 95, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "COLLEGE RETIREMENT EQUITIES FUND", "BLUE", 2, 36, 32);
            uiForm.setUiLabel("label_5", "START DATE:", "BLUE", 2, 82, 11);
            uiForm.setUiControl("display_Map_Date", display_Map_Date, true, 2, 95, 8, "WHITE", "MM/DD/YY", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_6", "DA/SRA CONTRACTS SUMMARY STATISTICS (BALANCING REPORT)", "BLUE", 4, 26, 54);
            uiForm.setUiLabel("label_7", "PREVIOUS", "BLUE", 7, 18, 8);
            uiForm.setUiLabel("label_8", "CURRENT", "BLUE", 7, 30, 7);
            uiForm.setUiLabel("label_9", "PRODUCT", "BLUE", 8, 1, 7);
            uiForm.setUiLabel("label_10", "CONTRACT", "BLUE", 8, 18, 8);
            uiForm.setUiLabel("label_11", "CONTRACT", "BLUE", 8, 30, 8);
            uiForm.setUiLabel("label_12", "DIFFERENCE", "BLUE", 8, 43, 10);
            uiForm.setUiLabel("label_13", "PRAP TOTALS", "BLUE", 8, 66, 11);
            uiForm.setUiLabel("label_14", "_______", "BLUE", 9, 1, 7);
            uiForm.setUiLabel("label_15", "_________", "BLUE", 9, 18, 9);
            uiForm.setUiLabel("label_16", "_________", "BLUE", 9, 30, 9);
            uiForm.setUiLabel("label_17", "___________", "BLUE", 9, 42, 11);
            uiForm.setUiLabel("label_18", "_____________________________________", "", 9, 57, 37);
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_1", roman_Global_Pnd_Cnt_Product_Code.getValue(1), true, 10, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_1", roman_Global_Display_Prev_Contract.getValue(1), true, 10, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_1", roman_Global_Display_Curr_Contract.getValue(1), true, 10, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_1", roman_Global_Display_Nums_Contract.getValue(1), true, 10, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "TOTAL PRAP READ", "BLUE", 10, 57, 15);
            uiForm.setUiControl("pnd_Prap_Cnt", pnd_Prap_Cnt, true, 10, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_2", roman_Global_Pnd_Cnt_Product_Code.getValue(2), true, 11, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_2", roman_Global_Display_Prev_Contract.getValue(2), true, 11, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_2", roman_Global_Display_Curr_Contract.getValue(2), true, 11, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_2", roman_Global_Display_Nums_Contract.getValue(2), true, 11, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_3", roman_Global_Pnd_Cnt_Product_Code.getValue(3), true, 12, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_3", roman_Global_Display_Prev_Contract.getValue(3), true, 12, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_3", roman_Global_Display_Curr_Contract.getValue(3), true, 12, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_3", roman_Global_Display_Nums_Contract.getValue(3), true, 12, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "TOTAL APPLICATIONS READ", "BLUE", 12, 57, 23);
            uiForm.setUiControl("pnd_Prap_Appl_Cnt", pnd_Prap_Appl_Cnt, true, 12, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_4", roman_Global_Pnd_Cnt_Product_Code.getValue(4), true, 13, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_4", roman_Global_Display_Prev_Contract.getValue(4), true, 13, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_4", roman_Global_Display_Curr_Contract.getValue(4), true, 13, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_4", roman_Global_Display_Nums_Contract.getValue(4), true, 13, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "NEW APPLICATIONS READ", "BLUE", 13, 59, 21);
            uiForm.setUiControl("pnd_Prap_New_Appl_Cnt", pnd_Prap_New_Appl_Cnt, true, 13, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_5", roman_Global_Pnd_Cnt_Product_Code.getValue(5), true, 14, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_5", roman_Global_Display_Prev_Contract.getValue(5), true, 14, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_5", roman_Global_Display_Curr_Contract.getValue(5), true, 14, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_5", roman_Global_Display_Nums_Contract.getValue(5), true, 14, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "NEW APPLICATIONS DELETED", "BLUE", 14, 59, 24);
            uiForm.setUiControl("pnd_Prap_New_Appl_Del_Cnt", pnd_Prap_New_Appl_Del_Cnt, true, 14, 84, 10, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_6", roman_Global_Pnd_Cnt_Product_Code.getValue(6), true, 15, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_6", roman_Global_Display_Prev_Contract.getValue(6), true, 15, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_6", roman_Global_Display_Curr_Contract.getValue(6), true, 15, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_6", roman_Global_Display_Nums_Contract.getValue(6), true, 15, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "OLD APPLICATIONS READ", "BLUE", 15, 59, 21);
            uiForm.setUiControl("pnd_Prap_Old_Appl_Cnt", pnd_Prap_Old_Appl_Cnt, true, 15, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_7", roman_Global_Pnd_Cnt_Product_Code.getValue(7), true, 16, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_7", roman_Global_Display_Prev_Contract.getValue(7), true, 16, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_7", roman_Global_Display_Curr_Contract.getValue(7), true, 16, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_7", roman_Global_Display_Nums_Contract.getValue(7), true, 16, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_8", roman_Global_Pnd_Cnt_Product_Code.getValue(8), true, 17, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_8", roman_Global_Display_Prev_Contract.getValue(8), true, 17, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_8", roman_Global_Display_Curr_Contract.getValue(8), true, 17, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_8", roman_Global_Display_Nums_Contract.getValue(8), true, 17, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "TOTAL", "BLUE", 17, 57, 5);
            uiForm.setUiLabel("label_25", "PREMIUMS", "BLUE", 17, 65, 8);
            uiForm.setUiLabel("label_26", "READ", "BLUE", 17, 76, 4);
            uiForm.setUiControl("pnd_Prap_Prem_Cnt", pnd_Prap_Prem_Cnt, true, 17, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_9", roman_Global_Pnd_Cnt_Product_Code.getValue(9), true, 18, 2, 4, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_9", roman_Global_Display_Prev_Contract.getValue(9), true, 18, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_9", roman_Global_Display_Curr_Contract.getValue(9), true, 18, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_9", roman_Global_Display_Nums_Contract.getValue(9), true, 18, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "NEW", "BLUE", 18, 59, 3);
            uiForm.setUiLabel("label_28", "PREMIUMS", "BLUE", 18, 65, 8);
            uiForm.setUiLabel("label_29", "READ", "BLUE", 18, 76, 4);
            uiForm.setUiControl("pnd_Prap_New_Prem_Cnt", pnd_Prap_New_Prem_Cnt, true, 18, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_10", roman_Global_Pnd_Cnt_Product_Code.getValue(10), true, 19, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_10", roman_Global_Display_Prev_Contract.getValue(10), true, 19, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_10", roman_Global_Display_Curr_Contract.getValue(10), true, 19, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_10", roman_Global_Display_Nums_Contract.getValue(10), true, 19, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "OLD", "BLUE", 19, 59, 3);
            uiForm.setUiLabel("label_31", "PREMIUMS", "BLUE", 19, 65, 8);
            uiForm.setUiLabel("label_32", "READ", "BLUE", 19, 76, 4);
            uiForm.setUiControl("pnd_Prap_Old_Prem_Cnt", pnd_Prap_Old_Prem_Cnt, true, 19, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_11", roman_Global_Pnd_Cnt_Product_Code.getValue(11), true, 20, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_11", roman_Global_Display_Prev_Contract.getValue(11), true, 20, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_11", roman_Global_Display_Curr_Contract.getValue(11), true, 20, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_11", roman_Global_Display_Nums_Contract.getValue(11), true, 20, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_12", roman_Global_Pnd_Cnt_Product_Code.getValue(12), true, 21, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_12", roman_Global_Display_Prev_Contract.getValue(12), true, 21, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_12", roman_Global_Display_Curr_Contract.getValue(12), true, 21, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_12", roman_Global_Display_Nums_Contract.getValue(12), true, 21, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "TOTAL", "BLUE", 21, 57, 5);
            uiForm.setUiLabel("label_34", "OTHERS", "BLUE", 21, 66, 6);
            uiForm.setUiLabel("label_35", "READ", "BLUE", 21, 76, 4);
            uiForm.setUiControl("pnd_Prap_Othr_Cnt", pnd_Prap_Othr_Cnt, true, 21, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_13", roman_Global_Pnd_Cnt_Product_Code.getValue(13), true, 22, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_13", roman_Global_Display_Prev_Contract.getValue(13), true, 22, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_13", roman_Global_Display_Curr_Contract.getValue(13), true, 22, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_13", roman_Global_Display_Nums_Contract.getValue(13), true, 22, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_14", roman_Global_Pnd_Cnt_Product_Code.getValue(14), true, 23, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_14", roman_Global_Display_Prev_Contract.getValue(14), true, 23, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_14", roman_Global_Display_Curr_Contract.getValue(14), true, 23, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_14", roman_Global_Display_Nums_Contract.getValue(14), true, 23, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "TOTAL PRAP UPDATED", "BLUE", 23, 57, 18);
            uiForm.setUiControl("pnd_Prap_Updt_Cnt", pnd_Prap_Updt_Cnt, true, 23, 84, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_15", roman_Global_Pnd_Cnt_Product_Code.getValue(15), true, 24, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_15", roman_Global_Display_Prev_Contract.getValue(15), true, 24, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_15", roman_Global_Display_Curr_Contract.getValue(15), true, 24, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_15", roman_Global_Display_Nums_Contract.getValue(15), true, 24, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_16", roman_Global_Pnd_Cnt_Product_Code.getValue(16), true, 25, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_16", roman_Global_Display_Prev_Contract.getValue(16), true, 25, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_16", roman_Global_Display_Curr_Contract.getValue(16), true, 25, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_16", roman_Global_Display_Nums_Contract.getValue(16), true, 25, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_17", roman_Global_Pnd_Cnt_Product_Code.getValue(17), true, 26, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_17", roman_Global_Display_Prev_Contract.getValue(17), true, 26, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_17", roman_Global_Display_Curr_Contract.getValue(17), true, 26, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_17", roman_Global_Display_Nums_Contract.getValue(17), true, 26, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_18", roman_Global_Pnd_Cnt_Product_Code.getValue(18), true, 27, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_18", roman_Global_Display_Prev_Contract.getValue(18), true, 27, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_18", roman_Global_Display_Curr_Contract.getValue(18), true, 27, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_18", roman_Global_Display_Nums_Contract.getValue(18), true, 27, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_19", roman_Global_Pnd_Cnt_Product_Code.getValue(19), true, 28, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_19", roman_Global_Display_Prev_Contract.getValue(19), true, 28, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_19", roman_Global_Display_Curr_Contract.getValue(19), true, 28, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_19", roman_Global_Display_Nums_Contract.getValue(19), true, 28, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_20", roman_Global_Pnd_Cnt_Product_Code.getValue(20), true, 29, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_20", roman_Global_Display_Prev_Contract.getValue(20), true, 29, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_20", roman_Global_Display_Curr_Contract.getValue(20), true, 29, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_20", roman_Global_Display_Nums_Contract.getValue(20), true, 29, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_21", roman_Global_Pnd_Cnt_Product_Code.getValue(21), true, 30, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_21", roman_Global_Display_Prev_Contract.getValue(21), true, 30, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_21", roman_Global_Display_Curr_Contract.getValue(21), true, 30, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_21", roman_Global_Display_Nums_Contract.getValue(21), true, 30, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_22", roman_Global_Pnd_Cnt_Product_Code.getValue(22), true, 31, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_22", roman_Global_Display_Prev_Contract.getValue(22), true, 31, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_22", roman_Global_Display_Curr_Contract.getValue(22), true, 31, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_22", roman_Global_Display_Nums_Contract.getValue(22), true, 31, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_23", roman_Global_Pnd_Cnt_Product_Code.getValue(23), true, 32, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_23", roman_Global_Display_Prev_Contract.getValue(23), true, 32, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_23", roman_Global_Display_Curr_Contract.getValue(23), true, 32, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_23", roman_Global_Display_Nums_Contract.getValue(23), true, 32, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Pnd_Cnt_Product_Code_24", roman_Global_Pnd_Cnt_Product_Code.getValue(24), true, 33, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Prev_Contract_24", roman_Global_Display_Prev_Contract.getValue(24), true, 33, 19, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Curr_Contract_24", roman_Global_Display_Curr_Contract.getValue(24), true, 33, 31, 8, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("roman_Global_Display_Nums_Contract_24", roman_Global_Display_Nums_Contract.getValue(24), true, 33, 46, 7, "WHITE", true, 
                false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "TOTAL (A)", "BLUE", 34, 30, 9);
            uiForm.setUiControl("roman_Global_Display_Contracts_Tot", roman_Global_Display_Contracts_Tot, true, 34, 43, 10, "WHITE", true, false, null, 
                "0123456789+-, ", "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "TOTAL APPLICATIONS ISSUED (B)", "BLUE", 35, 10, 29);
            uiForm.setUiControl("total_Issued_Applications", total_Issued_Applications, true, 35, 43, 10, "WHITE", true, false, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "TOTAL", "BLUE", 36, 10, 5);
            uiForm.setUiLabel("label_40", "PREMIUMS", "BLUE", 36, 17, 8);
            uiForm.setUiLabel("label_41", "ASSIGNED (C)", "BLUE", 36, 27, 12);
            uiForm.setUiControl("total_Assigned_Premiums", total_Assigned_Premiums, true, 36, 43, 10, "WHITE", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_42", "TOTAL", "BLUE", 37, 10, 5);
            uiForm.setUiLabel("label_43", "INVALID", "BLUE", 37, 18, 7);
            uiForm.setUiLabel("label_44", "REGIONS (D)", "BLUE", 37, 28, 11);
            uiForm.setUiControl("region_Wrong_Count", region_Wrong_Count, true, 37, 43, 10, "WHITE", true, false, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_45", "OUT-OF-BALANCE", "BLUE", 38, 10, 14);
            uiForm.setUiLabel("label_46", "(A-(B+C+D))", "BLUE", 38, 28, 11);
            uiForm.setUiControl("roman_Global_Display_Out_Balance", roman_Global_Display_Out_Balance, true, 38, 43, 10, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DROFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
