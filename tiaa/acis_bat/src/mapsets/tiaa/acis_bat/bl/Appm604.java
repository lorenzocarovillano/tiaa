/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:45 PM
**        *   FROM NATURAL MAP   :  Appm604
************************************************************
**        * FILE NAME               : Appm604.java
**        * CLASS NAME              : Appm604
**        * INSTANCE NAME           : Appm604
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     LIT-CORRT LIT-CORRU
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appm604 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField lit_Corrt;
    private DbsField lit_Corru;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        lit_Corrt = parameters.newFieldInRecord("lit_Corrt", "LIT-CORRT", FieldType.STRING, 15);
        lit_Corru = parameters.newFieldInRecord("lit_Corru", "LIT-CORRU", FieldType.STRING, 3);
        parameters.reset();
    }

    public Appm604() throws Exception
    {
        super("Appm604");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=001 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Appm604", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Appm604"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("lit_Corrt", lit_Corrt, true, 1, 87, 15, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("lit_Corru", lit_Corru, true, 1, 105, 3, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
