/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:44 PM
**        *   FROM NATURAL MAP   :  Appm601
************************************************************
**        * FILE NAME               : Appm601.java
**        * CLASS NAME              : Appm601
**        * INSTANCE NAME           : Appm601
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     ACIS-REPRINT-FL-VIEW.RP-PPG-TEAM-CDE REPORTLIT1
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appm601 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField acis_Reprint_Fl_View_Rp_Ppg_Team_Cde;
    private DbsField reportlit1;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        acis_Reprint_Fl_View_Rp_Ppg_Team_Cde = parameters.newFieldInRecord("acis_Reprint_Fl_View_Rp_Ppg_Team_Cde", "ACIS-REPRINT-FL-VIEW.RP-PPG-TEAM-CDE", 
            FieldType.STRING, 8);
        reportlit1 = parameters.newFieldInRecord("reportlit1", "REPORTLIT1", FieldType.STRING, 35);
        parameters.reset();
    }

    public Appm601() throws Exception
    {
        super("Appm601");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=007 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Appm601", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Appm601"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "DATE", "", 1, 2, 4);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 9, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "ANNUITY CONTRACT ISSUE SYSTEM", "", 1, 53, 29);
            uiForm.setUiLabel("label_3", "PAGE", "", 1, 115, 4);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 124, 5, "", true, true, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "TIME", "", 2, 2, 4);
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 2, 9, 10, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "CONTRACT PACKAGE CORRECTION DETAIL REPORT", "", 2, 49, 41);
            uiForm.setUiLabel("label_6", "PROGRAM", "", 2, 115, 7);
            uiForm.setUiControl("astPROGRAM", Global.getPROGRAM(), true, 2, 124, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("reportlit1", reportlit1, true, 3, 59, 35, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "TEAM:", "", 4, 2, 5);
            uiForm.setUiControl("acis_Reprint_Fl_View_Rp_Ppg_Team_Cde", acis_Reprint_Fl_View_Rp_Ppg_Team_Cde, true, 4, 9, 8, "", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "PIN#", "", 6, 2, 4);
            uiForm.setUiLabel("label_9", "POLICYHOLDER", "", 6, 18, 12);
            uiForm.setUiLabel("label_10", "CONTRACT", "", 6, 49, 8);
            uiForm.setUiLabel("label_11", "SSN#", "", 6, 63, 4);
            uiForm.setUiLabel("label_12", "SOURCE RQUESTOR CORRECTION-TYPE CORP-UPDT DESTINATION PACKAGE", "", 6, 71, 61);
            uiForm.setUiLabel("label_13", "-----------------------------------------------------------------------------------------------------------------------------------", 
                "", 7, 2, 131);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
