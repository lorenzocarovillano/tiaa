/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:45 PM
**        *   FROM NATURAL MAP   :  Appm603
************************************************************
**        * FILE NAME               : Appm603.java
**        * CLASS NAME              : Appm603
**        * INSTANCE NAME           : Appm603
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     ACIS-REPRINT-FL-VIEW.RP-COLL-CODE ACIS-REPRINT-FL-VIEW.RP-PIN-NBR                                                        *     ACIS-REPRINT-FL-VIEW.RP-RACF-ID 
    ACIS-REPRINT-FL-VIEW.RP-SOC-SEC                                                          *     ACIS-REPRINT-FL-VIEW.RP-TIAA-CONTR LIT-CORRT LIT-CORRU 
    LIT-DEST                                                          *     LIT-PULL STRING-NAME.STRING-NAME-SHORT
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appm603 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField acis_Reprint_Fl_View_Rp_Coll_Code;
    private DbsField acis_Reprint_Fl_View_Rp_Pin_Nbr;
    private DbsField acis_Reprint_Fl_View_Rp_Racf_Id;
    private DbsField acis_Reprint_Fl_View_Rp_Soc_Sec;
    private DbsField acis_Reprint_Fl_View_Rp_Tiaa_Contr;
    private DbsField lit_Corrt;
    private DbsField lit_Corru;
    private DbsField lit_Dest;
    private DbsField lit_Pull;
    private DbsField string_Name_String_Name_Short;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        acis_Reprint_Fl_View_Rp_Coll_Code = parameters.newFieldInRecord("acis_Reprint_Fl_View_Rp_Coll_Code", "ACIS-REPRINT-FL-VIEW.RP-COLL-CODE", FieldType.STRING, 
            6);
        acis_Reprint_Fl_View_Rp_Pin_Nbr = parameters.newFieldInRecord("acis_Reprint_Fl_View_Rp_Pin_Nbr", "ACIS-REPRINT-FL-VIEW.RP-PIN-NBR", FieldType.NUMERIC, 
            12);
        acis_Reprint_Fl_View_Rp_Racf_Id = parameters.newFieldInRecord("acis_Reprint_Fl_View_Rp_Racf_Id", "ACIS-REPRINT-FL-VIEW.RP-RACF-ID", FieldType.STRING, 
            8);
        acis_Reprint_Fl_View_Rp_Soc_Sec = parameters.newFieldInRecord("acis_Reprint_Fl_View_Rp_Soc_Sec", "ACIS-REPRINT-FL-VIEW.RP-SOC-SEC", FieldType.NUMERIC, 
            9);
        acis_Reprint_Fl_View_Rp_Tiaa_Contr = parameters.newFieldInRecord("acis_Reprint_Fl_View_Rp_Tiaa_Contr", "ACIS-REPRINT-FL-VIEW.RP-TIAA-CONTR", FieldType.STRING, 
            10);
        lit_Corrt = parameters.newFieldInRecord("lit_Corrt", "LIT-CORRT", FieldType.STRING, 15);
        lit_Corru = parameters.newFieldInRecord("lit_Corru", "LIT-CORRU", FieldType.STRING, 3);
        lit_Dest = parameters.newFieldInRecord("lit_Dest", "LIT-DEST", FieldType.STRING, 9);
        lit_Pull = parameters.newFieldInRecord("lit_Pull", "LIT-PULL", FieldType.STRING, 10);
        string_Name_String_Name_Short = parameters.newFieldInRecord("string_Name_String_Name_Short", "STRING-NAME.STRING-NAME-SHORT", FieldType.STRING, 
            35);
        parameters.reset();
    }

    public Appm603() throws Exception
    {
        super("Appm603");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=002 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Appm603", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Appm603"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("acis_Reprint_Fl_View_Rp_Pin_Nbr", acis_Reprint_Fl_View_Rp_Pin_Nbr, true, 2, 2, 12, "", true, false, null, "0123456789+-, ", 
                "AD=DZOFHW' '~TG=", ' ');
            uiForm.setUiControl("string_Name_String_Name_Short", string_Name_String_Name_Short, true, 2, 15, 33, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("acis_Reprint_Fl_View_Rp_Tiaa_Contr", acis_Reprint_Fl_View_Rp_Tiaa_Contr, true, 2, 49, 10, "", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("acis_Reprint_Fl_View_Rp_Soc_Sec", acis_Reprint_Fl_View_Rp_Soc_Sec, true, 2, 60, 9, "", true, false, null, "0123456789+-, ", 
                "AD=DZOFHW' '~TG=", ' ');
            uiForm.setUiControl("acis_Reprint_Fl_View_Rp_Coll_Code", acis_Reprint_Fl_View_Rp_Coll_Code, true, 2, 71, 6, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("acis_Reprint_Fl_View_Rp_Racf_Id", acis_Reprint_Fl_View_Rp_Racf_Id, true, 2, 78, 8, "", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("lit_Corrt", lit_Corrt, true, 2, 87, 15, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("lit_Corru", lit_Corru, true, 2, 105, 3, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("lit_Dest", lit_Dest, true, 2, 112, 9, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("lit_Pull", lit_Pull, true, 2, 122, 10, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
