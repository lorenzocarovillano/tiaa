/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:45 PM
**        *   FROM NATURAL MAP   :  Appm605
************************************************************
**        * FILE NAME               : Appm605.java
**        * CLASS NAME              : Appm605
**        * INSTANCE NAME           : Appm605
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     GRNDTOTALCNT.ALLOCTOT GRNDTOTALCNT.BENNYTOT GRNDTOTALCNT.CORTOT                                                          *     GRNDTOTALCNT.GRANDTOT 
    GRNDTOTALCNT.NATOT GRNDTOTALCNT.PPGVTOT                                                            *     GRNDTOTALCNT.SDTOT REPORTLIT1
************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appm605 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField grndtotalcnt_Alloctot;
    private DbsField grndtotalcnt_Bennytot;
    private DbsField grndtotalcnt_Cortot;
    private DbsField grndtotalcnt_Grandtot;
    private DbsField grndtotalcnt_Natot;
    private DbsField grndtotalcnt_Ppgvtot;
    private DbsField grndtotalcnt_Sdtot;
    private DbsField reportlit1;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        grndtotalcnt_Alloctot = parameters.newFieldInRecord("grndtotalcnt_Alloctot", "GRNDTOTALCNT.ALLOCTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Bennytot = parameters.newFieldInRecord("grndtotalcnt_Bennytot", "GRNDTOTALCNT.BENNYTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Cortot = parameters.newFieldInRecord("grndtotalcnt_Cortot", "GRNDTOTALCNT.CORTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Grandtot = parameters.newFieldInRecord("grndtotalcnt_Grandtot", "GRNDTOTALCNT.GRANDTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Natot = parameters.newFieldInRecord("grndtotalcnt_Natot", "GRNDTOTALCNT.NATOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Ppgvtot = parameters.newFieldInRecord("grndtotalcnt_Ppgvtot", "GRNDTOTALCNT.PPGVTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Sdtot = parameters.newFieldInRecord("grndtotalcnt_Sdtot", "GRNDTOTALCNT.SDTOT", FieldType.NUMERIC, 9);
        reportlit1 = parameters.newFieldInRecord("reportlit1", "REPORTLIT1", FieldType.STRING, 35);
        parameters.reset();
    }

    public Appm605() throws Exception
    {
        super("Appm605");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=030 LS=133 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Appm605", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Appm605"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "DATE", "", 1, 2, 4);
            uiForm.setUiControl("astDATU", Global.getDATU(), true, 1, 9, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "ANNUITY CONTRACT ISSUE SYSTEM", "", 1, 53, 29);
            uiForm.setUiLabel("label_3", "PAGE", "", 1, 115, 4);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 1, 124, 5, "", true, true, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "TIME", "", 2, 2, 4);
            uiForm.setUiControl("astTIME", Global.getTIME(), true, 2, 9, 10, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "CORPORATE FILE UPDATE SUMMARY REPORT", "", 2, 49, 36);
            uiForm.setUiLabel("label_6", "PROGRAM", "", 2, 115, 7);
            uiForm.setUiControl("astPROGRAM", Global.getPROGRAM(), true, 2, 124, 8, "", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("reportlit1", reportlit1, true, 3, 59, 35, "", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "CORPORATE FILE UPDATES BY CONTRACT:", "", 6, 32, 35);
            uiForm.setUiLabel("label_8", "------------------------------------------------------------------------------------------------------------------------------", 
                "", 7, 2, 126);
            uiForm.setUiLabel("label_9", "ALLOCATION.....................:", "", 9, 32, 32);
            uiForm.setUiControl("grndtotalcnt_Alloctot", grndtotalcnt_Alloctot, true, 9, 65, 9, "", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_10", "BENEFICIARY MAINTENANCE FILE...:", "", 10, 32, 32);
            uiForm.setUiControl("grndtotalcnt_Bennytot", grndtotalcnt_Bennytot, true, 10, 65, 9, "", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "COR............................:", "", 11, 32, 32);
            uiForm.setUiControl("grndtotalcnt_Cortot", grndtotalcnt_Cortot, true, 11, 65, 9, "", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "ADDRESS........................:", "", 12, 32, 32);
            uiForm.setUiControl("grndtotalcnt_Natot", grndtotalcnt_Natot, true, 12, 65, 9, "", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_13", "PPG/VESTING....................:", "", 13, 32, 32);
            uiForm.setUiControl("grndtotalcnt_Ppgvtot", grndtotalcnt_Ppgvtot, true, 13, 65, 9, "", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "ANNUITY START DATE.............:", "", 14, 32, 32);
            uiForm.setUiControl("grndtotalcnt_Sdtot", grndtotalcnt_Sdtot, true, 14, 65, 9, "", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_15", "-----------", "", 16, 63, 11);
            uiForm.setUiLabel("label_16", "GRAND TOTAL OF CORRECTED CONTRACTS....:", "", 18, 25, 39);
            uiForm.setUiControl("grndtotalcnt_Grandtot", grndtotalcnt_Grandtot, true, 18, 65, 9, "", true, true, null, "0123456789+-, ", "AD=DROFHW' '~TG=", 
                ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
