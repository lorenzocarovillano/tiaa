/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:55 PM
**        * FROM NATURAL PROGRAM : Appb525
************************************************************
**        * FILE NAME            : Appb525.java
**        * CLASS NAME           : Appb525
**        * INSTANCE NAME        : Appb525
************************************************************
************************************************************************
* PROGRAM  : APPB525 - PURGE COMPLETED/DELETED RECORDS FROM RECON FILE *
* FUNCTION : DELETE FROM ACIS-RECONCILE-FL ALL COMPLETED/DELETED RECS. *
*            THAT ARE MORE THAN 6 MONTHS OLD                           *
* UPDATED  : 08/13/09 C.AVE     - NEW PROGRAM                          *
*                                                                      *
* 10/2011 B.HOLLOWAY ADD LOGIC TO PERFORM A GET ON RECORDS SELECTED    *
*            FOR DELETION.  THIS WILL AVOID GETTING AN ADABAS ERROR    *
*            DUE TO EXCEEDING THE ALLOWABLE NUMBER OF RECORDS ON HOLD. *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb525 extends BLNatBase
{
    // Data Areas
    private LdaAppl530 ldaAppl530;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Wk_Control_File;

    private DbsGroup pnd_Wk_Control_File__R_Field_1;
    private DbsField pnd_Wk_Control_File_Pnd_Wk_Cntrl_Dte;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No;

    private DbsGroup pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt;

    private DbsGroup pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_3;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct;
    private DbsField pnd_Ttl_Rec_Deleted;
    private DbsField pnd_Control_Dte;
    private DbsField pnd_Control_Date;

    private DbsGroup pnd_Control_Date__R_Field_4;
    private DbsField pnd_Control_Date_Pnd_Control_Dten;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl530 = new LdaAppl530();
        registerRecord(ldaAppl530);
        registerRecord(ldaAppl530.getVw_acis_Reconcile_Fl_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Wk_Control_File = localVariables.newFieldInRecord("pnd_Wk_Control_File", "#WK-CONTROL-FILE", FieldType.STRING, 50);

        pnd_Wk_Control_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Wk_Control_File__R_Field_1", "REDEFINE", pnd_Wk_Control_File);
        pnd_Wk_Control_File_Pnd_Wk_Cntrl_Dte = pnd_Wk_Control_File__R_Field_1.newFieldInGroup("pnd_Wk_Control_File_Pnd_Wk_Cntrl_Dte", "#WK-CNTRL-DTE", 
            FieldType.STRING, 8);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No = localVariables.newFieldInRecord("pnd_Rc_Cntrl_Type_Cat_Tiaa_No", "#RC-CNTRL-TYPE-CAT-TIAA-NO", FieldType.STRING, 
            20);

        pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2 = localVariables.newGroupInRecord("pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2", "REDEFINE", pnd_Rc_Cntrl_Type_Cat_Tiaa_No);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt", 
            "#RC-CONTROL-DT", FieldType.STRING, 8);

        pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_3 = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2.newGroupInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_3", 
            "REDEFINE", pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_3.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date", 
            "#RC-CONTROL-DATE", FieldType.NUMERIC, 8);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type", 
            "#RC-RECONCILE-TYPE", FieldType.STRING, 1);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category", 
            "#RC-RECONCILE-CATEGORY", FieldType.STRING, 1);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_2.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct", 
            "#RC-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Ttl_Rec_Deleted = localVariables.newFieldInRecord("pnd_Ttl_Rec_Deleted", "#TTL-REC-DELETED", FieldType.PACKED_DECIMAL, 8);
        pnd_Control_Dte = localVariables.newFieldInRecord("pnd_Control_Dte", "#CONTROL-DTE", FieldType.DATE);
        pnd_Control_Date = localVariables.newFieldInRecord("pnd_Control_Date", "#CONTROL-DATE", FieldType.STRING, 8);

        pnd_Control_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Control_Date__R_Field_4", "REDEFINE", pnd_Control_Date);
        pnd_Control_Date_Pnd_Control_Dten = pnd_Control_Date__R_Field_4.newFieldInGroup("pnd_Control_Date_Pnd_Control_Dten", "#CONTROL-DTEN", FieldType.NUMERIC, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl530.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb525() throws Exception
    {
        super("Appb525");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB525", onError);
        pnd_Control_Dte.compute(new ComputeParameters(false, pnd_Control_Dte), Global.getDATX().subtract(180));                                                           //Natural: ASSIGN #CONTROL-DTE := *DATX - 180
        pnd_Control_Date.setValueEdited(pnd_Control_Dte,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED #CONTROL-DTE ( EM = YYYYMMDD ) TO #CONTROL-DATE
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No.reset();                                                                                                                            //Natural: RESET #RC-CNTRL-TYPE-CAT-TIAA-NO
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt.setValue(pnd_Control_Date_Pnd_Control_Dten);                                                                      //Natural: ASSIGN #RC-CONTROL-DT := #CONTROL-DTEN
        ldaAppl530.getVw_acis_Reconcile_Fl_View().startDatabaseRead                                                                                                       //Natural: READ ACIS-RECONCILE-FL-VIEW IN LOGICAL DESCENDING SEQUENCE BY RC-CNTRL-TYPE-CAT-TIAA-NO STARTING FROM #RC-CNTRL-TYPE-CAT-TIAA-NO
        (
        "RD1",
        new Wc[] { new Wc("RC_CNTRL_TYPE_CAT_TIAA_NO", "<=", pnd_Rc_Cntrl_Type_Cat_Tiaa_No, WcType.BY) },
        new Oc[] { new Oc("RC_CNTRL_TYPE_CAT_TIAA_NO", "DESC") }
        );
        RD1:
        while (condition(ldaAppl530.getVw_acis_Reconcile_Fl_View().readNextRow("RD1")))
        {
            //*  NEVER AN EXCEPTION
            if (condition(((ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Control_Dt().less(pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date) && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Dt().equals(getZero()))  //Natural: IF RC-CONTROL-DT < #RC-CONTROL-DATE AND RC-EXCEPTION-DT = 0 AND ( RC-RECONCILE-CATEGORY = 'C' OR = 'D' OR = 'N' )
                && ((ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals("C") || ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals("D")) 
                || ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals("N")))))
            {
                pnd_Ttl_Rec_Deleted.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TTL-REC-DELETED
                //*  10/2011 B.HOLLOWAY
                getWorkFiles().write(1, false, ldaAppl530.getVw_acis_Reconcile_Fl_View());                                                                                //Natural: WRITE WORK FILE 1 ACIS-RECONCILE-FL-VIEW
                GET1:                                                                                                                                                     //Natural: GET ACIS-RECONCILE-FL-VIEW *ISN ( RD1. )
                ldaAppl530.getVw_acis_Reconcile_Fl_View().readByID(ldaAppl530.getVw_acis_Reconcile_Fl_View().getAstISN("RD1"), "GET1");
                //*  10/2011 B.HOLLOWAY - (WAS DELETE(RD1.))
                ldaAppl530.getVw_acis_Reconcile_Fl_View().deleteDBRow("GET1");                                                                                            //Natural: DELETE ( GET1. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "CONTROL DATE        :",pnd_Control_Date,NEWLINE,"NO. OF DELETED RECS.:",pnd_Ttl_Rec_Deleted, new ReportEditMask ("ZZZ,ZZ9"));              //Natural: WRITE 'CONTROL DATE        :' #CONTROL-DATE / 'NO. OF DELETED RECS.:' #TTL-REC-DELETED ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE
            TabSetting(25),Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
