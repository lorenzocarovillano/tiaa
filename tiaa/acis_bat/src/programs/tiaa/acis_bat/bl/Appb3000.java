/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:11 PM
**        * FROM NATURAL PROGRAM : Appb3000
************************************************************
**        * FILE NAME            : Appb3000.java
**        * CLASS NAME           : Appb3000
**        * INSTANCE NAME        : Appb3000
************************************************************
**********************************************************************
* PROGRAM    : APPB3000
* WRITTEN ON : 06/02/99
* PURPOSE    : READ PRAP FILE AND SELECT OVERDUE, RELEASED OR ASSIGNED
*              STATUS , CONVERT ALL DATE FIELDS TO YYYYMMDD FORMAT AND
*              CREATE 2 WORKFILES TO BE PROCESSED FOR PRAP FIX .
*
* 12/07/00 V.RAQUENO - REASSIGN MIT-UNIT 'TMSR E1' TO 'TMSR S1' (VR1)
* 08/01/01 K.GATES   - NEW FIELD ADDED TO PRAP FILE AND WORK FILE
*                      (AP-PRAP-PREM-RSCH-IND)
* 11/13/01 K.GATES   - 457(B) PROJECT CHANGES - SEE 457(B) KG
* 02/10/04 SINGLETON - NEW LAYOUT OF APPL301 FOR SGRD
*          K.GATES   - EXCLUDING ALL SUNGARD CONTRACTS FROM PROCESS
* 12/09/05 D.MARPURI - RE-STOW NEW LAYOUT OF APPL301 FOR SGRD (RL8)
* 07/14/06 K.GATES   - RE-STOW NEW LAYOUT OF APPL301 FOR ARR  (ARR)
* 10/04/06 K.GATES   - DCA/CUNY DEPOSIT FUND CHANGES - DCA KG
* 05/03/07 K.GATES   - RE-STOW NEW LAYOUT OF APPL301 FOR ARR2 (ARR2)
* 09/23/08 DEVELBISS - RE-STOW NEW LAYOUT OF APPL301 FOR AUTO ENROLL
* 08/12/09 C. AVE    - RE-STOW NEW LAYOUT OF APPL301 FOR ACIS PERF
* 05/02/11 C. SCHNEIDER - RE-STOW NEW LAYOUT OF APPL301 (JHU)
* 06/27/11 C. SCHNEIDER - RE-STOW NEW LAYOUT OF APPL301 (TIC)
* 11/17/12 L. SHU    - RE-STOW NEW LAYOUT OF APPL301
*                      LEGAL PACKAGE ANNUITY FUND       (LPOA)
* 07/18/13 L.SHU - RESTOW FOR APPL301 THE NEW IRA
*                  SUBSTITUTION FIELDS
* 09/12/13 B.NEWSOM - RESTOW FOR AP-NON-PROPRIETARY-PKG-IND ADDED TO
*                     ADDED TO APPL301.                      (MTSIN)
* 09/01/15 L. SHU   - RESTOW FOR NEW FIELDS IN APPL301 FOR BENE IN
*                     THE PLAN.                                 (BIP)
* 11/01/16 L. SHU   - RESTOW FOR NEW FIELDS IN APPL301 FOR ONEIRA
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)(RESTOW) PINE
* 08/13/2018 L SHU  - REMOVE ALL REFERENCE TO IIS              (IIS1)
* 11/09/2019 B.NEWSOM CHANGED FOR CONTRACT STRATEGY            (IISG)
**********************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb3000 extends BLNatBase
{
    // Data Areas
    private LdaAppl301 ldaAppl301;
    private LdaAppl3100 ldaAppl3100;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Ap_Written;
    private DbsField pnd_Pp_Written;
    private DbsField pnd_Mmddyyyy;

    private DbsGroup pnd_Mmddyyyy__R_Field_1;
    private DbsField pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm;
    private DbsField pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd;
    private DbsField pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl301 = new LdaAppl301();
        registerRecord(ldaAppl301);
        registerRecord(ldaAppl301.getVw_prap());
        ldaAppl3100 = new LdaAppl3100();
        registerRecord(ldaAppl3100);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.NUMERIC, 7);
        pnd_Ap_Written = localVariables.newFieldInRecord("pnd_Ap_Written", "#AP-WRITTEN", FieldType.NUMERIC, 7);
        pnd_Pp_Written = localVariables.newFieldInRecord("pnd_Pp_Written", "#PP-WRITTEN", FieldType.NUMERIC, 7);
        pnd_Mmddyyyy = localVariables.newFieldInRecord("pnd_Mmddyyyy", "#MMDDYYYY", FieldType.STRING, 8);

        pnd_Mmddyyyy__R_Field_1 = localVariables.newGroupInRecord("pnd_Mmddyyyy__R_Field_1", "REDEFINE", pnd_Mmddyyyy);
        pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm = pnd_Mmddyyyy__R_Field_1.newFieldInGroup("pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm", "#MMDDYYYY-MM", FieldType.NUMERIC, 2);
        pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd = pnd_Mmddyyyy__R_Field_1.newFieldInGroup("pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd", "#MMDDYYYY-DD", FieldType.NUMERIC, 2);
        pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy = pnd_Mmddyyyy__R_Field_1.newFieldInGroup("pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy", "#MMDDYYYY-YYYY", FieldType.NUMERIC, 
            4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl301.initializeValues();
        ldaAppl3100.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb3000() throws Exception
    {
        super("Appb3000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ANNTY-ACTVTY-PRAP
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 HW = OFF PS = 52
        ldaAppl301.getVw_prap().startDatabaseRead                                                                                                                         //Natural: READ PRAP
        (
        "PND_PND_L0600",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        PND_PND_L0600:
        while (condition(ldaAppl301.getVw_prap().readNextRow("PND_PND_L0600")))
        {
            //*    REJECT IF  AP-COLL-CODE EQ 'SGRD'                     /* DCA KG
            if (condition(!((ldaAppl301.getPrap_Ap_Coll_Code().notEquals(" ") && ((((ldaAppl301.getPrap_Ap_Status().equals("B") || ldaAppl301.getPrap_Ap_Status().equals("D"))  //Natural: ACCEPT IF AP-COLL-CODE NE ' ' AND ( AP-STATUS = 'B' OR = 'D' OR = 'G' OR = 'H' OR = 'I' )
                || ldaAppl301.getPrap_Ap_Status().equals("G")) || ldaAppl301.getPrap_Ap_Status().equals("H")) || ldaAppl301.getPrap_Ap_Status().equals("I"))))))
            {
                continue;
            }
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
                                                                                                                                                                          //Natural: PERFORM MOVE-FIELDS
            sub_Move_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0600"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0600"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAppl301.getPrap_Ap_Record_Type().equals(1)))                                                                                                 //Natural: IF AP-RECORD-TYPE = 1
            {
                pnd_Ap_Written.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #AP-WRITTEN
                //*   APPLICATION RECORD
                //*  OIA KG
                //*  OIA KG
                //*  OIA KG
                //*  IISG
                //*  IISG
                //*  IISG
                //*  IISG
                //*  IISG
                getWorkFiles().write(1, false, ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_1(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_2(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_3(),  //Natural: WRITE WORK FILE 1 #AP-DATA-1 #AP-DATA-2 #AP-DATA-3 #AP-DATA-4 #AP-DATA-5 #AP-DATA-6 #AP-DATA-7 #AP-DATA-8 #AP-DATA-9 #AP-DATA-10 #AP-DATA-11 #AP-DATA-12 #AP-DATA-13 #AP-DATA-14 #AP-DATA-15 #AP-DATA-16 #AP-DATA-17 #AP-DATA-18 #AP-ADD-DATA-1 #AP-KEY
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_4(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_5(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_6(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_7(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_8(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_9(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_10(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_11(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_12(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_13(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_14(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_15(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_16(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_17(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_18(), 
                    ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1(), ldaAppl3100.getPnd_Ap_Key());
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pp_Written.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PP-WRITTEN
                //*   PREMIUM RECORD
                //*  OIA KG
                //*  OIA KG
                //*  OIA KG
                //*  IISG
                //*  IISG
                //*  IISG
                //*  IISG
                //*  IISG
                getWorkFiles().write(2, false, ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_1(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_2(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_3(),  //Natural: WRITE WORK FILE 2 #AP-DATA-1 #AP-DATA-2 #AP-DATA-3 #AP-DATA-4 #AP-DATA-5 #AP-DATA-6 #AP-DATA-7 #AP-DATA-8 #AP-DATA-9 #AP-DATA-10 #AP-DATA-11 #AP-DATA-12 #AP-DATA-13 #AP-DATA-14 #AP-DATA-15 #AP-DATA-16 #AP-DATA-17 #AP-DATA-18 #AP-ADD-DATA-1 #AP-KEY
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_4(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_5(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_6(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_7(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_8(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_9(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_10(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_11(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_12(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_13(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_14(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_15(), 
                    ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_16(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_17(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_18(), 
                    ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1(), ldaAppl3100.getPnd_Ap_Key());
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-FIELDS
        //*  #AP-AP-ALLOCATION(2)          :=  AP-ALLOCATION(2)
        //*  #AP-AP-ALLOCATION(3)          :=  AP-ALLOCATION(3)
        //*  #AP-AP-ALLOCATION(4)          :=  AP-ALLOCATION(4)
        //*  #AP-AP-ALLOCATION(5)          :=  AP-ALLOCATION(5)
        //*  #AP-AP-ALLOCATION(6)          :=  AP-ALLOCATION(6)
        //*  #AP-AP-ALLOCATION(7)          :=  AP-ALLOCATION(7)
        //*  #AP-AP-ALLOCATION(8)          :=  AP-ALLOCATION(8)
        //*  #AP-AP-ALLOCATION(9)          :=  AP-ALLOCATION(9)
        //*  #AP-AP-ALLOCATION(10)         :=  AP-ALLOCATION(10)
        //*  #AP-AP-ALLOCATION(11)         :=  AP-ALLOCATION(11)
        //*  #AP-AP-ALLOCATION(12)         :=  AP-ALLOCATION(12)
        //*  #AP-AP-ALLOCATION(13)         :=  AP-ALLOCATION(13)
        //*  #AP-AP-ALLOCATION(14)         :=  AP-ALLOCATION(14)
        //*  #AP-AP-ALLOCATION(15)         :=  AP-ALLOCATION(15)
        //*  #AP-AP-ALLOCATION(16)         :=  AP-ALLOCATION(16)
        //*  #AP-AP-ALLOCATION(17)         :=  AP-ALLOCATION(17)
        //*  #AP-AP-ALLOCATION(18)         :=  AP-ALLOCATION(18)
        //*  #AP-AP-ALLOCATION(19)         :=  AP-ALLOCATION(19)
        //*  #AP-AP-ALLOCATION(20)         :=  AP-ALLOCATION(20)
        //*  #AP-AP-BENE-INFO-TYPE           :=  AP-BENE-INFO-TYPE
        //*  #AP-AP-PRIMARY-STD-ENT          :=  AP-PRIMARY-STD-ENT
        //*  #AP-AP-PRIMARY-BENE-INFO(*)     :=  AP-PRIMARY-BENE-INFO(*)
        //*  #AP-AP-CONTINGENT-STD-ENT       :=  AP-CONTINGENT-STD-ENT
        //*  #AP-AP-CONTINGENT-BENE-INFO(*)  :=  AP-CONTINGENT-BENE-INFO(*)
        //*  #AP-AP-BENE-ESTATE              :=  AP-BENE-ESTATE
        //*  #AP-AP-BENE-TRUST               :=  AP-BENE-TRUST
        //*  #AP-AP-BENE-CATEGORY            :=  AP-BENE-CATEGORY
        getReports().write(1, "********************************************",NEWLINE,"PROGRAM",Global.getPROGRAM(),new ColumnSpacing(5),"DATE",Global.getDATE(),NEWLINE,"PRAP WORKFILE",NEWLINE,"  TOTAL  RECORDS READ      ",pnd_Rec_Read,  //Natural: WRITE ( 1 ) '********************************************' / 'PROGRAM' *PROGRAM 5X 'DATE' *DATE / 'PRAP WORKFILE' / '  TOTAL  RECORDS READ      ' #REC-READ ( EM = Z,ZZZ,ZZ9 ) / '              AP WRITTEN   ' #AP-WRITTEN ( EM = Z,ZZZ,ZZ9 ) / '              PP WRITTEN   ' #PP-WRITTEN ( EM = Z,ZZZ,ZZ9 ) / '********************************************'
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"              AP WRITTEN   ",pnd_Ap_Written, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"              PP WRITTEN   ",pnd_Pp_Written, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"********************************************");
        if (Global.isEscape()) return;
    }
    private void sub_Move_Fields() throws Exception                                                                                                                       //Natural: MOVE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaAppl3100.getPnd_Ap_Work_File().reset();                                                                                                                        //Natural: RESET #AP-WORK-FILE #AP-ADD-WORK-FILE #AP-KEY
        ldaAppl3100.getPnd_Ap_Add_Work_File().reset();
        ldaAppl3100.getPnd_Ap_Key().reset();
        ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Isn().setValue(ldaAppl301.getVw_prap().getAstISN("PND_PND_L0600"));                                                    //Natural: ASSIGN #AP-ISN := *ISN ( ##L0600. )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Coll_Code().setValue(ldaAppl301.getPrap_Ap_Coll_Code());                                                                //Natural: ASSIGN #AP-AP-COLL-CODE := AP-COLL-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_G_Key().setValue(ldaAppl301.getPrap_Ap_G_Key());                                                                        //Natural: ASSIGN #AP-AP-G-KEY := AP-G-KEY
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_G_Ind().setValue(ldaAppl301.getPrap_Ap_G_Ind());                                                                        //Natural: ASSIGN #AP-AP-G-IND := AP-G-IND
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec().setValue(ldaAppl301.getPrap_Ap_Soc_Sec());                                                                    //Natural: ASSIGN #AP-AP-SOC-SEC := AP-SOC-SEC
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Lob().setValue(ldaAppl301.getPrap_Ap_Lob());                                                                            //Natural: ASSIGN #AP-AP-LOB := AP-LOB
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Lob_Type().setValue(ldaAppl301.getPrap_Ap_Lob_Type());                                                                  //Natural: ASSIGN #AP-AP-LOB-TYPE := AP-LOB-TYPE
        //*  457(B) KG
        if (condition((ldaAppl301.getPrap_Ap_Lob().equals("S") && (ldaAppl301.getPrap_Ap_Lob_Type().equals("2") || ldaAppl301.getPrap_Ap_Lob_Type().equals("3")))))       //Natural: IF AP-LOB = 'S' AND ( AP-LOB-TYPE = '2' OR = '3' )
        {
            ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Sort_Lob_Type().setValue("0");                                                                                 //Natural: ASSIGN #AP-ADD-SORT-LOB-TYPE := '0'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Sort_Lob_Type().setValue(ldaAppl301.getPrap_Ap_Lob_Type());                                                    //Natural: ASSIGN #AP-ADD-SORT-LOB-TYPE := AP-LOB-TYPE
            //*  EAC (RODGER)
            //*  EAC (RODGER)
            //*  EAC (RODGER)
            //*  EAC (RODGER)
            //*  OIA KG
            //*  IISG
            //*  OIA JR
            //*  OIA JR
            //*  IISG
            //*  IISG
            //*  IISG
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Bill_Code().setValue(ldaAppl301.getPrap_Ap_Bill_Code());                                                                //Natural: ASSIGN #AP-AP-BILL-CODE := AP-BILL-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_App_Pref().setValue(ldaAppl301.getPrap_Ap_Tiaa_Cntrct().getSubstring(1,1));                                                //Natural: ASSIGN #AP-APP-PREF := SUBSTRING ( AP-TIAA-CNTRCT,1,1 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_App_Cont().setValue(ldaAppl301.getPrap_Ap_Tiaa_Cntrct().getSubstring(2,7));                                                //Natural: ASSIGN #AP-APP-CONT := SUBSTRING ( AP-TIAA-CNTRCT,2,7 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_App_C_Pref().setValue(ldaAppl301.getPrap_Ap_Cref_Cert().getSubstring(1,1));                                                //Natural: ASSIGN #AP-APP-C-PREF := SUBSTRING ( AP-CREF-CERT,1,1 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_App_C_Cont().setValue(ldaAppl301.getPrap_Ap_Cref_Cert().getSubstring(2,7));                                                //Natural: ASSIGN #AP-APP-C-CONT := SUBSTRING ( AP-CREF-CERT,2,7 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Doi().setValue(ldaAppl301.getPrap_Ap_T_Doi());                                                                        //Natural: ASSIGN #AP-AP-T-DOI := AP-T-DOI
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_C_Doi().setValue(ldaAppl301.getPrap_Ap_C_Doi());                                                                        //Natural: ASSIGN #AP-AP-C-DOI := AP-C-DOI
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Curr().setValue(ldaAppl301.getPrap_Ap_Curr());                                                                          //Natural: ASSIGN #AP-AP-CURR := AP-CURR
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Age_1st().setValue(ldaAppl301.getPrap_Ap_T_Age_1st());                                                                //Natural: ASSIGN #AP-AP-T-AGE-1ST := AP-T-AGE-1ST
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_C_Age_1st().setValue(ldaAppl301.getPrap_Ap_C_Age_1st());                                                                //Natural: ASSIGN #AP-AP-C-AGE-1ST := AP-C-AGE-1ST
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Sex().setValue(ldaAppl301.getPrap_Ap_Sex());                                                                            //Natural: ASSIGN #AP-AP-SEX := AP-SEX
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Zip().setValue(ldaAppl301.getPrap_Ap_Mail_Zip());                                                                  //Natural: ASSIGN #AP-AP-MAIL-ZIP := AP-MAIL-ZIP
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Name_Addr_Cd().setValue(ldaAppl301.getPrap_Ap_Name_Addr_Cd());                                                          //Natural: ASSIGN #AP-AP-NAME-ADDR-CD := AP-NAME-ADDR-CD
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Discr().setValue(ldaAppl301.getPrap_Ap_Alloc_Discr());                                                            //Natural: ASSIGN #AP-AP-ALLOC-DISCR := AP-ALLOC-DISCR
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Release_Ind().setValue(ldaAppl301.getPrap_Ap_Release_Ind());                                                            //Natural: ASSIGN #AP-AP-RELEASE-IND := AP-RELEASE-IND
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Type().setValue(ldaAppl301.getPrap_Ap_Type());                                                                          //Natural: ASSIGN #AP-AP-TYPE := AP-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Other_Pols().setValue(ldaAppl301.getPrap_Ap_Other_Pols());                                                              //Natural: ASSIGN #AP-AP-OTHER-POLS := AP-OTHER-POLS
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type().setValue(ldaAppl301.getPrap_Ap_Record_Type());                                                            //Natural: ASSIGN #AP-AP-RECORD-TYPE := AP-RECORD-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Status().setValue(ldaAppl301.getPrap_Ap_Status());                                                                      //Natural: ASSIGN #AP-AP-STATUS := AP-STATUS
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Numb_Dailys().setValue(ldaAppl301.getPrap_Ap_Numb_Dailys());                                                            //Natural: ASSIGN #AP-AP-NUMB-DAILYS := AP-NUMB-DAILYS
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Fmt().setValue(ldaAppl301.getPrap_Ap_Allocation_Fmt());                                                           //Natural: ASSIGN #AP-AP-ALLOC-FMT := AP-ALLOCATION-FMT
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_App_Source().setValue(ldaAppl301.getPrap_Ap_App_Source());                                                              //Natural: ASSIGN #AP-AP-APP-SOURCE := AP-APP-SOURCE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_1().setValue(ldaAppl301.getPrap_Ap_Fund_Source_Cde_1());                                                    //Natural: ASSIGN #AP-AP-FUND-SOURCE-1 := AP-FUND-SOURCE-CDE-1
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Allocation().getValue(1,":",100).setValue(ldaAppl301.getPrap_Ap_Allocation_Pct().getValue(1,":",100));                  //Natural: ASSIGN #AP-AP-ALLOCATION ( 1:100 ) := AP-ALLOCATION-PCT ( 1:100 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident().getValue(1,":",100).setValue(ldaAppl301.getPrap_Ap_Fund_Cde().getValue(1,":",100));                        //Natural: ASSIGN #AP-AP-FUND-IDENT ( 1:100 ) := AP-FUND-CDE ( 1:100 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Source_2().setValue(ldaAppl301.getPrap_Ap_Fund_Source_Cde_2());                                                    //Natural: ASSIGN #AP-AP-FUND-SOURCE-2 := AP-FUND-SOURCE-CDE-2
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Allocation_2().getValue(1,":",100).setValue(ldaAppl301.getPrap_Ap_Allocation_Pct_2().getValue(1,":",                    //Natural: ASSIGN #AP-AP-ALLOCATION-2 ( 1:100 ) := AP-ALLOCATION-PCT-2 ( 1:100 )
            100));
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Fund_Ident_2().getValue(1,":",100).setValue(ldaAppl301.getPrap_Ap_Fund_Cde_2().getValue(1,":",100));                    //Natural: ASSIGN #AP-AP-FUND-IDENT-2 ( 1:100 ) := AP-FUND-CDE-2 ( 1:100 )
        ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code().setValue(ldaAppl301.getPrap_Ap_Region_Code());                                                       //Natural: ASSIGN #AP-ADD-REGION-CODE := AP-REGION-CODE
        if (condition(ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_1_1().equals("W")))                                                                      //Natural: IF #AP-ADD-REGION-CODE-1-1 = 'W'
        {
            ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Region_Code_1_1().setValue("2");                                                                               //Natural: MOVE '2' TO #AP-ADD-REGION-CODE-1-1
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Region_Code().setValue(ldaAppl301.getPrap_Ap_Region_Code());                                                            //Natural: ASSIGN #AP-AP-REGION-CODE := AP-REGION-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Orig_Issue_State().setValue(ldaAppl301.getPrap_Ap_Orig_Issue_State());                                                  //Natural: ASSIGN #AP-AP-ORIG-ISSUE-STATE := AP-ORIG-ISSUE-STATE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Current_State_Code().setValue(ldaAppl301.getPrap_Ap_Current_State_Code());                                              //Natural: ASSIGN #AP-AP-CURRENT-STATE-CODE := AP-CURRENT-STATE-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Ownership_Type().setValue(ldaAppl301.getPrap_Ap_Ownership_Type());                                                      //Natural: ASSIGN #AP-AP-OWNERSHIP-TYPE := AP-OWNERSHIP-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Lob_Type().setValue(ldaAppl301.getPrap_Ap_Lob_Type());                                                                  //Natural: ASSIGN #AP-AP-LOB-TYPE := AP-LOB-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Split_Code().setValue(ldaAppl301.getPrap_Ap_Split_Code());                                                              //Natural: ASSIGN #AP-AP-SPLIT-CODE := AP-SPLIT-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Transaction_Cd().setValue(ldaAppl301.getPrap_Ap_Dana_Transaction_Cd());                                            //Natural: ASSIGN #AP-AP-DANA-TRANSACTION-CD := AP-DANA-TRANSACTION-CD
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Rtn_Cd().setValue(ldaAppl301.getPrap_Ap_Dana_Stndrd_Rtn_Cd());                                              //Natural: ASSIGN #AP-AP-DANA-STNDRD-RTN-CD := AP-DANA-STNDRD-RTN-CD
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Finalist_Reason_Cd().setValue(ldaAppl301.getPrap_Ap_Dana_Finalist_Reason_Cd());                                    //Natural: ASSIGN #AP-AP-DANA-FINALIST-REASON-CD := AP-DANA-FINALIST-REASON-CD
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Stndrd_Code().setValue(ldaAppl301.getPrap_Ap_Dana_Addr_Stndrd_Code());                                        //Natural: ASSIGN #AP-AP-DANA-ADDR-STNDRD-CODE := AP-DANA-ADDR-STNDRD-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Stndrd_Overide().setValue(ldaAppl301.getPrap_Ap_Dana_Stndrd_Overide());                                            //Natural: ASSIGN #AP-AP-DANA-STNDRD-OVERIDE := AP-DANA-STNDRD-OVERIDE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Postal_Data_Fields().setValue(ldaAppl301.getPrap_Ap_Dana_Postal_Data_Fields());                                    //Natural: ASSIGN #AP-AP-DANA-POSTAL-DATA-FIELDS := AP-DANA-POSTAL-DATA-FIELDS
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dana_Addr_Geographic_Code().setValue(ldaAppl301.getPrap_Ap_Dana_Addr_Geographic_Code());                                //Natural: ASSIGN #AP-AP-DANA-ADDR-GEOGRAPHIC-CODE := AP-DANA-ADDR-GEOGRAPHIC-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Annuity_Start_Date().setValue(ldaAppl301.getPrap_Ap_Annuity_Start_Date());                                              //Natural: ASSIGN #AP-AP-ANNUITY-START-DATE := AP-ANNUITY-START-DATE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct().getValue("*").setValue(ldaAppl301.getPrap_Ap_Max_Alloc_Pct().getValue("*"));                            //Natural: ASSIGN #AP-AP-MAX-ALLOC-PCT ( * ) := AP-MAX-ALLOC-PCT ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Max_Alloc_Pct_Sign().getValue("*").setValue(ldaAppl301.getPrap_Ap_Max_Alloc_Pct_Sign().getValue("*"));                  //Natural: ASSIGN #AP-AP-MAX-ALLOC-PCT-SIGN ( * ) := AP-MAX-ALLOC-PCT-SIGN ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mail_Instructions().setValue(ldaAppl301.getPrap_Ap_Mail_Instructions());                                                //Natural: ASSIGN #AP-AP-MAIL-INSTRUCTIONS := AP-MAIL-INSTRUCTIONS
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Eop_Addl_Cref_Request().setValue(ldaAppl301.getPrap_Ap_Eop_Addl_Cref_Request());                                        //Natural: ASSIGN #AP-AP-EOP-ADDL-CREF-REQUEST := AP-EOP-ADDL-CREF-REQUEST
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Process_Status().setValue(ldaAppl301.getPrap_Ap_Process_Status());                                                      //Natural: ASSIGN #AP-AP-PROCESS-STATUS := AP-PROCESS-STATUS
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Coll_St_Cd().setValue(ldaAppl301.getPrap_Ap_Coll_St_Cd());                                                              //Natural: ASSIGN #AP-AP-COLL-ST-CD := AP-COLL-ST-CD
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Csm_Sec_Seg().setValue(ldaAppl301.getPrap_Ap_Csm_Sec_Seg());                                                            //Natural: ASSIGN #AP-AP-CSM-SEC-SEG := AP-CSM-SEC-SEG
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_College().setValue(ldaAppl301.getPrap_Ap_Rlc_College());                                                            //Natural: ASSIGN #AP-AP-RLC-COLLEGE := AP-RLC-COLLEGE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Pref().setValue(ldaAppl301.getPrap_Ap_Rlc_Cref_Cert().getSubstring(1,1));                                      //Natural: ASSIGN #AP-AP-RLC-CREF-PREF := SUBSTRING ( AP-RLC-CREF-CERT,1,1 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Rlc_Cref_Cont().setValue(ldaAppl301.getPrap_Ap_Rlc_Cref_Cert().getSubstring(2,7));                                      //Natural: ASSIGN #AP-AP-RLC-CREF-CONT := SUBSTRING ( AP-RLC-CREF-CERT,2,7 )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Prfx_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Prfx_Nme());                                                          //Natural: ASSIGN #AP-AP-COR-PRFX-NME := AP-COR-PRFX-NME
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Last_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Last_Nme());                                                          //Natural: ASSIGN #AP-AP-COR-LAST-NME := AP-COR-LAST-NME
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_First_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_First_Nme());                                                        //Natural: ASSIGN #AP-AP-COR-FIRST-NME := AP-COR-FIRST-NME
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Mddle_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Mddle_Nme());                                                        //Natural: ASSIGN #AP-AP-COR-MDDLE-NME := AP-COR-MDDLE-NME
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Sffx_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Sffx_Nme());                                                          //Natural: ASSIGN #AP-AP-COR-SFFX-NME := AP-COR-SFFX-NME
        if (condition(ldaAppl301.getPrap_Ap_Dob().greater(getZero())))                                                                                                    //Natural: IF AP-DOB > 0
        {
            pnd_Mmddyyyy.setValueEdited(ldaAppl301.getPrap_Ap_Dob(),new ReportEditMask("99999999"));                                                                      //Natural: MOVE EDITED AP-DOB ( EM = 99999999 ) TO #MMDDYYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Yyyy().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy);                                                                //Natural: ASSIGN #AP-AP-DOB-YYYY := #MMDDYYYY-YYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Mm().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm);                                                                    //Natural: ASSIGN #AP-AP-DOB-MM := #MMDDYYYY-MM
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dob_Dd().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd);                                                                    //Natural: ASSIGN #AP-AP-DOB-DD := #MMDDYYYY-DD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl301.getPrap_Ap_Dt_Ent_Sys().greater(getZero())))                                                                                             //Natural: IF AP-DT-ENT-SYS > 0
        {
            pnd_Mmddyyyy.setValueEdited(ldaAppl301.getPrap_Ap_Dt_Ent_Sys(),new ReportEditMask("99999999"));                                                               //Natural: MOVE EDITED AP-DT-ENT-SYS ( EM = 99999999 ) TO #MMDDYYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Yyyy().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy);                                                         //Natural: ASSIGN #AP-AP-DT-ENT-SYS-YYYY := #MMDDYYYY-YYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Mm().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm);                                                             //Natural: ASSIGN #AP-AP-DT-ENT-SYS-MM := #MMDDYYYY-MM
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Ent_Sys_Dd().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd);                                                             //Natural: ASSIGN #AP-AP-DT-ENT-SYS-DD := #MMDDYYYY-DD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl301.getPrap_Ap_Dt_Released().greater(getZero())))                                                                                            //Natural: IF AP-DT-RELEASED > 0
        {
            pnd_Mmddyyyy.setValueEdited(ldaAppl301.getPrap_Ap_Dt_Released(),new ReportEditMask("99999999"));                                                              //Natural: MOVE EDITED AP-DT-RELEASED ( EM = 99999999 ) TO #MMDDYYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Yyyy().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy);                                                        //Natural: ASSIGN #AP-AP-DT-RELEASED-YYYY := #MMDDYYYY-YYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Mm().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm);                                                            //Natural: ASSIGN #AP-AP-DT-RELEASED-MM := #MMDDYYYY-MM
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Released_Dd().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd);                                                            //Natural: ASSIGN #AP-AP-DT-RELEASED-DD := #MMDDYYYY-DD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl301.getPrap_Ap_Dt_Matched().greater(getZero())))                                                                                             //Natural: IF AP-DT-MATCHED > 0
        {
            pnd_Mmddyyyy.setValueEdited(ldaAppl301.getPrap_Ap_Dt_Matched(),new ReportEditMask("99999999"));                                                               //Natural: MOVE EDITED AP-DT-MATCHED ( EM = 99999999 ) TO #MMDDYYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Yyyy().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy);                                                         //Natural: ASSIGN #AP-AP-DT-MATCHED-YYYY := #MMDDYYYY-YYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Mm().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm);                                                             //Natural: ASSIGN #AP-AP-DT-MATCHED-MM := #MMDDYYYY-MM
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Matched_Dd().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd);                                                             //Natural: ASSIGN #AP-AP-DT-MATCHED-DD := #MMDDYYYY-DD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl301.getPrap_Ap_Dt_Deleted().greater(getZero())))                                                                                             //Natural: IF AP-DT-DELETED > 0
        {
            pnd_Mmddyyyy.setValueEdited(ldaAppl301.getPrap_Ap_Dt_Deleted(),new ReportEditMask("99999999"));                                                               //Natural: MOVE EDITED AP-DT-DELETED ( EM = 99999999 ) TO #MMDDYYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Yyyy().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy);                                                         //Natural: ASSIGN #AP-AP-DT-DELETED-YYYY := #MMDDYYYY-YYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Mm().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm);                                                             //Natural: ASSIGN #AP-AP-DT-DELETED-MM := #MMDDYYYY-MM
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Deleted_Dd().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd);                                                             //Natural: ASSIGN #AP-AP-DT-DELETED-DD := #MMDDYYYY-DD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl301.getPrap_Ap_Dt_Withdrawn().greater(getZero())))                                                                                           //Natural: IF AP-DT-WITHDRAWN > 0
        {
            pnd_Mmddyyyy.setValueEdited(ldaAppl301.getPrap_Ap_Dt_Withdrawn(),new ReportEditMask("99999999"));                                                             //Natural: MOVE EDITED AP-DT-WITHDRAWN ( EM = 99999999 ) TO #MMDDYYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Yyyy().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy);                                                       //Natural: ASSIGN #AP-AP-DT-WITHDRAWN-YYYY := #MMDDYYYY-YYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Mm().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm);                                                           //Natural: ASSIGN #AP-AP-DT-WITHDRAWN-MM := #MMDDYYYY-MM
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_Withdrawn_Dd().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd);                                                           //Natural: ASSIGN #AP-AP-DT-WITHDRAWN-DD := #MMDDYYYY-DD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl301.getPrap_Ap_Dt_App_Recvd().greater(getZero())))                                                                                           //Natural: IF AP-DT-APP-RECVD > 0
        {
            pnd_Mmddyyyy.setValueEdited(ldaAppl301.getPrap_Ap_Dt_App_Recvd(),new ReportEditMask("99999999"));                                                             //Natural: MOVE EDITED AP-DT-APP-RECVD ( EM = 99999999 ) TO #MMDDYYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Yyyy().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Yyyy);                                                       //Natural: ASSIGN #AP-AP-DT-APP-RECVD-YYYY := #MMDDYYYY-YYYY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Mm().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Mm);                                                           //Natural: ASSIGN #AP-AP-DT-APP-RECVD-MM := #MMDDYYYY-MM
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd_Dd().setValue(pnd_Mmddyyyy_Pnd_Mmddyyyy_Dd);                                                           //Natural: ASSIGN #AP-AP-DT-APP-RECVD-DD := #MMDDYYYY-DD
            //*  KG 8/01
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Address_Line().getValue("*").setValue(ldaAppl301.getPrap_Ap_Address_Line().getValue("*"));                              //Natural: ASSIGN #AP-AP-ADDRESS-LINE ( * ) := AP-ADDRESS-LINE ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Pin_Nbr().setValue(ldaAppl301.getPrap_Ap_Pin_Nbr());                                                                    //Natural: ASSIGN #AP-AP-PIN-NBR := AP-PIN-NBR
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Ownership().setValue(ldaAppl301.getPrap_Ap_Ownership());                                                                //Natural: ASSIGN #AP-AP-OWNERSHIP := AP-OWNERSHIP
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Ph_Hist_Ind().setValue(ldaAppl301.getPrap_Ap_Ph_Hist_Ind());                                                            //Natural: ASSIGN #AP-AP-PH-HIST-IND := AP-PH-HIST-IND
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Rcrd_Updt_Tm_Stamp().setValue(ldaAppl301.getPrap_Ap_Rcrd_Updt_Tm_Stamp());                                              //Natural: ASSIGN #AP-AP-RCRD-UPDT-TM-STAMP := AP-RCRD-UPDT-TM-STAMP
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Contact_Mode().setValue(ldaAppl301.getPrap_Ap_Contact_Mode());                                                          //Natural: ASSIGN #AP-AP-CONTACT-MODE := AP-CONTACT-MODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Racf_Id().setValue(ldaAppl301.getPrap_Ap_Racf_Id());                                                                    //Natural: ASSIGN #AP-AP-RACF-ID := AP-RACF-ID
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Rqst_Log_Dte_Time().getValue("*").setValue(ldaAppl301.getPrap_Ap_Rqst_Log_Dte_Time().getValue("*"));                    //Natural: ASSIGN #AP-AP-RQST-LOG-DTE-TIME ( * ) := AP-RQST-LOG-DTE-TIME ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Sync_Ind().setValue(ldaAppl301.getPrap_Ap_Sync_Ind());                                                                  //Natural: ASSIGN #AP-AP-SYNC-IND := AP-SYNC-IND
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cor_Ind().setValue(ldaAppl301.getPrap_Ap_Cor_Ind());                                                                    //Natural: ASSIGN #AP-AP-COR-IND := AP-COR-IND
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Alloc_Ind().setValue(ldaAppl301.getPrap_Ap_Alloc_Ind());                                                                //Natural: ASSIGN #AP-AP-ALLOC-IND := AP-ALLOC-IND
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_City().setValue(ldaAppl301.getPrap_Ap_City());                                                                          //Natural: ASSIGN #AP-AP-CITY := AP-CITY
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Doi().setValue(ldaAppl301.getPrap_Ap_Tiaa_Doi());                                                                  //Natural: ASSIGN #AP-AP-TIAA-DOI := AP-TIAA-DOI
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cref_Doi().setValue(ldaAppl301.getPrap_Ap_Cref_Doi());                                                                  //Natural: ASSIGN #AP-AP-CREF-DOI := AP-CREF-DOI
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Unit().getValue("*").setValue(ldaAppl301.getPrap_Ap_Mit_Unit().getValue("*"));                                      //Natural: ASSIGN #AP-AP-MIT-UNIT ( * ) := AP-MIT-UNIT ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mit_Wpid().getValue("*").setValue(ldaAppl301.getPrap_Ap_Mit_Wpid().getValue("*"));                                      //Natural: ASSIGN #AP-AP-MIT-WPID ( * ) := AP-MIT-WPID ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Rollover_Type().setValue(ldaAppl301.getPrap_Ap_Ira_Rollover_Type());                                                //Natural: ASSIGN #AP-AP-IRA-ROLLOVER-TYPE := AP-IRA-ROLLOVER-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Ira_Record_Type().setValue(ldaAppl301.getPrap_Ap_Ira_Record_Type());                                                    //Natural: ASSIGN #AP-AP-IRA-RECORD-TYPE := AP-IRA-RECORD-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Status().setValue(ldaAppl301.getPrap_Ap_Mult_App_Status());                                                    //Natural: ASSIGN #AP-AP-MULT-APP-STATUS := AP-MULT-APP-STATUS
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob().setValue(ldaAppl301.getPrap_Ap_Mult_App_Lob());                                                          //Natural: ASSIGN #AP-AP-MULT-APP-LOB := AP-MULT-APP-LOB
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Lob_Type().setValue(ldaAppl301.getPrap_Ap_Mult_App_Lob_Type());                                                //Natural: ASSIGN #AP-AP-MULT-APP-LOB-TYPE := AP-MULT-APP-LOB-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Mult_App_Ppg().setValue(ldaAppl301.getPrap_Ap_Mult_App_Ppg());                                                          //Natural: ASSIGN #AP-AP-MULT-APP-PPG := AP-MULT-APP-PPG
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Print_Date().setValue(ldaAppl301.getPrap_Ap_Print_Date());                                                              //Natural: ASSIGN #AP-AP-PRINT-DATE := AP-PRINT-DATE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Type().getValue("*").setValue(ldaAppl301.getPrap_Ap_Cntrct_Type().getValue("*"));                                //Natural: ASSIGN #AP-AP-CNTRCT-TYPE ( * ) := AP-CNTRCT-TYPE ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Nbr().getValue("*").setValue(ldaAppl301.getPrap_Ap_Cntrct_Nbr().getValue("*"));                                  //Natural: ASSIGN #AP-AP-CNTRCT-NBR ( * ) := AP-CNTRCT-NBR ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Cntrct_Proceeds_Amt().getValue("*").setValue(ldaAppl301.getPrap_Ap_Cntrct_Proceeds_Amt().getValue("*"));                //Natural: ASSIGN #AP-AP-CNTRCT-PROCEEDS-AMT ( * ) := AP-CNTRCT-PROCEEDS-AMT ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Address_Txt().getValue("*").setValue(ldaAppl301.getPrap_Ap_Address_Txt().getValue("*"));                                //Natural: ASSIGN #AP-AP-ADDRESS-TXT ( * ) := AP-ADDRESS-TXT ( * )
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Address_Dest_Name().setValue(ldaAppl301.getPrap_Ap_Address_Dest_Name());                                                //Natural: ASSIGN #AP-AP-ADDRESS-DEST-NAME := AP-ADDRESS-DEST-NAME
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Zip_Code().setValue(ldaAppl301.getPrap_Ap_Zip_Code());                                                                  //Natural: ASSIGN #AP-AP-ZIP-CODE := AP-ZIP-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Pymnt_Acct_Nmbr().setValue(ldaAppl301.getPrap_Ap_Bank_Pymnt_Acct_Nmbr());                                          //Natural: ASSIGN #AP-AP-BANK-PYMNT-ACCT-NMBR := AP-BANK-PYMNT-ACCT-NMBR
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Bank_Aba_Acct_Nmbr().setValue(ldaAppl301.getPrap_Ap_Bank_Aba_Acct_Nmbr());                                              //Natural: ASSIGN #AP-AP-BANK-ABA-ACCT-NMBR := AP-BANK-ABA-ACCT-NMBR
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Usage_Code().setValue(ldaAppl301.getPrap_Ap_Addr_Usage_Code());                                                    //Natural: ASSIGN #AP-AP-ADDR-USAGE-CODE := AP-ADDR-USAGE-CODE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Amt().setValue(ldaAppl301.getPrap_Ap_Init_Paymt_Amt());                                                      //Natural: ASSIGN #AP-AP-INIT-PAYMT-AMT := AP-INIT-PAYMT-AMT
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Init_Paymt_Pct().setValue(ldaAppl301.getPrap_Ap_Init_Paymt_Pct());                                                      //Natural: ASSIGN #AP-AP-INIT-PAYMT-PCT := AP-INIT-PAYMT-PCT
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_1().setValue(ldaAppl301.getPrap_Ap_Financial_1());                                                            //Natural: ASSIGN #AP-AP-FINANCIAL-1 := AP-FINANCIAL-1
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_2().setValue(ldaAppl301.getPrap_Ap_Financial_2());                                                            //Natural: ASSIGN #AP-AP-FINANCIAL-2 := AP-FINANCIAL-2
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_3().setValue(ldaAppl301.getPrap_Ap_Financial_3());                                                            //Natural: ASSIGN #AP-AP-FINANCIAL-3 := AP-FINANCIAL-3
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_4().setValue(ldaAppl301.getPrap_Ap_Financial_4());                                                            //Natural: ASSIGN #AP-AP-FINANCIAL-4 := AP-FINANCIAL-4
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Financial_5().setValue(ldaAppl301.getPrap_Ap_Financial_5());                                                            //Natural: ASSIGN #AP-AP-FINANCIAL-5 := AP-FINANCIAL-5
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Grp_Cde().setValue(ldaAppl301.getPrap_Ap_Irc_Sectn_Grp_Cde());                                                //Natural: ASSIGN #AP-AP-IRC-SECTN-GRP-CDE := AP-IRC-SECTN-GRP-CDE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Irc_Sectn_Cde().setValue(ldaAppl301.getPrap_Ap_Irc_Sectn_Cde());                                                        //Natural: ASSIGN #AP-AP-IRC-SECTN-CDE := AP-IRC-SECTN-CDE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Inst_Link_Cde().setValue(ldaAppl301.getPrap_Ap_Inst_Link_Cde());                                                        //Natural: ASSIGN #AP-AP-INST-LINK-CDE := AP-INST-LINK-CDE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Applcnt_Req_Type().setValue(ldaAppl301.getPrap_Ap_Applcnt_Req_Type());                                                  //Natural: ASSIGN #AP-AP-APPLCNT-REQ-TYPE := AP-APPLCNT-REQ-TYPE
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Addr_Sync_Ind().setValue(ldaAppl301.getPrap_Ap_Addr_Sync_Ind());                                                        //Natural: ASSIGN #AP-AP-ADDR-SYNC-IND := AP-ADDR-SYNC-IND
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Tiaa_Service_Agent().setValue(ldaAppl301.getPrap_Ap_Tiaa_Service_Agent());                                              //Natural: ASSIGN #AP-AP-TIAA-SERVICE-AGENT := AP-TIAA-SERVICE-AGENT
        ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Prap_Rsch_Mit_Ind().setValue(ldaAppl301.getPrap_Ap_Prap_Rsch_Mit_Ind());                                                //Natural: ASSIGN #AP-AP-PRAP-RSCH-MIT-IND := AP-PRAP-RSCH-MIT-IND
        ldaAppl3100.getPnd_Ap_Key_Pnd_Ap_Ap_Prap_Prem_Rsch_Ind().setValue(ldaAppl301.getPrap_Ap_Prap_Prem_Rsch_Ind());                                                    //Natural: ASSIGN #AP-AP-PRAP-PREM-RSCH-IND := AP-PRAP-PREM-RSCH-IND
        //*  IF  AP-PPG-TEAM-CDE  =  ' '
        //*     READ (1) IIS-PREM-PYMNT-GRP WITH PPG-CDE = #AP-AP-COLL-CODE
        //*          IF  PPG-CDE  =  #AP-AP-COLL-CODE
        //*              #AP-AP-PPG-TEAM-CDE  :=  PREMIUM-TEAM-CDE
        //*          END-IF
        //*     END-READ
        //*     IF  #AP-AP-PPG-TEAM-CDE  =  ' '
        //*         #AP-AP-PPG-TEAM-CDE  :=  AP-PPG-TEAM-CDE
        //*     END-IF
        //*  ELSE
        //*     #AP-AP-PPG-TEAM-CDE  :=  AP-PPG-TEAM-CDE
        //*  END-IF
        //*                                                           /* BEGIN VR1
        //*   CHANGES TO MIT-UNIT OVERRIDE SHOULD ALSO BE MADE TO APPB3310
        //*  FOR #I = 1  5
        //*     IF  AP-MIT-UNIT(#I)   =  'TMSR E1'  OR
        //*                           =  'TMSR E2'
        //*         READ (1) IIS-PREM-PYMNT-GRP WITH PPG-CDE = #AP-AP-COLL-CODE
        //*              IF  PPG-CDE  =  #AP-AP-COLL-CODE
        //*                  #AP-AP-MIT-UNIT(#I)  :=  PREMIUM-TEAM-CDE
        //*              END-IF
        //*         END-READ
        //*     END-IF
        //*  END-FOR
        //*                                                           /* END VR1
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 HW=OFF PS=52");
    }
}
