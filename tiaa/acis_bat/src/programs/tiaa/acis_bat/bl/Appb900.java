/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:10:06 PM
**        * FROM NATURAL PROGRAM : Appb900
************************************************************
**        * FILE NAME            : Appb900.java
**        * CLASS NAME           : Appb900
**        * INSTANCE NAME        : Appb900
************************************************************
************************************************************************
* PROGRAM  : APPB900                                                   *
* SYSTEM   : ACIS                                                      *
* TITLE    : ENROLLMENT TOTALS PER MONTH                               *
* FUNCTION : COUNT ENROLLMENTS RECEIVE ON PAPER AND ONLINE.            *
*            APP-TABLE-ENTRY FILE WILL BE USED TO TRACK THE LAST       *
*            MONTH THAT THIS REPORT WAS RUN. A NEW ENTRY WILL BE       *
*            CREATED WITH THE FOLLOWING VAUES:                         *
*              ENTRY-TABLE-ID-NBR = 000900                             *
*              ENTRY-TABLE-SUB-ID = 'DT'                               *
*              ENTRY-CDE = 1ST DAY OF THE LAST REPORTING MONTH         *
* MOD DATE  MOD BY    DESCRIPTION OF CHANGES                           *
* --------  --------  -----------------------------------------------  *
* 08/21/09  DEVELBISS REWRITE TO COUNT BY ENROLLMENT METHOD AT THE     *
*                     PLAN LEVEL.                                      *
* 08/21/12  ELLO      ADDED NEW COUNTS FOR THE NEW ENROLLMENT METHOD   *
*                     'T' - TP NCC ENROLLMENTS VIA TELEPHONE.  THE     *
*                     COUNT FOR COLUMN 'T' REPRESENTS STANDARD         *
*                     CONTRACTS (TIAA NO. AND CREF NO.).  THE COLUMN   *
*                     'TT' REPRESENT COMPANION CONTRACTS.              *
* 03/17/17  B.NEWSOM  ADDED NEW COUNTS FOR ENROLLMENT METHOD 'M' -     *
*                     EASE ENROLLMENT.                   (EASEENR)     *
* 06/20/17  MITRAPU   PIN EXPANSION CHANGES.(C425939)         PINE.    *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb900 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_reprint;
    private DbsField reprint_Rp_Tiaa_Contr;
    private DbsField reprint_Rp_Sgrd_Plan_No;
    private DbsField reprint_Rp_Sgrd_Subplan_No;

    private DbsGroup reprint__R_Field_1;
    private DbsField reprint_Rp_Sgrd_Subplan_No_3;
    private DbsField reprint_Rp_T_Doi;
    private DbsField reprint_Rp_C_Doi;
    private DbsField reprint_Rp_Soc_Sec;
    private DbsField reprint_Rp_Extract_Date;

    private DbsGroup reprint__R_Field_2;
    private DbsField reprint_Rp_Extract_Yyyymm;
    private DbsField reprint_Rp_Extract_Dd;
    private DbsField reprint_Rp_Racf_Id;
    private DbsField reprint_Rp_Cor_First_Nme;
    private DbsField reprint_Rp_Cor_Last_Nme;
    private DbsField reprint_Rp_Package_Mail_Type;
    private DbsField reprint_Rp_Text_Udf_1;

    private DbsGroup reprint__R_Field_3;
    private DbsField reprint_Rp_Single_Issue_Ind;
    private DbsField reprint_Rp_Sgrd_Divsub;
    private DbsField reprint_Rp_Spec_Fund_Ind;
    private DbsField reprint_Rp_Plan_Issue_State;
    private DbsField reprint_Rp_Dt_Released;
    private DbsField reprint_Rp_Applcnt_Req_Type;
    private DbsField reprint_Rp_Coll_Code;
    private DbsField reprint_Rp_Lob;
    private DbsField reprint_Rp_Lob_Type;

    private DataAccessProgramView vw_table;
    private DbsField table_Entry_Actve_Ind;
    private DbsField table_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Table_Sub_Id;
    private DbsField table_Entry_Cde;

    private DbsGroup table__R_Field_4;
    private DbsField table_Last_Month;

    private DbsGroup table__R_Field_5;
    private DbsField table_Last_Month_Yyyy;
    private DbsField table_Last_Month_Mm;
    private DbsField table_Last_Month_Dd;

    private DbsGroup table__R_Field_6;
    private DbsField table_Plan;
    private DbsField table_Enr_Method;
    private DbsField table_Entry_Dscrptn_Txt;
    private DbsField table_Entry_User_Id;
    private DbsField table_State_Zip_From;

    private DbsGroup table__R_Field_7;
    private DbsField table_Enr_Count;

    private DataAccessProgramView vw_update_Table;
    private DbsField update_Table_Entry_Actve_Ind;
    private DbsField update_Table_Entry_Table_Id_Nbr;
    private DbsField update_Table_Entry_Table_Sub_Id;
    private DbsField update_Table_Entry_Cde;
    private DbsField update_Table_State_Zip_From;

    private DbsGroup update_Table__R_Field_8;
    private DbsField update_Table_Enr_Count;
    private DbsField pnd_Date;
    private DbsField pnd_Plan_Head;
    private DbsField pnd_A_Head;
    private DbsField pnd_Aa_Head;
    private DbsField pnd_B_Head;
    private DbsField pnd_Bb_Head;
    private DbsField pnd_C_Head;
    private DbsField pnd_Cc_Head;
    private DbsField pnd_E_Head;
    private DbsField pnd_Ee_Head;
    private DbsField pnd_F_Head;
    private DbsField pnd_G_Head;
    private DbsField pnd_H_Head;
    private DbsField pnd_I_Head;
    private DbsField pnd_Ii_Head;
    private DbsField pnd_J_Head;
    private DbsField pnd_L_Head;
    private DbsField pnd_N_Head;
    private DbsField pnd_Nn_Head;
    private DbsField pnd_P_Head;
    private DbsField pnd_Pp_Head;
    private DbsField pnd_R_Head;
    private DbsField pnd_Rr_Head;
    private DbsField pnd_U_Head;
    private DbsField pnd_Uu_Head;
    private DbsField pnd_X_Head;
    private DbsField pnd_Xx_Head;
    private DbsField pnd_T_Head;
    private DbsField pnd_Tt_Head;
    private DbsField pnd_M_Head;
    private DbsField pnd_Mm_Head;
    private DbsField pnd_Rpt_Date;
    private DbsField pnd_Save_Plan;
    private DbsField pnd_A_Rpt;
    private DbsField pnd_Aa_Rpt;
    private DbsField pnd_B_Rpt;
    private DbsField pnd_Bb_Rpt;
    private DbsField pnd_C_Rpt;
    private DbsField pnd_Cc_Rpt;
    private DbsField pnd_E_Rpt;
    private DbsField pnd_Ee_Rpt;
    private DbsField pnd_F_Rpt;
    private DbsField pnd_G_Rpt;
    private DbsField pnd_H_Rpt;
    private DbsField pnd_I_Rpt;
    private DbsField pnd_Ii_Rpt;
    private DbsField pnd_J_Rpt;
    private DbsField pnd_L_Rpt;
    private DbsField pnd_N_Rpt;
    private DbsField pnd_Nn_Rpt;
    private DbsField pnd_P_Rpt;
    private DbsField pnd_Pp_Rpt;
    private DbsField pnd_R_Rpt;
    private DbsField pnd_Rr_Rpt;
    private DbsField pnd_U_Rpt;
    private DbsField pnd_Uu_Rpt;
    private DbsField pnd_X_Rpt;
    private DbsField pnd_Xx_Rpt;
    private DbsField pnd_T_Rpt;
    private DbsField pnd_Tt_Rpt;
    private DbsField pnd_M_Rpt;
    private DbsField pnd_Mm_Rpt;
    private DbsField pnd_A_Cnt;
    private DbsField pnd_Aa_Cnt;
    private DbsField pnd_B_Cnt;
    private DbsField pnd_Bb_Cnt;
    private DbsField pnd_C_Cnt;
    private DbsField pnd_Cc_Cnt;
    private DbsField pnd_E_Cnt;
    private DbsField pnd_Ee_Cnt;
    private DbsField pnd_F_Cnt;
    private DbsField pnd_G_Cnt;
    private DbsField pnd_H_Cnt;
    private DbsField pnd_I_Cnt;
    private DbsField pnd_Ii_Cnt;
    private DbsField pnd_J_Cnt;
    private DbsField pnd_L_Cnt;
    private DbsField pnd_N_Cnt;
    private DbsField pnd_Nn_Cnt;
    private DbsField pnd_P_Cnt;
    private DbsField pnd_Pp_Cnt;
    private DbsField pnd_R_Cnt;
    private DbsField pnd_Rr_Cnt;
    private DbsField pnd_U_Cnt;
    private DbsField pnd_Uu_Cnt;
    private DbsField pnd_X_Cnt;
    private DbsField pnd_Xx_Cnt;
    private DbsField pnd_T_Cnt;
    private DbsField pnd_Tt_Cnt;
    private DbsField pnd_M_Cnt;
    private DbsField pnd_Mm_Cnt;
    private DbsField pnd_Method;
    private DbsField pnd_First;
    private DbsField pnd_Found;
    private DbsField pnd_Trans_Cnt;
    private DbsField pnd_Plan;
    private DbsField pnd_W_Extract_Yyyymm;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_9;
    private DbsField pnd_Start_Date_Pnd_Start_Date_Yyyymm;

    private DbsGroup pnd_Start_Date__R_Field_10;
    private DbsField pnd_Start_Date_Pnd_Start_Yyyy;

    private DbsGroup pnd_Start_Date__R_Field_11;
    private DbsField pnd_Start_Date_Pnd_Start_Yyyy_A;
    private DbsField pnd_Start_Date_Pnd_Start_Mm;

    private DbsGroup pnd_Start_Date__R_Field_12;
    private DbsField pnd_Start_Date_Pnd_Start_Mm_A;
    private DbsField pnd_Start_Date_Pnd_Start_Dd;

    private DbsGroup pnd_Start_Date__R_Field_13;
    private DbsField pnd_Start_Date_Pnd_Start_Dd_A;
    private DbsField pnd_End_Date;

    private DbsGroup pnd_End_Date__R_Field_14;
    private DbsField pnd_End_Date_Pnd_End_Date_Yyyymm;

    private DbsGroup pnd_End_Date__R_Field_15;
    private DbsField pnd_End_Date_Pnd_End_Yyyy;
    private DbsField pnd_End_Date_Pnd_End_Mm;
    private DbsField pnd_End_Date_Pnd_End_Dd;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_16;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_reprint = new DataAccessProgramView(new NameInfo("vw_reprint", "REPRINT"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE");
        reprint_Rp_Tiaa_Contr = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RP_TIAA_CONTR");
        reprint_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        reprint_Rp_Sgrd_Plan_No = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Sgrd_Plan_No", "RP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "RP_SGRD_PLAN_NO");
        reprint_Rp_Sgrd_Subplan_No = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Sgrd_Subplan_No", "RP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "RP_SGRD_SUBPLAN_NO");

        reprint__R_Field_1 = vw_reprint.getRecord().newGroupInGroup("reprint__R_Field_1", "REDEFINE", reprint_Rp_Sgrd_Subplan_No);
        reprint_Rp_Sgrd_Subplan_No_3 = reprint__R_Field_1.newFieldInGroup("reprint_Rp_Sgrd_Subplan_No_3", "RP-SGRD-SUBPLAN-NO-3", FieldType.STRING, 3);
        reprint_Rp_T_Doi = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_T_Doi", "RP-T-DOI", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_T_DOI");
        reprint_Rp_T_Doi.setDdmHeader("TIAA/DOI");
        reprint_Rp_C_Doi = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_C_Doi", "RP-C-DOI", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "RP_C_DOI");
        reprint_Rp_C_Doi.setDdmHeader("CREF/DOI");
        reprint_Rp_Soc_Sec = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Soc_Sec", "RP-SOC-SEC", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "RP_SOC_SEC");
        reprint_Rp_Soc_Sec.setDdmHeader("SOC/SEC");
        reprint_Rp_Extract_Date = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Extract_Date", "RP-EXTRACT-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RP_EXTRACT_DATE");
        reprint_Rp_Extract_Date.setDdmHeader("EXTRACT/ DATE");

        reprint__R_Field_2 = vw_reprint.getRecord().newGroupInGroup("reprint__R_Field_2", "REDEFINE", reprint_Rp_Extract_Date);
        reprint_Rp_Extract_Yyyymm = reprint__R_Field_2.newFieldInGroup("reprint_Rp_Extract_Yyyymm", "RP-EXTRACT-YYYYMM", FieldType.NUMERIC, 6);
        reprint_Rp_Extract_Dd = reprint__R_Field_2.newFieldInGroup("reprint_Rp_Extract_Dd", "RP-EXTRACT-DD", FieldType.NUMERIC, 2);
        reprint_Rp_Racf_Id = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Racf_Id", "RP-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RP_RACF_ID");
        reprint_Rp_Racf_Id.setDdmHeader("REQUESTOR/ID");
        reprint_Rp_Cor_First_Nme = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Cor_First_Nme", "RP-COR-FIRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "RP_COR_FIRST_NME");
        reprint_Rp_Cor_First_Nme.setDdmHeader("PARTICIPANT/FIRST NAME");
        reprint_Rp_Cor_Last_Nme = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Cor_Last_Nme", "RP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "RP_COR_LAST_NME");
        reprint_Rp_Cor_Last_Nme.setDdmHeader("PARTICIPANT/LAST NAME");
        reprint_Rp_Package_Mail_Type = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Package_Mail_Type", "RP-PACKAGE-MAIL-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RP_PACKAGE_MAIL_TYPE");
        reprint_Rp_Text_Udf_1 = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Text_Udf_1", "RP-TEXT-UDF-1", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RP_TEXT_UDF_1");

        reprint__R_Field_3 = vw_reprint.getRecord().newGroupInGroup("reprint__R_Field_3", "REDEFINE", reprint_Rp_Text_Udf_1);
        reprint_Rp_Single_Issue_Ind = reprint__R_Field_3.newFieldInGroup("reprint_Rp_Single_Issue_Ind", "RP-SINGLE-ISSUE-IND", FieldType.STRING, 1);
        reprint_Rp_Sgrd_Divsub = reprint__R_Field_3.newFieldInGroup("reprint_Rp_Sgrd_Divsub", "RP-SGRD-DIVSUB", FieldType.STRING, 4);
        reprint_Rp_Spec_Fund_Ind = reprint__R_Field_3.newFieldInGroup("reprint_Rp_Spec_Fund_Ind", "RP-SPEC-FUND-IND", FieldType.STRING, 3);
        reprint_Rp_Plan_Issue_State = reprint__R_Field_3.newFieldInGroup("reprint_Rp_Plan_Issue_State", "RP-PLAN-ISSUE-STATE", FieldType.STRING, 2);
        reprint_Rp_Dt_Released = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Dt_Released", "RP-DT-RELEASED", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RP_DT_RELEASED");
        reprint_Rp_Dt_Released.setDdmHeader("DATE/RELEASED");
        reprint_Rp_Applcnt_Req_Type = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Applcnt_Req_Type", "RP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RP_APPLCNT_REQ_TYPE");
        reprint_Rp_Applcnt_Req_Type.setDdmHeader("APP REQ/TYPE");
        reprint_Rp_Coll_Code = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Coll_Code", "RP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "RP_COLL_CODE");
        reprint_Rp_Coll_Code.setDdmHeader("PPG");
        reprint_Rp_Lob = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Lob", "RP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RP_LOB");
        reprint_Rp_Lob.setDdmHeader("LOB");
        reprint_Rp_Lob_Type = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Lob_Type", "RP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RP_LOB_TYPE");
        reprint_Rp_Lob_Type.setDdmHeader("LOB/TYPE");
        registerRecord(vw_reprint);

        vw_table = new DataAccessProgramView(new NameInfo("vw_table", "TABLE"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        table_Entry_Actve_Ind = vw_table.getRecord().newFieldInGroup("table_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ENTRY_ACTVE_IND");
        table_Entry_Table_Id_Nbr = vw_table.getRecord().newFieldInGroup("table_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, 
            "ENTRY_TABLE_ID_NBR");
        table_Entry_Table_Sub_Id = vw_table.getRecord().newFieldInGroup("table_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ENTRY_TABLE_SUB_ID");
        table_Entry_Cde = vw_table.getRecord().newFieldInGroup("table_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, "ENTRY_CDE");

        table__R_Field_4 = vw_table.getRecord().newGroupInGroup("table__R_Field_4", "REDEFINE", table_Entry_Cde);
        table_Last_Month = table__R_Field_4.newFieldInGroup("table_Last_Month", "LAST-MONTH", FieldType.NUMERIC, 8);

        table__R_Field_5 = table__R_Field_4.newGroupInGroup("table__R_Field_5", "REDEFINE", table_Last_Month);
        table_Last_Month_Yyyy = table__R_Field_5.newFieldInGroup("table_Last_Month_Yyyy", "LAST-MONTH-YYYY", FieldType.NUMERIC, 4);
        table_Last_Month_Mm = table__R_Field_5.newFieldInGroup("table_Last_Month_Mm", "LAST-MONTH-MM", FieldType.NUMERIC, 2);
        table_Last_Month_Dd = table__R_Field_5.newFieldInGroup("table_Last_Month_Dd", "LAST-MONTH-DD", FieldType.NUMERIC, 2);

        table__R_Field_6 = vw_table.getRecord().newGroupInGroup("table__R_Field_6", "REDEFINE", table_Entry_Cde);
        table_Plan = table__R_Field_6.newFieldInGroup("table_Plan", "PLAN", FieldType.STRING, 6);
        table_Enr_Method = table__R_Field_6.newFieldInGroup("table_Enr_Method", "ENR-METHOD", FieldType.STRING, 2);
        table_Entry_Dscrptn_Txt = vw_table.getRecord().newFieldInGroup("table_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 60, RepeatingFieldStrategy.None, 
            "ENTRY_DSCRPTN_TXT");
        table_Entry_User_Id = vw_table.getRecord().newFieldInGroup("table_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_USER_ID");
        table_State_Zip_From = vw_table.getRecord().newFieldInGroup("table_State_Zip_From", "STATE-ZIP-FROM", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "STATE_ZIP_FROM");

        table__R_Field_7 = vw_table.getRecord().newGroupInGroup("table__R_Field_7", "REDEFINE", table_State_Zip_From);
        table_Enr_Count = table__R_Field_7.newFieldInGroup("table_Enr_Count", "ENR-COUNT", FieldType.NUMERIC, 9);
        registerRecord(vw_table);

        vw_update_Table = new DataAccessProgramView(new NameInfo("vw_update_Table", "UPDATE-TABLE"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        update_Table_Entry_Actve_Ind = vw_update_Table.getRecord().newFieldInGroup("update_Table_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        update_Table_Entry_Table_Id_Nbr = vw_update_Table.getRecord().newFieldInGroup("update_Table_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        update_Table_Entry_Table_Sub_Id = vw_update_Table.getRecord().newFieldInGroup("update_Table_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        update_Table_Entry_Cde = vw_update_Table.getRecord().newFieldInGroup("update_Table_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        update_Table_State_Zip_From = vw_update_Table.getRecord().newFieldInGroup("update_Table_State_Zip_From", "STATE-ZIP-FROM", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "STATE_ZIP_FROM");

        update_Table__R_Field_8 = vw_update_Table.getRecord().newGroupInGroup("update_Table__R_Field_8", "REDEFINE", update_Table_State_Zip_From);
        update_Table_Enr_Count = update_Table__R_Field_8.newFieldInGroup("update_Table_Enr_Count", "ENR-COUNT", FieldType.NUMERIC, 9);
        registerRecord(vw_update_Table);

        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 12);
        pnd_Plan_Head = localVariables.newFieldInRecord("pnd_Plan_Head", "#PLAN-HEAD", FieldType.STRING, 8);
        pnd_A_Head = localVariables.newFieldInRecord("pnd_A_Head", "#A-HEAD", FieldType.STRING, 8);
        pnd_Aa_Head = localVariables.newFieldInRecord("pnd_Aa_Head", "#AA-HEAD", FieldType.STRING, 8);
        pnd_B_Head = localVariables.newFieldInRecord("pnd_B_Head", "#B-HEAD", FieldType.STRING, 8);
        pnd_Bb_Head = localVariables.newFieldInRecord("pnd_Bb_Head", "#BB-HEAD", FieldType.STRING, 8);
        pnd_C_Head = localVariables.newFieldInRecord("pnd_C_Head", "#C-HEAD", FieldType.STRING, 8);
        pnd_Cc_Head = localVariables.newFieldInRecord("pnd_Cc_Head", "#CC-HEAD", FieldType.STRING, 8);
        pnd_E_Head = localVariables.newFieldInRecord("pnd_E_Head", "#E-HEAD", FieldType.STRING, 8);
        pnd_Ee_Head = localVariables.newFieldInRecord("pnd_Ee_Head", "#EE-HEAD", FieldType.STRING, 8);
        pnd_F_Head = localVariables.newFieldInRecord("pnd_F_Head", "#F-HEAD", FieldType.STRING, 8);
        pnd_G_Head = localVariables.newFieldInRecord("pnd_G_Head", "#G-HEAD", FieldType.STRING, 8);
        pnd_H_Head = localVariables.newFieldInRecord("pnd_H_Head", "#H-HEAD", FieldType.STRING, 8);
        pnd_I_Head = localVariables.newFieldInRecord("pnd_I_Head", "#I-HEAD", FieldType.STRING, 8);
        pnd_Ii_Head = localVariables.newFieldInRecord("pnd_Ii_Head", "#II-HEAD", FieldType.STRING, 8);
        pnd_J_Head = localVariables.newFieldInRecord("pnd_J_Head", "#J-HEAD", FieldType.STRING, 8);
        pnd_L_Head = localVariables.newFieldInRecord("pnd_L_Head", "#L-HEAD", FieldType.STRING, 8);
        pnd_N_Head = localVariables.newFieldInRecord("pnd_N_Head", "#N-HEAD", FieldType.STRING, 8);
        pnd_Nn_Head = localVariables.newFieldInRecord("pnd_Nn_Head", "#NN-HEAD", FieldType.STRING, 8);
        pnd_P_Head = localVariables.newFieldInRecord("pnd_P_Head", "#P-HEAD", FieldType.STRING, 8);
        pnd_Pp_Head = localVariables.newFieldInRecord("pnd_Pp_Head", "#PP-HEAD", FieldType.STRING, 8);
        pnd_R_Head = localVariables.newFieldInRecord("pnd_R_Head", "#R-HEAD", FieldType.STRING, 8);
        pnd_Rr_Head = localVariables.newFieldInRecord("pnd_Rr_Head", "#RR-HEAD", FieldType.STRING, 8);
        pnd_U_Head = localVariables.newFieldInRecord("pnd_U_Head", "#U-HEAD", FieldType.STRING, 8);
        pnd_Uu_Head = localVariables.newFieldInRecord("pnd_Uu_Head", "#UU-HEAD", FieldType.STRING, 8);
        pnd_X_Head = localVariables.newFieldInRecord("pnd_X_Head", "#X-HEAD", FieldType.STRING, 8);
        pnd_Xx_Head = localVariables.newFieldInRecord("pnd_Xx_Head", "#XX-HEAD", FieldType.STRING, 8);
        pnd_T_Head = localVariables.newFieldInRecord("pnd_T_Head", "#T-HEAD", FieldType.STRING, 8);
        pnd_Tt_Head = localVariables.newFieldInRecord("pnd_Tt_Head", "#TT-HEAD", FieldType.STRING, 8);
        pnd_M_Head = localVariables.newFieldInRecord("pnd_M_Head", "#M-HEAD", FieldType.STRING, 8);
        pnd_Mm_Head = localVariables.newFieldInRecord("pnd_Mm_Head", "#MM-HEAD", FieldType.STRING, 8);
        pnd_Rpt_Date = localVariables.newFieldInRecord("pnd_Rpt_Date", "#RPT-DATE", FieldType.STRING, 10);
        pnd_Save_Plan = localVariables.newFieldInRecord("pnd_Save_Plan", "#SAVE-PLAN", FieldType.STRING, 6);
        pnd_A_Rpt = localVariables.newFieldInRecord("pnd_A_Rpt", "#A-RPT", FieldType.STRING, 6);
        pnd_Aa_Rpt = localVariables.newFieldInRecord("pnd_Aa_Rpt", "#AA-RPT", FieldType.STRING, 6);
        pnd_B_Rpt = localVariables.newFieldInRecord("pnd_B_Rpt", "#B-RPT", FieldType.STRING, 6);
        pnd_Bb_Rpt = localVariables.newFieldInRecord("pnd_Bb_Rpt", "#BB-RPT", FieldType.STRING, 6);
        pnd_C_Rpt = localVariables.newFieldInRecord("pnd_C_Rpt", "#C-RPT", FieldType.STRING, 6);
        pnd_Cc_Rpt = localVariables.newFieldInRecord("pnd_Cc_Rpt", "#CC-RPT", FieldType.STRING, 6);
        pnd_E_Rpt = localVariables.newFieldInRecord("pnd_E_Rpt", "#E-RPT", FieldType.STRING, 6);
        pnd_Ee_Rpt = localVariables.newFieldInRecord("pnd_Ee_Rpt", "#EE-RPT", FieldType.STRING, 6);
        pnd_F_Rpt = localVariables.newFieldInRecord("pnd_F_Rpt", "#F-RPT", FieldType.STRING, 6);
        pnd_G_Rpt = localVariables.newFieldInRecord("pnd_G_Rpt", "#G-RPT", FieldType.STRING, 6);
        pnd_H_Rpt = localVariables.newFieldInRecord("pnd_H_Rpt", "#H-RPT", FieldType.STRING, 6);
        pnd_I_Rpt = localVariables.newFieldInRecord("pnd_I_Rpt", "#I-RPT", FieldType.STRING, 6);
        pnd_Ii_Rpt = localVariables.newFieldInRecord("pnd_Ii_Rpt", "#II-RPT", FieldType.STRING, 6);
        pnd_J_Rpt = localVariables.newFieldInRecord("pnd_J_Rpt", "#J-RPT", FieldType.STRING, 6);
        pnd_L_Rpt = localVariables.newFieldInRecord("pnd_L_Rpt", "#L-RPT", FieldType.STRING, 6);
        pnd_N_Rpt = localVariables.newFieldInRecord("pnd_N_Rpt", "#N-RPT", FieldType.STRING, 6);
        pnd_Nn_Rpt = localVariables.newFieldInRecord("pnd_Nn_Rpt", "#NN-RPT", FieldType.STRING, 6);
        pnd_P_Rpt = localVariables.newFieldInRecord("pnd_P_Rpt", "#P-RPT", FieldType.STRING, 6);
        pnd_Pp_Rpt = localVariables.newFieldInRecord("pnd_Pp_Rpt", "#PP-RPT", FieldType.STRING, 6);
        pnd_R_Rpt = localVariables.newFieldInRecord("pnd_R_Rpt", "#R-RPT", FieldType.STRING, 6);
        pnd_Rr_Rpt = localVariables.newFieldInRecord("pnd_Rr_Rpt", "#RR-RPT", FieldType.STRING, 6);
        pnd_U_Rpt = localVariables.newFieldInRecord("pnd_U_Rpt", "#U-RPT", FieldType.STRING, 6);
        pnd_Uu_Rpt = localVariables.newFieldInRecord("pnd_Uu_Rpt", "#UU-RPT", FieldType.STRING, 6);
        pnd_X_Rpt = localVariables.newFieldInRecord("pnd_X_Rpt", "#X-RPT", FieldType.STRING, 6);
        pnd_Xx_Rpt = localVariables.newFieldInRecord("pnd_Xx_Rpt", "#XX-RPT", FieldType.STRING, 6);
        pnd_T_Rpt = localVariables.newFieldInRecord("pnd_T_Rpt", "#T-RPT", FieldType.STRING, 6);
        pnd_Tt_Rpt = localVariables.newFieldInRecord("pnd_Tt_Rpt", "#TT-RPT", FieldType.STRING, 6);
        pnd_M_Rpt = localVariables.newFieldInRecord("pnd_M_Rpt", "#M-RPT", FieldType.STRING, 6);
        pnd_Mm_Rpt = localVariables.newFieldInRecord("pnd_Mm_Rpt", "#MM-RPT", FieldType.STRING, 6);
        pnd_A_Cnt = localVariables.newFieldInRecord("pnd_A_Cnt", "#A-CNT", FieldType.NUMERIC, 6);
        pnd_Aa_Cnt = localVariables.newFieldInRecord("pnd_Aa_Cnt", "#AA-CNT", FieldType.NUMERIC, 6);
        pnd_B_Cnt = localVariables.newFieldInRecord("pnd_B_Cnt", "#B-CNT", FieldType.NUMERIC, 6);
        pnd_Bb_Cnt = localVariables.newFieldInRecord("pnd_Bb_Cnt", "#BB-CNT", FieldType.NUMERIC, 6);
        pnd_C_Cnt = localVariables.newFieldInRecord("pnd_C_Cnt", "#C-CNT", FieldType.NUMERIC, 6);
        pnd_Cc_Cnt = localVariables.newFieldInRecord("pnd_Cc_Cnt", "#CC-CNT", FieldType.NUMERIC, 6);
        pnd_E_Cnt = localVariables.newFieldInRecord("pnd_E_Cnt", "#E-CNT", FieldType.NUMERIC, 6);
        pnd_Ee_Cnt = localVariables.newFieldInRecord("pnd_Ee_Cnt", "#EE-CNT", FieldType.NUMERIC, 6);
        pnd_F_Cnt = localVariables.newFieldInRecord("pnd_F_Cnt", "#F-CNT", FieldType.NUMERIC, 6);
        pnd_G_Cnt = localVariables.newFieldInRecord("pnd_G_Cnt", "#G-CNT", FieldType.NUMERIC, 6);
        pnd_H_Cnt = localVariables.newFieldInRecord("pnd_H_Cnt", "#H-CNT", FieldType.NUMERIC, 6);
        pnd_I_Cnt = localVariables.newFieldInRecord("pnd_I_Cnt", "#I-CNT", FieldType.NUMERIC, 6);
        pnd_Ii_Cnt = localVariables.newFieldInRecord("pnd_Ii_Cnt", "#II-CNT", FieldType.NUMERIC, 6);
        pnd_J_Cnt = localVariables.newFieldInRecord("pnd_J_Cnt", "#J-CNT", FieldType.NUMERIC, 6);
        pnd_L_Cnt = localVariables.newFieldInRecord("pnd_L_Cnt", "#L-CNT", FieldType.NUMERIC, 6);
        pnd_N_Cnt = localVariables.newFieldInRecord("pnd_N_Cnt", "#N-CNT", FieldType.NUMERIC, 6);
        pnd_Nn_Cnt = localVariables.newFieldInRecord("pnd_Nn_Cnt", "#NN-CNT", FieldType.NUMERIC, 6);
        pnd_P_Cnt = localVariables.newFieldInRecord("pnd_P_Cnt", "#P-CNT", FieldType.NUMERIC, 6);
        pnd_Pp_Cnt = localVariables.newFieldInRecord("pnd_Pp_Cnt", "#PP-CNT", FieldType.NUMERIC, 6);
        pnd_R_Cnt = localVariables.newFieldInRecord("pnd_R_Cnt", "#R-CNT", FieldType.NUMERIC, 6);
        pnd_Rr_Cnt = localVariables.newFieldInRecord("pnd_Rr_Cnt", "#RR-CNT", FieldType.NUMERIC, 6);
        pnd_U_Cnt = localVariables.newFieldInRecord("pnd_U_Cnt", "#U-CNT", FieldType.NUMERIC, 6);
        pnd_Uu_Cnt = localVariables.newFieldInRecord("pnd_Uu_Cnt", "#UU-CNT", FieldType.NUMERIC, 6);
        pnd_X_Cnt = localVariables.newFieldInRecord("pnd_X_Cnt", "#X-CNT", FieldType.NUMERIC, 6);
        pnd_Xx_Cnt = localVariables.newFieldInRecord("pnd_Xx_Cnt", "#XX-CNT", FieldType.NUMERIC, 6);
        pnd_T_Cnt = localVariables.newFieldInRecord("pnd_T_Cnt", "#T-CNT", FieldType.NUMERIC, 6);
        pnd_Tt_Cnt = localVariables.newFieldInRecord("pnd_Tt_Cnt", "#TT-CNT", FieldType.NUMERIC, 6);
        pnd_M_Cnt = localVariables.newFieldInRecord("pnd_M_Cnt", "#M-CNT", FieldType.NUMERIC, 6);
        pnd_Mm_Cnt = localVariables.newFieldInRecord("pnd_Mm_Cnt", "#MM-CNT", FieldType.NUMERIC, 6);
        pnd_Method = localVariables.newFieldInRecord("pnd_Method", "#METHOD", FieldType.STRING, 2);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Trans_Cnt = localVariables.newFieldInRecord("pnd_Trans_Cnt", "#TRANS-CNT", FieldType.NUMERIC, 3);
        pnd_Plan = localVariables.newFieldInRecord("pnd_Plan", "#PLAN", FieldType.STRING, 6);
        pnd_W_Extract_Yyyymm = localVariables.newFieldInRecord("pnd_W_Extract_Yyyymm", "#W-EXTRACT-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);

        pnd_Start_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_9", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Start_Date_Yyyymm = pnd_Start_Date__R_Field_9.newFieldInGroup("pnd_Start_Date_Pnd_Start_Date_Yyyymm", "#START-DATE-YYYYMM", 
            FieldType.NUMERIC, 6);

        pnd_Start_Date__R_Field_10 = pnd_Start_Date__R_Field_9.newGroupInGroup("pnd_Start_Date__R_Field_10", "REDEFINE", pnd_Start_Date_Pnd_Start_Date_Yyyymm);
        pnd_Start_Date_Pnd_Start_Yyyy = pnd_Start_Date__R_Field_10.newFieldInGroup("pnd_Start_Date_Pnd_Start_Yyyy", "#START-YYYY", FieldType.NUMERIC, 
            4);

        pnd_Start_Date__R_Field_11 = pnd_Start_Date__R_Field_10.newGroupInGroup("pnd_Start_Date__R_Field_11", "REDEFINE", pnd_Start_Date_Pnd_Start_Yyyy);
        pnd_Start_Date_Pnd_Start_Yyyy_A = pnd_Start_Date__R_Field_11.newFieldInGroup("pnd_Start_Date_Pnd_Start_Yyyy_A", "#START-YYYY-A", FieldType.STRING, 
            4);
        pnd_Start_Date_Pnd_Start_Mm = pnd_Start_Date__R_Field_10.newFieldInGroup("pnd_Start_Date_Pnd_Start_Mm", "#START-MM", FieldType.NUMERIC, 2);

        pnd_Start_Date__R_Field_12 = pnd_Start_Date__R_Field_10.newGroupInGroup("pnd_Start_Date__R_Field_12", "REDEFINE", pnd_Start_Date_Pnd_Start_Mm);
        pnd_Start_Date_Pnd_Start_Mm_A = pnd_Start_Date__R_Field_12.newFieldInGroup("pnd_Start_Date_Pnd_Start_Mm_A", "#START-MM-A", FieldType.STRING, 2);
        pnd_Start_Date_Pnd_Start_Dd = pnd_Start_Date__R_Field_9.newFieldInGroup("pnd_Start_Date_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 2);

        pnd_Start_Date__R_Field_13 = pnd_Start_Date__R_Field_9.newGroupInGroup("pnd_Start_Date__R_Field_13", "REDEFINE", pnd_Start_Date_Pnd_Start_Dd);
        pnd_Start_Date_Pnd_Start_Dd_A = pnd_Start_Date__R_Field_13.newFieldInGroup("pnd_Start_Date_Pnd_Start_Dd_A", "#START-DD-A", FieldType.STRING, 2);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.NUMERIC, 8);

        pnd_End_Date__R_Field_14 = localVariables.newGroupInRecord("pnd_End_Date__R_Field_14", "REDEFINE", pnd_End_Date);
        pnd_End_Date_Pnd_End_Date_Yyyymm = pnd_End_Date__R_Field_14.newFieldInGroup("pnd_End_Date_Pnd_End_Date_Yyyymm", "#END-DATE-YYYYMM", FieldType.NUMERIC, 
            6);

        pnd_End_Date__R_Field_15 = pnd_End_Date__R_Field_14.newGroupInGroup("pnd_End_Date__R_Field_15", "REDEFINE", pnd_End_Date_Pnd_End_Date_Yyyymm);
        pnd_End_Date_Pnd_End_Yyyy = pnd_End_Date__R_Field_15.newFieldInGroup("pnd_End_Date_Pnd_End_Yyyy", "#END-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Date_Pnd_End_Mm = pnd_End_Date__R_Field_15.newFieldInGroup("pnd_End_Date_Pnd_End_Mm", "#END-MM", FieldType.NUMERIC, 2);
        pnd_End_Date_Pnd_End_Dd = pnd_End_Date__R_Field_14.newFieldInGroup("pnd_End_Date_Pnd_End_Dd", "#END-DD", FieldType.NUMERIC, 2);
        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 28);

        pnd_Table_Id_Entry_Cde__R_Field_16 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_16", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_16.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr", 
            "#ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id = pnd_Table_Id_Entry_Cde__R_Field_16.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id", 
            "#ENTRY-TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_16.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde", "#ENTRY-CDE", 
            FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_reprint.reset();
        vw_table.reset();
        vw_update_Table.reset();

        localVariables.reset();
        pnd_Date.setInitialValue(" DATE");
        pnd_Plan_Head.setInitialValue("PLAN_#");
        pnd_A_Head.setInitialValue(" A_CNT");
        pnd_Aa_Head.setInitialValue("AA_CNT");
        pnd_B_Head.setInitialValue(" B_CNT");
        pnd_Bb_Head.setInitialValue("BB_CNT");
        pnd_C_Head.setInitialValue(" C_CNT");
        pnd_Cc_Head.setInitialValue("CC_CNT");
        pnd_E_Head.setInitialValue(" E_CNT");
        pnd_Ee_Head.setInitialValue("EE_CNT");
        pnd_F_Head.setInitialValue(" F_CNT");
        pnd_G_Head.setInitialValue(" G_CNT");
        pnd_H_Head.setInitialValue(" H_CNT");
        pnd_I_Head.setInitialValue(" I_CNT");
        pnd_Ii_Head.setInitialValue("II_CNT");
        pnd_J_Head.setInitialValue(" J_CNT");
        pnd_L_Head.setInitialValue(" L_CNT");
        pnd_N_Head.setInitialValue(" N_CNT");
        pnd_Nn_Head.setInitialValue("NN_CNT");
        pnd_P_Head.setInitialValue(" P_CNT");
        pnd_Pp_Head.setInitialValue("PP_CNT");
        pnd_R_Head.setInitialValue(" R_CNT");
        pnd_Rr_Head.setInitialValue("RR_CNT");
        pnd_U_Head.setInitialValue(" U_CNT");
        pnd_Uu_Head.setInitialValue("UU_CNT");
        pnd_X_Head.setInitialValue(" X_CNT");
        pnd_Xx_Head.setInitialValue("XX_CNT");
        pnd_T_Head.setInitialValue(" T_CNT");
        pnd_Tt_Head.setInitialValue("TT_CNT");
        pnd_M_Head.setInitialValue(" M_CNT");
        pnd_Mm_Head.setInitialValue("MM_CNT");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb900() throws Exception
    {
        super("Appb900");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* **********************************************************************
        //* * MAIN LOGIC SECTION                                                **
        //* **********************************************************************
        //*  EASEENR. EXPAND LS FROM 237 TO 250 FOR NEW
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 250 PS = 0
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(900);                                                                                                      //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 000900
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("DT");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'DT'
        vw_table.startDatabaseRead                                                                                                                                        //Natural: READ TABLE BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "RD",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        RD:
        while (condition(vw_table.readNextRow("RD")))
        {
            if (condition(table_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || table_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id))) //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND := TRUE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            pnd_Start_Date.setValue(table_Last_Month);                                                                                                                    //Natural: ASSIGN #START-DATE := LAST-MONTH
            pnd_Start_Date_Pnd_Start_Mm.nadd(1);                                                                                                                          //Natural: ADD 1 TO #START-MM
            if (condition(pnd_Start_Date_Pnd_Start_Mm.greater(12)))                                                                                                       //Natural: IF #START-MM GT 12
            {
                pnd_Start_Date_Pnd_Start_Mm.setValue(1);                                                                                                                  //Natural: ASSIGN #START-MM := 1
                pnd_Start_Date_Pnd_Start_Yyyy.nadd(1);                                                                                                                    //Natural: ADD 1 TO #START-YYYY
            }                                                                                                                                                             //Natural: END-IF
            pnd_End_Date.setValue(pnd_Start_Date);                                                                                                                        //Natural: ASSIGN #END-DATE := #START-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Start_Date.setValue(20090701);                                                                                                                            //Natural: ASSIGN #START-DATE := #END-DATE := 20090701
            pnd_End_Date.setValue(20090701);
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rpt_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Start_Date_Pnd_Start_Mm_A, "/", pnd_Start_Date_Pnd_Start_Dd_A, "/",                     //Natural: COMPRESS #START-MM-A '/' #START-DD-A '/' #START-YYYY-A INTO #RPT-DATE LEAVING NO
            pnd_Start_Date_Pnd_Start_Yyyy_A));
        getReports().write(0, "START DATE:",pnd_Start_Date);                                                                                                              //Natural: WRITE 'START DATE:' #START-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "END DATE  :",pnd_End_Date);                                                                                                                //Natural: WRITE 'END DATE  :' #END-DATE
        if (Global.isEscape()) return;
        //*  BE1.
        //*  BE1.
        //*  EASEENR
        //*  EASEENR
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(4),"DATE",new TabSetting(12),"PLAN_#",new TabSetting(20)," A_CNT",new TabSetting(28),"AA_CNT",new       //Natural: WRITE ( 1 ) NOTITLE 4T 'DATE' 12T 'PLAN_#' 20T ' A_CNT' 28T 'AA_CNT' 36T ' B_CNT' 44T 'BB_CNT' 52T ' C_CNT' 60T 'CC_CNT' 68T ' E_CNT' 76T 'EE_CNT' 84T ' F_CNT' 92T ' G_CNT' 100T ' H_CNT' 107T ' I_CNT' 115T 'II_CNT' 123T ' J_CNT' 132T ' L_CNT' 140T ' N_CNT' 148T 'NN_CNT' 156T ' P_CNT' 164T 'PP_CNT' 172T ' R_CNT' 180T 'RR_CNT' 188T ' U_CNT' 196T 'UU_CNT' 204T ' X_CNT' 212T 'XX_CNT' 220T ' T_CNT' 228T 'TT_CNT' 236T ' M_CNT' 244T 'MM_CNT'
            TabSetting(36)," B_CNT",new TabSetting(44),"BB_CNT",new TabSetting(52)," C_CNT",new TabSetting(60),"CC_CNT",new TabSetting(68)," E_CNT",new 
            TabSetting(76),"EE_CNT",new TabSetting(84)," F_CNT",new TabSetting(92)," G_CNT",new TabSetting(100)," H_CNT",new TabSetting(107)," I_CNT",new 
            TabSetting(115),"II_CNT",new TabSetting(123)," J_CNT",new TabSetting(132)," L_CNT",new TabSetting(140)," N_CNT",new TabSetting(148),"NN_CNT",new 
            TabSetting(156)," P_CNT",new TabSetting(164),"PP_CNT",new TabSetting(172)," R_CNT",new TabSetting(180),"RR_CNT",new TabSetting(188)," U_CNT",new 
            TabSetting(196),"UU_CNT",new TabSetting(204)," X_CNT",new TabSetting(212),"XX_CNT",new TabSetting(220)," T_CNT",new TabSetting(228),"TT_CNT",new 
            TabSetting(236)," M_CNT",new TabSetting(244),"MM_CNT");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DEL-PLAN-ENTRIES
        sub_Del_Plan_Entries();
        if (condition(Global.isEscape())) {return;}
        vw_reprint.startDatabaseRead                                                                                                                                      //Natural: READ REPRINT BY RP-EXTRACT-DATE STARTING FROM #START-DATE
        (
        "READPRAP",
        new Wc[] { new Wc("RP_EXTRACT_DATE", ">=", pnd_Start_Date, WcType.BY) },
        new Oc[] { new Oc("RP_EXTRACT_DATE", "ASC") }
        );
        READPRAP:
        while (condition(vw_reprint.readNextRow("READPRAP")))
        {
            if (condition(reprint_Rp_Extract_Yyyymm.greater(pnd_End_Date_Pnd_End_Date_Yyyymm)))                                                                           //Natural: IF RP-EXTRACT-YYYYMM GT #END-DATE-YYYYMM
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_W_Extract_Yyyymm.equals(getZero())))                                                                                                        //Natural: IF #W-EXTRACT-YYYYMM = 0
            {
                pnd_W_Extract_Yyyymm.setValue(reprint_Rp_Extract_Yyyymm);                                                                                                 //Natural: ASSIGN #W-EXTRACT-YYYYMM := RP-EXTRACT-YYYYMM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(reprint_Rp_Extract_Yyyymm.notEquals(pnd_W_Extract_Yyyymm)))                                                                                     //Natural: IF RP-EXTRACT-YYYYMM NE #W-EXTRACT-YYYYMM
            {
                pnd_W_Extract_Yyyymm.setValue(reprint_Rp_Extract_Yyyymm);                                                                                                 //Natural: ASSIGN #W-EXTRACT-YYYYMM := RP-EXTRACT-YYYYMM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(reprint_Rp_Applcnt_Req_Type.equals("O")))                                                                                                       //Natural: IF RP-APPLCNT-REQ-TYPE = 'O'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Method.setValue(reprint_Rp_Applcnt_Req_Type);                                                                                                             //Natural: ASSIGN #METHOD := RP-APPLCNT-REQ-TYPE
            if (condition(reprint_Rp_Sgrd_Plan_No.getSubstring(1,3).equals("IRA")))                                                                                       //Natural: IF SUBSTR ( RP-SGRD-PLAN-NO,1,3 ) = 'IRA'
            {
                short decideConditionsMet290 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE RP-APPLCNT-REQ-TYPE;//Natural: VALUE 'C'
                if (condition((reprint_Rp_Applcnt_Req_Type.equals("C"))))
                {
                    decideConditionsMet290++;
                    pnd_Method.setValue("F");                                                                                                                             //Natural: ASSIGN #METHOD := 'F'
                }                                                                                                                                                         //Natural: VALUE 'P'
                else if (condition((reprint_Rp_Applcnt_Req_Type.equals("P"))))
                {
                    decideConditionsMet290++;
                    pnd_Method.setValue("G");                                                                                                                             //Natural: ASSIGN #METHOD := 'G'
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((reprint_Rp_Applcnt_Req_Type.equals("R"))))
                {
                    decideConditionsMet290++;
                    pnd_Method.setValue("H");                                                                                                                             //Natural: ASSIGN #METHOD := 'H'
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((reprint_Rp_Applcnt_Req_Type.equals("X"))))
                {
                    decideConditionsMet290++;
                    pnd_Method.setValue("J");                                                                                                                             //Natural: ASSIGN #METHOD := 'J'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(reprint_Rp_Sgrd_Plan_No.notEquals(" ")))                                                                                                        //Natural: IF RP-SGRD-PLAN-NO NE ' '
            {
                pnd_Plan.setValue(reprint_Rp_Sgrd_Plan_No);                                                                                                               //Natural: ASSIGN #PLAN := RP-SGRD-PLAN-NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Plan.setValue("LEGACY");                                                                                                                              //Natural: ASSIGN #PLAN := 'LEGACY'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(reprint_Rp_Applcnt_Req_Type.equals(" ")))                                                                                                       //Natural: IF RP-APPLCNT-REQ-TYPE = ' '
            {
                if (condition(reprint_Rp_Lob.equals("D") && reprint_Rp_Lob_Type.equals("6")))                                                                             //Natural: IF RP-LOB = 'D' AND RP-LOB-TYPE = '6'
                {
                    pnd_Method.setValue("L");                                                                                                                             //Natural: ASSIGN #METHOD := 'L'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Method.setValue("U");                                                                                                                             //Natural: ASSIGN #METHOD := 'U'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  IF RP-APPLCNT-REQ-TYPE = 'M'                                /* EASEENR
            //*    #METHOD := 'U'                                            /* EASEENR
            //*  END-IF                                                      /* EASEENR
            if (condition(((((reprint_Rp_T_Doi.equals(getZero()) || reprint_Rp_C_Doi.equals(getZero())) && pnd_Method.notEquals("L")) && ! ((reprint_Rp_Lob.equals("D")   //Natural: IF ( RP-T-DOI = 0 OR RP-C-DOI = 0 ) AND #METHOD NE 'L' AND NOT ( RP-LOB = 'D' AND RP-LOB-TYPE = 'A' ) AND NOT ( RP-LOB = 'S' AND RP-LOB-TYPE = '7' )
                && reprint_Rp_Lob_Type.equals("A")))) && ! ((reprint_Rp_Lob.equals("S") && reprint_Rp_Lob_Type.equals("7"))))))
            {
                //*  EASEENR
                //*  EASEENR
                //*  BE1.
                //*  BE1.
                short decideConditionsMet323 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #METHOD;//Natural: VALUE 'A'
                if (condition((pnd_Method.equals("A"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("AA");                                                                                                                            //Natural: ASSIGN #METHOD := 'AA'
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((pnd_Method.equals("B"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("BB");                                                                                                                            //Natural: ASSIGN #METHOD := 'BB'
                }                                                                                                                                                         //Natural: VALUE 'C'
                else if (condition((pnd_Method.equals("C"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("CC");                                                                                                                            //Natural: ASSIGN #METHOD := 'CC'
                }                                                                                                                                                         //Natural: VALUE 'E'
                else if (condition((pnd_Method.equals("E"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("EE");                                                                                                                            //Natural: ASSIGN #METHOD := 'EE'
                }                                                                                                                                                         //Natural: VALUE 'I'
                else if (condition((pnd_Method.equals("I"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("II");                                                                                                                            //Natural: ASSIGN #METHOD := 'II'
                }                                                                                                                                                         //Natural: VALUE 'M'
                else if (condition((pnd_Method.equals("M"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("MM");                                                                                                                            //Natural: ASSIGN #METHOD := 'MM'
                }                                                                                                                                                         //Natural: VALUE 'N'
                else if (condition((pnd_Method.equals("N"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("NN");                                                                                                                            //Natural: ASSIGN #METHOD := 'NN'
                }                                                                                                                                                         //Natural: VALUE 'P'
                else if (condition((pnd_Method.equals("P"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("PP");                                                                                                                            //Natural: ASSIGN #METHOD := 'PP'
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pnd_Method.equals("R"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("RR");                                                                                                                            //Natural: ASSIGN #METHOD := 'RR'
                }                                                                                                                                                         //Natural: VALUE 'U'
                else if (condition((pnd_Method.equals("U"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("UU");                                                                                                                            //Natural: ASSIGN #METHOD := 'UU'
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pnd_Method.equals("X"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("XX");                                                                                                                            //Natural: ASSIGN #METHOD := 'XX'
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((pnd_Method.equals("T"))))
                {
                    decideConditionsMet323++;
                    pnd_Method.setValue("TT");                                                                                                                            //Natural: ASSIGN #METHOD := 'TT'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PLAN-TABLE-UPDATE
            sub_Plan_Table_Update();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READPRAP"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READPRAP"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM CREATE-REPORT
        sub_Create_Report();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-OR-ADD-TABLE
        sub_Update_Or_Add_Table();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEL-PLAN-ENTRIES
        //* ***********************************************************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEL-ENTRY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-REPORT
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-OR-ADD-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PLAN-TABLE-UPDATE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PLAN-TABLE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-PLAN-TABLE
        //* ***********************************************************************
    }
    private void sub_Del_Plan_Entries() throws Exception                                                                                                                  //Natural: DEL-PLAN-ENTRIES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(900);                                                                                                      //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 000900
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("OE");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'OE'
        pnd_First.setValue(true);                                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        vw_table.startDatabaseRead                                                                                                                                        //Natural: READ TABLE BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "DELTBL",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        DELTBL:
        while (condition(vw_table.readNextRow("DELTBL")))
        {
            if (condition(table_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || table_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id))) //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Trans_Cnt.nadd(+1);                                                                                                                                   //Natural: ADD +1 TO #TRANS-CNT
                                                                                                                                                                          //Natural: PERFORM DEL-ENTRY
                sub_Del_Entry();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("DELTBL"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("DELTBL"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Del_Entry() throws Exception                                                                                                                         //Natural: DEL-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        GETTBL:                                                                                                                                                           //Natural: GET UPDATE-TABLE *ISN ( DELTBL. )
        vw_update_Table.readByID(vw_table.getAstISN("DELTBL"), "GETTBL");
        vw_update_Table.deleteDBRow("GETTBL");                                                                                                                            //Natural: DELETE ( GETTBL. )
        if (condition(pnd_Trans_Cnt.equals(100)))                                                                                                                         //Natural: IF #TRANS-CNT = 100
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Trans_Cnt.reset();                                                                                                                                        //Natural: RESET #TRANS-CNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Report() throws Exception                                                                                                                     //Natural: CREATE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(900);                                                                                                      //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 000900
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("OE");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'OE'
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.reset();                                                                                                                     //Natural: RESET #ENTRY-CDE
        pnd_First.setValue(true);                                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        vw_table.startDatabaseRead                                                                                                                                        //Natural: READ TABLE BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
        (
        "READ01",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_table.readNextRow("READ01")))
        {
            if (condition(table_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || table_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id))) //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_Save_Plan.setValue(table_Plan);                                                                                                                       //Natural: ASSIGN #SAVE-PLAN := PLAN
                pnd_First.setValue(false);                                                                                                                                //Natural: ASSIGN #FIRST := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(table_Plan.notEquals(pnd_Save_Plan)))                                                                                                           //Natural: IF PLAN NE #SAVE-PLAN
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL
                sub_Write_Detail();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  EASEENR.
                pnd_A_Cnt.reset();                                                                                                                                        //Natural: RESET #A-CNT #B-CNT #C-CNT #E-CNT #F-CNT #G-CNT #H-CNT #I-CNT #J-CNT #L-CNT #N-CNT #P-CNT #R-CNT #U-CNT #X-CNT #AA-CNT #NN-CNT #BB-CNT #CC-CNT #EE-CNT #II-CNT #PP-CNT #RR-CNT #UU-CNT #XX-CNT #T-CNT #TT-CNT #M-CNT #MM-CNT
                pnd_B_Cnt.reset();
                pnd_C_Cnt.reset();
                pnd_E_Cnt.reset();
                pnd_F_Cnt.reset();
                pnd_G_Cnt.reset();
                pnd_H_Cnt.reset();
                pnd_I_Cnt.reset();
                pnd_J_Cnt.reset();
                pnd_L_Cnt.reset();
                pnd_N_Cnt.reset();
                pnd_P_Cnt.reset();
                pnd_R_Cnt.reset();
                pnd_U_Cnt.reset();
                pnd_X_Cnt.reset();
                pnd_Aa_Cnt.reset();
                pnd_Nn_Cnt.reset();
                pnd_Bb_Cnt.reset();
                pnd_Cc_Cnt.reset();
                pnd_Ee_Cnt.reset();
                pnd_Ii_Cnt.reset();
                pnd_Pp_Cnt.reset();
                pnd_Rr_Cnt.reset();
                pnd_Uu_Cnt.reset();
                pnd_Xx_Cnt.reset();
                pnd_T_Cnt.reset();
                pnd_Tt_Cnt.reset();
                pnd_M_Cnt.reset();
                pnd_Mm_Cnt.reset();
                pnd_Save_Plan.setValue(table_Plan);                                                                                                                       //Natural: ASSIGN #SAVE-PLAN := PLAN
            }                                                                                                                                                             //Natural: END-IF
            //*  EASEENR
            //*  EASEENR
            //*  EASEENR
            //*  EASEENR
            //*  BE1.
            //*  BE1.
            //*  BE1.
            //*  BE1.
            short decideConditionsMet437 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE ENR-METHOD;//Natural: VALUE 'A'
            if (condition((table_Enr_Method.equals("A"))))
            {
                decideConditionsMet437++;
                pnd_A_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #A-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'AA'
            else if (condition((table_Enr_Method.equals("AA"))))
            {
                decideConditionsMet437++;
                pnd_Aa_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #AA-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((table_Enr_Method.equals("B"))))
            {
                decideConditionsMet437++;
                pnd_B_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #B-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'BB'
            else if (condition((table_Enr_Method.equals("BB"))))
            {
                decideConditionsMet437++;
                pnd_Bb_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #BB-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((table_Enr_Method.equals("C"))))
            {
                decideConditionsMet437++;
                pnd_C_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #C-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'CC'
            else if (condition((table_Enr_Method.equals("CC"))))
            {
                decideConditionsMet437++;
                pnd_Cc_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #CC-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((table_Enr_Method.equals("E"))))
            {
                decideConditionsMet437++;
                pnd_E_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #E-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'EE'
            else if (condition((table_Enr_Method.equals("EE"))))
            {
                decideConditionsMet437++;
                pnd_Ee_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #EE-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((table_Enr_Method.equals("F"))))
            {
                decideConditionsMet437++;
                pnd_F_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #F-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((table_Enr_Method.equals("G"))))
            {
                decideConditionsMet437++;
                pnd_G_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #G-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'H'
            else if (condition((table_Enr_Method.equals("H"))))
            {
                decideConditionsMet437++;
                pnd_H_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #H-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((table_Enr_Method.equals("I"))))
            {
                decideConditionsMet437++;
                pnd_I_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #I-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'II'
            else if (condition((table_Enr_Method.equals("II"))))
            {
                decideConditionsMet437++;
                pnd_Ii_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #II-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'J'
            else if (condition((table_Enr_Method.equals("J"))))
            {
                decideConditionsMet437++;
                pnd_J_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #J-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((table_Enr_Method.equals("L"))))
            {
                decideConditionsMet437++;
                pnd_L_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #L-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((table_Enr_Method.equals("M"))))
            {
                decideConditionsMet437++;
                pnd_M_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #M-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'MM'
            else if (condition((table_Enr_Method.equals("MM"))))
            {
                decideConditionsMet437++;
                pnd_Mm_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #MM-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((table_Enr_Method.equals("N"))))
            {
                decideConditionsMet437++;
                pnd_N_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #N-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'NN'
            else if (condition((table_Enr_Method.equals("NN"))))
            {
                decideConditionsMet437++;
                pnd_Nn_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #NN-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((table_Enr_Method.equals("P"))))
            {
                decideConditionsMet437++;
                pnd_P_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #P-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'PP'
            else if (condition((table_Enr_Method.equals("PP"))))
            {
                decideConditionsMet437++;
                pnd_Pp_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #PP-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((table_Enr_Method.equals("R"))))
            {
                decideConditionsMet437++;
                pnd_R_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #R-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'RR'
            else if (condition((table_Enr_Method.equals("RR"))))
            {
                decideConditionsMet437++;
                pnd_Rr_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #RR-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'U'
            else if (condition((table_Enr_Method.equals("U"))))
            {
                decideConditionsMet437++;
                pnd_U_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #U-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'UU'
            else if (condition((table_Enr_Method.equals("UU"))))
            {
                decideConditionsMet437++;
                pnd_Uu_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #UU-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'X'
            else if (condition((table_Enr_Method.equals("X"))))
            {
                decideConditionsMet437++;
                pnd_X_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #X-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'XX'
            else if (condition((table_Enr_Method.equals("XX"))))
            {
                decideConditionsMet437++;
                pnd_Xx_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #XX-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((table_Enr_Method.equals("T"))))
            {
                decideConditionsMet437++;
                pnd_T_Cnt.setValue(table_Enr_Count);                                                                                                                      //Natural: ASSIGN #T-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: VALUE 'TT'
            else if (condition((table_Enr_Method.equals("TT"))))
            {
                decideConditionsMet437++;
                pnd_Tt_Cnt.setValue(table_Enr_Count);                                                                                                                     //Natural: ASSIGN #TT-CNT := ENR-COUNT
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(0, "OTHER",table_Plan,table_Enr_Method,table_Enr_Count);                                                                               //Natural: WRITE 'OTHER' PLAN ENR-METHOD ENR-COUNT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL
        sub_Write_Detail();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Detail() throws Exception                                                                                                                      //Natural: WRITE-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_A_Rpt.setValueEdited(pnd_A_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #A-CNT ( EM = ZZZZZ9 ) TO #A-RPT
        pnd_Aa_Rpt.setValueEdited(pnd_Aa_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #AA-CNT ( EM = ZZZZZ9 ) TO #AA-RPT
        pnd_B_Rpt.setValueEdited(pnd_B_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #B-CNT ( EM = ZZZZZ9 ) TO #B-RPT
        pnd_Bb_Rpt.setValueEdited(pnd_Bb_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #BB-CNT ( EM = ZZZZZ9 ) TO #BB-RPT
        pnd_C_Rpt.setValueEdited(pnd_C_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #C-CNT ( EM = ZZZZZ9 ) TO #C-RPT
        pnd_Cc_Rpt.setValueEdited(pnd_Cc_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #CC-CNT ( EM = ZZZZZ9 ) TO #CC-RPT
        pnd_E_Rpt.setValueEdited(pnd_E_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #E-CNT ( EM = ZZZZZ9 ) TO #E-RPT
        pnd_Ee_Rpt.setValueEdited(pnd_Ee_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #EE-CNT ( EM = ZZZZZ9 ) TO #EE-RPT
        pnd_F_Rpt.setValueEdited(pnd_F_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #F-CNT ( EM = ZZZZZ9 ) TO #F-RPT
        pnd_G_Rpt.setValueEdited(pnd_G_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #G-CNT ( EM = ZZZZZ9 ) TO #G-RPT
        pnd_H_Rpt.setValueEdited(pnd_H_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #H-CNT ( EM = ZZZZZ9 ) TO #H-RPT
        pnd_I_Rpt.setValueEdited(pnd_I_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #I-CNT ( EM = ZZZZZ9 ) TO #I-RPT
        pnd_Ii_Rpt.setValueEdited(pnd_Ii_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #II-CNT ( EM = ZZZZZ9 ) TO #II-RPT
        pnd_J_Rpt.setValueEdited(pnd_J_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #J-CNT ( EM = ZZZZZ9 ) TO #J-RPT
        pnd_L_Rpt.setValueEdited(pnd_L_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #L-CNT ( EM = ZZZZZ9 ) TO #L-RPT
        //*  EASEENR
        pnd_M_Rpt.setValueEdited(pnd_M_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #M-CNT ( EM = ZZZZZ9 ) TO #M-RPT
        //*  EASEENR
        pnd_Mm_Rpt.setValueEdited(pnd_Mm_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #MM-CNT ( EM = ZZZZZ9 ) TO #MM-RPT
        pnd_N_Rpt.setValueEdited(pnd_N_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #N-CNT ( EM = ZZZZZ9 ) TO #N-RPT
        pnd_Nn_Rpt.setValueEdited(pnd_Nn_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #NN-CNT ( EM = ZZZZZ9 ) TO #NN-RPT
        pnd_P_Rpt.setValueEdited(pnd_P_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #P-CNT ( EM = ZZZZZ9 ) TO #P-RPT
        pnd_Pp_Rpt.setValueEdited(pnd_Pp_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #PP-CNT ( EM = ZZZZZ9 ) TO #PP-RPT
        pnd_R_Rpt.setValueEdited(pnd_R_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #R-CNT ( EM = ZZZZZ9 ) TO #R-RPT
        pnd_Rr_Rpt.setValueEdited(pnd_Rr_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #RR-CNT ( EM = ZZZZZ9 ) TO #RR-RPT
        pnd_U_Rpt.setValueEdited(pnd_U_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #U-CNT ( EM = ZZZZZ9 ) TO #U-RPT
        pnd_Uu_Rpt.setValueEdited(pnd_Uu_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #UU-CNT ( EM = ZZZZZ9 ) TO #UU-RPT
        pnd_X_Rpt.setValueEdited(pnd_X_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #X-CNT ( EM = ZZZZZ9 ) TO #X-RPT
        pnd_Xx_Rpt.setValueEdited(pnd_Xx_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #XX-CNT ( EM = ZZZZZ9 ) TO #XX-RPT
        //*  BE1.
        pnd_T_Rpt.setValueEdited(pnd_T_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                                 //Natural: MOVE EDITED #T-CNT ( EM = ZZZZZ9 ) TO #T-RPT
        //*  BE1.
        pnd_Tt_Rpt.setValueEdited(pnd_Tt_Cnt,new ReportEditMask("ZZZZZ9"));                                                                                               //Natural: MOVE EDITED #TT-CNT ( EM = ZZZZZ9 ) TO #TT-RPT
        //*  BE1.
        //*  BE1.
        //*  EASEENR
        //*  EASEENR
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Rpt_Date,new TabSetting(12),pnd_Save_Plan,new TabSetting(20),pnd_A_Rpt,new TabSetting(28),pnd_Aa_Rpt,new  //Natural: WRITE ( 1 ) NOTITLE 1T #RPT-DATE 12T #SAVE-PLAN 20T #A-RPT 28T #AA-RPT 36T #B-RPT 44T #BB-RPT 52T #C-RPT 60T #CC-RPT 68T #E-RPT 76T #EE-RPT 84T #F-RPT 92T #G-RPT 100T #H-RPT 108T #I-RPT 116T #II-RPT 124T #J-RPT 132T #L-RPT 140T #N-RPT 148T #NN-RPT 156T #P-RPT 164T #PP-RPT 172T #R-RPT 180T #RR-RPT 188T #U-RPT 196T #UU-RPT 204T #X-RPT 212T #XX-RPT 220T #T-RPT 228T #TT-RPT 236T #M-RPT 244T #MM-RPT
            TabSetting(36),pnd_B_Rpt,new TabSetting(44),pnd_Bb_Rpt,new TabSetting(52),pnd_C_Rpt,new TabSetting(60),pnd_Cc_Rpt,new TabSetting(68),pnd_E_Rpt,new 
            TabSetting(76),pnd_Ee_Rpt,new TabSetting(84),pnd_F_Rpt,new TabSetting(92),pnd_G_Rpt,new TabSetting(100),pnd_H_Rpt,new TabSetting(108),pnd_I_Rpt,new 
            TabSetting(116),pnd_Ii_Rpt,new TabSetting(124),pnd_J_Rpt,new TabSetting(132),pnd_L_Rpt,new TabSetting(140),pnd_N_Rpt,new TabSetting(148),pnd_Nn_Rpt,new 
            TabSetting(156),pnd_P_Rpt,new TabSetting(164),pnd_Pp_Rpt,new TabSetting(172),pnd_R_Rpt,new TabSetting(180),pnd_Rr_Rpt,new TabSetting(188),pnd_U_Rpt,new 
            TabSetting(196),pnd_Uu_Rpt,new TabSetting(204),pnd_X_Rpt,new TabSetting(212),pnd_Xx_Rpt,new TabSetting(220),pnd_T_Rpt,new TabSetting(228),pnd_Tt_Rpt,new 
            TabSetting(236),pnd_M_Rpt,new TabSetting(244),pnd_Mm_Rpt);
        if (Global.isEscape()) return;
    }
    private void sub_Update_Or_Add_Table() throws Exception                                                                                                               //Natural: UPDATE-OR-ADD-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            GT:                                                                                                                                                           //Natural: GET TABLE *ISN ( RD. )
            vw_table.readByID(vw_table.getAstISN("RD"), "GT");
            table_Last_Month.setValue(pnd_End_Date);                                                                                                                      //Natural: ASSIGN LAST-MONTH := #END-DATE
            table_Entry_User_Id.setValue(Global.getINIT_ID());                                                                                                            //Natural: ASSIGN ENTRY-USER-ID := *INIT-ID
            vw_table.updateDBRow("GT");                                                                                                                                   //Natural: UPDATE ( GT. )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_table.reset();                                                                                                                                             //Natural: RESET TABLE
            table_Entry_Table_Id_Nbr.setValue(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr);                                                                             //Natural: ASSIGN ENTRY-TABLE-ID-NBR := #ENTRY-TABLE-ID-NBR
            table_Entry_Table_Sub_Id.setValue(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id);                                                                             //Natural: ASSIGN ENTRY-TABLE-SUB-ID := #ENTRY-TABLE-SUB-ID
            table_Last_Month.setValue(pnd_End_Date);                                                                                                                      //Natural: ASSIGN LAST-MONTH := #END-DATE
            table_Entry_Dscrptn_Txt.setValue("ACIS Enrollment Report control record");                                                                                    //Natural: ASSIGN ENTRY-DSCRPTN-TXT := 'ACIS Enrollment Report control record'
            table_Entry_User_Id.setValue(Global.getINIT_ID());                                                                                                            //Natural: ASSIGN ENTRY-USER-ID := *INIT-ID
            vw_table.insertDBRow();                                                                                                                                       //Natural: STORE TABLE
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Plan_Table_Update() throws Exception                                                                                                                 //Natural: PLAN-TABLE-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(900);                                                                                                      //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 000900
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("OE");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'OE'
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.reset();                                                                                                                     //Natural: RESET #ENTRY-CDE
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Plan, pnd_Method));                                             //Natural: COMPRESS #PLAN #METHOD INTO #ENTRY-CDE LEAVING NO
        vw_table.startDatabaseFind                                                                                                                                        //Natural: FIND TABLE WITH TABLE-ID-ENTRY-CDE = #TABLE-ID-ENTRY-CDE
        (
        "FINDTBL",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", "=", pnd_Table_Id_Entry_Cde, WcType.WITH) }
        );
        FINDTBL:
        while (condition(vw_table.readNextRow("FINDTBL", true)))
        {
            vw_table.setIfNotFoundControlFlag(false);
            if (condition(vw_table.getAstCOUNTER().equals(0)))                                                                                                            //Natural: IF NO RECORDS FOUND
            {
                                                                                                                                                                          //Natural: PERFORM STORE-PLAN-TABLE
                sub_Store_Plan_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDTBL"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDTBL"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break FINDTBL;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FINDTBL. )
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM UPDATE-PLAN-TABLE
            sub_Update_Plan_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FINDTBL"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FINDTBL"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Trans_Cnt.equals(100)))                                                                                                                         //Natural: IF #TRANS-CNT = 100
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Trans_Cnt.reset();                                                                                                                                        //Natural: RESET #TRANS-CNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Plan_Table() throws Exception                                                                                                                 //Natural: UPDATE-PLAN-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        GETTBL2:                                                                                                                                                          //Natural: GET UPDATE-TABLE *ISN ( FINDTBL. )
        vw_update_Table.readByID(vw_table.getAstISN("FINDTBL"), "GETTBL2");
        update_Table_Enr_Count.nadd(+1);                                                                                                                                  //Natural: ADD +1 TO UPDATE-TABLE.ENR-COUNT
        vw_update_Table.updateDBRow("GETTBL2");                                                                                                                           //Natural: UPDATE ( GETTBL2. )
        pnd_Trans_Cnt.nadd(+1);                                                                                                                                           //Natural: ADD +1 TO #TRANS-CNT
    }
    private void sub_Store_Plan_Table() throws Exception                                                                                                                  //Natural: STORE-PLAN-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        table_Entry_Table_Id_Nbr.setValue(900);                                                                                                                           //Natural: ASSIGN TABLE.ENTRY-TABLE-ID-NBR := 00900
        table_Entry_Table_Sub_Id.setValue("OE");                                                                                                                          //Natural: ASSIGN TABLE.ENTRY-TABLE-SUB-ID := 'OE'
        table_Entry_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Plan, pnd_Method));                                                                  //Natural: COMPRESS #PLAN #METHOD INTO TABLE.ENTRY-CDE LEAVING NO
        table_Enr_Count.setValue(1);                                                                                                                                      //Natural: ASSIGN TABLE.ENR-COUNT := 1
        vw_table.insertDBRow();                                                                                                                                           //Natural: STORE TABLE
        pnd_Trans_Cnt.nadd(+1);                                                                                                                                           //Natural: ADD +1 TO #TRANS-CNT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=250 PS=0");
    }
}
