/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:52 PM
**        * FROM NATURAL PROGRAM : Appb500
************************************************************
**        * FILE NAME            : Appb500.java
**        * CLASS NAME           : Appb500
**        * INSTANCE NAME        : Appb500
************************************************************
**********************************************************************
* PROGRAM NAME : APPB500                                             *
* DATE WRITTEN : JAN 16, 98                                          *
* DESCRIPTION  : FUNCTION OF THIS PROGRAM IS THE SAME AS APPB500 ,   *
*                WITH ADDITIONAL FIELD TO DENOTE WHICH LETTER TO     *
*                PRODUCE BASED ON PAC-ACCSS-CDE AND # OF CONTRACTS   *
*                IN PRAPS FILE.                                      *
*                WF-LETTER-TYPE = '1'  -  PAC-ACCSS-CDE NOT = ' '    *
*                WF-LETTER-TYPE = '2'  -  PAC-ACCSS-CDE = ' ' AND    *
*                   (CRT-KOUNT > 1 OR                                *
*                   (CRT-KOUNT = 1 AND CRT-NUMBER NE #PREM-TIAA-CONT)*
*                WF-LETTER-TYPE = '3'  -  PAC-ACCSS-CDE = ' '        *
*                   (CRT-KOUNT = 1 AND CRT-NUMBER = #PREM-TIAA-CONT) *
*                                                                    *
*  HISTORY            DESCRIPTION OF CHANGE                          *
*  12/18/01 B.E.      - SEE B.E. OIA CHANGES                         *
*  03/29/04 C.S.      - RESTOW DUE TO CHANGES TO APPL500 FOR SGRD    *
*  02/02/05 MARPURI   - CHANGES FOR TOPS RELEASE 6 REQUIREMENTS - RL6*
*  07/20/05 MARPURI   - REMOVE PAC-CONTROL-FILEE PROCESS             *
*  12/15/05 MARPURI   - RE-STOW ONLY FOR THE DIV/SUB FIELD - RL8     *
*  07/18/13 K. GATES  - REMOVED READS OF LEGACY PREMIUM FILES AND    *
*                       PREMIUM FOUND LOGIC - SEE KG TNGSUB          *
*  09/01/15 L. SHU    - COR NAAD RETIREMENT REMOVE APPL504           *
*                     - CALL TO THE MDM API IS IN APPN211    (CNR)   *
*  06/16/17 BARUA     - PIN EXPANSION CHANGES.  (CHG425939) PINE
**********************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb500 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaAppl500 ldaAppl500;
    private LdaAppl170 ldaAppl170;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Pda_Cor_Batch;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme;
    private DbsField pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind;
    private DbsField pnd_City;
    private DbsField pnd_State;
    private DbsField pnd_Len;

    private DbsGroup pnd_Len__R_Field_1;
    private DbsField pnd_Len_Pnd_Len_7;
    private DbsField pnd_Prem_Found;
    private DbsField pnd_Dob;
    private DbsField pnd_Dob_Date;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Pnd_Wf_Rec_Type;
    private DbsField pnd_Work_File_Pnd_Wf_Pin;
    private DbsField pnd_Work_File_Pnd_Wf_Ssn;
    private DbsField pnd_Work_File_Pnd_Wf_Dob;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File_Pnd_Dob_Mm;
    private DbsField pnd_Work_File_Pnd_Dob_Dd;
    private DbsField pnd_Work_File_Pnd_Dob_Yy;
    private DbsField pnd_Work_File_Pnd_Wf_Prefix;
    private DbsField pnd_Work_File_Pnd_Wf_Last_Name;
    private DbsField pnd_Work_File_Pnd_Wf_First_Name;
    private DbsField pnd_Work_File_Pnd_Wf_Mid_Name;
    private DbsField pnd_Work_File_Pnd_Wf_Suffix;
    private DbsField pnd_Work_File_Pnd_Wf_Tiaa_Nbr;

    private DbsGroup pnd_Work_File__R_Field_3;
    private DbsField pnd_Work_File_Pnd_Wf_Tiaa_Pref;
    private DbsField pnd_Work_File_Pnd_Wf_Tiaa_Cont;
    private DbsField pnd_Work_File_Pnd_Wf_Cref_Nbr;

    private DbsGroup pnd_Work_File__R_Field_4;
    private DbsField pnd_Work_File_Pnd_Wf_Cref_Pref;
    private DbsField pnd_Work_File_Pnd_Wf_Cref_Cont;
    private DbsField pnd_Work_File_Pnd_Wf_Addr_Line;
    private DbsField pnd_Work_File_Pnd_Wf_City;
    private DbsField pnd_Work_File_Pnd_Wf_State;
    private DbsField pnd_Work_File_Pnd_Wf_Mail_Zip;
    private DbsField pnd_Work_File_Pnd_Wf_Allocation;
    private DbsField pnd_Work_File_Pnd_Wf_Fund_Ident;
    private DbsField pnd_Work_File_Pnd_Wf_Letter_Type;
    private DbsField pnd_Work_File_Pnd_Wk_T_Age_1st;
    private DbsField pnd_Work_File_Pnd_Wk_Tiaa_Serv_Agt;
    private DbsField pnd_Work_File_Pnd_Wf_Filler;
    private DbsField pnd_Isn;
    private DbsField pnd_Ap_Csm_Sec_Seg_Blank;
    private DbsField pnd_Name;
    private DbsField pnd_City_St_Zip;
    private DbsField pnd_Ii;
    private DbsField pnd_J;
    private DbsField pnd_N;
    private DbsField pnd_Fr_Nx;
    private DbsField pnd_Lst_Len;
    private DbsField pnd_Frst_Len;
    private DbsField pnd_Mid_Len;
    private DbsField pnd_Begin;
    private DbsField pnd_Begin1;
    private DbsField pnd_Last_Name_1;
    private DbsField pnd_Frst_Name;
    private DbsField pnd_Mid_Name;
    private DbsField pnd_Prem_Not_Found_Count;
    private DbsField pnd_Prem_Found_Count;
    private DbsField pnd_Total_Appl_Process;
    private DbsField pnd_Total_Extrc;
    private DbsField pnd_Total_Prem;
    private DbsField pnd_Total_Appl;
    private DbsField pnd_Total_Both;

    private DbsGroup pnd_Miscellanous_Variables;
    private DbsField pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_5;
    private DbsField pnd_Miscellanous_Variables_Pnd_Ph_Unique_Id_Number;
    private DbsField pnd_Miscellanous_Variables_Pnd_Ph_Rcd_Type_Cde;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_6;
    private DbsField pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype_Binry;
    private DbsField pnd_Miscellanous_Variables_Pnd_Crt_Number;
    private DbsField pnd_Miscellanous_Variables_Pnd_Crt_Kount;
    private DbsField pnd_Miscellanous_Variables_Pnd_Crt_Found;
    private DbsField pnd_Miscellanous_Variables_Pnd_K;
    private DbsField pnd_Miscellanous_Variables_Pnd_Wk_State_Code_N;
    private DbsField pnd_Miscellanous_Variables_Pnd_Wk_State_Code_A;
    private DbsField pnd_Previous_Issued_Not_Printed;
    private DbsField pnd_Previous_Issued_Printed;
    private DbsField pnd_New_Cntrct_Printed;
    private DbsField pnd_New_Issued_Not_Printed;
    private DbsField pnd_Total_Ira_App;
    private DbsField pnd_Type_Printed;
    private DbsField pnd_Date_Matched;

    private DbsGroup pnd_Date_Matched__R_Field_7;
    private DbsField pnd_Date_Matched_Pnd_Cc;
    private DbsField pnd_Date_Matched_Pnd_Yy;
    private DbsField pnd_Date_Matched_Pnd_Mm;
    private DbsField pnd_Date_Matched_Pnd_Dd;
    private DbsField pnd_Dt_Mt;

    private DbsGroup pnd_Dt_Mt__R_Field_8;
    private DbsField pnd_Dt_Mt_Pnd_Mm_Mt;
    private DbsField pnd_Dt_Mt_Pnd_Dd_Mt;
    private DbsField pnd_Dt_Mt_Pnd_Cc_Mt;
    private DbsField pnd_Dt_Mt_Pnd_Yy_Mt;
    private DbsField pnd_Debug;
    private DbsField pnd_Kount;
    private DbsField pnd_Rc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaAppl500 = new LdaAppl500();
        registerRecord(ldaAppl500);
        registerRecord(ldaAppl500.getVw_aap_View());
        ldaAppl170 = new LdaAppl170();
        registerRecord(ldaAppl170);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Pda_Cor_Batch = localVariables.newGroupInRecord("pnd_Pda_Cor_Batch", "#PDA-COR-BATCH");
        pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde", "#PAR-TO-RCD-TYPE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No", "#PAR-TO-SOCIAL-SECURITY-NO", 
            FieldType.NUMERIC, 9);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number", "#PAR-FM-UNIQUE-ID-NUMBER", 
            FieldType.NUMERIC, 12);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme", "#PAR-FM-PRFX-NME", FieldType.STRING, 
            8);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme", "#PAR-FM-LAST-NME", FieldType.STRING, 
            30);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme", "#PAR-FM-FIRST-NME", FieldType.STRING, 
            30);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme", "#PAR-FM-MDDLE-NME", FieldType.STRING, 
            30);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme", "#PAR-FM-SFFX-NME", FieldType.STRING, 
            8);
        pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind = pnd_Pda_Cor_Batch.newFieldInGroup("pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind", "#PAR-FM-HIS-IND", FieldType.STRING, 
            1);
        pnd_City = localVariables.newFieldInRecord("pnd_City", "#CITY", FieldType.STRING, 27);
        pnd_State = localVariables.newFieldInRecord("pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);

        pnd_Len__R_Field_1 = localVariables.newGroupInRecord("pnd_Len__R_Field_1", "REDEFINE", pnd_Len);
        pnd_Len_Pnd_Len_7 = pnd_Len__R_Field_1.newFieldInGroup("pnd_Len_Pnd_Len_7", "#LEN-7", FieldType.NUMERIC, 2);
        pnd_Prem_Found = localVariables.newFieldInRecord("pnd_Prem_Found", "#PREM-FOUND", FieldType.BOOLEAN, 1);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 8);
        pnd_Dob_Date = localVariables.newFieldInRecord("pnd_Dob_Date", "#DOB-DATE", FieldType.STRING, 10);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Pnd_Wf_Rec_Type = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Rec_Type", "#WF-REC-TYPE", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wf_Pin = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Pin", "#WF-PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Wf_Ssn = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Ssn", "#WF-SSN", FieldType.NUMERIC, 9);
        pnd_Work_File_Pnd_Wf_Dob = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Dob", "#WF-DOB", FieldType.STRING, 8);

        pnd_Work_File__R_Field_2 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File_Pnd_Wf_Dob);
        pnd_Work_File_Pnd_Dob_Mm = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Dob_Mm", "#DOB-MM", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Dob_Dd = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Dob_Dd", "#DOB-DD", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Dob_Yy = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Dob_Yy", "#DOB-YY", FieldType.STRING, 4);
        pnd_Work_File_Pnd_Wf_Prefix = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Prefix", "#WF-PREFIX", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Last_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Last_Name", "#WF-LAST-NAME", FieldType.STRING, 30);
        pnd_Work_File_Pnd_Wf_First_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_First_Name", "#WF-FIRST-NAME", FieldType.STRING, 30);
        pnd_Work_File_Pnd_Wf_Mid_Name = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Mid_Name", "#WF-MID-NAME", FieldType.STRING, 30);
        pnd_Work_File_Pnd_Wf_Suffix = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Suffix", "#WF-SUFFIX", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Wf_Tiaa_Nbr = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Tiaa_Nbr", "#WF-TIAA-NBR", FieldType.STRING, 10);

        pnd_Work_File__R_Field_3 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_3", "REDEFINE", pnd_Work_File_Pnd_Wf_Tiaa_Nbr);
        pnd_Work_File_Pnd_Wf_Tiaa_Pref = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wf_Tiaa_Pref", "#WF-TIAA-PREF", FieldType.STRING, 
            1);
        pnd_Work_File_Pnd_Wf_Tiaa_Cont = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wf_Tiaa_Cont", "#WF-TIAA-CONT", FieldType.STRING, 
            7);
        pnd_Work_File_Pnd_Wf_Cref_Nbr = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Cref_Nbr", "#WF-CREF-NBR", FieldType.STRING, 10);

        pnd_Work_File__R_Field_4 = pnd_Work_File.newGroupInGroup("pnd_Work_File__R_Field_4", "REDEFINE", pnd_Work_File_Pnd_Wf_Cref_Nbr);
        pnd_Work_File_Pnd_Wf_Cref_Pref = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Wf_Cref_Pref", "#WF-CREF-PREF", FieldType.STRING, 
            1);
        pnd_Work_File_Pnd_Wf_Cref_Cont = pnd_Work_File__R_Field_4.newFieldInGroup("pnd_Work_File_Pnd_Wf_Cref_Cont", "#WF-CREF-CONT", FieldType.STRING, 
            7);
        pnd_Work_File_Pnd_Wf_Addr_Line = pnd_Work_File.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Addr_Line", "#WF-ADDR-LINE", FieldType.STRING, 35, new 
            DbsArrayController(1, 5));
        pnd_Work_File_Pnd_Wf_City = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_City", "#WF-CITY", FieldType.STRING, 27);
        pnd_Work_File_Pnd_Wf_State = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_State", "#WF-STATE", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Wf_Mail_Zip = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Mail_Zip", "#WF-MAIL-ZIP", FieldType.STRING, 5);
        pnd_Work_File_Pnd_Wf_Allocation = pnd_Work_File.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Allocation", "#WF-ALLOCATION", FieldType.STRING, 3, 
            new DbsArrayController(1, 100));
        pnd_Work_File_Pnd_Wf_Fund_Ident = pnd_Work_File.newFieldArrayInGroup("pnd_Work_File_Pnd_Wf_Fund_Ident", "#WF-FUND-IDENT", FieldType.STRING, 10, 
            new DbsArrayController(1, 100));
        pnd_Work_File_Pnd_Wf_Letter_Type = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Letter_Type", "#WF-LETTER-TYPE", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wk_T_Age_1st = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wk_T_Age_1st", "#WK-T-AGE-1ST", FieldType.NUMERIC, 4);
        pnd_Work_File_Pnd_Wk_Tiaa_Serv_Agt = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wk_Tiaa_Serv_Agt", "#WK-TIAA-SERV-AGT", FieldType.STRING, 
            1);
        pnd_Work_File_Pnd_Wf_Filler = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Wf_Filler", "#WF-FILLER", FieldType.STRING, 24);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Ap_Csm_Sec_Seg_Blank = localVariables.newFieldInRecord("pnd_Ap_Csm_Sec_Seg_Blank", "#AP-CSM-SEC-SEG-BLANK", FieldType.BOOLEAN, 1);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 30);
        pnd_City_St_Zip = localVariables.newFieldInRecord("pnd_City_St_Zip", "#CITY-ST-ZIP", FieldType.STRING, 34);
        pnd_Ii = localVariables.newFieldInRecord("pnd_Ii", "#II", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.INTEGER, 2);
        pnd_Fr_Nx = localVariables.newFieldInRecord("pnd_Fr_Nx", "#FR-NX", FieldType.INTEGER, 2);
        pnd_Lst_Len = localVariables.newFieldInRecord("pnd_Lst_Len", "#LST-LEN", FieldType.INTEGER, 2);
        pnd_Frst_Len = localVariables.newFieldInRecord("pnd_Frst_Len", "#FRST-LEN", FieldType.INTEGER, 2);
        pnd_Mid_Len = localVariables.newFieldInRecord("pnd_Mid_Len", "#MID-LEN", FieldType.INTEGER, 2);
        pnd_Begin = localVariables.newFieldInRecord("pnd_Begin", "#BEGIN", FieldType.INTEGER, 2);
        pnd_Begin1 = localVariables.newFieldInRecord("pnd_Begin1", "#BEGIN1", FieldType.INTEGER, 2);
        pnd_Last_Name_1 = localVariables.newFieldInRecord("pnd_Last_Name_1", "#LAST-NAME-1", FieldType.STRING, 30);
        pnd_Frst_Name = localVariables.newFieldInRecord("pnd_Frst_Name", "#FRST-NAME", FieldType.STRING, 30);
        pnd_Mid_Name = localVariables.newFieldInRecord("pnd_Mid_Name", "#MID-NAME", FieldType.STRING, 30);
        pnd_Prem_Not_Found_Count = localVariables.newFieldInRecord("pnd_Prem_Not_Found_Count", "#PREM-NOT-FOUND-COUNT", FieldType.NUMERIC, 6);
        pnd_Prem_Found_Count = localVariables.newFieldInRecord("pnd_Prem_Found_Count", "#PREM-FOUND-COUNT", FieldType.NUMERIC, 6);
        pnd_Total_Appl_Process = localVariables.newFieldInRecord("pnd_Total_Appl_Process", "#TOTAL-APPL-PROCESS", FieldType.NUMERIC, 6);
        pnd_Total_Extrc = localVariables.newFieldInRecord("pnd_Total_Extrc", "#TOTAL-EXTRC", FieldType.NUMERIC, 6);
        pnd_Total_Prem = localVariables.newFieldInRecord("pnd_Total_Prem", "#TOTAL-PREM", FieldType.NUMERIC, 6);
        pnd_Total_Appl = localVariables.newFieldInRecord("pnd_Total_Appl", "#TOTAL-APPL", FieldType.NUMERIC, 6);
        pnd_Total_Both = localVariables.newFieldInRecord("pnd_Total_Both", "#TOTAL-BOTH", FieldType.NUMERIC, 6);

        pnd_Miscellanous_Variables = localVariables.newGroupInRecord("pnd_Miscellanous_Variables", "#MISCELLANOUS-VARIABLES");
        pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype", 
            "#COR-SUPER-PIN-RCDTYPE", FieldType.NUMERIC, 14);

        pnd_Miscellanous_Variables__R_Field_5 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_5", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype);
        pnd_Miscellanous_Variables_Pnd_Ph_Unique_Id_Number = pnd_Miscellanous_Variables__R_Field_5.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Ph_Unique_Id_Number", 
            "#PH-UNIQUE-ID-NUMBER", FieldType.NUMERIC, 12);
        pnd_Miscellanous_Variables_Pnd_Ph_Rcd_Type_Cde = pnd_Miscellanous_Variables__R_Field_5.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Ph_Rcd_Type_Cde", 
            "#PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);

        pnd_Miscellanous_Variables__R_Field_6 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_6", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype);
        pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype_Binry = pnd_Miscellanous_Variables__R_Field_6.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Cor_Super_Pin_Rcdtype_Binry", 
            "#COR-SUPER-PIN-RCDTYPE-BINRY", FieldType.BINARY, 14);
        pnd_Miscellanous_Variables_Pnd_Crt_Number = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Crt_Number", "#CRT-NUMBER", 
            FieldType.STRING, 10);
        pnd_Miscellanous_Variables_Pnd_Crt_Kount = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Crt_Kount", "#CRT-KOUNT", 
            FieldType.NUMERIC, 3);
        pnd_Miscellanous_Variables_Pnd_Crt_Found = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Crt_Found", "#CRT-FOUND", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_K = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Miscellanous_Variables_Pnd_Wk_State_Code_N = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Wk_State_Code_N", 
            "#WK-STATE-CODE-N", FieldType.STRING, 2);
        pnd_Miscellanous_Variables_Pnd_Wk_State_Code_A = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Wk_State_Code_A", 
            "#WK-STATE-CODE-A", FieldType.STRING, 2);
        pnd_Previous_Issued_Not_Printed = localVariables.newFieldInRecord("pnd_Previous_Issued_Not_Printed", "#PREVIOUS-ISSUED-NOT-PRINTED", FieldType.NUMERIC, 
            9);
        pnd_Previous_Issued_Printed = localVariables.newFieldInRecord("pnd_Previous_Issued_Printed", "#PREVIOUS-ISSUED-PRINTED", FieldType.NUMERIC, 9);
        pnd_New_Cntrct_Printed = localVariables.newFieldInRecord("pnd_New_Cntrct_Printed", "#NEW-CNTRCT-PRINTED", FieldType.NUMERIC, 9);
        pnd_New_Issued_Not_Printed = localVariables.newFieldInRecord("pnd_New_Issued_Not_Printed", "#NEW-ISSUED-NOT-PRINTED", FieldType.NUMERIC, 9);
        pnd_Total_Ira_App = localVariables.newFieldInRecord("pnd_Total_Ira_App", "#TOTAL-IRA-APP", FieldType.NUMERIC, 9);
        pnd_Type_Printed = localVariables.newFieldInRecord("pnd_Type_Printed", "#TYPE-PRINTED", FieldType.STRING, 15);
        pnd_Date_Matched = localVariables.newFieldInRecord("pnd_Date_Matched", "#DATE-MATCHED", FieldType.NUMERIC, 8);

        pnd_Date_Matched__R_Field_7 = localVariables.newGroupInRecord("pnd_Date_Matched__R_Field_7", "REDEFINE", pnd_Date_Matched);
        pnd_Date_Matched_Pnd_Cc = pnd_Date_Matched__R_Field_7.newFieldInGroup("pnd_Date_Matched_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Date_Matched_Pnd_Yy = pnd_Date_Matched__R_Field_7.newFieldInGroup("pnd_Date_Matched_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Date_Matched_Pnd_Mm = pnd_Date_Matched__R_Field_7.newFieldInGroup("pnd_Date_Matched_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Date_Matched_Pnd_Dd = pnd_Date_Matched__R_Field_7.newFieldInGroup("pnd_Date_Matched_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Dt_Mt = localVariables.newFieldInRecord("pnd_Dt_Mt", "#DT-MT", FieldType.NUMERIC, 8);

        pnd_Dt_Mt__R_Field_8 = localVariables.newGroupInRecord("pnd_Dt_Mt__R_Field_8", "REDEFINE", pnd_Dt_Mt);
        pnd_Dt_Mt_Pnd_Mm_Mt = pnd_Dt_Mt__R_Field_8.newFieldInGroup("pnd_Dt_Mt_Pnd_Mm_Mt", "#MM-MT", FieldType.NUMERIC, 2);
        pnd_Dt_Mt_Pnd_Dd_Mt = pnd_Dt_Mt__R_Field_8.newFieldInGroup("pnd_Dt_Mt_Pnd_Dd_Mt", "#DD-MT", FieldType.NUMERIC, 2);
        pnd_Dt_Mt_Pnd_Cc_Mt = pnd_Dt_Mt__R_Field_8.newFieldInGroup("pnd_Dt_Mt_Pnd_Cc_Mt", "#CC-MT", FieldType.NUMERIC, 2);
        pnd_Dt_Mt_Pnd_Yy_Mt = pnd_Dt_Mt__R_Field_8.newFieldInGroup("pnd_Dt_Mt_Pnd_Yy_Mt", "#YY-MT", FieldType.NUMERIC, 2);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Kount = localVariables.newFieldInRecord("pnd_Kount", "#KOUNT", FieldType.INTEGER, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl500.initializeValues();
        ldaAppl170.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb500() throws Exception
    {
        super("Appb500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132;//Natural: FORMAT ( 3 ) PS = 60 LS = 132
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE 20T '"ORP" Detail Extracted Report' 80T 'Run Date:' *DATX / 80T 'Run Time:' *TIMX ///
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE 20T 'New "ORP" Participant Process Report' 80T 'Run Date:' *DATX / 80T 'Run Time:' *TIMX ///
        getReports().write(3, " ");                                                                                                                                       //Natural: WRITE ( 3 ) ' '
        if (Global.isEscape()) return;
        getReports().write(3, " ");                                                                                                                                       //Natural: WRITE ( 3 ) ' '
        if (Global.isEscape()) return;
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE 20T 'Internal Ira Report' 80T 'Run Date:' *DATX / 80T 'Run Time:' *TIMX ///
        //*  CNR
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        ldaAppl500.getVw_aap_View().startDatabaseRead                                                                                                                     //Natural: READ AAP-VIEW BY AP-RELEASE-IND = 3 THRU 5 WHERE AP-RECORD-TYPE = 1
        (
        "AAP",
        new Wc[] { new Wc("AP_RECORD_TYPE", "=", 1, WcType.WHERE) ,
        new Wc("AP_RELEASE_IND", ">=", 3, "And", WcType.BY) ,
        new Wc("AP_RELEASE_IND", "<=", 5, WcType.BY) },
        new Oc[] { new Oc("AP_RELEASE_IND", "ASC") }
        );
        AAP:
        while (condition(ldaAppl500.getVw_aap_View().readNextRow("AAP")))
        {
            pnd_Type_Printed.reset();                                                                                                                                     //Natural: RESET #TYPE-PRINTED
            if (condition(ldaAppl500.getAap_View_Ap_Release_Ind().equals(6)))                                                                                             //Natural: REJECT IF AP-RELEASE-IND = 6
            {
                continue;
            }
            pnd_Prem_Found.reset();                                                                                                                                       //Natural: RESET #PREM-FOUND
            if (condition((ldaAppl500.getAap_View_Ap_Release_Ind().equals(3) && ldaAppl500.getAap_View_Ap_Ira_Record_Type().equals("I") && ldaAppl500.getAap_View_Ap_Print_Date().greater(getZero())))) //Natural: IF ( AP-RELEASE-IND = 3 AND AP-IRA-RECORD-TYPE = 'I' AND AP-PRINT-DATE > 0 )
            {
                pnd_Type_Printed.setValue("NEW/ISSUE PKG");                                                                                                               //Natural: ASSIGN #TYPE-PRINTED := 'NEW/ISSUE PKG'
                                                                                                                                                                          //Natural: PERFORM WRITE-NEW-IRA-RPT
                sub_Write_New_Ira_Rpt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("AAP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("AAP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_New_Cntrct_Printed.nadd(1);                                                                                                                           //Natural: ADD 1 TO #NEW-CNTRCT-PRINTED
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaAppl500.getAap_View_Ap_Release_Ind().equals(5) && ldaAppl500.getAap_View_Ap_Ira_Record_Type().equals("I") && ldaAppl500.getAap_View_Ap_Print_Date().equals(getZero())))) //Natural: IF ( AP-RELEASE-IND = 5 AND AP-IRA-RECORD-TYPE = 'I' AND AP-PRINT-DATE = 0 )
            {
                pnd_Previous_Issued_Not_Printed.nadd(1);                                                                                                                  //Natural: ADD 1 TO #PREVIOUS-ISSUED-NOT-PRINTED
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaAppl500.getAap_View_Ap_Release_Ind().equals(3) && ldaAppl500.getAap_View_Ap_Ira_Record_Type().equals("I") && ldaAppl500.getAap_View_Ap_Print_Date().equals(getZero())))) //Natural: IF ( AP-RELEASE-IND = 3 AND AP-IRA-RECORD-TYPE = 'I' AND AP-PRINT-DATE = 0 )
            {
                pnd_Type_Printed.setValue("NEW/ISSUE NO PKG");                                                                                                            //Natural: ASSIGN #TYPE-PRINTED := 'NEW/ISSUE NO PKG'
                                                                                                                                                                          //Natural: PERFORM WRITE-NEW-IRA-RPT
                sub_Write_New_Ira_Rpt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("AAP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("AAP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  HK INTERNAL IRA
                pnd_New_Issued_Not_Printed.nadd(1);                                                                                                                       //Natural: ADD 1 TO #NEW-ISSUED-NOT-PRINTED
                //*  DON'T PRINT CONTRACT NO
                                                                                                                                                                          //Natural: PERFORM INTERNAL-IRA
                sub_Internal_Ira();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("AAP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("AAP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FINANCIAL DATA
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaAppl500.getAap_View_Ap_Release_Ind().equals(5) && ldaAppl500.getAap_View_Ap_Ira_Record_Type().equals("I") && ldaAppl500.getAap_View_Ap_Print_Date().greater(getZero())))) //Natural: IF ( AP-RELEASE-IND = 5 AND AP-IRA-RECORD-TYPE = 'I' AND AP-PRINT-DATE > 0 )
            {
                //*  HK INTERNAL IRA
                pnd_Previous_Issued_Printed.nadd(1);                                                                                                                      //Natural: ADD 1 TO #PREVIOUS-ISSUED-PRINTED
                pnd_Type_Printed.setValue("PREV/ISSUE PKG");                                                                                                              //Natural: ASSIGN #TYPE-PRINTED := 'PREV/ISSUE PKG'
                                                                                                                                                                          //Natural: PERFORM WRITE-NEW-IRA-RPT
                sub_Write_New_Ira_Rpt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("AAP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("AAP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PRINT CONTRACT WITH FINANCIAL DATA
                                                                                                                                                                          //Natural: PERFORM INTERNAL-IRA-2
                sub_Internal_Ira_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("AAP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("AAP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl500.getAap_View_Ap_Ira_Record_Type().equals("I")))                                                                                       //Natural: IF AP-IRA-RECORD-TYPE = 'I'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaAppl500.getAap_View_Ap_Release_Ind().equals(3) && ldaAppl500.getAap_View_Ap_Csm_Sec_Seg().equals("ORP")) || (ldaAppl500.getAap_View_Ap_Release_Ind().equals(5)))) //Natural: IF ( AP-RELEASE-IND = 3 AND AP-CSM-SEC-SEG = 'ORP' ) OR ( AP-RELEASE-IND = 5 )
            {
                pnd_Prem_Found.setValue(false);                                                                                                                           //Natural: ASSIGN #PREM-FOUND := FALSE
                //*  KG TNGSUB
                if (condition((ldaAppl500.getAap_View_Ap_Release_Ind().equals(3) && ldaAppl500.getAap_View_Ap_Csm_Sec_Seg().equals("ORP"))))                              //Natural: IF ( AP-RELEASE-IND = 3 AND AP-CSM-SEC-SEG = 'ORP' )
                {
                    GET:                                                                                                                                                  //Natural: GET AAP-VIEW *ISN ( AAP. )
                    ldaAppl500.getVw_aap_View().readByID(ldaAppl500.getVw_aap_View().getAstISN("AAP"), "GET");
                    pnd_Ap_Csm_Sec_Seg_Blank.setValue(false);                                                                                                             //Natural: ASSIGN #AP-CSM-SEC-SEG-BLANK := FALSE
                    pnd_Work_File.reset();                                                                                                                                //Natural: RESET #WORK-FILE #NAME #DOB-DATE
                    pnd_Name.reset();
                    pnd_Dob_Date.reset();
                    pnd_Pda_Cor_Batch.reset();                                                                                                                            //Natural: RESET #PDA-COR-BATCH
                    pnd_Pda_Cor_Batch_Pnd_Par_To_Rcd_Type_Cde.setValue(ldaAppl500.getAap_View_Ap_Record_Type());                                                          //Natural: ASSIGN #PAR-TO-RCD-TYPE-CDE := AP-RECORD-TYPE
                    pnd_Pda_Cor_Batch_Pnd_Par_To_Social_Security_No.setValue(ldaAppl500.getAap_View_Ap_Soc_Sec());                                                        //Natural: ASSIGN #PAR-TO-SOCIAL-SECURITY-NO := AP-SOC-SEC
                    pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number.setValue(ldaAppl500.getAap_View_Ap_Pin_Nbr());                                                          //Natural: ASSIGN #PAR-FM-UNIQUE-ID-NUMBER := AP-PIN-NBR
                    pnd_Pda_Cor_Batch_Pnd_Par_Fm_Prfx_Nme.setValue(ldaAppl500.getAap_View_Ap_Cor_Prfx_Nme());                                                             //Natural: ASSIGN #PAR-FM-PRFX-NME := AP-COR-PRFX-NME
                    pnd_Pda_Cor_Batch_Pnd_Par_Fm_Last_Nme.setValue(ldaAppl500.getAap_View_Ap_Cor_Last_Nme());                                                             //Natural: ASSIGN #PAR-FM-LAST-NME := AP-COR-LAST-NME
                    pnd_Pda_Cor_Batch_Pnd_Par_Fm_First_Nme.setValue(ldaAppl500.getAap_View_Ap_Cor_First_Nme());                                                           //Natural: ASSIGN #PAR-FM-FIRST-NME := AP-COR-FIRST-NME
                    pnd_Pda_Cor_Batch_Pnd_Par_Fm_Mddle_Nme.setValue(ldaAppl500.getAap_View_Ap_Cor_Mddle_Nme());                                                           //Natural: ASSIGN #PAR-FM-MDDLE-NME := AP-COR-MDDLE-NME
                    pnd_Pda_Cor_Batch_Pnd_Par_Fm_Sffx_Nme.setValue(ldaAppl500.getAap_View_Ap_Cor_Sffx_Nme());                                                             //Natural: ASSIGN #PAR-FM-SFFX-NME := AP-COR-SFFX-NME
                    pnd_Pda_Cor_Batch_Pnd_Par_Fm_His_Ind.setValue(ldaAppl500.getAap_View_Ap_Ph_Hist_Ind());                                                               //Natural: ASSIGN #PAR-FM-HIS-IND := AP-PH-HIST-IND
                    DbsUtil.callnat(Appn211.class , getCurrentProcessState(), pnd_Pda_Cor_Batch);                                                                         //Natural: CALLNAT 'APPN211' #PDA-COR-BATCH
                    if (condition(Global.isEscape())) return;
                    if (condition(ldaAppl500.getAap_View_Ap_Release_Ind().equals(3) && ldaAppl500.getAap_View_Ap_Csm_Sec_Seg().equals("ORP")))                            //Natural: IF AP-RELEASE-IND = 3 AND AP-CSM-SEC-SEG = 'ORP'
                    {
                        ldaAppl500.getAap_View_Ap_Pin_Nbr().setValue(pnd_Pda_Cor_Batch_Pnd_Par_Fm_Unique_Id_Number);                                                      //Natural: ASSIGN AP-PIN-NBR := #PAR-FM-UNIQUE-ID-NUMBER
                        pnd_Ap_Csm_Sec_Seg_Blank.setValue(true);                                                                                                          //Natural: ASSIGN #AP-CSM-SEC-SEG-BLANK := TRUE
                        pnd_Total_Appl_Process.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-APPL-PROCESS
                                                                                                                                                                          //Natural: PERFORM WRITE-NEW-ORP-RPT
                        sub_Write_New_Orp_Rpt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("AAP"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("AAP"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAppl500.getAap_View_Ap_Release_Ind().equals(3) && ldaAppl500.getAap_View_Ap_Csm_Sec_Seg().equals("ORP") && ! (pnd_Prem_Found.getBoolean()))) //Natural: IF AP-RELEASE-IND = 3 AND AP-CSM-SEC-SEG = 'ORP' AND NOT #PREM-FOUND
                    {
                        pnd_Work_File_Pnd_Wf_Rec_Type.setValue("A");                                                                                                      //Natural: ASSIGN #WF-REC-TYPE := 'A'
                                                                                                                                                                          //Natural: PERFORM WRITE-WORKFILE
                        sub_Write_Workfile();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("AAP"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("AAP"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Prem_Not_Found_Count.nadd(1);                                                                                                                 //Natural: ADD 1 TO #PREM-NOT-FOUND-COUNT
                        pnd_Total_Appl.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-APPL
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ap_Csm_Sec_Seg_Blank.getBoolean()))                                                                                                 //Natural: IF #AP-CSM-SEC-SEG-BLANK
                    {
                        pnd_Ap_Csm_Sec_Seg_Blank.reset();                                                                                                                 //Natural: RESET #AP-CSM-SEC-SEG-BLANK
                        ldaAppl500.getAap_View_Ap_Csm_Sec_Seg().setValue(" ");                                                                                            //Natural: ASSIGN AP-CSM-SEC-SEG := ' '
                        ldaAppl500.getAap_View_Ap_Mail_Instructions().setValue("1");                                                                                      //Natural: ASSIGN AP-MAIL-INSTRUCTIONS := '1'
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAppl500.getVw_aap_View().updateDBRow("GET");                                                                                                       //Natural: UPDATE ( GET. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,new TabSetting(10),"      TOTAL Extracted        : ",pnd_Total_Extrc,NEWLINE,new TabSetting(10),"            Application TOTAL: ",pnd_Total_Appl,NEWLINE,new  //Natural: WRITE ( 1 ) // 10T '      TOTAL Extracted        : ' #TOTAL-EXTRC / 10T '            Application TOTAL: ' #TOTAL-APPL / 10T '                Premium TOTAL: ' #TOTAL-PREM / 10T ' Both Application and Premium: ' #TOTAL-BOTH
            TabSetting(10),"                Premium TOTAL: ",pnd_Total_Prem,NEWLINE,new TabSetting(10)," Both Application and Premium: ",pnd_Total_Both);
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,new TabSetting(10),"TOTAL Application Process: ",pnd_Total_Appl_Process,NEWLINE,new TabSetting(10),"            Premium Found: ",pnd_Prem_Found_Count,NEWLINE,new  //Natural: WRITE ( 2 ) // 10T 'TOTAL Application Process: ' #TOTAL-APPL-PROCESS / 10T '            Premium Found: ' #PREM-FOUND-COUNT / 10T '         Awaiting Premium: ' #PREM-NOT-FOUND-COUNT
            TabSetting(10),"         Awaiting Premium: ",pnd_Prem_Not_Found_Count);
        if (Global.isEscape()) return;
        pnd_Total_Ira_App.compute(new ComputeParameters(false, pnd_Total_Ira_App), pnd_New_Cntrct_Printed.add(pnd_New_Issued_Not_Printed));                               //Natural: ASSIGN #TOTAL-IRA-APP := #NEW-CNTRCT-PRINTED + #NEW-ISSUED-NOT-PRINTED
        getReports().write(3, NEWLINE,NEWLINE,new TabSetting(10),"               New Issued Application:",pnd_Total_Ira_App,NEWLINE,new TabSetting(10),"       New Issued Application Printed:",pnd_New_Cntrct_Printed,NEWLINE,new  //Natural: WRITE ( 3 ) // 10T '               New Issued Application:' #TOTAL-IRA-APP / 10T '       New Issued Application Printed:' #NEW-CNTRCT-PRINTED / 10T '   New Issued Application not Printed:' #NEW-ISSUED-NOT-PRINTED / 10T 'Previously Issued Application Printed:' #PREVIOUS-ISSUED-PRINTED
            TabSetting(10),"   New Issued Application not Printed:",pnd_New_Issued_Not_Printed,NEWLINE,new TabSetting(10),"Previously Issued Application Printed:",
            pnd_Previous_Issued_Printed);
        if (Global.isEscape()) return;
        //*  CLOSE MQ.  CNR
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORKFILE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NEW-ORP-RPT
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NEW-IRA-RPT
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUBSTRING-NAME-30
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-MID-NAME
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-STATE-CODE
        //* ***********************************
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INTERNAL-IRA
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INTERNAL-IRA-2
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
    }
    private void sub_Write_Workfile() throws Exception                                                                                                                    //Natural: WRITE-WORKFILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Work_File_Pnd_Wf_Dob.setValueEdited(ldaAppl500.getAap_View_Ap_Dob(),new ReportEditMask("99999999"));                                                          //Natural: MOVE EDITED AP-DOB ( EM = 99999999 ) TO #WF-DOB
        //*  EAC (RODGER)
        //*  EAC (RODGER EM=9999999 REMOVED)
        pnd_Work_File_Pnd_Wf_Tiaa_Cont.setValue(ldaAppl500.getAap_View_Ap_Tiaa_Cntrct().getSubstring(2,7));                                                               //Natural: MOVE SUBSTRING ( AP-TIAA-CNTRCT,2,7 ) TO #WF-TIAA-CONT
        //*  EAC (RODGER)
        //*  EAC (RODGER EM=9999999 REMOVED)
        pnd_Work_File_Pnd_Wf_Cref_Cont.setValue(ldaAppl500.getAap_View_Ap_Cref_Cert().getSubstring(2,7));                                                                 //Natural: MOVE SUBSTRING ( AP-CREF-CERT,2,7 ) TO #WF-CREF-CONT
        pnd_Work_File_Pnd_Wf_Pin.setValue(ldaAppl500.getAap_View_Ap_Pin_Nbr());                                                                                           //Natural: ASSIGN #WF-PIN := AP-PIN-NBR
        pnd_Work_File_Pnd_Wf_Ssn.setValue(ldaAppl500.getAap_View_Ap_Soc_Sec());                                                                                           //Natural: ASSIGN #WF-SSN := AP-SOC-SEC
        pnd_Work_File_Pnd_Wf_Prefix.setValue(ldaAppl500.getAap_View_Ap_Cor_Prfx_Nme());                                                                                   //Natural: ASSIGN #WF-PREFIX := AP-COR-PRFX-NME
        pnd_Work_File_Pnd_Wf_Last_Name.setValue(ldaAppl500.getAap_View_Ap_Cor_Last_Nme());                                                                                //Natural: ASSIGN #WF-LAST-NAME := AP-COR-LAST-NME
        pnd_Work_File_Pnd_Wf_First_Name.setValue(ldaAppl500.getAap_View_Ap_Cor_First_Nme());                                                                              //Natural: ASSIGN #WF-FIRST-NAME := AP-COR-FIRST-NME
        pnd_Work_File_Pnd_Wf_Mid_Name.setValue(ldaAppl500.getAap_View_Ap_Cor_Mddle_Nme());                                                                                //Natural: ASSIGN #WF-MID-NAME := AP-COR-MDDLE-NME
        pnd_Work_File_Pnd_Wf_Suffix.setValue(ldaAppl500.getAap_View_Ap_Cor_Sffx_Nme());                                                                                   //Natural: ASSIGN #WF-SUFFIX := AP-COR-SFFX-NME
        pnd_Work_File_Pnd_Wf_Addr_Line.getValue("*").setValue(ldaAppl500.getAap_View_Ap_Address_Line().getValue("*"));                                                    //Natural: ASSIGN #WF-ADDR-LINE ( * ) := AP-ADDRESS-LINE ( * )
        pnd_Work_File_Pnd_Wf_Tiaa_Pref.setValue(ldaAppl500.getAap_View_Ap_Tiaa_Cntrct().getSubstring(1,1));                                                               //Natural: ASSIGN #WF-TIAA-PREF := SUBSTRING ( AP-TIAA-CNTRCT,1,1 )
        pnd_Work_File_Pnd_Wf_Cref_Pref.setValue(ldaAppl500.getAap_View_Ap_Cref_Cert().getSubstring(2,7));                                                                 //Natural: ASSIGN #WF-CREF-PREF := SUBSTRING ( AP-CREF-CERT,2,7 )
        pnd_Work_File_Pnd_Wf_City.setValue(ldaAppl500.getAap_View_Ap_City());                                                                                             //Natural: ASSIGN #WF-CITY := AP-CITY
        pnd_Miscellanous_Variables_Pnd_Wk_State_Code_N.setValue(ldaAppl500.getAap_View_Ap_Orig_Issue_State());                                                            //Natural: ASSIGN #WK-STATE-CODE-N := AP-ORIG-ISSUE-STATE
        //*  OIA JR
        //*  OIA JR
        //*   LS
                                                                                                                                                                          //Natural: PERFORM CONVERT-STATE-CODE
        sub_Convert_State_Code();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_File_Pnd_Wf_Mail_Zip.setValue(ldaAppl500.getAap_View_Ap_Mail_Zip());                                                                                     //Natural: ASSIGN #WF-MAIL-ZIP := AP-MAIL-ZIP
        pnd_Work_File_Pnd_Wf_Allocation.getValue("*").setValue(ldaAppl500.getAap_View_Ap_Allocation_Pct().getValue("*"));                                                 //Natural: ASSIGN #WF-ALLOCATION ( * ) := AP-ALLOCATION-PCT ( * )
        pnd_Work_File_Pnd_Wf_Fund_Ident.getValue("*").setValue(ldaAppl500.getAap_View_Ap_Fund_Cde().getValue("*"));                                                       //Natural: ASSIGN #WF-FUND-IDENT ( * ) := AP-FUND-CDE ( * )
        pnd_Work_File_Pnd_Wk_T_Age_1st.setValue(ldaAppl500.getAap_View_Ap_T_Age_1st());                                                                                   //Natural: ASSIGN #WK-T-AGE-1ST := AP-T-AGE-1ST
        pnd_Work_File_Pnd_Wk_Tiaa_Serv_Agt.setValue(ldaAppl500.getAap_View_Ap_Tiaa_Service_Agent());                                                                      //Natural: ASSIGN #WK-TIAA-SERV-AGT := AP-TIAA-SERVICE-AGENT
        pnd_Work_File_Pnd_Wf_Filler.setValue(" ");                                                                                                                        //Natural: ASSIGN #WF-FILLER := ' '
        getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                    //Natural: WRITE WORK FILE 1 #WORK-FILE
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
                                                                                                                                                                          //Natural: PERFORM SUBSTRING-NAME-30
        sub_Substring_Name_30();
        if (condition(Global.isEscape())) {return;}
        pnd_Dob_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_File_Pnd_Dob_Mm, "/", pnd_Work_File_Pnd_Dob_Dd, "/", pnd_Work_File_Pnd_Dob_Yy));   //Natural: COMPRESS #DOB-MM '/' #DOB-DD '/' #DOB-YY INTO #DOB-DATE LEAVING NO
        getReports().display(1, "Type",                                                                                                                                   //Natural: DISPLAY ( 1 ) 'Type' #WF-REC-TYPE 'PIN Nbr ' #WF-PIN 'Soc Sec Nbr' #WF-SSN ( EM = 999-99-9999 ) 'Name' #NAME 'Birth Date' #DOB-DATE 'TIAA Nbr' #WF-TIAA-NBR 'CREF Nbr' #WF-CREF-NBR
        		pnd_Work_File_Pnd_Wf_Rec_Type,"PIN Nbr ",
        		pnd_Work_File_Pnd_Wf_Pin,"Soc Sec Nbr",
        		pnd_Work_File_Pnd_Wf_Ssn, new ReportEditMask ("999-99-9999"),"Name",
        		pnd_Name,"Birth Date",
        		pnd_Dob_Date,"TIAA Nbr",
        		pnd_Work_File_Pnd_Wf_Tiaa_Nbr,"CREF Nbr",
        		pnd_Work_File_Pnd_Wf_Cref_Nbr);
        if (Global.isEscape()) return;
        pnd_Total_Extrc.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #TOTAL-EXTRC
    }
    private void sub_Write_New_Orp_Rpt() throws Exception                                                                                                                 //Natural: WRITE-NEW-ORP-RPT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
                                                                                                                                                                          //Natural: PERFORM SUBSTRING-NAME-30
        sub_Substring_Name_30();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_File_Pnd_Wf_Tiaa_Pref.setValue(ldaAppl500.getAap_View_Ap_Tiaa_Cntrct().getSubstring(1,1));                                                               //Natural: ASSIGN #WF-TIAA-PREF := SUBSTRING ( AP-TIAA-CNTRCT,1,1 )
        pnd_Work_File_Pnd_Wf_Cref_Pref.setValue(ldaAppl500.getAap_View_Ap_Cref_Cert().getSubstring(1,1));                                                                 //Natural: ASSIGN #WF-CREF-PREF := SUBSTRING ( AP-CREF-CERT,1,1 )
        //*  EAC (RODGER)
        //*  EAC (RODGER EM=9999999 REMOVED)
        pnd_Work_File_Pnd_Wf_Tiaa_Cont.setValue(ldaAppl500.getAap_View_Ap_Tiaa_Cntrct().getSubstring(2,7));                                                               //Natural: MOVE SUBSTRING ( AP-TIAA-CNTRCT,2,7 ) TO #WF-TIAA-CONT
        //*  EAC (RODGER EM=9999999 REMOVED)
        pnd_Work_File_Pnd_Wf_Cref_Cont.setValue(ldaAppl500.getAap_View_Ap_Cref_Cert().getSubstring(2,7));                                                                 //Natural: MOVE SUBSTRING ( AP-CREF-CERT,2,7 ) TO #WF-CREF-CONT
        pnd_Work_File_Pnd_Wf_Dob.setValueEdited(ldaAppl500.getAap_View_Ap_Dob(),new ReportEditMask("99999999"));                                                          //Natural: MOVE EDITED AP-DOB ( EM = 99999999 ) TO #WF-DOB
        pnd_Dob_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_File_Pnd_Dob_Mm, "/", pnd_Work_File_Pnd_Dob_Dd, "/", pnd_Work_File_Pnd_Dob_Yy));   //Natural: COMPRESS #DOB-MM '/' #DOB-DD '/' #DOB-YY INTO #DOB-DATE LEAVING NO
        getReports().display(2, "PIN Nbr ",                                                                                                                               //Natural: DISPLAY ( 2 ) 'PIN Nbr ' AP-PIN-NBR 'Soc Sec Nbr' AP-SOC-SEC ( EM = 999-99-9999 ) 'Name' #NAME 'Birth Date' #DOB-DATE 'TIAA Nbr' #WF-TIAA-NBR 'CREF Nbr' #WF-CREF-NBR
        		ldaAppl500.getAap_View_Ap_Pin_Nbr(),"Soc Sec Nbr",
        		ldaAppl500.getAap_View_Ap_Soc_Sec(), new ReportEditMask ("999-99-9999"),"Name",
        		pnd_Name,"Birth Date",
        		pnd_Dob_Date,"TIAA Nbr",
        		pnd_Work_File_Pnd_Wf_Tiaa_Nbr,"CREF Nbr",
        		pnd_Work_File_Pnd_Wf_Cref_Nbr);
        if (Global.isEscape()) return;
    }
    private void sub_Write_New_Ira_Rpt() throws Exception                                                                                                                 //Natural: WRITE-NEW-IRA-RPT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  EAC (RODGER)
        //*  EAC (RODGER)
                                                                                                                                                                          //Natural: PERFORM SUBSTRING-NAME-30
        sub_Substring_Name_30();
        if (condition(Global.isEscape())) {return;}
        pnd_Work_File_Pnd_Wf_Tiaa_Pref.setValue(ldaAppl500.getAap_View_Ap_Tiaa_Cntrct().getSubstring(1,1));                                                               //Natural: ASSIGN #WF-TIAA-PREF := SUBSTRING ( AP-TIAA-CNTRCT,1,1 )
        pnd_Work_File_Pnd_Wf_Cref_Pref.setValue(ldaAppl500.getAap_View_Ap_Cref_Cert().getSubstring(1,1));                                                                 //Natural: ASSIGN #WF-CREF-PREF := SUBSTRING ( AP-CREF-CERT,1,1 )
        //*  EAC (RODGER)
        //*  EAC (RODGER EM=9999999 REMOVED)
        pnd_Work_File_Pnd_Wf_Tiaa_Cont.setValue(ldaAppl500.getAap_View_Ap_Tiaa_Cntrct().getSubstring(2,7));                                                               //Natural: MOVE SUBSTRING ( AP-TIAA-CNTRCT,2,7 ) TO #WF-TIAA-CONT
        //*  EAC (RODGER)
        //*  EAC (RODGER EM=9999999 REMOVED)
        pnd_Work_File_Pnd_Wf_Cref_Cont.setValue(ldaAppl500.getAap_View_Ap_Cref_Cert().getSubstring(2,7));                                                                 //Natural: MOVE SUBSTRING ( AP-CREF-CERT,2,7 ) TO #WF-CREF-CONT
        pnd_Work_File_Pnd_Wf_Dob.setValue(ldaAppl500.getAap_View_Ap_Dob());                                                                                               //Natural: MOVE AP-DOB TO #WF-DOB
        pnd_Dob_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_File_Pnd_Dob_Mm, "/", pnd_Work_File_Pnd_Dob_Dd, "/", pnd_Work_File_Pnd_Dob_Yy));   //Natural: COMPRESS #DOB-MM '/' #DOB-DD '/' #DOB-YY INTO #DOB-DATE LEAVING NO
        getReports().display(3, "        Status       ",                                                                                                                  //Natural: DISPLAY ( 3 ) '        Status       ' #TYPE-PRINTED 'TIAA Nbr' #WF-TIAA-NBR 'CREF Nbr' #WF-CREF-NBR 'PIN Nbr ' AP-PIN-NBR 'Soc Sec Nbr' AP-SOC-SEC ( EM = 999-99-9999 ) 'Name' #NAME 'Birth Date' #DOB-DATE
        		pnd_Type_Printed,"TIAA Nbr",
        		pnd_Work_File_Pnd_Wf_Tiaa_Nbr,"CREF Nbr",
        		pnd_Work_File_Pnd_Wf_Cref_Nbr,"PIN Nbr ",
        		ldaAppl500.getAap_View_Ap_Pin_Nbr(),"Soc Sec Nbr",
        		ldaAppl500.getAap_View_Ap_Soc_Sec(), new ReportEditMask ("999-99-9999"),"Name",
        		pnd_Name,"Birth Date",
        		pnd_Dob_Date);
        if (Global.isEscape()) return;
    }
    private void sub_Substring_Name_30() throws Exception                                                                                                                 //Natural: SUBSTRING-NAME-30
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Begin.reset();                                                                                                                                                //Natural: RESET #BEGIN #BEGIN1 #NAME #FR-NX #FRST-NAME #LAST-NAME-1 #MID-NAME
        pnd_Begin1.reset();
        pnd_Name.reset();
        pnd_Fr_Nx.reset();
        pnd_Frst_Name.reset();
        pnd_Last_Name_1.reset();
        pnd_Mid_Name.reset();
        DbsUtil.examine(new ExamineSource(ldaAppl500.getAap_View_Ap_Cor_First_Nme(),1,1), new ExamineSearch(ldaAppl500.getAap_View_Ap_Cor_First_Nme()),                   //Natural: EXAMINE SUBSTRING ( AP-COR-FIRST-NME,1 ) FOR AP-COR-FIRST-NME GIVING LENGTH #FRST-LEN
            new ExamineGivingLength(pnd_Frst_Len));
        DbsUtil.examine(new ExamineSource(ldaAppl500.getAap_View_Ap_Cor_Mddle_Nme(),1,1), new ExamineSearch(ldaAppl500.getAap_View_Ap_Cor_Mddle_Nme()),                   //Natural: EXAMINE SUBSTRING ( AP-COR-MDDLE-NME,1 ) FOR AP-COR-MDDLE-NME GIVING LENGTH #MID-LEN
            new ExamineGivingLength(pnd_Mid_Len));
        DbsUtil.examine(new ExamineSource(ldaAppl500.getAap_View_Ap_Cor_Last_Nme(),1,1), new ExamineSearch(ldaAppl500.getAap_View_Ap_Cor_Last_Nme()),                     //Natural: EXAMINE SUBSTRING ( AP-COR-LAST-NME,1 ) FOR AP-COR-LAST-NME GIVING LENGTH #LST-LEN
            new ExamineGivingLength(pnd_Lst_Len));
        pnd_Last_Name_1.setValue(ldaAppl500.getAap_View_Ap_Cor_Last_Nme().getSubstring(1,pnd_Lst_Len.getInt()));                                                          //Natural: MOVE SUBSTRING ( AP-COR-LAST-NME,1,#LST-LEN ) TO #LAST-NAME-1
        pnd_Begin.compute(new ComputeParameters(false, pnd_Begin), pnd_Frst_Len.add(pnd_Lst_Len).add(1));                                                                 //Natural: ASSIGN #BEGIN := #FRST-LEN + #LST-LEN + 1
        if (condition(pnd_Begin.lessOrEqual(30)))                                                                                                                         //Natural: IF #BEGIN LE 30
        {
            pnd_Frst_Name.setValue(ldaAppl500.getAap_View_Ap_Cor_First_Nme().getSubstring(1,pnd_Frst_Len.getInt()));                                                      //Natural: MOVE SUBSTRING ( AP-COR-FIRST-NME,1,#FRST-LEN ) TO #FRST-NAME
            pnd_Fr_Nx.setValue(pnd_Frst_Len);                                                                                                                             //Natural: ASSIGN #FR-NX := #FRST-LEN
                                                                                                                                                                          //Natural: PERFORM LOAD-MID-NAME
            sub_Load_Mid_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR:                                                                                                                                                          //Natural: FOR #N 1 30
            for (pnd_N.setValue(1); condition(pnd_N.lessOrEqual(30)); pnd_N.nadd(1))
            {
                if (condition(((pnd_N.add(pnd_Lst_Len).add(1)).greater(30)) || ldaAppl500.getAap_View_Ap_Cor_First_Nme().getSubstring(pnd_N.getInt(),1).equals(" ")))     //Natural: IF ( ( #N + #LST-LEN + 1 ) GT 30 ) OR SUBSTRING ( AP-COR-FIRST-NME,#N,1 ) = ' '
                {
                    if (true) break FOR;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FOR. )
                }                                                                                                                                                         //Natural: END-IF
                setValueToSubstring(ldaAppl500.getAap_View_Ap_Cor_First_Nme().getSubstring(pnd_N.getInt(),1),pnd_Frst_Name,pnd_N.getInt());                               //Natural: MOVE SUBSTRING ( AP-COR-FIRST-NME,#N,1 ) TO SUBSTRING ( #FRST-NAME,#N )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name_1));                                                                                     //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME-1 INTO #NAME LEAVING
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Mid_Name() throws Exception                                                                                                                     //Natural: LOAD-MID-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(ldaAppl500.getAap_View_Ap_Cor_Mddle_Nme().equals(" ")))                                                                                             //Natural: IF AP-COR-MDDLE-NME = ' '
        {
            pnd_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name_1));                                                                                     //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME-1 INTO #NAME LEAVING
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Begin1.compute(new ComputeParameters(false, pnd_Begin1), pnd_Fr_Nx.add(pnd_Lst_Len).add(pnd_Mid_Len).add(2));                                                 //Natural: ASSIGN #BEGIN1 := #FR-NX + #LST-LEN + #MID-LEN + 2
        if (condition(pnd_Begin1.lessOrEqual(30)))                                                                                                                        //Natural: IF #BEGIN1 LE 30
        {
            pnd_Mid_Name.setValue(ldaAppl500.getAap_View_Ap_Cor_Mddle_Nme().getSubstring(1,pnd_Mid_Len.getInt()));                                                        //Natural: MOVE SUBSTRING ( AP-COR-MDDLE-NME,1,#MID-LEN ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Mid_Name.setValue(ldaAppl500.getAap_View_Ap_Cor_Mddle_Nme().getSubstring(1,1));                                                                           //Natural: MOVE SUBSTRING ( AP-COR-MDDLE-NME,1,1 ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Mid_Name, " ", pnd_Last_Name_1));                                                                      //Natural: COMPRESS #FRST-NAME ' ' #MID-NAME ' ' #LAST-NAME-1 INTO #NAME LEAVING
    }
    private void sub_Convert_State_Code() throws Exception                                                                                                                //Natural: CONVERT-STATE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #K = 1 TO 62
        for (pnd_Miscellanous_Variables_Pnd_K.setValue(1); condition(pnd_Miscellanous_Variables_Pnd_K.lessOrEqual(62)); pnd_Miscellanous_Variables_Pnd_K.nadd(1))
        {
            if (condition(pnd_Miscellanous_Variables_Pnd_Wk_State_Code_N.equals(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_Miscellanous_Variables_Pnd_K)))) //Natural: IF #WK-STATE-CODE-N = #STATE-CODE-N ( #K )
            {
                pnd_Work_File_Pnd_Wf_State.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_Miscellanous_Variables_Pnd_K));                    //Natural: ASSIGN #WF-STATE = #STATE-CODE-A ( #K )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Internal_Ira() throws Exception                                                                                                                      //Natural: INTERNAL-IRA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        GET2:                                                                                                                                                             //Natural: GET AAP-VIEW *ISN ( AAP. )
        ldaAppl500.getVw_aap_View().readByID(ldaAppl500.getVw_aap_View().getAstISN("AAP"), "GET2");
        ldaAppl500.getAap_View_Ap_Release_Ind().setValue(5);                                                                                                              //Natural: ASSIGN AP-RELEASE-IND := 5
        ldaAppl500.getVw_aap_View().updateDBRow("GET2");                                                                                                                  //Natural: UPDATE ( GET2. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Internal_Ira_2() throws Exception                                                                                                                    //Natural: INTERNAL-IRA-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        GET3:                                                                                                                                                             //Natural: GET AAP-VIEW *ISN ( AAP. )
        ldaAppl500.getVw_aap_View().readByID(ldaAppl500.getVw_aap_View().getAstISN("AAP"), "GET3");
        ldaAppl500.getAap_View_Ap_Release_Ind().setValue(3);                                                                                                              //Natural: ASSIGN AP-RELEASE-IND := 3
        ldaAppl500.getVw_aap_View().updateDBRow("GET3");                                                                                                                  //Natural: UPDATE ( GET3. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    //*  CNR
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        getReports().write(0, "INVOKING OPEN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                     //Natural: WRITE 'INVOKING OPEN :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "FINISHED OPEN:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                      //Natural: WRITE 'FINISHED OPEN:' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #KOUNT = 1 TO 60
        for (pnd_Kount.setValue(1); condition(pnd_Kount.lessOrEqual(60)); pnd_Kount.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_Kount)));        //Natural: COMPRESS #RC ##DATA-RESPONSE ( #KOUNT ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
        Global.format(3, "PS=60 LS=132");

        getReports().write(1, ReportOption.TITLE,new TabSetting(20),"'ORP' Detail Extracted Report",new TabSetting(80),"Run Date:",Global.getDATX(),NEWLINE,new 
            TabSetting(80),"Run Time:",Global.getTIMX(),NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,new TabSetting(20),"New 'ORP' Participant Process Report",new TabSetting(80),"Run Date:",Global.getDATX(),NEWLINE,new 
            TabSetting(80),"Run Time:",Global.getTIMX(),NEWLINE,NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,new TabSetting(20),"Internal Ira Report",new TabSetting(80),"Run Date:",Global.getDATX(),NEWLINE,new 
            TabSetting(80),"Run Time:",Global.getTIMX(),NEWLINE,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "Type",
        		pnd_Work_File_Pnd_Wf_Rec_Type,"PIN Nbr ",
        		pnd_Work_File_Pnd_Wf_Pin,"Soc Sec Nbr",
        		pnd_Work_File_Pnd_Wf_Ssn, new ReportEditMask ("999-99-9999"),"Name",
        		pnd_Name,"Birth Date",
        		pnd_Dob_Date,"TIAA Nbr",
        		pnd_Work_File_Pnd_Wf_Tiaa_Nbr,"CREF Nbr",
        		pnd_Work_File_Pnd_Wf_Cref_Nbr);
        getReports().setDisplayColumns(2, "PIN Nbr ",
        		ldaAppl500.getAap_View_Ap_Pin_Nbr(),"Soc Sec Nbr",
        		ldaAppl500.getAap_View_Ap_Soc_Sec(), new ReportEditMask ("999-99-9999"),"Name",
        		pnd_Name,"Birth Date",
        		pnd_Dob_Date,"TIAA Nbr",
        		pnd_Work_File_Pnd_Wf_Tiaa_Nbr,"CREF Nbr",
        		pnd_Work_File_Pnd_Wf_Cref_Nbr);
        getReports().setDisplayColumns(3, "        Status       ",
        		pnd_Type_Printed,"TIAA Nbr",
        		pnd_Work_File_Pnd_Wf_Tiaa_Nbr,"CREF Nbr",
        		pnd_Work_File_Pnd_Wf_Cref_Nbr,"PIN Nbr ",
        		ldaAppl500.getAap_View_Ap_Pin_Nbr(),"Soc Sec Nbr",
        		ldaAppl500.getAap_View_Ap_Soc_Sec(), new ReportEditMask ("999-99-9999"),"Name",
        		pnd_Name,"Birth Date",
        		pnd_Dob_Date);
    }
}
