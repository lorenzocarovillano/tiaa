/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:12 PM
**        * FROM NATURAL PROGRAM : Acip2081
************************************************************
**        * FILE NAME            : Acip2081.java
**        * CLASS NAME           : Acip2081
**        * INSTANCE NAME        : Acip2081
************************************************************
**----------------------------------------------------------------------
*
* 03/29/00 V.RAQUENO - INCLUDE IRAP & LOANS IN PROCESSING       (VR1)
* 07/03/00 V.RAQUENO - POPULATE #SAME-AS FROM ACIS-BENE-FILE    (VR2)
* 04/05/04 B.ELLO    - ACCOMODATE SUNGARD BENEFICIARY
* 10/22/04 R.WILLIS  - ISN IS NOT FOUND WHEN TIAA CONTRACT IS ' '
*                      BECAUSE IT IS ASSIGNED AFTER THIS STEP. ADDING
*                      CODE TO RETREIVE IT.
* 04/05/05 R.WILLIS  - REL5.
* 07/19/12 B.ELLO    - MITRE CHANGE.  PASS RELATIONSHIP CODE TO THE
*                      BENEFICIARY INTERFACE MODULE (BENN970) WHEN
*                      SPECIAL TEXT FIELD IS SET TO PLAN/PRODUCT
*                      PROVISIONS.     SEE CHG256421
* 07/18/13 C. LAEVEY - DO NOT ADD ANOTHER ENTRY ON THE BENE INTERFACE
*                      FILE FOR 3% IRA CONTRACTS TO USE FOR QDRO
*                      (TNGPROC)
*                    - ADD FIELDS TO CALL OF BENN970A  (NY200)
*                    - CHANGE LDA ACIL3010 TO SCIL3010. BOTH LDAS ARE
*                      THE SAME.  CHANGING THE LDA TO SCIL3010 WILL RE-
*                      RESULT TO FEWER MODULES BEING IMPACTED. (NY200)
* 07/18/13 B. NEWSOM - FIX MAPPING OF FIELDS GOING TO LOAD-BENEA970-PDA
*                                                           (NY200-FIX)
* 03/31/2015 B NEWSOM - PH CAN'T CHG BENE ONLINE DUE TO IRREVOCABLE
*                       DESIGNATION.                         (CHG346370)
* 06/15/17   BARUA    - PIN EXPANSION CHANGES.  CHG425939   PINE
* 04/04/19  B.NEWSOM  BREAKING ACIS DEPENDENCIES OF THE BENE LEGACY
*                     SYSTEM                             (BADOTBLS)
**----------------------------------------------------------------------

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Acip2081 extends BLNatBase
{
    // Data Areas
    private LdaScil3010 ldaScil3010;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup bene_File;

    private DbsGroup bene_File_Bene_File_Data;
    private DbsField bene_File_Bene_Intrfcng_Systm;
    private DbsField bene_File_Bene_Rqstng_System;
    private DbsField bene_File_Bene_Intrfce_Bsnss_Dte;
    private DbsField bene_File_Bene_Intrfce_Mgrtn_Ind;
    private DbsField bene_File_Bene_Nmbr_Of_Benes;
    private DbsField bene_File_Bene_Tiaa_Nbr;
    private DbsField bene_File_Bene_Cref_Nbr;
    private DbsField bene_File_Bene_Pin_Nbr;
    private DbsField bene_File_Bene_Lob;
    private DbsField bene_File_Bene_Lob_Type;
    private DbsField bene_File_Bene_Part_Prfx;
    private DbsField bene_File_Bene_Part_Sffx;
    private DbsField bene_File_Bene_Part_First_Nme;
    private DbsField bene_File_Bene_Part_Middle_Nme;
    private DbsField bene_File_Bene_Part_Last_Nme;
    private DbsField bene_File_Bene_Part_Ssn;
    private DbsField bene_File_Bene_Part_Dob;
    private DbsField bene_File_Bene_Estate;
    private DbsField bene_File_Bene_Trust;
    private DbsField bene_File_Bene_Category;
    private DbsField bene_File_Bene_Effective_Dt;
    private DbsField bene_File_Bene_Mos_Ind;
    private DbsField bene_File_Bene_Mos_Irrvcble_Ind;
    private DbsField bene_File_Bene_Pymnt_Chld_Dcsd_Ind;
    private DbsField bene_File_Bene_Update_Dt;
    private DbsField bene_File_Bene_Update_Time;
    private DbsField bene_File_Bene_Update_By;
    private DbsField bene_File_Bene_Record_Status;
    private DbsField bene_File_Bene_Same_As_Ind;
    private DbsField bene_File_Bene_New_Issuefslash_Chng_Ind;
    private DbsField bene_File_Bene_Contract_Type;
    private DbsField bene_File_Bene_Tiaa_Cref_Ind;
    private DbsField bene_File_Bene_Stat;
    private DbsField bene_File_Bene_Dflt_To_Estate;
    private DbsField bene_File_Bene_More_Than_Five_Benes_Ind;
    private DbsField bene_File_Bene_Illgble_Ind;
    private DbsField bene_File_Bene_Exempt_Spouse_Rights;
    private DbsField bene_File_Bene_Spouse_Waived_Bnfts;
    private DbsField bene_File_Bene_Trust_Data_Fldr;
    private DbsField bene_File_Bene_Addr_Fldr;
    private DbsField bene_File_Bene_Co_Owner_Data_Fldr;
    private DbsField bene_File_Bene_Fldr_Log_Dte_Tme;
    private DbsField bene_File_Bene_Fldr_Min;
    private DbsField bene_File_Bene_Fldr_Srce_Id;
    private DbsField bene_File_Bene_Rqst_Timestamp;

    private DbsGroup bene_File__R_Field_1;
    private DbsField bene_File_Bene_Rqst_Date;
    private DbsField bene_File_Bene_Rqst_Time;
    private DbsField bene_File_Bene_Last_Vrfy_Dte;
    private DbsField bene_File_Bene_Last_Vrfy_Tme;
    private DbsField bene_File_Bene_Last_Vrfy_Userid;
    private DbsField bene_File_Bene_Last_Dsgntn_Srce;
    private DbsField bene_File_Bene_Last_Dsgntn_System;

    private DbsGroup bene_File_Bene_Data;
    private DbsField bene_File_Bene_Type;
    private DbsField bene_File_Bene_Name;
    private DbsField bene_File_Bene_Extended_Name;
    private DbsField bene_File_Bene_Ssn_Cd;
    private DbsField bene_File_Bene_Ssn_Nbr;
    private DbsField bene_File_Bene_Dob;
    private DbsField bene_File_Bene_Dte_Birth_Trust;
    private DbsField bene_File_Bene_Relationship_Free_Txt;
    private DbsField bene_File_Bene_Relationship_Cde;
    private DbsField bene_File_Bene_Prctge_Frctn_Ind;
    private DbsField bene_File_Bene_Irrvcbl_Ind;
    private DbsField bene_File_Bene_Alloc_Pct;
    private DbsField bene_File_Bene_Nmrtr_Nbr;
    private DbsField bene_File_Bene_Dnmntr_Nbr;
    private DbsField bene_File_Bene_Std_Txt_Ind;
    private DbsField bene_File_Bene_Sttlmnt_Rstrctn;

    private DbsGroup bene_File_Bene_Addtl_Info;
    private DbsField bene_File_Bene_Addr1;
    private DbsField bene_File_Bene_Addr2;
    private DbsField bene_File_Bene_Addr3_City;
    private DbsField bene_File_Bene_State;
    private DbsField bene_File_Bene_Zip;
    private DbsField bene_File_Bene_Country;
    private DbsField bene_File_Bene_Phone;
    private DbsField bene_File_Bene_Gender;
    private DbsField bene_File_Bene_Spcl_Txt;
    private DbsField bene_File_Bene_Mdo_Calc_Bene;
    private DbsField bene_File_Bene_Spcl_Dsgn_Txt;
    private DbsField bene_File_Bene_Hold_Cde;
    private DbsField bene_File_Pnd_Table_Rltn;

    private DbsGroup bene_File__R_Field_2;
    private DbsField bene_File_Pnd_Rltn_Cd;
    private DbsField bene_File_Pnd_Rltn_Text;
    private DbsField bene_File_Bene_Total_Contract_Written;
    private DbsField bene_File_Bene_Total_Mos_Written;
    private DbsField bene_File_Bene_Total_Dest_Written;
    private DbsField bene_File_Bene_Return_Code;
    private DbsField pnd_Intrfce_Bsnss_Date;

    private DataAccessProgramView vw_annty_Actvty_Prap;
    private DbsField annty_Actvty_Prap_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_Ap_G_Key;
    private DbsField annty_Actvty_Prap_Ap_G_Ind;
    private DbsField annty_Actvty_Prap_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_Ap_C_Doi;

    private DbsGroup annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_Ap_Lob;
    private DbsField annty_Actvty_Prap_Ap_Lob_Type;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;

    private DbsGroup app_Table_Entry__R_Field_3;
    private DbsField app_Table_Entry_Entry_Rule_Category;
    private DbsField app_Table_Entry_Entry_Rule_Plan;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;
    private DbsField app_Table_Entry_Entry_User_Id;
    private DbsField pnd_Lob;
    private DbsField pnd_Skip;
    private DbsField pnd_I;
    private DbsField pnd_Total_Bene;
    private DbsField pnd_Total;
    private DbsField pnd_Total_Error;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Isn;
    private DbsField pnd_Date;
    private DbsField pnd_Bene_Dob;

    private DbsGroup pnd_Bene_Dob__R_Field_4;
    private DbsField pnd_Bene_Dob_Pnd_Bene_Dob_A;

    private DbsGroup pnd_Bene_Dob__R_Field_5;
    private DbsField pnd_Bene_Dob_Pnd_Bene_Dob_A_Mm;
    private DbsField pnd_Bene_Dob_Pnd_Bene_Dob_A_Dd;
    private DbsField pnd_Bene_Dob_Pnd_Bene_Dob_A_Cc;
    private DbsField pnd_Bene_Dob_Pnd_Bene_Dob_A_Yy;
    private DbsField pnd_Bene_Dob_Yyyymmdd;

    private DbsGroup pnd_Bene_Dob_Yyyymmdd__R_Field_6;
    private DbsField pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yyyymmdd_A;

    private DbsGroup pnd_Bene_Dob_Yyyymmdd__R_Field_7;
    private DbsField pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Cc;
    private DbsField pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yy;
    private DbsField pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Mm;
    private DbsField pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Dd;
    private DbsField pnd_Eff_Dt;

    private DbsGroup pnd_Eff_Dt__R_Field_8;
    private DbsField pnd_Eff_Dt_Pnd_Eff_Dt_A;
    private DbsField pnd_Record_Sts;
    private DbsField pnd_Ira_Sub_Global_Start_Date;
    private DbsField pnd_Plan_Key;

    private DbsGroup pnd_Plan_Key__R_Field_9;

    private DbsGroup pnd_Plan_Key_Pnd_Plan_Key_Structure;
    private DbsField pnd_Plan_Key_Entry_Table_Id_Nbr;
    private DbsField pnd_Plan_Key_Entry_Table_Sub_Id;
    private DbsField pnd_Plan_Key_Entry_Cde;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_10;
    private DbsField pnd_Today_Pnd_Today_Cc;
    private DbsField pnd_Today_Pnd_Today_Yy;
    private DbsField pnd_Today_Pnd_Today_Mm;
    private DbsField pnd_Today_Pnd_Today_Dd;

    private DbsGroup pnd_Today__R_Field_11;
    private DbsField pnd_Today_Pnd_Today_A;
    private DbsField pnd_Todays_Dt;

    private DbsGroup pnd_Todays_Dt__R_Field_12;
    private DbsField pnd_Todays_Dt_Pnd_Todays_Yy;
    private DbsField pnd_Todays_Dt_Pnd_Todays_Mm;
    private DbsField pnd_Todays_Dt_Pnd_Todays_Dd;
    private DbsField pnd_Today_Date;
    private DbsField pnd_Same_As;

    private DbsGroup pnd_Same_As__R_Field_13;
    private DbsField pnd_Same_As_Pnd_Same_As_Txt;
    private DbsField pnd_Same_As_Pnd_Cntrct;
    private DbsField pnd_Same_As_Pnd_Fill;
    private DbsField pnd_Tiaa_Nbr;

    private DbsGroup pnd_Tiaa_Nbr__R_Field_14;
    private DbsField pnd_Tiaa_Nbr_Pnd_Tiaa_Pref;
    private DbsField pnd_Tiaa_Nbr_Pnd_Tiaa_Cont;
    private DbsField pnd_Cref_Nbr;

    private DbsGroup pnd_Cref_Nbr__R_Field_15;
    private DbsField pnd_Cref_Nbr_Pnd_Cref_Pref;
    private DbsField pnd_Cref_Nbr_Pnd_Cref_Cont;
    private DbsField pnd_Part_Name;
    private DbsField pnd_Ppg;
    private DbsField pnd_Excp_Cnt;
    private DbsField pnd_Display_Cntrct;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaScil3010 = new LdaScil3010();
        registerRecord(ldaScil3010);
        registerRecord(ldaScil3010.getVw_acis_Bene_File());

        // Local Variables
        localVariables = new DbsRecord();

        bene_File = localVariables.newGroupInRecord("bene_File", "BENE-FILE");

        bene_File_Bene_File_Data = bene_File.newGroupInGroup("bene_File_Bene_File_Data", "BENE-FILE-DATA");
        bene_File_Bene_Intrfcng_Systm = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfcng_Systm", "BENE-INTRFCNG-SYSTM", FieldType.STRING, 
            8);
        bene_File_Bene_Rqstng_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqstng_System", "BENE-RQSTNG-SYSTEM", FieldType.STRING, 
            10);
        bene_File_Bene_Intrfce_Bsnss_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Bsnss_Dte", "BENE-INTRFCE-BSNSS-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Intrfce_Mgrtn_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Mgrtn_Ind", "BENE-INTRFCE-MGRTN-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Nmbr_Of_Benes = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Nmbr_Of_Benes", "BENE-NMBR-OF-BENES", FieldType.NUMERIC, 
            3);
        bene_File_Bene_Tiaa_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Nbr", "BENE-TIAA-NBR", FieldType.STRING, 10);
        bene_File_Bene_Cref_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Cref_Nbr", "BENE-CREF-NBR", FieldType.STRING, 10);
        bene_File_Bene_Pin_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pin_Nbr", "BENE-PIN-NBR", FieldType.NUMERIC, 12);
        bene_File_Bene_Lob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob", "BENE-LOB", FieldType.STRING, 1);
        bene_File_Bene_Lob_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob_Type", "BENE-LOB-TYPE", FieldType.STRING, 1);
        bene_File_Bene_Part_Prfx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Prfx", "BENE-PART-PRFX", FieldType.STRING, 10);
        bene_File_Bene_Part_Sffx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Sffx", "BENE-PART-SFFX", FieldType.STRING, 10);
        bene_File_Bene_Part_First_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_First_Nme", "BENE-PART-FIRST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Middle_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Middle_Nme", "BENE-PART-MIDDLE-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Last_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Last_Nme", "BENE-PART-LAST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Ssn = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Ssn", "BENE-PART-SSN", FieldType.NUMERIC, 9);
        bene_File_Bene_Part_Dob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Dob", "BENE-PART-DOB", FieldType.NUMERIC, 8);
        bene_File_Bene_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Estate", "BENE-ESTATE", FieldType.STRING, 1);
        bene_File_Bene_Trust = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust", "BENE-TRUST", FieldType.STRING, 1);
        bene_File_Bene_Category = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Category", "BENE-CATEGORY", FieldType.STRING, 1);
        bene_File_Bene_Effective_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Effective_Dt", "BENE-EFFECTIVE-DT", FieldType.STRING, 8);
        bene_File_Bene_Mos_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Ind", "BENE-MOS-IND", FieldType.STRING, 1);
        bene_File_Bene_Mos_Irrvcble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Irrvcble_Ind", "BENE-MOS-IRRVCBLE-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Pymnt_Chld_Dcsd_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pymnt_Chld_Dcsd_Ind", "BENE-PYMNT-CHLD-DCSD-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Update_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Dt", "BENE-UPDATE-DT", FieldType.STRING, 8);
        bene_File_Bene_Update_Time = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Time", "BENE-UPDATE-TIME", FieldType.STRING, 7);
        bene_File_Bene_Update_By = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_By", "BENE-UPDATE-BY", FieldType.STRING, 8);
        bene_File_Bene_Record_Status = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Record_Status", "BENE-RECORD-STATUS", FieldType.STRING, 
            1);
        bene_File_Bene_Same_As_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Same_As_Ind", "BENE-SAME-AS-IND", FieldType.STRING, 1);
        bene_File_Bene_New_Issuefslash_Chng_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_New_Issuefslash_Chng_Ind", "BENE-NEW-ISSUE/CHNG-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Contract_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Contract_Type", "BENE-CONTRACT-TYPE", FieldType.STRING, 
            1);
        bene_File_Bene_Tiaa_Cref_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Cref_Ind", "BENE-TIAA-CREF-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Stat = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Stat", "BENE-STAT", FieldType.STRING, 1);
        bene_File_Bene_Dflt_To_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Dflt_To_Estate", "BENE-DFLT-TO-ESTATE", FieldType.STRING, 
            1);
        bene_File_Bene_More_Than_Five_Benes_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_More_Than_Five_Benes_Ind", "BENE-MORE-THAN-FIVE-BENES-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Illgble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Illgble_Ind", "BENE-ILLGBLE-IND", FieldType.STRING, 1);
        bene_File_Bene_Exempt_Spouse_Rights = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Exempt_Spouse_Rights", "BENE-EXEMPT-SPOUSE-RIGHTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Spouse_Waived_Bnfts = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Spouse_Waived_Bnfts", "BENE-SPOUSE-WAIVED-BNFTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Trust_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust_Data_Fldr", "BENE-TRUST-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Addr_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Addr_Fldr", "BENE-ADDR-FLDR", FieldType.STRING, 1);
        bene_File_Bene_Co_Owner_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Co_Owner_Data_Fldr", "BENE-CO-OWNER-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Fldr_Log_Dte_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Log_Dte_Tme", "BENE-FLDR-LOG-DTE-TME", FieldType.STRING, 
            15);
        bene_File_Bene_Fldr_Min = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Min", "BENE-FLDR-MIN", FieldType.STRING, 11);
        bene_File_Bene_Fldr_Srce_Id = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Srce_Id", "BENE-FLDR-SRCE-ID", FieldType.STRING, 6);
        bene_File_Bene_Rqst_Timestamp = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqst_Timestamp", "BENE-RQST-TIMESTAMP", FieldType.STRING, 
            15);

        bene_File__R_Field_1 = bene_File_Bene_File_Data.newGroupInGroup("bene_File__R_Field_1", "REDEFINE", bene_File_Bene_Rqst_Timestamp);
        bene_File_Bene_Rqst_Date = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Date", "BENE-RQST-DATE", FieldType.STRING, 8);
        bene_File_Bene_Rqst_Time = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Time", "BENE-RQST-TIME", FieldType.STRING, 7);
        bene_File_Bene_Last_Vrfy_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Dte", "BENE-LAST-VRFY-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Vrfy_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Tme", "BENE-LAST-VRFY-TME", FieldType.STRING, 
            7);
        bene_File_Bene_Last_Vrfy_Userid = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Userid", "BENE-LAST-VRFY-USERID", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Dsgntn_Srce = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_Srce", "BENE-LAST-DSGNTN-SRCE", FieldType.STRING, 
            1);
        bene_File_Bene_Last_Dsgntn_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_System", "BENE-LAST-DSGNTN-SYSTEM", FieldType.STRING, 
            1);

        bene_File_Bene_Data = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Data", "BENE-DATA");
        bene_File_Bene_Type = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Name", "BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Extended_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Extended_Name", "BENE-EXTENDED-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 40));
        bene_File_Bene_Ssn_Cd = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Cd", "BENE-SSN-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Ssn_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Nbr", "BENE-SSN-NBR", FieldType.STRING, 9, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dob = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dob", "BENE-DOB", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dte_Birth_Trust = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dte_Birth_Trust", "BENE-DTE-BIRTH-TRUST", FieldType.STRING, 
            8, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Free_Txt = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Free_Txt", "BENE-RELATIONSHIP-FREE-TXT", 
            FieldType.STRING, 15, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Cde = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Cde", "BENE-RELATIONSHIP-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 40));
        bene_File_Bene_Prctge_Frctn_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Prctge_Frctn_Ind", "BENE-PRCTGE-FRCTN-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Irrvcbl_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Irrvcbl_Ind", "BENE-IRRVCBL-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Alloc_Pct = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Alloc_Pct", "BENE-ALLOC-PCT", FieldType.NUMERIC, 5, 2, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Nmrtr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Nmrtr_Nbr", "BENE-NMRTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dnmntr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dnmntr_Nbr", "BENE-DNMNTR-NBR", FieldType.NUMERIC, 3, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Std_Txt_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Std_Txt_Ind", "BENE-STD-TXT-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Sttlmnt_Rstrctn = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Sttlmnt_Rstrctn", "BENE-STTLMNT-RSTRCTN", FieldType.STRING, 
            1, new DbsArrayController(1, 40));

        bene_File_Bene_Addtl_Info = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Addtl_Info", "BENE-ADDTL-INFO");
        bene_File_Bene_Addr1 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr2 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr3_City = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, 
            new DbsArrayController(1, 40));
        bene_File_Bene_State = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_State", "BENE-STATE", FieldType.STRING, 2, new DbsArrayController(1, 
            40));
        bene_File_Bene_Zip = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, new DbsArrayController(1, 
            40));
        bene_File_Bene_Country = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Phone = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, new DbsArrayController(1, 
            40));
        bene_File_Bene_Gender = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Spcl_Txt = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Spcl_Txt", "BENE-SPCL-TXT", FieldType.STRING, 72, new 
            DbsArrayController(1, 40, 1, 3));
        bene_File_Bene_Mdo_Calc_Bene = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Mdo_Calc_Bene", "BENE-MDO-CALC-BENE", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Spcl_Dsgn_Txt = bene_File_Bene_File_Data.newFieldArrayInGroup("bene_File_Bene_Spcl_Dsgn_Txt", "BENE-SPCL-DSGN-TXT", FieldType.STRING, 
            72, new DbsArrayController(1, 60));
        bene_File_Bene_Hold_Cde = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Hold_Cde", "BENE-HOLD-CDE", FieldType.STRING, 8);
        bene_File_Pnd_Table_Rltn = bene_File.newFieldInGroup("bene_File_Pnd_Table_Rltn", "#TABLE-RLTN", FieldType.STRING, 1683);

        bene_File__R_Field_2 = bene_File.newGroupInGroup("bene_File__R_Field_2", "REDEFINE", bene_File_Pnd_Table_Rltn);
        bene_File_Pnd_Rltn_Cd = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Cd", "#RLTN-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            99));
        bene_File_Pnd_Rltn_Text = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Text", "#RLTN-TEXT", FieldType.STRING, 15, new DbsArrayController(1, 
            99));
        bene_File_Bene_Total_Contract_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Contract_Written", "BENE-TOTAL-CONTRACT-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Mos_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Mos_Written", "BENE-TOTAL-MOS-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Dest_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Dest_Written", "BENE-TOTAL-DEST-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Return_Code = bene_File.newFieldInGroup("bene_File_Bene_Return_Code", "BENE-RETURN-CODE", FieldType.STRING, 1);
        pnd_Intrfce_Bsnss_Date = localVariables.newFieldInRecord("pnd_Intrfce_Bsnss_Date", "#INTRFCE-BSNSS-DATE", FieldType.STRING, 8);

        vw_annty_Actvty_Prap = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap", "ANNTY-ACTVTY-PRAP"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", 
            DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_Ap_Coll_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_Ap_G_Key = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_G_KEY");
        annty_Actvty_Prap_Ap_G_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "AP_G_IND");
        annty_Actvty_Prap_Ap_Soc_Sec = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_Ap_T_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_Ap_C_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");

        annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm", "AP-RQST-LOG-DTE-TM", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_Ap_Rqst_Log_Dte_Tm.newFieldInGroup("annty_Actvty_Prap_Ap_Rqst_Log_Dte_Time", "AP-RQST-LOG-DTE-TIME", 
            FieldType.STRING, 15, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_Ap_Record_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_Ap_Cref_Cert = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_Ap_Lob = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB");
        annty_Actvty_Prap_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_Ap_Lob_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        registerRecord(vw_annty_Actvty_Prap);

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        app_Table_Entry__R_Field_3 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_3", "REDEFINE", app_Table_Entry_Entry_Cde);
        app_Table_Entry_Entry_Rule_Category = app_Table_Entry__R_Field_3.newFieldInGroup("app_Table_Entry_Entry_Rule_Category", "ENTRY-RULE-CATEGORY", 
            FieldType.STRING, 14);
        app_Table_Entry_Entry_Rule_Plan = app_Table_Entry__R_Field_3.newFieldInGroup("app_Table_Entry_Entry_Rule_Plan", "ENTRY-RULE-PLAN", FieldType.STRING, 
            6);
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        app_Table_Entry_Entry_User_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        registerRecord(vw_app_Table_Entry);

        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 2);
        pnd_Skip = localVariables.newFieldInRecord("pnd_Skip", "#SKIP", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 9);
        pnd_Total_Bene = localVariables.newFieldInRecord("pnd_Total_Bene", "#TOTAL-BENE", FieldType.NUMERIC, 9);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.NUMERIC, 9);
        pnd_Total_Error = localVariables.newFieldInRecord("pnd_Total_Error", "#TOTAL-ERROR", FieldType.NUMERIC, 9);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.NUMERIC, 9);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.NUMERIC, 10);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Bene_Dob = localVariables.newFieldInRecord("pnd_Bene_Dob", "#BENE-DOB", FieldType.NUMERIC, 8);

        pnd_Bene_Dob__R_Field_4 = localVariables.newGroupInRecord("pnd_Bene_Dob__R_Field_4", "REDEFINE", pnd_Bene_Dob);
        pnd_Bene_Dob_Pnd_Bene_Dob_A = pnd_Bene_Dob__R_Field_4.newFieldInGroup("pnd_Bene_Dob_Pnd_Bene_Dob_A", "#BENE-DOB-A", FieldType.STRING, 8);

        pnd_Bene_Dob__R_Field_5 = pnd_Bene_Dob__R_Field_4.newGroupInGroup("pnd_Bene_Dob__R_Field_5", "REDEFINE", pnd_Bene_Dob_Pnd_Bene_Dob_A);
        pnd_Bene_Dob_Pnd_Bene_Dob_A_Mm = pnd_Bene_Dob__R_Field_5.newFieldInGroup("pnd_Bene_Dob_Pnd_Bene_Dob_A_Mm", "#BENE-DOB-A-MM", FieldType.STRING, 
            2);
        pnd_Bene_Dob_Pnd_Bene_Dob_A_Dd = pnd_Bene_Dob__R_Field_5.newFieldInGroup("pnd_Bene_Dob_Pnd_Bene_Dob_A_Dd", "#BENE-DOB-A-DD", FieldType.STRING, 
            2);
        pnd_Bene_Dob_Pnd_Bene_Dob_A_Cc = pnd_Bene_Dob__R_Field_5.newFieldInGroup("pnd_Bene_Dob_Pnd_Bene_Dob_A_Cc", "#BENE-DOB-A-CC", FieldType.STRING, 
            2);
        pnd_Bene_Dob_Pnd_Bene_Dob_A_Yy = pnd_Bene_Dob__R_Field_5.newFieldInGroup("pnd_Bene_Dob_Pnd_Bene_Dob_A_Yy", "#BENE-DOB-A-YY", FieldType.STRING, 
            2);
        pnd_Bene_Dob_Yyyymmdd = localVariables.newFieldInRecord("pnd_Bene_Dob_Yyyymmdd", "#BENE-DOB-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Bene_Dob_Yyyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Bene_Dob_Yyyymmdd__R_Field_6", "REDEFINE", pnd_Bene_Dob_Yyyymmdd);
        pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yyyymmdd_A = pnd_Bene_Dob_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yyyymmdd_A", 
            "#BENE-DOB-YYYYMMDD-A", FieldType.STRING, 8);

        pnd_Bene_Dob_Yyyymmdd__R_Field_7 = pnd_Bene_Dob_Yyyymmdd__R_Field_6.newGroupInGroup("pnd_Bene_Dob_Yyyymmdd__R_Field_7", "REDEFINE", pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yyyymmdd_A);
        pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Cc = pnd_Bene_Dob_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Cc", "#BENE-DOB-CC", 
            FieldType.STRING, 2);
        pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yy = pnd_Bene_Dob_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yy", "#BENE-DOB-YY", 
            FieldType.STRING, 2);
        pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Mm = pnd_Bene_Dob_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Mm", "#BENE-DOB-MM", 
            FieldType.STRING, 2);
        pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Dd = pnd_Bene_Dob_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Dd", "#BENE-DOB-DD", 
            FieldType.STRING, 2);
        pnd_Eff_Dt = localVariables.newFieldInRecord("pnd_Eff_Dt", "#EFF-DT", FieldType.NUMERIC, 8);

        pnd_Eff_Dt__R_Field_8 = localVariables.newGroupInRecord("pnd_Eff_Dt__R_Field_8", "REDEFINE", pnd_Eff_Dt);
        pnd_Eff_Dt_Pnd_Eff_Dt_A = pnd_Eff_Dt__R_Field_8.newFieldInGroup("pnd_Eff_Dt_Pnd_Eff_Dt_A", "#EFF-DT-A", FieldType.STRING, 8);
        pnd_Record_Sts = localVariables.newFieldInRecord("pnd_Record_Sts", "#RECORD-STS", FieldType.STRING, 1);
        pnd_Ira_Sub_Global_Start_Date = localVariables.newFieldInRecord("pnd_Ira_Sub_Global_Start_Date", "#IRA-SUB-GLOBAL-START-DATE", FieldType.NUMERIC, 
            8);
        pnd_Plan_Key = localVariables.newFieldInRecord("pnd_Plan_Key", "#PLAN-KEY", FieldType.STRING, 28);

        pnd_Plan_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Plan_Key__R_Field_9", "REDEFINE", pnd_Plan_Key);

        pnd_Plan_Key_Pnd_Plan_Key_Structure = pnd_Plan_Key__R_Field_9.newGroupInGroup("pnd_Plan_Key_Pnd_Plan_Key_Structure", "#PLAN-KEY-STRUCTURE");
        pnd_Plan_Key_Entry_Table_Id_Nbr = pnd_Plan_Key_Pnd_Plan_Key_Structure.newFieldInGroup("pnd_Plan_Key_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Plan_Key_Entry_Table_Sub_Id = pnd_Plan_Key_Pnd_Plan_Key_Structure.newFieldInGroup("pnd_Plan_Key_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Plan_Key_Entry_Cde = pnd_Plan_Key_Pnd_Plan_Key_Structure.newFieldInGroup("pnd_Plan_Key_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.NUMERIC, 8);

        pnd_Today__R_Field_10 = localVariables.newGroupInRecord("pnd_Today__R_Field_10", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_Cc = pnd_Today__R_Field_10.newFieldInGroup("pnd_Today_Pnd_Today_Cc", "#TODAY-CC", FieldType.NUMERIC, 2);
        pnd_Today_Pnd_Today_Yy = pnd_Today__R_Field_10.newFieldInGroup("pnd_Today_Pnd_Today_Yy", "#TODAY-YY", FieldType.NUMERIC, 2);
        pnd_Today_Pnd_Today_Mm = pnd_Today__R_Field_10.newFieldInGroup("pnd_Today_Pnd_Today_Mm", "#TODAY-MM", FieldType.NUMERIC, 2);
        pnd_Today_Pnd_Today_Dd = pnd_Today__R_Field_10.newFieldInGroup("pnd_Today_Pnd_Today_Dd", "#TODAY-DD", FieldType.NUMERIC, 2);

        pnd_Today__R_Field_11 = localVariables.newGroupInRecord("pnd_Today__R_Field_11", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_A = pnd_Today__R_Field_11.newFieldInGroup("pnd_Today_Pnd_Today_A", "#TODAY-A", FieldType.STRING, 8);
        pnd_Todays_Dt = localVariables.newFieldInRecord("pnd_Todays_Dt", "#TODAYS-DT", FieldType.NUMERIC, 6);

        pnd_Todays_Dt__R_Field_12 = localVariables.newGroupInRecord("pnd_Todays_Dt__R_Field_12", "REDEFINE", pnd_Todays_Dt);
        pnd_Todays_Dt_Pnd_Todays_Yy = pnd_Todays_Dt__R_Field_12.newFieldInGroup("pnd_Todays_Dt_Pnd_Todays_Yy", "#TODAYS-YY", FieldType.NUMERIC, 2);
        pnd_Todays_Dt_Pnd_Todays_Mm = pnd_Todays_Dt__R_Field_12.newFieldInGroup("pnd_Todays_Dt_Pnd_Todays_Mm", "#TODAYS-MM", FieldType.NUMERIC, 2);
        pnd_Todays_Dt_Pnd_Todays_Dd = pnd_Todays_Dt__R_Field_12.newFieldInGroup("pnd_Todays_Dt_Pnd_Todays_Dd", "#TODAYS-DD", FieldType.NUMERIC, 2);
        pnd_Today_Date = localVariables.newFieldInRecord("pnd_Today_Date", "#TODAY-DATE", FieldType.DATE);
        pnd_Same_As = localVariables.newFieldInRecord("pnd_Same_As", "#SAME-AS", FieldType.STRING, 35);

        pnd_Same_As__R_Field_13 = localVariables.newGroupInRecord("pnd_Same_As__R_Field_13", "REDEFINE", pnd_Same_As);
        pnd_Same_As_Pnd_Same_As_Txt = pnd_Same_As__R_Field_13.newFieldInGroup("pnd_Same_As_Pnd_Same_As_Txt", "#SAME-AS-TXT", FieldType.STRING, 8);
        pnd_Same_As_Pnd_Cntrct = pnd_Same_As__R_Field_13.newFieldInGroup("pnd_Same_As_Pnd_Cntrct", "#CNTRCT", FieldType.STRING, 10);
        pnd_Same_As_Pnd_Fill = pnd_Same_As__R_Field_13.newFieldInGroup("pnd_Same_As_Pnd_Fill", "#FILL", FieldType.STRING, 17);
        pnd_Tiaa_Nbr = localVariables.newFieldInRecord("pnd_Tiaa_Nbr", "#TIAA-NBR", FieldType.STRING, 10);

        pnd_Tiaa_Nbr__R_Field_14 = localVariables.newGroupInRecord("pnd_Tiaa_Nbr__R_Field_14", "REDEFINE", pnd_Tiaa_Nbr);
        pnd_Tiaa_Nbr_Pnd_Tiaa_Pref = pnd_Tiaa_Nbr__R_Field_14.newFieldInGroup("pnd_Tiaa_Nbr_Pnd_Tiaa_Pref", "#TIAA-PREF", FieldType.STRING, 1);
        pnd_Tiaa_Nbr_Pnd_Tiaa_Cont = pnd_Tiaa_Nbr__R_Field_14.newFieldInGroup("pnd_Tiaa_Nbr_Pnd_Tiaa_Cont", "#TIAA-CONT", FieldType.STRING, 7);
        pnd_Cref_Nbr = localVariables.newFieldInRecord("pnd_Cref_Nbr", "#CREF-NBR", FieldType.STRING, 10);

        pnd_Cref_Nbr__R_Field_15 = localVariables.newGroupInRecord("pnd_Cref_Nbr__R_Field_15", "REDEFINE", pnd_Cref_Nbr);
        pnd_Cref_Nbr_Pnd_Cref_Pref = pnd_Cref_Nbr__R_Field_15.newFieldInGroup("pnd_Cref_Nbr_Pnd_Cref_Pref", "#CREF-PREF", FieldType.STRING, 1);
        pnd_Cref_Nbr_Pnd_Cref_Cont = pnd_Cref_Nbr__R_Field_15.newFieldInGroup("pnd_Cref_Nbr_Pnd_Cref_Cont", "#CREF-CONT", FieldType.STRING, 7);
        pnd_Part_Name = localVariables.newFieldInRecord("pnd_Part_Name", "#PART-NAME", FieldType.STRING, 50);
        pnd_Ppg = localVariables.newFieldInRecord("pnd_Ppg", "#PPG", FieldType.STRING, 6);
        pnd_Excp_Cnt = localVariables.newFieldInRecord("pnd_Excp_Cnt", "#EXCP-CNT", FieldType.INTEGER, 4);
        pnd_Display_Cntrct = localVariables.newFieldInRecord("pnd_Display_Cntrct", "#DISPLAY-CNTRCT", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap.reset();
        vw_app_Table_Entry.reset();

        ldaScil3010.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Acip2081() throws Exception
    {
        super("Acip2081");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ACIP2081", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 133 PS = 60;//Natural: FORMAT ( 3 ) LS = 133 PS = 60;//Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 )
                                                                                                                                                                          //Natural: PERFORM GET-TODAYS-BDATE
        sub_Get_Todays_Bdate();
        if (condition(Global.isEscape())) {return;}
        //*  TNGPROC
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-IRASUB-DATE
        sub_Retrieve_Irasub_Date();
        if (condition(Global.isEscape())) {return;}
        ldaScil3010.getVw_acis_Bene_File().startDatabaseRead                                                                                                              //Natural: READ ACIS-BENE-FILE BY BENE-SYNC-IND EQ 'Y'
        (
        "READ1",
        new Wc[] { new Wc("BENE_SYNC_IND", ">=", "Y", WcType.BY) },
        new Oc[] { new Oc("BENE_SYNC_IND", "ASC") }
        );
        READ1:
        while (condition(ldaScil3010.getVw_acis_Bene_File().readNextRow("READ1")))
        {
            if (condition(ldaScil3010.getAcis_Bene_File_Bene_Sync_Ind().notEquals("Y")))                                                                                  //Natural: IF ACIS-BENE-FILE.BENE-SYNC-IND NE 'Y'
            {
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaScil3010.getAcis_Bene_File_Bene_Record_Status().notEquals("I")))                                                                             //Natural: REJECT IF ACIS-BENE-FILE.BENE-RECORD-STATUS NE 'I'
            {
                continue;
            }
            if (condition(ldaScil3010.getAcis_Bene_File_Bene_Tiaa_Nbr().equals(" ")))                                                                                     //Natural: IF ACIS-BENE-FILE.BENE-TIAA-NBR EQ ' '
            {
                getReports().write(0, "INVALID CASE ===>",ldaScil3010.getAcis_Bene_File_Bene_Part_Ssn());                                                                 //Natural: WRITE 'INVALID CASE ===>' ACIS-BENE-FILE.BENE-PART-SSN
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaScil3010.getAcis_Bene_File_Bene_Record_Status().notEquals("P")))                                                                         //Natural: IF ACIS-BENE-FILE.BENE-RECORD-STATUS NE 'P'
                {
                    pnd_Isn.reset();                                                                                                                                      //Natural: RESET #ISN
                    pnd_Isn.setValue(ldaScil3010.getVw_acis_Bene_File().getAstISN("READ1"));                                                                              //Natural: ASSIGN #ISN := *ISN
                    pnd_Excp_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #EXCP-CNT
                    //*  BADOTBLS
                    //*  BADOTBLS
                    getReports().write(0, "EXCEPTION:","TIAA#:",ldaScil3010.getAcis_Bene_File_Bene_Tiaa_Nbr(),"CREF#:",ldaScil3010.getAcis_Bene_File_Bene_Cref_Nbr(),     //Natural: WRITE 'EXCEPTION:' 'TIAA#:' ACIS-BENE-FILE.BENE-TIAA-NBR 'CREF#:' ACIS-BENE-FILE.BENE-CREF-NBR 'COUNT:' #EXCP-CNT
                        "COUNT:",pnd_Excp_Cnt);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM UPDATE-INVALID-CASE
                    sub_Update_Invalid_Case();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            pnd_Isn.reset();                                                                                                                                              //Natural: RESET #ISN #TOTAL-BENE BENE-FILE-DATA
            pnd_Total_Bene.reset();
            bene_File_Bene_File_Data.reset();
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            pnd_Isn.setValue(ldaScil3010.getVw_acis_Bene_File().getAstISN("READ1"));                                                                                      //Natural: ASSIGN #ISN := *ISN
            //*                                            /* COMMENTED - BEGIN VR1
            //*  NOT COMMENTED - VR1
                                                                                                                                                                          //Natural: PERFORM GET-PRAP
            sub_Get_Prap();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                            /* COMMENTED - END VR1
            //*  DO NOT ADD ANOTHER ENTRY ON THE BENE INTERFACE       /*  (TNGPROC)
            //*  FILE FOR 3% IRA CONTRACTS TO USE FOR QDRO
            if (condition(! (pnd_Skip.getBoolean())))                                                                                                                     //Natural: IF NOT #SKIP
            {
                                                                                                                                                                          //Natural: PERFORM LOAD-BENEA970-PDA
                sub_Load_Benea970_Pda();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CALL-BENE
                sub_Call_Bene();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*   (TNGPROC)
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-ACIS-BENE
            sub_Update_Acis_Bene();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "=",bene_File_Bene_Total_Contract_Written);                                                                                                 //Natural: WRITE '=' BENE-TOTAL-CONTRACT-WRITTEN
        if (Global.isEscape()) return;
        getReports().write(0, "=",bene_File_Bene_Total_Mos_Written);                                                                                                      //Natural: WRITE '=' BENE-TOTAL-MOS-WRITTEN
        if (Global.isEscape()) return;
        getReports().write(0, "=",bene_File_Bene_Total_Dest_Written);                                                                                                     //Natural: WRITE '=' BENE-TOTAL-DEST-WRITTEN
        if (Global.isEscape()) return;
        if (condition(bene_File_Bene_Total_Contract_Written.equals(getZero())))                                                                                           //Natural: IF BENE-TOTAL-CONTRACT-WRITTEN = 0
        {
            getWorkFiles().write(10, false, "                                        ");                                                                                  //Natural: WRITE WORK FILE 10 '                                        '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(bene_File_Bene_Total_Mos_Written.equals(getZero())))                                                                                                //Natural: IF BENE-TOTAL-MOS-WRITTEN = 0
        {
            getWorkFiles().write(11, false, "                                        ");                                                                                  //Natural: WRITE WORK FILE 11 '                                        '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(bene_File_Bene_Total_Dest_Written.equals(getZero())))                                                                                               //Natural: IF BENE-TOTAL-DEST-WRITTEN = 0
        {
            getWorkFiles().write(12, false, "                                        ");                                                                                  //Natural: WRITE WORK FILE 12 '                                        '
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");                                                 //Natural: WRITE ( 1 ) // 52T '*** E N D  O F  R E P O R T ***'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");                                                 //Natural: WRITE ( 2 ) // 52T '*** E N D  O F  R E P O R T ***'
        if (Global.isEscape()) return;
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-BENEA970-PDA
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* *---------------------------------
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-BENE
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-ACIS-BENE
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXCEPTION-REPORT
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RECORD-SENT-TO-BENE
        //*  TIAA ISSUE DATE NOT PRESENT
        //*  CREF ISSUE DATE NOT PRESENT
        //*  BOTH ISSUE DATES ARE PRESENT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TODAYS-BDATE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PRAP
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-INVALID-CASE
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-IRASUB-DATE
        //* *======
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    //*  BADOTBLS
    //*  BADOTBLS
    //*  BADOTBLS
    private void sub_Load_Benea970_Pda() throws Exception                                                                                                                 //Natural: LOAD-BENEA970-PDA
    {
        if (BLNatReinput.isReinput()) return;

        bene_File_Bene_Intrfce_Bsnss_Dte.setValue(pnd_Today);                                                                                                             //Natural: ASSIGN BENE-INTRFCE-BSNSS-DTE := #TODAY
        bene_File_Bene_Intrfcng_Systm.setValue("ACIS");                                                                                                                   //Natural: ASSIGN BENE-INTRFCNG-SYSTM := 'ACIS'
        bene_File_Bene_Rqstng_System.setValue(ldaScil3010.getAcis_Bene_File_Bene_Orign_System());                                                                         //Natural: ASSIGN BENE-RQSTNG-SYSTEM := ACIS-BENE-FILE.BENE-ORIGN-SYSTEM
        //*  SGRD B.ELLO
        //*  BADOTBLS
        if (condition(pnd_Ppg.equals("SGRD")))                                                                                                                            //Natural: IF #PPG = 'SGRD'
        {
            bene_File_Bene_Rqstng_System.setValue("SUNGARD");                                                                                                             //Natural: ASSIGN BENE-RQSTNG-SYSTEM := 'SUNGARD'
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        bene_File_Bene_New_Issuefslash_Chng_Ind.setValue("N");                                                                                                            //Natural: ASSIGN BENE-NEW-ISSUE/CHNG-IND := 'N'
        bene_File_Bene_Intrfce_Mgrtn_Ind.setValue("I");                                                                                                                   //Natural: ASSIGN BENE-INTRFCE-MGRTN-IND := 'I'
        bene_File_Bene_Last_Dsgntn_Srce.setValue("I");                                                                                                                    //Natural: ASSIGN BENE-LAST-DSGNTN-SRCE := 'I'
        bene_File_Bene_Last_Dsgntn_System.setValue("A");                                                                                                                  //Natural: ASSIGN BENE-LAST-DSGNTN-SYSTEM := 'A'
        bene_File_Bene_Pin_Nbr.setValue(ldaScil3010.getAcis_Bene_File_Bene_Pin_Nbr());                                                                                    //Natural: ASSIGN BENE-FILE.BENE-PIN-NBR := ACIS-BENE-FILE.BENE-PIN-NBR
        bene_File_Bene_Tiaa_Nbr.setValue(ldaScil3010.getAcis_Bene_File_Bene_Tiaa_Nbr());                                                                                  //Natural: ASSIGN BENE-FILE.BENE-TIAA-NBR := ACIS-BENE-FILE.BENE-TIAA-NBR
        bene_File_Bene_Cref_Nbr.setValue(ldaScil3010.getAcis_Bene_File_Bene_Cref_Nbr());                                                                                  //Natural: ASSIGN BENE-FILE.BENE-CREF-NBR := ACIS-BENE-FILE.BENE-CREF-NBR
        //*  BADOTBLS
        if (condition(ldaScil3010.getAcis_Bene_File_Bene_Tiaa_Cref_Ind().equals("B")))                                                                                    //Natural: IF ACIS-BENE-FILE.BENE-TIAA-CREF-IND = 'B'
        {
            bene_File_Bene_Tiaa_Cref_Ind.setValue(" ");                                                                                                                   //Natural: ASSIGN BENE-FILE.BENE-TIAA-CREF-IND := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bene_File_Bene_Tiaa_Cref_Ind.setValue(ldaScil3010.getAcis_Bene_File_Bene_Tiaa_Cref_Ind());                                                                    //Natural: ASSIGN BENE-FILE.BENE-TIAA-CREF-IND := ACIS-BENE-FILE.BENE-TIAA-CREF-IND
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        bene_File_Bene_Mos_Ind.setValue(ldaScil3010.getAcis_Bene_File_Bene_Mos_Ind());                                                                                    //Natural: ASSIGN BENE-FILE.BENE-MOS-IND := ACIS-BENE-FILE.BENE-MOS-IND
        //*  BADOTBLS
        //*  BADOTBLS
        pnd_Eff_Dt_Pnd_Eff_Dt_A.setValueEdited(ldaScil3010.getAcis_Bene_File_Bene_Effective_Dt(),new ReportEditMask("YYYYMMDD"));                                         //Natural: MOVE EDITED ACIS-BENE-FILE.BENE-EFFECTIVE-DT ( EM = YYYYMMDD ) TO #EFF-DT-A
        bene_File_Bene_Effective_Dt.setValue(pnd_Eff_Dt_Pnd_Eff_Dt_A);                                                                                                    //Natural: ASSIGN BENE-FILE.BENE-EFFECTIVE-DT := #EFF-DT-A
        bene_File_Bene_Contract_Type.setValue("D");                                                                                                                       //Natural: ASSIGN BENE-CONTRACT-TYPE := 'D'
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        if (condition(ldaScil3010.getAcis_Bene_File_Bene_Mos_Ind().equals("Y")))                                                                                          //Natural: IF ACIS-BENE-FILE.BENE-MOS-IND EQ 'Y'
        {
            bene_File_Bene_Mos_Ind.setValue("Y");                                                                                                                         //Natural: ASSIGN BENE-FILE.BENE-MOS-IND := 'Y'
            bene_File_Bene_Spcl_Dsgn_Txt.getValue("*").setValue(ldaScil3010.getAcis_Bene_File_Bene_Spcl_Dsgn_Txt().getValue("*"));                                        //Natural: ASSIGN BENE-FILE.BENE-SPCL-DSGN-TXT ( * ) := ACIS-BENE-FILE.BENE-SPCL-DSGN-TXT ( * )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(ldaScil3010.getAcis_Bene_File_Bene_Category().equals("2")))                                                                                         //Natural: IF ACIS-BENE-FILE.BENE-CATEGORY EQ '2'
        {
            bene_File_Bene_More_Than_Five_Benes_Ind.setValue("Y");                                                                                                        //Natural: ASSIGN BENE-MORE-THAN-FIVE-BENES-IND := 'Y'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(ldaScil3010.getAcis_Bene_File_Bene_Category().equals("3")))                                                                                         //Natural: IF ACIS-BENE-FILE.BENE-CATEGORY EQ '3'
        {
            bene_File_Bene_Illgble_Ind.setValue("Y");                                                                                                                     //Natural: ASSIGN BENE-ILLGBLE-IND := 'Y'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil3010.getAcis_Bene_File_Bene_Category().notEquals("1")))                                                                                      //Natural: IF ACIS-BENE-FILE.BENE-CATEGORY NE '1'
        {
            FOR1:                                                                                                                                                         //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                pnd_Bene_Dob.reset();                                                                                                                                     //Natural: RESET #BENE-DOB #BENE-DOB-YYYYMMDD
                pnd_Bene_Dob_Yyyymmdd.reset();
                if (condition(ldaScil3010.getAcis_Bene_File_Bene_Type().getValue(pnd_I).equals(" ")))                                                                     //Natural: IF ACIS-BENE-FILE.BENE-TYPE ( #I ) EQ ' '
                {
                    if (true) break FOR1;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR1. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Total_Bene.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-BENE
                    pnd_Same_As.setValue(ldaScil3010.getAcis_Bene_File_Bene_Name().getValue(pnd_I));                                                                      //Natural: ASSIGN #SAME-AS := ACIS-BENE-FILE.BENE-NAME ( #I )
                    DbsUtil.examine(new ExamineSource(pnd_Same_As), new ExamineTranslate(TranslateOption.Upper));                                                         //Natural: EXAMINE #SAME-AS TRANSLATE INTO UPPER CASE
                    //*  BADOTBLS
                    if (condition(pnd_Same_As_Pnd_Same_As_Txt.getSubstring(1,7).equals("SAME AS")))                                                                       //Natural: IF SUBSTR ( #SAME-AS-TXT,1,7 ) EQ 'SAME AS'
                    {
                        bene_File_Bene_Same_As_Ind.setValue("Y");                                                                                                         //Natural: ASSIGN BENE-SAME-AS-IND := 'Y'
                        //*  BADOTBLS
                        //*  BADOTBLS
                        //*  BADOTBLS
                        if (condition(bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_I).equals(" ")))                                                                       //Natural: IF BENE-PRCTGE-FRCTN-IND ( #I ) = ' '
                        {
                            bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_I).setValue("P");                                                                                //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( #I ) := 'P'
                            bene_File_Bene_Alloc_Pct.getValue(pnd_I).setValue(100);                                                                                       //Natural: ASSIGN BENE-FILE.BENE-ALLOC-PCT ( #I ) := 100.00
                            //*  BADOTBLS
                        }                                                                                                                                                 //Natural: END-IF
                        //*  BADOTBLS
                        //*  BADOTBLS
                    }                                                                                                                                                     //Natural: END-IF
                    bene_File_Bene_Name.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Name().getValue(pnd_I));                                     //Natural: ASSIGN BENE-FILE.BENE-NAME ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-NAME ( #I )
                    bene_File_Bene_Extended_Name.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Extended_Name().getValue(pnd_I));                   //Natural: ASSIGN BENE-FILE.BENE-EXTENDED-NAME ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-EXTENDED-NAME ( #I )
                    bene_File_Bene_Type.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Type().getValue(pnd_I));                                     //Natural: ASSIGN BENE-FILE.BENE-TYPE ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-TYPE ( #I )
                    pnd_Bene_Dob.setValue(ldaScil3010.getAcis_Bene_File_Bene_Dob().getValue(pnd_I));                                                                      //Natural: ASSIGN #BENE-DOB := ACIS-BENE-FILE.BENE-DOB ( #I )
                    pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Cc.setValue(pnd_Bene_Dob_Pnd_Bene_Dob_A_Cc);                                                                       //Natural: ASSIGN #BENE-DOB-CC := #BENE-DOB-A-CC
                    pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yy.setValue(pnd_Bene_Dob_Pnd_Bene_Dob_A_Yy);                                                                       //Natural: ASSIGN #BENE-DOB-YY := #BENE-DOB-A-YY
                    pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Mm.setValue(pnd_Bene_Dob_Pnd_Bene_Dob_A_Mm);                                                                       //Natural: ASSIGN #BENE-DOB-MM := #BENE-DOB-A-MM
                    pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Dd.setValue(pnd_Bene_Dob_Pnd_Bene_Dob_A_Dd);                                                                       //Natural: ASSIGN #BENE-DOB-DD := #BENE-DOB-A-DD
                    //*  BADOTBLS
                    //*  BADOTBLS
                    if (condition(pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yyyymmdd_A.equals("00000000")))                                                                      //Natural: IF #BENE-DOB-YYYYMMDD-A = '00000000'
                    {
                        pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yyyymmdd_A.setValue(" ");                                                                                      //Natural: ASSIGN #BENE-DOB-YYYYMMDD-A := ' '
                        //*  BADOTBLS
                    }                                                                                                                                                     //Natural: END-IF
                    bene_File_Bene_Dte_Birth_Trust.getValue(pnd_Total_Bene).setValue(pnd_Bene_Dob_Yyyymmdd_Pnd_Bene_Dob_Yyyymmdd_A);                                      //Natural: ASSIGN BENE-DTE-BIRTH-TRUST ( #TOTAL-BENE ) := #BENE-DOB-YYYYMMDD-A
                    //*  BADOTBLS
                    //*  BADOTBLS
                    //*  BADOTBLS
                    if (condition(ldaScil3010.getAcis_Bene_File_Bene_Name().getValue(pnd_I).equals("DEFAULT TO PLAN / PRODUCT PROVISION")))                               //Natural: IF ACIS-BENE-FILE.BENE-NAME ( #I ) = 'DEFAULT TO PLAN / PRODUCT PROVISION'
                    {
                        bene_File_Bene_Relationship_Cde.getValue(pnd_I).setValue("40");                                                                                   //Natural: ASSIGN BENE-FILE.BENE-RELATIONSHIP-CDE ( #I ) := '40'
                        bene_File_Bene_Extended_Name.getValue(pnd_Total_Bene).setValue("PLEASE ADD YOU CURRENT INTENTIONS");                                              //Natural: ASSIGN BENE-FILE.BENE-EXTENDED-NAME ( #TOTAL-BENE ) := 'PLEASE ADD YOU CURRENT INTENTIONS'
                        ldaScil3010.getAcis_Bene_File_Bene_Spcl_Txt().getValue(pnd_I,"*").reset();                                                                        //Natural: RESET ACIS-BENE-FILE.BENE-SPCL-TXT ( #I,* )
                        //*  BADOTBLS
                        //*  BADOTBLS
                    }                                                                                                                                                     //Natural: END-IF
                    bene_File_Bene_Relationship_Free_Txt.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Relationship().getValue(pnd_I));            //Natural: ASSIGN BENE-RELATIONSHIP-FREE-TXT ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-RELATIONSHIP ( #I )
                    //*  BADOTBLS
                    //*  BADOTBLS
                    if (condition(ldaScil3010.getAcis_Bene_File_Bene_Ssn_Nbr().getValue(pnd_I).notEquals(getZero())))                                                     //Natural: IF ACIS-BENE-FILE.BENE-SSN-NBR ( #I ) NE 0
                    {
                        bene_File_Bene_Ssn_Cd.getValue(pnd_Total_Bene).setValue("S");                                                                                     //Natural: ASSIGN BENE-SSN-CD ( #TOTAL-BENE ) := 'S'
                        bene_File_Bene_Ssn_Nbr.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Ssn_Nbr().getValue(pnd_I));                           //Natural: ASSIGN BENE-FILE.BENE-SSN-NBR ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-SSN-NBR ( #I )
                        //*  BADOTBLS
                        bene_File_Bene_Ssn_Nbr.getValue(pnd_Total_Bene).setValue(bene_File_Bene_Ssn_Nbr.getValue(pnd_Total_Bene), MoveOption.RightJustified);             //Natural: MOVE RIGHT JUSTIFIED BENE-FILE.BENE-SSN-NBR ( #TOTAL-BENE ) TO BENE-FILE.BENE-SSN-NBR ( #TOTAL-BENE )
                        //*  BADOTBLS
                        //*  BADOTBLS
                        DbsUtil.examine(new ExamineSource(bene_File_Bene_Ssn_Nbr.getValue(pnd_Total_Bene)), new ExamineSearch(" "), new ExamineReplace("0"));             //Natural: EXAMINE BENE-FILE.BENE-SSN-NBR ( #TOTAL-BENE ) FOR ' ' REPLACE WITH '0'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(((ldaScil3010.getAcis_Bene_File_Bene_Nmrtr_Nbr().getValue(pnd_I).notEquals(getZero()) && ldaScil3010.getAcis_Bene_File_Bene_Dnmntr_Nbr().getValue(pnd_I).notEquals(getZero()))  //Natural: IF ( ( ACIS-BENE-FILE.BENE-NMRTR-NBR ( #I ) NE 0 AND ACIS-BENE-FILE.BENE-DNMNTR-NBR ( #I ) NE 0 ) OR ACIS-BENE-FILE.BENE-ALLOC-PCT ( #I ) NE 0 )
                        || ldaScil3010.getAcis_Bene_File_Bene_Alloc_Pct().getValue(pnd_I).notEquals(getZero()))))
                    {
                        //*  BADOTBLS
                        //*  BADOTBLS
                        //*  BADOTBLS
                        //*  BADOTBLS
                        //*  BADOTBLS
                        if (condition(ldaScil3010.getAcis_Bene_File_Bene_Nmrtr_Nbr().getValue(pnd_I).notEquals(getZero()) && ldaScil3010.getAcis_Bene_File_Bene_Dnmntr_Nbr().getValue(pnd_I).notEquals(getZero()))) //Natural: IF ACIS-BENE-FILE.BENE-NMRTR-NBR ( #I ) NE 0 AND ACIS-BENE-FILE.BENE-DNMNTR-NBR ( #I ) NE 0
                        {
                            bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_Total_Bene).setValue("S");                                                                       //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( #TOTAL-BENE ) := 'S'
                            bene_File_Bene_Nmrtr_Nbr.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Nmrtr_Nbr().getValue(pnd_I));                   //Natural: ASSIGN BENE-FILE.BENE-NMRTR-NBR ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-NMRTR-NBR ( #I )
                            bene_File_Bene_Dnmntr_Nbr.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Dnmntr_Nbr().getValue(pnd_I));                 //Natural: ASSIGN BENE-FILE.BENE-DNMNTR-NBR ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-DNMNTR-NBR ( #I )
                            //*  BADOTBLS
                            //*  BADOTBLS
                            //*  BADOTBLS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_Total_Bene).setValue("P");                                                                       //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( #TOTAL-BENE ) := 'P'
                            bene_File_Bene_Alloc_Pct.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Alloc_Pct().getValue(pnd_I));                   //Natural: ASSIGN BENE-FILE.BENE-ALLOC-PCT ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-ALLOC-PCT ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BADOTBLS
                    //*  BADOTBLS
                }                                                                                                                                                         //Natural: END-IF
                bene_File_Bene_Spcl_Txt.getValue(pnd_Total_Bene,"*").setValue(ldaScil3010.getAcis_Bene_File_Bene_Spcl_Txt().getValue(pnd_I,"*"));                         //Natural: ASSIGN BENE-FILE.BENE-SPCL-TXT ( #TOTAL-BENE,* ) := ACIS-BENE-FILE.BENE-SPCL-TXT ( #I,* )
                //*  BADOTBLS
                //*  BADOTBLS
                //*  BADOTBLS
                //*  BADOTBLS
                //*  BADOTBLS
                getReports().write(0, "=",bene_File_Bene_Spcl_Txt.getValue(pnd_Total_Bene,"*"));                                                                          //Natural: WRITE '=' BENE-FILE.BENE-SPCL-TXT ( #TOTAL-BENE,* )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FOR1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FOR1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                bene_File_Bene_Addr1.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Addr1().getValue(pnd_I));                                       //Natural: ASSIGN BENE-FILE.BENE-ADDR1 ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-ADDR1 ( #I )
                bene_File_Bene_Addr2.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Addr2().getValue(pnd_I));                                       //Natural: ASSIGN BENE-FILE.BENE-ADDR2 ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-ADDR2 ( #I )
                bene_File_Bene_Addr3_City.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Addr3_City().getValue(pnd_I));                             //Natural: ASSIGN BENE-FILE.BENE-ADDR3-CITY ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-ADDR3-CITY ( #I )
                bene_File_Bene_State.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_State().getValue(pnd_I));                                       //Natural: ASSIGN BENE-FILE.BENE-STATE ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-STATE ( #I )
                bene_File_Bene_Zip.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Zip().getValue(pnd_I));                                           //Natural: ASSIGN BENE-FILE.BENE-ZIP ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-ZIP ( #I )
                bene_File_Bene_Phone.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Phone().getValue(pnd_I));                                       //Natural: ASSIGN BENE-FILE.BENE-PHONE ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-PHONE ( #I )
                bene_File_Bene_Gender.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Gender().getValue(pnd_I));                                     //Natural: ASSIGN BENE-FILE.BENE-GENDER ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-GENDER ( #I )
                bene_File_Bene_Country.getValue(pnd_Total_Bene).setValue(ldaScil3010.getAcis_Bene_File_Bene_Country().getValue(pnd_I));                                   //Natural: ASSIGN BENE-FILE.BENE-COUNTRY ( #TOTAL-BENE ) := ACIS-BENE-FILE.BENE-COUNTRY ( #I )
                //*  BADOTBLS
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            bene_File_Bene_Nmbr_Of_Benes.setValue(pnd_Total_Bene);                                                                                                        //Natural: ASSIGN BENE-NMBR-OF-BENES := #TOTAL-BENE
            pnd_Total_Bene.reset();                                                                                                                                       //Natural: RESET #TOTAL-BENE
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(ldaScil3010.getAcis_Bene_File_Bene_Category().equals("1")))                                                                                         //Natural: IF ACIS-BENE-FILE.BENE-CATEGORY EQ '1'
        {
            bene_File_Bene_Dflt_To_Estate.setValue("Y");                                                                                                                  //Natural: ASSIGN BENE-DFLT-TO-ESTATE := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Total_Read.subtract(pnd_Total_Error));                                                             //Natural: ASSIGN #TOTAL := #TOTAL-READ - #TOTAL-ERROR
        getReports().write(3, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 3 ) ' '
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(35),pnd_Total_Read,new TabSetting(65),pnd_Total,new TabSetting(95),pnd_Total_Error,NEWLINE,new          //Natural: WRITE ( 3 ) 35T #TOTAL-READ 65T #TOTAL 95T #TOTAL-ERROR / 1T '-' ( 125 ) / /
            TabSetting(1),"-",new RepeatItem(125),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE," ",NEWLINE,new TabSetting(1),"G r a n d   T o t a l",new TabSetting(35),pnd_Total_Read,new TabSetting(65),pnd_Total,new  //Natural: WRITE ( 3 ) ' ' / 1T 'G r a n d   T o t a l' 35T #TOTAL-READ 65T #TOTAL 95T #TOTAL-ERROR /// 52T '*** E N D  O F  R E P O R T ***'
            TabSetting(95),pnd_Total_Error,NEWLINE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");
        if (Global.isEscape()) return;
    }
    private void sub_Call_Bene() throws Exception                                                                                                                         //Natural: CALL-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        DbsUtil.callnat(Filtomdm.class , getCurrentProcessState(), bene_File);                                                                                            //Natural: CALLNAT 'FILTOMDM' BENE-FILE
        if (condition(Global.isEscape())) return;
        //*  BADOTBLS
        if (condition(bene_File_Bene_Return_Code.equals("E")))                                                                                                            //Natural: IF BENE-RETURN-CODE EQ 'E'
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-EXCEPTION-REPORT
            sub_Write_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Total_Error.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-ERROR
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(bene_File_Bene_Return_Code.equals("W")))                                                                                                            //Natural: IF BENE-RETURN-CODE EQ 'W'
        {
                                                                                                                                                                          //Natural: PERFORM RECORD-SENT-TO-BENE
            sub_Record_Sent_To_Bene();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Acis_Bene() throws Exception                                                                                                                  //Natural: UPDATE-ACIS-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        GET1:                                                                                                                                                             //Natural: GET ACIS-BENE-FILE #ISN
        ldaScil3010.getVw_acis_Bene_File().readByID(pnd_Isn.getLong(), "GET1");
        ldaScil3010.getAcis_Bene_File_Bene_Record_Status().setValue("C");                                                                                                 //Natural: ASSIGN ACIS-BENE-FILE.BENE-RECORD-STATUS := 'C'
        ldaScil3010.getAcis_Bene_File_Bene_Sync_Ind().setValue("M");                                                                                                      //Natural: ASSIGN ACIS-BENE-FILE.BENE-SYNC-IND := 'M'
        ldaScil3010.getAcis_Bene_File_Bene_Update_By().setValue(Global.getINIT_USER());                                                                                   //Natural: ASSIGN ACIS-BENE-FILE.BENE-UPDATE-BY := *INIT-USER
        ldaScil3010.getAcis_Bene_File_Bene_Update_Dt().setValue(Global.getDATX());                                                                                        //Natural: ASSIGN ACIS-BENE-FILE.BENE-UPDATE-DT := *DATX
        ldaScil3010.getVw_acis_Bene_File().updateDBRow("GET1");                                                                                                           //Natural: UPDATE ( GET1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Write_Exception_Report() throws Exception                                                                                                            //Natural: WRITE-EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        getReports().write(1, ReportOption.NOTITLE,ldaScil3010.getAcis_Bene_File_Bene_Pin_Nbr(),"/",ldaScil3010.getAcis_Bene_File_Bene_Tiaa_Nbr(),new                     //Natural: WRITE ( 1 ) ACIS-BENE-FILE.BENE-PIN-NBR '/' ACIS-BENE-FILE.BENE-TIAA-NBR 4X ACIS-BENE-FILE.BENE-ORIGN-SYSTEM
            ColumnSpacing(4),ldaScil3010.getAcis_Bene_File_Bene_Orign_System());
        if (Global.isEscape()) return;
    }
    private void sub_Record_Sent_To_Bene() throws Exception                                                                                                               //Natural: RECORD-SENT-TO-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Part_Name.reset();                                                                                                                                            //Natural: RESET #PART-NAME
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        pnd_Part_Name.setValue(DbsUtil.compress(ldaScil3010.getAcis_Bene_File_Bene_Part_Prfx(), " ", ldaScil3010.getAcis_Bene_File_Bene_Part_Last_Nme(),                  //Natural: COMPRESS ACIS-BENE-FILE.BENE-PART-PRFX ' ' ACIS-BENE-FILE.BENE-PART-LAST-NME ' ' ACIS-BENE-FILE.BENE-PART-FIRST-NME ' ' ACIS-BENE-FILE.BENE-PART-MIDDLE-NME ' ' ACIS-BENE-FILE.BENE-PART-SFFX INTO #PART-NAME
            " ", ldaScil3010.getAcis_Bene_File_Bene_Part_First_Nme(), " ", ldaScil3010.getAcis_Bene_File_Bene_Part_Middle_Nme(), " ", ldaScil3010.getAcis_Bene_File_Bene_Part_Sffx()));
        //*  **********************************************************************
        //*  1.IF TIAA ISSUE DATE IS PRESENT-USE TIAA CONTRACT# AND TIAA ISSUE DATE
        //*  2.IF CREF ISSUE DATE IS PRESENT-USE CREF CONTRACT# AND CREF ISSUE DATE
        //*  3.IF BOTH ISSUE DATES ARE PRESENT - USE TIAA CONTRACT# AND ISSUE DATE
        //*  **********************************************************************
        short decideConditionsMet763 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-T-DOI = 0
        if (condition(annty_Actvty_Prap_Ap_T_Doi.equals(getZero())))
        {
            decideConditionsMet763++;
            //*  BADOTBLS
            pnd_Display_Cntrct.setValue(bene_File_Bene_Cref_Nbr);                                                                                                         //Natural: MOVE BENE-FILE.BENE-CREF-NBR TO #DISPLAY-CNTRCT
        }                                                                                                                                                                 //Natural: WHEN AP-C-DOI = 0
        else if (condition(annty_Actvty_Prap_Ap_C_Doi.equals(getZero())))
        {
            decideConditionsMet763++;
            //*  BADOTBLS
            pnd_Display_Cntrct.setValue(bene_File_Bene_Tiaa_Nbr);                                                                                                         //Natural: MOVE BENE-FILE.BENE-TIAA-NBR TO #DISPLAY-CNTRCT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  BADOTBLS
            pnd_Display_Cntrct.setValue(bene_File_Bene_Tiaa_Nbr);                                                                                                         //Natural: MOVE BENE-FILE.BENE-TIAA-NBR TO #DISPLAY-CNTRCT
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  BADOTBLS
        //*  BADOTBLS
        getReports().write(2, ReportOption.NOTITLE,bene_File_Bene_Pin_Nbr,"/",pnd_Display_Cntrct,new ColumnSpacing(4),bene_File_Bene_Rqstng_System,new                    //Natural: WRITE ( 2 ) BENE-FILE.BENE-PIN-NBR '/' #DISPLAY-CNTRCT 4X BENE-RQSTNG-SYSTEM 4X #PART-NAME
            ColumnSpacing(4),pnd_Part_Name);
        if (Global.isEscape()) return;
    }
    private void sub_Get_Todays_Bdate() throws Exception                                                                                                                  //Natural: GET-TODAYS-BDATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  BADOTBLS
        pnd_Todays_Dt.reset();                                                                                                                                            //Natural: RESET #TODAYS-DT #TODAY #TODAY-DATE
        pnd_Today.reset();
        pnd_Today_Date.reset();
        pnd_Today.setValue(Global.getDATN());                                                                                                                             //Natural: ASSIGN #TODAY := *DATN
        //*  BADOTBLS
        DbsUtil.callnat(Scin8888.class , getCurrentProcessState(), pnd_Today);                                                                                            //Natural: CALLNAT 'SCIN8888' #TODAY
        if (condition(Global.isEscape())) return;
        //*  BADOTBLS
        if (condition(DbsUtil.maskMatches(pnd_Today,"YYYYMMDD")))                                                                                                         //Natural: IF #TODAY EQ MASK ( YYYYMMDD )
        {
            //*  BADOTBLS
            pnd_Today_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Today_Pnd_Today_A);                                                                          //Natural: MOVE EDITED #TODAY-A TO #TODAY-DATE ( EM = YYYYMMDD )
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Prap() throws Exception                                                                                                                          //Natural: GET-PRAP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ppg.reset();                                                                                                                                                  //Natural: RESET #PPG
        pnd_Skip.setValue(false);                                                                                                                                         //Natural: MOVE FALSE TO #SKIP
        vw_annty_Actvty_Prap.startDatabaseRead                                                                                                                            //Natural: READ ANNTY-ACTVTY-PRAP BY AP-SOC-SEC EQ ACIS-BENE-FILE.BENE-PART-SSN
        (
        "READ01",
        new Wc[] { new Wc("AP_SOC_SEC", ">=", ldaScil3010.getAcis_Bene_File_Bene_Part_Ssn(), WcType.BY) },
        new Oc[] { new Oc("AP_SOC_SEC", "ASC") }
        );
        READ01:
        while (condition(vw_annty_Actvty_Prap.readNextRow("READ01")))
        {
            if (condition(annty_Actvty_Prap_Ap_Soc_Sec.notEquals(ldaScil3010.getAcis_Bene_File_Bene_Part_Ssn())))                                                         //Natural: IF AP-SOC-SEC NE ACIS-BENE-FILE.BENE-PART-SSN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  EAC (RODGER)
            if (condition(annty_Actvty_Prap_Ap_Record_Type.notEquals(1)))                                                                                                 //Natural: REJECT IF AP-RECORD-TYPE NE 1
            {
                continue;
            }
            pnd_Tiaa_Nbr.setValue(annty_Actvty_Prap_Ap_Tiaa_Cntrct);                                                                                                      //Natural: ASSIGN #TIAA-NBR := AP-TIAA-CNTRCT
            if (condition(pnd_Tiaa_Nbr.notEquals(ldaScil3010.getAcis_Bene_File_Bene_Tiaa_Nbr())))                                                                         //Natural: IF #TIAA-NBR NE ACIS-BENE-FILE.BENE-TIAA-NBR
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  BADOTBLS
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ppg.setValue(annty_Actvty_Prap_Ap_Coll_Code);                                                                                                             //Natural: ASSIGN #PPG := AP-COLL-CODE
            bene_File_Bene_Fldr_Log_Dte_Tme.setValue(annty_Actvty_Prap_Ap_Rqst_Log_Dte_Time.getValue(1));                                                                 //Natural: ASSIGN BENE-FLDR-LOG-DTE-TME := AP-RQST-LOG-DTE-TIME ( 1 )
            pnd_Lob.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_Ap_Lob, annty_Actvty_Prap_Ap_Lob_Type));                                   //Natural: COMPRESS AP-LOB AP-LOB-TYPE INTO #LOB LEAVING NO SPACE
            if (condition(((((pnd_Lob.equals("I3") || pnd_Lob.equals("I4")) || pnd_Lob.equals("I6")) && pnd_Today.greater(pnd_Ira_Sub_Global_Start_Date))                 //Natural: IF ( #LOB = 'I3' OR = 'I4' OR = 'I6' ) AND #TODAY GT #IRA-SUB-GLOBAL-START-DATE AND #IRA-SUB-GLOBAL-START-DATE GT 0
                && pnd_Ira_Sub_Global_Start_Date.greater(getZero()))))
            {
                pnd_Skip.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #SKIP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Update_Invalid_Case() throws Exception                                                                                                               //Natural: UPDATE-INVALID-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        GET2:                                                                                                                                                             //Natural: GET ACIS-BENE-FILE #ISN
        ldaScil3010.getVw_acis_Bene_File().readByID(pnd_Isn.getLong(), "GET2");
        ldaScil3010.getAcis_Bene_File_Bene_Record_Status().setValue("P");                                                                                                 //Natural: ASSIGN ACIS-BENE-FILE.BENE-RECORD-STATUS := 'P'
        ldaScil3010.getAcis_Bene_File_Bene_Sync_Ind().setValue("N");                                                                                                      //Natural: ASSIGN ACIS-BENE-FILE.BENE-SYNC-IND := 'N'
        ldaScil3010.getAcis_Bene_File_Bene_Update_By().setValue(Global.getINIT_USER());                                                                                   //Natural: ASSIGN ACIS-BENE-FILE.BENE-UPDATE-BY := *INIT-USER
        ldaScil3010.getAcis_Bene_File_Bene_Update_Dt().setValue(Global.getDATX());                                                                                        //Natural: ASSIGN ACIS-BENE-FILE.BENE-UPDATE-DT := *DATX
        ldaScil3010.getVw_acis_Bene_File().updateDBRow("GET2");                                                                                                           //Natural: UPDATE ( GET2. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    //*  (TNGPROC START)
    private void sub_Retrieve_Irasub_Date() throws Exception                                                                                                              //Natural: RETRIEVE-IRASUB-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Plan_Key_Entry_Table_Id_Nbr.setValue(100);                                                                                                                    //Natural: ASSIGN #PLAN-KEY.ENTRY-TABLE-ID-NBR = 000100
        pnd_Plan_Key_Entry_Table_Sub_Id.setValue("IS");                                                                                                                   //Natural: ASSIGN #PLAN-KEY.ENTRY-TABLE-SUB-ID = 'IS'
        pnd_Plan_Key_Entry_Cde.setValue(" ");                                                                                                                             //Natural: ASSIGN #PLAN-KEY.ENTRY-CDE = ' '
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) APP-TABLE-ENTRY WITH TABLE-ID-ENTRY-CDE = #PLAN-KEY
        (
        "READ02",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Plan_Key, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") },
        1
        );
        READ02:
        while (condition(vw_app_Table_Entry.readNextRow("READ02")))
        {
            //*  TNG  AND OR
            //*  TNG
            if (condition(app_Table_Entry_Entry_Table_Sub_Id.greater("IS") || app_Table_Entry_Entry_Table_Id_Nbr.notEquals(100)))                                         //Natural: IF APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID > 'IS' OR APP-TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE 00100
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ira_Sub_Global_Start_Date.compute(new ComputeParameters(false, pnd_Ira_Sub_Global_Start_Date), app_Table_Entry_Entry_Cde.val());                          //Natural: ASSIGN #IRA-SUB-GLOBAL-START-DATE = VAL ( APP-TABLE-ENTRY.ENTRY-CDE )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  (TNGPROC END)
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(120),Global.getDATX(), new ReportEditMask             //Natural: WRITE ( 1 ) NOTITLE 1T *PROGRAM 120T *DATX ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 1 ) 52T 'ACIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T ' Exceptions Detail Report' / 40T 'Business Date: ' #TODAY-DATE ( EM = MM/DD/YYYY ) /// 'Sent Business Date:' #TODAY-DATE ( EM = MM/DD/YYYY ) // 'Pin' '/' 'Contract' 27T 'System' 5X 'Error Message'/ 1T '---------------------' 25T '-----------' 1X '-' ( 60 )
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"Page :",getReports().getPageNumberDbs(1),new TabSetting(52),"ACIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40)," Exceptions Detail Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,"Sent Business Date:",pnd_Today_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"Pin","/","Contract",new 
                        TabSetting(27),"System",new ColumnSpacing(5),"Error Message",NEWLINE,new TabSetting(1),"---------------------",new TabSetting(25),"-----------",new 
                        ColumnSpacing(1),"-",new RepeatItem(60));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(120),Global.getDATX(), new ReportEditMask             //Natural: WRITE ( 2 ) NOTITLE 1T *PROGRAM 120T *DATX ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 2 ) 52T 'ACIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T ' Successful Interface Detail Report' / 40T 'Business Date: ' #TODAY-DATE ( EM = MM/DD/YYYY ) /// 'Sent Business Date:' #TODAY-DATE ( EM = MM/DD/YYYY ) // 'Pin' '/' 'Contract' 27T 'System' 37T 'Name Associated with Contract' / 1T '---------------------' 25T '-----------' 1X '-' ( 60 )
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"Page :",getReports().getPageNumberDbs(2),new TabSetting(52),"ACIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40)," Successful Interface Detail Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,"Sent Business Date:",pnd_Today_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"Pin","/","Contract",new 
                        TabSetting(27),"System",new TabSetting(37),"Name Associated with Contract",NEWLINE,new TabSetting(1),"---------------------",new 
                        TabSetting(25),"-----------",new ColumnSpacing(1),"-",new RepeatItem(60));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(120),Global.getDATX(), new ReportEditMask             //Natural: WRITE ( 3 ) NOTITLE 1T *PROGRAM 120T *DATX ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 3 ) 52T 'ACIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T 'Interface Summary Report' / 40T 'Business Date: ' #TODAY-DATE ( EM = MM/DD/YYYY ) /// 32T 'Total Designations' 62T 'Total Designations' 92T 'Total' / 32T 'Read from ACIS' 62T 'Written to Interface' 92T 'Errors' 1X '_' ( 132 )
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"Page :",getReports().getPageNumberDbs(3),new TabSetting(52),"ACIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40),"Interface Summary Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(32),"Total Designations",new TabSetting(62),"Total Designations",new 
                        TabSetting(92),"Total",NEWLINE,new TabSetting(32),"Read from ACIS",new TabSetting(62),"Written to Interface",new TabSetting(92),"Errors",new 
                        ColumnSpacing(1),"_",new RepeatItem(132));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "PROGRAM:   ",Global.getPROGRAM(),NEWLINE,"ERROR LINE:",Global.getERROR_LINE(),NEWLINE,"ERROR:     ",Global.getERROR_NR());                 //Natural: WRITE 'PROGRAM:   ' *PROGRAM / 'ERROR LINE:' *ERROR-LINE / 'ERROR:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");
        Global.format(3, "LS=133 PS=60");
    }
}
