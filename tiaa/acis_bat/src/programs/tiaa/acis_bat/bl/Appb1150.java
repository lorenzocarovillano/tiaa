/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:56 PM
**        * FROM NATURAL PROGRAM : Appb1150
************************************************************
**        * FILE NAME            : Appb1150.java
**        * CLASS NAME           : Appb1150
**        * INSTANCE NAME        : Appb1150
************************************************************
************************************************************************
* PROGRAM  : APPB1150                                                  *
* SYSTEM   : ACIS                                                      *
* TITLE    : PLANINFO TABLE LOAD                                       *
* FUNCTION : LOAD PLANINFO DATA TO APP-TABLE-ENTRY TABLE 100/PL        *
*                                                                      *
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES                        *
* ----------  ---------  --------------------------------------------  *
* 07/29/2008  DEVELBISS  INITIAL WRITE                                 *
* 08/30/2010  BERGHEISER INCREASE FIELD SIZE OF #T-FUNDS (INC1124301)  *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb1150 extends BLNatBase
{
    // Data Areas
    private PdaPsta9200 pdaPsta9200;
    private LdaPstl9902 ldaPstl9902;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup workfile_Rec;
    private DbsField workfile_Rec_P_Plan_No;
    private DbsField workfile_Rec_P_Subplan_No;
    private DbsField workfile_Rec_P_Tiaa_Cntrct;
    private DbsField workfile_Rec_P_Cref_Cert;
    private DbsField workfile_Rec_P_Lob;
    private DbsField workfile_Rec_P_Lob_Type;
    private DbsField workfile_Rec_P_Region_Code;
    private DbsField workfile_Rec_P_Coll_Code;
    private DbsField workfile_Rec_P_Category;
    private DbsField workfile_Rec_P_Fund_Num_Cde;
    private DbsField workfile_Rec_P_Fund_Name;
    private DbsField workfile_Rec_P_Category_Cnt;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;

    private DbsGroup app_Table_Entry__R_Field_1;
    private DbsField app_Table_Entry_Plan_Type;
    private DbsField app_Table_Entry_Plan_Number;
    private DbsField app_Table_Entry_Fund_Occurance;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup app_Table_Entry__R_Field_2;
    private DbsField app_Table_Entry_Fund_Name;
    private DbsField app_Table_Entry_Entry_User_Id;
    private DbsField app_Table_Entry_Hold_Cde;

    private DbsGroup app_Table_Entry__R_Field_3;
    private DbsField app_Table_Entry_Fund_Num;
    private DbsField app_Table_Entry_State_Name;

    private DbsGroup app_Table_Entry__R_Field_4;
    private DbsField app_Table_Entry_Asset_Class_Name;
    private DbsField app_Table_Entry_Update_Date;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_5;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_6;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Type;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Number;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Occurrence;

    private DataAccessProgramView vw_update_Table;
    private DbsField pnd_Current_Date;

    private DbsGroup pnd_Current_Date__R_Field_7;
    private DbsField pnd_Current_Date_Pnd_Current_Date_N;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Deleted;
    private DbsField pnd_Et_Count;
    private DbsField pnd_T_Plans;
    private DbsField pnd_T_Funds;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9200 = new PdaPsta9200(localVariables);
        ldaPstl9902 = new LdaPstl9902();
        registerRecord(ldaPstl9902);

        // Local Variables

        workfile_Rec = localVariables.newGroupInRecord("workfile_Rec", "WORKFILE-REC");
        workfile_Rec_P_Plan_No = workfile_Rec.newFieldInGroup("workfile_Rec_P_Plan_No", "P-PLAN-NO", FieldType.STRING, 6);
        workfile_Rec_P_Subplan_No = workfile_Rec.newFieldInGroup("workfile_Rec_P_Subplan_No", "P-SUBPLAN-NO", FieldType.STRING, 6);
        workfile_Rec_P_Tiaa_Cntrct = workfile_Rec.newFieldInGroup("workfile_Rec_P_Tiaa_Cntrct", "P-TIAA-CNTRCT", FieldType.STRING, 10);
        workfile_Rec_P_Cref_Cert = workfile_Rec.newFieldInGroup("workfile_Rec_P_Cref_Cert", "P-CREF-CERT", FieldType.STRING, 10);
        workfile_Rec_P_Lob = workfile_Rec.newFieldInGroup("workfile_Rec_P_Lob", "P-LOB", FieldType.STRING, 1);
        workfile_Rec_P_Lob_Type = workfile_Rec.newFieldInGroup("workfile_Rec_P_Lob_Type", "P-LOB-TYPE", FieldType.STRING, 1);
        workfile_Rec_P_Region_Code = workfile_Rec.newFieldInGroup("workfile_Rec_P_Region_Code", "P-REGION-CODE", FieldType.STRING, 3);
        workfile_Rec_P_Coll_Code = workfile_Rec.newFieldInGroup("workfile_Rec_P_Coll_Code", "P-COLL-CODE", FieldType.STRING, 4);
        workfile_Rec_P_Category = workfile_Rec.newFieldArrayInGroup("workfile_Rec_P_Category", "P-CATEGORY", FieldType.STRING, 35, new DbsArrayController(1, 
            106));
        workfile_Rec_P_Fund_Num_Cde = workfile_Rec.newFieldArrayInGroup("workfile_Rec_P_Fund_Num_Cde", "P-FUND-NUM-CDE", FieldType.STRING, 5, new DbsArrayController(1, 
            106));
        workfile_Rec_P_Fund_Name = workfile_Rec.newFieldArrayInGroup("workfile_Rec_P_Fund_Name", "P-FUND-NAME", FieldType.STRING, 50, new DbsArrayController(1, 
            106));
        workfile_Rec_P_Category_Cnt = workfile_Rec.newFieldInGroup("workfile_Rec_P_Category_Cnt", "P-CATEGORY-CNT", FieldType.NUMERIC, 6);

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        app_Table_Entry__R_Field_1 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_1", "REDEFINE", app_Table_Entry_Entry_Cde);
        app_Table_Entry_Plan_Type = app_Table_Entry__R_Field_1.newFieldInGroup("app_Table_Entry_Plan_Type", "PLAN-TYPE", FieldType.STRING, 1);
        app_Table_Entry_Plan_Number = app_Table_Entry__R_Field_1.newFieldInGroup("app_Table_Entry_Plan_Number", "PLAN-NUMBER", FieldType.STRING, 6);
        app_Table_Entry_Fund_Occurance = app_Table_Entry__R_Field_1.newFieldInGroup("app_Table_Entry_Fund_Occurance", "FUND-OCCURANCE", FieldType.NUMERIC, 
            3);
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        app_Table_Entry__R_Field_2 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_2", "REDEFINE", app_Table_Entry_Entry_Dscrptn_Txt);
        app_Table_Entry_Fund_Name = app_Table_Entry__R_Field_2.newFieldInGroup("app_Table_Entry_Fund_Name", "FUND-NAME", FieldType.STRING, 50);
        app_Table_Entry_Entry_User_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        app_Table_Entry_Hold_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Hold_Cde", "HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "HOLD_CDE");

        app_Table_Entry__R_Field_3 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_3", "REDEFINE", app_Table_Entry_Hold_Cde);
        app_Table_Entry_Fund_Num = app_Table_Entry__R_Field_3.newFieldInGroup("app_Table_Entry_Fund_Num", "FUND-NUM", FieldType.STRING, 5);
        app_Table_Entry_State_Name = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_State_Name", "STATE-NAME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "STATE_NAME");

        app_Table_Entry__R_Field_4 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_4", "REDEFINE", app_Table_Entry_State_Name);
        app_Table_Entry_Asset_Class_Name = app_Table_Entry__R_Field_4.newFieldInGroup("app_Table_Entry_Asset_Class_Name", "ASSET-CLASS-NAME", FieldType.STRING, 
            35);
        app_Table_Entry_Update_Date = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Update_Date", "UPDATE-DATE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "UPDATE_DATE");
        registerRecord(vw_app_Table_Entry);

        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 28);

        pnd_Table_Id_Entry_Cde__R_Field_5 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_5", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_5.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr", 
            "#ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id = pnd_Table_Id_Entry_Cde__R_Field_5.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id", 
            "#ENTRY-TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_5.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde", "#ENTRY-CDE", 
            FieldType.STRING, 20);

        pnd_Table_Id_Entry_Cde__R_Field_6 = pnd_Table_Id_Entry_Cde__R_Field_5.newGroupInGroup("pnd_Table_Id_Entry_Cde__R_Field_6", "REDEFINE", pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Type = pnd_Table_Id_Entry_Cde__R_Field_6.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Type", "#KY-PLAN-TYPE", 
            FieldType.STRING, 1);
        pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Number = pnd_Table_Id_Entry_Cde__R_Field_6.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Number", "#KY-PLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Occurrence = pnd_Table_Id_Entry_Cde__R_Field_6.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Occurrence", 
            "#KY-PLAN-OCCURRENCE", FieldType.NUMERIC, 3);

        vw_update_Table = new DataAccessProgramView(new NameInfo("vw_update_Table", "UPDATE-TABLE"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        registerRecord(vw_update_Table);

        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.STRING, 8);

        pnd_Current_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Current_Date__R_Field_7", "REDEFINE", pnd_Current_Date);
        pnd_Current_Date_Pnd_Current_Date_N = pnd_Current_Date__R_Field_7.newFieldInGroup("pnd_Current_Date_Pnd_Current_Date_N", "#CURRENT-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 8);
        pnd_Deleted = localVariables.newFieldInRecord("pnd_Deleted", "#DELETED", FieldType.NUMERIC, 9);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 9);
        pnd_T_Plans = localVariables.newFieldInRecord("pnd_T_Plans", "#T-PLANS", FieldType.NUMERIC, 9);
        pnd_T_Funds = localVariables.newFieldInRecord("pnd_T_Funds", "#T-FUNDS", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_app_Table_Entry.reset();
        vw_update_Table.reset();

        ldaPstl9902.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb1150() throws Exception
    {
        super("Appb1150");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Appb1150|Main");
        OnErrorManager.pushEvent("APPB1150", onError);
        while(true)
        {
            try
            {
                //* **********************************************************************
                //* * MAIN LOGIC SECTION                                                **
                //* **********************************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                                                                                                                                                                          //Natural: PERFORM DEL-PLAN
                sub_Del_Plan();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADD-PLAN
                sub_Add_Plan();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEL-PLAN
                //* *-------------------------------------------------------------------**
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEL-ENTRY
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-PLAN
            }                                                                                                                                                             //Natural: ON ERROR
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Del_Plan() throws Exception                                                                                                                          //Natural: DEL-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(100);                                                                                                      //Natural: ASSIGN #ENTRY-TABLE-ID-NBR := 100
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("PL");                                                                                                     //Natural: ASSIGN #ENTRY-TABLE-SUB-ID := 'PL'
        if (condition(pnd_Input_Parm.equals("REPRINT")))                                                                                                                  //Natural: IF #INPUT-PARM = 'REPRINT'
        {
            pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Type.setValue("R");                                                                                                        //Natural: ASSIGN #KY-PLAN-TYPE := 'R'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Type.setValue("O");                                                                                                        //Natural: ASSIGN #KY-PLAN-TYPE := 'O'
        }                                                                                                                                                                 //Natural: END-IF
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE FROM #TABLE-ID-ENTRY-CDE
        (
        "READTBL",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READTBL:
        while (condition(vw_app_Table_Entry.readNextRow("READTBL")))
        {
            if (condition(app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id)  //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID OR PLAN-TYPE NE #KY-PLAN-TYPE
                || app_Table_Entry_Plan_Type.notEquals(pnd_Table_Id_Entry_Cde_Pnd_Ky_Plan_Type)))
            {
                if (true) break READTBL;                                                                                                                                  //Natural: ESCAPE BOTTOM ( READTBL. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Deleted.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #DELETED
                pnd_Et_Count.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #ET-COUNT
                                                                                                                                                                          //Natural: PERFORM DEL-ENTRY
                sub_Del_Entry();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READTBL"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READTBL"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "PLAN RECORDS DELETED: ",pnd_Deleted);                                                                                                      //Natural: WRITE 'PLAN RECORDS DELETED: ' #DELETED
        if (Global.isEscape()) return;
    }
    private void sub_Del_Entry() throws Exception                                                                                                                         //Natural: DEL-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        GETTBL:                                                                                                                                                           //Natural: GET UPDATE-TABLE *ISN ( READTBL. )
        vw_update_Table.readByID(vw_app_Table_Entry.getAstISN("READTBL"), "GETTBL");
        vw_update_Table.deleteDBRow("GETTBL");                                                                                                                            //Natural: DELETE ( GETTBL. )
        if (condition(pnd_Et_Count.equals(100)))                                                                                                                          //Natural: IF #ET-COUNT = 100
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Count.reset();                                                                                                                                         //Natural: RESET #ET-COUNT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Add_Plan() throws Exception                                                                                                                          //Natural: ADD-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        pnd_Current_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #CURRENT-DATE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 WORKFILE-REC
        while (condition(getWorkFiles().read(1, workfile_Rec)))
        {
            //* *DISPLAY P-TIAA-CNTRCT P-PLAN-NO
            pnd_J.reset();                                                                                                                                                //Natural: RESET #J
            FORADD:                                                                                                                                                       //Natural: FOR #I = 1 TO 106
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(106)); pnd_I.nadd(1))
            {
                if (condition(workfile_Rec_P_Fund_Num_Cde.getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF P-FUND-NUM-CDE ( #I ) NE ' '
                {
                    pnd_J.nadd(+1);                                                                                                                                       //Natural: ADD +1 TO #J
                    app_Table_Entry_Entry_Table_Id_Nbr.setValue(100);                                                                                                     //Natural: ASSIGN ENTRY-TABLE-ID-NBR := 100
                    app_Table_Entry_Entry_Table_Sub_Id.setValue("PL");                                                                                                    //Natural: ASSIGN ENTRY-TABLE-SUB-ID := 'PL'
                    app_Table_Entry_Plan_Number.setValue(workfile_Rec_P_Plan_No);                                                                                         //Natural: ASSIGN PLAN-NUMBER := P-PLAN-NO
                    if (condition(pnd_Input_Parm.equals("REPRINT")))                                                                                                      //Natural: IF #INPUT-PARM = 'REPRINT'
                    {
                        app_Table_Entry_Plan_Type.setValue("R");                                                                                                          //Natural: ASSIGN PLAN-TYPE := 'R'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        app_Table_Entry_Plan_Type.setValue("O");                                                                                                          //Natural: ASSIGN PLAN-TYPE := 'O'
                    }                                                                                                                                                     //Natural: END-IF
                    app_Table_Entry_Fund_Occurance.setValue(pnd_J);                                                                                                       //Natural: ASSIGN FUND-OCCURANCE := #J
                    app_Table_Entry_Asset_Class_Name.setValue(workfile_Rec_P_Category.getValue(pnd_I));                                                                   //Natural: ASSIGN ASSET-CLASS-NAME := P-CATEGORY ( #I )
                    app_Table_Entry_Fund_Num.setValue(workfile_Rec_P_Fund_Num_Cde.getValue(pnd_I));                                                                       //Natural: ASSIGN FUND-NUM := P-FUND-NUM-CDE ( #I )
                    app_Table_Entry_Fund_Name.setValue(workfile_Rec_P_Fund_Name.getValue(pnd_I));                                                                         //Natural: ASSIGN FUND-NAME := P-FUND-NAME ( #I )
                    app_Table_Entry_Entry_User_Id.setValue(Global.getINIT_USER());                                                                                        //Natural: ASSIGN ENTRY-USER-ID := *INIT-USER
                    app_Table_Entry_Update_Date.setValue(pnd_Current_Date_Pnd_Current_Date_N);                                                                            //Natural: ASSIGN UPDATE-DATE := #CURRENT-DATE-N
                    vw_app_Table_Entry.insertDBRow();                                                                                                                     //Natural: STORE APP-TABLE-ENTRY
                    pnd_T_Funds.nadd(+1);                                                                                                                                 //Natural: ADD +1 TO #T-FUNDS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_T_Plans.nadd(+1);                                                                                                                                         //Natural: ADD +1 TO #T-PLANS
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            getReports().write(0, "PLAN ADDED: ",workfile_Rec_P_Plan_No,"NUMBER OF FUNDS: ",pnd_J);                                                                       //Natural: WRITE 'PLAN ADDED: ' P-PLAN-NO 'NUMBER OF FUNDS: ' #J
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL PLANS ADDED: ",pnd_T_Plans, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),"NUMBER OF FUNDS: ",pnd_T_Funds, new ReportEditMask                   //Natural: WRITE 'TOTAL PLANS ADDED: ' #T-PLANS ( EM = ZZZ,ZZZ,ZZ9 ) 'NUMBER OF FUNDS: ' #T-FUNDS ( EM = ZZZ,ZZZ,ZZ9 )
            ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  RECORD NOT AVAILABLE AT PRESENT
        short decideConditionsMet133 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet133++;
            //* ***********************************************************************
            //*    START OF COPY CODE PSTC9902   : RETRY LOGIC
            //*  - FIRST CHECK FOR SUBSEQUENT RETRY - SAME LINE NUMBER.
            //*  - IF SUBSEQUENT RETRY, CHECK TOTAL ELAPSED TIME
            //*  - IF NEW RETRY, SET DEFAULT VALUES FOR WAIT PARAMETERS IF NONE SPECI-
            //*    FIED, START CLOCK.
            //*  - CALL CMROLL TO ACTIVATE WAITING PERIOD
            //*  - RETRY
            //*  ----------------------------------------------------------------------
            //*         PROGRAM USING THIS COPYCODE MUST USE LDA PSTL9902
            //*                                          AND PDA PSTA9200 AS AN LDA.
            //*         #WAIT-TIME & #TOTAL-WAITING-TIME MAY BE SPECIFIED BY THE
            //*         PROGRAM USING THIS COPY CODE, JUST BEFORE THE INCLUDE STATEMENT
            //*         THEY ARE BOTH IN T FORMAT. TO INITIALIZE A T FIELD, USE
            //*         MOVE T'00:mm:ss' TO #WAIT-TIME
            //*         WHERE SS = SECONDS  (MAX OF 99 SECONDS)
            //*         (THE LIMITATION IS BECAUSE CMROLL ACCEPT UP TO 99 SECONDS ONLY)
            //*  ----------------------------------------------------------------------
            //* ***********************************************************************
            if (condition(Global.getERROR_NR().equals(3145)))                                                                                                             //Natural: IF *ERROR-NR = 3145
            {
                ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error().setValue(Global.getTIMX());                                                                           //Natural: ASSIGN PSTL9902.#START-TIME-THIS-ERROR := *TIMX
                if (condition(ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().equals(Global.getERROR_LINE())))                                                              //Natural: IF PSTL9902.#PREV-ERROR-LINE = *ERROR-LINE
                {
                    ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time()),              //Natural: ASSIGN PSTL9902.#TOTAL-ELAPSED-TIME := *TIMX - PSTL9902.#TIME-STARTED
                        Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Started()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl9902.getPstl9902_Pnd_Time_Started().setValue(Global.getTIMX());                                                                                //Natural: ASSIGN PSTL9902.#TIME-STARTED := *TIMX
                    ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().setValue(Global.getERROR_LINE());                                                                       //Natural: ASSIGN PSTL9902.#PREV-ERROR-LINE := *ERROR-LINE
                    short decideConditionsMet170 = 0;                                                                                                                     //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN PSTL9902.#WAIT-TIME LE 0
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Wait_Time().lessOrEqual(getZero())))
                    {
                        decideConditionsMet170++;
                        if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                        {
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(20);                                                                                         //Natural: MOVE T'00:00:02' TO PSTL9902.#WAIT-TIME
                            getReports().write(0, "Setting WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Wait_Time());                                                        //Natural: WRITE 'Setting WAIT TIME to' PSTL9902.#WAIT-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                            //Natural: IF *DEVICE = 'BATCH'
                            {
                                ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(150);                                                                                    //Natural: MOVE T'00:00:15' TO PSTL9902.#WAIT-TIME
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(40);                                                                                     //Natural: MOVE T'00:00:04' TO PSTL9902.#WAIT-TIME
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN PSTL9902.#TOTAL-WAITING-TIME LE 0
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().lessOrEqual(getZero())))
                    {
                        decideConditionsMet170++;
                        if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                        {
                            ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(100);                                                                               //Natural: MOVE T'00:00:10' TO PSTL9902.#TOTAL-WAITING-TIME
                            getReports().write(0, "Setting TOTAL WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time());                                         //Natural: WRITE 'Setting TOTAL WAIT TIME to' PSTL9902.#TOTAL-WAITING-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                            //Natural: IF *DEVICE = 'BATCH'
                            {
                                ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(18000);                                                                         //Natural: MOVE T'00:30:00' TO PSTL9902.#TOTAL-WAITING-TIME
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(900);                                                                           //Natural: MOVE T'00:01:30' TO PSTL9902.#TOTAL-WAITING-TIME
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    if (condition(decideConditionsMet170 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().greater(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time())))                                //Natural: IF PSTL9902.#TOTAL-ELAPSED-TIME GT PSTL9902.#TOTAL-WAITING-TIME
                {
                    if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                    //Natural: IF *DEVICE = 'BATCH'
                    {
                        getReports().write(0, "Requested record in program",Global.getPROGRAM(),"line",Global.getERROR_LINE(),"was still on hold after",NEWLINE,ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time(),  //Natural: WRITE 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'was still on hold after' / PSTL9902.#TOTAL-WAITING-TIME ( EM = HH:II:SS )
                            new ReportEditMask ("HH:II:SS"));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaPsta9200.getPsta9200_Message_Text().setValue(DbsUtil.compress("Requested record in program", Global.getPROGRAM(), "line", Global.getERROR_LINE(),  //Natural: COMPRESS 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'exceeded hold time limit' INTO PSTA9200.MESSAGE-TEXT
                            "exceeded hold time limit"));
                        pdaPsta9200.getPsta9200_Enter_Allowed().reset();                                                                                                  //Natural: RESET PSTA9200.ENTER-ALLOWED PSTA9200.CONFIRM-ALLOWED
                        pdaPsta9200.getPsta9200_Confirm_Allowed().reset();
                        pdaPsta9200.getPsta9200_Exit_Allowed().setValue(true);                                                                                            //Natural: ASSIGN PSTA9200.EXIT-ALLOWED := TRUE
                        DbsUtil.callnat(Pstn9200.class , getCurrentProcessState(), pdaPsta9200.getPsta9200());                                                            //Natural: CALLNAT 'PSTN9200' PSTA9200
                        if (condition(Global.isEscape())) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Time_Elapsed()), Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error())); //Natural: ASSIGN PSTL9902.#TIME-ELAPSED := *TIMX - PSTL9902.#START-TIME-THIS-ERROR
                    if (condition(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().less(ldaPstl9902.getPstl9902_Pnd_Wait_Time())))                                              //Natural: IF PSTL9902.#TIME-ELAPSED LT PSTL9902.#WAIT-TIME
                    {
                        ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left()),                  //Natural: ASSIGN PSTL9902.#WAIT-TIME-LEFT := PSTL9902.#WAIT-TIME - PSTL9902.#TIME-ELAPSED
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed()));
                        ldaPstl9902.getPstl9902_Pnd_Seconds_Left_A().setValueEdited(ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left(),new ReportEditMask("SS"));               //Natural: MOVE EDITED PSTL9902.#WAIT-TIME-LEFT ( EM = SS ) TO PSTL9902.#SECONDS-LEFT-A
                        ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left().setValue(ldaPstl9902.getPstl9902_Pnd_Seconds_Left_N());                                           //Natural: ASSIGN PSTL9902.#WAIT-SECONDS-LEFT := PSTL9902.#SECONDS-LEFT-N
                        cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left(),ldaPstl9902.getPstl9902_Pnd_Cics_Rqst()); //Natural: CALL 'CMROLL' PSTL9902.#WAIT-SECONDS-LEFT PSTL9902.#CICS-RQST
                    }                                                                                                                                                     //Natural: END-IF
                    OnErrorManager.popRetry();                                                                                                                            //Natural: RETRY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* ***********************************************************************
            //*     END OF COPYCODE PSTC9902
            //* ***********************************************************************
            //*  INTERMEDIATE RESULT TOO LARGE
        }                                                                                                                                                                 //Natural: VALUE 1301
        else if (condition((Global.getERROR_NR().equals(1301))))
        {
            decideConditionsMet133++;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, "KEY",pnd_Table_Id_Entry_Cde,NEWLINE,"=",app_Table_Entry_Entry_Table_Id_Nbr,NEWLINE,"=",app_Table_Entry_Entry_Table_Sub_Id,             //Natural: WRITE 'KEY' #TABLE-ID-ENTRY-CDE / '=' ENTRY-TABLE-ID-NBR / '=' ENTRY-TABLE-SUB-ID / '=' ENTRY-CDE
                NEWLINE,"=",app_Table_Entry_Entry_Cde);
            getReports().write(0, "=",pnd_T_Plans,"=",pnd_T_Funds);                                                                                                       //Natural: WRITE '=' #T-PLANS '=' #T-FUNDS
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet133 > 0))
        {
            getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",            //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE /
                Global.getERROR_LINE(),NEWLINE);
            getReports().write(0, "CONTRACT:",workfile_Rec_P_Tiaa_Cntrct,"PLAN:",workfile_Rec_P_Plan_No);                                                                 //Natural: WRITE 'CONTRACT:' P-TIAA-CNTRCT 'PLAN:' P-PLAN-NO
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, "KEY",pnd_Table_Id_Entry_Cde,NEWLINE,"=",app_Table_Entry_Entry_Table_Id_Nbr,NEWLINE,"=",app_Table_Entry_Entry_Table_Sub_Id,             //Natural: WRITE 'KEY' #TABLE-ID-ENTRY-CDE / '=' ENTRY-TABLE-ID-NBR / '=' ENTRY-TABLE-SUB-ID / '=' ENTRY-CDE
                NEWLINE,"=",app_Table_Entry_Entry_Cde);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
}
