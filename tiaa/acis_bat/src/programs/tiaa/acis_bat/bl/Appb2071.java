/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:44 PM
**        * FROM NATURAL PROGRAM : Appb2071
************************************************************
**        * FILE NAME            : Appb2071.java
**        * CLASS NAME           : Appb2071
**        * INSTANCE NAME        : Appb2071
************************************************************
************************************************************************
* APPB2071 - WEEKLY CONTRACT RANGE USAGE REPORT                        *
* THIS PROGRAM IS RUN ONCE FOR DA CONTRACTS AND AGAIN FOR IA CONTRACTS *
* COMPARE CURRENT DA/IA CONTRACT RANGE TO LAST WEEK'S RANGE            *
* TO DETERMINE WHICH ONES CHANGED AND MAY NEED A NEXT CONTRACT RANGE   *
************************************************************************
*
************************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                     *
* ----------  ----------------  -------------------------------------- *
* 10/17/2005  JANET BERGHEISER  NEW                                    *
* 01/12/2007  JANET BERGHEISER  CONTRACT RANGE EXPANSION PROJECT (JB1) *
*                               ADD START DATE TO INPUT FILE LAYOUT    *
* 06/06/2007  JANET BERGHEISER  TERMINATE WITH RC FOR EMPTY FILES(JB2) *
* 06/07/2007  JANET BERGHEISER  ALLOW FOR FULL ALPHA RANGE FOR USAGE   *
*                               CALCULATIONS (JB3)                     *
* 06/24/2007  JANET BERGHEISER  INCREASE SIZE OF USAGE FIELD (JB4)     *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2071 extends BLNatBase
{
    // Data Areas
    private PdaAppa2075 pdaAppa2075;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Old_Record;

    private DbsGroup pnd_Old_Record__R_Field_1;
    private DbsField pnd_Old_Record_Pnd_O_Record_Type;
    private DbsField pnd_Old_Record_Pnd_O_Filler_1;
    private DbsField pnd_Old_Record_Pnd_O_Product_Code;
    private DbsField pnd_Old_Record_Pnd_O_Beg_Range;

    private DbsGroup pnd_Old_Record__R_Field_2;
    private DbsField pnd_Old_Record_Pnd_Beg_Prefix;
    private DbsField pnd_Old_Record_Pnd_Beg_Number;

    private DbsGroup pnd_Old_Record__R_Field_3;
    private DbsField pnd_Old_Record_Pnd_Beg_Char;

    private DbsGroup pnd_Old_Record__R_Field_4;
    private DbsField pnd_Old_Record_Pnd_O_Beg_2_4;
    private DbsField pnd_Old_Record_Pnd_O_Beg_5_7;

    private DbsGroup pnd_Old_Record__R_Field_5;
    private DbsField pnd_Old_Record_Pnd_O_Beg_5;
    private DbsField pnd_Old_Record_Pnd_O_Beg_6_7;

    private DbsGroup pnd_Old_Record__R_Field_6;
    private DbsField pnd_Old_Record_Pnd_O_Beg_6;
    private DbsField pnd_Old_Record_Pnd_O_Beg_7;
    private DbsField pnd_Old_Record_Pnd_O_End_Range;

    private DbsGroup pnd_Old_Record__R_Field_7;
    private DbsField pnd_Old_Record_Pnd_End_Prefix;
    private DbsField pnd_Old_Record_Pnd_End_Number;

    private DbsGroup pnd_Old_Record__R_Field_8;
    private DbsField pnd_Old_Record_Pnd_End_Char;

    private DbsGroup pnd_Old_Record__R_Field_9;
    private DbsField pnd_Old_Record_Pnd_O_End_2_4;
    private DbsField pnd_Old_Record_Pnd_O_End_5_7;

    private DbsGroup pnd_Old_Record__R_Field_10;
    private DbsField pnd_Old_Record_Pnd_O_End_5;
    private DbsField pnd_Old_Record_Pnd_O_End_6_7;

    private DbsGroup pnd_Old_Record__R_Field_11;
    private DbsField pnd_Old_Record_Pnd_O_End_6;
    private DbsField pnd_Old_Record_Pnd_O_End_7;
    private DbsField pnd_Old_Record_Pnd_O_Last_Contract;

    private DbsGroup pnd_Old_Record__R_Field_12;
    private DbsField pnd_Old_Record_Pnd_Last_Prefix;
    private DbsField pnd_Old_Record_Pnd_Last_Number;

    private DbsGroup pnd_Old_Record__R_Field_13;
    private DbsField pnd_Old_Record_Pnd_Last_Char;

    private DbsGroup pnd_Old_Record__R_Field_14;
    private DbsField pnd_Old_Record_Pnd_O_Last_2_4;
    private DbsField pnd_Old_Record_Pnd_O_Last_5_7;

    private DbsGroup pnd_Old_Record__R_Field_15;
    private DbsField pnd_Old_Record_Pnd_O_Last_5;
    private DbsField pnd_Old_Record_Pnd_O_Last_6_7;

    private DbsGroup pnd_Old_Record__R_Field_16;
    private DbsField pnd_Old_Record_Pnd_O_Last_6;
    private DbsField pnd_Old_Record_Pnd_O_Last_7;
    private DbsField pnd_Old_Record_Pnd_O_Range_Size;
    private DbsField pnd_Old_Record_Pnd_O_Filler_2;
    private DbsField pnd_Old_Record_Pnd_O_Range_Left;
    private DbsField pnd_Old_Record_Pnd_O_Filler_3;
    private DbsField pnd_Old_Record_Pnd_O_Next_Indicator;
    private DbsField pnd_Old_Record_Pnd_O_Filler_4;
    private DbsField pnd_Old_Record_Pnd_O_Start_Date;
    private DbsField pnd_Old_Record_Pnd_O_Filler_5;

    private DbsGroup pnd_Old_Record__R_Field_17;
    private DbsField pnd_Old_Record_Pnd_H_Record_Type;
    private DbsField pnd_Old_Record_Pnd_H_Filler_1;
    private DbsField pnd_Old_Record_Pnd_H_Contract_Type;
    private DbsField pnd_Old_Record_Pnd_H_Filler_2;
    private DbsField pnd_Old_Record_Pnd_H_Date;
    private DbsField pnd_Old_Record_Pnd_H_Filler_3;
    private DbsField pnd_New_Record;

    private DbsGroup pnd_New_Record__R_Field_18;
    private DbsField pnd_New_Record_Pnd_N_Record_Type;
    private DbsField pnd_New_Record_Pnd_N_Filler_1;
    private DbsField pnd_New_Record_Pnd_N_Product_Code;
    private DbsField pnd_New_Record_Pnd_N_Beg_Range;

    private DbsGroup pnd_New_Record__R_Field_19;
    private DbsField pnd_New_Record_Pnd_Beg_Prefix;
    private DbsField pnd_New_Record_Pnd_Beg_Number;

    private DbsGroup pnd_New_Record__R_Field_20;
    private DbsField pnd_New_Record_Pnd_Beg_Char;

    private DbsGroup pnd_New_Record__R_Field_21;
    private DbsField pnd_New_Record_Pnd_N_Beg_2_4;
    private DbsField pnd_New_Record_Pnd_N_Beg_5_7;

    private DbsGroup pnd_New_Record__R_Field_22;
    private DbsField pnd_New_Record_Pnd_N_Beg_5;
    private DbsField pnd_New_Record_Pnd_N_Beg_6_7;

    private DbsGroup pnd_New_Record__R_Field_23;
    private DbsField pnd_New_Record_Pnd_N_Beg_6;
    private DbsField pnd_New_Record_Pnd_N_Beg_7;
    private DbsField pnd_New_Record_Pnd_N_End_Range;

    private DbsGroup pnd_New_Record__R_Field_24;
    private DbsField pnd_New_Record_Pnd_End_Prefix;
    private DbsField pnd_New_Record_Pnd_End_Number;

    private DbsGroup pnd_New_Record__R_Field_25;
    private DbsField pnd_New_Record_Pnd_End_Char;

    private DbsGroup pnd_New_Record__R_Field_26;
    private DbsField pnd_New_Record_Pnd_N_End_2_4;
    private DbsField pnd_New_Record_Pnd_N_End_5_7;

    private DbsGroup pnd_New_Record__R_Field_27;
    private DbsField pnd_New_Record_Pnd_N_End_5;
    private DbsField pnd_New_Record_Pnd_N_End_6_7;

    private DbsGroup pnd_New_Record__R_Field_28;
    private DbsField pnd_New_Record_Pnd_N_End_6;
    private DbsField pnd_New_Record_Pnd_N_End_7;
    private DbsField pnd_New_Record_Pnd_N_Last_Contract;

    private DbsGroup pnd_New_Record__R_Field_29;
    private DbsField pnd_New_Record_Pnd_Last_Prefix;
    private DbsField pnd_New_Record_Pnd_Last_Number;

    private DbsGroup pnd_New_Record__R_Field_30;
    private DbsField pnd_New_Record_Pnd_Last_Char;

    private DbsGroup pnd_New_Record__R_Field_31;
    private DbsField pnd_New_Record_Pnd_N_Last_2_4;
    private DbsField pnd_New_Record_Pnd_N_Last_5_7;

    private DbsGroup pnd_New_Record__R_Field_32;
    private DbsField pnd_New_Record_Pnd_N_Last_5;
    private DbsField pnd_New_Record_Pnd_N_Last_6_7;

    private DbsGroup pnd_New_Record__R_Field_33;
    private DbsField pnd_New_Record_Pnd_N_Last_6;
    private DbsField pnd_New_Record_Pnd_N_Last_7;

    private DbsGroup pnd_New_Record__R_Field_34;
    private DbsField pnd_New_Record_Pnd_N_Contract_Char;
    private DbsField pnd_New_Record_Pnd_N_Range_Size;
    private DbsField pnd_New_Record_Pnd_N_Filler_2;
    private DbsField pnd_New_Record_Pnd_N_Range_Left;
    private DbsField pnd_New_Record_Pnd_N_Filler_3;
    private DbsField pnd_New_Record_Pnd_N_Next_Indicator;
    private DbsField pnd_New_Record_Pnd_N_Filler_4;
    private DbsField pnd_New_Record_Pnd_N_Start_Date;
    private DbsField pnd_New_Record_Pnd_N_Filler_5;

    private DbsGroup pnd_New_Record__R_Field_35;
    private DbsField pnd_New_Record_Pnd_H_Record_Type;
    private DbsField pnd_New_Record_Pnd_H_Filler_1;
    private DbsField pnd_New_Record_Pnd_H_Contract_Type;
    private DbsField pnd_New_Record_Pnd_H_Filler_2;
    private DbsField pnd_New_Record_Pnd_H_Date;
    private DbsField pnd_New_Record_Pnd_H_Filler_3;

    private DbsGroup pnd_Title_Fields;
    private DbsField pnd_Title_Fields_Pnd_Contract_Type;
    private DbsField pnd_Title_Fields_Pnd_Beg_Date;

    private DbsGroup pnd_Title_Fields__R_Field_36;
    private DbsField pnd_Title_Fields_Pnd_Beg_Month_Day;
    private DbsField pnd_Title_Fields_Pnd_End_Date;

    private DbsGroup pnd_Title_Fields__R_Field_37;
    private DbsField pnd_Title_Fields_Pnd_End_Month_Day;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Product_Code;
    private DbsField pnd_Report_Fields_Pnd_Beg_Range;
    private DbsField pnd_Report_Fields_Pnd_End_Range;
    private DbsField pnd_Report_Fields_Pnd_Last_Contract;
    private DbsField pnd_Report_Fields_Pnd_Range_Size;
    private DbsField pnd_Report_Fields_Pnd_Range_Left;
    private DbsField pnd_Report_Fields_Pnd_Percent_Left;
    private DbsField pnd_Report_Fields_Pnd_Used;
    private DbsField pnd_Report_Fields_Pnd_Next_Range;
    private DbsField pnd_Eof_New;
    private DbsField pnd_Eof_Old;
    private DbsField pnd_First_Time;
    private DbsField pnd_O_Alpha_Value;
    private DbsField pnd_N_Alpha_Value;
    private DbsField pnd_O_Alpha_Pos;
    private DbsField pnd_N_Alpha_Pos;
    private DbsField pnd_N_Record_Count;
    private DbsField pnd_O_Record_Count;
    private DbsField pnd_Alpha_Char;
    private DbsField pnd_Alpha_Pos;
    private DbsField pnd_Msg;
    private DbsField pnd_Msg_Max;
    private DbsField pnd_Msg_Sub;
    private DbsField pnd_Msg_Cnt;
    private DbsField pnd_Message;
    private DbsField pnd_Sub;
    private DbsField pnd_Record_Type;
    private DbsField pnd_Date_Range;
    private DbsField pnd_Total_Used;
    private DbsField pnd_Space_1;
    private DbsField pnd_Space_3;
    private DbsField pnd_Space_9;
    private DbsField pnd_Ratio;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAppa2075 = new PdaAppa2075(localVariables);

        // Local Variables
        pnd_Old_Record = localVariables.newFieldInRecord("pnd_Old_Record", "#OLD-RECORD", FieldType.STRING, 80);

        pnd_Old_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Old_Record__R_Field_1", "REDEFINE", pnd_Old_Record);
        pnd_Old_Record_Pnd_O_Record_Type = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Record_Type", "#O-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Old_Record_Pnd_O_Filler_1 = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Filler_1", "#O-FILLER-1", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Product_Code = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Product_Code", "#O-PRODUCT-CODE", FieldType.STRING, 
            10);
        pnd_Old_Record_Pnd_O_Beg_Range = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Beg_Range", "#O-BEG-RANGE", FieldType.STRING, 
            10);

        pnd_Old_Record__R_Field_2 = pnd_Old_Record__R_Field_1.newGroupInGroup("pnd_Old_Record__R_Field_2", "REDEFINE", pnd_Old_Record_Pnd_O_Beg_Range);
        pnd_Old_Record_Pnd_Beg_Prefix = pnd_Old_Record__R_Field_2.newFieldInGroup("pnd_Old_Record_Pnd_Beg_Prefix", "#BEG-PREFIX", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_Beg_Number = pnd_Old_Record__R_Field_2.newFieldInGroup("pnd_Old_Record_Pnd_Beg_Number", "#BEG-NUMBER", FieldType.NUMERIC, 6);

        pnd_Old_Record__R_Field_3 = pnd_Old_Record__R_Field_2.newGroupInGroup("pnd_Old_Record__R_Field_3", "REDEFINE", pnd_Old_Record_Pnd_Beg_Number);
        pnd_Old_Record_Pnd_Beg_Char = pnd_Old_Record__R_Field_3.newFieldArrayInGroup("pnd_Old_Record_Pnd_Beg_Char", "#BEG-CHAR", FieldType.STRING, 1, 
            new DbsArrayController(1, 6));

        pnd_Old_Record__R_Field_4 = pnd_Old_Record__R_Field_2.newGroupInGroup("pnd_Old_Record__R_Field_4", "REDEFINE", pnd_Old_Record_Pnd_Beg_Number);
        pnd_Old_Record_Pnd_O_Beg_2_4 = pnd_Old_Record__R_Field_4.newFieldInGroup("pnd_Old_Record_Pnd_O_Beg_2_4", "#O-BEG-2-4", FieldType.STRING, 3);
        pnd_Old_Record_Pnd_O_Beg_5_7 = pnd_Old_Record__R_Field_4.newFieldInGroup("pnd_Old_Record_Pnd_O_Beg_5_7", "#O-BEG-5-7", FieldType.STRING, 3);

        pnd_Old_Record__R_Field_5 = pnd_Old_Record__R_Field_4.newGroupInGroup("pnd_Old_Record__R_Field_5", "REDEFINE", pnd_Old_Record_Pnd_O_Beg_5_7);
        pnd_Old_Record_Pnd_O_Beg_5 = pnd_Old_Record__R_Field_5.newFieldInGroup("pnd_Old_Record_Pnd_O_Beg_5", "#O-BEG-5", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Beg_6_7 = pnd_Old_Record__R_Field_5.newFieldInGroup("pnd_Old_Record_Pnd_O_Beg_6_7", "#O-BEG-6-7", FieldType.STRING, 2);

        pnd_Old_Record__R_Field_6 = pnd_Old_Record__R_Field_5.newGroupInGroup("pnd_Old_Record__R_Field_6", "REDEFINE", pnd_Old_Record_Pnd_O_Beg_6_7);
        pnd_Old_Record_Pnd_O_Beg_6 = pnd_Old_Record__R_Field_6.newFieldInGroup("pnd_Old_Record_Pnd_O_Beg_6", "#O-BEG-6", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Beg_7 = pnd_Old_Record__R_Field_6.newFieldInGroup("pnd_Old_Record_Pnd_O_Beg_7", "#O-BEG-7", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_End_Range = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_End_Range", "#O-END-RANGE", FieldType.STRING, 
            10);

        pnd_Old_Record__R_Field_7 = pnd_Old_Record__R_Field_1.newGroupInGroup("pnd_Old_Record__R_Field_7", "REDEFINE", pnd_Old_Record_Pnd_O_End_Range);
        pnd_Old_Record_Pnd_End_Prefix = pnd_Old_Record__R_Field_7.newFieldInGroup("pnd_Old_Record_Pnd_End_Prefix", "#END-PREFIX", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_End_Number = pnd_Old_Record__R_Field_7.newFieldInGroup("pnd_Old_Record_Pnd_End_Number", "#END-NUMBER", FieldType.NUMERIC, 6);

        pnd_Old_Record__R_Field_8 = pnd_Old_Record__R_Field_7.newGroupInGroup("pnd_Old_Record__R_Field_8", "REDEFINE", pnd_Old_Record_Pnd_End_Number);
        pnd_Old_Record_Pnd_End_Char = pnd_Old_Record__R_Field_8.newFieldArrayInGroup("pnd_Old_Record_Pnd_End_Char", "#END-CHAR", FieldType.STRING, 1, 
            new DbsArrayController(1, 6));

        pnd_Old_Record__R_Field_9 = pnd_Old_Record__R_Field_7.newGroupInGroup("pnd_Old_Record__R_Field_9", "REDEFINE", pnd_Old_Record_Pnd_End_Number);
        pnd_Old_Record_Pnd_O_End_2_4 = pnd_Old_Record__R_Field_9.newFieldInGroup("pnd_Old_Record_Pnd_O_End_2_4", "#O-END-2-4", FieldType.STRING, 3);
        pnd_Old_Record_Pnd_O_End_5_7 = pnd_Old_Record__R_Field_9.newFieldInGroup("pnd_Old_Record_Pnd_O_End_5_7", "#O-END-5-7", FieldType.STRING, 3);

        pnd_Old_Record__R_Field_10 = pnd_Old_Record__R_Field_9.newGroupInGroup("pnd_Old_Record__R_Field_10", "REDEFINE", pnd_Old_Record_Pnd_O_End_5_7);
        pnd_Old_Record_Pnd_O_End_5 = pnd_Old_Record__R_Field_10.newFieldInGroup("pnd_Old_Record_Pnd_O_End_5", "#O-END-5", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_End_6_7 = pnd_Old_Record__R_Field_10.newFieldInGroup("pnd_Old_Record_Pnd_O_End_6_7", "#O-END-6-7", FieldType.STRING, 2);

        pnd_Old_Record__R_Field_11 = pnd_Old_Record__R_Field_10.newGroupInGroup("pnd_Old_Record__R_Field_11", "REDEFINE", pnd_Old_Record_Pnd_O_End_6_7);
        pnd_Old_Record_Pnd_O_End_6 = pnd_Old_Record__R_Field_11.newFieldInGroup("pnd_Old_Record_Pnd_O_End_6", "#O-END-6", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_End_7 = pnd_Old_Record__R_Field_11.newFieldInGroup("pnd_Old_Record_Pnd_O_End_7", "#O-END-7", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Last_Contract = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Last_Contract", "#O-LAST-CONTRACT", FieldType.STRING, 
            10);

        pnd_Old_Record__R_Field_12 = pnd_Old_Record__R_Field_1.newGroupInGroup("pnd_Old_Record__R_Field_12", "REDEFINE", pnd_Old_Record_Pnd_O_Last_Contract);
        pnd_Old_Record_Pnd_Last_Prefix = pnd_Old_Record__R_Field_12.newFieldInGroup("pnd_Old_Record_Pnd_Last_Prefix", "#LAST-PREFIX", FieldType.STRING, 
            1);
        pnd_Old_Record_Pnd_Last_Number = pnd_Old_Record__R_Field_12.newFieldInGroup("pnd_Old_Record_Pnd_Last_Number", "#LAST-NUMBER", FieldType.NUMERIC, 
            6);

        pnd_Old_Record__R_Field_13 = pnd_Old_Record__R_Field_12.newGroupInGroup("pnd_Old_Record__R_Field_13", "REDEFINE", pnd_Old_Record_Pnd_Last_Number);
        pnd_Old_Record_Pnd_Last_Char = pnd_Old_Record__R_Field_13.newFieldArrayInGroup("pnd_Old_Record_Pnd_Last_Char", "#LAST-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1, 6));

        pnd_Old_Record__R_Field_14 = pnd_Old_Record__R_Field_12.newGroupInGroup("pnd_Old_Record__R_Field_14", "REDEFINE", pnd_Old_Record_Pnd_Last_Number);
        pnd_Old_Record_Pnd_O_Last_2_4 = pnd_Old_Record__R_Field_14.newFieldInGroup("pnd_Old_Record_Pnd_O_Last_2_4", "#O-LAST-2-4", FieldType.STRING, 3);
        pnd_Old_Record_Pnd_O_Last_5_7 = pnd_Old_Record__R_Field_14.newFieldInGroup("pnd_Old_Record_Pnd_O_Last_5_7", "#O-LAST-5-7", FieldType.STRING, 3);

        pnd_Old_Record__R_Field_15 = pnd_Old_Record__R_Field_14.newGroupInGroup("pnd_Old_Record__R_Field_15", "REDEFINE", pnd_Old_Record_Pnd_O_Last_5_7);
        pnd_Old_Record_Pnd_O_Last_5 = pnd_Old_Record__R_Field_15.newFieldInGroup("pnd_Old_Record_Pnd_O_Last_5", "#O-LAST-5", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Last_6_7 = pnd_Old_Record__R_Field_15.newFieldInGroup("pnd_Old_Record_Pnd_O_Last_6_7", "#O-LAST-6-7", FieldType.STRING, 2);

        pnd_Old_Record__R_Field_16 = pnd_Old_Record__R_Field_15.newGroupInGroup("pnd_Old_Record__R_Field_16", "REDEFINE", pnd_Old_Record_Pnd_O_Last_6_7);
        pnd_Old_Record_Pnd_O_Last_6 = pnd_Old_Record__R_Field_16.newFieldInGroup("pnd_Old_Record_Pnd_O_Last_6", "#O-LAST-6", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Last_7 = pnd_Old_Record__R_Field_16.newFieldInGroup("pnd_Old_Record_Pnd_O_Last_7", "#O-LAST-7", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Range_Size = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Range_Size", "#O-RANGE-SIZE", FieldType.NUMERIC, 
            6);
        pnd_Old_Record_Pnd_O_Filler_2 = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Filler_2", "#O-FILLER-2", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Range_Left = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Range_Left", "#O-RANGE-LEFT", FieldType.NUMERIC, 
            6);
        pnd_Old_Record_Pnd_O_Filler_3 = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Filler_3", "#O-FILLER-3", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Next_Indicator = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Next_Indicator", "#O-NEXT-INDICATOR", FieldType.STRING, 
            1);
        pnd_Old_Record_Pnd_O_Filler_4 = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Filler_4", "#O-FILLER-4", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_O_Start_Date = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Start_Date", "#O-START-DATE", FieldType.STRING, 
            8);
        pnd_Old_Record_Pnd_O_Filler_5 = pnd_Old_Record__R_Field_1.newFieldInGroup("pnd_Old_Record_Pnd_O_Filler_5", "#O-FILLER-5", FieldType.STRING, 14);

        pnd_Old_Record__R_Field_17 = localVariables.newGroupInRecord("pnd_Old_Record__R_Field_17", "REDEFINE", pnd_Old_Record);
        pnd_Old_Record_Pnd_H_Record_Type = pnd_Old_Record__R_Field_17.newFieldInGroup("pnd_Old_Record_Pnd_H_Record_Type", "#H-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Old_Record_Pnd_H_Filler_1 = pnd_Old_Record__R_Field_17.newFieldInGroup("pnd_Old_Record_Pnd_H_Filler_1", "#H-FILLER-1", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_H_Contract_Type = pnd_Old_Record__R_Field_17.newFieldInGroup("pnd_Old_Record_Pnd_H_Contract_Type", "#H-CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Old_Record_Pnd_H_Filler_2 = pnd_Old_Record__R_Field_17.newFieldInGroup("pnd_Old_Record_Pnd_H_Filler_2", "#H-FILLER-2", FieldType.STRING, 1);
        pnd_Old_Record_Pnd_H_Date = pnd_Old_Record__R_Field_17.newFieldInGroup("pnd_Old_Record_Pnd_H_Date", "#H-DATE", FieldType.STRING, 10);
        pnd_Old_Record_Pnd_H_Filler_3 = pnd_Old_Record__R_Field_17.newFieldInGroup("pnd_Old_Record_Pnd_H_Filler_3", "#H-FILLER-3", FieldType.STRING, 65);
        pnd_New_Record = localVariables.newFieldInRecord("pnd_New_Record", "#NEW-RECORD", FieldType.STRING, 80);

        pnd_New_Record__R_Field_18 = localVariables.newGroupInRecord("pnd_New_Record__R_Field_18", "REDEFINE", pnd_New_Record);
        pnd_New_Record_Pnd_N_Record_Type = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Record_Type", "#N-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_New_Record_Pnd_N_Filler_1 = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Filler_1", "#N-FILLER-1", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Product_Code = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Product_Code", "#N-PRODUCT-CODE", FieldType.STRING, 
            10);
        pnd_New_Record_Pnd_N_Beg_Range = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Beg_Range", "#N-BEG-RANGE", FieldType.STRING, 
            10);

        pnd_New_Record__R_Field_19 = pnd_New_Record__R_Field_18.newGroupInGroup("pnd_New_Record__R_Field_19", "REDEFINE", pnd_New_Record_Pnd_N_Beg_Range);
        pnd_New_Record_Pnd_Beg_Prefix = pnd_New_Record__R_Field_19.newFieldInGroup("pnd_New_Record_Pnd_Beg_Prefix", "#BEG-PREFIX", FieldType.STRING, 1);
        pnd_New_Record_Pnd_Beg_Number = pnd_New_Record__R_Field_19.newFieldInGroup("pnd_New_Record_Pnd_Beg_Number", "#BEG-NUMBER", FieldType.NUMERIC, 
            6);

        pnd_New_Record__R_Field_20 = pnd_New_Record__R_Field_19.newGroupInGroup("pnd_New_Record__R_Field_20", "REDEFINE", pnd_New_Record_Pnd_Beg_Number);
        pnd_New_Record_Pnd_Beg_Char = pnd_New_Record__R_Field_20.newFieldArrayInGroup("pnd_New_Record_Pnd_Beg_Char", "#BEG-CHAR", FieldType.STRING, 1, 
            new DbsArrayController(1, 6));

        pnd_New_Record__R_Field_21 = pnd_New_Record__R_Field_19.newGroupInGroup("pnd_New_Record__R_Field_21", "REDEFINE", pnd_New_Record_Pnd_Beg_Number);
        pnd_New_Record_Pnd_N_Beg_2_4 = pnd_New_Record__R_Field_21.newFieldInGroup("pnd_New_Record_Pnd_N_Beg_2_4", "#N-BEG-2-4", FieldType.STRING, 3);
        pnd_New_Record_Pnd_N_Beg_5_7 = pnd_New_Record__R_Field_21.newFieldInGroup("pnd_New_Record_Pnd_N_Beg_5_7", "#N-BEG-5-7", FieldType.STRING, 3);

        pnd_New_Record__R_Field_22 = pnd_New_Record__R_Field_21.newGroupInGroup("pnd_New_Record__R_Field_22", "REDEFINE", pnd_New_Record_Pnd_N_Beg_5_7);
        pnd_New_Record_Pnd_N_Beg_5 = pnd_New_Record__R_Field_22.newFieldInGroup("pnd_New_Record_Pnd_N_Beg_5", "#N-BEG-5", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Beg_6_7 = pnd_New_Record__R_Field_22.newFieldInGroup("pnd_New_Record_Pnd_N_Beg_6_7", "#N-BEG-6-7", FieldType.STRING, 2);

        pnd_New_Record__R_Field_23 = pnd_New_Record__R_Field_22.newGroupInGroup("pnd_New_Record__R_Field_23", "REDEFINE", pnd_New_Record_Pnd_N_Beg_6_7);
        pnd_New_Record_Pnd_N_Beg_6 = pnd_New_Record__R_Field_23.newFieldInGroup("pnd_New_Record_Pnd_N_Beg_6", "#N-BEG-6", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Beg_7 = pnd_New_Record__R_Field_23.newFieldInGroup("pnd_New_Record_Pnd_N_Beg_7", "#N-BEG-7", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_End_Range = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_End_Range", "#N-END-RANGE", FieldType.STRING, 
            10);

        pnd_New_Record__R_Field_24 = pnd_New_Record__R_Field_18.newGroupInGroup("pnd_New_Record__R_Field_24", "REDEFINE", pnd_New_Record_Pnd_N_End_Range);
        pnd_New_Record_Pnd_End_Prefix = pnd_New_Record__R_Field_24.newFieldInGroup("pnd_New_Record_Pnd_End_Prefix", "#END-PREFIX", FieldType.STRING, 1);
        pnd_New_Record_Pnd_End_Number = pnd_New_Record__R_Field_24.newFieldInGroup("pnd_New_Record_Pnd_End_Number", "#END-NUMBER", FieldType.NUMERIC, 
            6);

        pnd_New_Record__R_Field_25 = pnd_New_Record__R_Field_24.newGroupInGroup("pnd_New_Record__R_Field_25", "REDEFINE", pnd_New_Record_Pnd_End_Number);
        pnd_New_Record_Pnd_End_Char = pnd_New_Record__R_Field_25.newFieldArrayInGroup("pnd_New_Record_Pnd_End_Char", "#END-CHAR", FieldType.STRING, 1, 
            new DbsArrayController(1, 6));

        pnd_New_Record__R_Field_26 = pnd_New_Record__R_Field_24.newGroupInGroup("pnd_New_Record__R_Field_26", "REDEFINE", pnd_New_Record_Pnd_End_Number);
        pnd_New_Record_Pnd_N_End_2_4 = pnd_New_Record__R_Field_26.newFieldInGroup("pnd_New_Record_Pnd_N_End_2_4", "#N-END-2-4", FieldType.STRING, 3);
        pnd_New_Record_Pnd_N_End_5_7 = pnd_New_Record__R_Field_26.newFieldInGroup("pnd_New_Record_Pnd_N_End_5_7", "#N-END-5-7", FieldType.STRING, 3);

        pnd_New_Record__R_Field_27 = pnd_New_Record__R_Field_26.newGroupInGroup("pnd_New_Record__R_Field_27", "REDEFINE", pnd_New_Record_Pnd_N_End_5_7);
        pnd_New_Record_Pnd_N_End_5 = pnd_New_Record__R_Field_27.newFieldInGroup("pnd_New_Record_Pnd_N_End_5", "#N-END-5", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_End_6_7 = pnd_New_Record__R_Field_27.newFieldInGroup("pnd_New_Record_Pnd_N_End_6_7", "#N-END-6-7", FieldType.STRING, 2);

        pnd_New_Record__R_Field_28 = pnd_New_Record__R_Field_27.newGroupInGroup("pnd_New_Record__R_Field_28", "REDEFINE", pnd_New_Record_Pnd_N_End_6_7);
        pnd_New_Record_Pnd_N_End_6 = pnd_New_Record__R_Field_28.newFieldInGroup("pnd_New_Record_Pnd_N_End_6", "#N-END-6", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_End_7 = pnd_New_Record__R_Field_28.newFieldInGroup("pnd_New_Record_Pnd_N_End_7", "#N-END-7", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Last_Contract = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Last_Contract", "#N-LAST-CONTRACT", FieldType.STRING, 
            10);

        pnd_New_Record__R_Field_29 = pnd_New_Record__R_Field_18.newGroupInGroup("pnd_New_Record__R_Field_29", "REDEFINE", pnd_New_Record_Pnd_N_Last_Contract);
        pnd_New_Record_Pnd_Last_Prefix = pnd_New_Record__R_Field_29.newFieldInGroup("pnd_New_Record_Pnd_Last_Prefix", "#LAST-PREFIX", FieldType.STRING, 
            1);
        pnd_New_Record_Pnd_Last_Number = pnd_New_Record__R_Field_29.newFieldInGroup("pnd_New_Record_Pnd_Last_Number", "#LAST-NUMBER", FieldType.NUMERIC, 
            6);

        pnd_New_Record__R_Field_30 = pnd_New_Record__R_Field_29.newGroupInGroup("pnd_New_Record__R_Field_30", "REDEFINE", pnd_New_Record_Pnd_Last_Number);
        pnd_New_Record_Pnd_Last_Char = pnd_New_Record__R_Field_30.newFieldArrayInGroup("pnd_New_Record_Pnd_Last_Char", "#LAST-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1, 6));

        pnd_New_Record__R_Field_31 = pnd_New_Record__R_Field_29.newGroupInGroup("pnd_New_Record__R_Field_31", "REDEFINE", pnd_New_Record_Pnd_Last_Number);
        pnd_New_Record_Pnd_N_Last_2_4 = pnd_New_Record__R_Field_31.newFieldInGroup("pnd_New_Record_Pnd_N_Last_2_4", "#N-LAST-2-4", FieldType.STRING, 3);
        pnd_New_Record_Pnd_N_Last_5_7 = pnd_New_Record__R_Field_31.newFieldInGroup("pnd_New_Record_Pnd_N_Last_5_7", "#N-LAST-5-7", FieldType.STRING, 3);

        pnd_New_Record__R_Field_32 = pnd_New_Record__R_Field_31.newGroupInGroup("pnd_New_Record__R_Field_32", "REDEFINE", pnd_New_Record_Pnd_N_Last_5_7);
        pnd_New_Record_Pnd_N_Last_5 = pnd_New_Record__R_Field_32.newFieldInGroup("pnd_New_Record_Pnd_N_Last_5", "#N-LAST-5", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Last_6_7 = pnd_New_Record__R_Field_32.newFieldInGroup("pnd_New_Record_Pnd_N_Last_6_7", "#N-LAST-6-7", FieldType.STRING, 2);

        pnd_New_Record__R_Field_33 = pnd_New_Record__R_Field_32.newGroupInGroup("pnd_New_Record__R_Field_33", "REDEFINE", pnd_New_Record_Pnd_N_Last_6_7);
        pnd_New_Record_Pnd_N_Last_6 = pnd_New_Record__R_Field_33.newFieldInGroup("pnd_New_Record_Pnd_N_Last_6", "#N-LAST-6", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Last_7 = pnd_New_Record__R_Field_33.newFieldInGroup("pnd_New_Record_Pnd_N_Last_7", "#N-LAST-7", FieldType.STRING, 1);

        pnd_New_Record__R_Field_34 = pnd_New_Record__R_Field_18.newGroupInGroup("pnd_New_Record__R_Field_34", "REDEFINE", pnd_New_Record_Pnd_N_Last_Contract);
        pnd_New_Record_Pnd_N_Contract_Char = pnd_New_Record__R_Field_34.newFieldArrayInGroup("pnd_New_Record_Pnd_N_Contract_Char", "#N-CONTRACT-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_New_Record_Pnd_N_Range_Size = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Range_Size", "#N-RANGE-SIZE", FieldType.NUMERIC, 
            6);
        pnd_New_Record_Pnd_N_Filler_2 = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Filler_2", "#N-FILLER-2", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Range_Left = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Range_Left", "#N-RANGE-LEFT", FieldType.NUMERIC, 
            6);
        pnd_New_Record_Pnd_N_Filler_3 = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Filler_3", "#N-FILLER-3", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Next_Indicator = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Next_Indicator", "#N-NEXT-INDICATOR", FieldType.STRING, 
            1);
        pnd_New_Record_Pnd_N_Filler_4 = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Filler_4", "#N-FILLER-4", FieldType.STRING, 1);
        pnd_New_Record_Pnd_N_Start_Date = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Start_Date", "#N-START-DATE", FieldType.STRING, 
            8);
        pnd_New_Record_Pnd_N_Filler_5 = pnd_New_Record__R_Field_18.newFieldInGroup("pnd_New_Record_Pnd_N_Filler_5", "#N-FILLER-5", FieldType.STRING, 14);

        pnd_New_Record__R_Field_35 = localVariables.newGroupInRecord("pnd_New_Record__R_Field_35", "REDEFINE", pnd_New_Record);
        pnd_New_Record_Pnd_H_Record_Type = pnd_New_Record__R_Field_35.newFieldInGroup("pnd_New_Record_Pnd_H_Record_Type", "#H-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_New_Record_Pnd_H_Filler_1 = pnd_New_Record__R_Field_35.newFieldInGroup("pnd_New_Record_Pnd_H_Filler_1", "#H-FILLER-1", FieldType.STRING, 1);
        pnd_New_Record_Pnd_H_Contract_Type = pnd_New_Record__R_Field_35.newFieldInGroup("pnd_New_Record_Pnd_H_Contract_Type", "#H-CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_New_Record_Pnd_H_Filler_2 = pnd_New_Record__R_Field_35.newFieldInGroup("pnd_New_Record_Pnd_H_Filler_2", "#H-FILLER-2", FieldType.STRING, 1);
        pnd_New_Record_Pnd_H_Date = pnd_New_Record__R_Field_35.newFieldInGroup("pnd_New_Record_Pnd_H_Date", "#H-DATE", FieldType.STRING, 10);
        pnd_New_Record_Pnd_H_Filler_3 = pnd_New_Record__R_Field_35.newFieldInGroup("pnd_New_Record_Pnd_H_Filler_3", "#H-FILLER-3", FieldType.STRING, 65);

        pnd_Title_Fields = localVariables.newGroupInRecord("pnd_Title_Fields", "#TITLE-FIELDS");
        pnd_Title_Fields_Pnd_Contract_Type = pnd_Title_Fields.newFieldInGroup("pnd_Title_Fields_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Title_Fields_Pnd_Beg_Date = pnd_Title_Fields.newFieldInGroup("pnd_Title_Fields_Pnd_Beg_Date", "#BEG-DATE", FieldType.STRING, 10);

        pnd_Title_Fields__R_Field_36 = pnd_Title_Fields.newGroupInGroup("pnd_Title_Fields__R_Field_36", "REDEFINE", pnd_Title_Fields_Pnd_Beg_Date);
        pnd_Title_Fields_Pnd_Beg_Month_Day = pnd_Title_Fields__R_Field_36.newFieldInGroup("pnd_Title_Fields_Pnd_Beg_Month_Day", "#BEG-MONTH-DAY", FieldType.STRING, 
            5);
        pnd_Title_Fields_Pnd_End_Date = pnd_Title_Fields.newFieldInGroup("pnd_Title_Fields_Pnd_End_Date", "#END-DATE", FieldType.STRING, 10);

        pnd_Title_Fields__R_Field_37 = pnd_Title_Fields.newGroupInGroup("pnd_Title_Fields__R_Field_37", "REDEFINE", pnd_Title_Fields_Pnd_End_Date);
        pnd_Title_Fields_Pnd_End_Month_Day = pnd_Title_Fields__R_Field_37.newFieldInGroup("pnd_Title_Fields_Pnd_End_Month_Day", "#END-MONTH-DAY", FieldType.STRING, 
            5);

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Product_Code = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Product_Code", "#PRODUCT-CODE", FieldType.STRING, 
            10);
        pnd_Report_Fields_Pnd_Beg_Range = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Beg_Range", "#BEG-RANGE", FieldType.STRING, 10);
        pnd_Report_Fields_Pnd_End_Range = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_End_Range", "#END-RANGE", FieldType.STRING, 10);
        pnd_Report_Fields_Pnd_Last_Contract = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Last_Contract", "#LAST-CONTRACT", FieldType.STRING, 
            10);
        pnd_Report_Fields_Pnd_Range_Size = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Range_Size", "#RANGE-SIZE", FieldType.NUMERIC, 6);
        pnd_Report_Fields_Pnd_Range_Left = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Range_Left", "#RANGE-LEFT", FieldType.NUMERIC, 6);
        pnd_Report_Fields_Pnd_Percent_Left = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Percent_Left", "#PERCENT-LEFT", FieldType.NUMERIC, 
            5, 2);
        pnd_Report_Fields_Pnd_Used = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Used", "#USED", FieldType.NUMERIC, 6);
        pnd_Report_Fields_Pnd_Next_Range = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Next_Range", "#NEXT-RANGE", FieldType.STRING, 3);
        pnd_Eof_New = localVariables.newFieldInRecord("pnd_Eof_New", "#EOF-NEW", FieldType.BOOLEAN, 1);
        pnd_Eof_Old = localVariables.newFieldInRecord("pnd_Eof_Old", "#EOF-OLD", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_O_Alpha_Value = localVariables.newFieldInRecord("pnd_O_Alpha_Value", "#O-ALPHA-VALUE", FieldType.STRING, 3);
        pnd_N_Alpha_Value = localVariables.newFieldInRecord("pnd_N_Alpha_Value", "#N-ALPHA-VALUE", FieldType.STRING, 3);
        pnd_O_Alpha_Pos = localVariables.newFieldInRecord("pnd_O_Alpha_Pos", "#O-ALPHA-POS", FieldType.NUMERIC, 1);
        pnd_N_Alpha_Pos = localVariables.newFieldInRecord("pnd_N_Alpha_Pos", "#N-ALPHA-POS", FieldType.NUMERIC, 1);
        pnd_N_Record_Count = localVariables.newFieldInRecord("pnd_N_Record_Count", "#N-RECORD-COUNT", FieldType.NUMERIC, 9);
        pnd_O_Record_Count = localVariables.newFieldInRecord("pnd_O_Record_Count", "#O-RECORD-COUNT", FieldType.NUMERIC, 9);
        pnd_Alpha_Char = localVariables.newFieldInRecord("pnd_Alpha_Char", "#ALPHA-CHAR", FieldType.STRING, 1);
        pnd_Alpha_Pos = localVariables.newFieldInRecord("pnd_Alpha_Pos", "#ALPHA-POS", FieldType.NUMERIC, 1);
        pnd_Msg = localVariables.newFieldArrayInRecord("pnd_Msg", "#MSG", FieldType.STRING, 50, new DbsArrayController(1, 5));
        pnd_Msg_Max = localVariables.newFieldInRecord("pnd_Msg_Max", "#MSG-MAX", FieldType.NUMERIC, 5);
        pnd_Msg_Sub = localVariables.newFieldInRecord("pnd_Msg_Sub", "#MSG-SUB", FieldType.NUMERIC, 5);
        pnd_Msg_Cnt = localVariables.newFieldInRecord("pnd_Msg_Cnt", "#MSG-CNT", FieldType.NUMERIC, 5);
        pnd_Message = localVariables.newFieldInRecord("pnd_Message", "#MESSAGE", FieldType.STRING, 50);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 3);
        pnd_Record_Type = localVariables.newFieldInRecord("pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 1);
        pnd_Date_Range = localVariables.newFieldInRecord("pnd_Date_Range", "#DATE-RANGE", FieldType.STRING, 10);
        pnd_Total_Used = localVariables.newFieldInRecord("pnd_Total_Used", "#TOTAL-USED", FieldType.NUMERIC, 9);
        pnd_Space_1 = localVariables.newFieldInRecord("pnd_Space_1", "#SPACE-1", FieldType.STRING, 1);
        pnd_Space_3 = localVariables.newFieldInRecord("pnd_Space_3", "#SPACE-3", FieldType.STRING, 3);
        pnd_Space_9 = localVariables.newFieldInRecord("pnd_Space_9", "#SPACE-9", FieldType.STRING, 9);
        pnd_Ratio = localVariables.newFieldInRecord("pnd_Ratio", "#RATIO", FieldType.NUMERIC, 11, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Msg_Max.setInitialValue(5);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2071() throws Exception
    {
        super("Appb2071");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 65
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 40X 'WEEKLY' #CONTRACT-TYPE 'CONTRACT RANGE USAGE REPORT' 37X *DATX ( EM = MM/DD/YYYY ) / 54X #BEG-DATE '-' #END-DATE /
        //*  READ FIRST RECORD (HEADER)
                                                                                                                                                                          //Natural: PERFORM READ-OLD-RANGE-RECORD
        sub_Read_Old_Range_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Old_Record_Pnd_H_Record_Type.equals("H")))                                                                                                      //Natural: IF #OLD-RECORD.#H-RECORD-TYPE = 'H'
        {
            //*  READ NEXT RECORD  (DETAIL)
                                                                                                                                                                          //Natural: PERFORM READ-OLD-RANGE-RECORD
            sub_Read_Old_Range_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ FIRST RECORD (HEADER)
                                                                                                                                                                          //Natural: PERFORM READ-NEW-RANGE-RECORD
        sub_Read_New_Range_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_New_Record_Pnd_H_Record_Type.equals("H")))                                                                                                      //Natural: IF #NEW-RECORD.#H-RECORD-TYPE = 'H'
        {
            //*  READ NEXT RECORD  (DETAIL)
                                                                                                                                                                          //Natural: PERFORM READ-NEW-RANGE-RECORD
            sub_Read_New_Range_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Record_Type.setValue("H");                                                                                                                                    //Natural: ASSIGN #RECORD-TYPE := 'H'
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-ACCUM-RECORD
        sub_Write_Header_Accum_Record();
        if (condition(Global.isEscape())) {return;}
        pnd_Date_Range.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Title_Fields_Pnd_Beg_Month_Day, pnd_Title_Fields_Pnd_End_Month_Day));                 //Natural: COMPRESS #BEG-MONTH-DAY #END-MONTH-DAY INTO #DATE-RANGE LEAVING NO SPACE
        pnd_Record_Type.setValue("D");                                                                                                                                    //Natural: ASSIGN #RECORD-TYPE := 'D'
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Eof_New.getBoolean() && pnd_Eof_Old.getBoolean())) {break;}                                                                                 //Natural: UNTIL #EOF-NEW AND #EOF-OLD
            pnd_Msg_Sub.reset();                                                                                                                                          //Natural: RESET #MSG-SUB #MSG ( * ) #MESSAGE
            pnd_Msg.getValue("*").reset();
            pnd_Message.reset();
            //*  NO MATCHING NEW RECORD
            short decideConditionsMet375 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #N-PRODUCT-CODE < #O-PRODUCT-CODE OR #EOF-OLD AND NOT #EOF-NEW
            if (condition((pnd_New_Record_Pnd_N_Product_Code.less(pnd_Old_Record_Pnd_O_Product_Code) || (pnd_Eof_Old.getBoolean() && ! (pnd_Eof_New.getBoolean())))))
            {
                decideConditionsMet375++;
                pnd_Report_Fields.reset();                                                                                                                                //Natural: RESET #REPORT-FIELDS #TOTAL-USED
                pnd_Total_Used.reset();
                                                                                                                                                                          //Natural: PERFORM MOVE-OLD-FIELDS
                sub_Move_Old_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Message.setValue("PRODUCT CODE NOT ON NEW FILE");                                                                                                     //Natural: ASSIGN #MESSAGE := 'PRODUCT CODE NOT ON NEW FILE'
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-MSG-ARRAY
                sub_Move_To_Msg_Array();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-RECORD
                sub_Write_Detail_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-ACCUM-RECORD
                sub_Write_Detail_Accum_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  NO MATCHING OLD RECORD
                                                                                                                                                                          //Natural: PERFORM READ-NEW-RANGE-RECORD
                sub_Read_New_Range_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #N-PRODUCT-CODE > #O-PRODUCT-CODE OR #EOF-NEW AND NOT #EOF-OLD
            else if (condition((pnd_New_Record_Pnd_N_Product_Code.greater(pnd_Old_Record_Pnd_O_Product_Code) || (pnd_Eof_New.getBoolean() && ! (pnd_Eof_Old.getBoolean())))))
            {
                decideConditionsMet375++;
                pnd_Report_Fields.reset();                                                                                                                                //Natural: RESET #REPORT-FIELDS #TOTAL-USED
                pnd_Total_Used.reset();
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-FIELDS
                sub_Move_New_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Message.setValue("PRODUCT CODE NOT ON OLD FILE");                                                                                                     //Natural: ASSIGN #MESSAGE := 'PRODUCT CODE NOT ON OLD FILE'
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-MSG-ARRAY
                sub_Move_To_Msg_Array();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-RECORD
                sub_Write_Detail_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-ACCUM-RECORD
                sub_Write_Detail_Accum_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PRODUCT MATCH
                                                                                                                                                                          //Natural: PERFORM READ-OLD-RANGE-RECORD
                sub_Read_Old_Range_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM PRODUCT-MATCH-PROCESSING
                sub_Product_Match_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-NEW-RANGE-RECORD
                sub_Read_New_Range_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM READ-OLD-RANGE-RECORD
                sub_Read_Old_Range_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Report_Fields.reset();                                                                                                                                    //Natural: RESET #REPORT-FIELDS
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,"TOTAL OLD CONTRACT RANGE RECORD:",pnd_O_Record_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL NEW CONTRACT RANGE RECORD:",pnd_N_Record_Count,  //Natural: WRITE ( 1 ) // 'TOTAL OLD CONTRACT RANGE RECORD:' #O-RECORD-COUNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'TOTAL NEW CONTRACT RANGE RECORD:' #N-RECORD-COUNT ( EM = ZZZ,ZZZ,ZZ9 ) // 56X '*** END OF REPORT ***'
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new ColumnSpacing(56),"*** END OF REPORT ***");
        if (Global.isEscape()) return;
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-OLD-RANGE-RECORD
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NEW-RANGE-RECORD
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRODUCT-MATCH-PROCESSING
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-OLD-FIELDS
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-NEW-FIELDS
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-RANGE-VALUES-O
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-RANGE-VALUES-N
        //*  ********************************************************************
        //*  * FINISHED USING CURR RANGE AND BEGAN USING NEXT RANGE             *
        //*  ********************************************************************
        //*  ********************************************************************
        //*  * CONTRACTS USED IN SAME CURR RANGE AS PREVIOUS WEEK               *
        //*  ********************************************************************
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-MSG-ARRAY
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-ALPHA-VALUE
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-RECORD
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER-ACCUM-RECORD
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-ACCUM-RECORD
    }
    private void sub_Read_Old_Range_Record() throws Exception                                                                                                             //Natural: READ-OLD-RANGE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        getWorkFiles().read(1, pnd_Old_Record);                                                                                                                           //Natural: READ WORK FILE 01 ONCE #OLD-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Eof_Old.setValue(true);                                                                                                                                   //Natural: ASSIGN #EOF-OLD := TRUE
            if (condition(pnd_O_Record_Count.equals(getZero())))                                                                                                          //Natural: IF #O-RECORD-COUNT = 0
            {
                getReports().write(1, "EMPTY 'OLD' INPUT FILE");                                                                                                          //Natural: WRITE ( 1 ) 'EMPTY "OLD" INPUT FILE'
                if (Global.isEscape()) return;
                //*  JB2
                DbsUtil.terminate(32);  if (true) return;                                                                                                                 //Natural: TERMINATE 32
            }                                                                                                                                                             //Natural: END-IF
            //*  (2850)
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  HEADER RECORD
        short decideConditionsMet470 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #OLD-RECORD.#H-RECORD-TYPE;//Natural: VALUE 'H'
        if (condition((pnd_Old_Record_Pnd_H_Record_Type.equals("H"))))
        {
            decideConditionsMet470++;
            //*  DETAIL RECORD
            pnd_Title_Fields_Pnd_Contract_Type.setValue(pnd_Old_Record_Pnd_H_Contract_Type);                                                                              //Natural: MOVE #OLD-RECORD.#H-CONTRACT-TYPE TO #CONTRACT-TYPE
            pnd_Title_Fields_Pnd_Beg_Date.setValue(pnd_Old_Record_Pnd_H_Date);                                                                                            //Natural: ASSIGN #BEG-DATE := #OLD-RECORD.#H-DATE
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((pnd_Old_Record_Pnd_H_Record_Type.equals("D"))))
        {
            decideConditionsMet470++;
            pnd_O_Record_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #O-RECORD-COUNT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Read_New_Range_Record() throws Exception                                                                                                             //Natural: READ-NEW-RANGE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        getWorkFiles().read(2, pnd_New_Record);                                                                                                                           //Natural: READ WORK FILE 02 ONCE #NEW-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Eof_New.setValue(true);                                                                                                                                   //Natural: ASSIGN #EOF-NEW := TRUE
            if (condition(pnd_N_Record_Count.equals(getZero())))                                                                                                          //Natural: IF #N-RECORD-COUNT = 0
            {
                getReports().write(1, "EMPTY 'NEW' INPUT FILE");                                                                                                          //Natural: WRITE ( 1 ) 'EMPTY "NEW" INPUT FILE'
                if (Global.isEscape()) return;
                //*  JB2
                DbsUtil.terminate(32);  if (true) return;                                                                                                                 //Natural: TERMINATE 32
            }                                                                                                                                                             //Natural: END-IF
            //*  (3080)
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  HEADER RECORD
        //*  DETAIL RECORD
        short decideConditionsMet496 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #NEW-RECORD.#H-RECORD-TYPE;//Natural: VALUE 'H'
        if (condition((pnd_New_Record_Pnd_H_Record_Type.equals("H"))))
        {
            decideConditionsMet496++;
            pnd_Title_Fields_Pnd_Contract_Type.setValue(pnd_New_Record_Pnd_H_Contract_Type);                                                                              //Natural: ASSIGN #CONTRACT-TYPE := #NEW-RECORD.#H-CONTRACT-TYPE
            pnd_Title_Fields_Pnd_End_Date.setValue(pnd_New_Record_Pnd_H_Date);                                                                                            //Natural: ASSIGN #END-DATE := #NEW-RECORD.#H-DATE
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((pnd_New_Record_Pnd_H_Record_Type.equals("D"))))
        {
            decideConditionsMet496++;
            pnd_N_Record_Count.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #N-RECORD-COUNT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  JB3
    private void sub_Product_Match_Processing() throws Exception                                                                                                          //Natural: PRODUCT-MATCH-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  JB3
        //*  JB3
        //*  JB3
        //*  JB3
        short decideConditionsMet513 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #N-LAST-5 = MASK ( A )
        if (condition(DbsUtil.maskMatches(pnd_New_Record_Pnd_N_Last_5,"A")))
        {
            decideConditionsMet513++;
            pnd_N_Alpha_Pos.setValue(5);                                                                                                                                  //Natural: ASSIGN #N-ALPHA-POS := 5
            pnd_N_Alpha_Value.setValue(pnd_New_Record_Pnd_N_Last_5_7);                                                                                                    //Natural: ASSIGN #N-ALPHA-VALUE := #N-LAST-5-7
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            pnd_New_Record_Pnd_N_Beg_5_7.setValue(pnd_New_Record_Pnd_N_Last_5_7);                                                                                         //Natural: MOVE #N-LAST-5-7 TO #N-BEG-5-7 #N-END-5-7
            pnd_New_Record_Pnd_N_End_5_7.setValue(pnd_New_Record_Pnd_N_Last_5_7);
        }                                                                                                                                                                 //Natural: WHEN #N-LAST-6 = MASK ( A )
        else if (condition(DbsUtil.maskMatches(pnd_New_Record_Pnd_N_Last_6,"A")))
        {
            decideConditionsMet513++;
            pnd_N_Alpha_Pos.setValue(6);                                                                                                                                  //Natural: ASSIGN #N-ALPHA-POS := 6
            pnd_N_Alpha_Value.setValue(pnd_New_Record_Pnd_N_Last_6_7);                                                                                                    //Natural: ASSIGN #N-ALPHA-VALUE := #N-LAST-6-7
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            pnd_New_Record_Pnd_N_Beg_6_7.setValue(pnd_New_Record_Pnd_N_Last_6_7);                                                                                         //Natural: MOVE #N-LAST-6-7 TO #N-BEG-6-7 #N-END-6-7
            pnd_New_Record_Pnd_N_End_6_7.setValue(pnd_New_Record_Pnd_N_Last_6_7);
        }                                                                                                                                                                 //Natural: WHEN #N-LAST-7 = MASK ( A )
        else if (condition(DbsUtil.maskMatches(pnd_New_Record_Pnd_N_Last_7,"A")))
        {
            decideConditionsMet513++;
            pnd_N_Alpha_Pos.setValue(7);                                                                                                                                  //Natural: ASSIGN #N-ALPHA-POS := 7
            pnd_N_Alpha_Value.setValue(pnd_New_Record_Pnd_N_Last_7);                                                                                                      //Natural: ASSIGN #N-ALPHA-VALUE := #N-LAST-7
            //*  JB3
            //*  JB3
            pnd_New_Record_Pnd_N_Beg_7.setValue(pnd_New_Record_Pnd_N_Last_7);                                                                                             //Natural: MOVE #N-LAST-7 TO #N-BEG-7 #N-END-7
            pnd_New_Record_Pnd_N_End_7.setValue(pnd_New_Record_Pnd_N_Last_7);
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  JB3
            pnd_N_Alpha_Value.reset();                                                                                                                                    //Natural: RESET #N-ALPHA-VALUE #N-ALPHA-POS
            pnd_N_Alpha_Pos.reset();
            //*  3310                                       /* JB3
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  JB3
        //*  JB3
        //*  JB3
        //*  JB3
        short decideConditionsMet546 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #O-LAST-5 = MASK ( A )
        if (condition(DbsUtil.maskMatches(pnd_Old_Record_Pnd_O_Last_5,"A")))
        {
            decideConditionsMet546++;
            pnd_O_Alpha_Pos.setValue(5);                                                                                                                                  //Natural: ASSIGN #O-ALPHA-POS := 5
            pnd_O_Alpha_Value.setValue(pnd_Old_Record_Pnd_O_Last_5_7);                                                                                                    //Natural: ASSIGN #O-ALPHA-VALUE := #O-LAST-5-7
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            pnd_Old_Record_Pnd_O_Beg_5_7.setValue(pnd_Old_Record_Pnd_O_Last_5_7);                                                                                         //Natural: MOVE #O-LAST-5-7 TO #O-BEG-5-7 #O-END-5-7
            pnd_Old_Record_Pnd_O_End_5_7.setValue(pnd_Old_Record_Pnd_O_Last_5_7);
        }                                                                                                                                                                 //Natural: WHEN #O-LAST-6 = MASK ( A )
        else if (condition(DbsUtil.maskMatches(pnd_Old_Record_Pnd_O_Last_6,"A")))
        {
            decideConditionsMet546++;
            pnd_O_Alpha_Pos.setValue(6);                                                                                                                                  //Natural: ASSIGN #O-ALPHA-POS := 6
            pnd_O_Alpha_Value.setValue(pnd_Old_Record_Pnd_O_Last_6_7);                                                                                                    //Natural: ASSIGN #O-ALPHA-VALUE := #O-LAST-6-7
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            pnd_Old_Record_Pnd_O_Beg_6_7.setValue(pnd_Old_Record_Pnd_O_Last_6_7);                                                                                         //Natural: MOVE #O-LAST-6-7 TO #O-BEG-6-7 #O-END-6-7
            pnd_Old_Record_Pnd_O_End_6_7.setValue(pnd_Old_Record_Pnd_O_Last_6_7);
        }                                                                                                                                                                 //Natural: WHEN #O-LAST-7 = MASK ( A )
        else if (condition(DbsUtil.maskMatches(pnd_Old_Record_Pnd_O_Last_7,"A")))
        {
            decideConditionsMet546++;
            pnd_O_Alpha_Pos.setValue(7);                                                                                                                                  //Natural: ASSIGN #O-ALPHA-POS := 7
            pnd_O_Alpha_Value.setValue(pnd_Old_Record_Pnd_O_Last_7);                                                                                                      //Natural: ASSIGN #O-ALPHA-VALUE := #O-LAST-7
            //*  JB3
            //*  JB3
            pnd_Old_Record_Pnd_O_Beg_7.setValue(pnd_Old_Record_Pnd_O_Last_7);                                                                                             //Natural: MOVE #O-LAST-7 TO #O-BEG-7 #O-END-7
            pnd_Old_Record_Pnd_O_End_7.setValue(pnd_Old_Record_Pnd_O_Last_7);
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  JB3
            pnd_O_Alpha_Value.reset();                                                                                                                                    //Natural: RESET #O-ALPHA-VALUE
            //*  3450                                       /* JB3
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_First_Time.setValue(true);                                                                                                                                    //Natural: ASSIGN #FIRST-TIME := TRUE
        //*  JB3
        //*  JB3
        pnd_Report_Fields.reset();                                                                                                                                        //Natural: RESET #REPORT-FIELDS #TOTAL-USED #APPA2075
        pnd_Total_Used.reset();
        pdaAppa2075.getPnd_Appa2075().reset();
        pdaAppa2075.getPnd_Appa2075_Pnd_P_Product_Code().setValue(pnd_Old_Record_Pnd_O_Product_Code);                                                                     //Natural: ASSIGN #APPA2075.#P-PRODUCT-CODE := #O-PRODUCT-CODE
        //*  JB3
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_O_Alpha_Value.equals(pnd_N_Alpha_Value))) {break;}                                                                                          //Natural: UNTIL #O-ALPHA-VALUE = #N-ALPHA-VALUE
            //*  *******************************************************
            //*  * PREV AND CURR HAVE DIFFERENT ALPHA VALUES           *
            //*  * PRINT PREV ALPHA RANGE USAGE                        *
            //*  * AND ADDITIONAL INTERVENING ALPHA RANGE USAGE        *
            //*  *******************************************************
                                                                                                                                                                          //Natural: PERFORM MOVE-OLD-FIELDS
            sub_Move_Old_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM COMPUTE-RANGE-VALUES-O
            sub_Compute_Range_Values_O();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-RECORD
            sub_Write_Detail_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Msg_Sub.reset();                                                                                                                                          //Natural: RESET #MSG-SUB #MSG ( * ) #MESSAGE
            pnd_Msg.getValue("*").reset();
            pnd_Message.reset();
            //*  JB3
            //*  JB3
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-ALPHA-VALUE
            sub_Get_Next_Alpha_Value();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_First_Time.setValue(false);                                                                                                                               //Natural: ASSIGN #FIRST-TIME := FALSE
            //*  3470                                       /* JB3
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  *******************************************************
        //*  * CURRENT ALPHA RANGE USAGE                           *
        //*  *******************************************************
        //*  JB3
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-FIELDS
        sub_Move_New_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  JB3
                                                                                                                                                                          //Natural: PERFORM COMPUTE-RANGE-VALUES-N
        sub_Compute_Range_Values_N();
        if (condition(Global.isEscape())) {return;}
        //*  JB3
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-RECORD
        sub_Write_Detail_Record();
        if (condition(Global.isEscape())) {return;}
        //*  JB3
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-ACCUM-RECORD
        sub_Write_Detail_Accum_Record();
        if (condition(Global.isEscape())) {return;}
        //*  JB3
        pnd_Msg_Sub.reset();                                                                                                                                              //Natural: RESET #MSG-SUB #MSG ( * ) #MESSAGE
        pnd_Msg.getValue("*").reset();
        pnd_Message.reset();
        //*  JB3
    }
    private void sub_Move_Old_Fields() throws Exception                                                                                                                   //Natural: MOVE-OLD-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Report_Fields_Pnd_Product_Code.setValue(pnd_Old_Record_Pnd_O_Product_Code);                                                                                   //Natural: ASSIGN #PRODUCT-CODE := #OLD-RECORD.#O-PRODUCT-CODE
        pnd_Report_Fields_Pnd_Beg_Range.setValue(pnd_Old_Record_Pnd_O_Beg_Range);                                                                                         //Natural: ASSIGN #BEG-RANGE := #OLD-RECORD.#O-BEG-RANGE
        pnd_Report_Fields_Pnd_End_Range.setValue(pnd_Old_Record_Pnd_O_End_Range);                                                                                         //Natural: ASSIGN #END-RANGE := #OLD-RECORD.#O-END-RANGE
        pnd_Report_Fields_Pnd_Last_Contract.setValue(pnd_Old_Record_Pnd_O_Last_Contract);                                                                                 //Natural: ASSIGN #LAST-CONTRACT := #OLD-RECORD.#O-LAST-CONTRACT
        pnd_Report_Fields_Pnd_Range_Size.setValue(pnd_Old_Record_Pnd_O_Range_Size);                                                                                       //Natural: ASSIGN #RANGE-SIZE := #OLD-RECORD.#O-RANGE-SIZE
        pnd_Report_Fields_Pnd_Range_Left.setValue(pnd_Old_Record_Pnd_O_Range_Left);                                                                                       //Natural: ASSIGN #RANGE-LEFT := #OLD-RECORD.#O-RANGE-LEFT
    }
    private void sub_Move_New_Fields() throws Exception                                                                                                                   //Natural: MOVE-NEW-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Report_Fields_Pnd_Product_Code.setValue(pnd_New_Record_Pnd_N_Product_Code);                                                                                   //Natural: ASSIGN #PRODUCT-CODE := #NEW-RECORD.#N-PRODUCT-CODE
        pnd_Report_Fields_Pnd_Beg_Range.setValue(pnd_New_Record_Pnd_N_Beg_Range);                                                                                         //Natural: ASSIGN #BEG-RANGE := #NEW-RECORD.#N-BEG-RANGE
        pnd_Report_Fields_Pnd_End_Range.setValue(pnd_New_Record_Pnd_N_End_Range);                                                                                         //Natural: ASSIGN #END-RANGE := #NEW-RECORD.#N-END-RANGE
        pnd_Report_Fields_Pnd_Last_Contract.setValue(pnd_New_Record_Pnd_N_Last_Contract);                                                                                 //Natural: ASSIGN #LAST-CONTRACT := #NEW-RECORD.#N-LAST-CONTRACT
        pnd_Report_Fields_Pnd_Range_Size.setValue(pnd_New_Record_Pnd_N_Range_Size);                                                                                       //Natural: ASSIGN #RANGE-SIZE := #NEW-RECORD.#N-RANGE-SIZE
        pnd_Report_Fields_Pnd_Range_Left.setValue(pnd_New_Record_Pnd_N_Range_Left);                                                                                       //Natural: ASSIGN #RANGE-LEFT := #NEW-RECORD.#N-RANGE-LEFT
        if (condition(pnd_New_Record_Pnd_N_Next_Indicator.equals("*")))                                                                                                   //Natural: IF #NEW-RECORD.#N-NEXT-INDICATOR = '*'
        {
            pnd_Report_Fields_Pnd_Next_Range.setValue("YES");                                                                                                             //Natural: ASSIGN #NEXT-RANGE := 'YES'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Next_Range.setValue("NO");                                                                                                              //Natural: ASSIGN #NEXT-RANGE := 'NO'
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  PREV RANGE USED UP
    private void sub_Compute_Range_Values_O() throws Exception                                                                                                            //Natural: COMPUTE-RANGE-VALUES-O
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Report_Fields_Pnd_Next_Range.setValue("YES");                                                                                                                 //Natural: ASSIGN #NEXT-RANGE := 'YES'
        pnd_Ratio.reset();                                                                                                                                                //Natural: RESET #RATIO #RANGE-LEFT #PERCENT-LEFT
        pnd_Report_Fields_Pnd_Range_Left.reset();
        pnd_Report_Fields_Pnd_Percent_Left.reset();
        //*  JB3
        if (condition(pnd_First_Time.getBoolean()))                                                                                                                       //Natural: IF #FIRST-TIME
        {
            pnd_Report_Fields_Pnd_Used.setValue(pnd_Old_Record_Pnd_O_Range_Left);                                                                                         //Natural: ASSIGN #USED := #OLD-RECORD.#O-RANGE-LEFT
            //*  JB3
            //*  JB3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Used.setValue(pnd_Old_Record_Pnd_O_Range_Size);                                                                                         //Natural: ASSIGN #USED := #OLD-RECORD.#O-RANGE-SIZE
            //*  JB3
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Total_Used.nadd(pnd_Report_Fields_Pnd_Used);                                                                                                                  //Natural: ADD #USED TO #TOTAL-USED
        pnd_Message.setValue("CURR RANGE EXHAUSTED - USING NEXT RANGE");                                                                                                  //Natural: ASSIGN #MESSAGE := 'CURR RANGE EXHAUSTED - USING NEXT RANGE'
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-MSG-ARRAY
        sub_Move_To_Msg_Array();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Compute_Range_Values_N() throws Exception                                                                                                            //Natural: COMPUTE-RANGE-VALUES-N
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        short decideConditionsMet655 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #FIRST-TIME
        if (condition(! ((pnd_First_Time.getBoolean()))))
        {
            decideConditionsMet655++;
            pnd_Report_Fields_Pnd_Used.compute(new ComputeParameters(false, pnd_Report_Fields_Pnd_Used), pnd_New_Record_Pnd_N_Range_Size.subtract(pnd_New_Record_Pnd_N_Range_Left)); //Natural: COMPUTE #USED = #NEW-RECORD.#N-RANGE-SIZE - #NEW-RECORD.#N-RANGE-LEFT
        }                                                                                                                                                                 //Natural: WHEN #NEW-RECORD.#N-RANGE-LEFT < #OLD-RECORD.#O-RANGE-LEFT
        else if (condition(pnd_New_Record_Pnd_N_Range_Left.less(pnd_Old_Record_Pnd_O_Range_Left)))
        {
            decideConditionsMet655++;
            pnd_Report_Fields_Pnd_Used.compute(new ComputeParameters(false, pnd_Report_Fields_Pnd_Used), pnd_Old_Record_Pnd_O_Range_Left.subtract(pnd_New_Record_Pnd_N_Range_Left)); //Natural: COMPUTE #USED = #OLD-RECORD.#O-RANGE-LEFT - #NEW-RECORD.#N-RANGE-LEFT
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Used.reset();                                                                                                                           //Natural: RESET #USED
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Total_Used.nadd(pnd_Report_Fields_Pnd_Used);                                                                                                                  //Natural: ADD #USED TO #TOTAL-USED
        pnd_Ratio.compute(new ComputeParameters(true, pnd_Ratio), pnd_Report_Fields_Pnd_Range_Left.divide(pnd_New_Record_Pnd_N_Range_Size));                              //Natural: COMPUTE ROUNDED #RATIO = #RANGE-LEFT / #N-RANGE-SIZE
        pnd_Report_Fields_Pnd_Percent_Left.compute(new ComputeParameters(false, pnd_Report_Fields_Pnd_Percent_Left), pnd_Ratio.multiply(100));                            //Natural: COMPUTE #PERCENT-LEFT = #RATIO * 100
    }
    private void sub_Move_To_Msg_Array() throws Exception                                                                                                                 //Natural: MOVE-TO-MSG-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Msg_Sub.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #MSG-SUB
        if (condition(pnd_Msg_Sub.greater(pnd_Msg_Max)))                                                                                                                  //Natural: IF #MSG-SUB > #MSG-MAX
        {
            getReports().write(0, "MESSAGE ARRAY SIZE OF",pnd_Msg_Max,"EXCEEDED");                                                                                        //Natural: WRITE 'MESSAGE ARRAY SIZE OF' #MSG-MAX 'EXCEEDED'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Msg.getValue(pnd_Msg_Sub).setValue(pnd_Message);                                                                                                          //Natural: ASSIGN #MSG ( #MSG-SUB ) := #MESSAGE
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  JB3
    //*  JB3
    private void sub_Get_Next_Alpha_Value() throws Exception                                                                                                              //Natural: GET-NEXT-ALPHA-VALUE
    {
        if (BLNatReinput.isReinput()) return;

        pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract().setValue(pnd_Old_Record_Pnd_O_Beg_Range);                                                                        //Natural: ASSIGN #APPA2075.#P-BEG-CONTRACT := #O-BEG-RANGE
        pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract().setValue(pnd_Old_Record_Pnd_O_End_Range);                                                                        //Natural: ASSIGN #APPA2075.#P-END-CONTRACT := #O-END-RANGE
        //*  JB3
        DbsUtil.callnat(Appn2075.class , getCurrentProcessState(), pdaAppa2075.getPnd_Appa2075());                                                                        //Natural: CALLNAT 'APPN2075' #APPA2075
        if (condition(Global.isEscape())) return;
        //*  JB3
        if (condition(pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Code().notEquals(getZero())))                                                                              //Natural: IF #P-RETURN-CODE NE 0
        {
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            getReports().write(0, pdaAppa2075.getPnd_Appa2075_Pnd_P_Product_Code(),new ColumnSpacing(4),pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract(),                 //Natural: WRITE #APPA2075.#P-PRODUCT-CODE 4X #APPA2075.#P-BEG-CONTRACT #APPA2075.#P-END-CONTRACT / #APPA2075.#P-RETURN-CODE #APPA2075.#P-RETURN-MESSAGE
                pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract(),NEWLINE,pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Code(),pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Message());
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Old_Record_Pnd_O_Beg_Range.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract());                                                                        //Natural: ASSIGN #O-BEG-RANGE := #APPA2075.#P-BEG-CONTRACT
        pnd_Old_Record_Pnd_O_End_Range.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract());                                                                        //Natural: ASSIGN #O-END-RANGE := #APPA2075.#P-END-CONTRACT
        pnd_Old_Record_Pnd_O_Last_Contract.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract());                                                                    //Natural: ASSIGN #O-LAST-CONTRACT := #APPA2075.#P-END-CONTRACT
        //*  JB3
        //*  JB3
        //*  JB3
        //*  JB3
        short decideConditionsMet708 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #O-BEG-5 = MASK ( A )
        if (condition(DbsUtil.maskMatches(pnd_Old_Record_Pnd_O_Beg_5,"A")))
        {
            decideConditionsMet708++;
            pnd_O_Alpha_Pos.setValue(5);                                                                                                                                  //Natural: ASSIGN #O-ALPHA-POS := 5
            pnd_O_Alpha_Value.setValue(pnd_Old_Record_Pnd_O_Beg_5_7);                                                                                                     //Natural: ASSIGN #O-ALPHA-VALUE := #O-BEG-5-7
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            pnd_Old_Record_Pnd_O_End_5_7.setValue(pnd_Old_Record_Pnd_O_Beg_5_7);                                                                                          //Natural: MOVE #O-BEG-5-7 TO #O-END-5-7 #O-LAST-5-7
            pnd_Old_Record_Pnd_O_Last_5_7.setValue(pnd_Old_Record_Pnd_O_Beg_5_7);
        }                                                                                                                                                                 //Natural: WHEN #O-BEG-6 = MASK ( A )
        else if (condition(DbsUtil.maskMatches(pnd_Old_Record_Pnd_O_Beg_6,"A")))
        {
            decideConditionsMet708++;
            pnd_O_Alpha_Pos.setValue(6);                                                                                                                                  //Natural: ASSIGN #O-ALPHA-POS := 6
            pnd_O_Alpha_Value.setValue(pnd_Old_Record_Pnd_O_Beg_6_7);                                                                                                     //Natural: ASSIGN #O-ALPHA-VALUE := #O-BEG-6-7
            //*  JB3
            //*  JB3
            //*  JB3
            //*  JB3
            pnd_Old_Record_Pnd_O_End_6_7.setValue(pnd_Old_Record_Pnd_O_Beg_6_7);                                                                                          //Natural: MOVE #O-BEG-6-7 TO #O-END-6-7 #O-LAST-6-7
            pnd_Old_Record_Pnd_O_Last_6_7.setValue(pnd_Old_Record_Pnd_O_Beg_6_7);
        }                                                                                                                                                                 //Natural: WHEN #O-BEG-7 = MASK ( A )
        else if (condition(DbsUtil.maskMatches(pnd_Old_Record_Pnd_O_Beg_7,"A")))
        {
            decideConditionsMet708++;
            pnd_O_Alpha_Pos.setValue(7);                                                                                                                                  //Natural: ASSIGN #O-ALPHA-POS := 7
            pnd_O_Alpha_Value.setValue(pnd_Old_Record_Pnd_O_Beg_7);                                                                                                       //Natural: ASSIGN #O-ALPHA-VALUE := #O-BEG-7
            //*  JB3
            //*  JB3
            pnd_Old_Record_Pnd_O_End_7.setValue(pnd_Old_Record_Pnd_O_Beg_7);                                                                                              //Natural: MOVE #O-BEG-7 TO #O-END-7 #O-LAST-7
            pnd_Old_Record_Pnd_O_Last_7.setValue(pnd_Old_Record_Pnd_O_Beg_7);
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  JB3
            pnd_O_Alpha_Value.reset();                                                                                                                                    //Natural: RESET #O-ALPHA-VALUE
            //*  5810                                  /* JB3
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Old_Record_Pnd_O_Range_Left.setValue(0);                                                                                                                      //Natural: ASSIGN #O-RANGE-LEFT := 0
        //*  JB3
        //*  JB3
        if (condition(pnd_O_Alpha_Pos.equals(pnd_N_Alpha_Pos)))                                                                                                           //Natural: IF #O-ALPHA-POS = #N-ALPHA-POS
        {
            pnd_Old_Record_Pnd_O_Range_Size.setValue(pnd_New_Record_Pnd_N_Range_Size);                                                                                    //Natural: ASSIGN #O-RANGE-SIZE := #N-RANGE-SIZE
            //*  JB3
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Detail_Record() throws Exception                                                                                                               //Natural: WRITE-DETAIL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Message.setValue(pnd_Msg.getValue(1));                                                                                                                        //Natural: ASSIGN #MESSAGE := #MSG ( 1 )
        getReports().display(1, "PRODUCT/CODE",                                                                                                                           //Natural: DISPLAY ( 1 ) 'PRODUCT/CODE' #PRODUCT-CODE 'BEG/RANGE' #BEG-RANGE 'END/RANGE' #END-RANGE 'LAST/CONTRACT' #LAST-CONTRACT 'RANGE/SIZE' #RANGE-SIZE 'RANGE/LEFT' #RANGE-LEFT 'WEEKLY/USAGE' #USED 'PERCENT/LEFT' #PERCENT-LEFT 'NEXT/RANGE' #NEXT-RANGE '/MESSAGE' #MESSAGE
        		pnd_Report_Fields_Pnd_Product_Code,"BEG/RANGE",
        		pnd_Report_Fields_Pnd_Beg_Range,"END/RANGE",
        		pnd_Report_Fields_Pnd_End_Range,"LAST/CONTRACT",
        		pnd_Report_Fields_Pnd_Last_Contract,"RANGE/SIZE",
        		pnd_Report_Fields_Pnd_Range_Size,"RANGE/LEFT",
        		pnd_Report_Fields_Pnd_Range_Left,"WEEKLY/USAGE",
        		pnd_Report_Fields_Pnd_Used,"PERCENT/LEFT",
        		pnd_Report_Fields_Pnd_Percent_Left,"NEXT/RANGE",
        		pnd_Report_Fields_Pnd_Next_Range,"/MESSAGE",
        		pnd_Message);
        if (Global.isEscape()) return;
        if (condition(pnd_Msg_Sub.greater(1)))                                                                                                                            //Natural: IF #MSG-SUB > 1
        {
            pnd_Msg_Cnt.setValue(pnd_Msg_Sub);                                                                                                                            //Natural: ASSIGN #MSG-CNT := #MSG-SUB
            FOR01:                                                                                                                                                        //Natural: FOR #MSG-SUB = 2 TO #MSG-CNT
            for (pnd_Msg_Sub.setValue(2); condition(pnd_Msg_Sub.lessOrEqual(pnd_Msg_Cnt)); pnd_Msg_Sub.nadd(1))
            {
                getReports().write(1, new ReportPAsterisk(pnd_Message),pnd_Msg.getValue(pnd_Msg_Sub));                                                                    //Natural: WRITE ( 1 ) P*#MESSAGE #MSG ( #MSG-SUB )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Header_Accum_Record() throws Exception                                                                                                         //Natural: WRITE-HEADER-ACCUM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        getWorkFiles().write(3, false, pnd_Record_Type, pnd_Space_1, pnd_Title_Fields_Pnd_Contract_Type, pnd_Space_1, pnd_Title_Fields_Pnd_Beg_Date, pnd_Space_1,         //Natural: WRITE WORK FILE 03 #RECORD-TYPE #SPACE-1 #CONTRACT-TYPE #SPACE-1 #BEG-DATE #SPACE-1 #END-DATE #SPACE-9
            pnd_Title_Fields_Pnd_End_Date, pnd_Space_9);
    }
    private void sub_Write_Detail_Accum_Record() throws Exception                                                                                                         //Natural: WRITE-DETAIL-ACCUM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        getWorkFiles().write(3, false, pnd_Record_Type, pnd_Space_1, pnd_Title_Fields_Pnd_Contract_Type, pnd_Space_1, pnd_Date_Range, pnd_Space_1, pnd_Report_Fields_Pnd_Product_Code,  //Natural: WRITE WORK FILE 03 #RECORD-TYPE #SPACE-1 #CONTRACT-TYPE #SPACE-1 #DATE-RANGE #SPACE-1 #PRODUCT-CODE #TOTAL-USED
            pnd_Total_Used);
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=65");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(40),"WEEKLY",pnd_Title_Fields_Pnd_Contract_Type,"CONTRACT RANGE USAGE REPORT",new 
            ColumnSpacing(37),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new ColumnSpacing(54),pnd_Title_Fields_Pnd_Beg_Date,"-",pnd_Title_Fields_Pnd_End_Date,
            NEWLINE);

        getReports().setDisplayColumns(1, "PRODUCT/CODE",
        		pnd_Report_Fields_Pnd_Product_Code,"BEG/RANGE",
        		pnd_Report_Fields_Pnd_Beg_Range,"END/RANGE",
        		pnd_Report_Fields_Pnd_End_Range,"LAST/CONTRACT",
        		pnd_Report_Fields_Pnd_Last_Contract,"RANGE/SIZE",
        		pnd_Report_Fields_Pnd_Range_Size,"RANGE/LEFT",
        		pnd_Report_Fields_Pnd_Range_Left,"WEEKLY/USAGE",
        		pnd_Report_Fields_Pnd_Used,"PERCENT/LEFT",
        		pnd_Report_Fields_Pnd_Percent_Left,"NEXT/RANGE",
        		pnd_Report_Fields_Pnd_Next_Range,"/MESSAGE",
        		pnd_Message);
    }
}
