/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:50 PM
**        * FROM NATURAL PROGRAM : Scib9001
************************************************************
**        * FILE NAME            : Scib9001.java
**        * CLASS NAME           : Scib9001
**        * INSTANCE NAME        : Scib9001
************************************************************
************************************************************************
* PROGRAM  : SCIB9001
* SYSTEM   : INTERFACE MODULE
* AUTHOR   : CHARLES SINGLETON
* GENERATED: MAR. 2004
* FUNCTION : THIS PROGRAM READS THE SEQUENTIAL FILE OF NEWLY ISSUED
*            SUNGARD CONTRACTS AND CREATES THE T813 CARDS TO INTERFACE
*            BACK TO SUNGARD FOR UPDATE.
*
* MOD DATE   MOD BY       DESCRIPTION OF CHANGES
* 12/08/04 SINGLETON SET DATA ELEMENT DEFAULTS AND CREATE THE T813
*                    TRANSACTIONS TO UPDATE THE DATA ELEMENTS IN OMNI
*                    WHEN INCOMPLETE BIO DATA HAS PREVIOUSLY BEEN SENT
*                    BY AN INSTITUTION FOR ENROLLMENT. ALSO, UPDATE
*                    ANNTY-START-DATE(PH660) AND APP-RECVD-DATE(PH773).
* 06/10/06 SINGLETON SET DATA ELEMENT PH657(REGISTER-ID) FOR AUTOMATED
*                    REPLACEMENT REGISTER.
* 01/07/08 GATES     SET DATA ELEMENT PH652(OWNERSHIP-CD) AND DATA
*                    ELEMENT PH637(ENROLLMENT-TYPE) FOR ACIS REJECT CCR.
*                    SEE REJECT KG.
* 01/20/11 SACHARNY  CHANGES FOR 'Star II eligibility' PROJECT (RS1)
* 10/11/11 SCHNEIDER ADDED #ORCHESTRATION-ID TO #OMNI-RECORD   (SRK)
* 06/14/13 LAEVEY    ADDED #T813-SUBSTITUTION-CONTRACT-IND AND (TNGSUB)
*                    #T813-DECEASED TO #OMNI-RECORD
*                    ADDED ROUTINE TO CONDITIONALLY CREATE TRAILER FOR
*                    IRA SUB RECORDS
*                    ADDED #DECEASED TO #OMNI-HEADER AND CONDITIONALLY
*                    POPULATE BEFORE WRITING HEADER
* 03/17/2017 B.NEWSOM - ADDED LOGIC FOR ONEIRA                  (ONEIRA)
*                     - SEND T813 DATA BACK TO OMNI FOR ONEIRA.
* 05/23/17 B.ELLO      PIN EXPANSION CHANGES                           *
*                      RESTOWED FOR UPDATED PDAS AND LDAS              *
*                      CHANGED PIN FORMAT FROM A7 TO A12      (PINE)   *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9001 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pls_Error_Occurence;
    private DbsField pls_Dar_Program;

    private DataAccessProgramView vw_batch_Restart;
    private DbsField batch_Restart_Rst_Actve_Ind;
    private DbsField batch_Restart_Rst_Job_Nme;
    private DbsField batch_Restart_Rst_Jobstep_Nme;
    private DbsField batch_Restart_Rst_Pgm_Nme;
    private DbsField batch_Restart_Rst_Curr_Dte;
    private DbsField batch_Restart_Rst_Curr_Tme;
    private DbsField batch_Restart_Rst_Start_Dte;
    private DbsField batch_Restart_Rst_Start_Tme;
    private DbsField batch_Restart_Rst_End_Dte;
    private DbsField batch_Restart_Rst_End_Tme;
    private DbsField batch_Restart_Rst_Key;
    private DbsField batch_Restart_Rst_Cnt;
    private DbsField batch_Restart_Rst_Isn_Nbr;
    private DbsField batch_Restart_Rst_Rstrt_Data_Ind;

    private DbsGroup batch_Restart_Rst_Data_Grp;
    private DbsField batch_Restart_Rst_Data_Txt;
    private DbsField batch_Restart_Rst_Frst_Run_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Tme;
    private DbsField batch_Restart_Rst_Updt_Dte;
    private DbsField batch_Restart_Rst_Updt_Tme;
    private DbsField pnd_Restart_Key;

    private DbsGroup pnd_Restart_Key__R_Field_1;

    private DbsGroup pnd_Restart_Key_Pnd_Restart_Structure;
    private DbsField pnd_Restart_Key_Pnd_Restart_Plan_No;
    private DbsField pnd_Restart_Key_Pnd_Restart_Part_Id;

    private DbsGroup pnd_Restart_Key__R_Field_2;
    private DbsField pnd_Restart_Key_Pnd_Restart_Subplan_No;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ssn;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ext;
    private DbsField pnd_Sp_Actve_Job_Step;

    private DbsGroup pnd_Sp_Actve_Job_Step__R_Field_3;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme;
    private DbsField pnd_Restart_Isn;

    private DbsGroup pnd_Omni_Record;
    private DbsField pnd_Omni_Record_Pnd_Plan_No;
    private DbsField pnd_Omni_Record_Pnd_Part_Id;

    private DbsGroup pnd_Omni_Record__R_Field_4;
    private DbsField pnd_Omni_Record_Pnd_Ssn;
    private DbsField pnd_Omni_Record_Pnd_Subplan_No;
    private DbsField pnd_Omni_Record_Pnd_Part_Ext;
    private DbsField pnd_Omni_Record_Pnd_Tiaa_Contract;
    private DbsField pnd_Omni_Record_Pnd_Cref_Contract;
    private DbsField pnd_Omni_Record_Pnd_Pin_No;

    private DbsGroup pnd_Omni_Record__R_Field_5;
    private DbsField pnd_Omni_Record_Pnd_Pin_No_Filler;
    private DbsField pnd_Omni_Record_Pnd_Pin_No_7;
    private DbsField pnd_Omni_Record_Pnd_Trade_Date;

    private DbsGroup pnd_Omni_Record__R_Field_6;
    private DbsField pnd_Omni_Record_Pnd_Trade_Date_N;
    private DbsField pnd_Omni_Record_Pnd_Tiaa_Issue_Date;
    private DbsField pnd_Omni_Record_Pnd_Cref_Issue_Date;
    private DbsField pnd_Omni_Record_Pnd_Annuity_Start_Date;
    private DbsField pnd_Omni_Record_Pnd_App_Rcvd_Date;
    private DbsField pnd_Omni_Record_Pnd_Issue_State;
    private DbsField pnd_Omni_Record_Pnd_Mail_Address_Line;
    private DbsField pnd_Omni_Record_Pnd_Mail_City;
    private DbsField pnd_Omni_Record_Pnd_Mail_State;
    private DbsField pnd_Omni_Record_Pnd_Mail_Zip;
    private DbsField pnd_Omni_Record_Pnd_Register_Id;
    private DbsField pnd_Omni_Record_Pnd_Divsub;
    private DbsField pnd_Omni_Record_Pnd_Enrollment_Type;
    private DbsField pnd_Omni_Record_Pnd_Vesting;
    private DbsField pnd_Omni_Record_Pnd_Update_Address;
    private DbsField pnd_Omni_Record_Pnd_Update_Divorce;
    private DbsField pnd_Omni_Record_Pnd_Update_Add_Cref;
    private DbsField pnd_Omni_Record_Pnd_Update_Pull_Code;
    private DbsField pnd_Omni_Record_Pnd_Update_Incompl_Data_Ind;
    private DbsField pnd_Omni_Record_Pnd_Update_Alloc_Model_Ind;
    private DbsField pnd_Omni_Record_Pnd_Update_Cai_Ind;
    private DbsField pnd_Omni_Record_Pnd_Update_App_Rcvd;
    private DbsField pnd_Omni_Record_Pnd_T813_Eir_Ind;
    private DbsField pnd_Omni_Record_Pnd_Orchestration_Id;
    private DbsField pnd_Omni_Record_Pnd_T813_Substitution_Contract_Ind;
    private DbsField pnd_Omni_Record_Pnd_T813_Deceased_Ind;
    private DbsField pnd_Omni_Record_Pnd_T813_Oneira_Acct_No;
    private DbsField pnd_Omni_Header;

    private DbsGroup pnd_Omni_Header__R_Field_7;
    private DbsField pnd_Omni_Header_Pnd_Tran_Code;
    private DbsField pnd_Omni_Header_Pnd_Seq_Code;
    private DbsField pnd_Omni_Header_Pnd_Notused1;
    private DbsField pnd_Omni_Header_Pnd_Rec_Type;
    private DbsField pnd_Omni_Header_Pnd_Plan_No;
    private DbsField pnd_Omni_Header_Pnd_Part_Id;
    private DbsField pnd_Omni_Header_Pnd_Notused2;
    private DbsField pnd_Omni_Header_Pnd_Trade_Date;
    private DbsField pnd_Omni_Header_Pnd_Notused3;
    private DbsField pnd_Omni_Header_Pnd_Deceased;
    private DbsField pnd_Omni_Detail;

    private DbsGroup pnd_Omni_Detail__R_Field_8;
    private DbsField pnd_Omni_Detail_Pnd_Tran_Code;
    private DbsField pnd_Omni_Detail_Pnd_Seq_Code;
    private DbsField pnd_Omni_Detail_Pnd_Notused1;
    private DbsField pnd_Omni_Detail_Pnd_Data_Elem;
    private DbsField pnd_Omni_Detail_Pnd_De_Value;
    private DbsField pnd_Compare_Key;

    private DbsGroup pnd_Compare_Key__R_Field_9;
    private DbsField pnd_Compare_Key_Pnd_Comp_Plan_No;
    private DbsField pnd_Compare_Key_Pnd_Comp_Part_Id;

    private DbsGroup pnd_Compare_Key__R_Field_10;
    private DbsField pnd_Compare_Key_Pnd_Comp_Ssn;
    private DbsField pnd_Compare_Key_Pnd_Comp_Subplan_No;
    private DbsField pnd_Compare_Key_Pnd_Comp_Part_Ext;

    private DbsGroup pnd_Accepted_Report_Rec;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Divsub;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Ssn;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No;

    private DbsGroup pnd_Rejected_Report_Rec;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Plan_No;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Divsub;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Ssn;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg;

    private DbsGroup pnd_Summary_Report_Rec;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt;
    private DbsField pnd_Restarted;
    private DbsField pnd_Skip_Restart_Record;
    private DbsField pnd_Hold_Part_Id;
    private DbsField pnd_Address_Standardized;
    private DbsField pnd_Record_Cnt;
    private DbsField pnd_Error_Rec_Cnt;
    private DbsField pnd_Max_Cards;
    private DbsField pnd_Card_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_Retry_Cnt;
    private DbsField intvl;
    private DbsField reqid;
    private DbsField pnd_Jobname;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pls_Error_Occurence = WsIndependent.getInstance().newFieldInRecord("pls_Error_Occurence", "+ERROR-OCCURENCE", FieldType.BOOLEAN, 1);
        pls_Dar_Program = WsIndependent.getInstance().newFieldInRecord("pls_Dar_Program", "+DAR-PROGRAM", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        // Local Variables
        localVariables = new DbsRecord();

        vw_batch_Restart = new DataAccessProgramView(new NameInfo("vw_batch_Restart", "BATCH-RESTART"), "BATCH_RESTART", "BATCH_RESTART", DdmPeriodicGroups.getInstance().getGroups("BATCH_RESTART"));
        batch_Restart_Rst_Actve_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Actve_Ind", "RST-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RST_ACTVE_IND");
        batch_Restart_Rst_Actve_Ind.setDdmHeader("ACTIVE IND");
        batch_Restart_Rst_Job_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Job_Nme", "RST-JOB-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_JOB_NME");
        batch_Restart_Rst_Job_Nme.setDdmHeader("JOB NAME");
        batch_Restart_Rst_Jobstep_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Jobstep_Nme", "RST-JOBSTEP-NME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_JOBSTEP_NME");
        batch_Restart_Rst_Jobstep_Nme.setDdmHeader("STEP NAME");
        batch_Restart_Rst_Pgm_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Pgm_Nme", "RST-PGM-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_PGM_NME");
        batch_Restart_Rst_Pgm_Nme.setDdmHeader("PROGRAM NAME");
        batch_Restart_Rst_Curr_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Dte", "RST-CURR-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_DTE");
        batch_Restart_Rst_Curr_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Tme", "RST-CURR-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_TME");
        batch_Restart_Rst_Start_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Dte", "RST-START-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_DTE");
        batch_Restart_Rst_Start_Dte.setDdmHeader("START/DATE");
        batch_Restart_Rst_Start_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Tme", "RST-START-TME", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_TME");
        batch_Restart_Rst_Start_Tme.setDdmHeader("START/TIME");
        batch_Restart_Rst_End_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Dte", "RST-END-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_DTE");
        batch_Restart_Rst_End_Dte.setDdmHeader("END/DATE");
        batch_Restart_Rst_End_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Tme", "RST-END-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_TME");
        batch_Restart_Rst_End_Tme.setDdmHeader("END/TIME");
        batch_Restart_Rst_Key = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Key", "RST-KEY", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "RST_KEY");
        batch_Restart_Rst_Cnt = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Cnt", "RST-CNT", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "RST_CNT");
        batch_Restart_Rst_Isn_Nbr = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Isn_Nbr", "RST-ISN-NBR", FieldType.PACKED_DECIMAL, 
            10, RepeatingFieldStrategy.None, "RST_ISN_NBR");
        batch_Restart_Rst_Rstrt_Data_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Rstrt_Data_Ind", "RST-RSTRT-DATA-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RST_RSTRT_DATA_IND");
        batch_Restart_Rst_Rstrt_Data_Ind.setDdmHeader("FAILED");

        batch_Restart_Rst_Data_Grp = vw_batch_Restart.getRecord().newGroupInGroup("batch_Restart_Rst_Data_Grp", "RST-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Data_Txt = batch_Restart_Rst_Data_Grp.newFieldArrayInGroup("batch_Restart_Rst_Data_Txt", "RST-DATA-TXT", FieldType.STRING, 250, 
            new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RST_DATA_TXT", "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Frst_Run_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Frst_Run_Dte", "RST-FRST-RUN-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_FRST_RUN_DTE");
        batch_Restart_Rst_Frst_Run_Dte.setDdmHeader("FIRST/RUN DTE");
        batch_Restart_Rst_Last_Rstrt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Dte", "RST-LAST-RSTRT-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_DTE");
        batch_Restart_Rst_Last_Rstrt_Dte.setDdmHeader("LAST RSTRT/DATE");
        batch_Restart_Rst_Last_Rstrt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Tme", "RST-LAST-RSTRT-TME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_TME");
        batch_Restart_Rst_Last_Rstrt_Tme.setDdmHeader("LAST RSTRT/TIME");
        batch_Restart_Rst_Updt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Dte", "RST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_DTE");
        batch_Restart_Rst_Updt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Tme", "RST-UPDT-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_TME");
        registerRecord(vw_batch_Restart);

        pnd_Restart_Key = localVariables.newFieldInRecord("pnd_Restart_Key", "#RESTART-KEY", FieldType.STRING, 50);

        pnd_Restart_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Restart_Key__R_Field_1", "REDEFINE", pnd_Restart_Key);

        pnd_Restart_Key_Pnd_Restart_Structure = pnd_Restart_Key__R_Field_1.newGroupInGroup("pnd_Restart_Key_Pnd_Restart_Structure", "#RESTART-STRUCTURE");
        pnd_Restart_Key_Pnd_Restart_Plan_No = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Plan_No", "#RESTART-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Restart_Key_Pnd_Restart_Part_Id = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Part_Id", "#RESTART-PART-ID", 
            FieldType.STRING, 17);

        pnd_Restart_Key__R_Field_2 = pnd_Restart_Key_Pnd_Restart_Structure.newGroupInGroup("pnd_Restart_Key__R_Field_2", "REDEFINE", pnd_Restart_Key_Pnd_Restart_Part_Id);
        pnd_Restart_Key_Pnd_Restart_Subplan_No = pnd_Restart_Key__R_Field_2.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Subplan_No", "#RESTART-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Restart_Key_Pnd_Restart_Ssn = pnd_Restart_Key__R_Field_2.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ssn", "#RESTART-SSN", FieldType.STRING, 
            9);
        pnd_Restart_Key_Pnd_Restart_Ext = pnd_Restart_Key__R_Field_2.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ext", "#RESTART-EXT", FieldType.STRING, 
            2);
        pnd_Sp_Actve_Job_Step = localVariables.newFieldInRecord("pnd_Sp_Actve_Job_Step", "#SP-ACTVE-JOB-STEP", FieldType.STRING, 17);

        pnd_Sp_Actve_Job_Step__R_Field_3 = localVariables.newGroupInRecord("pnd_Sp_Actve_Job_Step__R_Field_3", "REDEFINE", pnd_Sp_Actve_Job_Step);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind = pnd_Sp_Actve_Job_Step__R_Field_3.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind", "#RST-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme = pnd_Sp_Actve_Job_Step__R_Field_3.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme", "#RST-JOB-NME", 
            FieldType.STRING, 8);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme = pnd_Sp_Actve_Job_Step__R_Field_3.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme", "#RST-STEP-NME", 
            FieldType.STRING, 8);
        pnd_Restart_Isn = localVariables.newFieldInRecord("pnd_Restart_Isn", "#RESTART-ISN", FieldType.PACKED_DECIMAL, 10);

        pnd_Omni_Record = localVariables.newGroupInRecord("pnd_Omni_Record", "#OMNI-RECORD");
        pnd_Omni_Record_Pnd_Plan_No = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Plan_No", "#PLAN-NO", FieldType.STRING, 6);
        pnd_Omni_Record_Pnd_Part_Id = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Part_Id", "#PART-ID", FieldType.STRING, 17);

        pnd_Omni_Record__R_Field_4 = pnd_Omni_Record.newGroupInGroup("pnd_Omni_Record__R_Field_4", "REDEFINE", pnd_Omni_Record_Pnd_Part_Id);
        pnd_Omni_Record_Pnd_Ssn = pnd_Omni_Record__R_Field_4.newFieldInGroup("pnd_Omni_Record_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Omni_Record_Pnd_Subplan_No = pnd_Omni_Record__R_Field_4.newFieldInGroup("pnd_Omni_Record_Pnd_Subplan_No", "#SUBPLAN-NO", FieldType.STRING, 
            6);
        pnd_Omni_Record_Pnd_Part_Ext = pnd_Omni_Record__R_Field_4.newFieldInGroup("pnd_Omni_Record_Pnd_Part_Ext", "#PART-EXT", FieldType.STRING, 2);
        pnd_Omni_Record_Pnd_Tiaa_Contract = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Tiaa_Contract", "#TIAA-CONTRACT", FieldType.STRING, 10);
        pnd_Omni_Record_Pnd_Cref_Contract = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Cref_Contract", "#CREF-CONTRACT", FieldType.STRING, 10);
        pnd_Omni_Record_Pnd_Pin_No = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Pin_No", "#PIN-NO", FieldType.STRING, 12);

        pnd_Omni_Record__R_Field_5 = pnd_Omni_Record.newGroupInGroup("pnd_Omni_Record__R_Field_5", "REDEFINE", pnd_Omni_Record_Pnd_Pin_No);
        pnd_Omni_Record_Pnd_Pin_No_Filler = pnd_Omni_Record__R_Field_5.newFieldInGroup("pnd_Omni_Record_Pnd_Pin_No_Filler", "#PIN-NO-FILLER", FieldType.STRING, 
            5);
        pnd_Omni_Record_Pnd_Pin_No_7 = pnd_Omni_Record__R_Field_5.newFieldInGroup("pnd_Omni_Record_Pnd_Pin_No_7", "#PIN-NO-7", FieldType.STRING, 7);
        pnd_Omni_Record_Pnd_Trade_Date = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Trade_Date", "#TRADE-DATE", FieldType.STRING, 8);

        pnd_Omni_Record__R_Field_6 = pnd_Omni_Record.newGroupInGroup("pnd_Omni_Record__R_Field_6", "REDEFINE", pnd_Omni_Record_Pnd_Trade_Date);
        pnd_Omni_Record_Pnd_Trade_Date_N = pnd_Omni_Record__R_Field_6.newFieldInGroup("pnd_Omni_Record_Pnd_Trade_Date_N", "#TRADE-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Omni_Record_Pnd_Tiaa_Issue_Date = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Tiaa_Issue_Date", "#TIAA-ISSUE-DATE", FieldType.STRING, 
            8);
        pnd_Omni_Record_Pnd_Cref_Issue_Date = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Cref_Issue_Date", "#CREF-ISSUE-DATE", FieldType.STRING, 
            8);
        pnd_Omni_Record_Pnd_Annuity_Start_Date = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Annuity_Start_Date", "#ANNUITY-START-DATE", FieldType.STRING, 
            8);
        pnd_Omni_Record_Pnd_App_Rcvd_Date = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_App_Rcvd_Date", "#APP-RCVD-DATE", FieldType.STRING, 8);
        pnd_Omni_Record_Pnd_Issue_State = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Issue_State", "#ISSUE-STATE", FieldType.STRING, 2);
        pnd_Omni_Record_Pnd_Mail_Address_Line = pnd_Omni_Record.newFieldArrayInGroup("pnd_Omni_Record_Pnd_Mail_Address_Line", "#MAIL-ADDRESS-LINE", FieldType.STRING, 
            35, new DbsArrayController(1, 3));
        pnd_Omni_Record_Pnd_Mail_City = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Mail_City", "#MAIL-CITY", FieldType.STRING, 27);
        pnd_Omni_Record_Pnd_Mail_State = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Mail_State", "#MAIL-STATE", FieldType.STRING, 2);
        pnd_Omni_Record_Pnd_Mail_Zip = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Mail_Zip", "#MAIL-ZIP", FieldType.STRING, 9);
        pnd_Omni_Record_Pnd_Register_Id = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Register_Id", "#REGISTER-ID", FieldType.STRING, 11);
        pnd_Omni_Record_Pnd_Divsub = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Divsub", "#DIVSUB", FieldType.STRING, 4);
        pnd_Omni_Record_Pnd_Enrollment_Type = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Enrollment_Type", "#ENROLLMENT-TYPE", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_Vesting = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Vesting", "#VESTING", FieldType.STRING, 1);
        pnd_Omni_Record_Pnd_Update_Address = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_Address", "#UPDATE-ADDRESS", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_Update_Divorce = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_Divorce", "#UPDATE-DIVORCE", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_Update_Add_Cref = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_Add_Cref", "#UPDATE-ADD-CREF", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_Update_Pull_Code = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_Pull_Code", "#UPDATE-PULL-CODE", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_Update_Incompl_Data_Ind = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_Incompl_Data_Ind", "#UPDATE-INCOMPL-DATA-IND", 
            FieldType.STRING, 1);
        pnd_Omni_Record_Pnd_Update_Alloc_Model_Ind = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_Alloc_Model_Ind", "#UPDATE-ALLOC-MODEL-IND", 
            FieldType.STRING, 1);
        pnd_Omni_Record_Pnd_Update_Cai_Ind = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_Cai_Ind", "#UPDATE-CAI-IND", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_Update_App_Rcvd = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Update_App_Rcvd", "#UPDATE-APP-RCVD", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_T813_Eir_Ind = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_T813_Eir_Ind", "#T813-EIR-IND", FieldType.STRING, 1);
        pnd_Omni_Record_Pnd_Orchestration_Id = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_Orchestration_Id", "#ORCHESTRATION-ID", FieldType.STRING, 
            15);
        pnd_Omni_Record_Pnd_T813_Substitution_Contract_Ind = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_T813_Substitution_Contract_Ind", "#T813-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1);
        pnd_Omni_Record_Pnd_T813_Deceased_Ind = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_T813_Deceased_Ind", "#T813-DECEASED-IND", FieldType.STRING, 
            1);
        pnd_Omni_Record_Pnd_T813_Oneira_Acct_No = pnd_Omni_Record.newFieldInGroup("pnd_Omni_Record_Pnd_T813_Oneira_Acct_No", "#T813-ONEIRA-ACCT-NO", FieldType.STRING, 
            10);
        pnd_Omni_Header = localVariables.newFieldInRecord("pnd_Omni_Header", "#OMNI-HEADER", FieldType.STRING, 80);

        pnd_Omni_Header__R_Field_7 = localVariables.newGroupInRecord("pnd_Omni_Header__R_Field_7", "REDEFINE", pnd_Omni_Header);
        pnd_Omni_Header_Pnd_Tran_Code = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Tran_Code", "#TRAN-CODE", FieldType.STRING, 3);
        pnd_Omni_Header_Pnd_Seq_Code = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Seq_Code", "#SEQ-CODE", FieldType.STRING, 2);
        pnd_Omni_Header_Pnd_Notused1 = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Notused1", "#NOTUSED1", FieldType.STRING, 3);
        pnd_Omni_Header_Pnd_Rec_Type = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 7);
        pnd_Omni_Header_Pnd_Plan_No = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Plan_No", "#PLAN-NO", FieldType.STRING, 6);
        pnd_Omni_Header_Pnd_Part_Id = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Part_Id", "#PART-ID", FieldType.STRING, 17);
        pnd_Omni_Header_Pnd_Notused2 = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Notused2", "#NOTUSED2", FieldType.STRING, 3);
        pnd_Omni_Header_Pnd_Trade_Date = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Trade_Date", "#TRADE-DATE", FieldType.NUMERIC, 
            8);
        pnd_Omni_Header_Pnd_Notused3 = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Notused3", "#NOTUSED3", FieldType.STRING, 30);
        pnd_Omni_Header_Pnd_Deceased = pnd_Omni_Header__R_Field_7.newFieldInGroup("pnd_Omni_Header_Pnd_Deceased", "#DECEASED", FieldType.STRING, 1);
        pnd_Omni_Detail = localVariables.newFieldInRecord("pnd_Omni_Detail", "#OMNI-DETAIL", FieldType.STRING, 80);

        pnd_Omni_Detail__R_Field_8 = localVariables.newGroupInRecord("pnd_Omni_Detail__R_Field_8", "REDEFINE", pnd_Omni_Detail);
        pnd_Omni_Detail_Pnd_Tran_Code = pnd_Omni_Detail__R_Field_8.newFieldInGroup("pnd_Omni_Detail_Pnd_Tran_Code", "#TRAN-CODE", FieldType.STRING, 3);
        pnd_Omni_Detail_Pnd_Seq_Code = pnd_Omni_Detail__R_Field_8.newFieldInGroup("pnd_Omni_Detail_Pnd_Seq_Code", "#SEQ-CODE", FieldType.STRING, 2);
        pnd_Omni_Detail_Pnd_Notused1 = pnd_Omni_Detail__R_Field_8.newFieldInGroup("pnd_Omni_Detail_Pnd_Notused1", "#NOTUSED1", FieldType.STRING, 3);
        pnd_Omni_Detail_Pnd_Data_Elem = pnd_Omni_Detail__R_Field_8.newFieldInGroup("pnd_Omni_Detail_Pnd_Data_Elem", "#DATA-ELEM", FieldType.STRING, 3);
        pnd_Omni_Detail_Pnd_De_Value = pnd_Omni_Detail__R_Field_8.newFieldInGroup("pnd_Omni_Detail_Pnd_De_Value", "#DE-VALUE", FieldType.STRING, 40);
        pnd_Compare_Key = localVariables.newFieldInRecord("pnd_Compare_Key", "#COMPARE-KEY", FieldType.STRING, 23);

        pnd_Compare_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Compare_Key__R_Field_9", "REDEFINE", pnd_Compare_Key);
        pnd_Compare_Key_Pnd_Comp_Plan_No = pnd_Compare_Key__R_Field_9.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Plan_No", "#COMP-PLAN-NO", FieldType.STRING, 
            6);
        pnd_Compare_Key_Pnd_Comp_Part_Id = pnd_Compare_Key__R_Field_9.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Part_Id", "#COMP-PART-ID", FieldType.STRING, 
            17);

        pnd_Compare_Key__R_Field_10 = pnd_Compare_Key__R_Field_9.newGroupInGroup("pnd_Compare_Key__R_Field_10", "REDEFINE", pnd_Compare_Key_Pnd_Comp_Part_Id);
        pnd_Compare_Key_Pnd_Comp_Ssn = pnd_Compare_Key__R_Field_10.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Ssn", "#COMP-SSN", FieldType.STRING, 9);
        pnd_Compare_Key_Pnd_Comp_Subplan_No = pnd_Compare_Key__R_Field_10.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Subplan_No", "#COMP-SUBPLAN-NO", FieldType.STRING, 
            6);
        pnd_Compare_Key_Pnd_Comp_Part_Ext = pnd_Compare_Key__R_Field_10.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Part_Ext", "#COMP-PART-EXT", FieldType.STRING, 
            2);

        pnd_Accepted_Report_Rec = localVariables.newGroupInRecord("pnd_Accepted_Report_Rec", "#ACCEPTED-REPORT-REC");
        pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No", "#REPT-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Accepted_Report_Rec_Pnd_Rept_Divsub = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Divsub", "#REPT-DIVSUB", FieldType.STRING, 
            4);
        pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract", "#REPT-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract", "#REPT-CREF-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Accepted_Report_Rec_Pnd_Rept_Ssn = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Ssn", "#REPT-SSN", FieldType.STRING, 
            9);
        pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No", "#REPT-SUBPLAN-NO", 
            FieldType.STRING, 6);

        pnd_Rejected_Report_Rec = localVariables.newGroupInRecord("pnd_Rejected_Report_Rec", "#REJECTED-REPORT-REC");
        pnd_Rejected_Report_Rec_Pnd_Error_Plan_No = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Plan_No", "#ERROR-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Divsub = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Divsub", "#ERROR-DIVSUB", 
            FieldType.STRING, 4);
        pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract", "#ERROR-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract", "#ERROR-CREF-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Rejected_Report_Rec_Pnd_Error_Ssn = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Ssn", "#ERROR-SSN", FieldType.STRING, 
            9);
        pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No", "#ERROR-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg", "#ERROR-RETURN-MSG", 
            FieldType.STRING, 50);

        pnd_Summary_Report_Rec = localVariables.newGroupInRecord("pnd_Summary_Report_Rec", "#SUMMARY-REPORT-REC");
        pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt", "#TOT-TRANS-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt", "#TOT-ACCEPT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt", "#TOT-REJECT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Restarted = localVariables.newFieldInRecord("pnd_Restarted", "#RESTARTED", FieldType.BOOLEAN, 1);
        pnd_Skip_Restart_Record = localVariables.newFieldInRecord("pnd_Skip_Restart_Record", "#SKIP-RESTART-RECORD", FieldType.BOOLEAN, 1);
        pnd_Hold_Part_Id = localVariables.newFieldInRecord("pnd_Hold_Part_Id", "#HOLD-PART-ID", FieldType.STRING, 17);
        pnd_Address_Standardized = localVariables.newFieldInRecord("pnd_Address_Standardized", "#ADDRESS-STANDARDIZED", FieldType.BOOLEAN, 1);
        pnd_Record_Cnt = localVariables.newFieldInRecord("pnd_Record_Cnt", "#RECORD-CNT", FieldType.NUMERIC, 7);
        pnd_Error_Rec_Cnt = localVariables.newFieldInRecord("pnd_Error_Rec_Cnt", "#ERROR-REC-CNT", FieldType.NUMERIC, 7);
        pnd_Max_Cards = localVariables.newFieldInRecord("pnd_Max_Cards", "#MAX-CARDS", FieldType.NUMERIC, 2);
        pnd_Card_Cnt = localVariables.newFieldInRecord("pnd_Card_Cnt", "#CARD-CNT", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Retry_Cnt = localVariables.newFieldInRecord("pnd_Retry_Cnt", "#RETRY-CNT", FieldType.NUMERIC, 3);
        intvl = localVariables.newFieldInRecord("intvl", "INTVL", FieldType.INTEGER, 1);
        reqid = localVariables.newFieldInRecord("reqid", "REQID", FieldType.STRING, 8);
        pnd_Jobname = localVariables.newFieldInRecord("pnd_Jobname", "#JOBNAME", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_batch_Restart.reset();

        localVariables.reset();
        pnd_Restarted.setInitialValue(false);
        pnd_Skip_Restart_Record.setInitialValue(false);
        pnd_Max_Cards.setInitialValue(14);
        intvl.setInitialValue(1);
        reqid.setInitialValue("MYREQID");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9001() throws Exception
    {
        super("Scib9001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIB9001", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  OVERRIDES NATURAL ERROR ROUTINE
        //*  THIS REPLACES *STARTUP LOGIC.
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 133 PS = 60;//Natural: FORMAT ( 3 ) LS = 133 PS = 60
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pls_Dar_Program.setValue(Global.getPROGRAM());                                                                                                                    //Natural: ASSIGN +DAR-PROGRAM := *PROGRAM
        //*  CAUSES A TERMINATION IN THE EVENT OF
        if (condition(pls_Error_Occurence.getBoolean()))                                                                                                                  //Natural: IF +ERROR-OCCURENCE
        {
            //*  AN ERROR. NEEDED TO FORCE OPERATOR RESTART.
            DbsUtil.terminate(5);  if (true) return;                                                                                                                      //Natural: TERMINATE 5
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-RESTART
        sub_Check_For_Restart();
        if (condition(Global.isEscape())) {return;}
        Global.getERROR_TA().setValue(" ");                                                                                                                               //Natural: ASSIGN *ERROR-TA := ' '
        //* *======================================================================
        //* *    M A I N    P R O C E S S
        //* *======================================================================
        pnd_Omni_Detail.reset();                                                                                                                                          //Natural: RESET #OMNI-DETAIL #COMPARE-KEY
        pnd_Compare_Key.reset();
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getWorkFiles().read(1, pnd_Omni_Record);                                                                                                                      //Natural: READ WORK FILE 1 ONCE #OMNI-RECORD
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                if (condition(! (pnd_Restarted.getBoolean())))                                                                                                            //Natural: IF NOT #RESTARTED
                {
                                                                                                                                                                          //Natural: PERFORM DELETE-RESTART-RECORD
                    sub_Delete_Restart_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
                sub_Write_Summary_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 2 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 3 )
            if (condition(pnd_Restarted.getBoolean()))                                                                                                                    //Natural: IF #RESTARTED
            {
                if (condition(pnd_Omni_Record_Pnd_Part_Id.equals(pnd_Restart_Key_Pnd_Restart_Part_Id)))                                                                   //Natural: IF #OMNI-RECORD.#PART-ID = #RESTART-PART-ID
                {
                    pnd_Skip_Restart_Record.setValue(true);                                                                                                               //Natural: ASSIGN #SKIP-RESTART-RECORD := TRUE
                    pnd_Restarted.reset();                                                                                                                                //Natural: RESET #RESTARTED
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Compare_Key_Pnd_Comp_Part_Id.reset();                                                                                                                     //Natural: RESET #COMP-PART-ID
            pnd_Compare_Key_Pnd_Comp_Plan_No.setValue(pnd_Omni_Record_Pnd_Plan_No);                                                                                       //Natural: ASSIGN #COMP-PLAN-NO = #OMNI-RECORD.#PLAN-NO
            pnd_Compare_Key_Pnd_Comp_Subplan_No.setValue(pnd_Omni_Record_Pnd_Subplan_No);                                                                                 //Natural: ASSIGN #COMP-SUBPLAN-NO = #OMNI-RECORD.#SUBPLAN-NO
            pnd_Compare_Key_Pnd_Comp_Ssn.setValue(pnd_Omni_Record_Pnd_Ssn);                                                                                               //Natural: ASSIGN #COMP-SSN = #OMNI-RECORD.#SSN
            pnd_Compare_Key_Pnd_Comp_Part_Ext.setValue(pnd_Omni_Record_Pnd_Part_Ext);                                                                                     //Natural: ASSIGN #COMP-PART-EXT = #OMNI-RECORD.#PART-EXT
                                                                                                                                                                          //Natural: PERFORM UPDATE-BATCH-RESTART-RECORD
            sub_Update_Batch_Restart_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-T813-HEADER
            sub_Write_T813_Header();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ONEIRA
            if (condition(pnd_Omni_Record_Pnd_T813_Oneira_Acct_No.greater(" ")))                                                                                          //Natural: IF #T813-ONEIRA-ACCT-NO > ' '
            {
                //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM CREATE-T813-TRAILERS-FOR-ONEIRA
                sub_Create_T813_Trailers_For_Oneira();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  TNGSUB
                if (condition(pnd_Omni_Record_Pnd_T813_Substitution_Contract_Ind.equals(" ")))                                                                            //Natural: IF #T813-SUBSTITUTION-CONTRACT-IND = ' '
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-T813-TRAILERS
                    sub_Create_T813_Trailers();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-T813-TRAILERS-FOR-IRASUB
                    sub_Create_T813_Trailers_For_Irasub();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  TNGSUB
                }                                                                                                                                                         //Natural: END-IF
                //*  ONEIRA
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-REC
            sub_Write_Report_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  NO INPUT RECORDS TO PROCESS
        if (condition(pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.equals(getZero())))                                                                                        //Natural: IF #TOT-TRANS-CNT = 0
        {
            getReports().display(0, "***** INTERFACE FILE IS EMPTY *****");                                                                                               //Natural: DISPLAY '***** INTERFACE FILE IS EMPTY *****'
            if (Global.isEscape()) return;
            DbsUtil.terminate(1);  if (true) return;                                                                                                                      //Natural: TERMINATE 1
        }                                                                                                                                                                 //Natural: END-IF
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-T813-HEADER
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-T813-TRAILERS
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-T813-TRAILERS-FOR-IRASUB
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-T813-TRAILERS-FOR-ONEIRA
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-REC
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REC
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-RESTART
        //* *=====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-BATCH-RESTART-RECORD
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-RESTART-RECORD
        //* *======================================================================
        //*  RETRY ERROR LOGIC
        //* *======================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
        //* *======================================================================
    }
    private void sub_Write_T813_Header() throws Exception                                                                                                                 //Natural: WRITE-T813-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOT-TRANS-CNT
        pnd_Omni_Header.reset();                                                                                                                                          //Natural: RESET #OMNI-HEADER #HOLD-PART-ID #CARD-CNT
        pnd_Hold_Part_Id.reset();
        pnd_Card_Cnt.reset();
        pnd_Omni_Header_Pnd_Tran_Code.setValue("813");                                                                                                                    //Natural: ASSIGN #OMNI-HEADER.#TRAN-CODE = '813'
        pnd_Omni_Header_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-HEADER.#SEQ-CODE
        pnd_Omni_Header_Pnd_Rec_Type.setValue("$TIHDR");                                                                                                                  //Natural: ASSIGN #OMNI-HEADER.#REC-TYPE = '$TIHDR'
        pnd_Omni_Header_Pnd_Plan_No.setValue(pnd_Omni_Record_Pnd_Plan_No);                                                                                                //Natural: ASSIGN #OMNI-HEADER.#PLAN-NO = #OMNI-RECORD.#PLAN-NO
        pnd_Hold_Part_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Omni_Record_Pnd_Ssn, pnd_Omni_Record_Pnd_Subplan_No, pnd_Omni_Record_Pnd_Part_Ext)); //Natural: COMPRESS #OMNI-RECORD.#SSN #OMNI-RECORD.#SUBPLAN-NO #OMNI-RECORD.#PART-EXT INTO #HOLD-PART-ID LEAVING NO SPACE
        pnd_Omni_Header_Pnd_Part_Id.setValue(pnd_Hold_Part_Id);                                                                                                           //Natural: ASSIGN #OMNI-HEADER.#PART-ID = #HOLD-PART-ID
        pnd_Omni_Header_Pnd_Trade_Date.setValue(pnd_Omni_Record_Pnd_Trade_Date_N);                                                                                        //Natural: ASSIGN #OMNI-HEADER.#TRADE-DATE = #OMNI-RECORD.#TRADE-DATE-N
        //*                                  TNGSUB
        if (condition(pnd_Omni_Record_Pnd_T813_Deceased_Ind.equals("Y")))                                                                                                 //Natural: IF #T813-DECEASED-IND = 'Y'
        {
            pnd_Omni_Header_Pnd_Deceased.setValue(pnd_Omni_Record_Pnd_T813_Deceased_Ind);                                                                                 //Natural: ASSIGN #DECEASED = #T813-DECEASED-IND
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(2, false, pnd_Omni_Header);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-HEADER
        //*  WRITE-T813-HEADER
    }
    private void sub_Create_T813_Trailers() throws Exception                                                                                                              //Natural: CREATE-T813-TRAILERS
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Omni_Detail.reset();                                                                                                                                          //Natural: RESET #OMNI-DETAIL
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOT-ACCEPT-CNT
        pnd_Omni_Detail_Pnd_Tran_Code.setValue("813");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#TRAN-CODE := '813'
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Plan_No);                                                                                               //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#PLAN-NO
        //*  PH006
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("006");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '006'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Part_Id);                                                                                               //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#PART-ID
        //*  PH007
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("007");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '007'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        //*  PINE
        if (condition(pnd_Omni_Record_Pnd_Pin_No_Filler.equals("00000")))                                                                                                 //Natural: IF #PIN-NO-FILLER = '00000'
        {
            //*  PINE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Pin_No_7);                                                                                          //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#PIN-NO-7
            //*  PINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  PINE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Pin_No);                                                                                            //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#PIN-NO
            //*  PINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PH301
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("301");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '301'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        //*  ONLY PLAN,PIN,PART-ID IS WRITTEN FOR INELIGIBLE OR ELIGIBLE
        //*  NOT PARTICIPATING RECORDS
        //*  RS1
        if (condition(pnd_Omni_Record_Pnd_T813_Eir_Ind.equals("Y")))                                                                                                      //Natural: IF #OMNI-RECORD.#T813-EIR-IND = 'Y'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Tiaa_Contract);                                                                                         //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#TIAA-CONTRACT
        //*  PH655
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("655");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '655'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Cref_Contract);                                                                                         //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#CREF-CONTRACT
        //*  PH656
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("656");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '656'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        //*  CRS 6/20/05
        if (condition(pnd_Omni_Record_Pnd_Tiaa_Issue_Date.greater(" ")))                                                                                                  //Natural: IF #OMNI-RECORD.#TIAA-ISSUE-DATE GT ' '
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Tiaa_Issue_Date);                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#TIAA-ISSUE-DATE
            //*  PH771
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("771");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '771'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        //*  CRS 6/20/05
        if (condition(pnd_Omni_Record_Pnd_Cref_Issue_Date.greater(" ")))                                                                                                  //Natural: IF #OMNI-RECORD.#CREF-ISSUE-DATE GT ' '
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Cref_Issue_Date);                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#CREF-ISSUE-DATE
            //*  PH772
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("772");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '772'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        //*  REJECT KG
        //*  ACIS PRAP VALUE
        short decideConditionsMet560 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #OMNI-RECORD.#VESTING;//Natural: VALUE '1'
        if (condition((pnd_Omni_Record_Pnd_Vesting.equals("1"))))
        {
            decideConditionsMet560++;
            //*  PARTICIPANT OWNED OMNI
            //*  ACIS PRAP VALUE
            pnd_Omni_Record_Pnd_Vesting.setValue("0");                                                                                                                    //Natural: ASSIGN #OMNI-RECORD.#VESTING = '0'
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pnd_Omni_Record_Pnd_Vesting.equals("3"))))
        {
            decideConditionsMet560++;
            //*  INSTITUTION OWNED OMNI
            //*  ACIS PRAP VALUE
            pnd_Omni_Record_Pnd_Vesting.setValue("1");                                                                                                                    //Natural: ASSIGN #OMNI-RECORD.#VESTING = '1'
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pnd_Omni_Record_Pnd_Vesting.equals("2"))))
        {
            decideConditionsMet560++;
            //*  DELAYED VESTING OMNI
            pnd_Omni_Record_Pnd_Vesting.setValue("2");                                                                                                                    //Natural: ASSIGN #OMNI-RECORD.#VESTING = '2'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Vesting);                                                                                               //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#VESTING
        //*  PH652
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("652");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '652'
        //*  REJECT KG END
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Issue_State);                                                                                           //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#ISSUE-STATE
        //*  PH624
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("624");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '624'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Annuity_Start_Date);                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#ANNUITY-START-DATE
        //*  PH660
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("660");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '660'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        //*  CRS 1/13/05 \/
        if (condition(pnd_Omni_Record_Pnd_Update_App_Rcvd.equals("Y")))                                                                                                   //Natural: IF #UPDATE-APP-RCVD = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_App_Rcvd_Date);                                                                                     //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#APP-RCVD-DATE
            //*  PH773
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("773");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '773'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
            //*  CRS 1/13/05 /\
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Register_Id.greater(" ")))                                                                                                      //Natural: IF #OMNI-RECORD.#REGISTER-ID GT ' '
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Register_Id);                                                                                       //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#REGISTER-ID
            //*  PH657
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("657");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '657'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        //*  REJECT KG
        if (condition(pnd_Omni_Record_Pnd_Enrollment_Type.equals("U")))                                                                                                   //Natural: IF #OMNI-RECORD.#ENROLLMENT-TYPE = 'U'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Enrollment_Type);                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#ENROLLMENT-TYPE
            //*  PH637
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("637");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '637'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
            //*  REJECT KG END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Divorce.equals("Y")))                                                                                                    //Natural: IF #UPDATE-DIVORCE = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH625
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("625");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '625'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Add_Cref.equals("Y")))                                                                                                   //Natural: IF #UPDATE-ADD-CREF = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH629
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("629");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '629'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Pull_Code.equals("Y")))                                                                                                  //Natural: IF #UPDATE-PULL-CODE = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH636
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("636");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '636'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Incompl_Data_Ind.equals("Y")))                                                                                           //Natural: IF #UPDATE-INCOMPL-DATA-IND = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH627
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("627");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '627'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Alloc_Model_Ind.equals("Y")))                                                                                            //Natural: IF #UPDATE-ALLOC-MODEL-IND = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH640
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("640");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '640'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Cai_Ind.equals("Y")))                                                                                                    //Natural: IF #UPDATE-CAI-IND = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH628
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("628");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '628'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-T813-TRAILERS
    }
    //*  TNGSUB
    private void sub_Create_T813_Trailers_For_Irasub() throws Exception                                                                                                   //Natural: CREATE-T813-TRAILERS-FOR-IRASUB
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Omni_Detail.reset();                                                                                                                                          //Natural: RESET #OMNI-DETAIL
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOT-ACCEPT-CNT
        pnd_Omni_Detail_Pnd_Tran_Code.setValue("813");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#TRAN-CODE := '813'
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Tiaa_Contract);                                                                                         //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#TIAA-CONTRACT
        //*  PH638
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("638");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '638'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Tiaa_Issue_Date);                                                                                       //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#TIAA-ISSUE-DATE
        //*  PH639
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("639");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '639'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        if (condition(pnd_Omni_Record_Pnd_Annuity_Start_Date.greater(" ")))                                                                                               //Natural: IF #OMNI-RECORD.#ANNUITY-START-DATE GT ' '
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Annuity_Start_Date);                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#ANNUITY-START-DATE
            //*  PH660
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("660");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '660'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue("B");                                                                                                                       //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'B'
        //*  PH809
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("809");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '809'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        //*  CREATE-T813-TRAILERS-FOR-IRASUB
    }
    //*  ONEIRA
    private void sub_Create_T813_Trailers_For_Oneira() throws Exception                                                                                                   //Natural: CREATE-T813-TRAILERS-FOR-ONEIRA
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Omni_Detail.reset();                                                                                                                                          //Natural: RESET #OMNI-DETAIL
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOT-ACCEPT-CNT
        pnd_Omni_Detail_Pnd_Tran_Code.setValue("813");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#TRAN-CODE := '813'
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        //*  PINE
        if (condition(pnd_Omni_Record_Pnd_Pin_No_Filler.equals("00000")))                                                                                                 //Natural: IF #PIN-NO-FILLER = '00000'
        {
            //*  PINE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Pin_No_7);                                                                                          //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#PIN-NO-7
            //*  PINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  PINE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Pin_No);                                                                                            //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#PIN-NO
            //*  PINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PH301
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("301");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '301'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Annuity_Start_Date);                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#ANNUITY-START-DATE
        //*  PH660
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("660");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '660'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Issue_State);                                                                                           //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#ISSUE-STATE
        //*  PH624
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("624");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '624'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        if (condition(pnd_Omni_Record_Pnd_Register_Id.greater(" ")))                                                                                                      //Natural: IF #OMNI-RECORD.#REGISTER-ID GT ' '
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Register_Id);                                                                                       //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#REGISTER-ID
            //*  PH657
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("657");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '657'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        //*  REJECT KG
        if (condition(pnd_Omni_Record_Pnd_Enrollment_Type.equals("U")))                                                                                                   //Natural: IF #OMNI-RECORD.#ENROLLMENT-TYPE = 'U'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Enrollment_Type);                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#ENROLLMENT-TYPE
            //*  PH637
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("637");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '637'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
            //*  REJECT KG END
        }                                                                                                                                                                 //Natural: END-IF
        //*  REJECT KG
        //*  ACIS PRAP VALUE
        short decideConditionsMet784 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #OMNI-RECORD.#VESTING;//Natural: VALUE '1'
        if (condition((pnd_Omni_Record_Pnd_Vesting.equals("1"))))
        {
            decideConditionsMet784++;
            //*  PARTICIPANT OWNED OMNI
            //*  ACIS PRAP VALUE
            pnd_Omni_Record_Pnd_Vesting.setValue("0");                                                                                                                    //Natural: ASSIGN #OMNI-RECORD.#VESTING = '0'
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pnd_Omni_Record_Pnd_Vesting.equals("3"))))
        {
            decideConditionsMet784++;
            //*  INSTITUTION OWNED OMNI
            //*  ACIS PRAP VALUE
            pnd_Omni_Record_Pnd_Vesting.setValue("1");                                                                                                                    //Natural: ASSIGN #OMNI-RECORD.#VESTING = '1'
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pnd_Omni_Record_Pnd_Vesting.equals("2"))))
        {
            decideConditionsMet784++;
            //*  DELAYED VESTING OMNI
            pnd_Omni_Record_Pnd_Vesting.setValue("2");                                                                                                                    //Natural: ASSIGN #OMNI-RECORD.#VESTING = '2'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(pnd_Omni_Record_Pnd_Vesting);                                                                                               //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := #OMNI-RECORD.#VESTING
        //*  PH652
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("652");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '652'
        //*  REJECT KG END
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        if (condition(pnd_Omni_Record_Pnd_Update_Divorce.equals("Y")))                                                                                                    //Natural: IF #UPDATE-DIVORCE = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH625
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("625");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '625'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Add_Cref.equals("Y")))                                                                                                   //Natural: IF #UPDATE-ADD-CREF = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH629
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("629");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '629'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Pull_Code.equals("Y")))                                                                                                  //Natural: IF #UPDATE-PULL-CODE = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH636
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("636");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '636'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Incompl_Data_Ind.equals("Y")))                                                                                           //Natural: IF #UPDATE-INCOMPL-DATA-IND = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH627
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("627");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '627'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Alloc_Model_Ind.equals("Y")))                                                                                            //Natural: IF #UPDATE-ALLOC-MODEL-IND = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH640
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("640");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '640'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Omni_Record_Pnd_Update_Cai_Ind.equals("Y")))                                                                                                    //Natural: IF #UPDATE-CAI-IND = 'Y'
        {
            pnd_Card_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CARD-CNT
            pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                           //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
            pnd_Omni_Detail_Pnd_De_Value.setValue("N");                                                                                                                   //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := 'N'
            //*  PH628
            pnd_Omni_Detail_Pnd_Data_Elem.setValue("628");                                                                                                                //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '628'
            getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                              //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-T813-TRAILERS-FOR-ONEIRA
    }
    private void sub_Write_Report_Rec() throws Exception                                                                                                                  //Natural: WRITE-REPORT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Accepted_Report_Rec.reset();                                                                                                                                  //Natural: RESET #ACCEPTED-REPORT-REC
        pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No.setValue(pnd_Omni_Record_Pnd_Plan_No);                                                                                   //Natural: ASSIGN #REPT-PLAN-NO := #OMNI-RECORD.#PLAN-NO
        pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No.setValue(pnd_Omni_Record_Pnd_Subplan_No);                                                                             //Natural: ASSIGN #REPT-SUBPLAN-NO := #OMNI-RECORD.#SUBPLAN-NO
        pnd_Accepted_Report_Rec_Pnd_Rept_Divsub.setValue(pnd_Omni_Record_Pnd_Divsub);                                                                                     //Natural: ASSIGN #REPT-DIVSUB := #OMNI-RECORD.#DIVSUB
        pnd_Accepted_Report_Rec_Pnd_Rept_Ssn.setValue(pnd_Omni_Record_Pnd_Ssn);                                                                                           //Natural: ASSIGN #REPT-SSN := #OMNI-RECORD.#SSN
        //*  DON't write TIAA/CREF contract for ineligible or eligible not
        //*  PARTICIPATIONG RECORDS
        //*  RS1
        if (condition(pnd_Omni_Record_Pnd_T813_Eir_Ind.equals("Y")))                                                                                                      //Natural: IF #OMNI-RECORD.#T813-EIR-IND = 'Y'
        {
            pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract.reset();                                                                                                       //Natural: RESET #REPT-TIAA-CONTRACT #REPT-CREF-CONTRACT
            pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract.reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract.setValue(pnd_Omni_Record_Pnd_Tiaa_Contract);                                                                   //Natural: ASSIGN #REPT-TIAA-CONTRACT := #OMNI-RECORD.#TIAA-CONTRACT
            pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract.setValue(pnd_Omni_Record_Pnd_Cref_Contract);                                                                   //Natural: ASSIGN #REPT-CREF-CONTRACT := #OMNI-RECORD.#CREF-CONTRACT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,"Plan Number:",pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No,"SubPlan:",pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No,NEWLINE,"Div/Sub:",pnd_Accepted_Report_Rec_Pnd_Rept_Divsub,NEWLINE,"SSN:",pnd_Accepted_Report_Rec_Pnd_Rept_Ssn,  //Natural: WRITE ( 1 ) NOTITLE 'Plan Number:' #REPT-PLAN-NO 'SubPlan:' #REPT-SUBPLAN-NO / 'Div/Sub:' #REPT-DIVSUB / 'SSN:' #REPT-SSN ( EM = XXX-XX-XXXX ) / 'TIAA Contract:' #REPT-TIAA-CONTRACT 'CREF Contract:' #REPT-CREF-CONTRACT / '-----------------------------------------------' /
            new ReportEditMask ("XXX-XX-XXXX"),NEWLINE,"TIAA Contract:",pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract,"CREF Contract:",pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract,
            NEWLINE,"-----------------------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(1).less(10)))                                                                                                          //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-REPORT-REC
    }
    private void sub_Write_Error_Rec() throws Exception                                                                                                                   //Natural: WRITE-ERROR-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Rejected_Report_Rec.reset();                                                                                                                                  //Natural: RESET #REJECTED-REPORT-REC
        pnd_Error_Rec_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-REC-CNT
        //*  RESTART RECORD WILL NOT BE PROCESSED
        if (condition(pnd_Skip_Restart_Record.getBoolean()))                                                                                                              //Natural: IF #SKIP-RESTART-RECORD
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg.setValue("A PROCESSING ERROR HAS OCCURRED - RESTART RECORD");                                                    //Natural: MOVE 'A PROCESSING ERROR HAS OCCURRED - RESTART RECORD' TO #ERROR-RETURN-MSG
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rejected_Report_Rec_Pnd_Error_Plan_No.setValue(pnd_Omni_Record_Pnd_Plan_No);                                                                                  //Natural: ASSIGN #ERROR-PLAN-NO := #OMNI-RECORD.#PLAN-NO
        pnd_Rejected_Report_Rec_Pnd_Error_Divsub.setValue(pnd_Omni_Record_Pnd_Divsub);                                                                                    //Natural: ASSIGN #ERROR-DIVSUB := #OMNI-RECORD.#DIVSUB
        pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No.setValue(pnd_Omni_Record_Pnd_Subplan_No);                                                                            //Natural: ASSIGN #ERROR-SUBPLAN-NO := #OMNI-RECORD.#SUBPLAN-NO
        pnd_Rejected_Report_Rec_Pnd_Error_Ssn.setValue(pnd_Omni_Record_Pnd_Ssn);                                                                                          //Natural: ASSIGN #ERROR-SSN := #OMNI-RECORD.#SSN
        //*  DON't write TIAA/CREF contract for ineligible or eligible not
        //*  PARTICIPATIONG RECORDS
        //*  RS1
        if (condition(pnd_Omni_Record_Pnd_T813_Eir_Ind.equals("Y")))                                                                                                      //Natural: IF #OMNI-RECORD.#T813-EIR-IND = 'Y'
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract.reset();                                                                                                      //Natural: RESET #ERROR-TIAA-CONTRACT #ERROR-CREF-CONTRACT
            pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract.reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract.setValue(pnd_Omni_Record_Pnd_Tiaa_Contract);                                                                  //Natural: ASSIGN #ERROR-TIAA-CONTRACT := #OMNI-RECORD.#TIAA-CONTRACT
            pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract.setValue(pnd_Omni_Record_Pnd_Cref_Contract);                                                                  //Natural: ASSIGN #ERROR-CREF-CONTRACT := #OMNI-RECORD.#CREF-CONTRACT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,"Plan Number:",pnd_Rejected_Report_Rec_Pnd_Error_Plan_No,"SubPlan:",pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No,NEWLINE,"Div/Sub:",pnd_Rejected_Report_Rec_Pnd_Error_Divsub,NEWLINE,"SSN:",pnd_Rejected_Report_Rec_Pnd_Error_Ssn,  //Natural: WRITE ( 2 ) NOTITLE 'Plan Number:' #ERROR-PLAN-NO 'SubPlan:' #ERROR-SUBPLAN-NO / 'Div/Sub:' #ERROR-DIVSUB / 'SSN:' #ERROR-SSN ( EM = XXX-XX-XXXX ) / 'TIAA Contract:' #ERROR-TIAA-CONTRACT 'CREF Contract:' #ERROR-CREF-CONTRACT / 'Error Message' #ERROR-RETURN-MSG // '-----------------------------------------------' /
            new ReportEditMask ("XXX-XX-XXXX"),NEWLINE,"TIAA Contract:",pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract,"CREF Contract:",pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract,
            NEWLINE,"Error Message",pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg,NEWLINE,NEWLINE,"-----------------------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(2).less(10)))                                                                                                          //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-ERROR-REC
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(3, ReportOption.NOTITLE,"Total Records Read    :",pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Records Written :",pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt,  //Natural: WRITE ( 3 ) NOTITLE 'Total Records Read    :' #TOT-TRANS-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Records Written :' #TOT-ACCEPT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Records Rejected:' #TOT-REJECT-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Records Rejected:",pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  WRITE-SUMMARY-REPORT
    }
    private void sub_Check_For_Restart() throws Exception                                                                                                                 //Natural: CHECK-FOR-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind.setValue("A");                                                                                                            //Natural: ASSIGN #RST-ACTVE-IND = 'A'
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                            //Natural: ASSIGN #RST-JOB-NME = *INIT-USER
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme.setValue(Global.getINIT_ID());                                                                                             //Natural: ASSIGN #RST-STEP-NME = *INIT-ID
        vw_batch_Restart.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) BATCH-RESTART WITH SP-ACTVE-JOB-STEP = #SP-ACTVE-JOB-STEP
        (
        "FIND_R",
        new Wc[] { new Wc("SP_ACTVE_JOB_STEP", "=", pnd_Sp_Actve_Job_Step, WcType.WITH) },
        1
        );
        FIND_R:
        while (condition(vw_batch_Restart.readNextRow("FIND_R", true)))
        {
            vw_batch_Restart.setIfNotFoundControlFlag(false);
            //*  CREATE A RECORD IF NONE EXISTS.
            if (condition(vw_batch_Restart.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                batch_Restart_Rst_Actve_Ind.setValue("A");                                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-ACTVE-IND := 'A'
                batch_Restart_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-JOB-NME := *INIT-USER
                batch_Restart_Rst_Jobstep_Nme.setValue(Global.getINIT_ID());                                                                                              //Natural: ASSIGN BATCH-RESTART.RST-JOBSTEP-NME := *INIT-ID
                batch_Restart_Rst_Pgm_Nme.setValue(Global.getPROGRAM());                                                                                                  //Natural: ASSIGN BATCH-RESTART.RST-PGM-NME := *PROGRAM
                batch_Restart_Rst_Curr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Curr_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Start_Dte.setValue(batch_Restart_Rst_Curr_Dte);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-DTE := BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Start_Tme.setValue(batch_Restart_Rst_Curr_Tme);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-TME := BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Frst_Run_Dte.setValue(batch_Restart_Rst_Start_Dte);                                                                                     //Natural: ASSIGN BATCH-RESTART.RST-FRST-RUN-DTE := BATCH-RESTART.RST-START-DTE
                pnd_Restart_Key_Pnd_Restart_Plan_No.setValue(" ");                                                                                                        //Natural: ASSIGN #RESTART-KEY.#RESTART-PLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Subplan_No.setValue(" ");                                                                                                     //Natural: ASSIGN #RESTART-KEY.#RESTART-SUBPLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Ssn.setValue(" ");                                                                                                            //Natural: ASSIGN #RESTART-KEY.#RESTART-SSN := ' '
                pnd_Restart_Key_Pnd_Restart_Ext.setValue(" ");                                                                                                            //Natural: ASSIGN #RESTART-KEY.#RESTART-EXT := ' '
                batch_Restart_Rst_Key.setValue(pnd_Restart_Key);                                                                                                          //Natural: ASSIGN BATCH-RESTART.RST-KEY := #RESTART-KEY
                STORE_REST:                                                                                                                                               //Natural: STORE BATCH-RESTART
                vw_batch_Restart.insertDBRow("STORE_REST");
                pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                           //Natural: ASSIGN #RESTART-ISN := *ISN ( STORE-REST. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(0, "********************************************************");                                                                        //Natural: WRITE '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***");                                                                        //Natural: WRITE '***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "********************************************************");                                                                        //Natural: WRITE '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, NEWLINE,NEWLINE);                                                                                                                   //Natural: WRITE //
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break FIND_R;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FIND-R. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                               //Natural: ASSIGN #RESTART-ISN := *ISN ( FIND-R. )
            pnd_Restart_Key.setValue(batch_Restart_Rst_Key);                                                                                                              //Natural: ASSIGN #RESTART-KEY := BATCH-RESTART.RST-KEY
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Key,"***");                                                                                                 //Natural: WRITE '***' '=' BATCH-RESTART.RST-KEY '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Dte,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-DTE '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Tme,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Last_Rstrt_Tme,"***");                                                                                      //Natural: WRITE '***' '=' BATCH-RESTART.RST-LAST-RSTRT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",pnd_Restart_Isn,"***");                                                                                                       //Natural: WRITE '***' '=' #RESTART-ISN '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Restarted.setValue(true);                                                                                                                                 //Natural: ASSIGN #RESTARTED := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CHECK-FOR-RESTART
    }
    private void sub_Update_Batch_Restart_Record() throws Exception                                                                                                       //Natural: UPDATE-BATCH-RESTART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *=====================================================================
        GET_U:                                                                                                                                                            //Natural: GET BATCH-RESTART #RESTART-ISN
        vw_batch_Restart.readByID(pnd_Restart_Isn.getLong(), "GET_U");
        pnd_Restart_Key_Pnd_Restart_Plan_No.setValue(pnd_Omni_Record_Pnd_Plan_No);                                                                                        //Natural: ASSIGN #RESTART-KEY.#RESTART-PLAN-NO = #OMNI-RECORD.#PLAN-NO
        pnd_Restart_Key_Pnd_Restart_Subplan_No.setValue(pnd_Omni_Record_Pnd_Subplan_No);                                                                                  //Natural: ASSIGN #RESTART-KEY.#RESTART-SUBPLAN-NO = #OMNI-RECORD.#SUBPLAN-NO
        pnd_Restart_Key_Pnd_Restart_Ssn.setValue(pnd_Omni_Record_Pnd_Ssn);                                                                                                //Natural: ASSIGN #RESTART-KEY.#RESTART-SSN = #OMNI-RECORD.#SSN
        pnd_Restart_Key_Pnd_Restart_Ext.setValue(pnd_Omni_Record_Pnd_Part_Ext);                                                                                           //Natural: ASSIGN #RESTART-KEY.#RESTART-EXT = #OMNI-RECORD.#PART-EXT
        batch_Restart_Rst_Curr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-CURR-DTE
        batch_Restart_Rst_Curr_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                                       //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-CURR-TME
        batch_Restart_Rst_Start_Dte.setValue(batch_Restart_Rst_Curr_Dte);                                                                                                 //Natural: ASSIGN BATCH-RESTART.RST-START-DTE = BATCH-RESTART.RST-CURR-DTE
        batch_Restart_Rst_Start_Tme.setValue(batch_Restart_Rst_Curr_Tme);                                                                                                 //Natural: ASSIGN BATCH-RESTART.RST-START-TME = BATCH-RESTART.RST-CURR-TME
        batch_Restart_Rst_End_Dte.reset();                                                                                                                                //Natural: RESET BATCH-RESTART.RST-END-DTE BATCH-RESTART.RST-END-TME
        batch_Restart_Rst_End_Tme.reset();
        batch_Restart_Rst_Key.setValue(pnd_Restart_Key);                                                                                                                  //Natural: ASSIGN BATCH-RESTART.RST-KEY := #RESTART-KEY
        getReports().write(0, "*************************************************");                                                                                       //Natural: WRITE '*************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "        ---- NEW RESTART RECORD KEY ----         ");                                                                                       //Natural: WRITE '        ---- NEW RESTART RECORD KEY ----         '
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Restart_Key);                                                                                                                       //Natural: WRITE '=' #RESTART-KEY
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Updt_Dte);                                                                                                            //Natural: WRITE '=' BATCH-RESTART.RST-UPDT-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Updt_Tme);                                                                                                            //Natural: WRITE '=' BATCH-RESTART.RST-UPDT-TME
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Last_Rstrt_Tme);                                                                                                      //Natural: WRITE '=' BATCH-RESTART.RST-LAST-RSTRT-TME
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Last_Rstrt_Dte);                                                                                                      //Natural: WRITE '=' BATCH-RESTART.RST-LAST-RSTRT-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************");                                                                                       //Natural: WRITE '*************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE," ");                                                                                                                       //Natural: WRITE // ' '
        if (Global.isEscape()) return;
        batch_Restart_Rst_Rstrt_Data_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN BATCH-RESTART.RST-RSTRT-DATA-IND := 'Y'
        vw_batch_Restart.updateDBRow("GET_U");                                                                                                                            //Natural: UPDATE ( GET-U. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  UPDATE-BATCH-RESTART-RECORD
    }
    private void sub_Delete_Restart_Record() throws Exception                                                                                                             //Natural: DELETE-RESTART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //* ****************** UPDATE BATCH RESTART FOR NORMAL EOJ **************
        //*  THIS LOGIC DELETES THE BATCH RESTART RECORD, INDICATING THAT THE
        //*  JOB HAS BEEN RUN SUCESSFULLY.
        //* *********************************************************************
        GET_R:                                                                                                                                                            //Natural: GET BATCH-RESTART #RESTART-ISN
        vw_batch_Restart.readByID(pnd_Restart_Isn.getLong(), "GET_R");
        batch_Restart_Rst_Key.reset();                                                                                                                                    //Natural: RESET BATCH-RESTART.RST-KEY BATCH-RESTART.RST-CNT BATCH-RESTART.RST-ISN-NBR BATCH-RESTART.RST-CURR-DTE BATCH-RESTART.RST-CURR-TME BATCH-RESTART.RST-DATA-GRP ( * ) BATCH-RESTART.RST-RSTRT-DATA-IND BATCH-RESTART.RST-ACTVE-IND
        batch_Restart_Rst_Cnt.reset();
        batch_Restart_Rst_Isn_Nbr.reset();
        batch_Restart_Rst_Curr_Dte.reset();
        batch_Restart_Rst_Curr_Tme.reset();
        batch_Restart_Rst_Data_Grp.getValue("*").reset();
        batch_Restart_Rst_Rstrt_Data_Ind.reset();
        batch_Restart_Rst_Actve_Ind.reset();
        batch_Restart_Rst_End_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-END-DTE
        batch_Restart_Rst_End_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                                        //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-END-TME
        vw_batch_Restart.deleteDBRow("GET_R");                                                                                                                            //Natural: DELETE ( GET-R. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  RESET-RESTART-RECORD
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"ACIS To OMNI Interface Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 25T 'ACIS To OMNI Interface Report' 63T 'Run Date:' *DATX / 25T '     Accepted Records        ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"     Accepted Records        ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 1 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"ACIS To OMNI Error Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 02 ) NOTITLE NOHDR 25T 'ACIS To OMNI Error Report' 63T 'Run Date:' *DATX / 25T '    Rejected Records     ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"    Rejected Records     ",new TabSetting(63),"Run Time:",Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"ACIS To OMNI Summary Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 25T 'ACIS To OMNI Summary Report' 63T 'Run Date:' *DATX / 25T '      Process Totals       ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"      Process Totals       ",new TabSetting(63),"Run Time:",Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        short decideConditionsMet412 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet412++;
            if (condition(pnd_Retry_Cnt.less(25)))                                                                                                                        //Natural: IF #RETRY-CNT < 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.equals(25)))                                                                                                                      //Natural: IF #RETRY-CNT = 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",intvl,reqid);                                                                                     //Natural: CALL 'CMROLL' INTVL REQID
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(25) && pnd_Retry_Cnt.lessOrEqual(50)))                                                                                    //Natural: IF #RETRY-CNT > 25 AND #RETRY-CNT <= 50
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(50)))                                                                                                                     //Natural: IF #RETRY-CNT > 50
            {
                getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                        //Natural: WRITE NOHDR '*****************************' /
                getReports().write(0, ReportOption.NOHDR,"* ADABAS RECORD ON HOLD     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* ADABAS RECORD ON HOLD     *' /
                getReports().write(0, ReportOption.NOHDR,"* RETRY COUNT EXCEEDED.     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* RETRY COUNT EXCEEDED.     *' /
                getReports().write(0, ReportOption.NOHDR,"* BACKOUT UPDATES.          *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* BACKOUT UPDATES.          *' /
                getReports().write(0, ReportOption.NOHDR,"*****************************");                                                                                //Natural: WRITE NOHDR '*****************************'
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",                  //Natural: WRITE NOHDR '****************************' / 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE / '****************************' /
                Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE,"****************************",NEWLINE);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");
        Global.format(3, "LS=133 PS=60");

        getReports().setDisplayColumns(0, "***** INTERFACE FILE IS EMPTY *****");
    }
}
