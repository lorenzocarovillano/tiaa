/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:10:10 PM
**        * FROM NATURAL PROGRAM : Appb990p
************************************************************
**        * FILE NAME            : Appb990p.java
**        * CLASS NAME           : Appb990p
**        * INSTANCE NAME        : Appb990p
************************************************************
* DATE         USER ID        CHANGE DESCRIPTION         TAG           *
* 06/16/2017 - BARUA        - PIN EXPANSION CHANGES.  (CHG425939) PINE.*
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb990p extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField first_Time_Sw;

    private DbsGroup stable_Return_Rec;
    private DbsField stable_Return_Rec_Sv_Plan_Num;
    private DbsField stable_Return_Rec_Sv_Sub_Plan;
    private DbsField stable_Return_Rec_Sv_Fund_Fnd_Ind;
    private DbsField stable_Return_Rec_Sv_Active_Dt;
    private DbsField stable_Return_Rec_Sv_Inactive_Dt;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Dob;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Bill_Code;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Curr;
    private DbsField annty_Actvty_Prap_View_Ap_T_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_C_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership;
    private DbsField annty_Actvty_Prap_View_Ap_Sex;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Zip;
    private DbsField annty_Actvty_Prap_View_Ap_Name_Addr_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Ent_Sys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Released;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Discr;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_App_Recvd;
    private DbsField annty_Actvty_Prap_View_Ap_App_Source;
    private DbsField annty_Actvty_Prap_View_Ap_Region_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;

    private DbsGroup annty_Actvty_Prap_View_Ap_Address_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Line;
    private DbsField annty_Actvty_Prap_View_Ap_City;
    private DbsField annty_Actvty_Prap_View_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Annuity_Start_Date;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Primary_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Contingent_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Estate;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Trust;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Category;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Instructions;
    private DbsField annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_St_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Doi;

    private DbsGroup annty_Actvty_Prap_View_Ap_Mit_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Unit;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Wpid;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Rollover_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Ppg;
    private DbsField annty_Actvty_Prap_View_Ap_Print_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Addr_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Txt;
    private DbsField annty_Actvty_Prap_View_Ap_Zip_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup annty_Actvty_Prap_View__R_Field_1;
    private DbsField annty_Actvty_Prap_View_Bank_Home_Ac;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A3;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A4;
    private DbsField annty_Actvty_Prap_View_Bank_Filler_4;
    private DbsField annty_Actvty_Prap_View_Oia_Indicator;
    private DbsField annty_Actvty_Prap_View_Erisa_Ind;
    private DbsField annty_Actvty_Prap_View_Cai_Ind;
    private DbsField annty_Actvty_Prap_View_Cip_Ind;
    private DbsField annty_Actvty_Prap_View_Same_Addr;
    private DbsField annty_Actvty_Prap_View_Omni_Issue_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Usage_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_1;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Inst_Link_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Applcnt_Req_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent;
    private DbsField annty_Actvty_Prap_View_Ap_Ppg_Team_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Model_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Divorce_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Phone_No;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Fmt;
    private DbsField annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Eft_Requested_Ind;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Divsub;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_1;

    private DbsGroup annty_Actvty_Prap_View__R_Field_2;
    private DbsField annty_Actvty_Prap_View_Ap_Plan_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Tsv_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Index_Rate_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tsr_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_2;

    private DbsGroup annty_Actvty_Prap_View__R_Field_3;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Client_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Portfolio_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Replacement_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Register_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Exempt_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Autosave_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Max_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days;
    private DbsField annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_Ann_Option;
    private DbsField annty_Actvty_Prap_View_Ap_Orchestration_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Startdate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Enddate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Percentage;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postdays;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Limit;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postfreq;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Pl_Level;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Windowdays;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Recap_Prov;
    private DbsField annty_Actvty_Prap_View_Ap_Ann_Funding_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Conv_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Deceased_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind;
    private DbsField pnd_Cnt;
    private DbsField pnd_Cnt2;
    private DbsField pnd_Cnt3;
    private DbsField pnd_Cnt4;
    private DbsField pnd_Cnt5;
    private DbsField pnd_Cnt6;
    private DbsField pnd_Cnt7;
    private DbsField pnd_Cnt8;
    private DbsField pnd_Cnt9;
    private DbsField pnd_Cnt10;
    private DbsField pnd_Cnt11;
    private DbsField pnd_Set_Sv;
    private DbsField pnd_I;
    private int psg0300ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        first_Time_Sw = localVariables.newFieldInRecord("first_Time_Sw", "FIRST-TIME-SW", FieldType.STRING, 1);

        stable_Return_Rec = localVariables.newGroupInRecord("stable_Return_Rec", "STABLE-RETURN-REC");
        stable_Return_Rec_Sv_Plan_Num = stable_Return_Rec.newFieldInGroup("stable_Return_Rec_Sv_Plan_Num", "SV-PLAN-NUM", FieldType.STRING, 6);
        stable_Return_Rec_Sv_Sub_Plan = stable_Return_Rec.newFieldInGroup("stable_Return_Rec_Sv_Sub_Plan", "SV-SUB-PLAN", FieldType.STRING, 6);
        stable_Return_Rec_Sv_Fund_Fnd_Ind = stable_Return_Rec.newFieldInGroup("stable_Return_Rec_Sv_Fund_Fnd_Ind", "SV-FUND-FND-IND", FieldType.STRING, 
            1);
        stable_Return_Rec_Sv_Active_Dt = stable_Return_Rec.newFieldInGroup("stable_Return_Rec_Sv_Active_Dt", "SV-ACTIVE-DT", FieldType.NUMERIC, 8);
        stable_Return_Rec_Sv_Inactive_Dt = stable_Return_Rec.newFieldInGroup("stable_Return_Rec_Sv_Inactive_Dt", "SV-INACTIVE-DT", FieldType.NUMERIC, 
            8);

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Dob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_Bill_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bill_Code", "AP-BILL-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BILL_CODE");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Curr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_CURR");
        annty_Actvty_Prap_View_Ap_T_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Age_1st", "AP-T-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_T_AGE_1ST");
        annty_Actvty_Prap_View_Ap_C_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Age_1st", "AP-C-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_C_AGE_1ST");
        annty_Actvty_Prap_View_Ap_Ownership = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership", "AP-OWNERSHIP", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Sex = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sex", "AP-SEX", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SEX");
        annty_Actvty_Prap_View_Ap_Sex.setDdmHeader("SEX CDE");
        annty_Actvty_Prap_View_Ap_Mail_Zip = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Zip", "AP-MAIL-ZIP", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        annty_Actvty_Prap_View_Ap_Name_Addr_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NAME_ADDR_CD");
        annty_Actvty_Prap_View_Ap_Dt_Ent_Sys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        annty_Actvty_Prap_View_Ap_Dt_Released = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Released", "AP-DT-RELEASED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_RELEASED");
        annty_Actvty_Prap_View_Ap_Alloc_Discr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Discr", "AP-ALLOC-DISCR", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_ALLOC_DISCR");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Type", "AP-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_TYPE");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Dt_App_Recvd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");
        annty_Actvty_Prap_View_Ap_App_Source = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_App_Source", "AP-APP-SOURCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APP_SOURCE");
        annty_Actvty_Prap_View_Ap_App_Source.setDdmHeader("SOURCE");
        annty_Actvty_Prap_View_Ap_Region_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Region_Code", "AP-REGION-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_REGION_CODE");
        annty_Actvty_Prap_View_Ap_Orig_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orig_Issue_State", 
            "AP-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Ownership_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP_TYPE");
        annty_Actvty_Prap_View_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");

        annty_Actvty_Prap_View_Ap_Address_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Address_Info", 
            "AP-ADDRESS-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_Address_Line = annty_Actvty_Prap_View_Ap_Address_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Line", "AP-ADDRESS-LINE", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_City = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_City", "AP-CITY", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "AP_CITY");
        annty_Actvty_Prap_View_Ap_Current_State_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Current_State_Code", 
            "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_View_Ap_Annuity_Start_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Annuity_Start_Date", 
            "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");
        annty_Actvty_Prap_View_Ap_Primary_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Std_Ent", 
            "AP-PRIMARY-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Primary_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Primary_Info", 
            "AP-BENE-PRIMARY-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Primary_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Primary_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Bene_Info", 
            "AP-PRIMARY-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Std_Ent", 
            "AP-CONTINGENT-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Contingent_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Contingent_Info", 
            "AP-BENE-CONTINGENT-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Contingent_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Bene_Info", 
            "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Bene_Estate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Estate", "AP-BENE-ESTATE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_ESTATE");
        annty_Actvty_Prap_View_Ap_Bene_Trust = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Trust", "AP-BENE-TRUST", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_TRUST");
        annty_Actvty_Prap_View_Ap_Bene_Category = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Category", "AP-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_CATEGORY");
        annty_Actvty_Prap_View_Ap_Mail_Instructions = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Instructions", 
            "AP-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request", 
            "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        annty_Actvty_Prap_View_Ap_Coll_St_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_St_Cd", "AP-COLL-ST-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        annty_Actvty_Prap_View_Ap_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Tiaa_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_View_Ap_Cref_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Doi", "AP-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");

        annty_Actvty_Prap_View_Ap_Mit_Request = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Mit_Request", "AP-MIT-REQUEST", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Unit = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Unit", "AP-MIT-UNIT", 
            FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Wpid = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Wpid", "AP-MIT-WPID", 
            FieldType.STRING, 6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Ira_Rollover_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Rollover_Type", 
            "AP-IRA-ROLLOVER-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_ROLLOVER_TYPE");
        annty_Actvty_Prap_View_Ap_Ira_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Record_Type", 
            "AP-IRA-RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Status", 
            "AP-MULT-APP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_STATUS");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type", 
            "AP-MULT-APP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Ppg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_MULT_APP_PPG");
        annty_Actvty_Prap_View_Ap_Print_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Print_Date", "AP-PRINT-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_PRINT_DATE");

        annty_Actvty_Prap_View_Ap_Addr_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Addr_Info", "AP-ADDR-INFO", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Txt = annty_Actvty_Prap_View_Ap_Addr_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Txt", "AP-ADDRESS-TXT", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Zip_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Zip_Code", "AP-ZIP-CODE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        annty_Actvty_Prap_View__R_Field_1 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_1", "REDEFINE", annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        annty_Actvty_Prap_View_Bank_Home_Ac = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_Ac", "BANK-HOME-AC", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A3 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A3", "BANK-HOME-A3", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A4 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A4", "BANK-HOME-A4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Bank_Filler_4 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Filler_4", "BANK-FILLER-4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Oia_Indicator = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Oia_Indicator", "OIA-INDICATOR", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Erisa_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Erisa_Ind", "ERISA-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cai_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Cai_Ind", "CAI-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cip_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Cip_Ind", "CIP-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Same_Addr = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Same_Addr", "SAME-ADDR", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Omni_Issue_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Omni_Issue_Ind", "OMNI-ISSUE-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Addr_Usage_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Usage_Code", 
            "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_USAGE_CODE");
        annty_Actvty_Prap_View_Ap_Financial_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_1", "AP-FINANCIAL-1", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_1");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde", 
            "AP-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_GRP_CDE");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_CDE");
        annty_Actvty_Prap_View_Ap_Inst_Link_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "AP_INST_LINK_CDE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Applcnt_Req_Type", 
            "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent", 
            "AP-TIAA-SERVICE-AGENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIAA_SERVICE_AGENT");
        annty_Actvty_Prap_View_Ap_Ppg_Team_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_PPG_TEAM_CDE");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_RLC_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Allocation_Model_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Model_Type", 
            "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        annty_Actvty_Prap_View_Ap_Divorce_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Divorce_Ind", "AP-DIVORCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DIVORCE_IND");
        annty_Actvty_Prap_View_Ap_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Phone_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Phone_No", "AP-PHONE-NO", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "AP_PHONE_NO");
        annty_Actvty_Prap_View_Ap_Allocation_Fmt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOCATION_FMT");
        annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind", 
            "AP-E-SIGNED-APPL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        annty_Actvty_Prap_View_Ap_Eft_Requested_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eft_Requested_Ind", 
            "AP-EFT-REQUESTED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");

        annty_Actvty_Prap_View_Ap_Fund_Identifier = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier", 
            "AP-FUND-IDENTIFIER", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Fund_Cde = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde", "AP-FUND-CDE", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Allocation_Pct = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct", 
            "AP-ALLOCATION-PCT", FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        annty_Actvty_Prap_View_Ap_Sgrd_Divsub = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_SGRD_DIVSUB");
        annty_Actvty_Prap_View_Ap_Text_Udf_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_1", "AP-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_1");

        annty_Actvty_Prap_View__R_Field_2 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_2", "REDEFINE", annty_Actvty_Prap_View_Ap_Text_Udf_1);
        annty_Actvty_Prap_View_Ap_Plan_Issue_State = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Plan_Issue_State", "AP-PLAN-ISSUE-STATE", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Ap_Tsv_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Tsv_Ind", "AP-TSV-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Ap_Index_Rate_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Index_Rate_Ind", "AP-INDEX-RATE-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Status = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Status", "AP-SGRD-PART-STATUS", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind", "AP-SPCL-LGL-PKG-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Tsr_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Tsr_Ind", "AP-TSR-IND", FieldType.STRING, 
            3);
        annty_Actvty_Prap_View_Ap_Text_Udf_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_2", "AP-TEXT-UDF-2", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_2");

        annty_Actvty_Prap_View__R_Field_3 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_3", "REDEFINE", annty_Actvty_Prap_View_Ap_Text_Udf_2);
        annty_Actvty_Prap_View_Ap_Sgrd_Client_Id = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Client_Id", "AP-SGRD-CLIENT-ID", 
            FieldType.STRING, 6);
        annty_Actvty_Prap_View_Ap_Portfolio_Type = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Ap_Portfolio_Type", "AP-PORTFOLIO-TYPE", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Ap_Replacement_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Replacement_Ind", 
            "AP-REPLACEMENT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_REPLACEMENT_IND");
        annty_Actvty_Prap_View_Ap_Register_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Register_Id", "AP-REGISTER-ID", 
            FieldType.STRING, 11, RepeatingFieldStrategy.None, "AP_REGISTER_ID");
        annty_Actvty_Prap_View_Ap_Exempt_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Exempt_Ind", "AP-EXEMPT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EXEMPT_IND");
        annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind", 
            "AP-INCMPL-ACCT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_INCMPL_ACCT_IND");
        annty_Actvty_Prap_View_Ap_Autosave_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Autosave_Ind", "AP-AUTOSAVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AUTOSAVE_IND");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt", 
            "AP-AS-CUR-DFLT-OPT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_OPT");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt", 
            "AP-AS-CUR-DFLT-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_AMT");
        annty_Actvty_Prap_View_Ap_As_Incr_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Opt", "AP-AS-INCR-OPT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_INCR_OPT");
        annty_Actvty_Prap_View_Ap_As_Incr_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Amt", "AP-AS-INCR-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_INCR_AMT");
        annty_Actvty_Prap_View_Ap_As_Max_Pct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Max_Pct", "AP-AS-MAX-PCT", 
            FieldType.NUMERIC, 7, 2, RepeatingFieldStrategy.None, "AP_AS_MAX_PCT");
        annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days", 
            "AP-AE-OPT-OUT-DAYS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_AE_OPT_OUT_DAYS");
        annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id", 
            "AP-AGENT-OR-RACF-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_AGENT_OR_RACF_ID");
        annty_Actvty_Prap_View_Ap_Legal_Ann_Option = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_Ann_Option", 
            "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        annty_Actvty_Prap_View_Ap_Orchestration_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orchestration_Id", 
            "AP-ORCHESTRATION-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_ORCHESTRATION_ID");
        annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd", 
            "AP-MAIL-ADDR-COUNTRY-CD", FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_MAIL_ADDR_COUNTRY_CD");
        annty_Actvty_Prap_View_Ap_Tic_Startdate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Startdate", "AP-TIC-STARTDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_STARTDATE");
        annty_Actvty_Prap_View_Ap_Tic_Enddate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Enddate", "AP-TIC-ENDDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_ENDDATE");
        annty_Actvty_Prap_View_Ap_Tic_Percentage = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Percentage", "AP-TIC-PERCENTAGE", 
            FieldType.NUMERIC, 15, 6, RepeatingFieldStrategy.None, "AP_TIC_PERCENTAGE");
        annty_Actvty_Prap_View_Ap_Tic_Postdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postdays", "AP-TIC-POSTDAYS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_TIC_POSTDAYS");
        annty_Actvty_Prap_View_Ap_Tic_Limit = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Limit", "AP-TIC-LIMIT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "AP_TIC_LIMIT");
        annty_Actvty_Prap_View_Ap_Tic_Postfreq = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postfreq", "AP-TIC-POSTFREQ", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_POSTFREQ");
        annty_Actvty_Prap_View_Ap_Tic_Pl_Level = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Pl_Level", "AP-TIC-PL-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_PL_LEVEL");
        annty_Actvty_Prap_View_Ap_Tic_Windowdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Windowdays", "AP-TIC-WINDOWDAYS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_WINDOWDAYS");
        annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow", 
            "AP-TIC-REQDLYWINDOW", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_REQDLYWINDOW");
        annty_Actvty_Prap_View_Ap_Tic_Recap_Prov = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Recap_Prov", "AP-TIC-RECAP-PROV", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_RECAP_PROV");
        annty_Actvty_Prap_View_Ap_Ann_Funding_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ann_Funding_Dt", "AP-ANN-FUNDING-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_ANN_FUNDING_DT");
        annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt", 
            "AP-TIAA-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIAA_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt", 
            "AP-CREF-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_CREF_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind", 
            "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        annty_Actvty_Prap_View_Ap_Conv_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Conv_Issue_State", 
            "AP-CONV-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CONV_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Deceased_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Deceased_Ind", "AP-DECEASED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DECEASED_IND");
        annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind", 
            "AP-NON-PROPRIETARY-PKG-IND", FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NON_PROPRIETARY_PKG_IND");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 10);
        pnd_Cnt2 = localVariables.newFieldInRecord("pnd_Cnt2", "#CNT2", FieldType.NUMERIC, 10);
        pnd_Cnt3 = localVariables.newFieldInRecord("pnd_Cnt3", "#CNT3", FieldType.NUMERIC, 10);
        pnd_Cnt4 = localVariables.newFieldInRecord("pnd_Cnt4", "#CNT4", FieldType.NUMERIC, 10);
        pnd_Cnt5 = localVariables.newFieldInRecord("pnd_Cnt5", "#CNT5", FieldType.NUMERIC, 10);
        pnd_Cnt6 = localVariables.newFieldInRecord("pnd_Cnt6", "#CNT6", FieldType.NUMERIC, 10);
        pnd_Cnt7 = localVariables.newFieldInRecord("pnd_Cnt7", "#CNT7", FieldType.NUMERIC, 10);
        pnd_Cnt8 = localVariables.newFieldInRecord("pnd_Cnt8", "#CNT8", FieldType.NUMERIC, 10);
        pnd_Cnt9 = localVariables.newFieldInRecord("pnd_Cnt9", "#CNT9", FieldType.NUMERIC, 10);
        pnd_Cnt10 = localVariables.newFieldInRecord("pnd_Cnt10", "#CNT10", FieldType.NUMERIC, 10);
        pnd_Cnt11 = localVariables.newFieldInRecord("pnd_Cnt11", "#CNT11", FieldType.NUMERIC, 10);
        pnd_Set_Sv = localVariables.newFieldInRecord("pnd_Set_Sv", "#SET-SV", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb990p() throws Exception
    {
        super("Appb990p");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        first_Time_Sw.setValue("Y");                                                                                                                                      //Natural: ASSIGN FIRST-TIME-SW := 'Y'
        vw_annty_Actvty_Prap_View.startDatabaseRead                                                                                                                       //Natural: READ ANNTY-ACTVTY-PRAP-VIEW
        (
        "PND_PND_L1830",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        PND_PND_L1830:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("PND_PND_L1830")))
        {
            pnd_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CNT
            if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No.equals("      ")))                                                                                       //Natural: IF AP-SGRD-PLAN-NO = '      '
            {
                pnd_Cnt6.nadd(1);                                                                                                                                         //Natural: ASSIGN #CNT6 := #CNT6 + 1
                getReports().write(6, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,             //Natural: WRITE ( 6 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                    annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("RA6") || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("GR1")  //Natural: IF SUBSTR ( AP-SGRD-SUBPLAN-NO,1,3 ) = 'RA6' OR SUBSTR ( AP-SGRD-SUBPLAN-NO,1,3 ) = 'GR1' OR SUBSTR ( AP-SGRD-SUBPLAN-NO,1,2 ) = 'RS'
                || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,2).equals("RS")))
            {
                stable_Return_Rec.reset();                                                                                                                                //Natural: RESET STABLE-RETURN-REC
                stable_Return_Rec_Sv_Plan_Num.setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                           //Natural: ASSIGN SV-PLAN-NUM := AP-SGRD-PLAN-NO
                stable_Return_Rec_Sv_Sub_Plan.setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                                        //Natural: ASSIGN SV-SUB-PLAN := AP-SGRD-SUBPLAN-NO
                psg0300ReturnCode = DbsUtil.callExternalProgram("PSG0300",first_Time_Sw,stable_Return_Rec_Sv_Plan_Num,stable_Return_Rec_Sv_Sub_Plan,stable_Return_Rec_Sv_Fund_Fnd_Ind, //Natural: CALL 'PSG0300' USING FIRST-TIME-SW STABLE-RETURN-REC
                    stable_Return_Rec_Sv_Active_Dt,stable_Return_Rec_Sv_Inactive_Dt);
                if (condition(stable_Return_Rec_Sv_Fund_Fnd_Ind.equals("Y")))                                                                                             //Natural: IF SV-FUND-FND-IND = 'Y'
                {
                    pnd_Set_Sv.setValue("N");                                                                                                                             //Natural: ASSIGN #SET-SV := 'N'
                    if (condition(stable_Return_Rec_Sv_Active_Dt.equals(getZero()) && stable_Return_Rec_Sv_Inactive_Dt.equals(getZero())))                                //Natural: IF SV-ACTIVE-DT = 0 AND SV-INACTIVE-DT = 0
                    {
                        pnd_Cnt2.nadd(1);                                                                                                                                 //Natural: ASSIGN #CNT2 := #CNT2 + 1
                        pnd_Set_Sv.setValue("Y");                                                                                                                         //Natural: ASSIGN #SET-SV := 'Y'
                        getReports().write(2, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,     //Natural: WRITE ( 2 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                            annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(stable_Return_Rec_Sv_Active_Dt.greater(getZero()) && stable_Return_Rec_Sv_Inactive_Dt.greater(getZero())))                          //Natural: IF SV-ACTIVE-DT > 0 AND SV-INACTIVE-DT > 0
                        {
                            if (condition(annty_Actvty_Prap_View_Ap_T_Doi.greaterOrEqual(stable_Return_Rec_Sv_Active_Dt) && annty_Actvty_Prap_View_Ap_T_Doi.lessOrEqual(stable_Return_Rec_Sv_Inactive_Dt))) //Natural: IF AP-T-DOI >= SV-ACTIVE-DT AND AP-T-DOI <= SV-INACTIVE-DT
                            {
                                pnd_Cnt3.nadd(1);                                                                                                                         //Natural: ASSIGN #CNT3 := #CNT3 + 1
                                pnd_Set_Sv.setValue("Y");                                                                                                                 //Natural: ASSIGN #SET-SV := 'Y'
                                getReports().write(3, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No, //Natural: WRITE ( 3 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                                    annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Cnt9.nadd(1);                                                                                                                         //Natural: ASSIGN #CNT9 := #CNT9 + 1
                                getReports().write(9, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No, //Natural: WRITE ( 9 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                                    annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(stable_Return_Rec_Sv_Active_Dt.greater(getZero()) && stable_Return_Rec_Sv_Inactive_Dt.equals(getZero())))                       //Natural: IF SV-ACTIVE-DT > 0 AND SV-INACTIVE-DT = 0
                            {
                                if (condition(annty_Actvty_Prap_View_Ap_T_Doi.greaterOrEqual(stable_Return_Rec_Sv_Active_Dt)))                                            //Natural: IF AP-T-DOI >= SV-ACTIVE-DT
                                {
                                    pnd_Cnt4.nadd(1);                                                                                                                     //Natural: ASSIGN #CNT4 := #CNT4 + 1
                                    pnd_Set_Sv.setValue("Y");                                                                                                             //Natural: ASSIGN #SET-SV := 'Y'
                                    getReports().write(4, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No, //Natural: WRITE ( 4 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                                        annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Cnt10.nadd(1);                                                                                                                    //Natural: ASSIGN #CNT10 := #CNT10 + 1
                                    getReports().write(10, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No, //Natural: WRITE ( 10 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                                        annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(stable_Return_Rec_Sv_Active_Dt.equals(getZero()) && stable_Return_Rec_Sv_Inactive_Dt.greater(getZero())))                   //Natural: IF SV-ACTIVE-DT = 0 AND SV-INACTIVE-DT > 0
                                {
                                    if (condition(annty_Actvty_Prap_View_Ap_T_Doi.lessOrEqual(stable_Return_Rec_Sv_Inactive_Dt)))                                         //Natural: IF AP-T-DOI <= SV-INACTIVE-DT
                                    {
                                        pnd_Cnt5.nadd(1);                                                                                                                 //Natural: ASSIGN #CNT5 := #CNT5 + 1
                                        pnd_Set_Sv.setValue("Y");                                                                                                         //Natural: ASSIGN #SET-SV := 'Y'
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        pnd_Cnt11.nadd(1);                                                                                                                //Natural: ASSIGN #CNT11 := #CNT11 + 1
                                        getReports().write(11, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No, //Natural: WRITE ( 11 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                                            annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Set_Sv.equals("Y")))                                                                                                                //Natural: IF #SET-SV = 'Y'
                    {
                        PND_PND_L2890:                                                                                                                                    //Natural: GET ANNTY-ACTVTY-PRAP-VIEW *ISN ( ##L1830. )
                        vw_annty_Actvty_Prap_View.readByID(vw_annty_Actvty_Prap_View.getAstISN("PND_PND_L1830"), "PND_PND_L2890");
                        annty_Actvty_Prap_View_Ap_Tsr_Ind.setValue("SRF");                                                                                                //Natural: ASSIGN AP-TSR-IND := 'SRF'
                        getReports().write(1, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,     //Natural: WRITE ( 1 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                            annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        vw_annty_Actvty_Prap_View.updateDBRow("PND_PND_L2890");                                                                                           //Natural: UPDATE ( ##L2890. )
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cnt8.nadd(1);                                                                                                                                     //Natural: ASSIGN #CNT8 := #CNT8 + 1
                    getReports().write(8, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,         //Natural: WRITE ( 8 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                        annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cnt7.nadd(1);                                                                                                                                         //Natural: ASSIGN #CNT7 := #CNT7 + 1
                getReports().write(7, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,             //Natural: WRITE ( 7 ) AP-TIAA-CNTRCT AP-SGRD-PLAN-NO AP-SGRD-SUBPLAN-NO AP-TSV-IND AP-TSR-IND
                    annty_Actvty_Prap_View_Ap_Tsv_Ind,annty_Actvty_Prap_View_Ap_Tsr_Ind);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1830"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1830"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "RECS READ:                                            ",pnd_Cnt);                                                                          //Natural: WRITE 'RECS READ:                                            ' #CNT
        if (Global.isEscape()) return;
        getReports().write(0, "RECS BYPASSED - BLANK PLAN (6):                       ",pnd_Cnt6);                                                                         //Natural: WRITE 'RECS BYPASSED - BLANK PLAN (6):                       ' #CNT6
        if (Global.isEscape()) return;
        getReports().write(0, "RECS BYPASSED - SUBPLAN NOT RA6,GR1 OR RS(7):         ",pnd_Cnt7);                                                                         //Natural: WRITE 'RECS BYPASSED - SUBPLAN NOT RA6,GR1 OR RS(7):         ' #CNT7
        if (Global.isEscape()) return;
        getReports().write(0, "RECS BYPASSED - NOT A STABLE RETURN(8):               ",pnd_Cnt8);                                                                         //Natural: WRITE 'RECS BYPASSED - NOT A STABLE RETURN(8):               ' #CNT8
        if (Global.isEscape()) return;
        getReports().write(0, "RECS BYPASSED - WITH BOTH DATES---OUTSIDE RANGE(9):   ",pnd_Cnt9);                                                                         //Natural: WRITE 'RECS BYPASSED - WITH BOTH DATES---OUTSIDE RANGE(9):   ' #CNT9
        if (Global.isEscape()) return;
        getReports().write(0, "RECS BYPASSED - ACTIVE DATE ONLY--OUTSIDE RANGE(10):  ",pnd_Cnt10);                                                                        //Natural: WRITE 'RECS BYPASSED - ACTIVE DATE ONLY--OUTSIDE RANGE(10):  ' #CNT10
        if (Global.isEscape()) return;
        getReports().write(0, "RECS BYPASSED - INACTIVE DATE ONLY-OUTSIDE RANGE(11): ",pnd_Cnt11);                                                                        //Natural: WRITE 'RECS BYPASSED - INACTIVE DATE ONLY-OUTSIDE RANGE(11): ' #CNT11
        if (Global.isEscape()) return;
        getReports().write(0, "RECS UPDATED WITH NO DATES(2):                        ",pnd_Cnt2);                                                                         //Natural: WRITE 'RECS UPDATED WITH NO DATES(2):                        ' #CNT2
        if (Global.isEscape()) return;
        getReports().write(0, "RECS UPDATED BOTH DATES(3):                           ",pnd_Cnt3);                                                                         //Natural: WRITE 'RECS UPDATED BOTH DATES(3):                           ' #CNT3
        if (Global.isEscape()) return;
        getReports().write(0, "RECS UPDATED WITH ACTIVE DATE ONLY(4):                ",pnd_Cnt4);                                                                         //Natural: WRITE 'RECS UPDATED WITH ACTIVE DATE ONLY(4):                ' #CNT4
        if (Global.isEscape()) return;
        getReports().write(0, "RECS UPDATED WITH INACTIVE DATE ONLY(5):              ",pnd_Cnt5);                                                                         //Natural: WRITE 'RECS UPDATED WITH INACTIVE DATE ONLY(5):              ' #CNT5
        if (Global.isEscape()) return;
    }

    //
}
