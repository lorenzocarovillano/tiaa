/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:15 PM
**        * FROM NATURAL PROGRAM : Appb302
************************************************************
**        * FILE NAME            : Appb302.java
**        * CLASS NAME           : Appb302
**        * INSTANCE NAME        : Appb302
************************************************************
************************************************************************
* PROGRAM  : APPB302 - PRAP MONTHLY OVERDUE LETTER FACILITY            *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : JUNE 24, 1999                                             *
* FUNCTION : READS WORK FILE CREATED BY APPB301, SORTS FILE AND FEEDS  *
*            INTO THE POST SYSTEM TO CREATE THE OVERDUE LETTERS FOR    *
*            MONTHLY RUN, ONLY PREMIUM RECORDS WILL BE ACCEPTED        *
* UPDATED  : 08/30/01 K.GATES   - CV-STATUS WAS REPLACED IN SORT       *
*                                 STATEMENT BY CV-DAY-DIFF.            *
*          : 04/25/04 K.GATES     SUNGARD RELEASE 2  SEE SGRD KG       *
*                               - EXEMPTED SUNGARD CONTRACTS FROM      *
*                                 PROCESSING.                          *
*                               - SUNGARD FIELDS ADDED TO APPL302      *
*          : 8/21/04 WILLISR    - SUNGARD RELEASE 3                    *
*          : 9/26/06 K.GATES    - ADDED DIV/SUB FOR DCA  SEE DCA KG    *
*          : 7/11/17 GHOSBAE    - PIN EXPANSION CHANGES.CHG425939      *
*                                 STOW ONLY.                           *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb302 extends BLNatBase
{
    // Data Areas
    private LdaAppl302 ldaAppl302;
    private PdaCdaobj pdaCdaobj;
    private PdaIcwpda_D pdaIcwpda_D;
    private PdaIcwpda_M pdaIcwpda_M;
    private PdaIcwpda_P pdaIcwpda_P;
    private PdaIefa9000 pdaIefa9000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rec_In;
    private DbsField pnd_Rec_Out;
    private DbsField pnd_Rec_Post;
    private DbsField pnd_Status_P;
    private DbsField pnd_Stat_Desc;
    private DbsField pnd_Sub_Total;
    private DbsField pnd_I;
    private DbsField pnd_Write;

    private DbsRecord internalLoopRecord;
    private DbsField readWork02Cv_KeyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl302 = new LdaAppl302();
        registerRecord(ldaAppl302);
        localVariables = new DbsRecord();
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaIcwpda_D = new PdaIcwpda_D(localVariables);
        pdaIcwpda_M = new PdaIcwpda_M(localVariables);
        pdaIcwpda_P = new PdaIcwpda_P(localVariables);
        pdaIefa9000 = new PdaIefa9000(localVariables);

        // Local Variables
        pnd_Rec_In = localVariables.newFieldInRecord("pnd_Rec_In", "#REC-IN", FieldType.NUMERIC, 7);
        pnd_Rec_Out = localVariables.newFieldInRecord("pnd_Rec_Out", "#REC-OUT", FieldType.NUMERIC, 7);
        pnd_Rec_Post = localVariables.newFieldInRecord("pnd_Rec_Post", "#REC-POST", FieldType.NUMERIC, 7);
        pnd_Status_P = localVariables.newFieldArrayInRecord("pnd_Status_P", "#STATUS-P", FieldType.NUMERIC, 7, new DbsArrayController(1, 10));
        pnd_Stat_Desc = localVariables.newFieldArrayInRecord("pnd_Stat_Desc", "#STAT-DESC", FieldType.STRING, 16, new DbsArrayController(1, 10));
        pnd_Sub_Total = localVariables.newFieldInRecord("pnd_Sub_Total", "#SUB-TOTAL", FieldType.NUMERIC, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 5);
        pnd_Write = localVariables.newFieldInRecord("pnd_Write", "#WRITE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork02Cv_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork02_Cv_Key_OLD", "Cv_Key_OLD", FieldType.STRING, 112);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaAppl302.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Appb302() throws Exception
    {
        super("Appb302");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB302", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 HW = OFF PS = 52;//Natural: FORMAT ( 1 ) LS = 133 HW = OFF PS = 52
        pnd_Stat_Desc.getValue(1).setValue("Deleted");                                                                                                                    //Natural: MOVE 'Deleted' TO #STAT-DESC ( 1 )
        pnd_Stat_Desc.getValue(2).setValue("Assigned");                                                                                                                   //Natural: MOVE 'Assigned' TO #STAT-DESC ( 2 )
        pnd_Stat_Desc.getValue(3).setValue("Match & Release");                                                                                                            //Natural: MOVE 'Match & Release' TO #STAT-DESC ( 3 )
        pnd_Stat_Desc.getValue(4).setValue("Released");                                                                                                                   //Natural: MOVE 'Released' TO #STAT-DESC ( 4 )
        pnd_Stat_Desc.getValue(5).setValue("Unassigned");                                                                                                                 //Natural: MOVE 'Unassigned' TO #STAT-DESC ( 5 )
        pnd_Stat_Desc.getValue(6).setValue("Suspense");                                                                                                                   //Natural: MOVE 'Suspense' TO #STAT-DESC ( 6 )
        pnd_Stat_Desc.getValue(7).setValue("Overdue 30 Days");                                                                                                            //Natural: MOVE 'Overdue 30 Days' TO #STAT-DESC ( 7 )
        pnd_Stat_Desc.getValue(8).setValue("Overdue 60 Days");                                                                                                            //Natural: MOVE 'Overdue 60 Days' TO #STAT-DESC ( 8 )
        pnd_Stat_Desc.getValue(9).setValue("Overdue 90 Days");                                                                                                            //Natural: MOVE 'Overdue 90 Days' TO #STAT-DESC ( 9 )
        pnd_Stat_Desc.getValue(10).setValue("Unknown");                                                                                                                   //Natural: MOVE 'Unknown' TO #STAT-DESC ( 10 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 CV-RULE-DATA-L CV-RULE-DATA-M
        while (condition(getWorkFiles().read(1, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M())))
        {
            pnd_Rec_In.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #REC-IN
            //*  PREMIUM RECORDS ONLY
            if (condition(!(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(2))))                                                                                     //Natural: ACCEPT IF CV-RECORD-TYPE EQ 2
            {
                continue;
            }
            //*  IF CV-COLL-CODE EQ 'SGRD'            /* SGRD KG     8/21/04 RW
            //*   ESCAPE TOP                          /* SGRD KG     8/21/04 RW
            //*  END-IF                               /* SGRD KG     8/21/04 RW
            short decideConditionsMet373 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE CV-STATUS;//Natural: VALUE 'A'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("A"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(1);                                                                                                                                        //Natural: MOVE 1 TO #I
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("B"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(2);                                                                                                                                        //Natural: MOVE 2 TO #I
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("C"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(3);                                                                                                                                        //Natural: MOVE 3 TO #I
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("D"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(4);                                                                                                                                        //Natural: MOVE 4 TO #I
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("E"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(5);                                                                                                                                        //Natural: MOVE 5 TO #I
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("F"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(6);                                                                                                                                        //Natural: MOVE 6 TO #I
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("G"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(7);                                                                                                                                        //Natural: MOVE 7 TO #I
            }                                                                                                                                                             //Natural: VALUE 'H'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("H"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(8);                                                                                                                                        //Natural: MOVE 8 TO #I
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("I"))))
            {
                decideConditionsMet373++;
                pnd_I.setValue(9);                                                                                                                                        //Natural: MOVE 9 TO #I
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_I.setValue(10);                                                                                                                                       //Natural: MOVE 10 TO #I
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Status_P.getValue(pnd_I).nadd(1);                                                                                                                         //Natural: ADD 1 TO #STATUS-P ( #I )
            getSort().writeSortInData(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type(), ldaAppl302.getCv_Prap_Rule1_Cv_Key(), ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(),     //Natural: END-ALL
                ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Subplan_No(), 
                ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(), ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), 
                ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  8/21/04 RW
        //*  DCA KG
        //*  8/21/04 RW
        getSort().sortData(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type(), ldaAppl302.getCv_Prap_Rule1_Cv_Key(), ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(),                //Natural: SORT CV-RECORD-TYPE CV-KEY CV-COLL-CODE CV-SGRD-PLAN-NO CV-SGRD-DIVSUB CV-SGRD-SUBPLAN-NO CV-DAY-DIFF DESC CV-COR-LAST-NME CV-SOC-SEC USING CV-RULE-DATA-L CV-RULE-DATA-M
            ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Subplan_No(), 
            ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(), "DESCENDING", ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec());
        SORT01:
        while (condition(getSort().readSortOutData(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type(), ldaAppl302.getCv_Prap_Rule1_Cv_Key(), ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(), 
            ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Subplan_No(), 
            ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(), ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), 
            ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M())))
        {
            pnd_Rec_Out.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-OUT
            getWorkFiles().write(2, false, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M());                                   //Natural: WRITE WORK FILE 2 CV-RULE-DATA-L CV-RULE-DATA-M
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        boolean endOfDataReadwork02 = true;                                                                                                                               //Natural: READ WORK FILE 2 CV-RULE-DATA-L CV-RULE-DATA-M
        boolean firstReadwork02 = true;
        READWORK02:
        while (condition(getWorkFiles().read(2, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork02();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork02 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Rec_Post.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-POST
            if (condition(pnd_Rec_Post.equals(1)))                                                                                                                        //Natural: IF #REC-POST EQ 1
            {
                pdaIefa9000.getIefa9000().reset();                                                                                                                        //Natural: RESET IEFA9000
                pdaIefa9000.getIefa9000_Iis_Hrchy_Key_Cde().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Inst_Link_Cde());                                         //Natural: MOVE CV-INST-LINK-CDE TO IEFA9000.IIS-HRCHY-KEY-CDE ( 1 )
                pdaIefa9000.getIefa9000_Oprtr_Unit_Cde().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde());                                                         //Natural: MOVE CV-PPG-TEAM-CDE TO IEFA9000.OPRTR-UNIT-CDE IEFA9000.UNIT-CDE ( 1 )
                pdaIefa9000.getIefa9000_Unit_Cde().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde());
                //*  8/21/04 RW
                //*  8/21/04 RW
                if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No().greater(" ")))                                                                                //Natural: IF CV-SGRD-PLAN-NO GT ' '
                {
                    ignore();
                    //*  8/21/04 RW
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM LOAD-ICWF-ADD
                    sub_Load_Icwf_Add();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  8/21/04 RW
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOAD-ICWF-TO-FILE
            sub_Load_Icwf_To_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(3, false, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M());                                   //Natural: WRITE WORK FILE 3 CV-RULE-DATA-L CV-RULE-DATA-M
            //*                                                                                                                                                           //Natural: AT BREAK OF CV-KEY
            readWork02Cv_KeyOld.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Key());                                                                                           //Natural: END-WORK
        }
        READWORK02_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork02(endOfDataReadwork02);
        }
        if (Global.isEscape()) return;
        getReports().write(1, "*",new RepeatItem(79),NEWLINE,NEWLINE,"Program",Global.getPROGRAM(),new ColumnSpacing(50),"Date",Global.getDATU(),NEWLINE,new              //Natural: WRITE ( 1 ) '*' ( 79 ) // 'Program' *PROGRAM 50X 'Date' *DATU / 20X '- PRAP Overdue Premium Control Statistics -' // 4X 'Total records read from extract           ' #REC-IN ( EM = Z,ZZZ,ZZ9 ) / 4X 'Total records written for overdues        ' #REC-OUT ( EM = Z,ZZZ,ZZ9 ) / 4X 'Total records written to POST for overdues' #REC-POST ( EM = Z,ZZZ,ZZ9 ) //
            ColumnSpacing(20),"- PRAP Overdue Premium Control Statistics -",NEWLINE,NEWLINE,new ColumnSpacing(4),"Total records read from extract           ",pnd_Rec_In, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(4),"Total records written for overdues        ",pnd_Rec_Out, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
            ColumnSpacing(4),"Total records written to POST for overdues",pnd_Rec_Post, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Write.reset();                                                                                                                                                //Natural: RESET #WRITE
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(pnd_Status_P.getValue(pnd_I).equals(getZero())))                                                                                                //Natural: IF #STATUS-P ( #I ) EQ 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Write.equals(" ")))                                                                                                                         //Natural: IF #WRITE EQ ' '
            {
                getReports().write(1, NEWLINE,NEWLINE,"    PREMIUMS",NEWLINE);                                                                                            //Natural: WRITE ( 1 ) // '    PREMIUMS' /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Write.setValue("Y");                                                                                                                                  //Natural: MOVE 'Y' TO #WRITE
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(pnd_I),new ColumnSpacing(6),pnd_Status_P.getValue(pnd_I), new ReportEditMask                //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #I ) 6X #STATUS-P ( #I ) ( EM = Z,ZZZ,ZZZ )
                ("Z,ZZZ,ZZZ"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Sub_Total.nadd(pnd_Status_P.getValue(pnd_I));                                                                                                             //Natural: ADD #STATUS-P ( #I ) TO #SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Sub_Total.notEquals(getZero())))                                                                                                                //Natural: IF #SUB-TOTAL NE 0
        {
            getReports().write(1, NEWLINE,"    Total ovd premiums **",new ColumnSpacing(22),pnd_Sub_Total, new ReportEditMask ("Z,ZZZ,ZZZ"));                             //Natural: WRITE ( 1 ) / '    Total ovd premiums **' 22X #SUB-TOTAL ( EM = Z,ZZZ,ZZZ )
            if (Global.isEscape()) return;
            pnd_Sub_Total.reset();                                                                                                                                        //Natural: RESET #SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-IF
        //* *=============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-ICWF-ADD
        //* *=========================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-ICWF
        //* *=================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-ICWF-TO-FILE
        //* *======
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Load_Icwf_Add() throws Exception                                                                                                                     //Natural: LOAD-ICWF-ADD
    {
        if (BLNatReinput.isReinput()) return;

        //* *=============================
        pdaIefa9000.getIefa9000_Action().setValue("AR");                                                                                                                  //Natural: MOVE 'AR' TO IEFA9000.ACTION
        pdaIefa9000.getIefa9000_System().setValue("IMIT");                                                                                                                //Natural: MOVE 'IMIT' TO IEFA9000.SYSTEM
        pdaIefa9000.getIefa9000_Ppg_Cde().getValue(1).setValue("000000");                                                                                                 //Natural: MOVE '000000' TO IEFA9000.PPG-CDE ( 1 )
        pdaIefa9000.getIefa9000_Iis_Hrchy_Level_Cde().getValue(1).setValue("I");                                                                                          //Natural: MOVE 'I' TO IEFA9000.IIS-HRCHY-LEVEL-CDE ( 1 )
        pdaIefa9000.getIefa9000_Work_Prcss_Id().getValue(1).setValue("TPRPMT");                                                                                           //Natural: MOVE 'TPRPMT' TO IEFA9000.WORK-PRCSS-ID ( 1 )
        pdaIefa9000.getIefa9000_Check_Ind().getValue(1).setValue("N");                                                                                                    //Natural: MOVE 'N' TO IEFA9000.CHECK-IND ( 1 )
        pdaIefa9000.getIefa9000_Sap_Ind().getValue(1).setValue("N");                                                                                                      //Natural: MOVE 'N' TO IEFA9000.SAP-IND ( 1 )
        pdaIefa9000.getIefa9000_Tiaa_Rcvd_Dte_Tme().getValue(1).setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                   //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO IEFA9000.TIAA-RCVD-DTE-TME ( 1 )
        pdaIefa9000.getIefa9000_Cntct_Dte_Tme().setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDDHHIISST"));                                                   //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDDHHIISST ) TO IEFA9000.CNTCT-DTE-TME
        pdaIefa9000.getIefa9000_Oprtr_Cde().setValue("BATCHACIS");                                                                                                        //Natural: MOVE 'BATCHACIS' TO IEFA9000.OPRTR-CDE
        pdaIefa9000.getIefa9000_Rqst_Orgn_Cde().setValue("I");                                                                                                            //Natural: MOVE 'I' TO IEFA9000.RQST-ORGN-CDE
        pdaIefa9000.getIefa9000_Status_Cde().getValue(1).setValue("0009");                                                                                                //Natural: MOVE '0009' TO IEFA9000.STATUS-CDE ( 1 )
                                                                                                                                                                          //Natural: PERFORM CALL-ICWF
        sub_Call_Icwf();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Call_Icwf() throws Exception                                                                                                                         //Natural: CALL-ICWF
    {
        if (BLNatReinput.isReinput()) return;

        //* *=========================
        DbsUtil.callnat(Iefn9000.class , getCurrentProcessState(), pdaIefa9000.getIefa9000(), pdaCdaobj.getCdaobj(), pdaIcwpda_D.getDialog_Info_Sub(),                    //Natural: CALLNAT 'IEFN9000' IEFA9000 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaIcwpda_M.getMsg_Info_Sub(), pdaIcwpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ") && pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().equals(getZero())))                           //Natural: IF ##MSG EQ ' ' AND ##MSG-NR EQ 0
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "-",new RepeatItem(20),Global.getPROGRAM(),"(1820) - ICWF-ERROR","-",new RepeatItem(20),NEWLINE,"=",pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr(), //Natural: WRITE '-' ( 20 ) *PROGRAM '(1820) - ICWF-ERROR' '-' ( 20 ) / '=' ##MSG-NR / '=' ##MSG / '=' IEFA9000.PPG-CDE ( 1 ) / '=' IEFA9000.IIS-HRCHY-KEY-CDE ( 1 ) / '=' IEFA9000.IIS-HRCHY-LEVEL-CDE ( 1 ) / '=' IEFA9000.OPRTR-UNIT-CDE / '=' IEFA9000.UNIT-CDE ( 1 ) / '=' IEFA9000.WORK-PRCSS-ID ( 1 ) //
                NEWLINE,"=",pdaIcwpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),NEWLINE,"=",pdaIefa9000.getIefa9000_Ppg_Cde().getValue(1),NEWLINE,"=",pdaIefa9000.getIefa9000_Iis_Hrchy_Key_Cde().getValue(1),
                NEWLINE,"=",pdaIefa9000.getIefa9000_Iis_Hrchy_Level_Cde().getValue(1),NEWLINE,"=",pdaIefa9000.getIefa9000_Oprtr_Unit_Cde(),NEWLINE,"=",pdaIefa9000.getIefa9000_Unit_Cde().getValue(1),
                NEWLINE,"=",pdaIefa9000.getIefa9000_Work_Prcss_Id().getValue(1),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Icwf_To_File() throws Exception                                                                                                                 //Natural: LOAD-ICWF-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *=================================
        if (condition(pdaIefa9000.getIefa9000_Rqst_Log_Dte_Tme().getValue(1).equals(" ")))                                                                                //Natural: IF IEFA9000.RQST-LOG-DTE-TME ( 1 ) EQ ' '
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Log().reset();                                                                                                            //Natural: RESET CV-IMIT-LOG CV-IMIT-OPRTR CV-IMIT-UNIT CV-IMIT-WPID CV-IMIT-STEP CV-IMIT-STATUS
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Oprtr().reset();
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Unit().reset();
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Wpid().reset();
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Step().reset();
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Status().reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Log().setValue(pdaIefa9000.getIefa9000_Rqst_Log_Dte_Tme().getValue(1));                                                   //Natural: MOVE IEFA9000.RQST-LOG-DTE-TME ( 1 ) TO CV-IMIT-LOG
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Oprtr().setValue(pdaIefa9000.getIefa9000_Oprtr_Cde());                                                                    //Natural: MOVE IEFA9000.OPRTR-CDE TO CV-IMIT-OPRTR
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Unit().setValue(pdaIefa9000.getIefa9000_Oprtr_Unit_Cde());                                                                //Natural: MOVE IEFA9000.OPRTR-UNIT-CDE TO CV-IMIT-UNIT
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Wpid().setValue(pdaIefa9000.getIefa9000_Work_Prcss_Id().getValue(1));                                                     //Natural: MOVE IEFA9000.WORK-PRCSS-ID ( 1 ) TO CV-IMIT-WPID
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Step().setValue(pdaIefa9000.getIefa9000_Step_Id().getValue(1));                                                           //Natural: MOVE IEFA9000.STEP-ID ( 1 ) TO CV-IMIT-STEP
            ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Status().setValue(pdaIefa9000.getIefa9000_Status_Cde().getValue(1));                                                      //Natural: MOVE IEFA9000.STATUS-CDE ( 1 ) TO CV-IMIT-STATUS
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"Error:     ",Global.getERROR_NR());                 //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'Error:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork02() throws Exception {atBreakEventReadwork02(false);}
    private void atBreakEventReadwork02(boolean endOfData) throws Exception
    {
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Key().isBreak(endOfData);
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            pdaIefa9000.getIefa9000().reset();                                                                                                                            //Natural: RESET IEFA9000
            pdaIefa9000.getIefa9000_Iis_Hrchy_Key_Cde().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Inst_Link_Cde());                                             //Natural: MOVE CV-INST-LINK-CDE TO IEFA9000.IIS-HRCHY-KEY-CDE ( 1 )
            pdaIefa9000.getIefa9000_Oprtr_Unit_Cde().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde());                                                             //Natural: MOVE CV-PPG-TEAM-CDE TO IEFA9000.OPRTR-UNIT-CDE IEFA9000.UNIT-CDE ( 1 )
            pdaIefa9000.getIefa9000_Unit_Cde().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde());
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Key().notEquals(readWork02Cv_KeyOld)))                                                                           //Natural: IF CV-KEY NE OLD ( CV-KEY )
            {
                //*  8/21/04 RW
                //*  8/21/04 RW
                if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No().greater(" ")))                                                                                //Natural: IF CV-SGRD-PLAN-NO GT ' '
                {
                    ignore();
                    //*  8/21/04 RW
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM LOAD-ICWF-ADD
                    sub_Load_Icwf_Add();
                    if (condition(Global.isEscape())) {return;}
                    //*  8/21/04 RW
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 HW=OFF PS=52");
        Global.format(1, "LS=133 HW=OFF PS=52");
    }
}
