/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:47 PM
**        * FROM NATURAL PROGRAM : Appb2072
************************************************************
**        * FILE NAME            : Appb2072.java
**        * CLASS NAME           : Appb2072
**        * INSTANCE NAME        : Appb2072
************************************************************
************************************************************************
* APPB2072 - MONTHLY CONTRACT RANGE USAGE REPORT                       *
************************************************************************
*
************************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                     *
* ----------  ----------------  -------------------------------------- *
* 10/17/2005  JANET BERGHEISER  NEW                                    *
* 06/23/2007  JANET BERGHEISER  INCREASE SIZE OF USAGE FIELD (JB1)     *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2072 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Range_Usage;

    private DbsGroup pnd_Range_Usage__R_Field_1;
    private DbsField pnd_Range_Usage_Pnd_R_Record_Type;
    private DbsField pnd_Range_Usage_Pnd_R_Filler_1;
    private DbsField pnd_Range_Usage_Pnd_R_Contract_Type;
    private DbsField pnd_Range_Usage_Pnd_R_Filler_2;
    private DbsField pnd_Range_Usage_Pnd_R_Beg_Month_Day;
    private DbsField pnd_Range_Usage_Pnd_R_End_Month_Day;
    private DbsField pnd_Range_Usage_Pnd_R_Filler_3;
    private DbsField pnd_Range_Usage_Pnd_R_Product_Code;
    private DbsField pnd_Range_Usage_Pnd_R_Range_Used;

    private DbsGroup pnd_Range_Usage__R_Field_2;
    private DbsField pnd_Range_Usage_Pnd_H_Record_Type;
    private DbsField pnd_Range_Usage_Pnd_H_Filler_1;
    private DbsField pnd_Range_Usage_Pnd_H_Contract_Type;
    private DbsField pnd_Range_Usage_Pnd_H_Filler_2;
    private DbsField pnd_Range_Usage_Pnd_H_Beg_Date;

    private DbsGroup pnd_Range_Usage__R_Field_3;
    private DbsField pnd_Range_Usage_Pnd_H_Beg_Month_Day;
    private DbsField pnd_Range_Usage_Pnd_H_Filler_3;
    private DbsField pnd_Range_Usage_Pnd_H_Filler_4;
    private DbsField pnd_Range_Usage_Pnd_H_End_Date;

    private DbsGroup pnd_Range_Usage__R_Field_4;
    private DbsField pnd_Range_Usage_Pnd_H_End_Month_Day;
    private DbsField pnd_Range_Usage_Pnd_H_Filler_5;
    private DbsField pnd_Range_Usage_Pnd_H_Filler_6;

    private DbsGroup pnd_Title_Fields;
    private DbsField pnd_Title_Fields_Pnd_Contract_Type;
    private DbsField pnd_Title_Fields_Pnd_Beg_Date;
    private DbsField pnd_Title_Fields_Pnd_End_Date;

    private DbsGroup pnd_Column_Beg_Date;
    private DbsField pnd_Column_Beg_Date_Pnd_D_Beg_Date;

    private DbsGroup pnd_Column_Beg_Date__R_Field_5;
    private DbsField pnd_Column_Beg_Date_Pnd_D_Beg_Mmdd;
    private DbsField pnd_Column_Beg_Date_Pnd_D_Filler_1;

    private DbsGroup pnd_Column_End_Date;
    private DbsField pnd_Column_End_Date_Pnd_D_End_Date;

    private DbsGroup pnd_Column_End_Date__R_Field_6;
    private DbsField pnd_Column_End_Date_Pnd_D_End_Mmdd;
    private DbsField pnd_Column_End_Date_Pnd_D_Filler_1;

    private DbsGroup pnd_Detail_Fields;
    private DbsField pnd_Detail_Fields_Pnd_D_Product_Code;
    private DbsField pnd_Detail_Fields_Pnd_D_Range_Used;
    private DbsField pnd_Hold_Product_Code;
    private DbsField pnd_Product_Count;
    private DbsField pnd_Header_Count;
    private DbsField pnd_Detail_Count;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Product_Total;
    private DbsField pnd_Product_Average;
    private DbsField pnd_Tbl_Max;
    private DbsField pnd_Tbl_Sub;
    private DbsField pnd_Tbl_Cnt;
    private DbsField pnd_Sub;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Range_Usage = localVariables.newFieldInRecord("pnd_Range_Usage", "#RANGE-USAGE", FieldType.STRING, 45);

        pnd_Range_Usage__R_Field_1 = localVariables.newGroupInRecord("pnd_Range_Usage__R_Field_1", "REDEFINE", pnd_Range_Usage);
        pnd_Range_Usage_Pnd_R_Record_Type = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Record_Type", "#R-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_R_Filler_1 = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Filler_1", "#R-FILLER-1", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_R_Contract_Type = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Contract_Type", "#R-CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Range_Usage_Pnd_R_Filler_2 = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Filler_2", "#R-FILLER-2", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_R_Beg_Month_Day = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Beg_Month_Day", "#R-BEG-MONTH-DAY", FieldType.STRING, 
            5);
        pnd_Range_Usage_Pnd_R_End_Month_Day = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_End_Month_Day", "#R-END-MONTH-DAY", FieldType.STRING, 
            5);
        pnd_Range_Usage_Pnd_R_Filler_3 = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Filler_3", "#R-FILLER-3", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_R_Product_Code = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Product_Code", "#R-PRODUCT-CODE", FieldType.STRING, 
            10);
        pnd_Range_Usage_Pnd_R_Range_Used = pnd_Range_Usage__R_Field_1.newFieldInGroup("pnd_Range_Usage_Pnd_R_Range_Used", "#R-RANGE-USED", FieldType.NUMERIC, 
            9);

        pnd_Range_Usage__R_Field_2 = localVariables.newGroupInRecord("pnd_Range_Usage__R_Field_2", "REDEFINE", pnd_Range_Usage);
        pnd_Range_Usage_Pnd_H_Record_Type = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_Record_Type", "#H-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_H_Filler_1 = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_Filler_1", "#H-FILLER-1", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_H_Contract_Type = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_Contract_Type", "#H-CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Range_Usage_Pnd_H_Filler_2 = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_Filler_2", "#H-FILLER-2", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_H_Beg_Date = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_Beg_Date", "#H-BEG-DATE", FieldType.STRING, 
            10);

        pnd_Range_Usage__R_Field_3 = pnd_Range_Usage__R_Field_2.newGroupInGroup("pnd_Range_Usage__R_Field_3", "REDEFINE", pnd_Range_Usage_Pnd_H_Beg_Date);
        pnd_Range_Usage_Pnd_H_Beg_Month_Day = pnd_Range_Usage__R_Field_3.newFieldInGroup("pnd_Range_Usage_Pnd_H_Beg_Month_Day", "#H-BEG-MONTH-DAY", FieldType.STRING, 
            5);
        pnd_Range_Usage_Pnd_H_Filler_3 = pnd_Range_Usage__R_Field_3.newFieldInGroup("pnd_Range_Usage_Pnd_H_Filler_3", "#H-FILLER-3", FieldType.STRING, 
            5);
        pnd_Range_Usage_Pnd_H_Filler_4 = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_Filler_4", "#H-FILLER-4", FieldType.STRING, 
            1);
        pnd_Range_Usage_Pnd_H_End_Date = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_End_Date", "#H-END-DATE", FieldType.STRING, 
            10);

        pnd_Range_Usage__R_Field_4 = pnd_Range_Usage__R_Field_2.newGroupInGroup("pnd_Range_Usage__R_Field_4", "REDEFINE", pnd_Range_Usage_Pnd_H_End_Date);
        pnd_Range_Usage_Pnd_H_End_Month_Day = pnd_Range_Usage__R_Field_4.newFieldInGroup("pnd_Range_Usage_Pnd_H_End_Month_Day", "#H-END-MONTH-DAY", FieldType.STRING, 
            5);
        pnd_Range_Usage_Pnd_H_Filler_5 = pnd_Range_Usage__R_Field_4.newFieldInGroup("pnd_Range_Usage_Pnd_H_Filler_5", "#H-FILLER-5", FieldType.STRING, 
            5);
        pnd_Range_Usage_Pnd_H_Filler_6 = pnd_Range_Usage__R_Field_2.newFieldInGroup("pnd_Range_Usage_Pnd_H_Filler_6", "#H-FILLER-6", FieldType.STRING, 
            9);

        pnd_Title_Fields = localVariables.newGroupInRecord("pnd_Title_Fields", "#TITLE-FIELDS");
        pnd_Title_Fields_Pnd_Contract_Type = pnd_Title_Fields.newFieldInGroup("pnd_Title_Fields_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Title_Fields_Pnd_Beg_Date = pnd_Title_Fields.newFieldInGroup("pnd_Title_Fields_Pnd_Beg_Date", "#BEG-DATE", FieldType.STRING, 10);
        pnd_Title_Fields_Pnd_End_Date = pnd_Title_Fields.newFieldInGroup("pnd_Title_Fields_Pnd_End_Date", "#END-DATE", FieldType.STRING, 10);

        pnd_Column_Beg_Date = localVariables.newGroupArrayInRecord("pnd_Column_Beg_Date", "#COLUMN-BEG-DATE", new DbsArrayController(1, 5));
        pnd_Column_Beg_Date_Pnd_D_Beg_Date = pnd_Column_Beg_Date.newFieldInGroup("pnd_Column_Beg_Date_Pnd_D_Beg_Date", "#D-BEG-DATE", FieldType.STRING, 
            6);

        pnd_Column_Beg_Date__R_Field_5 = pnd_Column_Beg_Date.newGroupInGroup("pnd_Column_Beg_Date__R_Field_5", "REDEFINE", pnd_Column_Beg_Date_Pnd_D_Beg_Date);
        pnd_Column_Beg_Date_Pnd_D_Beg_Mmdd = pnd_Column_Beg_Date__R_Field_5.newFieldInGroup("pnd_Column_Beg_Date_Pnd_D_Beg_Mmdd", "#D-BEG-MMDD", FieldType.STRING, 
            5);
        pnd_Column_Beg_Date_Pnd_D_Filler_1 = pnd_Column_Beg_Date__R_Field_5.newFieldInGroup("pnd_Column_Beg_Date_Pnd_D_Filler_1", "#D-FILLER-1", FieldType.STRING, 
            1);

        pnd_Column_End_Date = localVariables.newGroupArrayInRecord("pnd_Column_End_Date", "#COLUMN-END-DATE", new DbsArrayController(1, 5));
        pnd_Column_End_Date_Pnd_D_End_Date = pnd_Column_End_Date.newFieldInGroup("pnd_Column_End_Date_Pnd_D_End_Date", "#D-END-DATE", FieldType.STRING, 
            6);

        pnd_Column_End_Date__R_Field_6 = pnd_Column_End_Date.newGroupInGroup("pnd_Column_End_Date__R_Field_6", "REDEFINE", pnd_Column_End_Date_Pnd_D_End_Date);
        pnd_Column_End_Date_Pnd_D_End_Mmdd = pnd_Column_End_Date__R_Field_6.newFieldInGroup("pnd_Column_End_Date_Pnd_D_End_Mmdd", "#D-END-MMDD", FieldType.STRING, 
            5);
        pnd_Column_End_Date_Pnd_D_Filler_1 = pnd_Column_End_Date__R_Field_6.newFieldInGroup("pnd_Column_End_Date_Pnd_D_Filler_1", "#D-FILLER-1", FieldType.STRING, 
            1);

        pnd_Detail_Fields = localVariables.newGroupInRecord("pnd_Detail_Fields", "#DETAIL-FIELDS");
        pnd_Detail_Fields_Pnd_D_Product_Code = pnd_Detail_Fields.newFieldInGroup("pnd_Detail_Fields_Pnd_D_Product_Code", "#D-PRODUCT-CODE", FieldType.STRING, 
            10);
        pnd_Detail_Fields_Pnd_D_Range_Used = pnd_Detail_Fields.newFieldArrayInGroup("pnd_Detail_Fields_Pnd_D_Range_Used", "#D-RANGE-USED", FieldType.STRING, 
            8, new DbsArrayController(1, 5));
        pnd_Hold_Product_Code = localVariables.newFieldInRecord("pnd_Hold_Product_Code", "#HOLD-PRODUCT-CODE", FieldType.STRING, 10);
        pnd_Product_Count = localVariables.newFieldInRecord("pnd_Product_Count", "#PRODUCT-COUNT", FieldType.NUMERIC, 9);
        pnd_Header_Count = localVariables.newFieldInRecord("pnd_Header_Count", "#HEADER-COUNT", FieldType.NUMERIC, 9);
        pnd_Detail_Count = localVariables.newFieldInRecord("pnd_Detail_Count", "#DETAIL-COUNT", FieldType.NUMERIC, 9);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 9);
        pnd_Product_Total = localVariables.newFieldInRecord("pnd_Product_Total", "#PRODUCT-TOTAL", FieldType.NUMERIC, 12);
        pnd_Product_Average = localVariables.newFieldInRecord("pnd_Product_Average", "#PRODUCT-AVERAGE", FieldType.NUMERIC, 8, 2);
        pnd_Tbl_Max = localVariables.newFieldInRecord("pnd_Tbl_Max", "#TBL-MAX", FieldType.NUMERIC, 5);
        pnd_Tbl_Sub = localVariables.newFieldInRecord("pnd_Tbl_Sub", "#TBL-SUB", FieldType.NUMERIC, 5);
        pnd_Tbl_Cnt = localVariables.newFieldInRecord("pnd_Tbl_Cnt", "#TBL-CNT", FieldType.NUMERIC, 5);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Tbl_Max.setInitialValue(5);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2072() throws Exception
    {
        super("Appb2072");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 65
        //*  JB1
        //*  JB1
        //*  JB1
        //*  JB1
        //*  JB1
        //*  JB1
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 1X *PROGRAM 13X 'MONTHLY' #CONTRACT-TYPE 'CONTRACT RANGE USAGE REPORT' 69T *DATX ( EM = MM/DD/YYYY ) / 29X #BEG-DATE '-' #END-DATE // 1X 'PRODUCT' 7X #D-BEG-DATE ( 1 ) 3X #D-BEG-DATE ( 2 ) 3X #D-BEG-DATE ( 3 ) 3X #D-BEG-DATE ( 4 ) 3X #D-BEG-DATE ( 5 ) 5X 'PRODUCT   PRODUCT' / 1X 'CODE' 10X #D-END-DATE ( 1 ) 3X #D-END-DATE ( 2 ) 3X #D-END-DATE ( 3 ) 3X #D-END-DATE ( 4 ) 3X #D-END-DATE ( 5 ) 5X '  TOTAL   AVERAGE' /
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #RANGE-USAGE
        while (condition(getWorkFiles().read(1, pnd_Range_Usage)))
        {
            //*  HEADER RECORD                                                                                                                                            //Natural: AT END OF DATA
            short decideConditionsMet108 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #R-RECORD-TYPE;//Natural: VALUE 'H'
            if (condition((pnd_Range_Usage_Pnd_R_Record_Type.equals("H"))))
            {
                decideConditionsMet108++;
                //*  DETAIL RECORD
                                                                                                                                                                          //Natural: PERFORM HEADER-PROCESSING
                sub_Header_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Range_Usage_Pnd_R_Record_Type.equals("D"))))
            {
                decideConditionsMet108++;
                                                                                                                                                                          //Natural: PERFORM DETAIL-PROCESSING
                sub_Detail_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Record_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORD-COUNT
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pnd_Record_Count.equals(getZero())))                                                                                                            //Natural: IF #RECORD-COUNT = 0
            {
                getReports().write(1, "EMPTY MONTHLY ACCUM FILE");                                                                                                        //Natural: WRITE ( 1 ) 'EMPTY MONTHLY ACCUM FILE'
                if (condition(Global.isEscape())) return;
                //* *      TERMINATE 32
                //*  (0960)
            }                                                                                                                                                             //Natural: ESCAPE BOTTOM;//Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BREAK-PROCESSING
            sub_Break_Processing();
            if (condition(Global.isEscape())) {return;}
            //*  (0960)
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,new ColumnSpacing(1),"PRODUCT COUNT:",pnd_Product_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new                     //Natural: WRITE ( 1 ) / 1X 'PRODUCT COUNT:' #PRODUCT-COUNT ( EM = ZZZ,ZZZ,ZZ9 ) // 29X '*** END OF REPORT ***'
            ColumnSpacing(29),"*** END OF REPORT ***");
        if (Global.isEscape()) return;
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HEADER-PROCESSING
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETAIL-PROCESSING
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-PROCESSING
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-USAGE-ARRAY
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-LINE
    }
    private void sub_Header_Processing() throws Exception                                                                                                                 //Natural: HEADER-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Header_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #HEADER-COUNT
        pnd_Sub.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #SUB
        if (condition(pnd_Header_Count.equals(1)))                                                                                                                        //Natural: IF #HEADER-COUNT = 1
        {
            pnd_Title_Fields_Pnd_Contract_Type.setValue(pnd_Range_Usage_Pnd_H_Contract_Type);                                                                             //Natural: ASSIGN #CONTRACT-TYPE := #H-CONTRACT-TYPE
            pnd_Title_Fields_Pnd_Beg_Date.setValue(pnd_Range_Usage_Pnd_H_Beg_Date);                                                                                       //Natural: ASSIGN #BEG-DATE := #H-BEG-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Title_Fields_Pnd_End_Date.setValue(pnd_Range_Usage_Pnd_H_End_Date);                                                                                       //Natural: ASSIGN #END-DATE := #H-END-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Column_Beg_Date_Pnd_D_Beg_Date.getValue(pnd_Sub).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Range_Usage_Pnd_H_Beg_Month_Day,                //Natural: COMPRESS #H-BEG-MONTH-DAY '-' INTO #D-BEG-DATE ( #SUB ) LEAVING NO
            "-"));
        pnd_Column_End_Date_Pnd_D_End_Date.getValue(pnd_Sub).setValue(pnd_Range_Usage_Pnd_H_End_Month_Day, MoveOption.LeftJustified);                                     //Natural: MOVE LEFT #H-END-MONTH-DAY TO #D-END-DATE ( #SUB )
    }
    private void sub_Detail_Processing() throws Exception                                                                                                                 //Natural: DETAIL-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Detail_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DETAIL-COUNT
        if (condition(pnd_Detail_Count.equals(1)))                                                                                                                        //Natural: IF #DETAIL-COUNT = 1
        {
            pnd_Sub.reset();                                                                                                                                              //Natural: RESET #SUB #PRODUCT-COUNT
            pnd_Product_Count.reset();
            pnd_Hold_Product_Code.setValue(pnd_Range_Usage_Pnd_R_Product_Code);                                                                                           //Natural: ASSIGN #HOLD-PRODUCT-CODE := #R-PRODUCT-CODE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Range_Usage_Pnd_R_Product_Code.notEquals(pnd_Hold_Product_Code)))                                                                               //Natural: IF #R-PRODUCT-CODE NE #HOLD-PRODUCT-CODE
        {
                                                                                                                                                                          //Natural: PERFORM BREAK-PROCESSING
            sub_Break_Processing();
            if (condition(Global.isEscape())) {return;}
            pnd_Hold_Product_Code.setValue(pnd_Range_Usage_Pnd_R_Product_Code);                                                                                           //Natural: ASSIGN #HOLD-PRODUCT-CODE := #R-PRODUCT-CODE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BUILD-USAGE-ARRAY
        sub_Build_Usage_Array();
        if (condition(Global.isEscape())) {return;}
        pnd_Product_Total.nadd(pnd_Range_Usage_Pnd_R_Range_Used);                                                                                                         //Natural: ADD #R-RANGE-USED TO #PRODUCT-TOTAL
    }
    //*  BREAK OF PRODUCT CODE
    private void sub_Break_Processing() throws Exception                                                                                                                  //Natural: BREAK-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        if (condition(pnd_Sub.greater(getZero())))                                                                                                                        //Natural: IF #SUB > 0
        {
            pnd_Product_Average.compute(new ComputeParameters(false, pnd_Product_Average), pnd_Product_Total.divide(pnd_Sub));                                            //Natural: ASSIGN #PRODUCT-AVERAGE := #PRODUCT-TOTAL / #SUB
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Detail_Fields_Pnd_D_Product_Code.setValue(pnd_Hold_Product_Code);                                                                                             //Natural: ASSIGN #D-PRODUCT-CODE := #HOLD-PRODUCT-CODE
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
        sub_Write_Detail_Line();
        if (condition(Global.isEscape())) {return;}
        pnd_Product_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #PRODUCT-COUNT
    }
    private void sub_Build_Usage_Array() throws Exception                                                                                                                 //Natural: BUILD-USAGE-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Sub.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #SUB
        if (condition((pnd_Sub.lessOrEqual(pnd_Tbl_Max)) && (pnd_Range_Usage_Pnd_R_Beg_Month_Day.equals(pnd_Column_Beg_Date_Pnd_D_Beg_Mmdd.getValue(pnd_Sub)))))          //Natural: IF ( #SUB LE #TBL-MAX ) AND ( #R-BEG-MONTH-DAY = #D-BEG-MMDD ( #SUB ) )
        {
            //*  JB1
            pnd_Detail_Fields_Pnd_D_Range_Used.getValue(pnd_Sub).setValueEdited(pnd_Range_Usage_Pnd_R_Range_Used,new ReportEditMask("ZZZZZZZ9"));                         //Natural: MOVE EDITED #R-RANGE-USED ( EM = ZZZZZZZ9 ) TO #D-RANGE-USED ( #SUB )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(pnd_Column_Beg_Date_Pnd_D_Beg_Mmdd.getValue("*")), new ExamineSearch(pnd_Range_Usage_Pnd_R_Beg_Month_Day),                  //Natural: EXAMINE #D-BEG-MMDD ( * ) #R-BEG-MONTH-DAY GIVING INDEX #SUB
                new ExamineGivingIndex(pnd_Sub));
            if (condition(pnd_Sub.greater(getZero()) && pnd_Sub.lessOrEqual(pnd_Tbl_Max)))                                                                                //Natural: IF #SUB > 0 AND #SUB LE #TBL-MAX
            {
                //*  JB1
                pnd_Detail_Fields_Pnd_D_Range_Used.getValue(pnd_Sub).setValueEdited(pnd_Range_Usage_Pnd_R_Range_Used,new ReportEditMask("ZZZZZZZ9"));                     //Natural: MOVE EDITED #R-RANGE-USED ( EM = ZZZZZZZ9 ) TO #D-RANGE-USED ( #SUB )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Detail_Line() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  JB1
        //*  JB1
        getReports().write(1, new ColumnSpacing(1),pnd_Detail_Fields_Pnd_D_Product_Code,new ColumnSpacing(1),pnd_Detail_Fields_Pnd_D_Range_Used.getValue(1),new           //Natural: WRITE ( 1 ) 1X #D-PRODUCT-CODE 1X #D-RANGE-USED ( 1 ) 1X #D-RANGE-USED ( 2 ) 1X #D-RANGE-USED ( 3 ) 1X #D-RANGE-USED ( 4 ) 1X #D-RANGE-USED ( 5 ) 1X #PRODUCT-TOTAL ( EM = ZZZZZZZZZZZ9 ) #PRODUCT-AVERAGE ( EM = ZZZZZ9.99 )
            ColumnSpacing(1),pnd_Detail_Fields_Pnd_D_Range_Used.getValue(2),new ColumnSpacing(1),pnd_Detail_Fields_Pnd_D_Range_Used.getValue(3),new ColumnSpacing(1),pnd_Detail_Fields_Pnd_D_Range_Used.getValue(4),new 
            ColumnSpacing(1),pnd_Detail_Fields_Pnd_D_Range_Used.getValue(5),new ColumnSpacing(1),pnd_Product_Total, new ReportEditMask ("ZZZZZZZZZZZ9"),pnd_Product_Average, 
            new ReportEditMask ("ZZZZZ9.99"));
        if (Global.isEscape()) return;
        pnd_Sub.reset();                                                                                                                                                  //Natural: RESET #SUB #DETAIL-FIELDS #PRODUCT-TOTAL #PRODUCT-AVERAGE
        pnd_Detail_Fields.reset();
        pnd_Product_Total.reset();
        pnd_Product_Average.reset();
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=65");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new ColumnSpacing(1),Global.getPROGRAM(),new ColumnSpacing(13),"MONTHLY",pnd_Title_Fields_Pnd_Contract_Type,"CONTRACT RANGE USAGE REPORT",new 
            TabSetting(69),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new ColumnSpacing(29),pnd_Title_Fields_Pnd_Beg_Date,"-",pnd_Title_Fields_Pnd_End_Date,NEWLINE,NEWLINE,new 
            ColumnSpacing(1),"PRODUCT",new ColumnSpacing(7),pnd_Column_Beg_Date_Pnd_D_Beg_Date,new ColumnSpacing(3),pnd_Column_Beg_Date_Pnd_D_Beg_Date,new 
            ColumnSpacing(3),pnd_Column_Beg_Date_Pnd_D_Beg_Date,new ColumnSpacing(3),pnd_Column_Beg_Date_Pnd_D_Beg_Date,new ColumnSpacing(3),pnd_Column_Beg_Date_Pnd_D_Beg_Date,new 
            ColumnSpacing(5),"PRODUCT   PRODUCT",NEWLINE,new ColumnSpacing(1),"CODE",new ColumnSpacing(10),pnd_Column_End_Date_Pnd_D_End_Date,new ColumnSpacing(3),pnd_Column_End_Date_Pnd_D_End_Date,new 
            ColumnSpacing(3),pnd_Column_End_Date_Pnd_D_End_Date,new ColumnSpacing(3),pnd_Column_End_Date_Pnd_D_End_Date,new ColumnSpacing(3),pnd_Column_End_Date_Pnd_D_End_Date,new 
            ColumnSpacing(5),"  TOTAL   AVERAGE",NEWLINE);
    }
}
