/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:42 PM
**        * FROM NATURAL PROGRAM : Appb1080
************************************************************
**        * FILE NAME            : Appb1080.java
**        * CLASS NAME           : Appb1080
**        * INSTANCE NAME        : Appb1080
************************************************************
************************************************************************
* PROGRAM  : APPB1080 - UPDATE REPRINT-FILE WITH DELETED PRAP RECORDS  *
* FUNCTION : READS DELETED RECORDS FROM PRAP FILE AND WRITES THEM OUT  *
*            REPRINT FILE.  ALSO WRITE THE CONTROL DATE TO A GDG FILE  *
*                                                                      *
* UPDATED  : 08/06/09 C.AVE     - NEW PROGRAM                          *
*            03/15/10 B.HOLLOWAY- RECOMPILE FOR TIAA STABLE VALUE      *
*                                 PROJECT - NEW VERSION OF APPL180     *
*          : 03/16/10 C.AVE     - INCLUDED IRA INDEXED PRODUCTS-TIGR   *
*          : 03/04/11 B. ELLO   - FLORIDA ENROLLMENT PROJECT BE2.      *
*          : 05/04/11 C. SCHNEIDER - ADD E-DELIVERY,ANNTY PRINT OPTION *
*                                 IND, AND CONSENT E-MAIL ADDR TO PRAP *
*                                 AND REPRINT FILES AND UPDATE  (JHU)  *
*          : 07/05/11 L.SHU     - TRANSFER-IN-CREDIT PROJECT LS1.      *
*          : 08/04/11 G. SUDSATAYA - RESTOW FOR CAMPUS - USE APPL180   *
*            03/23/12 K.GATES   - RE-STOW FOR APPL180 FOR ROCH2        *
*            11/17/12 L.SHU     - POPULATE THE ANNUITY FUNDS AND ISSUE *
*                                 DATE FIELDS TO THE REPRINT FILE FOR  *
*                                 DELETED RECORDS.             (LPAO)  *
*            07/18/13 L.SHU     - POPULATE FIELDS TO REPRINT FILE FOR  *
*                                 IRA SUBSTITUTION WITH:       (TNGSUB)*
*                                 AP-TIAA-DOI / AP-CREF-DOI            *
*                                 THE AP-TIAA-DOI AND AP-CREF-DOI WILL *
*                                 HOLD THE BUSINESS DATE OF WHEN THE   *
*                                 CONTRACT WAS ISSUED.                 *
*                                 AP-SUBSTITUTION-CONTRACT-IND         *
*                                 AP-CONV-ISSUE-STATE                  *
*                                 AP-DECEASED-IND                      *
*            09/13/13 L.SHU     - POPULATE FIELD TO REPRINT FILE FOR   *
*                                 MT SINAI                      (MTSIN)*
*            08/1/14  B.NEWSOB  - INVESTMENT MENU BY SUBPLAN    (IMBS) *
*                                 RESTOW FOR APPL180 CHANGES           *
*            09/2014  B.NEWSOB  - ACIS/CIS CREF REDESIGN COMMUNICATIONS*
*                                 RESTOW FOR APPL180 CHANGES    (ACCRC)*
*            09/01/15 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180     *
*                                 FOR BENE THE PLAN.             (BIP) *
*                                 REMOVE COR ACCESS              (CNR) *
* 08/26/2015 - BUDDY NEWSOM - INSTITUTIONAL PREMIUM FILE SUNSET (IPFS) *
* REMOVE VIEW INSIDE THE PROGRAM AS IT IS NOT BEING USED OR REFERENCED.*
* 1 APP-ACTVTY-CNTRL-RECORD-VIEW VIEW OF APP-ACTVTY-CNTRL-RECORD       *
*   2 CNTRL-TODAYS-BUSINESS-DT                                         *
* 03/07/17     L. SHU       - RESTOW FOR NEW FIELDS IN APPL180 (ONEIRA)*
* 06/15/17     BARUA        - PIN EXPANSION CHANGES.  (CHG425939) PINE
* 01/13/18     L. SHU       - MOVE TIAA & CREF INITIAL PREMIUM (ONEIRA2)
* 11/16/19     L. SHU       - MOVE ADDITIONAL ALLOCATION INFO  (IISG)
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb1080 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma211 pdaMdma211;
    private LdaAppl180 ldaAppl180;
    private LdaAppl170 ldaAppl170;
    private PdaAppa100 pdaAppa100;
    private LdaY2datebw ldaY2datebw;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Wk_Control_File;

    private DbsGroup pnd_Wk_Control_File__R_Field_1;
    private DbsField pnd_Wk_Control_File_Pnd_Wk_Control_Dte;

    private DbsGroup pnd_Wk_Control_File__R_Field_2;
    private DbsField pnd_Wk_Control_File_Pnd_Wk_Control_Yyyymmdd;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_G_Key;
    private DbsField annty_Actvty_Prap_View_Ap_G_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Dob;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Bill_Code;

    private DbsGroup annty_Actvty_Prap_View_Ap_Tiaa_Contr;
    private DbsField annty_Actvty_Prap_View_App_Pref;
    private DbsField annty_Actvty_Prap_View_App_Cont;

    private DbsGroup annty_Actvty_Prap_View_Ap_Cref_Contr;
    private DbsField annty_Actvty_Prap_View_App_C_Pref;
    private DbsField annty_Actvty_Prap_View_App_C_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Curr;
    private DbsField annty_Actvty_Prap_View_Ap_T_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_C_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership;
    private DbsField annty_Actvty_Prap_View_Ap_Sex;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Zip;
    private DbsField annty_Actvty_Prap_View_Ap_Name_Addr_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Ent_Sys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Released;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Matched;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Deleted;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Withdrawn;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Discr;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Other_Pols;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Numb_Dailys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_App_Recvd;
    private DbsField annty_Actvty_Prap_View_Ap_App_Source;
    private DbsField annty_Actvty_Prap_View_Ap_Region_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Split_Code;

    private DbsGroup annty_Actvty_Prap_View_Ap_Address_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Line;
    private DbsField annty_Actvty_Prap_View_Ap_City;
    private DbsField annty_Actvty_Prap_View_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Annuity_Start_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Info_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Primary_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Contingent_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Estate;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Trust;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Category;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Instructions;
    private DbsField annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Process_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_St_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Csm_Sec_Seg;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_College;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Ph_Hist_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField annty_Actvty_Prap_View_Ap_Contact_Mode;
    private DbsField annty_Actvty_Prap_View_Ap_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_View_Ap_Sync_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Doi;

    private DbsGroup annty_Actvty_Prap_View_Ap_Mit_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Unit;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Wpid;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Rollover_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Ppg;
    private DbsField annty_Actvty_Prap_View_Ap_Print_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Cntrct_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt;

    private DbsGroup annty_Actvty_Prap_View_Ap_Addr_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Txt;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Dest_Name;
    private DbsField annty_Actvty_Prap_View_Ap_Zip_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup annty_Actvty_Prap_View__R_Field_3;
    private DbsField annty_Actvty_Prap_View_Bank_Home_Ac;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A3;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A4;
    private DbsField annty_Actvty_Prap_View_Bank_Filler_4;
    private DbsField annty_Actvty_Prap_View_Oia_Indicator;
    private DbsField annty_Actvty_Prap_View_Erisa_Ind;
    private DbsField annty_Actvty_Prap_View_Cai_Ind;
    private DbsField annty_Actvty_Prap_View_Cip_Ind;
    private DbsField annty_Actvty_Prap_View_Same_Addr;
    private DbsField annty_Actvty_Prap_View_Omni_Issue_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Usage_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Init_Paymt_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_Init_Paymt_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_1;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_2;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_3;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_4;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_5;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Inst_Link_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Applcnt_Req_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Sync_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent;
    private DbsField annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Ppg_Team_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Model_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Divorce_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Phone_No;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Fmt;
    private DbsField annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Eft_Requested_Ind;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Divsub;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_1;

    private DbsGroup annty_Actvty_Prap_View__R_Field_4;
    private DbsField annty_Actvty_Prap_View_Ap_Plan_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_2;

    private DbsGroup annty_Actvty_Prap_View__R_Field_5;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Client_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Portfolio_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_3;
    private DbsField annty_Actvty_Prap_View_Ap_Replacement_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Register_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Exempt_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Autosave_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Max_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days;
    private DbsField annty_Actvty_Prap_View_Ap_Delete_User_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Delete_Reason_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Consent_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_Ann_Option;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Startdate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Enddate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Percentage;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postdays;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Limit;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postfreq;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Pl_Level;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Windowdays;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Recap_Prov;
    private DbsField annty_Actvty_Prap_View_Ap_Ann_Funding_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Conv_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Deceased_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier_2;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde_2;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct_2;
    private DbsField pnd_Cor_Universal_Data;

    private DbsGroup pnd_Cor_Universal_Data__R_Field_6;
    private DbsField pnd_Cor_Universal_Data_Pnd_Custodial_Agreement_Ind;
    private DbsField pnd_Cor_Universal_Data_Pnd_Aas_Ind;
    private DbsField pnd_Cor_Universal_Data_Pnd_Sungard_Navisys_Ind;
    private DbsField pnd_Cor_Universal_Data_Pnd_Multi_Source_Ind;
    private DbsField pnd_Cor_Universal_Data_Pnd_Tiaa_Mailed_Ind;
    private DbsField pnd_Cor_Universal_Data_Pnd_Cref_Mailed_Ind;
    private DbsField pnd_Cor_Universal_Data_Pnd_Omni_Acct_Iss_Ind;
    private DbsField pnd_Cor_Universal_Data_Pnd_Filler_143;
    private DbsField pnd_Ap_Stat_Dt_Deleted_Key;

    private DbsGroup pnd_Ap_Stat_Dt_Deleted_Key__R_Field_7;
    private DbsField pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Status_Ky;
    private DbsField pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Dt_Deleted_Ky;
    private DbsField pnd_Ap_Dlt_Date;

    private DbsGroup pnd_Ap_Dlt_Date__R_Field_8;
    private DbsField pnd_Ap_Dlt_Date_Pnd_Ap_Dlt_Dte;
    private DbsField pnd_Ap_Dlt_Dte_Mmddyyyy;

    private DbsGroup pnd_Ap_Dlt_Dte_Mmddyyyy__R_Field_9;
    private DbsField pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Mm;
    private DbsField pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Dd;
    private DbsField pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Yyyy;
    private DbsField pnd_Ap_Dlt_Dte_Yyyymmdd;

    private DbsGroup pnd_Ap_Dlt_Dte_Yyyymmdd__R_Field_10;
    private DbsField pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Yyyy;
    private DbsField pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Mm;
    private DbsField pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Dd;
    private DbsField pnd_Control_Dte;
    private DbsField pnd_Max_Control_Dte;
    private DbsField pnd_Release_Ind;
    private DbsField pnd_Pin_Id;
    private DbsField pnd_Cntrct_Found;
    private DbsField pnd_Reprint_Rec_Found;
    private DbsField pnd_Deleted_Info_Found;
    private DbsField pnd_W_Check_State;
    private DbsField pnd_St;
    private DbsField pnd_Zp;
    private DbsField pnd_Hold_State;
    private DbsField pnd_Hold_City_Zip;
    private DbsField pnd_Blank;
    private DbsField pnd_Icap_Tnt_Companion;
    private DbsField pnd_Annuity_Start_Date;

    private DbsGroup pnd_Annuity_Start_Date__R_Field_11;
    private DbsField pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N;
    private DbsField pnd_Rep_Status;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Cnt_Read;
    private DbsField pnd_Counters_Pnd_Cnt_Added;
    private DbsField pnd_Counters_Pnd_Cnt_Updated;
    private DbsField pnd_Counters_Pnd_Cnt_Bypassed;
    private DbsField pnd_V_Tiaa_Prfx;
    private DbsField pnd_V_Tiaa_No;
    private DbsField pnd_V_Cref_Prfx;
    private DbsField pnd_V_Cref_No;
    private DbsField pnd_V_Address_Table;
    private DbsField pnd_V_Pin_No;
    private DbsField pnd_V_Type_Cd;
    private DbsField pnd_V_Date_Table;

    private DbsGroup pnd_V_Date_Table__R_Field_12;
    private DbsField pnd_V_Date_Table_Pnd_V_Date_Table_N;
    private DbsField pnd_V_Single_Issue_Ind;
    private DbsField pnd_V_Effective_Date;

    private DbsGroup pnd_V_Effective_Date__R_Field_13;
    private DbsField pnd_V_Effective_Date_Pnd_V_Effective_Date_N;
    private DbsField pnd_Wk_Doi_A;

    private DbsGroup pnd_Wk_Doi_A__R_Field_14;
    private DbsField pnd_Wk_Doi_A_Pnd_Wk_Doi_N;
    private DbsField pnd_Rpt_Dtl;

    private DbsGroup pnd_Rpt_Dtl__R_Field_15;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Tiaa_No;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler1;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Cref_No;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler2;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Pin;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler3;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Ssn;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler4;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Plan;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler5;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Subplan;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler6;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Date;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler7;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Rsn;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler8;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Dlt_User;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Filler9;
    private DbsField pnd_Rpt_Dtl_Pnd_Rpt_Status;
    private DbsField pnd_Kount;
    private DbsField pnd_Rc;
    private DbsField pnd_Except_Reason;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma211 = new PdaMdma211(localVariables);
        ldaAppl180 = new LdaAppl180();
        registerRecord(ldaAppl180);
        registerRecord(ldaAppl180.getVw_acis_Reprint_Fl_View());
        ldaAppl170 = new LdaAppl170();
        registerRecord(ldaAppl170);
        pdaAppa100 = new PdaAppa100(localVariables);
        ldaY2datebw = new LdaY2datebw();
        registerRecord(ldaY2datebw);

        // Local Variables
        pnd_Wk_Control_File = localVariables.newFieldInRecord("pnd_Wk_Control_File", "#WK-CONTROL-FILE", FieldType.STRING, 50);

        pnd_Wk_Control_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Wk_Control_File__R_Field_1", "REDEFINE", pnd_Wk_Control_File);
        pnd_Wk_Control_File_Pnd_Wk_Control_Dte = pnd_Wk_Control_File__R_Field_1.newFieldInGroup("pnd_Wk_Control_File_Pnd_Wk_Control_Dte", "#WK-CONTROL-DTE", 
            FieldType.STRING, 8);

        pnd_Wk_Control_File__R_Field_2 = pnd_Wk_Control_File__R_Field_1.newGroupInGroup("pnd_Wk_Control_File__R_Field_2", "REDEFINE", pnd_Wk_Control_File_Pnd_Wk_Control_Dte);
        pnd_Wk_Control_File_Pnd_Wk_Control_Yyyymmdd = pnd_Wk_Control_File__R_Field_2.newFieldInGroup("pnd_Wk_Control_File_Pnd_Wk_Control_Yyyymmdd", "#WK-CONTROL-YYYYMMDD", 
            FieldType.NUMERIC, 8);

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_G_Key = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_G_KEY");
        annty_Actvty_Prap_View_Ap_G_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_G_IND");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Dob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_Bill_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bill_Code", "AP-BILL-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BILL_CODE");

        annty_Actvty_Prap_View_Ap_Tiaa_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_TIAA_CONTR", "AP-TIAA-CONTR");
        annty_Actvty_Prap_View_App_Pref = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Pref", "APP-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_PREF");
        annty_Actvty_Prap_View_App_Cont = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Cont", "APP-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_CONT");

        annty_Actvty_Prap_View_Ap_Cref_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_CREF_CONTR", "AP-CREF-CONTR");
        annty_Actvty_Prap_View_App_C_Pref = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Pref", "APP-C-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_C_PREF");
        annty_Actvty_Prap_View_App_C_Cont = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Cont", "APP-C-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_C_CONT");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Curr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_CURR");
        annty_Actvty_Prap_View_Ap_T_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Age_1st", "AP-T-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_T_AGE_1ST");
        annty_Actvty_Prap_View_Ap_C_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Age_1st", "AP-C-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_C_AGE_1ST");
        annty_Actvty_Prap_View_Ap_Ownership = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership", "AP-OWNERSHIP", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Sex = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sex", "AP-SEX", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SEX");
        annty_Actvty_Prap_View_Ap_Sex.setDdmHeader("SEX CDE");
        annty_Actvty_Prap_View_Ap_Mail_Zip = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Zip", "AP-MAIL-ZIP", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        annty_Actvty_Prap_View_Ap_Name_Addr_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NAME_ADDR_CD");
        annty_Actvty_Prap_View_Ap_Dt_Ent_Sys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        annty_Actvty_Prap_View_Ap_Dt_Released = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Released", "AP-DT-RELEASED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_RELEASED");
        annty_Actvty_Prap_View_Ap_Dt_Matched = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Matched", "AP-DT-MATCHED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_MATCHED");
        annty_Actvty_Prap_View_Ap_Dt_Deleted = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Deleted", "AP-DT-DELETED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_DELETED");
        annty_Actvty_Prap_View_Ap_Dt_Withdrawn = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Withdrawn", "AP-DT-WITHDRAWN", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_WITHDRAWN");
        annty_Actvty_Prap_View_Ap_Alloc_Discr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Discr", "AP-ALLOC-DISCR", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_ALLOC_DISCR");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Type", "AP-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_TYPE");
        annty_Actvty_Prap_View_Ap_Other_Pols = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Other_Pols", "AP-OTHER-POLS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OTHER_POLS");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Numb_Dailys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Numb_Dailys", "AP-NUMB-DAILYS", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_NUMB_DAILYS");
        annty_Actvty_Prap_View_Ap_Dt_App_Recvd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");
        annty_Actvty_Prap_View_Ap_App_Source = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_App_Source", "AP-APP-SOURCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APP_SOURCE");
        annty_Actvty_Prap_View_Ap_App_Source.setDdmHeader("SOURCE");
        annty_Actvty_Prap_View_Ap_Region_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Region_Code", "AP-REGION-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_REGION_CODE");
        annty_Actvty_Prap_View_Ap_Orig_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orig_Issue_State", 
            "AP-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Ownership_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP_TYPE");
        annty_Actvty_Prap_View_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_View_Ap_Split_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Split_Code", "AP-SPLIT-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SPLIT_CODE");

        annty_Actvty_Prap_View_Ap_Address_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Address_Info", 
            "AP-ADDRESS-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_Address_Line = annty_Actvty_Prap_View_Ap_Address_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Line", "AP-ADDRESS-LINE", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_City = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_City", "AP-CITY", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "AP_CITY");
        annty_Actvty_Prap_View_Ap_Current_State_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Current_State_Code", 
            "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd", 
            "AP-DANA-TRANSACTION-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_TRANSACTION_CD");
        annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd", 
            "AP-DANA-STNDRD-RTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_RTN_CD");
        annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd", 
            "AP-DANA-FINALIST-REASON-CD", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DANA_FINALIST_REASON_CD");
        annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code", 
            "AP-DANA-ADDR-STNDRD-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_ADDR_STNDRD_CODE");
        annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide", 
            "AP-DANA-STNDRD-OVERIDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_OVERIDE");
        annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields", 
            "AP-DANA-POSTAL-DATA-FIELDS", FieldType.STRING, 44, RepeatingFieldStrategy.None, "AP_DANA_POSTAL_DATA_FIELDS");
        annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code", 
            "AP-DANA-ADDR-GEOGRAPHIC-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_ADDR_GEOGRAPHIC_CODE");
        annty_Actvty_Prap_View_Ap_Annuity_Start_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Annuity_Start_Date", 
            "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");

        annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct", 
            "AP-MAXIMUM-ALLOC-PCT", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct", 
            "AP-MAX-ALLOC-PCT", FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign", 
            "AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Bene_Info_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Info_Type", "AP-BENE-INFO-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_INFO_TYPE");
        annty_Actvty_Prap_View_Ap_Primary_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Std_Ent", 
            "AP-PRIMARY-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Primary_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Primary_Info", 
            "AP-BENE-PRIMARY-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Primary_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Primary_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Bene_Info", 
            "AP-PRIMARY-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Std_Ent", 
            "AP-CONTINGENT-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Contingent_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Contingent_Info", 
            "AP-BENE-CONTINGENT-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Contingent_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Bene_Info", 
            "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Bene_Estate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Estate", "AP-BENE-ESTATE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_ESTATE");
        annty_Actvty_Prap_View_Ap_Bene_Trust = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Trust", "AP-BENE-TRUST", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_TRUST");
        annty_Actvty_Prap_View_Ap_Bene_Category = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Category", "AP-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_CATEGORY");
        annty_Actvty_Prap_View_Ap_Mail_Instructions = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Instructions", 
            "AP-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request", 
            "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        annty_Actvty_Prap_View_Ap_Process_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Process_Status", "AP-PROCESS-STATUS", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_PROCESS_STATUS");
        annty_Actvty_Prap_View_Ap_Coll_St_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_St_Cd", "AP-COLL-ST-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        annty_Actvty_Prap_View_Ap_Csm_Sec_Seg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Csm_Sec_Seg", "AP-CSM-SEC-SEG", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_CSM_SEC_SEG");
        annty_Actvty_Prap_View_Ap_Rlc_College = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_College", "AP-RLC-COLLEGE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_RLC_COLLEGE");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref", "AP-RLC-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_RLC_CREF_PREF");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont", "AP-RLC-CREF-CONT", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "AP_RLC_CREF_CONT");
        annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        annty_Actvty_Prap_View_Ap_Ph_Hist_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ph_Hist_Ind", "AP-PH-HIST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PH_HIST_IND");
        annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp", 
            "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        annty_Actvty_Prap_View_Ap_Contact_Mode = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contact_Mode", "AP-CONTACT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_CONTACT_MODE");
        annty_Actvty_Prap_View_Ap_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Sync_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sync_Ind", "AP-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SYNC_IND");
        annty_Actvty_Prap_View_Ap_Cor_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_COR_IND");
        annty_Actvty_Prap_View_Ap_Alloc_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Ind", "AP-ALLOC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOC_IND");
        annty_Actvty_Prap_View_Ap_Tiaa_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_View_Ap_Cref_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Doi", "AP-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");

        annty_Actvty_Prap_View_Ap_Mit_Request = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Mit_Request", "AP-MIT-REQUEST", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Unit = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Unit", "AP-MIT-UNIT", 
            FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Wpid = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Wpid", "AP-MIT-WPID", 
            FieldType.STRING, 6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Ira_Rollover_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Rollover_Type", 
            "AP-IRA-ROLLOVER-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_ROLLOVER_TYPE");
        annty_Actvty_Prap_View_Ap_Ira_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Record_Type", 
            "AP-IRA-RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Status", 
            "AP-MULT-APP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_STATUS");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type", 
            "AP-MULT-APP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Ppg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_MULT_APP_PPG");
        annty_Actvty_Prap_View_Ap_Print_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Print_Date", "AP-PRINT-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_PRINT_DATE");

        annty_Actvty_Prap_View_Ap_Cntrct_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Info", "AP-CNTRCT-INFO", 
            new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Type = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Type", "AP-CNTRCT-TYPE", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_TYPE", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Nbr = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Nbr", "AP-CNTRCT-NBR", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_NBR", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt", 
            "AP-CNTRCT-PROCEEDS-AMT", FieldType.NUMERIC, 10, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_PROCEEDS_AMT", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");

        annty_Actvty_Prap_View_Ap_Addr_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Addr_Info", "AP-ADDR-INFO", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Txt = annty_Actvty_Prap_View_Ap_Addr_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Txt", "AP-ADDRESS-TXT", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Dest_Name = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Dest_Name", 
            "AP-ADDRESS-DEST-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "AP_ADDRESS_DEST_NAME");
        annty_Actvty_Prap_View_Ap_Zip_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Zip_Code", "AP-ZIP-CODE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        annty_Actvty_Prap_View__R_Field_3 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_3", "REDEFINE", annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        annty_Actvty_Prap_View_Bank_Home_Ac = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_Ac", "BANK-HOME-AC", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A3 = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A3", "BANK-HOME-A3", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A4 = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A4", "BANK-HOME-A4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Bank_Filler_4 = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Bank_Filler_4", "BANK-FILLER-4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Oia_Indicator = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Oia_Indicator", "OIA-INDICATOR", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Erisa_Ind = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Erisa_Ind", "ERISA-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cai_Ind = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Cai_Ind", "CAI-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cip_Ind = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Cip_Ind", "CIP-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Same_Addr = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Same_Addr", "SAME-ADDR", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Omni_Issue_Ind = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Omni_Issue_Ind", "OMNI-ISSUE-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr", 
            "AP-BANK-ABA-ACCT-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_BANK_ABA_ACCT_NMBR");
        annty_Actvty_Prap_View_Ap_Addr_Usage_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Usage_Code", 
            "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_USAGE_CODE");
        annty_Actvty_Prap_View_Ap_Init_Paymt_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Init_Paymt_Amt", "AP-INIT-PAYMT-AMT", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_AMT");
        annty_Actvty_Prap_View_Ap_Init_Paymt_Pct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Init_Paymt_Pct", "AP-INIT-PAYMT-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_PCT");
        annty_Actvty_Prap_View_Ap_Financial_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_1", "AP-FINANCIAL-1", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_1");
        annty_Actvty_Prap_View_Ap_Financial_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_2", "AP-FINANCIAL-2", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_2");
        annty_Actvty_Prap_View_Ap_Financial_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_3", "AP-FINANCIAL-3", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_3");
        annty_Actvty_Prap_View_Ap_Financial_4 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_4", "AP-FINANCIAL-4", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_4");
        annty_Actvty_Prap_View_Ap_Financial_5 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_5", "AP-FINANCIAL-5", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_5");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde", 
            "AP-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_GRP_CDE");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_CDE");
        annty_Actvty_Prap_View_Ap_Inst_Link_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "AP_INST_LINK_CDE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Applcnt_Req_Type", 
            "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        annty_Actvty_Prap_View_Ap_Addr_Sync_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Sync_Ind", "AP-ADDR-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_SYNC_IND");
        annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent", 
            "AP-TIAA-SERVICE-AGENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIAA_SERVICE_AGENT");
        annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind", 
            "AP-PRAP-RSCH-MIT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PRAP_RSCH_MIT_IND");
        annty_Actvty_Prap_View_Ap_Ppg_Team_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_PPG_TEAM_CDE");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_RLC_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Allocation_Model_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Model_Type", 
            "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        annty_Actvty_Prap_View_Ap_Divorce_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Divorce_Ind", "AP-DIVORCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DIVORCE_IND");
        annty_Actvty_Prap_View_Ap_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Phone_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Phone_No", "AP-PHONE-NO", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "AP_PHONE_NO");
        annty_Actvty_Prap_View_Ap_Allocation_Fmt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOCATION_FMT");
        annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind", 
            "AP-E-SIGNED-APPL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        annty_Actvty_Prap_View_Ap_Eft_Requested_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eft_Requested_Ind", 
            "AP-EFT-REQUESTED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");

        annty_Actvty_Prap_View_Ap_Fund_Identifier = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier", 
            "AP-FUND-IDENTIFIER", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Fund_Cde = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde", "AP-FUND-CDE", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Allocation_Pct = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct", 
            "AP-ALLOCATION-PCT", FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        annty_Actvty_Prap_View_Ap_Sgrd_Divsub = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_SGRD_DIVSUB");
        annty_Actvty_Prap_View_Ap_Text_Udf_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_1", "AP-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_1");

        annty_Actvty_Prap_View__R_Field_4 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_4", "REDEFINE", annty_Actvty_Prap_View_Ap_Text_Udf_1);
        annty_Actvty_Prap_View_Ap_Plan_Issue_State = annty_Actvty_Prap_View__R_Field_4.newFieldInGroup("annty_Actvty_Prap_View_Ap_Plan_Issue_State", "AP-PLAN-ISSUE-STATE", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Ap_Text_Udf_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_2", "AP-TEXT-UDF-2", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_2");

        annty_Actvty_Prap_View__R_Field_5 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_5", "REDEFINE", annty_Actvty_Prap_View_Ap_Text_Udf_2);
        annty_Actvty_Prap_View_Ap_Sgrd_Client_Id = annty_Actvty_Prap_View__R_Field_5.newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Client_Id", "AP-SGRD-CLIENT-ID", 
            FieldType.STRING, 6);
        annty_Actvty_Prap_View_Ap_Portfolio_Type = annty_Actvty_Prap_View__R_Field_5.newFieldInGroup("annty_Actvty_Prap_View_Ap_Portfolio_Type", "AP-PORTFOLIO-TYPE", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Ap_Text_Udf_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_3", "AP-TEXT-UDF-3", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_3");
        annty_Actvty_Prap_View_Ap_Replacement_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Replacement_Ind", 
            "AP-REPLACEMENT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_REPLACEMENT_IND");
        annty_Actvty_Prap_View_Ap_Register_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Register_Id", "AP-REGISTER-ID", 
            FieldType.STRING, 11, RepeatingFieldStrategy.None, "AP_REGISTER_ID");
        annty_Actvty_Prap_View_Ap_Exempt_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Exempt_Ind", "AP-EXEMPT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EXEMPT_IND");
        annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind", 
            "AP-INCMPL-ACCT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_INCMPL_ACCT_IND");
        annty_Actvty_Prap_View_Ap_Autosave_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Autosave_Ind", "AP-AUTOSAVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AUTOSAVE_IND");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt", 
            "AP-AS-CUR-DFLT-OPT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_OPT");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt", 
            "AP-AS-CUR-DFLT-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_AMT");
        annty_Actvty_Prap_View_Ap_As_Incr_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Opt", "AP-AS-INCR-OPT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_INCR_OPT");
        annty_Actvty_Prap_View_Ap_As_Incr_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Amt", "AP-AS-INCR-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_INCR_AMT");
        annty_Actvty_Prap_View_Ap_As_Max_Pct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Max_Pct", "AP-AS-MAX-PCT", 
            FieldType.NUMERIC, 7, 2, RepeatingFieldStrategy.None, "AP_AS_MAX_PCT");
        annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days", 
            "AP-AE-OPT-OUT-DAYS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_AE_OPT_OUT_DAYS");
        annty_Actvty_Prap_View_Ap_Delete_User_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Delete_User_Id", "AP-DELETE-USER-ID", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "AP_DELETE_USER_ID");
        annty_Actvty_Prap_View_Ap_Delete_Reason_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Delete_Reason_Cd", 
            "AP-DELETE-REASON-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DELETE_REASON_CD");
        annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id", 
            "AP-AGENT-OR-RACF-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_AGENT_OR_RACF_ID");
        annty_Actvty_Prap_View_Ap_Consent_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Consent_Email_Address", 
            "AP-CONSENT-EMAIL-ADDRESS", FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_CONSENT_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind", 
            "AP-WELC-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_WELC_E_DELIVERY_IND");
        annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind.setDdmHeader("WELC EDLVRY IND");
        annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind", 
            "AP-LEGAL-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_E_DELIVERY_IND");
        annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind.setDdmHeader("LGL EDLVRY IND");
        annty_Actvty_Prap_View_Ap_Legal_Ann_Option = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_Ann_Option", 
            "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        annty_Actvty_Prap_View_Ap_Tic_Startdate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Startdate", "AP-TIC-STARTDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_STARTDATE");
        annty_Actvty_Prap_View_Ap_Tic_Enddate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Enddate", "AP-TIC-ENDDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_ENDDATE");
        annty_Actvty_Prap_View_Ap_Tic_Percentage = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Percentage", "AP-TIC-PERCENTAGE", 
            FieldType.NUMERIC, 15, 6, RepeatingFieldStrategy.None, "AP_TIC_PERCENTAGE");
        annty_Actvty_Prap_View_Ap_Tic_Postdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postdays", "AP-TIC-POSTDAYS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_TIC_POSTDAYS");
        annty_Actvty_Prap_View_Ap_Tic_Limit = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Limit", "AP-TIC-LIMIT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "AP_TIC_LIMIT");
        annty_Actvty_Prap_View_Ap_Tic_Postfreq = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postfreq", "AP-TIC-POSTFREQ", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_POSTFREQ");
        annty_Actvty_Prap_View_Ap_Tic_Pl_Level = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Pl_Level", "AP-TIC-PL-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_PL_LEVEL");
        annty_Actvty_Prap_View_Ap_Tic_Windowdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Windowdays", "AP-TIC-WINDOWDAYS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_WINDOWDAYS");
        annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow", 
            "AP-TIC-REQDLYWINDOW", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_REQDLYWINDOW");
        annty_Actvty_Prap_View_Ap_Tic_Recap_Prov = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Recap_Prov", "AP-TIC-RECAP-PROV", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_RECAP_PROV");
        annty_Actvty_Prap_View_Ap_Ann_Funding_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ann_Funding_Dt", "AP-ANN-FUNDING-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_ANN_FUNDING_DT");
        annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt", 
            "AP-TIAA-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIAA_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt", 
            "AP-CREF-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_CREF_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind", 
            "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        annty_Actvty_Prap_View_Ap_Conv_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Conv_Issue_State", 
            "AP-CONV-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CONV_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Deceased_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Deceased_Ind", "AP-DECEASED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DECEASED_IND");
        annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind", 
            "AP-NON-PROPRIETARY-PKG-IND", FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NON_PROPRIETARY_PKG_IND");
        annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1", 
            "AP-FUND-SOURCE-CDE-1", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_FUND_SOURCE_CDE_1");
        annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2", 
            "AP-FUND-SOURCE-CDE-2", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_FUND_SOURCE_CDE_2");

        annty_Actvty_Prap_View_Ap_Fund_Identifier_2 = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier_2", 
            "AP-FUND-IDENTIFIER-2", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        annty_Actvty_Prap_View_Ap_Fund_Cde_2 = annty_Actvty_Prap_View_Ap_Fund_Identifier_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde_2", "AP-FUND-CDE-2", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE_2", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        annty_Actvty_Prap_View_Ap_Allocation_Pct_2 = annty_Actvty_Prap_View_Ap_Fund_Identifier_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct_2", 
            "AP-ALLOCATION-PCT-2", FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT_2", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Cor_Universal_Data = localVariables.newFieldInRecord("pnd_Cor_Universal_Data", "#COR-UNIVERSAL-DATA", FieldType.STRING, 150);

        pnd_Cor_Universal_Data__R_Field_6 = localVariables.newGroupInRecord("pnd_Cor_Universal_Data__R_Field_6", "REDEFINE", pnd_Cor_Universal_Data);
        pnd_Cor_Universal_Data_Pnd_Custodial_Agreement_Ind = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Custodial_Agreement_Ind", 
            "#CUSTODIAL-AGREEMENT-IND", FieldType.STRING, 1);
        pnd_Cor_Universal_Data_Pnd_Aas_Ind = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Aas_Ind", "#AAS-IND", FieldType.STRING, 
            1);
        pnd_Cor_Universal_Data_Pnd_Sungard_Navisys_Ind = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Sungard_Navisys_Ind", 
            "#SUNGARD-NAVISYS-IND", FieldType.STRING, 1);
        pnd_Cor_Universal_Data_Pnd_Multi_Source_Ind = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Multi_Source_Ind", 
            "#MULTI-SOURCE-IND", FieldType.STRING, 1);
        pnd_Cor_Universal_Data_Pnd_Tiaa_Mailed_Ind = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Tiaa_Mailed_Ind", "#TIAA-MAILED-IND", 
            FieldType.STRING, 1);
        pnd_Cor_Universal_Data_Pnd_Cref_Mailed_Ind = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Cref_Mailed_Ind", "#CREF-MAILED-IND", 
            FieldType.STRING, 1);
        pnd_Cor_Universal_Data_Pnd_Omni_Acct_Iss_Ind = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Omni_Acct_Iss_Ind", 
            "#OMNI-ACCT-ISS-IND", FieldType.STRING, 1);
        pnd_Cor_Universal_Data_Pnd_Filler_143 = pnd_Cor_Universal_Data__R_Field_6.newFieldInGroup("pnd_Cor_Universal_Data_Pnd_Filler_143", "#FILLER-143", 
            FieldType.STRING, 143);
        pnd_Ap_Stat_Dt_Deleted_Key = localVariables.newFieldInRecord("pnd_Ap_Stat_Dt_Deleted_Key", "#AP-STAT-DT-DELETED-KEY", FieldType.STRING, 6);

        pnd_Ap_Stat_Dt_Deleted_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Ap_Stat_Dt_Deleted_Key__R_Field_7", "REDEFINE", pnd_Ap_Stat_Dt_Deleted_Key);
        pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Status_Ky = pnd_Ap_Stat_Dt_Deleted_Key__R_Field_7.newFieldInGroup("pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Status_Ky", 
            "#AP-STATUS-KY", FieldType.STRING, 1);
        pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Dt_Deleted_Ky = pnd_Ap_Stat_Dt_Deleted_Key__R_Field_7.newFieldInGroup("pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Dt_Deleted_Ky", 
            "#AP-DT-DELETED-KY", FieldType.STRING, 5);
        pnd_Ap_Dlt_Date = localVariables.newFieldInRecord("pnd_Ap_Dlt_Date", "#AP-DLT-DATE", FieldType.PACKED_DECIMAL, 8);

        pnd_Ap_Dlt_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Ap_Dlt_Date__R_Field_8", "REDEFINE", pnd_Ap_Dlt_Date);
        pnd_Ap_Dlt_Date_Pnd_Ap_Dlt_Dte = pnd_Ap_Dlt_Date__R_Field_8.newFieldInGroup("pnd_Ap_Dlt_Date_Pnd_Ap_Dlt_Dte", "#AP-DLT-DTE", FieldType.STRING, 
            5);
        pnd_Ap_Dlt_Dte_Mmddyyyy = localVariables.newFieldInRecord("pnd_Ap_Dlt_Dte_Mmddyyyy", "#AP-DLT-DTE-MMDDYYYY", FieldType.NUMERIC, 8);

        pnd_Ap_Dlt_Dte_Mmddyyyy__R_Field_9 = localVariables.newGroupInRecord("pnd_Ap_Dlt_Dte_Mmddyyyy__R_Field_9", "REDEFINE", pnd_Ap_Dlt_Dte_Mmddyyyy);
        pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Mm = pnd_Ap_Dlt_Dte_Mmddyyyy__R_Field_9.newFieldInGroup("pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Mm", "#AP-DLT-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Dd = pnd_Ap_Dlt_Dte_Mmddyyyy__R_Field_9.newFieldInGroup("pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Dd", "#AP-DLT-DD", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Yyyy = pnd_Ap_Dlt_Dte_Mmddyyyy__R_Field_9.newFieldInGroup("pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Yyyy", "#AP-DLT-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Ap_Dlt_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Ap_Dlt_Dte_Yyyymmdd", "#AP-DLT-DTE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Ap_Dlt_Dte_Yyyymmdd__R_Field_10 = localVariables.newGroupInRecord("pnd_Ap_Dlt_Dte_Yyyymmdd__R_Field_10", "REDEFINE", pnd_Ap_Dlt_Dte_Yyyymmdd);
        pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Yyyy = pnd_Ap_Dlt_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Yyyy", "#AP-DLT2-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Mm = pnd_Ap_Dlt_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Mm", "#AP-DLT2-MM", 
            FieldType.NUMERIC, 2);
        pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Dd = pnd_Ap_Dlt_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Dd", "#AP-DLT2-DD", 
            FieldType.NUMERIC, 2);
        pnd_Control_Dte = localVariables.newFieldInRecord("pnd_Control_Dte", "#CONTROL-DTE", FieldType.NUMERIC, 8);
        pnd_Max_Control_Dte = localVariables.newFieldInRecord("pnd_Max_Control_Dte", "#MAX-CONTROL-DTE", FieldType.NUMERIC, 8);
        pnd_Release_Ind = localVariables.newFieldInRecord("pnd_Release_Ind", "#RELEASE-IND", FieldType.NUMERIC, 1);
        pnd_Pin_Id = localVariables.newFieldInRecord("pnd_Pin_Id", "#PIN-ID", FieldType.NUMERIC, 12);
        pnd_Cntrct_Found = localVariables.newFieldInRecord("pnd_Cntrct_Found", "#CNTRCT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Reprint_Rec_Found = localVariables.newFieldInRecord("pnd_Reprint_Rec_Found", "#REPRINT-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Deleted_Info_Found = localVariables.newFieldInRecord("pnd_Deleted_Info_Found", "#DELETED-INFO-FOUND", FieldType.BOOLEAN, 1);
        pnd_W_Check_State = localVariables.newFieldInRecord("pnd_W_Check_State", "#W-CHECK-STATE", FieldType.STRING, 2);
        pnd_St = localVariables.newFieldInRecord("pnd_St", "#ST", FieldType.PACKED_DECIMAL, 2);
        pnd_Zp = localVariables.newFieldInRecord("pnd_Zp", "#ZP", FieldType.PACKED_DECIMAL, 1);
        pnd_Hold_State = localVariables.newFieldInRecord("pnd_Hold_State", "#HOLD-STATE", FieldType.STRING, 2);
        pnd_Hold_City_Zip = localVariables.newFieldInRecord("pnd_Hold_City_Zip", "#HOLD-CITY-ZIP", FieldType.STRING, 36);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.STRING, 35);
        pnd_Icap_Tnt_Companion = localVariables.newFieldInRecord("pnd_Icap_Tnt_Companion", "#ICAP-TNT-COMPANION", FieldType.STRING, 1);
        pnd_Annuity_Start_Date = localVariables.newFieldInRecord("pnd_Annuity_Start_Date", "#ANNUITY-START-DATE", FieldType.STRING, 8);

        pnd_Annuity_Start_Date__R_Field_11 = localVariables.newGroupInRecord("pnd_Annuity_Start_Date__R_Field_11", "REDEFINE", pnd_Annuity_Start_Date);
        pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N = pnd_Annuity_Start_Date__R_Field_11.newFieldInGroup("pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N", 
            "#ANNUITY-START-DATE-N", FieldType.NUMERIC, 8);
        pnd_Rep_Status = localVariables.newFieldInRecord("pnd_Rep_Status", "#REP-STATUS", FieldType.STRING, 30);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Cnt_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Cnt_Read", "#CNT-READ", FieldType.NUMERIC, 10);
        pnd_Counters_Pnd_Cnt_Added = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Cnt_Added", "#CNT-ADDED", FieldType.NUMERIC, 10);
        pnd_Counters_Pnd_Cnt_Updated = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Cnt_Updated", "#CNT-UPDATED", FieldType.NUMERIC, 10);
        pnd_Counters_Pnd_Cnt_Bypassed = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Cnt_Bypassed", "#CNT-BYPASSED", FieldType.NUMERIC, 10);
        pnd_V_Tiaa_Prfx = localVariables.newFieldInRecord("pnd_V_Tiaa_Prfx", "#V-TIAA-PRFX", FieldType.STRING, 1);
        pnd_V_Tiaa_No = localVariables.newFieldInRecord("pnd_V_Tiaa_No", "#V-TIAA-NO", FieldType.STRING, 7);
        pnd_V_Cref_Prfx = localVariables.newFieldInRecord("pnd_V_Cref_Prfx", "#V-CREF-PRFX", FieldType.STRING, 1);
        pnd_V_Cref_No = localVariables.newFieldInRecord("pnd_V_Cref_No", "#V-CREF-NO", FieldType.STRING, 7);
        pnd_V_Address_Table = localVariables.newFieldArrayInRecord("pnd_V_Address_Table", "#V-ADDRESS-TABLE", FieldType.STRING, 35, new DbsArrayController(1, 
            6));
        pnd_V_Pin_No = localVariables.newFieldInRecord("pnd_V_Pin_No", "#V-PIN-NO", FieldType.NUMERIC, 12);
        pnd_V_Type_Cd = localVariables.newFieldInRecord("pnd_V_Type_Cd", "#V-TYPE-CD", FieldType.STRING, 1);
        pnd_V_Date_Table = localVariables.newFieldArrayInRecord("pnd_V_Date_Table", "#V-DATE-TABLE", FieldType.STRING, 8, new DbsArrayController(1, 6));

        pnd_V_Date_Table__R_Field_12 = localVariables.newGroupInRecord("pnd_V_Date_Table__R_Field_12", "REDEFINE", pnd_V_Date_Table);
        pnd_V_Date_Table_Pnd_V_Date_Table_N = pnd_V_Date_Table__R_Field_12.newFieldArrayInGroup("pnd_V_Date_Table_Pnd_V_Date_Table_N", "#V-DATE-TABLE-N", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 6));
        pnd_V_Single_Issue_Ind = localVariables.newFieldInRecord("pnd_V_Single_Issue_Ind", "#V-SINGLE-ISSUE-IND", FieldType.STRING, 1);
        pnd_V_Effective_Date = localVariables.newFieldInRecord("pnd_V_Effective_Date", "#V-EFFECTIVE-DATE", FieldType.STRING, 8);

        pnd_V_Effective_Date__R_Field_13 = localVariables.newGroupInRecord("pnd_V_Effective_Date__R_Field_13", "REDEFINE", pnd_V_Effective_Date);
        pnd_V_Effective_Date_Pnd_V_Effective_Date_N = pnd_V_Effective_Date__R_Field_13.newFieldInGroup("pnd_V_Effective_Date_Pnd_V_Effective_Date_N", 
            "#V-EFFECTIVE-DATE-N", FieldType.NUMERIC, 8);
        pnd_Wk_Doi_A = localVariables.newFieldInRecord("pnd_Wk_Doi_A", "#WK-DOI-A", FieldType.STRING, 8);

        pnd_Wk_Doi_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Wk_Doi_A__R_Field_14", "REDEFINE", pnd_Wk_Doi_A);
        pnd_Wk_Doi_A_Pnd_Wk_Doi_N = pnd_Wk_Doi_A__R_Field_14.newFieldInGroup("pnd_Wk_Doi_A_Pnd_Wk_Doi_N", "#WK-DOI-N", FieldType.NUMERIC, 8);
        pnd_Rpt_Dtl = localVariables.newFieldInRecord("pnd_Rpt_Dtl", "#RPT-DTL", FieldType.STRING, 131);

        pnd_Rpt_Dtl__R_Field_15 = localVariables.newGroupInRecord("pnd_Rpt_Dtl__R_Field_15", "REDEFINE", pnd_Rpt_Dtl);
        pnd_Rpt_Dtl_Pnd_Rpt_Tiaa_No = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Tiaa_No", "#RPT-TIAA-NO", FieldType.STRING, 10);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler1 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler1", "#RPT-FILLER1", FieldType.STRING, 2);
        pnd_Rpt_Dtl_Pnd_Rpt_Cref_No = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Cref_No", "#RPT-CREF-NO", FieldType.STRING, 10);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler2 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler2", "#RPT-FILLER2", FieldType.STRING, 2);
        pnd_Rpt_Dtl_Pnd_Rpt_Pin = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Pin", "#RPT-PIN", FieldType.STRING, 12);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler3 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler3", "#RPT-FILLER3", FieldType.STRING, 2);
        pnd_Rpt_Dtl_Pnd_Rpt_Ssn = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Ssn", "#RPT-SSN", FieldType.STRING, 11);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler4 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler4", "#RPT-FILLER4", FieldType.STRING, 2);
        pnd_Rpt_Dtl_Pnd_Rpt_Plan = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Plan", "#RPT-PLAN", FieldType.STRING, 6);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler5 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler5", "#RPT-FILLER5", FieldType.STRING, 4);
        pnd_Rpt_Dtl_Pnd_Rpt_Subplan = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Subplan", "#RPT-SUBPLAN", FieldType.STRING, 6);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler6 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler6", "#RPT-FILLER6", FieldType.STRING, 3);
        pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Date = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Date", "#RPT-DLT-DATE", FieldType.STRING, 10);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler7 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler7", "#RPT-FILLER7", FieldType.STRING, 5);
        pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Rsn = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Rsn", "#RPT-DLT-RSN", FieldType.STRING, 1);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler8 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler8", "#RPT-FILLER8", FieldType.STRING, 5);
        pnd_Rpt_Dtl_Pnd_Rpt_Dlt_User = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Dlt_User", "#RPT-DLT-USER", FieldType.STRING, 8);
        pnd_Rpt_Dtl_Pnd_Rpt_Filler9 = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Filler9", "#RPT-FILLER9", FieldType.STRING, 2);
        pnd_Rpt_Dtl_Pnd_Rpt_Status = pnd_Rpt_Dtl__R_Field_15.newFieldInGroup("pnd_Rpt_Dtl_Pnd_Rpt_Status", "#RPT-STATUS", FieldType.STRING, 30);
        pnd_Kount = localVariables.newFieldInRecord("pnd_Kount", "#KOUNT", FieldType.INTEGER, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_Except_Reason = localVariables.newFieldInRecord("pnd_Except_Reason", "#EXCEPT-REASON", FieldType.STRING, 35);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        ldaAppl180.initializeValues();
        ldaAppl170.initializeValues();
        ldaY2datebw.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb1080() throws Exception
    {
        super("Appb1080");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB1080", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52;//Natural: FORMAT ( 2 ) LS = 133 PS = 52
        //*  APPL170
        ldaAppl170.getPnd_Work_Fields().resetInitial();                                                                                                                   //Natural: RESET INITIAL #WORK-FIELDS
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        ldaAppl170.getPnd_Current_Date_Yyyymmdd().setValue(Global.getDATN());                                                                                             //Natural: MOVE *DATN TO #CURRENT-DATE-YYYYMMDD #DATE-HOLD-YYYYMMDD
        ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(Global.getDATN());
        ldaAppl170.getPnd_Hold_Extract_Date().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Yymmdd());                                                                  //Natural: ASSIGN #HOLD-EXTRACT-DATE = #DATE-YYMMDD
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  GET THE CONTROL DATE
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WK-CONTROL-FILE
        while (condition(getWorkFiles().read(1, pnd_Wk_Control_File)))
        {
            if (condition(DbsUtil.maskMatches(pnd_Wk_Control_File_Pnd_Wk_Control_Dte,"YYYYMMDD")))                                                                        //Natural: IF #WK-CONTROL-DTE EQ MASK ( YYYYMMDD )
            {
                pnd_Control_Dte.setValue(pnd_Wk_Control_File_Pnd_Wk_Control_Yyyymmdd);                                                                                    //Natural: ASSIGN #CONTROL-DTE := #WK-CONTROL-YYYYMMDD
                pnd_Max_Control_Dte.setValue(pnd_Control_Dte);                                                                                                            //Natural: ASSIGN #MAX-CONTROL-DTE := #CONTROL-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CANNOT USE CONTROL DATE AS STARTING KEY SINCE
        pnd_Ap_Stat_Dt_Deleted_Key.reset();                                                                                                                               //Natural: RESET #AP-STAT-DT-DELETED-KEY
        pnd_Ap_Dlt_Date.setValue(0);                                                                                                                                      //Natural: ASSIGN #AP-DLT-DATE := 0
        //*                     DATA IS STORED IN ACIS AS MMDDYYYY
        pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Status_Ky.setValue("A");                                                                                                        //Natural: ASSIGN #AP-STATUS-KY := 'A'
        pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Dt_Deleted_Ky.setValue(pnd_Ap_Dlt_Date_Pnd_Ap_Dlt_Dte);                                                                         //Natural: ASSIGN #AP-DT-DELETED-KY := #AP-DLT-DTE
        //*  CNR
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        vw_annty_Actvty_Prap_View.startDatabaseRead                                                                                                                       //Natural: READ ANNTY-ACTVTY-PRAP-VIEW BY AP-STAT-DT-DELETED-KEY STARTING FROM #AP-STAT-DT-DELETED-KEY
        (
        "RD1",
        new Wc[] { new Wc("AP_STAT_DT_DELETED_KEY", ">=", pnd_Ap_Stat_Dt_Deleted_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("AP_STAT_DT_DELETED_KEY", "ASC") }
        );
        RD1:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("RD1")))
        {
            if (condition(annty_Actvty_Prap_View_Ap_Status.notEquals(pnd_Ap_Stat_Dt_Deleted_Key_Pnd_Ap_Status_Ky)))                                                       //Natural: IF AP-STATUS <> #AP-STATUS-KY
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  TRANSPOSE THE AP-DT-DELETED TO YYYYMMDD FORMAT
            pnd_Ap_Dlt_Dte_Mmddyyyy.setValue(annty_Actvty_Prap_View_Ap_Dt_Deleted);                                                                                       //Natural: ASSIGN #AP-DLT-DTE-MMDDYYYY := AP-DT-DELETED
            pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Yyyy.setValue(pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Yyyy);                                                                   //Natural: ASSIGN #AP-DLT2-YYYY := #AP-DLT-YYYY
            pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Mm.setValue(pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Mm);                                                                       //Natural: ASSIGN #AP-DLT2-MM := #AP-DLT-MM
            pnd_Ap_Dlt_Dte_Yyyymmdd_Pnd_Ap_Dlt2_Dd.setValue(pnd_Ap_Dlt_Dte_Mmddyyyy_Pnd_Ap_Dlt_Dd);                                                                       //Natural: ASSIGN #AP-DLT2-DD := #AP-DLT-DD
            if (condition(!(pnd_Ap_Dlt_Dte_Yyyymmdd.greater(pnd_Control_Dte) && annty_Actvty_Prap_View_Ap_Record_Type.equals(1))))                                        //Natural: ACCEPT IF #AP-DLT-DTE-YYYYMMDD > #CONTROL-DTE AND AP-RECORD-TYPE = 1
            {
                continue;
            }
            if (condition(pnd_Ap_Dlt_Dte_Yyyymmdd.greater(pnd_Max_Control_Dte)))                                                                                          //Natural: IF #AP-DLT-DTE-YYYYMMDD > #MAX-CONTROL-DTE
            {
                pnd_Max_Control_Dte.setValue(pnd_Ap_Dlt_Dte_Yyyymmdd);                                                                                                    //Natural: ASSIGN #MAX-CONTROL-DTE := #AP-DLT-DTE-YYYYMMDD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counters_Pnd_Cnt_Read.nadd(1);                                                                                                                            //Natural: ASSIGN #CNT-READ := #CNT-READ + 1
                                                                                                                                                                          //Natural: PERFORM STORE-REPRINT-DATA
            sub_Store_Reprint_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  WRITE THE CONTROL DATE ON THE CONTROL WORK FILE
        pnd_Wk_Control_File_Pnd_Wk_Control_Yyyymmdd.setValue(pnd_Max_Control_Dte);                                                                                        //Natural: ASSIGN #WK-CONTROL-YYYYMMDD := #MAX-CONTROL-DTE
        getWorkFiles().write(2, false, pnd_Wk_Control_File);                                                                                                              //Natural: WRITE WORK FILE 2 #WK-CONTROL-FILE
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        //*  DISPLAY CONTROL DATE READ AND MAX CONTROL DATE
        getReports().write(0, "Starting Control Date:",pnd_Control_Dte,NEWLINE,"Ending Control Date  :",pnd_Max_Control_Dte);                                             //Natural: WRITE 'Starting Control Date:' #CONTROL-DTE / 'Ending Control Date  :' #MAX-CONTROL-DTE
        if (Global.isEscape()) return;
        //*  CLOSE MQ.  CNR
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-REPRINT-DATA
        //*  TNGSUB END
        //*   END-IF
        //*  -------------------------------------------------------------
        //*  POPULATE ACIS-REPRINT-FL-VIEW
        //*  ADDING FOR TRANSFER-IN-CREDIT
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CONTRACT-TYPE
        //* **************************
        //* * DEFINE SUBROUTINE GET-PIN
        //* **************************
        //*  RESET #PIN-ID
        //*  #COR-PH-SSN-NBR     := AP-SOC-SEC
        //*  #COR-PH-SSN-REC-TYP := 1
        //*  #COR-PH-SSN-PUID    := 0
        //*  READ COR-XREF-PH BY COR-SUPER-SSN-RCDTYPE-PIN
        //*     STARTING FROM #COR-PH-SSN-KEY-BINRY
        //*   IF COR-XREF-PH.PH-SOCIAL-SECURITY-NO NE #COR-PH-SSN-NBR
        //*       OR
        //*       COR-XREF-PH.PH-RCD-TYPE-CDE NE #COR-PH-SSN-REC-TYP
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   IF COR-XREF-PH.PH-RLTNSHP-CDE  =  'X'
        //*     #PIN-ID := PH-XREF-PIN
        //*   ELSE
        //*     #PIN-ID := PH-UNIQUE-ID-NBR
        //*  END-IF
        //* *
        //*  PERFORM FIND-CNTRCT
        //* *
        //*  IF NOT #CNTRCT-FOUND
        //*    #PIN-ID := 0
        //*    ESCAPE ROUTINE
        //*  END-IF
        //*  END-READ
        //*  END-SUBROUTINE
        //* *****************************
        //*  DEFINE SUBROUTINE FIND-CNTRCT
        //* *****************************
        //*  #CNTRCT-FOUND := FALSE
        //*  #COR-CNTRCT-PIN-NBR := #PIN-ID
        //*  #COR-CNTRCT-REC-TYP := 2
        //* *
        //*  FIND COR-XREF-CNTRCT
        //*     WITH COR-SUPER-PIN-RCDTYPE = #COR-CNTRCT-KEY-BINRY
        //*   IF NO RECORDS FOUND
        //*     #PIN-ID := 0
        //*     ESCAPE BOTTOM
        //*   END-NOREC
        //*   IF COR-XREF-CNTRCT.CNTRCT-ISSUE-DTE = 0
        //*     ESCAPE TOP
        //*   END-IF
        //*   IF COR-XREF-CNTRCT.PH-UNIQUE-ID-NBR  NE #PIN-ID
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   IF COR-XREF-CNTRCT.CNTRCT-PAYEE-CDE NE '01'
        //*     ESCAPE TOP
        //*   END-IF
        //*   IF COR-XREF-CNTRCT.CNTRCT-LOB-CDE NE 'D'
        //*     ESCAPE TOP
        //*   END-IF
        //*   IF AP-COLL-CODE = 'SGRD'
        //*     IF COR-XREF-CNTRCT.CNTRCT-STATUS-CDE NE 'H' AND
        //*         COR-XREF-CNTRCT.CNTRCT-STATUS-CDE NE ' '
        //*       ESCAPE TOP
        //*     END-IF
        //*   ELSE
        //*     IF COR-XREF-CNTRCT.CNTRCT-STATUS-CDE NE ' '
        //*       ESCAPE TOP
        //*     END-IF
        //*   END-IF /* SGRD KG
        //*   #CNTRCT-FOUND := TRUE
        //*  END-FIND
        //*  END-SUBROUTINE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COR-FOR-COMPANION
        //* ***********************************
        //*  RESET #COR-CNTRCT-SP #COR-UNIVERSAL-DATA
        //*  #CNTRCT-NBR := AP-TIAA-CNTRCT
        //*  READ (1) COR BY COR-SUPER-CNTRCT-PAYEE-PIN FROM #COR-CNTRCT-SP
        //*   IF COR.CNTRCT-NBR > #CNTRCT-NBR
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   MOVE COR-UNIVERSAL-DATA TO #COR-UNIVERSAL-DATA
        //*   IF #TIAA-MAILED-IND NE 'Y'
        //*     MOVE 'Y' TO #V-SINGLE-ISSUE-IND
        //*   END-IF
        //*   IF #CREF-MAILED-IND NE 'Y'
        //*     MOVE 'Y' TO  #V-SINGLE-ISSUE-IND
        //*   END-IF
        //*  END-READ
        //*  START CNR  >>>
        //*  #MDMA210.#I-CONTRACT-NUMBER := AP-TIAA-CNTRCT
        //*  #MDMA210.#I-TIAA-CREF-IND   := 'T'
        //*  #MDMA210.#I-PAYEE-CODE := 01
        //* *******************************
        //* *********************************************************************
        //*  YR2000 COMPLIANT COPYCODE  ---> C.BROWN                            *
        //* *********************************************************************
        //*  Y2CHED SH CONVERTED FROM COBOL TO NATURAL --> E. DAVID 10/20/97
        //*                          Y 2 D A T E B P
        //* *******  BATCH DATE COMPARE ROUTINE FOR YR2000 PROCESSING  ******
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-3
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-4
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-5
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-3
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-4
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-5
        //* ********  END Y2K BATCH DATE-COMPARE PROCEDURE COPYCODE  ********
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER
        //*  'TIAA NO.    CREF NO.    PIN        SSN       PLAN NO.  SUBPLAN'-
        //*  '----------  ----------  -------  -----------  -------   -------'-
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-DTL
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Store_Reprint_Data() throws Exception                                                                                                                //Natural: STORE-REPRINT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        ldaAppl180.getVw_acis_Reprint_Fl_View().reset();                                                                                                                  //Natural: RESET ACIS-REPRINT-FL-VIEW #REP-STATUS
        pnd_Rep_Status.reset();
        pnd_Reprint_Rec_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #REPRINT-REC-FOUND := FALSE
        pnd_Deleted_Info_Found.setValue(false);                                                                                                                           //Natural: ASSIGN #DELETED-INFO-FOUND := FALSE
        ldaAppl180.getVw_acis_Reprint_Fl_View().startDatabaseFind                                                                                                         //Natural: FIND ACIS-REPRINT-FL-VIEW WITH RP-TIAA-CONTR = AP-TIAA-CNTRCT
        (
        "UPD2",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", annty_Actvty_Prap_View_Ap_Tiaa_Cntrct, WcType.WITH) }
        );
        UPD2:
        while (condition(ldaAppl180.getVw_acis_Reprint_Fl_View().readNextRow("UPD2")))
        {
            ldaAppl180.getVw_acis_Reprint_Fl_View().setIfNotFoundControlFlag(false);
            pnd_Reprint_Rec_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #REPRINT-REC-FOUND := TRUE
            if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Deleted().notEquals(getZero()) && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_User_Id().notEquals(" ")  //Natural: IF RP-DT-DELETED NE 0 AND RP-DELETE-USER-ID NE ' ' AND RP-DELETE-REASON-CD NE ' '
                && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_Reason_Cd().notEquals(" ")))
            {
                pnd_Deleted_Info_Found.setValue(true);                                                                                                                    //Natural: ASSIGN #DELETED-INFO-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Reprint_Rec_Found.getBoolean()))                                                                                                                //Natural: IF #REPRINT-REC-FOUND
        {
            UPD3:                                                                                                                                                         //Natural: GET ACIS-REPRINT-FL-VIEW *ISN ( UPD2. )
            ldaAppl180.getVw_acis_Reprint_Fl_View().readByID(ldaAppl180.getVw_acis_Reprint_Fl_View().getAstISN("UPD2"), "UPD3");
            //*  LPAO
            //*  BEGIN (JHU)
            if (condition(! (pnd_Deleted_Info_Found.getBoolean())))                                                                                                       //Natural: IF NOT #DELETED-INFO-FOUND
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Deleted().setValue(annty_Actvty_Prap_View_Ap_Dt_Deleted);                                                        //Natural: ASSIGN RP-DT-DELETED := AP-DT-DELETED
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_User_Id().setValue(annty_Actvty_Prap_View_Ap_Delete_User_Id);                                                //Natural: ASSIGN RP-DELETE-USER-ID := AP-DELETE-USER-ID
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_Reason_Cd().setValue(annty_Actvty_Prap_View_Ap_Delete_Reason_Cd);                                            //Natural: ASSIGN RP-DELETE-REASON-CD := AP-DELETE-REASON-CD
                pnd_Counters_Pnd_Cnt_Updated.nadd(1);                                                                                                                     //Natural: ASSIGN #CNT-UPDATED := #CNT-UPDATED + 1
                pnd_Rep_Status.setValue("RP RECORD UPDATED");                                                                                                             //Natural: ASSIGN #REP-STATUS := 'RP RECORD UPDATED'
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Ann_Option().setValue(annty_Actvty_Prap_View_Ap_Legal_Ann_Option);                                            //Natural: ASSIGN RP-LEGAL-ANN-OPTION := AP-LEGAL-ANN-OPTION
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind().setValue(annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind);                                      //Natural: ASSIGN RP-WELC-E-DELIVERY-IND := AP-WELC-E-DELIVERY-IND
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind().setValue(annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind);                                    //Natural: ASSIGN RP-LEGAL-E-DELIVERY-IND := AP-LEGAL-E-DELIVERY-IND
                if (condition(annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind.equals("E")))                                                                                 //Natural: IF AP-WELC-E-DELIVERY-IND = 'E'
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Consent_Email_Address);                                      //Natural: ASSIGN RP-EMAIL-ADDRESS := AP-CONSENT-EMAIL-ADDRESS
                }                                                                                                                                                         //Natural: END-IF
                if (condition(annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind.equals("E")))                                                                                //Natural: IF AP-LEGAL-E-DELIVERY-IND = 'E'
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Consent_Email_Address);                                      //Natural: ASSIGN RP-EMAIL-ADDRESS := AP-CONSENT-EMAIL-ADDRESS
                    //*  END   (JHU)
                }                                                                                                                                                         //Natural: END-IF
                //*  START LPAO
                if (condition(annty_Actvty_Prap_View_Ap_Ann_Funding_Dt.notEquals(getZero())))                                                                             //Natural: IF AP-ANN-FUNDING-DT NE 0
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ann_Funding_Dt().setValue(annty_Actvty_Prap_View_Ap_Ann_Funding_Dt);                                            //Natural: ASSIGN RP-ANN-FUNDING-DT := AP-ANN-FUNDING-DT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt.notEquals(getZero())))                                                                          //Natural: IF AP-TIAA-ANN-ISSUE-DT NE 0
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt);                                      //Natural: ASSIGN RP-TIAA-ANN-ISSUE-DT := AP-TIAA-ANN-ISSUE-DT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt.notEquals(getZero())))                                                                          //Natural: IF AP-CREF-ANN-ISSUE-DT NE 0
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt);                                      //Natural: ASSIGN RP-CREF-ANN-ISSUE-DT := AP-CREF-ANN-ISSUE-DT
                    //*  END LPAO
                }                                                                                                                                                         //Natural: END-IF
                //*  TNGSUB START
                if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.greater(" ")))                                                                          //Natural: IF AP-SUBSTITUTION-CONTRACT-IND > ' '
                {
                    pnd_Wk_Doi_A.reset();                                                                                                                                 //Natural: RESET #WK-DOI-A
                    if (condition(annty_Actvty_Prap_View_Ap_Tiaa_Doi.greater(getZero())))                                                                                 //Natural: IF AP-TIAA-DOI > 0
                    {
                        pnd_Wk_Doi_A.setValueEdited(annty_Actvty_Prap_View_Ap_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED AP-TIAA-DOI ( EM = YYYYMMDD ) TO #WK-DOI-A
                        ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi().setValue(pnd_Wk_Doi_A_Pnd_Wk_Doi_N);                                                                //Natural: MOVE #WK-DOI-N TO RP-T-DOI
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(annty_Actvty_Prap_View_Ap_Cref_Doi.greater(getZero())))                                                                                 //Natural: IF AP-CREF-DOI > 0
                    {
                        pnd_Wk_Doi_A.setValueEdited(annty_Actvty_Prap_View_Ap_Cref_Doi,new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED AP-CREF-DOI ( EM = YYYYMMDD ) TO #WK-DOI-A
                        ldaAppl180.getAcis_Reprint_Fl_View_Rp_C_Doi().setValue(pnd_Wk_Doi_A_Pnd_Wk_Doi_N);                                                                //Natural: MOVE #WK-DOI-N TO RP-C-DOI
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.greater(" ")))                                                                          //Natural: IF AP-SUBSTITUTION-CONTRACT-IND > ' '
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Substitution_Contract_Ind().setValue(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind);                      //Natural: MOVE AP-SUBSTITUTION-CONTRACT-IND TO RP-SUBSTITUTION-CONTRACT-IND
                }                                                                                                                                                         //Natural: END-IF
                if (condition(annty_Actvty_Prap_View_Ap_Conv_Issue_State.greater(" ")))                                                                                   //Natural: IF AP-CONV-ISSUE-STATE > ' '
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Conv_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Conv_Issue_State);                                        //Natural: MOVE AP-CONV-ISSUE-STATE TO RP-CONV-ISSUE-STATE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(annty_Actvty_Prap_View_Ap_Deceased_Ind.greater(" ")))                                                                                       //Natural: IF AP-DECEASED-IND > ' '
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Deceased_Ind().setValue(annty_Actvty_Prap_View_Ap_Deceased_Ind);                                                //Natural: MOVE AP-DECEASED-IND TO RP-DECEASED-IND
                }                                                                                                                                                         //Natural: END-IF
                ldaAppl180.getVw_acis_Reprint_Fl_View().updateDBRow("UPD3");                                                                                              //Natural: UPDATE ( UPD3. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Counters_Pnd_Cnt_Bypassed.nadd(1);                                                                                                                    //Natural: ASSIGN #CNT-BYPASSED := #CNT-BYPASSED + 1
                pnd_Rep_Status.setValue("RP RECORD BYPASSED-MARKED DLTD");                                                                                                //Natural: ASSIGN #REP-STATUS := 'RP RECORD BYPASSED-MARKED DLTD'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  DERIVE THE VALUES OF THE FOLLOWING DATA ELEMENTS USING THE SAME
            //*  LOGIC CONTAINED IN APPB100
            //*  1) RP-TIAA-NEGR-CONTRACT
            //*  2) RP-CREF-NEGR-CONTRACT
            //*  3) RP-ADDRESS-LINE
            //*  4) RP-PIN-NBR
            //*  5) RP-T-DOI
            //*  6) RP-C-DOI
            //*  7) RP-SINGLE-ISSUE-IND
            //*  8) RP-LS-EFFECTIVE-DATE
            //* *--------------------------------------------------------------
            //*  VARIOUS ROUTINES TO DERIVE THE FOLLOWING
            //*  1) RP-TIAA-NEGR-CONTRACT & 2) RP-CREF-NEGR-CONTRACT
            pnd_V_Tiaa_Prfx.reset();                                                                                                                                      //Natural: RESET #V-TIAA-PRFX #V-TIAA-NO #V-CREF-PRFX #V-CREF-NO
            pnd_V_Tiaa_No.reset();
            pnd_V_Cref_Prfx.reset();
            pnd_V_Cref_No.reset();
            if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("N")))                                                                                              //Natural: IF AP-APP-SOURCE = 'N'
            {
                pnd_V_Tiaa_Prfx.setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(4).getSubstring(1,1));                                                   //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 4 ) ,1,1 ) TO #V-TIAA-PRFX
                pnd_V_Tiaa_No.setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(4).getSubstring(2,7));                                                     //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 4 ) ,2,7 ) TO #V-TIAA-NO
                pnd_V_Cref_Prfx.setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(5).getSubstring(1,1));                                                   //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 5 ) ,1,1 ) TO #V-CREF-PRFX
                pnd_V_Cref_No.setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(5).getSubstring(2,7));                                                     //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 5 ) ,2,7 ) TO #V-CREF-NO
            }                                                                                                                                                             //Natural: END-IF
            //*  3) RP-ADDRESS-LINE
            pnd_W_Check_State.reset();                                                                                                                                    //Natural: RESET #W-CHECK-STATE #HOLD-STATE #V-ADDRESS-TABLE ( * )
            pnd_Hold_State.reset();
            pnd_V_Address_Table.getValue("*").reset();
            if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("S") || annty_Actvty_Prap_View_Ap_App_Source.equals("L")))                                          //Natural: IF AP-APP-SOURCE = 'S' OR = 'L'
            {
                pnd_W_Check_State.setValue(annty_Actvty_Prap_View_Ap_Orig_Issue_State);                                                                                   //Natural: ASSIGN #W-CHECK-STATE := AP-ORIG-ISSUE-STATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  OMNI
                //*  RA
                //*  SRA
                //*  IRA ROTH
                //*  IRA CLASS
                //*  IRA SEP
                //*  TIGR-IRA ROTH
                //*  TIGR-IRA CLASSIC
                //*  TIGR-IRA SEP
                if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.equals("SGRD") && ((annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("2"))  //Natural: IF AP-COLL-CODE = 'SGRD' AND ( ( AP-LOB = 'D' AND AP-LOB-TYPE = '2' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '2' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '3' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '4' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '6' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '7' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '8' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '9' ) )
                    || (annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("2")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") 
                    && annty_Actvty_Prap_View_Ap_Lob_Type.equals("3")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")) 
                    || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") 
                    && annty_Actvty_Prap_View_Ap_Lob_Type.equals("7")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")) 
                    || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))))
                {
                    pnd_W_Check_State.setValue(annty_Actvty_Prap_View_Ap_Orig_Issue_State);                                                                               //Natural: ASSIGN #W-CHECK-STATE := AP-ORIG-ISSUE-STATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_W_Check_State.setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                                                             //Natural: ASSIGN #W-CHECK-STATE := AP-CURRENT-STATE-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #ST 1 62
            for (pnd_St.setValue(1); condition(pnd_St.lessOrEqual(62)); pnd_St.nadd(1))
            {
                if (condition(pnd_W_Check_State.equals(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_St))))                                          //Natural: IF #W-CHECK-STATE = #STATE-CODE-N ( #ST )
                {
                    pnd_Hold_State.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_St));                                                      //Natural: MOVE #STATE-CODE-A ( #ST ) TO #HOLD-STATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Hold_City_Zip.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_City, ldaAppl170.getPnd_Work_Fields_Pnd_Comma(),         //Natural: COMPRESS AP-CITY #COMMA #HOLD-STATE INTO #HOLD-CITY-ZIP LEAVING NO
                pnd_Hold_State));
            pnd_V_Address_Table.getValue(1).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(1));                                                                 //Natural: MOVE AP-ADDRESS-LINE ( 1 ) TO #V-ADDRESS-TABLE ( 1 )
            pnd_V_Address_Table.getValue(2).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(2));                                                                 //Natural: MOVE AP-ADDRESS-LINE ( 2 ) TO #V-ADDRESS-TABLE ( 2 )
            pnd_V_Address_Table.getValue(3).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(3));                                                                 //Natural: MOVE AP-ADDRESS-LINE ( 3 ) TO #V-ADDRESS-TABLE ( 3 )
            pnd_V_Address_Table.getValue(4).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(4));                                                                 //Natural: MOVE AP-ADDRESS-LINE ( 4 ) TO #V-ADDRESS-TABLE ( 4 )
            pnd_V_Address_Table.getValue(5).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(5));                                                                 //Natural: MOVE AP-ADDRESS-LINE ( 5 ) TO #V-ADDRESS-TABLE ( 5 )
            DbsUtil.examine(new ExamineSource(pnd_V_Address_Table.getValue("*"),true), new ExamineSearch(pnd_Blank, true), new ExamineGivingIndex(pnd_Zp));               //Natural: EXAMINE FULL #V-ADDRESS-TABLE ( * ) FOR FULL VALUE OF #BLANK GIVING INDEX IN #ZP
            DbsUtil.examine(new ExamineSource(pnd_Hold_City_Zip,true), new ExamineSearch(","), new ExamineReplace(" "));                                                  //Natural: EXAMINE FULL #HOLD-CITY-ZIP ',' REPLACE WITH ' '
            pnd_V_Address_Table.getValue(pnd_Zp).setValue(pnd_Hold_City_Zip);                                                                                             //Natural: MOVE #HOLD-CITY-ZIP TO #V-ADDRESS-TABLE ( #ZP )
            //*  4) RP-PIN-NO
            pnd_V_Pin_No.reset();                                                                                                                                         //Natural: RESET #V-PIN-NO #PIN-ID
            pnd_Pin_Id.reset();
            //*   IF AP-APP-SOURCE = 'N'            /* CNR
            //*     PERFORM GET-PIN                 /* CNR
            //*   ELSE                              /* CNR
            pnd_Pin_Id.setValue(annty_Actvty_Prap_View_Ap_Pin_Nbr);                                                                                                       //Natural: MOVE AP-PIN-NBR TO #PIN-ID
            pnd_V_Pin_No.setValue(pnd_Pin_Id);                                                                                                                            //Natural: ASSIGN #V-PIN-NO := #PIN-ID
            //*  5) RP-T-DOI AND 6) RP-C-DOI
            pnd_V_Date_Table.getValue("*").reset();                                                                                                                       //Natural: RESET #V-DATE-TABLE ( * )
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CONTRACT-TYPE
            sub_Determine_Contract_Type();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RL")))                                                                                //Natural: IF #CONTRACT-TYPE = 'RL'
            {
                pnd_V_Type_Cd.setValue("1");                                                                                                                              //Natural: MOVE '1' TO #V-TYPE-CD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero())))                                                                                         //Natural: IF AP-T-DOI = 0
                {
                    pnd_V_Type_Cd.setValue("2");                                                                                                                          //Natural: MOVE '2' TO #V-TYPE-CD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(annty_Actvty_Prap_View_Ap_C_Doi.equals(getZero())))                                                                                     //Natural: IF AP-C-DOI = 0
                    {
                        pnd_V_Type_Cd.setValue("1");                                                                                                                      //Natural: MOVE '1' TO #V-TYPE-CD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(annty_Actvty_Prap_View_Ap_T_Doi.greater(getZero()) && annty_Actvty_Prap_View_Ap_C_Doi.greater(getZero())))                          //Natural: IF AP-T-DOI > 0 AND AP-C-DOI > 0
                        {
                            pnd_V_Type_Cd.setValue("3");                                                                                                                  //Natural: MOVE '3' TO #V-TYPE-CD
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().resetInitial();                                                                                        //Natural: RESET INITIAL #DATE-HOLD-YYYYMMDD
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(ldaAppl170.getPnd_Current_Date_Yyyymmdd());                                                   //Natural: MOVE #CURRENT-DATE-YYYYMMDD TO #DATE-HOLD-YYYYMMDD
            ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Hold_Mmyy().setValue(annty_Actvty_Prap_View_Ap_T_Doi);                                                                  //Natural: MOVE AP-T-DOI TO #DOI-HOLD-MMYY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Mm().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Mm());                                                             //Natural: MOVE #DOI-MM TO #DATE-MM
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Yy().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                             //Natural: MOVE #DOI-YY TO #DATE-YY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Dd().setValue(1);                                                                                                      //Natural: MOVE 01 TO #DATE-DD
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().reset();                                                                                           //Natural: RESET #Y2-YYMMDD-AREA1-A
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                    //Natural: MOVE #DOI-YY TO #Y2-YY-AREA1
                                                                                                                                                                          //Natural: PERFORM YR2000-DATE-COMPARE
            sub_Yr2000_Date_Compare();
            if (condition(Global.isEscape())) {return;}
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Cc().setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1());                                                   //Natural: MOVE #Y2-CC-AREA1 TO #DATE-CC
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Tiaa_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                            //Natural: MOVE #DATE-CCYYMM TO #PAR-TIAA-DOI
            pnd_V_Date_Table.getValue(1).setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd());                                                                //Natural: MOVE #DATE-HOLD-YYYYMMDD TO #V-DATE-TABLE ( 1 )
            ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Hold_Mmyy().setValue(annty_Actvty_Prap_View_Ap_C_Doi);                                                                  //Natural: MOVE AP-C-DOI TO #DOI-HOLD-MMYY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Mm().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Mm());                                                             //Natural: MOVE #DOI-MM TO #DATE-MM
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Yy().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                             //Natural: MOVE #DOI-YY TO #DATE-YY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Dd().setValue(1);                                                                                                      //Natural: MOVE 01 TO #DATE-DD
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().reset();                                                                                           //Natural: RESET #Y2-YYMMDD-AREA1-A
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                    //Natural: MOVE #DOI-YY TO #Y2-YY-AREA1
                                                                                                                                                                          //Natural: PERFORM YR2000-DATE-COMPARE
            sub_Yr2000_Date_Compare();
            if (condition(Global.isEscape())) {return;}
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Cc().setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1());                                                   //Natural: MOVE #Y2-CC-AREA1 TO #DATE-CC
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Cref_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                            //Natural: MOVE #DATE-CCYYMM TO #PAR-CREF-DOI
            pnd_V_Date_Table.getValue(2).setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd());                                                                //Natural: MOVE #DATE-HOLD-YYYYMMDD TO #V-DATE-TABLE ( 2 )
            //*  TNGSUB START
            if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.greater(" ")))                                                                              //Natural: IF AP-SUBSTITUTION-CONTRACT-IND > ' '
            {
                pnd_Wk_Doi_A.reset();                                                                                                                                     //Natural: RESET #WK-DOI-A
                if (condition(annty_Actvty_Prap_View_Ap_Tiaa_Doi.greater(getZero())))                                                                                     //Natural: IF AP-TIAA-DOI > 0
                {
                    pnd_Wk_Doi_A.setValueEdited(annty_Actvty_Prap_View_Ap_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED AP-TIAA-DOI ( EM = YYYYMMDD ) TO #WK-DOI-A
                    pnd_V_Date_Table.getValue(1).setValue(pnd_Wk_Doi_A_Pnd_Wk_Doi_N);                                                                                     //Natural: MOVE #WK-DOI-N TO #V-DATE-TABLE ( 1 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(annty_Actvty_Prap_View_Ap_Cref_Doi.greater(getZero())))                                                                                     //Natural: IF AP-CREF-DOI > 0
                {
                    pnd_Wk_Doi_A.setValueEdited(annty_Actvty_Prap_View_Ap_Cref_Doi,new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED AP-CREF-DOI ( EM = YYYYMMDD ) TO #WK-DOI-A
                    pnd_V_Date_Table.getValue(2).setValue(pnd_Wk_Doi_A_Pnd_Wk_Doi_N);                                                                                     //Natural: MOVE #WK-DOI-N TO #V-DATE-TABLE ( 2 )
                }                                                                                                                                                         //Natural: END-IF
                //*  TNGSUB END
            }                                                                                                                                                             //Natural: END-IF
            //*  TIAA ONLY
            if (condition(pnd_V_Type_Cd.equals("1")))                                                                                                                     //Natural: IF #V-TYPE-CD = '1'
            {
                pnd_V_Date_Table.getValue(2).moveAll("0");                                                                                                                //Natural: MOVE ALL '0' TO #V-DATE-TABLE ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  CREF ONLY
                if (condition(pnd_V_Type_Cd.equals("2")))                                                                                                                 //Natural: IF #V-TYPE-CD = '2'
                {
                    pnd_V_Date_Table.getValue(1).moveAll("0");                                                                                                            //Natural: MOVE ALL '0' TO #V-DATE-TABLE ( 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  7) RP-SINGLE-ISSUE-IND
            pnd_Cor_Universal_Data.reset();                                                                                                                               //Natural: RESET #COR-UNIVERSAL-DATA #ICAP-TNT-COMPANION
            pnd_Icap_Tnt_Companion.reset();
            pnd_V_Single_Issue_Ind.setValue("N");                                                                                                                         //Natural: MOVE 'N' TO #V-SINGLE-ISSUE-IND
            if (condition((annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero()) || annty_Actvty_Prap_View_Ap_C_Doi.equals(getZero()))))                                      //Natural: IF ( AP-T-DOI = 0 OR AP-C-DOI = 0 )
            {
                //*  ICAP
                //*  TNT
                //*  TNT
                //*  TNT
                //*  ICAP
                if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("A")) || (annty_Actvty_Prap_View_Ap_Lob.equals("D") //Natural: IF ( AP-LOB = 'D' AND AP-LOB-TYPE = 'A' ) OR ( AP-LOB = 'D' AND AP-LOB-TYPE = '5' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '5' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '6' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '7' )
                    && annty_Actvty_Prap_View_Ap_Lob_Type.equals("5")) || (annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("5")) 
                    || (annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")) || (annty_Actvty_Prap_View_Ap_Lob.equals("S") 
                    && annty_Actvty_Prap_View_Ap_Lob_Type.equals("7"))))
                {
                    pnd_Icap_Tnt_Companion.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #ICAP-TNT-COMPANION
                                                                                                                                                                          //Natural: PERFORM COR-FOR-COMPANION
                    sub_Cor_For_Companion();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  8) RP-LS-EFFECTIVE-DATE
            if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("L")))                                                                                              //Natural: IF AP-APP-SOURCE = 'L'
            {
                pnd_V_Effective_Date.setValueEdited(annty_Actvty_Prap_View_Ap_Print_Date,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED AP-PRINT-DATE ( EM = YYYYMMDD ) TO #V-EFFECTIVE-DATE
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Record_Type().setValue("1");                                                                                            //Natural: ASSIGN RP-RECORD-TYPE := '1'
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Contr().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct));          //Natural: COMPRESS AP-TIAA-CNTRCT INTO RP-TIAA-CONTR LEAVING NO SPACE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Contr().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Cref_Cert));            //Natural: COMPRESS AP-CREF-CERT INTO RP-CREF-CONTR LEAVING NO SPACE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_V_Tiaa_Prfx, pnd_V_Tiaa_No));         //Natural: COMPRESS #V-TIAA-PRFX #V-TIAA-NO INTO RP-TIAA-NEGR-CONTRACT LEAVING NO
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Negr_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_V_Cref_Prfx, pnd_V_Cref_No));         //Natural: COMPRESS #V-CREF-PRFX #V-CREF-NO INTO RP-CREF-NEGR-CONTRACT LEAVING NO
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob().setValue(annty_Actvty_Prap_View_Ap_Lob);                                                                          //Natural: ASSIGN RP-LOB := AP-LOB
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Lob_Type);                                                                //Natural: ASSIGN RP-LOB-TYPE := AP-LOB-TYPE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Bill_Code().setValue(annty_Actvty_Prap_View_Ap_Bill_Code);                                                              //Natural: ASSIGN RP-BILL-CODE := AP-BILL-CODE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Prfx_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme);                                                        //Natural: ASSIGN RP-COR-PRFX-NME := AP-COR-PRFX-NME
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Last_Nme);                                                        //Natural: ASSIGN RP-COR-LAST-NME := AP-COR-LAST-NME
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_First_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_First_Nme);                                                      //Natural: ASSIGN RP-COR-FIRST-NME := AP-COR-FIRST-NME
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme);                                                      //Natural: ASSIGN RP-COR-MDDLE-NME := AP-COR-MDDLE-NME
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme);                                                        //Natural: ASSIGN RP-COR-SFFX-NME := AP-COR-SFFX-NME
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(1).setValue(pnd_V_Address_Table.getValue(1));                                                   //Natural: ASSIGN RP-ADDRESS-LINE ( 1 ) := #V-ADDRESS-TABLE ( 1 )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(2).setValue(pnd_V_Address_Table.getValue(2));                                                   //Natural: ASSIGN RP-ADDRESS-LINE ( 2 ) := #V-ADDRESS-TABLE ( 2 )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(3).setValue(pnd_V_Address_Table.getValue(3));                                                   //Natural: ASSIGN RP-ADDRESS-LINE ( 3 ) := #V-ADDRESS-TABLE ( 3 )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(4).setValue(pnd_V_Address_Table.getValue(4));                                                   //Natural: ASSIGN RP-ADDRESS-LINE ( 4 ) := #V-ADDRESS-TABLE ( 4 )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(5).setValue(pnd_V_Address_Table.getValue(5));                                                   //Natural: ASSIGN RP-ADDRESS-LINE ( 5 ) := #V-ADDRESS-TABLE ( 5 )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Pin_Nbr().setValue(pnd_V_Pin_No);                                                                                       //Natural: ASSIGN RP-PIN-NBR := #V-PIN-NO
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Orig_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Orig_Issue_State);                                                //Natural: ASSIGN RP-ORIG-ISSUE-STATE := AP-ORIG-ISSUE-STATE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Current_State_Code().setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                            //Natural: ASSIGN RP-CURRENT-STATE-CODE := AP-CURRENT-STATE-CODE
            //*  OMNI
            if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.equals("SGRD")))                                                                                            //Natural: IF AP-COLL-CODE = 'SGRD'
            {
                //*  RA
                //*  SRA
                //*  IRA ROTH
                //*  IRA CLASS
                //*  IRA SEP
                //*  TIGR-IRA ROTH
                //*  TIGR-IRA CLASSIC
                //*  TIGR-IRA SEP
                if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("2")) || (annty_Actvty_Prap_View_Ap_Lob.equals("S") //Natural: IF ( AP-LOB = 'D' AND AP-LOB-TYPE = '2' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '2' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '3' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '4' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '6' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '7' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '8' ) OR ( AP-LOB = 'I' AND AP-LOB-TYPE = '9' )
                    && annty_Actvty_Prap_View_Ap_Lob_Type.equals("2")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("3")) 
                    || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") 
                    && annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("7")) 
                    || (annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")) || (annty_Actvty_Prap_View_Ap_Lob.equals("I") 
                    && annty_Actvty_Prap_View_Ap_Lob_Type.equals("9"))))
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St().setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                   //Natural: ASSIGN RP-ORIG-RESID-ISSUE-ST := AP-CURRENT-STATE-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St().setValue(" ");                                                                                //Natural: MOVE ' ' TO RP-ORIG-RESID-ISSUE-ST
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mail_Zip().setValue(annty_Actvty_Prap_View_Ap_Mail_Zip);                                                                //Natural: ASSIGN RP-MAIL-ZIP := AP-MAIL-ZIP
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mail_Instructions().setValue(annty_Actvty_Prap_View_Ap_Mail_Instructions);                                              //Natural: ASSIGN RP-MAIL-INSTRUCTIONS := AP-MAIL-INSTRUCTIONS
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Soc_Sec().setValue(annty_Actvty_Prap_View_Ap_Soc_Sec);                                                                  //Natural: ASSIGN RP-SOC-SEC := AP-SOC-SEC
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dob().setValue(annty_Actvty_Prap_View_Ap_Dob);                                                                          //Natural: ASSIGN RP-DOB := AP-DOB
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sex().setValue(annty_Actvty_Prap_View_Ap_Sex);                                                                          //Natural: ASSIGN RP-SEX := AP-SEX
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_Code().setValue(annty_Actvty_Prap_View_Ap_Coll_Code);                                                              //Natural: ASSIGN RP-COLL-CODE := AP-COLL-CODE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_St_Cd().setValue(annty_Actvty_Prap_View_Ap_Coll_St_Cd);                                                            //Natural: ASSIGN RP-COLL-ST-CD := AP-COLL-ST-CD
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde().setValue(annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde);                                              //Natural: ASSIGN RP-IRC-SECTN-GRP-CDE := AP-IRC-SECTN-GRP-CDE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Irc_Sectn_Cde().setValue(annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde);                                                      //Natural: ASSIGN RP-IRC-SECTN-CDE := AP-IRC-SECTN-CDE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde().setValue(annty_Actvty_Prap_View_Ap_Ppg_Team_Cde);                                                        //Natural: ASSIGN RP-PPG-TEAM-CDE := AP-PPG-TEAM-CDE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Inst_Link_Cde().setValue(annty_Actvty_Prap_View_Ap_Inst_Link_Cde);                                                      //Natural: ASSIGN RP-INST-LINK-CDE := AP-INST-LINK-CDE
            if (condition(pnd_V_Date_Table.getValue(1).greater("0")))                                                                                                     //Natural: IF #V-DATE-TABLE ( 1 ) > '0'
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi().setValue(pnd_V_Date_Table_Pnd_V_Date_Table_N.getValue(1));                                                  //Natural: ASSIGN RP-T-DOI := #V-DATE-TABLE-N ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Icap_Tnt_Companion.equals("Y")))                                                                                                        //Natural: IF #ICAP-TNT-COMPANION = 'Y'
                {
                    if (condition(pnd_Cor_Universal_Data_Pnd_Tiaa_Mailed_Ind.equals("Y") && pnd_Cor_Universal_Data_Pnd_Cref_Mailed_Ind.equals("Y")))                      //Natural: IF #TIAA-MAILED-IND = 'Y' AND #CREF-MAILED-IND = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi().setValue(0);                                                                                        //Natural: ASSIGN RP-T-DOI := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_V_Date_Table.getValue(2).greater("0")))                                                                                                     //Natural: IF #V-DATE-TABLE ( 2 ) > '0'
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_C_Doi().setValue(pnd_V_Date_Table_Pnd_V_Date_Table_N.getValue(2));                                                  //Natural: ASSIGN RP-C-DOI := #V-DATE-TABLE-N ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Icap_Tnt_Companion.equals("Y")))                                                                                                        //Natural: IF #ICAP-TNT-COMPANION = 'Y'
                {
                    if (condition(pnd_Cor_Universal_Data_Pnd_Tiaa_Mailed_Ind.equals("Y") && pnd_Cor_Universal_Data_Pnd_Cref_Mailed_Ind.equals("Y")))                      //Natural: IF #TIAA-MAILED-IND = 'Y' AND #CREF-MAILED-IND = 'Y'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAppl180.getAcis_Reprint_Fl_View_Rp_C_Doi().setValue(0);                                                                                        //Natural: ASSIGN RP-C-DOI := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ONEIRA2
                //*  ONEIRA2
                //*  IISG
                //*  IISG
                //*  IISG
                //*  IISG
            }                                                                                                                                                             //Natural: END-IF
            pnd_Annuity_Start_Date.setValue(annty_Actvty_Prap_View_Ap_Annuity_Start_Date);                                                                                //Natural: ASSIGN #ANNUITY-START-DATE := AP-ANNUITY-START-DATE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Annuity_Start_Date().setValue(pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N);                                         //Natural: ASSIGN RP-ANNUITY-START-DATE := #ANNUITY-START-DATE-N
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Released().setValue(annty_Actvty_Prap_View_Ap_Dt_Released);                                                          //Natural: ASSIGN RP-DT-RELEASED := AP-DT-RELEASED
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_App_Recvd().setValue(annty_Actvty_Prap_View_Ap_Dt_App_Recvd);                                                        //Natural: ASSIGN RP-DT-APP-RECVD := AP-DT-APP-RECVD
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Extract_Date().setValue(Global.getDATN());                                                                              //Natural: ASSIGN RP-EXTRACT-DATE := *DATN
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Curr().setValue(annty_Actvty_Prap_View_Ap_Curr);                                                                        //Natural: ASSIGN RP-CURR := AP-CURR
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Age_1st().setValue(annty_Actvty_Prap_View_Ap_T_Age_1st);                                                              //Natural: ASSIGN RP-T-AGE-1ST := AP-T-AGE-1ST
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ownership().setValue(annty_Actvty_Prap_View_Ap_Ownership);                                                              //Natural: ASSIGN RP-OWNERSHIP := AP-OWNERSHIP
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Alloc_Discr().setValue(annty_Actvty_Prap_View_Ap_Alloc_Discr);                                                          //Natural: ASSIGN RP-ALLOC-DISCR := AP-ALLOC-DISCR
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Fund_Cde().getValue("*").setValue(annty_Actvty_Prap_View_Ap_Fund_Cde.getValue("*"));                                    //Natural: ASSIGN RP-FUND-CDE ( * ) := AP-FUND-CDE ( * )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_App_Source().setValue(annty_Actvty_Prap_View_Ap_App_Source);                                                            //Natural: ASSIGN RP-APP-SOURCE := AP-APP-SOURCE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Region_Code().setValue(annty_Actvty_Prap_View_Ap_Region_Code);                                                          //Natural: ASSIGN RP-REGION-CODE := AP-REGION-CODE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request().setValue(annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request);                                      //Natural: ASSIGN RP-EOP-ADDL-CREF-REQUEST := AP-EOP-ADDL-CREF-REQUEST
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Rlc_Cref_Cert().setValue(annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert);                                                      //Natural: ASSIGN RP-RLC-CREF-CERT := AP-RLC-CREF-CERT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ira_Rollover_Type().setValue(annty_Actvty_Prap_View_Ap_Ira_Rollover_Type);                                              //Natural: ASSIGN RP-IRA-ROLLOVER-TYPE := AP-IRA-ROLLOVER-TYPE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ira_Record_Type().setValue(annty_Actvty_Prap_View_Ap_Ira_Record_Type);                                                  //Natural: ASSIGN RP-IRA-RECORD-TYPE := AP-IRA-RECORD-TYPE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Status().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Status);                                                  //Natural: ASSIGN RP-MULT-APP-STATUS := AP-MULT-APP-STATUS
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Lob().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Lob);                                                        //Natural: ASSIGN RP-MULT-APP-LOB := AP-MULT-APP-LOB
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type);                                              //Natural: ASSIGN RP-MULT-APP-LOB-TYPE := AP-MULT-APP-LOB-TYPE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Ppg().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Ppg);                                                        //Natural: ASSIGN RP-MULT-APP-PPG := AP-MULT-APP-PPG
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_1().setValue(annty_Actvty_Prap_View_Ap_Financial_1);                                                          //Natural: ASSIGN RP-FINANCIAL-1 := AP-FINANCIAL-1
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_2().setValue(annty_Actvty_Prap_View_Ap_Financial_2);                                                          //Natural: ASSIGN RP-FINANCIAL-2 := AP-FINANCIAL-2
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_3().setValue(annty_Actvty_Prap_View_Ap_Financial_3);                                                          //Natural: ASSIGN RP-FINANCIAL-3 := AP-FINANCIAL-3
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Applcnt_Req_Type().setValue(annty_Actvty_Prap_View_Ap_Applcnt_Req_Type);                                                //Natural: ASSIGN RP-APPLCNT-REQ-TYPE := AP-APPLCNT-REQ-TYPE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Service_Agent().setValue(annty_Actvty_Prap_View_Omni_Issue_Ind);                                                   //Natural: ASSIGN RP-TIAA-SERVICE-AGENT := OMNI-ISSUE-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Racf_Id().setValue(annty_Actvty_Prap_View_Ap_Racf_Id);                                                                  //Natural: ASSIGN RP-RACF-ID := AP-RACF-ID
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Model().setValue(annty_Actvty_Prap_View_Ap_Allocation_Model_Type);                                           //Natural: ASSIGN RP-ALLOCATION-MODEL := AP-ALLOCATION-MODEL-TYPE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Divorce_Ind().setValue(annty_Actvty_Prap_View_Ap_Divorce_Ind);                                                          //Natural: ASSIGN RP-DIVORCE-IND := AP-DIVORCE-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Email_Address);                                                      //Natural: ASSIGN RP-EMAIL-ADDRESS := AP-EMAIL-ADDRESS
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_E_Signed_Appl_Ind().setValue(annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind);                                              //Natural: ASSIGN RP-E-SIGNED-APPL-IND := AP-E-SIGNED-APPL-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Eft_Request_Ind().setValue(annty_Actvty_Prap_View_Ap_Eft_Requested_Ind);                                                //Natural: ASSIGN RP-EFT-REQUEST-IND := AP-EFT-REQUESTED-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Participant_Status_Ind().setValue("N");                                                                                 //Natural: ASSIGN RP-PARTICIPANT-STATUS-IND := 'N'
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("P");                                                                                      //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'P'
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Single_Issue_Ind().setValue(pnd_V_Single_Issue_Ind);                                                                    //Natural: ASSIGN RP-SINGLE-ISSUE-IND := #V-SINGLE-ISSUE-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Divsub().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Divsub);                                                          //Natural: ASSIGN RP-SGRD-DIVSUB := AP-SGRD-DIVSUB
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Fmt().setValue(annty_Actvty_Prap_View_Ap_Allocation_Fmt);                                                    //Natural: ASSIGN RP-ALLOCATION-FMT := AP-ALLOCATION-FMT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Phone_No().setValue(annty_Actvty_Prap_View_Ap_Phone_No);                                                                //Natural: ASSIGN RP-PHONE-NO := AP-PHONE-NO
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Source_Cde_1().setValue(annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1);                                                   //Natural: ASSIGN RP-SOURCE-CDE-1 := AP-FUND-SOURCE-CDE-1
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Fund_Cde().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde.getValue(1,":",100));                        //Natural: ASSIGN RP-FUND-CDE ( 1:100 ) := AP-FUND-CDE ( 1:100 )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Pct().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Allocation_Pct.getValue(1,":",                  //Natural: ASSIGN RP-ALLOCATION-PCT ( 1:100 ) := AP-ALLOCATION-PCT ( 1:100 )
                100));
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Source_Cde_2().setValue(annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2);                                                   //Natural: ASSIGN RP-SOURCE-CDE-2 := AP-FUND-SOURCE-CDE-2
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Fund_Cde_2().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde_2.getValue(1,":",100));                    //Natural: ASSIGN RP-FUND-CDE-2 ( 1:100 ) := AP-FUND-CDE-2 ( 1:100 )
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Pct_2().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Allocation_Pct_2.getValue(1,                  //Natural: ASSIGN RP-ALLOCATION-PCT-2 ( 1:100 ) := AP-ALLOCATION-PCT-2 ( 1:100 )
                ":",100));
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Plan_No().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                        //Natural: ASSIGN RP-SGRD-PLAN-NO := AP-SGRD-PLAN-NO
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Subplan_No().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                  //Natural: ASSIGN RP-SGRD-SUBPLAN-NO := AP-SGRD-SUBPLAN-NO
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Part_Ext().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext);                                                      //Natural: ASSIGN RP-SGRD-PART-EXT := AP-SGRD-PART-EXT
            if (condition(pnd_V_Effective_Date_Pnd_V_Effective_Date_N.greater(getZero())))                                                                                //Natural: IF #V-EFFECTIVE-DATE-N > 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ls_Effective_Date().setValue(pnd_V_Effective_Date_Pnd_V_Effective_Date_N);                                          //Natural: ASSIGN RP-LS-EFFECTIVE-DATE := #V-EFFECTIVE-DATE-N
                //*  BE2.
                //*  BEGIN (JHU)
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Interest_Rate().setValue(0);                                                                                            //Natural: ASSIGN RP-INTEREST-RATE := 0
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Replacement_Ind().setValue(annty_Actvty_Prap_View_Ap_Replacement_Ind);                                                  //Natural: ASSIGN RP-REPLACEMENT-IND := AP-REPLACEMENT-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Register_Id().setValue(annty_Actvty_Prap_View_Ap_Register_Id);                                                          //Natural: ASSIGN RP-REGISTER-ID := AP-REGISTER-ID
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Exempt_Ind().setValue(annty_Actvty_Prap_View_Ap_Exempt_Ind);                                                            //Natural: ASSIGN RP-EXEMPT-IND := AP-EXEMPT-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Plan_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Plan_Issue_State);                                                //Natural: ASSIGN RP-PLAN-ISSUE-STATE := AP-PLAN-ISSUE-STATE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Client_Id().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Client_Id);                                                    //Natural: ASSIGN RP-SGRD-CLIENT-ID := AP-SGRD-CLIENT-ID
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Portfolio_Type().setValue(annty_Actvty_Prap_View_Ap_Portfolio_Type);                                                    //Natural: ASSIGN RP-PORTFOLIO-TYPE := AP-PORTFOLIO-TYPE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Incmpl_Acct_Ind().setValue(annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind);                                                  //Natural: ASSIGN RP-INCMPL-ACCT-IND := AP-INCMPL-ACCT-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Autosave_Ind().setValue(annty_Actvty_Prap_View_Ap_Autosave_Ind);                                                        //Natural: ASSIGN RP-AUTOSAVE-IND := AP-AUTOSAVE-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Cur_Dflt_Opt().setValue(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt);                                                  //Natural: ASSIGN RP-AS-CUR-DFLT-OPT := AP-AS-CUR-DFLT-OPT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Cur_Dflt_Amt().setValue(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt);                                                  //Natural: ASSIGN RP-AS-CUR-DFLT-AMT := AP-AS-CUR-DFLT-AMT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Incr_Opt().setValue(annty_Actvty_Prap_View_Ap_As_Incr_Opt);                                                          //Natural: ASSIGN RP-AS-INCR-OPT := AP-AS-INCR-OPT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Incr_Amt().setValue(annty_Actvty_Prap_View_Ap_As_Incr_Amt);                                                          //Natural: ASSIGN RP-AS-INCR-AMT := AP-AS-INCR-AMT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Max_Pct().setValue(annty_Actvty_Prap_View_Ap_As_Max_Pct);                                                            //Natural: ASSIGN RP-AS-MAX-PCT := AP-AS-MAX-PCT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ae_Opt_Out_Days().setValue(annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days);                                                  //Natural: ASSIGN RP-AE-OPT-OUT-DAYS := AP-AE-OPT-OUT-DAYS
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Deleted().setValue(annty_Actvty_Prap_View_Ap_Dt_Deleted);                                                            //Natural: ASSIGN RP-DT-DELETED := AP-DT-DELETED
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_User_Id().setValue(annty_Actvty_Prap_View_Ap_Delete_User_Id);                                                    //Natural: ASSIGN RP-DELETE-USER-ID := AP-DELETE-USER-ID
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_Reason_Cd().setValue(annty_Actvty_Prap_View_Ap_Delete_Reason_Cd);                                                //Natural: ASSIGN RP-DELETE-REASON-CD := AP-DELETE-REASON-CD
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Agent_Crd_No().setValue(annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id);                                                    //Natural: ASSIGN RP-AGENT-CRD-NO := AP-AGENT-OR-RACF-ID
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Welc_E_Delivery_Ind().setValue(annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind);                                          //Natural: ASSIGN RP-WELC-E-DELIVERY-IND := AP-WELC-E-DELIVERY-IND
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_E_Delivery_Ind().setValue(annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind);                                        //Natural: ASSIGN RP-LEGAL-E-DELIVERY-IND := AP-LEGAL-E-DELIVERY-IND
            if (condition(annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind.equals("E")))                                                                                     //Natural: IF AP-WELC-E-DELIVERY-IND = 'E'
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Consent_Email_Address);                                          //Natural: ASSIGN RP-EMAIL-ADDRESS := AP-CONSENT-EMAIL-ADDRESS
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind.equals("E")))                                                                                    //Natural: IF AP-LEGAL-E-DELIVERY-IND = 'E'
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Consent_Email_Address);                                          //Natural: ASSIGN RP-EMAIL-ADDRESS := AP-CONSENT-EMAIL-ADDRESS
                //*  END   (JHU)
                //*   >>> LS1
                //*  <<< LS1
                //*  START LPAO
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Startdate().setValue(annty_Actvty_Prap_View_Ap_Tic_Startdate);                                                      //Natural: ASSIGN RP-TIC-STARTDATE := AP-TIC-STARTDATE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Enddate().setValue(annty_Actvty_Prap_View_Ap_Tic_Enddate);                                                          //Natural: ASSIGN RP-TIC-ENDDATE := AP-TIC-ENDDATE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Percentage().setValue(annty_Actvty_Prap_View_Ap_Tic_Percentage);                                                    //Natural: ASSIGN RP-TIC-PERCENTAGE := AP-TIC-PERCENTAGE
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Postdays().setValue(annty_Actvty_Prap_View_Ap_Tic_Postdays);                                                        //Natural: ASSIGN RP-TIC-POSTDAYS := AP-TIC-POSTDAYS
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Limit().setValue(annty_Actvty_Prap_View_Ap_Tic_Limit);                                                              //Natural: ASSIGN RP-TIC-LIMIT := AP-TIC-LIMIT
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Postfreq().setValue(annty_Actvty_Prap_View_Ap_Tic_Postfreq);                                                        //Natural: ASSIGN RP-TIC-POSTFREQ := AP-TIC-POSTFREQ
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Pl_Level().setValue(annty_Actvty_Prap_View_Ap_Tic_Pl_Level);                                                        //Natural: ASSIGN RP-TIC-PL-LEVEL := AP-TIC-PL-LEVEL
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Windowdays().setValue(annty_Actvty_Prap_View_Ap_Tic_Windowdays);                                                    //Natural: ASSIGN RP-TIC-WINDOWDAYS := AP-TIC-WINDOWDAYS
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Reqdlywindow().setValue(annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow);                                                //Natural: ASSIGN RP-TIC-REQDLYWINDOW := AP-TIC-REQDLYWINDOW
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Recap_Prov().setValue(annty_Actvty_Prap_View_Ap_Tic_Recap_Prov);                                                    //Natural: ASSIGN RP-TIC-RECAP-PROV := AP-TIC-RECAP-PROV
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Ann_Option().setValue(annty_Actvty_Prap_View_Ap_Legal_Ann_Option);                                                //Natural: ASSIGN RP-LEGAL-ANN-OPTION := AP-LEGAL-ANN-OPTION
            if (condition(annty_Actvty_Prap_View_Ap_Ann_Funding_Dt.notEquals(getZero())))                                                                                 //Natural: IF AP-ANN-FUNDING-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ann_Funding_Dt().setValue(annty_Actvty_Prap_View_Ap_Ann_Funding_Dt);                                                //Natural: ASSIGN RP-ANN-FUNDING-DT := AP-ANN-FUNDING-DT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt.notEquals(getZero())))                                                                              //Natural: IF AP-TIAA-ANN-ISSUE-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt);                                          //Natural: ASSIGN RP-TIAA-ANN-ISSUE-DT := AP-TIAA-ANN-ISSUE-DT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt.notEquals(getZero())))                                                                              //Natural: IF AP-CREF-ANN-ISSUE-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt);                                          //Natural: ASSIGN RP-CREF-ANN-ISSUE-DT := AP-CREF-ANN-ISSUE-DT
                //*  END LPAO
            }                                                                                                                                                             //Natural: END-IF
            //*  TNGSUB START
            if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.greater(" ")))                                                                              //Natural: IF AP-SUBSTITUTION-CONTRACT-IND > ' '
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Substitution_Contract_Ind().setValue(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind);                          //Natural: MOVE AP-SUBSTITUTION-CONTRACT-IND TO RP-SUBSTITUTION-CONTRACT-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Conv_Issue_State.greater(" ")))                                                                                       //Natural: IF AP-CONV-ISSUE-STATE > ' '
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Conv_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Conv_Issue_State);                                            //Natural: MOVE AP-CONV-ISSUE-STATE TO RP-CONV-ISSUE-STATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Deceased_Ind.greater(" ")))                                                                                           //Natural: IF AP-DECEASED-IND > ' '
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Deceased_Ind().setValue(annty_Actvty_Prap_View_Ap_Deceased_Ind);                                                    //Natural: MOVE AP-DECEASED-IND TO RP-DECEASED-IND
                //*  TNGSUB END
            }                                                                                                                                                             //Natural: END-IF
            //*  MTSIN
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Non_Proprietary_Pkg_Ind().setValue(annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind);                                  //Natural: MOVE AP-NON-PROPRIETARY-PKG-IND TO RP-NON-PROPRIETARY-PKG-IND
            pnd_Counters_Pnd_Cnt_Added.nadd(1);                                                                                                                           //Natural: ASSIGN #CNT-ADDED := #CNT-ADDED + 1
            pnd_Rep_Status.setValue("RP RECORD ADDED");                                                                                                                   //Natural: ASSIGN #REP-STATUS := 'RP RECORD ADDED'
            ldaAppl180.getVw_acis_Reprint_Fl_View().insertDBRow();                                                                                                        //Natural: STORE ACIS-REPRINT-FL-VIEW
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-DTL
        sub_Write_Report_Dtl();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Determine_Contract_Type() throws Exception                                                                                                           //Natural: DETERMINE-CONTRACT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().resetInitial();                                                                                                 //Natural: RESET INITIAL #CONTRACT-TYPE
        short decideConditionsMet2187 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB = 'D'
        if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("D")))
        {
            decideConditionsMet2187++;
            short decideConditionsMet2189 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '2' OR = '8'
            if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("2") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")))
            {
                decideConditionsMet2189++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RA");                                                                                         //Natural: MOVE 'RA' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '7' OR = '9'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("7") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))
            {
                decideConditionsMet2189++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("GRA");                                                                                        //Natural: MOVE 'GRA' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '5'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("5")))
            {
                decideConditionsMet2189++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RS");                                                                                         //Natural: MOVE 'RS' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '6'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")))
            {
                decideConditionsMet2189++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RL");                                                                                         //Natural: MOVE 'RL' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = 'A'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("A")))
            {
                decideConditionsMet2189++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RC");                                                                                         //Natural: MOVE 'RC' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = 'B'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("B")))
            {
                decideConditionsMet2189++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RHSP");                                                                                       //Natural: MOVE 'RHSP' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB = 'S'
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("S")))
        {
            decideConditionsMet2187++;
            short decideConditionsMet2206 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '2' OR = '8'
            if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("2") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")))
            {
                decideConditionsMet2206++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("SRA");                                                                                        //Natural: MOVE 'SRA' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '3'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("3")))
            {
                decideConditionsMet2206++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("GSRA");                                                                                       //Natural: MOVE 'GSRA' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '5'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("5")))
            {
                decideConditionsMet2206++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RSP");                                                                                        //Natural: MOVE 'RSP' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '6'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")))
            {
                decideConditionsMet2206++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RSP2");                                                                                       //Natural: MOVE 'RSP2' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '7'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("7")))
            {
                decideConditionsMet2206++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RCP");                                                                                        //Natural: MOVE 'RCP' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB = 'I'
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("I")))
        {
            decideConditionsMet2187++;
            short decideConditionsMet2221 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '1' OR = '2'
            if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("1") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("2")))
            {
                decideConditionsMet2221++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRA");                                                                                        //Natural: MOVE 'IRA' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '3'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("3")))
            {
                decideConditionsMet2221++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRAR");                                                                                       //Natural: MOVE 'IRAR' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '4'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")))
            {
                decideConditionsMet2221++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRAC");                                                                                       //Natural: MOVE 'IRAC' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '6'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")))
            {
                decideConditionsMet2221++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRAS");                                                                                       //Natural: MOVE 'IRAS' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '5'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("5")))
            {
                decideConditionsMet2221++;
                //*  TIGR-START
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("KEOG");                                                                                       //Natural: MOVE 'KEOG' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '7'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("7")))
            {
                decideConditionsMet2221++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRIR");                                                                                       //Natural: MOVE 'IRIR' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '8'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")))
            {
                decideConditionsMet2221++;
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRIC");                                                                                       //Natural: MOVE 'IRIC' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '9'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))
            {
                decideConditionsMet2221++;
                //*  TIGR-END
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRIS");                                                                                       //Natural: MOVE 'IRIS' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  PINE
    //*  PINE
    //*  PINE
    private void sub_Cor_For_Companion() throws Exception                                                                                                                 //Natural: COR-FOR-COMPANION
    {
        if (BLNatReinput.isReinput()) return;

        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                                //Natural: ASSIGN #MDMA211.#I-CONTRACT-NUMBER := AP-TIAA-CNTRCT
        pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                    //Natural: ASSIGN #MDMA211.#I-TIAA-CREF-IND := 'T'
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(1);                                                                                                         //Natural: ASSIGN #MDMA211.#I-PAYEE-CODE := 01
        //*  CALLNAT 'MDMN210A' #MDMA210                              /* PINE
        //*  PINE
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*  IF #MDMA210.#O-RETURN-CODE NE '0000'                     /* PINE
        //*  PINE
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            //*     COMPRESS ' MDMN210A ERROR' #MDMA210.#O-RETURN-CODE    /* PINE
            //*  PINE
            pnd_Except_Reason.setValue(DbsUtil.compress(" MDMN211A ERROR", pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code()));                                               //Natural: COMPRESS ' MDMN211A ERROR' #MDMA211.#O-RETURN-CODE TO #EXCEPT-REASON
            //*     WRITE ' MDMN210A ERROR ' AP-TIAA-CNTRCT #EXCEPT-REASON /* PINE
            //*  PINE
            getReports().write(0, " MDMN211A ERROR ",annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,pnd_Except_Reason);                                                            //Natural: WRITE ' MDMN211A ERROR ' AP-TIAA-CNTRCT #EXCEPT-REASON
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   TESTING ONLY
        //*  WRITE 'AFTER MDMN210A' #O-RETURN-CODE
        //*  WRITE '=' AP-TIAA-CNTRCT  '=' #O-TNT-TIAA-MAILED
        //*                            '=' #O-TNT-TIAA-MAILED
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Tnt_Tiaa_Mailed().notEquals("Y")))                                                                                  //Natural: IF #O-TNT-TIAA-MAILED NE 'Y'
        {
            pnd_V_Single_Issue_Ind.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #V-SINGLE-ISSUE-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Tnt_Cref_Mailed().notEquals("Y")))                                                                                  //Natural: IF #O-TNT-CREF-MAILED NE 'Y'
        {
            pnd_V_Single_Issue_Ind.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #V-SINGLE-ISSUE-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*    END   CNR  <<<
        //*  COR-FOR-COMPANION
    }
    private void sub_Yr2000_Date_Compare() throws Exception                                                                                                               //Natural: YR2000-DATE-COMPARE
    {
        if (BLNatReinput.isReinput()) return;

        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Currccyymmdd().setValue(Global.getDATN());                                                                              //Natural: MOVE *DATN TO #Y2-CURRCCYYMMDD
        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9().setValue(9);                                                                                                    //Natural: MOVE 9 TO #Y2-DTE-9
        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy().setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Curryy());                                                  //Natural: MOVE #Y2-CURRYY TO #Y2-DTE-YY
        //*  60/40 RULE
        short decideConditionsMet2290 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #Y2-DATE-RULE;//Natural: VALUE '1'
        if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("1"))))
        {
            decideConditionsMet2290++;
            //*  80/20 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy60());                                              //Natural: SUBTRACT #Y2-YY60 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("2"))))
        {
            decideConditionsMet2290++;
            //*  90/10 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy80());                                              //Natural: SUBTRACT #Y2-YY80 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("3"))))
        {
            decideConditionsMet2290++;
            //*  DEFAULT TO 70/30 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy90());                                              //Natural: SUBTRACT #Y2-YY90 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy70());                                              //Natural: SUBTRACT #Y2-YY70 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA1 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area2().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA2 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area2().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area2().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA2
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2321 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #Y2-YYMMDD-AREA1-A = '000000' OR = ' ' OR = '0000' OR = '0'
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals(" ") 
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("0")))
        {
            decideConditionsMet2321++;
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area1_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA1-9
        }                                                                                                                                                                 //Natural: WHEN #Y2-YYMMDD-AREA2-A = '000000' OR = ' ' OR = '0000' OR = '0'
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals(" ") 
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("0")))
        {
            decideConditionsMet2321++;
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area2_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA2-9
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2321 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Yr2000_Date_Compare_3() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-3
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area3().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA3 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area3().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area3().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA3
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA3-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area3_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA3-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_Date_Compare_4() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-4
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area4().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA4 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area4().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area4().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA4
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA4-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area4_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA4-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_Date_Compare_5() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-5
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area5().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA5 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area5().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area5().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA5
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA5-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area5_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA5-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare() throws Exception                                                                                                             //Natural: YR2000-J-DATE-COMPARE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area1().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA1 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area1().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area1().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area2().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA2 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area2().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area2().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA2
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_3() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-3
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area3().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA3 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area3().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area3().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA3
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_4() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-4
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area4().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA4 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area4().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area4().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA4
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_5() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-5
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area5().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA5 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area5().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area5().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA5
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Header() throws Exception                                                                                                                      //Natural: WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"PAGE : ",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZZZ"),new TabSetting(105),"PROGRAM : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 2 ) NOTITLE 1T 'PAGE : ' *PAGE-NUMBER ( 2 ) ( EM = ZZZZ ) 105T 'PROGRAM : ' *PROGRAM / 1T 'TIME : ' *TIME ( EM = XXXXXXXX ) // 43T 'LIST OF CONTRACTS DELETED FROM PRAP FILE' / 55T 'AS OF' *DATX ( EM = MM/DD/YY ) // 2T 'TIAA NO.    CREF NO.    PIN             SSN       PLAN NO. SUBPLAN  DATE DLTD   DLT RSN  DLTD BY            STATUS' / 1T '----------  ----------  ------------  -----------  -------  -------  ----------  -------  --------  ------------------------------'//
            TabSetting(1),"TIME : ",Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(43),"LIST OF CONTRACTS DELETED FROM PRAP FILE",NEWLINE,new 
            TabSetting(55),"AS OF",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE,new TabSetting(2),"TIAA NO.    CREF NO.    PIN             SSN       PLAN NO. SUBPLAN  DATE DLTD   DLT RSN  DLTD BY            STATUS",NEWLINE,new 
            TabSetting(1),"----------  ----------  ------------  -----------  -------  -------  ----------  -------  --------  ------------------------------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-HEADER
    }
    private void sub_Write_Report_Dtl() throws Exception                                                                                                                  //Natural: WRITE-REPORT-DTL
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Rpt_Dtl.reset();                                                                                                                                              //Natural: RESET #RPT-DTL
        pnd_Rpt_Dtl_Pnd_Rpt_Tiaa_No.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Contr());                                                                         //Natural: ASSIGN #RPT-TIAA-NO := RP-TIAA-CONTR
        pnd_Rpt_Dtl_Pnd_Rpt_Cref_No.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Contr());                                                                         //Natural: ASSIGN #RPT-CREF-NO := RP-CREF-CONTR
        //*  MOVE EDITED RP-PIN-NBR (EM=9999999) TO #RPT-PIN              /* PINE
        //*  PINE
        pnd_Rpt_Dtl_Pnd_Rpt_Pin.setValueEdited(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Pin_Nbr(),new ReportEditMask("999999999999"));                                       //Natural: MOVE EDITED RP-PIN-NBR ( EM = 999999999999 ) TO #RPT-PIN
        pnd_Rpt_Dtl_Pnd_Rpt_Ssn.setValueEdited(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Soc_Sec(),new ReportEditMask("999-99-9999"));                                        //Natural: MOVE EDITED RP-SOC-SEC ( EM = 999-99-9999 ) TO #RPT-SSN
        pnd_Rpt_Dtl_Pnd_Rpt_Plan.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Plan_No());                                                                          //Natural: ASSIGN #RPT-PLAN := RP-SGRD-PLAN-NO
        pnd_Rpt_Dtl_Pnd_Rpt_Subplan.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Subplan_No());                                                                    //Natural: ASSIGN #RPT-SUBPLAN := RP-SGRD-SUBPLAN-NO
        pnd_Rpt_Dtl_Pnd_Rpt_Status.setValue(pnd_Rep_Status);                                                                                                              //Natural: ASSIGN #RPT-STATUS := #REP-STATUS
        pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Date.setValueEdited(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Deleted(),new ReportEditMask("99/99/9999"));                                 //Natural: MOVE EDITED RP-DT-DELETED ( EM = 99/99/9999 ) TO #RPT-DLT-DATE
        pnd_Rpt_Dtl_Pnd_Rpt_Dlt_Rsn.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_Reason_Cd());                                                                   //Natural: ASSIGN #RPT-DLT-RSN := RP-DELETE-REASON-CD
        pnd_Rpt_Dtl_Pnd_Rpt_Dlt_User.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_User_Id());                                                                    //Natural: ASSIGN #RPT-DLT-USER := RP-DELETE-USER-ID
        getReports().write(2, ReportOption.NOTITLE,pnd_Rpt_Dtl);                                                                                                          //Natural: WRITE ( 2 ) #RPT-DTL
        if (Global.isEscape()) return;
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  DETAIL REPORT CONTROL SUMMARY
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"NUMBER OF 'DELETED PRAP RECORDS' READ",new TabSetting(68),":",new           //Natural: WRITE ( 2 ) /// 2T 'NUMBER OF "DELETED PRAP RECORDS" READ' 68T ':' 70T #CNT-READ ( EM = Z,ZZZ,ZZ9 ) / 2T 'NUMBER OF PRAP RECS ADDED TO RP FILE' 68T ':' 70T #CNT-ADDED ( EM = Z,ZZZ,ZZ9 ) / 2T 'NUMBER OF PRAP RECS EXIST IN RP FILE ("DELETE INFO." UPDATED)' 68T ':' 70T #CNT-UPDATED ( EM = Z,ZZZ,ZZ9 ) / 2T 'NUMBER OF PRAP RECS EXIST IN RP FILE(ALREADY HAS "DELETE INFO")' 68T ':' 70T #CNT-BYPASSED ( EM = Z,ZZZ,ZZ9 ) // /// 50T '<<<   END OF REPORT   >>>'
            TabSetting(70),pnd_Counters_Pnd_Cnt_Read, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"NUMBER OF PRAP RECS ADDED TO RP FILE",new 
            TabSetting(68),":",new TabSetting(70),pnd_Counters_Pnd_Cnt_Added, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"NUMBER OF PRAP RECS EXIST IN RP FILE ('DELETE INFO.' UPDATED)",new 
            TabSetting(68),":",new TabSetting(70),pnd_Counters_Pnd_Cnt_Updated, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"NUMBER OF PRAP RECS EXIST IN RP FILE(ALREADY HAS 'DELETE INFO')",new 
            TabSetting(68),":",new TabSetting(70),pnd_Counters_Pnd_Cnt_Bypassed, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
            TabSetting(50),"<<<   END OF REPORT   >>>");
        if (Global.isEscape()) return;
        //*  SUMMARY REPORT
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"PAGE : ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZZZ"),new TabSetting(61),"PROGRAM : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE 1T 'PAGE : ' *PAGE-NUMBER ( 1 ) ( EM = ZZZZ ) 61T 'PROGRAM : ' *PROGRAM / 1T 'TIME : ' *TIME ( EM = XXXXXXXX ) // 21T 'SUMMARY REPORT OF DELETED PRAP RECORDS' / 32T 'AS OF' *DATX ( EM = MM/DD/YY ) // 2T 'NUMBER OF "DELETED PRAP RECORDS" READ' 68T ':' 70T #CNT-READ ( EM = Z,ZZZ,ZZ9 ) // 2T 'NUMBER OF PRAP RECS ADDED TO RP FILE' 68T ':' 70T #CNT-ADDED ( EM = Z,ZZZ,ZZ9 ) // 2T 'NUMBER OF PRAP RECS EXIST IN RP FILE ("DELETE INFO." UPDATED)' 68T ':' 70T #CNT-UPDATED ( EM = Z,ZZZ,ZZ9 ) // 2T 'NUMBER OF PRAP RECS EXIST IN RP FILE(ALREADY HAS "DELETE INFO")' 68T ':' 70T #CNT-BYPASSED ( EM = Z,ZZZ,ZZ9 ) // 26T '<<<   END OF REPORT   >>>'
            TabSetting(1),"TIME : ",Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(21),"SUMMARY REPORT OF DELETED PRAP RECORDS",NEWLINE,new 
            TabSetting(32),"AS OF",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE,new TabSetting(2),"NUMBER OF 'DELETED PRAP RECORDS' READ",new 
            TabSetting(68),":",new TabSetting(70),pnd_Counters_Pnd_Cnt_Read, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(2),"NUMBER OF PRAP RECS ADDED TO RP FILE",new 
            TabSetting(68),":",new TabSetting(70),pnd_Counters_Pnd_Cnt_Added, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(2),"NUMBER OF PRAP RECS EXIST IN RP FILE ('DELETE INFO.' UPDATED)",new 
            TabSetting(68),":",new TabSetting(70),pnd_Counters_Pnd_Cnt_Updated, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(2),"NUMBER OF PRAP RECS EXIST IN RP FILE(ALREADY HAS 'DELETE INFO')",new 
            TabSetting(68),":",new TabSetting(70),pnd_Counters_Pnd_Cnt_Bypassed, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(26),"<<<   END OF REPORT   >>>");
        if (Global.isEscape()) return;
    }
    //*  CORMDM
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        getReports().write(0, "INVOKING OPEN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                     //Natural: WRITE 'INVOKING OPEN :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "FINISHED OPEN:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                      //Natural: WRITE 'FINISHED OPEN:' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #KOUNT = 1 TO 60
        for (pnd_Kount.setValue(1); condition(pnd_Kount.lessOrEqual(60)); pnd_Kount.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_Kount)));        //Natural: COMPRESS #RC ##DATA-RESPONSE ( #KOUNT ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER
                    sub_Write_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE // 'CONTRACT:' 25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),annty_Actvty_Prap_View_Ap_Pin_Nbr);
        if (condition(Global.getERROR_NR().equals(3052)))                                                                                                                 //Natural: IF *ERROR-NR = 3052
        {
            getReports().write(0, "BIRTHDATE:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Dob,NEWLINE,"ANNUITY START DATE:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Annuity_Start_Date); //Natural: WRITE 'BIRTHDATE:' 25T AP-DOB / 'ANNUITY START DATE:' 25T AP-ANNUITY-START-DATE
        }                                                                                                                                                                 //Natural: END-IF
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52");
        Global.format(2, "LS=133 PS=52");
    }
}
