/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:52 PM
**        * FROM NATURAL PROGRAM : Appb112
************************************************************
**        * FILE NAME            : Appb112.java
**        * CLASS NAME           : Appb112
**        * INSTANCE NAME        : Appb112
************************************************************
************************************************************************
* SUBPROGRAM: APPB112
*
* FUNCTION: EXTRACT ALL PRAP RECORDS WHOSE SSN,PLAN, SUBPLAN OR DIVSUB
*           GOT UPDATED.  THIS PROGRAM WILL CREATE AN EXTRACT FILE TO
*           EXP AG TO BUILD AN AUDIT HISTORY TASK.  THE TASK IN EXP AG
*           IS ENRACISAUD.
*
* CREATED:  06/03/2008    BOBBY ELLO
*
* HISTORY:
* 06/20/17 MITRAPU  PIN EXPANSION CHANGES.(C425939)              PINE. *
*
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb112 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_prap;
    private DbsField prap_Ap_Soc_Sec;
    private DbsField prap_Ap_Dob;
    private DbsField prap_Ap_Email_Address;
    private DbsField prap_Ap_Ownership_Type;
    private DbsField prap_Ap_Ownership;
    private DbsField prap_Ap_Divorce_Ind;
    private DbsField prap_Ap_Record_Type;
    private DbsField prap_Ap_Release_Ind;
    private DbsField prap_Ap_Coll_Code;
    private DbsField prap_Ap_Cor_Last_Nme;
    private DbsField prap_Ap_Cor_First_Nme;
    private DbsField prap_Ap_Cor_Mddle_Nme;
    private DbsField prap_Ap_Tiaa_Cntrct;
    private DbsField prap_Ap_T_Age_1st;
    private DbsField prap_Ap_Annuity_Start_Date;
    private DbsField prap_Ap_Addr_Usage_Code;
    private DbsField prap_Ap_Current_State_Code;
    private DbsField prap_Ap_Sgrd_Plan_No;
    private DbsField prap_Ap_Sgrd_Subplan_No;
    private DbsField prap_Ap_Sgrd_Divsub;

    private DbsGroup prap_Ap_Address_Info;
    private DbsField prap_Ap_Address_Line;
    private DbsField prap_Ap_City;
    private DbsField prap_Ap_Mail_Zip;

    private DbsGroup prap_Ap_Addr_Info;
    private DbsField prap_Ap_Address_Txt;
    private DbsField prap_Ap_Zip_Code;
    private DbsField prap_Ap_Racf_Id;

    private DbsGroup prap__R_Field_1;
    private DbsField prap_Ap_Racf_Id_1_8;
    private DbsField prap_Ap_Racf_Id_9_16;

    private DbsGroup prap_Ap_Bene_Contingent_Info;
    private DbsField prap_Ap_Contingent_Bene_Info;

    private DbsGroup prap__R_Field_2;
    private DbsField prap_Ap_Old_Ssn;
    private DbsField prap_Ap_Old_Plan_No;
    private DbsField prap_Ap_Old_Subplan_No;
    private DbsField prap_Ap_Old_Div_Sub;
    private DbsField prap_Ap_Rcrd_Updt_Tm_Stamp;

    private DataAccessProgramView vw_prap2;
    private DbsField prap2_Ap_Racf_Id;

    private DbsGroup prap2__R_Field_3;
    private DbsField prap2_Ap_Racf_Id_1_8;
    private DbsField prap2_Ap_Racf_Id_9_16;

    private DbsGroup exp_Ag_Interface_File;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Plan_No;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Soc_Sec;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Subplan_No;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Div_Sub;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Old_Plan;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Old_Ssn;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Old_Sub_Plan;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Old_Div_Sub;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Ap_Racf_Id_1_8;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Date;
    private DbsField exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Time;
    private DbsField pnd_Ssn_A;
    private DbsField pnd_Ssn_B;
    private DbsField pnd_1st_Pass;
    private DbsField pnd_Et_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_prap = new DataAccessProgramView(new NameInfo("vw_prap", "PRAP"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        prap_Ap_Soc_Sec = vw_prap.getRecord().newFieldInGroup("prap_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_Ap_Dob = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DOB");
        prap_Ap_Email_Address = vw_prap.getRecord().newFieldInGroup("prap_Ap_Email_Address", "AP-EMAIL-ADDRESS", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "AP_EMAIL_ADDRESS");
        prap_Ap_Ownership_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_OWNERSHIP_TYPE");
        prap_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        prap_Ap_Ownership = vw_prap.getRecord().newFieldInGroup("prap_Ap_Ownership", "AP-OWNERSHIP", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_OWNERSHIP");
        prap_Ap_Divorce_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Divorce_Ind", "AP-DIVORCE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_DIVORCE_IND");
        prap_Ap_Record_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_Ap_Release_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        prap_Ap_Coll_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_Ap_Cor_Last_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        prap_Ap_Cor_First_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_FIRST_NME");
        prap_Ap_Cor_Mddle_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_MDDLE_NME");
        prap_Ap_Tiaa_Cntrct = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_Ap_T_Age_1st = vw_prap.getRecord().newFieldInGroup("prap_Ap_T_Age_1st", "AP-T-AGE-1ST", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_AGE_1ST");
        prap_Ap_Annuity_Start_Date = vw_prap.getRecord().newFieldInGroup("prap_Ap_Annuity_Start_Date", "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_ANNUITY_START_DATE");
        prap_Ap_Addr_Usage_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Addr_Usage_Code", "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_ADDR_USAGE_CODE");
        prap_Ap_Current_State_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Current_State_Code", "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_CURRENT_STATE_CODE");
        prap_Ap_Sgrd_Plan_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_Ap_Sgrd_Subplan_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_SUBPLAN_NO");
        prap_Ap_Sgrd_Divsub = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_SGRD_DIVSUB");

        prap_Ap_Address_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Address_Info", "AP-ADDRESS-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        prap_Ap_Address_Line = prap_Ap_Address_Info.newFieldArrayInGroup("prap_Ap_Address_Line", "AP-ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1, 
            5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        prap_Ap_City = vw_prap.getRecord().newFieldInGroup("prap_Ap_City", "AP-CITY", FieldType.STRING, 27, RepeatingFieldStrategy.None, "AP_CITY");
        prap_Ap_Mail_Zip = vw_prap.getRecord().newFieldInGroup("prap_Ap_Mail_Zip", "AP-MAIL-ZIP", FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");

        prap_Ap_Addr_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Addr_Info", "AP-ADDR-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        prap_Ap_Address_Txt = prap_Ap_Addr_Info.newFieldArrayInGroup("prap_Ap_Address_Txt", "AP-ADDRESS-TXT", FieldType.STRING, 35, new DbsArrayController(1, 
            5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        prap_Ap_Zip_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Zip_Code", "AP-ZIP-CODE", FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        prap_Ap_Racf_Id = vw_prap.getRecord().newFieldInGroup("prap_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 16, RepeatingFieldStrategy.None, "AP_RACF_ID");

        prap__R_Field_1 = vw_prap.getRecord().newGroupInGroup("prap__R_Field_1", "REDEFINE", prap_Ap_Racf_Id);
        prap_Ap_Racf_Id_1_8 = prap__R_Field_1.newFieldInGroup("prap_Ap_Racf_Id_1_8", "AP-RACF-ID-1-8", FieldType.STRING, 8);
        prap_Ap_Racf_Id_9_16 = prap__R_Field_1.newFieldInGroup("prap_Ap_Racf_Id_9_16", "AP-RACF-ID-9-16", FieldType.STRING, 8);

        prap_Ap_Bene_Contingent_Info = vw_prap.getRecord().newGroupInGroup("prap_Ap_Bene_Contingent_Info", "AP-BENE-CONTINGENT-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        prap_Ap_Contingent_Bene_Info = prap_Ap_Bene_Contingent_Info.newFieldArrayInGroup("prap_Ap_Contingent_Bene_Info", "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 
            72, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");

        prap__R_Field_2 = prap_Ap_Bene_Contingent_Info.newGroupInGroup("prap__R_Field_2", "REDEFINE", prap_Ap_Contingent_Bene_Info);
        prap_Ap_Old_Ssn = prap__R_Field_2.newFieldInGroup("prap_Ap_Old_Ssn", "AP-OLD-SSN", FieldType.STRING, 9);
        prap_Ap_Old_Plan_No = prap__R_Field_2.newFieldInGroup("prap_Ap_Old_Plan_No", "AP-OLD-PLAN-NO", FieldType.STRING, 6);
        prap_Ap_Old_Subplan_No = prap__R_Field_2.newFieldInGroup("prap_Ap_Old_Subplan_No", "AP-OLD-SUBPLAN-NO", FieldType.STRING, 6);
        prap_Ap_Old_Div_Sub = prap__R_Field_2.newFieldInGroup("prap_Ap_Old_Div_Sub", "AP-OLD-DIV-SUB", FieldType.STRING, 4);
        prap_Ap_Rcrd_Updt_Tm_Stamp = vw_prap.getRecord().newFieldInGroup("prap_Ap_Rcrd_Updt_Tm_Stamp", "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "AP_RCRD_UPDT_TM_STAMP");
        registerRecord(vw_prap);

        vw_prap2 = new DataAccessProgramView(new NameInfo("vw_prap2", "PRAP2"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap2_Ap_Racf_Id = vw_prap2.getRecord().newFieldInGroup("prap2_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 16, RepeatingFieldStrategy.None, "AP_RACF_ID");

        prap2__R_Field_3 = vw_prap2.getRecord().newGroupInGroup("prap2__R_Field_3", "REDEFINE", prap2_Ap_Racf_Id);
        prap2_Ap_Racf_Id_1_8 = prap2__R_Field_3.newFieldInGroup("prap2_Ap_Racf_Id_1_8", "AP-RACF-ID-1-8", FieldType.STRING, 8);
        prap2_Ap_Racf_Id_9_16 = prap2__R_Field_3.newFieldInGroup("prap2_Ap_Racf_Id_9_16", "AP-RACF-ID-9-16", FieldType.STRING, 8);
        registerRecord(vw_prap2);

        exp_Ag_Interface_File = localVariables.newGroupInRecord("exp_Ag_Interface_File", "EXP-AG-INTERFACE-FILE");
        exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Plan_No = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Plan_No", "EXP-AG-AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6);
        exp_Ag_Interface_File_Exp_Ag_Ap_Soc_Sec = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Soc_Sec", "EXP-AG-AP-SOC-SEC", 
            FieldType.STRING, 9);
        exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Subplan_No = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Subplan_No", "EXP-AG-AP-SGRD-SUBPLAN-NO", 
            FieldType.STRING, 6);
        exp_Ag_Interface_File_Exp_Ag_Ap_Div_Sub = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Div_Sub", "EXP-AG-AP-DIV-SUB", 
            FieldType.STRING, 4);
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Plan = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Old_Plan", "EXP-AG-AP-OLD-PLAN", 
            FieldType.STRING, 6);
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Ssn = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Old_Ssn", "EXP-AG-AP-OLD-SSN", 
            FieldType.STRING, 9);
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Sub_Plan = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Old_Sub_Plan", "EXP-AG-AP-OLD-SUB-PLAN", 
            FieldType.STRING, 6);
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Div_Sub = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Old_Div_Sub", "EXP-AG-AP-OLD-DIV-SUB", 
            FieldType.STRING, 4);
        exp_Ag_Interface_File_Exp_Ag_Ap_Racf_Id_1_8 = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Ap_Racf_Id_1_8", "EXP-AG-AP-RACF-ID-1-8", 
            FieldType.STRING, 8);
        exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Date = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Date", "EXP-AG-RCRD-UPDT-DATE", 
            FieldType.NUMERIC, 8);
        exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Time = exp_Ag_Interface_File.newFieldInGroup("exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Time", "EXP-AG-RCRD-UPDT-TIME", 
            FieldType.NUMERIC, 7);
        pnd_Ssn_A = localVariables.newFieldInRecord("pnd_Ssn_A", "#SSN-A", FieldType.STRING, 9);
        pnd_Ssn_B = localVariables.newFieldInRecord("pnd_Ssn_B", "#SSN-B", FieldType.STRING, 9);
        pnd_1st_Pass = localVariables.newFieldInRecord("pnd_1st_Pass", "#1ST-PASS", FieldType.BOOLEAN, 1);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap.reset();
        vw_prap2.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb112() throws Exception
    {
        super("Appb112");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60
        pnd_1st_Pass.setValue(true);                                                                                                                                      //Natural: ASSIGN #1ST-PASS := TRUE
                                                                                                                                                                          //Natural: PERFORM READ-PRAP-RECORDS
        sub_Read_Prap_Records();
        if (condition(Global.isEscape())) {return;}
        pnd_1st_Pass.setValue(false);                                                                                                                                     //Natural: ASSIGN #1ST-PASS := FALSE
                                                                                                                                                                          //Natural: PERFORM READ-PRAP-RECORDS
        sub_Read_Prap_Records();
        if (condition(Global.isEscape())) {return;}
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED 001T 'REPORT DATE:' 1X *DATU '-' *TIMX ( EM = HH:IIAP ) 045T 'ACIS TO EXP AG AUDIT REPORT' 103T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZZZ9 ) / 001T 'NAT PROGRAM:' *PROGRAM 057T 'AUDIT CONTROL RPT' 103T *INIT-USER / 001T '-' ( 113 )
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PRAP-RECORDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EXP-AG-INTERFACE-FILE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-REPORT
    }
    private void sub_Read_Prap_Records() throws Exception                                                                                                                 //Natural: READ-PRAP-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_prap.startDatabaseRead                                                                                                                                         //Natural: READ PRAP BY AP-SGRD-NEW-PRAP-KEY
        (
        "L_READ",
        new Oc[] { new Oc("AP_SGRD_NEW_PRAP_KEY", "ASC") }
        );
        L_READ:
        while (condition(vw_prap.readNextRow("L_READ")))
        {
            if (condition(pnd_1st_Pass.getBoolean()))                                                                                                                     //Natural: IF #1ST-PASS
            {
                if (condition(prap_Ap_Racf_Id_9_16.notEquals("UPDSNDPI")))                                                                                                //Natural: IF PRAP.AP-RACF-ID-9-16 NE 'UPDSNDPI'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-EXP-AG-INTERFACE-FILE
                sub_Create_Exp_Ag_Interface_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CREATE-REPORT
                sub_Create_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(prap_Ap_Racf_Id_9_16.notEquals("UPDSNDPI")))                                                                                                //Natural: IF PRAP.AP-RACF-ID-9-16 NE 'UPDSNDPI'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                L_GET:                                                                                                                                                    //Natural: GET PRAP2 *ISN ( L-READ. )
                vw_prap2.readByID(vw_prap.getAstISN("L_READ"), "L_GET");
                prap2_Ap_Racf_Id_9_16.setValue("AUDITFLD");                                                                                                               //Natural: ASSIGN PRAP2.AP-RACF-ID-9-16 := 'AUDITFLD'
                vw_prap2.updateDBRow("L_GET");                                                                                                                            //Natural: UPDATE ( L-GET. )
                if (condition(pnd_Et_Count.equals(50)))                                                                                                                   //Natural: IF #ET-COUNT = 50
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Et_Count.reset();                                                                                                                                 //Natural: RESET #ET-COUNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  FINAL ET
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Create_Exp_Ag_Interface_File() throws Exception                                                                                                      //Natural: CREATE-EXP-AG-INTERFACE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Plan_No.setValue(prap_Ap_Sgrd_Plan_No);                                                                                      //Natural: ASSIGN EXP-AG-AP-SGRD-PLAN-NO := AP-SGRD-PLAN-NO
        exp_Ag_Interface_File_Exp_Ag_Ap_Sgrd_Subplan_No.setValue(prap_Ap_Sgrd_Subplan_No);                                                                                //Natural: ASSIGN EXP-AG-AP-SGRD-SUBPLAN-NO := AP-SGRD-SUBPLAN-NO
        exp_Ag_Interface_File_Exp_Ag_Ap_Soc_Sec.setValueEdited(prap_Ap_Soc_Sec,new ReportEditMask("999999999"));                                                          //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999999999 ) TO EXP-AG-AP-SOC-SEC
        exp_Ag_Interface_File_Exp_Ag_Ap_Div_Sub.setValue(prap_Ap_Sgrd_Divsub);                                                                                            //Natural: ASSIGN EXP-AG-AP-DIV-SUB := AP-SGRD-DIVSUB
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Plan.setValue(prap_Ap_Old_Plan_No);                                                                                           //Natural: ASSIGN EXP-AG-AP-OLD-PLAN := AP-OLD-PLAN-NO
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Sub_Plan.setValue(prap_Ap_Old_Subplan_No);                                                                                    //Natural: ASSIGN EXP-AG-AP-OLD-SUB-PLAN := AP-OLD-SUBPLAN-NO
        prap_Ap_Old_Ssn.setValue(prap_Ap_Old_Ssn, MoveOption.RightJustified);                                                                                             //Natural: MOVE RIGHT AP-OLD-SSN TO AP-OLD-SSN
        DbsUtil.examine(new ExamineSource(prap_Ap_Old_Ssn), new ExamineSearch(" "), new ExamineReplace("0"));                                                             //Natural: EXAMINE AP-OLD-SSN FOR ' ' REPLACE WITH '0'
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Ssn.setValue(prap_Ap_Old_Ssn);                                                                                                //Natural: ASSIGN EXP-AG-AP-OLD-SSN := AP-OLD-SSN
        exp_Ag_Interface_File_Exp_Ag_Ap_Old_Div_Sub.setValue(prap_Ap_Old_Div_Sub);                                                                                        //Natural: ASSIGN EXP-AG-AP-OLD-DIV-SUB := AP-OLD-DIV-SUB
        exp_Ag_Interface_File_Exp_Ag_Ap_Racf_Id_1_8.setValue(prap_Ap_Racf_Id_1_8);                                                                                        //Natural: ASSIGN EXP-AG-AP-RACF-ID-1-8 := PRAP.AP-RACF-ID-1-8
        exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Date.compute(new ComputeParameters(false, exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Date), prap_Ap_Rcrd_Updt_Tm_Stamp.val()); //Natural: ASSIGN EXP-AG-RCRD-UPDT-DATE := VAL ( AP-RCRD-UPDT-TM-STAMP )
        exp_Ag_Interface_File_Exp_Ag_Rcrd_Updt_Time.setValue(Global.getTIMN());                                                                                           //Natural: ASSIGN EXP-AG-RCRD-UPDT-TIME := *TIMN
        getWorkFiles().write(1, false, exp_Ag_Interface_File);                                                                                                            //Natural: WRITE WORK FILE 1 EXP-AG-INTERFACE-FILE
    }
    private void sub_Create_Report() throws Exception                                                                                                                     //Natural: CREATE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Ssn_A.setValueEdited(prap_Ap_Soc_Sec,new ReportEditMask("999999999"));                                                                                        //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999999999 ) TO #SSN-A
        pnd_Ssn_B.setValue(prap_Ap_Old_Ssn, MoveOption.RightJustified);                                                                                                   //Natural: MOVE RIGHT AP-OLD-SSN TO #SSN-B
        DbsUtil.examine(new ExamineSource(pnd_Ssn_B), new ExamineSearch(" "), new ExamineReplace("0"));                                                                   //Natural: EXAMINE #SSN-B FOR ' ' REPLACE WITH '0'
        getReports().display(1, "PLAN    ",                                                                                                                               //Natural: DISPLAY ( 1 ) 'PLAN    ' AP-SGRD-PLAN-NO 'SUBPLAN ' AP-SGRD-SUBPLAN-NO 'SSN      ' #SSN-A 'DIVSUB' AP-SGRD-DIVSUB 'OLD PLAN' AP-OLD-PLAN-NO 'OLD SUBPLAN' AP-OLD-SUBPLAN-NO 'OLD SSN' #SSN-B 'OLD DIV-SUB' AP-OLD-DIV-SUB 'RACF-ID  ' PRAP.AP-RACF-ID-1-8 'UPDT DATE' AP-RCRD-UPDT-TM-STAMP
        		prap_Ap_Sgrd_Plan_No,"SUBPLAN ",
        		prap_Ap_Sgrd_Subplan_No,"SSN      ",
        		pnd_Ssn_A,"DIVSUB",
        		prap_Ap_Sgrd_Divsub,"OLD PLAN",
        		prap_Ap_Old_Plan_No,"OLD SUBPLAN",
        		prap_Ap_Old_Subplan_No,"OLD SSN",
        		pnd_Ssn_B,"OLD DIV-SUB",
        		prap_Ap_Old_Div_Sub,"RACF-ID  ",
        		prap_Ap_Racf_Id_1_8,"UPDT DATE",
        		prap_Ap_Rcrd_Updt_Tm_Stamp);
        if (Global.isEscape()) return;
        //*  'UPDT TIME'
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),"REPORT DATE:",new ColumnSpacing(1),Global.getDATU(),"-",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),new TabSetting(45),"ACIS TO EXP AG AUDIT REPORT",new TabSetting(103),"PAGE:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZZZ9"),NEWLINE,new TabSetting(1),"NAT PROGRAM:",Global.getPROGRAM(),new TabSetting(57),"AUDIT CONTROL RPT",new TabSetting(103),Global.getINIT_USER(),NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(113));

        getReports().setDisplayColumns(1, "PLAN    ",
        		prap_Ap_Sgrd_Plan_No,"SUBPLAN ",
        		prap_Ap_Sgrd_Subplan_No,"SSN      ",
        		pnd_Ssn_A,"DIVSUB",
        		prap_Ap_Sgrd_Divsub,"OLD PLAN",
        		prap_Ap_Old_Plan_No,"OLD SUBPLAN",
        		prap_Ap_Old_Subplan_No,"OLD SSN",
        		pnd_Ssn_B,"OLD DIV-SUB",
        		prap_Ap_Old_Div_Sub,"RACF-ID  ",
        		prap_Ap_Racf_Id_1_8,"UPDT DATE",
        		prap_Ap_Rcrd_Updt_Tm_Stamp);
    }
}
