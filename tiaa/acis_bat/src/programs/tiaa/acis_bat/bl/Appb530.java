/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:57 PM
**        * FROM NATURAL PROGRAM : Appb530
************************************************************
**        * FILE NAME            : Appb530.java
**        * CLASS NAME           : Appb530
**        * INSTANCE NAME        : Appb530
************************************************************
************************************************************************
* PROGRAM  : APPB530 - RECONCILE EDW EXTRACT WITH ACIS RECORDS         *
* FUNCTION : READS EDW EXTRACTS CONTAINING NEWLY ENROLLED AND NEWLY    *
*            FUNDED CONTRACTS THEN RECONCILE THESE RECORDS WITH ACIS   *
*            REPRINT FILE. PRODUCE A REPORT FILE CONTAINING THE STATUS *
*            OF EACH RECORD.                                           *
* HISTORY  :                                                           *
* 08/13/09 C.AVE     - NEW PROGRAM                                     *
* 09/03/09 C.AVE     - MODIFIED DATES USED IN IDENTIFYING PENDED LEGAL *
*                      VERSUS EXCEPTION                                *
* 09/04/09 C.AVE     - INCLUDED IMPLEMENTATION DATE PARM AS THE REF.   *
*                      DATE WHEN CHECKING THE VALIDATION OF THE NEWLY  *
*                      CREATED WELCOME AND LEGAL MAIL DATES            *
* 09/09/09 C.AVE     - INCLUDE RA8 AS "NOT APPLICABLE"                 *
* 10/01/09 C.AVE     - INCLUDE CONTRACTS PREFIX WITH A,B,AND C AS      *
*                      "NOT APPLICABLE"                                *
* 11/02/09 C.AVE     - INCLUDE LOGIC TO ADDRESS NULL CONTRACT          *
* 11/03/09 C.AVE     - MODIFIED DATE CRITERIA FOR DETERMINING PENDING  *
*                      CONTRACTS
* 01/28/10 DEVELBISS - USE RP-OWNERSHIP INSTEAD OF RP-PACKAGE-MAIL-TYPE
*                      TO DETERMINE COLLEGE OWNED CONTRACTS.
*                      SEE BJD1.
* 03/15/10 B.HOLLOWAY - TIAA STABLE VALUE - RESTOW FOR APPL180
* 03/04/11 B. ELLO   - FLORIDA PROJECT - RESTOW FOR APPL180
* 05/04/2011  C. SCHNEIDER RESTOW FOR NEW VERSION OF APPL180 FOR JHU
* 07/01/11 L. SHU    - TRANSFER IN CREDIT - RESTOW FOR APPL180
* 08/04/11 G. SUDSATAYA - RESTOW FOR CAMPUS - USE APPL180
* 03/23/12 K.GATES   - RE-STOW FOR APPL180 FOR ROCH2
* 11/17/12 L. SHU    - RE-STOW FOR APPL180 FOR LPAO
* 07/18/13 L. SHU    - RE-STOW FOR APPL180 FOR IRA SUBSTITUTION  TNGSUB
* 09/13/13 L. SHU    - RE-STOW FOR APPL180 FOR MT SINAI          MTSIN
* 03/04/14 K. BALA   - FIX NAT3047 ERROR                     /*CHG307711
* 10/24/14 C. AVE    - CHANGES MADE TO SUPPORT THE JHU REQUIREMENT TO
*                      IDENTIFY THE MUTUAL FUND ONLY (MFO) CONTRACTS AS
*                      NOT-APPLICABLE INSTEAD BEING MARKED AS EXCEPTIONS
*                      IN THE RECONCILIATION REPORTS. SEE MFO.
* 09/15/14 B.NEWSOM  - ACIS/CIS CREF REDESIGN COMMUNICATIONS     (ACCRC)
*                      RESTOW FOR APPL180 CHANGES
* 09/01/15 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180
*                      FOR BENE THE PLAN.                        (BIP)
* 03/17/17 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180
*                      FOR ONEIRA                                ONEIRA
* 06/19/17 BABRE     - PIN EXPANSION CHANGES. CHG425939 STOW ONLY
* 19/16/19 L SHU     - RESTOW FOR NEW FIELDS IN APPL180  CNTRSTRG & IISG
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb530 extends BLNatBase
{
    // Data Areas
    private LdaAppl180 ldaAppl180;
    private LdaAppl530 ldaAppl530;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_table_Entry;
    private DbsField table_Entry_Entry_Actve_Ind;
    private DbsField table_Entry_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Entry_Table_Sub_Id;
    private DbsField table_Entry_Entry_Cde;

    private DbsGroup table_Entry__R_Field_1;
    private DbsField table_Entry_Entry_Plan;
    private DbsField table_Entry_Entry_Sub_Plan_Number;
    private DbsField table_Entry_Entry_Pkg_Cntrl_Type;
    private DbsField table_Entry_Entry_Cde_Addl;
    private DbsField table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup table_Entry__R_Field_2;
    private DbsField table_Entry_Entry_Pckg_Type;
    private DbsField table_Entry_Entry_Enr_Source;
    private DbsField table_Entry_Entry_Pkg_Trigger;
    private DbsField table_Entry_Addl_Entry_Txt_2;
    private DbsField pnd_Edw_File;

    private DbsGroup pnd_Edw_File__R_Field_3;
    private DbsField pnd_Edw_File_Pnd_Edw_Rec_Id;

    private DbsGroup pnd_Edw_File__R_Field_4;

    private DbsGroup pnd_Edw_File_Pnd_Edw_Hdr_Rec;
    private DbsField pnd_Edw_File_Pnd_Edw_Hdr_Rec_Id;
    private DbsField pnd_Edw_File_Pnd_Edw_Hdr_File_Typ;
    private DbsField pnd_Edw_File_Pnd_Edw_Hdr_Control_Dte;
    private DbsField pnd_Edw_File_Pnd_Edw_Hdr_Cntrl_Count;

    private DbsGroup pnd_Edw_File__R_Field_5;

    private DbsGroup pnd_Edw_File_Pnd_Edw_Dtl_Rec;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Ssn;

    private DbsGroup pnd_Edw_File__R_Field_6;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Plan_Subplan;

    private DbsGroup pnd_Edw_File__R_Field_7;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Plan;

    private DbsGroup pnd_Edw_File__R_Field_8;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Plan_A3;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Subplan;

    private DbsGroup pnd_Edw_File__R_Field_9;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Mlob;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Slob;

    private DbsGroup pnd_Edw_File__R_Field_10;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No;

    private DbsGroup pnd_Edw_File__R_Field_11;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Cntrct_Prefix;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Cref_No;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Dflt_Enroll_Ind;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Funded_Ind;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Dte;

    private DbsGroup pnd_Edw_File__R_Field_12;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Date;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Control_Dte;

    private DbsGroup pnd_Edw_File__R_Field_13;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Control_Date;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type;
    private DbsField pnd_Edw_File_Pnd_Edw_Dtl_Category;

    private DbsGroup pnd_Edw_File__R_Field_14;

    private DbsGroup pnd_Edw_File_Pnd_Edw_Trl_Rec;
    private DbsField pnd_Edw_File_Pnd_Edw_Trl_Rec_Id;
    private DbsField pnd_Edw_File_Pnd_Edw_Trl_Control_Dte;
    private DbsField pnd_Edw_File_Pnd_Edw_Trl_Cntrl_Count;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No;

    private DbsGroup pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt;

    private DbsGroup pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_16;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category;
    private DbsField pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct;
    private DbsField pnd_New_Enrollment;
    private DbsField pnd_Newly_Funded;
    private DbsField pnd_Reprint_Rec_Found;
    private DbsField pnd_Control_Date_Found;
    private DbsField pnd_Abort_Processing;
    private DbsField pnd_Exception_Processing;
    private DbsField pnd_Expctd_Legal_Mail_Dte;
    private DbsField pnd_Current_Dte;
    private DbsField pnd_Current_Dte_N;
    private DbsField pnd_Header_Control_Dte;
    private DbsField pnd_Dte;

    private DbsGroup pnd_Dte__R_Field_17;
    private DbsField pnd_Dte_Pnd_Dte2;
    private DbsField pnd_From_Dte;
    private DbsField pnd_Rp_Extracted_Date;

    private DbsGroup pnd_Rp_Extracted_Date__R_Field_18;
    private DbsField pnd_Rp_Extracted_Date_Pnd_Rp_Extracted_Dte;
    private DbsField pnd_Rp_Xtrct_Dte;
    private DbsField pnd_No_Days;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Read;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Deleted;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Read;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Deleted;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Pending;
    private DbsField pnd_Counters_Pnd_Ttl_Header_Found;
    private DbsField pnd_Counters_Pnd_Ttl_Trailer_Found;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Read;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Deleted;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Read;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Deleted;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Pending;
    private DbsField pnd_Var_Reconcile_Type;
    private DbsField pnd_Var_Control_Enroll_Cnt;
    private DbsField pnd_Var_Control_Funded_Cnt;

    private DbsGroup pnd_Var_Group;
    private DbsField pnd_Var_Group_Pnd_Var_Category;
    private DbsField pnd_Var_Group_Pnd_Var_Exception_Ind;
    private DbsField pnd_Var_Group_Pnd_Var_Exception_Dte;
    private DbsField pnd_Var_Group_Pnd_Var_Reprint_Dte;
    private DbsField pnd_Var_Group_Pnd_Var_Delete_Dte;
    private DbsField pnd_Var_Group_Pnd_Var_Delete_User;
    private DbsField pnd_Var_Group_Pnd_Var_Delete_Reason;
    private DbsField pnd_Var_Group_Pnd_Var_Welcome_Mail_Dte;
    private DbsField pnd_Var_Group_Pnd_Var_Legal_Mail_Dte;
    private DbsField pnd_Var_Group_Pnd_Var_Package_Mail_Type;
    private DbsField pnd_Var_Group_Pnd_Var_Resolved_Dte;
    private DbsField pnd_Var_Group_Pnd_Var_Old_Category;
    private DbsField pnd_Var_Group_Pnd_Var_Excptn_Age_Days;
    private DbsField pnd_Work_Rpt_File;

    private DbsGroup pnd_Work_Rpt_File__R_Field_19;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Rec_Id;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Rec_Type;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Category_Ind;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Cref_No;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Plan;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Subplan;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Ssn;

    private DbsGroup pnd_Work_Rpt_File__R_Field_20;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_21;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_22;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Reprint_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_23;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_User;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_24;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_25;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Control_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_26;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Control_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Old_Category;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_27;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Exception_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_28;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Resolved_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days;

    private DbsGroup pnd_Work_Rpt_File__R_Field_29;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Excptn_Age_Days;
    private DbsField pnd_Wk_Control_File;

    private DbsGroup pnd_Wk_Control_File__R_Field_30;
    private DbsField pnd_Wk_Control_File_Pnd_Wk_Cntrl_Dte;
    private DbsField pnd_Input_Implementation_Dte;

    private DbsGroup pnd_Input_Implementation_Dte__R_Field_31;
    private DbsField pnd_Input_Implementation_Dte_Pnd_Input_Implementation_Date;

    private DbsGroup pnd_Suppress_Table;
    private DbsField pnd_Suppress_Table_Pnd_S_Plan_Subplan;

    private DbsGroup pnd_Suppress_Table__R_Field_32;
    private DbsField pnd_Suppress_Table_Pnd_S_Plan_Number;
    private DbsField pnd_Suppress_Table_Pnd_S_Subplan_Number;
    private DbsField pnd_Suppress_Table_Pnd_S_Pckg_Type;
    private DbsField pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type;
    private DbsField pnd_Suppress_Table_Pnd_S_Enr_Source;
    private DbsField pnd_I_S;
    private DbsField pnd_Ps_Key;

    private DbsGroup pnd_Ps_Key__R_Field_33;
    private DbsField pnd_Ps_Key_Pnd_Ps_Entry_Actve_Ind;
    private DbsField pnd_Ps_Key_Pnd_Ps_Entry_Table_Id_Nbr;
    private DbsField pnd_Ps_Key_Pnd_Ps_Entry_Table_Sub_Id;
    private DbsField pnd_Ps_Key_Pnd_Ps_Entry_Cde;
    private DbsField pnd_Indx;
    private DbsField pnd_Suppress_Welcome_Pkg;
    private DbsField pnd_Suppress_Legal_Pkg;
    private DbsField pnd_Blank_Enr_Source;
    private DbsField pnd_Entry_Enr_Source;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl180 = new LdaAppl180();
        registerRecord(ldaAppl180);
        registerRecord(ldaAppl180.getVw_acis_Reprint_Fl_View());
        ldaAppl530 = new LdaAppl530();
        registerRecord(ldaAppl530);
        registerRecord(ldaAppl530.getVw_acis_Reconcile_Fl_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_table_Entry = new DataAccessProgramView(new NameInfo("vw_table_Entry", "TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        table_Entry_Entry_Actve_Ind = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        table_Entry_Entry_Table_Id_Nbr = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        table_Entry_Entry_Table_Sub_Id = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        table_Entry_Entry_Cde = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        table_Entry__R_Field_1 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_1", "REDEFINE", table_Entry_Entry_Cde);
        table_Entry_Entry_Plan = table_Entry__R_Field_1.newFieldInGroup("table_Entry_Entry_Plan", "ENTRY-PLAN", FieldType.STRING, 6);
        table_Entry_Entry_Sub_Plan_Number = table_Entry__R_Field_1.newFieldInGroup("table_Entry_Entry_Sub_Plan_Number", "ENTRY-SUB-PLAN-NUMBER", FieldType.STRING, 
            6);
        table_Entry_Entry_Pkg_Cntrl_Type = table_Entry__R_Field_1.newFieldInGroup("table_Entry_Entry_Pkg_Cntrl_Type", "ENTRY-PKG-CNTRL-TYPE", FieldType.STRING, 
            1);
        table_Entry_Entry_Cde_Addl = table_Entry__R_Field_1.newFieldInGroup("table_Entry_Entry_Cde_Addl", "ENTRY-CDE-ADDL", FieldType.STRING, 7);
        table_Entry_Entry_Dscrptn_Txt = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        table_Entry__R_Field_2 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_2", "REDEFINE", table_Entry_Entry_Dscrptn_Txt);
        table_Entry_Entry_Pckg_Type = table_Entry__R_Field_2.newFieldInGroup("table_Entry_Entry_Pckg_Type", "ENTRY-PCKG-TYPE", FieldType.STRING, 1);
        table_Entry_Entry_Enr_Source = table_Entry__R_Field_2.newFieldInGroup("table_Entry_Entry_Enr_Source", "ENTRY-ENR-SOURCE", FieldType.STRING, 1);
        table_Entry_Entry_Pkg_Trigger = table_Entry__R_Field_2.newFieldInGroup("table_Entry_Entry_Pkg_Trigger", "ENTRY-PKG-TRIGGER", FieldType.STRING, 
            1);
        table_Entry_Addl_Entry_Txt_2 = table_Entry__R_Field_2.newFieldInGroup("table_Entry_Addl_Entry_Txt_2", "ADDL-ENTRY-TXT-2", FieldType.STRING, 57);
        registerRecord(vw_table_Entry);

        pnd_Edw_File = localVariables.newFieldInRecord("pnd_Edw_File", "#EDW-FILE", FieldType.STRING, 100);

        pnd_Edw_File__R_Field_3 = localVariables.newGroupInRecord("pnd_Edw_File__R_Field_3", "REDEFINE", pnd_Edw_File);
        pnd_Edw_File_Pnd_Edw_Rec_Id = pnd_Edw_File__R_Field_3.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Rec_Id", "#EDW-REC-ID", FieldType.STRING, 1);

        pnd_Edw_File__R_Field_4 = localVariables.newGroupInRecord("pnd_Edw_File__R_Field_4", "REDEFINE", pnd_Edw_File);

        pnd_Edw_File_Pnd_Edw_Hdr_Rec = pnd_Edw_File__R_Field_4.newGroupInGroup("pnd_Edw_File_Pnd_Edw_Hdr_Rec", "#EDW-HDR-REC");
        pnd_Edw_File_Pnd_Edw_Hdr_Rec_Id = pnd_Edw_File_Pnd_Edw_Hdr_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Hdr_Rec_Id", "#EDW-HDR-REC-ID", FieldType.STRING, 
            1);
        pnd_Edw_File_Pnd_Edw_Hdr_File_Typ = pnd_Edw_File_Pnd_Edw_Hdr_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Hdr_File_Typ", "#EDW-HDR-FILE-TYP", FieldType.STRING, 
            1);
        pnd_Edw_File_Pnd_Edw_Hdr_Control_Dte = pnd_Edw_File_Pnd_Edw_Hdr_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Hdr_Control_Dte", "#EDW-HDR-CONTROL-DTE", 
            FieldType.STRING, 8);
        pnd_Edw_File_Pnd_Edw_Hdr_Cntrl_Count = pnd_Edw_File_Pnd_Edw_Hdr_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Hdr_Cntrl_Count", "#EDW-HDR-CNTRL-COUNT", 
            FieldType.NUMERIC, 9);

        pnd_Edw_File__R_Field_5 = localVariables.newGroupInRecord("pnd_Edw_File__R_Field_5", "REDEFINE", pnd_Edw_File);

        pnd_Edw_File_Pnd_Edw_Dtl_Rec = pnd_Edw_File__R_Field_5.newGroupInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Rec", "#EDW-DTL-REC");
        pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id", "#EDW-DTL-REC-ID", FieldType.STRING, 
            1);
        pnd_Edw_File_Pnd_Edw_Dtl_Ssn = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Ssn", "#EDW-DTL-SSN", FieldType.STRING, 
            9);

        pnd_Edw_File__R_Field_6 = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newGroupInGroup("pnd_Edw_File__R_Field_6", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Ssn);
        pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec = pnd_Edw_File__R_Field_6.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec", "#EDW-DTL-SOC-SEC", FieldType.NUMERIC, 
            9);
        pnd_Edw_File_Pnd_Edw_Dtl_Plan_Subplan = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Plan_Subplan", "#EDW-DTL-PLAN-SUBPLAN", 
            FieldType.STRING, 12);

        pnd_Edw_File__R_Field_7 = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newGroupInGroup("pnd_Edw_File__R_Field_7", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Plan_Subplan);
        pnd_Edw_File_Pnd_Edw_Dtl_Plan = pnd_Edw_File__R_Field_7.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Plan", "#EDW-DTL-PLAN", FieldType.STRING, 6);

        pnd_Edw_File__R_Field_8 = pnd_Edw_File__R_Field_7.newGroupInGroup("pnd_Edw_File__R_Field_8", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Plan);
        pnd_Edw_File_Pnd_Edw_Dtl_Plan_A3 = pnd_Edw_File__R_Field_8.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Plan_A3", "#EDW-DTL-PLAN-A3", FieldType.STRING, 
            3);
        pnd_Edw_File_Pnd_Edw_Dtl_Subplan = pnd_Edw_File__R_Field_7.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Subplan", "#EDW-DTL-SUBPLAN", FieldType.STRING, 
            6);

        pnd_Edw_File__R_Field_9 = pnd_Edw_File__R_Field_7.newGroupInGroup("pnd_Edw_File__R_Field_9", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Subplan);
        pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Mlob = pnd_Edw_File__R_Field_9.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Mlob", "#EDW-DTL-SUBPLAN-MLOB", 
            FieldType.STRING, 2);
        pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Slob = pnd_Edw_File__R_Field_9.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Slob", "#EDW-DTL-SUBPLAN-SLOB", 
            FieldType.STRING, 1);

        pnd_Edw_File__R_Field_10 = pnd_Edw_File__R_Field_7.newGroupInGroup("pnd_Edw_File__R_Field_10", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Subplan);
        pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob = pnd_Edw_File__R_Field_10.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob", "#EDW-DTL-SUBPLAN-LOB", 
            FieldType.STRING, 3);
        pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No", "#EDW-DTL-TIAA-NO", FieldType.STRING, 
            10);

        pnd_Edw_File__R_Field_11 = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newGroupInGroup("pnd_Edw_File__R_Field_11", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No);
        pnd_Edw_File_Pnd_Edw_Dtl_Cntrct_Prefix = pnd_Edw_File__R_Field_11.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Cntrct_Prefix", "#EDW-DTL-CNTRCT-PREFIX", 
            FieldType.STRING, 1);
        pnd_Edw_File_Pnd_Edw_Dtl_Cref_No = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Cref_No", "#EDW-DTL-CREF-NO", FieldType.STRING, 
            10);
        pnd_Edw_File_Pnd_Edw_Dtl_Dflt_Enroll_Ind = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Dflt_Enroll_Ind", "#EDW-DTL-DFLT-ENROLL-IND", 
            FieldType.STRING, 1);
        pnd_Edw_File_Pnd_Edw_Dtl_Funded_Ind = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Funded_Ind", "#EDW-DTL-FUNDED-IND", 
            FieldType.STRING, 1);
        pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Dte = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Dte", "#EDW-DTL-INIT-FUNDED-DTE", 
            FieldType.STRING, 8);

        pnd_Edw_File__R_Field_12 = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newGroupInGroup("pnd_Edw_File__R_Field_12", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Dte);
        pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Date = pnd_Edw_File__R_Field_12.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Date", "#EDW-DTL-INIT-FUNDED-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Edw_File_Pnd_Edw_Dtl_Control_Dte = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Control_Dte", "#EDW-DTL-CONTROL-DTE", 
            FieldType.STRING, 8);

        pnd_Edw_File__R_Field_13 = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newGroupInGroup("pnd_Edw_File__R_Field_13", "REDEFINE", pnd_Edw_File_Pnd_Edw_Dtl_Control_Dte);
        pnd_Edw_File_Pnd_Edw_Dtl_Control_Date = pnd_Edw_File__R_Field_13.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Control_Date", "#EDW-DTL-CONTROL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type", "#EDW-DTL-RECON-TYPE", 
            FieldType.STRING, 1);
        pnd_Edw_File_Pnd_Edw_Dtl_Category = pnd_Edw_File_Pnd_Edw_Dtl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Dtl_Category", "#EDW-DTL-CATEGORY", FieldType.STRING, 
            1);

        pnd_Edw_File__R_Field_14 = localVariables.newGroupInRecord("pnd_Edw_File__R_Field_14", "REDEFINE", pnd_Edw_File);

        pnd_Edw_File_Pnd_Edw_Trl_Rec = pnd_Edw_File__R_Field_14.newGroupInGroup("pnd_Edw_File_Pnd_Edw_Trl_Rec", "#EDW-TRL-REC");
        pnd_Edw_File_Pnd_Edw_Trl_Rec_Id = pnd_Edw_File_Pnd_Edw_Trl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Trl_Rec_Id", "#EDW-TRL-REC-ID", FieldType.STRING, 
            1);
        pnd_Edw_File_Pnd_Edw_Trl_Control_Dte = pnd_Edw_File_Pnd_Edw_Trl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Trl_Control_Dte", "#EDW-TRL-CONTROL-DTE", 
            FieldType.STRING, 8);
        pnd_Edw_File_Pnd_Edw_Trl_Cntrl_Count = pnd_Edw_File_Pnd_Edw_Trl_Rec.newFieldInGroup("pnd_Edw_File_Pnd_Edw_Trl_Cntrl_Count", "#EDW-TRL-CNTRL-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No = localVariables.newFieldInRecord("pnd_Rc_Cntrl_Type_Cat_Tiaa_No", "#RC-CNTRL-TYPE-CAT-TIAA-NO", FieldType.STRING, 
            20);

        pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15 = localVariables.newGroupInRecord("pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15", "REDEFINE", pnd_Rc_Cntrl_Type_Cat_Tiaa_No);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt", 
            "#RC-CONTROL-DT", FieldType.STRING, 8);

        pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_16 = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15.newGroupInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_16", 
            "REDEFINE", pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_16.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date", 
            "#RC-CONTROL-DATE", FieldType.NUMERIC, 8);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type", 
            "#RC-RECONCILE-TYPE", FieldType.STRING, 1);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category", 
            "#RC-RECONCILE-CATEGORY", FieldType.STRING, 1);
        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct = pnd_Rc_Cntrl_Type_Cat_Tiaa_No__R_Field_15.newFieldInGroup("pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct", 
            "#RC-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_New_Enrollment = localVariables.newFieldInRecord("pnd_New_Enrollment", "#NEW-ENROLLMENT", FieldType.BOOLEAN, 1);
        pnd_Newly_Funded = localVariables.newFieldInRecord("pnd_Newly_Funded", "#NEWLY-FUNDED", FieldType.BOOLEAN, 1);
        pnd_Reprint_Rec_Found = localVariables.newFieldInRecord("pnd_Reprint_Rec_Found", "#REPRINT-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Control_Date_Found = localVariables.newFieldInRecord("pnd_Control_Date_Found", "#CONTROL-DATE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Abort_Processing = localVariables.newFieldInRecord("pnd_Abort_Processing", "#ABORT-PROCESSING", FieldType.BOOLEAN, 1);
        pnd_Exception_Processing = localVariables.newFieldInRecord("pnd_Exception_Processing", "#EXCEPTION-PROCESSING", FieldType.BOOLEAN, 1);
        pnd_Expctd_Legal_Mail_Dte = localVariables.newFieldInRecord("pnd_Expctd_Legal_Mail_Dte", "#EXPCTD-LEGAL-MAIL-DTE", FieldType.DATE);
        pnd_Current_Dte = localVariables.newFieldInRecord("pnd_Current_Dte", "#CURRENT-DTE", FieldType.DATE);
        pnd_Current_Dte_N = localVariables.newFieldInRecord("pnd_Current_Dte_N", "#CURRENT-DTE-N", FieldType.NUMERIC, 8);
        pnd_Header_Control_Dte = localVariables.newFieldInRecord("pnd_Header_Control_Dte", "#HEADER-CONTROL-DTE", FieldType.STRING, 8);
        pnd_Dte = localVariables.newFieldInRecord("pnd_Dte", "#DTE", FieldType.NUMERIC, 8);

        pnd_Dte__R_Field_17 = localVariables.newGroupInRecord("pnd_Dte__R_Field_17", "REDEFINE", pnd_Dte);
        pnd_Dte_Pnd_Dte2 = pnd_Dte__R_Field_17.newFieldInGroup("pnd_Dte_Pnd_Dte2", "#DTE2", FieldType.STRING, 8);
        pnd_From_Dte = localVariables.newFieldInRecord("pnd_From_Dte", "#FROM-DTE", FieldType.DATE);
        pnd_Rp_Extracted_Date = localVariables.newFieldInRecord("pnd_Rp_Extracted_Date", "#RP-EXTRACTED-DATE", FieldType.NUMERIC, 8);

        pnd_Rp_Extracted_Date__R_Field_18 = localVariables.newGroupInRecord("pnd_Rp_Extracted_Date__R_Field_18", "REDEFINE", pnd_Rp_Extracted_Date);
        pnd_Rp_Extracted_Date_Pnd_Rp_Extracted_Dte = pnd_Rp_Extracted_Date__R_Field_18.newFieldInGroup("pnd_Rp_Extracted_Date_Pnd_Rp_Extracted_Dte", "#RP-EXTRACTED-DTE", 
            FieldType.STRING, 8);
        pnd_Rp_Xtrct_Dte = localVariables.newFieldInRecord("pnd_Rp_Xtrct_Dte", "#RP-XTRCT-DTE", FieldType.DATE);
        pnd_No_Days = localVariables.newFieldInRecord("pnd_No_Days", "#NO-DAYS", FieldType.NUMERIC, 4);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Ttl_Enroll_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Read", "#TTL-ENROLL-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt", "#TTL-ENROLL-WITH-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt", "#TTL-ENROLL-NO-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Enroll_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Deleted", "#TTL-ENROLL-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Enroll_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Not_Found", "#TTL-ENROLL-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable", "#TTL-ENROLL-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Read", "#TTL-FUNDED-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt", "#TTL-FUNDED-WITH-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt", "#TTL-FUNDED-NO-LEGAL-DT", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Deleted", "#TTL-FUNDED-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Not_Found", "#TTL-FUNDED-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Not_Applicable", "#TTL-FUNDED-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt", "#TTL-FUNDED-DFLT-NO-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_Pending = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Pending", "#TTL-FUNDED-PENDING", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Header_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Header_Found", "#TTL-HEADER-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Trailer_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Trailer_Found", "#TTL-TRAILER-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Read", "#TTLX-ENROLL-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt", "#TTLX-ENROLL-WITH-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt", "#TTLX-ENROLL-NO-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Enroll_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Deleted", "#TTLX-ENROLL-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Not_Found", "#TTLX-ENROLL-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable", "#TTLX-ENROLL-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Read", "#TTLX-FUNDED-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt", "#TTLX-FUNDED-WITH-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt", "#TTLX-FUNDED-NO-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Deleted", "#TTLX-FUNDED-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Funded_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Not_Found", "#TTLX-FUNDED-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable", "#TTLX-FUNDED-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt", "#TTLX-FUNDED-DFLT-NO-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Pending = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Pending", "#TTLX-FUNDED-PENDING", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Var_Reconcile_Type = localVariables.newFieldInRecord("pnd_Var_Reconcile_Type", "#VAR-RECONCILE-TYPE", FieldType.STRING, 1);
        pnd_Var_Control_Enroll_Cnt = localVariables.newFieldInRecord("pnd_Var_Control_Enroll_Cnt", "#VAR-CONTROL-ENROLL-CNT", FieldType.NUMERIC, 8);
        pnd_Var_Control_Funded_Cnt = localVariables.newFieldInRecord("pnd_Var_Control_Funded_Cnt", "#VAR-CONTROL-FUNDED-CNT", FieldType.NUMERIC, 8);

        pnd_Var_Group = localVariables.newGroupInRecord("pnd_Var_Group", "#VAR-GROUP");
        pnd_Var_Group_Pnd_Var_Category = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Category", "#VAR-CATEGORY", FieldType.STRING, 1);
        pnd_Var_Group_Pnd_Var_Exception_Ind = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Exception_Ind", "#VAR-EXCEPTION-IND", FieldType.STRING, 
            1);
        pnd_Var_Group_Pnd_Var_Exception_Dte = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Exception_Dte", "#VAR-EXCEPTION-DTE", FieldType.NUMERIC, 
            8);
        pnd_Var_Group_Pnd_Var_Reprint_Dte = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Reprint_Dte", "#VAR-REPRINT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Var_Group_Pnd_Var_Delete_Dte = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Delete_Dte", "#VAR-DELETE-DTE", FieldType.NUMERIC, 8);
        pnd_Var_Group_Pnd_Var_Delete_User = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Delete_User", "#VAR-DELETE-USER", FieldType.STRING, 16);
        pnd_Var_Group_Pnd_Var_Delete_Reason = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Delete_Reason", "#VAR-DELETE-REASON", FieldType.STRING, 
            1);
        pnd_Var_Group_Pnd_Var_Welcome_Mail_Dte = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Welcome_Mail_Dte", "#VAR-WELCOME-MAIL-DTE", FieldType.NUMERIC, 
            8);
        pnd_Var_Group_Pnd_Var_Legal_Mail_Dte = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Legal_Mail_Dte", "#VAR-LEGAL-MAIL-DTE", FieldType.NUMERIC, 
            8);
        pnd_Var_Group_Pnd_Var_Package_Mail_Type = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Package_Mail_Type", "#VAR-PACKAGE-MAIL-TYPE", FieldType.STRING, 
            1);
        pnd_Var_Group_Pnd_Var_Resolved_Dte = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Resolved_Dte", "#VAR-RESOLVED-DTE", FieldType.NUMERIC, 
            8);
        pnd_Var_Group_Pnd_Var_Old_Category = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Old_Category", "#VAR-OLD-CATEGORY", FieldType.STRING, 
            1);
        pnd_Var_Group_Pnd_Var_Excptn_Age_Days = pnd_Var_Group.newFieldInGroup("pnd_Var_Group_Pnd_Var_Excptn_Age_Days", "#VAR-EXCPTN-AGE-DAYS", FieldType.NUMERIC, 
            7);
        pnd_Work_Rpt_File = localVariables.newFieldInRecord("pnd_Work_Rpt_File", "#WORK-RPT-FILE", FieldType.STRING, 150);

        pnd_Work_Rpt_File__R_Field_19 = localVariables.newGroupInRecord("pnd_Work_Rpt_File__R_Field_19", "REDEFINE", pnd_Work_Rpt_File);
        pnd_Work_Rpt_File_Pnd_Wk_Rec_Id = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Rec_Id", "#WK-REC-ID", FieldType.STRING, 
            1);
        pnd_Work_Rpt_File_Pnd_Wk_Rec_Type = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Rec_Type", "#WK-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Work_Rpt_File_Pnd_Wk_Category_Ind = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Category_Ind", "#WK-CATEGORY-IND", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No", "#WK-TIAA-NO", FieldType.STRING, 
            10);
        pnd_Work_Rpt_File_Pnd_Wk_Cref_No = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Cref_No", "#WK-CREF-NO", FieldType.STRING, 
            10);
        pnd_Work_Rpt_File_Pnd_Wk_Plan = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Plan", "#WK-PLAN", FieldType.STRING, 6);
        pnd_Work_Rpt_File_Pnd_Wk_Subplan = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Subplan", "#WK-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Work_Rpt_File_Pnd_Wk_Ssn = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Ssn", "#WK-SSN", FieldType.STRING, 9);

        pnd_Work_Rpt_File__R_Field_20 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_20", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Ssn);
        pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec = pnd_Work_Rpt_File__R_Field_20.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec", "#WK-SOC-SEC", FieldType.NUMERIC, 
            9);
        pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind", "#WK-FUNDED-IND", FieldType.STRING, 
            1);
        pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte", "#WK-INIT-FUNDED-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_21 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_21", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Date = pnd_Work_Rpt_File__R_Field_21.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Date", "#WK-INIT-FUNDED-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte", "#WK-REPRINT-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_22 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_22", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Reprint_Date = pnd_Work_Rpt_File__R_Field_22.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Reprint_Date", "#WK-REPRINT-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte", "#WK-DELETE-DTE", FieldType.STRING, 
            8);

        pnd_Work_Rpt_File__R_Field_23 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_23", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Date = pnd_Work_Rpt_File__R_Field_23.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Date", "#WK-DELETE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_User = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_User", "#WK-DELETE-USER", 
            FieldType.STRING, 16);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason", "#WK-DELETE-REASON", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte", "#WK-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_24 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_24", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date = pnd_Work_Rpt_File__R_Field_24.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date", "#WK-WELCOME-MAIL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte", "#WK-LEGAL-MAIL-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_25 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_25", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date = pnd_Work_Rpt_File__R_Field_25.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date", "#WK-LEGAL-MAIL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Control_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Control_Dte", "#WK-CONTROL-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_26 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_26", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Control_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Control_Date = pnd_Work_Rpt_File__R_Field_26.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Control_Date", "#WK-CONTROL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll", "#WK-DEFAULT-ENROLL", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type", "#WK-PACKAGE-MAIL-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Old_Category = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Old_Category", "#WK-OLD-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte", "#WK-EXCEPTION-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_27 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_27", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Exception_Date = pnd_Work_Rpt_File__R_Field_27.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Exception_Date", "#WK-EXCEPTION-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte", "#WK-RESOLVED-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_28 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_28", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Resolved_Date = pnd_Work_Rpt_File__R_Field_28.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Resolved_Date", "#WK-RESOLVED-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days = pnd_Work_Rpt_File__R_Field_19.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days", "#WK-EXCEPTION-AGE-DAYS", 
            FieldType.STRING, 7);

        pnd_Work_Rpt_File__R_Field_29 = pnd_Work_Rpt_File__R_Field_19.newGroupInGroup("pnd_Work_Rpt_File__R_Field_29", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days);
        pnd_Work_Rpt_File_Pnd_Wk_Excptn_Age_Days = pnd_Work_Rpt_File__R_Field_29.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Excptn_Age_Days", "#WK-EXCPTN-AGE-DAYS", 
            FieldType.NUMERIC, 7);
        pnd_Wk_Control_File = localVariables.newFieldInRecord("pnd_Wk_Control_File", "#WK-CONTROL-FILE", FieldType.STRING, 50);

        pnd_Wk_Control_File__R_Field_30 = localVariables.newGroupInRecord("pnd_Wk_Control_File__R_Field_30", "REDEFINE", pnd_Wk_Control_File);
        pnd_Wk_Control_File_Pnd_Wk_Cntrl_Dte = pnd_Wk_Control_File__R_Field_30.newFieldInGroup("pnd_Wk_Control_File_Pnd_Wk_Cntrl_Dte", "#WK-CNTRL-DTE", 
            FieldType.STRING, 8);
        pnd_Input_Implementation_Dte = localVariables.newFieldInRecord("pnd_Input_Implementation_Dte", "#INPUT-IMPLEMENTATION-DTE", FieldType.STRING, 
            8);

        pnd_Input_Implementation_Dte__R_Field_31 = localVariables.newGroupInRecord("pnd_Input_Implementation_Dte__R_Field_31", "REDEFINE", pnd_Input_Implementation_Dte);
        pnd_Input_Implementation_Dte_Pnd_Input_Implementation_Date = pnd_Input_Implementation_Dte__R_Field_31.newFieldInGroup("pnd_Input_Implementation_Dte_Pnd_Input_Implementation_Date", 
            "#INPUT-IMPLEMENTATION-DATE", FieldType.NUMERIC, 8);

        pnd_Suppress_Table = localVariables.newGroupArrayInRecord("pnd_Suppress_Table", "#SUPPRESS-TABLE", new DbsArrayController(1, 300));
        pnd_Suppress_Table_Pnd_S_Plan_Subplan = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Plan_Subplan", "#S-PLAN-SUBPLAN", FieldType.STRING, 
            12);

        pnd_Suppress_Table__R_Field_32 = pnd_Suppress_Table.newGroupInGroup("pnd_Suppress_Table__R_Field_32", "REDEFINE", pnd_Suppress_Table_Pnd_S_Plan_Subplan);
        pnd_Suppress_Table_Pnd_S_Plan_Number = pnd_Suppress_Table__R_Field_32.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Plan_Number", "#S-PLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Suppress_Table_Pnd_S_Subplan_Number = pnd_Suppress_Table__R_Field_32.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Subplan_Number", "#S-SUBPLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Suppress_Table_Pnd_S_Pckg_Type = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Pckg_Type", "#S-PCKG-TYPE", FieldType.STRING, 
            1);
        pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type", "#S-PKG-CNTRL-TYPE", FieldType.STRING, 
            1);
        pnd_Suppress_Table_Pnd_S_Enr_Source = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Enr_Source", "#S-ENR-SOURCE", FieldType.STRING, 
            1);
        pnd_I_S = localVariables.newFieldInRecord("pnd_I_S", "#I-S", FieldType.NUMERIC, 3);
        pnd_Ps_Key = localVariables.newFieldInRecord("pnd_Ps_Key", "#PS-KEY", FieldType.STRING, 29);

        pnd_Ps_Key__R_Field_33 = localVariables.newGroupInRecord("pnd_Ps_Key__R_Field_33", "REDEFINE", pnd_Ps_Key);
        pnd_Ps_Key_Pnd_Ps_Entry_Actve_Ind = pnd_Ps_Key__R_Field_33.newFieldInGroup("pnd_Ps_Key_Pnd_Ps_Entry_Actve_Ind", "#PS-ENTRY-ACTVE-IND", FieldType.STRING, 
            1);
        pnd_Ps_Key_Pnd_Ps_Entry_Table_Id_Nbr = pnd_Ps_Key__R_Field_33.newFieldInGroup("pnd_Ps_Key_Pnd_Ps_Entry_Table_Id_Nbr", "#PS-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Ps_Key_Pnd_Ps_Entry_Table_Sub_Id = pnd_Ps_Key__R_Field_33.newFieldInGroup("pnd_Ps_Key_Pnd_Ps_Entry_Table_Sub_Id", "#PS-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Ps_Key_Pnd_Ps_Entry_Cde = pnd_Ps_Key__R_Field_33.newFieldInGroup("pnd_Ps_Key_Pnd_Ps_Entry_Cde", "#PS-ENTRY-CDE", FieldType.STRING, 20);
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.NUMERIC, 3);
        pnd_Suppress_Welcome_Pkg = localVariables.newFieldInRecord("pnd_Suppress_Welcome_Pkg", "#SUPPRESS-WELCOME-PKG", FieldType.BOOLEAN, 1);
        pnd_Suppress_Legal_Pkg = localVariables.newFieldInRecord("pnd_Suppress_Legal_Pkg", "#SUPPRESS-LEGAL-PKG", FieldType.BOOLEAN, 1);
        pnd_Blank_Enr_Source = localVariables.newFieldInRecord("pnd_Blank_Enr_Source", "#BLANK-ENR-SOURCE", FieldType.BOOLEAN, 1);
        pnd_Entry_Enr_Source = localVariables.newFieldInRecord("pnd_Entry_Enr_Source", "#ENTRY-ENR-SOURCE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_table_Entry.reset();

        ldaAppl180.initializeValues();
        ldaAppl530.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb530() throws Exception
    {
        super("Appb530");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Appb530|Main");
        OnErrorManager.pushEvent("APPB530", onError);
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=M"),pnd_Input_Implementation_Dte);                                            //Natural: INPUT ( AD = M ) #INPUT-IMPLEMENTATION-DTE
                getReports().write(0, "IMPLEMENTATION DATE:",pnd_Input_Implementation_Dte);                                                                               //Natural: WRITE 'IMPLEMENTATION DATE:' #INPUT-IMPLEMENTATION-DTE
                if (Global.isEscape()) return;
                pnd_Current_Dte.setValue(Global.getDATX());                                                                                                               //Natural: ASSIGN #CURRENT-DTE := *DATX
                pnd_Current_Dte_N.setValue(Global.getDATN());                                                                                                             //Natural: ASSIGN #CURRENT-DTE-N := *DATN
                pnd_New_Enrollment.reset();                                                                                                                               //Natural: RESET #NEW-ENROLLMENT #NEWLY-FUNDED #HEADER-CONTROL-DTE #EXCEPTION-PROCESSING
                pnd_Newly_Funded.reset();
                pnd_Header_Control_Dte.reset();
                pnd_Exception_Processing.reset();
                pnd_Counters.reset();                                                                                                                                     //Natural: RESET #COUNTERS
                //*  MFO
                                                                                                                                                                          //Natural: PERFORM LOAD-PACKAGE-SUPPRESSED-PLANS-SUBPLANS
                sub_Load_Package_Suppressed_Plans_Subplans();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  READ THE FOLLOWING INPUT FILES:
                //*  1) NEW ENROLLMENT FILE FROM EDW
                //*  2) NEWLY FUNDED CONTRACTS FROM EDW
                //*  3) OUTSTANDING EXCEPTIONS
                RD1:                                                                                                                                                      //Natural: READ WORK 1 #EDW-FILE
                while (condition(getWorkFiles().read(1, pnd_Edw_File)))
                {
                    short decideConditionsMet779 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #EDW-REC-ID;//Natural: VALUE '0'
                    if (condition((pnd_Edw_File_Pnd_Edw_Rec_Id.equals("0"))))
                    {
                        decideConditionsMet779++;
                        pnd_Var_Reconcile_Type.reset();                                                                                                                   //Natural: RESET #VAR-RECONCILE-TYPE #VAR-CONTROL-ENROLL-CNT #VAR-CONTROL-FUNDED-CNT #NEW-ENROLLMENT #NEWLY-FUNDED
                        pnd_Var_Control_Enroll_Cnt.reset();
                        pnd_Var_Control_Funded_Cnt.reset();
                        pnd_New_Enrollment.reset();
                        pnd_Newly_Funded.reset();
                        pnd_Counters_Pnd_Ttl_Header_Found.nadd(1);                                                                                                        //Natural: ADD 1 TO #TTL-HEADER-FOUND
                        pnd_Header_Control_Dte.setValue(pnd_Edw_File_Pnd_Edw_Hdr_Control_Dte);                                                                            //Natural: ASSIGN #HEADER-CONTROL-DTE := #EDW-HDR-CONTROL-DTE
                        //*  ENROLLMENT
                        if (condition(pnd_Edw_File_Pnd_Edw_Hdr_File_Typ.equals("E")))                                                                                     //Natural: IF #EDW-HDR-FILE-TYP = 'E'
                        {
                            pnd_New_Enrollment.setValue(true);                                                                                                            //Natural: ASSIGN #NEW-ENROLLMENT := TRUE
                            pnd_Var_Reconcile_Type.setValue("W");                                                                                                         //Natural: ASSIGN #VAR-RECONCILE-TYPE := 'W'
                            pnd_Var_Control_Enroll_Cnt.setValue(pnd_Edw_File_Pnd_Edw_Hdr_Cntrl_Count);                                                                    //Natural: ASSIGN #VAR-CONTROL-ENROLL-CNT := #EDW-HDR-CNTRL-COUNT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  NEWLY FUNDED
                            if (condition(pnd_Edw_File_Pnd_Edw_Hdr_File_Typ.equals("F")))                                                                                 //Natural: IF #EDW-HDR-FILE-TYP = 'F'
                            {
                                pnd_Newly_Funded.setValue(true);                                                                                                          //Natural: ASSIGN #NEWLY-FUNDED := TRUE
                                pnd_Var_Reconcile_Type.setValue("L");                                                                                                     //Natural: ASSIGN #VAR-RECONCILE-TYPE := 'L'
                                pnd_Var_Control_Funded_Cnt.setValue(pnd_Edw_File_Pnd_Edw_Hdr_Cntrl_Count);                                                                //Natural: ASSIGN #VAR-CONTROL-FUNDED-CNT := #EDW-HDR-CNTRL-COUNT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Wk_Control_File_Pnd_Wk_Cntrl_Dte.setValue(pnd_Edw_File_Pnd_Edw_Hdr_Control_Dte);                                                              //Natural: ASSIGN #WK-CNTRL-DTE := #EDW-HDR-CONTROL-DTE
                        getWorkFiles().write(3, false, pnd_Wk_Control_File);                                                                                              //Natural: WRITE WORK FILE 3 #WK-CONTROL-FILE
                        //*  CHECK IF CONTROL DATE EXIST IN THE ACIS-RECONCILE-FL
                        pnd_Rc_Cntrl_Type_Cat_Tiaa_No.reset();                                                                                                            //Natural: RESET #RC-CNTRL-TYPE-CAT-TIAA-NO #CONTROL-DATE-FOUND
                        pnd_Control_Date_Found.reset();
                        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Dt.setValue(pnd_Edw_File_Pnd_Edw_Hdr_Control_Dte);                                                   //Natural: ASSIGN #RC-CONTROL-DT := #EDW-HDR-CONTROL-DTE
                        pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type.setValue(pnd_Var_Reconcile_Type);                                                             //Natural: ASSIGN #RC-RECONCILE-TYPE := #VAR-RECONCILE-TYPE
                        ldaAppl530.getVw_acis_Reconcile_Fl_View().startDatabaseRead                                                                                       //Natural: READ ( 1 ) ACIS-RECONCILE-FL-VIEW BY RC-CNTRL-TYPE-CAT-TIAA-NO STARTING FROM #RC-CNTRL-TYPE-CAT-TIAA-NO
                        (
                        "READ01",
                        new Wc[] { new Wc("RC_CNTRL_TYPE_CAT_TIAA_NO", ">=", pnd_Rc_Cntrl_Type_Cat_Tiaa_No, WcType.BY) },
                        new Oc[] { new Oc("RC_CNTRL_TYPE_CAT_TIAA_NO", "ASC") },
                        1
                        );
                        READ01:
                        while (condition(ldaAppl530.getVw_acis_Reconcile_Fl_View().readNextRow("READ01")))
                        {
                            if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Control_Dt().equals(pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date)                  //Natural: IF RC-CONTROL-DT = #RC-CONTROL-DATE AND RC-RECONCILE-TYPE = #RC-RECONCILE-TYPE
                                && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Type().equals(pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type)))
                            {
                                pnd_Control_Date_Found.setValue(true);                                                                                                    //Natural: ASSIGN #CONTROL-DATE-FOUND := TRUE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ABORT PROCESSING
                        if (condition(pnd_Control_Date_Found.getBoolean()))                                                                                               //Natural: IF #CONTROL-DATE-FOUND
                        {
                            getReports().write(0, "*********************************************************",NEWLINE,"*   WARNING: CONTROL DATE EXISTS IN ACIS-RECONCILE-FL   *", //Natural: WRITE '*********************************************************' / '*   WARNING: CONTROL DATE EXISTS IN ACIS-RECONCILE-FL   *' / '*                CONTACT SYSTEMS                        *' / '*********************************************************' / 'CONTROL DATE: ' #EDW-HDR-CONTROL-DTE
                                NEWLINE,"*                CONTACT SYSTEMS                        *",NEWLINE,"*********************************************************",
                                NEWLINE,"CONTROL DATE: ",pnd_Edw_File_Pnd_Edw_Hdr_Control_Dte);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Abort_Processing.setValue(true);                                                                                                          //Natural: ASSIGN #ABORT-PROCESSING := TRUE
                            if (true) break RD1;                                                                                                                          //Natural: ESCAPE BOTTOM ( RD1. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE '1'
                    else if (condition((pnd_Edw_File_Pnd_Edw_Rec_Id.equals("1"))))
                    {
                        decideConditionsMet779++;
                        if (condition(pnd_Header_Control_Dte.equals(pnd_Edw_File_Pnd_Edw_Dtl_Control_Dte)))                                                               //Natural: IF #HEADER-CONTROL-DTE = #EDW-DTL-CONTROL-DTE
                        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-DETAIL-REC
                            sub_Process_Detail_Rec();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  HEADER RECORD IS MISSING
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, "<<< WARNING: EDW HEADER CONTROL DO NOT MATCH DETAIL CONTROL RECORD >>>",NEWLINE,"HEADER CONTROL DATE :",               //Natural: WRITE '<<< WARNING: EDW HEADER CONTROL DO NOT MATCH DETAIL CONTROL RECORD >>>' / 'HEADER CONTROL DATE :' #HEADER-CONTROL-DTE / 'DETAIL CONTROL DATE :' #EDW-DTL-CONTROL-DTE
                                pnd_Header_Control_Dte,NEWLINE,"DETAIL CONTROL DATE :",pnd_Edw_File_Pnd_Edw_Dtl_Control_Dte);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Abort_Processing.setValue(true);                                                                                                          //Natural: ASSIGN #ABORT-PROCESSING := TRUE
                            if (true) break RD1;                                                                                                                          //Natural: ESCAPE BOTTOM ( RD1. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE '2'
                    else if (condition((pnd_Edw_File_Pnd_Edw_Rec_Id.equals("2"))))
                    {
                        decideConditionsMet779++;
                        pnd_Header_Control_Dte.reset();                                                                                                                   //Natural: RESET #HEADER-CONTROL-DTE
                        pnd_Counters_Pnd_Ttl_Trailer_Found.nadd(1);                                                                                                       //Natural: ADD 1 TO #TTL-TRAILER-FOUND
                        if (condition(pnd_New_Enrollment.getBoolean()))                                                                                                   //Natural: IF #NEW-ENROLLMENT
                        {
                            if (condition(pnd_Counters_Pnd_Ttl_Enroll_Read.equals(pnd_Var_Control_Enroll_Cnt)))                                                           //Natural: IF #TTL-ENROLL-READ = #VAR-CONTROL-ENROLL-CNT
                            {
                                getReports().write(0, "<<< SUCCESSFUL : ENROLLMENT CONTROLS MATCH >>>",NEWLINE,"CONTROL CNT :",pnd_Var_Control_Enroll_Cnt,                //Natural: WRITE '<<< SUCCESSFUL : ENROLLMENT CONTROLS MATCH >>>' / 'CONTROL CNT :' #VAR-CONTROL-ENROLL-CNT ( EM = Z,ZZZ,ZZ9 ) / 'DETAIL CNT  :' #TTL-ENROLL-READ ( EM = Z,ZZZ,ZZ9 )
                                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"DETAIL CNT  :",pnd_Counters_Pnd_Ttl_Enroll_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RD1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().write(0, "<<< WARNING : ENROLLMENT CONTROLS DO NOT MATCH >>>",NEWLINE,"CONTROL CNT :",pnd_Var_Control_Enroll_Cnt,            //Natural: WRITE '<<< WARNING : ENROLLMENT CONTROLS DO NOT MATCH >>>' / 'CONTROL CNT :' #VAR-CONTROL-ENROLL-CNT ( EM = Z,ZZZ,ZZ9 ) / 'DETAIL CNT  :' #TTL-ENROLL-READ ( EM = Z,ZZZ,ZZ9 )
                                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"DETAIL CNT  :",pnd_Counters_Pnd_Ttl_Enroll_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("RD1"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Abort_Processing.setValue(true);                                                                                                      //Natural: ASSIGN #ABORT-PROCESSING := TRUE
                                if (true) break RD1;                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Newly_Funded.getBoolean()))                                                                                                 //Natural: IF #NEWLY-FUNDED
                            {
                                if (condition(pnd_Counters_Pnd_Ttl_Funded_Read.equals(pnd_Var_Control_Funded_Cnt)))                                                       //Natural: IF #TTL-FUNDED-READ = #VAR-CONTROL-FUNDED-CNT
                                {
                                    getReports().write(0, "<<< SUCCESSFUL : NEWLY FUNDED CONTROLS MATCH >>>",NEWLINE,"CONTROL CNT :",pnd_Var_Control_Funded_Cnt,          //Natural: WRITE '<<< SUCCESSFUL : NEWLY FUNDED CONTROLS MATCH >>>' / 'CONTROL CNT :' #VAR-CONTROL-FUNDED-CNT ( EM = Z,ZZZ,ZZ9 ) / 'DETAIL CNT  :' #TTL-FUNDED-READ ( EM = Z,ZZZ,ZZ9 )
                                        new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"DETAIL CNT  :",pnd_Counters_Pnd_Ttl_Funded_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("RD1"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    getReports().write(0, "<<< WARNING : NEWLY FUNDED CONTROLS DO NOT MATCH >>>",NEWLINE,"CONTROL CNT :",pnd_Var_Control_Funded_Cnt,      //Natural: WRITE '<<< WARNING : NEWLY FUNDED CONTROLS DO NOT MATCH >>>' / 'CONTROL CNT :' #VAR-CONTROL-FUNDED-CNT ( EM = Z,ZZZ,ZZ9 ) / 'DETAIL CNT  :' #TTL-FUNDED-READ ( EM = Z,ZZZ,ZZ9 )
                                        new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"DETAIL CNT  :",pnd_Counters_Pnd_Ttl_Funded_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("RD1"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_Abort_Processing.setValue(true);                                                                                                  //Natural: ASSIGN #ABORT-PROCESSING := TRUE
                                    if (true) break RD1;                                                                                                                  //Natural: ESCAPE BOTTOM ( RD1. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            //*  OUTSTANDING EXCEPTIONS
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE '3'
                    else if (condition((pnd_Edw_File_Pnd_Edw_Rec_Id.equals("3"))))
                    {
                        decideConditionsMet779++;
                        pnd_Exception_Processing.setValue(true);                                                                                                          //Natural: ASSIGN #EXCEPTION-PROCESSING := TRUE
                        pnd_Newly_Funded.setValue(false);                                                                                                                 //Natural: ASSIGN #NEWLY-FUNDED := FALSE
                        pnd_New_Enrollment.setValue(false);                                                                                                               //Natural: ASSIGN #NEW-ENROLLMENT := FALSE
                        pnd_Var_Reconcile_Type.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type);                                                                             //Natural: ASSIGN #VAR-RECONCILE-TYPE := #EDW-DTL-RECON-TYPE
                        //*  NEW ENROLL.
                        if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type.equals("W")))                                                                                   //Natural: IF #EDW-DTL-RECON-TYPE = 'W'
                        {
                            pnd_New_Enrollment.setValue(true);                                                                                                            //Natural: ASSIGN #NEW-ENROLLMENT := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  NEWLY FUNDED
                            if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type.equals("L")))                                                                               //Natural: IF #EDW-DTL-RECON-TYPE = 'L'
                            {
                                pnd_Newly_Funded.setValue(true);                                                                                                          //Natural: ASSIGN #NEWLY-FUNDED := TRUE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-DETAIL-REC
                        sub_Process_Detail_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-WORK
                RD1_Exit:
                if (Global.isEscape()) return;
                if (condition(pnd_Abort_Processing.getBoolean()))                                                                                                         //Natural: IF #ABORT-PROCESSING
                {
                    DbsUtil.terminate(16);  if (true) return;                                                                                                             //Natural: TERMINATE 16
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROLS
                sub_Display_Controls();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *******************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PACKAGE-SUPPRESSED-PLANS-SUBPLANS
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DETAIL-REC
                //* *            MOVE EDITED #EDW-DTL-INIT-FUNDED-DTE TO
                //* *              #EXPCTD-LEGAL-MAIL-DTE (EM=YYYYMMDD)
                //* *            #NO-DAYS := #EXPCTD-LEGAL-MAIL-DTE - #RP-XTRCT-DTE
                //* *  WRITE 'FOUND' #EDW-DTL-TIAA-NO #VAR-CATEGORY
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-RECON-REC
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RECON-REC
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-FILE
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRACK-CONTROL-COUNTS
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-CONTROLS
                //* ***********************************************************************
                //* ***********************************************************************                                                                               //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*  MFO START
    private void sub_Load_Package_Suppressed_Plans_Subplans() throws Exception                                                                                            //Natural: LOAD-PACKAGE-SUPPRESSED-PLANS-SUBPLANS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pnd_Ps_Key.reset();                                                                                                                                               //Natural: RESET #PS-KEY #I-S
        pnd_I_S.reset();
        pnd_Ps_Key_Pnd_Ps_Entry_Actve_Ind.setValue("Y");                                                                                                                  //Natural: ASSIGN #PS-ENTRY-ACTVE-IND := 'Y'
        pnd_Ps_Key_Pnd_Ps_Entry_Table_Id_Nbr.setValue(310);                                                                                                               //Natural: ASSIGN #PS-ENTRY-TABLE-ID-NBR := 310
        pnd_Ps_Key_Pnd_Ps_Entry_Table_Sub_Id.setValue("PS");                                                                                                              //Natural: ASSIGN #PS-ENTRY-TABLE-SUB-ID := 'PS'
        vw_table_Entry.startDatabaseRead                                                                                                                                  //Natural: READ TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE FROM #PS-KEY
        (
        "READ02",
        new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Ps_Key, WcType.BY) },
        new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ02:
        while (condition(vw_table_Entry.readNextRow("READ02")))
        {
            if (condition(table_Entry_Entry_Actve_Ind.notEquals("Y") || table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Ps_Key_Pnd_Ps_Entry_Table_Id_Nbr)                    //Natural: IF ENTRY-ACTVE-IND NE 'Y' OR ENTRY-TABLE-ID-NBR NE #PS-ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #PS-ENTRY-TABLE-SUB-ID
                || table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Ps_Key_Pnd_Ps_Entry_Table_Sub_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(table_Entry_Entry_Pkg_Cntrl_Type.equals("S")))                                                                                                  //Natural: IF ENTRY-PKG-CNTRL-TYPE = 'S'
            {
                pnd_I_S.nadd(1);                                                                                                                                          //Natural: ASSIGN #I-S := #I-S + 1
                if (condition(pnd_I_S.greater(300)))                                                                                                                      //Natural: IF #I-S GT 300
                {
                    getReports().write(0, "*** MAX NO. OF 300 FOR PACKAGE SUPPRESS TABLE REACHED ***");                                                                   //Natural: WRITE '*** MAX NO. OF 300 FOR PACKAGE SUPPRESS TABLE REACHED ***'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(4);  if (true) return;                                                                                                              //Natural: TERMINATE 4
                }                                                                                                                                                         //Natural: END-IF
                pnd_Suppress_Table_Pnd_S_Plan_Number.getValue(pnd_I_S).setValue(table_Entry_Entry_Plan);                                                                  //Natural: ASSIGN #S-PLAN-NUMBER ( #I-S ) := ENTRY-PLAN
                pnd_Suppress_Table_Pnd_S_Subplan_Number.getValue(pnd_I_S).setValue(table_Entry_Entry_Sub_Plan_Number);                                                    //Natural: ASSIGN #S-SUBPLAN-NUMBER ( #I-S ) := ENTRY-SUB-PLAN-NUMBER
                pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type.getValue(pnd_I_S).setValue(table_Entry_Entry_Pkg_Cntrl_Type);                                                     //Natural: ASSIGN #S-PKG-CNTRL-TYPE ( #I-S ) := ENTRY-PKG-CNTRL-TYPE
                pnd_Suppress_Table_Pnd_S_Enr_Source.getValue(pnd_I_S).setValue(table_Entry_Entry_Enr_Source);                                                             //Natural: ASSIGN #S-ENR-SOURCE ( #I-S ) := ENTRY-ENR-SOURCE
                pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(pnd_I_S).setValue(table_Entry_Entry_Pckg_Type);                                                               //Natural: ASSIGN #S-PCKG-TYPE ( #I-S ) := ENTRY-PCKG-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  LOAD-PACKAGE-SUPPRESSED-PLANS-SUBPLANS /* MFO END
    }
    private void sub_Process_Detail_Rec() throws Exception                                                                                                                //Natural: PROCESS-DETAIL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_Var_Group.reset();                                                                                                                                            //Natural: RESET #VAR-GROUP
        pnd_Reprint_Rec_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #REPRINT-REC-FOUND := FALSE
        //*  CHECK IF PLAN-SUBPLAN IS ONE OF THOSE IN THE LIST OF SUPPRESSED PKGS
        //*  MFO START
        pnd_Indx.reset();                                                                                                                                                 //Natural: RESET #INDX
        pnd_Suppress_Welcome_Pkg.setValue(false);                                                                                                                         //Natural: ASSIGN #SUPPRESS-WELCOME-PKG := FALSE
        pnd_Suppress_Legal_Pkg.setValue(false);                                                                                                                           //Natural: ASSIGN #SUPPRESS-LEGAL-PKG := FALSE
        pnd_Blank_Enr_Source.setValue(true);                                                                                                                              //Natural: ASSIGN #BLANK-ENR-SOURCE := TRUE
        pnd_Entry_Enr_Source.setValue(" ");                                                                                                                               //Natural: ASSIGN #ENTRY-ENR-SOURCE := ' '
        DbsUtil.examine(new ExamineSource(pnd_Suppress_Table_Pnd_S_Plan_Subplan.getValue("*"),true), new ExamineSearch(pnd_Edw_File_Pnd_Edw_Dtl_Plan_Subplan,             //Natural: EXAMINE FULL #S-PLAN-SUBPLAN ( * ) FOR FULL VALUE OF #EDW-DTL-PLAN-SUBPLAN GIVING INDEX IN #INDX
            true), new ExamineGivingIndex(pnd_Indx));
        if (condition(pnd_Indx.greater(getZero())))                                                                                                                       //Natural: IF #INDX > 0
        {
            if (condition(pnd_Suppress_Table_Pnd_S_Enr_Source.getValue(pnd_Indx).notEquals(" ")))                                                                         //Natural: IF #S-ENR-SOURCE ( #INDX ) <> ' '
            {
                pnd_Blank_Enr_Source.setValue(false);                                                                                                                     //Natural: ASSIGN #BLANK-ENR-SOURCE := FALSE
                pnd_Entry_Enr_Source.setValue(pnd_Suppress_Table_Pnd_S_Enr_Source.getValue(pnd_Indx));                                                                    //Natural: ASSIGN #ENTRY-ENR-SOURCE := #S-ENR-SOURCE ( #INDX )
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet969 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #S-PCKG-TYPE ( #INDX );//Natural: VALUE 'W'
            if (condition((pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(pnd_Indx).equals("W"))))
            {
                decideConditionsMet969++;
                pnd_Suppress_Welcome_Pkg.setValue(true);                                                                                                                  //Natural: ASSIGN #SUPPRESS-WELCOME-PKG := TRUE
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(pnd_Indx).equals("L"))))
            {
                decideConditionsMet969++;
                pnd_Suppress_Legal_Pkg.setValue(true);                                                                                                                    //Natural: ASSIGN #SUPPRESS-LEGAL-PKG := TRUE
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(pnd_Indx).equals("B"))))
            {
                decideConditionsMet969++;
                pnd_Suppress_Welcome_Pkg.setValue(true);                                                                                                                  //Natural: ASSIGN #SUPPRESS-WELCOME-PKG := TRUE
                pnd_Suppress_Legal_Pkg.setValue(true);                                                                                                                    //Natural: ASSIGN #SUPPRESS-LEGAL-PKG := TRUE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MFO END
        //*  CHECK FOR "Not Applicable" RECORDS
        //*  NOT VALID
        if (condition(((((((((((pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Mlob.equals("TP") || pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Mlob.equals("IP")) || pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Mlob.equals("RH"))  //Natural: IF ( #EDW-DTL-SUBPLAN-MLOB = 'TP' OR = 'IP' OR = 'RH' ) OR ( #EDW-DTL-SUBPLAN-SLOB = 'M' ) OR ( #EDW-DTL-SUBPLAN-LOB = 'SRS' OR = 'GA6' OR = 'GA5' OR = 'RA8' ) OR ( #EDW-DTL-PLAN-A3 = 'SIP' ) OR ( #NEW-ENROLLMENT AND ( #EDW-DTL-SUBPLAN-LOB = 'RL1' OR #EDW-DTL-PLAN = '100825' ) ) OR ( #NEWLY-FUNDED AND #EDW-DTL-DFLT-ENROLL-IND = '2' ) OR ( #EDW-DTL-CNTRCT-PREFIX = 'A' OR = 'B' OR = 'C' ) OR ( #NEW-ENROLLMENT AND #SUPPRESS-WELCOME-PKG AND #BLANK-ENR-SOURCE ) OR ( #NEWLY-FUNDED AND #SUPPRESS-LEGAL-PKG AND #BLANK-ENR-SOURCE )
            || pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Slob.equals("M")) || (((pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob.equals("SRS") || pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob.equals("GA6")) 
            || pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob.equals("GA5")) || pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob.equals("RA8"))) || pnd_Edw_File_Pnd_Edw_Dtl_Plan_A3.equals("SIP")) 
            || (pnd_New_Enrollment.getBoolean() && (pnd_Edw_File_Pnd_Edw_Dtl_Subplan_Lob.equals("RL1") || pnd_Edw_File_Pnd_Edw_Dtl_Plan.equals("100825")))) 
            || (pnd_Newly_Funded.getBoolean() && pnd_Edw_File_Pnd_Edw_Dtl_Dflt_Enroll_Ind.equals("2"))) || ((pnd_Edw_File_Pnd_Edw_Dtl_Cntrct_Prefix.equals("A") 
            || pnd_Edw_File_Pnd_Edw_Dtl_Cntrct_Prefix.equals("B")) || pnd_Edw_File_Pnd_Edw_Dtl_Cntrct_Prefix.equals("C"))) || ((pnd_New_Enrollment.getBoolean() 
            && pnd_Suppress_Welcome_Pkg.getBoolean()) && pnd_Blank_Enr_Source.getBoolean())) || ((pnd_Newly_Funded.getBoolean() && pnd_Suppress_Legal_Pkg.getBoolean()) 
            && pnd_Blank_Enr_Source.getBoolean()))))
        {
            pnd_Var_Group_Pnd_Var_Category.setValue("N");                                                                                                                 //Natural: ASSIGN #VAR-CATEGORY := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl180.getVw_acis_Reprint_Fl_View().startDatabaseFind                                                                                                     //Natural: FIND ACIS-REPRINT-FL-VIEW WITH RP-TIAA-CONTR = #EDW-DTL-TIAA-NO
            (
            "F1",
            new Wc[] { new Wc("RP_TIAA_CONTR", "=", pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No, WcType.WITH) }
            );
            F1:
            while (condition(ldaAppl180.getVw_acis_Reprint_Fl_View().readNextRow("F1")))
            {
                ldaAppl180.getVw_acis_Reprint_Fl_View().setIfNotFoundControlFlag(false);
                //*  IF SSN IS DIFFERENT .. IT WILL BE CONSIDERED AS NOT FOUND
                if (condition(!(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Soc_Sec().equals(pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec))))                                               //Natural: ACCEPT IF RP-SOC-SEC = #EDW-DTL-SOC-SEC
                {
                    continue;
                }
                pnd_Reprint_Rec_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #REPRINT-REC-FOUND := TRUE
                pnd_Var_Group_Pnd_Var_Category.setValue("N");                                                                                                             //Natural: ASSIGN #VAR-CATEGORY := 'N'
                pnd_Var_Group_Pnd_Var_Reprint_Dte.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Extract_Date());                                                         //Natural: ASSIGN #VAR-REPRINT-DTE := RP-EXTRACT-DATE
                pnd_Var_Group_Pnd_Var_Delete_Dte.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Deleted());                                                            //Natural: ASSIGN #VAR-DELETE-DTE := RP-DT-DELETED
                pnd_Var_Group_Pnd_Var_Delete_User.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_User_Id());                                                       //Natural: ASSIGN #VAR-DELETE-USER := RP-DELETE-USER-ID
                pnd_Var_Group_Pnd_Var_Delete_Reason.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Delete_Reason_Cd());                                                   //Natural: ASSIGN #VAR-DELETE-REASON := RP-DELETE-REASON-CD
                pnd_Var_Group_Pnd_Var_Welcome_Mail_Dte.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Welcome_Mail_Dt());                                                 //Natural: ASSIGN #VAR-WELCOME-MAIL-DTE := RP-WELCOME-MAIL-DT
                pnd_Var_Group_Pnd_Var_Legal_Mail_Dte.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Mail_Dt());                                                     //Natural: ASSIGN #VAR-LEGAL-MAIL-DTE := RP-LEGAL-MAIL-DT
                pnd_Var_Group_Pnd_Var_Package_Mail_Type.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type());                                              //Natural: ASSIGN #VAR-PACKAGE-MAIL-TYPE := RP-PACKAGE-MAIL-TYPE
                //*  457(B) PRIVATE CONTRACT (RP-LOB=S AND RP-LOB-TYPE=4)  WILL BE
                //*  CLASSIFIED AS NOT VALID (CATEGORY = N)
                if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob().equals("S") && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob_Type().equals("4")))                   //Natural: REJECT IF RP-LOB = 'S' AND RP-LOB-TYPE = '4'
                {
                    continue;
                }
                //*  MFO
                //*  MFO
                //*  MFO
                //*  NOT VALID                    /* MFO
                //*  DELETED
                //*  COMPLETED & SUCCESSFUL
                //*  EXCEPTION
                short decideConditionsMet1011 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( ( #NEW-ENROLLMENT AND #SUPPRESS-WELCOME-PKG ) OR ( #NEWLY-FUNDED AND #SUPPRESS-LEGAL-PKG ) ) AND ( RP-APP-SOURCE = #ENTRY-ENR-SOURCE )
                if (condition((((pnd_New_Enrollment.getBoolean() && pnd_Suppress_Welcome_Pkg.getBoolean()) || (pnd_Newly_Funded.getBoolean() && pnd_Suppress_Legal_Pkg.getBoolean())) 
                    && ldaAppl180.getAcis_Reprint_Fl_View_Rp_App_Source().equals(pnd_Entry_Enr_Source))))
                {
                    decideConditionsMet1011++;
                    pnd_Var_Group_Pnd_Var_Category.setValue("N");                                                                                                         //Natural: ASSIGN #VAR-CATEGORY := 'N'
                }                                                                                                                                                         //Natural: WHEN RP-DT-DELETED <> 0
                else if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Deleted().notEquals(getZero())))
                {
                    decideConditionsMet1011++;
                    pnd_Var_Group_Pnd_Var_Category.setValue("D");                                                                                                         //Natural: ASSIGN #VAR-CATEGORY := 'D'
                }                                                                                                                                                         //Natural: WHEN ( #NEW-ENROLLMENT AND RP-WELCOME-MAIL-DT <> 0 ) OR ( #NEWLY-FUNDED AND RP-LEGAL-MAIL-DT <> 0 )
                else if (condition(((pnd_New_Enrollment.getBoolean() && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Welcome_Mail_Dt().notEquals(getZero())) 
                    || (pnd_Newly_Funded.getBoolean() && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Mail_Dt().notEquals(getZero())))))
                {
                    decideConditionsMet1011++;
                    pnd_Var_Group_Pnd_Var_Category.setValue("C");                                                                                                         //Natural: ASSIGN #VAR-CATEGORY := 'C'
                }                                                                                                                                                         //Natural: WHEN ( #NEW-ENROLLMENT AND RP-WELCOME-MAIL-DT = 0 ) OR ( #NEWLY-FUNDED AND RP-LEGAL-MAIL-DT = 0 )
                else if (condition(((pnd_New_Enrollment.getBoolean() && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Welcome_Mail_Dt().equals(getZero())) || 
                    (pnd_Newly_Funded.getBoolean() && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Mail_Dt().equals(getZero())))))
                {
                    decideConditionsMet1011++;
                    pnd_Var_Group_Pnd_Var_Category.setValue("E");                                                                                                         //Natural: ASSIGN #VAR-CATEGORY := 'E'
                    short decideConditionsMet1020 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NEW-ENROLLMENT
                    if (condition(pnd_New_Enrollment.getBoolean()))
                    {
                        decideConditionsMet1020++;
                        //*            IF RP-PACKAGE-MAIL-TYPE = 'P'                 /* BJD1
                        //*  BJD1
                        if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ownership().equals(3)))                                                                       //Natural: IF RP-OWNERSHIP = 3
                        {
                            //*  BJD1
                            //*  BJD1
                            //*  BJD1
                            if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob().equals("D") && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob_Type().equals("A"))       //Natural: IF ( RP-LOB = 'D' AND RP-LOB-TYPE = 'A' ) OR ( RP-LOB = 'S' AND RP-LOB-TYPE = '7' )
                                || (ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob().equals("S") && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob_Type().equals("7"))))
                            {
                                ignore();
                                //*  BJD1
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Var_Group_Pnd_Var_Category.setValue("N");                                                                                             //Natural: ASSIGN #VAR-CATEGORY := 'N'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Extract_Date().less(pnd_Input_Implementation_Dte_Pnd_Input_Implementation_Date)              //Natural: IF RP-EXTRACT-DATE < #INPUT-IMPLEMENTATION-DATE AND RP-PACKAGE-MAIL-TYPE = 'W' OR = 'B'
                            && (ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().equals("W") || ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().equals("B")))))
                        {
                            pnd_Var_Group_Pnd_Var_Category.setValue("C");                                                                                                 //Natural: ASSIGN #VAR-CATEGORY := 'C'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN #NEWLY-FUNDED
                    else if (condition(pnd_Newly_Funded.getBoolean()))
                    {
                        decideConditionsMet1020++;
                        if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Extract_Date().less(pnd_Input_Implementation_Dte_Pnd_Input_Implementation_Date)))             //Natural: IF RP-EXTRACT-DATE < #INPUT-IMPLEMENTATION-DATE
                        {
                            if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().equals("L") || ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().equals("B"))) //Natural: IF RP-PACKAGE-MAIL-TYPE = 'L' OR = 'B'
                            {
                                pnd_Var_Group_Pnd_Var_Category.setValue("C");                                                                                             //Natural: ASSIGN #VAR-CATEGORY := 'C'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Rp_Extracted_Date.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Extract_Date());                                                         //Natural: ASSIGN #RP-EXTRACTED-DATE := RP-EXTRACT-DATE
                            pnd_Rp_Xtrct_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Rp_Extracted_Date_Pnd_Rp_Extracted_Dte);                                   //Natural: MOVE EDITED #RP-EXTRACTED-DTE TO #RP-XTRCT-DTE ( EM = YYYYMMDD )
                            pnd_No_Days.compute(new ComputeParameters(false, pnd_No_Days), pnd_Current_Dte.subtract(pnd_Rp_Xtrct_Dte));                                   //Natural: ASSIGN #NO-DAYS := #CURRENT-DTE - #RP-XTRCT-DTE
                            //*  PENDING - NOT AN EXCEPTION YET
                            if (condition(pnd_No_Days.less(17)))                                                                                                          //Natural: IF #NO-DAYS < 17
                            {
                                pnd_Var_Group_Pnd_Var_Category.setValue("P");                                                                                             //Natural: ASSIGN #VAR-CATEGORY := 'P'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            if (condition(pnd_Reprint_Rec_Found.getBoolean()))                                                                                                            //Natural: IF #REPRINT-REC-FOUND
            {
                ignore();
                //*  NOT FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Var_Group_Pnd_Var_Category.setValue("F");                                                                                                             //Natural: ASSIGN #VAR-CATEGORY := 'F'
                //* *  WRITE 'NOT FOUND' #EDW-DTL-TIAA-NO #VAR-CATEGORY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Var_Group_Pnd_Var_Exception_Ind.setValue("N");                                                                                                                //Natural: ASSIGN #VAR-EXCEPTION-IND := 'N'
        if (condition(! (pnd_Exception_Processing.getBoolean())))                                                                                                         //Natural: IF NOT #EXCEPTION-PROCESSING
        {
            if (condition(pnd_Var_Group_Pnd_Var_Category.equals("E") || pnd_Var_Group_Pnd_Var_Category.equals("F")))                                                      //Natural: IF #VAR-CATEGORY = 'E' OR = 'F'
            {
                pnd_Var_Group_Pnd_Var_Exception_Ind.setValue("Y");                                                                                                        //Natural: ASSIGN #VAR-EXCEPTION-IND := 'Y'
                pnd_Var_Group_Pnd_Var_Exception_Dte.setValue(pnd_Current_Dte_N);                                                                                          //Natural: ASSIGN #VAR-EXCEPTION-DTE := #CURRENT-DTE-N
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM STORE-RECON-REC
            sub_Store_Recon_Rec();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-RECON-REC
            sub_Update_Recon_Rec();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-FILE
        sub_Write_Report_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM TRACK-CONTROL-COUNTS
        sub_Track_Control_Counts();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  PROCESS-DETAIL-REC
    }
    private void sub_Store_Recon_Rec() throws Exception                                                                                                                   //Natural: STORE-RECON-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        ldaAppl530.getVw_acis_Reconcile_Fl_View().reset();                                                                                                                //Natural: RESET ACIS-RECONCILE-FL-VIEW
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Ssn().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec);                                                                         //Natural: ASSIGN RC-SSN := #EDW-DTL-SOC-SEC
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Plan().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Plan);                                                                           //Natural: ASSIGN RC-PLAN := #EDW-DTL-PLAN
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Sub_Plan().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Subplan);                                                                    //Natural: ASSIGN RC-SUB-PLAN := #EDW-DTL-SUBPLAN
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Tiaa_Cntrct().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No);                                                                 //Natural: ASSIGN RC-TIAA-CNTRCT := #EDW-DTL-TIAA-NO
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Cref_Cntrct().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Cref_No);                                                                 //Natural: ASSIGN RC-CREF-CNTRCT := #EDW-DTL-CREF-NO
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Dflt_Enroll_Ind().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Dflt_Enroll_Ind);                                                     //Natural: ASSIGN RC-DFLT-ENROLL-IND := #EDW-DTL-DFLT-ENROLL-IND
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Funded_Ind().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Funded_Ind);                                                               //Natural: ASSIGN RC-FUNDED-IND := #EDW-DTL-FUNDED-IND
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Initial_Funding_Dt().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Date);                                                 //Natural: ASSIGN RC-INITIAL-FUNDING-DT := #EDW-DTL-INIT-FUNDED-DATE
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Type().setValue(pnd_Var_Reconcile_Type);                                                                        //Natural: ASSIGN RC-RECONCILE-TYPE := #VAR-RECONCILE-TYPE
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Dt().setValue(pnd_Current_Dte_N);                                                                               //Natural: ASSIGN RC-RECONCILE-DT := #CURRENT-DTE-N
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().setValue(pnd_Var_Group_Pnd_Var_Category);                                                            //Natural: ASSIGN RC-RECONCILE-CATEGORY := #VAR-CATEGORY
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Ind().setValue(pnd_Var_Group_Pnd_Var_Exception_Ind);                                                            //Natural: ASSIGN RC-EXCEPTION-IND := #VAR-EXCEPTION-IND
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Dt().setValue(pnd_Var_Group_Pnd_Var_Exception_Dte);                                                             //Natural: ASSIGN RC-EXCEPTION-DT := #VAR-EXCEPTION-DTE
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt().setValue(0);                                                                                         //Natural: ASSIGN RC-EXCEPTION-RSLVD-DT := 0
        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Control_Dt().setValue(pnd_Edw_File_Pnd_Edw_Dtl_Control_Date);                                                             //Natural: ASSIGN RC-CONTROL-DT := #EDW-DTL-CONTROL-DATE
        ldaAppl530.getVw_acis_Reconcile_Fl_View().insertDBRow();                                                                                                          //Natural: STORE ACIS-RECONCILE-FL-VIEW
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  STORE-RECON-REC
    }
    private void sub_Update_Recon_Rec() throws Exception                                                                                                                  //Natural: UPDATE-RECON-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No.notEquals(" ")))                                                                                                   //Natural: IF #EDW-DTL-TIAA-NO <> ' '
        {
            pnd_Rc_Cntrl_Type_Cat_Tiaa_No.reset();                                                                                                                        //Natural: RESET #RC-CNTRL-TYPE-CAT-TIAA-NO
            pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Control_Date);                                                            //Natural: ASSIGN #RC-CONTROL-DATE := #EDW-DTL-CONTROL-DATE
            pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type);                                                            //Natural: ASSIGN #RC-RECONCILE-TYPE := #EDW-DTL-RECON-TYPE
            pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Category);                                                          //Natural: ASSIGN #RC-RECONCILE-CATEGORY := #EDW-DTL-CATEGORY
            pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No);                                                                  //Natural: ASSIGN #RC-TIAA-CNTRCT := #EDW-DTL-TIAA-NO
            ldaAppl530.getVw_acis_Reconcile_Fl_View().startDatabaseFind                                                                                                   //Natural: FIND ACIS-RECONCILE-FL-VIEW WITH RC-CNTRL-TYPE-CAT-TIAA-NO = #RC-CNTRL-TYPE-CAT-TIAA-NO
            (
            "UPD2",
            new Wc[] { new Wc("RC_CNTRL_TYPE_CAT_TIAA_NO", "=", pnd_Rc_Cntrl_Type_Cat_Tiaa_No, WcType.WITH) }
            );
            UPD2:
            while (condition(ldaAppl530.getVw_acis_Reconcile_Fl_View().readNextRow("UPD2")))
            {
                ldaAppl530.getVw_acis_Reconcile_Fl_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Control_Dt().equals(pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Control_Date) && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Type().equals(pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Type)  //Natural: IF RC-CONTROL-DT = #RC-CONTROL-DATE AND RC-RECONCILE-TYPE = #RC-RECONCILE-TYPE AND RC-RECONCILE-CATEGORY = #RC-RECONCILE-CATEGORY AND RC-TIAA-CNTRCT = #RC-TIAA-CNTRCT
                    && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals(pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Reconcile_Category) && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Tiaa_Cntrct().equals(pnd_Rc_Cntrl_Type_Cat_Tiaa_No_Pnd_Rc_Tiaa_Cntrct)))
                {
                    pnd_Var_Group_Pnd_Var_Old_Category.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category());                                            //Natural: ASSIGN #VAR-OLD-CATEGORY := RC-RECONCILE-CATEGORY
                    if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals("P")))                                                              //Natural: IF RC-RECONCILE-CATEGORY = 'P'
                    {
                        pnd_Var_Group_Pnd_Var_Exception_Dte.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Dt());                                             //Natural: ASSIGN #VAR-EXCEPTION-DTE := RC-RECONCILE-DT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Var_Group_Pnd_Var_Exception_Dte.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Dt());                                             //Natural: ASSIGN #VAR-EXCEPTION-DTE := RC-EXCEPTION-DT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  OUTSTANDING
                    if (condition(pnd_Var_Group_Pnd_Var_Category.equals("E") || pnd_Var_Group_Pnd_Var_Category.equals("F") || pnd_Var_Group_Pnd_Var_Category.equals("P"))) //Natural: IF #VAR-CATEGORY = 'E' OR = 'F' OR = 'P'
                    {
                        pnd_Dte.setValue(pnd_Var_Group_Pnd_Var_Exception_Dte);                                                                                            //Natural: ASSIGN #DTE := #VAR-EXCEPTION-DTE
                        pnd_From_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Dte_Pnd_Dte2);                                                                     //Natural: MOVE EDITED #DTE2 TO #FROM-DTE ( EM = YYYYMMDD )
                        pnd_Var_Group_Pnd_Var_Excptn_Age_Days.compute(new ComputeParameters(false, pnd_Var_Group_Pnd_Var_Excptn_Age_Days), Global.getDATX().subtract(pnd_From_Dte)); //Natural: ASSIGN #VAR-EXCPTN-AGE-DAYS := *DATX - #FROM-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SOMETHING HAS CHANGED
                    if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().notEquals(pnd_Var_Group_Pnd_Var_Category)))                                //Natural: IF RC-RECONCILE-CATEGORY <> #VAR-CATEGORY
                    {
                        //*  PENDED LEGAL NOW BECOMES EXCEPTION
                        //*  ORIG. RECON DTE
                        if (condition((ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals("P") && (pnd_Var_Group_Pnd_Var_Category.equals("E")            //Natural: IF RC-RECONCILE-CATEGORY = 'P' AND ( #VAR-CATEGORY = 'E' OR = 'F' )
                            || pnd_Var_Group_Pnd_Var_Category.equals("F")))))
                        {
                            pnd_Var_Group_Pnd_Var_Exception_Ind.setValue("Y");                                                                                            //Natural: ASSIGN #VAR-EXCEPTION-IND := 'Y'
                            ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Ind().setValue(pnd_Var_Group_Pnd_Var_Exception_Ind);                                        //Natural: ASSIGN RC-EXCEPTION-IND := #VAR-EXCEPTION-IND
                            ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Dt().setValue(pnd_Var_Group_Pnd_Var_Exception_Dte);                                         //Natural: ASSIGN RC-EXCEPTION-DT := #VAR-EXCEPTION-DTE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(! (pnd_Var_Group_Pnd_Var_Category.equals("E") || pnd_Var_Group_Pnd_Var_Category.equals("F") || pnd_Var_Group_Pnd_Var_Category.equals("P")))) //Natural: IF NOT #VAR-CATEGORY = 'E' OR = 'F' OR = 'P'
                            {
                                pnd_Var_Group_Pnd_Var_Exception_Ind.setValue("N");                                                                                        //Natural: ASSIGN #VAR-EXCEPTION-IND := 'N'
                                pnd_Var_Group_Pnd_Var_Resolved_Dte.setValue(pnd_Current_Dte_N);                                                                           //Natural: ASSIGN #VAR-RESOLVED-DTE := #CURRENT-DTE-N
                                ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Ind().setValue(pnd_Var_Group_Pnd_Var_Exception_Ind);                                    //Natural: ASSIGN RC-EXCEPTION-IND := #VAR-EXCEPTION-IND
                                ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt().setValue(pnd_Var_Group_Pnd_Var_Resolved_Dte);                                //Natural: ASSIGN RC-EXCEPTION-RSLVD-DT := #VAR-RESOLVED-DTE
                            }                                                                                                                                             //Natural: END-IF
                            //* CHG307711
                        }                                                                                                                                                 //Natural: END-IF
                        GET2:                                                                                                                                             //Natural: GET ACIS-RECONCILE-FL-VIEW *ISN ( UPD2. )
                        ldaAppl530.getVw_acis_Reconcile_Fl_View().readByID(ldaAppl530.getVw_acis_Reconcile_Fl_View().getAstISN("UPD2"), "GET2");
                        ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().setValue(pnd_Var_Group_Pnd_Var_Category);                                            //Natural: ASSIGN RC-RECONCILE-CATEGORY := #VAR-CATEGORY
                        //* *      UPDATE (UPD2.)                                       /*CHG307711
                        //* CHG307711
                        ldaAppl530.getVw_acis_Reconcile_Fl_View().updateDBRow("GET2");                                                                                    //Natural: UPDATE ( GET2. )
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) break UPD2;                                                                                                                                 //Natural: ESCAPE BOTTOM ( UPD2. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl530.getVw_acis_Reconcile_Fl_View().startDatabaseFind                                                                                                   //Natural: FIND ACIS-RECONCILE-FL-VIEW WITH RC-SSN = #EDW-DTL-SOC-SEC
            (
            "UPD3",
            new Wc[] { new Wc("RC_SSN", "=", pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec, WcType.WITH) }
            );
            UPD3:
            while (condition(ldaAppl530.getVw_acis_Reconcile_Fl_View().readNextRow("UPD3")))
            {
                ldaAppl530.getVw_acis_Reconcile_Fl_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Ssn().equals(pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec)))                                                    //Natural: IF RC-SSN = #EDW-DTL-SOC-SEC
                {
                    if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Control_Dt().equals(pnd_Edw_File_Pnd_Edw_Dtl_Control_Date) && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Type().equals(pnd_Edw_File_Pnd_Edw_Dtl_Recon_Type)  //Natural: IF RC-CONTROL-DT = #EDW-DTL-CONTROL-DATE AND RC-RECONCILE-TYPE = #EDW-DTL-RECON-TYPE AND RC-RECONCILE-CATEGORY = #EDW-DTL-CATEGORY AND RC-PLAN = #EDW-DTL-PLAN AND RC-SUB-PLAN = #EDW-DTL-SUBPLAN AND RC-TIAA-CNTRCT = ' '
                        && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals(pnd_Edw_File_Pnd_Edw_Dtl_Category) && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Plan().equals(pnd_Edw_File_Pnd_Edw_Dtl_Plan) 
                        && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Sub_Plan().equals(pnd_Edw_File_Pnd_Edw_Dtl_Subplan) && ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Tiaa_Cntrct().equals(" ")))
                    {
                        pnd_Var_Group_Pnd_Var_Old_Category.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category());                                        //Natural: ASSIGN #VAR-OLD-CATEGORY := RC-RECONCILE-CATEGORY
                        if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals("P")))                                                          //Natural: IF RC-RECONCILE-CATEGORY = 'P'
                        {
                            pnd_Var_Group_Pnd_Var_Exception_Dte.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Dt());                                         //Natural: ASSIGN #VAR-EXCEPTION-DTE := RC-RECONCILE-DT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Var_Group_Pnd_Var_Exception_Dte.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Dt());                                         //Natural: ASSIGN #VAR-EXCEPTION-DTE := RC-EXCEPTION-DT
                        }                                                                                                                                                 //Natural: END-IF
                        //*  OUTSTANDING
                        if (condition(pnd_Var_Group_Pnd_Var_Category.equals("E") || pnd_Var_Group_Pnd_Var_Category.equals("F") || pnd_Var_Group_Pnd_Var_Category.equals("P"))) //Natural: IF #VAR-CATEGORY = 'E' OR = 'F' OR = 'P'
                        {
                            pnd_Dte.setValue(pnd_Var_Group_Pnd_Var_Exception_Dte);                                                                                        //Natural: ASSIGN #DTE := #VAR-EXCEPTION-DTE
                            pnd_From_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Dte_Pnd_Dte2);                                                                 //Natural: MOVE EDITED #DTE2 TO #FROM-DTE ( EM = YYYYMMDD )
                            pnd_Var_Group_Pnd_Var_Excptn_Age_Days.compute(new ComputeParameters(false, pnd_Var_Group_Pnd_Var_Excptn_Age_Days), Global.getDATX().subtract(pnd_From_Dte)); //Natural: ASSIGN #VAR-EXCPTN-AGE-DAYS := *DATX - #FROM-DTE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SOMETHING HAS CHANGED
                        if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().notEquals(pnd_Var_Group_Pnd_Var_Category)))                            //Natural: IF RC-RECONCILE-CATEGORY <> #VAR-CATEGORY
                        {
                            //*  PENDED LEGAL NOW BECOMES EXCEPTION
                            //*  ORIG. RECON DTE
                            if (condition((ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().equals("P") && (pnd_Var_Group_Pnd_Var_Category.equals("E")        //Natural: IF RC-RECONCILE-CATEGORY = 'P' AND ( #VAR-CATEGORY = 'E' OR = 'F' )
                                || pnd_Var_Group_Pnd_Var_Category.equals("F")))))
                            {
                                pnd_Var_Group_Pnd_Var_Exception_Ind.setValue("Y");                                                                                        //Natural: ASSIGN #VAR-EXCEPTION-IND := 'Y'
                                ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Ind().setValue(pnd_Var_Group_Pnd_Var_Exception_Ind);                                    //Natural: ASSIGN RC-EXCEPTION-IND := #VAR-EXCEPTION-IND
                                ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Dt().setValue(pnd_Var_Group_Pnd_Var_Exception_Dte);                                     //Natural: ASSIGN RC-EXCEPTION-DT := #VAR-EXCEPTION-DTE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(! (pnd_Var_Group_Pnd_Var_Category.equals("E") || pnd_Var_Group_Pnd_Var_Category.equals("F") || pnd_Var_Group_Pnd_Var_Category.equals("P")))) //Natural: IF NOT #VAR-CATEGORY = 'E' OR = 'F' OR = 'P'
                                {
                                    pnd_Var_Group_Pnd_Var_Exception_Ind.setValue("N");                                                                                    //Natural: ASSIGN #VAR-EXCEPTION-IND := 'N'
                                    pnd_Var_Group_Pnd_Var_Resolved_Dte.setValue(pnd_Current_Dte_N);                                                                       //Natural: ASSIGN #VAR-RESOLVED-DTE := #CURRENT-DTE-N
                                    ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Ind().setValue(pnd_Var_Group_Pnd_Var_Exception_Ind);                                //Natural: ASSIGN RC-EXCEPTION-IND := #VAR-EXCEPTION-IND
                                    ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Rslvd_Dt().setValue(pnd_Var_Group_Pnd_Var_Resolved_Dte);                            //Natural: ASSIGN RC-EXCEPTION-RSLVD-DT := #VAR-RESOLVED-DTE
                                }                                                                                                                                         //Natural: END-IF
                                //* CHG307711
                            }                                                                                                                                             //Natural: END-IF
                            GET3:                                                                                                                                         //Natural: GET ACIS-RECONCILE-FL-VIEW *ISN ( UPD3. )
                            ldaAppl530.getVw_acis_Reconcile_Fl_View().readByID(ldaAppl530.getVw_acis_Reconcile_Fl_View().getAstISN("UPD3"), "GET3");
                            ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().setValue(pnd_Var_Group_Pnd_Var_Category);                                        //Natural: ASSIGN RC-RECONCILE-CATEGORY := #VAR-CATEGORY
                            //* *        UPDATE (UPD3.)                                     /*CHG307711
                            //* CHG307711
                            ldaAppl530.getVw_acis_Reconcile_Fl_View().updateDBRow("GET3");                                                                                //Natural: UPDATE ( GET3. )
                            getCurrentProcessState().getDbConv().dbCommit();                                                                                              //Natural: END TRANSACTION
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (true) break UPD3;                                                                                                                                 //Natural: ESCAPE BOTTOM ( UPD3. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-RECON-REC
    }
    private void sub_Write_Report_File() throws Exception                                                                                                                 //Natural: WRITE-REPORT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  WRITE TO WORK-FILE
        //*  1(REGULAR); 3(EXCEPTION)
        //*  W(WELCOME) ; L(LEGAL)
        pnd_Work_Rpt_File.reset();                                                                                                                                        //Natural: RESET #WORK-RPT-FILE
        pnd_Work_Rpt_File_Pnd_Wk_Rec_Id.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id);                                                                                        //Natural: ASSIGN #WK-REC-ID := #EDW-DTL-REC-ID
        pnd_Work_Rpt_File_Pnd_Wk_Rec_Type.setValue(pnd_Var_Reconcile_Type);                                                                                               //Natural: ASSIGN #WK-REC-TYPE := #VAR-RECONCILE-TYPE
        pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.setValue(pnd_Var_Group_Pnd_Var_Category);                                                                                   //Natural: ASSIGN #WK-CATEGORY-IND := #VAR-CATEGORY
        pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No);                                                                                      //Natural: ASSIGN #WK-TIAA-NO := #EDW-DTL-TIAA-NO
        pnd_Work_Rpt_File_Pnd_Wk_Cref_No.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Cref_No);                                                                                      //Natural: ASSIGN #WK-CREF-NO := #EDW-DTL-CREF-NO
        pnd_Work_Rpt_File_Pnd_Wk_Plan.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Plan);                                                                                            //Natural: ASSIGN #WK-PLAN := #EDW-DTL-PLAN
        pnd_Work_Rpt_File_Pnd_Wk_Subplan.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Subplan);                                                                                      //Natural: ASSIGN #WK-SUBPLAN := #EDW-DTL-SUBPLAN
        pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Soc_Sec);                                                                                      //Natural: ASSIGN #WK-SOC-SEC := #EDW-DTL-SOC-SEC
        pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Funded_Ind);                                                                                //Natural: ASSIGN #WK-FUNDED-IND := #EDW-DTL-FUNDED-IND
        pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Init_Funded_Date);                                                                     //Natural: ASSIGN #WK-INIT-FUNDED-DTE := #EDW-DTL-INIT-FUNDED-DATE
        pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte.setValue(pnd_Var_Group_Pnd_Var_Reprint_Dte);                                                                                 //Natural: ASSIGN #WK-REPRINT-DTE := #VAR-REPRINT-DTE
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte.setValue(pnd_Var_Group_Pnd_Var_Delete_Dte);                                                                                   //Natural: ASSIGN #WK-DELETE-DTE := #VAR-DELETE-DTE
        pnd_Work_Rpt_File_Pnd_Wk_Delete_User.setValue(pnd_Var_Group_Pnd_Var_Delete_User);                                                                                 //Natural: ASSIGN #WK-DELETE-USER := #VAR-DELETE-USER
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason.setValue(pnd_Var_Group_Pnd_Var_Delete_Reason);                                                                             //Natural: ASSIGN #WK-DELETE-REASON := #VAR-DELETE-REASON
        pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date.setValue(pnd_Var_Group_Pnd_Var_Welcome_Mail_Dte);                                                                      //Natural: ASSIGN #WK-WELCOME-MAIL-DATE := #VAR-WELCOME-MAIL-DTE
        pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date.setValue(pnd_Var_Group_Pnd_Var_Legal_Mail_Dte);                                                                          //Natural: ASSIGN #WK-LEGAL-MAIL-DATE := #VAR-LEGAL-MAIL-DTE
        pnd_Work_Rpt_File_Pnd_Wk_Control_Date.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Control_Date);                                                                            //Natural: ASSIGN #WK-CONTROL-DATE := #EDW-DTL-CONTROL-DATE
        pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll.setValue(pnd_Edw_File_Pnd_Edw_Dtl_Dflt_Enroll_Ind);                                                                       //Natural: ASSIGN #WK-DEFAULT-ENROLL := #EDW-DTL-DFLT-ENROLL-IND
        pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type.setValue(pnd_Var_Group_Pnd_Var_Package_Mail_Type);                                                                     //Natural: ASSIGN #WK-PACKAGE-MAIL-TYPE := #VAR-PACKAGE-MAIL-TYPE
        pnd_Work_Rpt_File_Pnd_Wk_Old_Category.setValue(pnd_Var_Group_Pnd_Var_Old_Category);                                                                               //Natural: ASSIGN #WK-OLD-CATEGORY := #VAR-OLD-CATEGORY
        pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte.setValue(pnd_Var_Group_Pnd_Var_Exception_Dte);                                                                             //Natural: ASSIGN #WK-EXCEPTION-DTE := #VAR-EXCEPTION-DTE
        pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte.setValue(pnd_Var_Group_Pnd_Var_Resolved_Dte);                                                                               //Natural: ASSIGN #WK-RESOLVED-DTE := #VAR-RESOLVED-DTE
        pnd_Work_Rpt_File_Pnd_Wk_Excptn_Age_Days.setValue(pnd_Var_Group_Pnd_Var_Excptn_Age_Days);                                                                         //Natural: ASSIGN #WK-EXCPTN-AGE-DAYS := #VAR-EXCPTN-AGE-DAYS
        getWorkFiles().write(2, false, pnd_Work_Rpt_File);                                                                                                                //Natural: WRITE WORK FILE 2 #WORK-RPT-FILE
        //*  WRITE-REPORT-FILE
    }
    private void sub_Track_Control_Counts() throws Exception                                                                                                              //Natural: TRACK-CONTROL-COUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  KEEP TRACK OF CONTROL TOTALS
        short decideConditionsMet1246 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NEW-ENROLLMENT
        if (condition(pnd_New_Enrollment.getBoolean()))
        {
            decideConditionsMet1246++;
            //*  REGULAR PROCESSING
            if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                                   //Natural: IF #EDW-DTL-REC-ID = '1'
            {
                pnd_Counters_Pnd_Ttl_Enroll_Read.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TTL-ENROLL-READ
                //*  EXCEPTION PROCESSING
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Counters_Pnd_Ttlx_Enroll_Read.nadd(1);                                                                                                                //Natural: ADD 1 TO #TTLX-ENROLL-READ
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1255 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #VAR-CATEGORY;//Natural: VALUE 'C'
            if (condition((pnd_Var_Group_Pnd_Var_Category.equals("C"))))
            {
                decideConditionsMet1255++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt.nadd(1);                                                                                                  //Natural: ADD 1 TO #TTL-ENROLL-WITH-WELCOME-DT
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt.nadd(1);                                                                                                 //Natural: ADD 1 TO #TTLX-ENROLL-WITH-WELCOME-DT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("E"))))
            {
                decideConditionsMet1255++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt.nadd(1);                                                                                                    //Natural: ADD 1 TO #TTL-ENROLL-NO-WELCOME-DT
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt.nadd(1);                                                                                                   //Natural: ADD 1 TO #TTLX-ENROLL-NO-WELCOME-DT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("D"))))
            {
                decideConditionsMet1255++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Enroll_Deleted.nadd(1);                                                                                                          //Natural: ADD 1 TO #TTL-ENROLL-DELETED
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Enroll_Deleted.nadd(1);                                                                                                         //Natural: ADD 1 TO #TTLX-ENROLL-DELETED
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("F"))))
            {
                decideConditionsMet1255++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Enroll_Not_Found.nadd(1);                                                                                                        //Natural: ADD 1 TO #TTL-ENROLL-NOT-FOUND
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Enroll_Not_Found.nadd(1);                                                                                                       //Natural: ADD 1 TO #TTLX-ENROLL-NOT-FOUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("N"))))
            {
                decideConditionsMet1255++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable.nadd(1);                                                                                                   //Natural: ADD 1 TO #TTL-ENROLL-NOT-APPLICABLE
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable.nadd(1);                                                                                                  //Natural: ADD 1 TO #TTLX-ENROLL-NOT-APPLICABLE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #NEWLY-FUNDED
        else if (condition(pnd_Newly_Funded.getBoolean()))
        {
            decideConditionsMet1246++;
            //*  REGULAR PROCESSING
            if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                                   //Natural: IF #EDW-DTL-REC-ID = '1'
            {
                pnd_Counters_Pnd_Ttl_Funded_Read.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TTL-FUNDED-READ
                //*  EXCEPTION PROCESSING
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Counters_Pnd_Ttlx_Funded_Read.nadd(1);                                                                                                                //Natural: ADD 1 TO #TTLX-FUNDED-READ
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1307 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #VAR-CATEGORY;//Natural: VALUE 'C'
            if (condition((pnd_Var_Group_Pnd_Var_Category.equals("C"))))
            {
                decideConditionsMet1307++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt.nadd(1);                                                                                                    //Natural: ADD 1 TO #TTL-FUNDED-WITH-LEGAL-DT
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt.nadd(1);                                                                                                   //Natural: ADD 1 TO #TTLX-FUNDED-WITH-LEGAL-DT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("E"))))
            {
                decideConditionsMet1307++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt.nadd(1);                                                                                                      //Natural: ADD 1 TO #TTL-FUNDED-NO-LEGAL-DT
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt.nadd(1);                                                                                                     //Natural: ADD 1 TO #TTLX-FUNDED-NO-LEGAL-DT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("P"))))
            {
                decideConditionsMet1307++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Funded_Pending.nadd(1);                                                                                                          //Natural: ADD 1 TO #TTL-FUNDED-PENDING
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Funded_Pending.nadd(1);                                                                                                         //Natural: ADD 1 TO #TTLX-FUNDED-PENDING
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("D"))))
            {
                decideConditionsMet1307++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Funded_Deleted.nadd(1);                                                                                                          //Natural: ADD 1 TO #TTL-FUNDED-DELETED
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Funded_Deleted.nadd(1);                                                                                                         //Natural: ADD 1 TO #TTLX-FUNDED-DELETED
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("F"))))
            {
                decideConditionsMet1307++;
                //*  REGULAR PROCESSING
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                               //Natural: IF #EDW-DTL-REC-ID = '1'
                {
                    pnd_Counters_Pnd_Ttl_Funded_Not_Found.nadd(1);                                                                                                        //Natural: ADD 1 TO #TTL-FUNDED-NOT-FOUND
                    //*  EXCEPTION PROCESSING
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counters_Pnd_Ttlx_Funded_Not_Found.nadd(1);                                                                                                       //Natural: ADD 1 TO #TTLX-FUNDED-NOT-FOUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pnd_Var_Group_Pnd_Var_Category.equals("N"))))
            {
                decideConditionsMet1307++;
                if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Dflt_Enroll_Ind.equals("2")))                                                                                      //Natural: IF #EDW-DTL-DFLT-ENROLL-IND = '2'
                {
                    //*  REGULAR PROCESSING
                    if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                           //Natural: IF #EDW-DTL-REC-ID = '1'
                    {
                        pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt.nadd(1);                                                                                             //Natural: ADD 1 TO #TTL-FUNDED-DFLT-NO-LEGAL-DT
                        //*  EXCEPTION PROCESSING
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt.nadd(1);                                                                                            //Natural: ADD 1 TO #TTLX-FUNDED-DFLT-NO-LEGAL-DT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  REGULAR PROCESSING
                    if (condition(pnd_Edw_File_Pnd_Edw_Dtl_Rec_Id.equals("1")))                                                                                           //Natural: IF #EDW-DTL-REC-ID = '1'
                    {
                        pnd_Counters_Pnd_Ttl_Funded_Not_Applicable.nadd(1);                                                                                               //Natural: ADD 1 TO #TTL-FUNDED-NOT-APPLICABLE
                        //*  EXCEPTION PROCESSING
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable.nadd(1);                                                                                              //Natural: ADD 1 TO #TTLX-FUNDED-NOT-APPLICABLE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  TRACK-CONTROL-COUNTS
    }
    private void sub_Display_Controls() throws Exception                                                                                                                  //Natural: DISPLAY-CONTROLS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "<<< REGULAR CONTROL SUMMARY >>>",NEWLINE,"NO. OF HEADER RECORDS  :",pnd_Counters_Pnd_Ttl_Header_Found, new ReportEditMask                  //Natural: WRITE '<<< REGULAR CONTROL SUMMARY >>>' / 'NO. OF HEADER RECORDS  :' #TTL-HEADER-FOUND ( EM = Z9 ) / 'NO. OF TRAILER RECORDS :' #TTL-TRAILER-FOUND ( EM = Z9 ) // 'NO. OF NEW ENROLLMENT RECORDS READ:' 47T #TTL-ENROLL-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS WITH WELCOME PKG.:' 47T #TTL-ENROLL-WITH-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS WITHOUT WELCOME PKG.:' 47T #TTL-ENROLL-NO-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS DELETED:' 47T #TTL-ENROLL-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS NOT FOUND:' 47T #TTL-ENROLL-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS NOT APPLICABLE:' 47T #TTL-ENROLL-NOT-APPLICABLE ( EM = ZZZ,ZZZ,ZZ9 ) // 'NO. OF NEW FUNDED RECORDS READ:' 47T #TTL-FUNDED-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS WITH LEGAL PKG.:' 47T #TTL-FUNDED-WITH-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS PENDING:' 47T #TTL-FUNDED-PENDING ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS WITHOUT LEGAL PKG.:' 47T #TTL-FUNDED-NO-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS DELETED:' 47T #TTL-FUNDED-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS NOT FOUND:' 47T #TTL-FUNDED-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS NOT APPLICABLE:' 47T #TTL-FUNDED-NOT-APPLICABLE ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS DEFAULT ENROLL:' 47T #TTL-FUNDED-DFLT-NO-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 )
            ("Z9"),NEWLINE,"NO. OF TRAILER RECORDS :",pnd_Counters_Pnd_Ttl_Trailer_Found, new ReportEditMask ("Z9"),NEWLINE,NEWLINE,"NO. OF NEW ENROLLMENT RECORDS READ:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Read, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS WITH WELCOME PKG.:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS WITHOUT WELCOME PKG.:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS DELETED:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Deleted, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS NOT FOUND:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Not_Found, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS NOT APPLICABLE:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"NO. OF NEW FUNDED RECORDS READ:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Read, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS WITH LEGAL PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS PENDING:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Pending, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS WITHOUT LEGAL PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS DELETED:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Deleted, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS NOT FOUND:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Not_Found, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS NOT APPLICABLE:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Not_Applicable, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS DEFAULT ENROLL:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        if (condition(pnd_Exception_Processing.getBoolean()))                                                                                                             //Natural: IF #EXCEPTION-PROCESSING
        {
            getReports().write(0, "<<< EXCEPTION CONTROL SUMMARY >>>",NEWLINE,NEWLINE,"NO. OF NEW ENROLLMENT RECORDS READ:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Enroll_Read,  //Natural: WRITE '<<< EXCEPTION CONTROL SUMMARY >>>' // 'NO. OF NEW ENROLLMENT RECORDS READ:' 47T #TTLX-ENROLL-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS WITH WELCOME PKG.:' 47T #TTLX-ENROLL-WITH-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS WITHOUT WELCOME PKG.:' 47T #TTLX-ENROLL-NO-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS DELETED:' 47T #TTLX-ENROLL-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS NOT FOUND:' 47T #TTLX-ENROLL-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS NOT APPLICABLE:' 47T #TTLX-ENROLL-NOT-APPLICABLE ( EM = ZZZ,ZZZ,ZZ9 ) // 'NO. OF NEW FUNDED RECORDS READ:' 47T #TTLX-FUNDED-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS WITH LEGAL PKG.:' 47T #TTLX-FUNDED-WITH-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS PENDING:' 47T #TTLX-FUNDED-PENDING ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS WITHOUT LEGAL PKG.:' 47T #TTLX-FUNDED-NO-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS DELETED:' 47T #TTLX-FUNDED-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS NOT FOUND:' 47T #TTLX-FUNDED-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS NOT APPLICABLE:' 47T #TTLX-FUNDED-NOT-APPLICABLE ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS DEFAULT ENROLL:' 47T #TTLX-FUNDED-DFLT-NO-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS WITH WELCOME PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS WITHOUT WELCOME PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS DELETED:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Enroll_Deleted, new 
                ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS NOT FOUND:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Enroll_Not_Found, new 
                ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS NOT APPLICABLE:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"NO. OF NEW FUNDED RECORDS READ:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_Read, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS WITH LEGAL PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS PENDING:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_Pending, new 
                ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS WITHOUT LEGAL PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS DELETED:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_Deleted, new 
                ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS NOT FOUND:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_Not_Found, new 
                ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS NOT APPLICABLE:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS DEFAULT ENROLL:",new TabSetting(47),pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  DISPLAY-CONTROLS
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE // 'CONTRACT:' 25T #EDW-DTL-TIAA-NO / 'SSN:' 25T #EDW-DTL-SSN
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),pnd_Edw_File_Pnd_Edw_Dtl_Tiaa_No,NEWLINE,"SSN:",new TabSetting(25),
            pnd_Edw_File_Pnd_Edw_Dtl_Ssn);
    };                                                                                                                                                                    //Natural: END-ERROR
}
