/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:54 PM
**        * FROM NATURAL PROGRAM : Appb520
************************************************************
**        * FILE NAME            : Appb520.java
**        * CLASS NAME           : Appb520
**        * INSTANCE NAME        : Appb520
************************************************************
************************************************************************
* PROGRAM  : APPB520 - PROCESS RECONCILIATION EXCEPTIONS               *
* FUNCTION : READS ALL EXCEPTIONS AND PENDED RECORDS FROM ACIS-RECON   *
*            FILE AND DUMP THE RECORDS INTO DATASET                    *
* UPDATED  : 08/13/09 C.AVE     - NEW PROGRAM                          *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb520 extends BLNatBase
{
    // Data Areas
    private LdaAppl530 ldaAppl530;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Excptn_File;

    private DbsGroup pnd_Excptn_File__R_Field_1;

    private DbsGroup pnd_Excptn_File_Pnd_Excptn_Dtl_Rec;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Rec_Id;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Ssn;

    private DbsGroup pnd_Excptn_File__R_Field_2;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Soc_Sec;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Plan;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Subplan;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Tiaa_No;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Cref_No;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Dflt_Enroll_Ind;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Funded_Ind;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Dte;

    private DbsGroup pnd_Excptn_File__R_Field_3;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Date;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Dte;

    private DbsGroup pnd_Excptn_File__R_Field_4;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Date;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Recon_Type;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Dtl_Category;

    private DbsGroup pnd_Excptn_File__R_Field_5;

    private DbsGroup pnd_Excptn_File_Pnd_Excptn_Ctl_Rec;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Ctl_Rec_Id;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Ctl_File_Typ;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Ctl_Run_Dte;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Cnt;

    private DbsGroup pnd_Excptn_File__R_Field_6;
    private DbsField pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Count;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Ttl_Pended_Records;
    private DbsField pnd_Counters_Pnd_Ttl_Excptn_Records;
    private DbsField pnd_Rc_Excptn_Ctgry_Ind;

    private DbsGroup pnd_Rc_Excptn_Ctgry_Ind__R_Field_7;
    private DbsField pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Exception_Ind;
    private DbsField pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Reconcile_Category;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl530 = new LdaAppl530();
        registerRecord(ldaAppl530);
        registerRecord(ldaAppl530.getVw_acis_Reconcile_Fl_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Excptn_File = localVariables.newFieldInRecord("pnd_Excptn_File", "#EXCPTN-FILE", FieldType.STRING, 100);

        pnd_Excptn_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Excptn_File__R_Field_1", "REDEFINE", pnd_Excptn_File);

        pnd_Excptn_File_Pnd_Excptn_Dtl_Rec = pnd_Excptn_File__R_Field_1.newGroupInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Rec", "#EXCPTN-DTL-REC");
        pnd_Excptn_File_Pnd_Excptn_Dtl_Rec_Id = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Rec_Id", "#EXCPTN-DTL-REC-ID", 
            FieldType.STRING, 1);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Ssn = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Ssn", "#EXCPTN-DTL-SSN", 
            FieldType.STRING, 9);

        pnd_Excptn_File__R_Field_2 = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newGroupInGroup("pnd_Excptn_File__R_Field_2", "REDEFINE", pnd_Excptn_File_Pnd_Excptn_Dtl_Ssn);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Soc_Sec = pnd_Excptn_File__R_Field_2.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Soc_Sec", "#EXCPTN-DTL-SOC-SEC", 
            FieldType.NUMERIC, 9);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Plan = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Plan", "#EXCPTN-DTL-PLAN", 
            FieldType.STRING, 6);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Subplan = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Subplan", "#EXCPTN-DTL-SUBPLAN", 
            FieldType.STRING, 6);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Tiaa_No = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Tiaa_No", "#EXCPTN-DTL-TIAA-NO", 
            FieldType.STRING, 10);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Cref_No = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Cref_No", "#EXCPTN-DTL-CREF-NO", 
            FieldType.STRING, 10);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Dflt_Enroll_Ind = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Dflt_Enroll_Ind", 
            "#EXCPTN-DTL-DFLT-ENROLL-IND", FieldType.STRING, 1);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Funded_Ind = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Funded_Ind", "#EXCPTN-DTL-FUNDED-IND", 
            FieldType.STRING, 1);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Dte = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Dte", 
            "#EXCPTN-DTL-INIT-FUNDED-DTE", FieldType.STRING, 8);

        pnd_Excptn_File__R_Field_3 = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newGroupInGroup("pnd_Excptn_File__R_Field_3", "REDEFINE", pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Dte);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Date = pnd_Excptn_File__R_Field_3.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Date", 
            "#EXCPTN-DTL-INIT-FUNDED-DATE", FieldType.NUMERIC, 8);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Dte = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Dte", 
            "#EXCPTN-DTL-CONTROL-DTE", FieldType.STRING, 8);

        pnd_Excptn_File__R_Field_4 = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newGroupInGroup("pnd_Excptn_File__R_Field_4", "REDEFINE", pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Dte);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Date = pnd_Excptn_File__R_Field_4.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Date", "#EXCPTN-DTL-CONTROL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Recon_Type = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Recon_Type", "#EXCPTN-DTL-RECON-TYPE", 
            FieldType.STRING, 1);
        pnd_Excptn_File_Pnd_Excptn_Dtl_Category = pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Dtl_Category", "#EXCPTN-DTL-CATEGORY", 
            FieldType.STRING, 1);

        pnd_Excptn_File__R_Field_5 = localVariables.newGroupInRecord("pnd_Excptn_File__R_Field_5", "REDEFINE", pnd_Excptn_File);

        pnd_Excptn_File_Pnd_Excptn_Ctl_Rec = pnd_Excptn_File__R_Field_5.newGroupInGroup("pnd_Excptn_File_Pnd_Excptn_Ctl_Rec", "#EXCPTN-CTL-REC");
        pnd_Excptn_File_Pnd_Excptn_Ctl_Rec_Id = pnd_Excptn_File_Pnd_Excptn_Ctl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Ctl_Rec_Id", "#EXCPTN-CTL-REC-ID", 
            FieldType.STRING, 1);
        pnd_Excptn_File_Pnd_Excptn_Ctl_File_Typ = pnd_Excptn_File_Pnd_Excptn_Ctl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Ctl_File_Typ", "#EXCPTN-CTL-FILE-TYP", 
            FieldType.STRING, 1);
        pnd_Excptn_File_Pnd_Excptn_Ctl_Run_Dte = pnd_Excptn_File_Pnd_Excptn_Ctl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Ctl_Run_Dte", "#EXCPTN-CTL-RUN-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Cnt = pnd_Excptn_File_Pnd_Excptn_Ctl_Rec.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Cnt", "#EXCPTN-CTL-CNTRL-CNT", 
            FieldType.STRING, 9);

        pnd_Excptn_File__R_Field_6 = pnd_Excptn_File_Pnd_Excptn_Ctl_Rec.newGroupInGroup("pnd_Excptn_File__R_Field_6", "REDEFINE", pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Cnt);
        pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Count = pnd_Excptn_File__R_Field_6.newFieldInGroup("pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Count", "#EXCPTN-CTL-CNTRL-COUNT", 
            FieldType.NUMERIC, 9);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Ttl_Pended_Records = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Pended_Records", "#TTL-PENDED-RECORDS", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Excptn_Records = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Excptn_Records", "#TTL-EXCPTN-RECORDS", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Rc_Excptn_Ctgry_Ind = localVariables.newFieldInRecord("pnd_Rc_Excptn_Ctgry_Ind", "#RC-EXCPTN-CTGRY-IND", FieldType.STRING, 2);

        pnd_Rc_Excptn_Ctgry_Ind__R_Field_7 = localVariables.newGroupInRecord("pnd_Rc_Excptn_Ctgry_Ind__R_Field_7", "REDEFINE", pnd_Rc_Excptn_Ctgry_Ind);
        pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Exception_Ind = pnd_Rc_Excptn_Ctgry_Ind__R_Field_7.newFieldInGroup("pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Exception_Ind", 
            "#RC-EXCEPTION-IND", FieldType.STRING, 1);
        pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Reconcile_Category = pnd_Rc_Excptn_Ctgry_Ind__R_Field_7.newFieldInGroup("pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Reconcile_Category", 
            "#RC-RECONCILE-CATEGORY", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl530.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb520() throws Exception
    {
        super("Appb520");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB520", onError);
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        pnd_Rc_Excptn_Ctgry_Ind.reset();                                                                                                                                  //Natural: RESET #RC-EXCPTN-CTGRY-IND
        pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Exception_Ind.setValue("N");                                                                                                       //Natural: ASSIGN #RC-EXCEPTION-IND := 'N'
        pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Reconcile_Category.setValue("P");                                                                                                  //Natural: ASSIGN #RC-RECONCILE-CATEGORY := 'P'
        ldaAppl530.getVw_acis_Reconcile_Fl_View().startDatabaseRead                                                                                                       //Natural: READ ACIS-RECONCILE-FL-VIEW BY RC-EXCPTN-CTGRY-IND STARTING FROM #RC-EXCPTN-CTGRY-IND
        (
        "RD1",
        new Wc[] { new Wc("RC_EXCPTN_CTGRY_IND", ">=", pnd_Rc_Excptn_Ctgry_Ind, WcType.BY) },
        new Oc[] { new Oc("RC_EXCPTN_CTGRY_IND", "ASC") }
        );
        RD1:
        while (condition(ldaAppl530.getVw_acis_Reconcile_Fl_View().readNextRow("RD1")))
        {
            if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Ind().notEquals(pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Exception_Ind) || ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category().notEquals(pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Reconcile_Category))) //Natural: IF RC-EXCEPTION-IND <> #RC-EXCEPTION-IND OR RC-RECONCILE-CATEGORY <> #RC-RECONCILE-CATEGORY
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-DTL-REC
            sub_Write_Dtl_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Counters_Pnd_Ttl_Pended_Records.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TTL-PENDED-RECORDS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Rc_Excptn_Ctgry_Ind.reset();                                                                                                                                  //Natural: RESET #RC-EXCPTN-CTGRY-IND
        pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Exception_Ind.setValue("Y");                                                                                                       //Natural: ASSIGN #RC-EXCEPTION-IND := 'Y'
        ldaAppl530.getVw_acis_Reconcile_Fl_View().startDatabaseRead                                                                                                       //Natural: READ ACIS-RECONCILE-FL-VIEW BY RC-EXCPTN-CTGRY-IND STARTING FROM #RC-EXCPTN-CTGRY-IND
        (
        "RD2",
        new Wc[] { new Wc("RC_EXCPTN_CTGRY_IND", ">=", pnd_Rc_Excptn_Ctgry_Ind, WcType.BY) },
        new Oc[] { new Oc("RC_EXCPTN_CTGRY_IND", "ASC") }
        );
        RD2:
        while (condition(ldaAppl530.getVw_acis_Reconcile_Fl_View().readNextRow("RD2")))
        {
            if (condition(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Exception_Ind().notEquals(pnd_Rc_Excptn_Ctgry_Ind_Pnd_Rc_Exception_Ind)))                               //Natural: IF RC-EXCEPTION-IND <> #RC-EXCEPTION-IND
            {
                if (true) break RD2;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD2. )
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-DTL-REC
            sub_Write_Dtl_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Counters_Pnd_Ttl_Excptn_Records.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TTL-EXCPTN-RECORDS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CTL-REC
        sub_Write_Ctl_Rec();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "<<< CONTROL SUMMARY >>>",NEWLINE,"NO. OF PENDED RECORDS    :",pnd_Counters_Pnd_Ttl_Pended_Records, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,"NO. OF EXCEPTION RECORDS :",pnd_Counters_Pnd_Ttl_Excptn_Records,  //Natural: WRITE '<<< CONTROL SUMMARY >>>' / 'NO. OF PENDED RECORDS    :' #TTL-PENDED-RECORDS ( EM = ZZZ,ZZ9 ) / 'NO. OF EXCEPTION RECORDS :' #TTL-EXCPTN-RECORDS ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DTL-REC
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CTL-REC
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Write_Dtl_Rec() throws Exception                                                                                                                     //Natural: WRITE-DTL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  OUTSTANDING EXCEPTIONS
        pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.reset();                                                                                                                       //Natural: RESET #EXCPTN-DTL-REC
        pnd_Excptn_File_Pnd_Excptn_Dtl_Rec_Id.setValue("3");                                                                                                              //Natural: ASSIGN #EXCPTN-DTL-REC-ID := '3'
        pnd_Excptn_File_Pnd_Excptn_Dtl_Soc_Sec.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Ssn());                                                                   //Natural: ASSIGN #EXCPTN-DTL-SOC-SEC := RC-SSN
        pnd_Excptn_File_Pnd_Excptn_Dtl_Plan.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Plan());                                                                     //Natural: ASSIGN #EXCPTN-DTL-PLAN := RC-PLAN
        pnd_Excptn_File_Pnd_Excptn_Dtl_Subplan.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Sub_Plan());                                                              //Natural: ASSIGN #EXCPTN-DTL-SUBPLAN := RC-SUB-PLAN
        pnd_Excptn_File_Pnd_Excptn_Dtl_Tiaa_No.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Tiaa_Cntrct());                                                           //Natural: ASSIGN #EXCPTN-DTL-TIAA-NO := RC-TIAA-CNTRCT
        pnd_Excptn_File_Pnd_Excptn_Dtl_Cref_No.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Cref_Cntrct());                                                           //Natural: ASSIGN #EXCPTN-DTL-CREF-NO := RC-CREF-CNTRCT
        pnd_Excptn_File_Pnd_Excptn_Dtl_Dflt_Enroll_Ind.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Dflt_Enroll_Ind());                                               //Natural: ASSIGN #EXCPTN-DTL-DFLT-ENROLL-IND := RC-DFLT-ENROLL-IND
        pnd_Excptn_File_Pnd_Excptn_Dtl_Funded_Ind.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Funded_Ind());                                                         //Natural: ASSIGN #EXCPTN-DTL-FUNDED-IND := RC-FUNDED-IND
        pnd_Excptn_File_Pnd_Excptn_Dtl_Init_Funded_Date.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Initial_Funding_Dt());                                           //Natural: ASSIGN #EXCPTN-DTL-INIT-FUNDED-DATE := RC-INITIAL-FUNDING-DT
        pnd_Excptn_File_Pnd_Excptn_Dtl_Control_Date.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Control_Dt());                                                       //Natural: ASSIGN #EXCPTN-DTL-CONTROL-DATE := RC-CONTROL-DT
        pnd_Excptn_File_Pnd_Excptn_Dtl_Recon_Type.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Type());                                                     //Natural: ASSIGN #EXCPTN-DTL-RECON-TYPE := RC-RECONCILE-TYPE
        pnd_Excptn_File_Pnd_Excptn_Dtl_Category.setValue(ldaAppl530.getAcis_Reconcile_Fl_View_Rc_Reconcile_Category());                                                   //Natural: ASSIGN #EXCPTN-DTL-CATEGORY := RC-RECONCILE-CATEGORY
        getWorkFiles().write(1, false, pnd_Excptn_File_Pnd_Excptn_Dtl_Rec);                                                                                               //Natural: WRITE WORK FILE 1 #EXCPTN-DTL-REC
        //*   WRITE-DTL-REC
    }
    private void sub_Write_Ctl_Rec() throws Exception                                                                                                                     //Natural: WRITE-CTL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  CONTROL
        pnd_Excptn_File_Pnd_Excptn_Dtl_Rec.reset();                                                                                                                       //Natural: RESET #EXCPTN-DTL-REC
        pnd_Excptn_File_Pnd_Excptn_Ctl_Rec_Id.setValue("4");                                                                                                              //Natural: ASSIGN #EXCPTN-CTL-REC-ID := '4'
        pnd_Excptn_File_Pnd_Excptn_Ctl_File_Typ.setValue("X");                                                                                                            //Natural: ASSIGN #EXCPTN-CTL-FILE-TYP := 'X'
        pnd_Excptn_File_Pnd_Excptn_Ctl_Run_Dte.setValue(Global.getDATN());                                                                                                //Natural: ASSIGN #EXCPTN-CTL-RUN-DTE := *DATN
        pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Count.compute(new ComputeParameters(false, pnd_Excptn_File_Pnd_Excptn_Ctl_Cntrl_Count), pnd_Counters_Pnd_Ttl_Excptn_Records.add(pnd_Counters_Pnd_Ttl_Pended_Records)); //Natural: ASSIGN #EXCPTN-CTL-CNTRL-COUNT := #TTL-EXCPTN-RECORDS + #TTL-PENDED-RECORDS
        getWorkFiles().write(1, false, pnd_Excptn_File_Pnd_Excptn_Dtl_Rec);                                                                                               //Natural: WRITE WORK FILE 1 #EXCPTN-DTL-REC
        //*   WRITE-CTL-REC
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE
            TabSetting(25),Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
}
