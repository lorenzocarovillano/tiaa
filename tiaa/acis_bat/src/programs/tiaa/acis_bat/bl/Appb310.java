/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:22 PM
**        * FROM NATURAL PROGRAM : Appb310
************************************************************
**        * FILE NAME            : Appb310.java
**        * CLASS NAME           : Appb310
**        * INSTANCE NAME        : Appb310
************************************************************
************************************************************************
* PROGRAM  : APPB310 - PRAP PURGE PROCESS                              *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : AUGUST 31, 1999                                           *
* FUNCTION : READS THE WORK FILE CREATED IN APPB300, SORTS IT AND WRITE*
*            A TOTAL REPORT BY TEAM FOR STATUS                         *
*                                                                      *
* 02/10/04 SINGLETON - NEW LAYOUT OF APPL300 FOR SGRD                  *
* 12/09/05 D.MARPURI - RE-STOW NEW LAYOUT APPL300 FOR SGRD (RL8)       *
* 07/14/06 K.GATES   - RE-STOW NEW LAYOUT APPL300 FOR ARR  (ARR)       *
* 10/17/06 DEVELBISS - REMOVE INTERNAL SORT AND APPL300 LDA AND ADD    *
*                      NEW WORK FILE LAYOUT #CV-DATA.      (BD1)       *
* 06/20/17  (BARUA)  - PIN EXPANSION CHANGES. (C425939) STOW ONLY
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb310 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Cv_Data;
    private DbsField pnd_Cv_Data_Cv_Record_Type;
    private DbsField pnd_Cv_Data_Cv_Status;
    private DbsField pnd_Cv_Data_Cv_Ppg_Team_Cde;
    private DbsField pnd_Rec_In;
    private DbsField pnd_Rec_Out;
    private DbsField pnd_Stat_Desc;
    private DbsField pnd_Team;
    private DbsField pnd_Status_A;
    private DbsField pnd_Status_P;
    private DbsField pnd_Sub_Total;
    private DbsField pnd_Grand;

    private DbsGroup pnd_Team_Totals;
    private DbsField pnd_Team_Totals_Pnd_T_Status_A;
    private DbsField pnd_Team_Totals_Pnd_T_Status_P;
    private DbsField pnd_Team_Totals_Pnd_T_Sub_Total;
    private DbsField pnd_Team_Totals_Pnd_T_Grand;
    private DbsField pnd_I;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Cv_Ppg_Team_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Cv_Data = localVariables.newGroupInRecord("pnd_Cv_Data", "#CV-DATA");
        pnd_Cv_Data_Cv_Record_Type = pnd_Cv_Data.newFieldInGroup("pnd_Cv_Data_Cv_Record_Type", "CV-RECORD-TYPE", FieldType.NUMERIC, 1);
        pnd_Cv_Data_Cv_Status = pnd_Cv_Data.newFieldInGroup("pnd_Cv_Data_Cv_Status", "CV-STATUS", FieldType.STRING, 1);
        pnd_Cv_Data_Cv_Ppg_Team_Cde = pnd_Cv_Data.newFieldInGroup("pnd_Cv_Data_Cv_Ppg_Team_Cde", "CV-PPG-TEAM-CDE", FieldType.STRING, 8);
        pnd_Rec_In = localVariables.newFieldInRecord("pnd_Rec_In", "#REC-IN", FieldType.NUMERIC, 7);
        pnd_Rec_Out = localVariables.newFieldInRecord("pnd_Rec_Out", "#REC-OUT", FieldType.NUMERIC, 7);
        pnd_Stat_Desc = localVariables.newFieldArrayInRecord("pnd_Stat_Desc", "#STAT-DESC", FieldType.STRING, 16, new DbsArrayController(1, 10));
        pnd_Team = localVariables.newFieldInRecord("pnd_Team", "#TEAM", FieldType.STRING, 8);
        pnd_Status_A = localVariables.newFieldArrayInRecord("pnd_Status_A", "#STATUS-A", FieldType.NUMERIC, 7, new DbsArrayController(1, 10));
        pnd_Status_P = localVariables.newFieldArrayInRecord("pnd_Status_P", "#STATUS-P", FieldType.NUMERIC, 7, new DbsArrayController(1, 10));
        pnd_Sub_Total = localVariables.newFieldInRecord("pnd_Sub_Total", "#SUB-TOTAL", FieldType.NUMERIC, 7);
        pnd_Grand = localVariables.newFieldInRecord("pnd_Grand", "#GRAND", FieldType.NUMERIC, 7);

        pnd_Team_Totals = localVariables.newGroupInRecord("pnd_Team_Totals", "#TEAM-TOTALS");
        pnd_Team_Totals_Pnd_T_Status_A = pnd_Team_Totals.newFieldArrayInGroup("pnd_Team_Totals_Pnd_T_Status_A", "#T-STATUS-A", FieldType.NUMERIC, 7, new 
            DbsArrayController(1, 10));
        pnd_Team_Totals_Pnd_T_Status_P = pnd_Team_Totals.newFieldArrayInGroup("pnd_Team_Totals_Pnd_T_Status_P", "#T-STATUS-P", FieldType.NUMERIC, 7, new 
            DbsArrayController(1, 10));
        pnd_Team_Totals_Pnd_T_Sub_Total = pnd_Team_Totals.newFieldInGroup("pnd_Team_Totals_Pnd_T_Sub_Total", "#T-SUB-TOTAL", FieldType.NUMERIC, 7);
        pnd_Team_Totals_Pnd_T_Grand = pnd_Team_Totals.newFieldInGroup("pnd_Team_Totals_Pnd_T_Grand", "#T-GRAND", FieldType.NUMERIC, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Cv_Ppg_Team_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cv_Ppg_Team_Cde_OLD", "Cv_Ppg_Team_Cde_OLD", FieldType.STRING, 
            8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb310() throws Exception
    {
        super("Appb310");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB310", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 HW = OFF PS = 52
        pnd_Stat_Desc.getValue(1).setValue("Deleted");                                                                                                                    //Natural: MOVE 'Deleted' TO #STAT-DESC ( 1 )
        pnd_Stat_Desc.getValue(2).setValue("Assigned");                                                                                                                   //Natural: MOVE 'Assigned' TO #STAT-DESC ( 2 )
        pnd_Stat_Desc.getValue(3).setValue("Match & Release");                                                                                                            //Natural: MOVE 'Match & Release' TO #STAT-DESC ( 3 )
        pnd_Stat_Desc.getValue(4).setValue("Released");                                                                                                                   //Natural: MOVE 'Released' TO #STAT-DESC ( 4 )
        pnd_Stat_Desc.getValue(5).setValue("Unassigned");                                                                                                                 //Natural: MOVE 'Unassigned' TO #STAT-DESC ( 5 )
        pnd_Stat_Desc.getValue(6).setValue("Suspense");                                                                                                                   //Natural: MOVE 'Suspense' TO #STAT-DESC ( 6 )
        pnd_Stat_Desc.getValue(7).setValue("Overdue 30 Days");                                                                                                            //Natural: MOVE 'Overdue 30 Days' TO #STAT-DESC ( 7 )
        pnd_Stat_Desc.getValue(8).setValue("Overdue 60 Days");                                                                                                            //Natural: MOVE 'Overdue 60 Days' TO #STAT-DESC ( 8 )
        pnd_Stat_Desc.getValue(9).setValue("Overdue 90 Days");                                                                                                            //Natural: MOVE 'Overdue 90 Days' TO #STAT-DESC ( 9 )
        pnd_Stat_Desc.getValue(10).setValue("Unknown");                                                                                                                   //Natural: MOVE 'Unknown' TO #STAT-DESC ( 10 )
        //*  BD1 START
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 1 #CV-DATA
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Cv_Data)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  READ WORK 1 CV-RULE-DATA-1
            //*              CV-RULE-DATA-2
            //*              CV-RULE-DATA-3
            //*              CV-RULE-DATA-4
            //*              CV-RULE-DATA-5
            //*              CV-RULE-DATA-6
            //*              CV-RULE-DATA-7
            //*              CV-RULE-DATA-8
            //*              CV-RULE-DATA-9
            //*              CV-RULE-DATA-10
            //*              CV-RULE-DATA-11 /* OIA (CRS) \/
            //*              CV-RULE-DATA-12
            //*              CV-RULE-DATA-13
            //*              CV-RULE-DATA-14
            //*              CV-RULE-DATA-15
            //*              CV-RULE-DATA-16 /* OIA (CRS) /\
            //*              CV-RULE-DATA-17 /* ARR KG
            //*  END-ALL
            //*  SORT CV-PPG-TEAM-CDE USING CV-RULE-DATA-1 CV-RULE-DATA-2CV-RULE-DATA-3
            //*                      CV-RULE-DATA-4 CV-RULE-DATA-5 CV-RULE-DATA-6
            //*                      CV-RULE-DATA-7 CV-RULE-DATA-8 CV-RULE-DATA-9
            //*                      CV-RULE-DATA-10 CV-RULE-DATA-11 CV-RULE-DATA-12
            //*                      CV-RULE-DATA-13 CV-RULE-DATA-14 CV-RULE-DATA-15
            //*                      CV-RULE-DATA-16 /* OIA (CRS)
            //*                      CV-RULE-DATA-17 /* ARR - KG     /* BD1 END
            pnd_Rec_In.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #REC-IN
            short decideConditionsMet89 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE CV-STATUS;//Natural: VALUE 'A'
            if (condition((pnd_Cv_Data_Cv_Status.equals("A"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(1);                                                                                                                                        //Natural: MOVE 1 TO #I
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("B"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(2);                                                                                                                                        //Natural: MOVE 2 TO #I
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("C"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(3);                                                                                                                                        //Natural: MOVE 3 TO #I
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("D"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(4);                                                                                                                                        //Natural: MOVE 4 TO #I
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("E"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(5);                                                                                                                                        //Natural: MOVE 5 TO #I
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("F"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(6);                                                                                                                                        //Natural: MOVE 6 TO #I
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("G"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(7);                                                                                                                                        //Natural: MOVE 7 TO #I
            }                                                                                                                                                             //Natural: VALUE 'H'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("H"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(8);                                                                                                                                        //Natural: MOVE 8 TO #I
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pnd_Cv_Data_Cv_Status.equals("I"))))
            {
                decideConditionsMet89++;
                pnd_I.setValue(9);                                                                                                                                        //Natural: MOVE 9 TO #I
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_I.setValue(10);                                                                                                                                       //Natural: MOVE 10 TO #I
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Cv_Data_Cv_Record_Type.equals(1)))                                                                                                          //Natural: IF CV-RECORD-TYPE EQ 1
            {
                pnd_Team_Totals_Pnd_T_Status_A.getValue(pnd_I).nadd(1);                                                                                                   //Natural: ADD 1 TO #T-STATUS-A ( #I )
                pnd_Status_A.getValue(pnd_I).nadd(1);                                                                                                                     //Natural: ADD 1 TO #STATUS-A ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Team_Totals_Pnd_T_Status_P.getValue(pnd_I).nadd(1);                                                                                                   //Natural: ADD 1 TO #T-STATUS-P ( #I )
                pnd_Status_P.getValue(pnd_I).nadd(1);                                                                                                                     //Natural: ADD 1 TO #STATUS-P ( #I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Out.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-OUT
            //*                                                                                                                                                           //Natural: AT BREAK OF CV-PPG-TEAM-CDE
            //*  END-SORT                                           /* BD1                                                                                                //Natural: AT END OF DATA
            readWork01Cv_Ppg_Team_CdeOld.setValue(pnd_Cv_Data_Cv_Ppg_Team_Cde);                                                                                           //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTAL-PAGE
            sub_Print_Total_Page();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* *===============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEAM-TOTAL-PAGE
        //* *================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTAL-PAGE
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 120T *DATU / 'PAGE' *PAGE-NUMBER ( 1 ) 120T *TIMX // '-' ( 132 ) //
        //* *======
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Team_Total_Page() throws Exception                                                                                                                   //Natural: TEAM-TOTAL-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *===============================
        getReports().write(1, new ColumnSpacing(51),"PRAP Purge Control Statistics",NEWLINE,NEWLINE,"TEAM:",pnd_Team,NEWLINE,"_",new RepeatItem(132),NEWLINE,             //Natural: WRITE ( 1 ) 51X 'PRAP Purge Control Statistics' // 'TEAM:' #TEAM / '_' ( 132 ) // '    APPLICATIONS' /
            NEWLINE,"    APPLICATIONS",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(1),new ColumnSpacing(6),pnd_Team_Totals_Pnd_T_Status_A.getValue(1));                            //Natural: WRITE ( 1 ) 6X #STAT-DESC ( 1 ) 6X #T-STATUS-A ( 1 )
        if (Global.isEscape()) return;
        pnd_Team_Totals_Pnd_T_Sub_Total.nadd(pnd_Team_Totals_Pnd_T_Status_A.getValue(1));                                                                                 //Natural: ADD #T-STATUS-A ( 1 ) TO #T-SUB-TOTAL
        getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(3),new ColumnSpacing(6),pnd_Team_Totals_Pnd_T_Status_A.getValue(3));                            //Natural: WRITE ( 1 ) 6X #STAT-DESC ( 3 ) 6X #T-STATUS-A ( 3 )
        if (Global.isEscape()) return;
        pnd_Team_Totals_Pnd_T_Sub_Total.nadd(pnd_Team_Totals_Pnd_T_Status_A.getValue(3));                                                                                 //Natural: ADD #T-STATUS-A ( 3 ) TO #T-SUB-TOTAL
        getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(4),new ColumnSpacing(6),pnd_Team_Totals_Pnd_T_Status_A.getValue(4));                            //Natural: WRITE ( 1 ) 6X #STAT-DESC ( 4 ) 6X #T-STATUS-A ( 4 )
        if (Global.isEscape()) return;
        pnd_Team_Totals_Pnd_T_Sub_Total.nadd(pnd_Team_Totals_Pnd_T_Status_A.getValue(4));                                                                                 //Natural: ADD #T-STATUS-A ( 4 ) TO #T-SUB-TOTAL
        FOR01:                                                                                                                                                            //Natural: FOR #I 7 9
        for (pnd_I.setValue(7); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(pnd_I),new ColumnSpacing(6),pnd_Team_Totals_Pnd_T_Status_A.getValue(pnd_I));                //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #I ) 6X #T-STATUS-A ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Team_Totals_Pnd_T_Sub_Total.nadd(pnd_Team_Totals_Pnd_T_Status_A.getValue(pnd_I));                                                                         //Natural: ADD #T-STATUS-A ( #I ) TO #T-SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"    Total Apps ******",new ColumnSpacing(15),pnd_Team_Totals_Pnd_T_Sub_Total);                                                     //Natural: WRITE ( 1 ) / '    Total Apps ******' 15X #T-SUB-TOTAL
        if (Global.isEscape()) return;
        pnd_Team_Totals_Pnd_T_Grand.nadd(pnd_Team_Totals_Pnd_T_Sub_Total);                                                                                                //Natural: ADD #T-SUB-TOTAL TO #T-GRAND
        pnd_Team_Totals_Pnd_T_Sub_Total.reset();                                                                                                                          //Natural: RESET #T-SUB-TOTAL
        getReports().write(1, NEWLINE,NEWLINE,"    PREMIUMS",NEWLINE);                                                                                                    //Natural: WRITE ( 1 ) // '    PREMIUMS' /
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 3
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
        {
            getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(pnd_I),new ColumnSpacing(6),pnd_Team_Totals_Pnd_T_Status_P.getValue(pnd_I));                //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #I ) 6X #T-STATUS-P ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Team_Totals_Pnd_T_Sub_Total.nadd(pnd_Team_Totals_Pnd_T_Status_P.getValue(pnd_I));                                                                         //Natural: ADD #T-STATUS-P ( #I ) TO #T-SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I 7 9
        for (pnd_I.setValue(7); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(pnd_I),new ColumnSpacing(6),pnd_Team_Totals_Pnd_T_Status_P.getValue(pnd_I));                //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #I ) 6X #T-STATUS-P ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Team_Totals_Pnd_T_Sub_Total.nadd(pnd_Team_Totals_Pnd_T_Status_P.getValue(pnd_I));                                                                         //Natural: ADD #T-STATUS-P ( #I ) TO #T-SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"    Total premiums **",new ColumnSpacing(15),pnd_Team_Totals_Pnd_T_Sub_Total);                                                     //Natural: WRITE ( 1 ) / '    Total premiums **' 15X #T-SUB-TOTAL
        if (Global.isEscape()) return;
        pnd_Team_Totals_Pnd_T_Grand.nadd(pnd_Team_Totals_Pnd_T_Sub_Total);                                                                                                //Natural: ADD #T-SUB-TOTAL TO #T-GRAND
        getReports().write(1, NEWLINE,NEWLINE,"    Grand Total *****",new ColumnSpacing(15),pnd_Team_Totals_Pnd_T_Grand);                                                 //Natural: WRITE ( 1 ) //'    Grand Total *****' 15X #T-GRAND
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Print_Total_Page() throws Exception                                                                                                                  //Natural: PRINT-TOTAL-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *================================
        getReports().write(1, new ColumnSpacing(51),"PRAP Purge Control Statistics",NEWLINE,NEWLINE,"     Total records read from PRAP  ",pnd_Rec_In,NEWLINE,"     Total records written to PRAP ",pnd_Rec_Out,NEWLINE,NEWLINE,"_",new  //Natural: WRITE ( 1 ) 51X 'PRAP Purge Control Statistics' // '     Total records read from PRAP  ' #REC-IN / '     Total records written to PRAP ' #REC-OUT // '_' ( 132 ) // '    APPLICATIONS' /
            RepeatItem(132),NEWLINE,NEWLINE,"    APPLICATIONS",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(1),new ColumnSpacing(6),pnd_Status_A.getValue(1));                                              //Natural: WRITE ( 1 ) 6X #STAT-DESC ( 1 ) 6X #STATUS-A ( 1 )
        if (Global.isEscape()) return;
        pnd_Sub_Total.nadd(pnd_Status_A.getValue(1));                                                                                                                     //Natural: ADD #STATUS-A ( 1 ) TO #SUB-TOTAL
        getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(3),new ColumnSpacing(6),pnd_Status_A.getValue(3));                                              //Natural: WRITE ( 1 ) 6X #STAT-DESC ( 3 ) 6X #STATUS-A ( 3 )
        if (Global.isEscape()) return;
        pnd_Sub_Total.nadd(pnd_Status_A.getValue(3));                                                                                                                     //Natural: ADD #STATUS-A ( 3 ) TO #SUB-TOTAL
        getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(4),new ColumnSpacing(6),pnd_Status_A.getValue(4));                                              //Natural: WRITE ( 1 ) 6X #STAT-DESC ( 4 ) 6X #STATUS-A ( 4 )
        if (Global.isEscape()) return;
        pnd_Sub_Total.nadd(pnd_Status_A.getValue(4));                                                                                                                     //Natural: ADD #STATUS-A ( 4 ) TO #SUB-TOTAL
        FOR04:                                                                                                                                                            //Natural: FOR #I 7 9
        for (pnd_I.setValue(7); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(pnd_I),new ColumnSpacing(6),pnd_Status_A.getValue(pnd_I));                                  //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #I ) 6X #STATUS-A ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Sub_Total.nadd(pnd_Status_A.getValue(pnd_I));                                                                                                             //Natural: ADD #STATUS-A ( #I ) TO #SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"    Total Apps ******",new ColumnSpacing(15),pnd_Sub_Total);                                                                       //Natural: WRITE ( 1 ) / '    Total Apps ******' 15X #SUB-TOTAL
        if (Global.isEscape()) return;
        pnd_Grand.nadd(pnd_Sub_Total);                                                                                                                                    //Natural: ADD #SUB-TOTAL TO #GRAND
        pnd_Sub_Total.reset();                                                                                                                                            //Natural: RESET #SUB-TOTAL
        getReports().write(1, NEWLINE,NEWLINE,"    PREMIUMS",NEWLINE);                                                                                                    //Natural: WRITE ( 1 ) // '    PREMIUMS' /
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 3
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
        {
            getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(pnd_I),new ColumnSpacing(6),pnd_Status_P.getValue(pnd_I));                                  //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #I ) 6X #STATUS-P ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Sub_Total.nadd(pnd_Status_P.getValue(pnd_I));                                                                                                             //Natural: ADD #STATUS-P ( #I ) TO #SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #I 7 9
        for (pnd_I.setValue(7); condition(pnd_I.lessOrEqual(9)); pnd_I.nadd(1))
        {
            getReports().write(1, new ColumnSpacing(6),pnd_Stat_Desc.getValue(pnd_I),new ColumnSpacing(6),pnd_Status_P.getValue(pnd_I));                                  //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #I ) 6X #STATUS-P ( #I )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Sub_Total.nadd(pnd_Status_P.getValue(pnd_I));                                                                                                             //Natural: ADD #STATUS-P ( #I ) TO #SUB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"    Total premiums **",new ColumnSpacing(15),pnd_Sub_Total);                                                                       //Natural: WRITE ( 1 ) / '    Total premiums **' 15X #SUB-TOTAL
        if (Global.isEscape()) return;
        pnd_Grand.nadd(pnd_Sub_Total);                                                                                                                                    //Natural: ADD #SUB-TOTAL TO #GRAND
        getReports().write(1, NEWLINE,NEWLINE,"    Grand Total *****",new ColumnSpacing(15),pnd_Grand);                                                                   //Natural: WRITE ( 1 ) //'    Grand Total *****' 15X #GRAND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"Error:     ",Global.getERROR_NR());                 //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'Error:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Cv_Data_Cv_Ppg_Team_CdeIsBreak = pnd_Cv_Data_Cv_Ppg_Team_Cde.isBreak(endOfData);
        if (condition(pnd_Cv_Data_Cv_Ppg_Team_CdeIsBreak))
        {
            pnd_Team.setValue(readWork01Cv_Ppg_Team_CdeOld);                                                                                                              //Natural: MOVE OLD ( CV-PPG-TEAM-CDE ) TO #TEAM
            if (condition(pnd_Team.equals(" ")))                                                                                                                          //Natural: IF #TEAM EQ ' '
            {
                pnd_Team.setValue("UNKNOWN");                                                                                                                             //Natural: MOVE 'UNKNOWN' TO #TEAM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TEAM-TOTAL-PAGE
            sub_Team_Total_Page();
            if (condition(Global.isEscape())) {return;}
            pnd_Team_Totals.reset();                                                                                                                                      //Natural: RESET #TEAM-TOTALS
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 HW=OFF PS=52");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(120),Global.getDATU(),NEWLINE,"PAGE",getReports().getPageNumberDbs(1),new 
            TabSetting(120),Global.getTIMX(),NEWLINE,NEWLINE,"-",new RepeatItem(132),NEWLINE,NEWLINE);
    }
}
