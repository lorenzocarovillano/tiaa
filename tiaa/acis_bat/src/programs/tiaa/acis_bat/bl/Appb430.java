/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:48 PM
**        * FROM NATURAL PROGRAM : Appb430
************************************************************
**        * FILE NAME            : Appb430.java
**        * CLASS NAME           : Appb430
**        * INSTANCE NAME        : Appb430
************************************************************
************************************************************************
* PROGRAM  : APPB4300 - ANNUITY FUNDING DETECTION
* AUTHOR   : G. SUDSATAYA
* CREATED  : 5/2011
* FUNCTION : RETRIEVES THE ANNUITY FUNDING DATE AND CALCULATES THE
*            ANNUITY ISSUE DATES FOR CONTRACTS SUBJECT TO THE LEGAL
*            PACKAGE ANNUITY OPTION (LPAO).  THIS IS THE ONLY MODULE
*            WHERE CONTRACTS SUBJECT TO LPAO CAN BE MARKED AS FUNDED.
*
*            THIS PROGRAM READS THE LAST 5 GENERATIONS OF THE FIRST
*            DOLLAR FILE. IF THE INPUT RECORD CONTAINS AN ANNUITY FUND
*            (TIAA TRADITIONAL, TIAA REAL ESTATE, CREF FUNDS, OR ANY OF
*            THE TIAA ACCESS FUNDS) AND THE CONTRACT IS A GROUP CONTRACT
*            (GRA, GSRA, RC OR RCP), THE PROGRAM WILL TRY TO FIND A
*            MATCH ON THE ACIS PRAP FILE. THE PRAP RECORD WILL BE
*            UPDATED IF THE RECORD IS SUBJECT TO LPAO AND NOT DELETED
*            OR MATCHED/RELEASED. THE FOLLOWING WILL BE UPDATED:
*            1) USE THE ACTIVITY DATE FROM THE FIRST DOLLAR FILE TO
*               UPDATE THE ANNUITY FUNDING DATE.
*            2) CALCULATE THE TIAA AND CREF ISSUE DATES USING THE MONTH
*               AND YEAR OF THE ANNUITY FUNDING DATE IF THE ORIGINAL
*               ISSUE DATE WAS POPULATED.
*            3) UPDATE THE AP-STATUS INDICATOR TO 'C' TO MARK THE RECORD
*               AS FUNDED.
*
*            IF THE CONTRACT IS NOT SUBJECT TO LPAO, IT WILL BE SKIPPED
*            AND FOLLOW THE CURRENT FUNDING (MATCH AND RELEASE) PROCESS.
*
* INPUT    : LAST 5 GENERATIONS OF THE FIRST DOLLAR FILES FROM THE
*            PROSPECTUS APPLICATION.  ANY CHANGES TO THE PTCFEVT COBOL
*            COPYBOOK WILL REQUIRE CHANGES TO THE INPUT LAYOUT IN THIS
*            PROGRAM.
*
* OUTPUT   : UPDATES THE ANNTY-ACTVTY-PRAP FILE AND PRODUCES REPORTS ON
*            UPDATED RECORDS
*
**--------------------------------------------------------------------
* CHANGE HISTORY    (MOST RECENT AT BOTTOM)
*
* IMPL      CHANGED                                          CHANGE
* DATE      BY         CHANGE DESCRIPTION                    TAG
* --------  ---------  ------------------------------------  ---------
* 01/05/12             IMPLEMENTING IN LIGHTS OUT MODE
* 11/17/12  L. SHU     MOVE ANNUITY FUND DATE AND ANNUITY
*                      ISSUE DATE FOR GROUP PRODUCT, NOT
*                      DELETED AND SUBJECT TO LEGAL PACKAGE
*                      ANNUITY OPTION .
*                      READ NEW EXTERNALIZATION AND BUILD
*                      AN ARRAY OF 500 OCCURANCE OF ANNUITY
*                      FUNDS FOR IDENTIFYING ANNUITY FUNDS
*                      FROM FIRST DOLLARS FILE.
* 06/20/14  L. SHU     REMOVING HARD CODING OF CUSIP AND       CREA
*                      'ANW' FOR ACCESS FUND, REPLACING
*                      WITH LOOKUP TO NEW EXTERNALISATION
*                      USING INVESTMENT TYPE ID = F,R AND V
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE. **
* 10/09/2017  B. NEWSOM - INITIAL PREMIUM MODIFICATION         (IPM)
*                       AP-FINANCIAL-2 = TIAA ANNTY INIT PREM AMT  (IPM)
*                       AP-FINANCIAL-3 = CREF ANNTY INIT PREM AMT  (IPM)
*                       AP-FINANCIAL-4 = INIT PREM AMT - ANY FUND  (IPM)
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb430 extends BLNatBase
{
    // Data Areas
    private PdaScia8251 pdaScia8251;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;

    private DbsGroup annty_Actvty_Prap_View_Ap_Tiaa_Contr;
    private DbsField annty_Actvty_Prap_View_App_Pref;
    private DbsField annty_Actvty_Prap_View_App_Cont;

    private DbsGroup annty_Actvty_Prap_View_Ap_Cref_Contr;
    private DbsField annty_Actvty_Prap_View_App_C_Pref;
    private DbsField annty_Actvty_Prap_View_App_C_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Matched;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_Ann_Option;
    private DbsField annty_Actvty_Prap_View_Ap_Ann_Funding_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Oneira_Acct_No;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_2;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_3;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_4;
    private DbsField annty_Actvty_Prap_View_Ap_Decedent_Contract;

    private DataAccessProgramView vw_new_Ext_Cntrl_Fnd;
    private DbsField new_Ext_Cntrl_Fnd_Nec_Table_Cde;
    private DbsField new_Ext_Cntrl_Fnd_Nec_Ticker_Symbol;
    private DbsField new_Ext_Cntrl_Fnd_Nec_Fund_Family_Cde;
    private DbsField new_Ext_Cntrl_Fnd_Nec_Fund_Inception_Dte;
    private DbsField new_Ext_Cntrl_Fnd_Nec_Fund_Cusip;

    private DbsGroup new_Ext_Cntrl_Fnd__R_Field_1;
    private DbsField new_Ext_Cntrl_Fnd_Pnd_Cusip_Access_Fund;
    private DbsField new_Ext_Cntrl_Fnd_Nec_Investment_Typ_Id;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_J;
    private DbsField pnd_Idx;
    private DbsField pnd_Fund;
    private DbsField pnd_Found_Tiaa_Access;

    private DbsGroup pnd_Cusip_Array;
    private DbsField pnd_Cusip_Array_Pnd_All_Cusip;
    private DbsField pnd_Date_Mmddyyyy_A;

    private DbsGroup pnd_Date_Mmddyyyy_A__R_Field_2;
    private DbsField pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N;
    private DbsField pnd_Issue_Date;

    private DbsGroup pnd_Issue_Date__R_Field_3;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_N;

    private DbsGroup pnd_Issue_Date__R_Field_4;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Yy1;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Yy2;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Mm;
    private DbsField pnd_Issue_Date_Pnd_Issue_Date_Dd;
    private DbsField pnd_Trade_Date;

    private DbsGroup pnd_Trade_Date__R_Field_5;
    private DbsField pnd_Trade_Date_Pnd_Trade_Date_N;
    private DbsField pnd_Hold_Sub_Plan;

    private DbsGroup pnd_Hold_Sub_Plan__R_Field_6;
    private DbsField pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1;
    private DbsField pnd_Hold_Sub_Plan_Pnd_Hold_Sub_2;
    private DbsField pnd_Ct_Input;
    private DbsField pnd_Ct_Tiaa_Cref_Input;
    private DbsField pnd_Ct_F;
    private DbsField pnd_Ct_Contract;
    private DbsField pnd_Ct_Update;
    private DbsField pnd_Ct_Update_Oneira;
    private DbsField pnd_Ct_Not_A;
    private DbsField pnd_Ct_Sub_Plan;
    private DbsField pnd_Ct_Skip_Prap;
    private DbsField pnd_Ct_Skip_Oneira;
    private DbsField pnd_Ct_Skip_Bip;
    private DbsField pnd_Ct_Cusip;
    private DbsField pnd_Ct_N_Reprint;
    private DbsField pnd_Ct_Nb_Reprint;
    private DbsField pnd_Ct_Nl_Reprint;
    private DbsField pnd_Ct_Nw_Reprint;
    private DbsField pnd_Ct_Y_Reprint;
    private DbsField pnd_Ct_Yb_Reprint;
    private DbsField pnd_Ct_Yl_Reprint;
    private DbsField pnd_Ct_Yw_Reprint;
    private DbsField pnd_Ct_Email_Reprint;
    private DbsField pnd_Reprint_Update;
    private DbsField pnd_D;

    private DbsGroup pnd_Ticker_Fund_Id_Table;
    private DbsField pnd_Ticker_Fund_Id_Table_Pnd_Fund_Id;
    private DbsField pnd_Ticker_Fund_Id_Table_Pnd_Ticker;
    private DbsField pnd_Ticker_Fund_Id_Table_Pnd_Investment_Type;

    private DbsGroup pnd_Ticker_Fund_Id;
    private DbsField pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Fund_Id;
    private DbsField pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Ticker;
    private DbsField pnd_Ticker_Fund_Id_Pnd_Investment_Typ;
    private DbsField pnd_Ticker_Fund_Id_Pnd_Share_Class;

    private DbsGroup first_Dollar_Event_Rec;
    private DbsField first_Dollar_Event_Rec_Fevt_Event_Indicator;
    private DbsField first_Dollar_Event_Rec_Fevt_Plan_Num;

    private DbsGroup first_Dollar_Event_Rec_Fevt_Part_Id;
    private DbsField first_Dollar_Event_Rec_Fevt_Part_Num;
    private DbsField first_Dollar_Event_Rec_Fevt_Sub_Plan;
    private DbsField first_Dollar_Event_Rec_Fevt_Pin;

    private DbsGroup first_Dollar_Event_Rec__R_Field_7;
    private DbsField first_Dollar_Event_Rec_Fevt_Pin_A7;
    private DbsField first_Dollar_Event_Rec_Filler_Pin_A7;
    private DbsField first_Dollar_Event_Rec_Fevt_Fund_Iv;
    private DbsField first_Dollar_Event_Rec_Fevt_Trade_Date;
    private DbsField first_Dollar_Event_Rec_Fevt_Activity_Date;
    private DbsField first_Dollar_Event_Rec_Fevt_Tiaa_Contract;
    private DbsField first_Dollar_Event_Rec_Fevt_Cusip;
    private DbsField first_Dollar_Event_Rec_Fevt_Plan_Type;
    private DbsField first_Dollar_Event_Rec_Fevt_Death_Date;
    private DbsField first_Dollar_Event_Rec_Fevt_Tran_Code;
    private DbsField first_Dollar_Event_Rec_Fevt_Activity_Code;
    private int psg9023ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaScia8251 = new PdaScia8251(localVariables);

        // Local Variables

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");

        annty_Actvty_Prap_View_Ap_Tiaa_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_TIAA_CONTR", "AP-TIAA-CONTR");
        annty_Actvty_Prap_View_App_Pref = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Pref", "APP-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_PREF");
        annty_Actvty_Prap_View_App_Cont = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Cont", "APP-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_CONT");

        annty_Actvty_Prap_View_Ap_Cref_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_CREF_CONTR", "AP-CREF-CONTR");
        annty_Actvty_Prap_View_App_C_Pref = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Pref", "APP-C-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_C_PREF");
        annty_Actvty_Prap_View_App_C_Cont = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Cont", "APP-C-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_C_CONT");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Dt_Matched = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Matched", "AP-DT-MATCHED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_MATCHED");
        annty_Actvty_Prap_View_Ap_Tiaa_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_View_Ap_Cref_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Doi", "AP-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp", 
            "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind", 
            "AP-WELC-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_WELC_E_DELIVERY_IND");
        annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind.setDdmHeader("WELC EDLVRY IND");
        annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind", 
            "AP-LEGAL-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_E_DELIVERY_IND");
        annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind.setDdmHeader("LGL EDLVRY IND");
        annty_Actvty_Prap_View_Ap_Legal_Ann_Option = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_Ann_Option", 
            "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        annty_Actvty_Prap_View_Ap_Ann_Funding_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ann_Funding_Dt", "AP-ANN-FUNDING-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_ANN_FUNDING_DT");
        annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt", 
            "AP-TIAA-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIAA_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt", 
            "AP-CREF-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_CREF_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Oneira_Acct_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Oneira_Acct_No", "AP-ONEIRA-ACCT-NO", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_ONEIRA_ACCT_NO");
        annty_Actvty_Prap_View_Ap_Financial_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_2", "AP-FINANCIAL-2", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_2");
        annty_Actvty_Prap_View_Ap_Financial_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_3", "AP-FINANCIAL-3", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_3");
        annty_Actvty_Prap_View_Ap_Financial_4 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_4", "AP-FINANCIAL-4", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_4");
        annty_Actvty_Prap_View_Ap_Decedent_Contract = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Decedent_Contract", 
            "AP-DECEDENT-CONTRACT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DECEDENT_CONTRACT");
        registerRecord(vw_annty_Actvty_Prap_View);

        vw_new_Ext_Cntrl_Fnd = new DataAccessProgramView(new NameInfo("vw_new_Ext_Cntrl_Fnd", "NEW-EXT-CNTRL-FND"), "NEW_EXT_CNTRL_FND", "NEW_EXT_CNTRL");
        new_Ext_Cntrl_Fnd_Nec_Table_Cde = vw_new_Ext_Cntrl_Fnd.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        new_Ext_Cntrl_Fnd_Nec_Ticker_Symbol = vw_new_Ext_Cntrl_Fnd.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        new_Ext_Cntrl_Fnd_Nec_Fund_Family_Cde = vw_new_Ext_Cntrl_Fnd.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_Nec_Fund_Family_Cde", "NEC-FUND-FAMILY-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_FUND_FAMILY_CDE");
        new_Ext_Cntrl_Fnd_Nec_Fund_Inception_Dte = vw_new_Ext_Cntrl_Fnd.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_Nec_Fund_Inception_Dte", "NEC-FUND-INCEPTION-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "NEC_FUND_INCEPTION_DTE");
        new_Ext_Cntrl_Fnd_Nec_Fund_Cusip = vw_new_Ext_Cntrl_Fnd.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_Nec_Fund_Cusip", "NEC-FUND-CUSIP", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_FUND_CUSIP");

        new_Ext_Cntrl_Fnd__R_Field_1 = vw_new_Ext_Cntrl_Fnd.getRecord().newGroupInGroup("new_Ext_Cntrl_Fnd__R_Field_1", "REDEFINE", new_Ext_Cntrl_Fnd_Nec_Fund_Cusip);
        new_Ext_Cntrl_Fnd_Pnd_Cusip_Access_Fund = new_Ext_Cntrl_Fnd__R_Field_1.newFieldInGroup("new_Ext_Cntrl_Fnd_Pnd_Cusip_Access_Fund", "#CUSIP-ACCESS-FUND", 
            FieldType.STRING, 3);
        new_Ext_Cntrl_Fnd_Nec_Investment_Typ_Id = vw_new_Ext_Cntrl_Fnd.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_Nec_Investment_Typ_Id", "NEC-INVESTMENT-TYP-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_INVESTMENT_TYP_ID");
        registerRecord(vw_new_Ext_Cntrl_Fnd);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 3);
        pnd_Fund = localVariables.newFieldInRecord("pnd_Fund", "#FUND", FieldType.STRING, 2);
        pnd_Found_Tiaa_Access = localVariables.newFieldInRecord("pnd_Found_Tiaa_Access", "#FOUND-TIAA-ACCESS", FieldType.BOOLEAN, 1);

        pnd_Cusip_Array = localVariables.newGroupInRecord("pnd_Cusip_Array", "#CUSIP-ARRAY");
        pnd_Cusip_Array_Pnd_All_Cusip = pnd_Cusip_Array.newFieldArrayInGroup("pnd_Cusip_Array_Pnd_All_Cusip", "#ALL-CUSIP", FieldType.STRING, 10, new 
            DbsArrayController(1, 600));
        pnd_Date_Mmddyyyy_A = localVariables.newFieldInRecord("pnd_Date_Mmddyyyy_A", "#DATE-MMDDYYYY-A", FieldType.STRING, 8);

        pnd_Date_Mmddyyyy_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Mmddyyyy_A__R_Field_2", "REDEFINE", pnd_Date_Mmddyyyy_A);
        pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N = pnd_Date_Mmddyyyy_A__R_Field_2.newFieldInGroup("pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N", "#DATE-MMDDYYYY-N", 
            FieldType.NUMERIC, 8);
        pnd_Issue_Date = localVariables.newFieldInRecord("pnd_Issue_Date", "#ISSUE-DATE", FieldType.STRING, 8);

        pnd_Issue_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Issue_Date__R_Field_3", "REDEFINE", pnd_Issue_Date);
        pnd_Issue_Date_Pnd_Issue_Date_N = pnd_Issue_Date__R_Field_3.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_N", "#ISSUE-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date__R_Field_4 = pnd_Issue_Date__R_Field_3.newGroupInGroup("pnd_Issue_Date__R_Field_4", "REDEFINE", pnd_Issue_Date_Pnd_Issue_Date_N);
        pnd_Issue_Date_Pnd_Issue_Date_Yy1 = pnd_Issue_Date__R_Field_4.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Yy1", "#ISSUE-DATE-YY1", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_Pnd_Issue_Date_Yy2 = pnd_Issue_Date__R_Field_4.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Yy2", "#ISSUE-DATE-YY2", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_Pnd_Issue_Date_Mm = pnd_Issue_Date__R_Field_4.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Mm", "#ISSUE-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_Pnd_Issue_Date_Dd = pnd_Issue_Date__R_Field_4.newFieldInGroup("pnd_Issue_Date_Pnd_Issue_Date_Dd", "#ISSUE-DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Trade_Date = localVariables.newFieldInRecord("pnd_Trade_Date", "#TRADE-DATE", FieldType.STRING, 8);

        pnd_Trade_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Trade_Date__R_Field_5", "REDEFINE", pnd_Trade_Date);
        pnd_Trade_Date_Pnd_Trade_Date_N = pnd_Trade_Date__R_Field_5.newFieldInGroup("pnd_Trade_Date_Pnd_Trade_Date_N", "#TRADE-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Hold_Sub_Plan = localVariables.newFieldInRecord("pnd_Hold_Sub_Plan", "#HOLD-SUB-PLAN", FieldType.STRING, 6);

        pnd_Hold_Sub_Plan__R_Field_6 = localVariables.newGroupInRecord("pnd_Hold_Sub_Plan__R_Field_6", "REDEFINE", pnd_Hold_Sub_Plan);
        pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1 = pnd_Hold_Sub_Plan__R_Field_6.newFieldInGroup("pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1", "#HOLD-SUB-1", FieldType.STRING, 
            3);
        pnd_Hold_Sub_Plan_Pnd_Hold_Sub_2 = pnd_Hold_Sub_Plan__R_Field_6.newFieldInGroup("pnd_Hold_Sub_Plan_Pnd_Hold_Sub_2", "#HOLD-SUB-2", FieldType.STRING, 
            3);
        pnd_Ct_Input = localVariables.newFieldInRecord("pnd_Ct_Input", "#CT-INPUT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Tiaa_Cref_Input = localVariables.newFieldInRecord("pnd_Ct_Tiaa_Cref_Input", "#CT-TIAA-CREF-INPUT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_F = localVariables.newFieldInRecord("pnd_Ct_F", "#CT-F", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Contract = localVariables.newFieldInRecord("pnd_Ct_Contract", "#CT-CONTRACT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Update = localVariables.newFieldInRecord("pnd_Ct_Update", "#CT-UPDATE", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Update_Oneira = localVariables.newFieldInRecord("pnd_Ct_Update_Oneira", "#CT-UPDATE-ONEIRA", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Not_A = localVariables.newFieldInRecord("pnd_Ct_Not_A", "#CT-NOT-A", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Sub_Plan = localVariables.newFieldInRecord("pnd_Ct_Sub_Plan", "#CT-SUB-PLAN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Skip_Prap = localVariables.newFieldInRecord("pnd_Ct_Skip_Prap", "#CT-SKIP-PRAP", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Skip_Oneira = localVariables.newFieldInRecord("pnd_Ct_Skip_Oneira", "#CT-SKIP-ONEIRA", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Skip_Bip = localVariables.newFieldInRecord("pnd_Ct_Skip_Bip", "#CT-SKIP-BIP", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Cusip = localVariables.newFieldInRecord("pnd_Ct_Cusip", "#CT-CUSIP", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_N_Reprint = localVariables.newFieldInRecord("pnd_Ct_N_Reprint", "#CT-N-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Nb_Reprint = localVariables.newFieldInRecord("pnd_Ct_Nb_Reprint", "#CT-NB-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Nl_Reprint = localVariables.newFieldInRecord("pnd_Ct_Nl_Reprint", "#CT-NL-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Nw_Reprint = localVariables.newFieldInRecord("pnd_Ct_Nw_Reprint", "#CT-NW-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Y_Reprint = localVariables.newFieldInRecord("pnd_Ct_Y_Reprint", "#CT-Y-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Yb_Reprint = localVariables.newFieldInRecord("pnd_Ct_Yb_Reprint", "#CT-YB-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Yl_Reprint = localVariables.newFieldInRecord("pnd_Ct_Yl_Reprint", "#CT-YL-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Yw_Reprint = localVariables.newFieldInRecord("pnd_Ct_Yw_Reprint", "#CT-YW-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Email_Reprint = localVariables.newFieldInRecord("pnd_Ct_Email_Reprint", "#CT-EMAIL-REPRINT", FieldType.PACKED_DECIMAL, 7);
        pnd_Reprint_Update = localVariables.newFieldInRecord("pnd_Reprint_Update", "#REPRINT-UPDATE", FieldType.BOOLEAN, 1);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);

        pnd_Ticker_Fund_Id_Table = localVariables.newGroupArrayInRecord("pnd_Ticker_Fund_Id_Table", "#TICKER-FUND-ID-TABLE", new DbsArrayController(1, 
            600));
        pnd_Ticker_Fund_Id_Table_Pnd_Fund_Id = pnd_Ticker_Fund_Id_Table.newFieldInGroup("pnd_Ticker_Fund_Id_Table_Pnd_Fund_Id", "#FUND-ID", FieldType.STRING, 
            2);
        pnd_Ticker_Fund_Id_Table_Pnd_Ticker = pnd_Ticker_Fund_Id_Table.newFieldInGroup("pnd_Ticker_Fund_Id_Table_Pnd_Ticker", "#TICKER", FieldType.STRING, 
            10);
        pnd_Ticker_Fund_Id_Table_Pnd_Investment_Type = pnd_Ticker_Fund_Id_Table.newFieldInGroup("pnd_Ticker_Fund_Id_Table_Pnd_Investment_Type", "#INVESTMENT-TYPE", 
            FieldType.STRING, 1);

        pnd_Ticker_Fund_Id = localVariables.newGroupInRecord("pnd_Ticker_Fund_Id", "#TICKER-FUND-ID");
        pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Fund_Id = pnd_Ticker_Fund_Id.newFieldInGroup("pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Fund_Id", "#TIAA-CREF-FUND-ID", 
            FieldType.STRING, 2);
        pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Ticker = pnd_Ticker_Fund_Id.newFieldInGroup("pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Ticker", "#TIAA-CREF-TICKER", FieldType.STRING, 
            10);
        pnd_Ticker_Fund_Id_Pnd_Investment_Typ = pnd_Ticker_Fund_Id.newFieldInGroup("pnd_Ticker_Fund_Id_Pnd_Investment_Typ", "#INVESTMENT-TYP", FieldType.STRING, 
            1);
        pnd_Ticker_Fund_Id_Pnd_Share_Class = pnd_Ticker_Fund_Id.newFieldInGroup("pnd_Ticker_Fund_Id_Pnd_Share_Class", "#SHARE-CLASS", FieldType.STRING, 
            2);

        first_Dollar_Event_Rec = localVariables.newGroupInRecord("first_Dollar_Event_Rec", "FIRST-DOLLAR-EVENT-REC");
        first_Dollar_Event_Rec_Fevt_Event_Indicator = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Event_Indicator", "FEVT-EVENT-INDICATOR", 
            FieldType.STRING, 1);
        first_Dollar_Event_Rec_Fevt_Plan_Num = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Plan_Num", "FEVT-PLAN-NUM", FieldType.STRING, 
            6);

        first_Dollar_Event_Rec_Fevt_Part_Id = first_Dollar_Event_Rec.newGroupInGroup("first_Dollar_Event_Rec_Fevt_Part_Id", "FEVT-PART-ID");
        first_Dollar_Event_Rec_Fevt_Part_Num = first_Dollar_Event_Rec_Fevt_Part_Id.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Part_Num", "FEVT-PART-NUM", 
            FieldType.NUMERIC, 9);
        first_Dollar_Event_Rec_Fevt_Sub_Plan = first_Dollar_Event_Rec_Fevt_Part_Id.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Sub_Plan", "FEVT-SUB-PLAN", 
            FieldType.STRING, 6);
        first_Dollar_Event_Rec_Fevt_Pin = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Pin", "FEVT-PIN", FieldType.STRING, 12);

        first_Dollar_Event_Rec__R_Field_7 = first_Dollar_Event_Rec.newGroupInGroup("first_Dollar_Event_Rec__R_Field_7", "REDEFINE", first_Dollar_Event_Rec_Fevt_Pin);
        first_Dollar_Event_Rec_Fevt_Pin_A7 = first_Dollar_Event_Rec__R_Field_7.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Pin_A7", "FEVT-PIN-A7", FieldType.STRING, 
            7);
        first_Dollar_Event_Rec_Filler_Pin_A7 = first_Dollar_Event_Rec__R_Field_7.newFieldInGroup("first_Dollar_Event_Rec_Filler_Pin_A7", "FILLER-PIN-A7", 
            FieldType.STRING, 5);
        first_Dollar_Event_Rec_Fevt_Fund_Iv = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Fund_Iv", "FEVT-FUND-IV", FieldType.STRING, 
            2);
        first_Dollar_Event_Rec_Fevt_Trade_Date = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Trade_Date", "FEVT-TRADE-DATE", FieldType.STRING, 
            8);
        first_Dollar_Event_Rec_Fevt_Activity_Date = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Activity_Date", "FEVT-ACTIVITY-DATE", 
            FieldType.STRING, 8);
        first_Dollar_Event_Rec_Fevt_Tiaa_Contract = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Tiaa_Contract", "FEVT-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        first_Dollar_Event_Rec_Fevt_Cusip = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Cusip", "FEVT-CUSIP", FieldType.STRING, 
            9);
        first_Dollar_Event_Rec_Fevt_Plan_Type = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Plan_Type", "FEVT-PLAN-TYPE", FieldType.STRING, 
            1);
        first_Dollar_Event_Rec_Fevt_Death_Date = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Death_Date", "FEVT-DEATH-DATE", FieldType.STRING, 
            8);
        first_Dollar_Event_Rec_Fevt_Tran_Code = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Tran_Code", "FEVT-TRAN-CODE", FieldType.STRING, 
            3);
        first_Dollar_Event_Rec_Fevt_Activity_Code = first_Dollar_Event_Rec.newFieldInGroup("first_Dollar_Event_Rec_Fevt_Activity_Code", "FEVT-ACTIVITY-CODE", 
            FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();
        vw_new_Ext_Cntrl_Fnd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb430() throws Exception
    {
        super("Appb430");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB430", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  IPM
        //*  IPM
        //*                                                                                                                                                               //Natural: FORMAT LS = 130 PS = 100;//Natural: FORMAT ( 1 ) LS = 130 PS = 80
        //*  WRITE OPENING OMNI FILES
        pdaScia8251.getI_O_Parameters_I_Files_Open_Sw().setValue("N");                                                                                                    //Natural: ASSIGN I-FILES-OPEN-SW := 'N'
        pdaScia8251.getI_O_Parameters_I_End_Of_File_Sw().setValue("N");                                                                                                   //Natural: ASSIGN I-END-OF-FILE-SW := 'N'
        //*  IPM
        psg9023ReturnCode = DbsUtil.callExternalProgram("PSG9023",pdaScia8251.getI_O_Parameters_I_Files_Open_Sw(),pdaScia8251.getI_O_Parameters_I_End_Of_File_Sw(),       //Natural: CALL 'PSG9023' USING I-O-PARAMETERS
            pdaScia8251.getI_O_Parameters_I_Plan_Num(),pdaScia8251.getI_O_Parameters_I_Part_Num(),pdaScia8251.getI_O_Parameters_I_Part_Sub_Plan(),pdaScia8251.getI_O_Parameters_I_Part_Ext(),
            pdaScia8251.getI_O_Parameters_I_Effective_Date(),pdaScia8251.getI_O_Parameters_O_Tiaa_Pin(),pdaScia8251.getI_O_Parameters_O_Tiaa_Contract(),
            pdaScia8251.getI_O_Parameters_O_Cref_Cert(),pdaScia8251.getI_O_Parameters_O_Fund_Id().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Cusip().getValue(1,
            ":",200),pdaScia8251.getI_O_Parameters_O_Shares().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Balance().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Total_Fund_Balance(),
            pdaScia8251.getI_O_Parameters_O_Return_Code(),pdaScia8251.getI_O_Parameters_O_Return_Message());
        //*  FILES-OPEN-SW  := 'Y'                                           /* IPM
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-ALL-CUSIP
        sub_Retrieve_All_Cusip();
        if (condition(Global.isEscape())) {return;}
        //*  IPM
        //*  IPM
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD #TICKER-FUND-ID
        while (condition(getWorkFiles().read(1, pnd_Ticker_Fund_Id)))
        {
            pnd_Ct_Tiaa_Cref_Input.nadd(1);                                                                                                                               //Natural: ASSIGN #CT-TIAA-CREF-INPUT := #CT-TIAA-CREF-INPUT + 1
            //*  IPM
            //*  IPM
            //*  IPM
            //*  IPM
            if (condition(pnd_Ct_Tiaa_Cref_Input.less(201)))                                                                                                              //Natural: IF #CT-TIAA-CREF-INPUT < 201
            {
                pnd_Ticker_Fund_Id_Table_Pnd_Fund_Id.getValue(pnd_Ct_Tiaa_Cref_Input).setValue(pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Fund_Id);                                 //Natural: ASSIGN #FUND-ID ( #CT-TIAA-CREF-INPUT ) := #TIAA-CREF-FUND-ID
                pnd_Ticker_Fund_Id_Table_Pnd_Ticker.getValue(pnd_Ct_Tiaa_Cref_Input).setValue(pnd_Ticker_Fund_Id_Pnd_Tiaa_Cref_Ticker);                                   //Natural: ASSIGN #TICKER ( #CT-TIAA-CREF-INPUT ) := #TIAA-CREF-TICKER
                pnd_Ticker_Fund_Id_Table_Pnd_Investment_Type.getValue(pnd_Ct_Tiaa_Cref_Input).setValue(pnd_Ticker_Fund_Id_Pnd_Investment_Typ);                            //Natural: ASSIGN #INVESTMENT-TYPE ( #CT-TIAA-CREF-INPUT ) := #INVESTMENT-TYP
                //*  IPM
            }                                                                                                                                                             //Natural: END-IF
            //*  IPM
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 RECORD FIRST-DOLLAR-EVENT-REC
        while (condition(getWorkFiles().read(2, first_Dollar_Event_Rec)))
        {
            pnd_Ct_Input.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CT-INPUT
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            pnd_Found_Tiaa_Access.setValue(false);                                                                                                                        //Natural: ASSIGN #FOUND-TIAA-ACCESS = FALSE
            //*  FIRST-DOLLAR-EVENT
            if (condition(first_Dollar_Event_Rec_Fevt_Event_Indicator.equals("F")))                                                                                       //Natural: IF FEVT-EVENT-INDICATOR = 'F'
            {
                pnd_Ct_F.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CT-F
                FOR01:                                                                                                                                                    //Natural: FOR #J = 1 TO #I
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_I)); pnd_J.nadd(1))
                {
                    //*  CHECKING FOR
                    if (condition(first_Dollar_Event_Rec_Fevt_Cusip.equals(pnd_Cusip_Array_Pnd_All_Cusip.getValue(pnd_J))))                                               //Natural: IF FEVT-CUSIP = #ALL-CUSIP ( #J )
                    {
                        //*  ANNUITY FUND
                        pnd_Found_Tiaa_Access.setValue(true);                                                                                                             //Natural: ASSIGN #FOUND-TIAA-ACCESS = TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Found_Tiaa_Access.getBoolean()))                                                                                                        //Natural: IF #FOUND-TIAA-ACCESS
                {
                    pnd_Ct_Cusip.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CT-CUSIP
                    pnd_Hold_Sub_Plan.setValue(first_Dollar_Event_Rec_Fevt_Sub_Plan);                                                                                     //Natural: MOVE FEVT-SUB-PLAN TO #HOLD-SUB-PLAN
                    //*  GSRA
                    //*  GRA
                    //*  RC
                    //*  RCP
                    //*  IPM
                    if (condition(pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("GS1") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("GR1") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("RS1")  //Natural: IF #HOLD-SUB-1 = 'GS1' OR = 'GR1' OR = 'RS1' OR = 'RS2' OR = 'RS3' OR = 'RP1' OR = 'RP2' OR = 'RP3' OR = 'IR1' OR = 'IR2' OR = 'IR3' OR = 'IR8'
                        || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("RS2") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("RS3") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("RP1") 
                        || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("RP2") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("RP3") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("IR1") 
                        || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("IR2") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("IR3") || pnd_Hold_Sub_Plan_Pnd_Hold_Sub_1.equals("IR8")))
                    {
                        pnd_Ct_Sub_Plan.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CT-SUB-PLAN
                                                                                                                                                                          //Natural: PERFORM FIND-PRAP-CONTRACT
                        sub_Find_Prap_Contract();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  (2460)  FEVT-EVENT-INDICATOR  = 'F'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  WRITE 'CLOSING OMNI FILES VIA PSG9023'                          /* IPM
        //*  IPM
        //*  IPM
        pdaScia8251.getI_O_Parameters().reset();                                                                                                                          //Natural: RESET I-O-PARAMETERS
        pdaScia8251.getI_O_Parameters_I_End_Of_File_Sw().setValue("Y");                                                                                                   //Natural: ASSIGN I-END-OF-FILE-SW := 'Y'
        //*  IPM
        psg9023ReturnCode = DbsUtil.callExternalProgram("PSG9023",pdaScia8251.getI_O_Parameters_I_Files_Open_Sw(),pdaScia8251.getI_O_Parameters_I_End_Of_File_Sw(),       //Natural: CALL 'PSG9023' USING I-O-PARAMETERS
            pdaScia8251.getI_O_Parameters_I_Plan_Num(),pdaScia8251.getI_O_Parameters_I_Part_Num(),pdaScia8251.getI_O_Parameters_I_Part_Sub_Plan(),pdaScia8251.getI_O_Parameters_I_Part_Ext(),
            pdaScia8251.getI_O_Parameters_I_Effective_Date(),pdaScia8251.getI_O_Parameters_O_Tiaa_Pin(),pdaScia8251.getI_O_Parameters_O_Tiaa_Contract(),
            pdaScia8251.getI_O_Parameters_O_Cref_Cert(),pdaScia8251.getI_O_Parameters_O_Fund_Id().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Cusip().getValue(1,
            ":",200),pdaScia8251.getI_O_Parameters_O_Shares().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Balance().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Total_Fund_Balance(),
            pdaScia8251.getI_O_Parameters_O_Return_Code(),pdaScia8251.getI_O_Parameters_O_Return_Message());
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            getReports().write(0, "NO FIRST DOLLAR WORK FILE           ",pnd_Ct_Input);                                                                                   //Natural: WRITE 'NO FIRST DOLLAR WORK FILE           ' #CT-INPUT
            if (Global.isEscape()) return;
            getReports().write(0, "NO. RECORD F     =                   ",pnd_Ct_F);                                                                                      //Natural: WRITE 'NO. RECORD F     =                   ' #CT-F
            if (Global.isEscape()) return;
            getReports().write(0, "NO. RECORD CUSIP - CUSIP FOUND       ",pnd_Ct_Cusip);                                                                                  //Natural: WRITE 'NO. RECORD CUSIP - CUSIP FOUND       ' #CT-CUSIP
            if (Global.isEscape()) return;
            getReports().write(0, "NO. RECORD SUB   - SUB PLAN FOUND    ",pnd_Ct_Sub_Plan);                                                                               //Natural: WRITE 'NO. RECORD SUB   - SUB PLAN FOUND    ' #CT-SUB-PLAN
            if (Global.isEscape()) return;
            getReports().write(0, "NO. RECORD PRAP  - CONTRACT FOUND    ",pnd_Ct_Contract);                                                                               //Natural: WRITE 'NO. RECORD PRAP  - CONTRACT FOUND    ' #CT-CONTRACT
            if (Global.isEscape()) return;
            getReports().write(0, "NO. RECORD PRAP  - STATUS NOT A/C    ",pnd_Ct_Not_A);                                                                                  //Natural: WRITE 'NO. RECORD PRAP  - STATUS NOT A/C    ' #CT-NOT-A
            if (Global.isEscape()) return;
            getReports().write(0, "NO. LPAO RECORDS - SKIPPED A/C       ",pnd_Ct_Skip_Prap);                                                                              //Natural: WRITE 'NO. LPAO RECORDS - SKIPPED A/C       ' #CT-SKIP-PRAP
            if (Global.isEscape()) return;
            getReports().write(0, "NO. IRA  RECORDS - SKIPPED A/C       ",pnd_Ct_Skip_Oneira);                                                                            //Natural: WRITE 'NO. IRA  RECORDS - SKIPPED A/C       ' #CT-SKIP-ONEIRA
            if (Global.isEscape()) return;
            getReports().write(0, "NO. IRA  RECORDS - SKIPPED BIP       ",pnd_Ct_Skip_Bip);                                                                               //Natural: WRITE 'NO. IRA  RECORDS - SKIPPED BIP       ' #CT-SKIP-BIP
            if (Global.isEscape()) return;
            getReports().write(0, "NO. RECORD PRAP  - UPDATE            ",pnd_Ct_Update);                                                                                 //Natural: WRITE 'NO. RECORD PRAP  - UPDATE            ' #CT-UPDATE
            if (Global.isEscape()) return;
            getReports().write(0, "NO. RECORD PRAP  - UPDATE FOR ONEIRA ",pnd_Ct_Update_Oneira);                                                                          //Natural: WRITE 'NO. RECORD PRAP  - UPDATE FOR ONEIRA ' #CT-UPDATE-ONEIRA
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                                                                                                                               //Natural: ON ERROR
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-PRAP-CONTRACT
        //* ***********************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-PRAP
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PRAP-FILE
        //* *3X   FEVT-PART-NUM
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-ALL-CUSIP
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-UPDATE-ONEIRA-INIT-PREMS
    }
    private void sub_Find_Prap_Contract() throws Exception                                                                                                                //Natural: FIND-PRAP-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        vw_annty_Actvty_Prap_View.startDatabaseFind                                                                                                                       //Natural: FIND ANNTY-ACTVTY-PRAP-VIEW WITH AP-TIAA-CNTRCT = FEVT-TIAA-CONTRACT
        (
        "PND_PND_L3000",
        new Wc[] { new Wc("AP_TIAA_CNTRCT", "=", first_Dollar_Event_Rec_Fevt_Tiaa_Contract, WcType.WITH) }
        );
        PND_PND_L3000:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("PND_PND_L3000")))
        {
            vw_annty_Actvty_Prap_View.setIfNotFoundControlFlag(false);
            //*  IF AP-STATUS = 'A' OR = 'C' OR (AP-LEGAL-ANN-OPTION NOT = 'Y')  /* IPM
            //*    ADD 1 TO #CT-SKIP-PRAP                                        /* IPM
            //*    ESCAPE TOP                                                    /* IPM
            //*  END-IF                                                          /* IPM
            //*  DELETED STATUS        /* IPM
            //*  IPM
            //*  IPM
            if (condition(annty_Actvty_Prap_View_Ap_Status.equals("A") || (annty_Actvty_Prap_View_Ap_Status.equals("C") && annty_Actvty_Prap_View_Ap_Oneira_Acct_No.equals(" "))  //Natural: IF AP-STATUS = 'A' OR ( AP-STATUS = 'C' AND AP-ONEIRA-ACCT-NO = ' ' ) OR ( AP-LEGAL-ANN-OPTION NE 'Y' AND AP-ONEIRA-ACCT-NO = ' ' )
                || (annty_Actvty_Prap_View_Ap_Legal_Ann_Option.notEquals("Y") && annty_Actvty_Prap_View_Ap_Oneira_Acct_No.equals(" "))))
            {
                //*  IPM
                pnd_Ct_Skip_Prap.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CT-SKIP-PRAP
                //*  IPM
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  IPM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ct_Contract.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CT-CONTRACT
            PND_PND_L3180:                                                                                                                                                //Natural: GET ANNTY-ACTVTY-PRAP-VIEW *ISN ( ##L3000. )
            vw_annty_Actvty_Prap_View.readByID(vw_annty_Actvty_Prap_View.getAstISN("PND_PND_L3000"), "PND_PND_L3180");
            if (condition(annty_Actvty_Prap_View_Ap_Legal_Ann_Option.equals("Y")))                                                                                        //Natural: IF AP-LEGAL-ANN-OPTION = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM VALIDATE-PRAP
                sub_Validate_Prap();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L3000"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3000"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ct_Update.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CT-UPDATE
                                                                                                                                                                          //Natural: PERFORM PRINT-PRAP-FILE
                sub_Print_Prap_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L3000"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3000"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  IPM - BENE IN THE PLAN
                if (condition(annty_Actvty_Prap_View_Ap_Decedent_Contract.greater(" ")))                                                                                  //Natural: IF AP-DECEDENT-CONTRACT GT ' '
                {
                    //*  IPM
                    pnd_Ct_Skip_Bip.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CT-SKIP-BIP
                    //*  IPM
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPM
                    if (condition(annty_Actvty_Prap_View_Ap_Oneira_Acct_No.greater(" ")))                                                                                 //Natural: IF AP-ONEIRA-ACCT-NO GT ' '
                    {
                        //*  TIAA PREM EXISTS IN PRAP  /* IPM
                        //*  CREF PREM EXISTS IN PRAP  /* IPM
                        //*  PREM EXISTS ANY FUND      /* IPM
                        if (condition(annty_Actvty_Prap_View_Ap_Financial_2.greater(getZero()) || annty_Actvty_Prap_View_Ap_Financial_3.greater(getZero())                //Natural: IF AP-FINANCIAL-2 GT 0 OR AP-FINANCIAL-3 GT 0 OR AP-FINANCIAL-4 GT 0
                            || annty_Actvty_Prap_View_Ap_Financial_4.greater(getZero())))
                        {
                            //*  IPM
                            pnd_Ct_Skip_Oneira.nadd(1);                                                                                                                   //Natural: ADD 1 TO #CT-SKIP-ONEIRA
                            //*  IPM
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                            //*  IPM
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-UPDATE-ONEIRA-INIT-PREMS
                        sub_Get_Update_Oneira_Init_Prems();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L3000"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3000"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            vw_annty_Actvty_Prap_View.updateDBRow("PND_PND_L3180");                                                                                                       //Natural: UPDATE ( ##L3180. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Validate_Prap() throws Exception                                                                                                                     //Natural: VALIDATE-PRAP
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        //*  ANNUITY ISSUE DATES WILL BE POPULATED ONLY IF THE ORIGINAL ISSUE DATE
        //*  WAS POPULATED FOR EACH PORTION (TIAA AND CREF) OF THE CONTRACT.
        //*  THIS PREVENTS CHANGING A COMPANION OR SINGLE ISSUANCE (RC OR RCP)
        //*  CONTRACT INTO ANOTHER TYPE OF CONTRACT WITH BOTH PORTIONS ISSUED.
        //* ***********************************************************************
        pnd_Ct_Not_A.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CT-NOT-A
        annty_Actvty_Prap_View_Ap_Status.setValue("C");                                                                                                                   //Natural: ASSIGN AP-STATUS := 'C'
        pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),first_Dollar_Event_Rec_Fevt_Activity_Date);                                                                   //Natural: MOVE EDITED FEVT-ACTIVITY-DATE TO #D ( EM = YYYYMMDD )
        pnd_Issue_Date.setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                                                              //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO #ISSUE-DATE
        pnd_Issue_Date_Pnd_Issue_Date_Dd.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #ISSUE-DATE-DD
        if (condition(annty_Actvty_Prap_View_Ap_T_Doi.notEquals(getZero())))                                                                                              //Natural: IF AP-T-DOI NOT = 0
        {
            annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt.setValue(pnd_Issue_Date_Pnd_Issue_Date_N);                                                                        //Natural: MOVE #ISSUE-DATE-N TO AP-TIAA-ANN-ISSUE-DT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_C_Doi.notEquals(getZero())))                                                                                              //Natural: IF AP-C-DOI NOT = 0
        {
            annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt.setValue(pnd_Issue_Date_Pnd_Issue_Date_N);                                                                        //Natural: MOVE #ISSUE-DATE-N TO AP-CREF-ANN-ISSUE-DT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_Mmddyyyy_A.setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYYYY"));                                                                              //Natural: MOVE EDITED *DATX ( EM = MMDDYYYY ) TO #DATE-MMDDYYYY-A
        annty_Actvty_Prap_View_Ap_Dt_Matched.setValue(pnd_Date_Mmddyyyy_A_Pnd_Date_Mmddyyyy_N);                                                                           //Natural: MOVE #DATE-MMDDYYYY-N TO AP-DT-MATCHED
        pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),first_Dollar_Event_Rec_Fevt_Trade_Date);                                                                      //Natural: MOVE EDITED FEVT-TRADE-DATE TO #D ( EM = YYYYMMDD )
        pnd_Trade_Date.setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                                                              //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO #TRADE-DATE
        annty_Actvty_Prap_View_Ap_Ann_Funding_Dt.setValue(pnd_Trade_Date_Pnd_Trade_Date_N);                                                                               //Natural: MOVE #TRADE-DATE-N TO AP-ANN-FUNDING-DT
    }
    private void sub_Print_Prap_File() throws Exception                                                                                                                   //Natural: PRINT-PRAP-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        getReports().write(1, new ColumnSpacing(1),first_Dollar_Event_Rec_Fevt_Pin,new ColumnSpacing(2),first_Dollar_Event_Rec_Fevt_Sub_Plan,new ColumnSpacing(1),first_Dollar_Event_Rec_Fevt_Activity_Date,new  //Natural: WRITE ( 1 ) 1X FEVT-PIN 2X FEVT-SUB-PLAN 1X FEVT-ACTIVITY-DATE 2X FEVT-CUSIP 2X FEVT-TIAA-CONTRACT 2X AP-TIAA-CNTRCT 3X AP-STATUS 2X AP-TIAA-ANN-ISSUE-DT 2X AP-CREF-ANN-ISSUE-DT 2X AP-DT-MATCHED
            ColumnSpacing(2),first_Dollar_Event_Rec_Fevt_Cusip,new ColumnSpacing(2),first_Dollar_Event_Rec_Fevt_Tiaa_Contract,new ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,new 
            ColumnSpacing(3),annty_Actvty_Prap_View_Ap_Status,new ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt,new ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt,new 
            ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Dt_Matched);
        if (Global.isEscape()) return;
    }
    //*  START OF LPAO
    private void sub_Retrieve_All_Cusip() throws Exception                                                                                                                //Natural: RETRIEVE-ALL-CUSIP
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************        /* GET ALL TIAA ACCESS CUSIP
        pnd_Cusip_Array.reset();                                                                                                                                          //Natural: RESET #CUSIP-ARRAY
        //*  COMMENTED AND REPLACE WITH NEW EXTERNALISATION FILE      START CREA
        //* *ASSIGN #ALL-CUSIP (1)  =  '878094101'         /* TIAA#
        //* *ASSIGN #ALL-CUSIP (2)  =  '878094200'         /* TREA#
        //* *ASSIGN #ALL-CUSIP (3)  =  '194408803'         /* CSTK#
        //* *ASSIGN #ALL-CUSIP (4)  =  '194408704'         /* CMMA#
        //* *ASSIGN #ALL-CUSIP (5)  =  '194408605'         /* CSCL#
        //* *ASSIGN #ALL-CUSIP (6)  =  '194408506'         /* CILB#
        //* *ASSIGN #ALL-CUSIP (7)  =  '194408407'         /* CBND#
        //* *ASSIGN #ALL-CUSIP (8)  =  '194408308'         /* CEQX#
        //* *ASSIGN #ALL-CUSIP (9)  =  '194408209'         /* CGRW#
        //* *ASSIGN #ALL-CUSIP (10) =  '194408100'         /* CGLB#
        //* *ASSIGN #I              = 10
        //*  END OF CREFREA
        //*  READ EXTERNALISATION FND FILE AND SELECT INVESTMENT TYPE AS V,F,R
        //*  TO REPLACE HARD CODING OF PRICE ID AND 'ANW'  TIAA ACCESS FUNDS.
        vw_new_Ext_Cntrl_Fnd.startDatabaseRead                                                                                                                            //Natural: READ NEW-EXT-CNTRL-FND BY NEC-TABLE-CDE = 'FND'
        (
        "READ03",
        new Wc[] { new Wc("NEC_TABLE_CDE", ">=", "FND", WcType.BY) },
        new Oc[] { new Oc("NEC_TABLE_CDE", "ASC") }
        );
        READ03:
        while (condition(vw_new_Ext_Cntrl_Fnd.readNextRow("READ03")))
        {
            //*  ACCEPT IF #CUSIP-ACCESS-FUND  =  'ANW'  /* ANW = TIAA ACCESS   CREA
            //*  CREA
            if (condition(!(new_Ext_Cntrl_Fnd_Nec_Investment_Typ_Id.equals("F") || new_Ext_Cntrl_Fnd_Nec_Investment_Typ_Id.equals("R") || new_Ext_Cntrl_Fnd_Nec_Investment_Typ_Id.equals("V")))) //Natural: ACCEPT IF NEC-INVESTMENT-TYP-ID = 'F' OR = 'R' OR = 'V'
            {
                continue;
            }
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            if (condition(pnd_I.greater(599)))                                                                                                                            //Natural: IF #I > 599
            {
                getReports().write(0, "***************************************",NEWLINE,"*   ARRAY HAS REACHED MAXIMUM OF 600  *",NEWLINE,"***************************************"); //Natural: WRITE '***************************************' / '*   ARRAY HAS REACHED MAXIMUM OF 600  *' / '***************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cusip_Array_Pnd_All_Cusip.getValue(pnd_I).setValue(new_Ext_Cntrl_Fnd_Nec_Fund_Cusip);                                                                 //Natural: ASSIGN #ALL-CUSIP ( #I ) := NEC-FUND-CUSIP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  END OF LPAO
    }
    //*  IPM - NEW SUBROUTINE
    private void sub_Get_Update_Oneira_Init_Prems() throws Exception                                                                                                      //Natural: GET-UPDATE-ONEIRA-INIT-PREMS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        pdaScia8251.getI_O_Parameters_Output_Parms().reset();                                                                                                             //Natural: RESET OUTPUT-PARMS
        pdaScia8251.getI_O_Parameters_I_Plan_Num().setValue(first_Dollar_Event_Rec_Fevt_Plan_Num);                                                                        //Natural: ASSIGN I-PLAN-NUM := FEVT-PLAN-NUM
        pdaScia8251.getI_O_Parameters_I_Part_Num().setValue(first_Dollar_Event_Rec_Fevt_Part_Num);                                                                        //Natural: ASSIGN I-PART-NUM := FEVT-PART-NUM
        pdaScia8251.getI_O_Parameters_I_Part_Sub_Plan().setValue(first_Dollar_Event_Rec_Fevt_Sub_Plan);                                                                   //Natural: ASSIGN I-PART-SUB-PLAN := FEVT-SUB-PLAN
        pdaScia8251.getI_O_Parameters_I_Part_Ext().setValue(" ");                                                                                                         //Natural: ASSIGN I-PART-EXT := ' '
        pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),first_Dollar_Event_Rec_Fevt_Trade_Date);                                                                      //Natural: MOVE EDITED FEVT-TRADE-DATE TO #D ( EM = YYYYMMDD )
        pnd_Trade_Date.setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                                                              //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO #TRADE-DATE
        pdaScia8251.getI_O_Parameters_I_Effective_Date().setValue(pnd_Trade_Date_Pnd_Trade_Date_N);                                                                       //Natural: MOVE #TRADE-DATE-N TO I-EFFECTIVE-DATE
        psg9023ReturnCode = DbsUtil.callExternalProgram("PSG9023",pdaScia8251.getI_O_Parameters_I_Files_Open_Sw(),pdaScia8251.getI_O_Parameters_I_End_Of_File_Sw(),       //Natural: CALL 'PSG9023' USING I-O-PARAMETERS
            pdaScia8251.getI_O_Parameters_I_Plan_Num(),pdaScia8251.getI_O_Parameters_I_Part_Num(),pdaScia8251.getI_O_Parameters_I_Part_Sub_Plan(),pdaScia8251.getI_O_Parameters_I_Part_Ext(),
            pdaScia8251.getI_O_Parameters_I_Effective_Date(),pdaScia8251.getI_O_Parameters_O_Tiaa_Pin(),pdaScia8251.getI_O_Parameters_O_Tiaa_Contract(),
            pdaScia8251.getI_O_Parameters_O_Cref_Cert(),pdaScia8251.getI_O_Parameters_O_Fund_Id().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Cusip().getValue(1,
            ":",200),pdaScia8251.getI_O_Parameters_O_Shares().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Balance().getValue(1,":",200),pdaScia8251.getI_O_Parameters_O_Total_Fund_Balance(),
            pdaScia8251.getI_O_Parameters_O_Return_Code(),pdaScia8251.getI_O_Parameters_O_Return_Message());
        if (condition(pdaScia8251.getI_O_Parameters_O_Return_Code().greater(getZero())))                                                                                  //Natural: IF O-RETURN-CODE GT 0
        {
            getReports().write(0, "ERROR FROM PSG9023",pdaScia8251.getI_O_Parameters_O_Return_Message());                                                                 //Natural: WRITE 'ERROR FROM PSG9023' O-RETURN-MESSAGE
            if (Global.isEscape()) return;
            getReports().write(0, "  FOR:",first_Dollar_Event_Rec_Fevt_Plan_Num,first_Dollar_Event_Rec_Fevt_Part_Num,first_Dollar_Event_Rec_Fevt_Sub_Plan);               //Natural: WRITE '  FOR:' FEVT-PLAN-NUM FEVT-PART-NUM FEVT-SUB-PLAN
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaScia8251.getI_O_Parameters_O_Total_Fund_Balance().equals(getZero())))                                                                        //Natural: IF O-TOTAL-FUND-BALANCE = 0
            {
                getReports().write(0, "NO FUND BALANCE FOUND FOR CONTRACT",first_Dollar_Event_Rec_Fevt_Plan_Num,first_Dollar_Event_Rec_Fevt_Part_Num,first_Dollar_Event_Rec_Fevt_Sub_Plan); //Natural: WRITE 'NO FUND BALANCE FOUND FOR CONTRACT' FEVT-PLAN-NUM FEVT-PART-NUM FEVT-SUB-PLAN
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "CALLED PSG9023:",first_Dollar_Event_Rec_Fevt_Plan_Num,first_Dollar_Event_Rec_Fevt_Part_Num,first_Dollar_Event_Rec_Fevt_Sub_Plan);          //Natural: WRITE 'CALLED PSG9023:' FEVT-PLAN-NUM FEVT-PART-NUM FEVT-SUB-PLAN
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #X = 1 TO 200
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(200)); pnd_X.nadd(1))
        {
            if (condition(pdaScia8251.getI_O_Parameters_O_Fund_Id().getValue(pnd_X).equals(" ")))                                                                         //Natural: IF O-FUND-ID ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaScia8251.getI_O_Parameters_O_Shares().getValue(pnd_X).greater(getZero())))                                                                   //Natural: IF O-SHARES ( #X ) GT 0
            {
                pnd_Fund.setValue(pdaScia8251.getI_O_Parameters_O_Fund_Id().getValue(pnd_X).getSubstring(1,2));                                                           //Natural: ASSIGN #FUND := SUBSTR ( O-FUND-ID ( #X ) ,1,2 )
                DbsUtil.examine(new ExamineSource(pnd_Ticker_Fund_Id_Table_Pnd_Fund_Id.getValue("*")), new ExamineSearch(pnd_Fund), new ExamineGivingIndex(pnd_Idx));     //Natural: EXAMINE #FUND-ID ( * ) FOR #FUND INDEX #IDX
                if (condition(pnd_Idx.greater(getZero())))                                                                                                                //Natural: IF #IDX > 0
                {
                    if (condition(pnd_Ticker_Fund_Id_Table_Pnd_Investment_Type.getValue(pnd_Idx).equals("T")))                                                            //Natural: IF #INVESTMENT-TYPE ( #IDX ) = 'T'
                    {
                        if (condition(annty_Actvty_Prap_View_Ap_Financial_2.add(pdaScia8251.getI_O_Parameters_O_Balance().getValue(pnd_X)).greater(new                    //Natural: IF AP-FINANCIAL-2 + O-BALANCE ( #X ) GT 99999999.99
                            DbsDecimal("99999999.99"))))
                        {
                            getReports().write(0, "AMOUNT IS MAXED OUT FOR TIAA",first_Dollar_Event_Rec_Fevt_Plan_Num,first_Dollar_Event_Rec_Fevt_Part_Num,               //Natural: WRITE 'AMOUNT IS MAXED OUT FOR TIAA' FEVT-PLAN-NUM FEVT-PART-NUM FEVT-SUB-PLAN
                                first_Dollar_Event_Rec_Fevt_Sub_Plan);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            annty_Actvty_Prap_View_Ap_Financial_2.nadd(pdaScia8251.getI_O_Parameters_O_Balance().getValue(pnd_X));                                        //Natural: ASSIGN AP-FINANCIAL-2 := AP-FINANCIAL-2 + O-BALANCE ( #X )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Ticker_Fund_Id_Table_Pnd_Investment_Type.getValue(pnd_Idx).equals("C")))                                                        //Natural: IF #INVESTMENT-TYPE ( #IDX ) = 'C'
                        {
                            if (condition(annty_Actvty_Prap_View_Ap_Financial_3.add(pdaScia8251.getI_O_Parameters_O_Balance().getValue(pnd_X)).greater(new                //Natural: IF AP-FINANCIAL-3 + O-BALANCE ( #X ) GT 99999999.99
                                DbsDecimal("99999999.99"))))
                            {
                                getReports().write(0, "AMOUNT IS MAXED OUT FOR CREF",first_Dollar_Event_Rec_Fevt_Plan_Num,first_Dollar_Event_Rec_Fevt_Part_Num,           //Natural: WRITE 'AMOUNT IS MAXED OUT FOR CREF' FEVT-PLAN-NUM FEVT-PART-NUM FEVT-SUB-PLAN
                                    first_Dollar_Event_Rec_Fevt_Sub_Plan);
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                annty_Actvty_Prap_View_Ap_Financial_3.nadd(pdaScia8251.getI_O_Parameters_O_Balance().getValue(pnd_X));                                    //Natural: ASSIGN AP-FINANCIAL-3 := AP-FINANCIAL-3 + O-BALANCE ( #X )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaScia8251.getI_O_Parameters_O_Total_Fund_Balance().greater(getZero())))                                                                           //Natural: IF O-TOTAL-FUND-BALANCE GT 0
        {
            //*  MORE THAN 99 MILLION
            //*  ASSIGN MAX. FIELD ALLOWS
            if (condition(pdaScia8251.getI_O_Parameters_O_Total_Fund_Balance().greater(new DbsDecimal("99999999.99"))))                                                   //Natural: IF O-TOTAL-FUND-BALANCE GT 99999999.99
            {
                annty_Actvty_Prap_View_Ap_Financial_4.setValue(99999999);                                                                                                 //Natural: ASSIGN AP-FINANCIAL-4 := 99999999.99
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                annty_Actvty_Prap_View_Ap_Financial_4.setValue(pdaScia8251.getI_O_Parameters_O_Total_Fund_Balance());                                                     //Natural: ASSIGN AP-FINANCIAL-4 := O-TOTAL-FUND-BALANCE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Financial_2.greater(getZero()) || annty_Actvty_Prap_View_Ap_Financial_3.greater(getZero())))                              //Natural: IF AP-FINANCIAL-2 GT 0 OR AP-FINANCIAL-3 GT 0
        {
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),first_Dollar_Event_Rec_Fevt_Trade_Date);                                                                  //Natural: MOVE EDITED FEVT-TRADE-DATE TO #D ( EM = YYYYMMDD )
            pnd_Trade_Date.setValueEdited(pnd_D,new ReportEditMask("YYYYMMDD"));                                                                                          //Natural: MOVE EDITED #D ( EM = YYYYMMDD ) TO #TRADE-DATE
            annty_Actvty_Prap_View_Ap_Ann_Funding_Dt.setValue(pnd_Trade_Date_Pnd_Trade_Date_N);                                                                           //Natural: MOVE #TRADE-DATE-N TO AP-ANN-FUNDING-DT
            pnd_Ct_Update_Oneira.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CT-UPDATE-ONEIRA
                                                                                                                                                                          //Natural: PERFORM PRINT-PRAP-FILE
            sub_Print_Prap_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",annty_Actvty_Prap_View_Ap_Financial_2,"=",annty_Actvty_Prap_View_Ap_Financial_3,"=",annty_Actvty_Prap_View_Ap_Financial_4);             //Natural: WRITE '=' AP-FINANCIAL-2 '=' AP-FINANCIAL-3 '=' AP-FINANCIAL-4
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(53),Global.getDATU(),new ColumnSpacing(2),Global.getTIME(),NEWLINE,new                    //Natural: WRITE ( 1 ) *PROGRAM 53X *DATU 2X *TIME / 30X'PRAP FILE UPDATE REPORT' / 1T '-' ( 100 ) / 1X 'PIN' 11X 'SUB-PL' 2X 'ACT DTE' 4X 'CUSIP' 4X 'FEV CNTRCT' 2X 'TIA CNTRCT' 2X 'STS' 2X 'T-DOI' 6X 'C-DOI' 6X 'MATCHED-DT'
                        ColumnSpacing(30),"PRAP FILE UPDATE REPORT",NEWLINE,new TabSetting(1),"-",new RepeatItem(100),NEWLINE,new ColumnSpacing(1),"PIN",new 
                        ColumnSpacing(11),"SUB-PL",new ColumnSpacing(2),"ACT DTE",new ColumnSpacing(4),"CUSIP",new ColumnSpacing(4),"FEV CNTRCT",new ColumnSpacing(2),"TIA CNTRCT",new 
                        ColumnSpacing(2),"STS",new ColumnSpacing(2),"T-DOI",new ColumnSpacing(6),"C-DOI",new ColumnSpacing(6),"MATCHED-DT");
                    getReports().write(1, NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE ( 1 ) //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE // 'FRSTD CONTRACT:'25T FEVT-TIAA-CONTRACT / 'PIN:' 25T FEVT-PIN // 'FRSTD CUSIP   :'25T FEVT-CUSIP /
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"FRSTD CONTRACT:",new TabSetting(25),first_Dollar_Event_Rec_Fevt_Tiaa_Contract,NEWLINE,"PIN:",new 
            TabSetting(25),first_Dollar_Event_Rec_Fevt_Pin,NEWLINE,NEWLINE,"FRSTD CUSIP   :",new TabSetting(25),first_Dollar_Event_Rec_Fevt_Cusip,NEWLINE);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=130 PS=100");
        Global.format(1, "LS=130 PS=80");
    }
}
