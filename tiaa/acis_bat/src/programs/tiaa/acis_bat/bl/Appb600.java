/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:10:04 PM
**        * FROM NATURAL PROGRAM : Appb600
************************************************************
**        * FILE NAME            : Appb600.java
**        * CLASS NAME           : Appb600
**        * INSTANCE NAME        : Appb600
************************************************************
************************************************************************
* COMMENTS ADDED 04/21/2004 K.GATES                                    *
* PROGRAM  : APPB600 - REPRINT REPORTS                                 *
* FUNCTION : READS ACIS REPRINT FILE AND EXTRACTS ALL PARTICIPANT      *
*            CONTRACT RECORDS REQUESTS FOR REPRINT OR CONTRACT         *
*            CORRECTION.  CREATES FOUR REPORTS USING THE EXTRACTED     *
*            DATA.                                                     *
* UPDATED  : 04/21/04 K.GATES   - RESTOW FOR APPL180 FOR SUNGARD       *
*                                 RELEASE 2                            *
*          : 09/02/04 K.GATES   - RESTOW FOR APPL180 SUNGARD RELEASE 3 *
*          : 12/31/04 K.GATES   - RESTOW FOR APPL180 SUNGARD RELEASE 4 *
*          : 03/01/06 K.GATES   - RESTOW FOR APPL180 DIV/SUB (RL8)     *
*          : 04/11/06 K.GATES   - RESTOW FOR APPL180 SUNGARD STABLE RE *
*          : 07/20/06 K.GATES   - RESTOW FOR APPL180 ARR PROJECT       *
*          : 07/26/07 DEVELBISS - RESTOW FOR APPL180 STABLE RETURN SV  *
*          : 04/07/08 K.GATES   - RESTOW FOR APPL180 CALSTRS PROJECT   *
*          : 09/23/08 DEVELBISS - RESTOW FOR APPL180 AUTO ENROLL       *
*          : 08/12/09 C. AVE    - RESTOW FOR APPL180 ACIS PERF.        *
*          : 03/15/10 B.HOLLOWAY- RESTOW FOR APPL180 TIAA STABLE VALUE *
*          : 03/04/11 B. ELLO   - FLORIDA PROJECT - RESTOW FOR APPL180 *
*            05/04/2011  C. SCHNEIDER RESTOW FOR NEW VERSION OF APPL180*
*                        FOR JHU                                       *
*          : 07/01/11 L. SHU    - TRNFR IN CREDIT - RESTOW FOR APPL180 *
*            08/04/11 G. SUDSATAYA - RESTOW FOR CAMPUS - USE APPL180   *
*            03/23/12 K.GATES   - RE-STOW FOR APPL180 FOR ROCH2        *
*            11/17/12 L. SHU    - RE-STOW FOR APPL180 FOR LPAO         *
*            07/18/13 L. SHU    - RE-STOW FOR APPL180 FOR TNGSUB       *
*            09/13/13 L. SHU    - RE-STOW FOR APPL180 FOR MTSIN        *
*            08/1/14  B.NEWSOM  - INVESTMENT MENU BY SUBPLAN    (IMBS) *
*                                 RESTOW FOR APPL180 CHANGES           *
* (ACCRC)    09/15/14 B.NEWSOM  - ACIS/CIS CREF REDESIGN COMMUNICATIONS*
*                                 RESTOW FOR APPL180 CHANGES           *
*            09/01/15 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180     *
*                                 FOR BENE THE PLAN.             (BIP) *
*            03/17/17 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180     *
*                                 FOR ONEIRA                    ONEIRA *
*            06/16/17 BARUA     - PIN EXPANSION CHANGES. (CHG425939)   *
*                                 STOW ONLY                            *
*            01/20/18 ELLO      - RESTOW FOR APPL180 WITH NEW FIELDS   *
*                                 FOR ONEIRA:                          *
*                                 RP-FINANCIAL-2 = TIAA INIT. PREMIUM  *
*                                 RP-FINANCIAL-3 = CREF INIT. PREMIUM  *
*            11/16/19 L. SHU    - RE-STOW FOR APPL180 FOR CNTRSTRG IISG*
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb600 extends BLNatBase
{
    // Data Areas
    private LdaAppl180 ldaAppl180;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup tally_Litteral;
    private DbsField tally_Litteral_Tallylit1;
    private DbsField tally_Litteral_Tallylit3;
    private DbsField tally_Litteral_Tallylit2;
    private DbsField tally_Litteral_Tallylit4;
    private DbsField reportlit1;

    private DbsGroup grndtotalcnt;
    private DbsField grndtotalcnt_Alloctot;
    private DbsField grndtotalcnt_Bennytot;
    private DbsField grndtotalcnt_Cortot;
    private DbsField grndtotalcnt_Natot;
    private DbsField grndtotalcnt_Sdtot;
    private DbsField grndtotalcnt_Ppgvtot;
    private DbsField grndtotalcnt_Grandtot;

    private DbsGroup pagetotalcnt;
    private DbsField pagetotalcnt_Reprinttot;
    private DbsField pagetotalcnt_Reprintgrd;
    private DbsField pagetotalcnt_Correcttot;
    private DbsField pagetotalcnt_Correctgrd;
    private DbsField corr_Sub;
    private DbsField string_Name;

    private DbsGroup string_Name__R_Field_1;
    private DbsField string_Name_String_Name_Short;
    private DbsField line_One;
    private DbsField lit_Dest;
    private DbsField lit_Pull;
    private DbsField lit_Requ;
    private DbsField lit_Corru;
    private DbsField lit_Corrt;

    private DbsGroup lit_Corr_Vals;
    private DbsField lit_Corr_Vals_Filler1;
    private DbsField lit_Corr_Vals_Filler2;
    private DbsField lit_Corr_Vals_Filler3;
    private DbsField lit_Corr_Vals_Filler4;
    private DbsField lit_Corr_Vals_Filler5;
    private DbsField lit_Corr_Vals_Filler6;
    private DbsField lit_Corr_Vals_Filler7;
    private DbsField lit_Corr_Vals_Filler8;
    private DbsField lit_Corr_Vals_Filler9;
    private DbsField lit_Corr_Vals_Filler10;
    private DbsField lit_Corr_Vals_Filler11;
    private DbsField lit_Corr_Vals_Filler12;
    private DbsField lit_Corr_Vals_Filler13;
    private DbsField lit_Corr_Vals_Filler14;
    private DbsField lit_Corr_Vals_Filler15;
    private DbsField lit_Corr_Vals_Filler16;
    private DbsField lit_Corr_Vals_Filler17;
    private DbsField lit_Corr_Vals_Filler18;
    private DbsField lit_Corr_Vals_Filler19;
    private DbsField lit_Corr_Vals_Filler20;

    private DbsGroup lit_Corr_Vals__R_Field_2;
    private DbsField lit_Corr_Vals_Lit_Corr_Table;
    private DbsField cnt_Corr_Table;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Rp_Ppg_Team_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl180 = new LdaAppl180();
        registerRecord(ldaAppl180);
        registerRecord(ldaAppl180.getVw_acis_Reprint_Fl_View());

        // Local Variables
        localVariables = new DbsRecord();

        tally_Litteral = localVariables.newGroupInRecord("tally_Litteral", "TALLY-LITTERAL");
        tally_Litteral_Tallylit1 = tally_Litteral.newFieldInGroup("tally_Litteral_Tallylit1", "TALLYLIT1", FieldType.STRING, 25);
        tally_Litteral_Tallylit3 = tally_Litteral.newFieldInGroup("tally_Litteral_Tallylit3", "TALLYLIT3", FieldType.STRING, 25);
        tally_Litteral_Tallylit2 = tally_Litteral.newFieldInGroup("tally_Litteral_Tallylit2", "TALLYLIT2", FieldType.STRING, 35);
        tally_Litteral_Tallylit4 = tally_Litteral.newFieldInGroup("tally_Litteral_Tallylit4", "TALLYLIT4", FieldType.STRING, 35);
        reportlit1 = localVariables.newFieldInRecord("reportlit1", "REPORTLIT1", FieldType.STRING, 35);

        grndtotalcnt = localVariables.newGroupInRecord("grndtotalcnt", "GRNDTOTALCNT");
        grndtotalcnt_Alloctot = grndtotalcnt.newFieldInGroup("grndtotalcnt_Alloctot", "ALLOCTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Bennytot = grndtotalcnt.newFieldInGroup("grndtotalcnt_Bennytot", "BENNYTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Cortot = grndtotalcnt.newFieldInGroup("grndtotalcnt_Cortot", "CORTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Natot = grndtotalcnt.newFieldInGroup("grndtotalcnt_Natot", "NATOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Sdtot = grndtotalcnt.newFieldInGroup("grndtotalcnt_Sdtot", "SDTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Ppgvtot = grndtotalcnt.newFieldInGroup("grndtotalcnt_Ppgvtot", "PPGVTOT", FieldType.NUMERIC, 9);
        grndtotalcnt_Grandtot = grndtotalcnt.newFieldInGroup("grndtotalcnt_Grandtot", "GRANDTOT", FieldType.NUMERIC, 9);

        pagetotalcnt = localVariables.newGroupInRecord("pagetotalcnt", "PAGETOTALCNT");
        pagetotalcnt_Reprinttot = pagetotalcnt.newFieldInGroup("pagetotalcnt_Reprinttot", "REPRINTTOT", FieldType.NUMERIC, 9);
        pagetotalcnt_Reprintgrd = pagetotalcnt.newFieldInGroup("pagetotalcnt_Reprintgrd", "REPRINTGRD", FieldType.NUMERIC, 9);
        pagetotalcnt_Correcttot = pagetotalcnt.newFieldInGroup("pagetotalcnt_Correcttot", "CORRECTTOT", FieldType.NUMERIC, 9);
        pagetotalcnt_Correctgrd = pagetotalcnt.newFieldInGroup("pagetotalcnt_Correctgrd", "CORRECTGRD", FieldType.NUMERIC, 9);
        corr_Sub = localVariables.newFieldInRecord("corr_Sub", "CORR-SUB", FieldType.NUMERIC, 2);
        string_Name = localVariables.newFieldInRecord("string_Name", "STRING-NAME", FieldType.STRING, 95);

        string_Name__R_Field_1 = localVariables.newGroupInRecord("string_Name__R_Field_1", "REDEFINE", string_Name);
        string_Name_String_Name_Short = string_Name__R_Field_1.newFieldInGroup("string_Name_String_Name_Short", "STRING-NAME-SHORT", FieldType.STRING, 
            35);
        line_One = localVariables.newFieldInRecord("line_One", "LINE-ONE", FieldType.BOOLEAN, 1);
        lit_Dest = localVariables.newFieldInRecord("lit_Dest", "LIT-DEST", FieldType.STRING, 9);
        lit_Pull = localVariables.newFieldInRecord("lit_Pull", "LIT-PULL", FieldType.STRING, 10);
        lit_Requ = localVariables.newFieldInRecord("lit_Requ", "LIT-REQU", FieldType.STRING, 17);
        lit_Corru = localVariables.newFieldInRecord("lit_Corru", "LIT-CORRU", FieldType.STRING, 3);
        lit_Corrt = localVariables.newFieldInRecord("lit_Corrt", "LIT-CORRT", FieldType.STRING, 15);

        lit_Corr_Vals = localVariables.newGroupInRecord("lit_Corr_Vals", "LIT-CORR-VALS");
        lit_Corr_Vals_Filler1 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler1", "FILLER1", FieldType.STRING, 15);
        lit_Corr_Vals_Filler2 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler2", "FILLER2", FieldType.STRING, 15);
        lit_Corr_Vals_Filler3 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler3", "FILLER3", FieldType.STRING, 15);
        lit_Corr_Vals_Filler4 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler4", "FILLER4", FieldType.STRING, 15);
        lit_Corr_Vals_Filler5 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler5", "FILLER5", FieldType.STRING, 15);
        lit_Corr_Vals_Filler6 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler6", "FILLER6", FieldType.STRING, 15);
        lit_Corr_Vals_Filler7 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler7", "FILLER7", FieldType.STRING, 15);
        lit_Corr_Vals_Filler8 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler8", "FILLER8", FieldType.STRING, 15);
        lit_Corr_Vals_Filler9 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler9", "FILLER9", FieldType.STRING, 15);
        lit_Corr_Vals_Filler10 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler10", "FILLER10", FieldType.STRING, 15);
        lit_Corr_Vals_Filler11 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler11", "FILLER11", FieldType.STRING, 15);
        lit_Corr_Vals_Filler12 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler12", "FILLER12", FieldType.STRING, 15);
        lit_Corr_Vals_Filler13 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler13", "FILLER13", FieldType.STRING, 15);
        lit_Corr_Vals_Filler14 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler14", "FILLER14", FieldType.STRING, 15);
        lit_Corr_Vals_Filler15 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler15", "FILLER15", FieldType.STRING, 15);
        lit_Corr_Vals_Filler16 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler16", "FILLER16", FieldType.STRING, 15);
        lit_Corr_Vals_Filler17 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler17", "FILLER17", FieldType.STRING, 15);
        lit_Corr_Vals_Filler18 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler18", "FILLER18", FieldType.STRING, 15);
        lit_Corr_Vals_Filler19 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler19", "FILLER19", FieldType.STRING, 15);
        lit_Corr_Vals_Filler20 = lit_Corr_Vals.newFieldInGroup("lit_Corr_Vals_Filler20", "FILLER20", FieldType.STRING, 15);

        lit_Corr_Vals__R_Field_2 = localVariables.newGroupInRecord("lit_Corr_Vals__R_Field_2", "REDEFINE", lit_Corr_Vals);
        lit_Corr_Vals_Lit_Corr_Table = lit_Corr_Vals__R_Field_2.newFieldArrayInGroup("lit_Corr_Vals_Lit_Corr_Table", "LIT-CORR-TABLE", FieldType.STRING, 
            15, new DbsArrayController(1, 20));
        cnt_Corr_Table = localVariables.newFieldArrayInRecord("cnt_Corr_Table", "CNT-CORR-TABLE", FieldType.NUMERIC, 9, new DbsArrayController(1, 20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Rp_Ppg_Team_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Rp_Ppg_Team_Cde_OLD", "Rp_Ppg_Team_Cde_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaAppl180.initializeValues();

        localVariables.reset();
        tally_Litteral_Tallylit1.setInitialValue("TOTAL REPRINTS FOR TEAM ");
        tally_Litteral_Tallylit3.setInitialValue("GRAND TOTAL REPRINTS ");
        tally_Litteral_Tallylit2.setInitialValue("TOTAL CONTRACTS CORRECTED FOR TEAM ");
        tally_Litteral_Tallylit4.setInitialValue("GRAND TOTAL CONTRACTS CORRECTED ");
        reportlit1.setInitialValue("DAILY   REPORT");
        lit_Corr_Vals_Filler1.setInitialValue("PART NAME");
        lit_Corr_Vals_Filler2.setInitialValue("D O B");
        lit_Corr_Vals_Filler3.setInitialValue("SOC SEC NUM");
        lit_Corr_Vals_Filler4.setInitialValue("ALLOCATION");
        lit_Corr_Vals_Filler5.setInitialValue("BENEFICIARY");
        lit_Corr_Vals_Filler6.setInitialValue("PPG");
        lit_Corr_Vals_Filler7.setInitialValue("VESTING");
        lit_Corr_Vals_Filler8.setInitialValue("ADDRESS");
        lit_Corr_Vals_Filler9.setInitialValue("ANNTY START DT");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Appb600() throws Exception
    {
        super("Appb600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        //*  -------------------------
        //*  1  BL  RP-TEAM-REQUESTOR                A   15  N S
        //*         HD=TEAM/KEY
        //*         -------- SOURCE FIELD(S) -------
        //*         RP-PPG-TEAM-CDE(1-8)
        //*         RP-RACF-ID(1-7)
        //*  -------------------------
        ldaAppl180.getVw_acis_Reprint_Fl_View().startDatabaseFind                                                                                                         //Natural: FIND ACIS-REPRINT-FL-VIEW WITH RP-MAINTENANCE-IND = 'Y'
        (
        "FIND01",
        new Wc[] { new Wc("RP_MAINTENANCE_IND", "=", "Y", WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAppl180.getVw_acis_Reprint_Fl_View().readNextRow("FIND01")))
        {
            ldaAppl180.getVw_acis_Reprint_Fl_View().setIfNotFoundControlFlag(false);
            //*  --------------------------------------
            getWorkFiles().write(1, false, ldaAppl180.getAcis_Reprint_Fl_View_Rp_Record_Type(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Maintenance_Ind(),                  //Natural: WRITE WORK FILE 1 RP-RECORD-TYPE RP-MAINTENANCE-IND RP-MAINTENANCE-DATE RP-CORRECT-MAINT-IND RP-REPRINT-PULL-CODE RP-REPRINT-REQUEST-TYPE RP-PRINT-DESTINATION RP-TIAA-CONTR RP-COR-PRFX-NME RP-COR-LAST-NME RP-COR-FIRST-NME RP-COR-MDDLE-NME RP-COR-SFFX-NME RP-SOC-SEC RP-PIN-NBR RP-COLL-CODE RP-PPG-TEAM-CDE RP-T-DOI RP-CORRECTION-IND ( 1:20 ) RP-CORRECTION-SYNC ( 1:20 ) RP-UPDATE-DATE RP-UPDATE-TIME RP-RACF-ID RP-REQUEST-PKG-TYPE
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Maintenance_Date(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correct_Maint_Ind(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Pull_Code(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Print_Destination(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Contr(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Prfx_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_First_Nme(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Soc_Sec(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Pin_Nbr(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_Code(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(1,":",20), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(1,":",20), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Update_Date(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Update_Time(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Racf_Id(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type());
            getSort().writeSortInData(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Racf_Id(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Record_Type(),  //Natural: END-ALL
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Maintenance_Ind(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Maintenance_Date(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correct_Maint_Ind(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Pull_Code(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Print_Destination(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Contr(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Prfx_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_First_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Soc_Sec(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Pin_Nbr(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_Code(), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(1), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(2), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(3), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(4), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(5), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(6), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(7), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(8), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(9), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(10), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(11), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(12), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(13), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(14), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(15), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(16), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(17), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(18), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(19), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(20), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(1), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(2), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(3), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(4), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(5), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(6), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(7), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(8), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(9), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(10), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(11), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(12), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(13), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(14), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(15), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(16), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(17), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(18), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(19), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(20), 
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Update_Date(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Update_Time(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type());
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  --------------------------------------
        getSort().sortData(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Racf_Id());                                        //Natural: SORT BY RP-PPG-TEAM-CDE RP-RACF-ID USING RP-RECORD-TYPE RP-MAINTENANCE-IND RP-MAINTENANCE-DATE RP-CORRECT-MAINT-IND RP-REPRINT-PULL-CODE RP-REPRINT-REQUEST-TYPE RP-PRINT-DESTINATION RP-TIAA-CONTR RP-COR-PRFX-NME RP-COR-LAST-NME RP-COR-FIRST-NME RP-COR-MDDLE-NME RP-COR-SFFX-NME RP-SOC-SEC RP-PIN-NBR RP-COLL-CODE RP-T-DOI RP-CORRECTION-IND ( 1:20 ) RP-CORRECTION-SYNC ( 1:20 ) RP-UPDATE-DATE RP-UPDATE-TIME RP-REQUEST-PKG-TYPE
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Racf_Id(), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Record_Type(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Maintenance_Ind(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Maintenance_Date(), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correct_Maint_Ind(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Pull_Code(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type(), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Print_Destination(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Contr(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Prfx_Nme(), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_First_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme(), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Soc_Sec(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Pin_Nbr(), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_Code(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(1), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(2), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(3), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(4), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(5), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(6), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(7), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(8), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(9), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(10), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(11), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(12), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(13), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(14), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(15), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(16), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(17), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(18), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(19), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(20), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(1), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(2), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(3), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(4), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(5), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(6), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(7), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(8), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(9), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(10), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(11), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(12), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(13), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(14), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(15), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(16), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(17), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(18), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(19), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Sync().getValue(20), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Update_Date(), ldaAppl180.getAcis_Reprint_Fl_View_Rp_Update_Time(), 
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pagetotalcnt_Reprinttot.nadd(1);                                                                                                                              //Natural: ASSIGN REPRINTTOT := REPRINTTOT + 1
            pagetotalcnt_Reprintgrd.nadd(1);                                                                                                                              //Natural: ASSIGN REPRINTGRD := REPRINTGRD + 1
            //*  --------------------------------------
            //*  --------------------------------------
            //*  --------------------------------------
            short decideConditionsMet591 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUES OF RP-REPRINT-REQUEST-TYPE;//Natural: VALUES 'A'
            if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type().equals("A"))))
            {
                decideConditionsMet591++;
                lit_Requ.setValue("    REPRINT/CORR");                                                                                                                    //Natural: ASSIGN LIT-REQU := '    REPRINT/CORR'
            }                                                                                                                                                             //Natural: VALUES 'B'
            else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type().equals("B"))))
            {
                decideConditionsMet591++;
                lit_Requ.setValue("    REPRINT/CORR");                                                                                                                    //Natural: ASSIGN LIT-REQU := '    REPRINT/CORR'
            }                                                                                                                                                             //Natural: VALUES 'R'
            else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type().equals("R"))))
            {
                decideConditionsMet591++;
                lit_Requ.setValue("    REPRINT ONLY");                                                                                                                    //Natural: ASSIGN LIT-REQU := '    REPRINT ONLY'
            }                                                                                                                                                             //Natural: VALUES 'C'
            else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Request_Type().equals("C"))))
            {
                decideConditionsMet591++;
                lit_Requ.setValue("    CORR ONLY   ");                                                                                                                    //Natural: ASSIGN LIT-REQU := '    CORR ONLY   '
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                lit_Requ.reset();                                                                                                                                         //Natural: RESET LIT-REQU
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  --------------------------------------
            short decideConditionsMet604 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUES OF RP-REPRINT-PULL-CODE;//Natural: VALUES 'Y'
            if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Pull_Code().equals("Y"))))
            {
                decideConditionsMet604++;
                short decideConditionsMet606 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUES OF RP-REQUEST-PKG-TYPE;//Natural: VALUE 'W'
                if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("W"))))
                {
                    decideConditionsMet606++;
                    lit_Pull.setValue("PL-WELC");                                                                                                                         //Natural: ASSIGN LIT-PULL := 'PL-WELC'
                }                                                                                                                                                         //Natural: VALUE 'L'
                else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("L"))))
                {
                    decideConditionsMet606++;
                    lit_Pull.setValue("PL-LEGL");                                                                                                                         //Natural: ASSIGN LIT-PULL := 'PL-LEGL'
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("B"))))
                {
                    decideConditionsMet606++;
                    lit_Pull.setValue("PL-WEL/LEG");                                                                                                                      //Natural: ASSIGN LIT-PULL := 'PL-WEL/LEG'
                }                                                                                                                                                         //Natural: VALUE 'P'
                else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("P"))))
                {
                    decideConditionsMet606++;
                    lit_Pull.setValue("NO PKG");                                                                                                                          //Natural: ASSIGN LIT-PULL := 'NO PKG'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUES 'N'
            else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Reprint_Pull_Code().equals("N"))))
            {
                decideConditionsMet604++;
                short decideConditionsMet619 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUES OF RP-REQUEST-PKG-TYPE;//Natural: VALUE 'W'
                if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("W"))))
                {
                    decideConditionsMet619++;
                    lit_Pull.setValue("ML-WELC");                                                                                                                         //Natural: ASSIGN LIT-PULL := 'ML-WELC'
                }                                                                                                                                                         //Natural: VALUE 'L'
                else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("L"))))
                {
                    decideConditionsMet619++;
                    lit_Pull.setValue("ML-LEGL");                                                                                                                         //Natural: ASSIGN LIT-PULL := 'ML-LEGL'
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("B"))))
                {
                    decideConditionsMet619++;
                    lit_Pull.setValue("ML-WEL/LEG");                                                                                                                      //Natural: ASSIGN LIT-PULL := 'ML-WEL/LEG'
                }                                                                                                                                                         //Natural: VALUE 'P'
                else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("P"))))
                {
                    decideConditionsMet619++;
                    lit_Pull.setValue("NO PKG");                                                                                                                          //Natural: ASSIGN LIT-PULL := 'NO PKG'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                lit_Pull.reset();                                                                                                                                         //Natural: RESET LIT-PULL
                if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Request_Pkg_Type().equals("P")))                                                                      //Natural: IF RP-REQUEST-PKG-TYPE EQ 'P'
                {
                    lit_Pull.setValue("NO PKG");                                                                                                                          //Natural: ASSIGN LIT-PULL := 'NO PKG'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  --------------------------------------
            short decideConditionsMet638 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUES OF RP-PRINT-DESTINATION;//Natural: VALUES 'D'
            if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Print_Destination().equals("D"))))
            {
                decideConditionsMet638++;
                lit_Dest.setValue("DENVER");                                                                                                                              //Natural: ASSIGN LIT-DEST := 'DENVER'
            }                                                                                                                                                             //Natural: VALUES 'N'
            else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Print_Destination().equals("N"))))
            {
                decideConditionsMet638++;
                lit_Dest.setValue("NEW YORK");                                                                                                                            //Natural: ASSIGN LIT-DEST := 'NEW YORK'
            }                                                                                                                                                             //Natural: VALUES 'C'
            else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Print_Destination().equals("C"))))
            {
                decideConditionsMet638++;
                lit_Dest.setValue("CHARLOTTE");                                                                                                                           //Natural: ASSIGN LIT-DEST := 'CHARLOTTE'
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                lit_Dest.reset();                                                                                                                                         //Natural: RESET LIT-DEST
                lit_Dest.setValue("NO PKG");                                                                                                                              //Natural: ASSIGN LIT-DEST := 'NO PKG'
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  --------------------------------------
            if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme().notEquals(" ") && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_First_Nme().notEquals(" ")))   //Natural: IF RP-COR-LAST-NME NOT = ' ' AND RP-COR-FIRST-NME NOT = ' '
            {
                string_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme(), ",", ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_First_Nme())); //Natural: COMPRESS RP-COR-LAST-NME ',' RP-COR-FIRST-NME INTO STRING-NAME LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme().notEquals(" ")))                                                                          //Natural: IF RP-COR-MDDLE-NME NOT = ' '
            {
                string_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, string_Name, ",", ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme()));           //Natural: COMPRESS STRING-NAME ',' RP-COR-MDDLE-NME INTO STRING-NAME LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme().notEquals(" ")))                                                                           //Natural: IF RP-COR-SFFX-NME NOT = ' '
            {
                string_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, string_Name, ",", ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme()));            //Natural: COMPRESS STRING-NAME ',' RP-COR-SFFX-NME INTO STRING-NAME LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------------------------------
            if (condition(pagetotalcnt_Reprinttot.equals(1)))                                                                                                             //Natural: IF REPRINTTOT = 1
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Appm602.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING MAP 'APPM602 '
            //*  --------------------------------------
            if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue("*").equals("C") || ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue("*").equals("B"))) //Natural: IF RP-CORRECTION-IND ( * ) = 'C' OR RP-CORRECTION-IND ( * ) = 'B'
            {
                pagetotalcnt_Correcttot.nadd(1);                                                                                                                          //Natural: ASSIGN CORRECTTOT := CORRECTTOT + 1
                pagetotalcnt_Correctgrd.nadd(1);                                                                                                                          //Natural: ASSIGN CORRECTGRD := CORRECTGRD + 1
                if (condition(pagetotalcnt_Correcttot.equals(1)))                                                                                                         //Natural: IF CORRECTTOT = 1
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                line_One.setValue(true);                                                                                                                                  //Natural: ASSIGN LINE-ONE := TRUE
                FOR01:                                                                                                                                                    //Natural: FOR CORR-SUB = 1 20
                for (corr_Sub.setValue(1); condition(corr_Sub.lessOrEqual(20)); corr_Sub.nadd(1))
                {
                    short decideConditionsMet673 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUES OF RP-CORRECTION-IND ( CORR-SUB );//Natural: VALUES 'C'
                    if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(corr_Sub).equals("C"))))
                    {
                        decideConditionsMet673++;
                        lit_Corru.setValue("NO");                                                                                                                         //Natural: ASSIGN LIT-CORRU := 'NO'
                        lit_Corrt.setValue(lit_Corr_Vals_Lit_Corr_Table.getValue(corr_Sub));                                                                              //Natural: ASSIGN LIT-CORRT := LIT-CORR-TABLE ( CORR-SUB )
                    }                                                                                                                                                     //Natural: VALUES 'B'
                    else if (condition((ldaAppl180.getAcis_Reprint_Fl_View_Rp_Correction_Ind().getValue(corr_Sub).equals("B"))))
                    {
                        decideConditionsMet673++;
                        lit_Corru.setValue("YES");                                                                                                                        //Natural: ASSIGN LIT-CORRU := 'YES'
                        lit_Corrt.setValue(lit_Corr_Vals_Lit_Corr_Table.getValue(corr_Sub));                                                                              //Natural: ASSIGN LIT-CORRT := LIT-CORR-TABLE ( CORR-SUB )
                        cnt_Corr_Table.getValue(corr_Sub).nadd(1);                                                                                                        //Natural: ADD 1 TO CNT-CORR-TABLE ( CORR-SUB )
                    }                                                                                                                                                     //Natural: NONE VALUES
                    else if (condition())
                    {
                        lit_Corru.reset();                                                                                                                                //Natural: RESET LIT-CORRU LIT-CORRT
                        lit_Corrt.reset();
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(line_One.equals(true)))                                                                                                                 //Natural: IF LINE-ONE = TRUE
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Appm603.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING MAP 'APPM603 '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Appm604.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING MAP 'APPM604 '
                    }                                                                                                                                                     //Natural: END-IF
                    line_One.setValue(false);                                                                                                                             //Natural: ASSIGN LINE-ONE := FALSE
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------------------------------
            grndtotalcnt_Cortot.compute(new ComputeParameters(false, grndtotalcnt_Cortot), cnt_Corr_Table.getValue(1).add(cnt_Corr_Table.getValue(2)).add(cnt_Corr_Table.getValue(3))); //Natural: ASSIGN CORTOT := CNT-CORR-TABLE ( 1 ) + CNT-CORR-TABLE ( 2 ) + CNT-CORR-TABLE ( 3 )
            grndtotalcnt_Alloctot.setValue(cnt_Corr_Table.getValue(4));                                                                                                   //Natural: ASSIGN ALLOCTOT := CNT-CORR-TABLE ( 4 )
            grndtotalcnt_Bennytot.setValue(cnt_Corr_Table.getValue(5));                                                                                                   //Natural: ASSIGN BENNYTOT := CNT-CORR-TABLE ( 5 )
            grndtotalcnt_Ppgvtot.compute(new ComputeParameters(false, grndtotalcnt_Ppgvtot), cnt_Corr_Table.getValue(6).add(cnt_Corr_Table.getValue(7)));                 //Natural: ASSIGN PPGVTOT := CNT-CORR-TABLE ( 6 ) + CNT-CORR-TABLE ( 7 )
            grndtotalcnt_Natot.setValue(cnt_Corr_Table.getValue(8));                                                                                                      //Natural: ASSIGN NATOT := CNT-CORR-TABLE ( 8 )
            grndtotalcnt_Sdtot.setValue(cnt_Corr_Table.getValue(9));                                                                                                      //Natural: ASSIGN SDTOT := CNT-CORR-TABLE ( 9 )
            grndtotalcnt_Grandtot.compute(new ComputeParameters(false, grndtotalcnt_Grandtot), cnt_Corr_Table.getValue(1).add(cnt_Corr_Table.getValue(2)).add(cnt_Corr_Table.getValue(3)).add(cnt_Corr_Table.getValue(4)).add(cnt_Corr_Table.getValue(5)).add(cnt_Corr_Table.getValue(6)).add(cnt_Corr_Table.getValue(7)).add(cnt_Corr_Table.getValue(8)).add(cnt_Corr_Table.getValue(9))); //Natural: ASSIGN GRANDTOT := CNT-CORR-TABLE ( 1 ) + CNT-CORR-TABLE ( 2 ) + CNT-CORR-TABLE ( 3 ) + CNT-CORR-TABLE ( 4 ) + CNT-CORR-TABLE ( 5 ) + CNT-CORR-TABLE ( 6 ) + CNT-CORR-TABLE ( 7 ) + CNT-CORR-TABLE ( 8 ) + CNT-CORR-TABLE ( 9 )
            //*  --------------------------------------
            //*  --------                                                                                                                                                 //Natural: AT BREAK OF RP-PPG-TEAM-CDE
            //*  --------                                                                                                                                                 //Natural: AT TOP OF PAGE ( 1 )
            //*  --------                                                                                                                                                 //Natural: AT TOP OF PAGE ( 2 )
            //*  --------------------------------------                                                                                                                   //Natural: AT END OF DATA
            sort01Rp_Ppg_Team_CdeOld.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde());                                                                      //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            getReports().write(3, ReportOption.NOTITLE, writeMapToStringOutput(Appm605.class));                                                                           //Natural: WRITE ( 3 ) NOTITLE USING MAP 'APPM605'
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,tally_Litteral_Tallylit3," : ",pagetotalcnt_Reprintgrd);                                                   //Natural: WRITE ( 1 ) / TALLYLIT3 ' : ' REPRINTGRD
            if (condition(Global.isEscape())) return;
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,tally_Litteral_Tallylit4," : ",pagetotalcnt_Correctgrd);                                                   //Natural: WRITE ( 2 ) / TALLYLIT4 ' : ' CORRECTGRD
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Appm606.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING MAP 'APPM606'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Appm601.class));                                                                   //Natural: WRITE ( 2 ) NOTITLE USING MAP 'APPM601'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaAppl180_getAcis_Reprint_Fl_View_Rp_Ppg_Team_CdeIsBreak = ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde().isBreak(endOfData);
        if (condition(ldaAppl180_getAcis_Reprint_Fl_View_Rp_Ppg_Team_CdeIsBreak))
        {
            if (condition(pagetotalcnt_Reprinttot.greater(getZero())))                                                                                                    //Natural: IF REPRINTTOT > 0
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,tally_Litteral_Tallylit1,sort01Rp_Ppg_Team_CdeOld," : ",pagetotalcnt_Reprinttot);                      //Natural: WRITE ( 1 ) / TALLYLIT1 OLD ( RP-PPG-TEAM-CDE ) ' : ' REPRINTTOT
                if (condition(Global.isEscape())) return;
                pagetotalcnt_Reprinttot.reset();                                                                                                                          //Natural: RESET REPRINTTOT
            }                                                                                                                                                             //Natural: END-IF
            //*  --------
            if (condition(pagetotalcnt_Correcttot.greater(getZero())))                                                                                                    //Natural: IF CORRECTTOT > 0
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,tally_Litteral_Tallylit2,sort01Rp_Ppg_Team_CdeOld," : ",pagetotalcnt_Correcttot);                      //Natural: WRITE ( 2 ) / TALLYLIT2 OLD ( RP-PPG-TEAM-CDE ) ' : ' CORRECTTOT
                if (condition(Global.isEscape())) return;
                pagetotalcnt_Correcttot.reset();                                                                                                                          //Natural: RESET CORRECTTOT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
}
