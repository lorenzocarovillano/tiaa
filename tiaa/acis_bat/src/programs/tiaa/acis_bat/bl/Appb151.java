/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:14 PM
**        * FROM NATURAL PROGRAM : Appb151
************************************************************
**        * FILE NAME            : Appb151.java
**        * CLASS NAME           : Appb151
**        * INSTANCE NAME        : Appb151
************************************************************
************************************************************************
* PROGRAM  : APPB151 - DEFAULT ENROLLMENT PROCESSING                   *
* FUNCTION : THIS PROGRAM PERFORMS THE FOLLOWING:                      *
*            1. READS ACIS PRAP FILE AND EXTRACTS THE CONTRACTS THAT   *
*               HAVE AN AP-RELEASE-IND OF '7' AND OMNI-ISSUE-IND = '2'.*
*               THESE RECORDS ARE CONSIDERED DEFAULT ENROLLED INTO AN  *
*               INDIVIDUAL PROD DUE TO MISSING SIGNATURE/E-SIGNATURE.  *
*            2. RECORDS ARE WRITTEN TO A WORKFILE WHICH IS THEN SENT   *
*               TO MDM TO QUERY THE CURRENT DEFAULT ENROLLMENT IND.    *
*            2. THE PROGRAM IS EXECUTED IN JOB PNI2104D AND THE NDM    *
*               JOB IS PNI2104X WHICH SENDS THE FILE TO MDM.           *
*                                                                      *
* UPDATED  : 09/10/15 L.SHU     - PROGRAM CREATED                      *
* 06/13/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb151 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup annty_Actvty_Prap_View__R_Field_1;
    private DbsField annty_Actvty_Prap_View_Bank_Home_Ac;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A3;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A4;
    private DbsField annty_Actvty_Prap_View_Bank_Filler_4;
    private DbsField annty_Actvty_Prap_View_Oia_Indicator;
    private DbsField annty_Actvty_Prap_View_Erisa_Ind;
    private DbsField annty_Actvty_Prap_View_Cai_Ind;
    private DbsField annty_Actvty_Prap_View_Cip_Ind;
    private DbsField annty_Actvty_Prap_View_Same_Addr;
    private DbsField annty_Actvty_Prap_View_Omni_Issue_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Conv_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Deceased_Ind;

    private DbsGroup pnd_Output_File;
    private DbsField pnd_Output_File_Pnd_Ap_Tiaa_Cntrct;
    private DbsField pnd_Output_File_Pnd_Ap_Cref_Cert;
    private DbsField pnd_Output_File_Pnd_Ap_Isn;
    private DbsField pnd_Output_File_Pnd_Ap_Release_Ind;
    private DbsField pnd_Output_File_Pnd_Cntrct_Found_Ind;
    private DbsField pnd_Output_File_Pnd_Default_Enrollment_Ind;

    private DbsGroup pnd_Miscellanous_Variables;
    private DbsField pnd_Miscellanous_Variables_Pnd_Read_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Write_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Exception_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Process_Ind;
    private DbsField pnd_Miscellanous_Variables_Pnd_Hold_Date;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_2;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time;
    private DbsField pnd_Miscellanous_Variables_Pnd_Except_Reason;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        annty_Actvty_Prap_View__R_Field_1 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_1", "REDEFINE", annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        annty_Actvty_Prap_View_Bank_Home_Ac = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_Ac", "BANK-HOME-AC", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A3 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A3", "BANK-HOME-A3", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A4 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A4", "BANK-HOME-A4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Bank_Filler_4 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Filler_4", "BANK-FILLER-4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Oia_Indicator = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Oia_Indicator", "OIA-INDICATOR", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Erisa_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Erisa_Ind", "ERISA-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cai_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Cai_Ind", "CAI-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cip_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Cip_Ind", "CIP-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Same_Addr = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Same_Addr", "SAME-ADDR", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Omni_Issue_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Omni_Issue_Ind", "OMNI-ISSUE-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", 
            "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Tiaa_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_View_Ap_Cref_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Doi", "AP-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");
        annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind", 
            "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        annty_Actvty_Prap_View_Ap_Conv_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Conv_Issue_State", 
            "AP-CONV-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CONV_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Deceased_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Deceased_Ind", "AP-DECEASED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DECEASED_IND");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Output_File = localVariables.newGroupInRecord("pnd_Output_File", "#OUTPUT-FILE");
        pnd_Output_File_Pnd_Ap_Tiaa_Cntrct = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_Ap_Tiaa_Cntrct", "#AP-TIAA-CNTRCT", FieldType.STRING, 
            10);
        pnd_Output_File_Pnd_Ap_Cref_Cert = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_Ap_Cref_Cert", "#AP-CREF-CERT", FieldType.STRING, 10);
        pnd_Output_File_Pnd_Ap_Isn = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_Ap_Isn", "#AP-ISN", FieldType.NUMERIC, 10);
        pnd_Output_File_Pnd_Ap_Release_Ind = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_Ap_Release_Ind", "#AP-RELEASE-IND", FieldType.STRING, 
            1);
        pnd_Output_File_Pnd_Cntrct_Found_Ind = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_Cntrct_Found_Ind", "#CNTRCT-FOUND-IND", FieldType.STRING, 
            1);
        pnd_Output_File_Pnd_Default_Enrollment_Ind = pnd_Output_File.newFieldInGroup("pnd_Output_File_Pnd_Default_Enrollment_Ind", "#DEFAULT-ENROLLMENT-IND", 
            FieldType.STRING, 1);

        pnd_Miscellanous_Variables = localVariables.newGroupInRecord("pnd_Miscellanous_Variables", "#MISCELLANOUS-VARIABLES");
        pnd_Miscellanous_Variables_Pnd_Read_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Read_Count", "#READ-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Write_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Write_Count", "#WRITE-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Exception_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Exception_Count", 
            "#EXCEPTION-COUNT", FieldType.NUMERIC, 6);
        pnd_Miscellanous_Variables_Pnd_Process_Ind = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Process_Ind", "#PROCESS-IND", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_Hold_Date = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Hold_Date", "#HOLD-DATE", 
            FieldType.DATE);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time", 
            "#RQST-LOG-DTE-TIME", FieldType.STRING, 15);

        pnd_Miscellanous_Variables__R_Field_2 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_2", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date = pnd_Miscellanous_Variables__R_Field_2.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date", 
            "#RQST-LOG-DATE", FieldType.STRING, 8);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time = pnd_Miscellanous_Variables__R_Field_2.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time", 
            "#RQST-LOG-TIME", FieldType.STRING, 7);
        pnd_Miscellanous_Variables_Pnd_Except_Reason = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Except_Reason", "#EXCEPT-REASON", 
            FieldType.STRING, 35);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb151() throws Exception
    {
        super("Appb151");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_annty_Actvty_Prap_View.startDatabaseRead                                                                                                                       //Natural: READ ANNTY-ACTVTY-PRAP-VIEW BY AP-RELEASE-IND STARTING FROM 7
        (
        "READ",
        new Wc[] { new Wc("AP_RELEASE_IND", ">=", 7, WcType.BY) },
        new Oc[] { new Oc("AP_RELEASE_IND", "ASC") }
        );
        READ:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("READ")))
        {
            if (condition(annty_Actvty_Prap_View_Ap_Release_Ind.greater(7)))                                                                                              //Natural: IF AP-RELEASE-IND GT 7
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Miscellanous_Variables_Pnd_Read_Count.nadd(1);                                                                                                            //Natural: ADD 1 TO #READ-COUNT
            if (condition(annty_Actvty_Prap_View_Omni_Issue_Ind.notEquals("2")))                                                                                          //Natural: IF OMNI-ISSUE-IND NE '2'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Record_Type.notEquals(1)))                                                                                            //Natural: IF AP-RECORD-TYPE NE 1
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Miscellanous_Variables_Pnd_Write_Count.nadd(1);                                                                                                           //Natural: ADD 1 TO #WRITE-COUNT
            pnd_Output_File.reset();                                                                                                                                      //Natural: RESET #OUTPUT-FILE
            pnd_Output_File_Pnd_Ap_Tiaa_Cntrct.setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                                           //Natural: ASSIGN #AP-TIAA-CNTRCT := AP-TIAA-CNTRCT
            pnd_Output_File_Pnd_Ap_Cref_Cert.setValue(annty_Actvty_Prap_View_Ap_Cref_Cert);                                                                               //Natural: ASSIGN #AP-CREF-CERT := AP-CREF-CERT
            pnd_Output_File_Pnd_Ap_Isn.setValue(vw_annty_Actvty_Prap_View.getAstISN("READ"));                                                                             //Natural: ASSIGN #AP-ISN := *ISN
            pnd_Output_File_Pnd_Ap_Release_Ind.setValue(annty_Actvty_Prap_View_Ap_Release_Ind);                                                                           //Natural: ASSIGN #AP-RELEASE-IND := AP-RELEASE-IND
            getWorkFiles().write(1, false, pnd_Output_File);                                                                                                              //Natural: WRITE WORK FILE 1 #OUTPUT-FILE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT
        sub_Write_Control_Report();
        if (condition(Global.isEscape())) {return;}
        //* *---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REPORT
    }
    private void sub_Write_Control_Report() throws Exception                                                                                                              //Natural: WRITE-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        //*  TITLE LEFT JUSTIFIED
        getReports().write(0, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. DEFAULT ENROLL EXTRACT TOTALS    ",new ColumnSpacing(16),Global.getDATU());              //Natural: WRITE *PROGRAM 5X 'A.C.I.S. DEFAULT ENROLL EXTRACT TOTALS    ' 16X *DATU
        if (Global.isEscape()) return;
        getReports().write(0, Global.getTIMX(),new ColumnSpacing(58),NEWLINE,NEWLINE,NEWLINE);                                                                            //Natural: WRITE *TIMX 58X ///
        if (Global.isEscape()) return;
        getReports().write(0, new ColumnSpacing(14),"TOTAL PRAP RECORDS READ               ",pnd_Miscellanous_Variables_Pnd_Read_Count,NEWLINE);                          //Natural: WRITE 14X 'TOTAL PRAP RECORDS READ               ' #READ-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, new ColumnSpacing(14),"TOTAL PRAP DEFAULT ENROLL RECORDS READ",pnd_Miscellanous_Variables_Pnd_Write_Count,NEWLINE);                         //Natural: WRITE 14X 'TOTAL PRAP DEFAULT ENROLL RECORDS READ' #WRITE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, new ColumnSpacing(16),"**    END    OF   REPORT   **");                                                                                     //Natural: WRITE 16X '**    END    OF   REPORT   **'
        if (Global.isEscape()) return;
    }

    //
}
