/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:54 PM
**        * FROM NATURAL PROGRAM : Appb2081
************************************************************
**        * FILE NAME            : Appb2081.java
**        * CLASS NAME           : Appb2081
**        * INSTANCE NAME        : Appb2081
************************************************************
***********************************************************************
* APPB2081 - UPDATE G-KEYS TO MAKE UNIQUE                             *
***********************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                    *
**--------------------------------------------------------------------*
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.**
***********************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2081 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_File;
    private DbsField pnd_Input_File_Pnd_Isn;
    private DbsField pnd_Input_File_Pnd_Pin_Ppg;

    private DbsGroup pnd_Input_File__R_Field_1;
    private DbsField pnd_Input_File_Pnd_Pin;
    private DbsField pnd_Input_File_Pnd_Ppg_Code;
    private DbsField pnd_Input_File_Pnd_Date;
    private DbsField pnd_Input_File_Pnd_Time;
    private DbsField pnd_Input_File_Pnd_Filler;

    private DataAccessProgramView vw_updt_View;
    private DbsField updt_View_Ap_Pin_Nbr;
    private DbsField updt_View_Ap_Coll_Code;
    private DbsField updt_View_Ap_G_Key;
    private DbsField updt_View_Ap_G_Ind;
    private DbsField updt_View_Ap_Dt_Ent_Sys;
    private DbsField updt_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField pnd_Isn_Entry;
    private DbsField pnd_Isn_Max;
    private DbsField pnd_Isn_Cnt;
    private DbsField pnd_Sub;
    private DbsField pnd_Key_Value;
    private DbsField pnd_Ind_Value;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Update_Count;
    private DbsField pnd_Record_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input_File = localVariables.newGroupInRecord("pnd_Input_File", "#INPUT-FILE");
        pnd_Input_File_Pnd_Isn = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Isn", "#ISN", FieldType.NUMERIC, 9);
        pnd_Input_File_Pnd_Pin_Ppg = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Pin_Ppg", "#PIN-PPG", FieldType.STRING, 16);

        pnd_Input_File__R_Field_1 = pnd_Input_File.newGroupInGroup("pnd_Input_File__R_Field_1", "REDEFINE", pnd_Input_File_Pnd_Pin_Ppg);
        pnd_Input_File_Pnd_Pin = pnd_Input_File__R_Field_1.newFieldInGroup("pnd_Input_File_Pnd_Pin", "#PIN", FieldType.STRING, 12);
        pnd_Input_File_Pnd_Ppg_Code = pnd_Input_File__R_Field_1.newFieldInGroup("pnd_Input_File_Pnd_Ppg_Code", "#PPG-CODE", FieldType.STRING, 4);
        pnd_Input_File_Pnd_Date = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Date", "#DATE", FieldType.STRING, 8);
        pnd_Input_File_Pnd_Time = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Time", "#TIME", FieldType.STRING, 8);
        pnd_Input_File_Pnd_Filler = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Filler", "#FILLER", FieldType.STRING, 2);

        vw_updt_View = new DataAccessProgramView(new NameInfo("vw_updt_View", "UPDT-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        updt_View_Ap_Pin_Nbr = vw_updt_View.getRecord().newFieldInGroup("updt_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AP_PIN_NBR");
        updt_View_Ap_Coll_Code = vw_updt_View.getRecord().newFieldInGroup("updt_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        updt_View_Ap_G_Key = vw_updt_View.getRecord().newFieldInGroup("updt_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_G_KEY");
        updt_View_Ap_G_Ind = vw_updt_View.getRecord().newFieldInGroup("updt_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_G_IND");
        updt_View_Ap_Dt_Ent_Sys = vw_updt_View.getRecord().newFieldInGroup("updt_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_ENT_SYS");
        updt_View_Ap_Rcrd_Updt_Tm_Stamp = vw_updt_View.getRecord().newFieldInGroup("updt_View_Ap_Rcrd_Updt_Tm_Stamp", "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        registerRecord(vw_updt_View);

        pnd_Isn_Entry = localVariables.newFieldArrayInRecord("pnd_Isn_Entry", "#ISN-ENTRY", FieldType.NUMERIC, 9, new DbsArrayController(1, 50));
        pnd_Isn_Max = localVariables.newFieldInRecord("pnd_Isn_Max", "#ISN-MAX", FieldType.NUMERIC, 2);
        pnd_Isn_Cnt = localVariables.newFieldInRecord("pnd_Isn_Cnt", "#ISN-CNT", FieldType.NUMERIC, 3);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 3);
        pnd_Key_Value = localVariables.newFieldInRecord("pnd_Key_Value", "#KEY-VALUE", FieldType.NUMERIC, 2);
        pnd_Ind_Value = localVariables.newFieldInRecord("pnd_Ind_Value", "#IND-VALUE", FieldType.NUMERIC, 2);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 2);
        pnd_Update_Count = localVariables.newFieldInRecord("pnd_Update_Count", "#UPDATE-COUNT", FieldType.NUMERIC, 9);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_updt_View.reset();

        localVariables.reset();
        pnd_Isn_Max.setInitialValue(50);
        pnd_Key_Value.setInitialValue(1);
        pnd_Ind_Value.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2081() throws Exception
    {
        super("Appb2081");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 1 ) LS = 80 PS = 65;//Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 10X 'PRAP G-KEY UPDATE REPORT' /
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 #INPUT-FILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_File)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Record_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORD-COUNT
            //*                                                                                                                                                           //Natural: AT BREAK OF #PIN-PPG
            pnd_Sub.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #SUB
            if (condition(pnd_Sub.greater(pnd_Isn_Max)))                                                                                                                  //Natural: IF #SUB > #ISN-MAX
            {
                getReports().write(1, NEWLINE,"ISN TABLE EXCEEDED MAX SIZE OF",pnd_Isn_Max,NEWLINE,"INCREASE ISN TABLE SIZE",NEWLINE,"JOB ABENDING WITH RC 32");          //Natural: WRITE ( 1 ) / 'ISN TABLE EXCEEDED MAX SIZE OF'#ISN-MAX / 'INCREASE ISN TABLE SIZE' / 'JOB ABENDING WITH RC 32'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(32);  if (true) return;                                                                                                                 //Natural: TERMINATE 32
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn_Entry.getValue(pnd_Sub).setValue(pnd_Input_File_Pnd_Isn);                                                                                             //Natural: ASSIGN #ISN-ENTRY ( #SUB ) := #ISN
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, NEWLINE,"RECORDS READ:",pnd_Record_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"UPDATE COUNT:",pnd_Update_Count, new                  //Natural: WRITE ( 1 ) / 'RECORDS READ:' #RECORD-COUNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'UPDATE COUNT:' #UPDATE-COUNT ( EM = ZZZ,ZZZ,ZZ9 )
            ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PPG-GKEYS
        //* ***********************************************************************
    }
    private void sub_Update_Ppg_Gkeys() throws Exception                                                                                                                  //Natural: UPDATE-PPG-GKEYS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Isn_Cnt.setValue(pnd_Sub);                                                                                                                                    //Natural: ASSIGN #ISN-CNT := #SUB
        FOR01:                                                                                                                                                            //Natural: FOR #SUB = 1 TO #ISN-CNT
        for (pnd_Sub.setValue(1); condition(pnd_Sub.lessOrEqual(pnd_Isn_Cnt)); pnd_Sub.nadd(1))
        {
            //*    ********************************************************************
            //*    * THE AP-G-KEY AND AP-G-IND ARE UNIQUE FOR EACH CONTRACT           *
            //*    * WITH THE SAME SSN AND PPG                                        *
            //*    * AP-G-IND 1 - LAST RECORD, 0 - ALL PREVIOUS RECORDS               *
            //*    * AP-G-KEY 1 - FIRST RECORD, ALL OTHER RECORDS INCREMENTED BY 1    *
            //*    ********************************************************************
            //*  FIRST RECORD
            //*  LAST RECORD
            short decideConditionsMet90 = 0;                                                                                                                              //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SUB = 1
            if (condition(pnd_Sub.equals(1)))
            {
                decideConditionsMet90++;
                pnd_Key_Value.setValue(1);                                                                                                                                //Natural: ASSIGN #KEY-VALUE := 1
                pnd_Ind_Value.setValue(0);                                                                                                                                //Natural: ASSIGN #IND-VALUE := 0
            }                                                                                                                                                             //Natural: WHEN #SUB = #ISN-CNT
            else if (condition(pnd_Sub.equals(pnd_Isn_Cnt)))
            {
                decideConditionsMet90++;
                //*  OTHER RECORDS
                pnd_Key_Value.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #KEY-VALUE
                pnd_Ind_Value.setValue(1);                                                                                                                                //Natural: ASSIGN #IND-VALUE := 1
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Key_Value.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #KEY-VALUE
            }                                                                                                                                                             //Natural: END-DECIDE
            GET01:                                                                                                                                                        //Natural: GET UPDT-VIEW #ISN-ENTRY ( #SUB )
            vw_updt_View.readByID(pnd_Isn_Entry.getValue(pnd_Sub).getLong(), "GET01");
            updt_View_Ap_G_Key.setValue(pnd_Key_Value);                                                                                                                   //Natural: ASSIGN AP-G-KEY := #KEY-VALUE
            updt_View_Ap_G_Ind.setValue(pnd_Ind_Value);                                                                                                                   //Natural: ASSIGN AP-G-IND := #IND-VALUE
            //*  (0950)
            vw_updt_View.updateDBRow("GET01");                                                                                                                            //Natural: UPDATE
            pnd_Et_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ET-COUNT
            pnd_Update_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #UPDATE-COUNT
            if (condition(pnd_Et_Count.greater(25)))                                                                                                                      //Natural: IF #ET-COUNT > 25
            {
                pnd_Et_Count.reset();                                                                                                                                     //Natural: RESET #ET-COUNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, "ISN",                                                                                                                                //Natural: DISPLAY ( 1 ) 'ISN' #ISN-ENTRY ( #SUB ) 'PIN' UPDT-VIEW.AP-PIN-NBR 'PPG' UPDT-VIEW.AP-COLL-CODE 'DATE' UPDT-VIEW.AP-DT-ENT-SYS 'TIME' UPDT-VIEW.AP-RCRD-UPDT-TM-STAMP 'KEY' UPDT-VIEW.AP-G-KEY UPDT-VIEW.AP-G-IND
            		pnd_Isn_Entry.getValue(pnd_Sub),"PIN",
            		updt_View_Ap_Pin_Nbr,"PPG",
            		updt_View_Ap_Coll_Code,"DATE",
            		updt_View_Ap_Dt_Ent_Sys,"TIME",
            		updt_View_Ap_Rcrd_Updt_Tm_Stamp,"KEY",
            		updt_View_Ap_G_Key,updt_View_Ap_G_Ind);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_File_Pnd_Pin_PpgIsBreak = pnd_Input_File_Pnd_Pin_Ppg.isBreak(endOfData);
        if (condition(pnd_Input_File_Pnd_Pin_PpgIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-PPG-GKEYS
            sub_Update_Ppg_Gkeys();
            if (condition(Global.isEscape())) {return;}
            pnd_Sub.reset();                                                                                                                                              //Natural: RESET #SUB #ISN-ENTRY ( * )
            pnd_Isn_Entry.getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=65");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(10),"PRAP G-KEY UPDATE REPORT",
            NEWLINE);

        getReports().setDisplayColumns(1, "ISN",
        		pnd_Isn_Entry,"PIN",
        		updt_View_Ap_Pin_Nbr,"PPG",
        		updt_View_Ap_Coll_Code,"DATE",
        		updt_View_Ap_Dt_Ent_Sys,"TIME",
        		updt_View_Ap_Rcrd_Updt_Tm_Stamp,"KEY",
        		updt_View_Ap_G_Key,updt_View_Ap_G_Ind);
    }
}
