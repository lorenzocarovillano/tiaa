/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:30 PM
**        * FROM NATURAL PROGRAM : Appb320
************************************************************
**        * FILE NAME            : Appb320.java
**        * CLASS NAME           : Appb320
**        * INSTANCE NAME        : Appb320
************************************************************
************************************************************************
* PROGRAM  : APPB320 - PRAP PURGE PROCESS                              *
* AUTHOR   : KRIS GATES                                                *
* DATE     : JANUARY 5, 2007                                           *
* FUNCTION : READS WORK FILE CREATED BY APPB300 AND ACCEPTS OPEN STATUS*
*            TO BE USED BY OVERDUE LETTERS PROCESSING                  *
* UPDATED  : 1/23/08  DEVELBISS  SKIP PREMIUM RECORDS IF               *
*            #PPG-MAIL-PREM FROM THE 'PG' TABLE IS 'N'.      CHG46840  *
* 09/23/08 DEVELBISS - RE-STOW NEW LAYOUT OF APPL301 FOR AUTO ENROLL   *
* 09/17/09 DEVELBISS - FIX DUPLICATE RECORD, USE PLAN NUMBER FOR PG    *
*            TABLE LOOKUP FOR OMNI CONTRACTS.  SEE BJD SUNY            *
* 10/20/09 C. AVE    - RE-STOW NEW LAYOUT OF APPL301 FOR ACIS PERF     *
* 03/10/10 C. AVE    - INCLUDED IRA INDEXED PRODUCTS - TIGR            *
* 05/02/11 C. SCHNEIDER - RESTOWED TO PICK UP CHANGES TO APPL300 (JHU) *
* 06/27/11 C. SCHNEIDER - RESTOWED TO PICK UP CHANGES TO APPL300 (TIC) *
* 11/17/12 L. SHU    - RESTOWED TO PICK UP CHANGES TO APPL300    (LPOA)*
* 07/18/13 L. SHU    - RESTOWED TO FOR APPL300 FOR IRA SUBSTITUTION    *
* 09/12/13 B.NEWSOM - RESTOW FOR AP-NON-PROPRIETARY-PKG-IND ADDED TO   *
*                     ADDED TO APPL301.                         (MTSIN)*
* 09/01/15 L. SHU   - RESTOW FOR NEW FIELDS IN APPL300 FOR BENE IN     *
*                     THE PLAN.                                 (BIP)  *
* 11/01/16 L SHU    - RESTOW FOR NEW FIELDS IN APPL300 FOR      (1IRA) *
*                     ONEIRA                                           *
* 06/19/17 BABRE    - PIN EXPANSION CHANGES. CHG425939 STOW ONLY       *
* 11/09/19 B.NEWSOM - ADDED THE EXTENDED AREA OF APPL300        (IISG) *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb320 extends BLNatBase
{
    // Data Areas
    private PdaAciadate pdaAciadate;
    private LdaAppl300 ldaAppl300;
    private LdaAppl302 ldaAppl302;
    private PdaAcia2100 pdaAcia2100;
    private PdaIisa8000 pdaIisa8000;
    private PdaScia8200 pdaScia8200;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Data;
    private DbsField pnd_Date;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt;

    private DbsGroup pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Appl;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Del_Ovd;
    private DbsField pnd_Debug;
    private DbsField pnd_Contact;
    private DbsField pnd_I;
    private DbsField pnd_Temp_Cntr;

    private DbsGroup pnd_Temp_Cntr__R_Field_2;
    private DbsField pnd_Temp_Cntr_Pnd_Cntr_Pref;
    private DbsField pnd_Temp_Cntr_Pnd_Cntr_Nbr;
    private DbsField pnd_Temp_C_Cntr;

    private DbsGroup pnd_Temp_C_Cntr__R_Field_3;
    private DbsField pnd_Temp_C_Cntr_Pnd_Cntr_C_Pref;
    private DbsField pnd_Temp_C_Cntr_Pnd_Cntr_C_Nbr;
    private DbsField pnd_Sgrd;
    private DbsField pnd_Hold_Plan;
    private DbsField pnd_Sgrd_Call_Cnt;
    private DbsField pnd_File_Open_Sw;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Pnd_Wk_Compressed_Nme;
    private DbsField pnd_Work_Pnd_Wk_F_Name;
    private DbsField pnd_Work_Pnd_Wk_L_Name;
    private DbsField pnd_Work_Pnd_Wk_M_Name;
    private DbsField pnd_Work_Pnd_Wk_Extra_Name;
    private DbsField pnd_Work_Pnd_Wk_Zip;

    private DbsGroup pnd_Work__R_Field_4;
    private DbsField pnd_Work_Pnd_Wk_Zip5;
    private DbsField pnd_Work_Pnd_Wk_Zip4;
    private DbsField pnd_Work_Pnd_Wk_Zip9;
    private DbsField pnd_Work_Pnd_Wk_Cntr;
    private DbsField pnd_Work_Pnd_Wk_City;
    private DbsField pnd_Work_Pnd_Wk_City_State_Zip9;
    private DbsField pnd_Wk_Issue_Dt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAciadate = new PdaAciadate(localVariables);
        ldaAppl300 = new LdaAppl300();
        registerRecord(ldaAppl300);
        ldaAppl302 = new LdaAppl302();
        registerRecord(ldaAppl302);
        pdaAcia2100 = new PdaAcia2100(localVariables);
        pdaIisa8000 = new PdaIisa8000(localVariables);
        pdaScia8200 = new PdaScia8200(localVariables);

        // Local Variables
        pnd_Data = localVariables.newFieldInRecord("pnd_Data", "#DATA", FieldType.STRING, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Ppg_Entry_Dscrptn_Txt = localVariables.newFieldInRecord("pnd_Ppg_Entry_Dscrptn_Txt", "#PPG-ENTRY-DSCRPTN-TXT", FieldType.STRING, 60);

        pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1 = localVariables.newGroupInRecord("pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1", "REDEFINE", pnd_Ppg_Entry_Dscrptn_Txt);
        pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Appl = pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1.newFieldInGroup("pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Appl", 
            "#PPG-MAIL-APPL", FieldType.STRING, 1);
        pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem = pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1.newFieldInGroup("pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem", 
            "#PPG-MAIL-PREM", FieldType.STRING, 1);
        pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Del_Ovd = pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1.newFieldInGroup("pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Del_Ovd", 
            "#PPG-DEL-OVD", FieldType.STRING, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Contact = localVariables.newFieldInRecord("pnd_Contact", "#CONTACT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Temp_Cntr = localVariables.newFieldInRecord("pnd_Temp_Cntr", "#TEMP-CNTR", FieldType.STRING, 10);

        pnd_Temp_Cntr__R_Field_2 = localVariables.newGroupInRecord("pnd_Temp_Cntr__R_Field_2", "REDEFINE", pnd_Temp_Cntr);
        pnd_Temp_Cntr_Pnd_Cntr_Pref = pnd_Temp_Cntr__R_Field_2.newFieldInGroup("pnd_Temp_Cntr_Pnd_Cntr_Pref", "#CNTR-PREF", FieldType.STRING, 1);
        pnd_Temp_Cntr_Pnd_Cntr_Nbr = pnd_Temp_Cntr__R_Field_2.newFieldInGroup("pnd_Temp_Cntr_Pnd_Cntr_Nbr", "#CNTR-NBR", FieldType.STRING, 9);
        pnd_Temp_C_Cntr = localVariables.newFieldInRecord("pnd_Temp_C_Cntr", "#TEMP-C-CNTR", FieldType.STRING, 10);

        pnd_Temp_C_Cntr__R_Field_3 = localVariables.newGroupInRecord("pnd_Temp_C_Cntr__R_Field_3", "REDEFINE", pnd_Temp_C_Cntr);
        pnd_Temp_C_Cntr_Pnd_Cntr_C_Pref = pnd_Temp_C_Cntr__R_Field_3.newFieldInGroup("pnd_Temp_C_Cntr_Pnd_Cntr_C_Pref", "#CNTR-C-PREF", FieldType.STRING, 
            1);
        pnd_Temp_C_Cntr_Pnd_Cntr_C_Nbr = pnd_Temp_C_Cntr__R_Field_3.newFieldInGroup("pnd_Temp_C_Cntr_Pnd_Cntr_C_Nbr", "#CNTR-C-NBR", FieldType.STRING, 
            9);
        pnd_Sgrd = localVariables.newFieldInRecord("pnd_Sgrd", "#SGRD", FieldType.BOOLEAN, 1);
        pnd_Hold_Plan = localVariables.newFieldInRecord("pnd_Hold_Plan", "#HOLD-PLAN", FieldType.STRING, 6);
        pnd_Sgrd_Call_Cnt = localVariables.newFieldInRecord("pnd_Sgrd_Call_Cnt", "#SGRD-CALL-CNT", FieldType.INTEGER, 4);
        pnd_File_Open_Sw = localVariables.newFieldInRecord("pnd_File_Open_Sw", "#FILE-OPEN-SW", FieldType.STRING, 1);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Pnd_Wk_Compressed_Nme = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Compressed_Nme", "#WK-COMPRESSED-NME", FieldType.STRING, 40);
        pnd_Work_Pnd_Wk_F_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_F_Name", "#WK-F-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_L_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_L_Name", "#WK-L-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_M_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_M_Name", "#WK-M-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_Extra_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Extra_Name", "#WK-EXTRA-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_Zip = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip", "#WK-ZIP", FieldType.STRING, 9);

        pnd_Work__R_Field_4 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_4", "REDEFINE", pnd_Work_Pnd_Wk_Zip);
        pnd_Work_Pnd_Wk_Zip5 = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Pnd_Wk_Zip5", "#WK-ZIP5", FieldType.STRING, 5);
        pnd_Work_Pnd_Wk_Zip4 = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Pnd_Wk_Zip4", "#WK-ZIP4", FieldType.STRING, 4);
        pnd_Work_Pnd_Wk_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip9", "#WK-ZIP9", FieldType.STRING, 10);
        pnd_Work_Pnd_Wk_Cntr = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Cntr", "#WK-CNTR", FieldType.INTEGER, 2);
        pnd_Work_Pnd_Wk_City = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City", "#WK-CITY", FieldType.STRING, 29);
        pnd_Work_Pnd_Wk_City_State_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City_State_Zip9", "#WK-CITY-STATE-ZIP9", FieldType.STRING, 35);
        pnd_Wk_Issue_Dt = localVariables.newFieldInRecord("pnd_Wk_Issue_Dt", "#WK-ISSUE-DT", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl300.initializeValues();
        ldaAppl302.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb320() throws Exception
    {
        super("Appb320");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB320", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60 ZP = ON;//Natural: FORMAT ( 1 ) LS = 132 PS = 60 ZP = ON;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 ZP = ON
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  ARR KG
        //*  TIC
        //*  IISG
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 CV-RULE-DATA-1 CV-RULE-DATA-2 CV-RULE-DATA-3 CV-RULE-DATA-4 CV-RULE-DATA-5 CV-RULE-DATA-6 CV-RULE-DATA-7 CV-RULE-DATA-8 CV-RULE-DATA-9 CV-RULE-DATA-10 CV-RULE-DATA-11 CV-RULE-DATA-12 CV-RULE-DATA-13 CV-RULE-DATA-14 CV-RULE-DATA-15 CV-RULE-DATA-16 CV-RULE-DATA-17 CV-RULE-DATA-18 CV-RULE-DATA-19
        while (condition(getWorkFiles().read(1, ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_1(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_2(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_3(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_4(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_5(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_6(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_7(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_8(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_9(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_10(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_11(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_12(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_13(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_14(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_15(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_16(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_17(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_18(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_19())))
        {
            if (condition(!(ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("G") || ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("H") || ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("I")))) //Natural: ACCEPT IF CV-PRAP-RULE.CV-STATUS EQ 'G' OR EQ 'H' OR EQ 'I'
            {
                continue;
            }
            //*  IRA PRODUCTS
            //*  TIGR - INCREASED FROM 6 TO 9
            if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Lob().equals("I") && ldaAppl300.getCv_Prap_Rule_Cv_Lob_Type().lessOrEqual("9")))                                  //Natural: IF CV-PRAP-RULE.CV-LOB EQ 'I' AND CV-PRAP-RULE.CV-LOB-TYPE LE '9'
            {
                //* *    CV-PRAP-RULE.CV-LOB-TYPE LE '6'
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  GA5 PRODUCT
            if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Lob().equals("S") && ldaAppl300.getCv_Prap_Rule_Cv_Lob_Type().equals("9")))                                       //Natural: IF CV-PRAP-RULE.CV-LOB EQ 'S' AND CV-PRAP-RULE.CV-LOB-TYPE EQ '9'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DAY-DIFF-RTN
            sub_Day_Diff_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaAppl302.getCv_Prap_Rule1().setValuesByName(ldaAppl300.getCv_Prap_Rule());                                                                                  //Natural: MOVE BY NAME CV-PRAP-RULE TO CV-PRAP-RULE1
            ldaAppl302.getCv_Prap_Rule1_Cv_Pref().reset();                                                                                                                //Natural: RESET CV-PRAP-RULE1.CV-PREF CV-PRAP-RULE1.CV-CONT #TEMP-CNTR #TEMP-C-CNTR
            ldaAppl302.getCv_Prap_Rule1_Cv_Cont().reset();
            pnd_Temp_Cntr.reset();
            pnd_Temp_C_Cntr.reset();
            pnd_Temp_Cntr.setValue(ldaAppl300.getCv_Prap_Rule_Cv_Tiaa_Cntrct());                                                                                          //Natural: ASSIGN #TEMP-CNTR := CV-PRAP-RULE.CV-TIAA-CNTRCT
            ldaAppl302.getCv_Prap_Rule1_Cv_Pref().setValue(pnd_Temp_Cntr_Pnd_Cntr_Pref);                                                                                  //Natural: ASSIGN CV-PRAP-RULE1.CV-PREF := #CNTR-PREF
            ldaAppl302.getCv_Prap_Rule1_Cv_Cont().setValue(pnd_Temp_Cntr_Pnd_Cntr_Nbr);                                                                                   //Natural: ASSIGN CV-PRAP-RULE1.CV-CONT := #CNTR-NBR
            pnd_Temp_C_Cntr.setValue(ldaAppl300.getCv_Prap_Rule_Cv_Cref_Cert());                                                                                          //Natural: ASSIGN #TEMP-C-CNTR := CV-PRAP-RULE.CV-CREF-CERT
            ldaAppl302.getCv_Prap_Rule1_Cv_C_Pref().setValue(pnd_Temp_C_Cntr_Pnd_Cntr_C_Pref);                                                                            //Natural: ASSIGN CV-PRAP-RULE1.CV-C-PREF := #CNTR-C-PREF
            ldaAppl302.getCv_Prap_Rule1_Cv_C_Cont().setValue(pnd_Temp_C_Cntr_Pnd_Cntr_C_Nbr);                                                                             //Natural: ASSIGN CV-PRAP-RULE1.CV-C-CONT := #CNTR-C-NBR
                                                                                                                                                                          //Natural: PERFORM GET-MAIL-IND
            sub_Get_Mail_Ind();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Record_Type().equals(1)))                                                                                         //Natural: IF CV-PRAP-RULE.CV-RECORD-TYPE EQ 1
            {
                ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().setValue(pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Appl);                                                          //Natural: MOVE #PPG-MAIL-APPL TO CV-MAIL-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  CHG46840
                if (condition(pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem.equals("N")))                                                                                   //Natural: IF #PPG-MAIL-PREM EQ 'N'
                {
                    //*  CHG46840
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  CHG46840
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().setValue(pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem);                                                      //Natural: MOVE #PPG-MAIL-PREM TO CV-MAIL-IND
                    //*  CHG46860
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().equals(" ")))                                                                                         //Natural: IF CV-MAIL-IND EQ ' '
            {
                ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().setValue("Y");                                                                                                  //Natural: MOVE 'Y' TO CV-MAIL-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Coll_Code().equals("SGRD")))                                                                                      //Natural: IF CV-PRAP-RULE.CV-COLL-CODE EQ 'SGRD'
            {
                if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Plan_No().equals("151169") || ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Plan_No().equals("151166")              //Natural: IF CV-PRAP-RULE.CV-SGRD-PLAN-NO EQ '151169' OR EQ '151166' OR EQ '151212'
                    || ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Plan_No().equals("151212")))
                {
                    if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().less(366)))                                                                                   //Natural: IF CV-DAY-DIFF LT 366
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Sgrd_Call_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #SGRD-CALL-CNT
                                                                                                                                                                          //Natural: PERFORM CALL-OMNI-CONTACT
                    sub_Call_Omni_Contact();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  BJD SUNY START
                    //*      WRITE WORK FILE 2 CV-RULE-DATA-L CV-RULE-DATA-M
                    if (condition(pnd_Contact.equals("Y")))                                                                                                               //Natural: IF #CONTACT = 'Y'
                    {
                        getWorkFiles().write(2, false, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M());                       //Natural: WRITE WORK FILE 2 CV-RULE-DATA-L CV-RULE-DATA-M
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BJD SUNY END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GET-CONTACT
                sub_Get_Contact();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Contact.reset();                                                                                                                                      //Natural: RESET #CONTACT CV-FIRST-NME CV-MDDLE-NME CV-LAST-NME
                ldaAppl302.getCv_Prap_Rule1_Cv_First_Nme().reset();
                ldaAppl302.getCv_Prap_Rule1_Cv_Mddle_Nme().reset();
                ldaAppl302.getCv_Prap_Rule1_Cv_Last_Nme().reset();
                FOR01:                                                                                                                                                    //Natural: FOR #I 1 #CONTACT-COUNT
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaIisa8000.getPnd_Ac_Pda_Pnd_Contact_Count())); pnd_I.nadd(1))
                {
                    if (condition(pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(pnd_I).equals(" ") || pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_1().getValue(pnd_I).equals(" "))) //Natural: IF #LAST-NME ( #I ) EQ ' ' OR #ADDRESS-LINE-1 ( #I ) EQ ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Contact.setValue("Y");                                                                                                                            //Natural: MOVE 'Y' TO #CONTACT
                    ldaAppl302.getCv_Prap_Rule1_Cv_First_Nme().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_First_Nme().getValue(pnd_I));                                       //Natural: MOVE #FIRST-NME ( #I ) TO CV-FIRST-NME
                    ldaAppl302.getCv_Prap_Rule1_Cv_Mddle_Nme().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Middle_Nme().getValue(pnd_I));                                      //Natural: MOVE #MIDDLE-NME ( #I ) TO CV-MDDLE-NME
                    ldaAppl302.getCv_Prap_Rule1_Cv_Last_Nme().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(pnd_I));                                         //Natural: MOVE #LAST-NME ( #I ) TO CV-LAST-NME
                    ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index().setValue(pnd_I);                                                                                          //Natural: MOVE #I TO CV-ADDR-INDEX
                    pnd_Wk_Issue_Dt.reset();                                                                                                                              //Natural: RESET #WK-ISSUE-DT
                    pnd_Wk_Issue_Dt.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_T_Doi(),new ReportEditMask("9999"));                                                     //Natural: MOVE EDITED CV-PRAP-RULE.CV-T-DOI ( EM = 9999 ) TO #WK-ISSUE-DT
                    if (condition(DbsUtil.maskMatches(pnd_Wk_Issue_Dt,"MMYY")))                                                                                           //Natural: IF #WK-ISSUE-DT = MASK ( MMYY )
                    {
                        ldaAppl302.getCv_Prap_Rule1_Cv_Tiaa_Doi().setValueEdited(new ReportEditMask("MMYY"),pnd_Wk_Issue_Dt);                                             //Natural: MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-TIAA-DOI ( EM = MMYY )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAppl302.getCv_Prap_Rule1_Cv_Tiaa_Doi().reset();                                                                                                //Natural: RESET CV-PRAP-RULE1.CV-TIAA-DOI
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Wk_Issue_Dt.reset();                                                                                                                              //Natural: RESET #WK-ISSUE-DT
                    pnd_Wk_Issue_Dt.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_C_Doi(),new ReportEditMask("9999"));                                                     //Natural: MOVE EDITED CV-PRAP-RULE.CV-C-DOI ( EM = 9999 ) TO #WK-ISSUE-DT
                    if (condition(DbsUtil.maskMatches(pnd_Wk_Issue_Dt,"MMYY")))                                                                                           //Natural: IF #WK-ISSUE-DT = MASK ( MMYY )
                    {
                        ldaAppl302.getCv_Prap_Rule1_Cv_Cref_Doi().setValueEdited(new ReportEditMask("MMYY"),pnd_Wk_Issue_Dt);                                             //Natural: MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-CREF-DOI ( EM = MMYY )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAppl302.getCv_Prap_Rule1_Cv_Cref_Doi().reset();                                                                                                //Natural: RESET CV-PRAP-RULE1.CV-CREF-DOI
                    }                                                                                                                                                     //Natural: END-IF
                    getWorkFiles().write(2, false, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M());                           //Natural: WRITE WORK FILE 2 CV-RULE-DATA-L CV-RULE-DATA-M
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Contact.equals(" ")))                                                                                                                       //Natural: IF #CONTACT EQ ' '
            {
                ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().setValue("N");                                                                                                  //Natural: MOVE 'N' TO CV-MAIL-IND
                ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index().reset();                                                                                                      //Natural: RESET CV-ADDR-INDEX
                pnd_Wk_Issue_Dt.reset();                                                                                                                                  //Natural: RESET #WK-ISSUE-DT
                pnd_Wk_Issue_Dt.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_T_Doi(),new ReportEditMask("9999"));                                                         //Natural: MOVE EDITED CV-PRAP-RULE.CV-T-DOI ( EM = 9999 ) TO #WK-ISSUE-DT
                if (condition(DbsUtil.maskMatches(pnd_Wk_Issue_Dt,"MMYY")))                                                                                               //Natural: IF #WK-ISSUE-DT = MASK ( MMYY )
                {
                    ldaAppl302.getCv_Prap_Rule1_Cv_Tiaa_Doi().setValueEdited(new ReportEditMask("MMYY"),pnd_Wk_Issue_Dt);                                                 //Natural: MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-TIAA-DOI ( EM = MMYY )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl302.getCv_Prap_Rule1_Cv_Tiaa_Doi().reset();                                                                                                    //Natural: RESET CV-PRAP-RULE1.CV-TIAA-DOI
                }                                                                                                                                                         //Natural: END-IF
                pnd_Wk_Issue_Dt.reset();                                                                                                                                  //Natural: RESET #WK-ISSUE-DT
                pnd_Wk_Issue_Dt.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_C_Doi(),new ReportEditMask("9999"));                                                         //Natural: MOVE EDITED CV-PRAP-RULE.CV-C-DOI ( EM = 9999 ) TO #WK-ISSUE-DT
                if (condition(DbsUtil.maskMatches(pnd_Wk_Issue_Dt,"MMYY")))                                                                                               //Natural: IF #WK-ISSUE-DT = MASK ( MMYY )
                {
                    ldaAppl302.getCv_Prap_Rule1_Cv_Cref_Doi().setValueEdited(new ReportEditMask("MMYY"),pnd_Wk_Issue_Dt);                                                 //Natural: MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-CREF-DOI ( EM = MMYY )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl302.getCv_Prap_Rule1_Cv_Cref_Doi().reset();                                                                                                    //Natural: RESET CV-PRAP-RULE1.CV-CREF-DOI
                }                                                                                                                                                         //Natural: END-IF
                getWorkFiles().write(2, false, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M());                               //Natural: WRITE WORK FILE 2 CV-RULE-DATA-L CV-RULE-DATA-M
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CLOSE FILE - CALL TO OMNI FOR CONTACT INFORMATION
        //*  ----------
        if (condition(pnd_Sgrd_Call_Cnt.greater(getZero())))                                                                                                              //Natural: IF #SGRD-CALL-CNT > 0
        {
            pdaScia8200.getCon_Part_Rec().reset();                                                                                                                        //Natural: RESET CON-PART-REC
            pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("Y");                                                                                                       //Natural: ASSIGN CON-EOF-SW := 'Y'
            DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                 //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
            if (condition(Global.isEscape())) return;
            getReports().write(0, "NUMBER OF CALLS TO RETRIEVE SUNGARD CONTACT DATA:",pnd_Sgrd_Call_Cnt);                                                                 //Natural: WRITE 'NUMBER OF CALLS TO RETRIEVE SUNGARD CONTACT DATA:' #SGRD-CALL-CNT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DAY-DIFF-RTN
        //* *============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DAY-DIFF-RTN2
        //* *===========================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTACT
        //*  #FUNCT-CDE-IN(1)   := '01'
        //*  #FUNCT-CDE-IN(2)   := '02'
        //*  #FUNCT-CDE-IN(3)   := '03'
        //*  #FUNCT-CDE-IN(4)   := '06'
        //*  #FUNCT-CDE-IN(5)   := '07'
        //* *============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MAIL-IND
        //* *===========================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-OMNI-CONTACT
        //* *======
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Day_Diff_Rtn() throws Exception                                                                                                                      //Natural: DAY-DIFF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *============================
        pdaAciadate.getAciadate_Pnd_Date_N_1().reset();                                                                                                                   //Natural: RESET #DATE-N-1
        if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Record_Type().equals(1)))                                                                                             //Natural: IF CV-PRAP-RULE.CV-RECORD-TYPE EQ 1
        {
            pnd_Data.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_Dt_App_Recvd(),new ReportEditMask("99999999"));                                                         //Natural: MOVE EDITED CV-PRAP-RULE.CV-DT-APP-RECVD ( EM = 99999999 ) TO #DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Data.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_Dt_Ent_Sys(),new ReportEditMask("99999999"));                                                           //Natural: MOVE EDITED CV-PRAP-RULE.CV-DT-ENT-SYS ( EM = 99999999 ) TO #DATA
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DAY-DIFF-RTN2
        sub_Day_Diff_Rtn2();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Day_Diff_Rtn2() throws Exception                                                                                                                     //Natural: DAY-DIFF-RTN2
    {
        if (BLNatReinput.isReinput()) return;

        //* *============================
        if (condition(DbsUtil.maskMatches(pnd_Data,"MMDDYYYY")))                                                                                                          //Natural: IF #DATA EQ MASK ( MMDDYYYY )
        {
            pnd_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Data);                                                                                             //Natural: MOVE EDITED #DATA TO #DATE ( EM = MMDDYYYY )
            pdaAciadate.getAciadate_Pnd_Date_A_1().setValueEdited(pnd_Date,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #DATE-A-1
        }                                                                                                                                                                 //Natural: END-IF
        pdaAciadate.getAciadate_Pnd_Date_N_2().setValue(Global.getDATN());                                                                                                //Natural: MOVE *DATN TO #DATE-N-2
        pdaAciadate.getAciadate_Pnd_Function().setValue("01");                                                                                                            //Natural: MOVE '01' TO #FUNCTION
        if (condition(pdaAciadate.getAciadate_Pnd_Date_N_1().notEquals(getZero())))                                                                                       //Natural: IF #DATE-N-1 NE 0
        {
            DbsUtil.callnat(Acindate.class , getCurrentProcessState(), pdaAciadate.getAciadate());                                                                        //Natural: CALLNAT 'ACINDATE' ACIADATE
            if (condition(Global.isEscape())) return;
            if (condition(pdaAciadate.getAciadate_Pnd_Error_Msg().notEquals(" ") || pdaAciadate.getAciadate_Pnd_Error_Code().notEquals(getZero())))                       //Natural: IF #ERROR-MSG NE ' ' OR #ERROR-CODE NE 0
            {
                ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().reset();                                                                                                        //Natural: RESET CV-DAY-DIFF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().setValue(pdaAciadate.getAciadate_Pnd_Diff());                                                                   //Natural: MOVE #DIFF TO CV-DAY-DIFF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().reset();                                                                                                            //Natural: RESET CV-DAY-DIFF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Contact() throws Exception                                                                                                                       //Natural: GET-CONTACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *===========================
        pdaIisa8000.getPnd_Ac_Pda().reset();                                                                                                                              //Natural: RESET #AC-PDA
        pdaIisa8000.getPnd_Ac_Pda_Pnd_Ppg_Cde_In().setValue(ldaAppl300.getCv_Prap_Rule_Cv_Coll_Code());                                                                   //Natural: ASSIGN #PPG-CDE-IN := CV-PRAP-RULE.CV-COLL-CODE
        DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_Ppg_Cde_In()), new ExamineTranslate(TranslateOption.Upper));                                      //Natural: EXAMINE #PPG-CDE-IN TRANSLATE INTO UPPER CASE
        pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Type_In().setValue("01");                                                                                                   //Natural: ASSIGN #ADDRESS-TYPE-IN := '01'
        pdaIisa8000.getPnd_Ac_Pda_Pnd_Contact_Level().setValue(" ");                                                                                                      //Natural: ASSIGN #CONTACT-LEVEL := ' '
        pdaIisa8000.getPnd_Ac_Pda_Pnd_Funct_Cde_In().getValue(1).setValue("01");                                                                                          //Natural: ASSIGN #FUNCT-CDE-IN ( 1 ) := '01'
        pdaIisa8000.getPnd_Ac_Pda_Pnd_Ext_Contact_Count().setValue(1);                                                                                                    //Natural: ASSIGN #EXT-CONTACT-COUNT := 1
        pdaIisa8000.getPnd_Ac_Pda_Pnd_Role_Cde_In().setValue(" ");                                                                                                        //Natural: ASSIGN #ROLE-CDE-IN := ' '
        pdaIisa8000.getPnd_Ac_Pda_Pnd_Int_Contact_Count().setValue(0);                                                                                                    //Natural: ASSIGN #INT-CONTACT-COUNT := 0
        DbsUtil.callnat(Iisn8000.class , getCurrentProcessState(), pdaIisa8000.getPnd_Ac_Pda());                                                                          //Natural: CALLNAT 'IISN8000' #AC-PDA
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Mail_Ind() throws Exception                                                                                                                      //Natural: GET-MAIL-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *============================
        pdaAcia2100.getAcia2100().reset();                                                                                                                                //Natural: RESET ACIA2100 #PPG-ENTRY-DSCRPTN-TXT
        pnd_Ppg_Entry_Dscrptn_Txt.reset();
        //*  SEARCH BY PPG
        pdaAcia2100.getAcia2100_Pnd_Search_Mode().setValue("AW");                                                                                                         //Natural: MOVE 'AW' TO ACIA2100.#SEARCH-MODE
        pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Actve_Ind().setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO ACIA2100.#S-AW-ENTRY-ACTVE-IND
        pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Table_Id_Nbr().setValue(300);                                                                                              //Natural: MOVE 000300 TO ACIA2100.#S-AW-ENTRY-TABLE-ID-NBR
        pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Table_Sub_Id().setValue("PG");                                                                                             //Natural: MOVE 'PG' TO ACIA2100.#S-AW-ENTRY-TABLE-SUB-ID
        //*  BJD SUNY START
        //*  MOVE CV-PRAP-RULE.CV-COLL-CODE TO ACIA2100.#S-AW-ENTRY-CDE
        if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Coll_Code().equals("SGRD")))                                                                                          //Natural: IF CV-PRAP-RULE.CV-COLL-CODE EQ 'SGRD'
        {
            pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Cde().setValue(ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Plan_No());                                                          //Natural: MOVE CV-PRAP-RULE.CV-SGRD-PLAN-NO TO ACIA2100.#S-AW-ENTRY-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Cde().setValue(ldaAppl300.getCv_Prap_Rule_Cv_Coll_Code());                                                             //Natural: MOVE CV-PRAP-RULE.CV-COLL-CODE TO ACIA2100.#S-AW-ENTRY-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  BJD SUNY END
        //*  READ APP-TABLE-ENTRY
        DbsUtil.callnat(Acin2100.class , getCurrentProcessState(), pdaAcia2100.getAcia2100(), pnd_Debug);                                                                 //Natural: CALLNAT 'ACIN2100' ACIA2100 #DEBUG
        if (condition(Global.isEscape())) return;
        if (condition(pdaAcia2100.getAcia2100_Pnd_Return_Code().equals("  ")))                                                                                            //Natural: IF ACIA2100.#RETURN-CODE = '  '
        {
            pnd_Ppg_Entry_Dscrptn_Txt.setValue(pdaAcia2100.getAcia2100_Pnd_Entry_Dscrptn_Txt());                                                                          //Natural: MOVE ACIA2100.#ENTRY-DSCRPTN-TXT TO #PPG-ENTRY-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Omni_Contact() throws Exception                                                                                                                 //Natural: CALL-OMNI-CONTACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *===========================
        //*  SET FILE OPEN SWITCH ON FIRST PASS
        //*  ----------------------------------
        if (condition(pnd_Sgrd_Call_Cnt.equals(1)))                                                                                                                       //Natural: IF #SGRD-CALL-CNT = 1
        {
            pdaScia8200.getCon_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN CON-FILE-OPEN-SW := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("N");                                                                                                           //Natural: ASSIGN CON-EOF-SW := 'N'
        pdaScia8200.getCon_Part_Rec_Con_Function_Code().getValue(1).setValue("08");                                                                                       //Natural: ASSIGN CON-FUNCTION-CODE ( 1 ) := '08'
        pdaScia8200.getCon_Part_Rec_Con_Access_Level().setValue(" ");                                                                                                     //Natural: ASSIGN CON-ACCESS-LEVEL := ' '
        pdaScia8200.getCon_Part_Rec_Con_Contact_Type().setValue("E");                                                                                                     //Natural: ASSIGN CON-CONTACT-TYPE := 'E'
        pdaScia8200.getCon_Part_Rec_Con_Plan_Num().setValue(ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Plan_No());                                                                //Natural: ASSIGN CON-PLAN-NUM := CV-PRAP-RULE.CV-SGRD-PLAN-NO
        pdaScia8200.getCon_Part_Rec_Con_Part_Divsub().setValue(ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Divsub());                                                              //Natural: ASSIGN CON-PART-DIVSUB := CV-PRAP-RULE.CV-SGRD-DIVSUB
        DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                     //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia8200.getCon_Part_Rec_Con_Return_Code().notEquals(getZero()) || pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg().notEquals(" ")))            //Natural: IF CON-RETURN-CODE NE 0 OR CON-CONTACT-ERR-MSG NE ' '
        {
            getReports().write(0, "*CALL ERROR (PSG9070):",pdaScia8200.getCon_Part_Rec_Con_Return_Code(),pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg());              //Natural: WRITE '*CALL ERROR (PSG9070):' CON-RETURN-CODE CON-CONTACT-ERR-MSG
            if (Global.isEscape()) return;
            getReports().write(0, "PLAN:",pdaScia8200.getCon_Part_Rec_Con_Plan_Num());                                                                                    //Natural: WRITE 'PLAN:' CON-PLAN-NUM
            if (Global.isEscape()) return;
            ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().setValue("N");                                                                                                      //Natural: MOVE 'N' TO CV-MAIL-IND
            ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index().reset();                                                                                                          //Natural: RESET CV-ADDR-INDEX
            pnd_Contact.reset();                                                                                                                                          //Natural: RESET #CONTACT CV-FIRST-NME CV-MDDLE-NME CV-LAST-NME
            ldaAppl302.getCv_Prap_Rule1_Cv_First_Nme().reset();
            ldaAppl302.getCv_Prap_Rule1_Cv_Mddle_Nme().reset();
            ldaAppl302.getCv_Prap_Rule1_Cv_Last_Nme().reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SGRD KG
            pnd_Contact.setValue("Y");                                                                                                                                    //Natural: MOVE 'Y' TO #CONTACT
            //*  SGRD KG
            ldaAppl302.getCv_Prap_Rule1_Cv_Mddle_Nme().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO CV-MDDLE-NME
            //*  SGRD KG
            ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index().setValue(1);                                                                                                      //Natural: MOVE 1 TO CV-ADDR-INDEX
            //*  CREATE NAME VALUES
            //*  ------------------
            pdaScia8200.getCon_Part_Rec_Con_Contact_Name().separate(EnumSet.of(SeparateOption.Ignore,SeparateOption.WithAnyDelimiters), ",", pnd_Work_Pnd_Wk_L_Name,      //Natural: SEPARATE CON-CONTACT-NAME INTO #WK-L-NAME #WK-F-NAME #WK-M-NAME #WK-EXTRA-NAME IGNORE WITH DELIMITERS ','
                pnd_Work_Pnd_Wk_F_Name, pnd_Work_Pnd_Wk_M_Name, pnd_Work_Pnd_Wk_Extra_Name);
            ldaAppl302.getCv_Prap_Rule1_Cv_First_Nme().setValue(pnd_Work_Pnd_Wk_F_Name);                                                                                  //Natural: MOVE #WK-F-NAME TO CV-FIRST-NME
            ldaAppl302.getCv_Prap_Rule1_Cv_Last_Nme().setValue(pnd_Work_Pnd_Wk_L_Name);                                                                                   //Natural: MOVE #WK-L-NAME TO CV-LAST-NME
            //*         WRITE(2)
            //*           CV-PRAP-RULE.CV-SGRD-PLAN-NO
            //*           #SGRD-CALL-CNT
            //*           CV-FIRST-NME
            //*           CV-LAST-NME  //
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia8200.getCon_Part_Rec().reset();                                                                                                                            //Natural: RESET CON-PART-REC #WORK
        pnd_Work.reset();
        pnd_Wk_Issue_Dt.reset();                                                                                                                                          //Natural: RESET #WK-ISSUE-DT
        pnd_Wk_Issue_Dt.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_T_Doi(),new ReportEditMask("9999"));                                                                 //Natural: MOVE EDITED CV-PRAP-RULE.CV-T-DOI ( EM = 9999 ) TO #WK-ISSUE-DT
        if (condition(DbsUtil.maskMatches(pnd_Wk_Issue_Dt,"MMYY")))                                                                                                       //Natural: IF #WK-ISSUE-DT = MASK ( MMYY )
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Tiaa_Doi().setValueEdited(new ReportEditMask("MMYY"),pnd_Wk_Issue_Dt);                                                         //Natural: MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-TIAA-DOI ( EM = MMYY )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Tiaa_Doi().reset();                                                                                                            //Natural: RESET CV-PRAP-RULE1.CV-TIAA-DOI
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wk_Issue_Dt.reset();                                                                                                                                          //Natural: RESET #WK-ISSUE-DT
        pnd_Wk_Issue_Dt.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_C_Doi(),new ReportEditMask("9999"));                                                                 //Natural: MOVE EDITED CV-PRAP-RULE.CV-C-DOI ( EM = 9999 ) TO #WK-ISSUE-DT
        if (condition(DbsUtil.maskMatches(pnd_Wk_Issue_Dt,"MMYY")))                                                                                                       //Natural: IF #WK-ISSUE-DT = MASK ( MMYY )
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Cref_Doi().setValueEdited(new ReportEditMask("MMYY"),pnd_Wk_Issue_Dt);                                                         //Natural: MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-CREF-DOI ( EM = MMYY )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Cref_Doi().reset();                                                                                                            //Natural: RESET CV-PRAP-RULE1.CV-CREF-DOI
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-OMNI-CONTACT
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"ERROR:     ",Global.getERROR_NR(),                  //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'ERROR:     ' *ERROR-NR / 'TIAA CONTRACT:' CV-PRAP-RULE1.CV-PREF CV-PRAP-RULE1.CV-CONT / 'CREF CONTRACT:' CV-PRAP-RULE1.CV-C-PREF CV-PRAP-RULE1.CV-C-CONT /
            NEWLINE,"TIAA CONTRACT:",ldaAppl302.getCv_Prap_Rule1_Cv_Pref(),ldaAppl302.getCv_Prap_Rule1_Cv_Cont(),NEWLINE,"CREF CONTRACT:",ldaAppl302.getCv_Prap_Rule1_Cv_C_Pref(),
            ldaAppl302.getCv_Prap_Rule1_Cv_C_Cont(),NEWLINE);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60 ZP=ON");
        Global.format(1, "LS=132 PS=60 ZP=ON");
        Global.format(2, "LS=132 PS=60 ZP=ON");
    }
}
