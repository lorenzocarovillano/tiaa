/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:58 PM
**        * FROM NATURAL PROGRAM : Appb1151
************************************************************
**        * FILE NAME            : Appb1151.java
**        * CLASS NAME           : Appb1151
**        * INSTANCE NAME        : Appb1151
************************************************************
************************************************************************
* PROGRAM  : APPB1151                                                  *
* SYSTEM   : ACIS                                                      *
* TITLE    : DIALOGUE RECORD ID BUILD PROGRAM                          *
* FUNCTION : ADDS DIALOGUE RECORD ID TO THE FRONT OF EACH ACIS         *
*            EXTRACT RECORD.                                           *
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES                        *
* ----------  ---------  --------------------------------------------  *
* 07/25/2008  DEVELBISS  INITIAL WRITE                                 *
* 03/15/10    B.HOLLOWAY RECOMPILE FOR TIAA STABLE VALUE PROJECT       *
*                         - NEW VERSION OF APPL1151 & APPL1152         *
* 03/16/2010  C. AVE     INCLUDED IRA INDEXED PRODUCTS - TIGR          *
* 03/04/2011  B. ELLO    RESTOW FOR NEW VERSION OF APPL1151 & APPL1152 *
*                        FOR THE FLORIDA STATE ENROLLMENT PROJECT      *
* 05/04/2011  C. SCHNEIDER RESTOW FOR NEW VERSION OF APPL1151 &        *
*                        APPLL152 FOR JHU                              *
* 07/01/2011  L. SHU     RESTOW FOR NEW VERSION OF APPL1151 & APPL1152 *
*                        FOR THE TRANSFER IN CREDIT PROJECT            *
* 09/20/2011  C. SCHNEIDER RESTOW- NEW VERSION OF APPL1151 AND APPL1152*
*                        (MOBIUS KEY CHANGES)                          *
* 07/18/2013  C. LAEVEY  REPLACED APPL1152 WITH APPL1158   (TNGSUB)
*                        FILL RT-BUNDLE-TYPE IN APPL1158
* 09/13/2013  L. SHU     ADD MOVE FIELDS RT-NON-PROPRIETARY-PKG-IND
*                        FOR MT. SINAI                     (MTSIN)
* 09/12/2014  B. NEWSOM  ACIS/CIS CREF REDESIGN COMMUNICATIONS   (ACCRC)
*                        MOVE MULTIPLE PLAN ARRAY TO OUTPUT FILE
* 03/17/2017  B. NEWSOM  ADD NEW PROFILE - '50' FOR EASE ENROLLMENT
*                                                         (EASEENR)
* 06/15/2017  BARUA      PIN EXPANSION CHANGES. (CHG425939) STOW ONLY
* 01/13/2018  L. SHU     ADD MOVE FIELDS FOR TIAA/CREF INITIAL PREMIUM
*                                                         (ONEIRA2)
* 11/16/2019  L. SHU    ADD MOVE TPA/IPRO FIELDS FOR CNTRSTRG
*                       ADD MOVE 2ND FUND / ALLOC FOR IISG
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb1151 extends BLNatBase
{
    // Data Areas
    private LdaAppl1151 ldaAppl1151;
    private LdaAppl1158 ldaAppl1158;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read;
    private DbsField pnd_Written;
    private DbsField pnd_Contract_Type;
    private DbsField pnd_Found;
    private DbsField pnd_W_Lob;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Max_Count;
    private DbsField pnd_Lob_Table_Cnt;
    private DbsField pnd_Lob_Table_X;

    private DbsGroup pnd_Lob_Table_X__R_Field_1;

    private DbsGroup pnd_Lob_Table_X_Pnd_Lob_Table;
    private DbsField pnd_Lob_Table_X_Pnd_Lob;
    private DbsField pnd_Lob_Table_X_Pnd_Fill;
    private DbsField pnd_Lob_Table_X_Pnd_Product;

    private DataAccessProgramView vw_table_Entry;
    private DbsField table_Entry_Entry_Actve_Ind;
    private DbsField table_Entry_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Entry_Table_Sub_Id;
    private DbsField table_Entry_Entry_Cde;

    private DbsGroup table_Entry__R_Field_2;
    private DbsField table_Entry_Entry_Job_Name;

    private DbsGroup table_Entry__R_Field_3;
    private DbsField table_Entry_Entry_Lob;
    private DbsField table_Entry_Entry_Type;
    private DbsField table_Entry_Entry_Value;
    private DbsField table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup table_Entry__R_Field_4;
    private DbsField table_Entry_Profile;

    private DbsGroup table_Entry__R_Field_5;
    private DbsField table_Entry_Product;
    private DbsField pnd_Profile_Key;

    private DbsGroup pnd_Profile_Key__R_Field_6;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Cde;

    private DbsGroup pnd_Profile_Key__R_Field_7;
    private DbsField pnd_Profile_Key_Pnd_Pj_Job_Name;
    private DbsField pnd_Product_Key;

    private DbsGroup pnd_Product_Key__R_Field_8;
    private DbsField pnd_Product_Key_Pnd_Pc_Entry_Actve_Ind;
    private DbsField pnd_Product_Key_Pnd_Pc_Entry_Table_Id_Nbr;
    private DbsField pnd_Product_Key_Pnd_Pc_Entry_Table_Sub_Id;
    private DbsField pnd_Product_Key_Pnd_Pc_Entry_Cde;

    private DbsGroup pnd_Product_Key__R_Field_9;
    private DbsField pnd_Product_Key_Pnd_Pc_Lob;
    private DbsField pnd_Product_Key_Pnd_Pc_Type;
    private DbsField pnd_Product_Key_Pnd_Pc_Value;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl1151 = new LdaAppl1151();
        registerRecord(ldaAppl1151);
        ldaAppl1158 = new LdaAppl1158();
        registerRecord(ldaAppl1158);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read = localVariables.newFieldInRecord("pnd_Read", "#READ", FieldType.NUMERIC, 6);
        pnd_Written = localVariables.newFieldInRecord("pnd_Written", "#WRITTEN", FieldType.NUMERIC, 6);
        pnd_Contract_Type = localVariables.newFieldInRecord("pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 6);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_W_Lob = localVariables.newFieldInRecord("pnd_W_Lob", "#W-LOB", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Max_Count = localVariables.newFieldInRecord("pnd_Max_Count", "#MAX-COUNT", FieldType.NUMERIC, 3);
        pnd_Lob_Table_Cnt = localVariables.newFieldInRecord("pnd_Lob_Table_Cnt", "#LOB-TABLE-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Lob_Table_X = localVariables.newFieldArrayInRecord("pnd_Lob_Table_X", "#LOB-TABLE-X", FieldType.STRING, 11, new DbsArrayController(1, 26));

        pnd_Lob_Table_X__R_Field_1 = localVariables.newGroupInRecord("pnd_Lob_Table_X__R_Field_1", "REDEFINE", pnd_Lob_Table_X);

        pnd_Lob_Table_X_Pnd_Lob_Table = pnd_Lob_Table_X__R_Field_1.newGroupArrayInGroup("pnd_Lob_Table_X_Pnd_Lob_Table", "#LOB-TABLE", new DbsArrayController(1, 
            26));
        pnd_Lob_Table_X_Pnd_Lob = pnd_Lob_Table_X_Pnd_Lob_Table.newFieldInGroup("pnd_Lob_Table_X_Pnd_Lob", "#LOB", FieldType.STRING, 2);
        pnd_Lob_Table_X_Pnd_Fill = pnd_Lob_Table_X_Pnd_Lob_Table.newFieldInGroup("pnd_Lob_Table_X_Pnd_Fill", "#FILL", FieldType.STRING, 3);
        pnd_Lob_Table_X_Pnd_Product = pnd_Lob_Table_X_Pnd_Lob_Table.newFieldInGroup("pnd_Lob_Table_X_Pnd_Product", "#PRODUCT", FieldType.STRING, 6);

        vw_table_Entry = new DataAccessProgramView(new NameInfo("vw_table_Entry", "TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        table_Entry_Entry_Actve_Ind = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        table_Entry_Entry_Table_Id_Nbr = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        table_Entry_Entry_Table_Sub_Id = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        table_Entry_Entry_Cde = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        table_Entry__R_Field_2 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_2", "REDEFINE", table_Entry_Entry_Cde);
        table_Entry_Entry_Job_Name = table_Entry__R_Field_2.newFieldInGroup("table_Entry_Entry_Job_Name", "ENTRY-JOB-NAME", FieldType.STRING, 8);

        table_Entry__R_Field_3 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_3", "REDEFINE", table_Entry_Entry_Cde);
        table_Entry_Entry_Lob = table_Entry__R_Field_3.newFieldInGroup("table_Entry_Entry_Lob", "ENTRY-LOB", FieldType.STRING, 2);
        table_Entry_Entry_Type = table_Entry__R_Field_3.newFieldInGroup("table_Entry_Entry_Type", "ENTRY-TYPE", FieldType.STRING, 4);
        table_Entry_Entry_Value = table_Entry__R_Field_3.newFieldInGroup("table_Entry_Entry_Value", "ENTRY-VALUE", FieldType.STRING, 6);
        table_Entry_Entry_Dscrptn_Txt = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        table_Entry__R_Field_4 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_4", "REDEFINE", table_Entry_Entry_Dscrptn_Txt);
        table_Entry_Profile = table_Entry__R_Field_4.newFieldInGroup("table_Entry_Profile", "PROFILE", FieldType.STRING, 2);

        table_Entry__R_Field_5 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_5", "REDEFINE", table_Entry_Entry_Dscrptn_Txt);
        table_Entry_Product = table_Entry__R_Field_5.newFieldInGroup("table_Entry_Product", "PRODUCT", FieldType.STRING, 6);
        registerRecord(vw_table_Entry);

        pnd_Profile_Key = localVariables.newFieldInRecord("pnd_Profile_Key", "#PROFILE-KEY", FieldType.STRING, 29);

        pnd_Profile_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Profile_Key__R_Field_6", "REDEFINE", pnd_Profile_Key);
        pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind = pnd_Profile_Key__R_Field_6.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind", "#PJ-ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr = pnd_Profile_Key__R_Field_6.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr", "#PJ-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id = pnd_Profile_Key__R_Field_6.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id", "#PJ-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Profile_Key_Pnd_Pj_Entry_Cde = pnd_Profile_Key__R_Field_6.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Cde", "#PJ-ENTRY-CDE", FieldType.STRING, 
            20);

        pnd_Profile_Key__R_Field_7 = pnd_Profile_Key__R_Field_6.newGroupInGroup("pnd_Profile_Key__R_Field_7", "REDEFINE", pnd_Profile_Key_Pnd_Pj_Entry_Cde);
        pnd_Profile_Key_Pnd_Pj_Job_Name = pnd_Profile_Key__R_Field_7.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Job_Name", "#PJ-JOB-NAME", FieldType.STRING, 
            8);
        pnd_Product_Key = localVariables.newFieldInRecord("pnd_Product_Key", "#PRODUCT-KEY", FieldType.STRING, 29);

        pnd_Product_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Product_Key__R_Field_8", "REDEFINE", pnd_Product_Key);
        pnd_Product_Key_Pnd_Pc_Entry_Actve_Ind = pnd_Product_Key__R_Field_8.newFieldInGroup("pnd_Product_Key_Pnd_Pc_Entry_Actve_Ind", "#PC-ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Product_Key_Pnd_Pc_Entry_Table_Id_Nbr = pnd_Product_Key__R_Field_8.newFieldInGroup("pnd_Product_Key_Pnd_Pc_Entry_Table_Id_Nbr", "#PC-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Product_Key_Pnd_Pc_Entry_Table_Sub_Id = pnd_Product_Key__R_Field_8.newFieldInGroup("pnd_Product_Key_Pnd_Pc_Entry_Table_Sub_Id", "#PC-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Product_Key_Pnd_Pc_Entry_Cde = pnd_Product_Key__R_Field_8.newFieldInGroup("pnd_Product_Key_Pnd_Pc_Entry_Cde", "#PC-ENTRY-CDE", FieldType.STRING, 
            20);

        pnd_Product_Key__R_Field_9 = pnd_Product_Key__R_Field_8.newGroupInGroup("pnd_Product_Key__R_Field_9", "REDEFINE", pnd_Product_Key_Pnd_Pc_Entry_Cde);
        pnd_Product_Key_Pnd_Pc_Lob = pnd_Product_Key__R_Field_9.newFieldInGroup("pnd_Product_Key_Pnd_Pc_Lob", "#PC-LOB", FieldType.STRING, 2);
        pnd_Product_Key_Pnd_Pc_Type = pnd_Product_Key__R_Field_9.newFieldInGroup("pnd_Product_Key_Pnd_Pc_Type", "#PC-TYPE", FieldType.STRING, 4);
        pnd_Product_Key_Pnd_Pc_Value = pnd_Product_Key__R_Field_9.newFieldInGroup("pnd_Product_Key_Pnd_Pc_Value", "#PC-VALUE", FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_table_Entry.reset();

        ldaAppl1151.initializeValues();
        ldaAppl1158.initializeValues();

        localVariables.reset();
        pnd_Lob_Table_Cnt.setInitialValue(26);
        pnd_Lob_Table_X.getValue(1).setInitialValue("DA   RC    ");
        pnd_Lob_Table_X.getValue(2).setInitialValue("D2   RA    ");
        pnd_Lob_Table_X.getValue(3).setInitialValue("D3   CANP  ");
        pnd_Lob_Table_X.getValue(4).setInitialValue("D4   CANS  ");
        pnd_Lob_Table_X.getValue(5).setInitialValue("D5   RS    ");
        pnd_Lob_Table_X.getValue(6).setInitialValue("D6   RL    ");
        pnd_Lob_Table_X.getValue(7).setInitialValue("D7   GRA   ");
        pnd_Lob_Table_X.getValue(8).setInitialValue("D8   RAQV  ");
        pnd_Lob_Table_X.getValue(9).setInitialValue("D9   GRA2  ");
        pnd_Lob_Table_X.getValue(10).setInitialValue("I1   IRA   ");
        pnd_Lob_Table_X.getValue(11).setInitialValue("I2   IRA   ");
        pnd_Lob_Table_X.getValue(12).setInitialValue("I3   IRAR  ");
        pnd_Lob_Table_X.getValue(13).setInitialValue("I4   IRAC  ");
        pnd_Lob_Table_X.getValue(14).setInitialValue("I5   KEOGH ");
        pnd_Lob_Table_X.getValue(15).setInitialValue("I6   IRAS  ");
        pnd_Lob_Table_X.getValue(16).setInitialValue("S2   SRA   ");
        pnd_Lob_Table_X.getValue(17).setInitialValue("S3   GSRA  ");
        pnd_Lob_Table_X.getValue(18).setInitialValue("S4   GA    ");
        pnd_Lob_Table_X.getValue(19).setInitialValue("S5   RSP   ");
        pnd_Lob_Table_X.getValue(20).setInitialValue("S6   RSP2  ");
        pnd_Lob_Table_X.getValue(21).setInitialValue("S7   RCP   ");
        pnd_Lob_Table_X.getValue(22).setInitialValue("S8   SRAQ  ");
        pnd_Lob_Table_X.getValue(23).setInitialValue("S9   TGA   ");
        pnd_Lob_Table_X.getValue(24).setInitialValue("I7   IRIR  ");
        pnd_Lob_Table_X.getValue(25).setInitialValue("I8   IRIC  ");
        pnd_Lob_Table_X.getValue(26).setInitialValue("I9   IRIS  ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb1151() throws Exception
    {
        super("Appb1151");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB1151", onError);
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD ROUTER-EX-FILE
        while (condition(getWorkFiles().read(1, ldaAppl1151.getRouter_Ex_File())))
        {
            pnd_Read.nadd(1);                                                                                                                                             //Natural: ASSIGN #READ := #READ + 1
            ldaAppl1158.getRecord_Id_Ex_File_3().reset();                                                                                                                 //Natural: RESET RECORD-ID-EX-FILE-3
            //* *  MOVE BY NAME ROUTER-EX-FILE TO RECORD-ID-EX-FILE-3     /* TNGSUB2
            //*  TNGSUB2
                                                                                                                                                                          //Natural: PERFORM MOVE-ALL-FIELDS
            sub_Move_All_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAppl1151.getRouter_Ex_File_Rt_Short_Name().notEquals(" ")))                                                                                  //Natural: IF ROUTER-EX-FILE.RT-SHORT-NAME NE ' '
            {
                //*     RESET ROUTER-EX-FILE-3.RT-INST-NAME
                ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Short_Name());                                                //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-NAME := ROUTER-EX-FILE.RT-SHORT-NAME
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl1158.getRecord_Id_Ex_File_3_Hd_Record_Type().setValue("01");                                                                                           //Natural: ASSIGN HD-RECORD-TYPE := '01'
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PROFILE
            sub_Lookup_Profile();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CONVERT-LOB-TO-PRODUCT
            sub_Convert_Lob_To_Product();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   TNGSUB
            if (condition(ldaAppl1151.getRouter_Ex_File_Rt_Substitution_Contract_Ind().notEquals(" ")))                                                                   //Natural: IF ROUTER-EX-FILE.RT-SUBSTITUTION-CONTRACT-IND NE ' '
            {
                ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bundle_Type().setValue("I");                                                                                        //Natural: ASSIGN RT-BUNDLE-TYPE = 'I'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bundle_Type().setValue("R");                                                                                        //Natural: ASSIGN RT-BUNDLE-TYPE = 'R'
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, ldaAppl1158.getRecord_Id_Ex_File_3());                                                                                         //Natural: WRITE WORK FILE 2 RECORD-ID-EX-FILE-3
            pnd_Written.nadd(1);                                                                                                                                          //Natural: ASSIGN #WRITTEN := #WRITTEN + 1
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Read.equals(getZero())))                                                                                                                        //Natural: IF #READ = 0
        {
            getReports().display(0, "******** EXTRACT FILE IS EMPTY ********");                                                                                           //Natural: DISPLAY '******** EXTRACT FILE IS EMPTY ********'
            if (Global.isEscape()) return;
            DbsUtil.terminate(1);  if (true) return;                                                                                                                      //Natural: TERMINATE 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "ROUTER RECORDS READ:    ",pnd_Read,"HEADER RECORDS WRITTEN: ",pnd_Written);                                                            //Natural: WRITE 'ROUTER RECORDS READ:    ' #READ 'HEADER RECORDS WRITTEN: ' #WRITTEN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-PROFILE
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-LOB-TO-PRODUCT
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-PRODUCT
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-ALL-FIELDS
        //* ********************************
        //*  START MTSIN
        //*  END MTSIN
        //*  START ACCRC
        //*  WRITE ' TESTING ONLY - ' REMOVE THIS WRITE STATEMENT 08/30/19
        //*   / '='ROUTER-EX-FILE.RT-TIAA-INIT-PREM
        //*    '='RECORD-ID-EX-FILE-3.RT-TIAA-INIT-PREM
        //*   / '='ROUTER-EX-FILE.RT-CREF-INIT-PREM
        //*    '='RECORD-ID-EX-FILE-3.RT-CREF-INIT-PREM
        //*   / '='ROUTER-EX-FILE.RT-JOB-NAME
        //*  END  ONEIRA2
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Lookup_Profile() throws Exception                                                                                                                    //Natural: LOOKUP-PROFILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  EASEENR
        //*  EASEENR
        //*  EASEENR
        if (condition(ldaAppl1151.getRouter_Ex_File_Rt_Enroll_Request_Type().equals("M") && ldaAppl1151.getRouter_Ex_File_Rt_Reprint_Request_Type().notEquals("R")))      //Natural: IF ROUTER-EX-FILE.RT-ENROLL-REQUEST-TYPE = 'M' AND ROUTER-EX-FILE.RT-REPRINT-REQUEST-TYPE NE 'R'
        {
            ldaAppl1158.getRecord_Id_Ex_File_3_Hd_Output_Profile().setValue("50");                                                                                        //Natural: ASSIGN HD-OUTPUT-PROFILE := '50'
            //*  EASEENR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Profile_Key.reset();                                                                                                                                      //Natural: RESET #PROFILE-KEY #FOUND
            pnd_Found.reset();
            pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind.setValue("Y");                                                                                                         //Natural: ASSIGN #PJ-ENTRY-ACTVE-IND := 'Y'
            pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr.setValue(100);                                                                                                      //Natural: ASSIGN #PJ-ENTRY-TABLE-ID-NBR := 100
            pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id.setValue("PJ");                                                                                                     //Natural: ASSIGN #PJ-ENTRY-TABLE-SUB-ID := 'PJ'
            pnd_Profile_Key_Pnd_Pj_Job_Name.setValue(ldaAppl1151.getRouter_Ex_File_Rt_Job_Name());                                                                        //Natural: ASSIGN #PJ-JOB-NAME := ROUTER-EX-FILE.RT-JOB-NAME
            vw_table_Entry.startDatabaseRead                                                                                                                              //Natural: READ TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE FROM #PROFILE-KEY
            (
            "READ02",
            new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Profile_Key, WcType.BY) },
            new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
            );
            READ02:
            while (condition(vw_table_Entry.readNextRow("READ02")))
            {
                if (condition(table_Entry_Entry_Actve_Ind.notEquals(pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind) || table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr)  //Natural: IF ENTRY-ACTVE-IND NE #PJ-ENTRY-ACTVE-IND OR ENTRY-TABLE-ID-NBR NE #PJ-ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #PJ-ENTRY-TABLE-SUB-ID OR ENTRY-JOB-NAME NE #PJ-JOB-NAME
                    || table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id) || table_Entry_Entry_Job_Name.notEquals(pnd_Profile_Key_Pnd_Pj_Job_Name)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Found.setValue(true);                                                                                                                                 //Natural: ASSIGN #FOUND := TRUE
                ldaAppl1158.getRecord_Id_Ex_File_3_Hd_Output_Profile().setValue(table_Entry_Profile);                                                                     //Natural: ASSIGN HD-OUTPUT-PROFILE := PROFILE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(! (pnd_Found.getBoolean())))                                                                                                                    //Natural: IF NOT #FOUND
            {
                getReports().write(0, ReportOption.NOHDR,"JOB NAME: ",pnd_Profile_Key_Pnd_Pj_Job_Name,"NOT FOUND ON TABLE 100 PJ");                                       //Natural: WRITE NOHDR 'JOB NAME: ' #PJ-JOB-NAME 'NOT FOUND ON TABLE 100 PJ'
                if (Global.isEscape()) return;
                DbsUtil.terminate(8);  if (true) return;                                                                                                                  //Natural: TERMINATE 8
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  LOOKUP-PROFILE
    }
    private void sub_Convert_Lob_To_Product() throws Exception                                                                                                            //Natural: CONVERT-LOB-TO-PRODUCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Contract_Type.reset();                                                                                                                                        //Natural: RESET #CONTRACT-TYPE
        pnd_W_Lob.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Lob(), ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Lob_Type())); //Natural: COMPRESS RECORD-ID-EX-FILE-3.RT-LOB RECORD-ID-EX-FILE-3.RT-LOB-TYPE INTO #W-LOB LEAVING NO SPACE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #LOB-TABLE-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Lob_Table_Cnt)); pnd_I.nadd(1))
        {
            if (condition(pnd_W_Lob.equals(pnd_Lob_Table_X_Pnd_Lob.getValue(pnd_I))))                                                                                     //Natural: IF #W-LOB = #LOB ( #I )
            {
                pnd_Contract_Type.setValue(pnd_Lob_Table_X_Pnd_Product.getValue(pnd_I));                                                                                  //Natural: ASSIGN #CONTRACT-TYPE := #PRODUCT ( #I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PRODUCT
        sub_Lookup_Product();
        if (condition(Global.isEscape())) {return;}
        ldaAppl1158.getRecord_Id_Ex_File_3_Hd_Product_Type().setValue(pnd_Contract_Type);                                                                                 //Natural: ASSIGN HD-PRODUCT-TYPE := #CONTRACT-TYPE
    }
    private void sub_Lookup_Product() throws Exception                                                                                                                    //Natural: LOOKUP-PRODUCT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  OVERRIDE #CONTRACT-TYPE WITH TABLE VALUE FOR SUNY, NJ ABP, ATRA
        //*  PLUS ANY OTHERS ADDED TO THE TABLE LATER
        pnd_Product_Key.reset();                                                                                                                                          //Natural: RESET #PRODUCT-KEY
        pnd_Product_Key_Pnd_Pc_Entry_Actve_Ind.setValue("Y");                                                                                                             //Natural: ASSIGN #PC-ENTRY-ACTVE-IND := 'Y'
        pnd_Product_Key_Pnd_Pc_Entry_Table_Id_Nbr.setValue(100);                                                                                                          //Natural: ASSIGN #PC-ENTRY-TABLE-ID-NBR := 100
        pnd_Product_Key_Pnd_Pc_Entry_Table_Sub_Id.setValue("PC");                                                                                                         //Natural: ASSIGN #PC-ENTRY-TABLE-SUB-ID := 'PC'
        pnd_Product_Key_Pnd_Pc_Lob.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Lob(), ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Lob_Type())); //Natural: COMPRESS RECORD-ID-EX-FILE-3.RT-LOB RECORD-ID-EX-FILE-3.RT-LOB-TYPE INTO #PC-LOB LEAVING NO SPACE
        if (condition(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Code().equals("SGRD")))                                                                                  //Natural: IF RECORD-ID-EX-FILE-3.RT-INST-CODE = 'SGRD'
        {
            pnd_Product_Key_Pnd_Pc_Type.setValue("PLAN");                                                                                                                 //Natural: ASSIGN #PC-TYPE := 'PLAN'
            pnd_Product_Key_Pnd_Pc_Value.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Sg_Plan_No());                                                                    //Natural: ASSIGN #PC-VALUE := RECORD-ID-EX-FILE-3.RT-SG-PLAN-NO
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Product_Key_Pnd_Pc_Type.setValue("PPG");                                                                                                                  //Natural: ASSIGN #PC-TYPE := 'PPG'
            pnd_Product_Key_Pnd_Pc_Value.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Code());                                                                     //Natural: ASSIGN #PC-VALUE := RECORD-ID-EX-FILE-3.RT-INST-CODE
        }                                                                                                                                                                 //Natural: END-IF
        vw_table_Entry.startDatabaseRead                                                                                                                                  //Natural: READ TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE FROM #PRODUCT-KEY
        (
        "READ03",
        new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Product_Key, WcType.BY) },
        new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ03:
        while (condition(vw_table_Entry.readNextRow("READ03")))
        {
            if (condition(table_Entry_Entry_Actve_Ind.notEquals(pnd_Product_Key_Pnd_Pc_Entry_Actve_Ind) || table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Product_Key_Pnd_Pc_Entry_Table_Id_Nbr)  //Natural: IF ENTRY-ACTVE-IND NE #PC-ENTRY-ACTVE-IND OR ENTRY-TABLE-ID-NBR NE #PC-ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #PC-ENTRY-TABLE-SUB-ID OR ENTRY-LOB NE #PC-LOB OR ENTRY-TYPE NE #PC-TYPE OR ENTRY-VALUE NE #PC-VALUE
                || table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Product_Key_Pnd_Pc_Entry_Table_Sub_Id) || table_Entry_Entry_Lob.notEquals(pnd_Product_Key_Pnd_Pc_Lob) 
                || table_Entry_Entry_Type.notEquals(pnd_Product_Key_Pnd_Pc_Type) || table_Entry_Entry_Value.notEquals(pnd_Product_Key_Pnd_Pc_Value)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Type.setValue(table_Entry_Product);                                                                                                              //Natural: ASSIGN #CONTRACT-TYPE := PRODUCT
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  LOOKUP-PRODUCT
    }
    //*  IISG
    //*  IISG
    private void sub_Move_All_Fields() throws Exception                                                                                                                   //Natural: MOVE-ALL-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Type_Cd().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Type_Cd());                                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TYPE-CD := ROUTER-EX-FILE.RT-TYPE-CD
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_No_1().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_No_1());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-NO-1 := ROUTER-EX-FILE.RT-TIAA-NO-1
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_No_1().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_No_1());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-NO-1 := ROUTER-EX-FILE.RT-CREF-NO-1
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_No_2().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_No_2());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-NO-2 := ROUTER-EX-FILE.RT-TIAA-NO-2
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_No_2().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_No_2());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-NO-2 := ROUTER-EX-FILE.RT-CREF-NO-2
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_No_3().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_No_3());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-NO-3 := ROUTER-EX-FILE.RT-TIAA-NO-3
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_No_3().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_No_3());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-NO-3 := ROUTER-EX-FILE.RT-CREF-NO-3
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_No_4().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_No_4());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-NO-4 := ROUTER-EX-FILE.RT-TIAA-NO-4
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_No_4().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_No_4());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-NO-4 := ROUTER-EX-FILE.RT-CREF-NO-4
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Date_Table().getValue(1,":",6).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Date_Table().getValue(1,":",6));                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-DATE-TABLE ( 1:6 ) := ROUTER-EX-FILE.RT-DATE-TABLE ( 1:6 )
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Age_First_Pymt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Age_First_Pymt());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGE-FIRST-PYMT := ROUTER-EX-FILE.RT-AGE-FIRST-PYMT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Ownership().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Ownership());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-OWNERSHIP := ROUTER-EX-FILE.RT-OWNERSHIP
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Alloc_Disc().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Alloc_Disc());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ALLOC-DISC := ROUTER-EX-FILE.RT-ALLOC-DISC
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Currency().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Currency());                                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CURRENCY := ROUTER-EX-FILE.RT-CURRENCY
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Extr_Form().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_Extr_Form());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-EXTR-FORM := ROUTER-EX-FILE.RT-TIAA-EXTR-FORM
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_Extr_Form().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_Extr_Form());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-EXTR-FORM := ROUTER-EX-FILE.RT-CREF-EXTR-FORM
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Extr_Ed().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_Extr_Ed());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-EXTR-ED := ROUTER-EX-FILE.RT-TIAA-EXTR-ED
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_Extr_Ed().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_Extr_Ed());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-EXTR-ED := ROUTER-EX-FILE.RT-CREF-EXTR-ED
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Annuitant_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Annuitant_Name());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ANNUITANT-NAME := ROUTER-EX-FILE.RT-ANNUITANT-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_Last_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_Last_Name());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-LAST-NAME := ROUTER-EX-FILE.RT-EOP-LAST-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_First_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_First_Name());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-FIRST-NAME := ROUTER-EX-FILE.RT-EOP-FIRST-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_Middle_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_Middle_Name());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-MIDDLE-NAME := ROUTER-EX-FILE.RT-EOP-MIDDLE-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_Title().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_Title());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-TITLE := ROUTER-EX-FILE.RT-EOP-TITLE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_Prefix().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_Prefix());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-PREFIX := ROUTER-EX-FILE.RT-EOP-PREFIX
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Soc_Sec().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Soc_Sec());                                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SOC-SEC := ROUTER-EX-FILE.RT-SOC-SEC
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Pin_No().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Pin_No());                                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PIN-NO := ROUTER-EX-FILE.RT-PIN-NO
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Address_Table().getValue(1,":",6).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Address_Table().getValue(1,":",                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ADDRESS-TABLE ( 1:6 ) := ROUTER-EX-FILE.RT-ADDRESS-TABLE ( 1:6 )
            6));
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_Zipcode().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_Zipcode());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-ZIPCODE := ROUTER-EX-FILE.RT-EOP-ZIPCODE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Participant_Status().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Participant_Status());                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PARTICIPANT-STATUS := ROUTER-EX-FILE.RT-PARTICIPANT-STATUS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Email_Address().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Email_Address());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EMAIL-ADDRESS := ROUTER-EX-FILE.RT-EMAIL-ADDRESS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Request_Package_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Request_Package_Type());                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-REQUEST-PACKAGE-TYPE := ROUTER-EX-FILE.RT-REQUEST-PACKAGE-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Portfolio_Selected().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Portfolio_Selected());                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PORTFOLIO-SELECTED := ROUTER-EX-FILE.RT-PORTFOLIO-SELECTED
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Divorce_Contract().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Divorce_Contract());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-DIVORCE-CONTRACT := ROUTER-EX-FILE.RT-DIVORCE-CONTRACT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Access_Cd_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Access_Cd_Ind());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ACCESS-CD-IND := ROUTER-EX-FILE.RT-ACCESS-CD-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Address_Change().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Address_Change());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ADDRESS-CHANGE := ROUTER-EX-FILE.RT-ADDRESS-CHANGE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Package_Version().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Package_Version());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PACKAGE-VERSION := ROUTER-EX-FILE.RT-PACKAGE-VERSION
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Unit_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Unit_Name());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-UNIT-NAME := ROUTER-EX-FILE.RT-UNIT-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Print_Package_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Print_Package_Type());                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PRINT-PACKAGE-TYPE := ROUTER-EX-FILE.RT-PRINT-PACKAGE-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Future_Filler().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Future_Filler());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-FUTURE-FILLER := ROUTER-EX-FILE.RT-FUTURE-FILLER
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bene_Estate().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Bene_Estate());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-BENE-ESTATE := ROUTER-EX-FILE.RT-BENE-ESTATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bene_Trust().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Bene_Trust());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-BENE-TRUST := ROUTER-EX-FILE.RT-BENE-TRUST
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bene_Category().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Bene_Category());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-BENE-CATEGORY := ROUTER-EX-FILE.RT-BENE-CATEGORY
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Sex().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Sex());                                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SEX := ROUTER-EX-FILE.RT-SEX
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Name_Table().getValue(1,":",10).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Name_Table().getValue(1,                //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-NAME-TABLE ( 1:10 ) := ROUTER-EX-FILE.RT-INST-NAME-TABLE ( 1:10 )
            ":",10));
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Name());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-NAME := ROUTER-EX-FILE.RT-INST-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Address_Table().getValue(1,":",6).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Address_Table().getValue(1,           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-ADDRESS-TABLE ( 1:6 ) := ROUTER-EX-FILE.RT-INST-ADDRESS-TABLE ( 1:6 )
            ":",6));
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Zip_Cd().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Zip_Cd());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-ZIP-CD := ROUTER-EX-FILE.RT-INST-ZIP-CD
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mail_Instructions().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mail_Instructions());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MAIL-INSTRUCTIONS := ROUTER-EX-FILE.RT-MAIL-INSTRUCTIONS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Orig_Iss_State().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Orig_Iss_State());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ORIG-ISS-STATE := ROUTER-EX-FILE.RT-ORIG-ISS-STATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Current_Iss_State().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Current_Iss_State());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CURRENT-ISS-STATE := ROUTER-EX-FILE.RT-CURRENT-ISS-STATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_State_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_State_Name());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-STATE-NAME := ROUTER-EX-FILE.RT-EOP-STATE-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Lob().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Lob());                                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-LOB := ROUTER-EX-FILE.RT-LOB
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Lob_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Lob_Type());                                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-LOB-TYPE := ROUTER-EX-FILE.RT-LOB-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Code().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Code());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-CODE := ROUTER-EX-FILE.RT-INST-CODE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Bill_Cd().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Bill_Cd());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-BILL-CD := ROUTER-EX-FILE.RT-INST-BILL-CD
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Permit_Trans().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Permit_Trans());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-PERMIT-TRANS := ROUTER-EX-FILE.RT-INST-PERMIT-TRANS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Perm_Gra_Trans().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Perm_Gra_Trans());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PERM-GRA-TRANS := ROUTER-EX-FILE.RT-PERM-GRA-TRANS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Full_Immed_Vest().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Full_Immed_Vest());                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-FULL-IMMED-VEST := ROUTER-EX-FILE.RT-INST-FULL-IMMED-VEST
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Cashable_Ra().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Cashable_Ra());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-CASHABLE-RA := ROUTER-EX-FILE.RT-INST-CASHABLE-RA
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Cashable_Gra().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Cashable_Gra());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-CASHABLE-GRA := ROUTER-EX-FILE.RT-INST-CASHABLE-GRA
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Fixed_Per_Opt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Fixed_Per_Opt());                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-FIXED-PER-OPT := ROUTER-EX-FILE.RT-INST-FIXED-PER-OPT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Inst_Cntrl_Consent().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Inst_Cntrl_Consent());                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-INST-CNTRL-CONSENT := ROUTER-EX-FILE.RT-INST-CNTRL-CONSENT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop());                                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP := ROUTER-EX-FILE.RT-EOP
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Gsra_Inst_St_Code().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Gsra_Inst_St_Code());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-GSRA-INST-ST-CODE := ROUTER-EX-FILE.RT-GSRA-INST-ST-CODE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Region_Cd().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Region_Cd());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-REGION-CD := ROUTER-EX-FILE.RT-REGION-CD
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Staff_Id().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Staff_Id());                                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-STAFF-ID := ROUTER-EX-FILE.RT-STAFF-ID
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Appl_Source().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Appl_Source());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-APPL-SOURCE := ROUTER-EX-FILE.RT-APPL-SOURCE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_Appl_Status().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_Appl_Status());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-APPL-STATUS := ROUTER-EX-FILE.RT-EOP-APPL-STATUS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Eop_Addl_Cref_Req().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Eop_Addl_Cref_Req());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EOP-ADDL-CREF-REQ := ROUTER-EX-FILE.RT-EOP-ADDL-CREF-REQ
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Extract_Date().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Extract_Date());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EXTRACT-DATE := ROUTER-EX-FILE.RT-EXTRACT-DATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Gsra_Loan_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Gsra_Loan_Ind());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-GSRA-LOAN-IND := ROUTER-EX-FILE.RT-GSRA-LOAN-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Spec_Ppg_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Spec_Ppg_Ind());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SPEC-PPG-IND := ROUTER-EX-FILE.RT-SPEC-PPG-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mail_Pull_Cd().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mail_Pull_Cd());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MAIL-PULL-CD := ROUTER-EX-FILE.RT-MAIL-PULL-CD
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mit_Log_Dt_Tm().getValue(1,":",5).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mit_Log_Dt_Tm().getValue(1,":",                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MIT-LOG-DT-TM ( 1:5 ) := ROUTER-EX-FILE.RT-MIT-LOG-DT-TM ( 1:5 )
            5));
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Addr_Page_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Addr_Page_Name());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ADDR-PAGE-NAME := ROUTER-EX-FILE.RT-ADDR-PAGE-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Welcome_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Welcome_Name());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-WELCOME-NAME := ROUTER-EX-FILE.RT-WELCOME-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Pin_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Pin_Name());                                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PIN-NAME := ROUTER-EX-FILE.RT-PIN-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Zip().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Zip());                                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ZIP := ROUTER-EX-FILE.RT-ZIP
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Suspension().getValue(1,":",20).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Suspension().getValue(1,":",20));                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SUSPENSION ( 1:20 ) := ROUTER-EX-FILE.RT-SUSPENSION ( 1:20 )
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mult_App_Version().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mult_App_Version());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MULT-APP-VERSION := ROUTER-EX-FILE.RT-MULT-APP-VERSION
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mult_App_Status().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mult_App_Status());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MULT-APP-STATUS := ROUTER-EX-FILE.RT-MULT-APP-STATUS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mult_App_Lob().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mult_App_Lob());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MULT-APP-LOB := ROUTER-EX-FILE.RT-MULT-APP-LOB
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mult_App_Lob_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mult_App_Lob_Type());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MULT-APP-LOB-TYPE := ROUTER-EX-FILE.RT-MULT-APP-LOB-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mult_App_Ppg().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mult_App_Ppg());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MULT-APP-PPG := ROUTER-EX-FILE.RT-MULT-APP-PPG
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_K12_Ppg().setValue(ldaAppl1151.getRouter_Ex_File_Rt_K12_Ppg());                                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-K12-PPG := ROUTER-EX-FILE.RT-K12-PPG
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Ira_Rollover_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Ira_Rollover_Type());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-IRA-ROLLOVER-TYPE := ROUTER-EX-FILE.RT-IRA-ROLLOVER-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Ira_Record_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Ira_Record_Type());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-IRA-RECORD-TYPE := ROUTER-EX-FILE.RT-IRA-RECORD-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Rollover_Amt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Rollover_Amt());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ROLLOVER-AMT := ROUTER-EX-FILE.RT-ROLLOVER-AMT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mit_Unit().getValue(1,":",5).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mit_Unit().getValue(1,":",5));                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MIT-UNIT ( 1:5 ) := ROUTER-EX-FILE.RT-MIT-UNIT ( 1:5 )
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mit_Wpid().getValue(1,":",5).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mit_Wpid().getValue(1,":",5));                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MIT-WPID ( 1:5 ) := ROUTER-EX-FILE.RT-MIT-WPID ( 1:5 )
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_New_Bene_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_New_Bene_Ind());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-NEW-BENE-IND := ROUTER-EX-FILE.RT-NEW-BENE-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Irc_Sectn_Cde().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Irc_Sectn_Cde());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-IRC-SECTN-CDE := ROUTER-EX-FILE.RT-IRC-SECTN-CDE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Released_Dt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Released_Dt());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-RELEASED-DT := ROUTER-EX-FILE.RT-RELEASED-DT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Irc_Sectn_Grp().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Irc_Sectn_Grp());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-IRC-SECTN-GRP := ROUTER-EX-FILE.RT-IRC-SECTN-GRP
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Enroll_Request_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Enroll_Request_Type());                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ENROLL-REQUEST-TYPE := ROUTER-EX-FILE.RT-ENROLL-REQUEST-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Erisa_Inst().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Erisa_Inst());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ERISA-INST := ROUTER-EX-FILE.RT-ERISA-INST
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Reprint_Request_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Reprint_Request_Type());                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-REPRINT-REQUEST-TYPE := ROUTER-EX-FILE.RT-REPRINT-REQUEST-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Print_Destination().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Print_Destination());                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PRINT-DESTINATION := ROUTER-EX-FILE.RT-PRINT-DESTINATION
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Correction_Type().getValue(1,":",20).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Correction_Type().getValue(1,                //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CORRECTION-TYPE ( 1:20 ) := ROUTER-EX-FILE.RT-CORRECTION-TYPE ( 1:20 )
            ":",20));
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Processor_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Processor_Name());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PROCESSOR-NAME := ROUTER-EX-FILE.RT-PROCESSOR-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Mail_Date().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Mail_Date());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MAIL-DATE := ROUTER-EX-FILE.RT-MAIL-DATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Alloc_Fmt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Alloc_Fmt());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ALLOC-FMT := ROUTER-EX-FILE.RT-ALLOC-FMT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Phone_No().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Phone_No());                                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PHONE-NO := ROUTER-EX-FILE.RT-PHONE-NO
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Fund_Source_1().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Fund_Source_1());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-FUND-SOURCE-1 := ROUTER-EX-FILE.RT-FUND-SOURCE-1
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Fund_Cde().getValue(1,":",100).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Fund_Cde().getValue(1,":",100));                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-FUND-CDE ( 1:100 ) := ROUTER-EX-FILE.RT-FUND-CDE ( 1:100 )
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Alloc_Pct().getValue(1,":",100).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Alloc_Pct().getValue(1,":",100));                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ALLOC-PCT ( 1:100 ) := ROUTER-EX-FILE.RT-ALLOC-PCT ( 1:100 )
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Oia_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Oia_Ind());                                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-OIA-IND := ROUTER-EX-FILE.RT-OIA-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Product_Cde().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Product_Cde());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PRODUCT-CDE := ROUTER-EX-FILE.RT-PRODUCT-CDE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Acct_Sum_Sheet_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Acct_Sum_Sheet_Type());                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ACCT-SUM-SHEET-TYPE := ROUTER-EX-FILE.RT-ACCT-SUM-SHEET-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Sg_Plan_No().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Sg_Plan_No());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SG-PLAN-NO := ROUTER-EX-FILE.RT-SG-PLAN-NO
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Sg_Subplan_No().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Sg_Subplan_No());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SG-SUBPLAN-NO := ROUTER-EX-FILE.RT-SG-SUBPLAN-NO
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Sg_Subplan_Type().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Sg_Subplan_Type());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SG-SUBPLAN-TYPE := ROUTER-EX-FILE.RT-SG-SUBPLAN-TYPE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Omni_Issue_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Omni_Issue_Ind());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-OMNI-ISSUE-IND := ROUTER-EX-FILE.RT-OMNI-ISSUE-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Effective_Dt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Effective_Dt());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EFFECTIVE-DT := ROUTER-EX-FILE.RT-EFFECTIVE-DT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Employer_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Employer_Name());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-EMPLOYER-NAME := ROUTER-EX-FILE.RT-EMPLOYER-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Rate().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_Rate());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-RATE := ROUTER-EX-FILE.RT-TIAA-RATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Sg_Plan_Id().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Sg_Plan_Id());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SG-PLAN-ID := ROUTER-EX-FILE.RT-SG-PLAN-ID
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Pg4_Numb().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_Pg4_Numb());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-PG4-NUMB := ROUTER-EX-FILE.RT-TIAA-PG4-NUMB
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_Pg4_Numb().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_Pg4_Numb());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-PG4-NUMB := ROUTER-EX-FILE.RT-CREF-PG4-NUMB
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Dflt_Access_Code().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Dflt_Access_Code());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-DFLT-ACCESS-CODE := ROUTER-EX-FILE.RT-DFLT-ACCESS-CODE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Single_Issue_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Single_Issue_Ind());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SINGLE-ISSUE-IND := ROUTER-EX-FILE.RT-SINGLE-ISSUE-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Spec_Fund_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Spec_Fund_Ind());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SPEC-FUND-IND := ROUTER-EX-FILE.RT-SPEC-FUND-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Product_Price_Level().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Product_Price_Level());                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PRODUCT-PRICE-LEVEL := ROUTER-EX-FILE.RT-PRODUCT-PRICE-LEVEL
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Roth_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Roth_Ind());                                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ROTH-IND := ROUTER-EX-FILE.RT-ROTH-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Plan_Issue_State().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Plan_Issue_State());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PLAN-ISSUE-STATE := ROUTER-EX-FILE.RT-PLAN-ISSUE-STATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Client_Id().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Client_Id());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CLIENT-ID := ROUTER-EX-FILE.RT-CLIENT-ID
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Portfolio_Model().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Portfolio_Model());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-PORTFOLIO-MODEL := ROUTER-EX-FILE.RT-PORTFOLIO-MODEL
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Auto_Enroll_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Auto_Enroll_Ind());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AUTO-ENROLL-IND := ROUTER-EX-FILE.RT-AUTO-ENROLL-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Auto_Save_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Auto_Save_Ind());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AUTO-SAVE-IND := ROUTER-EX-FILE.RT-AUTO-SAVE-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_As_Cur_Dflt_Amt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_As_Cur_Dflt_Amt());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AS-CUR-DFLT-AMT := ROUTER-EX-FILE.RT-AS-CUR-DFLT-AMT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_As_Incr_Amt().setValue(ldaAppl1151.getRouter_Ex_File_Rt_As_Incr_Amt());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AS-INCR-AMT := ROUTER-EX-FILE.RT-AS-INCR-AMT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_As_Max_Pct().setValue(ldaAppl1151.getRouter_Ex_File_Rt_As_Max_Pct());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AS-MAX-PCT := ROUTER-EX-FILE.RT-AS-MAX-PCT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Ae_Opt_Out_Days().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Ae_Opt_Out_Days());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AE-OPT-OUT-DAYS := ROUTER-EX-FILE.RT-AE-OPT-OUT-DAYS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tsv_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tsv_Ind());                                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TSV-IND := ROUTER-EX-FILE.RT-TSV-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Indx_Guarntd_Rte().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_Indx_Guarntd_Rte());                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-INDX-GUARNTD-RTE := ROUTER-EX-FILE.RT-TIAA-INDX-GUARNTD-RTE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Crd_No().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Crd_No());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-CRD-NO := ROUTER-EX-FILE.RT-AGENT-CRD-NO
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Crd_Pull_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Crd_Pull_Ind());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CRD-PULL-IND := ROUTER-EX-FILE.RT-CRD-PULL-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Name());                                                       //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-NAME := ROUTER-EX-FILE.RT-AGENT-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Work_Addr1().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Work_Addr1());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-WORK-ADDR1 := ROUTER-EX-FILE.RT-AGENT-WORK-ADDR1
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Work_Addr2().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Work_Addr2());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-WORK-ADDR2 := ROUTER-EX-FILE.RT-AGENT-WORK-ADDR2
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Work_City().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Work_City());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-WORK-CITY := ROUTER-EX-FILE.RT-AGENT-WORK-CITY
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Work_State().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Work_State());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-WORK-STATE := ROUTER-EX-FILE.RT-AGENT-WORK-STATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Work_Zip().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Work_Zip());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-WORK-ZIP := ROUTER-EX-FILE.RT-AGENT-WORK-ZIP
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Agent_Work_Phone().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Agent_Work_Phone());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-AGENT-WORK-PHONE := ROUTER-EX-FILE.RT-AGENT-WORK-PHONE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Ind());                                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-IND := ROUTER-EX-FILE.RT-TIC-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Startdate().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Startdate());                                                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-STARTDATE := ROUTER-EX-FILE.RT-TIC-STARTDATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Enddate().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Enddate());                                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-ENDDATE := ROUTER-EX-FILE.RT-TIC-ENDDATE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Percentage().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Percentage());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-PERCENTAGE := ROUTER-EX-FILE.RT-TIC-PERCENTAGE
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Postdays().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Postdays());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-POSTDAYS := ROUTER-EX-FILE.RT-TIC-POSTDAYS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Limit().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Limit());                                                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-LIMIT := ROUTER-EX-FILE.RT-TIC-LIMIT
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Postfreq().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Postfreq());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-POSTFREQ := ROUTER-EX-FILE.RT-TIC-POSTFREQ
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Pl_Level().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Pl_Level());                                                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-PL-LEVEL := ROUTER-EX-FILE.RT-TIC-PL-LEVEL
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Windowdays().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Windowdays());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-WINDOWDAYS := ROUTER-EX-FILE.RT-TIC-WINDOWDAYS
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Reqdlywindow().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Reqdlywindow());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-REQDLYWINDOW := ROUTER-EX-FILE.RT-TIC-REQDLYWINDOW
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tic_Recap_Prov().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tic_Recap_Prov());                                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIC-RECAP-PROV := ROUTER-EX-FILE.RT-TIC-RECAP-PROV
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Ecs_Dcs_E_Dlvry_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Ecs_Dcs_E_Dlvry_Ind());                                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ECS-DCS-E-DLVRY-IND := ROUTER-EX-FILE.RT-ECS-DCS-E-DLVRY-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Ecs_Dcs_Annty_Optn_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Ecs_Dcs_Annty_Optn_Ind());                               //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ECS-DCS-ANNTY-OPTN-IND := ROUTER-EX-FILE.RT-ECS-DCS-ANNTY-OPTN-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Full_Middle_Name().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Full_Middle_Name());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-FULL-MIDDLE-NAME := ROUTER-EX-FILE.RT-FULL-MIDDLE-NAME
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Substitution_Contract_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Substitution_Contract_Ind());                         //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-SUBSTITUTION-CONTRACT-IND := ROUTER-EX-FILE.RT-SUBSTITUTION-CONTRACT-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Non_Proprietary_Pkg_Ind().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Non_Proprietary_Pkg_Ind());                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-NON-PROPRIETARY-PKG-IND := ROUTER-EX-FILE.RT-NON-PROPRIETARY-PKG-IND
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Multi_Plan_No().getValue("*").setValue(ldaAppl1151.getRouter_Ex_File_Rt_Multi_Plan_No().getValue("*"));                     //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MULTI-PLAN-NO ( * ) := ROUTER-EX-FILE.RT-MULTI-PLAN-NO ( * )
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Multi_Sub_Plan().getValue("*").setValue(ldaAppl1151.getRouter_Ex_File_Rt_Multi_Sub_Plan().getValue("*"));                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-MULTI-SUB-PLAN ( * ) := ROUTER-EX-FILE.RT-MULTI-SUB-PLAN ( * )
        //*  END ACCRC
        //*  START ONEIRA2 FOR TIAA/CREF INITIAL PREMIUM
        if (condition(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_Init_Prem().greater(getZero())))                                                                              //Natural: IF ROUTER-EX-FILE.RT-TIAA-INIT-PREM > 0
        {
            ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Init_Prem().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Tiaa_Init_Prem());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-TIAA-INIT-PREM := ROUTER-EX-FILE.RT-TIAA-INIT-PREM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Init_Prem().setValue(0);                                                                                           //Natural: MOVE 0.00 TO RECORD-ID-EX-FILE-3.RT-TIAA-INIT-PREM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl1151.getRouter_Ex_File_Rt_Cref_Init_Prem().greater(getZero())))                                                                              //Natural: IF ROUTER-EX-FILE.RT-CREF-INIT-PREM > 0
        {
            ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_Init_Prem().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Cref_Init_Prem());                                           //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-CREF-INIT-PREM := ROUTER-EX-FILE.RT-CREF-INIT-PREM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Cref_Init_Prem().setValue(0);                                                                                           //Natural: MOVE 0.00 TO RECORD-ID-EX-FILE-3.RT-CREF-INIT-PREM
            //*  CONTRSTRG
            //*  CONTRSTRG
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Related_Contract_Info().getValue(1,":",30).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Related_Contract_Info().getValue(1,    //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-RELATED-CONTRACT-INFO ( 1:30 ) := ROUTER-EX-FILE.RT-RELATED-CONTRACT-INFO ( 1:30 )
            ":",30));
        //*  IISG >>>
        if (condition(ldaAppl1151.getRouter_Ex_File_Rt_Fund_Source_2().notEquals(" ")))                                                                                   //Natural: IF ROUTER-EX-FILE.RT-FUND-SOURCE-2 <> ' '
        {
            ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Fund_Source_2().setValue(ldaAppl1151.getRouter_Ex_File_Rt_Fund_Source_2());                                             //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-FUND-SOURCE-2 := ROUTER-EX-FILE.RT-FUND-SOURCE-2
            ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Fund_Cde_2().getValue(1,":",100).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Fund_Cde_2().getValue(1,":",                 //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-FUND-CDE-2 ( 1:100 ) := ROUTER-EX-FILE.RT-FUND-CDE-2 ( 1:100 )
                100));
            ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Alloc_Pct_2().getValue(1,":",100).setValue(ldaAppl1151.getRouter_Ex_File_Rt_Alloc_Pct_2().getValue(1,                   //Natural: ASSIGN RECORD-ID-EX-FILE-3.RT-ALLOC-PCT-2 ( 1:100 ) := ROUTER-EX-FILE.RT-ALLOC-PCT-2 ( 1:100 )
                ":",100));
            //*  IISG  <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  RECORD-ID-EX-FILE-3.RT-FILLER      := ROUTER-EX-FILE.RT-FILLER
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, ReportOption.NOHDR,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",                //Natural: WRITE NOHDR 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE /
            Global.getERROR_LINE(),NEWLINE);
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, "******** EXTRACT FILE IS EMPTY ********");
    }
}
