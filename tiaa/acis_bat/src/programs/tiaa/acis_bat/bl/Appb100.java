/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:07:24 PM
**        * FROM NATURAL PROGRAM : Appb100
************************************************************
**        * FILE NAME            : Appb100.java
**        * CLASS NAME           : Appb100
**        * INSTANCE NAME        : Appb100
************************************************************
************************************************************************
* COMMENTS ADDED 11/12/2001 K.GATES                                    *
* -------------------------------------------------------------------- *
* NOTE: REMOVED COMMENTED CODE DUE TO SPACE ISSUES AND SAVED ON SHARE  *
*       DRIVE UNDER ACIS PRODUCTION APPB100 COMMENTED CODE.TXT FILE    *
* -------------------------------------------------------------------- *
* PROGRAM  : APPB100 - DOCUMERGE EXTRACT                               *
* FUNCTION : READS PRAP FILE AND EXTRACTS PARTICIPANT CONTRACTS FOR    *
*            PRINTING OF WELCOME AND LEGAL CONTRACT PACKAGES.  ALSO    *
*            WRITES EXTRACTED CONTRACTS TO THE REPRINT FILE.           *
* UPDATED  : 11/12/01 K.GATES   - 457(B) CHANGES SEE 457(B) KG         *
*            01/29/02 K.GATES   - OIA CHANGES                          *
*            07/11/02 L DEDIOS  - MAYO SPECIAL HANDLING SEE MAYO LD    *
*            08/26/02 GATES     - CALLNAT ACIN8550 TO RETRIEVE NEW     *
*                     ELLO        OIA PLAN TYPE INDICATOR FOR OIA      *
*                                 PROSPECTUS CHANGES.                  *
*            01/31/03 GATES     - CHANGES TO SUPRESS WELCOME PACKAGE   *
*                                 FOR ILLINOIS ORP (K560) CONTRACTS    *
*            03/20/03 GATES     - CHANGES TO SUPPORT ACIS VOLUME MGMT. *
*                     ELLO                                             *
*            05/05/03 SINGLETON - ALLOW FOR SPACES TO BE USED IN FIRST *
*                                 AND LAST NAME.                       *
*            05/21/03 J.BAUER   - ADD CODE TO MOVE NEW FIELD FROM      *
*                                 ACIA8550 TO FIELD IN APPL160.        *
*                                 SUPPRESS PRINTING OF LEGAL IF ARIZONA*
*            06/19/03 L DEDIOS  - FOR S/R USE ORIG STATE           LD1 *
*            08/01/03 L DEDIOS  - GRA ISSUE STATE                  LD2 *
*            08/25/03 K GATES   - CODE CHANGES FOR CIP PROJECT  KG-CIP *
*            03/29/04 K GATES   - CHANGES FOR SGRD RELEASE 2   SGRD KG *
*            04/19/04 K.GATES   CHANGES FOR SUNGARD RELEASE 2 SGRD KG  *
*                               - ADDED PLAN NO, SUBPLAN NO, AND       *
*                                 SUBPLAN TYPE TO PROGRAM              *
*                               - EXCLUDED SUNGARD CONTRACTS FROM IIS  *
*                                 CALLS AND ADDED CALL TO COBOL PGM    *
*                                 PSG9030 TO GET PLAN INFORMATION      *
*                                 OR HARDCODED INFO FOR SUNGARD        *
*                               - ADDED EXCEPTION HANDLING AND REPORT  *
*                                 FOR ERRORS RETURNED FROM COBOL PGM   *
*            08/31/04 K.GATES   CHANGES FOR SUNGARD RELEASE 3 SGRD3 KG *
*                               - ADDED OMNI ISSUANCE INDICATOR FOR    *
*                                 DEFAULT ENROLLMENT PROCESSING        *
*                               - REMOVED HARDCODED CONTACT INFO FOR   *
*                                 SUNGARD PLANS AND ADDED CALL TO GET  *
*                                 PLAN CONTACT INFO FROM OMNI/SUNGARD  *
*            10/14/04 R.WILLIS  - COMMENT OUT WRONG FUNCTION CODE
*            12/20/04 K.GATES   CHANGES FOR SUNGARD RELEASE 4 SGRD4 KG *
*                               - ADDED TNT FIELDS: PLAN ID, EMPLOYER  *
*                                 NAME, AND TIAA INTEREST RATE         *
*                               - ADDED TNT PRODUCTS                   *
*                               - CONTRACT SPLIT CHANGES FOR EFFECTIVE *
*                                 DATE AND ISSUE STATE.                *
*            04/05/05 K.GATES   CHANGES FOR SUNGARD RELEASE 5 SGRD5 KG *
*                               - CHANGED STORE TO REPRINT FILE TO     *
*                                 UPDATE IF CONTRACT ALREADY ON FILE   *
*                               - ADDED ICAP PRODUCTS                  *
*                               - SUPPRESSED ICAP LEGAL PACKAGES       *
*                               - CHANGED CONTACT INTERFACE            *
*            04/22/05 K.GATES   RELEASE 5 OIA IND CHANGES    SGRD5A KG *
*            05/03/05 R.WILLIS  REDEFINE AP-BANK-PYMNT-ACCT-NMBR       *
*            06/10/05 K.GATES   ICAP PROCESSING UPDATE FIX   SGRD5B KG *
*            07/20/05 D.MARPURI REMOVED PAC-CONTROL-FILE PROCESS       *
*            08/24/05 K.GATES   CHANGES FOR OMNI RELEASE 6.5  SGRD6 KG *
*                               - REMOVED ICAP LEGAL PKG SUPPRESSION   *
*                               - ADDED IRA FUND INTERFACE             *
*                               - ADDED ICAP/TNT COMPANION PROCESSING  *
*                               - ADDED IRA SEP PRODUCT                *
*            10/12/05 K.GATES   CHANGES FOR OMNI RELEASE 6.5           *
*                               - ADDED COMMENTED CODE BACK IN FOR IRA *
*                                 LEGACY (UNDER SGRD6 KG 10/2005)      *
*            11/07/05 K.GATES   CHANGES FOR OMNI MINI RELEASE SGRD7 KG *
*                               - ADDED GENERIC DEFAULT ENROLL LETTER  *
*                                 PROCESSING                           *
*                               - ADDED SINGLE ISSUE INDICATOR FOR     *
*                                 COMPANION PROCESSING                 *
*                               - ADDED PACKAGE SUPPRESSION TABLE      *
*                                 PROCESSING                           *
*            12/19/05 D.MARPURI - ADD NEW FIELD DIV/SUB TO PRAP - RL8  *
*             1/03/06 K.GATES   - ADD NEW DIV/SUB TO REPRINT  SGRD8 KG *
*             2/17/06 K.GATES   - ADD ILL SMP CONV CHANGES  SGRD8B KG  *
*                               AND LEGAL EXTRACT FILES (JRB1)         *
*            02/28/06 JANET B.  ADD ON ERROR CONDITION (JRB2)          *
*            04/11/06 K.GATES   - ADD SPECIAL FUND IND AND PROCESSING  *
*                                 SEE SGRDSR KG                        *
*            06/23/06 K GATES -   RESTOW FOR SCIA8100 PRODUCTION FIX   *
*            06/06/06 K.GATES   - ADD PRODUCT/PRICING LEVEL IND AND    *
*                                 PROCESSING   SEE SGRDAW KG           *
*            07/20/06 K.GATES   - ADD REPLACEMENT IND, REGISTER ID,    *
*                                 AND EXEMPT IND TO REPRINT FILE       *
*                                 SEE ARR KG                           *
*            08/24/06 K.GATES   - ADD ROTH IND AND PROCESSING          *
*                                 SEE ROTH KG                          *
*            09/25/06 K.GATES   - ADDED DIV/SUB FOR DCA SIEBEL CONTACT *
*                               - ADDED GA5 PACKAGE BLOCK              *
*                                 SEE DCA KG                           *
*            01/05/07 J.CAMPBELL- ADD IGNORE TO SEPARATE TO IGNORE     *
*                                 EXTRA COMMAS.           SCAN JC1     *
*            01/31/07 K.GATES   - REPLACE ACIN8540 WITH SCIN8540 FOR   *
*                                 MAYO CONVERSION TO OMNI - SEE MAYO KG*
*            07/02/07 DEVELBISS - ADD PLAN ISSUE STATE - POPULATED ON  *
*                                 ALL RECORDS - USED FOR STABLE RETURN *
*                                 ON INDIVIDUAL PRODUCTS - SEE SRF BJD *
*            09/18/07 K.GATES   - CHANGE ILL SMP (PLAN 100825) LOGIC   *
*                                 FOR ICAP CONVERSION - SEE ILLSMP KG  *
*            02/28/08 K.GATES   - MAIL VS RESIDENT ISSUE STATE FIX     *
*                                 (ACIS REJECTS) - SEE MAIL-RES FIX KG *
*            04/07/08 K.GATES   - ADDED CLIENT ID AND PORTFOLIO TYPE   *
*                                 (CALSTRS 2008) - SEE CALSTRS KG      *
*            08/07/08 K.GATES   - ADDED RESET OF MAYO LOGICAL IND FOR  *
*                                 PRODUCTION FIX - SEE 8-07-2008 KG    *
*            09/15/08 DEVELBISS - AUTO ENROLL PROJECT - EXTRACT NEW    *
*                                 FIELDS AND ADD TO REPRINT SEE BJD AE.*
*            08/07/08 DEVELBISS - RHSP PROJECT - SET RHSP CONTRACT     *
*                                 TYPE - SUPPRESS RHSP EXTRACT         *
*                                 SEE BJD RHSP                         *
*            05/01/09 BERGHEISER- CREF COMPANION - PASS ALL ZEROES     *
*                                 INSTEAD OF SINGLE ZEROR IN ISSUE     *
*                                 DATE FIELD (JRB2)                    *
*            05/26/09 DEVELBISS - ADD ENROLLMENT SOURCE FIELD TO PS    *
*                                 TABLE LOOKUP (BJD1)                  *
*            05/11/09 BERGHEISER- CREF COMPANION - BLOCK COMPANIONS    *
*                                 FROM PRINTING WELCOME AND BYPASS     *
*                                 14 DAY DELAY (JRB3)                  *
*            08/01/09 B. ELLO     CHANGES FOR ENROLLMENT IMPROVEMENTS  *
*                                 PROJECT. USE ONLY ADDRESS LINES WHEN*
*                                 FORMATTING FOREIGN ADDRESS LINES.    *
*            08/12/09 C. AVE - SET THE VALUES OF WELCOME-MAIL-DATE AND *
*                     LEGAL-MAIL-DATE (CHANGES MARKED BY CA-082009)    *
*            07/14/09 GUERRERO  - RECOMPILE FOR OMNI UPDGRADE PROJECT  *
*                                 NEW VERSION OF SCIA8250              *
*            01/06/10 K.GATES   - ADDED EMORY WELCOME PACKAGE TRIGGER  *
*                                 FOR FUNDING CHANGES - SEE EMORY KG   *
*            02/19/10 K.GATES   - ADDED TSV INDICATOR - SEE TSV KG     *
*            03/08/10 C. AVE - INCLUDED IRA INDEXED PRODUCTS - TIGR    *
*            03/04/11 B. ELLO - FLORIDA ENROLLMENT PROJECT BE2.        *
*            05/04/11 C. SCHNEIDER - ADD E-DELIVERY,ANNTY OPTION INDS, *
*                                  PRINT INDS, AND CONSENT E-MAIL ADDR *
*                                  TO PRAP, REPRINT, AND EXTRACT FILES *
*                                  (JHU)                               *
*            05/25/11 L. SHU    - ADDED INST WELCOME PKG TRIGGER AND   *
*          RE-ASSIGN DX-OWNERSHIP & DX-INST-FULL-IMMED-VEST - INST LS  *
*            08/04/11 G. SUDSATAYA - AP-MAIL-ADDR-COUNTRY-CD AND IF    *
*                                  PRPOPULATED, CONVERT IT TO COUNTRY  *
*                                  NAME AND POPULATE IT IN LINE 4 OF   *
*                                  ADDRESS - CAMPUS PROJECT            *
*            07/13/11 C SCHNEIDER TIC CHANGES (TIC)                    *
*                                1) ADDED FIELDS TO PRAP, REPRINT, AND *
*                                EXTRACT FILES                         *
*                                2) SET VALUE OF DX-TIC-IND            *
*            09/21/11 C SCHNEIDER MOBIUS KEY (CS MOBKEY) CHANGES       *
*            10/17/11 L SHU - TIC CHANGES - ADDED ERROR MESSAGE AND    *
*                             REMOVE CHECKING OF TIC START DATE    LS1 *
*            12/29/11 K GATES - ADDED RESET FOR FIELDS AFTER FIND STMT *
*                             - COMMENTED OUT SETTING E-DELIVERY IND   *
*                               DUE TO DEFERRAL. MOVED E-DELIVERY CODE *
*                               AFTER IRA ESCAPE TOP LOGIC. SEE KG SRK *
*            02/16/12 K.GATES - ADDED CALL TO APPN7900 TO CHECK FOR    *
*                               457(B) PRIV. AND GA5 SUPPRESS          *
*                               EXCEPTIONS - SEE ROCH KG               *
*            03/23/12 K.GATES - ADDED CHANGES TO ONLY PRINT LEGAL PKGS *
*                               FOR ROCHESTER WHEN ROCH LEGAL PKG IND  *
*                               HAS BEEN SET TO Y. - SEE ROCH2 KG      *
*            04/17/12 L.SHU   - ADDED CHECKING FOR FOREIGN ADDRESS     *
*                                                  - SEE SRK LS3       *
*            07/10/12 P.GOLDEN - OVERRIDE ALL 457(B) PRIVATE CONTRACTS *
*                                THAT REQUIRE WELCOME PACKAGES TO HAVE *
*                                AN OWNERSHIP CODE OF PARTICIPANT      *
*                                OWNED - SEE PG1                       *
*            11/17/12 K.GATES - POPULATED ANNUITY ISSUE DATES AFTER    *
*                               FIRST ANNUITY FUNDING FOR LEGAL PACKAGE*
*                               ANNUITY OPTION (LPAO) CONTRACTS. FOR   *
*                               LPAO CONTRACTS, THE ISSUE DATE IS BASED*
*                               ON WHEN ANNUITY FUNDING IS APPLIED,    *
*                               INSTEAD OF THE APPL. RECEIVED DATE.    *
*                             - REMOVED ROCH2 3/23/12 CHGS THAT USED   *
*                               PLANS 100901 AND 100903. - SEE LPAO KG *
*            08/14/12 B.NEWSOM - MOVE NEW FIELDS FROM THE SRK PROJECT TO
*                                THE REPRINT FILE.  COMPRESS AP-CITY
*                                WITH ADDRESS LINE 3 FOR BPEL CASES
*                                WITH CANADIAN ADDRESS.  SEE BN1.
*            03/14/13 B.NEWSOM - THE EXISTING LOGIC TO SUPPRESS ISSUE
*                                PACKAGES FOR NON TIAA-CREF ADMINISTERED
*                                PLANS WILL BE FOLLOWED FOR DATASHARE
*                                CLIENTS.           (DSP4)
*            07/18/13 K.GATES  - LEGACY PROCESSING WAS REMOVED AND THE
*                                PROGRAM RESTRUCTURED. SEE KG TNGSUB
*            07/18/13 K.GATES  - BLOCKED IRA SUBSTITUTION CONTRACTS
*                                FROM BEING EXTRACTED FOR WELCOME
*                              - BYPASS FUNDING REQUIREMENTS FOR IRA
*                                SUBSTITUTION CONTRACTS FOR LEGAL
*                              - BYPASS GETTING FUND LIST FOR IRA
*                                SUBSTITUTION CONTRACTS (SCIN8100)
*                                SEE KG TNGSUB2
*            07.18.13  LAEVEY  - REMOVED OLD CODE AND CALLS TO UNUSED
*                                SUBROUTINES
*                                INCLUDE NEW PRAP FIELDS ON THE REPRINT
*                                FILE.
*                                WHEN SUB CONTRACT, USE CONVERSION
*                                ISSUE STATE IN PLACE OF CURRENT STATE
*            07/18/13 L SHU    - RETRIEVE TRIGGER/SUPPRESS FROM TABLE
*                                ENTRY FILE TO REDUCE I/O   - TNGSUB2 LS
*                              - REMOVE CALLING APP-TABLE-ENTRY TO GET
*                                PRODUCT CODE.              - TNGSUB2 LS
*                              - ALLOW PACKAGE PRINTING FOR DECEASED
*                                PARTICIPANT                  TNGSUB3 LS
*                              - SET DX-SUBTITUTION-CONTRACT-IND SAME AS
*                                IN PRAP FILE                 TNGSUB3
*            07/31/13 B NEWSOM - FIX SUPPRESSION AND STATE CODE LOGIC.
*                                                            CHG289910
*            09/13/13 L SHU    - MOVE NON PROPRIETARY PKG IND TO REPRINT
*                                FILE FOR MT SINAI            MTSIN
*            06/2014 B. NEWSOM
*                      INTRODUCED A NEW PARM FILE (WORKFILE 3)    (CREA)
*                      CONTAINING THE FUND/TICKER INFORMATION     (CREA)
*                      NEEDED TO PROCESS THE RECORDS. THIS ELIMI- (CREA)
*                      NATES THE HARDCODING OF THE TICKER SYMBOLS.(CREA)
*            08/2014 B. NEWSOM
*                      INVESTMENT MENU BY SUBPLAN                 (IMBS)
*                      ADDED STABLE RETURN INDICATOR TO REPRINT   (IMBS)
*            09/2014 B. NEWSOM
*                      ACIS/CIS CREF REDESIGN COMMUNICATIONS     (ACCRC)
*                      RESTOW FOR APPL160 AND APPL180           (ACCRC)
* 04/02/15 B NEWSOM ADDED CALL TO APPN7900 TO SUPPRESS 457 (B) FOR
*                   ALL STATES EXCEPT FOR NEW YORK.          (SP457B)
* 05/2015  RJ FRANKLIN
*            - REMOVE ILLINOIS SURS (PLAN 100825) WELCOME SUPPRESSION.
*            - RETAIN ALL OTHER 100825 SPECIFIC HARD CODING.
*            BEFORE THIS CHANGE THE ILLINOIS SURS PARTICIPANT EXPERIENCE
*            WAS TO RECIEVE A SINGLE PACKAGE WHEN THE CONDITIONS WERE
*            MET TO PRODUCE A LEGAL PACKAGE.  THE LEGAL PACKAGE WAS
*            CUSTOMIZED TO INCLUDE THE WELCOME LETTER.
*            NOW THE EXPERIENCE WILL BE TO RECIEVE BOTH THE LEGAL AND
*            WELCOME PACKAGES BUT BOTH BE SENT OUT AT THE SAME TIME.
*            SO THIS PROGRAM CHANGE WILL COINCIDE WITH ADDING 100825 TO
*            THE PS TABLE IN ACIS WITH THE WELCOME TO BE TRIGGERED BY
*            FUNDING.
*                      SCAN FOR TAGS ( ILLWELL )
*            09/2015 L. SHU
*                      ADDED LOGIC TO SUPPRESS BOTH WELCOME AND LEGAL
*                      PACKAGE FOR BENE IN THE PLAN                (BIP)
* 05/2016  B NEWSOM
*            NY LOAN ENDORSEMENTS PROJECT                        (NYLEP)
* 03/17/17 SHUL - ADDED ONEIRA ACCOUNT NUMBER TO PRAP FILE AND
*                 STORE ONEIRA ACCOUNT NUMBER TO REPRINT FILE  (ONEIRA)
* 03/17/17 B NEWSOM EASE ENROLLMENT PROJECT.
*                   ALLOW SUPPRESSION OF PACKAGES BASED ON ENROLLMENT
*                   METHOD.                                  (EASEENR)
* 05/24/17 DATTASO - CORRECT MILITARY STATE CODE MAPPING.
*                  SWITCH THE STATE CODE BETWEEN NY AND FL. (MILITARY)
* 07/04/17 BABRE   - PIN EXPANSION CHANGES.  CHG425939      (PINE)
* 09/17    BISWP   - ONEIRA - INITIAL PREMIUM FIELD         (INITP)
*                  EXPAND FORMATTED OUTPUT TO INCLUDE ONE MORE DIGIT
*                  IN AN ATTEMPT TO REDUCE PARTICIPANT COMPLAINTS.
*                  THIS IS A STOP GAP MEASURE TO BE REPLACED BY
*                  FURTHER DEVELOPMENT.
* 01/18    SHUL    - ONEIRA - INITIAL PREMIUM FIELD           (ONEIRA2)
*                  POPULATE TIAA AND CREF INITIAL PREMIUM INTO
*                  SEPARATE DATA FIELDS
* 06/19    JENABI  - ADDED THE LOGIC TO TERMINATE THE PROGRAM: (C566813)
*                  1) WITH RC=01 WHENEVER LEGAL EXTRACT COUNTS > 7000
*                  2) WITH RC=02 WHENEVER WELCOME EXTRACT COUNTS > 7000
* 11/19/19 B.NEWSOM - EXTRACT NEW ISSUANCE DATA FOR WELCOME AND LEGAL
*                     PACKAGES.                          (IISG)
* 03/14/19 L.SHU    - TO FIX A DEFECT                    (IISG1)
*                     MOVE AP-FUND-SOURCE-CDE-1 & 2 TO REPRINT FIELDS
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb100 extends BLNatBase
{
    // Data Areas
    private LdaAppl160 ldaAppl160;
    private LdaAppl170 ldaAppl170;
    private PdaAppa100 pdaAppa100;
    private PdaAppa200 pdaAppa200;
    private PdaScia8100 pdaScia8100;
    private PdaScia8200 pdaScia8200;
    private PdaScia8250 pdaScia8250;
    private PdaEcta051 pdaEcta051;
    private LdaAppl180 ldaAppl180;
    private PdaAciadate pdaAciadate;
    private LdaY2datebw ldaY2datebw;
    private PdaAcia2100 pdaAcia2100;
    private PdaAppa7900 pdaAppa7900;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup scia2301;

    private DbsGroup scia2301_Pnd_Input;
    private DbsField scia2301_Pnd_Rl_Contract_No;

    private DbsGroup scia2301_Pnd_Output;
    private DbsField scia2301_Pnd_Product_Cde;
    private DbsField scia2301_Pnd_Return_Code;
    private DbsField scia2301_Pnd_Return_Txt;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Dob;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Bill_Code;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Curr;
    private DbsField annty_Actvty_Prap_View_Ap_T_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_C_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership;
    private DbsField annty_Actvty_Prap_View_Ap_Sex;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Zip;
    private DbsField annty_Actvty_Prap_View_Ap_Name_Addr_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Ent_Sys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Released;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Discr;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_App_Recvd;
    private DbsField annty_Actvty_Prap_View_Ap_App_Source;
    private DbsField annty_Actvty_Prap_View_Ap_Region_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;

    private DbsGroup annty_Actvty_Prap_View_Ap_Address_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Line;
    private DbsField annty_Actvty_Prap_View_Ap_City;
    private DbsField annty_Actvty_Prap_View_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Annuity_Start_Date;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Primary_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Contingent_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Estate;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Trust;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Category;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Instructions;
    private DbsField annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_St_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Doi;

    private DbsGroup annty_Actvty_Prap_View_Ap_Mit_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Unit;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Wpid;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Rollover_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Ppg;
    private DbsField annty_Actvty_Prap_View_Ap_Print_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Addr_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Txt;
    private DbsField annty_Actvty_Prap_View_Ap_Zip_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup annty_Actvty_Prap_View__R_Field_1;
    private DbsField annty_Actvty_Prap_View_Bank_Home_Ac;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A3;
    private DbsField annty_Actvty_Prap_View_Bank_Home_A4;
    private DbsField annty_Actvty_Prap_View_Bank_Filler_4;
    private DbsField annty_Actvty_Prap_View_Oia_Indicator;
    private DbsField annty_Actvty_Prap_View_Erisa_Ind;
    private DbsField annty_Actvty_Prap_View_Cai_Ind;
    private DbsField annty_Actvty_Prap_View_Cip_Ind;
    private DbsField annty_Actvty_Prap_View_Same_Addr;
    private DbsField annty_Actvty_Prap_View_Omni_Issue_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Usage_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_1;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_2;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_3;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_4;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Inst_Link_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Applcnt_Req_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent;
    private DbsField annty_Actvty_Prap_View_Ap_Ppg_Team_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Model_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Divorce_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Phone_No;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Fmt;
    private DbsField annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Eft_Requested_Ind;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Divsub;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_1;

    private DbsGroup annty_Actvty_Prap_View__R_Field_2;
    private DbsField annty_Actvty_Prap_View_Ap_Plan_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Tsv_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Index_Rate_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tsr_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_2;

    private DbsGroup annty_Actvty_Prap_View__R_Field_3;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Client_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Portfolio_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Replacement_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Register_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Exempt_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Autosave_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Max_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days;
    private DbsField annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_Ann_Option;
    private DbsField annty_Actvty_Prap_View_Ap_Orchestration_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Startdate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Enddate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Percentage;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postdays;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Limit;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postfreq;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Pl_Level;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Windowdays;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Recap_Prov;
    private DbsField annty_Actvty_Prap_View_Ap_Ann_Funding_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt;
    private DbsField annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Conv_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Deceased_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Decedent_Contract;
    private DbsField annty_Actvty_Prap_View_Ap_Relation_To_Decedent;
    private DbsField annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Oneira_Acct_No;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier_2;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde_2;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct_2;

    private DataAccessProgramView vw_reprint_Fl;
    private DbsField reprint_Fl_Rp_Tiaa_Contr;
    private DbsField reprint_Fl_Rp_Cref_Contr;
    private DbsField reprint_Fl_Rp_Soc_Sec;
    private DbsField reprint_Fl_Rp_T_Doi;
    private DbsField reprint_Fl_Rp_C_Doi;

    private DataAccessProgramView vw_table_Entry;
    private DbsField table_Entry_Entry_Actve_Ind;
    private DbsField table_Entry_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Entry_Table_Sub_Id;
    private DbsField table_Entry_Entry_Cde;

    private DbsGroup table_Entry__R_Field_4;
    private DbsField table_Entry_Entry_Plan;
    private DbsField table_Entry_Entry_Sub_Plan_Number;
    private DbsField table_Entry_Entry_Pkg_Cntrl_Type;
    private DbsField table_Entry_Entry_Cde_Addl;
    private DbsField table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup table_Entry__R_Field_5;
    private DbsField table_Entry_Entry_Oln_Access;
    private DbsField table_Entry_Addl_Entry_Txt;

    private DbsGroup table_Entry__R_Field_6;
    private DbsField table_Entry_Entry_Pckg_Type;
    private DbsField table_Entry_Entry_Enr_Source;
    private DbsField table_Entry_Entry_Pkg_Trigger;
    private DbsField table_Entry_Addl_Entry_Txt_2;
    private DbsField pnd_Wk_Collb;

    private DbsGroup pnd_Wk_Collb__R_Field_7;
    private DbsField pnd_Wk_Collb_Pnd_Wk_Coll;
    private DbsField pnd_Product_Type;
    private DbsField pnd_Cref_Companion;
    private DbsField pnd_Dx_Error_Found;
    private DbsField pnd_Over_Flow;
    private DbsField pnd_Zp;
    private DbsField pnd_St;
    private DbsField space;
    private DbsField pnd_Hold_Coll_Code;

    private DbsGroup pnd_Hold_Coll_Code__R_Field_8;
    private DbsField pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_4;
    private DbsField pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_2;
    private DbsField pnd_Ap_Name;
    private DbsField pnd_Cor_Full_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_9;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_10;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_11;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_12;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_13;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20;
    private DbsField pnd_Hold_Nme_Prfx;

    private DbsGroup pnd_Hold_Nme_Prfx__R_Field_14;
    private DbsField pnd_Hold_Nme_Prfx_Pnd_Hold_Nme_Prfx_4;
    private DbsField pnd_Hold_Nme_Prfx_Pnd_Hold_Nme_Prfx_Bal;
    private DbsField pnd_Hold_State;
    private DbsField pnd_Hold_City_Zip;
    private DbsField pnd_W_State_Code_Tab;

    private DbsGroup pnd_W_State_Code_Tab__R_Field_15;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_State_Code_N;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_State_Filler_1;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_State_Code_A;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_State_Filler_2;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_State_Name;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_State_Filler_3;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_Ira_T_Msg_Typ;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_State_Filler_4;
    private DbsField pnd_W_State_Code_Tab_Pnd_W_Ira_C_Msg_Typ;
    private DbsField pnd_M;
    private DbsField pnd_Reprint_Ssn_Tiaa_No;

    private DbsGroup pnd_Reprint_Ssn_Tiaa_No__R_Field_16;
    private DbsField pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Soc_Sec;
    private DbsField pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Tiaa_Contr;
    private DbsField pnd_Cntrct_Nbr;

    private DbsGroup pnd_Ac_Pda;

    private DbsGroup pnd_Ac_Pda_Pnd_Ac_Data;
    private DbsField pnd_Ac_Pda_Pnd_Compressed_Nme;
    private DbsField pnd_Ac_Pda_Pnd_Prefix_Nme;
    private DbsField pnd_Ac_Pda_Pnd_First_Nme;
    private DbsField pnd_Ac_Pda_Pnd_Middle_Nme;
    private DbsField pnd_Ac_Pda_Pnd_Last_Nme;
    private DbsField pnd_Ac_Pda_Pnd_Title_Nme;
    private DbsField pnd_Ac_Pda_Pnd_Address_Line_1;
    private DbsField pnd_Ac_Pda_Pnd_Address_Line_2;
    private DbsField pnd_Ac_Pda_Pnd_Address_Line_3;
    private DbsField pnd_Ac_Pda_Pnd_Address_Line_4;
    private DbsField pnd_Ac_Pda_Pnd_Postal_Cde;
    private DbsField pnd_Ac_Pda_Pnd_Org_Nme;
    private DbsField pnd_Pin_Id;

    private DbsGroup pnd_Ap_Tic_Wk;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_17;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_A;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_18;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_A;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_19;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_A;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_20;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_A;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_21;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_A;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_22;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_A;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_23;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_A;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_N;

    private DbsGroup pnd_Ap_Tic_Wk__R_Field_24;
    private DbsField pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_A;
    private DbsField pnd_Date_Numeric;

    private DbsGroup pnd_Date_Numeric__R_Field_25;
    private DbsField pnd_Date_Numeric_Pnd_Date_Alpha;

    private DbsGroup pnd_Date_Numeric__R_Field_26;
    private DbsField pnd_Date_Numeric_Pnd_Date_Numeric_Yyyymm;
    private DbsField pnd_Date_Startdate_D;
    private DbsField pnd_Tiaa_Nbr;

    private DbsGroup pnd_Tiaa_Nbr__R_Field_27;
    private DbsField pnd_Tiaa_Nbr_Pnd_Tiaa_Nbr_Prfx;
    private DbsField pnd_Tiaa_Nbr_Pnd_Tiaa_Nbr_Cont;
    private DbsField pnd_Dflt_Amt;

    private DbsGroup pnd_Dflt_Amt__R_Field_28;
    private DbsField pnd_Dflt_Amt_Pnd_Dflt_Amt_N;
    private DbsField pnd_Dflt_Pct;

    private DbsGroup pnd_Dflt_Pct__R_Field_29;
    private DbsField pnd_Dflt_Pct_Pnd_Dflt_Pct_N;
    private DbsField pnd_Incr_Pct;

    private DbsGroup pnd_Incr_Pct__R_Field_30;
    private DbsField pnd_Incr_Pct_Pnd_Incr_Pct_N;
    private DbsField pnd_Max_Pct;

    private DbsGroup pnd_Max_Pct__R_Field_31;
    private DbsField pnd_Max_Pct_Pnd_Max_Pct_N;
    private DbsField pnd_Annuity_Start_Date;

    private DbsGroup pnd_Annuity_Start_Date__R_Field_32;
    private DbsField pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N;
    private DbsField pnd_Tiaa_Iss_Date;

    private DbsGroup pnd_Tiaa_Iss_Date__R_Field_33;
    private DbsField pnd_Tiaa_Iss_Date_Pnd_Tiaa_Iss_Date_N;
    private DbsField pnd_Cref_Iss_Date;

    private DbsGroup pnd_Cref_Iss_Date__R_Field_34;
    private DbsField pnd_Cref_Iss_Date_Pnd_Cref_Iss_Date_N;
    private DbsField pnd_Pin_Number_N;

    private DbsGroup pnd_Pin_Number_N__R_Field_35;
    private DbsField pnd_Pin_Number_N_Pnd_Pin_Number;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Extract_Record;
    private DbsField pnd_Reprint_Rec_Found;
    private DbsField pnd_Business_Date;

    private DbsGroup pnd_Business_Date__R_Field_36;
    private DbsField pnd_Business_Date_Pnd_Cc;
    private DbsField pnd_Business_Date_Pnd_Yy;
    private DbsField pnd_Business_Date_Pnd_Mm;
    private DbsField pnd_Business_Date_Pnd_Dd;
    private DbsField pnd_Prem_Found;
    private DbsField pnd_Ira_Except_Found;
    private DbsField pnd_Date_Diff;

    private DbsGroup pnd_Date_Diff__R_Field_37;
    private DbsField pnd_Date_Diff_Pnd_Date_Diff_Mm;
    private DbsField pnd_Date_Diff_Pnd_Date_Diff_Dd;
    private DbsField pnd_Date_Diff_Pnd_Date_Diff_Cc;
    private DbsField pnd_Date_Diff_Pnd_Date_Diff_Yy;
    private DbsField pnd_Date_Yyyymmdd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_38;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Cc_1;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yy_1;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Mm_1;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Dd_1;
    private DbsField pnd_Release_Ind;
    private DbsField pnd_Da_Tiaa_Found;
    private DbsField pnd_Da_Cref_Found;
    private DbsField pnd_Rc_Or_Rcp;
    private DbsField pnd_Indiv_Product;
    private DbsField pnd_Group_Product;
    private DbsField pnd_Product;
    private DbsField pnd_Team;
    private DbsField pnd_Temp_Date;
    private DbsField pnd_X;
    private DbsField pnd_Debug;
    private DbsField pnd_Non_T_C_Administered_Ppg;
    private DbsField pnd_Non_T_C_Administered_Plan;
    private DbsField pnd_Mail_Date;
    private DbsField pnd_Search_Key;

    private DbsGroup pnd_Search_Key__R_Field_39;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Cde;

    private DbsGroup pnd_Search_Key__R_Field_40;
    private DbsField pnd_Search_Key_Pnd_Ky_Plan_Number;
    private DbsField pnd_Search_Key_Pnd_Ky_Subplan_Number;
    private DbsField pnd_Search_Key_Pnd_Ky_Addl_Cde;

    private DbsGroup pnd_Search_Key__R_Field_41;
    private DbsField pnd_Search_Key_Pnd_Ky_Pkg_Cntrl_Type;
    private DbsField pnd_Search_Key_Pnd_Ky_Addl_Cde_2;

    private DbsGroup pnd_Trigger_Table;
    private DbsField pnd_Trigger_Table_Pnd_T_Plan_Number;
    private DbsField pnd_Trigger_Table_Pnd_T_Subplan_Number;
    private DbsField pnd_Trigger_Table_Pnd_T_Pkg_Cntrl_Type;
    private DbsField pnd_Trigger_Table_Pnd_T_Pkg_Trigger;

    private DbsGroup pnd_Suppress_Table;
    private DbsField pnd_Suppress_Table_Pnd_S_Plan_Number;
    private DbsField pnd_Suppress_Table_Pnd_S_Subplan_Number;
    private DbsField pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type;
    private DbsField pnd_Suppress_Table_Pnd_S_Enr_Source;
    private DbsField pnd_Suppress_Table_Pnd_S_Pckg_Type;
    private DbsField pnd_I_S;
    private DbsField pnd_I_T;
    private DbsField pnd_Fund_Count;
    private DbsField pnd_Extract_Contract_Cnt;
    private DbsField pnd_Legacy_Contract_Cnt;
    private DbsField pnd_W_Check_State;
    private DbsField pnd_Except_Reason;
    private DbsField pnd_Exception_Cnt;
    private DbsField pnd_Exc_Line_Cnt;
    private DbsField pnd_Exc_Report_Type;
    private DbsField pnd_Close_Plan_Interface;
    private DbsField pnd_Inst_Oia_Is_Type;

    private DbsGroup pnd_Inst_Oia_Is_Type__R_Field_42;
    private DbsField pnd_Inst_Oia_Is_Type_Pnd_Inst_Oia_Is_Type_1;
    private DbsField pnd_Inst_Oia_Is_Type_Pnd_Inst_Oia_Is_Type_2;
    private DbsField pnd_Int_First_Time;
    private DbsField pnd_Con_First_Time;
    private DbsField pnd_Close_Cntct_Interface;
    private DbsField pnd_Sgrd_Contct_Call_Cnt;
    private DbsField pnd_Hold_City_St;
    private DbsField pnd_Hold_Cstate;
    private DbsField pnd_Hold_Addr;
    private DbsField pnd_Hold_Zip;

    private DbsGroup pnd_Hold_Zip__R_Field_43;
    private DbsField pnd_Hold_Zip_Pnd_Hold_Zip5;
    private DbsField pnd_Hold_Zip_Pnd_Hold_Zip4;
    private DbsField pnd_Hold_City_State_Zip;
    private DbsField pnd_Char;
    private DbsField pnd_Dec1;
    private DbsField pnd_Tiaa_Rate;
    private DbsField pnd_Hold_Interest_Rt;

    private DbsGroup pnd_Hold_Interest_Rt__R_Field_44;
    private DbsField pnd_Hold_Interest_Rt_Pnd_Hold_Int_Rate_Print;
    private DbsField pnd_Hold_Interest_Rt_Pnd_Hold_Int_Rate_Num1;
    private DbsField pnd_Hold_Effective_Dt;

    private DbsGroup pnd_Hold_Effective_Dt__R_Field_45;
    private DbsField pnd_Hold_Effective_Dt_Pnd_Hold_Effective_Dt_A;
    private DbsField pnd_Icap_Tnt_Companion;
    private DbsField pnd_Tiaa_Contact_Nbr;
    private DbsField pnd_Cref_Contact_Nbr;
    private DbsField pnd_Suppress_Welc_Pkg;
    private DbsField pnd_Suppress_Legal_Pkg;
    private DbsField pnd_Welc_Fund_Trigger;
    private DbsField pnd_Welc_Force_Inst_Owned;
    private DbsField pnd_Rp_Text_Udf_1;
    private DbsField pnd_Nbr;
    private DbsField pnd_Spec_Fund_Ind;
    private DbsField pnd_Cal_Country_Cd;
    private DbsField pnd_Cal_Country_Name;
    private DbsField pnd_Cal_Return_Code;
    private DbsField pnd_Cal_Return_Msg;
    private DbsField pnd_Date;
    private DbsField pnd_Date_1;
    private DbsField pnd_Hold_Orig_Tiaa_Doi;

    private DbsGroup pnd_Hold_Orig_Tiaa_Doi__R_Field_46;
    private DbsField pnd_Hold_Orig_Tiaa_Doi_Pnd_Hold_Orig_Tiaa_Doi_A;
    private DbsField pnd_Hold_Orig_Cref_Doi;

    private DbsGroup pnd_Hold_Orig_Cref_Doi__R_Field_47;
    private DbsField pnd_Hold_Orig_Cref_Doi_Pnd_Hold_Orig_Cref_Doi_A;

    private DbsGroup pnd_In_Translte_File;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Ticker;
    private DbsField pnd_In_Translte_File_Pnd_F1;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name;
    private DbsField pnd_In_Translte_File_Pnd_F2;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq;
    private DbsField pnd_In_Translte_File_Pnd_F3;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Fund_First_4;
    private DbsField pnd_In_Translte_File_Pnd_F5;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Fund_Code;

    private DbsGroup pnd_Translte_File;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker;
    private DbsField pnd_Translte_File_Pnd_F1;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker_Name;
    private DbsField pnd_Translte_File_Pnd_F2;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Seq;
    private DbsField pnd_Translte_File_Pnd_F3;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_First_4;
    private DbsField pnd_Translte_File_Pnd_F4;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Code;
    private DbsField pnd_Translte_File_Pnd_F5;
    private DbsField pnd_Translte_File_Pnd_Translte_Error_Message;
    private DbsField pnd_Wk_Inst_State;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl160 = new LdaAppl160();
        registerRecord(ldaAppl160);
        ldaAppl170 = new LdaAppl170();
        registerRecord(ldaAppl170);
        localVariables = new DbsRecord();
        pdaAppa100 = new PdaAppa100(localVariables);
        pdaAppa200 = new PdaAppa200(localVariables);
        pdaScia8100 = new PdaScia8100(localVariables);
        pdaScia8200 = new PdaScia8200(localVariables);
        pdaScia8250 = new PdaScia8250(localVariables);
        pdaEcta051 = new PdaEcta051(localVariables);
        ldaAppl180 = new LdaAppl180();
        registerRecord(ldaAppl180);
        registerRecord(ldaAppl180.getVw_acis_Reprint_Fl_View());
        pdaAciadate = new PdaAciadate(localVariables);
        ldaY2datebw = new LdaY2datebw();
        registerRecord(ldaY2datebw);
        pdaAcia2100 = new PdaAcia2100(localVariables);
        pdaAppa7900 = new PdaAppa7900(localVariables);

        // Local Variables

        scia2301 = localVariables.newGroupInRecord("scia2301", "SCIA2301");

        scia2301_Pnd_Input = scia2301.newGroupInGroup("scia2301_Pnd_Input", "#INPUT");
        scia2301_Pnd_Rl_Contract_No = scia2301_Pnd_Input.newFieldInGroup("scia2301_Pnd_Rl_Contract_No", "#RL-CONTRACT-NO", FieldType.STRING, 10);

        scia2301_Pnd_Output = scia2301.newGroupInGroup("scia2301_Pnd_Output", "#OUTPUT");
        scia2301_Pnd_Product_Cde = scia2301_Pnd_Output.newFieldInGroup("scia2301_Pnd_Product_Cde", "#PRODUCT-CDE", FieldType.STRING, 10);
        scia2301_Pnd_Return_Code = scia2301_Pnd_Output.newFieldInGroup("scia2301_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 4);
        scia2301_Pnd_Return_Txt = scia2301_Pnd_Output.newFieldInGroup("scia2301_Pnd_Return_Txt", "#RETURN-TXT", FieldType.STRING, 70);

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Dob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_Bill_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bill_Code", "AP-BILL-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BILL_CODE");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Curr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_CURR");
        annty_Actvty_Prap_View_Ap_T_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Age_1st", "AP-T-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_T_AGE_1ST");
        annty_Actvty_Prap_View_Ap_C_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Age_1st", "AP-C-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_C_AGE_1ST");
        annty_Actvty_Prap_View_Ap_Ownership = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership", "AP-OWNERSHIP", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Sex = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sex", "AP-SEX", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SEX");
        annty_Actvty_Prap_View_Ap_Sex.setDdmHeader("SEX CDE");
        annty_Actvty_Prap_View_Ap_Mail_Zip = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Zip", "AP-MAIL-ZIP", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        annty_Actvty_Prap_View_Ap_Name_Addr_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NAME_ADDR_CD");
        annty_Actvty_Prap_View_Ap_Dt_Ent_Sys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        annty_Actvty_Prap_View_Ap_Dt_Released = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Released", "AP-DT-RELEASED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_RELEASED");
        annty_Actvty_Prap_View_Ap_Alloc_Discr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Discr", "AP-ALLOC-DISCR", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_ALLOC_DISCR");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Type", "AP-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_TYPE");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Dt_App_Recvd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");
        annty_Actvty_Prap_View_Ap_App_Source = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_App_Source", "AP-APP-SOURCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APP_SOURCE");
        annty_Actvty_Prap_View_Ap_App_Source.setDdmHeader("SOURCE");
        annty_Actvty_Prap_View_Ap_Region_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Region_Code", "AP-REGION-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_REGION_CODE");
        annty_Actvty_Prap_View_Ap_Orig_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orig_Issue_State", 
            "AP-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Ownership_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP_TYPE");
        annty_Actvty_Prap_View_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");

        annty_Actvty_Prap_View_Ap_Address_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Address_Info", 
            "AP-ADDRESS-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_Address_Line = annty_Actvty_Prap_View_Ap_Address_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Line", "AP-ADDRESS-LINE", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_City = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_City", "AP-CITY", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "AP_CITY");
        annty_Actvty_Prap_View_Ap_Current_State_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Current_State_Code", 
            "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_View_Ap_Annuity_Start_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Annuity_Start_Date", 
            "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");
        annty_Actvty_Prap_View_Ap_Primary_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Std_Ent", 
            "AP-PRIMARY-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Primary_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Primary_Info", 
            "AP-BENE-PRIMARY-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Primary_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Primary_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Bene_Info", 
            "AP-PRIMARY-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Std_Ent", 
            "AP-CONTINGENT-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Contingent_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Contingent_Info", 
            "AP-BENE-CONTINGENT-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Contingent_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Bene_Info", 
            "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Bene_Estate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Estate", "AP-BENE-ESTATE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_ESTATE");
        annty_Actvty_Prap_View_Ap_Bene_Trust = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Trust", "AP-BENE-TRUST", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_TRUST");
        annty_Actvty_Prap_View_Ap_Bene_Category = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Category", "AP-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_CATEGORY");
        annty_Actvty_Prap_View_Ap_Mail_Instructions = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Instructions", 
            "AP-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request", 
            "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        annty_Actvty_Prap_View_Ap_Coll_St_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_St_Cd", "AP-COLL-ST-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        annty_Actvty_Prap_View_Ap_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Tiaa_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_View_Ap_Cref_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Doi", "AP-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");

        annty_Actvty_Prap_View_Ap_Mit_Request = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Mit_Request", "AP-MIT-REQUEST", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Unit = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Unit", "AP-MIT-UNIT", 
            FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Wpid = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Wpid", "AP-MIT-WPID", 
            FieldType.STRING, 6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Ira_Rollover_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Rollover_Type", 
            "AP-IRA-ROLLOVER-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_ROLLOVER_TYPE");
        annty_Actvty_Prap_View_Ap_Ira_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Record_Type", 
            "AP-IRA-RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Status", 
            "AP-MULT-APP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_STATUS");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type", 
            "AP-MULT-APP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Ppg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_MULT_APP_PPG");
        annty_Actvty_Prap_View_Ap_Print_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Print_Date", "AP-PRINT-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_PRINT_DATE");

        annty_Actvty_Prap_View_Ap_Addr_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Addr_Info", "AP-ADDR-INFO", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Txt = annty_Actvty_Prap_View_Ap_Addr_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Txt", "AP-ADDRESS-TXT", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Zip_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Zip_Code", "AP-ZIP-CODE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        annty_Actvty_Prap_View__R_Field_1 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_1", "REDEFINE", annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        annty_Actvty_Prap_View_Bank_Home_Ac = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_Ac", "BANK-HOME-AC", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A3 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A3", "BANK-HOME-A3", 
            FieldType.STRING, 3);
        annty_Actvty_Prap_View_Bank_Home_A4 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Home_A4", "BANK-HOME-A4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Bank_Filler_4 = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Bank_Filler_4", "BANK-FILLER-4", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Oia_Indicator = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Oia_Indicator", "OIA-INDICATOR", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Erisa_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Erisa_Ind", "ERISA-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cai_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Cai_Ind", "CAI-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Cip_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Cip_Ind", "CIP-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Same_Addr = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Same_Addr", "SAME-ADDR", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Omni_Issue_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Omni_Issue_Ind", "OMNI-ISSUE-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Addr_Usage_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Usage_Code", 
            "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_USAGE_CODE");
        annty_Actvty_Prap_View_Ap_Financial_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_1", "AP-FINANCIAL-1", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_1");
        annty_Actvty_Prap_View_Ap_Financial_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_2", "AP-FINANCIAL-2", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_2");
        annty_Actvty_Prap_View_Ap_Financial_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_3", "AP-FINANCIAL-3", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_3");
        annty_Actvty_Prap_View_Ap_Financial_4 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_4", "AP-FINANCIAL-4", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_4");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde", 
            "AP-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_GRP_CDE");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_CDE");
        annty_Actvty_Prap_View_Ap_Inst_Link_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "AP_INST_LINK_CDE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Applcnt_Req_Type", 
            "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent", 
            "AP-TIAA-SERVICE-AGENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIAA_SERVICE_AGENT");
        annty_Actvty_Prap_View_Ap_Ppg_Team_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_PPG_TEAM_CDE");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_RLC_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Allocation_Model_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Model_Type", 
            "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        annty_Actvty_Prap_View_Ap_Divorce_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Divorce_Ind", "AP-DIVORCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DIVORCE_IND");
        annty_Actvty_Prap_View_Ap_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Phone_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Phone_No", "AP-PHONE-NO", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "AP_PHONE_NO");
        annty_Actvty_Prap_View_Ap_Allocation_Fmt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOCATION_FMT");
        annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind", 
            "AP-E-SIGNED-APPL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        annty_Actvty_Prap_View_Ap_Eft_Requested_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eft_Requested_Ind", 
            "AP-EFT-REQUESTED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");

        annty_Actvty_Prap_View_Ap_Fund_Identifier = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier", 
            "AP-FUND-IDENTIFIER", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Fund_Cde = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde", "AP-FUND-CDE", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Allocation_Pct = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct", 
            "AP-ALLOCATION-PCT", FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        annty_Actvty_Prap_View_Ap_Sgrd_Divsub = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_SGRD_DIVSUB");
        annty_Actvty_Prap_View_Ap_Text_Udf_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_1", "AP-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_1");

        annty_Actvty_Prap_View__R_Field_2 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_2", "REDEFINE", annty_Actvty_Prap_View_Ap_Text_Udf_1);
        annty_Actvty_Prap_View_Ap_Plan_Issue_State = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Plan_Issue_State", "AP-PLAN-ISSUE-STATE", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Ap_Tsv_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Tsv_Ind", "AP-TSV-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Ap_Index_Rate_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Index_Rate_Ind", "AP-INDEX-RATE-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Status = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Status", "AP-SGRD-PART-STATUS", 
            FieldType.STRING, 2);
        annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind", "AP-SPCL-LGL-PKG-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Tsr_Ind = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Ap_Tsr_Ind", "AP-TSR-IND", FieldType.STRING, 
            3);
        annty_Actvty_Prap_View_Ap_Text_Udf_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_2", "AP-TEXT-UDF-2", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_2");

        annty_Actvty_Prap_View__R_Field_3 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_3", "REDEFINE", annty_Actvty_Prap_View_Ap_Text_Udf_2);
        annty_Actvty_Prap_View_Ap_Sgrd_Client_Id = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Client_Id", "AP-SGRD-CLIENT-ID", 
            FieldType.STRING, 6);
        annty_Actvty_Prap_View_Ap_Portfolio_Type = annty_Actvty_Prap_View__R_Field_3.newFieldInGroup("annty_Actvty_Prap_View_Ap_Portfolio_Type", "AP-PORTFOLIO-TYPE", 
            FieldType.STRING, 4);
        annty_Actvty_Prap_View_Ap_Replacement_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Replacement_Ind", 
            "AP-REPLACEMENT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_REPLACEMENT_IND");
        annty_Actvty_Prap_View_Ap_Register_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Register_Id", "AP-REGISTER-ID", 
            FieldType.STRING, 11, RepeatingFieldStrategy.None, "AP_REGISTER_ID");
        annty_Actvty_Prap_View_Ap_Exempt_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Exempt_Ind", "AP-EXEMPT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EXEMPT_IND");
        annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind", 
            "AP-INCMPL-ACCT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_INCMPL_ACCT_IND");
        annty_Actvty_Prap_View_Ap_Autosave_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Autosave_Ind", "AP-AUTOSAVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AUTOSAVE_IND");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt", 
            "AP-AS-CUR-DFLT-OPT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_OPT");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt", 
            "AP-AS-CUR-DFLT-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_AMT");
        annty_Actvty_Prap_View_Ap_As_Incr_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Opt", "AP-AS-INCR-OPT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_INCR_OPT");
        annty_Actvty_Prap_View_Ap_As_Incr_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Amt", "AP-AS-INCR-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_INCR_AMT");
        annty_Actvty_Prap_View_Ap_As_Max_Pct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Max_Pct", "AP-AS-MAX-PCT", 
            FieldType.NUMERIC, 7, 2, RepeatingFieldStrategy.None, "AP_AS_MAX_PCT");
        annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days", 
            "AP-AE-OPT-OUT-DAYS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_AE_OPT_OUT_DAYS");
        annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id", 
            "AP-AGENT-OR-RACF-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_AGENT_OR_RACF_ID");
        annty_Actvty_Prap_View_Ap_Legal_Ann_Option = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_Ann_Option", 
            "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        annty_Actvty_Prap_View_Ap_Orchestration_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orchestration_Id", 
            "AP-ORCHESTRATION-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_ORCHESTRATION_ID");
        annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd", 
            "AP-MAIL-ADDR-COUNTRY-CD", FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_MAIL_ADDR_COUNTRY_CD");
        annty_Actvty_Prap_View_Ap_Tic_Startdate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Startdate", "AP-TIC-STARTDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_STARTDATE");
        annty_Actvty_Prap_View_Ap_Tic_Enddate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Enddate", "AP-TIC-ENDDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_ENDDATE");
        annty_Actvty_Prap_View_Ap_Tic_Percentage = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Percentage", "AP-TIC-PERCENTAGE", 
            FieldType.NUMERIC, 15, 6, RepeatingFieldStrategy.None, "AP_TIC_PERCENTAGE");
        annty_Actvty_Prap_View_Ap_Tic_Postdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postdays", "AP-TIC-POSTDAYS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_TIC_POSTDAYS");
        annty_Actvty_Prap_View_Ap_Tic_Limit = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Limit", "AP-TIC-LIMIT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "AP_TIC_LIMIT");
        annty_Actvty_Prap_View_Ap_Tic_Postfreq = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postfreq", "AP-TIC-POSTFREQ", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_POSTFREQ");
        annty_Actvty_Prap_View_Ap_Tic_Pl_Level = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Pl_Level", "AP-TIC-PL-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_PL_LEVEL");
        annty_Actvty_Prap_View_Ap_Tic_Windowdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Windowdays", "AP-TIC-WINDOWDAYS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_WINDOWDAYS");
        annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow", 
            "AP-TIC-REQDLYWINDOW", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_REQDLYWINDOW");
        annty_Actvty_Prap_View_Ap_Tic_Recap_Prov = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Recap_Prov", "AP-TIC-RECAP-PROV", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_RECAP_PROV");
        annty_Actvty_Prap_View_Ap_Ann_Funding_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ann_Funding_Dt", "AP-ANN-FUNDING-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_ANN_FUNDING_DT");
        annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt", 
            "AP-TIAA-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIAA_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt", 
            "AP-CREF-ANN-ISSUE-DT", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_CREF_ANN_ISSUE_DT");
        annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind", 
            "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        annty_Actvty_Prap_View_Ap_Conv_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Conv_Issue_State", 
            "AP-CONV-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CONV_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Deceased_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Deceased_Ind", "AP-DECEASED-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DECEASED_IND");
        annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind", 
            "AP-NON-PROPRIETARY-PKG-IND", FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NON_PROPRIETARY_PKG_IND");
        annty_Actvty_Prap_View_Ap_Decedent_Contract = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Decedent_Contract", 
            "AP-DECEDENT-CONTRACT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DECEDENT_CONTRACT");
        annty_Actvty_Prap_View_Ap_Relation_To_Decedent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Relation_To_Decedent", 
            "AP-RELATION-TO-DECEDENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_RELATION_TO_DECEDENT");
        annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind", "AP-SSN-TIN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SSN_TIN_IND");
        annty_Actvty_Prap_View_Ap_Oneira_Acct_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Oneira_Acct_No", "AP-ONEIRA-ACCT-NO", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_ONEIRA_ACCT_NO");
        annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1", 
            "AP-FUND-SOURCE-CDE-1", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_FUND_SOURCE_CDE_1");
        annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2", 
            "AP-FUND-SOURCE-CDE-2", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_FUND_SOURCE_CDE_2");

        annty_Actvty_Prap_View_Ap_Fund_Identifier_2 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier_2", 
            "AP-FUND-IDENTIFIER-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        annty_Actvty_Prap_View_Ap_Fund_Cde_2 = annty_Actvty_Prap_View_Ap_Fund_Identifier_2.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde_2", 
            "AP-FUND-CDE-2", FieldType.STRING, 10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE_2", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        annty_Actvty_Prap_View_Ap_Allocation_Pct_2 = annty_Actvty_Prap_View_Ap_Fund_Identifier_2.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct_2", 
            "AP-ALLOCATION-PCT-2", FieldType.NUMERIC, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT_2", 
            "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER_2");
        registerRecord(vw_annty_Actvty_Prap_View);

        vw_reprint_Fl = new DataAccessProgramView(new NameInfo("vw_reprint_Fl", "REPRINT-FL"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE");
        reprint_Fl_Rp_Tiaa_Contr = vw_reprint_Fl.getRecord().newFieldInGroup("reprint_Fl_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RP_TIAA_CONTR");
        reprint_Fl_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        reprint_Fl_Rp_Cref_Contr = vw_reprint_Fl.getRecord().newFieldInGroup("reprint_Fl_Rp_Cref_Contr", "RP-CREF-CONTR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RP_CREF_CONTR");
        reprint_Fl_Rp_Cref_Contr.setDdmHeader("CREF/CONTR");
        reprint_Fl_Rp_Soc_Sec = vw_reprint_Fl.getRecord().newFieldInGroup("reprint_Fl_Rp_Soc_Sec", "RP-SOC-SEC", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "RP_SOC_SEC");
        reprint_Fl_Rp_Soc_Sec.setDdmHeader("SOC/SEC");
        reprint_Fl_Rp_T_Doi = vw_reprint_Fl.getRecord().newFieldInGroup("reprint_Fl_Rp_T_Doi", "RP-T-DOI", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RP_T_DOI");
        reprint_Fl_Rp_T_Doi.setDdmHeader("TIAA/DOI");
        reprint_Fl_Rp_C_Doi = vw_reprint_Fl.getRecord().newFieldInGroup("reprint_Fl_Rp_C_Doi", "RP-C-DOI", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "RP_C_DOI");
        reprint_Fl_Rp_C_Doi.setDdmHeader("CREF/DOI");
        registerRecord(vw_reprint_Fl);

        vw_table_Entry = new DataAccessProgramView(new NameInfo("vw_table_Entry", "TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        table_Entry_Entry_Actve_Ind = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        table_Entry_Entry_Table_Id_Nbr = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        table_Entry_Entry_Table_Sub_Id = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        table_Entry_Entry_Cde = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        table_Entry__R_Field_4 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_4", "REDEFINE", table_Entry_Entry_Cde);
        table_Entry_Entry_Plan = table_Entry__R_Field_4.newFieldInGroup("table_Entry_Entry_Plan", "ENTRY-PLAN", FieldType.STRING, 6);
        table_Entry_Entry_Sub_Plan_Number = table_Entry__R_Field_4.newFieldInGroup("table_Entry_Entry_Sub_Plan_Number", "ENTRY-SUB-PLAN-NUMBER", FieldType.STRING, 
            6);
        table_Entry_Entry_Pkg_Cntrl_Type = table_Entry__R_Field_4.newFieldInGroup("table_Entry_Entry_Pkg_Cntrl_Type", "ENTRY-PKG-CNTRL-TYPE", FieldType.STRING, 
            1);
        table_Entry_Entry_Cde_Addl = table_Entry__R_Field_4.newFieldInGroup("table_Entry_Entry_Cde_Addl", "ENTRY-CDE-ADDL", FieldType.STRING, 7);
        table_Entry_Entry_Dscrptn_Txt = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        table_Entry__R_Field_5 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_5", "REDEFINE", table_Entry_Entry_Dscrptn_Txt);
        table_Entry_Entry_Oln_Access = table_Entry__R_Field_5.newFieldInGroup("table_Entry_Entry_Oln_Access", "ENTRY-OLN-ACCESS", FieldType.STRING, 9);
        table_Entry_Addl_Entry_Txt = table_Entry__R_Field_5.newFieldInGroup("table_Entry_Addl_Entry_Txt", "ADDL-ENTRY-TXT", FieldType.STRING, 51);

        table_Entry__R_Field_6 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_6", "REDEFINE", table_Entry_Entry_Dscrptn_Txt);
        table_Entry_Entry_Pckg_Type = table_Entry__R_Field_6.newFieldInGroup("table_Entry_Entry_Pckg_Type", "ENTRY-PCKG-TYPE", FieldType.STRING, 1);
        table_Entry_Entry_Enr_Source = table_Entry__R_Field_6.newFieldInGroup("table_Entry_Entry_Enr_Source", "ENTRY-ENR-SOURCE", FieldType.STRING, 1);
        table_Entry_Entry_Pkg_Trigger = table_Entry__R_Field_6.newFieldInGroup("table_Entry_Entry_Pkg_Trigger", "ENTRY-PKG-TRIGGER", FieldType.STRING, 
            1);
        table_Entry_Addl_Entry_Txt_2 = table_Entry__R_Field_6.newFieldInGroup("table_Entry_Addl_Entry_Txt_2", "ADDL-ENTRY-TXT-2", FieldType.STRING, 57);
        registerRecord(vw_table_Entry);

        pnd_Wk_Collb = localVariables.newFieldInRecord("pnd_Wk_Collb", "#WK-COLLB", FieldType.STRING, 5);

        pnd_Wk_Collb__R_Field_7 = localVariables.newGroupInRecord("pnd_Wk_Collb__R_Field_7", "REDEFINE", pnd_Wk_Collb);
        pnd_Wk_Collb_Pnd_Wk_Coll = pnd_Wk_Collb__R_Field_7.newFieldInGroup("pnd_Wk_Collb_Pnd_Wk_Coll", "#WK-COLL", FieldType.STRING, 4);
        pnd_Product_Type = localVariables.newFieldInRecord("pnd_Product_Type", "#PRODUCT-TYPE", FieldType.STRING, 2);
        pnd_Cref_Companion = localVariables.newFieldInRecord("pnd_Cref_Companion", "#CREF-COMPANION", FieldType.BOOLEAN, 1);
        pnd_Dx_Error_Found = localVariables.newFieldInRecord("pnd_Dx_Error_Found", "#DX-ERROR-FOUND", FieldType.STRING, 1);
        pnd_Over_Flow = localVariables.newFieldInRecord("pnd_Over_Flow", "#OVER-FLOW", FieldType.STRING, 38);
        pnd_Zp = localVariables.newFieldInRecord("pnd_Zp", "#ZP", FieldType.PACKED_DECIMAL, 1);
        pnd_St = localVariables.newFieldInRecord("pnd_St", "#ST", FieldType.PACKED_DECIMAL, 2);
        space = localVariables.newFieldInRecord("space", "SPACE", FieldType.STRING, 35);
        pnd_Hold_Coll_Code = localVariables.newFieldInRecord("pnd_Hold_Coll_Code", "#HOLD-COLL-CODE", FieldType.STRING, 6);

        pnd_Hold_Coll_Code__R_Field_8 = localVariables.newGroupInRecord("pnd_Hold_Coll_Code__R_Field_8", "REDEFINE", pnd_Hold_Coll_Code);
        pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_4 = pnd_Hold_Coll_Code__R_Field_8.newFieldInGroup("pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_4", "#HOLD-COLL-CODE-4", 
            FieldType.STRING, 4);
        pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_2 = pnd_Hold_Coll_Code__R_Field_8.newFieldInGroup("pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_2", "#HOLD-COLL-CODE-2", 
            FieldType.STRING, 2);
        pnd_Ap_Name = localVariables.newFieldInRecord("pnd_Ap_Name", "#AP-NAME", FieldType.STRING, 38);
        pnd_Cor_Full_Nme = localVariables.newFieldInRecord("pnd_Cor_Full_Nme", "#COR-FULL-NME", FieldType.STRING, 90);

        pnd_Cor_Full_Nme__R_Field_9 = localVariables.newGroupInRecord("pnd_Cor_Full_Nme__R_Field_9", "REDEFINE", pnd_Cor_Full_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme = pnd_Cor_Full_Nme__R_Field_9.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme", "#COR-LAST-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_10 = pnd_Cor_Full_Nme__R_Field_9.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_10", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16 = pnd_Cor_Full_Nme__R_Field_10.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16", "#COR-LAST-NME-16", 
            FieldType.STRING, 16);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14 = pnd_Cor_Full_Nme__R_Field_10.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14", "#COR-LAST-NME-14", 
            FieldType.STRING, 14);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme = pnd_Cor_Full_Nme__R_Field_9.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme", "#COR-FIRST-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_11 = pnd_Cor_Full_Nme__R_Field_9.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_11", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_First_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10 = pnd_Cor_Full_Nme__R_Field_11.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10", "#COR-FIRST-NME-10", 
            FieldType.STRING, 10);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20 = pnd_Cor_Full_Nme__R_Field_11.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20", "#COR-FIRST-NME-20", 
            FieldType.STRING, 20);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme = pnd_Cor_Full_Nme__R_Field_9.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme", "#COR-MDDLE-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_12 = pnd_Cor_Full_Nme__R_Field_9.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_12", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10 = pnd_Cor_Full_Nme__R_Field_12.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10", "#COR-MDDLE-NME-10", 
            FieldType.STRING, 10);

        pnd_Cor_Full_Nme__R_Field_13 = pnd_Cor_Full_Nme__R_Field_12.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_13", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1 = pnd_Cor_Full_Nme__R_Field_13.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1", "#COR-MDDLE-NME-1", 
            FieldType.STRING, 1);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9 = pnd_Cor_Full_Nme__R_Field_13.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9", "#COR-MDDLE-NME-9", 
            FieldType.STRING, 9);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20 = pnd_Cor_Full_Nme__R_Field_12.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20", "#COR-MDDLE-NME-20", 
            FieldType.STRING, 20);
        pnd_Hold_Nme_Prfx = localVariables.newFieldInRecord("pnd_Hold_Nme_Prfx", "#HOLD-NME-PRFX", FieldType.STRING, 8);

        pnd_Hold_Nme_Prfx__R_Field_14 = localVariables.newGroupInRecord("pnd_Hold_Nme_Prfx__R_Field_14", "REDEFINE", pnd_Hold_Nme_Prfx);
        pnd_Hold_Nme_Prfx_Pnd_Hold_Nme_Prfx_4 = pnd_Hold_Nme_Prfx__R_Field_14.newFieldInGroup("pnd_Hold_Nme_Prfx_Pnd_Hold_Nme_Prfx_4", "#HOLD-NME-PRFX-4", 
            FieldType.STRING, 4);
        pnd_Hold_Nme_Prfx_Pnd_Hold_Nme_Prfx_Bal = pnd_Hold_Nme_Prfx__R_Field_14.newFieldInGroup("pnd_Hold_Nme_Prfx_Pnd_Hold_Nme_Prfx_Bal", "#HOLD-NME-PRFX-BAL", 
            FieldType.STRING, 4);
        pnd_Hold_State = localVariables.newFieldInRecord("pnd_Hold_State", "#HOLD-STATE", FieldType.STRING, 2);
        pnd_Hold_City_Zip = localVariables.newFieldInRecord("pnd_Hold_City_Zip", "#HOLD-CITY-ZIP", FieldType.STRING, 36);
        pnd_W_State_Code_Tab = localVariables.newFieldInRecord("pnd_W_State_Code_Tab", "#W-STATE-CODE-TAB", FieldType.STRING, 25);

        pnd_W_State_Code_Tab__R_Field_15 = localVariables.newGroupInRecord("pnd_W_State_Code_Tab__R_Field_15", "REDEFINE", pnd_W_State_Code_Tab);
        pnd_W_State_Code_Tab_Pnd_W_State_Code_N = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_State_Code_N", "#W-STATE-CODE-N", 
            FieldType.STRING, 2);
        pnd_W_State_Code_Tab_Pnd_W_State_Filler_1 = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_State_Filler_1", "#W-STATE-FILLER-1", 
            FieldType.STRING, 1);
        pnd_W_State_Code_Tab_Pnd_W_State_Code_A = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_State_Code_A", "#W-STATE-CODE-A", 
            FieldType.STRING, 2);
        pnd_W_State_Code_Tab_Pnd_W_State_Filler_2 = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_State_Filler_2", "#W-STATE-FILLER-2", 
            FieldType.STRING, 1);
        pnd_W_State_Code_Tab_Pnd_W_State_Name = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_State_Name", "#W-STATE-NAME", 
            FieldType.STRING, 15);
        pnd_W_State_Code_Tab_Pnd_W_State_Filler_3 = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_State_Filler_3", "#W-STATE-FILLER-3", 
            FieldType.STRING, 1);
        pnd_W_State_Code_Tab_Pnd_W_Ira_T_Msg_Typ = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_Ira_T_Msg_Typ", "#W-IRA-T-MSG-TYP", 
            FieldType.STRING, 1);
        pnd_W_State_Code_Tab_Pnd_W_State_Filler_4 = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_State_Filler_4", "#W-STATE-FILLER-4", 
            FieldType.STRING, 1);
        pnd_W_State_Code_Tab_Pnd_W_Ira_C_Msg_Typ = pnd_W_State_Code_Tab__R_Field_15.newFieldInGroup("pnd_W_State_Code_Tab_Pnd_W_Ira_C_Msg_Typ", "#W-IRA-C-MSG-TYP", 
            FieldType.STRING, 1);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);
        pnd_Reprint_Ssn_Tiaa_No = localVariables.newFieldInRecord("pnd_Reprint_Ssn_Tiaa_No", "#REPRINT-SSN-TIAA-NO", FieldType.STRING, 19);

        pnd_Reprint_Ssn_Tiaa_No__R_Field_16 = localVariables.newGroupInRecord("pnd_Reprint_Ssn_Tiaa_No__R_Field_16", "REDEFINE", pnd_Reprint_Ssn_Tiaa_No);
        pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Soc_Sec = pnd_Reprint_Ssn_Tiaa_No__R_Field_16.newFieldInGroup("pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Soc_Sec", "#RP-SOC-SEC", 
            FieldType.NUMERIC, 9);
        pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Tiaa_Contr = pnd_Reprint_Ssn_Tiaa_No__R_Field_16.newFieldInGroup("pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Tiaa_Contr", "#RP-TIAA-CONTR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 10);

        pnd_Ac_Pda = localVariables.newGroupInRecord("pnd_Ac_Pda", "#AC-PDA");

        pnd_Ac_Pda_Pnd_Ac_Data = pnd_Ac_Pda.newGroupArrayInGroup("pnd_Ac_Pda_Pnd_Ac_Data", "#AC-DATA", new DbsArrayController(1, 3));
        pnd_Ac_Pda_Pnd_Compressed_Nme = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Compressed_Nme", "#COMPRESSED-NME", FieldType.STRING, 40);
        pnd_Ac_Pda_Pnd_Prefix_Nme = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Prefix_Nme", "#PREFIX-NME", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_First_Nme = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_First_Nme", "#FIRST-NME", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_Middle_Nme = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Middle_Nme", "#MIDDLE-NME", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_Last_Nme = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Last_Nme", "#LAST-NME", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_Title_Nme = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Title_Nme", "#TITLE-NME", FieldType.STRING, 40);
        pnd_Ac_Pda_Pnd_Address_Line_1 = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Address_Line_1", "#ADDRESS-LINE-1", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_Address_Line_2 = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Address_Line_2", "#ADDRESS-LINE-2", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_Address_Line_3 = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Address_Line_3", "#ADDRESS-LINE-3", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_Address_Line_4 = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Address_Line_4", "#ADDRESS-LINE-4", FieldType.STRING, 35);
        pnd_Ac_Pda_Pnd_Postal_Cde = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Postal_Cde", "#POSTAL-CDE", FieldType.STRING, 5);
        pnd_Ac_Pda_Pnd_Org_Nme = pnd_Ac_Pda_Pnd_Ac_Data.newFieldInGroup("pnd_Ac_Pda_Pnd_Org_Nme", "#ORG-NME", FieldType.STRING, 40);
        pnd_Pin_Id = localVariables.newFieldInRecord("pnd_Pin_Id", "#PIN-ID", FieldType.NUMERIC, 12);

        pnd_Ap_Tic_Wk = localVariables.newGroupInRecord("pnd_Ap_Tic_Wk", "#AP-TIC-WK");
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N", "#AP-TIC-STARTDATE-N", FieldType.NUMERIC, 
            8);

        pnd_Ap_Tic_Wk__R_Field_17 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_17", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_A = pnd_Ap_Tic_Wk__R_Field_17.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_A", "#AP-TIC-STARTDATE-A", 
            FieldType.STRING, 8);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_N", "#AP-TIC-ENDDATE-N", FieldType.NUMERIC, 
            8);

        pnd_Ap_Tic_Wk__R_Field_18 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_18", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_A = pnd_Ap_Tic_Wk__R_Field_18.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_A", "#AP-TIC-ENDDATE-A", FieldType.STRING, 
            8);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_N", "#AP-TIC-PERCENTAGE-N", FieldType.NUMERIC, 
            9, 6);

        pnd_Ap_Tic_Wk__R_Field_19 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_19", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_A = pnd_Ap_Tic_Wk__R_Field_19.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_A", "#AP-TIC-PERCENTAGE-A", 
            FieldType.STRING, 9);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_N", "#AP-TIC-POSTDAYS-N", FieldType.NUMERIC, 
            2);

        pnd_Ap_Tic_Wk__R_Field_20 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_20", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_A = pnd_Ap_Tic_Wk__R_Field_20.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_A", "#AP-TIC-POSTDAYS-A", FieldType.STRING, 
            2);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_N", "#AP-TIC-LIMIT-N", FieldType.NUMERIC, 11, 
            2);

        pnd_Ap_Tic_Wk__R_Field_21 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_21", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_A = pnd_Ap_Tic_Wk__R_Field_21.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_A", "#AP-TIC-LIMIT-A", FieldType.STRING, 
            11);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_N", "#AP-TIC-WINDOWDAYS-N", FieldType.NUMERIC, 
            3);

        pnd_Ap_Tic_Wk__R_Field_22 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_22", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_A = pnd_Ap_Tic_Wk__R_Field_22.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_A", "#AP-TIC-WINDOWDAYS-A", 
            FieldType.STRING, 3);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_N", "#AP-TIC-REQDLYWINDOW-N", FieldType.NUMERIC, 
            3);

        pnd_Ap_Tic_Wk__R_Field_23 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_23", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_A = pnd_Ap_Tic_Wk__R_Field_23.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_A", "#AP-TIC-REQDLYWINDOW-A", 
            FieldType.STRING, 3);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_N = pnd_Ap_Tic_Wk.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_N", "#AP-TIC-RECAP-PROV-N", FieldType.NUMERIC, 
            3);

        pnd_Ap_Tic_Wk__R_Field_24 = pnd_Ap_Tic_Wk.newGroupInGroup("pnd_Ap_Tic_Wk__R_Field_24", "REDEFINE", pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_N);
        pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_A = pnd_Ap_Tic_Wk__R_Field_24.newFieldInGroup("pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_A", "#AP-TIC-RECAP-PROV-A", 
            FieldType.STRING, 3);
        pnd_Date_Numeric = localVariables.newFieldInRecord("pnd_Date_Numeric", "#DATE-NUMERIC", FieldType.NUMERIC, 8);

        pnd_Date_Numeric__R_Field_25 = localVariables.newGroupInRecord("pnd_Date_Numeric__R_Field_25", "REDEFINE", pnd_Date_Numeric);
        pnd_Date_Numeric_Pnd_Date_Alpha = pnd_Date_Numeric__R_Field_25.newFieldInGroup("pnd_Date_Numeric_Pnd_Date_Alpha", "#DATE-ALPHA", FieldType.STRING, 
            8);

        pnd_Date_Numeric__R_Field_26 = localVariables.newGroupInRecord("pnd_Date_Numeric__R_Field_26", "REDEFINE", pnd_Date_Numeric);
        pnd_Date_Numeric_Pnd_Date_Numeric_Yyyymm = pnd_Date_Numeric__R_Field_26.newFieldInGroup("pnd_Date_Numeric_Pnd_Date_Numeric_Yyyymm", "#DATE-NUMERIC-YYYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Date_Startdate_D = localVariables.newFieldInRecord("pnd_Date_Startdate_D", "#DATE-STARTDATE-D", FieldType.DATE);
        pnd_Tiaa_Nbr = localVariables.newFieldInRecord("pnd_Tiaa_Nbr", "#TIAA-NBR", FieldType.STRING, 10);

        pnd_Tiaa_Nbr__R_Field_27 = localVariables.newGroupInRecord("pnd_Tiaa_Nbr__R_Field_27", "REDEFINE", pnd_Tiaa_Nbr);
        pnd_Tiaa_Nbr_Pnd_Tiaa_Nbr_Prfx = pnd_Tiaa_Nbr__R_Field_27.newFieldInGroup("pnd_Tiaa_Nbr_Pnd_Tiaa_Nbr_Prfx", "#TIAA-NBR-PRFX", FieldType.STRING, 
            1);
        pnd_Tiaa_Nbr_Pnd_Tiaa_Nbr_Cont = pnd_Tiaa_Nbr__R_Field_27.newFieldInGroup("pnd_Tiaa_Nbr_Pnd_Tiaa_Nbr_Cont", "#TIAA-NBR-CONT", FieldType.STRING, 
            9);
        pnd_Dflt_Amt = localVariables.newFieldInRecord("pnd_Dflt_Amt", "#DFLT-AMT", FieldType.STRING, 9);

        pnd_Dflt_Amt__R_Field_28 = localVariables.newGroupInRecord("pnd_Dflt_Amt__R_Field_28", "REDEFINE", pnd_Dflt_Amt);
        pnd_Dflt_Amt_Pnd_Dflt_Amt_N = pnd_Dflt_Amt__R_Field_28.newFieldInGroup("pnd_Dflt_Amt_Pnd_Dflt_Amt_N", "#DFLT-AMT-N", FieldType.NUMERIC, 9);
        pnd_Dflt_Pct = localVariables.newFieldInRecord("pnd_Dflt_Pct", "#DFLT-PCT", FieldType.STRING, 6);

        pnd_Dflt_Pct__R_Field_29 = localVariables.newGroupInRecord("pnd_Dflt_Pct__R_Field_29", "REDEFINE", pnd_Dflt_Pct);
        pnd_Dflt_Pct_Pnd_Dflt_Pct_N = pnd_Dflt_Pct__R_Field_29.newFieldInGroup("pnd_Dflt_Pct_Pnd_Dflt_Pct_N", "#DFLT-PCT-N", FieldType.NUMERIC, 6);
        pnd_Incr_Pct = localVariables.newFieldInRecord("pnd_Incr_Pct", "#INCR-PCT", FieldType.STRING, 6);

        pnd_Incr_Pct__R_Field_30 = localVariables.newGroupInRecord("pnd_Incr_Pct__R_Field_30", "REDEFINE", pnd_Incr_Pct);
        pnd_Incr_Pct_Pnd_Incr_Pct_N = pnd_Incr_Pct__R_Field_30.newFieldInGroup("pnd_Incr_Pct_Pnd_Incr_Pct_N", "#INCR-PCT-N", FieldType.NUMERIC, 6);
        pnd_Max_Pct = localVariables.newFieldInRecord("pnd_Max_Pct", "#MAX-PCT", FieldType.STRING, 6);

        pnd_Max_Pct__R_Field_31 = localVariables.newGroupInRecord("pnd_Max_Pct__R_Field_31", "REDEFINE", pnd_Max_Pct);
        pnd_Max_Pct_Pnd_Max_Pct_N = pnd_Max_Pct__R_Field_31.newFieldInGroup("pnd_Max_Pct_Pnd_Max_Pct_N", "#MAX-PCT-N", FieldType.NUMERIC, 6);
        pnd_Annuity_Start_Date = localVariables.newFieldInRecord("pnd_Annuity_Start_Date", "#ANNUITY-START-DATE", FieldType.STRING, 8);

        pnd_Annuity_Start_Date__R_Field_32 = localVariables.newGroupInRecord("pnd_Annuity_Start_Date__R_Field_32", "REDEFINE", pnd_Annuity_Start_Date);
        pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N = pnd_Annuity_Start_Date__R_Field_32.newFieldInGroup("pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N", 
            "#ANNUITY-START-DATE-N", FieldType.NUMERIC, 8);
        pnd_Tiaa_Iss_Date = localVariables.newFieldInRecord("pnd_Tiaa_Iss_Date", "#TIAA-ISS-DATE", FieldType.STRING, 8);

        pnd_Tiaa_Iss_Date__R_Field_33 = localVariables.newGroupInRecord("pnd_Tiaa_Iss_Date__R_Field_33", "REDEFINE", pnd_Tiaa_Iss_Date);
        pnd_Tiaa_Iss_Date_Pnd_Tiaa_Iss_Date_N = pnd_Tiaa_Iss_Date__R_Field_33.newFieldInGroup("pnd_Tiaa_Iss_Date_Pnd_Tiaa_Iss_Date_N", "#TIAA-ISS-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Cref_Iss_Date = localVariables.newFieldInRecord("pnd_Cref_Iss_Date", "#CREF-ISS-DATE", FieldType.STRING, 8);

        pnd_Cref_Iss_Date__R_Field_34 = localVariables.newGroupInRecord("pnd_Cref_Iss_Date__R_Field_34", "REDEFINE", pnd_Cref_Iss_Date);
        pnd_Cref_Iss_Date_Pnd_Cref_Iss_Date_N = pnd_Cref_Iss_Date__R_Field_34.newFieldInGroup("pnd_Cref_Iss_Date_Pnd_Cref_Iss_Date_N", "#CREF-ISS-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_Pin_Number_N = localVariables.newFieldInRecord("pnd_Pin_Number_N", "#PIN-NUMBER-N", FieldType.NUMERIC, 12);

        pnd_Pin_Number_N__R_Field_35 = localVariables.newGroupInRecord("pnd_Pin_Number_N__R_Field_35", "REDEFINE", pnd_Pin_Number_N);
        pnd_Pin_Number_N_Pnd_Pin_Number = pnd_Pin_Number_N__R_Field_35.newFieldInGroup("pnd_Pin_Number_N_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 
            12);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 8);
        pnd_Extract_Record = localVariables.newFieldInRecord("pnd_Extract_Record", "#EXTRACT-RECORD", FieldType.BOOLEAN, 1);
        pnd_Reprint_Rec_Found = localVariables.newFieldInRecord("pnd_Reprint_Rec_Found", "#REPRINT-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Business_Date = localVariables.newFieldInRecord("pnd_Business_Date", "#BUSINESS-DATE", FieldType.NUMERIC, 8);

        pnd_Business_Date__R_Field_36 = localVariables.newGroupInRecord("pnd_Business_Date__R_Field_36", "REDEFINE", pnd_Business_Date);
        pnd_Business_Date_Pnd_Cc = pnd_Business_Date__R_Field_36.newFieldInGroup("pnd_Business_Date_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Business_Date_Pnd_Yy = pnd_Business_Date__R_Field_36.newFieldInGroup("pnd_Business_Date_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Business_Date_Pnd_Mm = pnd_Business_Date__R_Field_36.newFieldInGroup("pnd_Business_Date_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Business_Date_Pnd_Dd = pnd_Business_Date__R_Field_36.newFieldInGroup("pnd_Business_Date_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Prem_Found = localVariables.newFieldInRecord("pnd_Prem_Found", "#PREM-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ira_Except_Found = localVariables.newFieldInRecord("pnd_Ira_Except_Found", "#IRA-EXCEPT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Date_Diff = localVariables.newFieldInRecord("pnd_Date_Diff", "#DATE-DIFF", FieldType.NUMERIC, 8);

        pnd_Date_Diff__R_Field_37 = localVariables.newGroupInRecord("pnd_Date_Diff__R_Field_37", "REDEFINE", pnd_Date_Diff);
        pnd_Date_Diff_Pnd_Date_Diff_Mm = pnd_Date_Diff__R_Field_37.newFieldInGroup("pnd_Date_Diff_Pnd_Date_Diff_Mm", "#DATE-DIFF-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Diff_Pnd_Date_Diff_Dd = pnd_Date_Diff__R_Field_37.newFieldInGroup("pnd_Date_Diff_Pnd_Date_Diff_Dd", "#DATE-DIFF-DD", FieldType.NUMERIC, 
            2);
        pnd_Date_Diff_Pnd_Date_Diff_Cc = pnd_Date_Diff__R_Field_37.newFieldInGroup("pnd_Date_Diff_Pnd_Date_Diff_Cc", "#DATE-DIFF-CC", FieldType.NUMERIC, 
            2);
        pnd_Date_Diff_Pnd_Date_Diff_Yy = pnd_Date_Diff__R_Field_37.newFieldInGroup("pnd_Date_Diff_Pnd_Date_Diff_Yy", "#DATE-DIFF-YY", FieldType.NUMERIC, 
            2);
        pnd_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Date_Yyyymmdd__R_Field_38 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_38", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Cc_1 = pnd_Date_Yyyymmdd__R_Field_38.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Cc_1", "#DATE-CC-1", FieldType.NUMERIC, 
            2);
        pnd_Date_Yyyymmdd_Pnd_Date_Yy_1 = pnd_Date_Yyyymmdd__R_Field_38.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yy_1", "#DATE-YY-1", FieldType.NUMERIC, 
            2);
        pnd_Date_Yyyymmdd_Pnd_Date_Mm_1 = pnd_Date_Yyyymmdd__R_Field_38.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Mm_1", "#DATE-MM-1", FieldType.NUMERIC, 
            2);
        pnd_Date_Yyyymmdd_Pnd_Date_Dd_1 = pnd_Date_Yyyymmdd__R_Field_38.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Dd_1", "#DATE-DD-1", FieldType.NUMERIC, 
            2);
        pnd_Release_Ind = localVariables.newFieldInRecord("pnd_Release_Ind", "#RELEASE-IND", FieldType.NUMERIC, 1);
        pnd_Da_Tiaa_Found = localVariables.newFieldInRecord("pnd_Da_Tiaa_Found", "#DA-TIAA-FOUND", FieldType.BOOLEAN, 1);
        pnd_Da_Cref_Found = localVariables.newFieldInRecord("pnd_Da_Cref_Found", "#DA-CREF-FOUND", FieldType.BOOLEAN, 1);
        pnd_Rc_Or_Rcp = localVariables.newFieldInRecord("pnd_Rc_Or_Rcp", "#RC-OR-RCP", FieldType.BOOLEAN, 1);
        pnd_Indiv_Product = localVariables.newFieldInRecord("pnd_Indiv_Product", "#INDIV-PRODUCT", FieldType.BOOLEAN, 1);
        pnd_Group_Product = localVariables.newFieldInRecord("pnd_Group_Product", "#GROUP-PRODUCT", FieldType.BOOLEAN, 1);
        pnd_Product = localVariables.newFieldInRecord("pnd_Product", "#PRODUCT", FieldType.STRING, 10);
        pnd_Team = localVariables.newFieldInRecord("pnd_Team", "#TEAM", FieldType.STRING, 8);
        pnd_Temp_Date = localVariables.newFieldInRecord("pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 8);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Non_T_C_Administered_Ppg = localVariables.newFieldInRecord("pnd_Non_T_C_Administered_Ppg", "#NON-T-C-ADMINISTERED-PPG", FieldType.BOOLEAN, 
            1);
        pnd_Non_T_C_Administered_Plan = localVariables.newFieldInRecord("pnd_Non_T_C_Administered_Plan", "#NON-T-C-ADMINISTERED-PLAN", FieldType.BOOLEAN, 
            1);
        pnd_Mail_Date = localVariables.newFieldInRecord("pnd_Mail_Date", "#MAIL-DATE", FieldType.STRING, 8);
        pnd_Search_Key = localVariables.newFieldInRecord("pnd_Search_Key", "#SEARCH-KEY", FieldType.STRING, 29);

        pnd_Search_Key__R_Field_39 = localVariables.newGroupInRecord("pnd_Search_Key__R_Field_39", "REDEFINE", pnd_Search_Key);
        pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind = pnd_Search_Key__R_Field_39.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind", "#KY-ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr = pnd_Search_Key__R_Field_39.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr", "#KY-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id = pnd_Search_Key__R_Field_39.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id", "#KY-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Search_Key_Pnd_Ky_Entry_Cde = pnd_Search_Key__R_Field_39.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Cde", "#KY-ENTRY-CDE", FieldType.STRING, 
            20);

        pnd_Search_Key__R_Field_40 = pnd_Search_Key__R_Field_39.newGroupInGroup("pnd_Search_Key__R_Field_40", "REDEFINE", pnd_Search_Key_Pnd_Ky_Entry_Cde);
        pnd_Search_Key_Pnd_Ky_Plan_Number = pnd_Search_Key__R_Field_40.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Plan_Number", "#KY-PLAN-NUMBER", FieldType.STRING, 
            6);
        pnd_Search_Key_Pnd_Ky_Subplan_Number = pnd_Search_Key__R_Field_40.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Subplan_Number", "#KY-SUBPLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Search_Key_Pnd_Ky_Addl_Cde = pnd_Search_Key__R_Field_40.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Addl_Cde", "#KY-ADDL-CDE", FieldType.STRING, 
            8);

        pnd_Search_Key__R_Field_41 = pnd_Search_Key__R_Field_40.newGroupInGroup("pnd_Search_Key__R_Field_41", "REDEFINE", pnd_Search_Key_Pnd_Ky_Addl_Cde);
        pnd_Search_Key_Pnd_Ky_Pkg_Cntrl_Type = pnd_Search_Key__R_Field_41.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Pkg_Cntrl_Type", "#KY-PKG-CNTRL-TYPE", 
            FieldType.STRING, 1);
        pnd_Search_Key_Pnd_Ky_Addl_Cde_2 = pnd_Search_Key__R_Field_41.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Addl_Cde_2", "#KY-ADDL-CDE-2", FieldType.STRING, 
            7);

        pnd_Trigger_Table = localVariables.newGroupArrayInRecord("pnd_Trigger_Table", "#TRIGGER-TABLE", new DbsArrayController(1, 300));
        pnd_Trigger_Table_Pnd_T_Plan_Number = pnd_Trigger_Table.newFieldInGroup("pnd_Trigger_Table_Pnd_T_Plan_Number", "#T-PLAN-NUMBER", FieldType.STRING, 
            6);
        pnd_Trigger_Table_Pnd_T_Subplan_Number = pnd_Trigger_Table.newFieldInGroup("pnd_Trigger_Table_Pnd_T_Subplan_Number", "#T-SUBPLAN-NUMBER", FieldType.STRING, 
            6);
        pnd_Trigger_Table_Pnd_T_Pkg_Cntrl_Type = pnd_Trigger_Table.newFieldInGroup("pnd_Trigger_Table_Pnd_T_Pkg_Cntrl_Type", "#T-PKG-CNTRL-TYPE", FieldType.STRING, 
            1);
        pnd_Trigger_Table_Pnd_T_Pkg_Trigger = pnd_Trigger_Table.newFieldInGroup("pnd_Trigger_Table_Pnd_T_Pkg_Trigger", "#T-PKG-TRIGGER", FieldType.STRING, 
            1);

        pnd_Suppress_Table = localVariables.newGroupArrayInRecord("pnd_Suppress_Table", "#SUPPRESS-TABLE", new DbsArrayController(1, 300));
        pnd_Suppress_Table_Pnd_S_Plan_Number = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Plan_Number", "#S-PLAN-NUMBER", FieldType.STRING, 
            6);
        pnd_Suppress_Table_Pnd_S_Subplan_Number = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Subplan_Number", "#S-SUBPLAN-NUMBER", FieldType.STRING, 
            6);
        pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type", "#S-PKG-CNTRL-TYPE", FieldType.STRING, 
            1);
        pnd_Suppress_Table_Pnd_S_Enr_Source = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Enr_Source", "#S-ENR-SOURCE", FieldType.STRING, 
            1);
        pnd_Suppress_Table_Pnd_S_Pckg_Type = pnd_Suppress_Table.newFieldInGroup("pnd_Suppress_Table_Pnd_S_Pckg_Type", "#S-PCKG-TYPE", FieldType.STRING, 
            1);
        pnd_I_S = localVariables.newFieldInRecord("pnd_I_S", "#I-S", FieldType.NUMERIC, 3);
        pnd_I_T = localVariables.newFieldInRecord("pnd_I_T", "#I-T", FieldType.NUMERIC, 3);
        pnd_Fund_Count = localVariables.newFieldInRecord("pnd_Fund_Count", "#FUND-COUNT", FieldType.NUMERIC, 4);
        pnd_Extract_Contract_Cnt = localVariables.newFieldInRecord("pnd_Extract_Contract_Cnt", "#EXTRACT-CONTRACT-CNT", FieldType.NUMERIC, 9);
        pnd_Legacy_Contract_Cnt = localVariables.newFieldInRecord("pnd_Legacy_Contract_Cnt", "#LEGACY-CONTRACT-CNT", FieldType.NUMERIC, 9);
        pnd_W_Check_State = localVariables.newFieldInRecord("pnd_W_Check_State", "#W-CHECK-STATE", FieldType.STRING, 2);
        pnd_Except_Reason = localVariables.newFieldInRecord("pnd_Except_Reason", "#EXCEPT-REASON", FieldType.STRING, 35);
        pnd_Exception_Cnt = localVariables.newFieldInRecord("pnd_Exception_Cnt", "#EXCEPTION-CNT", FieldType.NUMERIC, 6);
        pnd_Exc_Line_Cnt = localVariables.newFieldInRecord("pnd_Exc_Line_Cnt", "#EXC-LINE-CNT", FieldType.NUMERIC, 3);
        pnd_Exc_Report_Type = localVariables.newFieldInRecord("pnd_Exc_Report_Type", "#EXC-REPORT-TYPE", FieldType.STRING, 7);
        pnd_Close_Plan_Interface = localVariables.newFieldInRecord("pnd_Close_Plan_Interface", "#CLOSE-PLAN-INTERFACE", FieldType.STRING, 1);
        pnd_Inst_Oia_Is_Type = localVariables.newFieldInRecord("pnd_Inst_Oia_Is_Type", "#INST-OIA-IS-TYPE", FieldType.STRING, 2);

        pnd_Inst_Oia_Is_Type__R_Field_42 = localVariables.newGroupInRecord("pnd_Inst_Oia_Is_Type__R_Field_42", "REDEFINE", pnd_Inst_Oia_Is_Type);
        pnd_Inst_Oia_Is_Type_Pnd_Inst_Oia_Is_Type_1 = pnd_Inst_Oia_Is_Type__R_Field_42.newFieldInGroup("pnd_Inst_Oia_Is_Type_Pnd_Inst_Oia_Is_Type_1", 
            "#INST-OIA-IS-TYPE-1", FieldType.STRING, 1);
        pnd_Inst_Oia_Is_Type_Pnd_Inst_Oia_Is_Type_2 = pnd_Inst_Oia_Is_Type__R_Field_42.newFieldInGroup("pnd_Inst_Oia_Is_Type_Pnd_Inst_Oia_Is_Type_2", 
            "#INST-OIA-IS-TYPE-2", FieldType.STRING, 1);
        pnd_Int_First_Time = localVariables.newFieldInRecord("pnd_Int_First_Time", "#INT-FIRST-TIME", FieldType.STRING, 1);
        pnd_Con_First_Time = localVariables.newFieldInRecord("pnd_Con_First_Time", "#CON-FIRST-TIME", FieldType.STRING, 1);
        pnd_Close_Cntct_Interface = localVariables.newFieldInRecord("pnd_Close_Cntct_Interface", "#CLOSE-CNTCT-INTERFACE", FieldType.BOOLEAN, 1);
        pnd_Sgrd_Contct_Call_Cnt = localVariables.newFieldInRecord("pnd_Sgrd_Contct_Call_Cnt", "#SGRD-CONTCT-CALL-CNT", FieldType.NUMERIC, 9);
        pnd_Hold_City_St = localVariables.newFieldInRecord("pnd_Hold_City_St", "#HOLD-CITY-ST", FieldType.STRING, 35);
        pnd_Hold_Cstate = localVariables.newFieldInRecord("pnd_Hold_Cstate", "#HOLD-CSTATE", FieldType.STRING, 3);
        pnd_Hold_Addr = localVariables.newFieldInRecord("pnd_Hold_Addr", "#HOLD-ADDR", FieldType.STRING, 35);
        pnd_Hold_Zip = localVariables.newFieldInRecord("pnd_Hold_Zip", "#HOLD-ZIP", FieldType.STRING, 9);

        pnd_Hold_Zip__R_Field_43 = localVariables.newGroupInRecord("pnd_Hold_Zip__R_Field_43", "REDEFINE", pnd_Hold_Zip);
        pnd_Hold_Zip_Pnd_Hold_Zip5 = pnd_Hold_Zip__R_Field_43.newFieldInGroup("pnd_Hold_Zip_Pnd_Hold_Zip5", "#HOLD-ZIP5", FieldType.STRING, 5);
        pnd_Hold_Zip_Pnd_Hold_Zip4 = pnd_Hold_Zip__R_Field_43.newFieldInGroup("pnd_Hold_Zip_Pnd_Hold_Zip4", "#HOLD-ZIP4", FieldType.STRING, 4);
        pnd_Hold_City_State_Zip = localVariables.newFieldInRecord("pnd_Hold_City_State_Zip", "#HOLD-CITY-STATE-ZIP", FieldType.STRING, 35);
        pnd_Char = localVariables.newFieldInRecord("pnd_Char", "#CHAR", FieldType.STRING, 1);
        pnd_Dec1 = localVariables.newFieldInRecord("pnd_Dec1", "#DEC1", FieldType.STRING, 1);
        pnd_Tiaa_Rate = localVariables.newFieldInRecord("pnd_Tiaa_Rate", "#TIAA-RATE", FieldType.STRING, 8);
        pnd_Hold_Interest_Rt = localVariables.newFieldInRecord("pnd_Hold_Interest_Rt", "#HOLD-INTEREST-RT", FieldType.STRING, 7);

        pnd_Hold_Interest_Rt__R_Field_44 = localVariables.newGroupInRecord("pnd_Hold_Interest_Rt__R_Field_44", "REDEFINE", pnd_Hold_Interest_Rt);
        pnd_Hold_Interest_Rt_Pnd_Hold_Int_Rate_Print = pnd_Hold_Interest_Rt__R_Field_44.newFieldInGroup("pnd_Hold_Interest_Rt_Pnd_Hold_Int_Rate_Print", 
            "#HOLD-INT-RATE-PRINT", FieldType.STRING, 6);
        pnd_Hold_Interest_Rt_Pnd_Hold_Int_Rate_Num1 = pnd_Hold_Interest_Rt__R_Field_44.newFieldInGroup("pnd_Hold_Interest_Rt_Pnd_Hold_Int_Rate_Num1", 
            "#HOLD-INT-RATE-NUM1", FieldType.STRING, 1);
        pnd_Hold_Effective_Dt = localVariables.newFieldInRecord("pnd_Hold_Effective_Dt", "#HOLD-EFFECTIVE-DT", FieldType.NUMERIC, 8);

        pnd_Hold_Effective_Dt__R_Field_45 = localVariables.newGroupInRecord("pnd_Hold_Effective_Dt__R_Field_45", "REDEFINE", pnd_Hold_Effective_Dt);
        pnd_Hold_Effective_Dt_Pnd_Hold_Effective_Dt_A = pnd_Hold_Effective_Dt__R_Field_45.newFieldInGroup("pnd_Hold_Effective_Dt_Pnd_Hold_Effective_Dt_A", 
            "#HOLD-EFFECTIVE-DT-A", FieldType.STRING, 8);
        pnd_Icap_Tnt_Companion = localVariables.newFieldInRecord("pnd_Icap_Tnt_Companion", "#ICAP-TNT-COMPANION", FieldType.STRING, 1);
        pnd_Tiaa_Contact_Nbr = localVariables.newFieldInRecord("pnd_Tiaa_Contact_Nbr", "#TIAA-CONTACT-NBR", FieldType.STRING, 10);
        pnd_Cref_Contact_Nbr = localVariables.newFieldInRecord("pnd_Cref_Contact_Nbr", "#CREF-CONTACT-NBR", FieldType.STRING, 10);
        pnd_Suppress_Welc_Pkg = localVariables.newFieldInRecord("pnd_Suppress_Welc_Pkg", "#SUPPRESS-WELC-PKG", FieldType.BOOLEAN, 1);
        pnd_Suppress_Legal_Pkg = localVariables.newFieldInRecord("pnd_Suppress_Legal_Pkg", "#SUPPRESS-LEGAL-PKG", FieldType.BOOLEAN, 1);
        pnd_Welc_Fund_Trigger = localVariables.newFieldInRecord("pnd_Welc_Fund_Trigger", "#WELC-FUND-TRIGGER", FieldType.BOOLEAN, 1);
        pnd_Welc_Force_Inst_Owned = localVariables.newFieldInRecord("pnd_Welc_Force_Inst_Owned", "#WELC-FORCE-INST-OWNED", FieldType.BOOLEAN, 1);
        pnd_Rp_Text_Udf_1 = localVariables.newFieldInRecord("pnd_Rp_Text_Udf_1", "#RP-TEXT-UDF-1", FieldType.STRING, 10);
        pnd_Nbr = localVariables.newFieldInRecord("pnd_Nbr", "#NBR", FieldType.NUMERIC, 2);
        pnd_Spec_Fund_Ind = localVariables.newFieldInRecord("pnd_Spec_Fund_Ind", "#SPEC-FUND-IND", FieldType.STRING, 3);
        pnd_Cal_Country_Cd = localVariables.newFieldInRecord("pnd_Cal_Country_Cd", "#CAL-COUNTRY-CD", FieldType.STRING, 2);
        pnd_Cal_Country_Name = localVariables.newFieldInRecord("pnd_Cal_Country_Name", "#CAL-COUNTRY-NAME", FieldType.STRING, 25);
        pnd_Cal_Return_Code = localVariables.newFieldInRecord("pnd_Cal_Return_Code", "#CAL-RETURN-CODE", FieldType.STRING, 4);
        pnd_Cal_Return_Msg = localVariables.newFieldInRecord("pnd_Cal_Return_Msg", "#CAL-RETURN-MSG", FieldType.STRING, 30);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Date_1 = localVariables.newFieldInRecord("pnd_Date_1", "#DATE-1", FieldType.DATE);
        pnd_Hold_Orig_Tiaa_Doi = localVariables.newFieldInRecord("pnd_Hold_Orig_Tiaa_Doi", "#HOLD-ORIG-TIAA-DOI", FieldType.NUMERIC, 8);

        pnd_Hold_Orig_Tiaa_Doi__R_Field_46 = localVariables.newGroupInRecord("pnd_Hold_Orig_Tiaa_Doi__R_Field_46", "REDEFINE", pnd_Hold_Orig_Tiaa_Doi);
        pnd_Hold_Orig_Tiaa_Doi_Pnd_Hold_Orig_Tiaa_Doi_A = pnd_Hold_Orig_Tiaa_Doi__R_Field_46.newFieldInGroup("pnd_Hold_Orig_Tiaa_Doi_Pnd_Hold_Orig_Tiaa_Doi_A", 
            "#HOLD-ORIG-TIAA-DOI-A", FieldType.STRING, 8);
        pnd_Hold_Orig_Cref_Doi = localVariables.newFieldInRecord("pnd_Hold_Orig_Cref_Doi", "#HOLD-ORIG-CREF-DOI", FieldType.NUMERIC, 8);

        pnd_Hold_Orig_Cref_Doi__R_Field_47 = localVariables.newGroupInRecord("pnd_Hold_Orig_Cref_Doi__R_Field_47", "REDEFINE", pnd_Hold_Orig_Cref_Doi);
        pnd_Hold_Orig_Cref_Doi_Pnd_Hold_Orig_Cref_Doi_A = pnd_Hold_Orig_Cref_Doi__R_Field_47.newFieldInGroup("pnd_Hold_Orig_Cref_Doi_Pnd_Hold_Orig_Cref_Doi_A", 
            "#HOLD-ORIG-CREF-DOI-A", FieldType.STRING, 8);

        pnd_In_Translte_File = localVariables.newGroupInRecord("pnd_In_Translte_File", "#IN-TRANSLTE-FILE");
        pnd_In_Translte_File_Pnd_In_Translte_Ticker = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Ticker", "#IN-TRANSLTE-TICKER", 
            FieldType.STRING, 10);
        pnd_In_Translte_File_Pnd_F1 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name", "#IN-TRANSLTE-TICKER-NAME", 
            FieldType.STRING, 26);
        pnd_In_Translte_File_Pnd_F2 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq", "#IN-TRANSLTE-FUND-SEQ", 
            FieldType.NUMERIC, 2);
        pnd_In_Translte_File_Pnd_F3 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Fund_First_4 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Fund_First_4", 
            "#IN-TRANSLTE-FUND-FIRST-4", FieldType.STRING, 4);
        pnd_In_Translte_File_Pnd_F5 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Fund_Code = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Fund_Code", "#IN-TRANSLTE-FUND-CODE", 
            FieldType.STRING, 3);

        pnd_Translte_File = localVariables.newGroupArrayInRecord("pnd_Translte_File", "#TRANSLTE-FILE", new DbsArrayController(1, 15));
        pnd_Translte_File_Pnd_Translte_Ticker = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker", "#TRANSLTE-TICKER", FieldType.STRING, 
            10);
        pnd_Translte_File_Pnd_F1 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Ticker_Name = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker_Name", "#TRANSLTE-TICKER-NAME", 
            FieldType.STRING, 26);
        pnd_Translte_File_Pnd_F2 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Seq = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Seq", "#TRANSLTE-FUND-SEQ", FieldType.NUMERIC, 
            2);
        pnd_Translte_File_Pnd_F3 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_First_4 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_First_4", "#TRANSLTE-FUND-FIRST-4", 
            FieldType.STRING, 4);
        pnd_Translte_File_Pnd_F4 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Code = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Code", "#TRANSLTE-FUND-CODE", 
            FieldType.STRING, 3);
        pnd_Translte_File_Pnd_F5 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Error_Message = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Error_Message", "#TRANSLTE-ERROR-MESSAGE", 
            FieldType.STRING, 40);
        pnd_Wk_Inst_State = localVariables.newFieldInRecord("pnd_Wk_Inst_State", "#WK-INST-STATE", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();
        vw_reprint_Fl.reset();
        vw_table_Entry.reset();

        ldaAppl160.initializeValues();
        ldaAppl170.initializeValues();
        ldaAppl180.initializeValues();
        ldaY2datebw.initializeValues();

        localVariables.reset();
        pnd_Mail_Date.setInitialValue(Global.getDATN());
        pnd_Close_Plan_Interface.setInitialValue("N");
        pnd_Int_First_Time.setInitialValue("Y");
        pnd_Con_First_Time.setInitialValue("Y");
        pnd_Char.setInitialValue("#");
        pnd_Dec1.setInitialValue(".");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb100() throws Exception
    {
        super("Appb100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Appb100|Main");
        OnErrorManager.pushEvent("APPB100", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                getReports().definePrinter(2, "NOT DEFINED");                                                                                                             //Natural: DEFINE PRINTER ( 1 )
                //*  KG TNGSUB - CHANGED BUSINESS DATE LOGIC TO USE CURRENT PROCESS DATE                                                                                  //Natural: FORMAT ( 2 ) LS = 133 PS = 52
                ldaAppl170.getPnd_Current_Date_Yyyymmdd().setValue(Global.getDATN());                                                                                     //Natural: MOVE *DATN TO #CURRENT-DATE-YYYYMMDD #BUSINESS-DATE #DATE-HOLD-YYYYMMDD
                pnd_Business_Date.setValue(Global.getDATN());
                ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(Global.getDATN());
                ldaAppl170.getPnd_Hold_Extract_Date().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Yymmdd());                                                          //Natural: ASSIGN #HOLD-EXTRACT-DATE = #DATE-YYMMDD
                //*  K.G. 09/20 INCLUDE SYSTEM DATE IN THE WELCOME PACKAGE
                //*  START OF CREF REA                                             /* CREA
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 3 #IN-TRANSLTE-FILE
                while (condition(getWorkFiles().read(3, pnd_In_Translte_File)))
                {
                    if (condition(!(pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq.greater(0))))                                                                           //Natural: ACCEPT IF #IN-TRANSLTE-FUND-SEQ > 00
                    {
                        continue;
                    }
                    pnd_Fund_Count.nadd(1);                                                                                                                               //Natural: ASSIGN #FUND-COUNT := #FUND-COUNT + 1
                    pnd_Translte_File_Pnd_Translte_Ticker.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Ticker);                                 //Natural: ASSIGN #TRANSLTE-TICKER ( #FUND-COUNT ) := #IN-TRANSLTE-TICKER
                    pnd_Translte_File_Pnd_Translte_Ticker_Name.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name);                       //Natural: ASSIGN #TRANSLTE-TICKER-NAME ( #FUND-COUNT ) := #IN-TRANSLTE-TICKER-NAME
                    pnd_Translte_File_Pnd_Translte_Fund_Seq.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq);                             //Natural: ASSIGN #TRANSLTE-FUND-SEQ ( #FUND-COUNT ) := #IN-TRANSLTE-FUND-SEQ
                    pnd_Translte_File_Pnd_Translte_Fund_First_4.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Fund_First_4);                     //Natural: ASSIGN #TRANSLTE-FUND-FIRST-4 ( #FUND-COUNT ) := #IN-TRANSLTE-FUND-FIRST-4
                    pnd_Translte_File_Pnd_Translte_Fund_Code.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Fund_Code);                           //Natural: ASSIGN #TRANSLTE-FUND-CODE ( #FUND-COUNT ) := #IN-TRANSLTE-FUND-CODE
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Fund_Count);                                                                                                                //Natural: WRITE '=' #FUND-COUNT
                if (Global.isEscape()) return;
                if (condition(pnd_Fund_Count.equals(getZero()) || pnd_Fund_Count.greater(15)))                                                                            //Natural: IF #FUND-COUNT = 0 OR #FUND-COUNT > 15
                {
                    getReports().write(0, "**********************************************************");                                                                  //Natural: WRITE '**********************************************************'
                    if (Global.isEscape()) return;
                    getReports().write(0, "*  THE TRANSLATION FILE IS EMPTY OR EXCEEDED 15 ENTRIES  *");                                                                  //Natural: WRITE '*  THE TRANSLATION FILE IS EMPTY OR EXCEEDED 15 ENTRIES  *'
                    if (Global.isEscape()) return;
                    getReports().write(0, "**********************************************************");                                                                  //Natural: WRITE '**********************************************************'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(12);  if (true) return;                                                                                                             //Natural: TERMINATE 12
                    //*  SGRD KG
                }                                                                                                                                                         //Natural: END-IF
                //*  END OF CREF REA
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 2 )
                //*  SGRD KG MOVED TO CORRECT PLACEMENT AFTER READ OF INPUT PARM
                //*  K.G. 03/20 ACIS VOLUME CHANGES
                short decideConditionsMet2530 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM;//Natural: VALUE 'WELCOME'
                if (condition((pnd_Input_Parm.equals("WELCOME"))))
                {
                    decideConditionsMet2530++;
                    pnd_Release_Ind.setValue(3);                                                                                                                          //Natural: ASSIGN #RELEASE-IND := 3
                }                                                                                                                                                         //Natural: VALUE 'LEGAL'
                else if (condition((pnd_Input_Parm.equals("LEGAL"))))
                {
                    decideConditionsMet2530++;
                    pnd_Release_Ind.setValue(4);                                                                                                                          //Natural: ASSIGN #RELEASE-IND := 4
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  TO LOAD TRIGGER AND SUPPRESS PKG PRINTING FROM APP-TABLE-ENTRY
                //*  TNGSUB2 LS
                                                                                                                                                                          //Natural: PERFORM LOAD-TRIGGER-SUPPRESS-TABLE
                sub_Load_Trigger_Suppress_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                vw_annty_Actvty_Prap_View.startDatabaseFind                                                                                                               //Natural: FIND ANNTY-ACTVTY-PRAP-VIEW WITH AP-RELEASE-IND = #RELEASE-IND
                (
                "FINDUPD",
                new Wc[] { new Wc("AP_RELEASE_IND", "=", pnd_Release_Ind, WcType.WITH) }
                );
                FINDUPD:
                while (condition(vw_annty_Actvty_Prap_View.readNextRow("FINDUPD", true)))
                {
                    vw_annty_Actvty_Prap_View.setIfNotFoundControlFlag(false);
                    if (condition(vw_annty_Actvty_Prap_View.getAstCOUNTER().equals(0)))                                                                                   //Natural: IF NO RECORDS FOUND
                    {
                        getReports().display(0, "********  NO PRAP RECORDS FOUND  ********");                                                                             //Natural: DISPLAY '********  NO PRAP RECORDS FOUND  ********'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    //*   LS (TIC)
                    pnd_Ap_Tic_Wk.reset();                                                                                                                                //Natural: RESET #AP-TIC-WK
                    //*  KG SRK - ADDED RESET START
                    pnd_Ac_Pda.resetInitial();                                                                                                                            //Natural: RESET INITIAL #AC-PDA
                    ldaAppl170.getPnd_Work_Fields().resetInitial();                                                                                                       //Natural: RESET INITIAL #WORK-FIELDS
                    ldaAppl160.getDoc_Ext_File().resetInitial();                                                                                                          //Natural: RESET INITIAL DOC-EXT-FILE
                    pdaAppa100.getPnd_Par_Parameter_Area().resetInitial();                                                                                                //Natural: RESET INITIAL #PAR-PARAMETER-AREA
                    //*  KG SRK - ADDED RESET END
                    pdaAppa200.getPnd_Par_Parameter_Area_2().resetInitial();                                                                                              //Natural: RESET INITIAL #PAR-PARAMETER-AREA-2
                    ldaAppl170.getPnd_Recs_Selected_Count().setValue(vw_annty_Actvty_Prap_View.getAstCOUNTER());                                                          //Natural: MOVE *COUNTER TO #RECS-SELECTED-COUNT
                    if (condition(vw_annty_Actvty_Prap_View.getAstCOUNTER().equals(1)))                                                                                   //Natural: IF *COUNTER = 1
                    {
                        ldaAppl170.getPnd_Read_Count().setValue(vw_annty_Actvty_Prap_View.getAstNUMBER());                                                                //Natural: MOVE *NUMBER TO #READ-COUNT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  KG TNGSUB - BLOCK ALL LEGACY CONTRACT RECORDS FROM BEING PROCESSED.
                    //*  KG TNGSUB START
                    if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.notEquals("SGRD")))                                                                                 //Natural: IF AP-COLL-CODE NE 'SGRD'
                    {
                        pnd_Legacy_Contract_Cnt.nadd(1);                                                                                                                  //Natural: ADD 1 TO #LEGACY-CONTRACT-CNT
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  KG TNGSUB END
                    }                                                                                                                                                     //Natural: END-IF
                    //*  KG TNGSUB - BLOCK ALL CONTRACTS WITHOUT A PIN FROM BEING PROCESSED
                    //*  AND SEND THEM TO THE EXCEPTION REPORT
                    //*  IF AP-PIN-NBR NE MASK(NNNNNNN) OR       /* KG TNGSUB START   /* PINE
                    //*  KG TNGSUB START   /* PINE
                    if (condition(! (DbsUtil.maskMatches(annty_Actvty_Prap_View_Ap_Pin_Nbr,"NNNNNNNNNNNN")) || annty_Actvty_Prap_View_Ap_Pin_Nbr.equals(getZero())))      //Natural: IF AP-PIN-NBR NE MASK ( NNNNNNNNNNNN ) OR AP-PIN-NBR EQ 0
                    {
                        pnd_Exception_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EXCEPTION-CNT
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT
                        sub_Exception_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  KG TNGSUB END
                    }                                                                                                                                                     //Natural: END-IF
                    //*  8-7-08 KG PROD FIX
                    pnd_Non_T_C_Administered_Plan.reset();                                                                                                                //Natural: RESET #NON-T-C-ADMINISTERED-PLAN
                    //*  MAYO KG START
                    //*  MAYO KG END
                    DbsUtil.callnat(Scin8540.class , getCurrentProcessState(), annty_Actvty_Prap_View_Ap_Sgrd_Plan_No, pnd_Non_T_C_Administered_Plan);                    //Natural: CALLNAT 'SCIN8540' AP-SGRD-PLAN-NO #NON-T-C-ADMINISTERED-PLAN
                    if (condition(Global.isEscape())) return;
                    //*  MAYO
                    //*  BJD RHSP
                    if (condition(pnd_Non_T_C_Administered_Plan.getBoolean() || (annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("B")))) //Natural: IF #NON-T-C-ADMINISTERED-PLAN OR ( AP-LOB = 'D' AND AP-LOB-TYPE = 'B' )
                    {
                        if (condition(annty_Actvty_Prap_View_Ap_Ppg_Team_Cde.equals(" ")))                                                                                //Natural: IF AP-PPG-TEAM-CDE = ' '
                        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CONTRACT-TYPE
                            sub_Determine_Contract_Type();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-RECORDS
                        sub_Update_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ac_Pda.resetInitial();                                                                                                                        //Natural: RESET INITIAL #AC-PDA #WORK-FIELDS DOC-EXT-FILE #PAR-PARAMETER-AREA #PAR-PARAMETER-AREA-2
                        ldaAppl170.getPnd_Work_Fields().resetInitial();
                        ldaAppl160.getDoc_Ext_File().resetInitial();
                        pdaAppa100.getPnd_Par_Parameter_Area().resetInitial();
                        pdaAppa200.getPnd_Par_Parameter_Area_2().resetInitial();
                        //*  RHSP
                        if (condition(pnd_Non_T_C_Administered_Plan.getBoolean()))                                                                                        //Natural: IF #NON-T-C-ADMINISTERED-PLAN
                        {
                            //*  DSP4 - 3PRK ACCOUNTS
                            if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No.getSubstring(1,1).equals("M")))                                                          //Natural: IF SUBSTR ( AP-SGRD-PLAN-NO,1,1 ) = 'M'
                            {
                                //*  DSP4
                                getReports().write(0, "DATASHARE 3PRK");                                                                                                  //Natural: WRITE 'DATASHARE 3PRK'
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*  DSP4
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  DSP4
                                getReports().write(0, "MAYO");                                                                                                            //Natural: WRITE 'MAYO'
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*  DSP4
                            }                                                                                                                                             //Natural: END-IF
                            getReports().write(0, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Coll_Code,annty_Actvty_Prap_View_Ap_Lob,                //Natural: WRITE AP-TIAA-CNTRCT AP-COLL-CODE AP-LOB AP-PPG-TEAM-CDE AP-COR-LAST-NME AP-COR-FIRST-NME
                                annty_Actvty_Prap_View_Ap_Ppg_Team_Cde,annty_Actvty_Prap_View_Ap_Cor_Last_Nme,annty_Actvty_Prap_View_Ap_Cor_First_Nme);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  BJD RHSP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  BJD RHSP
                            //*  BJD RHSP
                            //*  BJD RHSP
                            getReports().write(0, "RHSP",annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,annty_Actvty_Prap_View_Ap_Coll_Code,annty_Actvty_Prap_View_Ap_Lob,         //Natural: WRITE 'RHSP' AP-TIAA-CNTRCT AP-COLL-CODE AP-LOB AP-LOB-TYPE AP-PPG-TEAM-CDE AP-COR-LAST-NME AP-COR-FIRST-NME
                                annty_Actvty_Prap_View_Ap_Lob_Type,annty_Actvty_Prap_View_Ap_Ppg_Team_Cde,annty_Actvty_Prap_View_Ap_Cor_Last_Nme,annty_Actvty_Prap_View_Ap_Cor_First_Nme);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  BJD RHSP
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  MAYO LD END
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Extract_Record.reset();                                                                                                                           //Natural: RESET #EXTRACT-RECORD
                    //*  KG TNGSUB - MOVED UP IN PROGRAM
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CONTRACT-TYPE
                    sub_Determine_Contract_Type();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  SGRD7 KG
                                                                                                                                                                          //Natural: PERFORM CHECK-PKG-SUPPRESS
                    sub_Check_Pkg_Suppress();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  CAMPUS - GS 8-2011
                                                                                                                                                                          //Natural: PERFORM CHECK-COUNTRY-CODE
                    sub_Check_Country_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  -------------------------------------------------------------------- *
                    //*  CHECK FOR COMPANION CONTRACTS OTHER THAN ICAP AND TNT /* JRB3        *
                    //*  SUPPRESS PRINTING WELCOME PACKAGE AND BYPASS 14 DAY DELAY FOR LEGAL  *
                    //*  -------------------------------------------------------------------- *
                    //*  KG TNGSUB - REMOVED CALL TO COR-FOR-COMPANION AS IT WAS NOT NEEDED
                    //*  JRB3
                    pnd_Cref_Companion.reset();                                                                                                                           //Natural: RESET #CREF-COMPANION
                    //*  JRB3
                    if (condition(annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero())))                                                                                     //Natural: IF AP-T-DOI = 0
                    {
                        //*  ICAP /* JRB3                    /* KG TNGSUB
                        //*  ICAP AND TNT EXCLUDED /* JRB3
                        if (condition(! (pnd_Rc_Or_Rcp.getBoolean())))                                                                                                    //Natural: IF NOT ( #RC-OR-RCP )
                        {
                            pnd_Cref_Companion.setValue(true);                                                                                                            //Natural: ASSIGN #CREF-COMPANION := TRUE
                            //*  JRB3
                        }                                                                                                                                                 //Natural: END-IF
                        //*  JRB3
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ILLSMP KG START
                    short decideConditionsMet2658 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM;//Natural: VALUE 'WELCOME'
                    if (condition((pnd_Input_Parm.equals("WELCOME"))))
                    {
                        decideConditionsMet2658++;
                        pnd_Extract_Record.setValue(true);                                                                                                                //Natural: ASSIGN #EXTRACT-RECORD := TRUE
                        //*  INST
                        if (condition(annty_Actvty_Prap_View_Ap_Ownership.equals(3) && ! (pnd_Welc_Force_Inst_Owned.getBoolean())))                                       //Natural: IF ( AP-OWNERSHIP = 3 ) AND ( NOT #WELC-FORCE-INST-OWNED )
                        {
                            //*  KG TNGSUB
                            if (condition(pnd_Rc_Or_Rcp.getBoolean()))                                                                                                    //Natural: IF #RC-OR-RCP
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Extract_Record.setValue(false);                                                                                                       //Natural: ASSIGN #EXTRACT-RECORD := FALSE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("6"))))                                     //Natural: IF ( AP-LOB = 'D' AND AP-LOB-TYPE = '6' )
                        {
                            //*          OR AP-SGRD-PLAN-NO = '100825'
                            pnd_Extract_Record.setValue(false);                                                                                                           //Natural: ASSIGN #EXTRACT-RECORD := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  KG TNGSUB2
                        //*  BIP
                        //*  KG TNGSUB2
                        if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.equals("W") || annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.equals("O")  //Natural: IF ( AP-SUBSTITUTION-CONTRACT-IND = 'W' OR = 'O' ) OR AP-DECEDENT-CONTRACT NE ' '
                            || annty_Actvty_Prap_View_Ap_Decedent_Contract.notEquals(" ")))
                        {
                            //* ***      OR AP-DECEASED-IND = 'Y'
                            pnd_Extract_Record.setValue(false);                                                                                                           //Natural: ASSIGN #EXTRACT-RECORD := FALSE
                            //*  KG TNGSUB2
                        }                                                                                                                                                 //Natural: END-IF
                        //*  KG TNGSUB
                        //*  SGRD7 KG
                        if (condition(pnd_Suppress_Welc_Pkg.getBoolean() || pnd_Cref_Companion.getBoolean()))                                                             //Natural: IF #SUPPRESS-WELC-PKG OR #CREF-COMPANION
                        {
                            pnd_Extract_Record.setValue(false);                                                                                                           //Natural: ASSIGN #EXTRACT-RECORD := FALSE
                            //*  SGRD7 KG
                        }                                                                                                                                                 //Natural: END-IF
                        //*  EMORY KG START
                        if (condition(pnd_Welc_Fund_Trigger.getBoolean() && annty_Actvty_Prap_View_Ap_Status.notEquals("C")))                                             //Natural: IF #WELC-FUND-TRIGGER AND AP-STATUS NE 'C'
                        {
                            pnd_Extract_Record.setValue(false);                                                                                                           //Natural: ASSIGN #EXTRACT-RECORD := FALSE
                            //*  EMORY KG END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'LEGAL'
                    else if (condition((pnd_Input_Parm.equals("LEGAL"))))
                    {
                        decideConditionsMet2658++;
                        //*  KG CIP 8/25/03
                        if (condition(annty_Actvty_Prap_View_Ap_Status.notEquals("C") || annty_Actvty_Prap_View_Ap_Addr_Usage_Code.equals("3")))                          //Natural: IF AP-STATUS NE 'C' OR AP-ADDR-USAGE-CODE = '3'
                        {
                            //* ***      OR AP-DECEASED-IND = 'Y'               /* TNGSUB  TNGSUB3 LS
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        //*  K.G. 12/6/01
                        //*  TNGSUB2
                        if (condition((((annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")) || annty_Actvty_Prap_View_Ap_Divorce_Ind.equals("Y"))  //Natural: IF ( AP-LOB = 'D' AND AP-LOB-TYPE = '6' ) OR AP-DIVORCE-IND = 'Y' OR ( AP-SUBSTITUTION-CONTRACT-IND = 'W' OR = 'O' )
                            || (annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.equals("W") || annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.equals("O")))))
                        {
                            //* **       AND AP-DECEASED-IND NE 'Y')
                            pnd_Extract_Record.setValue(true);                                                                                                            //Natural: ASSIGN #EXTRACT-RECORD := TRUE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  ICAP AND TNT EXLCUDED /* JRB3
                            //*  EASEENR
                            //*  BYPASS 14 DAY DELAY /* JRB3
                            if (condition(pnd_Cref_Companion.getBoolean() || annty_Actvty_Prap_View_Ap_Applcnt_Req_Type.equals("M")))                                     //Natural: IF #CREF-COMPANION OR AP-APPLCNT-REQ-TYPE = 'M'
                            {
                                ignore();
                                //*  JRB3
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                                                                                                                                                          //Natural: PERFORM CHECK-14-DAYS
                                sub_Check_14_Days();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                if (condition(pdaAciadate.getAciadate_Pnd_Diff().less(14)))                                                                               //Natural: IF #DIFF < 14
                                {
                                    if (condition(true)) continue;                                                                                                        //Natural: ESCAPE TOP
                                }                                                                                                                                         //Natural: END-IF
                                //*  JRB3
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Extract_Record.setValue(true);                                                                                                            //Natural: ASSIGN #EXTRACT-RECORD := TRUE
                            if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("I")))                                                                                     //Natural: IF AP-LOB = 'I'
                            {
                                //*  IF DIFF IS GREATER THEN 14 DAYS AND LOB IS IRA
                                //*  CHECK FOR PREMIUM
                                //*  ONEIRA2. EQ 0 CHECK FOR PREM,
                                if (condition(annty_Actvty_Prap_View_Ap_Financial_4.equals(getZero())))                                                                   //Natural: IF AP-FINANCIAL-4 = 0
                                {
                                    //*  SGRD6 KG
                                                                                                                                                                          //Natural: PERFORM CALL-PREM-INTERFACE
                                    sub_Call_Prem_Interface();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    if (condition(Map.getDoInput())) {return;}
                                    //*  SGRD6 KG 10/2005
                                    //*  ERROR CHECK  SGRD6 KG
                                    if (condition(! (pnd_Prem_Found.getBoolean()) || pnd_Ira_Except_Found.getBoolean()))                                                  //Natural: IF NOT #PREM-FOUND OR #IRA-EXCEPT-FOUND
                                    {
                                        //*  SGRD6 KG 10/2005
                                        if (condition(true)) continue;                                                                                                    //Natural: ESCAPE TOP
                                        //*  SGRD6 KG 10/2005
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SGRD7 KG
                        //*  BIP
                        //*  SGRD7 KG
                        if (condition(pnd_Suppress_Legal_Pkg.getBoolean() || annty_Actvty_Prap_View_Ap_Decedent_Contract.notEquals(" ")))                                 //Natural: IF #SUPPRESS-LEGAL-PKG OR AP-DECEDENT-CONTRACT NE ' '
                        {
                            pnd_Extract_Record.setValue(false);                                                                                                           //Natural: ASSIGN #EXTRACT-RECORD := FALSE
                            //*  SGRD7 KG
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  KG TNGSUB - ANY ROUTINES THAT NEED TO ESCAPE TOP WHEN AN ERROR IS
                    //*  DETECTED TO PREVENT FURTHER PROCESSING WERE MOVED TO PRIOR TO THE
                    //*  PERFORM OF THE NEW ROUTINE (MOVE DATA TO THE EXTRACT FILE).
                    //* * SGRD KG - SUNGARD CONTRACTS WILL CALL NAT/COBOL PLAN INTERFACE
                    //*  SGRD7 KG
                    //*  SGRD7 KG
                    pdaScia8100.getDoc_Rec().reset();                                                                                                                     //Natural: RESET DOC-REC
                    pdaScia8100.getDoc_Rec_Doc_Eof().setValue("N");                                                                                                       //Natural: ASSIGN DOC-EOF := 'N'
                    //*  SGRD KG
                                                                                                                                                                          //Natural: PERFORM CALL-PLAN-INTERFACE
                    sub_Call_Plan_Interface();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  SGRD KG
                    if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Code().notEquals(getZero())))                                                                         //Natural: IF DOC-RETURN-CODE NE 0
                    {
                        //*  SGRD KG
                        pnd_Exception_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #EXCEPTION-CNT
                        //*  SGRD KG
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT
                        sub_Exception_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  SGRD5 KG
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  SGRD KG
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                          /* >>> (TIC)
                    if (condition(pnd_Input_Parm.equals("LEGAL") && (pdaScia8100.getDoc_Rec_Doc_Tic_Startdate().notEquals(space))))                                       //Natural: IF #INPUT-PARM = 'LEGAL' AND ( DOC-TIC-STARTDATE NOT = SPACE )
                    {
                        //* *.....  IF RP-TIC-ENDDATE 30 DAYS GT RP-DT-APP-RECVD, AND IS RC OR RCP,
                        //* *.....  SET DX-TIC-IND TO 'Y'
                        //*  TIC LS1 >>>
                        if (condition(! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Startdate(),"NNNNNNNN")) || ! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Enddate(),"NNNNNNNN"))  //Natural: IF DOC-TIC-STARTDATE NE MASK ( NNNNNNNN ) OR DOC-TIC-ENDDATE NE MASK ( NNNNNNNN ) OR DOC-TIC-PERCENTAGE NE MASK ( NNNNNNNNN ) OR DOC-TIC-POSTDAYS NE MASK ( NN ) OR DOC-TIC-LIMIT NE MASK ( NNNNNNNNNNN ) OR DOC-TIC-WINDOWDAYS NE MASK ( NNN ) OR DOC-TIC-REQDLYWINDOW NE MASK ( NNN ) OR DOC-TIC-RECAP-PROV NE MASK ( NNN )
                            || ! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Percentage(),"NNNNNNNNN")) || ! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Postdays(),"NN")) 
                            || ! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Limit(),"NNNNNNNNNNN")) || ! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Windowdays(),"NNN")) 
                            || ! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Reqdlywindow(),"NNN")) || ! (DbsUtil.maskMatches(pdaScia8100.getDoc_Rec_Doc_Tic_Recap_Prov(),
                            "NNN"))))
                        {
                            pnd_Exception_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #EXCEPTION-CNT
                            pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().setValue("GROSSUP");                                                                           //Natural: ASSIGN DOC-RETURN-MSG-MODULE := 'GROSSUP'
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT
                            sub_Exception_Report();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                            //*  TB.GROSSUP VALIDATION   TIC LS1  <<<
                        }                                                                                                                                                 //Natural: END-IF
                        //*  KG TNGSUB
                    }                                                                                                                                                     //Natural: END-IF
                    //*  KG TNGSUB - NEW PERFORM FOR DATA MOVES TO THE EXTRACT FILE
                    //*  KG TNGSUB
                                                                                                                                                                          //Natural: PERFORM MOVE-EXTRACT-DATA
                    sub_Move_Extract_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //* * CA-082009 : NEW LOCATION OF THE BLOCK OF CODES THAT UPDATES
                    //* * THE REPRINT-FILE
                    //*  START OF BLOCK
                    short decideConditionsMet2792 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM;//Natural: VALUE 'WELCOME'
                    if (condition((pnd_Input_Parm.equals("WELCOME"))))
                    {
                        decideConditionsMet2792++;
                        ldaAppl160.getDoc_Ext_File_Dx_Request_Package_Type().setValue("W");                                                                               //Natural: ASSIGN DX-REQUEST-PACKAGE-TYPE := 'W'
                        ldaAppl160.getDoc_Ext_File_Dx_Print_Package_Type().setValue("W");                                                                                 //Natural: ASSIGN DX-PRINT-PACKAGE-TYPE := 'W'
                                                                                                                                                                          //Natural: PERFORM STORE-REPRINT-DATA
                        sub_Store_Reprint_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 'LEGAL'
                    else if (condition((pnd_Input_Parm.equals("LEGAL"))))
                    {
                        decideConditionsMet2792++;
                        ldaAppl160.getDoc_Ext_File_Dx_Request_Package_Type().setValue("L");                                                                               //Natural: ASSIGN DX-REQUEST-PACKAGE-TYPE := 'L'
                        ldaAppl160.getDoc_Ext_File_Dx_Print_Package_Type().setValue("L");                                                                                 //Natural: ASSIGN DX-PRINT-PACKAGE-TYPE := 'L'
                                                                                                                                                                          //Natural: PERFORM UPDATE-REPRINT-DATA
                        sub_Update_Reprint_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  END OF BLOCK
                    if (condition(pnd_Extract_Record.getBoolean()))                                                                                                       //Natural: IF #EXTRACT-RECORD
                    {
                        //*  CS (TIC)
                        //*  IISG
                        //*  IISG
                        //*  IISG
                        //*  IISG
                        getWorkFiles().write(1, false, ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_1(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_2(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_3(),  //Natural: WRITE WORK FILE 1 DOC-EXT-DATA-1 DOC-EXT-DATA-2 DOC-EXT-DATA-3 DOC-EXT-DATA-4 DOC-EXT-DATA-5 DOC-EXT-DATA-6 DOC-EXT-DATA-7 DOC-EXT-DATA-8 DOC-EXT-DATA-9 DOC-EXT-DATA-10 DOC-EXT-DATA-11 DOC-EXT-DATA-12 DOC-EXT-DATA-13 DOC-EXT-DATA-14 DOC-EXT-DATA-15 DOC-EXT-DATA-16 DOC-EXT-DATA-17 DOC-EXT-DATA-18 DOC-EXT-DATA-19 DOC-EXT-DATA-20 DOC-EXT-DATA-21 DOC-EXT-DATA-22 DOC-EXT-DATA-23 DOC-EXT-DATA-24 DOC-EXT-DATA-25 DOC-EXT-DATA-26
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_4(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_5(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_6(), 
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_7(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_8(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_9(), 
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_10(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_11(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_12(), 
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_13(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_14(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_15(), 
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_16(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_17(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_18(), 
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_19(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_20(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_21(), 
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_22(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_23(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_24(), 
                            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_25(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_26());
                        ldaAppl170.getPnd_Write_Count().nadd(1);                                                                                                          //Natural: ADD 1 TO #WRITE-COUNT
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ac_Pda.resetInitial();                                                                                                                            //Natural: RESET INITIAL #AC-PDA #WORK-FIELDS DOC-EXT-FILE #PAR-PARAMETER-AREA #PAR-PARAMETER-AREA-2
                    ldaAppl170.getPnd_Work_Fields().resetInitial();
                    ldaAppl160.getDoc_Ext_File().resetInitial();
                    pdaAppa100.getPnd_Par_Parameter_Area().resetInitial();
                    pdaAppa200.getPnd_Par_Parameter_Area_2().resetInitial();
                    //*  EMORY KG
                    //*  EMORY KG
                    if (condition(pnd_Welc_Fund_Trigger.getBoolean() && annty_Actvty_Prap_View_Ap_Status.notEquals("C")))                                                 //Natural: IF #WELC-FUND-TRIGGER AND AP-STATUS NE 'C'
                    {
                        ignore();
                        //*  EMORY KG
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM UPDATE-RECORDS
                        sub_Update_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  EMORY KG
                    }                                                                                                                                                     //Natural: END-IF
                    //*  K.G. 03/20 ACIS VOLUME CHANGES
                    if (condition(pnd_Extract_Record.getBoolean()))                                                                                                       //Natural: IF #EXTRACT-RECORD
                    {
                        pnd_Extract_Contract_Cnt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #EXTRACT-CONTRACT-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
                short decideConditionsMet2834 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM;//Natural: VALUE 'WELCOME'
                if (condition((pnd_Input_Parm.equals("WELCOME"))))
                {
                    decideConditionsMet2834++;
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT-WELCOME
                    sub_Write_Control_Report_Welcome();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: VALUE 'LEGAL'
                else if (condition((pnd_Input_Parm.equals("LEGAL"))))
                {
                    decideConditionsMet2834++;
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT-LEGAL
                    sub_Write_Control_Report_Legal();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //* * SGRD KG - CALL PLAN INTERFACE TO CLOSE OMNI FILES
                //*  SGRD5 KG
                //*  SGRD6 KG
                if (condition(pdaScia8100.getDoc_File_Open_Sw().equals("Y") || pdaScia8250.getPpt_File_Open_Sw().equals("Y")))                                            //Natural: IF DOC-FILE-OPEN-SW = 'Y' OR PPT-FILE-OPEN-SW = 'Y'
                {
                    //*  SGRD7 KG
                    //*  SGRD KG
                    pdaScia8100.getDoc_Rec().reset();                                                                                                                     //Natural: RESET DOC-REC
                    pdaScia8100.getDoc_Rec_Doc_Eof().setValue("Y");                                                                                                       //Natural: ASSIGN DOC-EOF := 'Y'
                    //*  SGRD KG
                                                                                                                                                                          //Natural: PERFORM CALL-PLAN-INTERFACE
                    sub_Call_Plan_Interface();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  SGRD KG
                }                                                                                                                                                         //Natural: END-IF
                //* * SGRD5 KG - CALL CNTCT INTERFACE TO CLOSE SEIBEL FILES
                //*  SGRD5 KG
                if (condition(pdaScia8200.getCon_File_Open_Sw().equals("Y")))                                                                                             //Natural: IF CON-FILE-OPEN-SW = 'Y'
                {
                    //*  SGRD7 KG
                    pdaScia8200.getCon_Part_Rec().reset();                                                                                                                //Natural: RESET CON-PART-REC
                    pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("Y");                                                                                               //Natural: ASSIGN CON-EOF-SW := 'Y'
                                                                                                                                                                          //Natural: PERFORM CALL-CNTCT-INTERFACE
                    sub_Call_Cntct_Interface();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  SGRD5 KG
                }                                                                                                                                                         //Natural: END-IF
                //* *--------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-EXTRACT-DATA
                //* *-------------------------
                //* *-----------------------------------
                //* *--------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALPHA-STATE-CODE
                //* *--------------------------
                //* *--------------------------
                //* *----------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CONTRACT-TYPE
                //* *-----------------------------------
                //* *----------------------------------
                //* *-----------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-MAIL-PULL-CODE
                //* *---------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REPORT-WELCOME
                //* *---------------------------------------------
                //* *-------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REPORT-LEGAL
                //* *-------------------------------------------
                //* *---------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PART-STATUS
                //* *----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-REPRINT-DATA
                //*  RP-PIN-NBR := #PIN-NUMBER-N
                //*  RP-ALLOCATION(*)        := AP-ALLOCATION(*)
                //*        AND AP-SGRD-PLAN-NO NE '100825')
                //*  IF  AP-SGRD-PLAN-NO = '100825'
                //*    RP-PACKAGE-MAIL-TYPE      := 'P'
                //*  END-IF
                //*  RP-TEXT-UDF-1             := #RP-TEXT-UDF-1
                //*  AP-FUND-SOURCE-CDE-1        := AP-FUND-SOURCE-CDE-1
                //*  AP-FUND-SOURCE-CDE-2        := AP-FUND-SOURCE-CDE-2
                //* * ARR KG - ADDED AUTOMATIC REPLACEMENT REGISTER FIELDS TO REPRINT
                //* *          FILE
                //*  BJD AE START
                //*  BJD-END
                //*  LPAO KG START - MOVE ANNUITY FUNDING FIELDS TO REPRINT
                //*  LPAO KG END
                //*  LPAO KG - REMOVED ROCH2 PLAN SPECIFIC CHANGES EXCEPT FOR SPECIAL LEGAL
                //*  PKG IND
                //* *------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-14-DAYS
                //* *------------------------------
                //* *-----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-REPRINT-DATA
                //*    WHEN AP-SGRD-PLAN-NO = '100825'
                //*      RP-PACKAGE-MAIL-TYPE := 'L'
                //*  LPAO KG END
                //*  LPAO KG - REMOVED ROCH2 PLAN SPECIFIC CHANGES EXCEPT FOR SPECIAL LEGAL
                //*  PKG IND
                //* *------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RECORDS
                //*  LPAO KG - REMOVED ROCH2 PLAN SPECIFIC CHANGES
                //* * Y2K FIX 02/10/98
                //* * Y2K FIX END
                //* *----------------------------------------------------------------------
                //* *********************************************************************
                //*  YR2000 COMPLIANT COPYCODE  ---> C.BROWN                            *
                //* *********************************************************************
                //*  Y2CHED SH CONVERTED FROM COBOL TO NATURAL --> E. DAVID 10/20/97
                //*                          Y 2 D A T E B P
                //* *******  BATCH DATE COMPARE ROUTINE FOR YR2000 PROCESSING  ******
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-3
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-4
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-5
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-3
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-4
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-5
                //* ********  END Y2K BATCH DATE-COMPARE PROCEDURE COPYCODE  ********
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-ACIN2100-GET-PRODUCT-CDE
                //* *----------------------------------
                //* *-------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-PLAN-INTERFACE
                //* *-------------------------------------
                //*  #SPEC-FUND-IND
                //* *-------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CNTCT-INTERFACE
                //* *-------------------------------------
                //* *-------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-PREM-INTERFACE
                //* *---------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT
                //* *----------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT-WRITE-HEADER
                //* *---------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-SINGLE-ISSUE
                //* *---------------------------------------
                //*  MODIFIED AND RENAMED THE ORIGINAL COR-FOR-COMPANION ROUTINE TO USE THE
                //*  ACIS REPRINT FILE INSTEAD OF COR.
                //*  CONTRACT WILL BE USED TO DETERMINE IF THE PARTICIPANT ALREADY HAS A
                //*  SINGLE ISSUE ON THE ACIS-REPRINT-FL. IF THERE ARE NO CONTRACTS THEN
                //*  THE RC OR RCP CONTRACT IS CONSIDERED A SINGLE ISSUANCE, OTHERWISE IT
                //*  IS A RC OR RCP COMPANION.
                //* *----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-DEFLT-ENROLL
                //* *-----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-TRIGGER-SUPPRESS-TABLE
                //* *-----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PKG-SUPPRESS
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-CONVERSIONS
                //* *-----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-COUNTRY-CODE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-FORM-NUM
                //* *----------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ED-NUM
                //*   >> START OF C566813 >>
                short decideConditionsMet3017 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM;//Natural: VALUE 'WELCOME'
                if (condition((pnd_Input_Parm.equals("WELCOME"))))
                {
                    decideConditionsMet3017++;
                    if (condition(ldaAppl170.getPnd_Write_Count().greater(7000)))                                                                                         //Natural: IF #WRITE-COUNT > 7000
                    {
                        DbsUtil.terminate(2);  if (true) return;                                                                                                          //Natural: TERMINATE 2
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'LEGAL'
                else if (condition((pnd_Input_Parm.equals("LEGAL"))))
                {
                    decideConditionsMet3017++;
                    if (condition(ldaAppl170.getPnd_Write_Count().greater(7000)))                                                                                         //Natural: IF #WRITE-COUNT > 7000
                    {
                        DbsUtil.terminate(1);  if (true) return;                                                                                                          //Natural: TERMINATE 1
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*   << END OF C566813 <<
                //* ***********************************************************************
                //*  JRB2
                //* ***********************************************************************                                                                               //Natural: ON ERROR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*  KG TNGSUB
    private void sub_Move_Extract_Data() throws Exception                                                                                                                 //Natural: MOVE-EXTRACT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------
        //*  NEW ROUTINE TO CONSOLIDATE ALL MOVES OF DATA TO THE WELCOME AND LEGAL
        //*  EXTRACT FILES (APPL160).
        //*  THIS ROUTINE SHOULD NOT CONTAIN ANY DATA MOVES OR VALIDATIONS THAT
        //*  REQUIRE AN ESCAPE TOP TO PREVENT FURTHER PROCESSING ON A RECORD IF AN
        //*  EXCEPTION IS ENCOUNTERED.
        //*  **********************************************************************
        ldaAppl160.getDoc_Ext_File_Dx_Extract_Date().setValueEdited(ldaAppl170.getPnd_Hold_Extract_Date(),new ReportEditMask("999999"));                                  //Natural: MOVE EDITED #HOLD-EXTRACT-DATE ( EM = 999999 ) TO DX-EXTRACT-DATE
        if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RL")))                                                                                    //Natural: IF #CONTRACT-TYPE = 'RL'
        {
            ldaAppl160.getDoc_Ext_File_Dx_Type_Cd().setValue("1");                                                                                                        //Natural: MOVE '1' TO DX-TYPE-CD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero())))                                                                                             //Natural: IF AP-T-DOI = 0
            {
                ldaAppl160.getDoc_Ext_File_Dx_Type_Cd().setValue("2");                                                                                                    //Natural: MOVE '2' TO DX-TYPE-CD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(annty_Actvty_Prap_View_Ap_C_Doi.equals(getZero())))                                                                                         //Natural: IF AP-C-DOI = 0
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Type_Cd().setValue("1");                                                                                                //Natural: MOVE '1' TO DX-TYPE-CD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(annty_Actvty_Prap_View_Ap_T_Doi.greater(getZero()) && annty_Actvty_Prap_View_Ap_C_Doi.greater(getZero())))                              //Natural: IF AP-T-DOI > 0 AND AP-C-DOI > 0
                    {
                        ldaAppl160.getDoc_Ext_File_Dx_Type_Cd().setValue("3");                                                                                            //Natural: MOVE '3' TO DX-TYPE-CD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Alloc_Disc().setValue(annty_Actvty_Prap_View_Ap_Alloc_Discr);                                                                       //Natural: MOVE AP-ALLOC-DISCR TO DX-ALLOC-DISC
        //*  EAC (RODGER)
        pnd_Tiaa_Nbr.setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                                                                     //Natural: MOVE AP-TIAA-CNTRCT TO #TIAA-NBR
        //*  EAC (RODGER)
        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Prfx().getValue(1).setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct.getSubstring(1,1));                                          //Natural: MOVE SUBSTRING ( AP-TIAA-CNTRCT,1,1 ) TO DX-TIAA-PRFX ( 1 )
        //*  EAC (RODGER)
        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_No().getValue(1).setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct.getSubstring(2,7));                                            //Natural: MOVE SUBSTRING ( AP-TIAA-CNTRCT,2,7 ) TO DX-TIAA-NO ( 1 )
        //*  EAC (RODGER)
        ldaAppl160.getDoc_Ext_File_Dx_Cref_Prfx().getValue(1).setValue(annty_Actvty_Prap_View_Ap_Cref_Cert.getSubstring(1,1));                                            //Natural: MOVE SUBSTRING ( AP-CREF-CERT,1,1 ) TO DX-CREF-PRFX ( 1 )
        //*  EAC (RODGER)
        ldaAppl160.getDoc_Ext_File_Dx_Cref_No().getValue(1).setValue(annty_Actvty_Prap_View_Ap_Cref_Cert.getSubstring(2,7));                                              //Natural: MOVE SUBSTRING ( AP-CREF-CERT,2,7 ) TO DX-CREF-NO ( 1 )
        //*  SGRD6 KG   /* KG TNGSUB
        pnd_Icap_Tnt_Companion.reset();                                                                                                                                   //Natural: RESET #ICAP-TNT-COMPANION
        //*  SGRD7 KG
        ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind().setValue("N");                                                                                                   //Natural: MOVE 'N' TO DX-SINGLE-ISSUE-IND
        //*  KG TNGSUB
        if (condition(((annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero()) || annty_Actvty_Prap_View_Ap_C_Doi.equals(getZero())) && pnd_Rc_Or_Rcp.getBoolean())))          //Natural: IF ( ( AP-T-DOI = 0 OR AP-C-DOI = 0 ) AND #RC-OR-RCP )
        {
            //*  SGRD6 KG
            pnd_Icap_Tnt_Companion.setValue("Y");                                                                                                                         //Natural: MOVE 'Y' TO #ICAP-TNT-COMPANION
            //*  SGRD6 KG
                                                                                                                                                                          //Natural: PERFORM DETERMINE-SINGLE-ISSUE
            sub_Determine_Single_Issue();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  SGRD6 KG
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")))                                                       //Natural: IF AP-LOB = 'D' AND AP-LOB-TYPE = '6'
        {
            //*  EAC (RO)
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Prfx().getValue(4).setValue(annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert.getSubstring(1,1));                                    //Natural: MOVE SUBSTRING ( AP-RLC-CREF-CERT,1,1 ) TO DX-CREF-PRFX ( 4 )
            //*  EAC (RO)
            ldaAppl160.getDoc_Ext_File_Dx_Cref_No().getValue(4).setValue(annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert.getSubstring(2,7));                                      //Natural: MOVE SUBSTRING ( AP-RLC-CREF-CERT,2,7 ) TO DX-CREF-NO ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Prfx().getValue(2).reset();                                                                                                    //Natural: RESET DX-TIAA-PRFX ( 2 ) DX-TIAA-NO ( 2 ) DX-CREF-PRFX ( 2 ) DX-CREF-NO ( 2 )
        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_No().getValue(2).reset();
        ldaAppl160.getDoc_Ext_File_Dx_Cref_Prfx().getValue(2).reset();
        ldaAppl160.getDoc_Ext_File_Dx_Cref_No().getValue(2).reset();
        if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("N")))                                                                                                  //Natural: IF AP-APP-SOURCE = 'N'
        {
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Prfx().getValue(2).setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(4).getSubstring(1,1));                 //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 4 ) ,1,1 ) TO DX-TIAA-PRFX ( 2 )
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_No().getValue(2).setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(4).getSubstring(2,7));                   //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 4 ) ,2,7 ) TO DX-TIAA-NO ( 2 )
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Prfx().getValue(2).setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(5).getSubstring(1,1));                 //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 5 ) ,1,1 ) TO DX-CREF-PRFX ( 2 )
            ldaAppl160.getDoc_Ext_File_Dx_Cref_No().getValue(2).setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(5).getSubstring(2,7));                   //Natural: MOVE SUBSTRING ( AP-CONTINGENT-BENE-INFO ( 5 ) ,2,7 ) TO DX-CREF-NO ( 2 )
            annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue("*").setValue(" ");                                                                                   //Natural: ASSIGN AP-CONTINGENT-BENE-INFO ( * ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Age_First_Pymt().setValue(annty_Actvty_Prap_View_Ap_T_Age_1st);                                                                     //Natural: MOVE AP-T-AGE-1ST TO DX-AGE-FIRST-PYMT
        ldaAppl160.getDoc_Ext_File_Dx_Ownership().setValue(annty_Actvty_Prap_View_Ap_Ownership);                                                                          //Natural: MOVE AP-OWNERSHIP TO DX-OWNERSHIP
        //*  KG TNGSUB START
        short decideConditionsMet3124 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-OWNERSHIP = 1
        if (condition(annty_Actvty_Prap_View_Ap_Ownership.equals(1)))
        {
            decideConditionsMet3124++;
            ldaAppl160.getDoc_Ext_File_Dx_Ownership().setValue(0);                                                                                                        //Natural: MOVE 0 TO DX-OWNERSHIP
        }                                                                                                                                                                 //Natural: WHEN AP-OWNERSHIP = 2
        else if (condition(annty_Actvty_Prap_View_Ap_Ownership.equals(2)))
        {
            decideConditionsMet3124++;
            ldaAppl160.getDoc_Ext_File_Dx_Ownership().setValue(2);                                                                                                        //Natural: MOVE 2 TO DX-OWNERSHIP
        }                                                                                                                                                                 //Natural: WHEN AP-OWNERSHIP = 3
        else if (condition(annty_Actvty_Prap_View_Ap_Ownership.equals(3)))
        {
            decideConditionsMet3124++;
            //*  INST  >>
            //*  457B PG1
            if (condition(pnd_Welc_Force_Inst_Owned.getBoolean() || (annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("4"))))       //Natural: IF #WELC-FORCE-INST-OWNED OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '4' )
            {
                //*  TO AVOID MANUAL
                ldaAppl160.getDoc_Ext_File_Dx_Ownership().setValue(0);                                                                                                    //Natural: MOVE 0 TO DX-OWNERSHIP
                //*  PROCESS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl160.getDoc_Ext_File_Dx_Ownership().setValue(1);                                                                                                    //Natural: MOVE 1 TO DX-OWNERSHIP
                //*        <<
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
            //*  KG TNGSUB END
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   INST
        //*   INST
        if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(2) || (annty_Actvty_Prap_View_Ap_Ownership.equals(3) && ! (pnd_Welc_Force_Inst_Owned.getBoolean())))))  //Natural: IF ( AP-OWNERSHIP = 2 ) OR ( AP-OWNERSHIP = 3 AND NOT #WELC-FORCE-INST-OWNED )
        {
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Full_Immed_Vest().setValue("N");                                                                                           //Natural: MOVE 'N' TO DX-INST-FULL-IMMED-VEST
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  J.B. 12/18/95
            //*   INST
            if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(1)) || (annty_Actvty_Prap_View_Ap_Ownership.equals(3) && pnd_Welc_Force_Inst_Owned.getBoolean())))  //Natural: IF ( AP-OWNERSHIP = 1 ) OR ( AP-OWNERSHIP = 3 AND #WELC-FORCE-INST-OWNED )
            {
                ldaAppl160.getDoc_Ext_File_Dx_Inst_Full_Immed_Vest().setValue("Y");                                                                                       //Natural: MOVE 'Y' TO DX-INST-FULL-IMMED-VEST
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  IISG
        ldaAppl160.getDoc_Ext_File_Dx_Fund_Source_Cde_1().setValue(annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1);                                                          //Natural: MOVE AP-FUND-SOURCE-CDE-1 TO DX-FUND-SOURCE-CDE-1
        //*  OIA JR
        ldaAppl170.getPnd_Work_Fields_Pnd_Ap_Alloc().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Allocation_Pct.getValue(1,":",100));                          //Natural: MOVE AP-ALLOCATION-PCT ( 1:100 ) TO #AP-ALLOC ( 1:100 )
        //*  OIA JR
        ldaAppl170.getPnd_Work_Fields_Pnd_Ap_Fund_Ident().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde.getValue(1,":",100));                           //Natural: MOVE AP-FUND-CDE ( 1:100 ) TO #AP-FUND-IDENT ( 1:100 )
        //*  OIA KG
        ldaAppl160.getDoc_Ext_File_Dx_Fund_Cde().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde.getValue(1,":",100));                                    //Natural: MOVE AP-FUND-CDE ( 1:100 ) TO DX-FUND-CDE ( 1:100 )
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 100
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(100)); pnd_X.nadd(1))
        {
            //*  OIA KG
            ldaAppl160.getDoc_Ext_File_Dx_Alloc_Pct().getValue(pnd_X).setValueEdited(ldaAppl170.getPnd_Work_Fields_Pnd_Ap_Alloc().getValue(pnd_X),new                     //Natural: MOVE EDITED #AP-ALLOC ( #X ) ( EM = ZZ9 ) TO DX-ALLOC-PCT ( #X )
                ReportEditMask("ZZ9"));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  IISG
        if (condition(annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2.greater(" ")))                                                                                          //Natural: IF AP-FUND-SOURCE-CDE-2 > ' '
        {
            //*  IISG
            ldaAppl160.getDoc_Ext_File_Dx_Fund_Source_Cde_2().setValue(annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2);                                                      //Natural: MOVE AP-FUND-SOURCE-CDE-2 TO DX-FUND-SOURCE-CDE-2
            //*  IISG
            ldaAppl170.getPnd_Work_Fields_Pnd_Ap_Alloc().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Allocation_Pct_2.getValue(1,":",100));                    //Natural: MOVE AP-ALLOCATION-PCT-2 ( 1:100 ) TO #AP-ALLOC ( 1:100 )
            //*  IISG
            ldaAppl160.getDoc_Ext_File_Dx_Fund_Cde_2().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde_2.getValue(1,":",100));                            //Natural: MOVE AP-FUND-CDE-2 ( 1:100 ) TO DX-FUND-CDE-2 ( 1:100 )
            FOR02:                                                                                                                                                        //Natural: FOR #X 1 100
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(100)); pnd_X.nadd(1))
            {
                //*  IISG
                ldaAppl160.getDoc_Ext_File_Dx_Alloc_Pct_2().getValue(pnd_X).setValueEdited(ldaAppl170.getPnd_Work_Fields_Pnd_Ap_Alloc().getValue(pnd_X),new               //Natural: MOVE EDITED #AP-ALLOC ( #X ) ( EM = ZZ9 ) TO DX-ALLOC-PCT-2 ( #X )
                    ReportEditMask("ZZ9"));
                //*  IISG
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  IISG
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Currency().setValue(annty_Actvty_Prap_View_Ap_Curr);                                                                                //Natural: MOVE AP-CURR TO DX-CURRENCY
        pnd_Hold_Coll_Code.setValue(annty_Actvty_Prap_View_Ap_Coll_Code);                                                                                                 //Natural: MOVE AP-COLL-CODE TO #HOLD-COLL-CODE
        ldaAppl160.getDoc_Ext_File_Dx_Soc_Sec().setValueEdited(annty_Actvty_Prap_View_Ap_Soc_Sec,new ReportEditMask("999-99-9999"));                                      //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999-99-9999 ) TO DX-SOC-SEC
        if (condition(annty_Actvty_Prap_View_Ap_Bene_Category.equals("1")))                                                                                               //Natural: IF AP-BENE-CATEGORY = '1'
        {
            annty_Actvty_Prap_View_Ap_Primary_Bene_Info.getValue(1).setValue("NONE");                                                                                     //Natural: MOVE 'NONE' TO AP-PRIMARY-BENE-INFO ( 1 )
            annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(1).setValue("NONE");                                                                                  //Natural: MOVE 'NONE' TO AP-CONTINGENT-BENE-INFO ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Zip().setValue(annty_Actvty_Prap_View_Ap_Mail_Zip);                                                                                 //Natural: MOVE AP-MAIL-ZIP TO DX-ZIP
        ldaAppl160.getDoc_Ext_File_Dx_Addr_Sffx().setValue(annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme);                                                                       //Natural: MOVE AP-COR-SFFX-NME TO DX-ADDR-SFFX
        ldaAppl160.getDoc_Ext_File_Dx_Mit_Log_Dt_Tm().getValue("*").setValue(annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time.getValue("*"));                                  //Natural: MOVE AP-RQST-LOG-DTE-TIME ( * ) TO DX-MIT-LOG-DT-TM ( * )
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme.setValue(annty_Actvty_Prap_View_Ap_Cor_Last_Nme);                                                                               //Natural: MOVE AP-COR-LAST-NME TO #COR-LAST-NME DX-ADDR-LAST
        ldaAppl160.getDoc_Ext_File_Dx_Addr_Last().setValue(annty_Actvty_Prap_View_Ap_Cor_Last_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme.setValue(annty_Actvty_Prap_View_Ap_Cor_First_Nme);                                                                             //Natural: MOVE AP-COR-FIRST-NME TO #COR-FIRST-NME DX-ADDR-FRST
        ldaAppl160.getDoc_Ext_File_Dx_Addr_Frst().setValue(annty_Actvty_Prap_View_Ap_Cor_First_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme.setValue(annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme);                                                                             //Natural: MOVE AP-COR-MDDLE-NME TO #COR-MDDLE-NME
        ldaAppl160.getDoc_Ext_File_Dx_Addr_Mddle().setValue(pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1);                                                                        //Natural: MOVE #COR-MDDLE-NME-1 TO DX-ADDR-MDDLE
        //*  CS MOBKEY
        ldaAppl160.getDoc_Ext_File_Dx_Full_Middle_Name().setValue(annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme);                                                               //Natural: MOVE AP-COR-MDDLE-NME TO DX-FULL-MIDDLE-NAME
        if (condition(pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10.equals(" ")))                                                                                                 //Natural: IF #COR-MDDLE-NME-10 = ' '
        {
            pnd_Ap_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16, ldaAppl170.getPnd_Work_Fields_Pnd_Comma(),         //Natural: COMPRESS #COR-LAST-NME-16 #COMMA #COR-FIRST-NME-10 INTO #AP-NAME LEAVING NO SPACE
                pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ap_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16, ldaAppl170.getPnd_Work_Fields_Pnd_Comma(),         //Natural: COMPRESS #COR-LAST-NME-16 #COMMA #COR-FIRST-NME-10 #COMMA #COR-MDDLE-NME-10 INTO #AP-NAME LEAVING NO SPACE
                pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10, ldaAppl170.getPnd_Work_Fields_Pnd_Comma(), pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10));
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Annuitant_Name().setValue(pnd_Ap_Name);                                                                                             //Natural: MOVE #AP-NAME TO DX-ANNUITANT-NAME
        pnd_Ap_Name.separate(SeparateOption.WithAnyDelimiters, ",", ldaAppl170.getPnd_Work_Fields_Pnd_Last_Name(), ldaAppl170.getPnd_Work_Fields_Pnd_First_Name(),        //Natural: SEPARATE #AP-NAME INTO #LAST-NAME #FIRST-NAME #MIDDLE-NAME #OVER-FLOW WITH DELIMITER ','
            ldaAppl170.getPnd_Work_Fields_Pnd_Middle_Name(), pnd_Over_Flow);
        pnd_Hold_Nme_Prfx.setValue(annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme);                                                                                               //Natural: MOVE AP-COR-PRFX-NME TO #HOLD-NME-PRFX DX-ADDR-PREF
        ldaAppl160.getDoc_Ext_File_Dx_Addr_Pref().setValue(annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme);
        //*  SGRD4 KG
        //*   TNGSUB CML
        //*  SGRD4 KG
        if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("S") || annty_Actvty_Prap_View_Ap_App_Source.equals("L") || pnd_Indiv_Product.getBoolean()))            //Natural: IF ( AP-APP-SOURCE = 'S' OR = 'L' ) OR #INDIV-PRODUCT
        {
            pnd_W_Check_State.setValue(annty_Actvty_Prap_View_Ap_Orig_Issue_State);                                                                                       //Natural: ASSIGN #W-CHECK-STATE := AP-ORIG-ISSUE-STATE
            //*  SGRD4 KG
            //*  SGRD4 KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Check_State.setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                                                                     //Natural: ASSIGN #W-CHECK-STATE := AP-CURRENT-STATE-CODE
            //*  MAIL-RES FIX KG END
        }                                                                                                                                                                 //Natural: END-IF
        //*  BWN
                                                                                                                                                                          //Natural: PERFORM STATE-CONVERSIONS
        sub_State_Conversions();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        FOR03:                                                                                                                                                            //Natural: FOR #ST 1 62
        for (pnd_St.setValue(1); condition(pnd_St.lessOrEqual(62)); pnd_St.nadd(1))
        {
            //*  LD1
            if (condition(pnd_W_Check_State.equals(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_St))))                                              //Natural: IF #W-CHECK-STATE = #STATE-CODE-N ( #ST )
            {
                pnd_Hold_State.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_St));                                                          //Natural: MOVE #STATE-CODE-A ( #ST ) TO #HOLD-STATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Hold_City_Zip.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_City, ldaAppl170.getPnd_Work_Fields_Pnd_Comma(),             //Natural: COMPRESS AP-CITY #COMMA #HOLD-STATE INTO #HOLD-CITY-ZIP LEAVING NO SPACE
            pnd_Hold_State));
        //*  BN1 - BE
        if (condition(annty_Actvty_Prap_View_Ap_Mail_Zip.equals("CANAD")))                                                                                                //Natural: IF AP-MAIL-ZIP = 'CANAD'
        {
            //*  BN1 - BE
            //*  BN1 - BE
            if (condition(annty_Actvty_Prap_View_Ap_Orchestration_Id.greater(" ") && annty_Actvty_Prap_View_Ap_Address_Line.getValue(3).getSubstring(3,                   //Natural: IF AP-ORCHESTRATION-ID GT ' ' AND SUBSTR ( AP-ADDRESS-LINE ( 3 ) ,3,1 ) = ','
                1).equals(",")))
            {
                annty_Actvty_Prap_View_Ap_Address_Line.getValue(3).setValue(DbsUtil.compress(annty_Actvty_Prap_View_Ap_City, annty_Actvty_Prap_View_Ap_Address_Line.getValue(3))); //Natural: COMPRESS AP-CITY AP-ADDRESS-LINE ( 3 ) INTO AP-ADDRESS-LINE ( 3 )
                //*  BN1 - BE
            }                                                                                                                                                             //Natural: END-IF
            //*  BN1 - BE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue(1,":",5).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(1,":",5));                               //Natural: MOVE AP-ADDRESS-LINE ( 1:5 ) TO DX-ADDRESS-TABLE ( 1:5 )
        DbsUtil.examine(new ExamineSource(ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue("*"),true), new ExamineSearch(space, true), new ExamineGivingIndex(pnd_Zp)); //Natural: EXAMINE FULL DX-ADDRESS-TABLE ( * ) FOR FULL VALUE OF SPACE GIVING INDEX IN #ZP
        //*  BE1 - IF STMT ADDED BY ELLO
        if (condition(! (pnd_Hold_State.equals("FN") || pnd_Hold_State.equals("CN"))))                                                                                    //Natural: IF NOT ( #HOLD-STATE = 'FN' OR = 'CN' )
        {
            DbsUtil.examine(new ExamineSource(pnd_Hold_City_Zip,true), new ExamineSearch(","), new ExamineReplace(" "));                                                  //Natural: EXAMINE FULL #HOLD-CITY-ZIP ',' REPLACE WITH ' '
            ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue(pnd_Zp).setValue(pnd_Hold_City_Zip);                                                                   //Natural: MOVE #HOLD-CITY-ZIP TO DX-ADDRESS-TABLE ( #ZP )
            //*  BE1 - END-IF ADDED BY ELLO
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Eop_Zipcode().setValue(annty_Actvty_Prap_View_Ap_Mail_Zip);                                                                         //Natural: MOVE AP-MAIL-ZIP TO DX-EOP-ZIPCODE
        ldaAppl160.getDoc_Ext_File_Dx_Bene_Estate().setValue(annty_Actvty_Prap_View_Ap_Bene_Estate);                                                                      //Natural: MOVE AP-BENE-ESTATE TO DX-BENE-ESTATE
        ldaAppl160.getDoc_Ext_File_Dx_Bene_Trust().setValue(annty_Actvty_Prap_View_Ap_Bene_Trust);                                                                        //Natural: MOVE AP-BENE-TRUST TO DX-BENE-TRUST
        if (condition(annty_Actvty_Prap_View_Ap_Bene_Category.equals("4")))                                                                                               //Natural: IF AP-BENE-CATEGORY = '4'
        {
            ldaAppl160.getDoc_Ext_File_Dx_Bene_Category().setValue(" ");                                                                                                  //Natural: MOVE ' ' TO DX-BENE-CATEGORY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_Bene_Category().setValue(annty_Actvty_Prap_View_Ap_Bene_Category);                                                              //Natural: MOVE AP-BENE-CATEGORY TO DX-BENE-CATEGORY
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Sex().setValue(annty_Actvty_Prap_View_Ap_Sex);                                                                                      //Natural: MOVE AP-SEX TO DX-SEX
        if (condition(annty_Actvty_Prap_View_Ap_Mail_Instructions.equals(" ")))                                                                                           //Natural: IF AP-MAIL-INSTRUCTIONS = ' '
        {
            ldaAppl160.getDoc_Ext_File_Dx_Mail_Instructions().setValue("1");                                                                                              //Natural: MOVE '1' TO DX-MAIL-INSTRUCTIONS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_Mail_Instructions().setValue(annty_Actvty_Prap_View_Ap_Mail_Instructions);                                                      //Natural: MOVE AP-MAIL-INSTRUCTIONS TO DX-MAIL-INSTRUCTIONS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Orig_Issue_State.equals(" ") || annty_Actvty_Prap_View_Ap_Orig_Issue_State.equals("00")))                                 //Natural: IF AP-ORIG-ISSUE-STATE = ' ' OR = '00'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl170.getPnd_Work_Fields_Pnd_Prap_State_Cd().setValue(annty_Actvty_Prap_View_Ap_Orig_Issue_State);                                                       //Natural: MOVE AP-ORIG-ISSUE-STATE TO #PRAP-STATE-CD
                                                                                                                                                                          //Natural: PERFORM GET-ALPHA-STATE-CODE
            sub_Get_Alpha_State_Code();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            ldaAppl160.getDoc_Ext_File_Dx_Orig_Iss_State().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Alpha_State_Cd());                                                  //Natural: MOVE #ALPHA-STATE-CD TO DX-ORIG-ISS-STATE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRD8B KG
        //*  SGRD8B KG
        if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No.equals("100825")))                                                                                           //Natural: IF AP-SGRD-PLAN-NO = '100825'
        {
            annty_Actvty_Prap_View_Ap_Coll_St_Cd.setValue("16");                                                                                                          //Natural: ASSIGN AP-COLL-ST-CD := '16'
            //*  SGRD8B KG
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Coll_St_Cd.equals(" ")))                                                                                                  //Natural: IF AP-COLL-ST-CD = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl170.getPnd_Work_Fields_Pnd_Prap_State_Cd().setValue(annty_Actvty_Prap_View_Ap_Coll_St_Cd);                                                             //Natural: MOVE AP-COLL-ST-CD TO #PRAP-STATE-CD
                                                                                                                                                                          //Natural: PERFORM GET-ALPHA-STATE-CODE
            sub_Get_Alpha_State_Code();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            ldaAppl160.getDoc_Ext_File_Dx_Gsra_Inst_St_Code().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Alpha_State_Cd());                                               //Natural: MOVE #ALPHA-STATE-CD TO DX-GSRA-INST-ST-CODE DX-ORIG-ISS-STATE
            ldaAppl160.getDoc_Ext_File_Dx_Orig_Iss_State().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Alpha_State_Cd());
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Current_State_Code.equals(" ")))                                                                                          //Natural: IF AP-CURRENT-STATE-CODE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(DbsUtil.maskMatches(annty_Actvty_Prap_View_Ap_Current_State_Code,"NN")))                                                                        //Natural: IF AP-CURRENT-STATE-CODE = MASK ( NN )
            {
                ldaAppl170.getPnd_Work_Fields_Pnd_Prap_State_Cd().setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                                 //Natural: MOVE AP-CURRENT-STATE-CODE TO #PRAP-STATE-CD
                                                                                                                                                                          //Natural: PERFORM GET-ALPHA-STATE-CODE
                sub_Get_Alpha_State_Code();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                ldaAppl160.getDoc_Ext_File_Dx_Current_Iss_State().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Alpha_State_Cd());                                           //Natural: MOVE #ALPHA-STATE-CD TO DX-CURRENT-ISS-STATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl160.getDoc_Ext_File_Dx_Current_Iss_State().setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                                 //Natural: MOVE AP-CURRENT-STATE-CODE TO DX-CURRENT-ISS-STATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  KG TNGSUB2 START - REPLACE CURRENT STATE WITH CONVERSION ISSUE STATE
        //*  FOR IRA SUBSTITUTION CONTRACTS
        if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.notEquals(" ")))                                                                                //Natural: IF AP-SUBSTITUTION-CONTRACT-IND NE ' '
        {
            if (condition(annty_Actvty_Prap_View_Ap_Conv_Issue_State.equals(" ")))                                                                                        //Natural: IF AP-CONV-ISSUE-STATE = ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(DbsUtil.maskMatches(annty_Actvty_Prap_View_Ap_Conv_Issue_State,"NN")))                                                                      //Natural: IF AP-CONV-ISSUE-STATE = MASK ( NN )
                {
                    ldaAppl170.getPnd_Work_Fields_Pnd_Prap_State_Cd().setValue(annty_Actvty_Prap_View_Ap_Conv_Issue_State);                                               //Natural: MOVE AP-CONV-ISSUE-STATE TO #PRAP-STATE-CD
                                                                                                                                                                          //Natural: PERFORM GET-ALPHA-STATE-CODE
                    sub_Get_Alpha_State_Code();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    ldaAppl160.getDoc_Ext_File_Dx_Current_Iss_State().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Alpha_State_Cd());                                       //Natural: MOVE #ALPHA-STATE-CD TO DX-CURRENT-ISS-STATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Current_Iss_State().setValue(annty_Actvty_Prap_View_Ap_Conv_Issue_State);                                               //Natural: MOVE AP-CONV-ISSUE-STATE TO DX-CURRENT-ISS-STATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  KG TNGSUB2 END
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Lob().setValue(annty_Actvty_Prap_View_Ap_Lob);                                                                                      //Natural: MOVE AP-LOB TO DX-LOB
        ldaAppl160.getDoc_Ext_File_Dx_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Lob_Type);                                                                            //Natural: MOVE AP-LOB-TYPE TO DX-LOB-TYPE
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Code().setValue(pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_4);                                                                      //Natural: MOVE #HOLD-COLL-CODE-4 TO DX-INST-CODE
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Bill_Cd().setValue(annty_Actvty_Prap_View_Ap_Bill_Code);                                                                       //Natural: MOVE AP-BILL-CODE TO DX-INST-BILL-CD
        ldaAppl160.getDoc_Ext_File_Dx_Appl_Source().setValue(annty_Actvty_Prap_View_Ap_App_Source);                                                                       //Natural: MOVE AP-APP-SOURCE TO DX-APPL-SOURCE
        ldaAppl170.getPnd_Work_Fields_Pnd_Hold_Region_Cd().setValue(annty_Actvty_Prap_View_Ap_Region_Code);                                                               //Natural: MOVE AP-REGION-CODE TO #HOLD-REGION-CD
        if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_Hold_Reg_Byte_3().equals("P")))                                                                                   //Natural: IF #HOLD-REG-BYTE-3 = 'P'
        {
            ldaAppl170.getPnd_Work_Fields_Pnd_Hold_Reg_Byte_3().setValue("R");                                                                                            //Natural: MOVE 'R' TO #HOLD-REG-BYTE-3
            ldaAppl160.getDoc_Ext_File_Dx_Region_Cd().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Hold_Region_Cd());                                                       //Natural: MOVE #HOLD-REGION-CD TO DX-REGION-CD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_Region_Cd().setValue(annty_Actvty_Prap_View_Ap_Region_Code);                                                                    //Natural: MOVE AP-REGION-CODE TO DX-REGION-CD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_Hold_Reg_Byte_1().equals("4")))                                                                                   //Natural: IF #HOLD-REG-BYTE-1 = '4'
        {
            ldaAppl160.getDoc_Ext_File_Dx_Region_Cd().setValue("13R");                                                                                                    //Natural: MOVE '13R' TO DX-REGION-CD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_Hold_Reg_Byte_1().equals("3")))                                                                               //Natural: IF #HOLD-REG-BYTE-1 = '3'
            {
                ldaAppl160.getDoc_Ext_File_Dx_Region_Cd().setValue("24S");                                                                                                //Natural: MOVE '24S' TO DX-REGION-CD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Eop_Addl_Cref_Req().setValue(annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request);                                                      //Natural: MOVE AP-EOP-ADDL-CREF-REQUEST TO DX-EOP-ADDL-CREF-REQ
        ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(ldaAppl170.getPnd_Current_Date_Yyyymmdd());                                                       //Natural: MOVE #CURRENT-DATE-YYYYMMDD TO #DATE-HOLD-YYYYMMDD
        //*   TNGSUB CML
        if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.equals(" ")))                                                                                   //Natural: IF AP-SUBSTITUTION-CONTRACT-IND = ' '
        {
            ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Hold_Mmyy().setValue(annty_Actvty_Prap_View_Ap_T_Doi);                                                                  //Natural: MOVE AP-T-DOI TO #DOI-HOLD-MMYY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Mm().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Mm());                                                             //Natural: MOVE #DOI-MM TO #DATE-MM
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Yy().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                             //Natural: MOVE #DOI-YY TO #DATE-YY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Dd().setValue(1);                                                                                                      //Natural: MOVE 01 TO #DATE-DD
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().reset();                                                                                           //Natural: RESET #Y2-YYMMDD-AREA1-A
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                    //Natural: MOVE #DOI-YY TO #Y2-YY-AREA1
                                                                                                                                                                          //Natural: PERFORM YR2000-DATE-COMPARE
            sub_Yr2000_Date_Compare();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Cc().setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1());                                                   //Natural: MOVE #Y2-CC-AREA1 TO #DATE-CC
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Tiaa_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                            //Natural: MOVE #DATE-CCYYMM TO #PAR-TIAA-DOI
            ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd());                                      //Natural: MOVE #DATE-HOLD-YYYYMMDD TO DX-DATE-TABLE ( 1 )
            ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Hold_Mmyy().setValue(annty_Actvty_Prap_View_Ap_C_Doi);                                                                  //Natural: MOVE AP-C-DOI TO #DOI-HOLD-MMYY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Mm().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Mm());                                                             //Natural: MOVE #DOI-MM TO #DATE-MM
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Yy().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                             //Natural: MOVE #DOI-YY TO #DATE-YY
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Dd().setValue(1);                                                                                                      //Natural: MOVE 01 TO #DATE-DD
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().reset();                                                                                           //Natural: RESET #Y2-YYMMDD-AREA1-A
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Doi_Yy());                                                    //Natural: MOVE #DOI-YY TO #Y2-YY-AREA1
                                                                                                                                                                          //Natural: PERFORM YR2000-DATE-COMPARE
            sub_Yr2000_Date_Compare();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Cc().setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1());                                                   //Natural: MOVE #Y2-CC-AREA1 TO #DATE-CC
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Cref_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                            //Natural: MOVE #DATE-CCYYMM TO #PAR-CREF-DOI
            ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd());                                      //Natural: MOVE #DATE-HOLD-YYYYMMDD TO DX-DATE-TABLE ( 2 )
            //*  TNGSUB CML START
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Date_Numeric_Pnd_Date_Alpha.setValueEdited(annty_Actvty_Prap_View_Ap_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED AP-TIAA-DOI ( EM = YYYYMMDD ) TO #DATE-ALPHA
            //*  041813
            ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).setValue(pnd_Date_Numeric);                                                                            //Natural: MOVE #DATE-NUMERIC TO DX-DATE-TABLE ( 1 ) #DATE-HOLD-YYYYMMDD
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(pnd_Date_Numeric);
            //*  041813
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Tiaa_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                            //Natural: MOVE #DATE-CCYYMM TO #PAR-TIAA-DOI
            pnd_Date_Numeric_Pnd_Date_Alpha.setValueEdited(annty_Actvty_Prap_View_Ap_Cref_Doi,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED AP-CREF-DOI ( EM = YYYYMMDD ) TO #DATE-ALPHA
            //*  041813
            ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).setValue(pnd_Date_Numeric);                                                                            //Natural: MOVE #DATE-NUMERIC TO DX-DATE-TABLE ( 2 ) #DATE-HOLD-YYYYMMDD
            ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(pnd_Date_Numeric);
            //*  041813
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Cref_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                            //Natural: MOVE #DATE-CCYYMM TO #PAR-CREF-DOI
            //*  TNGSUB3
            //*  TNGSUB END TNGSUB3
            ldaAppl160.getDoc_Ext_File_Dx_Substitution_Contract_Ind().setValue(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind);                                      //Natural: MOVE AP-SUBSTITUTION-CONTRACT-IND TO DX-SUBSTITUTION-CONTRACT-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  TIAA ONLY
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Type_Cd().equals("1")))                                                                                               //Natural: IF DX-TYPE-CD = '1'
        {
            //*  JRB2
            ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).moveAll("0");                                                                                          //Natural: MOVE ALL '0' TO DX-DATE-TABLE ( 2 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CREF ONLY
            if (condition(ldaAppl160.getDoc_Ext_File_Dx_Type_Cd().equals("2")))                                                                                           //Natural: IF DX-TYPE-CD = '2'
            {
                //*  JRB2
                ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).moveAll("0");                                                                                      //Natural: MOVE ALL '0' TO DX-DATE-TABLE ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  LPAO KG START - MOVE ANNUITY ISSUE DATES TO THE ISSUE DATE ON THE
        //*  EXTRACT FILE AND #PAR FIELDS FOR GROUP PRODUCTS SUBJECT TO LPAO.
        //*  HOLD-ORIG DATES USED TO POPULATE ORIGINAL ISSUE DATES ON ACIS-REPRINT
        ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().resetInitial();                                                                                            //Natural: RESET INITIAL #DATE-HOLD-YYYYMMDD #HOLD-ORIG-TIAA-DOI #HOLD-ORIG-CREF-DOI
        pnd_Hold_Orig_Tiaa_Doi.resetInitial();
        pnd_Hold_Orig_Cref_Doi.resetInitial();
        if (condition((annty_Actvty_Prap_View_Ap_Legal_Ann_Option.equals("Y") && (((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GRA") ||                    //Natural: IF AP-LEGAL-ANN-OPTION = 'Y' AND ( #CONTRACT-TYPE = 'GRA' OR = 'GSRA' OR = 'RC' OR = 'RCP' )
            ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GSRA")) || ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RC")) || ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RCP")))))
        {
            if (condition((ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).notEquals("0") && ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).notEquals(" "))  //Natural: IF ( DX-DATE-TABLE ( 1 ) NE '0' AND DX-DATE-TABLE ( 1 ) NE ' ' ) AND AP-TIAA-ANN-ISSUE-DT NE 0
                && annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt.notEquals(getZero())))
            {
                pnd_Hold_Orig_Tiaa_Doi_Pnd_Hold_Orig_Tiaa_Doi_A.setValue(ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1));                                         //Natural: MOVE DX-DATE-TABLE ( 1 ) TO #HOLD-ORIG-TIAA-DOI-A
                ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).setValue(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt);                                             //Natural: MOVE AP-TIAA-ANN-ISSUE-DT TO DX-DATE-TABLE ( 1 )
                ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt);                                             //Natural: MOVE AP-TIAA-ANN-ISSUE-DT TO #DATE-HOLD-YYYYMMDD
                pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Tiaa_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                        //Natural: MOVE #DATE-CCYYMM TO #PAR-TIAA-DOI
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).notEquals("0") && ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).notEquals(" "))  //Natural: IF ( DX-DATE-TABLE ( 2 ) NE '0' AND DX-DATE-TABLE ( 2 ) NE ' ' ) AND AP-CREF-ANN-ISSUE-DT NE 0
                && annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt.notEquals(getZero())))
            {
                pnd_Hold_Orig_Cref_Doi_Pnd_Hold_Orig_Cref_Doi_A.setValue(ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2));                                         //Natural: MOVE DX-DATE-TABLE ( 2 ) TO #HOLD-ORIG-CREF-DOI-A
                ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).setValue(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt);                                             //Natural: MOVE AP-CREF-ANN-ISSUE-DT TO DX-DATE-TABLE ( 2 )
                ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt);                                             //Natural: MOVE AP-CREF-ANN-ISSUE-DT TO #DATE-HOLD-YYYYMMDD
                pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Cref_Doi().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Date_Ccyymm());                                        //Natural: MOVE #DATE-CCYYMM TO #PAR-CREF-DOI
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  LPAO KG END
        ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(3,":",4).setValue(annty_Actvty_Prap_View_Ap_Annuity_Start_Date);                                              //Natural: MOVE AP-ANNUITY-START-DATE TO DX-DATE-TABLE ( 3:4 )
        ldaAppl170.getPnd_Work_Fields_Pnd_Date_Hold_Yyyymmdd().setValue(ldaAppl170.getPnd_Current_Date_Yyyymmdd());                                                       //Natural: MOVE #CURRENT-DATE-YYYYMMDD TO #DATE-HOLD-YYYYMMDD
        ldaAppl170.getPnd_Mp1_Date_Mmddyyyy().setValue(annty_Actvty_Prap_View_Ap_Dob);                                                                                    //Natural: MOVE AP-DOB TO #MP1-DATE-MMDDYYYY
        ldaAppl170.getPnd_Mp2_Date_Yyyymmdd_Pnd_Mp2_Date_Mm().setValue(ldaAppl170.getPnd_Mp1_Date_Mmddyyyy_Pnd_Mp1_Date_Mm());                                            //Natural: MOVE #MP1-DATE-MM TO #MP2-DATE-MM
        ldaAppl170.getPnd_Mp2_Date_Yyyymmdd_Pnd_Mp2_Date_Dd().setValue(ldaAppl170.getPnd_Mp1_Date_Mmddyyyy_Pnd_Mp1_Date_Dd());                                            //Natural: MOVE #MP1-DATE-DD TO #MP2-DATE-DD
        ldaAppl170.getPnd_Mp2_Date_Yyyymmdd_Pnd_Mp2_Date_Yyyy().setValue(ldaAppl170.getPnd_Mp1_Date_Mmddyyyy_Pnd_Mp1_Date_Yyyy());                                        //Natural: MOVE #MP1-DATE-YYYY TO #MP2-DATE-YYYY
        ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(5).setValue(ldaAppl170.getPnd_Mp2_Date_Yyyymmdd());                                                           //Natural: MOVE #MP2-DATE-YYYYMMDD TO DX-DATE-TABLE ( 5 )
        ldaAppl170.getPnd_Mp1_Date_Mmddyyyy().setValue(annty_Actvty_Prap_View_Ap_Dt_App_Recvd);                                                                           //Natural: MOVE AP-DT-APP-RECVD TO #MP1-DATE-MMDDYYYY
        ldaAppl170.getPnd_Mp2_Date_Yyyymmdd_Pnd_Mp2_Date_Mm().setValue(ldaAppl170.getPnd_Mp1_Date_Mmddyyyy_Pnd_Mp1_Date_Mm());                                            //Natural: MOVE #MP1-DATE-MM TO #MP2-DATE-MM
        ldaAppl170.getPnd_Mp2_Date_Yyyymmdd_Pnd_Mp2_Date_Dd().setValue(ldaAppl170.getPnd_Mp1_Date_Mmddyyyy_Pnd_Mp1_Date_Dd());                                            //Natural: MOVE #MP1-DATE-DD TO #MP2-DATE-DD
        ldaAppl170.getPnd_Mp2_Date_Yyyymmdd_Pnd_Mp2_Date_Yyyy().setValue(ldaAppl170.getPnd_Mp1_Date_Mmddyyyy_Pnd_Mp1_Date_Yyyy());                                        //Natural: MOVE #MP1-DATE-YYYY TO #MP2-DATE-YYYY
        ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(6).setValue(ldaAppl170.getPnd_Mp2_Date_Yyyymmdd());                                                           //Natural: MOVE #MP2-DATE-YYYYMMDD TO DX-DATE-TABLE ( 6 )
        ldaAppl160.getDoc_Ext_File_Dx_Pin_No().setValue(annty_Actvty_Prap_View_Ap_Pin_Nbr);                                                                               //Natural: MOVE AP-PIN-NBR TO DX-PIN-NO #PIN-ID
        pnd_Pin_Id.setValue(annty_Actvty_Prap_View_Ap_Pin_Nbr);
        //*  KG TNGSUB
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PART-STATUS
        sub_Determine_Part_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*                                                          /* >>> (TIC)
        if (condition(pnd_Input_Parm.equals("LEGAL") && (pdaScia8100.getDoc_Rec_Doc_Tic_Startdate().notEquals(space))))                                                   //Natural: IF #INPUT-PARM = 'LEGAL' AND ( DOC-TIC-STARTDATE NOT = SPACE )
        {
            //* *.....  IF RP-TIC-ENDDATE 30 DAYS GT RP-DT-APP-RECVD, AND IS RC OR RCP,
            //* *.....  SET DX-TIC-IND TO 'Y'
            if (condition(pnd_Rc_Or_Rcp.getBoolean()))                                                                                                                    //Natural: IF #RC-OR-RCP
            {
                //*        MOVE EDITED DOC-TIC-STARTDATE  TO
                //*                                       #DATE-STARTDATE-D (EM=YYYYMMDD)
                pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaScia8100.getDoc_Rec_Doc_Tic_Enddate());                                                         //Natural: MOVE EDITED DOC-TIC-ENDDATE TO #DATE ( EM = YYYYMMDD )
                pnd_Date_Diff.setValue(annty_Actvty_Prap_View_Ap_Dt_App_Recvd);                                                                                           //Natural: ASSIGN #DATE-DIFF := AP-DT-APP-RECVD
                pnd_Date_Yyyymmdd_Pnd_Date_Cc_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Cc);                                                                                 //Natural: ASSIGN #DATE-CC-1 := #DATE-DIFF-CC
                pnd_Date_Yyyymmdd_Pnd_Date_Yy_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Yy);                                                                                 //Natural: ASSIGN #DATE-YY-1 := #DATE-DIFF-YY
                pnd_Date_Yyyymmdd_Pnd_Date_Mm_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Mm);                                                                                 //Natural: ASSIGN #DATE-MM-1 := #DATE-DIFF-MM
                pnd_Date_Yyyymmdd_Pnd_Date_Dd_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Dd);                                                                                 //Natural: ASSIGN #DATE-DD-1 := #DATE-DIFF-DD
                pnd_Date_Numeric.setValue(pnd_Date_Yyyymmdd);                                                                                                             //Natural: MOVE #DATE-YYYYMMDD TO #DATE-NUMERIC
                pnd_Date_1.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Numeric_Pnd_Date_Alpha);                                                                //Natural: MOVE EDITED #DATE-ALPHA TO #DATE-1 ( EM = YYYYMMDD )
                pnd_Date.nadd(30);                                                                                                                                        //Natural: COMPUTE #DATE = #DATE + 30
                //*        IF #DATE-1 GE #DATE-STARTDATE-D  /* APP-RECVD >= TIC-SD LS1
                //*  APP-RECVD <= TIC-ENDDATE
                if (condition(pnd_Date_1.lessOrEqual(pnd_Date)))                                                                                                          //Natural: IF #DATE-1 LE #DATE
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Ind().setValue("Y");                                                                                                //Natural: ASSIGN DX-TIC-IND := 'Y'
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Startdate());                                                            //Natural: MOVE DOC-TIC-STARTDATE TO #AP-TIC-STARTDATE-A DX-TIC-STARTDATE
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Startdate().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Startdate());
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Enddate());                                                                //Natural: MOVE DOC-TIC-ENDDATE TO #AP-TIC-ENDDATE-A DX-TIC-ENDDATE
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Enddate().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Enddate());
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Percentage());                                                          //Natural: MOVE DOC-TIC-PERCENTAGE TO #AP-TIC-PERCENTAGE-A DX-TIC-PERCENTAGE
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Percentage().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Percentage());
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Postdays());                                                              //Natural: MOVE DOC-TIC-POSTDAYS TO #AP-TIC-POSTDAYS-A DX-TIC-POSTDAYS
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Postdays().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Postdays());
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Limit());                                                                    //Natural: MOVE DOC-TIC-LIMIT TO #AP-TIC-LIMIT-A DX-TIC-LIMIT
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Limit().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Limit());
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Postfreq().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Postfreq());                                                     //Natural: MOVE DOC-TIC-POSTFREQ TO DX-TIC-POSTFREQ
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Pl_Level().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Pl_Level());                                                     //Natural: MOVE DOC-TIC-PL-LEVEL TO DX-TIC-PL-LEVEL
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Windowdays());                                                          //Natural: MOVE DOC-TIC-WINDOWDAYS TO #AP-TIC-WINDOWDAYS-A DX-TIC-WINDOWDAYS
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Windowdays().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Windowdays());
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Reqdlywindow());                                                      //Natural: MOVE DOC-TIC-REQDLYWINDOW TO #AP-TIC-REQDLYWINDOW-A DX-TIC-REQDLYWINDOW
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Reqdlywindow().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Reqdlywindow());
                    pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_A.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Recap_Prov());                                                          //Natural: MOVE DOC-TIC-RECAP-PROV TO #AP-TIC-RECAP-PROV-A DX-TIC-RECAP-PROV
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Recap_Prov().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Recap_Prov());
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Tic_Ind().setValue("N");                                                                                                //Natural: ASSIGN DX-TIC-IND := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  (TIC) CS <<<<
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Unit_Name().setValue(annty_Actvty_Prap_View_Ap_Ppg_Team_Cde);                                                                       //Natural: ASSIGN DX-UNIT-NAME := AP-PPG-TEAM-CDE
        if (condition(annty_Actvty_Prap_View_Ap_Ppg_Team_Cde.equals(" ")))                                                                                                //Natural: IF AP-PPG-TEAM-CDE = ' '
        {
            //*  SGRD KG
            annty_Actvty_Prap_View_Ap_Ppg_Team_Cde.setValue("TMOMN");                                                                                                     //Natural: MOVE 'TMOMN' TO AP-PPG-TEAM-CDE
            //*  SGRD KG
            ldaAppl160.getDoc_Ext_File_Dx_Unit_Name().setValue("TMOMN");                                                                                                  //Natural: MOVE 'TMOMN' TO DX-UNIT-NAME
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Unit_Name().equals(" ")))                                                                                             //Natural: IF DX-UNIT-NAME = ' '
        {
            ldaAppl160.getDoc_Ext_File_Dx_Unit_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "UKWN-", ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type())); //Natural: COMPRESS 'UKWN-' #CONTRACT-TYPE INTO DX-UNIT-NAME LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        //* * AP-APPLCNT-REQ-TYPE = B - BULK ENROLLMENT
        //* *                       I - INTERNET DATA ENROLLMENT
        //* *                       R - REQULAR ACIS ISSUE
        //* *
        //* * #ERISA-IND = Y -ERISA PLAN
        //* *              N - NON-ERISA PLAN
        ldaAppl160.getDoc_Ext_File_Dx_Enroll_Request_Type().setValue(annty_Actvty_Prap_View_Ap_Applcnt_Req_Type);                                                         //Natural: MOVE AP-APPLCNT-REQ-TYPE TO DX-ENROLL-REQUEST-TYPE
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Erisa_Inst().setValue(pdaScia8100.getDoc_Rec_Doc_Inst_Erisa_Ind());                                                                 //Natural: MOVE DOC-INST-ERISA-IND TO DX-ERISA-INST
        ldaAppl160.getDoc_Ext_File_Dx_Mult_App_Version().reset();                                                                                                         //Natural: RESET DX-MULT-APP-VERSION
        if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("3") && annty_Actvty_Prap_View_Ap_Mult_App_Status.equals(" ")))) //Natural: IF ( AP-LOB = 'S' AND AP-LOB-TYPE = '3' AND AP-MULT-APP-STATUS = ' ' )
        {
            annty_Actvty_Prap_View_Ap_Mult_App_Status.setValue("C");                                                                                                      //Natural: ASSIGN AP-MULT-APP-STATUS := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Mult_App_Status.notEquals(" ")))                                                                                          //Natural: IF AP-MULT-APP-STATUS NE ' '
        {
            ldaAppl160.getDoc_Ext_File_Dx_Mult_App_Version().setValue("Y");                                                                                               //Natural: MOVE 'Y' TO DX-MULT-APP-VERSION
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Mult_App_Status().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Status);                                                              //Natural: MOVE AP-MULT-APP-STATUS TO DX-MULT-APP-STATUS
        ldaAppl160.getDoc_Ext_File_Dx_Mult_App_Lob().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Lob);                                                                    //Natural: MOVE AP-MULT-APP-LOB TO DX-MULT-APP-LOB
        ldaAppl160.getDoc_Ext_File_Dx_Mult_App_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type);                                                          //Natural: MOVE AP-MULT-APP-LOB-TYPE TO DX-MULT-APP-LOB-TYPE
        ldaAppl160.getDoc_Ext_File_Dx_Mult_App_Ppg().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Ppg);                                                                    //Natural: MOVE AP-MULT-APP-PPG TO DX-MULT-APP-PPG
        ldaAppl160.getDoc_Ext_File_Dx_Ira_Rollover_Type().setValue(annty_Actvty_Prap_View_Ap_Ira_Rollover_Type);                                                          //Natural: MOVE AP-IRA-ROLLOVER-TYPE TO DX-IRA-ROLLOVER-TYPE
        ldaAppl160.getDoc_Ext_File_Dx_Ira_Record_Type().setValue(annty_Actvty_Prap_View_Ap_Ira_Record_Type);                                                              //Natural: MOVE AP-IRA-RECORD-TYPE TO DX-IRA-RECORD-TYPE
        ldaAppl160.getDoc_Ext_File_Dx_Mit_Unit().getValue("*").setValue(annty_Actvty_Prap_View_Ap_Mit_Unit.getValue("*"));                                                //Natural: MOVE AP-MIT-UNIT ( * ) TO DX-MIT-UNIT ( * )
        ldaAppl160.getDoc_Ext_File_Dx_Mit_Wpid().getValue("*").setValue(annty_Actvty_Prap_View_Ap_Mit_Wpid.getValue("*"));                                                //Natural: MOVE AP-MIT-WPID ( * ) TO DX-MIT-WPID ( * )
        ldaAppl160.getDoc_Ext_File_Dx_New_Bene_Ind().setValue("Y");                                                                                                       //Natural: MOVE 'Y' TO DX-NEW-BENE-IND
        ldaAppl160.getDoc_Ext_File_Dx_Irc_Sectn_Grp().setValue(annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde);                                                              //Natural: MOVE AP-IRC-SECTN-GRP-CDE TO DX-IRC-SECTN-GRP
        ldaAppl160.getDoc_Ext_File_Dx_Irc_Sectn_Cde().setValue(annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde);                                                                  //Natural: MOVE AP-IRC-SECTN-CDE TO DX-IRC-SECTN-CDE
        ldaAppl160.getDoc_Ext_File_Dx_Portfolio_Selected().setValue(annty_Actvty_Prap_View_Ap_Allocation_Model_Type);                                                     //Natural: MOVE AP-ALLOCATION-MODEL-TYPE TO DX-PORTFOLIO-SELECTED
        ldaAppl160.getDoc_Ext_File_Dx_Divorce_Contract().setValue(annty_Actvty_Prap_View_Ap_Divorce_Ind);                                                                 //Natural: MOVE AP-DIVORCE-IND TO DX-DIVORCE-CONTRACT
        //*  JRB1
        DbsUtil.examine(new ExamineSource(annty_Actvty_Prap_View_Ap_Email_Address,true), new ExamineSearch("�"), new ExamineGivingNumber(pnd_Nbr));                       //Natural: EXAMINE FULL AP-EMAIL-ADDRESS FOR '�' GIVING NUMBER #NBR
        //*  JRB1
        //*  JRB1
        if (condition(pnd_Nbr.greater(getZero()) || ! (DbsUtil.maskMatches(annty_Actvty_Prap_View_Ap_Email_Address,"PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP")))) //Natural: IF #NBR > 0 OR ( AP-EMAIL-ADDRESS NOT = MASK ( PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP ) )
        {
            //*  JRB1
            //*  JRB1
            getReports().write(0, "INVALID EMAIL ADDRESS:",annty_Actvty_Prap_View_Ap_Email_Address,"CONTRACT:",annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                    //Natural: WRITE 'INVALID EMAIL ADDRESS:' AP-EMAIL-ADDRESS 'CONTRACT:' AP-TIAA-CNTRCT
            if (Global.isEscape()) return;
            //*  JRB1
            ldaAppl160.getDoc_Ext_File_Dx_Email_Address().reset();                                                                                                        //Natural: RESET DX-EMAIL-ADDRESS
            //*  JRB1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  JRB1
            ldaAppl160.getDoc_Ext_File_Dx_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Email_Address);                                                              //Natural: MOVE AP-EMAIL-ADDRESS TO DX-EMAIL-ADDRESS
            //*  JRB1
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Alloc_Fmt().setValue(annty_Actvty_Prap_View_Ap_Allocation_Fmt);                                                                     //Natural: MOVE AP-ALLOCATION-FMT TO DX-ALLOC-FMT
        ldaAppl160.getDoc_Ext_File_Dx_Phone_No().setValue(annty_Actvty_Prap_View_Ap_Phone_No);                                                                            //Natural: MOVE AP-PHONE-NO TO DX-PHONE-NO
        //* * SGRD KG - DX-K12-PPG FIELD NOT NEEDED FOR SUNGARD
        ldaAppl160.getDoc_Ext_File_Dx_K12_Ppg().reset();                                                                                                                  //Natural: RESET DX-K12-PPG
        //*  INTERNAL IRA
        if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("I") && annty_Actvty_Prap_View_Ap_Ira_Record_Type.equals("I"))))                                              //Natural: IF ( AP-LOB = 'I' AND AP-IRA-RECORD-TYPE = 'I' )
        {
            ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).setValueEdited(annty_Actvty_Prap_View_Ap_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                     //Natural: MOVE EDITED AP-TIAA-DOI ( EM = YYYYMMDD ) TO DX-DATE-TABLE ( 1 )
            ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).setValueEdited(annty_Actvty_Prap_View_Ap_Cref_Doi,new ReportEditMask("YYYYMMDD"));                     //Natural: MOVE EDITED AP-CREF-DOI ( EM = YYYYMMDD ) TO DX-DATE-TABLE ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  TIGR - COMMENTED OUT THE LINES BELOW COZ AP-FINANCIAL-1 IS NOW USED
        //*         FOR TIAA INDEX GUARANTEED RATE
        //* *IF  AP-FINANCIAL-1 > 0
        //* *  MOVE EDITED AP-FINANCIAL-1  (EM=ZZ,ZZZ,ZZ9.99) TO #ROLLOVER-AMT
        //* *  COMPRESS '$' #ROLLOVER-AMT-N INTO DX-ROLLOVER-AMT LEAVING NO SPACE
        //* *ELSE
        ldaAppl160.getDoc_Ext_File_Dx_Rollover_Amt().setValue("          0.00");                                                                                          //Natural: MOVE '          0.00' TO DX-ROLLOVER-AMT
        //* *END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Eop_State_Name().setValue("E");                                                                                                     //Natural: MOVE 'E' TO DX-EOP-STATE-NAME
        FOR04:                                                                                                                                                            //Natural: FOR #T 1 62
        for (ldaAppl170.getPnd_Work_Fields_Pnd_T().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_T().lessOrEqual(62)); ldaAppl170.getPnd_Work_Fields_Pnd_T().nadd(1))
        {
            pnd_W_State_Code_Tab.setValue(ldaAppl170.getPnd_State_Code_Table().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T()));                                          //Natural: ASSIGN #W-STATE-CODE-TAB := #STATE-CODE-TABLE ( #T )
            //*  KG TNGSUB
            if (condition(pnd_Group_Product.getBoolean() || (ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("KEOG"))))                                          //Natural: IF #GROUP-PRODUCT OR ( #CONTRACT-TYPE = 'KEOG' )
            {
                if (condition(annty_Actvty_Prap_View_Ap_Coll_St_Cd.equals(pnd_W_State_Code_Tab_Pnd_W_State_Code_N)))                                                      //Natural: IF AP-COLL-ST-CD = #W-STATE-CODE-N
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Eop_State_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_State_Code_Tab_Pnd_W_State_Name,        //Natural: COMPRESS #W-STATE-NAME '.' INTO DX-EOP-STATE-NAME LEAVING NO
                        "."));
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   TNGSUB
                if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.equals(" ")))                                                                           //Natural: IF AP-SUBSTITUTION-CONTRACT-IND = ' '
                {
                    if (condition(annty_Actvty_Prap_View_Ap_Current_State_Code.equals(pnd_W_State_Code_Tab_Pnd_W_State_Code_N)))                                          //Natural: IF AP-CURRENT-STATE-CODE = #W-STATE-CODE-N
                    {
                        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_State_Cd().setValue(pnd_W_State_Code_Tab_Pnd_W_State_Code_A);                                    //Natural: MOVE #W-STATE-CODE-A TO #PAR-GRM-STATE-CD
                        ldaAppl160.getDoc_Ext_File_Dx_Eop_State_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_State_Code_Tab_Pnd_W_State_Name,    //Natural: COMPRESS #W-STATE-NAME '.' INTO DX-EOP-STATE-NAME LEAVING NO
                            "."));
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*   TNGSUB START
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(annty_Actvty_Prap_View_Ap_Conv_Issue_State.equals(pnd_W_State_Code_Tab_Pnd_W_State_Code_A)))                                            //Natural: IF AP-CONV-ISSUE-STATE = #W-STATE-CODE-A
                    {
                        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_State_Cd().setValue(pnd_W_State_Code_Tab_Pnd_W_State_Code_A);                                    //Natural: MOVE #W-STATE-CODE-A TO #PAR-GRM-STATE-CD
                        ldaAppl160.getDoc_Ext_File_Dx_Eop_State_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_State_Code_Tab_Pnd_W_State_Name,    //Natural: COMPRESS #W-STATE-NAME '.' INTO DX-EOP-STATE-NAME LEAVING NO
                            "."));
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TNGSUB END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-FORM-NUM
        sub_Determine_Form_Num();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ED-NUM
        sub_Determine_Ed_Num();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaAppl170.getPnd_Work_Fields_Pnd_Idf_Sk_Inst_Cntl_Cd().setValue("C");                                                                                            //Natural: ASSIGN #IDF-SK-INST-CNTL-CD = 'C'
        ldaAppl170.getPnd_Work_Fields_Pnd_Idf_Sk_Inst_Coll_Cd().setValue(pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_4);                                                        //Natural: ASSIGN #IDF-SK-INST-COLL-CD = #HOLD-COLL-CODE-4
        ldaAppl170.getPnd_Work_Fields_Pnd_Idf_Sk_Filler().setValue(" ");                                                                                                  //Natural: ASSIGN #IDF-SK-FILLER = ' '
        ldaAppl170.getPnd_Work_Fields_Pnd_Idf_Sk_Low_Value().setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Idf_Lv());                                                        //Natural: ASSIGN #IDF-SK-LOW-VALUE = #IDF-LV
        //*  BJD AE
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Name_Table().getValue(1).setValue(pdaScia8100.getDoc_Rec_Doc_Plan_Name().getSubstring(1,30));                                  //Natural: MOVE SUBSTR ( DOC-PLAN-NAME,1,30 ) TO DX-INST-NAME-TABLE ( 1 )
        //*  BJD AE
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Name_Table().getValue(2).setValue(pdaScia8100.getDoc_Rec_Doc_Plan_Name().getSubstring(31,30));                                 //Natural: MOVE SUBSTR ( DOC-PLAN-NAME,31,30 ) TO DX-INST-NAME-TABLE ( 2 )
        //*  BJD AE
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Name_Table().getValue(3).setValue(pdaScia8100.getDoc_Rec_Doc_Plan_Name().getSubstring(61,4));                                  //Natural: MOVE SUBSTR ( DOC-PLAN-NAME,61,4 ) TO DX-INST-NAME-TABLE ( 3 )
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Name_76().setValue(pdaScia8100.getDoc_Rec_Doc_Inst_Name());                                                                    //Natural: MOVE DOC-INST-NAME TO DX-INST-NAME-76
        //*  SGRD KG
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Permit_Trans().setValue(pdaScia8100.getDoc_Rec_Doc_Inst_Ext_Xfer_Ind());                                                       //Natural: MOVE DOC-INST-EXT-XFER-IND TO DX-INST-PERMIT-TRANS DX-PERM-GRA-TRANS
        ldaAppl160.getDoc_Ext_File_Dx_Perm_Gra_Trans().setValue(pdaScia8100.getDoc_Rec_Doc_Inst_Ext_Xfer_Ind());
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Gsra_Loan_Ind().setValue(pdaScia8100.getDoc_Rec_Doc_Inst_Gsra_Loans_Ind());                                                         //Natural: MOVE DOC-INST-GSRA-LOANS-IND TO DX-GSRA-LOAN-IND
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Cntrl_Consent().setValue(" ");                                                                                                 //Natural: MOVE ' ' TO DX-INST-CNTRL-CONSENT DX-INST-FIXED-PER-OPT DX-INST-CASHABLE-RA DX-INST-CASHABLE-GRA
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Fixed_Per_Opt().setValue(" ");
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Cashable_Ra().setValue(" ");
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Cashable_Gra().setValue(" ");
        //*  SGRD4 KG
        ldaAppl160.getDoc_Ext_File_Dx_Sg_Plan_Id().setValue(pdaScia8100.getDoc_Rec_Doc_Plan_Id());                                                                        //Natural: MOVE DOC-PLAN-ID TO DX-SG-PLAN-ID
        //*  SGRD4 KG
        ldaAppl160.getDoc_Ext_File_Dx_Employer_Name().setValue(pdaScia8100.getDoc_Rec_Doc_Employer_Name());                                                               //Natural: MOVE DOC-EMPLOYER-NAME TO DX-EMPLOYER-NAME
        //*  IMBS
        ldaAppl160.getDoc_Ext_File_Dx_Spec_Fund_Ind().setValue(annty_Actvty_Prap_View_Ap_Tsr_Ind);                                                                        //Natural: MOVE AP-TSR-IND TO DX-SPEC-FUND-IND
        //*  SGRDAW KG
        //*  SGRDAW KG
        ldaAppl160.getDoc_Ext_File_Dx_Product_Price_Level().setValue(pdaScia8100.getDoc_Rec_Doc_Product_Pricing_Level());                                                 //Natural: MOVE DOC-PRODUCT-PRICING-LEVEL TO DX-PRODUCT-PRICE-LEVEL
        //*  ROTH KG
        ldaAppl160.getDoc_Ext_File_Dx_Roth_Ind().setValue(pdaScia8100.getDoc_Rec_Doc_Roth_Ind());                                                                         //Natural: MOVE DOC-ROTH-IND TO DX-ROTH-IND
        //*  SGRD5 KG
        if (condition(pdaScia8100.getDoc_Rec_Doc_Guar_Int_Rate_Out().equals(new DbsDecimal("0.0"))))                                                                      //Natural: IF DOC-GUAR-INT-RATE-OUT = 0.0
        {
            //*  SGRD5 KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Rate().setValue(0);                                                                                                        //Natural: MOVE 0 TO DX-TIAA-RATE
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SGRD4 KG
            //*  ROUNDED UP SGRD4 KG
            pdaScia8100.getDoc_Rec_Doc_Guar_Int_Rate_Out().nadd(0);                                                                                                       //Natural: COMPUTE DOC-GUAR-INT-RATE-OUT = DOC-GUAR-INT-RATE-OUT + .005
            //*  SGRD4 KG
            //*  SGRD4 KG
            pnd_Hold_Interest_Rt.setValueEdited(pdaScia8100.getDoc_Rec_Doc_Guar_Int_Rate_Out(),new ReportEditMask("ZZ9.999"));                                            //Natural: MOVE EDITED DOC-GUAR-INT-RATE-OUT ( EM = ZZ9.999 ) TO #HOLD-INTEREST-RT
            //*  SGRD4 KG
            //*  SGRD4 KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Rate().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Hold_Interest_Rt_Pnd_Hold_Int_Rate_Print,              //Natural: COMPRESS #HOLD-INT-RATE-PRINT '%' '.' INTO DX-TIAA-RATE LEAVING NO
                "%", "."));
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: END-IF
        //* * SGRD3 KG - ADDED OMNI-ISSUE-IND TO EXTRACT FILE
        //*  SGRD3 KG
        ldaAppl160.getDoc_Ext_File_Dx_Omni_Issue_Ind().setValue(annty_Actvty_Prap_View_Omni_Issue_Ind);                                                                   //Natural: MOVE OMNI-ISSUE-IND TO DX-OMNI-ISSUE-IND
        //*  SGRD7 KG
        ldaAppl160.getDoc_Ext_File_Dx_Access_Cd_Ind().setValue("N");                                                                                                      //Natural: MOVE 'N' TO DX-ACCESS-CD-IND
        //*  SGRD7 KG
        ldaAppl160.getDoc_Ext_File_Dx_Dflt_Access_Code().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO DX-DFLT-ACCESS-CODE
        //*  SGRD7 KG
        if (condition(annty_Actvty_Prap_View_Omni_Issue_Ind.equals("2")))                                                                                                 //Natural: IF OMNI-ISSUE-IND = '2'
        {
            //*  SGRD7 KG
                                                                                                                                                                          //Natural: PERFORM CHECK-DEFLT-ENROLL
            sub_Check_Deflt_Enroll();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  SGRD7 KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRD7 KG
        //*  SGRD7 KG
        pdaScia8200.getCon_Part_Rec().reset();                                                                                                                            //Natural: RESET CON-PART-REC
        pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("N");                                                                                                           //Natural: ASSIGN CON-EOF-SW := 'N'
        //*  SGRD3 KG
                                                                                                                                                                          //Natural: PERFORM CALL-CNTCT-INTERFACE
        sub_Call_Cntct_Interface();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue(1).setValue("E");                                                                                     //Natural: MOVE 'E' TO DX-INST-ADDRESS-TABLE ( 1 )
        if (condition(pnd_Ac_Pda_Pnd_Last_Nme.getValue(1).notEquals(" ") || pnd_Ac_Pda_Pnd_Address_Line_1.getValue(1).notEquals(" ")))                                    //Natural: IF #LAST-NME ( 1 ) NE ' ' OR #ADDRESS-LINE-1 ( 1 ) NE ' '
        {
            pnd_Ac_Pda_Pnd_Compressed_Nme.getValue(1).setValue(DbsUtil.compress(pnd_Ac_Pda_Pnd_First_Nme.getValue(1), " ", pnd_Ac_Pda_Pnd_Middle_Nme.getValue(1),         //Natural: COMPRESS #FIRST-NME ( 1 ) ' ' #MIDDLE-NME ( 1 ) ' ' #LAST-NME ( 1 ) INTO #COMPRESSED-NME ( 1 ) LEAVING
                " ", pnd_Ac_Pda_Pnd_Last_Nme.getValue(1)));
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Parameter_Data().getValue(1).setValue(pnd_Ac_Pda_Pnd_Compressed_Nme.getValue(1));                                //Natural: MOVE #COMPRESSED-NME ( 1 ) TO #PAR-PARAMETER-DATA ( 1 )
            pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Parameter_Count().setValue(1);                                                                                   //Natural: MOVE 1 TO #PAR-PARAMETER-COUNT
            //*  LOWER CASE CONV
            DbsUtil.callnat(Appn100.class , getCurrentProcessState(), pdaAppa100.getPnd_Par_Parameter_Area());                                                            //Natural: CALLNAT 'APPN100' #PAR-PARAMETER-AREA
            if (condition(Global.isEscape())) return;
            pnd_Ac_Pda_Pnd_Compressed_Nme.getValue(1).setValue(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Parameter_Data().getValue(1));                                //Natural: MOVE #PAR-PARAMETER-DATA ( 1 ) TO #COMPRESSED-NME ( 1 )
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue(1).setValue(DbsUtil.compress(pnd_Ac_Pda_Pnd_Prefix_Nme.getValue(1), " ", pnd_Ac_Pda_Pnd_Compressed_Nme.getValue(1))); //Natural: COMPRESS #PREFIX-NME ( 1 ) ' ' #COMPRESSED-NME ( 1 ) INTO DX-INST-ADDRESS-TABLE ( 1 ) LEAVING
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue(2).setValue(pnd_Ac_Pda_Pnd_Title_Nme.getValue(1));                                                //Natural: ASSIGN DX-INST-ADDRESS-TABLE ( 2 ) := #TITLE-NME ( 1 )
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue(3).setValue(pnd_Ac_Pda_Pnd_Org_Nme.getValue(1));                                                  //Natural: ASSIGN DX-INST-ADDRESS-TABLE ( 3 ) := #ORG-NME ( 1 )
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue(4).setValue(pnd_Ac_Pda_Pnd_Address_Line_1.getValue(1));                                           //Natural: ASSIGN DX-INST-ADDRESS-TABLE ( 4 ) := #ADDRESS-LINE-1 ( 1 )
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue(5).setValue(pnd_Ac_Pda_Pnd_Address_Line_2.getValue(1));                                           //Natural: ASSIGN DX-INST-ADDRESS-TABLE ( 5 ) := #ADDRESS-LINE-2 ( 1 )
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue(6).setValue(pnd_Ac_Pda_Pnd_Address_Line_3.getValue(1));                                           //Natural: ASSIGN DX-INST-ADDRESS-TABLE ( 6 ) := #ADDRESS-LINE-3 ( 1 )
            DbsUtil.examine(new ExamineSource(ldaAppl160.getDoc_Ext_File_Dx_Inst_Address_Table().getValue("*")), new ExamineTranslate(TranslateOption.Upper));            //Natural: EXAMINE DX-INST-ADDRESS-TABLE ( * ) TRANSLATE INTO UPPER CASE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Inst_Zip_Cd().setValue("E");                                                                                                        //Natural: MOVE 'E' TO DX-INST-ZIP-CD
        if (condition(pnd_Ac_Pda_Pnd_Postal_Cde.getValue(1).notEquals(" ")))                                                                                              //Natural: IF #POSTAL-CDE ( 1 ) NE ' '
        {
            ldaAppl160.getDoc_Ext_File_Dx_Inst_Zip_Cd().setValue(pnd_Ac_Pda_Pnd_Postal_Cde.getValue(1));                                                                  //Natural: MOVE #POSTAL-CDE ( 1 ) TO DX-INST-ZIP-CD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Coll_Code_Pnd_Hold_Coll_Code_4.equals(pnd_Wk_Collb_Pnd_Wk_Coll) || pnd_Wk_Collb_Pnd_Wk_Coll.equals(" ")))                                  //Natural: IF #HOLD-COLL-CODE-4 = #WK-COLL OR #WK-COLL = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl170.getPnd_Work_Fields_Pnd_Idf_Sk_Inst_Coll_Cd().setValue(pnd_Wk_Collb_Pnd_Wk_Coll);                                                                   //Natural: ASSIGN #IDF-SK-INST-COLL-CD = #WK-COLL
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-MAIL-PULL-CODE
        sub_Determine_Mail_Pull_Code();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaAppl160.getDoc_Ext_File_Dx_Released_Dt().setValue(annty_Actvty_Prap_View_Ap_Dt_Released);                                                                      //Natural: ASSIGN DX-RELEASED-DT := AP-DT-RELEASED
        //*  FOR CONTRACT SPLIT PROCESSING, THE EFFECTIVE DATE IS STORED IN THE
        //*  AP-PRINT-DATE FIELD.              SGRD4 KG
        if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("L")))                                                                                                  //Natural: IF AP-APP-SOURCE = 'L'
        {
            ldaAppl160.getDoc_Ext_File_Dx_Effective_Date().setValueEdited(annty_Actvty_Prap_View_Ap_Print_Date,new ReportEditMask("YYYYMMDD"));                           //Natural: MOVE EDITED AP-PRINT-DATE ( EM = YYYYMMDD ) TO DX-EFFECTIVE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* * CA-082009 : REMOVE FROM THIS LOCATION THE BLOCK OF CODES THAT
        //* * UPDATES THE REPRINT-FILE AND MOVE IT BELOW -- AFTER THE VALUE OF
        //* * #EXTRACT-RECORD IS FINALLY DETERMINED
        //*  REWROTE THE BLOCK FOR 457(B) PRIVATE (S4) AND GA (S9) CONTRACT PKG
        //*  SUPPRESSION TO CHECK PRODUCT RULE AND PRODUCT EXCEPTION TBLS - ROCH KG
        //*  SUPPRESS 457(B) PRIVATE KG
        //*  DCA KG - SUPPRESS GA5
        //*  SUPPRESS 457(B) SP457B
        //*  SUPPRESS 457(B) SP457B
        if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")) || (annty_Actvty_Prap_View_Ap_Lob.equals("S")         //Natural: IF ( AP-LOB = 'S' AND AP-LOB-TYPE = '4' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '9' ) OR ( AP-LOB = 'D' AND AP-LOB-TYPE = 'A' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '7' )
            && annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")) || (annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("A")) 
            || (annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("7"))))
        {
            //*  EASEENR
            pdaAppa7900.getAppa7900().reset();                                                                                                                            //Natural: RESET APPA7900
            pdaAppa7900.getAppa7900_Inp_Lob().setValue(annty_Actvty_Prap_View_Ap_Lob);                                                                                    //Natural: ASSIGN INP-LOB := AP-LOB
            pdaAppa7900.getAppa7900_Inp_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Lob_Type);                                                                          //Natural: ASSIGN INP-LOB-TYPE := AP-LOB-TYPE
            pdaAppa7900.getAppa7900_Inp_Rule_Category().setValue("PKG_SUPPRESS");                                                                                         //Natural: ASSIGN INP-RULE-CATEGORY := 'PKG_SUPPRESS'
            pdaAppa7900.getAppa7900_Inp_Plan_Nbr().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                      //Natural: ASSIGN INP-PLAN-NBR := AP-SGRD-PLAN-NO
            pdaAppa7900.getAppa7900_Inp_Vesting().setValue(annty_Actvty_Prap_View_Ap_Ownership);                                                                          //Natural: ASSIGN INP-VESTING := AP-OWNERSHIP
            pdaAppa7900.getAppa7900_Inp_Enrollment_Method().setValue(annty_Actvty_Prap_View_Ap_Applcnt_Req_Type);                                                         //Natural: ASSIGN INP-ENROLLMENT-METHOD := AP-APPLCNT-REQ-TYPE
            //*  SP457B
            //*  SP457B
            if (condition(annty_Actvty_Prap_View_Ap_Coll_St_Cd.equals(" ")))                                                                                              //Natural: IF AP-COLL-ST-CD = ' '
            {
                pnd_Wk_Inst_State.setValue(annty_Actvty_Prap_View_Ap_Orig_Issue_State);                                                                                   //Natural: ASSIGN #WK-INST-STATE := AP-ORIG-ISSUE-STATE
                //*  SP457B
                //*  SP457B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wk_Inst_State.setValue(annty_Actvty_Prap_View_Ap_Coll_St_Cd);                                                                                         //Natural: ASSIGN #WK-INST-STATE := AP-COLL-ST-CD
                //*  SP457B
            }                                                                                                                                                             //Natural: END-IF
            pnd_St.reset();                                                                                                                                               //Natural: RESET #ST
            //*  SP457B
            DbsUtil.examine(new ExamineSource(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue("*")), new ExamineSearch(pnd_Wk_Inst_State),                 //Natural: EXAMINE #STATE-CODE-N ( * ) FOR #WK-INST-STATE GIVING INDEX #ST
                new ExamineGivingIndex(pnd_St));
            //*  SP457B
            //*  SP457B
            if (condition(pnd_St.greater(getZero())))                                                                                                                     //Natural: IF #ST > 0
            {
                pnd_Wk_Inst_State.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_St));                                                       //Natural: ASSIGN #WK-INST-STATE := #STATE-CODE-A ( #ST )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  SP457B
                //*  SP457B
                getReports().write(0, "STATE CODE NOT FOUND IN STATE CODE TABLE:",pnd_Wk_Inst_State);                                                                     //Natural: WRITE 'STATE CODE NOT FOUND IN STATE CODE TABLE:' #WK-INST-STATE
                if (Global.isEscape()) return;
                //*  SP457B
            }                                                                                                                                                             //Natural: END-IF
            //*  SP457B
            //*  SP457B
            pdaAppa7900.getAppa7900_Inp_Ircis().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde, pnd_Wk_Inst_State));    //Natural: COMPRESS AP-IRC-SECTN-CDE #WK-INST-STATE INTO INP-IRCIS LEAVING NO SPACE
            if (condition(pnd_Release_Ind.equals(3)))                                                                                                                     //Natural: IF #RELEASE-IND = 3
            {
                pdaAppa7900.getAppa7900_Inp_Pkg_Type().setValue("W");                                                                                                     //Natural: ASSIGN INP-PKG-TYPE := 'W'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAppa7900.getAppa7900_Inp_Pkg_Type().setValue("L");                                                                                                     //Natural: ASSIGN INP-PKG-TYPE := 'L'
                //*  AP-DT-ENT-SYS CONV TO YYYYMMDD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date_Diff.setValue(annty_Actvty_Prap_View_Ap_Dt_Ent_Sys);                                                                                                 //Natural: ASSIGN #DATE-DIFF := AP-DT-ENT-SYS
            pnd_Date_Yyyymmdd_Pnd_Date_Cc_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Cc);                                                                                     //Natural: ASSIGN #DATE-CC-1 := #DATE-DIFF-CC
            pnd_Date_Yyyymmdd_Pnd_Date_Yy_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Yy);                                                                                     //Natural: ASSIGN #DATE-YY-1 := #DATE-DIFF-YY
            pnd_Date_Yyyymmdd_Pnd_Date_Mm_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Mm);                                                                                     //Natural: ASSIGN #DATE-MM-1 := #DATE-DIFF-MM
            pnd_Date_Yyyymmdd_Pnd_Date_Dd_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Dd);                                                                                     //Natural: ASSIGN #DATE-DD-1 := #DATE-DIFF-DD
            pdaAppa7900.getAppa7900_Inp_Date().setValue(pnd_Date_Yyyymmdd);                                                                                               //Natural: ASSIGN INP-DATE := #DATE-YYYYMMDD
            DbsUtil.callnat(Appn7900.class , getCurrentProcessState(), pdaAppa7900.getAppa7900());                                                                        //Natural: CALLNAT 'APPN7900' APPA7900
            if (condition(Global.isEscape())) return;
            if (condition(pdaAppa7900.getAppa7900_Ret_Suppress_Ind().equals("Y")))                                                                                        //Natural: IF RET-SUPPRESS-IND = 'Y'
            {
                pnd_Extract_Record.setValue(false);                                                                                                                       //Natural: ASSIGN #EXTRACT-RECORD := FALSE
                //*  ELSE                                               /* ILLWELL
                //*    #EXTRACT-RECORD := TRUE                          /* ILLWELL
                //*  ROCH KG END
            }                                                                                                                                                             //Natural: END-IF
            //*  457(B) KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR OIA, A PLAN TYPE IS REQUIRED.
        //*  VALID PLAN TYPES:
        //*  '1' = CORE
        //*  '2' = CHOICE
        //*  '3' = SELECT
        //*  '10' = TIAA ONLY
        //*  '11' = CREF ONLY
        //*  KG TNGSUB - REMOVED LEGACY ASSIGNMENT OF THE OIA INDICATOR AND
        //*  THE SETTING OF THE SPECIAL HIERARCHY INDICATOR FROM ACIA8550.
        //*  SGRD KG
        if (condition(pdaScia8100.getDoc_Rec_Doc_Inst_Oia_Is_Ind().equals("Y")))                                                                                          //Natural: IF DOC-INST-OIA-IS-IND = 'Y'
        {
            //*  SGRD5A KG
            if (condition(pdaScia8100.getDoc_Rec_Doc_Inst_Oia_Is_Type().equals("10") || pdaScia8100.getDoc_Rec_Doc_Inst_Oia_Is_Type().equals("11")))                      //Natural: IF DOC-INST-OIA-IS-TYPE = '10' OR = '11'
            {
                //*  SGRD5A KG
                ldaAppl160.getDoc_Ext_File_Dx_Oia_Ind().setValue("3");                                                                                                    //Natural: MOVE '3' TO DX-OIA-IND
                //*  SGRD5A KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  SGRD KG
                pnd_Inst_Oia_Is_Type.setValue(pdaScia8100.getDoc_Rec_Doc_Inst_Oia_Is_Type());                                                                             //Natural: MOVE DOC-INST-OIA-IS-TYPE TO #INST-OIA-IS-TYPE
                //*  SGRD KG
                ldaAppl160.getDoc_Ext_File_Dx_Oia_Ind().setValue(pnd_Inst_Oia_Is_Type_Pnd_Inst_Oia_Is_Type_2);                                                            //Natural: MOVE #INST-OIA-IS-TYPE-2 TO DX-OIA-IND
                //*  SGRD5A KG
            }                                                                                                                                                             //Natural: END-IF
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SGRD KG
            ldaAppl160.getDoc_Ext_File_Dx_Oia_Ind().setValue(pdaScia8100.getDoc_Rec_Doc_Inst_Oia_Is_Ind());                                                               //Natural: MOVE DOC-INST-OIA-IS-IND TO DX-OIA-IND
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Spec_Ppg_Ind().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO DX-SPEC-PPG-IND
        //*  TNGSUB2 LS
        ldaAppl160.getDoc_Ext_File_Dx_Product_Cde().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO DX-PRODUCT-CDE
        short decideConditionsMet3814 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-LOB = 'D' AND AP-LOB-TYPE = '2'
        if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("2")))
        {
            decideConditionsMet3814++;
            ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type().setValue("Retirement Annuities ");                                                                        //Natural: MOVE 'Retirement Annuities ' TO DX-ACCT-SUM-SHEET-TYPE
        }                                                                                                                                                                 //Natural: WHEN AP-LOB = 'D' AND AP-LOB-TYPE = '7'
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("7")))
        {
            decideConditionsMet3814++;
            ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type().setValue("Group Retirement Annuities ");                                                                  //Natural: MOVE 'Group Retirement Annuities ' TO DX-ACCT-SUM-SHEET-TYPE
        }                                                                                                                                                                 //Natural: WHEN AP-LOB = 'S' AND AP-LOB-TYPE = '2'
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("2")))
        {
            decideConditionsMet3814++;
            ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type().setValue("Supplemental Retirement Annuities ");                                                           //Natural: MOVE 'Supplemental Retirement Annuities ' TO DX-ACCT-SUM-SHEET-TYPE
        }                                                                                                                                                                 //Natural: WHEN AP-LOB = 'S' AND AP-LOB-TYPE = '3'
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("3")))
        {
            decideConditionsMet3814++;
            ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type().setValue("Group Supplemental Retirement Annuities ");                                                     //Natural: MOVE 'Group Supplemental Retirement Annuities ' TO DX-ACCT-SUM-SHEET-TYPE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type().reset();                                                                                                  //Natural: RESET DX-ACCT-SUM-SHEET-TYPE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  K.GATES AND B.ELLO - NEW PROSPECTUS CHANGES 01/09/03
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type().greater(" ")))                                                                                  //Natural: IF DX-ACCT-SUM-SHEET-TYPE > ' '
        {
            if (condition(ldaAppl160.getDoc_Ext_File_Dx_Oia_Ind().equals("1") || ldaAppl160.getDoc_Ext_File_Dx_Oia_Ind().equals("2") || ldaAppl160.getDoc_Ext_File_Dx_Oia_Ind().equals("3"))) //Natural: IF DX-OIA-IND = '1' OR = '2' OR = '3'
            {
                ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type().setValue(DbsUtil.compress(ldaAppl160.getDoc_Ext_File_Dx_Acct_Sum_Sheet_Type(), "and Mutual Funds"));  //Natural: COMPRESS DX-ACCT-SUM-SHEET-TYPE 'and Mutual Funds' INTO DX-ACCT-SUM-SHEET-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  K.G. 09/20 INCLUDE SYSTEM DATE IN THE WELCOME PACKAGE
        ldaAppl160.getDoc_Ext_File_Dx_Mail_Date().setValue(pnd_Mail_Date);                                                                                                //Natural: MOVE #MAIL-DATE TO DX-MAIL-DATE
        //*  CHECK IF PPG IS OIA OR NOT  - OIA KG ENDS HERE.
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Sg_Plan_No().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                      //Natural: MOVE AP-SGRD-PLAN-NO TO DX-SG-PLAN-NO
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_No().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                                //Natural: MOVE AP-SGRD-SUBPLAN-NO TO DX-SG-SUBPLAN-NO
        //*  SGRD KG
        //*  SGRD KG
        ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_Type().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3));                                            //Natural: MOVE SUBSTR ( AP-SGRD-SUBPLAN-NO,1,3 ) TO DX-SG-SUBPLAN-TYPE
        //* (NYLEP)
        //* (NYLEP)
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_No().getSubstring(1,2).equals("RL")))                                                                      //Natural: IF SUBSTR ( DX-SG-SUBPLAN-NO,1,2 ) = 'RL'
        {
            scia2301_Pnd_Rl_Contract_No.setValue(annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert);                                                                                //Natural: ASSIGN #RL-CONTRACT-NO := AP-RLC-CREF-CERT
            //* (NYLEP)
            DbsUtil.callnat(Scin2301.class , getCurrentProcessState(), scia2301);                                                                                         //Natural: CALLNAT 'SCIN2301' SCIA2301
            if (condition(Global.isEscape())) return;
            //* (NYLEP)
            //* (NYLEP)
            if (condition(scia2301_Pnd_Return_Code.equals(getZero())))                                                                                                    //Natural: IF SCIA2301.#RETURN-CODE = 0
            {
                ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_Type().setValue(scia2301_Pnd_Product_Cde.getSubstring(1,3));                                                     //Natural: ASSIGN DX-SG-SUBPLAN-TYPE := SUBSTR ( #PRODUCT-CDE,1,3 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* (NYLEP)
                getReports().write(0, scia2301_Pnd_Return_Txt);                                                                                                           //Natural: WRITE SCIA2301.#RETURN-TXT
                if (Global.isEscape()) return;
                //* (NYLEP)
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  SRF BJD
        ldaAppl160.getDoc_Ext_File_Dx_Plan_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Plan_Issue_State);                                                            //Natural: MOVE AP-PLAN-ISSUE-STATE TO DX-PLAN-ISSUE-STATE
        //*  CALSTRS KG
        ldaAppl160.getDoc_Ext_File_Dx_Sgrd_Client_Id().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Client_Id);                                                                //Natural: MOVE AP-SGRD-CLIENT-ID TO DX-SGRD-CLIENT-ID
        //*  CALSTRS KG
        ldaAppl160.getDoc_Ext_File_Dx_Portfolio_Type().setValue(annty_Actvty_Prap_View_Ap_Portfolio_Type);                                                                //Natural: MOVE AP-PORTFOLIO-TYPE TO DX-PORTFOLIO-TYPE
        //* *************
        //*  BJD AE START
        //* *************
        pnd_Dflt_Amt.reset();                                                                                                                                             //Natural: RESET #DFLT-AMT #DFLT-PCT #INCR-PCT #MAX-PCT
        pnd_Dflt_Pct.reset();
        pnd_Incr_Pct.reset();
        pnd_Max_Pct.reset();
        ldaAppl160.getDoc_Ext_File_Dx_Auto_Enroll_Ind().setValue(annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind);                                                              //Natural: MOVE AP-INCMPL-ACCT-IND TO DX-AUTO-ENROLL-IND
        ldaAppl160.getDoc_Ext_File_Dx_Auto_Save_Ind().setValue(annty_Actvty_Prap_View_Ap_Autosave_Ind);                                                                   //Natural: MOVE AP-AUTOSAVE-IND TO DX-AUTO-SAVE-IND
        if (condition(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt.greater(getZero())))                                                                                      //Natural: IF AP-AS-CUR-DFLT-AMT > 0
        {
            if (condition(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt.equals("P")))                                                                                         //Natural: IF AP-AS-CUR-DFLT-OPT = 'P'
            {
                pnd_Dflt_Pct.setValueEdited(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt,new ReportEditMask("ZZ9.99"));                                                      //Natural: MOVE EDITED AP-AS-CUR-DFLT-AMT ( EM = ZZ9.99 ) TO #DFLT-PCT
                ldaAppl160.getDoc_Ext_File_Dx_As_Cur_Dflt_Amt().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dflt_Pct_Pnd_Dflt_Pct_N,                     //Natural: COMPRESS #DFLT-PCT-N '%' INTO DX-AS-CUR-DFLT-AMT LEAVING NO SPACE
                    "%"));
                ldaAppl160.getDoc_Ext_File_Dx_As_Cur_Dflt_Amt().setValue(ldaAppl160.getDoc_Ext_File_Dx_As_Cur_Dflt_Amt(), MoveOption.RightJustified);                     //Natural: MOVE RIGHT JUSTIFIED DX-AS-CUR-DFLT-AMT TO DX-AS-CUR-DFLT-AMT
                //*  DO WE NEED TO DO ELSE ??? DATA REPAIR TO RESET FOR IRA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    MOVE EDITED AP-AS-CUR-DFLT-AMT (EM=ZZ,ZZ9.99) TO #DFLT-AMT /*INITP
                //* INITP
                pnd_Dflt_Amt.setValueEdited(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt,new ReportEditMask("ZZZZZ9.99"));                                                   //Natural: MOVE EDITED AP-AS-CUR-DFLT-AMT ( EM = ZZZZZ9.99 ) TO #DFLT-AMT
                ldaAppl160.getDoc_Ext_File_Dx_As_Cur_Dflt_Amt().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "$", pnd_Dflt_Amt_Pnd_Dflt_Amt_N));              //Natural: COMPRESS '$' #DFLT-AMT-N INTO DX-AS-CUR-DFLT-AMT LEAVING NO SPACE
                ldaAppl160.getDoc_Ext_File_Dx_As_Cur_Dflt_Amt().setValue(ldaAppl160.getDoc_Ext_File_Dx_As_Cur_Dflt_Amt(), MoveOption.RightJustified);                     //Natural: MOVE RIGHT JUSTIFIED DX-AS-CUR-DFLT-AMT TO DX-AS-CUR-DFLT-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_As_Cur_Dflt_Amt().setValue("      0.00");                                                                                       //Natural: MOVE '      0.00' TO DX-AS-CUR-DFLT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                 /* START ONEIRA2 >>>
        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Init_Prem().reset();                                                                                                           //Natural: RESET DX-TIAA-INIT-PREM DX-CREF-INIT-PREM
        ldaAppl160.getDoc_Ext_File_Dx_Cref_Init_Prem().reset();
        if (condition(pnd_Input_Parm.equals("LEGAL")))                                                                                                                    //Natural: IF #INPUT-PARM = 'LEGAL'
        {
            if (condition(annty_Actvty_Prap_View_Ap_Financial_2.greater(getZero())))                                                                                      //Natural: IF AP-FINANCIAL-2 > 0
            {
                ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Init_Prem().setValue(annty_Actvty_Prap_View_Ap_Financial_2);                                                           //Natural: MOVE AP-FINANCIAL-2 TO DX-TIAA-INIT-PREM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Init_Prem().setValue(0);                                                                                               //Natural: MOVE 0.00 TO DX-TIAA-INIT-PREM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Financial_3.greater(getZero())))                                                                                      //Natural: IF AP-FINANCIAL-3 > 0
            {
                ldaAppl160.getDoc_Ext_File_Dx_Cref_Init_Prem().setValue(annty_Actvty_Prap_View_Ap_Financial_3);                                                           //Natural: MOVE AP-FINANCIAL-3 TO DX-CREF-INIT-PREM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl160.getDoc_Ext_File_Dx_Cref_Init_Prem().setValue(0);                                                                                               //Natural: MOVE 0.00 TO DX-CREF-INIT-PREM
            }                                                                                                                                                             //Natural: END-IF
            //*  END  ONEIRA2  <<<
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,NEWLINE," INITIAL PREMIUM TESTING ","=",pnd_Input_Parm,NEWLINE,"=",ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Init_Prem(),                  //Natural: WRITE // ' INITIAL PREMIUM TESTING ' '=' #INPUT-PARM / '=' DX-TIAA-INIT-PREM / '=' DX-CREF-INIT-PREM
            NEWLINE,"=",ldaAppl160.getDoc_Ext_File_Dx_Cref_Init_Prem());
        if (Global.isEscape()) return;
        //*  AP-AS-INCR-AMT FIELD SIZE ON DATA BASE ALLOWS FOR PHASE 2 - AMT OR PCT
        //*  BASED ON VALUE OF AP-AS-INCR-OPT.  JUST SENDING PCT FOR NOW.
        if (condition(annty_Actvty_Prap_View_Ap_As_Incr_Amt.greater(getZero())))                                                                                          //Natural: IF AP-AS-INCR-AMT > 0
        {
            pnd_Incr_Pct.setValueEdited(annty_Actvty_Prap_View_Ap_As_Incr_Amt,new ReportEditMask("ZZ9.99"));                                                              //Natural: MOVE EDITED AP-AS-INCR-AMT ( EM = ZZ9.99 ) TO #INCR-PCT
            ldaAppl160.getDoc_Ext_File_Dx_As_Incr_Amt().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Incr_Pct_Pnd_Incr_Pct_N, "%"));                      //Natural: COMPRESS #INCR-PCT-N '%' INTO DX-AS-INCR-AMT LEAVING NO SPACE
            ldaAppl160.getDoc_Ext_File_Dx_As_Incr_Amt().setValue(ldaAppl160.getDoc_Ext_File_Dx_As_Incr_Amt(), MoveOption.RightJustified);                                 //Natural: MOVE RIGHT JUSTIFIED DX-AS-INCR-AMT TO DX-AS-INCR-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_As_Incr_Amt().setValue("   0.00");                                                                                              //Natural: MOVE '   0.00' TO DX-AS-INCR-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_As_Max_Pct.greater(getZero())))                                                                                           //Natural: IF AP-AS-MAX-PCT > 0
        {
            pnd_Max_Pct.setValueEdited(annty_Actvty_Prap_View_Ap_As_Max_Pct,new ReportEditMask("ZZ9.99"));                                                                //Natural: MOVE EDITED AP-AS-MAX-PCT ( EM = ZZ9.99 ) TO #MAX-PCT
            ldaAppl160.getDoc_Ext_File_Dx_As_Max_Pct().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Max_Pct_Pnd_Max_Pct_N, "%"));                         //Natural: COMPRESS #MAX-PCT-N '%' INTO DX-AS-MAX-PCT LEAVING NO SPACE
            ldaAppl160.getDoc_Ext_File_Dx_As_Max_Pct().setValue(ldaAppl160.getDoc_Ext_File_Dx_As_Max_Pct(), MoveOption.RightJustified);                                   //Natural: MOVE RIGHT JUSTIFIED DX-AS-MAX-PCT TO DX-AS-MAX-PCT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_As_Max_Pct().setValue("   0.00");                                                                                               //Natural: MOVE '   0.00' TO DX-AS-MAX-PCT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Ae_Opt_Out_Days().setValueEdited(annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days,new ReportEditMask("Z9"));                               //Natural: MOVE EDITED AP-AE-OPT-OUT-DAYS ( EM = Z9 ) TO DX-AE-OPT-OUT-DAYS
        //* ***********
        //*  BJD AE END
        //* ***********
        //*  TSV KG
        ldaAppl160.getDoc_Ext_File_Dx_Tsv_Ind().setValue(annty_Actvty_Prap_View_Ap_Tsv_Ind);                                                                              //Natural: MOVE AP-TSV-IND TO DX-TSV-IND
        //*  BE2.
        ldaAppl160.getDoc_Ext_File_Dx_Agent_Crd_No().setValue(annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id);                                                                //Natural: MOVE AP-AGENT-OR-RACF-ID TO DX-AGENT-CRD-NO
        //*  TIGR
        if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("I") && ((annty_Actvty_Prap_View_Ap_Lob_Type.equals("7") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("8"))   //Natural: IF AP-LOB = 'I' AND ( AP-LOB-TYPE = '7' OR = '8' OR = '9' )
            || annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))))
        {
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Indx_Guarntd_Rte().setValueEdited(annty_Actvty_Prap_View_Ap_Financial_1,new ReportEditMask("ZZ9.99"));                     //Natural: MOVE EDITED AP-FINANCIAL-1 ( EM = ZZ9.99 ) TO DX-TIAA-INDX-GUARNTD-RTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Indx_Guarntd_Rte().setValue("  0.00");                                                                                     //Natural: MOVE '  0.00' TO DX-TIAA-INDX-GUARNTD-RTE
            //*  MTSIN
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Non_Proprietary_Pkg_Ind().setValue(annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind);                                              //Natural: ASSIGN DX-NON-PROPRIETARY-PKG-IND := AP-NON-PROPRIETARY-PKG-IND
        //*  MOVE-EXTRACT-DATA
    }
    //*  BWN
    private void sub_Get_Alpha_State_Code() throws Exception                                                                                                              //Natural: GET-ALPHA-STATE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_W_Check_State.setValue(ldaAppl170.getPnd_Work_Fields_Pnd_Prap_State_Cd());                                                                                    //Natural: ASSIGN #W-CHECK-STATE := #PRAP-STATE-CD
        //*  BWN
        //*  BWN
                                                                                                                                                                          //Natural: PERFORM STATE-CONVERSIONS
        sub_State_Conversions();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaAppl170.getPnd_Work_Fields_Pnd_Prap_State_Cd().setValue(pnd_W_Check_State);                                                                                    //Natural: ASSIGN #PRAP-STATE-CD := #W-CHECK-STATE
        //* *--------------------------
        ldaAppl170.getPnd_Work_Fields_Pnd_Alpha_State_Cd().resetInitial();                                                                                                //Natural: RESET INITIAL #ALPHA-STATE-CD
        FOR05:                                                                                                                                                            //Natural: FOR #T 1 62
        for (ldaAppl170.getPnd_Work_Fields_Pnd_T().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_T().lessOrEqual(62)); ldaAppl170.getPnd_Work_Fields_Pnd_T().nadd(1))
        {
            if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_Prap_State_Cd().equals(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T())))) //Natural: IF #PRAP-STATE-CD = #STATE-CODE-N ( #T )
            {
                ldaAppl170.getPnd_Work_Fields_Pnd_Alpha_State_Cd().setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T())); //Natural: MOVE #STATE-CODE-A ( #T ) TO #ALPHA-STATE-CD
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Contract_Type() throws Exception                                                                                                           //Natural: DETERMINE-CONTRACT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        //*  KG TNGSUB - COMBINED ALL PRODUCT TYPE VALIDATIONS INTO THIS ROUTINE
        //*              (RC-OR-RCP, INDIV-PRODUCT, GROUP-PRODUCT)
        //*            - REMOVED TNT PRODUCTS AND REFERENCES AS OBSOLETE
        //*            - ADDED GA AND TGA PRODUCTS
        //*  KG TNGSUB
        //*  KG TNGSUB
        //*  KG TNGSUB
        ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().resetInitial();                                                                                                 //Natural: RESET INITIAL #CONTRACT-TYPE
        pnd_Rc_Or_Rcp.setValue(false);                                                                                                                                    //Natural: ASSIGN #RC-OR-RCP := FALSE
        pnd_Indiv_Product.setValue(false);                                                                                                                                //Natural: ASSIGN #INDIV-PRODUCT := FALSE
        pnd_Group_Product.setValue(false);                                                                                                                                //Natural: ASSIGN #GROUP-PRODUCT := FALSE
        short decideConditionsMet3982 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB = 'D'
        if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("D")))
        {
            decideConditionsMet3982++;
            short decideConditionsMet3984 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '2' OR = '8'
            if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("2") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")))
            {
                decideConditionsMet3984++;
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RA");                                                                                         //Natural: MOVE 'RA' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '7' OR = '9'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("7") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))
            {
                decideConditionsMet3984++;
                //*  KG TNGSUB
                //*  SGRD4 KG
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("GRA");                                                                                        //Natural: MOVE 'GRA' TO #CONTRACT-TYPE
                pnd_Group_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #GROUP-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '6'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")))
            {
                decideConditionsMet3984++;
                //*  SGRD5 KG
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RL");                                                                                         //Natural: MOVE 'RL' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = 'A'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("A")))
            {
                decideConditionsMet3984++;
                //*  SGRD5 KG
                //*  KG TNGSUB
                //*  KG TNGSUB
                //*  BJD RHSP
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RC");                                                                                         //Natural: MOVE 'RC' TO #CONTRACT-TYPE
                pnd_Rc_Or_Rcp.setValue(true);                                                                                                                             //Natural: ASSIGN #RC-OR-RCP := TRUE
                pnd_Group_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #GROUP-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = 'B'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("B")))
            {
                decideConditionsMet3984++;
                //*  BJD RHSP
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RHSP");                                                                                       //Natural: MOVE 'RHSP' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB = 'S'
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("S")))
        {
            decideConditionsMet3982++;
            short decideConditionsMet4012 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '2' OR = '8'
            if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("2") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")))
            {
                decideConditionsMet4012++;
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("SRA");                                                                                        //Natural: MOVE 'SRA' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '3'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("3")))
            {
                decideConditionsMet4012++;
                //*  KG TNGSUB
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("GSRA");                                                                                       //Natural: MOVE 'GSRA' TO #CONTRACT-TYPE
                pnd_Group_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #GROUP-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '4'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")))
            {
                decideConditionsMet4012++;
                //*  KG TNGSUB
                //*  KG TNGSUB
                //*  SGRD5 KG
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("GA");                                                                                         //Natural: MOVE 'GA' TO #CONTRACT-TYPE
                pnd_Group_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #GROUP-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '7'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("7")))
            {
                decideConditionsMet4012++;
                //*  SGRD5 KG
                //*  KG TNGSUB
                //*  KG TNGSUB
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("RCP");                                                                                        //Natural: MOVE 'RCP' TO #CONTRACT-TYPE
                pnd_Rc_Or_Rcp.setValue(true);                                                                                                                             //Natural: ASSIGN #RC-OR-RCP := TRUE
                pnd_Group_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #GROUP-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '9'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))
            {
                decideConditionsMet4012++;
                //*  KG TNGSUB
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("TGA");                                                                                        //Natural: MOVE 'TGA' TO #CONTRACT-TYPE
                pnd_Group_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #GROUP-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB = 'I'
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("I")))
        {
            decideConditionsMet3982++;
            short decideConditionsMet4045 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '1' OR = '2'
            if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("1") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("2")))
            {
                decideConditionsMet4045++;
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRA");                                                                                        //Natural: MOVE 'IRA' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '3'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("3")))
            {
                decideConditionsMet4045++;
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRAR");                                                                                       //Natural: MOVE 'IRAR' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '4'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")))
            {
                decideConditionsMet4045++;
                //*  KG TNGSUB
                //*  SGRD6 KG
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRAC");                                                                                       //Natural: MOVE 'IRAC' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '6'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")))
            {
                decideConditionsMet4045++;
                //*  SGRD6 KG
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRAS");                                                                                       //Natural: MOVE 'IRAS' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '5'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("5")))
            {
                decideConditionsMet4045++;
                //*  TIGR-START
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("KEOG");                                                                                       //Natural: MOVE 'KEOG' TO #CONTRACT-TYPE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '7'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("7")))
            {
                decideConditionsMet4045++;
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRIR");                                                                                       //Natural: MOVE 'IRIR' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '8'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("8")))
            {
                decideConditionsMet4045++;
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRIC");                                                                                       //Natural: MOVE 'IRIC' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN ANNTY-ACTVTY-PRAP-VIEW.AP-LOB-TYPE = '9'
            else if (condition(annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))
            {
                decideConditionsMet4045++;
                //*  TIGR-END
                //*  KG TNGSUB
                ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().setValue("IRIS");                                                                                       //Natural: MOVE 'IRIS' TO #CONTRACT-TYPE
                pnd_Indiv_Product.setValue(true);                                                                                                                         //Natural: ASSIGN #INDIV-PRODUCT := TRUE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Mail_Pull_Code() throws Exception                                                                                                          //Natural: DETERMINE-MAIL-PULL-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        pnd_Dx_Error_Found.reset();                                                                                                                                       //Natural: RESET #DX-ERROR-FOUND
        //*  IF  DX-PIN-NO = '0000000'                             /* PINE
        //*  PINE
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Pin_No().equals("000000000000")))                                                                                     //Natural: IF DX-PIN-NO = '000000000000'
        {
            pnd_Dx_Error_Found.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #DX-ERROR-FOUND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Inst_Name_76().equals("E") || ldaAppl160.getDoc_Ext_File_Dx_Eop_State_Name().equals("E") || ldaAppl160.getDoc_Ext_File_Dx_Inst_Permit_Trans().equals("E")  //Natural: IF DX-INST-NAME-76 = 'E' OR DX-EOP-STATE-NAME = 'E' OR DX-INST-PERMIT-TRANS = 'E' OR DX-PERM-GRA-TRANS = 'E' OR DX-INST-CASHABLE-RA = 'E' OR DX-INST-CASHABLE-GRA = 'E' OR DX-INST-FIXED-PER-OPT = 'E' OR DX-GSRA-LOAN-IND = 'E'
            || ldaAppl160.getDoc_Ext_File_Dx_Perm_Gra_Trans().equals("E") || ldaAppl160.getDoc_Ext_File_Dx_Inst_Cashable_Ra().equals("E") || ldaAppl160.getDoc_Ext_File_Dx_Inst_Cashable_Gra().equals("E") 
            || ldaAppl160.getDoc_Ext_File_Dx_Inst_Fixed_Per_Opt().equals("E") || ldaAppl160.getDoc_Ext_File_Dx_Gsra_Loan_Ind().equals("E")))
        {
            pnd_Dx_Error_Found.setValue("Y");                                                                                                                             //Natural: MOVE 'Y' TO #DX-ERROR-FOUND
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl160.getDoc_Ext_File_Dx_Mail_Pull_Cd().setValue(annty_Actvty_Prap_View_Ap_Ownership_Type);                                                                  //Natural: MOVE ANNTY-ACTVTY-PRAP-VIEW.AP-OWNERSHIP-TYPE TO DX-MAIL-PULL-CD
        //*  MAIL-PULL-CD
        if (condition(annty_Actvty_Prap_View_Ap_Ownership_Type.equals(" ")))                                                                                              //Natural: IF ANNTY-ACTVTY-PRAP-VIEW.AP-OWNERSHIP-TYPE = ' '
        {
            if (condition(pnd_Dx_Error_Found.equals("Y")))                                                                                                                //Natural: IF #DX-ERROR-FOUND = 'Y'
            {
                ldaAppl160.getDoc_Ext_File_Dx_Mail_Pull_Cd().setValue("E");                                                                                               //Natural: MOVE 'E' TO DX-MAIL-PULL-CD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(annty_Actvty_Prap_View_Ap_Ownership_Type.equals("A") || annty_Actvty_Prap_View_Ap_Ownership_Type.equals("I")))                                  //Natural: IF ANNTY-ACTVTY-PRAP-VIEW.AP-OWNERSHIP-TYPE = 'A' OR = 'I'
            {
                if (condition(pnd_Dx_Error_Found.equals("Y")))                                                                                                            //Natural: IF #DX-ERROR-FOUND = 'Y'
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Mail_Pull_Cd().setValue("B");                                                                                           //Natural: MOVE 'B' TO DX-MAIL-PULL-CD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  TITLE LEFT JUSTIFIED
    //*  KG TNGSUB - ADD LEG CNT
    private void sub_Write_Control_Report_Welcome() throws Exception                                                                                                      //Natural: WRITE-CONTROL-REPORT-WELCOME
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. DOCUMERGE WELC PKG EXTRACT TOTALS",new ColumnSpacing(16),Global.getDATU());              //Natural: WRITE ( 1 ) *PROGRAM 5X 'A.C.I.S. DOCUMERGE WELC PKG EXTRACT TOTALS' 16X *DATU
        if (Global.isEscape()) return;
        getReports().write(1, Global.getTIMX(),new ColumnSpacing(58),"PAGE  1 OF 1",NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);                                             //Natural: WRITE ( 1 ) *TIMX 58X 'PAGE  1 OF 1' /////
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"1.","TOTAL PRAP RECORDS READ               ",ldaAppl170.getPnd_Read_Count(),NEWLINE);                                //Natural: WRITE ( 1 ) 14X '1.' 'TOTAL PRAP RECORDS READ               ' #READ-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"2.","TOTAL EXTRACT RECORDS FOR WELC PKG    ",ldaAppl170.getPnd_Write_Count(),NEWLINE);                               //Natural: WRITE ( 1 ) 14X '2.' 'TOTAL EXTRACT RECORDS FOR WELC PKG    ' #WRITE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"3.","TOTAL LEGACY RECORDS READ             ",pnd_Legacy_Contract_Cnt,NEWLINE,NEWLINE,NEWLINE,NEWLINE,                //Natural: WRITE ( 1 ) 14X '3.' 'TOTAL LEGACY RECORDS READ             ' #LEGACY-CONTRACT-CNT //////////
            NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(16),"**    END    OF   REPORT   **");                                                                                     //Natural: WRITE ( 1 ) 16X '**    END    OF   REPORT   **'
        if (Global.isEscape()) return;
    }
    //*  TITLE LEFT JUSTIFIED
    //*  KG TNGSUB - ADD LEG CNT
    private void sub_Write_Control_Report_Legal() throws Exception                                                                                                        //Natural: WRITE-CONTROL-REPORT-LEGAL
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. DOCUMERGE LEGL PKG EXTRACT TOTALS",new ColumnSpacing(16),Global.getDATU());              //Natural: WRITE ( 1 ) *PROGRAM 5X 'A.C.I.S. DOCUMERGE LEGL PKG EXTRACT TOTALS' 16X *DATU
        if (Global.isEscape()) return;
        getReports().write(1, Global.getTIMX(),new ColumnSpacing(58),"PAGE  1 OF 1",NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);                                             //Natural: WRITE ( 1 ) *TIMX 58X 'PAGE  1 OF 1' /////
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"1.","TOTAL PRAP RECORDS READ FOR LEGL",ldaAppl170.getPnd_Read_Count(),NEWLINE);                                      //Natural: WRITE ( 1 ) 14X '1.' 'TOTAL PRAP RECORDS READ FOR LEGL' #READ-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"2.","TOTAL EXTRACT RECORDS FOR LEGL PKG ",ldaAppl170.getPnd_Write_Count(),NEWLINE);                                  //Natural: WRITE ( 1 ) 14X '2.' 'TOTAL EXTRACT RECORDS FOR LEGL PKG ' #WRITE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"3.","TOTAL LEGACY RECORDS READ             ",pnd_Legacy_Contract_Cnt,NEWLINE,NEWLINE,NEWLINE,NEWLINE,                //Natural: WRITE ( 1 ) 14X '3.' 'TOTAL LEGACY RECORDS READ             ' #LEGACY-CONTRACT-CNT //////////
            NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(16),"**    END    OF   REPORT   **");                                                                                     //Natural: WRITE ( 1 ) 16X '**    END    OF   REPORT   **'
        if (Global.isEscape()) return;
    }
    //*  ADDED KG TNGSUB
    private void sub_Determine_Part_Status() throws Exception                                                                                                             //Natural: DETERMINE-PART-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------------------------------
        //*  CONTRACT SSN WILL BE USED TO DETERMINE IF THE PARTICIPANT ALREADY
        //*  HAS CONTRACTS AT TIAA-CREF BY USING THE ACIS-REPRINT-FL. IF THERE ARE
        //*  NO CONTRACTS OR THE ONLY CONTRACT ON REPRINT IS THE CONTRACT BEING
        //*  EXTRACTED, PARTICIPANT IS CONSIDERED TO BE NEW.
        //* **********************************************************************
        pnd_Reprint_Ssn_Tiaa_No.reset();                                                                                                                                  //Natural: RESET #REPRINT-SSN-TIAA-NO #RP-SOC-SEC #RP-TIAA-CONTR
        pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Soc_Sec.reset();
        pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Tiaa_Contr.reset();
        ldaAppl160.getDoc_Ext_File_Dx_Participant_Status().setValue("N");                                                                                                 //Natural: ASSIGN DX-PARTICIPANT-STATUS := 'N'
        pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Soc_Sec.setValue(annty_Actvty_Prap_View_Ap_Soc_Sec);                                                                               //Natural: ASSIGN #RP-SOC-SEC := AP-SOC-SEC
        if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("N")))                                                                                                  //Natural: IF AP-APP-SOURCE = 'N'
        {
            ldaAppl160.getDoc_Ext_File_Dx_Participant_Status().setValue("E");                                                                                             //Natural: ASSIGN DX-PARTICIPANT-STATUS := 'E'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_reprint_Fl.startDatabaseRead                                                                                                                               //Natural: READ REPRINT-FL BY RP-SSN-TIAA-NO FROM #REPRINT-SSN-TIAA-NO
            (
            "READ02",
            new Wc[] { new Wc("RP_SSN_TIAA_NO", ">=", pnd_Reprint_Ssn_Tiaa_No, WcType.BY) },
            new Oc[] { new Oc("RP_SSN_TIAA_NO", "ASC") }
            );
            READ02:
            while (condition(vw_reprint_Fl.readNextRow("READ02")))
            {
                if (condition(reprint_Fl_Rp_Soc_Sec.notEquals(pnd_Reprint_Ssn_Tiaa_No_Pnd_Rp_Soc_Sec)))                                                                   //Natural: IF RP-SOC-SEC NE #RP-SOC-SEC
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(reprint_Fl_Rp_Tiaa_Contr.equals(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct)))                                                                //Natural: IF RP-TIAA-CONTR = AP-TIAA-CNTRCT
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAppl160.getDoc_Ext_File_Dx_Participant_Status().setValue("E");                                                                                 //Natural: ASSIGN DX-PARTICIPANT-STATUS := 'E'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Store_Reprint_Data() throws Exception                                                                                                                //Natural: STORE-REPRINT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        //*  SGRD5 KG
        ldaAppl180.getVw_acis_Reprint_Fl_View().reset();                                                                                                                  //Natural: RESET ACIS-REPRINT-FL-VIEW
        pnd_Reprint_Rec_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #REPRINT-REC-FOUND := FALSE
        //*  SGRD5 KG
        //*  SGRD5 KG
        ldaAppl180.getVw_acis_Reprint_Fl_View().startDatabaseFind                                                                                                         //Natural: FIND ACIS-REPRINT-FL-VIEW WITH RP-TIAA-CONTR = AP-TIAA-CNTRCT
        (
        "UPD2",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", annty_Actvty_Prap_View_Ap_Tiaa_Cntrct, WcType.WITH) }
        );
        UPD2:
        while (condition(ldaAppl180.getVw_acis_Reprint_Fl_View().readNextRow("UPD2")))
        {
            ldaAppl180.getVw_acis_Reprint_Fl_View().setIfNotFoundControlFlag(false);
            pnd_Reprint_Rec_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #REPRINT-REC-FOUND := TRUE
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  SGRD5 KG
        if (condition(pnd_Reprint_Rec_Found.getBoolean()))                                                                                                                //Natural: IF #REPRINT-REC-FOUND
        {
            //*  SGRD5 KG
            //*  SGRD5 KG
            UPD3:                                                                                                                                                         //Natural: GET ACIS-REPRINT-FL-VIEW *ISN ( UPD2. )
            ldaAppl180.getVw_acis_Reprint_Fl_View().readByID(ldaAppl180.getVw_acis_Reprint_Fl_View().getAstISN("UPD2"), "UPD3");
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Record_Type().setValue("1");                                                                                                //Natural: ASSIGN RP-RECORD-TYPE := '1'
        //*  SGRD6 KG
        //*  SGRD6 KG
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Contr().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct));              //Natural: COMPRESS AP-TIAA-CNTRCT INTO RP-TIAA-CONTR LEAVING NO SPACE
        //*  SGRD6 KG
        //*  SGRD6 KG
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Contr().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Cref_Cert));                //Natural: COMPRESS AP-CREF-CERT INTO RP-CREF-CONTR LEAVING NO SPACE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Negr_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Prfx().getValue(2),  //Natural: COMPRESS DX-TIAA-PRFX ( 2 ) DX-TIAA-NO ( 2 ) INTO RP-TIAA-NEGR-CONTRACT LEAVING NO SPACE
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_No().getValue(2)));
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Negr_Contract().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl160.getDoc_Ext_File_Dx_Cref_Prfx().getValue(2),  //Natural: COMPRESS DX-CREF-PRFX ( 2 ) DX-CREF-NO ( 2 ) INTO RP-CREF-NEGR-CONTRACT LEAVING NO SPACE
            ldaAppl160.getDoc_Ext_File_Dx_Cref_No().getValue(2)));
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob().setValue(annty_Actvty_Prap_View_Ap_Lob);                                                                              //Natural: ASSIGN RP-LOB := AP-LOB
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Lob_Type);                                                                    //Natural: ASSIGN RP-LOB-TYPE := AP-LOB-TYPE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Bill_Code().setValue(annty_Actvty_Prap_View_Ap_Bill_Code);                                                                  //Natural: ASSIGN RP-BILL-CODE := AP-BILL-CODE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Prfx_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme);                                                            //Natural: ASSIGN RP-COR-PRFX-NME := AP-COR-PRFX-NME
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Last_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Last_Nme);                                                            //Natural: ASSIGN RP-COR-LAST-NME := AP-COR-LAST-NME
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_First_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_First_Nme);                                                          //Natural: ASSIGN RP-COR-FIRST-NME := AP-COR-FIRST-NME
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Mddle_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme);                                                          //Natural: ASSIGN RP-COR-MDDLE-NME := AP-COR-MDDLE-NME
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cor_Sffx_Nme().setValue(annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme);                                                            //Natural: ASSIGN RP-COR-SFFX-NME := AP-COR-SFFX-NME
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(1).setValue(ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue(1));                             //Natural: MOVE DX-ADDRESS-TABLE ( 1 ) TO RP-ADDRESS-LINE ( 1 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(2).setValue(ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue(2));                             //Natural: MOVE DX-ADDRESS-TABLE ( 2 ) TO RP-ADDRESS-LINE ( 2 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(3).setValue(ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue(3));                             //Natural: MOVE DX-ADDRESS-TABLE ( 3 ) TO RP-ADDRESS-LINE ( 3 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(4).setValue(ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue(4));                             //Natural: MOVE DX-ADDRESS-TABLE ( 4 ) TO RP-ADDRESS-LINE ( 4 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Address_Line().getValue(5).setValue(ldaAppl160.getDoc_Ext_File_Dx_Address_Table().getValue(5));                             //Natural: MOVE DX-ADDRESS-TABLE ( 5 ) TO RP-ADDRESS-LINE ( 5 )
        //*  PINE
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Pin_No().greater(" ")))                                                                                               //Natural: IF DX-PIN-NO > ' '
        {
            pnd_Pin_Number_N_Pnd_Pin_Number.setValue(ldaAppl160.getDoc_Ext_File_Dx_Pin_No());                                                                             //Natural: ASSIGN #PIN-NUMBER := DX-PIN-NO
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Pin_Nbr().setValue(annty_Actvty_Prap_View_Ap_Pin_Nbr);                                                                  //Natural: ASSIGN RP-PIN-NBR := AP-PIN-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Pin_Nbr().setValue(0);                                                                                                  //Natural: ASSIGN RP-PIN-NBR := 0
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Orig_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Orig_Issue_State);                                                    //Natural: ASSIGN RP-ORIG-ISSUE-STATE := AP-ORIG-ISSUE-STATE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Current_State_Code().setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                                //Natural: ASSIGN RP-CURRENT-STATE-CODE := AP-CURRENT-STATE-CODE
        //*                          /* MAIL-RES FIX KG START
        //*  OMNI
        if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.equals("SGRD")))                                                                                                //Natural: IF AP-COLL-CODE = 'SGRD'
        {
            //*  KG TNGSUB
            if (condition(pnd_Indiv_Product.getBoolean()))                                                                                                                //Natural: IF #INDIV-PRODUCT
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St().setValue(annty_Actvty_Prap_View_Ap_Current_State_Code);                                       //Natural: ASSIGN RP-ORIG-RESID-ISSUE-ST := AP-CURRENT-STATE-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Orig_Resid_Issue_St().setValue(" ");                                                                                    //Natural: MOVE ' ' TO RP-ORIG-RESID-ISSUE-ST
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mail_Zip().setValue(annty_Actvty_Prap_View_Ap_Mail_Zip);                                                                    //Natural: ASSIGN RP-MAIL-ZIP := AP-MAIL-ZIP
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mail_Instructions().setValue(annty_Actvty_Prap_View_Ap_Mail_Instructions);                                                  //Natural: ASSIGN RP-MAIL-INSTRUCTIONS := AP-MAIL-INSTRUCTIONS
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Soc_Sec().setValue(annty_Actvty_Prap_View_Ap_Soc_Sec);                                                                      //Natural: ASSIGN RP-SOC-SEC := AP-SOC-SEC
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dob().setValue(annty_Actvty_Prap_View_Ap_Dob);                                                                              //Natural: ASSIGN RP-DOB := AP-DOB
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sex().setValue(annty_Actvty_Prap_View_Ap_Sex);                                                                              //Natural: ASSIGN RP-SEX := AP-SEX
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_Code().setValue(annty_Actvty_Prap_View_Ap_Coll_Code);                                                                  //Natural: ASSIGN RP-COLL-CODE := AP-COLL-CODE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_St_Cd().setValue(annty_Actvty_Prap_View_Ap_Coll_St_Cd);                                                                //Natural: ASSIGN RP-COLL-ST-CD := AP-COLL-ST-CD
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Irc_Sectn_Grp_Cde().setValue(annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde);                                                  //Natural: ASSIGN RP-IRC-SECTN-GRP-CDE := AP-IRC-SECTN-GRP-CDE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Irc_Sectn_Cde().setValue(annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde);                                                          //Natural: ASSIGN RP-IRC-SECTN-CDE := AP-IRC-SECTN-CDE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ppg_Team_Cde().setValue(annty_Actvty_Prap_View_Ap_Ppg_Team_Cde);                                                            //Natural: ASSIGN RP-PPG-TEAM-CDE := AP-PPG-TEAM-CDE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Inst_Link_Cde().setValue(annty_Actvty_Prap_View_Ap_Inst_Link_Cde);                                                          //Natural: ASSIGN RP-INST-LINK-CDE := AP-INST-LINK-CDE
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1).greater("0")))                                                                               //Natural: IF DX-DATE-TABLE ( 1 ) > '0'
        {
            //*  LPAO KG
            //*  LPAO KG
            //*  LPAO KG
            if (condition(annty_Actvty_Prap_View_Ap_Legal_Ann_Option.equals("Y") && annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt.notEquals(getZero())))                    //Natural: IF AP-LEGAL-ANN-OPTION = 'Y' AND AP-TIAA-ANN-ISSUE-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi().setValue(pnd_Hold_Orig_Tiaa_Doi);                                                                           //Natural: ASSIGN RP-T-DOI := #HOLD-ORIG-TIAA-DOI
                //*  LPAO KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Iss_Date.setValue(ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(1));                                                                       //Natural: ASSIGN #TIAA-ISS-DATE := DX-DATE-TABLE ( 1 )
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi().setValue(pnd_Tiaa_Iss_Date_Pnd_Tiaa_Iss_Date_N);                                                            //Natural: ASSIGN RP-T-DOI := #TIAA-ISS-DATE-N
                //*  LPAO KG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SGRD6 KG
            if (condition(pnd_Icap_Tnt_Companion.equals("Y")))                                                                                                            //Natural: IF #ICAP-TNT-COMPANION = 'Y'
            {
                //*  KG TNGSUB
                if (condition(ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind().equals("Y")))                                                                              //Natural: IF DX-SINGLE-ISSUE-IND = 'Y'
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Doi().setValue(0);                                                                                            //Natural: ASSIGN RP-T-DOI := 0
                    //*  SGRD6 KG
                }                                                                                                                                                         //Natural: END-IF
                //*  SGRD6 KG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2).greater("0")))                                                                               //Natural: IF DX-DATE-TABLE ( 2 ) > '0'
        {
            //*  LPAO KG
            //*  LPAO KG
            //*  LPAO KG
            if (condition(annty_Actvty_Prap_View_Ap_Legal_Ann_Option.equals("Y") && annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt.notEquals(getZero())))                    //Natural: IF AP-LEGAL-ANN-OPTION = 'Y' AND AP-CREF-ANN-ISSUE-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_C_Doi().setValue(pnd_Hold_Orig_Cref_Doi);                                                                           //Natural: ASSIGN RP-C-DOI := #HOLD-ORIG-CREF-DOI
                //*  LPAO KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Iss_Date.setValue(ldaAppl160.getDoc_Ext_File_Dx_Date_Table().getValue(2));                                                                       //Natural: ASSIGN #CREF-ISS-DATE := DX-DATE-TABLE ( 2 )
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_C_Doi().setValue(pnd_Cref_Iss_Date_Pnd_Cref_Iss_Date_N);                                                            //Natural: ASSIGN RP-C-DOI := #CREF-ISS-DATE-N
                //*  LPAO KG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SGRD6 KG
            if (condition(pnd_Icap_Tnt_Companion.equals("Y")))                                                                                                            //Natural: IF #ICAP-TNT-COMPANION = 'Y'
            {
                //*  KG TNGSUB
                if (condition(ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind().equals("Y")))                                                                              //Natural: IF DX-SINGLE-ISSUE-IND = 'Y'
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_C_Doi().setValue(0);                                                                                            //Natural: ASSIGN RP-C-DOI := 0
                    //*  SGRD6 KG
                }                                                                                                                                                         //Natural: END-IF
                //*  SGRD6 KG
            }                                                                                                                                                             //Natural: END-IF
            //*  EAC (RODGER)
            //*  TIAA  INIT PREM ONEIRA2
            //*  CREF  INIT PREM ONEIRA2
            //*  TOTAL INIT PREM ONEIRA2
            //*  BIP
            //*  BIP
            //*  BIP
            //*  ONEIRA
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Annuity_Start_Date.setValue(annty_Actvty_Prap_View_Ap_Annuity_Start_Date);                                                                                    //Natural: ASSIGN #ANNUITY-START-DATE := AP-ANNUITY-START-DATE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Annuity_Start_Date().setValue(pnd_Annuity_Start_Date_Pnd_Annuity_Start_Date_N);                                             //Natural: ASSIGN RP-ANNUITY-START-DATE := #ANNUITY-START-DATE-N
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_Released().setValue(annty_Actvty_Prap_View_Ap_Dt_Released);                                                              //Natural: ASSIGN RP-DT-RELEASED := AP-DT-RELEASED
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Dt_App_Recvd().setValue(annty_Actvty_Prap_View_Ap_Dt_App_Recvd);                                                            //Natural: ASSIGN RP-DT-APP-RECVD := AP-DT-APP-RECVD
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Extract_Date().setValue(Global.getDATN());                                                                                  //Natural: ASSIGN RP-EXTRACT-DATE := *DATN
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Curr().setValue(annty_Actvty_Prap_View_Ap_Curr);                                                                            //Natural: ASSIGN RP-CURR := AP-CURR
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_T_Age_1st().setValue(annty_Actvty_Prap_View_Ap_T_Age_1st);                                                                  //Natural: ASSIGN RP-T-AGE-1ST := AP-T-AGE-1ST
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ownership().setValue(annty_Actvty_Prap_View_Ap_Ownership);                                                                  //Natural: ASSIGN RP-OWNERSHIP := AP-OWNERSHIP
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Alloc_Discr().setValue(annty_Actvty_Prap_View_Ap_Alloc_Discr);                                                              //Natural: ASSIGN RP-ALLOC-DISCR := AP-ALLOC-DISCR
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Fund_Cde().getValue("*").setValue(annty_Actvty_Prap_View_Ap_Fund_Cde.getValue("*"));                                        //Natural: ASSIGN RP-FUND-CDE ( * ) := AP-FUND-CDE ( * )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_App_Source().setValue(annty_Actvty_Prap_View_Ap_App_Source);                                                                //Natural: ASSIGN RP-APP-SOURCE := AP-APP-SOURCE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Region_Code().setValue(annty_Actvty_Prap_View_Ap_Region_Code);                                                              //Natural: ASSIGN RP-REGION-CODE := AP-REGION-CODE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Eop_Addl_Cref_Request().setValue(annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request);                                          //Natural: ASSIGN RP-EOP-ADDL-CREF-REQUEST := AP-EOP-ADDL-CREF-REQUEST
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Rlc_Cref_Cert().setValue(annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert);                                                          //Natural: ASSIGN RP-RLC-CREF-CERT := AP-RLC-CREF-CERT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ira_Rollover_Type().setValue(annty_Actvty_Prap_View_Ap_Ira_Rollover_Type);                                                  //Natural: ASSIGN RP-IRA-ROLLOVER-TYPE := AP-IRA-ROLLOVER-TYPE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ira_Record_Type().setValue(annty_Actvty_Prap_View_Ap_Ira_Record_Type);                                                      //Natural: ASSIGN RP-IRA-RECORD-TYPE := AP-IRA-RECORD-TYPE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Status().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Status);                                                      //Natural: ASSIGN RP-MULT-APP-STATUS := AP-MULT-APP-STATUS
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Lob().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Lob);                                                            //Natural: ASSIGN RP-MULT-APP-LOB := AP-MULT-APP-LOB
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type);                                                  //Natural: ASSIGN RP-MULT-APP-LOB-TYPE := AP-MULT-APP-LOB-TYPE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mult_App_Ppg().setValue(annty_Actvty_Prap_View_Ap_Mult_App_Ppg);                                                            //Natural: ASSIGN RP-MULT-APP-PPG := AP-MULT-APP-PPG
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_1().setValue(annty_Actvty_Prap_View_Ap_Financial_1);                                                              //Natural: ASSIGN RP-FINANCIAL-1 := AP-FINANCIAL-1
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_2().setValue(annty_Actvty_Prap_View_Ap_Financial_2);                                                              //Natural: ASSIGN RP-FINANCIAL-2 := AP-FINANCIAL-2
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_3().setValue(annty_Actvty_Prap_View_Ap_Financial_3);                                                              //Natural: ASSIGN RP-FINANCIAL-3 := AP-FINANCIAL-3
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_4().setValue(annty_Actvty_Prap_View_Ap_Financial_4);                                                              //Natural: ASSIGN RP-FINANCIAL-4 := AP-FINANCIAL-4
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Applcnt_Req_Type().setValue(annty_Actvty_Prap_View_Ap_Applcnt_Req_Type);                                                    //Natural: ASSIGN RP-APPLCNT-REQ-TYPE := AP-APPLCNT-REQ-TYPE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Service_Agent().setValue(annty_Actvty_Prap_View_Omni_Issue_Ind);                                                       //Natural: ASSIGN RP-TIAA-SERVICE-AGENT := OMNI-ISSUE-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Racf_Id().setValue(annty_Actvty_Prap_View_Ap_Racf_Id);                                                                      //Natural: ASSIGN RP-RACF-ID := AP-RACF-ID
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Model().setValue(annty_Actvty_Prap_View_Ap_Allocation_Model_Type);                                               //Natural: ASSIGN RP-ALLOCATION-MODEL := AP-ALLOCATION-MODEL-TYPE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Divorce_Ind().setValue(annty_Actvty_Prap_View_Ap_Divorce_Ind);                                                              //Natural: ASSIGN RP-DIVORCE-IND := AP-DIVORCE-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Email_Address);                                                          //Natural: ASSIGN RP-EMAIL-ADDRESS := AP-EMAIL-ADDRESS
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_E_Signed_Appl_Ind().setValue(annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind);                                                  //Natural: ASSIGN RP-E-SIGNED-APPL-IND := AP-E-SIGNED-APPL-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Eft_Request_Ind().setValue(annty_Actvty_Prap_View_Ap_Eft_Requested_Ind);                                                    //Natural: ASSIGN RP-EFT-REQUEST-IND := AP-EFT-REQUESTED-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Participant_Status_Ind().setValue(ldaAppl160.getDoc_Ext_File_Dx_Participant_Status());                                      //Natural: ASSIGN RP-PARTICIPANT-STATUS-IND := DX-PARTICIPANT-STATUS
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Decedent_Contract().setValue(annty_Actvty_Prap_View_Ap_Decedent_Contract);                                                  //Natural: ASSIGN RP-DECEDENT-CONTRACT := AP-DECEDENT-CONTRACT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Relation_To_Decedent().setValue(annty_Actvty_Prap_View_Ap_Relation_To_Decedent);                                            //Natural: ASSIGN RP-RELATION-TO-DECEDENT := AP-RELATION-TO-DECEDENT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ssn_Tin_Ind().setValue(annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind);                                                              //Natural: ASSIGN RP-SSN-TIN-IND := AP-SSN-TIN-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Oneira_Acct_No().setValue(annty_Actvty_Prap_View_Ap_Oneira_Acct_No);                                                        //Natural: ASSIGN RP-ONEIRA-ACCT-NO := AP-ONEIRA-ACCT-NO
        //*  ROCH KG - SET REPRINT PKG MAIL TYPE FOR WELCOME PACKAGE IF THE
        //*  457(B) PRIVATE AND GA5 CONTRACTS ARE NOT SUPPRESSED.
        //*  BIP
        //*  BIP
        //*  ROCH KG
        //*  ROCH KG
        short decideConditionsMet4343 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-DECEDENT-CONTRACT > ' '
        if (condition(annty_Actvty_Prap_View_Ap_Decedent_Contract.greater(" ")))
        {
            decideConditionsMet4343++;
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("P");                                                                                      //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'P'
        }                                                                                                                                                                 //Natural: WHEN ( AP-LOB = 'D' AND AP-LOB-TYPE = '6' )
        else if (condition(annty_Actvty_Prap_View_Ap_Lob.equals("D") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("6")))
        {
            decideConditionsMet4343++;
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("L");                                                                                      //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'L'
        }                                                                                                                                                                 //Natural: WHEN ( AP-LOB = 'S' AND AP-LOB-TYPE = '4' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '9' )
        else if (condition(((annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")) || (annty_Actvty_Prap_View_Ap_Lob.equals("S") 
            && annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))))
        {
            decideConditionsMet4343++;
            //*  ROCH KG
            //*  ROCH KG
            if (condition((pdaAppa7900.getAppa7900_Ret_Suppress_Ind().equals("N"))))                                                                                      //Natural: IF ( RET-SUPPRESS-IND = 'N' )
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("W");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'W'
                //*  ROCH KG
                //*  ROCH KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("P");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'P'
                //*  ROCH KG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN AP-OWNERSHIP = 3
        else if (condition(annty_Actvty_Prap_View_Ap_Ownership.equals(3)))
        {
            decideConditionsMet4343++;
            //*  INST  LS
            //*  KG TNGSUB  /* ILLWELL
            //*  SGRD5 KG
            if (condition((pnd_Welc_Force_Inst_Owned.getBoolean()) || (pnd_Rc_Or_Rcp.getBoolean())))                                                                      //Natural: IF ( #WELC-FORCE-INST-OWNED ) OR ( #RC-OR-RCP )
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("W");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'W'
                //*  SGRD5 KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("P");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'P'
                //*  SGRD5 KG
                //*  EMORY KG
                //*  EMORY KG
                //*  TNGSUB
                //*  TNGSUB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ( #WELC-FUND-TRIGGER AND AP-STATUS NE 'C' )
        else if (condition(pnd_Welc_Fund_Trigger.getBoolean() && annty_Actvty_Prap_View_Ap_Status.notEquals("C")))
        {
            decideConditionsMet4343++;
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("P");                                                                                      //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'P'
        }                                                                                                                                                                 //Natural: WHEN AP-SUBSTITUTION-CONTRACT-IND GT ' '
        else if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.greater(" ")))
        {
            decideConditionsMet4343++;
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("P");                                                                                      //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'P'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("W");                                                                                      //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'W'
            //*  SGRD8 KG
            //*  SGRD8 KG
            //*  OIA BE
            //*  OIA BE
            //*  IISG IISG1
            //*  OIA BE
            //*  OIA BE
            //*  IISG IISG1
            //*  IISG
            //*  IISG
            //*  SGRD KG
            //*  SGRD KG
            //*  SGRD KG
            //*  SGRD4 KG
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Single_Issue_Ind().setValue(ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind());                                              //Natural: ASSIGN RP-SINGLE-ISSUE-IND := DX-SINGLE-ISSUE-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Divsub().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Divsub);                                                              //Natural: ASSIGN RP-SGRD-DIVSUB := AP-SGRD-DIVSUB
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Fmt().setValue(annty_Actvty_Prap_View_Ap_Allocation_Fmt);                                                        //Natural: ASSIGN RP-ALLOCATION-FMT := AP-ALLOCATION-FMT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Phone_No().setValue(annty_Actvty_Prap_View_Ap_Phone_No);                                                                    //Natural: ASSIGN RP-PHONE-NO := AP-PHONE-NO
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Source_Cde_1().setValue(annty_Actvty_Prap_View_Ap_Fund_Source_Cde_1);                                                       //Natural: ASSIGN RP-SOURCE-CDE-1 := AP-FUND-SOURCE-CDE-1
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Fund_Cde().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde.getValue(1,":",100));                            //Natural: ASSIGN RP-FUND-CDE ( 1:100 ) := AP-FUND-CDE ( 1:100 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Pct().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Allocation_Pct.getValue(1,":",100));                //Natural: ASSIGN RP-ALLOCATION-PCT ( 1:100 ) := AP-ALLOCATION-PCT ( 1:100 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Source_Cde_2().setValue(annty_Actvty_Prap_View_Ap_Fund_Source_Cde_2);                                                       //Natural: ASSIGN RP-SOURCE-CDE-2 := AP-FUND-SOURCE-CDE-2
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Fund_Cde_2().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde_2.getValue(1,":",100));                        //Natural: ASSIGN RP-FUND-CDE-2 ( 1:100 ) := AP-FUND-CDE-2 ( 1:100 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Allocation_Pct_2().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Allocation_Pct_2.getValue(1,":",                  //Natural: ASSIGN RP-ALLOCATION-PCT-2 ( 1:100 ) := AP-ALLOCATION-PCT-2 ( 1:100 )
            100));
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Plan_No().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                            //Natural: ASSIGN RP-SGRD-PLAN-NO := AP-SGRD-PLAN-NO
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Subplan_No().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                      //Natural: ASSIGN RP-SGRD-SUBPLAN-NO := AP-SGRD-SUBPLAN-NO
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Part_Ext().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext);                                                          //Natural: ASSIGN RP-SGRD-PART-EXT := AP-SGRD-PART-EXT
        pnd_Hold_Effective_Dt_Pnd_Hold_Effective_Dt_A.setValue(ldaAppl160.getDoc_Ext_File_Dx_Effective_Date());                                                           //Natural: ASSIGN #HOLD-EFFECTIVE-DT-A := DX-EFFECTIVE-DATE
        //*  BE 040805
        //*  SGRD4 KG
        if (condition(pnd_Hold_Effective_Dt.greater(getZero())))                                                                                                          //Natural: IF #HOLD-EFFECTIVE-DT > 0
        {
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ls_Effective_Date().setValue(pnd_Hold_Effective_Dt);                                                                    //Natural: ASSIGN RP-LS-EFFECTIVE-DATE := #HOLD-EFFECTIVE-DT
            //*  BE 040805
            //*  SGRD4 KG
            //*  SRF BJD
            //*  CALSTRS KG
            //*  CALSTRS KG
            //*  BE2.
            //*  TSV KG
            //*  IMBS
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Interest_Rate().setValue(pdaScia8100.getDoc_Rec_Doc_Guar_Int_Rate_Out());                                                   //Natural: ASSIGN RP-INTEREST-RATE := DOC-GUAR-INT-RATE-OUT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Replacement_Ind().setValue(annty_Actvty_Prap_View_Ap_Replacement_Ind);                                                      //Natural: ASSIGN RP-REPLACEMENT-IND := AP-REPLACEMENT-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Register_Id().setValue(annty_Actvty_Prap_View_Ap_Register_Id);                                                              //Natural: ASSIGN RP-REGISTER-ID := AP-REGISTER-ID
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Exempt_Ind().setValue(annty_Actvty_Prap_View_Ap_Exempt_Ind);                                                                //Natural: ASSIGN RP-EXEMPT-IND := AP-EXEMPT-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Plan_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Plan_Issue_State);                                                    //Natural: ASSIGN RP-PLAN-ISSUE-STATE := AP-PLAN-ISSUE-STATE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Client_Id().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Client_Id);                                                        //Natural: ASSIGN RP-SGRD-CLIENT-ID := AP-SGRD-CLIENT-ID
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Portfolio_Type().setValue(annty_Actvty_Prap_View_Ap_Portfolio_Type);                                                        //Natural: ASSIGN RP-PORTFOLIO-TYPE := AP-PORTFOLIO-TYPE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Incmpl_Acct_Ind().setValue(annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind);                                                      //Natural: ASSIGN RP-INCMPL-ACCT-IND := AP-INCMPL-ACCT-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Autosave_Ind().setValue(annty_Actvty_Prap_View_Ap_Autosave_Ind);                                                            //Natural: ASSIGN RP-AUTOSAVE-IND := AP-AUTOSAVE-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Cur_Dflt_Opt().setValue(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt);                                                      //Natural: ASSIGN RP-AS-CUR-DFLT-OPT := AP-AS-CUR-DFLT-OPT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Cur_Dflt_Amt().setValue(annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt);                                                      //Natural: ASSIGN RP-AS-CUR-DFLT-AMT := AP-AS-CUR-DFLT-AMT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Incr_Opt().setValue(annty_Actvty_Prap_View_Ap_As_Incr_Opt);                                                              //Natural: ASSIGN RP-AS-INCR-OPT := AP-AS-INCR-OPT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Incr_Amt().setValue(annty_Actvty_Prap_View_Ap_As_Incr_Amt);                                                              //Natural: ASSIGN RP-AS-INCR-AMT := AP-AS-INCR-AMT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_As_Max_Pct().setValue(annty_Actvty_Prap_View_Ap_As_Max_Pct);                                                                //Natural: ASSIGN RP-AS-MAX-PCT := AP-AS-MAX-PCT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ae_Opt_Out_Days().setValue(annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days);                                                      //Natural: ASSIGN RP-AE-OPT-OUT-DAYS := AP-AE-OPT-OUT-DAYS
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Agent_Crd_No().setValue(annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id);                                                        //Natural: ASSIGN RP-AGENT-CRD-NO := AP-AGENT-OR-RACF-ID
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tsv_Ind().setValue(annty_Actvty_Prap_View_Ap_Tsv_Ind);                                                                      //Natural: ASSIGN RP-TSV-IND := AP-TSV-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tsr_Ind().setValue(annty_Actvty_Prap_View_Ap_Tsr_Ind);                                                                      //Natural: ASSIGN RP-TSR-IND := AP-TSR-IND
        //*  CA-082009 - ADDED THE FOLLOWING NEW FIELDS -- START
        if (condition(pnd_Extract_Record.getBoolean()))                                                                                                                   //Natural: IF #EXTRACT-RECORD
        {
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Welcome_Mail_Dt().setValue(Global.getDATN());                                                                           //Natural: ASSIGN RP-WELCOME-MAIL-DT := *DATN
            //*  BN1
            //*  BN1
            //*  BN1
            //*  BN1
            //*  BN1
            //*  BN1
            //*  BN1
            //*  BN1
            //*  TNGSUB
            //*  TNGSUB
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Ann_Option().setValue(annty_Actvty_Prap_View_Ap_Legal_Ann_Option);                                                    //Natural: ASSIGN RP-LEGAL-ANN-OPTION := AP-LEGAL-ANN-OPTION
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ann_Funding_Dt().setValue(annty_Actvty_Prap_View_Ap_Ann_Funding_Dt);                                                        //Natural: ASSIGN RP-ANN-FUNDING-DT := AP-ANN-FUNDING-DT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt);                                                  //Natural: ASSIGN RP-TIAA-ANN-ISSUE-DT := AP-TIAA-ANN-ISSUE-DT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt);                                                  //Natural: ASSIGN RP-CREF-ANN-ISSUE-DT := AP-CREF-ANN-ISSUE-DT
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Spcl_Lgl_Pkg_Ind().setValue(annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind);                                                    //Natural: ASSIGN RP-SPCL-LGL-PKG-IND := AP-SPCL-LGL-PKG-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Orchestration_Id().setValue(annty_Actvty_Prap_View_Ap_Orchestration_Id);                                                    //Natural: ASSIGN RP-ORCHESTRATION-ID := AP-ORCHESTRATION-ID
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Mail_Addr_Country_Cd().setValue(annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd);                                            //Natural: ASSIGN RP-MAIL-ADDR-COUNTRY-CD := AP-MAIL-ADDR-COUNTRY-CD
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Res_Addr_Line().getValue(1).setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(1));                                    //Natural: ASSIGN RP-RES-ADDR-LINE ( 1 ) := AP-ADDRESS-TXT ( 1 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Res_Addr_Line().getValue(2).setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(2));                                    //Natural: ASSIGN RP-RES-ADDR-LINE ( 2 ) := AP-ADDRESS-TXT ( 2 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Res_Addr_Line().getValue(3).setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(3));                                    //Natural: ASSIGN RP-RES-ADDR-LINE ( 3 ) := AP-ADDRESS-TXT ( 3 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Res_Addr_City().setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(4));                                                //Natural: ASSIGN RP-RES-ADDR-CITY := AP-ADDRESS-TXT ( 4 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Res_Addr_State_Code().setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(5));                                          //Natural: ASSIGN RP-RES-ADDR-STATE-CODE := AP-ADDRESS-TXT ( 5 )
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Res_Addr_Mail_Zip().setValue(annty_Actvty_Prap_View_Ap_Zip_Code);                                                           //Natural: ASSIGN RP-RES-ADDR-MAIL-ZIP := AP-ZIP-CODE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Substitution_Contract_Ind().setValue(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind);                                  //Natural: ASSIGN RP-SUBSTITUTION-CONTRACT-IND := AP-SUBSTITUTION-CONTRACT-IND
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Conv_Issue_State().setValue(annty_Actvty_Prap_View_Ap_Conv_Issue_State);                                                    //Natural: ASSIGN RP-CONV-ISSUE-STATE := AP-CONV-ISSUE-STATE
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Deceased_Ind().setValue(annty_Actvty_Prap_View_Ap_Deceased_Ind);                                                            //Natural: ASSIGN RP-DECEASED-IND := AP-DECEASED-IND
        //*  BN1
        if (condition(annty_Actvty_Prap_View_Ap_Name_Addr_Cd.greater(" ")))                                                                                               //Natural: IF AP-NAME-ADDR-CD GT ' '
        {
            //*  BN1
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Res_Addr_Country_Cd().setValueEdited(annty_Actvty_Prap_View_Ap_Name_Addr_Cd,new ReportEditMask("XXX"));                 //Natural: MOVE EDITED AP-NAME-ADDR-CD ( EM = XXX ) TO RP-RES-ADDR-COUNTRY-CD
            //*  BN1
            //*  MTSIN
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl180.getAcis_Reprint_Fl_View_Rp_Non_Proprietary_Pkg_Ind().setValue(annty_Actvty_Prap_View_Ap_Non_Proprietary_Pkg_Ind);                                      //Natural: ASSIGN RP-NON-PROPRIETARY-PKG-IND := AP-NON-PROPRIETARY-PKG-IND
        //*  SGRD5 KG
        if (condition(! (pnd_Reprint_Rec_Found.getBoolean())))                                                                                                            //Natural: IF NOT #REPRINT-REC-FOUND
        {
            //*  SGRD5 KG
            ldaAppl180.getVw_acis_Reprint_Fl_View().insertDBRow();                                                                                                        //Natural: STORE ACIS-REPRINT-FL-VIEW
            //*  SGRD5 KG
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl180.getVw_acis_Reprint_Fl_View().updateDBRow("UPD3");                                                                                                  //Natural: UPDATE ( UPD3. )
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRD5 KG
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Check_14_Days() throws Exception                                                                                                                     //Natural: CHECK-14-DAYS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Date_Diff.setValue(annty_Actvty_Prap_View_Ap_Dt_Ent_Sys);                                                                                                     //Natural: ASSIGN #DATE-DIFF := AP-DT-ENT-SYS
        pnd_Date_Yyyymmdd_Pnd_Date_Cc_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Cc);                                                                                         //Natural: ASSIGN #DATE-CC-1 := #DATE-DIFF-CC
        pnd_Date_Yyyymmdd_Pnd_Date_Yy_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Yy);                                                                                         //Natural: ASSIGN #DATE-YY-1 := #DATE-DIFF-YY
        pnd_Date_Yyyymmdd_Pnd_Date_Mm_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Mm);                                                                                         //Natural: ASSIGN #DATE-MM-1 := #DATE-DIFF-MM
        pnd_Date_Yyyymmdd_Pnd_Date_Dd_1.setValue(pnd_Date_Diff_Pnd_Date_Diff_Dd);                                                                                         //Natural: ASSIGN #DATE-DD-1 := #DATE-DIFF-DD
        pdaAciadate.getAciadate_Pnd_Date_N_1().setValue(pnd_Date_Yyyymmdd);                                                                                               //Natural: ASSIGN #DATE-N-1 := #DATE-YYYYMMDD
        pdaAciadate.getAciadate_Pnd_Date_N_2().setValue(pnd_Business_Date);                                                                                               //Natural: ASSIGN #DATE-N-2 := #BUSINESS-DATE
        pdaAciadate.getAciadate_Pnd_Function().setValue("01");                                                                                                            //Natural: ASSIGN ACIADATE.#FUNCTION := '01'
        DbsUtil.callnat(Acindate.class , getCurrentProcessState(), pdaAciadate.getAciadate());                                                                            //Natural: CALLNAT 'ACINDATE' ACIADATE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Update_Reprint_Data() throws Exception                                                                                                               //Natural: UPDATE-REPRINT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        ldaAppl180.getVw_acis_Reprint_Fl_View().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) ACIS-REPRINT-FL-VIEW BY RP-TIAA-CONTR = AP-TIAA-CNTRCT
        (
        "UPD",
        new Wc[] { new Wc("RP_TIAA_CONTR", ">=", annty_Actvty_Prap_View_Ap_Tiaa_Cntrct, WcType.BY) },
        new Oc[] { new Oc("RP_TIAA_CONTR", "ASC") },
        1
        );
        UPD:
        while (condition(ldaAppl180.getVw_acis_Reprint_Fl_View().readNextRow("UPD")))
        {
            if (condition(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Contr().notEquals(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct)))                                           //Natural: IF ACIS-REPRINT-FL-VIEW.RP-TIAA-CONTR NE AP-TIAA-CNTRCT
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            UPD1:                                                                                                                                                         //Natural: GET ACIS-REPRINT-FL-VIEW *ISN ( UPD. )
            ldaAppl180.getVw_acis_Reprint_Fl_View().readByID(ldaAppl180.getVw_acis_Reprint_Fl_View().getAstISN("UPD"), "UPD1");
            //*  ROCH KG - COMBINED IF STMTS FOR RP-PACKAGE-MAIL-TYPE INTO DECIDE STMT.
            //*          - DON'T RESET REPRINT PKG MAIL TYPE FOR LEGAL PACKAGE IF
            //*            457(B) PRIVATE AND GA5 LEGAL CONTRACTS ARE SUPPRESSED.
            //*  BIP
            //*  BIP
            //*  ROCH KG
            //*  ROCH KG
            short decideConditionsMet4521 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-DECEDENT-CONTRACT NOT = ' '
            if (condition(annty_Actvty_Prap_View_Ap_Decedent_Contract.notEquals(" ")))
            {
                decideConditionsMet4521++;
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("P");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'P'
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-TYPE = 'RL'
            else if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RL")))
            {
                decideConditionsMet4521++;
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("L");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'L'
            }                                                                                                                                                             //Natural: WHEN ( AP-LOB = 'S' AND AP-LOB-TYPE = '4' ) OR ( AP-LOB = 'S' AND AP-LOB-TYPE = '9' )
            else if (condition(((annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("4")) || (annty_Actvty_Prap_View_Ap_Lob.equals("S") 
                && annty_Actvty_Prap_View_Ap_Lob_Type.equals("9")))))
            {
                decideConditionsMet4521++;
                //*  ROCH KG
                //*  ROCH KG
                if (condition((pdaAppa7900.getAppa7900_Ret_Suppress_Ind().equals("N"))))                                                                                  //Natural: IF ( RET-SUPPRESS-IND = 'N' )
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("B");                                                                              //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'B'
                    //*  ROCH KG
                    //*  ILLSMP KG START             /* ROCH KG
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN AP-OWNERSHIP = 3
            else if (condition(annty_Actvty_Prap_View_Ap_Ownership.equals(3)))
            {
                decideConditionsMet4521++;
                //*  INST  LS
                //*  KG TNGSUB
                //*  ROCH KG
                if (condition((pnd_Welc_Force_Inst_Owned.getBoolean()) || (pnd_Rc_Or_Rcp.getBoolean())))                                                                  //Natural: IF ( #WELC-FORCE-INST-OWNED ) OR ( #RC-OR-RCP )
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("B");                                                                              //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'B'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("L");                                                                              //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'L'
                    //*  TNGSUB
                    //*  TNGSUB
                    //*  ROCH KG
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN AP-SUBSTITUTION-CONTRACT-IND GT ' '
            else if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.greater(" ")))
            {
                decideConditionsMet4521++;
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("L");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'L'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Package_Mail_Type().setValue("B");                                                                                  //Natural: ASSIGN RP-PACKAGE-MAIL-TYPE := 'B'
                //*  ROCH KG
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CA-082009 - KEEP TRACK OF LEGAL MAIL DATE
            if (condition(pnd_Extract_Record.getBoolean()))                                                                                                               //Natural: IF #EXTRACT-RECORD
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Mail_Dt().setValue(Global.getDATN());                                                                         //Natural: ASSIGN RP-LEGAL-MAIL-DT := *DATN
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Legal_Ann_Option().setValue(annty_Actvty_Prap_View_Ap_Legal_Ann_Option);                                                //Natural: ASSIGN RP-LEGAL-ANN-OPTION := AP-LEGAL-ANN-OPTION
            //*  LS TIC >>>
            if (condition(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N.greater(getZero())))                                                                                       //Natural: IF #AP-TIC-STARTDATE-N > 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Startdate().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N);                                                     //Natural: MOVE #AP-TIC-STARTDATE-N TO RP-TIC-STARTDATE
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Enddate().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_N);                                                         //Natural: MOVE #AP-TIC-ENDDATE-N TO RP-TIC-ENDDATE
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Percentage().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_N);                                                   //Natural: MOVE #AP-TIC-PERCENTAGE-N TO RP-TIC-PERCENTAGE
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Postdays().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_N);                                                       //Natural: MOVE #AP-TIC-POSTDAYS-N TO RP-TIC-POSTDAYS
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Limit().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_N);                                                             //Natural: MOVE #AP-TIC-LIMIT-N TO RP-TIC-LIMIT
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Postfreq().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Postfreq());                                                 //Natural: MOVE DOC-TIC-POSTFREQ TO RP-TIC-POSTFREQ
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Pl_Level().setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Pl_Level());                                                 //Natural: MOVE DOC-TIC-PL-LEVEL TO RP-TIC-PL-LEVEL
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Windowdays().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_N);                                                   //Natural: MOVE #AP-TIC-WINDOWDAYS-N TO RP-TIC-WINDOWDAYS
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Reqdlywindow().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_N);                                               //Natural: MOVE #AP-TIC-REQDLYWINDOW-N TO RP-TIC-REQDLYWINDOW
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tic_Recap_Prov().setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_N);                                                   //Natural: MOVE #AP-TIC-RECAP-PROV-N TO RP-TIC-RECAP-PROV
                //*   LS TIC <<<
            }                                                                                                                                                             //Natural: END-IF
            //*  LPAO KG START - MOVE ANNUITY FUNDED FIELDS TO REPRINT IF POPULATED
            if (condition(annty_Actvty_Prap_View_Ap_Ann_Funding_Dt.notEquals(getZero())))                                                                                 //Natural: IF AP-ANN-FUNDING-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Ann_Funding_Dt().setValue(annty_Actvty_Prap_View_Ap_Ann_Funding_Dt);                                                //Natural: ASSIGN RP-ANN-FUNDING-DT := AP-ANN-FUNDING-DT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt.notEquals(getZero())))                                                                              //Natural: IF AP-TIAA-ANN-ISSUE-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Tiaa_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Ann_Issue_Dt);                                          //Natural: ASSIGN RP-TIAA-ANN-ISSUE-DT := AP-TIAA-ANN-ISSUE-DT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt.notEquals(getZero())))                                                                              //Natural: IF AP-CREF-ANN-ISSUE-DT NE 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Cref_Ann_Issue_Dt().setValue(annty_Actvty_Prap_View_Ap_Cref_Ann_Issue_Dt);                                          //Natural: ASSIGN RP-CREF-ANN-ISSUE-DT := AP-CREF-ANN-ISSUE-DT
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl180.getAcis_Reprint_Fl_View_Rp_Spcl_Lgl_Pkg_Ind().setValue(annty_Actvty_Prap_View_Ap_Spcl_Lgl_Pkg_Ind);                                                //Natural: ASSIGN RP-SPCL-LGL-PKG-IND := AP-SPCL-LGL-PKG-IND
            //*  ONEIRA2
            //*  ONEIRA2
            if (condition(annty_Actvty_Prap_View_Ap_Financial_2.greater(getZero()) && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_2().equals(getZero())))             //Natural: IF AP-FINANCIAL-2 GT 0 AND RP-FINANCIAL-2 = 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_2().setValue(annty_Actvty_Prap_View_Ap_Financial_2);                                                      //Natural: ASSIGN RP-FINANCIAL-2 := AP-FINANCIAL-2
                //*  ONEIRA2
            }                                                                                                                                                             //Natural: END-IF
            //*  ONEIRA2
            //*  ONEIRA2
            if (condition(annty_Actvty_Prap_View_Ap_Financial_3.greater(getZero()) && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_3().equals(getZero())))             //Natural: IF AP-FINANCIAL-3 GT 0 AND RP-FINANCIAL-3 = 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_3().setValue(annty_Actvty_Prap_View_Ap_Financial_3);                                                      //Natural: ASSIGN RP-FINANCIAL-3 := AP-FINANCIAL-3
                //*  ONEIRA2
            }                                                                                                                                                             //Natural: END-IF
            //*  ONEIRA2
            //*  ONEIRA2
            if (condition(annty_Actvty_Prap_View_Ap_Financial_4.greater(getZero()) && ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_4().equals(getZero())))             //Natural: IF AP-FINANCIAL-4 GT 0 AND RP-FINANCIAL-4 = 0
            {
                ldaAppl180.getAcis_Reprint_Fl_View_Rp_Financial_4().setValue(annty_Actvty_Prap_View_Ap_Financial_4);                                                      //Natural: ASSIGN RP-FINANCIAL-4 := AP-FINANCIAL-4
                //*  ONEIRA2
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl180.getVw_acis_Reprint_Fl_View().updateDBRow("UPD1");                                                                                                  //Natural: UPDATE ( UPD1. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Update_Records() throws Exception                                                                                                                    //Natural: UPDATE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------
        GET1:                                                                                                                                                             //Natural: GET ANNTY-ACTVTY-PRAP-VIEW *ISN ( FINDUPD. )
        vw_annty_Actvty_Prap_View.readByID(vw_annty_Actvty_Prap_View.getAstISN("FINDUPD"), "GET1");
        short decideConditionsMet4609 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #INPUT-PARM;//Natural: VALUE 'WELCOME'
        if (condition((pnd_Input_Parm.equals("WELCOME"))))
        {
            decideConditionsMet4609++;
            annty_Actvty_Prap_View_Ap_Release_Ind.setValue(4);                                                                                                            //Natural: MOVE 4 TO AP-RELEASE-IND
        }                                                                                                                                                                 //Natural: VALUE 'LEGAL'
        else if (condition((pnd_Input_Parm.equals("LEGAL"))))
        {
            decideConditionsMet4609++;
            annty_Actvty_Prap_View_Ap_Release_Ind.setValue(6);                                                                                                            //Natural: MOVE 6 TO AP-RELEASE-IND
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* * SGRD KG - HARDCODED SUNGARD TEAM IF NECESSARY
        if (condition(annty_Actvty_Prap_View_Ap_Ppg_Team_Cde.equals(" ")))                                                                                                //Natural: IF AP-PPG-TEAM-CDE = ' '
        {
            //*  SGRD KG
            if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.equals("SGRD")))                                                                                            //Natural: IF AP-COLL-CODE = 'SGRD'
            {
                //*  SGRD KG
                annty_Actvty_Prap_View_Ap_Ppg_Team_Cde.setValue("TMOMN");                                                                                                 //Natural: MOVE 'TMOMN' TO AP-PPG-TEAM-CDE
                //*  SGRD KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                annty_Actvty_Prap_View_Ap_Ppg_Team_Cde.setValue(pnd_Team);                                                                                                //Natural: ASSIGN AP-PPG-TEAM-CDE := #TEAM
                //*  SGRD KG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("S") && annty_Actvty_Prap_View_Ap_Lob_Type.equals("3") && annty_Actvty_Prap_View_Ap_Mult_App_Status.equals(" ")))) //Natural: IF ( AP-LOB = 'S' AND AP-LOB-TYPE = '3' AND AP-MULT-APP-STATUS = ' ' )
        {
            annty_Actvty_Prap_View_Ap_Mult_App_Status.setValue("C");                                                                                                      //Natural: ASSIGN AP-MULT-APP-STATUS := 'C'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Bene_Category.equals("1")))                                                                                               //Natural: IF AP-BENE-CATEGORY = '1'
        {
            annty_Actvty_Prap_View_Ap_Primary_Bene_Info.getValue(1).setValue("NONE");                                                                                     //Natural: MOVE 'NONE' TO AP-PRIMARY-BENE-INFO ( 1 )
            annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(1).setValue("NONE");                                                                                  //Natural: MOVE 'NONE' TO AP-CONTINGENT-BENE-INFO ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  LS TIC >>>
        if (condition(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N.greater(getZero())))                                                                                           //Natural: IF #AP-TIC-STARTDATE-N > 0
        {
            annty_Actvty_Prap_View_Ap_Tic_Startdate.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Startdate_N);                                                                       //Natural: MOVE #AP-TIC-STARTDATE-N TO AP-TIC-STARTDATE
            annty_Actvty_Prap_View_Ap_Tic_Enddate.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Enddate_N);                                                                           //Natural: MOVE #AP-TIC-ENDDATE-N TO AP-TIC-ENDDATE
            annty_Actvty_Prap_View_Ap_Tic_Percentage.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Percentage_N);                                                                     //Natural: MOVE #AP-TIC-PERCENTAGE-N TO AP-TIC-PERCENTAGE
            annty_Actvty_Prap_View_Ap_Tic_Postdays.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Postdays_N);                                                                         //Natural: MOVE #AP-TIC-POSTDAYS-N TO AP-TIC-POSTDAYS
            annty_Actvty_Prap_View_Ap_Tic_Limit.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Limit_N);                                                                               //Natural: MOVE #AP-TIC-LIMIT-N TO AP-TIC-LIMIT
            annty_Actvty_Prap_View_Ap_Tic_Postfreq.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Postfreq());                                                                   //Natural: MOVE DOC-TIC-POSTFREQ TO AP-TIC-POSTFREQ
            annty_Actvty_Prap_View_Ap_Tic_Pl_Level.setValue(pdaScia8100.getDoc_Rec_Doc_Tic_Pl_Level());                                                                   //Natural: MOVE DOC-TIC-PL-LEVEL TO AP-TIC-PL-LEVEL
            annty_Actvty_Prap_View_Ap_Tic_Windowdays.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Windowdays_N);                                                                     //Natural: MOVE #AP-TIC-WINDOWDAYS-N TO AP-TIC-WINDOWDAYS
            annty_Actvty_Prap_View_Ap_Tic_Reqdlywindow.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Reqdlywindow_N);                                                                 //Natural: MOVE #AP-TIC-REQDLYWINDOW-N TO AP-TIC-REQDLYWINDOW
            annty_Actvty_Prap_View_Ap_Tic_Recap_Prov.setValue(pnd_Ap_Tic_Wk_Pnd_Ap_Tic_Recap_Prov_N);                                                                     //Natural: MOVE #AP-TIC-RECAP-PROV-N TO AP-TIC-RECAP-PROV
            //*   LS TIC <<<
        }                                                                                                                                                                 //Natural: END-IF
        vw_annty_Actvty_Prap_View.updateDBRow("GET1");                                                                                                                    //Natural: UPDATE ( GET1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Yr2000_Date_Compare() throws Exception                                                                                                               //Natural: YR2000-DATE-COMPARE
    {
        if (BLNatReinput.isReinput()) return;

        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Currccyymmdd().setValue(Global.getDATN());                                                                              //Natural: MOVE *DATN TO #Y2-CURRCCYYMMDD
        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9().setValue(9);                                                                                                    //Natural: MOVE 9 TO #Y2-DTE-9
        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy().setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Curryy());                                                  //Natural: MOVE #Y2-CURRYY TO #Y2-DTE-YY
        //*  60/40 RULE
        short decideConditionsMet4660 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #Y2-DATE-RULE;//Natural: VALUE '1'
        if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("1"))))
        {
            decideConditionsMet4660++;
            //*  80/20 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy60());                                              //Natural: SUBTRACT #Y2-YY60 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("2"))))
        {
            decideConditionsMet4660++;
            //*  90/10 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy80());                                              //Natural: SUBTRACT #Y2-YY80 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("3"))))
        {
            decideConditionsMet4660++;
            //*  DEFAULT TO 70/30 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy90());                                              //Natural: SUBTRACT #Y2-YY90 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy70());                                              //Natural: SUBTRACT #Y2-YY70 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA1 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area2().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA2 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area2().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area2().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA2
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet4691 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #Y2-YYMMDD-AREA1-A = '000000' OR = ' ' OR = '0000' OR = '0'
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals(" ") 
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("0")))
        {
            decideConditionsMet4691++;
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area1_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA1-9
        }                                                                                                                                                                 //Natural: WHEN #Y2-YYMMDD-AREA2-A = '000000' OR = ' ' OR = '0000' OR = '0'
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals(" ") 
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("0")))
        {
            decideConditionsMet4691++;
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area2_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA2-9
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet4691 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Yr2000_Date_Compare_3() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-3
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area3().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA3 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area3().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area3().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA3
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA3-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area3_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA3-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_Date_Compare_4() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-4
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area4().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA4 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area4().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area4().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA4
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA4-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area4_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA4-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_Date_Compare_5() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-5
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area5().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA5 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area5().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area5().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA5
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA5-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area5_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA5-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare() throws Exception                                                                                                             //Natural: YR2000-J-DATE-COMPARE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area1().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA1 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area1().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area1().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area2().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA2 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area2().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area2().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA2
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_3() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-3
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area3().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA3 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area3().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area3().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA3
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_4() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-4
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area4().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA4 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area4().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area4().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA4
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_5() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-5
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area5().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA5 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area5().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area5().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA5
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  OIA KG
    private void sub_Call_Acin2100_Get_Product_Cde() throws Exception                                                                                                     //Natural: CALL-ACIN2100-GET-PRODUCT-CDE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pdaAcia2100.getAcia2100().reset();                                                                                                                                //Natural: RESET ACIA2100
        //*  BY LOB/LBTYPE
        pdaAcia2100.getAcia2100_Pnd_Search_Mode().setValue("LB");                                                                                                         //Natural: MOVE 'LB' TO ACIA2100.#SEARCH-MODE
        pdaAcia2100.getAcia2100_Pnd_S_Lb_Entry_Actve_Ind().setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO ACIA2100.#S-LB-ENTRY-ACTVE-IND
        pdaAcia2100.getAcia2100_Pnd_S_Lb_Entry_Table_Id_Nbr().setValue(100);                                                                                              //Natural: MOVE 000100 TO ACIA2100.#S-LB-ENTRY-TABLE-ID-NBR
        pdaAcia2100.getAcia2100_Pnd_S_Lb_Entry_Table_Sub_Id().setValue("LB");                                                                                             //Natural: MOVE 'LB' TO ACIA2100.#S-LB-ENTRY-TABLE-SUB-ID
        pdaAcia2100.getAcia2100_Pnd_S_Lb_Lob().setValue(annty_Actvty_Prap_View_Ap_Lob);                                                                                   //Natural: MOVE AP-LOB TO ACIA2100.#S-LB-LOB
        pdaAcia2100.getAcia2100_Pnd_S_Lb_Lob_Type().setValue(annty_Actvty_Prap_View_Ap_Lob_Type);                                                                         //Natural: MOVE AP-LOB-TYPE TO ACIA2100.#S-LB-LOB-TYPE
        pdaAcia2100.getAcia2100_Pnd_S_Lb_Vesting().setValue(annty_Actvty_Prap_View_Ap_Ownership);                                                                         //Natural: MOVE AP-OWNERSHIP TO ACIA2100.#S-LB-VESTING
        //*  APP-TABLE-ENTRY ('LB' TABLE)
        DbsUtil.callnat(Acin2100.class , getCurrentProcessState(), pdaAcia2100.getAcia2100(), pnd_Debug);                                                                 //Natural: CALLNAT 'ACIN2100' ACIA2100 #DEBUG
        if (condition(Global.isEscape())) return;
        if (condition(pdaAcia2100.getAcia2100_Pnd_Return_Code().equals(" ")))                                                                                             //Natural: IF ACIA2100.#RETURN-CODE = ' '
        {
            ldaAppl160.getDoc_Ext_File_Dx_Product_Cde().setValueEdited(pdaAcia2100.getAcia2100_Pnd_Lb_Product_Code(),new ReportEditMask("999"));                          //Natural: MOVE EDITED ACIA2100.#LB-PRODUCT-CODE ( EM = 999 ) TO DX-PRODUCT-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-ACIN2100-GET-PRODUCT-CDE
    }
    //*  SGRD KG
    //*  SGRD4 KG
    private void sub_Call_Plan_Interface() throws Exception                                                                                                               //Natural: CALL-PLAN-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        pdaScia8100.getDoc_Rec_Doc_Plan_Num().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                           //Natural: ASSIGN DOC-PLAN-NUM := AP-SGRD-PLAN-NO
        pdaScia8100.getDoc_Rec_Doc_Subplan_Num().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                                     //Natural: ASSIGN DOC-SUBPLAN-NUM := AP-SGRD-SUBPLAN-NO
        pdaScia8100.getDoc_Rec_Doc_Part_Num().setValue(annty_Actvty_Prap_View_Ap_Soc_Sec);                                                                                //Natural: ASSIGN DOC-PART-NUM := AP-SOC-SEC
        //*  SGRD6 KG
        pnd_Tiaa_Contact_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Prfx().getValue(1), ldaAppl160.getDoc_Ext_File_Dx_Tiaa_No().getValue(1))); //Natural: COMPRESS DX-TIAA-PRFX ( 1 ) DX-TIAA-NO ( 1 ) INTO #TIAA-CONTACT-NBR LEAVING NO SPACE
        //*  SGRD6 KG
        pnd_Cref_Contact_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl160.getDoc_Ext_File_Dx_Cref_Prfx().getValue(1), ldaAppl160.getDoc_Ext_File_Dx_Cref_No().getValue(1))); //Natural: COMPRESS DX-CREF-PRFX ( 1 ) DX-CREF-NO ( 1 ) INTO #CREF-CONTACT-NBR LEAVING NO SPACE
        //*                                                 /* SGRD6 KG
        if (condition(pnd_Int_First_Time.equals("Y")))                                                                                                                    //Natural: IF #INT-FIRST-TIME = 'Y'
        {
            pdaScia8100.getDoc_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN DOC-FILE-OPEN-SW := 'N'
            pnd_Int_First_Time.setValue("N");                                                                                                                             //Natural: ASSIGN #INT-FIRST-TIME := 'N'
            //*  SGRD6 KG
            //*  SGRD6 KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia8100.getDoc_File_Open_Sw().setValue("Y");                                                                                                              //Natural: ASSIGN DOC-FILE-OPEN-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRDSR KG
        pnd_Spec_Fund_Ind.reset();                                                                                                                                        //Natural: RESET #SPEC-FUND-IND
        //*  SGRD6 KG
        //*  SGRD6 KG
        //*  CALSTRS KG
        //*  KG TNGSUB2
        //*  CREA
        DbsUtil.callnat(Scin8100.class , getCurrentProcessState(), pnd_Input_Parm, pnd_Tiaa_Contact_Nbr, pnd_Cref_Contact_Nbr, annty_Actvty_Prap_View_Ap_Lob,             //Natural: CALLNAT 'SCIN8100' #INPUT-PARM #TIAA-CONTACT-NBR #CREF-CONTACT-NBR AP-LOB AP-LOB-TYPE AP-REGION-CODE AP-COLL-CODE AP-SGRD-CLIENT-ID AP-SUBSTITUTION-CONTRACT-IND #TRANSLTE-FILE ( * ) DOC-FILE-OPEN-SW DOC-REC
            annty_Actvty_Prap_View_Ap_Lob_Type, annty_Actvty_Prap_View_Ap_Region_Code, annty_Actvty_Prap_View_Ap_Coll_Code, annty_Actvty_Prap_View_Ap_Sgrd_Client_Id, 
            annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind, pnd_Translte_File.getValue("*"), pdaScia8100.getDoc_File_Open_Sw(), pdaScia8100.getDoc_Rec());
        if (condition(Global.isEscape())) return;
        //*  CALL-PLAN-INTERFACE
    }
    //*  SGRD3 KG
    private void sub_Call_Cntct_Interface() throws Exception                                                                                                              //Natural: CALL-CNTCT-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        pdaScia8200.getCon_Part_Rec_Con_Plan_Num().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                      //Natural: ASSIGN CON-PLAN-NUM := AP-SGRD-PLAN-NO
        //*  DCA KG
        pdaScia8200.getCon_Part_Rec_Con_Part_Divsub().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Divsub);                                                                    //Natural: MOVE AP-SGRD-DIVSUB TO CON-PART-DIVSUB
        //*  SGRD5 KG
        //*  SGRD5 KG
        //*  SGRD5 KG
        if (condition(pnd_Con_First_Time.equals("Y")))                                                                                                                    //Natural: IF #CON-FIRST-TIME = 'Y'
        {
            pdaScia8200.getCon_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN CON-FILE-OPEN-SW := 'N'
            pnd_Con_First_Time.setValue("N");                                                                                                                             //Natural: ASSIGN #CON-FIRST-TIME := 'N'
            //*  SGRD7 KG
            //*  SGRD7 KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia8200.getCon_File_Open_Sw().setValue("Y");                                                                                                              //Natural: ASSIGN CON-FILE-OPEN-SW := 'Y'
            //*  SGRD5 KG
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia8200.getCon_Part_Rec_Con_Function_Code().getValue(1).setValue("08");                                                                                       //Natural: ASSIGN CON-FUNCTION-CODE ( 1 ) := '08'
        pdaScia8200.getCon_Part_Rec_Con_Access_Level().setValue(" ");                                                                                                     //Natural: ASSIGN CON-ACCESS-LEVEL := ' '
        pdaScia8200.getCon_Part_Rec_Con_Contact_Type().setValue("E");                                                                                                     //Natural: ASSIGN CON-CONTACT-TYPE := 'E'
        DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                     //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia8200.getCon_Part_Rec_Con_Return_Code().notEquals(getZero()) || pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg().notEquals(" ")))            //Natural: IF CON-RETURN-CODE NE 0 OR CON-CONTACT-ERR-MSG NE ' '
        {
            pnd_Ac_Pda_Pnd_Last_Nme.getValue(1).reset();                                                                                                                  //Natural: RESET #LAST-NME ( 1 ) #ADDRESS-LINE-1 ( 1 )
            pnd_Ac_Pda_Pnd_Address_Line_1.getValue(1).reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().equals("N")))                                                                                          //Natural: IF CON-EOF-SW = 'N'
            {
                //*  JC1
                pdaScia8200.getCon_Part_Rec_Con_Contact_Name().separate(EnumSet.of(SeparateOption.Ignore,SeparateOption.WithAnyDelimiters), ",", pnd_Ac_Pda_Pnd_Last_Nme.getValue(1),  //Natural: SEPARATE CON-CONTACT-NAME INTO #LAST-NME ( 1 ) #FIRST-NME ( 1 ) IGNORE WITH DELIMITERS ','
                    pnd_Ac_Pda_Pnd_First_Nme.getValue(1));
                pnd_Ac_Pda_Pnd_Middle_Nme.getValue(1).setValue(" ");                                                                                                      //Natural: ASSIGN #MIDDLE-NME ( 1 ) := ' '
                pnd_Ac_Pda_Pnd_Prefix_Nme.getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Title());                                                                  //Natural: ASSIGN #PREFIX-NME ( 1 ) := CON-TITLE
                pnd_Ac_Pda_Pnd_Title_Nme.getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Job_Title());                                                               //Natural: ASSIGN #TITLE-NME ( 1 ) := CON-JOB-TITLE
                pnd_Ac_Pda_Pnd_Org_Nme.getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Company_Name());                                                              //Natural: ASSIGN #ORG-NME ( 1 ) := CON-COMPANY-NAME
                pnd_Ac_Pda_Pnd_Postal_Cde.getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Zip());                                                                    //Natural: ASSIGN #POSTAL-CDE ( 1 ) := CON-ZIP
                pnd_Ac_Pda_Pnd_Address_Line_1.getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line1());                                                      //Natural: ASSIGN #ADDRESS-LINE-1 ( 1 ) := CON-ADDRESS-LINE1
                pnd_Hold_Cstate.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Char, pdaScia8200.getCon_Part_Rec_Con_State()));                             //Natural: COMPRESS #CHAR CON-STATE INTO #HOLD-CSTATE LEAVING NO SPACE
                pnd_Hold_City_St.setValue(DbsUtil.compress(CompressOption.WithDelimiters, ',', pdaScia8200.getCon_Part_Rec_Con_City(), pnd_Hold_Cstate));                 //Natural: COMPRESS CON-CITY #HOLD-CSTATE INTO #HOLD-CITY-ST WITH DELIMITER ','
                DbsUtil.examine(new ExamineSource(pnd_Hold_City_St), new ExamineSearch("#"), new ExamineReplace(" "));                                                    //Natural: EXAMINE #HOLD-CITY-ST FOR '#' REPLACE WITH ' '
                pnd_Hold_Zip.setValue(pdaScia8200.getCon_Part_Rec_Con_Zip());                                                                                             //Natural: MOVE CON-ZIP TO #HOLD-ZIP
                pnd_Hold_Addr.setValue(DbsUtil.compress(pnd_Hold_City_St, pnd_Hold_Zip_Pnd_Hold_Zip5));                                                                   //Natural: COMPRESS #HOLD-CITY-ST #HOLD-ZIP5 INTO #HOLD-ADDR WITH DELIMITER ' '
                pnd_Ac_Pda_Pnd_Address_Line_3.getValue(1).setValue(" ");                                                                                                  //Natural: MOVE ' ' TO #ADDRESS-LINE-3 ( 1 ) #ADDRESS-LINE-4 ( 1 )
                pnd_Ac_Pda_Pnd_Address_Line_4.getValue(1).setValue(" ");
                if (condition(pdaScia8200.getCon_Part_Rec_Con_Address_Line2().notEquals(" ")))                                                                            //Natural: IF CON-ADDRESS-LINE2 NE ' '
                {
                    pnd_Ac_Pda_Pnd_Address_Line_2.getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line2());                                                  //Natural: ASSIGN #ADDRESS-LINE-2 ( 1 ) := CON-ADDRESS-LINE2
                    pnd_Ac_Pda_Pnd_Address_Line_3.getValue(1).setValue(pnd_Hold_Addr);                                                                                    //Natural: ASSIGN #ADDRESS-LINE-3 ( 1 ) := #HOLD-ADDR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ac_Pda_Pnd_Address_Line_2.getValue(1).setValue(pnd_Hold_Addr);                                                                                    //Natural: ASSIGN #ADDRESS-LINE-2 ( 1 ) := #HOLD-ADDR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-CNTCT-INTERFACE /* SGRD3 KG
    }
    //*  SGRD6 KG
    private void sub_Call_Prem_Interface() throws Exception                                                                                                               //Natural: CALL-PREM-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        pnd_Prem_Found.reset();                                                                                                                                           //Natural: RESET #PREM-FOUND #IRA-EXCEPT-FOUND PPT-PART-FUND-REC
        pnd_Ira_Except_Found.reset();
        pdaScia8250.getPpt_Part_Fund_Rec().reset();
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Request_Type().setValue("F");                                                                                                //Natural: ASSIGN PPT-REQUEST-TYPE := 'F'
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Eof_Sw().setValue("N");                                                                                                      //Natural: ASSIGN PPT-EOF-SW := 'N'
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Plan_Num().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                 //Natural: ASSIGN PPT-PLAN-NUM := AP-SGRD-PLAN-NO
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Part_Sub_Plan().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                         //Natural: ASSIGN PPT-PART-SUB-PLAN := AP-SGRD-SUBPLAN-NO
        //*  PROD FIX SGRD8B KG
        if (condition(annty_Actvty_Prap_View_Ap_Soc_Sec.greater(getZero())))                                                                                              //Natural: IF AP-SOC-SEC > 0
        {
            pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Part_Number().setValueEdited(annty_Actvty_Prap_View_Ap_Soc_Sec,new ReportEditMask("999999999"));                         //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999999999 ) TO PPT-PART-NUMBER
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Int_First_Time.equals("Y")))                                                                                                                    //Natural: IF #INT-FIRST-TIME = 'Y'
        {
            pdaScia8250.getPpt_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN PPT-FILE-OPEN-SW := 'N'
            pnd_Int_First_Time.setValue("N");                                                                                                                             //Natural: ASSIGN #INT-FIRST-TIME := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia8250.getPpt_File_Open_Sw().setValue("Y");                                                                                                              //Natural: ASSIGN PPT-FILE-OPEN-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Scin8250.class , getCurrentProcessState(), pdaScia8250.getPpt_File_Open_Sw(), pdaScia8250.getPpt_Part_Fund_Rec());                                //Natural: CALLNAT 'SCIN8250' PPT-FILE-OPEN-SW PPT-PART-FUND-REC
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Code().notEquals(getZero())))                                                                           //Natural: IF PPT-RETURN-CODE NE 0
        {
            pnd_Ira_Except_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #IRA-EXCEPT-FOUND = TRUE
            pnd_Exception_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #EXCEPTION-CNT
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT
            sub_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_First_Contrib_Date().greater(getZero())))                                                                  //Natural: IF PPT-FIRST-CONTRIB-DATE > 0
            {
                pnd_Prem_Found.setValue(true);                                                                                                                            //Natural: ASSIGN #PREM-FOUND = TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-PREM-INTERFACE
    }
    //*  SGRD KG
    private void sub_Exception_Report() throws Exception                                                                                                                  //Natural: EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        short decideConditionsMet4924 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN DOC-RETURN-MSG-MODULE = 'PLPLIO'
        if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().equals("PLPLIO")))
        {
            decideConditionsMet4924++;
            pnd_Except_Reason.setValue("PLAN NOT FOUND IN OMNI");                                                                                                         //Natural: MOVE 'PLAN NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN DOC-RETURN-MSG-MODULE = 'SPSPIO'
        else if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().equals("SPSPIO")))
        {
            decideConditionsMet4924++;
            pnd_Except_Reason.setValue("SUBPLAN NOT FOUND IN OMNI");                                                                                                      //Natural: MOVE 'SUBPLAN NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN DOC-RETURN-MSG-MODULE = 'IVICIO'
        else if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().equals("IVICIO")))
        {
            decideConditionsMet4924++;
            pnd_Except_Reason.setValue("INVESTMENTS/FUNDS NOT FOUND IN OMNI");                                                                                            //Natural: MOVE 'INVESTMENTS/FUNDS NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN DOC-RETURN-MSG-MODULE = 'ADA6888'
        else if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().equals("ADA6888")))
        {
            decideConditionsMet4924++;
            //*  TIC LS1
            pnd_Except_Reason.setValue("INTEREST RATE NOT FOUND IN OMNI");                                                                                                //Natural: MOVE 'INTEREST RATE NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN DOC-RETURN-MSG-MODULE = 'GROSSUP'
        else if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().equals("GROSSUP")))
        {
            decideConditionsMet4924++;
            //*  TIC LS1
            //*  SGRD6 KG
            pnd_Except_Reason.setValue("INVALID TB.GROSSUP FILE IN OMNI");                                                                                                //Natural: MOVE 'INVALID TB.GROSSUP FILE IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN PPT-RETURN-MSG-MODULE = 'PTPTIOX'
        else if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().equals("PTPTIOX")))
        {
            decideConditionsMet4924++;
            //*  SGRD6 KG
            pnd_Except_Reason.setValue("PARTICIPANT NOT FOUND IN OMNI");                                                                                                  //Natural: MOVE 'PARTICIPANT NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN PPT-RETURN-MSG-MODULE = 'PLPLIO'
        else if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().equals("PLPLIO")))
        {
            decideConditionsMet4924++;
            pnd_Except_Reason.setValue("PLAN NOT FOUND IN OMNI");                                                                                                         //Natural: MOVE 'PLAN NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            //*  SGRD6 KG
            if (condition(pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().equals(" ")))                                                                                    //Natural: IF DOC-RETURN-MSG-MODULE = ' '
            {
                //*  SGRD6 KG
                pnd_Except_Reason.setValue(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Message());                                                                        //Natural: MOVE PPT-RETURN-MESSAGE TO #EXCEPT-REASON
                //*  SGRD6 KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  SGRD6 KG
                pnd_Except_Reason.setValue(pdaScia8100.getDoc_Rec_Doc_Return_Message());                                                                                  //Natural: MOVE DOC-RETURN-MESSAGE TO #EXCEPT-REASON
                //*  SGRD6 KG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  KG TNGSUB - NEW ERROR MESSAGE FOR MISSING PIN
        //*  IF AP-PIN-NBR NE MASK(NNNNNNN) OR       /* KG TNGSUB START  /* PINE
        //*  KG TNGSUB START  /* PINE
        if (condition(! (DbsUtil.maskMatches(annty_Actvty_Prap_View_Ap_Pin_Nbr,"NNNNNNNNNNNN")) || annty_Actvty_Prap_View_Ap_Pin_Nbr.equals(getZero())))                  //Natural: IF AP-PIN-NBR NE MASK ( NNNNNNNNNNNN ) OR AP-PIN-NBR EQ 0
        {
            pnd_Except_Reason.setValue("PIN NBR IS ZERO OR INVALID");                                                                                                     //Natural: MOVE 'PIN NBR IS ZERO OR INVALID' TO #EXCEPT-REASON
            //*  KG TNGSUB END
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, NEWLINE,new ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Soc_Sec, new ReportEditMask ("999-99-9999"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,  //Natural: WRITE ( 2 ) / 2X AP-SOC-SEC ( EM = 999-99-9999 ) 7X AP-TIAA-CNTRCT ( EM = XXXXXXX-X ) 7X AP-CREF-CERT ( EM = XXXXXXX-X ) 8X AP-SGRD-PLAN-NO 11X AP-SGRD-SUBPLAN-NO 7X #EXCEPT-REASON
            new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Cref_Cert, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(8),annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,new 
            ColumnSpacing(11),annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,new ColumnSpacing(7),pnd_Except_Reason);
        if (Global.isEscape()) return;
        pnd_Exc_Line_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #EXC-LINE-CNT
        pdaScia8100.getDoc_Rec_Doc_Return_Msg_Module().reset();                                                                                                           //Natural: RESET DOC-RETURN-MSG-MODULE PPT-RETURN-MSG-MODULE
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().reset();
        //*  EXCEPTION-REPORT /* SGRD KG
    }
    //*  SGRD KG
    private void sub_Exception_Report_Write_Header() throws Exception                                                                                                     //Natural: EXCEPTION-REPORT-WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        pnd_Exc_Line_Cnt.reset();                                                                                                                                         //Natural: RESET #EXC-LINE-CNT
        if (condition(pnd_Input_Parm.equals("LEGAL")))                                                                                                                    //Natural: IF #INPUT-PARM = 'LEGAL'
        {
            pnd_Exc_Report_Type.setValue("  LEGAL");                                                                                                                      //Natural: MOVE '  LEGAL' TO #EXC-REPORT-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Exc_Report_Type.setValue(pnd_Input_Parm);                                                                                                                 //Natural: MOVE #INPUT-PARM TO #EXC-REPORT-TYPE
            //*  TITLE LEFT JUSTIFIED
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. DOCUMERGE DAILY EXCEPTION REPORT",new ColumnSpacing(16),Global.getDATU());               //Natural: WRITE ( 2 ) *PROGRAM 5X 'A.C.I.S. DOCUMERGE DAILY EXCEPTION REPORT' 16X *DATU
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(25),pnd_Exc_Report_Type,"PACKAGES",new ColumnSpacing(29),Global.getTIMX(),NEWLINE,NEWLINE);                               //Natural: WRITE ( 2 ) 25X #EXC-REPORT-TYPE 'PACKAGES' 29X *TIMX //
        if (Global.isEscape()) return;
        getReports().write(2, "SOCIAL SECURITY",new ColumnSpacing(3),"TIAA CONTRACT",new ColumnSpacing(3),"CREF CONTRACT",new ColumnSpacing(3),"SUNGARD PLAN",new         //Natural: WRITE ( 2 ) 'SOCIAL SECURITY' 3X 'TIAA CONTRACT' 3X 'CREF CONTRACT' 3X 'SUNGARD PLAN' 3X 'SUNGARD SUBPLAN' 3X 'EXCEPTION'/
            ColumnSpacing(3),"SUNGARD SUBPLAN",new ColumnSpacing(3),"EXCEPTION",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(4),"NUMBER",new ColumnSpacing(11),"NUMBER",new ColumnSpacing(10),"NUMBER",new ColumnSpacing(10),"NUMBER",new              //Natural: WRITE ( 2 ) 4X 'NUMBER' 11X 'NUMBER' 10X 'NUMBER' 10X 'NUMBER' 11X 'NUMBER' 7X 'REASON' /
            ColumnSpacing(11),"NUMBER",new ColumnSpacing(7),"REASON",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, "---------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"------------",new         //Natural: WRITE ( 2 ) '---------------' 3X '-------------' 3X '-------------' 3X '------------' 3X '---------------' 3X '------------------------------------------'/
            ColumnSpacing(3),"---------------",new ColumnSpacing(3),"------------------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Exc_Line_Cnt.nadd(5);                                                                                                                                         //Natural: ADD 5 TO #EXC-LINE-CNT
        //*  EXCEPTION-REPORT-WRITE-HEADER /* SGRD KG
    }
    //*  KG TNGSUB UPDATED
    private void sub_Determine_Single_Issue() throws Exception                                                                                                            //Natural: DETERMINE-SINGLE-ISSUE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Nbr.setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                                                                   //Natural: ASSIGN #CNTRCT-NBR := AP-TIAA-CNTRCT
        if (condition(annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero())))                                                                                                 //Natural: IF AP-T-DOI = 0
        {
            vw_reprint_Fl.startDatabaseFind                                                                                                                               //Natural: FIND REPRINT-FL WITH RP-TIAA-CONTR = #CNTRCT-NBR
            (
            "FIND01",
            new Wc[] { new Wc("RP_TIAA_CONTR", "=", pnd_Cntrct_Nbr, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_reprint_Fl.readNextRow("FIND01", true)))
            {
                vw_reprint_Fl.setIfNotFoundControlFlag(false);
                if (condition(vw_reprint_Fl.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORDS FOUND
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind().setValue("Y");                                                                                       //Natural: ASSIGN DX-SINGLE-ISSUE-IND := 'Y'
                    ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Prfx().getValue(1).setValue(" ");                                                                                  //Natural: MOVE ' ' TO DX-TIAA-PRFX ( 1 ) DX-TIAA-NO ( 1 )
                    ldaAppl160.getDoc_Ext_File_Dx_Tiaa_No().getValue(1).setValue(" ");
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(reprint_Fl_Rp_T_Doi.notEquals(getZero())))                                                                                                  //Natural: IF RP-T-DOI NE 0
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind().setValue("N");                                                                                       //Natural: ASSIGN DX-SINGLE-ISSUE-IND := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Nbr.setValue(annty_Actvty_Prap_View_Ap_Cref_Cert);                                                                                                     //Natural: ASSIGN #CNTRCT-NBR := AP-CREF-CERT
        if (condition(annty_Actvty_Prap_View_Ap_C_Doi.equals(getZero())))                                                                                                 //Natural: IF AP-C-DOI = 0
        {
            vw_reprint_Fl.startDatabaseFind                                                                                                                               //Natural: FIND REPRINT-FL WITH RP-CREF-CONTR = #CNTRCT-NBR
            (
            "FIND02",
            new Wc[] { new Wc("RP_CREF_CONTR", "=", pnd_Cntrct_Nbr, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_reprint_Fl.readNextRow("FIND02", true)))
            {
                vw_reprint_Fl.setIfNotFoundControlFlag(false);
                if (condition(vw_reprint_Fl.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORDS FOUND
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind().setValue("Y");                                                                                       //Natural: ASSIGN DX-SINGLE-ISSUE-IND := 'Y'
                    ldaAppl160.getDoc_Ext_File_Dx_Cref_Prfx().getValue(1).setValue(" ");                                                                                  //Natural: MOVE ' ' TO DX-CREF-PRFX ( 1 ) DX-CREF-NO ( 1 )
                    ldaAppl160.getDoc_Ext_File_Dx_Cref_No().getValue(1).setValue(" ");
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(reprint_Fl_Rp_C_Doi.notEquals(getZero())))                                                                                                  //Natural: IF RP-C-DOI NE 0
                {
                    ldaAppl160.getDoc_Ext_File_Dx_Single_Issue_Ind().setValue("N");                                                                                       //Natural: ASSIGN DX-SINGLE-ISSUE-IND := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-SINGLE-ISSUE
    }
    //*  NEW FOR SGRD7 KG
    private void sub_Check_Deflt_Enroll() throws Exception                                                                                                                //Natural: CHECK-DEFLT-ENROLL
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        pnd_Search_Key.reset();                                                                                                                                           //Natural: RESET #SEARCH-KEY
        pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind.setValue("Y");                                                                                                              //Natural: ASSIGN #KY-ENTRY-ACTVE-IND := 'Y'
        pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr.setValue(310);                                                                                                           //Natural: ASSIGN #KY-ENTRY-TABLE-ID-NBR := 310
        pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id.setValue("PV");                                                                                                          //Natural: ASSIGN #KY-ENTRY-TABLE-SUB-ID := 'PV'
        pnd_Search_Key_Pnd_Ky_Plan_Number.setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                               //Natural: ASSIGN #KY-PLAN-NUMBER := AP-SGRD-PLAN-NO
        pnd_Search_Key_Pnd_Ky_Subplan_Number.setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                                         //Natural: ASSIGN #KY-SUBPLAN-NUMBER := AP-SGRD-SUBPLAN-NO
        vw_table_Entry.startDatabaseFind                                                                                                                                  //Natural: FIND TABLE-ENTRY WITH ACTVE-IND-TABLE-ID-ENTRY-CDE = #SEARCH-KEY
        (
        "FIND03",
        new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "=", pnd_Search_Key, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_table_Entry.readNextRow("FIND03", true)))
        {
            vw_table_Entry.setIfNotFoundControlFlag(false);
            if (condition(vw_table_Entry.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO RECORD FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            ldaAppl160.getDoc_Ext_File_Dx_Access_Cd_Ind().setValue("Y");                                                                                                  //Natural: MOVE 'Y' TO DX-ACCESS-CD-IND
            ldaAppl160.getDoc_Ext_File_Dx_Dflt_Access_Code().setValue(table_Entry_Entry_Oln_Access);                                                                      //Natural: MOVE ENTRY-OLN-ACCESS TO DX-DFLT-ACCESS-CODE
            getReports().write(0, "=",table_Entry_Entry_Oln_Access,"=",ldaAppl160.getDoc_Ext_File_Dx_Dflt_Access_Code());                                                 //Natural: WRITE '=' ENTRY-OLN-ACCESS '=' DX-DFLT-ACCESS-CODE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CHECK-DEFLT-ENROLL
    }
    //*  TNGSUB2 START LS
    private void sub_Load_Trigger_Suppress_Table() throws Exception                                                                                                       //Natural: LOAD-TRIGGER-SUPPRESS-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        pnd_Search_Key.reset();                                                                                                                                           //Natural: RESET #SEARCH-KEY #I-T #I-S
        pnd_I_T.reset();
        pnd_I_S.reset();
        pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind.setValue("Y");                                                                                                              //Natural: ASSIGN #KY-ENTRY-ACTVE-IND := 'Y'
        pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr.setValue(310);                                                                                                           //Natural: ASSIGN #KY-ENTRY-TABLE-ID-NBR := 310
        pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id.setValue("PS");                                                                                                          //Natural: ASSIGN #KY-ENTRY-TABLE-SUB-ID := 'PS'
        vw_table_Entry.startDatabaseRead                                                                                                                                  //Natural: READ TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE STARTING FROM #SEARCH-KEY
        (
        "READ03",
        new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Search_Key, WcType.BY) },
        new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ03:
        while (condition(vw_table_Entry.readNextRow("READ03")))
        {
            if (condition(table_Entry_Entry_Actve_Ind.notEquals("Y") || table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr)                //Natural: IF ENTRY-ACTVE-IND NE 'Y' OR ENTRY-TABLE-ID-NBR NE #KY-ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #KY-ENTRY-TABLE-SUB-ID
                || table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I_T.greater(300)))                                                                                                                          //Natural: IF #I-T GT 300
            {
                getReports().write(0, "***!!! IN APPB100 - MAXIMUM NUMBER OF 300 FOR PACKAGE !!!***",NEWLINE,"***!!!    TRIGGER TABLE HAS REACHED                   !!!***"); //Natural: WRITE '***!!! IN APPB100 - MAXIMUM NUMBER OF 300 FOR PACKAGE !!!***' / '***!!!    TRIGGER TABLE HAS REACHED                   !!!***'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I_S.greater(300)))                                                                                                                          //Natural: IF #I-S GT 300
            {
                getReports().write(0, "***!!! IN APPB100 - MAXIMUM NUMBER OF 300 FOR PACKAGE !!!***",NEWLINE,"***!!!    SUPPRESS TABLE HAS REACHED                  !!!***"); //Natural: WRITE '***!!! IN APPB100 - MAXIMUM NUMBER OF 300 FOR PACKAGE !!!***' / '***!!!    SUPPRESS TABLE HAS REACHED                  !!!***'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet5061 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF ENTRY-PKG-CNTRL-TYPE;//Natural: VALUE 'T'
            if (condition((table_Entry_Entry_Pkg_Cntrl_Type.equals("T"))))
            {
                decideConditionsMet5061++;
                pnd_I_T.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #I-T
                pnd_Trigger_Table_Pnd_T_Plan_Number.getValue(pnd_I_T).setValue(table_Entry_Entry_Plan);                                                                   //Natural: ASSIGN #T-PLAN-NUMBER ( #I-T ) := ENTRY-PLAN
                pnd_Trigger_Table_Pnd_T_Subplan_Number.getValue(pnd_I_T).setValue(table_Entry_Entry_Sub_Plan_Number);                                                     //Natural: ASSIGN #T-SUBPLAN-NUMBER ( #I-T ) := ENTRY-SUB-PLAN-NUMBER
                pnd_Trigger_Table_Pnd_T_Pkg_Cntrl_Type.getValue(pnd_I_T).setValue(table_Entry_Entry_Pkg_Cntrl_Type);                                                      //Natural: ASSIGN #T-PKG-CNTRL-TYPE ( #I-T ) := ENTRY-PKG-CNTRL-TYPE
                pnd_Trigger_Table_Pnd_T_Pkg_Trigger.getValue(pnd_I_T).setValue(table_Entry_Entry_Pkg_Trigger);                                                            //Natural: ASSIGN #T-PKG-TRIGGER ( #I-T ) := ENTRY-PKG-TRIGGER
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((table_Entry_Entry_Pkg_Cntrl_Type.equals("S"))))
            {
                decideConditionsMet5061++;
                pnd_I_S.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #I-S
                pnd_Suppress_Table_Pnd_S_Plan_Number.getValue(pnd_I_S).setValue(table_Entry_Entry_Plan);                                                                  //Natural: ASSIGN #S-PLAN-NUMBER ( #I-S ) := ENTRY-PLAN
                pnd_Suppress_Table_Pnd_S_Subplan_Number.getValue(pnd_I_S).setValue(table_Entry_Entry_Sub_Plan_Number);                                                    //Natural: ASSIGN #S-SUBPLAN-NUMBER ( #I-S ) := ENTRY-SUB-PLAN-NUMBER
                pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type.getValue(pnd_I_S).setValue(table_Entry_Entry_Pkg_Cntrl_Type);                                                     //Natural: ASSIGN #S-PKG-CNTRL-TYPE ( #I-S ) := ENTRY-PKG-CNTRL-TYPE
                pnd_Suppress_Table_Pnd_S_Enr_Source.getValue(pnd_I_S).setValue(table_Entry_Entry_Enr_Source);                                                             //Natural: ASSIGN #S-ENR-SOURCE ( #I-S ) := ENTRY-ENR-SOURCE
                pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(pnd_I_S).setValue(table_Entry_Entry_Pckg_Type);                                                               //Natural: ASSIGN #S-PCKG-TYPE ( #I-S ) := ENTRY-PCKG-TYPE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  LOAD-TRIGGER-SUPPRESS-TABLE             TNGSUB2 END LS
    }
    //*  NEW FOR SGRD7 KG  /* TNGSUB2 LS
    private void sub_Check_Pkg_Suppress() throws Exception                                                                                                                //Natural: CHECK-PKG-SUPPRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        pnd_Welc_Fund_Trigger.reset();                                                                                                                                    //Natural: RESET #WELC-FUND-TRIGGER
        pnd_Welc_Force_Inst_Owned.reset();                                                                                                                                //Natural: RESET #WELC-FORCE-INST-OWNED
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO #I-T
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_I_T)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(pnd_Trigger_Table_Pnd_T_Plan_Number.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No)              //Natural: IF #T-PLAN-NUMBER ( #I ) = AP-SGRD-PLAN-NO AND #T-SUBPLAN-NUMBER ( #I ) = AP-SGRD-SUBPLAN-NO AND #T-PKG-CNTRL-TYPE ( #I ) = 'T'
                && pnd_Trigger_Table_Pnd_T_Subplan_Number.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No) 
                && pnd_Trigger_Table_Pnd_T_Pkg_Cntrl_Type.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("T")))
            {
                short decideConditionsMet5089 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #T-PKG-TRIGGER ( #I );//Natural: VALUE 'F', 'H'
                if (condition((pnd_Trigger_Table_Pnd_T_Pkg_Trigger.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("F") || pnd_Trigger_Table_Pnd_T_Pkg_Trigger.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("H"))))
                {
                    decideConditionsMet5089++;
                    pnd_Welc_Fund_Trigger.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #WELC-FUND-TRIGGER
                }                                                                                                                                                         //Natural: VALUE 'G'
                else if (condition((pnd_Trigger_Table_Pnd_T_Pkg_Trigger.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("G"))))
                {
                    decideConditionsMet5089++;
                    pnd_Welc_Force_Inst_Owned.setValue(true);                                                                                                             //Natural: MOVE TRUE TO #WELC-FORCE-INST-OWNED
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Suppress_Welc_Pkg.reset();                                                                                                                                    //Natural: RESET #SUPPRESS-WELC-PKG #SUPPRESS-LEGAL-PKG
        pnd_Suppress_Legal_Pkg.reset();
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO #I-S
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_I_S)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            //*  BWN
            //*  BWN
            //*  BWN
            if (condition(pnd_Suppress_Table_Pnd_S_Plan_Number.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No)             //Natural: IF #S-PLAN-NUMBER ( #I ) = AP-SGRD-PLAN-NO AND #S-SUBPLAN-NUMBER ( #I ) = AP-SGRD-SUBPLAN-NO AND #S-PKG-CNTRL-TYPE ( #I ) = 'S' AND ( #S-ENR-SOURCE ( #I ) = ' ' OR ( #S-ENR-SOURCE ( #I ) <> ' ' AND #S-ENR-SOURCE ( #I ) = AP-APP-SOURCE ) )
                && pnd_Suppress_Table_Pnd_S_Subplan_Number.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No) 
                && pnd_Suppress_Table_Pnd_S_Pkg_Cntrl_Type.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("S") && (pnd_Suppress_Table_Pnd_S_Enr_Source.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" ") 
                || (pnd_Suppress_Table_Pnd_S_Enr_Source.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).notEquals(" ") && pnd_Suppress_Table_Pnd_S_Enr_Source.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(annty_Actvty_Prap_View_Ap_App_Source)))))
            {
                //*  BWN
                short decideConditionsMet5108 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #S-PCKG-TYPE ( #I ) = 'W'
                if (condition(pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("W")))
                {
                    decideConditionsMet5108++;
                    //*  BWN
                    pnd_Suppress_Welc_Pkg.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #SUPPRESS-WELC-PKG
                }                                                                                                                                                         //Natural: WHEN #S-PCKG-TYPE ( #I ) = 'L'
                else if (condition(pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("L")))
                {
                    decideConditionsMet5108++;
                    //*  BWN
                    pnd_Suppress_Legal_Pkg.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #SUPPRESS-LEGAL-PKG
                }                                                                                                                                                         //Natural: WHEN #S-PCKG-TYPE ( #I ) = 'B'
                else if (condition(pnd_Suppress_Table_Pnd_S_Pckg_Type.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("B")))
                {
                    decideConditionsMet5108++;
                    pnd_Suppress_Welc_Pkg.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #SUPPRESS-WELC-PKG
                    pnd_Suppress_Legal_Pkg.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #SUPPRESS-LEGAL-PKG
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CHECK-PKG-SUPPRESS
    }
    //*  BWN
    private void sub_State_Conversions() throws Exception                                                                                                                 //Natural: STATE-CONVERSIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*   BECAUSE THE STATE CODE TABLE IS USED IN OTHER PROGRAMS AND THE
        //*   VALUES BELOW EXIST AND STILL COME TO CIS THE CODE HERE
        //*   ACCOUNTS FOR WHAT THE PACKAGE RULES EXPECTS IN STATE CODES.
        //*   ANYTHING NOT IN THE US IS TO BE 'FN'.  AND A NUMBER WITH BLANK
        //*   WILL BE TRANSLATED TO THE CODE THE TABLE CAN UNDERSTAND WITH THE
        //*   LEADING ZERO.
        //* *--------------------------------------------------------------------**
        //*  MILITARY
        short decideConditionsMet5137 = 0;                                                                                                                                //Natural: DECIDE ON FIRST #W-CHECK-STATE;//Natural: VALUES 'AE', '81'
        if (condition((pnd_W_Check_State.equals("AE") || pnd_W_Check_State.equals("81"))))
        {
            decideConditionsMet5137++;
            //*  NY MILITARY BASE
            pnd_W_Check_State.setValue("35");                                                                                                                             //Natural: MOVE '35' TO #W-CHECK-STATE
        }                                                                                                                                                                 //Natural: VALUES 'AP', '82'
        else if (condition((pnd_W_Check_State.equals("AP") || pnd_W_Check_State.equals("82"))))
        {
            decideConditionsMet5137++;
            //*  MILITARY
            pnd_W_Check_State.setValue("05");                                                                                                                             //Natural: MOVE '05' TO #W-CHECK-STATE
        }                                                                                                                                                                 //Natural: VALUES 'AA', '80'
        else if (condition((pnd_W_Check_State.equals("AA") || pnd_W_Check_State.equals("80"))))
        {
            decideConditionsMet5137++;
            pnd_W_Check_State.setValue("11");                                                                                                                             //Natural: MOVE '11' TO #W-CHECK-STATE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
            //*  BWN
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  STATE-CONVERSIONS
    }
    //*  CAMPUS - 8-2011 GET COUNTRY NAME
    private void sub_Check_Country_Code() throws Exception                                                                                                                //Natural: CHECK-COUNTRY-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        if (condition(annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd.notEquals(" ")))                                                                                     //Natural: IF AP-MAIL-ADDR-COUNTRY-CD NE ' '
        {
            pnd_Cal_Country_Name.reset();                                                                                                                                 //Natural: RESET #CAL-COUNTRY-NAME #CAL-RETURN-CODE #CAL-RETURN-MSG
            pnd_Cal_Return_Code.reset();
            pnd_Cal_Return_Msg.reset();
            pnd_Cal_Country_Cd.setValue(annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd);                                                                                  //Natural: ASSIGN #CAL-COUNTRY-CD := AP-MAIL-ADDR-COUNTRY-CD
            DbsUtil.callnat(Scin3050.class , getCurrentProcessState(), pnd_Cal_Country_Cd, pnd_Cal_Country_Name, pnd_Cal_Return_Code, pnd_Cal_Return_Msg);                //Natural: CALLNAT 'SCIN3050' #CAL-COUNTRY-CD #CAL-COUNTRY-NAME #CAL-RETURN-CODE #CAL-RETURN-MSG
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Cal_Return_Code.equals("0")))                                                                                                               //Natural: IF #CAL-RETURN-CODE = '0'
            {
                //*  SRK LS3
                DbsUtil.examine(new ExamineSource(pnd_Cal_Country_Name), new ExamineTranslate(TranslateOption.Upper));                                                    //Natural: EXAMINE #CAL-COUNTRY-NAME AND TRANSLATE INTO UPPER CASE
                //*   START
                if (condition(annty_Actvty_Prap_View_Ap_Address_Line.getValue(3).equals(" ")))                                                                            //Natural: IF AP-ADDRESS-LINE ( 3 ) = ' '
                {
                    annty_Actvty_Prap_View_Ap_Address_Line.getValue(3).setValue(pnd_Cal_Country_Name);                                                                    //Natural: MOVE #CAL-COUNTRY-NAME TO AP-ADDRESS-LINE ( 3 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(annty_Actvty_Prap_View_Ap_Address_Line.getValue(4).equals(" ")))                                                                        //Natural: IF AP-ADDRESS-LINE ( 4 ) = ' '
                    {
                        annty_Actvty_Prap_View_Ap_Address_Line.getValue(4).setValue(pnd_Cal_Country_Name);                                                                //Natural: MOVE #CAL-COUNTRY-NAME TO AP-ADDRESS-LINE ( 4 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(annty_Actvty_Prap_View_Ap_Address_Line.getValue(5).equals(" ")))                                                                    //Natural: IF AP-ADDRESS-LINE ( 5 ) = ' '
                        {
                            annty_Actvty_Prap_View_Ap_Address_Line.getValue(5).setValue(pnd_Cal_Country_Name);                                                            //Natural: MOVE #CAL-COUNTRY-NAME TO AP-ADDRESS-LINE ( 5 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, "NO ADDRESS LINE AVAILABLE FOR COUNTRY NAME");                                                                          //Natural: WRITE 'NO ADDRESS LINE AVAILABLE FOR COUNTRY NAME'
                            if (Global.isEscape()) return;
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SRK LS3 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, " BAD RETURN CODE FROM COUNTRY CODE TABLE:",pnd_Cal_Country_Cd);                                                                    //Natural: WRITE ' BAD RETURN CODE FROM COUNTRY CODE TABLE:' #CAL-COUNTRY-CD
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-COUNTRY-CODE
    }
    private void sub_Determine_Form_Num() throws Exception                                                                                                                //Natural: DETERMINE-FORM-NUM
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("E");                                                                                                       //Natural: MOVE 'E' TO DX-TIAA-FORM-NO
        ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("E");                                                                                                       //Natural: MOVE 'E' TO DX-CREF-FORM-NO
        short decideConditionsMet5189 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #CONTRACT-TYPE;//Natural: VALUE 'GSRA'
        if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GSRA"))))
        {
            decideConditionsMet5189++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("E");                                                                                                   //Natural: MOVE 'E' TO DX-TIAA-FORM-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("E");                                                                                                   //Natural: MOVE 'E' TO DX-CREF-FORM-NO
            //*  457(B) KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'GA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GA"))))
        {
            decideConditionsMet5189++;
            //*  457(B) KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("E");                                                                                                   //Natural: MOVE 'E' TO DX-TIAA-FORM-NO
            //*  457(B) KG
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("E");                                                                                                   //Natural: MOVE 'E' TO DX-CREF-FORM-NO
            //*  457(B) KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'GRA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GRA"))))
        {
            decideConditionsMet5189++;
            if (condition(annty_Actvty_Prap_View_Ap_Current_State_Code.equals("54")))                                                                                     //Natural: IF ANNTY-ACTVTY-PRAP-VIEW.AP-CURRENT-STATE-CODE = '54'
            {
                //*  CHG PER MEMO OF 3/2/93
                ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("G1000.4A");                                                                                        //Natural: MOVE 'G1000.4A' TO DX-TIAA-FORM-NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ALL OTHER STATES
                ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("G1000.4");                                                                                         //Natural: MOVE 'G1000.4' TO DX-TIAA-FORM-NO
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("CG1000.1");                                                                                            //Natural: MOVE 'CG1000.1' TO DX-CREF-FORM-NO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRA"))))
        {
            decideConditionsMet5189++;
            FOR08:                                                                                                                                                        //Natural: FOR #T 1 62
            for (ldaAppl170.getPnd_Work_Fields_Pnd_T().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_T().lessOrEqual(62)); ldaAppl170.getPnd_Work_Fields_Pnd_T().nadd(1))
            {
                if (condition(annty_Actvty_Prap_View_Ap_Current_State_Code.equals(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T())))) //Natural: IF ANNTY-ACTVTY-PRAP-VIEW.AP-CURRENT-STATE-CODE = #STATE-CODE-N ( #T )
                {
                    if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_Ira_T_Msg_Typ().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T()).equals("1")))                    //Natural: IF #IRA-T-MSG-TYP ( #T ) = '1'
                    {
                        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("1280");                                                                                    //Natural: MOVE '1280' TO DX-TIAA-FORM-NO
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_Ira_T_Msg_Typ().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T()).equals("2")))                    //Natural: IF #IRA-T-MSG-TYP ( #T ) = '2'
                    {
                        ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("1280A");                                                                                   //Natural: MOVE '1280A' TO DX-TIAA-FORM-NO
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_Ira_C_Msg_Typ().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T()).equals("1")))                    //Natural: IF #IRA-C-MSG-TYP ( #T ) = '1'
                    {
                        ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("C1280");                                                                                   //Natural: MOVE 'C1280' TO DX-CREF-FORM-NO
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_Ira_C_Msg_Typ().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_T()).equals("N")))                    //Natural: IF #IRA-C-MSG-TYP ( #T ) = 'N'
                    {
                        ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("C1280A");                                                                                  //Natural: MOVE 'C1280A' TO DX-CREF-FORM-NO
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'RL'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RL"))))
        {
            decideConditionsMet5189++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("G1301");                                                                                               //Natural: MOVE 'G1301' TO DX-TIAA-FORM-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO DX-CREF-FORM-NO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRAC'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRAC"))))
        {
            decideConditionsMet5189++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("1280.2");                                                                                              //Natural: MOVE '1280.2' TO DX-TIAA-FORM-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("C1280.2");                                                                                             //Natural: MOVE 'C1280.2' TO DX-CREF-FORM-NO
            //*  SGRD6 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRAS'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRAS"))))
        {
            decideConditionsMet5189++;
            //*  SGRD6 KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("1280.2");                                                                                              //Natural: MOVE '1280.2' TO DX-TIAA-FORM-NO
            //*  SGRD6 KG
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("C1280.2");                                                                                             //Natural: MOVE 'C1280.2' TO DX-CREF-FORM-NO
            //*  SGRD6 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRAR'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRAR"))))
        {
            decideConditionsMet5189++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("1280.3");                                                                                              //Natural: MOVE '1280.3' TO DX-TIAA-FORM-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("C1280.3");                                                                                             //Natural: MOVE 'C1280.3' TO DX-CREF-FORM-NO
            //*  LS1
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'KEOG'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("KEOG"))))
        {
            decideConditionsMet5189++;
            //*  LS1
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("G1350");                                                                                               //Natural: MOVE 'G1350' TO DX-TIAA-FORM-NO
            //*  LS1
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("CG1350");                                                                                              //Natural: MOVE 'CG1350' TO DX-CREF-FORM-NO
            //*  LS1
            //*  SGRD4 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'RS'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RS"))))
        {
            decideConditionsMet5189++;
            //*  SGRD4 KG
            //*  SGRD4 KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("GRS-01");                                                                                              //Natural: MOVE 'GRS-01' TO DX-TIAA-FORM-NO DX-TIAA-PG4-NUMB
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Pg4_Numb().setValue("GRS-01");
            //*  SGRD4 KG
            //*  SGRD4 KG
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("CGRS-01");                                                                                             //Natural: MOVE 'CGRS-01' TO DX-CREF-FORM-NO DX-CREF-PG4-NUMB
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Pg4_Numb().setValue("CGRS-01");
            //*  SGRD4 KG
            //*  SGRD4 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'RSP'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RSP"))))
        {
            decideConditionsMet5189++;
            //*  SGRD4 KG
            //*  SGRD4 KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("GRSP-01");                                                                                             //Natural: MOVE 'GRSP-01' TO DX-TIAA-FORM-NO DX-TIAA-PG4-NUMB
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Pg4_Numb().setValue("GRSP-01");
            //*  SGRD4 KG
            //*  SGRD4 KG
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("CGRSP-01");                                                                                            //Natural: MOVE 'CGRSP-01' TO DX-CREF-FORM-NO DX-CREF-PG4-NUMB
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Pg4_Numb().setValue("CGRSP-01");
            //*  SGRD4 KG
            //*  SGRD4 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'RSP2'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RSP2"))))
        {
            decideConditionsMet5189++;
            //*  SGRD4 KG
            //*  SGRD4 KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("GRSP-NE-01");                                                                                          //Natural: MOVE 'GRSP-NE-01' TO DX-TIAA-FORM-NO DX-TIAA-PG4-NUMB
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Pg4_Numb().setValue("GRSP-NE-01");
            //*  SGRD4 KG
            //*  SGRD4 KG
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("CGRSP-NE-01");                                                                                         //Natural: MOVE 'CGRSP-NE-01' TO DX-CREF-FORM-NO DX-CREF-PG4-NUMB
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Pg4_Numb().setValue("CGRSP-NE-01");
            //*  SGRD4 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Lob().setValue(annty_Actvty_Prap_View_Ap_Lob);                                                                   //Natural: MOVE ANNTY-ACTVTY-PRAP-VIEW.AP-LOB TO #PAR-GRM-LOB
        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Return_Cd().resetInitial();                                                                                      //Natural: RESET INITIAL #PAR-GRM-RETURN-CD
        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Tiaa_Form_Num().resetInitial();                                                                                  //Natural: RESET INITIAL #PAR-GRM-TIAA-FORM-NUM
        pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Cref_Form_Num().resetInitial();                                                                                  //Natural: RESET INITIAL #PAR-GRM-CREF-FORM-NUM
        //*  GRM FILE LOOKUP
        DbsUtil.callnat(Appn210.class , getCurrentProcessState(), pdaAppa100.getPnd_Par_Parameter_Area());                                                                //Natural: CALLNAT 'APPN210' #PAR-PARAMETER-AREA
        if (condition(Global.isEscape())) return;
        if (condition(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Return_Cd().equals(1)))                                                                            //Natural: IF #PAR-GRM-RETURN-CD = 1
        {
            if (condition(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Tiaa_Form_Num().equals(" ")))                                                                  //Natural: IF #PAR-GRM-TIAA-FORM-NUM = ' '
            {
                ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue("E");                                                                                               //Natural: MOVE 'E' TO DX-TIAA-FORM-NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Form_No().setValue(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Tiaa_Form_Num());                                  //Natural: MOVE #PAR-GRM-TIAA-FORM-NUM TO DX-TIAA-FORM-NO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Cref_Form_Num().equals(" ")))                                                                  //Natural: IF #PAR-GRM-CREF-FORM-NUM = ' '
            {
                ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue("E");                                                                                               //Natural: MOVE 'E' TO DX-CREF-FORM-NO
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl160.getDoc_Ext_File_Dx_Cref_Form_No().setValue(pdaAppa100.getPnd_Par_Parameter_Area_Pnd_Par_Grm_Cref_Form_Num());                                  //Natural: MOVE #PAR-GRM-CREF-FORM-NUM TO DX-CREF-FORM-NO
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Ed_Num() throws Exception                                                                                                                  //Natural: DETERMINE-ED-NUM
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        short decideConditionsMet5315 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #CONTRACT-TYPE;//Natural: VALUE 'GSRA'
        if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GSRA"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("E");                                                                                                     //Natural: MOVE 'E' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("E");                                                                                                     //Natural: MOVE 'E' TO DX-CREF-ED-NO
            //*  457(B) KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'GA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GA"))))
        {
            decideConditionsMet5315++;
            //*  457(B) KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("E");                                                                                                     //Natural: MOVE 'E' TO DX-TIAA-ED-NO
            //*  457(B) KG
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("E");                                                                                                     //Natural: MOVE 'E' TO DX-CREF-ED-NO
            //*  457(B) KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'RA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RA"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED. 1-86");                                                                                              //Natural: MOVE 'ED. 1-86' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED. 1-85");                                                                                              //Natural: MOVE 'ED. 1-85' TO DX-CREF-ED-NO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'SRA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("SRA"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED.11-92");                                                                                              //Natural: MOVE 'ED.11-92' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED.11-92");                                                                                              //Natural: MOVE 'ED.11-92' TO DX-CREF-ED-NO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'GRA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("GRA"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED. 6-90");                                                                                              //Natural: MOVE 'ED. 6-90' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED. 6-90");                                                                                              //Natural: MOVE 'ED. 6-90' TO DX-CREF-ED-NO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRA'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRA"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED. 4-92");                                                                                              //Natural: MOVE 'ED. 4-92' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED. 4-92");                                                                                              //Natural: MOVE 'ED. 4-92' TO DX-CREF-ED-NO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRAC'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRAC"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED. 1-98");                                                                                              //Natural: MOVE 'ED. 1-98' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED. 1-98");                                                                                              //Natural: MOVE 'ED. 1-98' TO DX-CREF-ED-NO
            //*  SGRD6 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRAS'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRAS"))))
        {
            decideConditionsMet5315++;
            //*  SGRD6 KG
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED. 1-98");                                                                                              //Natural: MOVE 'ED. 1-98' TO DX-TIAA-ED-NO
            //*  SGRD6 KG
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED. 1-98");                                                                                              //Natural: MOVE 'ED. 1-98' TO DX-CREF-ED-NO
            //*  SGRD6 KG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'IRAR'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("IRAR"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED. 1-98");                                                                                              //Natural: MOVE 'ED. 1-98' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED. 1-98");                                                                                              //Natural: MOVE 'ED. 1-98' TO DX-CREF-ED-NO
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'RL'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("RL"))))
        {
            decideConditionsMet5315++;
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED.10-93");                                                                                              //Natural: MOVE 'ED.10-93' TO DX-TIAA-ED-NO
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO DX-CREF-ED-NO
            //*  LS1
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'KEOG'
        else if (condition((ldaAppl170.getPnd_Work_Fields_Pnd_Contract_Type().equals("KEOG"))))
        {
            decideConditionsMet5315++;
            //*  LS1
            ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Ed_No().setValue("ED. 7-99");                                                                                              //Natural: MOVE 'ED. 7-99' TO DX-TIAA-ED-NO
            //*  LS1
            ldaAppl160.getDoc_Ext_File_Dx_Cref_Ed_No().setValue("ED. 7-99");                                                                                              //Natural: MOVE 'ED. 7-99' TO DX-CREF-ED-NO
            //*  LS1
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  SGRD KG
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT-WRITE-HEADER
                    sub_Exception_Report_Write_Header();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  SGRD KG
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  JRB2
        //*  JRB2
        //*  JRB2
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE // 'CONTRACT:' 25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),annty_Actvty_Prap_View_Ap_Pin_Nbr);
        //*  JRB2
        if (condition(Global.getERROR_NR().equals(3052)))                                                                                                                 //Natural: IF *ERROR-NR = 3052
        {
            //*  JRB2
            //*  JRB2
            getReports().write(0, "BIRTHDATE:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Dob,NEWLINE,"ANNUITY START DATE:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Annuity_Start_Date); //Natural: WRITE 'BIRTHDATE:' 25T AP-DOB / 'ANNUITY START DATE:' 25T AP-ANNUITY-START-DATE
            //*  JRB2
        }                                                                                                                                                                 //Natural: END-IF
        //*  JRB2
        getReports().write(0, "EXTRACT RECORDS WRITTEN:",ldaAppl170.getPnd_Write_Count(), new ReportEditMask ("ZZZ,ZZ9"));                                                //Natural: WRITE 'EXTRACT RECORDS WRITTEN:' #WRITE-COUNT ( EM = ZZZ,ZZ9 )
        //*  JRB2
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=52");

        getReports().setDisplayColumns(0, "********  NO PRAP RECORDS FOUND  ********");
    }
}
