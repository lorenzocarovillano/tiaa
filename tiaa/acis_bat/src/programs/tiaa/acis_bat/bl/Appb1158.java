/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:07 PM
**        * FROM NATURAL PROGRAM : Appb1158
************************************************************
**        * FILE NAME            : Appb1158.java
**        * CLASS NAME           : Appb1158
**        * INSTANCE NAME        : Appb1158
************************************************************
************************************************************************
* PROGRAM ID:     APPB1158
* AUTHOR:         CHRISTOPHER LAEVEY
* DATE CREATED:   7/18/2013
* BUSINESS GROUP: ACIS
* FUNCTION:       ADDS THE BUNDLING INFORMATION TO THE PACKAGE EXTRACT
*                 FILE.
*
* INPUT:  &HLQ8.REPREXTR.DIAG.SORTWHDR
* OUTPUT: &HLQ8.REPREXTR.DIAG.FRMTBNDL
*
* CHANGE HISTORY:   (MOST RECENT AT BOTTOM)
* IMPL
* DATE      CHANGED                                              CHANGE
* MM/DD/YY     BY    CHANGE DESCRIPTION                          TAG
**--------  -------- ------------------------------------------  ------
* 07/18/13  L SHU    POPULATE 3% ORIG TIAA NUMBER IN THE COVER   TNGSUB
*                    LETTER ARRAY FOR IRA SUBSTITUTION CONTRACT
* 09/13/13  L SHU    ADDING NON PROPRIETARY PKG IND IN HOLD FILE MTSIN
* 09/12/14  B. NEWSOM  ACIS/CIS CREF REDESIGN COMMUNICATIONS   (ACCRC)
*                      RE-STOW FOR APPL1158 AND APPL1152
* 06/15/17  BARUA    PIN EXPANSION CHANGES. (CHG425939)  PINE
* 01/20/18  ELLO     CHANGES TO INCLUDE THE FOLLOWING FIELDS FOR ONEIRA
*                    RT-TIAA-INIT-PREM = TIAA INIT. PREMIUM
*                    RT-CREF-INIT-PREM = CREF INIT. PREMIUM
* 11/17/19  L SHU    INCLUDE ADDITIONAL FIELDS TPA/IPRO FOR
*                    CONTRACT STRATEGY AND 2ND ALLOCATION FOR IISG
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb1158 extends BLNatBase
{
    // Data Areas
    private LdaAppl1152 ldaAppl1152;
    private LdaAppl1158 ldaAppl1158;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup hold_File;
    private DbsField hold_File_Record_Id;

    private DbsGroup hold_File__R_Field_1;
    private DbsField hold_File_Hd_Record_Type;
    private DbsField hold_File_Hd_Output_Profile;
    private DbsField hold_File_Rt_Print_Package_Type;
    private DbsField hold_File_Rt_Reprint_Request_Type;
    private DbsField hold_File_Hd_Product_Type;
    private DbsField hold_File_Rt_Type_Cd;
    private DbsField hold_File_Rt_Tiaa_No_1;
    private DbsField hold_File_Rt_Tiaa_Fill_1;
    private DbsField hold_File_Rt_Cref_No_1;
    private DbsField hold_File_Rt_Cref_Fill_1;
    private DbsField hold_File_Rt_Tiaa_Cntr_2;

    private DbsGroup hold_File__R_Field_2;
    private DbsField hold_File_Rt_Tiaa_No_2;
    private DbsField hold_File_Rt_Tiaa_Fill_2;
    private DbsField hold_File_Rt_Cref_Cert_2;

    private DbsGroup hold_File__R_Field_3;
    private DbsField hold_File_Rt_Cref_No_2;
    private DbsField hold_File_Rt_Cref_Fill_2;
    private DbsField hold_File_Rt_Tiaa_Cntr_3;

    private DbsGroup hold_File__R_Field_4;
    private DbsField hold_File_Rt_Tiaa_No_3;
    private DbsField hold_File_Rt_Tiaa_Fill_3;
    private DbsField hold_File_Rt_Cref_Cert_3;

    private DbsGroup hold_File__R_Field_5;
    private DbsField hold_File_Rt_Cref_No_3;
    private DbsField hold_File_Rt_Cref_Fill_3;
    private DbsField hold_File_Rt_Tiaa_Cntr_4;

    private DbsGroup hold_File__R_Field_6;
    private DbsField hold_File_Rt_Tiaa_No_4;
    private DbsField hold_File_Rt_Tiaa_Fill_4;
    private DbsField hold_File_Rt_Cref_Cert_4;

    private DbsGroup hold_File__R_Field_7;
    private DbsField hold_File_Rt_Cref_No_4;
    private DbsField hold_File_Rt_Cref_Fill_4;
    private DbsField hold_File_Rt_Date_Table;
    private DbsField hold_File_Rt_Age_First_Pymt;
    private DbsField hold_File_Rt_Ownership;
    private DbsField hold_File_Rt_Alloc_Disc;
    private DbsField hold_File_Rt_Currency;
    private DbsField hold_File_Rt_Tiaa_Extr_Form;
    private DbsField hold_File_Rt_Cref_Extr_Form;
    private DbsField hold_File_Rt_Tiaa_Extr_Ed;
    private DbsField hold_File_Rt_Cref_Extr_Ed;
    private DbsField hold_File_Rt_Annuitant_Name;

    private DbsGroup hold_File_Rt_Eop_Name;
    private DbsField hold_File_Rt_Eop_Last_Name;
    private DbsField hold_File_Rt_Eop_First_Name;
    private DbsField hold_File_Rt_Eop_Middle_Name;

    private DbsGroup hold_File__R_Field_8;
    private DbsField hold_File_Rt_Eop_Mid_Init;
    private DbsField hold_File_Rt_Eop_Mid_Filler;
    private DbsField hold_File_Rt_Eop_Title;
    private DbsField hold_File_Rt_Eop_Prefix;
    private DbsField hold_File_Rt_Soc_Sec;
    private DbsField hold_File_Rt_Pin_No;
    private DbsField hold_File_Rt_Address_Table;
    private DbsField hold_File_Rt_Eop_Zipcode;
    private DbsField hold_File_Rt_Participant_Status;
    private DbsField hold_File_Rt_Email_Address;
    private DbsField hold_File_Rt_Request_Package_Type;
    private DbsField hold_File_Rt_Portfolio_Selected;
    private DbsField hold_File_Rt_Divorce_Contract;
    private DbsField hold_File_Rt_Access_Cd_Ind;
    private DbsField hold_File_Rt_Address_Change;
    private DbsField hold_File_Rt_Package_Version;
    private DbsField hold_File_Rt_Unit_Name;
    private DbsField hold_File_Rt_Future_Filler;
    private DbsField hold_File_Rt_Bene_Estate;
    private DbsField hold_File_Rt_Bene_Trust;
    private DbsField hold_File_Rt_Bene_Category;
    private DbsField hold_File_Rt_Sex;
    private DbsField hold_File_Rt_Inst_Name_Table;
    private DbsField hold_File_Rt_Inst_Name;
    private DbsField hold_File_Rt_Inst_Address_Table;
    private DbsField hold_File_Rt_Inst_Zip_Cd;
    private DbsField hold_File_Rt_Mail_Instructions;
    private DbsField hold_File_Rt_Orig_Iss_State;
    private DbsField hold_File_Rt_Current_Iss_State;
    private DbsField hold_File_Rt_Eop_State_Name;
    private DbsField hold_File_Rt_Lob;
    private DbsField hold_File_Rt_Lob_Type;
    private DbsField hold_File_Rt_Inst_Code;
    private DbsField hold_File_Rt_Inst_Bill_Cd;
    private DbsField hold_File_Rt_Inst_Permit_Trans;
    private DbsField hold_File_Rt_Perm_Gra_Trans;
    private DbsField hold_File_Rt_Inst_Full_Immed_Vest;
    private DbsField hold_File_Rt_Inst_Cashable_Ra;
    private DbsField hold_File_Rt_Inst_Cashable_Gra;
    private DbsField hold_File_Rt_Inst_Fixed_Per_Opt;
    private DbsField hold_File_Rt_Inst_Cntrl_Consent;
    private DbsField hold_File_Rt_Eop;
    private DbsField hold_File_Rt_Gsra_Inst_St_Code;
    private DbsField hold_File_Rt_Region_Cd;

    private DbsGroup hold_File__R_Field_9;
    private DbsField hold_File_Rt_Region;
    private DbsField hold_File_Rt_Branch;
    private DbsField hold_File_Rt_Need;
    private DbsField hold_File_Rt_Staff_Id;
    private DbsField hold_File_Rt_Appl_Source;
    private DbsField hold_File_Rt_Eop_Appl_Status;
    private DbsField hold_File_Rt_Eop_Addl_Cref_Req;
    private DbsField hold_File_Rt_Extract_Date;
    private DbsField hold_File_Rt_Gsra_Loan_Ind;
    private DbsField hold_File_Rt_Spec_Ppg_Ind;
    private DbsField hold_File_Rt_Mail_Pull_Cd;
    private DbsField hold_File_Rt_Mit_Log_Dt_Tm;
    private DbsField hold_File_Rt_Addr_Page_Name;

    private DbsGroup hold_File__R_Field_10;
    private DbsField hold_File_Rt_Addr_Pref;
    private DbsField hold_File_Rt_Addr_Filler_1;
    private DbsField hold_File_Rt_Addr_Frst;
    private DbsField hold_File_Rt_Addr_Filler_2;
    private DbsField hold_File_Rt_Addr_Mddle;
    private DbsField hold_File_Rt_Addr_Filler_3;
    private DbsField hold_File_Rt_Addr_Last;
    private DbsField hold_File_Rt_Addr_Filler_4;
    private DbsField hold_File_Rt_Addr_Sffx;
    private DbsField hold_File_Rt_Welcome_Name;

    private DbsGroup hold_File__R_Field_11;
    private DbsField hold_File_Rt_Welcome_Pref;
    private DbsField hold_File_Rt_Welcome_Filler_1;
    private DbsField hold_File_Rt_Welcome_Last;
    private DbsField hold_File_Rt_Pin_Name;

    private DbsGroup hold_File__R_Field_12;
    private DbsField hold_File_Rt_Pin_First;
    private DbsField hold_File_Rt_Pin_Filler_1;
    private DbsField hold_File_Rt_Pin_Mddle;
    private DbsField hold_File_Rt_Pin_Filler_2;
    private DbsField hold_File_Rt_Pin_Last;
    private DbsField hold_File_Rt_Zip;

    private DbsGroup hold_File__R_Field_13;
    private DbsField hold_File_Rt_Zip_Plus4;
    private DbsField hold_File_Rt_Zip_Plus1;
    private DbsField hold_File_Rt_Suspension;
    private DbsField hold_File_Rt_Mult_App_Version;
    private DbsField hold_File_Rt_Mult_App_Status;
    private DbsField hold_File_Rt_Mult_App_Lob;
    private DbsField hold_File_Rt_Mult_App_Lob_Type;
    private DbsField hold_File_Rt_Mult_App_Ppg;
    private DbsField hold_File_Rt_K12_Ppg;
    private DbsField hold_File_Rt_Ira_Rollover_Type;
    private DbsField hold_File_Rt_Ira_Record_Type;
    private DbsField hold_File_Rt_Rollover_Amt;
    private DbsField hold_File_Rt_Mit_Unit;
    private DbsField hold_File_Rt_Mit_Wpid;
    private DbsField hold_File_Rt_New_Bene_Ind;
    private DbsField hold_File_Rt_Irc_Sectn_Cde;
    private DbsField hold_File_Rt_Released_Dt;
    private DbsField hold_File_Rt_Irc_Sectn_Grp;
    private DbsField hold_File_Rt_Enroll_Request_Type;
    private DbsField hold_File_Rt_Erisa_Inst;
    private DbsField hold_File_Rt_Print_Destination;
    private DbsField hold_File_Rt_Correction_Type;
    private DbsField hold_File_Rt_Processor_Name;
    private DbsField hold_File_Rt_Mail_Date;
    private DbsField hold_File_Rt_Alloc_Fmt;
    private DbsField hold_File_Rt_Phone_No;
    private DbsField hold_File_Rt_Fund_Cde;
    private DbsField hold_File_Rt_Alloc_Pct;
    private DbsField hold_File_Rt_Oia_Ind;
    private DbsField hold_File_Rt_Product_Cde;
    private DbsField hold_File_Rt_Acct_Sum_Sheet_Type;
    private DbsField hold_File_Rt_Sg_Plan_No;
    private DbsField hold_File_Rt_Sg_Subplan_No;
    private DbsField hold_File_Rt_Sg_Subplan_Type;
    private DbsField hold_File_Rt_Omni_Issue_Ind;
    private DbsField hold_File_Rt_Effective_Dt;
    private DbsField hold_File_Rt_Employer_Name;
    private DbsField hold_File_Rt_Tiaa_Rate;
    private DbsField hold_File_Rt_Sg_Plan_Id;
    private DbsField hold_File_Rt_Tiaa_Pg4_Numb;
    private DbsField hold_File_Rt_Cref_Pg4_Numb;
    private DbsField hold_File_Rt_Dflt_Access_Code;
    private DbsField hold_File_Rt_Single_Issue_Ind;
    private DbsField hold_File_Rt_Spec_Fund_Ind;
    private DbsField hold_File_Rt_Product_Price_Level;
    private DbsField hold_File_Rt_Roth_Ind;
    private DbsField hold_File_Rt_Plan_Issue_State;
    private DbsField hold_File_Rt_Client_Id;
    private DbsField hold_File_Rt_Portfolio_Model;
    private DbsField hold_File_Rt_Auto_Enroll_Ind;
    private DbsField hold_File_Rt_Auto_Save_Ind;
    private DbsField hold_File_Rt_As_Cur_Dflt_Amt;
    private DbsField hold_File_Rt_As_Incr_Amt;
    private DbsField hold_File_Rt_As_Max_Pct;
    private DbsField hold_File_Rt_Ae_Opt_Out_Days;
    private DbsField hold_File_Rt_Tsv_Ind;
    private DbsField hold_File_Rt_Tiaa_Indx_Guarntd_Rte;
    private DbsField hold_File_Rt_Agent_Crd_No;
    private DbsField hold_File_Rt_Crd_Pull_Ind;
    private DbsField hold_File_Rt_Agent_Name;
    private DbsField hold_File_Rt_Agent_Work_Addr1;
    private DbsField hold_File_Rt_Agent_Work_Addr2;
    private DbsField hold_File_Rt_Agent_Work_City;
    private DbsField hold_File_Rt_Agent_Work_State;
    private DbsField hold_File_Rt_Agent_Work_Zip;
    private DbsField hold_File_Rt_Agent_Work_Phone;
    private DbsField hold_File_Rt_Tic_Ind;
    private DbsField hold_File_Rt_Tic_Startdate;
    private DbsField hold_File_Rt_Tic_Enddate;
    private DbsField hold_File_Rt_Tic_Percentage;
    private DbsField hold_File_Rt_Tic_Postdays;
    private DbsField hold_File_Rt_Tic_Limit;
    private DbsField hold_File_Rt_Tic_Postfreq;
    private DbsField hold_File_Rt_Tic_Pl_Level;
    private DbsField hold_File_Rt_Tic_Windowdays;
    private DbsField hold_File_Rt_Tic_Reqdlywindow;
    private DbsField hold_File_Rt_Tic_Recap_Prov;
    private DbsField hold_File_Rt_Ecs_Dcs_E_Dlvry_Ind;
    private DbsField hold_File_Rt_Ecs_Dcs_Annty_Optn_Ind;
    private DbsField hold_File_Rt_Full_Middle_Name;
    private DbsField hold_File_Rt_Substitution_Contract_Ind;
    private DbsField hold_File_Rt_Bundle_Type;
    private DbsField hold_File_Rt_Bundle_Record_Type;
    private DbsField hold_File_Rt_Bundle_Number;
    private DbsField hold_File_Rt_Cover_Letter_Contracts;

    private DbsGroup hold_File__R_Field_14;
    private DbsField hold_File_Rt_Cl_Tiaa_Contract_1;
    private DbsField hold_File_Rt_Cl_Tiaa_Contract_2;
    private DbsField hold_File_Rt_Cl_Tiaa_Contract_3;
    private DbsField hold_File_Rt_Cl_Tiaa_Contract_4;
    private DbsField hold_File_Rt_Non_Proprietary_Pkg_Ind;
    private DbsField hold_File_Rt_Multi_Plan_Table;

    private DbsGroup hold_File__R_Field_15;

    private DbsGroup hold_File_Rt_Multi_Plan_Info;
    private DbsField hold_File_Rt_Multi_Plan_No;
    private DbsField hold_File_Rt_Multi_Sub_Plan;
    private DbsField hold_File_Rt_Filler;
    private DbsField hold_File_Rt_Tiaa_Init_Prem;
    private DbsField hold_File_Rt_Cref_Init_Prem;
    private DbsField hold_File_Rt_Related_Contract_Info_Table;

    private DbsGroup hold_File__R_Field_16;

    private DbsGroup hold_File_Rt_Related_Contract_Info;
    private DbsField hold_File_Rt_Related_Contract_Type;
    private DbsField hold_File_Rt_Related_Tiaa_No;
    private DbsField hold_File_Rt_Related_Tiaa_Issue_Date;
    private DbsField hold_File_Rt_Related_First_Payment_Date;
    private DbsField hold_File_Rt_Related_Tiaa_Total_Amt;
    private DbsField hold_File_Rt_Related_Last_Payment_Date;
    private DbsField hold_File_Rt_Related_Payment_Frequency;
    private DbsField hold_File_Rt_Related_Tiaa_Issue_State;
    private DbsField hold_File_Rt_First_Tpa_Ipro_Ind;
    private DbsField hold_File_Rt_Fund_Source_1;
    private DbsField hold_File_Rt_Fund_Source_2;
    private DbsField hold_File_Rt_Fund_Cde_2;
    private DbsField hold_File_Rt_Alloc_Pct_2;
    private DbsField pnd_Ctr;
    private DbsField pnd_Hold_Profile;
    private DbsField pnd_Hold_Ssn;
    private DbsField pnd_Hold_Bundle_Type;
    private DbsField pnd_Contract;

    private DbsGroup pnd_Contract__R_Field_17;
    private DbsField pnd_Contract_Pnd_Tiaa_No_1;
    private DbsField pnd_Contract_Pnd_Tiaa_Fill_1;
    private DbsField pnd_Bundle_Number;
    private DbsField pnd_Rec_Ctr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl1152 = new LdaAppl1152();
        registerRecord(ldaAppl1152);
        ldaAppl1158 = new LdaAppl1158();
        registerRecord(ldaAppl1158);

        // Local Variables
        localVariables = new DbsRecord();

        hold_File = localVariables.newGroupInRecord("hold_File", "HOLD-FILE");
        hold_File_Record_Id = hold_File.newFieldInGroup("hold_File_Record_Id", "RECORD-ID", FieldType.STRING, 33);

        hold_File__R_Field_1 = hold_File.newGroupInGroup("hold_File__R_Field_1", "REDEFINE", hold_File_Record_Id);
        hold_File_Hd_Record_Type = hold_File__R_Field_1.newFieldInGroup("hold_File_Hd_Record_Type", "HD-RECORD-TYPE", FieldType.STRING, 2);
        hold_File_Hd_Output_Profile = hold_File__R_Field_1.newFieldInGroup("hold_File_Hd_Output_Profile", "HD-OUTPUT-PROFILE", FieldType.STRING, 2);
        hold_File_Rt_Print_Package_Type = hold_File__R_Field_1.newFieldInGroup("hold_File_Rt_Print_Package_Type", "RT-PRINT-PACKAGE-TYPE", FieldType.STRING, 
            1);
        hold_File_Rt_Reprint_Request_Type = hold_File__R_Field_1.newFieldInGroup("hold_File_Rt_Reprint_Request_Type", "RT-REPRINT-REQUEST-TYPE", FieldType.STRING, 
            1);
        hold_File_Hd_Product_Type = hold_File__R_Field_1.newFieldInGroup("hold_File_Hd_Product_Type", "HD-PRODUCT-TYPE", FieldType.STRING, 6);
        hold_File_Rt_Type_Cd = hold_File__R_Field_1.newFieldInGroup("hold_File_Rt_Type_Cd", "RT-TYPE-CD", FieldType.STRING, 1);
        hold_File_Rt_Tiaa_No_1 = hold_File__R_Field_1.newFieldInGroup("hold_File_Rt_Tiaa_No_1", "RT-TIAA-NO-1", FieldType.STRING, 8);
        hold_File_Rt_Tiaa_Fill_1 = hold_File__R_Field_1.newFieldInGroup("hold_File_Rt_Tiaa_Fill_1", "RT-TIAA-FILL-1", FieldType.STRING, 2);
        hold_File_Rt_Cref_No_1 = hold_File__R_Field_1.newFieldInGroup("hold_File_Rt_Cref_No_1", "RT-CREF-NO-1", FieldType.STRING, 8);
        hold_File_Rt_Cref_Fill_1 = hold_File__R_Field_1.newFieldInGroup("hold_File_Rt_Cref_Fill_1", "RT-CREF-FILL-1", FieldType.STRING, 2);
        hold_File_Rt_Tiaa_Cntr_2 = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Cntr_2", "RT-TIAA-CNTR-2", FieldType.STRING, 10);

        hold_File__R_Field_2 = hold_File.newGroupInGroup("hold_File__R_Field_2", "REDEFINE", hold_File_Rt_Tiaa_Cntr_2);
        hold_File_Rt_Tiaa_No_2 = hold_File__R_Field_2.newFieldInGroup("hold_File_Rt_Tiaa_No_2", "RT-TIAA-NO-2", FieldType.STRING, 8);
        hold_File_Rt_Tiaa_Fill_2 = hold_File__R_Field_2.newFieldInGroup("hold_File_Rt_Tiaa_Fill_2", "RT-TIAA-FILL-2", FieldType.STRING, 2);
        hold_File_Rt_Cref_Cert_2 = hold_File.newFieldInGroup("hold_File_Rt_Cref_Cert_2", "RT-CREF-CERT-2", FieldType.STRING, 10);

        hold_File__R_Field_3 = hold_File.newGroupInGroup("hold_File__R_Field_3", "REDEFINE", hold_File_Rt_Cref_Cert_2);
        hold_File_Rt_Cref_No_2 = hold_File__R_Field_3.newFieldInGroup("hold_File_Rt_Cref_No_2", "RT-CREF-NO-2", FieldType.STRING, 8);
        hold_File_Rt_Cref_Fill_2 = hold_File__R_Field_3.newFieldInGroup("hold_File_Rt_Cref_Fill_2", "RT-CREF-FILL-2", FieldType.STRING, 2);
        hold_File_Rt_Tiaa_Cntr_3 = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Cntr_3", "RT-TIAA-CNTR-3", FieldType.STRING, 10);

        hold_File__R_Field_4 = hold_File.newGroupInGroup("hold_File__R_Field_4", "REDEFINE", hold_File_Rt_Tiaa_Cntr_3);
        hold_File_Rt_Tiaa_No_3 = hold_File__R_Field_4.newFieldInGroup("hold_File_Rt_Tiaa_No_3", "RT-TIAA-NO-3", FieldType.STRING, 8);
        hold_File_Rt_Tiaa_Fill_3 = hold_File__R_Field_4.newFieldInGroup("hold_File_Rt_Tiaa_Fill_3", "RT-TIAA-FILL-3", FieldType.STRING, 2);
        hold_File_Rt_Cref_Cert_3 = hold_File.newFieldInGroup("hold_File_Rt_Cref_Cert_3", "RT-CREF-CERT-3", FieldType.STRING, 10);

        hold_File__R_Field_5 = hold_File.newGroupInGroup("hold_File__R_Field_5", "REDEFINE", hold_File_Rt_Cref_Cert_3);
        hold_File_Rt_Cref_No_3 = hold_File__R_Field_5.newFieldInGroup("hold_File_Rt_Cref_No_3", "RT-CREF-NO-3", FieldType.STRING, 8);
        hold_File_Rt_Cref_Fill_3 = hold_File__R_Field_5.newFieldInGroup("hold_File_Rt_Cref_Fill_3", "RT-CREF-FILL-3", FieldType.STRING, 2);
        hold_File_Rt_Tiaa_Cntr_4 = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Cntr_4", "RT-TIAA-CNTR-4", FieldType.STRING, 10);

        hold_File__R_Field_6 = hold_File.newGroupInGroup("hold_File__R_Field_6", "REDEFINE", hold_File_Rt_Tiaa_Cntr_4);
        hold_File_Rt_Tiaa_No_4 = hold_File__R_Field_6.newFieldInGroup("hold_File_Rt_Tiaa_No_4", "RT-TIAA-NO-4", FieldType.STRING, 8);
        hold_File_Rt_Tiaa_Fill_4 = hold_File__R_Field_6.newFieldInGroup("hold_File_Rt_Tiaa_Fill_4", "RT-TIAA-FILL-4", FieldType.STRING, 2);
        hold_File_Rt_Cref_Cert_4 = hold_File.newFieldInGroup("hold_File_Rt_Cref_Cert_4", "RT-CREF-CERT-4", FieldType.STRING, 10);

        hold_File__R_Field_7 = hold_File.newGroupInGroup("hold_File__R_Field_7", "REDEFINE", hold_File_Rt_Cref_Cert_4);
        hold_File_Rt_Cref_No_4 = hold_File__R_Field_7.newFieldInGroup("hold_File_Rt_Cref_No_4", "RT-CREF-NO-4", FieldType.STRING, 8);
        hold_File_Rt_Cref_Fill_4 = hold_File__R_Field_7.newFieldInGroup("hold_File_Rt_Cref_Fill_4", "RT-CREF-FILL-4", FieldType.STRING, 2);
        hold_File_Rt_Date_Table = hold_File.newFieldArrayInGroup("hold_File_Rt_Date_Table", "RT-DATE-TABLE", FieldType.STRING, 8, new DbsArrayController(1, 
            6));
        hold_File_Rt_Age_First_Pymt = hold_File.newFieldInGroup("hold_File_Rt_Age_First_Pymt", "RT-AGE-FIRST-PYMT", FieldType.STRING, 4);
        hold_File_Rt_Ownership = hold_File.newFieldInGroup("hold_File_Rt_Ownership", "RT-OWNERSHIP", FieldType.STRING, 1);
        hold_File_Rt_Alloc_Disc = hold_File.newFieldInGroup("hold_File_Rt_Alloc_Disc", "RT-ALLOC-DISC", FieldType.STRING, 1);
        hold_File_Rt_Currency = hold_File.newFieldInGroup("hold_File_Rt_Currency", "RT-CURRENCY", FieldType.STRING, 1);
        hold_File_Rt_Tiaa_Extr_Form = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Extr_Form", "RT-TIAA-EXTR-FORM", FieldType.STRING, 9);
        hold_File_Rt_Cref_Extr_Form = hold_File.newFieldInGroup("hold_File_Rt_Cref_Extr_Form", "RT-CREF-EXTR-FORM", FieldType.STRING, 9);
        hold_File_Rt_Tiaa_Extr_Ed = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Extr_Ed", "RT-TIAA-EXTR-ED", FieldType.STRING, 9);
        hold_File_Rt_Cref_Extr_Ed = hold_File.newFieldInGroup("hold_File_Rt_Cref_Extr_Ed", "RT-CREF-EXTR-ED", FieldType.STRING, 9);
        hold_File_Rt_Annuitant_Name = hold_File.newFieldInGroup("hold_File_Rt_Annuitant_Name", "RT-ANNUITANT-NAME", FieldType.STRING, 38);

        hold_File_Rt_Eop_Name = hold_File.newGroupInGroup("hold_File_Rt_Eop_Name", "RT-EOP-NAME");
        hold_File_Rt_Eop_Last_Name = hold_File_Rt_Eop_Name.newFieldInGroup("hold_File_Rt_Eop_Last_Name", "RT-EOP-LAST-NAME", FieldType.STRING, 16);
        hold_File_Rt_Eop_First_Name = hold_File_Rt_Eop_Name.newFieldInGroup("hold_File_Rt_Eop_First_Name", "RT-EOP-FIRST-NAME", FieldType.STRING, 10);
        hold_File_Rt_Eop_Middle_Name = hold_File_Rt_Eop_Name.newFieldInGroup("hold_File_Rt_Eop_Middle_Name", "RT-EOP-MIDDLE-NAME", FieldType.STRING, 10);

        hold_File__R_Field_8 = hold_File_Rt_Eop_Name.newGroupInGroup("hold_File__R_Field_8", "REDEFINE", hold_File_Rt_Eop_Middle_Name);
        hold_File_Rt_Eop_Mid_Init = hold_File__R_Field_8.newFieldInGroup("hold_File_Rt_Eop_Mid_Init", "RT-EOP-MID-INIT", FieldType.STRING, 1);
        hold_File_Rt_Eop_Mid_Filler = hold_File__R_Field_8.newFieldInGroup("hold_File_Rt_Eop_Mid_Filler", "RT-EOP-MID-FILLER", FieldType.STRING, 9);
        hold_File_Rt_Eop_Title = hold_File_Rt_Eop_Name.newFieldInGroup("hold_File_Rt_Eop_Title", "RT-EOP-TITLE", FieldType.STRING, 4);
        hold_File_Rt_Eop_Prefix = hold_File_Rt_Eop_Name.newFieldInGroup("hold_File_Rt_Eop_Prefix", "RT-EOP-PREFIX", FieldType.STRING, 4);
        hold_File_Rt_Soc_Sec = hold_File.newFieldInGroup("hold_File_Rt_Soc_Sec", "RT-SOC-SEC", FieldType.STRING, 11);
        hold_File_Rt_Pin_No = hold_File.newFieldInGroup("hold_File_Rt_Pin_No", "RT-PIN-NO", FieldType.STRING, 12);
        hold_File_Rt_Address_Table = hold_File.newFieldArrayInGroup("hold_File_Rt_Address_Table", "RT-ADDRESS-TABLE", FieldType.STRING, 35, new DbsArrayController(1, 
            6));
        hold_File_Rt_Eop_Zipcode = hold_File.newFieldInGroup("hold_File_Rt_Eop_Zipcode", "RT-EOP-ZIPCODE", FieldType.STRING, 5);
        hold_File_Rt_Participant_Status = hold_File.newFieldInGroup("hold_File_Rt_Participant_Status", "RT-PARTICIPANT-STATUS", FieldType.STRING, 1);
        hold_File_Rt_Email_Address = hold_File.newFieldInGroup("hold_File_Rt_Email_Address", "RT-EMAIL-ADDRESS", FieldType.STRING, 50);
        hold_File_Rt_Request_Package_Type = hold_File.newFieldInGroup("hold_File_Rt_Request_Package_Type", "RT-REQUEST-PACKAGE-TYPE", FieldType.STRING, 
            1);
        hold_File_Rt_Portfolio_Selected = hold_File.newFieldInGroup("hold_File_Rt_Portfolio_Selected", "RT-PORTFOLIO-SELECTED", FieldType.STRING, 1);
        hold_File_Rt_Divorce_Contract = hold_File.newFieldInGroup("hold_File_Rt_Divorce_Contract", "RT-DIVORCE-CONTRACT", FieldType.STRING, 1);
        hold_File_Rt_Access_Cd_Ind = hold_File.newFieldInGroup("hold_File_Rt_Access_Cd_Ind", "RT-ACCESS-CD-IND", FieldType.STRING, 1);
        hold_File_Rt_Address_Change = hold_File.newFieldInGroup("hold_File_Rt_Address_Change", "RT-ADDRESS-CHANGE", FieldType.STRING, 1);
        hold_File_Rt_Package_Version = hold_File.newFieldInGroup("hold_File_Rt_Package_Version", "RT-PACKAGE-VERSION", FieldType.STRING, 1);
        hold_File_Rt_Unit_Name = hold_File.newFieldInGroup("hold_File_Rt_Unit_Name", "RT-UNIT-NAME", FieldType.STRING, 10);
        hold_File_Rt_Future_Filler = hold_File.newFieldInGroup("hold_File_Rt_Future_Filler", "RT-FUTURE-FILLER", FieldType.STRING, 5);
        hold_File_Rt_Bene_Estate = hold_File.newFieldInGroup("hold_File_Rt_Bene_Estate", "RT-BENE-ESTATE", FieldType.STRING, 1);
        hold_File_Rt_Bene_Trust = hold_File.newFieldInGroup("hold_File_Rt_Bene_Trust", "RT-BENE-TRUST", FieldType.STRING, 1);
        hold_File_Rt_Bene_Category = hold_File.newFieldInGroup("hold_File_Rt_Bene_Category", "RT-BENE-CATEGORY", FieldType.STRING, 1);
        hold_File_Rt_Sex = hold_File.newFieldInGroup("hold_File_Rt_Sex", "RT-SEX", FieldType.STRING, 1);
        hold_File_Rt_Inst_Name_Table = hold_File.newFieldArrayInGroup("hold_File_Rt_Inst_Name_Table", "RT-INST-NAME-TABLE", FieldType.STRING, 30, new 
            DbsArrayController(1, 10));
        hold_File_Rt_Inst_Name = hold_File.newFieldInGroup("hold_File_Rt_Inst_Name", "RT-INST-NAME", FieldType.STRING, 76);
        hold_File_Rt_Inst_Address_Table = hold_File.newFieldArrayInGroup("hold_File_Rt_Inst_Address_Table", "RT-INST-ADDRESS-TABLE", FieldType.STRING, 
            35, new DbsArrayController(1, 6));
        hold_File_Rt_Inst_Zip_Cd = hold_File.newFieldInGroup("hold_File_Rt_Inst_Zip_Cd", "RT-INST-ZIP-CD", FieldType.STRING, 5);
        hold_File_Rt_Mail_Instructions = hold_File.newFieldInGroup("hold_File_Rt_Mail_Instructions", "RT-MAIL-INSTRUCTIONS", FieldType.STRING, 1);
        hold_File_Rt_Orig_Iss_State = hold_File.newFieldInGroup("hold_File_Rt_Orig_Iss_State", "RT-ORIG-ISS-STATE", FieldType.STRING, 2);
        hold_File_Rt_Current_Iss_State = hold_File.newFieldInGroup("hold_File_Rt_Current_Iss_State", "RT-CURRENT-ISS-STATE", FieldType.STRING, 2);
        hold_File_Rt_Eop_State_Name = hold_File.newFieldInGroup("hold_File_Rt_Eop_State_Name", "RT-EOP-STATE-NAME", FieldType.STRING, 15);
        hold_File_Rt_Lob = hold_File.newFieldInGroup("hold_File_Rt_Lob", "RT-LOB", FieldType.STRING, 1);
        hold_File_Rt_Lob_Type = hold_File.newFieldInGroup("hold_File_Rt_Lob_Type", "RT-LOB-TYPE", FieldType.STRING, 1);
        hold_File_Rt_Inst_Code = hold_File.newFieldInGroup("hold_File_Rt_Inst_Code", "RT-INST-CODE", FieldType.STRING, 4);
        hold_File_Rt_Inst_Bill_Cd = hold_File.newFieldInGroup("hold_File_Rt_Inst_Bill_Cd", "RT-INST-BILL-CD", FieldType.STRING, 1);
        hold_File_Rt_Inst_Permit_Trans = hold_File.newFieldInGroup("hold_File_Rt_Inst_Permit_Trans", "RT-INST-PERMIT-TRANS", FieldType.STRING, 1);
        hold_File_Rt_Perm_Gra_Trans = hold_File.newFieldInGroup("hold_File_Rt_Perm_Gra_Trans", "RT-PERM-GRA-TRANS", FieldType.STRING, 1);
        hold_File_Rt_Inst_Full_Immed_Vest = hold_File.newFieldInGroup("hold_File_Rt_Inst_Full_Immed_Vest", "RT-INST-FULL-IMMED-VEST", FieldType.STRING, 
            1);
        hold_File_Rt_Inst_Cashable_Ra = hold_File.newFieldInGroup("hold_File_Rt_Inst_Cashable_Ra", "RT-INST-CASHABLE-RA", FieldType.STRING, 1);
        hold_File_Rt_Inst_Cashable_Gra = hold_File.newFieldInGroup("hold_File_Rt_Inst_Cashable_Gra", "RT-INST-CASHABLE-GRA", FieldType.STRING, 1);
        hold_File_Rt_Inst_Fixed_Per_Opt = hold_File.newFieldInGroup("hold_File_Rt_Inst_Fixed_Per_Opt", "RT-INST-FIXED-PER-OPT", FieldType.STRING, 1);
        hold_File_Rt_Inst_Cntrl_Consent = hold_File.newFieldInGroup("hold_File_Rt_Inst_Cntrl_Consent", "RT-INST-CNTRL-CONSENT", FieldType.STRING, 1);
        hold_File_Rt_Eop = hold_File.newFieldInGroup("hold_File_Rt_Eop", "RT-EOP", FieldType.STRING, 1);
        hold_File_Rt_Gsra_Inst_St_Code = hold_File.newFieldInGroup("hold_File_Rt_Gsra_Inst_St_Code", "RT-GSRA-INST-ST-CODE", FieldType.STRING, 2);
        hold_File_Rt_Region_Cd = hold_File.newFieldInGroup("hold_File_Rt_Region_Cd", "RT-REGION-CD", FieldType.STRING, 3);

        hold_File__R_Field_9 = hold_File.newGroupInGroup("hold_File__R_Field_9", "REDEFINE", hold_File_Rt_Region_Cd);
        hold_File_Rt_Region = hold_File__R_Field_9.newFieldInGroup("hold_File_Rt_Region", "RT-REGION", FieldType.STRING, 1);
        hold_File_Rt_Branch = hold_File__R_Field_9.newFieldInGroup("hold_File_Rt_Branch", "RT-BRANCH", FieldType.STRING, 1);
        hold_File_Rt_Need = hold_File__R_Field_9.newFieldInGroup("hold_File_Rt_Need", "RT-NEED", FieldType.STRING, 1);
        hold_File_Rt_Staff_Id = hold_File.newFieldInGroup("hold_File_Rt_Staff_Id", "RT-STAFF-ID", FieldType.STRING, 3);
        hold_File_Rt_Appl_Source = hold_File.newFieldInGroup("hold_File_Rt_Appl_Source", "RT-APPL-SOURCE", FieldType.STRING, 1);
        hold_File_Rt_Eop_Appl_Status = hold_File.newFieldInGroup("hold_File_Rt_Eop_Appl_Status", "RT-EOP-APPL-STATUS", FieldType.STRING, 1);
        hold_File_Rt_Eop_Addl_Cref_Req = hold_File.newFieldInGroup("hold_File_Rt_Eop_Addl_Cref_Req", "RT-EOP-ADDL-CREF-REQ", FieldType.STRING, 1);
        hold_File_Rt_Extract_Date = hold_File.newFieldInGroup("hold_File_Rt_Extract_Date", "RT-EXTRACT-DATE", FieldType.STRING, 6);
        hold_File_Rt_Gsra_Loan_Ind = hold_File.newFieldInGroup("hold_File_Rt_Gsra_Loan_Ind", "RT-GSRA-LOAN-IND", FieldType.STRING, 1);
        hold_File_Rt_Spec_Ppg_Ind = hold_File.newFieldInGroup("hold_File_Rt_Spec_Ppg_Ind", "RT-SPEC-PPG-IND", FieldType.STRING, 2);
        hold_File_Rt_Mail_Pull_Cd = hold_File.newFieldInGroup("hold_File_Rt_Mail_Pull_Cd", "RT-MAIL-PULL-CD", FieldType.STRING, 1);
        hold_File_Rt_Mit_Log_Dt_Tm = hold_File.newFieldArrayInGroup("hold_File_Rt_Mit_Log_Dt_Tm", "RT-MIT-LOG-DT-TM", FieldType.STRING, 15, new DbsArrayController(1, 
            5));
        hold_File_Rt_Addr_Page_Name = hold_File.newFieldInGroup("hold_File_Rt_Addr_Page_Name", "RT-ADDR-PAGE-NAME", FieldType.STRING, 81);

        hold_File__R_Field_10 = hold_File.newGroupInGroup("hold_File__R_Field_10", "REDEFINE", hold_File_Rt_Addr_Page_Name);
        hold_File_Rt_Addr_Pref = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Pref", "RT-ADDR-PREF", FieldType.STRING, 8);
        hold_File_Rt_Addr_Filler_1 = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Filler_1", "RT-ADDR-FILLER-1", FieldType.STRING, 1);
        hold_File_Rt_Addr_Frst = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Frst", "RT-ADDR-FRST", FieldType.STRING, 30);
        hold_File_Rt_Addr_Filler_2 = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Filler_2", "RT-ADDR-FILLER-2", FieldType.STRING, 1);
        hold_File_Rt_Addr_Mddle = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Mddle", "RT-ADDR-MDDLE", FieldType.STRING, 1);
        hold_File_Rt_Addr_Filler_3 = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Filler_3", "RT-ADDR-FILLER-3", FieldType.STRING, 1);
        hold_File_Rt_Addr_Last = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Last", "RT-ADDR-LAST", FieldType.STRING, 30);
        hold_File_Rt_Addr_Filler_4 = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Filler_4", "RT-ADDR-FILLER-4", FieldType.STRING, 1);
        hold_File_Rt_Addr_Sffx = hold_File__R_Field_10.newFieldInGroup("hold_File_Rt_Addr_Sffx", "RT-ADDR-SFFX", FieldType.STRING, 8);
        hold_File_Rt_Welcome_Name = hold_File.newFieldInGroup("hold_File_Rt_Welcome_Name", "RT-WELCOME-NAME", FieldType.STRING, 39);

        hold_File__R_Field_11 = hold_File.newGroupInGroup("hold_File__R_Field_11", "REDEFINE", hold_File_Rt_Welcome_Name);
        hold_File_Rt_Welcome_Pref = hold_File__R_Field_11.newFieldInGroup("hold_File_Rt_Welcome_Pref", "RT-WELCOME-PREF", FieldType.STRING, 8);
        hold_File_Rt_Welcome_Filler_1 = hold_File__R_Field_11.newFieldInGroup("hold_File_Rt_Welcome_Filler_1", "RT-WELCOME-FILLER-1", FieldType.STRING, 
            1);
        hold_File_Rt_Welcome_Last = hold_File__R_Field_11.newFieldInGroup("hold_File_Rt_Welcome_Last", "RT-WELCOME-LAST", FieldType.STRING, 30);
        hold_File_Rt_Pin_Name = hold_File.newFieldInGroup("hold_File_Rt_Pin_Name", "RT-PIN-NAME", FieldType.STRING, 63);

        hold_File__R_Field_12 = hold_File.newGroupInGroup("hold_File__R_Field_12", "REDEFINE", hold_File_Rt_Pin_Name);
        hold_File_Rt_Pin_First = hold_File__R_Field_12.newFieldInGroup("hold_File_Rt_Pin_First", "RT-PIN-FIRST", FieldType.STRING, 30);
        hold_File_Rt_Pin_Filler_1 = hold_File__R_Field_12.newFieldInGroup("hold_File_Rt_Pin_Filler_1", "RT-PIN-FILLER-1", FieldType.STRING, 1);
        hold_File_Rt_Pin_Mddle = hold_File__R_Field_12.newFieldInGroup("hold_File_Rt_Pin_Mddle", "RT-PIN-MDDLE", FieldType.STRING, 1);
        hold_File_Rt_Pin_Filler_2 = hold_File__R_Field_12.newFieldInGroup("hold_File_Rt_Pin_Filler_2", "RT-PIN-FILLER-2", FieldType.STRING, 1);
        hold_File_Rt_Pin_Last = hold_File__R_Field_12.newFieldInGroup("hold_File_Rt_Pin_Last", "RT-PIN-LAST", FieldType.STRING, 30);
        hold_File_Rt_Zip = hold_File.newFieldInGroup("hold_File_Rt_Zip", "RT-ZIP", FieldType.STRING, 5);

        hold_File__R_Field_13 = hold_File.newGroupInGroup("hold_File__R_Field_13", "REDEFINE", hold_File_Rt_Zip);
        hold_File_Rt_Zip_Plus4 = hold_File__R_Field_13.newFieldInGroup("hold_File_Rt_Zip_Plus4", "RT-ZIP-PLUS4", FieldType.STRING, 4);
        hold_File_Rt_Zip_Plus1 = hold_File__R_Field_13.newFieldInGroup("hold_File_Rt_Zip_Plus1", "RT-ZIP-PLUS1", FieldType.STRING, 1);
        hold_File_Rt_Suspension = hold_File.newFieldArrayInGroup("hold_File_Rt_Suspension", "RT-SUSPENSION", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        hold_File_Rt_Mult_App_Version = hold_File.newFieldInGroup("hold_File_Rt_Mult_App_Version", "RT-MULT-APP-VERSION", FieldType.STRING, 1);
        hold_File_Rt_Mult_App_Status = hold_File.newFieldInGroup("hold_File_Rt_Mult_App_Status", "RT-MULT-APP-STATUS", FieldType.STRING, 1);
        hold_File_Rt_Mult_App_Lob = hold_File.newFieldInGroup("hold_File_Rt_Mult_App_Lob", "RT-MULT-APP-LOB", FieldType.STRING, 1);
        hold_File_Rt_Mult_App_Lob_Type = hold_File.newFieldInGroup("hold_File_Rt_Mult_App_Lob_Type", "RT-MULT-APP-LOB-TYPE", FieldType.STRING, 1);
        hold_File_Rt_Mult_App_Ppg = hold_File.newFieldInGroup("hold_File_Rt_Mult_App_Ppg", "RT-MULT-APP-PPG", FieldType.STRING, 6);
        hold_File_Rt_K12_Ppg = hold_File.newFieldInGroup("hold_File_Rt_K12_Ppg", "RT-K12-PPG", FieldType.STRING, 1);
        hold_File_Rt_Ira_Rollover_Type = hold_File.newFieldInGroup("hold_File_Rt_Ira_Rollover_Type", "RT-IRA-ROLLOVER-TYPE", FieldType.STRING, 1);
        hold_File_Rt_Ira_Record_Type = hold_File.newFieldInGroup("hold_File_Rt_Ira_Record_Type", "RT-IRA-RECORD-TYPE", FieldType.STRING, 1);
        hold_File_Rt_Rollover_Amt = hold_File.newFieldInGroup("hold_File_Rt_Rollover_Amt", "RT-ROLLOVER-AMT", FieldType.STRING, 14);
        hold_File_Rt_Mit_Unit = hold_File.newFieldArrayInGroup("hold_File_Rt_Mit_Unit", "RT-MIT-UNIT", FieldType.STRING, 8, new DbsArrayController(1, 
            5));
        hold_File_Rt_Mit_Wpid = hold_File.newFieldArrayInGroup("hold_File_Rt_Mit_Wpid", "RT-MIT-WPID", FieldType.STRING, 6, new DbsArrayController(1, 
            5));
        hold_File_Rt_New_Bene_Ind = hold_File.newFieldInGroup("hold_File_Rt_New_Bene_Ind", "RT-NEW-BENE-IND", FieldType.STRING, 1);
        hold_File_Rt_Irc_Sectn_Cde = hold_File.newFieldInGroup("hold_File_Rt_Irc_Sectn_Cde", "RT-IRC-SECTN-CDE", FieldType.NUMERIC, 2);
        hold_File_Rt_Released_Dt = hold_File.newFieldInGroup("hold_File_Rt_Released_Dt", "RT-RELEASED-DT", FieldType.NUMERIC, 8);
        hold_File_Rt_Irc_Sectn_Grp = hold_File.newFieldInGroup("hold_File_Rt_Irc_Sectn_Grp", "RT-IRC-SECTN-GRP", FieldType.NUMERIC, 2);
        hold_File_Rt_Enroll_Request_Type = hold_File.newFieldInGroup("hold_File_Rt_Enroll_Request_Type", "RT-ENROLL-REQUEST-TYPE", FieldType.STRING, 1);
        hold_File_Rt_Erisa_Inst = hold_File.newFieldInGroup("hold_File_Rt_Erisa_Inst", "RT-ERISA-INST", FieldType.STRING, 1);
        hold_File_Rt_Print_Destination = hold_File.newFieldInGroup("hold_File_Rt_Print_Destination", "RT-PRINT-DESTINATION", FieldType.STRING, 1);
        hold_File_Rt_Correction_Type = hold_File.newFieldArrayInGroup("hold_File_Rt_Correction_Type", "RT-CORRECTION-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        hold_File_Rt_Processor_Name = hold_File.newFieldInGroup("hold_File_Rt_Processor_Name", "RT-PROCESSOR-NAME", FieldType.STRING, 40);
        hold_File_Rt_Mail_Date = hold_File.newFieldInGroup("hold_File_Rt_Mail_Date", "RT-MAIL-DATE", FieldType.STRING, 17);
        hold_File_Rt_Alloc_Fmt = hold_File.newFieldInGroup("hold_File_Rt_Alloc_Fmt", "RT-ALLOC-FMT", FieldType.STRING, 1);
        hold_File_Rt_Phone_No = hold_File.newFieldInGroup("hold_File_Rt_Phone_No", "RT-PHONE-NO", FieldType.STRING, 20);
        hold_File_Rt_Fund_Cde = hold_File.newFieldArrayInGroup("hold_File_Rt_Fund_Cde", "RT-FUND-CDE", FieldType.STRING, 10, new DbsArrayController(1, 
            100));
        hold_File_Rt_Alloc_Pct = hold_File.newFieldArrayInGroup("hold_File_Rt_Alloc_Pct", "RT-ALLOC-PCT", FieldType.STRING, 3, new DbsArrayController(1, 
            100));
        hold_File_Rt_Oia_Ind = hold_File.newFieldInGroup("hold_File_Rt_Oia_Ind", "RT-OIA-IND", FieldType.STRING, 1);
        hold_File_Rt_Product_Cde = hold_File.newFieldInGroup("hold_File_Rt_Product_Cde", "RT-PRODUCT-CDE", FieldType.STRING, 3);
        hold_File_Rt_Acct_Sum_Sheet_Type = hold_File.newFieldInGroup("hold_File_Rt_Acct_Sum_Sheet_Type", "RT-ACCT-SUM-SHEET-TYPE", FieldType.STRING, 56);
        hold_File_Rt_Sg_Plan_No = hold_File.newFieldInGroup("hold_File_Rt_Sg_Plan_No", "RT-SG-PLAN-NO", FieldType.STRING, 6);
        hold_File_Rt_Sg_Subplan_No = hold_File.newFieldInGroup("hold_File_Rt_Sg_Subplan_No", "RT-SG-SUBPLAN-NO", FieldType.STRING, 6);
        hold_File_Rt_Sg_Subplan_Type = hold_File.newFieldInGroup("hold_File_Rt_Sg_Subplan_Type", "RT-SG-SUBPLAN-TYPE", FieldType.STRING, 3);
        hold_File_Rt_Omni_Issue_Ind = hold_File.newFieldInGroup("hold_File_Rt_Omni_Issue_Ind", "RT-OMNI-ISSUE-IND", FieldType.STRING, 1);
        hold_File_Rt_Effective_Dt = hold_File.newFieldInGroup("hold_File_Rt_Effective_Dt", "RT-EFFECTIVE-DT", FieldType.STRING, 8);
        hold_File_Rt_Employer_Name = hold_File.newFieldInGroup("hold_File_Rt_Employer_Name", "RT-EMPLOYER-NAME", FieldType.STRING, 40);
        hold_File_Rt_Tiaa_Rate = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Rate", "RT-TIAA-RATE", FieldType.STRING, 8);
        hold_File_Rt_Sg_Plan_Id = hold_File.newFieldInGroup("hold_File_Rt_Sg_Plan_Id", "RT-SG-PLAN-ID", FieldType.STRING, 6);
        hold_File_Rt_Tiaa_Pg4_Numb = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Pg4_Numb", "RT-TIAA-PG4-NUMB", FieldType.STRING, 15);
        hold_File_Rt_Cref_Pg4_Numb = hold_File.newFieldInGroup("hold_File_Rt_Cref_Pg4_Numb", "RT-CREF-PG4-NUMB", FieldType.STRING, 15);
        hold_File_Rt_Dflt_Access_Code = hold_File.newFieldInGroup("hold_File_Rt_Dflt_Access_Code", "RT-DFLT-ACCESS-CODE", FieldType.STRING, 9);
        hold_File_Rt_Single_Issue_Ind = hold_File.newFieldInGroup("hold_File_Rt_Single_Issue_Ind", "RT-SINGLE-ISSUE-IND", FieldType.STRING, 1);
        hold_File_Rt_Spec_Fund_Ind = hold_File.newFieldInGroup("hold_File_Rt_Spec_Fund_Ind", "RT-SPEC-FUND-IND", FieldType.STRING, 3);
        hold_File_Rt_Product_Price_Level = hold_File.newFieldInGroup("hold_File_Rt_Product_Price_Level", "RT-PRODUCT-PRICE-LEVEL", FieldType.STRING, 5);
        hold_File_Rt_Roth_Ind = hold_File.newFieldInGroup("hold_File_Rt_Roth_Ind", "RT-ROTH-IND", FieldType.STRING, 1);
        hold_File_Rt_Plan_Issue_State = hold_File.newFieldInGroup("hold_File_Rt_Plan_Issue_State", "RT-PLAN-ISSUE-STATE", FieldType.STRING, 2);
        hold_File_Rt_Client_Id = hold_File.newFieldInGroup("hold_File_Rt_Client_Id", "RT-CLIENT-ID", FieldType.STRING, 6);
        hold_File_Rt_Portfolio_Model = hold_File.newFieldInGroup("hold_File_Rt_Portfolio_Model", "RT-PORTFOLIO-MODEL", FieldType.STRING, 4);
        hold_File_Rt_Auto_Enroll_Ind = hold_File.newFieldInGroup("hold_File_Rt_Auto_Enroll_Ind", "RT-AUTO-ENROLL-IND", FieldType.STRING, 1);
        hold_File_Rt_Auto_Save_Ind = hold_File.newFieldInGroup("hold_File_Rt_Auto_Save_Ind", "RT-AUTO-SAVE-IND", FieldType.STRING, 1);
        hold_File_Rt_As_Cur_Dflt_Amt = hold_File.newFieldInGroup("hold_File_Rt_As_Cur_Dflt_Amt", "RT-AS-CUR-DFLT-AMT", FieldType.STRING, 10);
        hold_File_Rt_As_Incr_Amt = hold_File.newFieldInGroup("hold_File_Rt_As_Incr_Amt", "RT-AS-INCR-AMT", FieldType.STRING, 7);
        hold_File_Rt_As_Max_Pct = hold_File.newFieldInGroup("hold_File_Rt_As_Max_Pct", "RT-AS-MAX-PCT", FieldType.STRING, 7);
        hold_File_Rt_Ae_Opt_Out_Days = hold_File.newFieldInGroup("hold_File_Rt_Ae_Opt_Out_Days", "RT-AE-OPT-OUT-DAYS", FieldType.STRING, 2);
        hold_File_Rt_Tsv_Ind = hold_File.newFieldInGroup("hold_File_Rt_Tsv_Ind", "RT-TSV-IND", FieldType.STRING, 1);
        hold_File_Rt_Tiaa_Indx_Guarntd_Rte = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Indx_Guarntd_Rte", "RT-TIAA-INDX-GUARNTD-RTE", FieldType.STRING, 
            6);
        hold_File_Rt_Agent_Crd_No = hold_File.newFieldInGroup("hold_File_Rt_Agent_Crd_No", "RT-AGENT-CRD-NO", FieldType.STRING, 15);
        hold_File_Rt_Crd_Pull_Ind = hold_File.newFieldInGroup("hold_File_Rt_Crd_Pull_Ind", "RT-CRD-PULL-IND", FieldType.STRING, 1);
        hold_File_Rt_Agent_Name = hold_File.newFieldInGroup("hold_File_Rt_Agent_Name", "RT-AGENT-NAME", FieldType.STRING, 90);
        hold_File_Rt_Agent_Work_Addr1 = hold_File.newFieldInGroup("hold_File_Rt_Agent_Work_Addr1", "RT-AGENT-WORK-ADDR1", FieldType.STRING, 35);
        hold_File_Rt_Agent_Work_Addr2 = hold_File.newFieldInGroup("hold_File_Rt_Agent_Work_Addr2", "RT-AGENT-WORK-ADDR2", FieldType.STRING, 35);
        hold_File_Rt_Agent_Work_City = hold_File.newFieldInGroup("hold_File_Rt_Agent_Work_City", "RT-AGENT-WORK-CITY", FieldType.STRING, 35);
        hold_File_Rt_Agent_Work_State = hold_File.newFieldInGroup("hold_File_Rt_Agent_Work_State", "RT-AGENT-WORK-STATE", FieldType.STRING, 2);
        hold_File_Rt_Agent_Work_Zip = hold_File.newFieldInGroup("hold_File_Rt_Agent_Work_Zip", "RT-AGENT-WORK-ZIP", FieldType.STRING, 10);
        hold_File_Rt_Agent_Work_Phone = hold_File.newFieldInGroup("hold_File_Rt_Agent_Work_Phone", "RT-AGENT-WORK-PHONE", FieldType.STRING, 25);
        hold_File_Rt_Tic_Ind = hold_File.newFieldInGroup("hold_File_Rt_Tic_Ind", "RT-TIC-IND", FieldType.STRING, 1);
        hold_File_Rt_Tic_Startdate = hold_File.newFieldInGroup("hold_File_Rt_Tic_Startdate", "RT-TIC-STARTDATE", FieldType.STRING, 8);
        hold_File_Rt_Tic_Enddate = hold_File.newFieldInGroup("hold_File_Rt_Tic_Enddate", "RT-TIC-ENDDATE", FieldType.STRING, 8);
        hold_File_Rt_Tic_Percentage = hold_File.newFieldInGroup("hold_File_Rt_Tic_Percentage", "RT-TIC-PERCENTAGE", FieldType.STRING, 9);
        hold_File_Rt_Tic_Postdays = hold_File.newFieldInGroup("hold_File_Rt_Tic_Postdays", "RT-TIC-POSTDAYS", FieldType.STRING, 2);
        hold_File_Rt_Tic_Limit = hold_File.newFieldInGroup("hold_File_Rt_Tic_Limit", "RT-TIC-LIMIT", FieldType.STRING, 11);
        hold_File_Rt_Tic_Postfreq = hold_File.newFieldInGroup("hold_File_Rt_Tic_Postfreq", "RT-TIC-POSTFREQ", FieldType.STRING, 1);
        hold_File_Rt_Tic_Pl_Level = hold_File.newFieldInGroup("hold_File_Rt_Tic_Pl_Level", "RT-TIC-PL-LEVEL", FieldType.STRING, 1);
        hold_File_Rt_Tic_Windowdays = hold_File.newFieldInGroup("hold_File_Rt_Tic_Windowdays", "RT-TIC-WINDOWDAYS", FieldType.STRING, 3);
        hold_File_Rt_Tic_Reqdlywindow = hold_File.newFieldInGroup("hold_File_Rt_Tic_Reqdlywindow", "RT-TIC-REQDLYWINDOW", FieldType.STRING, 3);
        hold_File_Rt_Tic_Recap_Prov = hold_File.newFieldInGroup("hold_File_Rt_Tic_Recap_Prov", "RT-TIC-RECAP-PROV", FieldType.STRING, 3);
        hold_File_Rt_Ecs_Dcs_E_Dlvry_Ind = hold_File.newFieldInGroup("hold_File_Rt_Ecs_Dcs_E_Dlvry_Ind", "RT-ECS-DCS-E-DLVRY-IND", FieldType.STRING, 1);
        hold_File_Rt_Ecs_Dcs_Annty_Optn_Ind = hold_File.newFieldInGroup("hold_File_Rt_Ecs_Dcs_Annty_Optn_Ind", "RT-ECS-DCS-ANNTY-OPTN-IND", FieldType.STRING, 
            1);
        hold_File_Rt_Full_Middle_Name = hold_File.newFieldInGroup("hold_File_Rt_Full_Middle_Name", "RT-FULL-MIDDLE-NAME", FieldType.STRING, 30);
        hold_File_Rt_Substitution_Contract_Ind = hold_File.newFieldInGroup("hold_File_Rt_Substitution_Contract_Ind", "RT-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 
            1);
        hold_File_Rt_Bundle_Type = hold_File.newFieldInGroup("hold_File_Rt_Bundle_Type", "RT-BUNDLE-TYPE", FieldType.STRING, 1);
        hold_File_Rt_Bundle_Record_Type = hold_File.newFieldInGroup("hold_File_Rt_Bundle_Record_Type", "RT-BUNDLE-RECORD-TYPE", FieldType.STRING, 1);
        hold_File_Rt_Bundle_Number = hold_File.newFieldInGroup("hold_File_Rt_Bundle_Number", "RT-BUNDLE-NUMBER", FieldType.NUMERIC, 9);
        hold_File_Rt_Cover_Letter_Contracts = hold_File.newFieldInGroup("hold_File_Rt_Cover_Letter_Contracts", "RT-COVER-LETTER-CONTRACTS", FieldType.STRING, 
            40);

        hold_File__R_Field_14 = hold_File.newGroupInGroup("hold_File__R_Field_14", "REDEFINE", hold_File_Rt_Cover_Letter_Contracts);
        hold_File_Rt_Cl_Tiaa_Contract_1 = hold_File__R_Field_14.newFieldInGroup("hold_File_Rt_Cl_Tiaa_Contract_1", "RT-CL-TIAA-CONTRACT-1", FieldType.STRING, 
            10);
        hold_File_Rt_Cl_Tiaa_Contract_2 = hold_File__R_Field_14.newFieldInGroup("hold_File_Rt_Cl_Tiaa_Contract_2", "RT-CL-TIAA-CONTRACT-2", FieldType.STRING, 
            10);
        hold_File_Rt_Cl_Tiaa_Contract_3 = hold_File__R_Field_14.newFieldInGroup("hold_File_Rt_Cl_Tiaa_Contract_3", "RT-CL-TIAA-CONTRACT-3", FieldType.STRING, 
            10);
        hold_File_Rt_Cl_Tiaa_Contract_4 = hold_File__R_Field_14.newFieldInGroup("hold_File_Rt_Cl_Tiaa_Contract_4", "RT-CL-TIAA-CONTRACT-4", FieldType.STRING, 
            10);
        hold_File_Rt_Non_Proprietary_Pkg_Ind = hold_File.newFieldInGroup("hold_File_Rt_Non_Proprietary_Pkg_Ind", "RT-NON-PROPRIETARY-PKG-IND", FieldType.STRING, 
            5);
        hold_File_Rt_Multi_Plan_Table = hold_File.newFieldArrayInGroup("hold_File_Rt_Multi_Plan_Table", "RT-MULTI-PLAN-TABLE", FieldType.STRING, 12, new 
            DbsArrayController(1, 15));

        hold_File__R_Field_15 = hold_File.newGroupInGroup("hold_File__R_Field_15", "REDEFINE", hold_File_Rt_Multi_Plan_Table);

        hold_File_Rt_Multi_Plan_Info = hold_File__R_Field_15.newGroupArrayInGroup("hold_File_Rt_Multi_Plan_Info", "RT-MULTI-PLAN-INFO", new DbsArrayController(1, 
            15));
        hold_File_Rt_Multi_Plan_No = hold_File_Rt_Multi_Plan_Info.newFieldInGroup("hold_File_Rt_Multi_Plan_No", "RT-MULTI-PLAN-NO", FieldType.STRING, 
            6);
        hold_File_Rt_Multi_Sub_Plan = hold_File_Rt_Multi_Plan_Info.newFieldInGroup("hold_File_Rt_Multi_Sub_Plan", "RT-MULTI-SUB-PLAN", FieldType.STRING, 
            6);
        hold_File_Rt_Filler = hold_File.newFieldInGroup("hold_File_Rt_Filler", "RT-FILLER", FieldType.STRING, 15);
        hold_File_Rt_Tiaa_Init_Prem = hold_File.newFieldInGroup("hold_File_Rt_Tiaa_Init_Prem", "RT-TIAA-INIT-PREM", FieldType.NUMERIC, 10, 2);
        hold_File_Rt_Cref_Init_Prem = hold_File.newFieldInGroup("hold_File_Rt_Cref_Init_Prem", "RT-CREF-INIT-PREM", FieldType.NUMERIC, 10, 2);
        hold_File_Rt_Related_Contract_Info_Table = hold_File.newFieldArrayInGroup("hold_File_Rt_Related_Contract_Info_Table", "RT-RELATED-CONTRACT-INFO-TABLE", 
            FieldType.STRING, 49, new DbsArrayController(1, 30));

        hold_File__R_Field_16 = hold_File.newGroupInGroup("hold_File__R_Field_16", "REDEFINE", hold_File_Rt_Related_Contract_Info_Table);

        hold_File_Rt_Related_Contract_Info = hold_File__R_Field_16.newGroupArrayInGroup("hold_File_Rt_Related_Contract_Info", "RT-RELATED-CONTRACT-INFO", 
            new DbsArrayController(1, 30));
        hold_File_Rt_Related_Contract_Type = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_Contract_Type", "RT-RELATED-CONTRACT-TYPE", 
            FieldType.STRING, 1);
        hold_File_Rt_Related_Tiaa_No = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_Tiaa_No", "RT-RELATED-TIAA-NO", FieldType.STRING, 
            10);
        hold_File_Rt_Related_Tiaa_Issue_Date = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_Tiaa_Issue_Date", "RT-RELATED-TIAA-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        hold_File_Rt_Related_First_Payment_Date = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_First_Payment_Date", "RT-RELATED-FIRST-PAYMENT-DATE", 
            FieldType.NUMERIC, 8);
        hold_File_Rt_Related_Tiaa_Total_Amt = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_Tiaa_Total_Amt", "RT-RELATED-TIAA-TOTAL-AMT", 
            FieldType.NUMERIC, 10, 2);
        hold_File_Rt_Related_Last_Payment_Date = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_Last_Payment_Date", "RT-RELATED-LAST-PAYMENT-DATE", 
            FieldType.NUMERIC, 8);
        hold_File_Rt_Related_Payment_Frequency = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_Payment_Frequency", "RT-RELATED-PAYMENT-FREQUENCY", 
            FieldType.STRING, 1);
        hold_File_Rt_Related_Tiaa_Issue_State = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_Related_Tiaa_Issue_State", "RT-RELATED-TIAA-ISSUE-STATE", 
            FieldType.STRING, 2);
        hold_File_Rt_First_Tpa_Ipro_Ind = hold_File_Rt_Related_Contract_Info.newFieldInGroup("hold_File_Rt_First_Tpa_Ipro_Ind", "RT-FIRST-TPA-IPRO-IND", 
            FieldType.STRING, 1);
        hold_File_Rt_Fund_Source_1 = hold_File.newFieldInGroup("hold_File_Rt_Fund_Source_1", "RT-FUND-SOURCE-1", FieldType.STRING, 1);
        hold_File_Rt_Fund_Source_2 = hold_File.newFieldInGroup("hold_File_Rt_Fund_Source_2", "RT-FUND-SOURCE-2", FieldType.STRING, 1);
        hold_File_Rt_Fund_Cde_2 = hold_File.newFieldArrayInGroup("hold_File_Rt_Fund_Cde_2", "RT-FUND-CDE-2", FieldType.STRING, 10, new DbsArrayController(1, 
            100));
        hold_File_Rt_Alloc_Pct_2 = hold_File.newFieldArrayInGroup("hold_File_Rt_Alloc_Pct_2", "RT-ALLOC-PCT-2", FieldType.STRING, 3, new DbsArrayController(1, 
            100));
        pnd_Ctr = localVariables.newFieldInRecord("pnd_Ctr", "#CTR", FieldType.NUMERIC, 1);
        pnd_Hold_Profile = localVariables.newFieldInRecord("pnd_Hold_Profile", "#HOLD-PROFILE", FieldType.STRING, 2);
        pnd_Hold_Ssn = localVariables.newFieldInRecord("pnd_Hold_Ssn", "#HOLD-SSN", FieldType.STRING, 11);
        pnd_Hold_Bundle_Type = localVariables.newFieldInRecord("pnd_Hold_Bundle_Type", "#HOLD-BUNDLE-TYPE", FieldType.STRING, 1);
        pnd_Contract = localVariables.newFieldInRecord("pnd_Contract", "#CONTRACT", FieldType.STRING, 10);

        pnd_Contract__R_Field_17 = localVariables.newGroupInRecord("pnd_Contract__R_Field_17", "REDEFINE", pnd_Contract);
        pnd_Contract_Pnd_Tiaa_No_1 = pnd_Contract__R_Field_17.newFieldInGroup("pnd_Contract_Pnd_Tiaa_No_1", "#TIAA-NO-1", FieldType.STRING, 8);
        pnd_Contract_Pnd_Tiaa_Fill_1 = pnd_Contract__R_Field_17.newFieldInGroup("pnd_Contract_Pnd_Tiaa_Fill_1", "#TIAA-FILL-1", FieldType.STRING, 2);
        pnd_Bundle_Number = localVariables.newFieldInRecord("pnd_Bundle_Number", "#BUNDLE-NUMBER", FieldType.NUMERIC, 10);
        pnd_Rec_Ctr = localVariables.newFieldInRecord("pnd_Rec_Ctr", "#REC-CTR", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl1152.initializeValues();
        ldaAppl1158.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb1158() throws Exception
    {
        super("Appb1158");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *
        READWORK01:                                                                                                                                                       //Natural: FORMAT PS = 50 LS = 132;//Natural: READ WORK FILE 1 RECORD-ID-EX-FILE-3
        while (condition(getWorkFiles().read(1, ldaAppl1158.getRecord_Id_Ex_File_3())))
        {
            pnd_Rec_Ctr.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CTR
            if (condition(ldaAppl1158.getRecord_Id_Ex_File_3_Hd_Output_Profile().notEquals(pnd_Hold_Profile) || ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bundle_Type().notEquals(pnd_Hold_Bundle_Type)  //Natural: IF RECORD-ID-EX-FILE-3.HD-OUTPUT-PROFILE NE #HOLD-PROFILE OR RECORD-ID-EX-FILE-3.RT-BUNDLE-TYPE NE #HOLD-BUNDLE-TYPE OR RECORD-ID-EX-FILE-3.RT-SOC-SEC NE #HOLD-SSN OR RECORD-ID-EX-FILE-3.RT-BUNDLE-TYPE = 'R'
                || ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Soc_Sec().notEquals(pnd_Hold_Ssn) || ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bundle_Type().equals("R")))
            {
                if (condition(hold_File_Rt_Bundle_Record_Type.notEquals(" ")))                                                                                            //Natural: IF HOLD-FILE.RT-BUNDLE-RECORD-TYPE NE ' '
                {
                    getWorkFiles().write(2, false, hold_File);                                                                                                            //Natural: WRITE WORK FILE 2 HOLD-FILE
                    hold_File.reset();                                                                                                                                    //Natural: RESET HOLD-FILE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr.setValue(1);                                                                                                                                      //Natural: ASSIGN #CTR = 1
                hold_File.setValuesByName(ldaAppl1158.getRecord_Id_Ex_File_3());                                                                                          //Natural: MOVE BY NAME RECORD-ID-EX-FILE-3 TO HOLD-FILE
                hold_File_Rt_Bundle_Record_Type.setValue("M");                                                                                                            //Natural: ASSIGN HOLD-FILE.RT-BUNDLE-RECORD-TYPE = 'M'
                hold_File_Rt_Cover_Letter_Contracts.reset();                                                                                                              //Natural: RESET HOLD-FILE.RT-COVER-LETTER-CONTRACTS
                if (condition(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bundle_Type().equals("I")))                                                                           //Natural: IF RECORD-ID-EX-FILE-3.RT-BUNDLE-TYPE = 'I'
                {
                    pnd_Bundle_Number.setValue(1);                                                                                                                        //Natural: ASSIGN #BUNDLE-NUMBER = 1
                    //*  TNGSUB
                    pnd_Contract_Pnd_Tiaa_No_1.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_No_2());                                                               //Natural: ASSIGN #TIAA-NO-1 = RECORD-ID-EX-FILE-3.RT-TIAA-NO-2
                    //*  TNGSUB
                    pnd_Contract_Pnd_Tiaa_Fill_1.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Fill_2());                                                           //Natural: ASSIGN #TIAA-FILL-1 = RECORD-ID-EX-FILE-3.RT-TIAA-FILL-2
                    hold_File_Rt_Cl_Tiaa_Contract_1.setValue(pnd_Contract);                                                                                               //Natural: ASSIGN HOLD-FILE.RT-CL-TIAA-CONTRACT-1 = #CONTRACT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAppl1158.getRecord_Id_Ex_File_3_Hd_Output_Profile().notEquals(pnd_Hold_Profile)))                                                    //Natural: IF RECORD-ID-EX-FILE-3.HD-OUTPUT-PROFILE NE #HOLD-PROFILE
                    {
                        pnd_Bundle_Number.setValue(1);                                                                                                                    //Natural: ASSIGN #BUNDLE-NUMBER = 1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Bundle_Number.nadd(1);                                                                                                                        //Natural: ADD 1 TO #BUNDLE-NUMBER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                hold_File_Rt_Bundle_Number.setValue(pnd_Bundle_Number);                                                                                                   //Natural: ASSIGN HOLD-FILE.RT-BUNDLE-NUMBER = #BUNDLE-NUMBER
                pnd_Hold_Profile.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Hd_Output_Profile());                                                                        //Natural: ASSIGN #HOLD-PROFILE = RECORD-ID-EX-FILE-3.HD-OUTPUT-PROFILE
                pnd_Hold_Ssn.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Soc_Sec());                                                                                   //Natural: ASSIGN #HOLD-SSN = RECORD-ID-EX-FILE-3.RT-SOC-SEC
                pnd_Hold_Bundle_Type.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Bundle_Type());                                                                       //Natural: ASSIGN #HOLD-BUNDLE-TYPE = RECORD-ID-EX-FILE-3.RT-BUNDLE-TYPE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #CTR
            if (condition(pnd_Ctr.greater(4)))                                                                                                                            //Natural: IF #CTR GT 4
            {
                getWorkFiles().write(2, false, hold_File);                                                                                                                //Natural: WRITE WORK FILE 2 HOLD-FILE
                hold_File.reset();                                                                                                                                        //Natural: RESET HOLD-FILE
                pnd_Bundle_Number.nadd(1);                                                                                                                                //Natural: ADD 1 TO #BUNDLE-NUMBER
                hold_File.setValuesByName(ldaAppl1158.getRecord_Id_Ex_File_3());                                                                                          //Natural: MOVE BY NAME RECORD-ID-EX-FILE-3 TO HOLD-FILE
                hold_File_Rt_Bundle_Number.setValue(pnd_Bundle_Number);                                                                                                   //Natural: ASSIGN HOLD-FILE.RT-BUNDLE-NUMBER = #BUNDLE-NUMBER
                hold_File_Rt_Bundle_Record_Type.setValue("M");                                                                                                            //Natural: ASSIGN HOLD-FILE.RT-BUNDLE-RECORD-TYPE = 'M'
                //*  TNGSUB
                pnd_Contract_Pnd_Tiaa_No_1.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_No_2());                                                                   //Natural: ASSIGN #TIAA-NO-1 = RECORD-ID-EX-FILE-3.RT-TIAA-NO-2
                //*  TNGSUB
                pnd_Contract_Pnd_Tiaa_Fill_1.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Fill_2());                                                               //Natural: ASSIGN #TIAA-FILL-1 = RECORD-ID-EX-FILE-3.RT-TIAA-FILL-2
                hold_File_Rt_Cl_Tiaa_Contract_1.setValue(pnd_Contract);                                                                                                   //Natural: ASSIGN HOLD-FILE.RT-CL-TIAA-CONTRACT-1 = #CONTRACT
                pnd_Ctr.setValue(1);                                                                                                                                      //Natural: ASSIGN #CTR = 1
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl1152.getRecord_Id_Ex_File().setValuesByName(ldaAppl1158.getRecord_Id_Ex_File_3());                                                                     //Natural: MOVE BY NAME RECORD-ID-EX-FILE-3 TO RECORD-ID-EX-FILE
            ldaAppl1152.getRecord_Id_Ex_File_Rt_Bundle_Number().setValue(pnd_Bundle_Number);                                                                              //Natural: ASSIGN RECORD-ID-EX-FILE.RT-BUNDLE-NUMBER = #BUNDLE-NUMBER
            ldaAppl1152.getRecord_Id_Ex_File_Rt_Bundle_Record_Type().setValue("S");                                                                                       //Natural: ASSIGN RECORD-ID-EX-FILE.RT-BUNDLE-RECORD-TYPE = 'S'
            //*  TNGSUB
            pnd_Contract_Pnd_Tiaa_No_1.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_No_2());                                                                       //Natural: ASSIGN #TIAA-NO-1 = RECORD-ID-EX-FILE-3.RT-TIAA-NO-2
            //*  TNGSUB
            pnd_Contract_Pnd_Tiaa_Fill_1.setValue(ldaAppl1158.getRecord_Id_Ex_File_3_Rt_Tiaa_Fill_2());                                                                   //Natural: ASSIGN #TIAA-FILL-1 = RECORD-ID-EX-FILE-3.RT-TIAA-FILL-2
            if (condition(pnd_Ctr.equals(2)))                                                                                                                             //Natural: IF #CTR = 2
            {
                hold_File_Rt_Cl_Tiaa_Contract_2.setValue(pnd_Contract);                                                                                                   //Natural: ASSIGN HOLD-FILE.RT-CL-TIAA-CONTRACT-2 = #CONTRACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ctr.equals(3)))                                                                                                                         //Natural: IF #CTR = 3
                {
                    hold_File_Rt_Cl_Tiaa_Contract_3.setValue(pnd_Contract);                                                                                               //Natural: ASSIGN HOLD-FILE.RT-CL-TIAA-CONTRACT-3 = #CONTRACT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    hold_File_Rt_Cl_Tiaa_Contract_4.setValue(pnd_Contract);                                                                                               //Natural: ASSIGN HOLD-FILE.RT-CL-TIAA-CONTRACT-4 = #CONTRACT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, ldaAppl1152.getRecord_Id_Ex_File());                                                                                           //Natural: WRITE WORK FILE 2 RECORD-ID-EX-FILE
            ldaAppl1152.getRecord_Id_Ex_File().reset();                                                                                                                   //Natural: RESET RECORD-ID-EX-FILE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getWorkFiles().write(2, false, hold_File);                                                                                                                        //Natural: WRITE WORK FILE 2 HOLD-FILE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=50 LS=132");
    }
}
