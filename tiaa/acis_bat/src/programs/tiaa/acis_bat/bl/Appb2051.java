/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:39 PM
**        * FROM NATURAL PROGRAM : Appb2051
************************************************************
**        * FILE NAME            : Appb2051.java
**        * CLASS NAME           : Appb2051
**        * INSTANCE NAME        : Appb2051
************************************************************
************************************************************************
* APPB2051 - DAILY PRAP EXTRACT OF PREMIUM RECORDS THAT HAVE NOT BEEN  *
*            MATCHED AND RELEASED (FOR RA, GRA, SRA, GSRA ONLY)        *
************************************************************************
*
************************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                     *
* ----------  ----------------  -------------------------------------- *
* 01/08/2008  JANET BERGHEISER  NEW                                    *
* 06/20/17  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.    *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2051 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_prap;
    private DbsField prap_Ap_Tiaa_Cntrct;

    private DbsGroup prap__R_Field_1;
    private DbsField prap_Pnd_Contract_Number;
    private DbsField prap_Ap_Cor_Last_Nme;

    private DbsGroup prap__R_Field_2;
    private DbsField prap_Pnd_Last_Name;
    private DbsField prap_Ap_Cor_First_Nme;

    private DbsGroup prap__R_Field_3;
    private DbsField prap_Pnd_First_Name;
    private DbsField prap_Ap_Coll_Code;
    private DbsField prap_Ap_Soc_Sec;
    private DbsField prap_Ap_Dob;
    private DbsField prap_Ap_Lob;
    private DbsField prap_Ap_Lob_Type;
    private DbsField prap_Ap_Record_Type;
    private DbsField prap_Ap_Status;
    private DbsField prap_Ap_Release_Ind;
    private DbsField pnd_Contract_Type;
    private DbsField pnd_Contract_Type_Desc;
    private DbsField pnd_Record_Type;
    private DbsField pnd_Contract_Status;
    private DbsField pnd_Ssn_N;

    private DbsGroup pnd_Ssn_N__R_Field_4;
    private DbsField pnd_Ssn_N_Pnd_Ssn_A;
    private DbsField pnd_Birthdate_N;

    private DbsGroup pnd_Birthdate_N__R_Field_5;
    private DbsField pnd_Birthdate_N_Pnd_Birthdate_A;
    private DbsField pnd_Comma;
    private DbsField pnd_Extract_File;
    private DbsField pnd_Extract_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_prap = new DataAccessProgramView(new NameInfo("vw_prap", "PRAP"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap_Ap_Tiaa_Cntrct = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");

        prap__R_Field_1 = vw_prap.getRecord().newGroupInGroup("prap__R_Field_1", "REDEFINE", prap_Ap_Tiaa_Cntrct);
        prap_Pnd_Contract_Number = prap__R_Field_1.newFieldInGroup("prap_Pnd_Contract_Number", "#CONTRACT-NUMBER", FieldType.STRING, 8);
        prap_Ap_Cor_Last_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");

        prap__R_Field_2 = vw_prap.getRecord().newGroupInGroup("prap__R_Field_2", "REDEFINE", prap_Ap_Cor_Last_Nme);
        prap_Pnd_Last_Name = prap__R_Field_2.newFieldInGroup("prap_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 10);
        prap_Ap_Cor_First_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_FIRST_NME");

        prap__R_Field_3 = vw_prap.getRecord().newGroupInGroup("prap__R_Field_3", "REDEFINE", prap_Ap_Cor_First_Nme);
        prap_Pnd_First_Name = prap__R_Field_3.newFieldInGroup("prap_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 10);
        prap_Ap_Coll_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_Ap_Soc_Sec = vw_prap.getRecord().newFieldInGroup("prap_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_Ap_Dob = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DOB");
        prap_Ap_Lob = vw_prap.getRecord().newFieldInGroup("prap_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB");
        prap_Ap_Lob.setDdmHeader("LOB");
        prap_Ap_Lob_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        prap_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        prap_Ap_Record_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_Ap_Status = vw_prap.getRecord().newFieldInGroup("prap_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_STATUS");
        prap_Ap_Release_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        registerRecord(vw_prap);

        pnd_Contract_Type = localVariables.newFieldInRecord("pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 2);
        pnd_Contract_Type_Desc = localVariables.newFieldInRecord("pnd_Contract_Type_Desc", "#CONTRACT-TYPE-DESC", FieldType.STRING, 6);
        pnd_Record_Type = localVariables.newFieldInRecord("pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 1);
        pnd_Contract_Status = localVariables.newFieldInRecord("pnd_Contract_Status", "#CONTRACT-STATUS", FieldType.STRING, 6);
        pnd_Ssn_N = localVariables.newFieldInRecord("pnd_Ssn_N", "#SSN-N", FieldType.NUMERIC, 9);

        pnd_Ssn_N__R_Field_4 = localVariables.newGroupInRecord("pnd_Ssn_N__R_Field_4", "REDEFINE", pnd_Ssn_N);
        pnd_Ssn_N_Pnd_Ssn_A = pnd_Ssn_N__R_Field_4.newFieldInGroup("pnd_Ssn_N_Pnd_Ssn_A", "#SSN-A", FieldType.STRING, 9);
        pnd_Birthdate_N = localVariables.newFieldInRecord("pnd_Birthdate_N", "#BIRTHDATE-N", FieldType.NUMERIC, 8);

        pnd_Birthdate_N__R_Field_5 = localVariables.newGroupInRecord("pnd_Birthdate_N__R_Field_5", "REDEFINE", pnd_Birthdate_N);
        pnd_Birthdate_N_Pnd_Birthdate_A = pnd_Birthdate_N__R_Field_5.newFieldInGroup("pnd_Birthdate_N_Pnd_Birthdate_A", "#BIRTHDATE-A", FieldType.STRING, 
            8);
        pnd_Comma = localVariables.newFieldInRecord("pnd_Comma", "#COMMA", FieldType.STRING, 1);
        pnd_Extract_File = localVariables.newFieldInRecord("pnd_Extract_File", "#EXTRACT-FILE", FieldType.STRING, 80);
        pnd_Extract_Count = localVariables.newFieldInRecord("pnd_Extract_Count", "#EXTRACT-COUNT", FieldType.NUMERIC, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap.reset();

        localVariables.reset();
        pnd_Comma.setInitialValue(",");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2051() throws Exception
    {
        super("Appb2051");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT LS = 80 PS = 55
        //*  PHYSICAL SEQUENCE
        vw_prap.startDatabaseRead                                                                                                                                         //Natural: READ PRAP
        (
        "READ01",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ01:
        while (condition(vw_prap.readNextRow("READ01")))
        {
            if (condition(prap_Ap_Record_Type.notEquals(2)))                                                                                                              //Natural: REJECT IF AP-RECORD-TYPE NE 2
            {
                continue;
            }
            if (condition(prap_Ap_Status.equals("C")))                                                                                                                    //Natural: REJECT IF AP-STATUS = 'C'
            {
                continue;
            }
            pnd_Contract_Type.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, prap_Ap_Lob, prap_Ap_Lob_Type));                                                   //Natural: COMPRESS AP-LOB AP-LOB-TYPE INTO #CONTRACT-TYPE LEAVING NO SPACE
            pnd_Ssn_N.setValue(prap_Ap_Soc_Sec);                                                                                                                          //Natural: ASSIGN #SSN-N := AP-SOC-SEC
            pnd_Birthdate_N.setValue(prap_Ap_Dob);                                                                                                                        //Natural: ASSIGN #BIRTHDATE-N := AP-DOB
            short decideConditionsMet71 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF AP-RECORD-TYPE;//Natural: VALUE 1
            if (condition((prap_Ap_Record_Type.equals(1))))
            {
                decideConditionsMet71++;
                pnd_Record_Type.setValue("A");                                                                                                                            //Natural: ASSIGN #RECORD-TYPE := 'A'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((prap_Ap_Record_Type.equals(2))))
            {
                decideConditionsMet71++;
                pnd_Record_Type.setValue("P");                                                                                                                            //Natural: ASSIGN #RECORD-TYPE := 'P'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((prap_Ap_Record_Type.equals(3))))
            {
                decideConditionsMet71++;
                pnd_Record_Type.setValue("R");                                                                                                                            //Natural: ASSIGN #RECORD-TYPE := 'R'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Record_Type.setValue(prap_Ap_Record_Type);                                                                                                            //Natural: ASSIGN #RECORD-TYPE := AP-RECORD-TYPE
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet82 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF #CONTRACT-TYPE;//Natural: VALUE 'D2'
            if (condition((pnd_Contract_Type.equals("D2"))))
            {
                decideConditionsMet82++;
                pnd_Contract_Type_Desc.setValue("RA");                                                                                                                    //Natural: ASSIGN #CONTRACT-TYPE-DESC := 'RA'
            }                                                                                                                                                             //Natural: VALUE 'D7'
            else if (condition((pnd_Contract_Type.equals("D7"))))
            {
                decideConditionsMet82++;
                pnd_Contract_Type_Desc.setValue("GRA");                                                                                                                   //Natural: ASSIGN #CONTRACT-TYPE-DESC := 'GRA'
                //* *  VALUE 'D5'
                //* *    #CONTRACT-TYPE-DESC := 'RS'
                //* *  VALUE 'D6'
                //* *    #CONTRACT-TYPE-DESC := 'RL'
                //* *  VALUE 'DA'
                //* *    #CONTRACT-TYPE-DESC := 'RC'
            }                                                                                                                                                             //Natural: VALUE 'S2'
            else if (condition((pnd_Contract_Type.equals("S2"))))
            {
                decideConditionsMet82++;
                pnd_Contract_Type_Desc.setValue("SRA");                                                                                                                   //Natural: ASSIGN #CONTRACT-TYPE-DESC := 'SRA'
            }                                                                                                                                                             //Natural: VALUE 'S3'
            else if (condition((pnd_Contract_Type.equals("S3"))))
            {
                decideConditionsMet82++;
                pnd_Contract_Type_Desc.setValue("GSRA");                                                                                                                  //Natural: ASSIGN #CONTRACT-TYPE-DESC := 'GSRA'
                //* *  VALUE 'S4'
                //* *    #CONTRACT-TYPE-DESC := 'GA'
                //* *  VALUE 'S5'
                //* *    #CONTRACT-TYPE-DESC := 'RSP'
                //* *  VALUE 'S6'
                //* *    #CONTRACT-TYPE-DESC := 'RSP2'
                //* *  VALUE 'S7'
                //* *    #CONTRACT-TYPE-DESC := 'RCP'
                //* *  VALUE 'I1','I2'
                //* *    #CONTRACT-TYPE-DESC := 'IRA'
                //* *  VALUE 'I3'
                //* *    #CONTRACT-TYPE-DESC := 'IRAR'
                //* *  VALUE 'I4'
                //* *    #CONTRACT-TYPE-DESC := 'IRAC'
                //* *  VALUE 'I6'
                //* *    #CONTRACT-TYPE-DESC := 'IRAS'
                //* *  VALUE 'I5'
                //* *    #CONTRACT-TYPE-DESC := 'KEOGH'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                //* *    #CONTRACT-TYPE-DESC := #CONTRACT-TYPE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet120 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF AP-STATUS;//Natural: VALUE 'A'
            if (condition((prap_Ap_Status.equals("A"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("DEL");                                                                                                                      //Natural: ASSIGN #CONTRACT-STATUS := 'DEL'
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((prap_Ap_Status.equals("B"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("ASSGN");                                                                                                                    //Natural: ASSIGN #CONTRACT-STATUS := 'ASSGN'
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((prap_Ap_Status.equals("C"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("M & R");                                                                                                                    //Natural: ASSIGN #CONTRACT-STATUS := 'M & R'
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((prap_Ap_Status.equals("D"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("REL");                                                                                                                      //Natural: ASSIGN #CONTRACT-STATUS := 'REL'
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((prap_Ap_Status.equals("E"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("UNASG");                                                                                                                    //Natural: ASSIGN #CONTRACT-STATUS := 'UNASG'
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((prap_Ap_Status.equals("F"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("SUSPN");                                                                                                                    //Natural: ASSIGN #CONTRACT-STATUS := 'SUSPN'
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((prap_Ap_Status.equals("G"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("OVD 1");                                                                                                                    //Natural: ASSIGN #CONTRACT-STATUS := 'OVD 1'
            }                                                                                                                                                             //Natural: VALUE 'H'
            else if (condition((prap_Ap_Status.equals("H"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("OVD 2");                                                                                                                    //Natural: ASSIGN #CONTRACT-STATUS := 'OVD 2'
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((prap_Ap_Status.equals("I"))))
            {
                decideConditionsMet120++;
                pnd_Contract_Status.setValue("OVD 3");                                                                                                                    //Natural: ASSIGN #CONTRACT-STATUS := 'OVD 3'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Contract_Status.setValue(prap_Ap_Status);                                                                                                             //Natural: ASSIGN #CONTRACT-STATUS := AP-STATUS
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(1, "CONTRACT/NUMBER",                                                                                                                    //Natural: DISPLAY ( 1 ) 'CONTRACT/NUMBER' #CONTRACT-NUMBER 'CONTRACT/TYPE' #CONTRACT-TYPE-DESC 'PPG/CODE' AP-COLL-CODE 'LAST/NAME' #LAST-NAME 'FIRT/NAME' #FIRST-NAME 'BIRTH/DATE' #BIRTHDATE-A 'SSN/NUMBER' #SSN-A 'REC/TYP' #RECORD-TYPE '/STATUS' #CONTRACT-STATUS
            		prap_Pnd_Contract_Number,"CONTRACT/TYPE",
            		pnd_Contract_Type_Desc,"PPG/CODE",
            		prap_Ap_Coll_Code,"LAST/NAME",
            		prap_Pnd_Last_Name,"FIRT/NAME",
            		prap_Pnd_First_Name,"BIRTH/DATE",
            		pnd_Birthdate_N_Pnd_Birthdate_A,"SSN/NUMBER",
            		pnd_Ssn_N_Pnd_Ssn_A,"REC/TYP",
            		pnd_Record_Type,"/STATUS",
            		pnd_Contract_Status);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Extract_File.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, prap_Pnd_Contract_Number, pnd_Comma, pnd_Contract_Type, pnd_Comma,                  //Natural: COMPRESS #CONTRACT-NUMBER #COMMA #CONTRACT-TYPE #COMMA #CONTRACT-TYPE-DESC #COMMA AP-COLL-CODE #COMMA #LAST-NAME #COMMA #FIRST-NAME #COMMA #BIRTHDATE-A #COMMA #SSN-A #COMMA AP-RECORD-TYPE #COMMA #RECORD-TYPE #COMMA AP-STATUS #COMMA #CONTRACT-STATUS INTO #EXTRACT-FILE LEAVING NO SPACE
                pnd_Contract_Type_Desc, pnd_Comma, prap_Ap_Coll_Code, pnd_Comma, prap_Pnd_Last_Name, pnd_Comma, prap_Pnd_First_Name, pnd_Comma, pnd_Birthdate_N_Pnd_Birthdate_A, 
                pnd_Comma, pnd_Ssn_N_Pnd_Ssn_A, pnd_Comma, prap_Ap_Record_Type, pnd_Comma, pnd_Record_Type, pnd_Comma, prap_Ap_Status, pnd_Comma, pnd_Contract_Status));
            getWorkFiles().write(1, false, pnd_Extract_File);                                                                                                             //Natural: WRITE WORK FILE 01 #EXTRACT-FILE
            pnd_Extract_Count.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #EXTRACT-COUNT
            //*  (0590)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, "RECORDS EXTRACTED:",pnd_Extract_Count, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZ9"));                                                            //Natural: WRITE ( 1 ) 'RECORDS EXTRACTED:' #EXTRACT-COUNT ( EM = ZZZ,ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=55");

        getReports().setDisplayColumns(1, "CONTRACT/NUMBER",
        		prap_Pnd_Contract_Number,"CONTRACT/TYPE",
        		pnd_Contract_Type_Desc,"PPG/CODE",
        		prap_Ap_Coll_Code,"LAST/NAME",
        		prap_Pnd_Last_Name,"FIRT/NAME",
        		prap_Pnd_First_Name,"BIRTH/DATE",
        		pnd_Birthdate_N_Pnd_Birthdate_A,"SSN/NUMBER",
        		pnd_Ssn_N_Pnd_Ssn_A,"REC/TYP",
        		pnd_Record_Type,"/STATUS",
        		pnd_Contract_Status);
    }
}
