/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:01 PM
**        * FROM NATURAL PROGRAM : Scib9100
************************************************************
**        * FILE NAME            : Scib9100.java
**        * CLASS NAME           : Scib9100
**        * INSTANCE NAME        : Scib9100
************************************************************
************************************************************************
* PROGRAM  : SCIB9100
* SYSTEM   : CONTRIBUTION PROCESSING
* GENERATED: MAR 2004
* FUNCTION : THIS PROGRAM READS THE CONFIRMATION FILE OF ALL PARTICPANTS
*            WHO WERE APPLIED THEIR FIRST PREMIUM AND CALLS ACIS TO
*            MATCH AND RELEASE THE APPLICATION WITH THE PREMIUM.
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 4/23/2010  DEVELBISS CORRECT THE PLAN/SUBPLAN EDIT.  REMOVE THE
*                      #COMPARE-KEY (NOT USED).  CORRECT THE FIELD
*                      ORDER IN #RESTART-KEY.           SEE BJD1.
* ADDED 6/2011 - GUY - AP-LEGAL-ANN-OPTION  CHECK FOR Y -  JHU PROJECT
*                      SKIP UPDATING THE PRAP STATUS   SEE SRK
* 05/23/17 B.ELLO      PIN EXPANSION CHANGES
*                      RESTOWED FOR UPDATED PDAS AND LDAS
*                      CHANGED VIEW OF PRAP WITH EXPANDED PIN.  (PINE)
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9100 extends BLNatBase
{
    // Data Areas
    private PdaAcipda_D pdaAcipda_D;
    private PdaAcipda_M pdaAcipda_M;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pls_Error_Occurence;
    private DbsField pls_Dar_Program;

    private DataAccessProgramView vw_batch_Restart;
    private DbsField batch_Restart_Rst_Actve_Ind;
    private DbsField batch_Restart_Rst_Job_Nme;
    private DbsField batch_Restart_Rst_Jobstep_Nme;
    private DbsField batch_Restart_Rst_Pgm_Nme;
    private DbsField batch_Restart_Rst_Curr_Dte;
    private DbsField batch_Restart_Rst_Curr_Tme;
    private DbsField batch_Restart_Rst_Start_Dte;
    private DbsField batch_Restart_Rst_Start_Tme;
    private DbsField batch_Restart_Rst_End_Dte;
    private DbsField batch_Restart_Rst_End_Tme;
    private DbsField batch_Restart_Rst_Key;
    private DbsField batch_Restart_Rst_Cnt;
    private DbsField batch_Restart_Rst_Isn_Nbr;
    private DbsField batch_Restart_Rst_Rstrt_Data_Ind;

    private DbsGroup batch_Restart_Rst_Data_Grp;
    private DbsField batch_Restart_Rst_Data_Txt;
    private DbsField batch_Restart_Rst_Frst_Run_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Tme;
    private DbsField batch_Restart_Rst_Updt_Dte;
    private DbsField batch_Restart_Rst_Updt_Tme;
    private DbsField pnd_Restart_Key;

    private DbsGroup pnd_Restart_Key__R_Field_1;

    private DbsGroup pnd_Restart_Key_Pnd_Restart_Structure;
    private DbsField pnd_Restart_Key_Pnd_Restart_Tiaa_Contract;
    private DbsField pnd_Restart_Key_Pnd_Restart_Plan_No;
    private DbsField pnd_Restart_Key_Pnd_Restart_Part_Id;

    private DbsGroup pnd_Restart_Key__R_Field_2;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ssn;
    private DbsField pnd_Restart_Key_Pnd_Restart_Subplan_No;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ext;
    private DbsField pnd_Sp_Actve_Job_Step;

    private DbsGroup pnd_Sp_Actve_Job_Step__R_Field_3;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme;
    private DbsField pnd_Restart_Isn;

    private DbsGroup contribs;
    private DbsField contribs_Pnd_Record_Type;
    private DbsField contribs_Pnd_Filler1;
    private DbsField contribs_Pnd_Plan_No;
    private DbsField contribs_Pnd_Filler2;
    private DbsField contribs_Pnd_Part_Id;

    private DbsGroup contribs__R_Field_4;
    private DbsField contribs_Pnd_Ssn;

    private DbsGroup contribs__R_Field_5;
    private DbsField contribs_Pnd_Ssn_N;
    private DbsField contribs_Pnd_Subplan_No;
    private DbsField contribs_Pnd_Filler3;
    private DbsField contribs_Pnd_Tiaa_Contract;
    private DbsField contribs_Pnd_Filler4;
    private DbsField contribs_Pnd_Cref_Contract;
    private DbsField contribs_Pnd_Filler5;
    private DbsField contribs_Pnd_Part_Name;

    private DataAccessProgramView vw_prap_View;
    private DbsField prap_View_Ap_Coll_Code;
    private DbsField prap_View_Ap_Soc_Sec;
    private DbsField prap_View_Ap_Status;
    private DbsField prap_View_Ap_Record_Type;
    private DbsField prap_View_Ap_Release_Ind;
    private DbsField prap_View_Ap_Tiaa_Cntrct;
    private DbsField prap_View_Ap_Cref_Cert;
    private DbsField prap_View_Ap_Cor_Last_Nme;
    private DbsField prap_View_Ap_Cor_First_Nme;
    private DbsField prap_View_Ap_Cor_Mddle_Nme;
    private DbsField prap_View_Ap_Sgrd_Plan_No;
    private DbsField prap_View_Ap_Sgrd_Subplan_No;
    private DbsField prap_View_Ap_Sgrd_Divsub;
    private DbsField prap_View_Ap_Text_Udf_1;
    private DbsField prap_View_Ap_Text_Udf_2;
    private DbsField prap_View_Ap_Text_Udf_3;
    private DbsField prap_View_Ap_Legal_Ann_Option;
    private DbsField pnd_Todays_Date;

    private DbsGroup pnd_Accepted_Report_Rec;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Name;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Ssn;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Plan_Number;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_Number;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract;

    private DbsGroup pnd_Rejected_Report_Rec;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Name;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Ssn;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Subplan_Number;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg;

    private DbsGroup pnd_Summary_Report_Rec;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt;
    private DbsField pnd_Restarted;
    private DbsField pnd_Skip_Restart_Record;
    private DbsField pnd_Error_Rec_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_Selected_Isn;
    private DbsField pnd_Retry_Cnt;
    private DbsField intvl;
    private DbsField reqid;
    private DbsField pnd_Jobname;
    private DbsField pnd_J;
    private DbsField pnd_A;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAcipda_D = new PdaAcipda_D(localVariables);
        pdaAcipda_M = new PdaAcipda_M(localVariables);

        // Local Variables
        pls_Error_Occurence = WsIndependent.getInstance().newFieldInRecord("pls_Error_Occurence", "+ERROR-OCCURENCE", FieldType.BOOLEAN, 1);
        pls_Dar_Program = WsIndependent.getInstance().newFieldInRecord("pls_Dar_Program", "+DAR-PROGRAM", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        // Local Variables

        vw_batch_Restart = new DataAccessProgramView(new NameInfo("vw_batch_Restart", "BATCH-RESTART"), "BATCH_RESTART", "BATCH_RESTART", DdmPeriodicGroups.getInstance().getGroups("BATCH_RESTART"));
        batch_Restart_Rst_Actve_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Actve_Ind", "RST-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RST_ACTVE_IND");
        batch_Restart_Rst_Actve_Ind.setDdmHeader("ACTIVE IND");
        batch_Restart_Rst_Job_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Job_Nme", "RST-JOB-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_JOB_NME");
        batch_Restart_Rst_Job_Nme.setDdmHeader("JOB NAME");
        batch_Restart_Rst_Jobstep_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Jobstep_Nme", "RST-JOBSTEP-NME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_JOBSTEP_NME");
        batch_Restart_Rst_Jobstep_Nme.setDdmHeader("STEP NAME");
        batch_Restart_Rst_Pgm_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Pgm_Nme", "RST-PGM-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_PGM_NME");
        batch_Restart_Rst_Pgm_Nme.setDdmHeader("PROGRAM NAME");
        batch_Restart_Rst_Curr_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Dte", "RST-CURR-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_DTE");
        batch_Restart_Rst_Curr_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Tme", "RST-CURR-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_TME");
        batch_Restart_Rst_Start_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Dte", "RST-START-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_DTE");
        batch_Restart_Rst_Start_Dte.setDdmHeader("START/DATE");
        batch_Restart_Rst_Start_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Tme", "RST-START-TME", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_TME");
        batch_Restart_Rst_Start_Tme.setDdmHeader("START/TIME");
        batch_Restart_Rst_End_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Dte", "RST-END-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_DTE");
        batch_Restart_Rst_End_Dte.setDdmHeader("END/DATE");
        batch_Restart_Rst_End_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Tme", "RST-END-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_TME");
        batch_Restart_Rst_End_Tme.setDdmHeader("END/TIME");
        batch_Restart_Rst_Key = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Key", "RST-KEY", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "RST_KEY");
        batch_Restart_Rst_Cnt = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Cnt", "RST-CNT", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "RST_CNT");
        batch_Restart_Rst_Isn_Nbr = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Isn_Nbr", "RST-ISN-NBR", FieldType.PACKED_DECIMAL, 
            10, RepeatingFieldStrategy.None, "RST_ISN_NBR");
        batch_Restart_Rst_Rstrt_Data_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Rstrt_Data_Ind", "RST-RSTRT-DATA-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RST_RSTRT_DATA_IND");
        batch_Restart_Rst_Rstrt_Data_Ind.setDdmHeader("FAILED");

        batch_Restart_Rst_Data_Grp = vw_batch_Restart.getRecord().newGroupInGroup("batch_Restart_Rst_Data_Grp", "RST-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Data_Txt = batch_Restart_Rst_Data_Grp.newFieldArrayInGroup("batch_Restart_Rst_Data_Txt", "RST-DATA-TXT", FieldType.STRING, 250, 
            new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RST_DATA_TXT", "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Frst_Run_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Frst_Run_Dte", "RST-FRST-RUN-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_FRST_RUN_DTE");
        batch_Restart_Rst_Frst_Run_Dte.setDdmHeader("FIRST/RUN DTE");
        batch_Restart_Rst_Last_Rstrt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Dte", "RST-LAST-RSTRT-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_DTE");
        batch_Restart_Rst_Last_Rstrt_Dte.setDdmHeader("LAST RSTRT/DATE");
        batch_Restart_Rst_Last_Rstrt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Tme", "RST-LAST-RSTRT-TME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_TME");
        batch_Restart_Rst_Last_Rstrt_Tme.setDdmHeader("LAST RSTRT/TIME");
        batch_Restart_Rst_Updt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Dte", "RST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_DTE");
        batch_Restart_Rst_Updt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Tme", "RST-UPDT-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_TME");
        registerRecord(vw_batch_Restart);

        pnd_Restart_Key = localVariables.newFieldInRecord("pnd_Restart_Key", "#RESTART-KEY", FieldType.STRING, 50);

        pnd_Restart_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Restart_Key__R_Field_1", "REDEFINE", pnd_Restart_Key);

        pnd_Restart_Key_Pnd_Restart_Structure = pnd_Restart_Key__R_Field_1.newGroupInGroup("pnd_Restart_Key_Pnd_Restart_Structure", "#RESTART-STRUCTURE");
        pnd_Restart_Key_Pnd_Restart_Tiaa_Contract = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Tiaa_Contract", 
            "#RESTART-TIAA-CONTRACT", FieldType.STRING, 10);
        pnd_Restart_Key_Pnd_Restart_Plan_No = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Plan_No", "#RESTART-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Restart_Key_Pnd_Restart_Part_Id = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Part_Id", "#RESTART-PART-ID", 
            FieldType.STRING, 17);

        pnd_Restart_Key__R_Field_2 = pnd_Restart_Key_Pnd_Restart_Structure.newGroupInGroup("pnd_Restart_Key__R_Field_2", "REDEFINE", pnd_Restart_Key_Pnd_Restart_Part_Id);
        pnd_Restart_Key_Pnd_Restart_Ssn = pnd_Restart_Key__R_Field_2.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ssn", "#RESTART-SSN", FieldType.STRING, 
            9);
        pnd_Restart_Key_Pnd_Restart_Subplan_No = pnd_Restart_Key__R_Field_2.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Subplan_No", "#RESTART-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Restart_Key_Pnd_Restart_Ext = pnd_Restart_Key__R_Field_2.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ext", "#RESTART-EXT", FieldType.STRING, 
            2);
        pnd_Sp_Actve_Job_Step = localVariables.newFieldInRecord("pnd_Sp_Actve_Job_Step", "#SP-ACTVE-JOB-STEP", FieldType.STRING, 17);

        pnd_Sp_Actve_Job_Step__R_Field_3 = localVariables.newGroupInRecord("pnd_Sp_Actve_Job_Step__R_Field_3", "REDEFINE", pnd_Sp_Actve_Job_Step);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind = pnd_Sp_Actve_Job_Step__R_Field_3.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind", "#RST-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme = pnd_Sp_Actve_Job_Step__R_Field_3.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme", "#RST-JOB-NME", 
            FieldType.STRING, 8);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme = pnd_Sp_Actve_Job_Step__R_Field_3.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme", "#RST-STEP-NME", 
            FieldType.STRING, 8);
        pnd_Restart_Isn = localVariables.newFieldInRecord("pnd_Restart_Isn", "#RESTART-ISN", FieldType.PACKED_DECIMAL, 10);

        contribs = localVariables.newGroupInRecord("contribs", "CONTRIBS");
        contribs_Pnd_Record_Type = contribs.newFieldInGroup("contribs_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 4);
        contribs_Pnd_Filler1 = contribs.newFieldInGroup("contribs_Pnd_Filler1", "#FILLER1", FieldType.STRING, 1);
        contribs_Pnd_Plan_No = contribs.newFieldInGroup("contribs_Pnd_Plan_No", "#PLAN-NO", FieldType.STRING, 6);
        contribs_Pnd_Filler2 = contribs.newFieldInGroup("contribs_Pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        contribs_Pnd_Part_Id = contribs.newFieldInGroup("contribs_Pnd_Part_Id", "#PART-ID", FieldType.STRING, 15);

        contribs__R_Field_4 = contribs.newGroupInGroup("contribs__R_Field_4", "REDEFINE", contribs_Pnd_Part_Id);
        contribs_Pnd_Ssn = contribs__R_Field_4.newFieldInGroup("contribs_Pnd_Ssn", "#SSN", FieldType.STRING, 9);

        contribs__R_Field_5 = contribs__R_Field_4.newGroupInGroup("contribs__R_Field_5", "REDEFINE", contribs_Pnd_Ssn);
        contribs_Pnd_Ssn_N = contribs__R_Field_5.newFieldInGroup("contribs_Pnd_Ssn_N", "#SSN-N", FieldType.NUMERIC, 9);
        contribs_Pnd_Subplan_No = contribs__R_Field_4.newFieldInGroup("contribs_Pnd_Subplan_No", "#SUBPLAN-NO", FieldType.STRING, 6);
        contribs_Pnd_Filler3 = contribs.newFieldInGroup("contribs_Pnd_Filler3", "#FILLER3", FieldType.STRING, 1);
        contribs_Pnd_Tiaa_Contract = contribs.newFieldInGroup("contribs_Pnd_Tiaa_Contract", "#TIAA-CONTRACT", FieldType.STRING, 10);
        contribs_Pnd_Filler4 = contribs.newFieldInGroup("contribs_Pnd_Filler4", "#FILLER4", FieldType.STRING, 1);
        contribs_Pnd_Cref_Contract = contribs.newFieldInGroup("contribs_Pnd_Cref_Contract", "#CREF-CONTRACT", FieldType.STRING, 10);
        contribs_Pnd_Filler5 = contribs.newFieldInGroup("contribs_Pnd_Filler5", "#FILLER5", FieldType.STRING, 1);
        contribs_Pnd_Part_Name = contribs.newFieldInGroup("contribs_Pnd_Part_Name", "#PART-NAME", FieldType.STRING, 30);

        vw_prap_View = new DataAccessProgramView(new NameInfo("vw_prap_View", "PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap_View_Ap_Coll_Code = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_View_Ap_Soc_Sec = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_View_Ap_Status = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_STATUS");
        prap_View_Ap_Record_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_View_Ap_Release_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        prap_View_Ap_Tiaa_Cntrct = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_View_Ap_Cref_Cert = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        prap_View_Ap_Cor_Last_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        prap_View_Ap_Cor_First_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        prap_View_Ap_Cor_Mddle_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        prap_View_Ap_Sgrd_Plan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_View_Ap_Sgrd_Subplan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        prap_View_Ap_Sgrd_Divsub = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "AP_SGRD_DIVSUB");
        prap_View_Ap_Text_Udf_1 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_1", "AP-TEXT-UDF-1", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_1");
        prap_View_Ap_Text_Udf_2 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_2", "AP-TEXT-UDF-2", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_2");
        prap_View_Ap_Text_Udf_3 = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Text_Udf_3", "AP-TEXT-UDF-3", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TEXT_UDF_3");
        prap_View_Ap_Legal_Ann_Option = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Legal_Ann_Option", "AP-LEGAL-ANN-OPTION", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        registerRecord(vw_prap_View);

        pnd_Todays_Date = localVariables.newFieldInRecord("pnd_Todays_Date", "#TODAYS-DATE", FieldType.NUMERIC, 8);

        pnd_Accepted_Report_Rec = localVariables.newGroupInRecord("pnd_Accepted_Report_Rec", "#ACCEPTED-REPORT-REC");
        pnd_Accepted_Report_Rec_Pnd_Rept_Name = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Name", "#REPT-NAME", FieldType.STRING, 
            35);
        pnd_Accepted_Report_Rec_Pnd_Rept_Ssn = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Ssn", "#REPT-SSN", FieldType.STRING, 
            9);
        pnd_Accepted_Report_Rec_Pnd_Rept_Plan_Number = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Plan_Number", "#REPT-PLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_Number = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_Number", "#REPT-SUBPLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract", "#REPT-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract", "#REPT-CREF-CONTRACT", 
            FieldType.STRING, 10);

        pnd_Rejected_Report_Rec = localVariables.newGroupInRecord("pnd_Rejected_Report_Rec", "#REJECTED-REPORT-REC");
        pnd_Rejected_Report_Rec_Pnd_Error_Name = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Name", "#ERROR-NAME", FieldType.STRING, 
            35);
        pnd_Rejected_Report_Rec_Pnd_Error_Ssn = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Ssn", "#ERROR-SSN", FieldType.STRING, 
            9);
        pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number", "#ERROR-PLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Subplan_Number = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Subplan_Number", 
            "#ERROR-SUBPLAN-NUMBER", FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract", "#ERROR-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract", "#ERROR-CREF-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg", "#ERROR-RETURN-MSG", 
            FieldType.STRING, 50);

        pnd_Summary_Report_Rec = localVariables.newGroupInRecord("pnd_Summary_Report_Rec", "#SUMMARY-REPORT-REC");
        pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt", "#TOT-TRANS-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt", "#TOT-ACCEPT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt", "#TOT-REJECT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Restarted = localVariables.newFieldInRecord("pnd_Restarted", "#RESTARTED", FieldType.BOOLEAN, 1);
        pnd_Skip_Restart_Record = localVariables.newFieldInRecord("pnd_Skip_Restart_Record", "#SKIP-RESTART-RECORD", FieldType.BOOLEAN, 1);
        pnd_Error_Rec_Cnt = localVariables.newFieldInRecord("pnd_Error_Rec_Cnt", "#ERROR-REC-CNT", FieldType.NUMERIC, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Selected_Isn = localVariables.newFieldInRecord("pnd_Selected_Isn", "#SELECTED-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Retry_Cnt = localVariables.newFieldInRecord("pnd_Retry_Cnt", "#RETRY-CNT", FieldType.NUMERIC, 3);
        intvl = localVariables.newFieldInRecord("intvl", "INTVL", FieldType.INTEGER, 1);
        reqid = localVariables.newFieldInRecord("reqid", "REQID", FieldType.STRING, 8);
        pnd_Jobname = localVariables.newFieldInRecord("pnd_Jobname", "#JOBNAME", FieldType.STRING, 10);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_batch_Restart.reset();
        vw_prap_View.reset();

        localVariables.reset();
        pnd_Restarted.setInitialValue(false);
        pnd_Skip_Restart_Record.setInitialValue(false);
        intvl.setInitialValue(1);
        reqid.setInitialValue("MYREQID");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9100() throws Exception
    {
        super("Scib9100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIB9100", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  OVERRIDES NATURAL ERROR ROUTINE
        //*  THIS REPLACES *STARTUP LOGIC.
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 133 PS = 60;//Natural: FORMAT ( 3 ) LS = 133 PS = 60
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pls_Dar_Program.setValue(Global.getPROGRAM());                                                                                                                    //Natural: ASSIGN +DAR-PROGRAM := *PROGRAM
        //*  CAUSES A TERMINATION IN THE EVENT OF
        if (condition(pls_Error_Occurence.getBoolean()))                                                                                                                  //Natural: IF +ERROR-OCCURENCE
        {
            //*  AN ERROR. NEEDED TO FORCE OPERATOR RESTART.
            DbsUtil.terminate(5);  if (true) return;                                                                                                                      //Natural: TERMINATE 5
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-RESTART
        sub_Check_For_Restart();
        if (condition(Global.isEscape())) {return;}
        Global.getERROR_TA().setValue(" ");                                                                                                                               //Natural: ASSIGN *ERROR-TA := ' '
        pnd_Todays_Date.setValue(Global.getDATN());                                                                                                                       //Natural: ASSIGN #TODAYS-DATE := *DATN
        //* *======================================================================
        //* *    M A I N    P R O C E S S
        //* *======================================================================
        //*  RESET #COMPARE-KEY
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            getWorkFiles().read(1, contribs);                                                                                                                             //Natural: READ WORK FILE 1 ONCE CONTRIBS
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                if (condition(! (pnd_Restarted.getBoolean())))                                                                                                            //Natural: IF NOT #RESTARTED
                {
                                                                                                                                                                          //Natural: PERFORM DELETE-RESTART-RECORD
                    sub_Delete_Restart_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
                sub_Write_Summary_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            //* ***************************************************
            //* * ONLY PROCESS RECORDS WITH REC-TYPE 'CNTB'      **
            //* ***************************************************
            //* *
            //*  ERROR - INVALID RECORDS
            if (condition(contribs_Pnd_Record_Type.notEquals("CNTB")))                                                                                                    //Natural: IF CONTRIBS.#RECORD-TYPE NE 'CNTB'
            {
                getReports().display(0, "*----------------------------------------*");                                                                                    //Natural: DISPLAY '*----------------------------------------*'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "  SORT ERROR ** RECORD TYPE NOT EQ CNTB   ");                                                                                      //Natural: WRITE '  SORT ERROR ** RECORD TYPE NOT EQ CNTB   '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().display(0, "*----------------------------------------*");                                                                                    //Natural: DISPLAY '*----------------------------------------*'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(50);  if (true) return;                                                                                                                 //Natural: TERMINATE 50
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 2 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 3 )
            if (condition(pnd_Restarted.getBoolean()))                                                                                                                    //Natural: IF #RESTARTED
            {
                if (condition(contribs_Pnd_Tiaa_Contract.greater(" ")))                                                                                                   //Natural: IF CONTRIBS.#TIAA-CONTRACT GT ' '
                {
                    if (condition(contribs_Pnd_Tiaa_Contract.equals(pnd_Restart_Key_Pnd_Restart_Tiaa_Contract) && contribs_Pnd_Part_Id.equals(pnd_Restart_Key_Pnd_Restart_Part_Id))) //Natural: IF CONTRIBS.#TIAA-CONTRACT = #RESTART-TIAA-CONTRACT AND CONTRIBS.#PART-ID = #RESTART-PART-ID
                    {
                        pnd_Skip_Restart_Record.setValue(true);                                                                                                           //Natural: ASSIGN #SKIP-RESTART-RECORD := TRUE
                        pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                        sub_Write_Error_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Restarted.reset();                                                                                                                            //Natural: RESET #RESTARTED
                        pnd_Skip_Restart_Record.reset();                                                                                                                  //Natural: RESET #SKIP-RESTART-RECORD
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  READ THE NEXT RECORD
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(contribs_Pnd_Plan_No.equals(pnd_Restart_Key_Pnd_Restart_Plan_No) && contribs_Pnd_Part_Id.equals(pnd_Restart_Key_Pnd_Restart_Part_Id)))  //Natural: IF CONTRIBS.#PLAN-NO = #RESTART-PLAN-NO AND CONTRIBS.#PART-ID = #RESTART-PART-ID
                    {
                        pnd_Skip_Restart_Record.setValue(true);                                                                                                           //Natural: ASSIGN #SKIP-RESTART-RECORD := TRUE
                        pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                        sub_Write_Error_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Restarted.reset();                                                                                                                            //Natural: RESET #RESTARTED
                        pnd_Skip_Restart_Record.reset();                                                                                                                  //Natural: RESET #SKIP-RESTART-RECORD
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  READ THE NEXT RECORD
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-TRANS-CNT
            //*    RESET #COMPARE-KEY
            //*    ASSIGN #COMP-PLAN-NO     =  CONTRIBS.#PLAN-NO
            //*    ASSIGN #COMP-PART-ID     =  CONTRIBS.#PART-ID
                                                                                                                                                                          //Natural: PERFORM UPDATE-BATCH-RESTART-RECORD
            sub_Update_Batch_Restart_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(contribs_Pnd_Tiaa_Contract.notEquals(" ")))                                                                                                     //Natural: IF CONTRIBS.#TIAA-CONTRACT NE ' '
            {
                //*      ASSIGN #COMP-CONTRACT-NO =  CONTRIBS.#TIAA-CONTRACT
                                                                                                                                                                          //Natural: PERFORM FIND-PRAP-RECORD
                sub_Find_Prap_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM FIND-PRAP-RECORD-BY-SSN
                sub_Find_Prap_Record_By_Ssn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  NO INPUT RECORDS TO PROCESS
        if (condition(pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.equals(getZero())))                                                                                        //Natural: IF #TOT-TRANS-CNT = 0
        {
            getReports().display(0, "********  Contribution File Is Empty ********");                                                                                     //Natural: DISPLAY '********  Contribution File Is Empty ********'
            if (Global.isEscape()) return;
            DbsUtil.terminate(1);  if (true) return;                                                                                                                      //Natural: TERMINATE 1
        }                                                                                                                                                                 //Natural: END-IF
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-PRAP-RECORD
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-PRAP-RECORD-BY-SSN
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PRAP-STATUS
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-REC
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REC
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-RESTART
        //* *=====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-BATCH-RESTART-RECORD
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-RESTART-RECORD
        //* *======================================================================
        //*  RETRY ERROR LOGIC
        //* *======================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
        //* *======================================================================
    }
    //*  READ BY CONTRACT
    private void sub_Find_Prap_Record() throws Exception                                                                                                                  //Natural: FIND-PRAP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();                                                                                                                //Natural: RESET ##MSG
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-TIAA-CNTRCT EQ CONTRIBS.#TIAA-CONTRACT WHERE AP-RECORD-TYPE EQ 1
        (
        "FINDREC",
        new Wc[] { new Wc("AP_TIAA_CNTRCT", "=", contribs_Pnd_Tiaa_Contract, WcType.WITH) ,
        new Wc("AP_RECORD_TYPE", "=", 1, WcType.WHERE) }
        );
        FINDREC:
        while (condition(vw_prap_View.readNextRow("FINDREC", true)))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            if (condition(vw_prap_View.getAstCOUNTER().equals(0)))                                                                                                        //Natural: IF NO RECORDS FOUND
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("NO PRAP RECORD FOUND");                                                                               //Natural: MOVE 'NO PRAP RECORD FOUND' TO ##MSG
                pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                sub_Write_Error_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDREC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDREC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(! (prap_View_Ap_Soc_Sec.equals(contribs_Pnd_Ssn_N))))                                                                                           //Natural: IF NOT AP-SOC-SEC = CONTRIBS.#SSN-N
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("CONTRACT NUMBER DOES NOT MATCH SSN");                                                                 //Natural: MOVE 'CONTRACT NUMBER DOES NOT MATCH SSN' TO ##MSG
                pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                sub_Write_Error_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDREC"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDREC"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();                                                                                                        //Natural: RESET ##MSG
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  DELETED OR MATCH&RELEASE
            //*  ANNUITY OPTION NO UPDATE- SRK
            if (condition(prap_View_Ap_Status.equals("A") || prap_View_Ap_Status.equals("C") || prap_View_Ap_Legal_Ann_Option.equals("Y")))                               //Natural: IF AP-STATUS = 'A' OR = 'C' OR AP-LEGAL-ANN-OPTION = 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Selected_Isn.setValue(vw_prap_View.getAstISN("FINDREC"));                                                                                                 //Natural: ASSIGN #SELECTED-ISN := *ISN
                                                                                                                                                                          //Natural: PERFORM UPDATE-PRAP-STATUS
            sub_Update_Prap_Status();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FINDREC"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FINDREC"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-PRAP-RECORD
    }
    //*  READ PRAP BY SSN
    private void sub_Find_Prap_Record_By_Ssn() throws Exception                                                                                                           //Natural: FIND-PRAP-RECORD-BY-SSN
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();                                                                                                                //Natural: RESET ##MSG
        vw_prap_View.startDatabaseFind                                                                                                                                    //Natural: FIND PRAP-VIEW WITH AP-SOC-SEC EQ CONTRIBS.#SSN-N WHERE AP-RECORD-TYPE EQ 1
        (
        "FINDSSN",
        new Wc[] { new Wc("AP_SOC_SEC", "=", contribs_Pnd_Ssn_N, WcType.WITH) ,
        new Wc("AP_RECORD_TYPE", "=", 1, WcType.WHERE) }
        );
        FINDSSN:
        while (condition(vw_prap_View.readNextRow("FINDSSN", true)))
        {
            vw_prap_View.setIfNotFoundControlFlag(false);
            if (condition(vw_prap_View.getAstCOUNTER().equals(0)))                                                                                                        //Natural: IF NO RECORDS FOUND
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("NO PRAP RECORD FOUND");                                                                               //Natural: MOVE 'NO PRAP RECORD FOUND' TO ##MSG
                pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                sub_Write_Error_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDSSN"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDSSN"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  IF AP-SGRD-PLAN-NO NE CONTRIBS.#PLAN-NO AND                /* BJD1
            //*  BJD1
            if (condition(prap_View_Ap_Sgrd_Plan_No.notEquals(contribs_Pnd_Plan_No) || prap_View_Ap_Sgrd_Subplan_No.notEquals(contribs_Pnd_Subplan_No)))                  //Natural: IF AP-SGRD-PLAN-NO NE CONTRIBS.#PLAN-NO OR AP-SGRD-SUBPLAN-NO NE CONTRIBS.#SUBPLAN-NO
            {
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("PLAN/SUBPLANS DO NOT MATCH");                                                                         //Natural: MOVE 'PLAN/SUBPLANS DO NOT MATCH' TO ##MSG
                pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                sub_Write_Error_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDSSN"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDSSN"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();                                                                                                        //Natural: RESET ##MSG
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  DELETED OR MATCH&RELEASE
            //*  ANNUITY OPTION - NO UPDATE-GUY
            if (condition(prap_View_Ap_Status.equals("A") || prap_View_Ap_Status.equals("C") || prap_View_Ap_Legal_Ann_Option.equals("Y")))                               //Natural: IF AP-STATUS = 'A' OR = 'C' OR AP-LEGAL-ANN-OPTION = 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Selected_Isn.setValue(vw_prap_View.getAstISN("FINDSSN"));                                                                                                 //Natural: ASSIGN #SELECTED-ISN := *ISN
                                                                                                                                                                          //Natural: PERFORM UPDATE-PRAP-STATUS
            sub_Update_Prap_Status();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FINDSSN"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FINDSSN"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  FIND-PRAP-RECORD
    }
    private void sub_Update_Prap_Status() throws Exception                                                                                                                //Natural: UPDATE-PRAP-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //*  CALL SCIN5200 TO MATCH & RELEASE PRAP
        DbsUtil.callnat(Scin5200.class , getCurrentProcessState(), pnd_Selected_Isn, pdaAcipda_D.getDialog_Info_Sub(), pdaAcipda_M.getMsg_Info_Sub());                    //Natural: CALLNAT 'SCIN5200' #SELECTED-ISN DIALOG-INFO-SUB MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        if (condition(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("    ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE = '    '
        {
            pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt.nadd(1);                                                                                                            //Natural: ADD 1 TO #TOT-ACCEPT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-REC
            sub_Write_Report_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                            //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
            sub_Write_Error_Rec();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-PRAP-STATUS
    }
    private void sub_Write_Report_Rec() throws Exception                                                                                                                  //Natural: WRITE-REPORT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Accepted_Report_Rec.reset();                                                                                                                                  //Natural: RESET #ACCEPTED-REPORT-REC
        pnd_Accepted_Report_Rec_Pnd_Rept_Plan_Number.setValue(contribs_Pnd_Plan_No);                                                                                      //Natural: ASSIGN #REPT-PLAN-NUMBER := CONTRIBS.#PLAN-NO
        pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_Number.setValue(contribs_Pnd_Subplan_No);                                                                                //Natural: ASSIGN #REPT-SUBPLAN-NUMBER := CONTRIBS.#SUBPLAN-NO
        pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract.setValue(contribs_Pnd_Tiaa_Contract);                                                                              //Natural: ASSIGN #REPT-TIAA-CONTRACT := CONTRIBS.#TIAA-CONTRACT
        pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract.setValue(contribs_Pnd_Cref_Contract);                                                                              //Natural: ASSIGN #REPT-CREF-CONTRACT := CONTRIBS.#CREF-CONTRACT
        pnd_Accepted_Report_Rec_Pnd_Rept_Ssn.setValue(contribs_Pnd_Ssn);                                                                                                  //Natural: ASSIGN #REPT-SSN := CONTRIBS.#SSN
        pnd_Accepted_Report_Rec_Pnd_Rept_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, prap_View_Ap_Cor_Last_Nme, ",", prap_View_Ap_Cor_First_Nme));      //Natural: COMPRESS AP-COR-LAST-NME ',' AP-COR-FIRST-NME INTO #REPT-NAME LEAVING NO
        getReports().write(1, ReportOption.NOTITLE,"Name",pnd_Accepted_Report_Rec_Pnd_Rept_Name,"Part. SSN",pnd_Accepted_Report_Rec_Pnd_Rept_Ssn,NEWLINE,                 //Natural: WRITE ( 1 ) NOTITLE 'Name' #REPT-NAME 'Part. SSN' #REPT-SSN / 'Contrib Plan Number' #REPT-PLAN-NUMBER 'Contrib SubPlan Number' #REPT-SUBPLAN-NUMBER / 'ACIS Plan Number' AP-SGRD-PLAN-NO 'ACIS SubPlan Number' AP-SGRD-SUBPLAN-NO / 'TIAA Contract' #REPT-TIAA-CONTRACT 'CREF Contract' #REPT-CREF-CONTRACT /
            "Contrib Plan Number",pnd_Accepted_Report_Rec_Pnd_Rept_Plan_Number,"Contrib SubPlan Number",pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_Number,
            NEWLINE,"ACIS Plan Number",prap_View_Ap_Sgrd_Plan_No,"ACIS SubPlan Number",prap_View_Ap_Sgrd_Subplan_No,NEWLINE,"TIAA Contract",pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract,
            "CREF Contract",pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract,NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 3 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-REPORT-REC
    }
    private void sub_Write_Error_Rec() throws Exception                                                                                                                   //Natural: WRITE-ERROR-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Rejected_Report_Rec.reset();                                                                                                                                  //Natural: RESET #REJECTED-REPORT-REC
        pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number.setValue(contribs_Pnd_Plan_No);                                                                                     //Natural: ASSIGN #ERROR-PLAN-NUMBER := CONTRIBS.#PLAN-NO
        pnd_Rejected_Report_Rec_Pnd_Error_Subplan_Number.setValue(contribs_Pnd_Subplan_No);                                                                               //Natural: ASSIGN #ERROR-SUBPLAN-NUMBER := CONTRIBS.#SUBPLAN-NO
        pnd_Rejected_Report_Rec_Pnd_Error_Name.setValue(contribs_Pnd_Part_Name);                                                                                          //Natural: ASSIGN #ERROR-NAME := CONTRIBS.#PART-NAME
        pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract.setValue(contribs_Pnd_Tiaa_Contract);                                                                             //Natural: ASSIGN #ERROR-TIAA-CONTRACT := CONTRIBS.#TIAA-CONTRACT
        pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract.setValue(contribs_Pnd_Cref_Contract);                                                                             //Natural: ASSIGN #ERROR-CREF-CONTRACT := CONTRIBS.#CREF-CONTRACT
        pnd_Rejected_Report_Rec_Pnd_Error_Ssn.setValue(contribs_Pnd_Ssn);                                                                                                 //Natural: ASSIGN #ERROR-SSN := CONTRIBS.#SSN
        pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg.setValue(pdaAcipda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                 //Natural: ASSIGN #ERROR-RETURN-MSG := ##MSG
        if (condition(pnd_Skip_Restart_Record.getBoolean()))                                                                                                              //Natural: IF #SKIP-RESTART-RECORD
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg.setValue("A PROCESSING ERROR HAS OCCURRED - REASON UNKNOWN");                                                    //Natural: MOVE 'A PROCESSING ERROR HAS OCCURRED - REASON UNKNOWN' TO #ERROR-RETURN-MSG
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,"Name",pnd_Rejected_Report_Rec_Pnd_Error_Name,"Part. SSN",pnd_Rejected_Report_Rec_Pnd_Error_Ssn,NEWLINE,               //Natural: WRITE ( 2 ) NOTITLE 'Name' #ERROR-NAME 'Part. SSN' #ERROR-SSN / 'Contrib Plan Number' #ERROR-PLAN-NUMBER 'Contrib SubPlan Number' #ERROR-SUBPLAN-NUMBER / 'ACIS Plan Number' AP-SGRD-PLAN-NO 'ACIS SubPlan Number' AP-SGRD-SUBPLAN-NO / 'TIAA Contract' #ERROR-TIAA-CONTRACT 'CREF Contract' #ERROR-CREF-CONTRACT / 'Return Message' #ERROR-RETURN-MSG //
            "Contrib Plan Number",pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number,"Contrib SubPlan Number",pnd_Rejected_Report_Rec_Pnd_Error_Subplan_Number,
            NEWLINE,"ACIS Plan Number",prap_View_Ap_Sgrd_Plan_No,"ACIS SubPlan Number",prap_View_Ap_Sgrd_Subplan_No,NEWLINE,"TIAA Contract",pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract,
            "CREF Contract",pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract,NEWLINE,"Return Message",pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg,NEWLINE,
            NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(2).less(3)))                                                                                                           //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 3 LINES LEFT
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-ERROR-REC
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(3, ReportOption.NOTITLE,"Total Records Read    :",pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Records Written :",pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt,  //Natural: WRITE ( 3 ) NOTITLE 'Total Records Read    :' #TOT-TRANS-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Records Written :' #TOT-ACCEPT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Records Rejected:' #TOT-REJECT-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Records Rejected:",pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  WRITE-SUMMARY-REPORT
    }
    private void sub_Check_For_Restart() throws Exception                                                                                                                 //Natural: CHECK-FOR-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind.setValue("A");                                                                                                            //Natural: ASSIGN #RST-ACTVE-IND = 'A'
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                            //Natural: ASSIGN #RST-JOB-NME = *INIT-USER
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme.setValue(Global.getINIT_ID());                                                                                             //Natural: ASSIGN #RST-STEP-NME = *INIT-ID
        vw_batch_Restart.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) BATCH-RESTART WITH SP-ACTVE-JOB-STEP = #SP-ACTVE-JOB-STEP
        (
        "FIND_R",
        new Wc[] { new Wc("SP_ACTVE_JOB_STEP", "=", pnd_Sp_Actve_Job_Step, WcType.WITH) },
        1
        );
        FIND_R:
        while (condition(vw_batch_Restart.readNextRow("FIND_R", true)))
        {
            vw_batch_Restart.setIfNotFoundControlFlag(false);
            //*  CREATE A RECORD IF NONE EXISTS.
            if (condition(vw_batch_Restart.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                batch_Restart_Rst_Actve_Ind.setValue("A");                                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-ACTVE-IND := 'A'
                batch_Restart_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-JOB-NME := *INIT-USER
                batch_Restart_Rst_Jobstep_Nme.setValue(Global.getINIT_ID());                                                                                              //Natural: ASSIGN BATCH-RESTART.RST-JOBSTEP-NME := *INIT-ID
                batch_Restart_Rst_Pgm_Nme.setValue(Global.getPROGRAM());                                                                                                  //Natural: ASSIGN BATCH-RESTART.RST-PGM-NME := *PROGRAM
                batch_Restart_Rst_Curr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Curr_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Start_Dte.setValue(batch_Restart_Rst_Curr_Dte);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-DTE := BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Start_Tme.setValue(batch_Restart_Rst_Curr_Tme);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-TME := BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Frst_Run_Dte.setValue(batch_Restart_Rst_Start_Dte);                                                                                     //Natural: ASSIGN BATCH-RESTART.RST-FRST-RUN-DTE := BATCH-RESTART.RST-START-DTE
                pnd_Restart_Key_Pnd_Restart_Plan_No.setValue(" ");                                                                                                        //Natural: ASSIGN #RESTART-KEY.#RESTART-PLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Tiaa_Contract.setValue(" ");                                                                                                  //Natural: ASSIGN #RESTART-KEY.#RESTART-TIAA-CONTRACT := ' '
                pnd_Restart_Key_Pnd_Restart_Subplan_No.setValue(" ");                                                                                                     //Natural: ASSIGN #RESTART-KEY.#RESTART-SUBPLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Ssn.setValue(" ");                                                                                                            //Natural: ASSIGN #RESTART-KEY.#RESTART-SSN := ' '
                pnd_Restart_Key_Pnd_Restart_Ext.setValue(" ");                                                                                                            //Natural: ASSIGN #RESTART-KEY.#RESTART-EXT := ' '
                batch_Restart_Rst_Key.setValue(pnd_Restart_Key);                                                                                                          //Natural: ASSIGN BATCH-RESTART.RST-KEY := #RESTART-KEY
                STORE_REST:                                                                                                                                               //Natural: STORE BATCH-RESTART
                vw_batch_Restart.insertDBRow("STORE_REST");
                pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                           //Natural: ASSIGN #RESTART-ISN := *ISN ( STORE-REST. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(0, "********************************************************");                                                                        //Natural: WRITE '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***");                                                                        //Natural: WRITE '***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "********************************************************");                                                                        //Natural: WRITE '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, NEWLINE,NEWLINE);                                                                                                                   //Natural: WRITE //
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break FIND_R;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FIND-R. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                               //Natural: ASSIGN #RESTART-ISN := *ISN ( FIND-R. )
            pnd_Restart_Key.setValue(batch_Restart_Rst_Key);                                                                                                              //Natural: ASSIGN #RESTART-KEY := BATCH-RESTART.RST-KEY
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Key,"***");                                                                                                 //Natural: WRITE '***' '=' BATCH-RESTART.RST-KEY '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Dte,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-DTE '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Tme,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Last_Rstrt_Tme,"***");                                                                                      //Natural: WRITE '***' '=' BATCH-RESTART.RST-LAST-RSTRT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",pnd_Restart_Isn,"***");                                                                                                       //Natural: WRITE '***' '=' #RESTART-ISN '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FOUND RESTART RECORD
            getReports().write(0, NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Restarted.setValue(true);                                                                                                                                 //Natural: ASSIGN #RESTARTED := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CHECK-FOR-RESTART
    }
    private void sub_Update_Batch_Restart_Record() throws Exception                                                                                                       //Natural: UPDATE-BATCH-RESTART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *=====================================================================
        GET_U:                                                                                                                                                            //Natural: GET BATCH-RESTART #RESTART-ISN
        vw_batch_Restart.readByID(pnd_Restart_Isn.getLong(), "GET_U");
        pnd_Restart_Key_Pnd_Restart_Tiaa_Contract.setValue(contribs_Pnd_Tiaa_Contract);                                                                                   //Natural: ASSIGN #RESTART-KEY.#RESTART-TIAA-CONTRACT = CONTRIBS.#TIAA-CONTRACT
        pnd_Restart_Key_Pnd_Restart_Plan_No.setValue(contribs_Pnd_Plan_No);                                                                                               //Natural: ASSIGN #RESTART-KEY.#RESTART-PLAN-NO = CONTRIBS.#PLAN-NO
        pnd_Restart_Key_Pnd_Restart_Subplan_No.setValue(contribs_Pnd_Subplan_No);                                                                                         //Natural: ASSIGN #RESTART-KEY.#RESTART-SUBPLAN-NO = CONTRIBS.#SUBPLAN-NO
        pnd_Restart_Key_Pnd_Restart_Ssn.setValue(contribs_Pnd_Ssn);                                                                                                       //Natural: ASSIGN #RESTART-KEY.#RESTART-SSN = CONTRIBS.#SSN
        batch_Restart_Rst_Curr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-CURR-DTE
        batch_Restart_Rst_Curr_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                                       //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-CURR-TME
        batch_Restart_Rst_Start_Dte.setValue(batch_Restart_Rst_Curr_Dte);                                                                                                 //Natural: ASSIGN BATCH-RESTART.RST-START-DTE = BATCH-RESTART.RST-CURR-DTE
        batch_Restart_Rst_Start_Tme.setValue(batch_Restart_Rst_Curr_Tme);                                                                                                 //Natural: ASSIGN BATCH-RESTART.RST-START-TME = BATCH-RESTART.RST-CURR-TME
        batch_Restart_Rst_End_Dte.reset();                                                                                                                                //Natural: RESET BATCH-RESTART.RST-END-DTE BATCH-RESTART.RST-END-TME
        batch_Restart_Rst_End_Tme.reset();
        batch_Restart_Rst_Key.setValue(pnd_Restart_Key);                                                                                                                  //Natural: ASSIGN BATCH-RESTART.RST-KEY := #RESTART-KEY
        getReports().write(0, "*************************************************");                                                                                       //Natural: WRITE '*************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "        ---- NEW RESTART RECORD KEY ----         ");                                                                                       //Natural: WRITE '        ---- NEW RESTART RECORD KEY ----         '
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Restart_Key);                                                                                                                       //Natural: WRITE '=' #RESTART-KEY
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Updt_Dte);                                                                                                            //Natural: WRITE '=' BATCH-RESTART.RST-UPDT-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Updt_Tme);                                                                                                            //Natural: WRITE '=' BATCH-RESTART.RST-UPDT-TME
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Last_Rstrt_Tme);                                                                                                      //Natural: WRITE '=' BATCH-RESTART.RST-LAST-RSTRT-TME
        if (Global.isEscape()) return;
        getReports().write(0, "=",batch_Restart_Rst_Last_Rstrt_Dte);                                                                                                      //Natural: WRITE '=' BATCH-RESTART.RST-LAST-RSTRT-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "*************************************************");                                                                                       //Natural: WRITE '*************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE," ");                                                                                                                       //Natural: WRITE // ' '
        if (Global.isEscape()) return;
        batch_Restart_Rst_Rstrt_Data_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN BATCH-RESTART.RST-RSTRT-DATA-IND := 'Y'
        vw_batch_Restart.updateDBRow("GET_U");                                                                                                                            //Natural: UPDATE ( GET-U. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  UPDATE-BATCH-RESTART-RECORD
    }
    private void sub_Delete_Restart_Record() throws Exception                                                                                                             //Natural: DELETE-RESTART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //* ****************** UPDATE BATCH RESTART FOR NORMAL EOJ **************
        //*  THIS LOGIC DELETES THE BATCH RESTART RECORD, INDICATING THAT THE
        //*  JOB HAS BEEN RUN SUCESSFULLY.
        //* *********************************************************************
        GET_R:                                                                                                                                                            //Natural: GET BATCH-RESTART #RESTART-ISN
        vw_batch_Restart.readByID(pnd_Restart_Isn.getLong(), "GET_R");
        batch_Restart_Rst_Key.reset();                                                                                                                                    //Natural: RESET BATCH-RESTART.RST-KEY BATCH-RESTART.RST-CNT BATCH-RESTART.RST-ISN-NBR BATCH-RESTART.RST-CURR-DTE BATCH-RESTART.RST-CURR-TME BATCH-RESTART.RST-DATA-GRP ( * ) BATCH-RESTART.RST-RSTRT-DATA-IND BATCH-RESTART.RST-ACTVE-IND
        batch_Restart_Rst_Cnt.reset();
        batch_Restart_Rst_Isn_Nbr.reset();
        batch_Restart_Rst_Curr_Dte.reset();
        batch_Restart_Rst_Curr_Tme.reset();
        batch_Restart_Rst_Data_Grp.getValue("*").reset();
        batch_Restart_Rst_Rstrt_Data_Ind.reset();
        batch_Restart_Rst_Actve_Ind.reset();
        batch_Restart_Rst_End_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-END-DTE
        batch_Restart_Rst_End_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                                        //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-END-TME
        vw_batch_Restart.deleteDBRow("GET_R");                                                                                                                            //Natural: DELETE ( GET-R. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  DELETE-RESTART-RECORD
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"Contribution Processing Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 25T 'Contribution Processing Report' 63T 'Run Date:' *DATX / 25T '      Accepted Records        ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"      Accepted Records        ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 1 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"Contribution Processing Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 02 ) NOTITLE NOHDR 25T 'Contribution Processing Report' 63T 'Run Date:' *DATX / 25T '      Rejected Records        ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"      Rejected Records        ",new TabSetting(63),"Run Time:",Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"Contribution Processing Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 25T 'Contribution Processing Report' 63T 'Run Date:' *DATX / 25T '        Process Totals        ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"        Process Totals        ",new TabSetting(63),"Run Time:",Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        short decideConditionsMet408 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet408++;
            if (condition(pnd_Retry_Cnt.less(25)))                                                                                                                        //Natural: IF #RETRY-CNT < 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.equals(25)))                                                                                                                      //Natural: IF #RETRY-CNT = 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",intvl,reqid);                                                                                     //Natural: CALL 'CMROLL' INTVL REQID
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(25) && pnd_Retry_Cnt.lessOrEqual(50)))                                                                                    //Natural: IF #RETRY-CNT > 25 AND #RETRY-CNT <= 50
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(50)))                                                                                                                     //Natural: IF #RETRY-CNT > 50
            {
                getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                        //Natural: WRITE NOHDR '*****************************' /
                getReports().write(0, ReportOption.NOHDR,"* ADABAS RECORD ON HOLD     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* ADABAS RECORD ON HOLD     *' /
                getReports().write(0, ReportOption.NOHDR,"* RETRY COUNT EXCEEDED.     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* RETRY COUNT EXCEEDED.     *' /
                getReports().write(0, ReportOption.NOHDR,"* BACKOUT UPDATES.          *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* BACKOUT UPDATES.          *' /
                getReports().write(0, ReportOption.NOHDR,"*****************************");                                                                                //Natural: WRITE NOHDR '*****************************'
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",                  //Natural: WRITE NOHDR '****************************' / 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE / '****************************' /
                Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE,"****************************",NEWLINE);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");
        Global.format(3, "LS=133 PS=60");

        getReports().setDisplayColumns(0, "*----------------------------------------*");
    }
}
