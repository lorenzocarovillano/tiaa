/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:32 PM
**        * FROM NATURAL PROGRAM : Appb3300
************************************************************
**        * FILE NAME            : Appb3300.java
**        * CLASS NAME           : Appb3300
**        * INSTANCE NAME        : Appb3300
************************************************************
**********************************************************************
*
*  PROGRAM-ID    :  APPB3300
*
*  DESCRIPTION   :  READ UNMATCHED EXTRACT FILE AND UPDATE PRAP
*                   APPLCTN W/ NEW OVD STATUS
*
*  MODIFIED
* 09/23/08 DEVELBISS - RE-STOW NEW LAYOUT OF APPL301 FOR AUTO ENROLL
* 08/12/09 C. AVE    - RE-STOW NEW LAYOUT OF APPL301 FOR ACIS PERF
* 05/02/11 C. SCHNEIDER - RE-STOW NEW LAYOUT OF APPL301       (JHU)
* 06/27/11 C. SCHNEIDER - RE-STOW NEW LAYOUT OF APPL301       (TIC)
* 11/17/12 L. SHU    - RE-STOW NEW LAYOUT OF APPL301          (LPOA)
* 07/18/13 L. SHU    - RE-STOW FOR APPL301 FOR IRA SUBSTITUTION
* 09/12/13 B.NEWSOM - RESTOW FOR AP-NON-PROPRIETARY-PKG-IND ADDED TO
*                     ADDED TO APPL301.                      (MTSIN)
* 12/08/13 L. SHU   - USE SYSTEM DATE INSTEAD OF CONTROL BUSINESS
*                     RECORD DATE
*                   - REMOVE CODES FOR MATCHED DATA PROCESSING
*                   - REMOVE PREMIUM PROCESSING FOR UNMATCHED DATA
*                                                           (CUSPORT)
* 06/20/14 L. SHU   - RESTOW NECA4000 CHANGES FOR CREF REA   (CREA)
*                   - REMOVE FACTOR FILES
*                   - REMOVE LEGACY CODES AND MIT PROCESSING AS
*                     THE CODES NEVER GET EXECUTED
* 09/01/15 L. SHU   - RESTOW FOR NEW FIELDS IN APPL301 FOR BENE IN
*                     THE PLAN.                                 (BIP)
* 11/01/16 L. SHU   - RESTOW FOR NEW FIELDS IN APPL301 FOR 1IRA
* 06/20/17  (MITRAPU) PIN EXPANSION CHANGES. (C425939) STOW ONLY
* 11/09/19 B.NEWSOM - ADDED THE EXTENDED AREA OF APPL300- IISG
**********************************************************************
*
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb3300 extends BLNatBase
{
    // Data Areas
    private LdaAppl301 ldaAppl301;
    private LdaAppl3100 ldaAppl3100;
    private PdaAciadate pdaAciadate;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Today_Ccyymmdd;
    private DbsField pnd_Ccyymmdd;

    private DbsGroup pnd_Ccyymmdd__R_Field_1;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Cc;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Yy;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Mm;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Dd;
    private DbsField pnd_Mmddccyy;

    private DbsGroup pnd_Mmddccyy__R_Field_2;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Mm;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Dd;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Cc;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Yy;
    private DbsField pnd_Unmatch_Rec_Read;
    private DbsField pnd_Old_Ap_Status;
    private DbsField pnd_New_Ap_Status;
    private DbsField pnd_Date_D_1;
    private DbsField pnd_Date_D_2;
    private DbsField pnd_Check_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl301 = new LdaAppl301();
        registerRecord(ldaAppl301);
        registerRecord(ldaAppl301.getVw_prap());
        ldaAppl3100 = new LdaAppl3100();
        registerRecord(ldaAppl3100);
        localVariables = new DbsRecord();
        pdaAciadate = new PdaAciadate(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_Today_Ccyymmdd = localVariables.newFieldInRecord("pnd_Today_Ccyymmdd", "#TODAY-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Ccyymmdd = localVariables.newFieldInRecord("pnd_Ccyymmdd", "#CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Ccyymmdd__R_Field_1 = localVariables.newGroupInRecord("pnd_Ccyymmdd__R_Field_1", "REDEFINE", pnd_Ccyymmdd);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Cc = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Cc", "#CCYYMMDD-CC", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Yy = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Yy", "#CCYYMMDD-YY", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Mm = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Mm", "#CCYYMMDD-MM", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Dd = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Dd", "#CCYYMMDD-DD", FieldType.NUMERIC, 2);
        pnd_Mmddccyy = localVariables.newFieldInRecord("pnd_Mmddccyy", "#MMDDCCYY", FieldType.NUMERIC, 8);

        pnd_Mmddccyy__R_Field_2 = localVariables.newGroupInRecord("pnd_Mmddccyy__R_Field_2", "REDEFINE", pnd_Mmddccyy);
        pnd_Mmddccyy_Pnd_Mmddccyy_Mm = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Mm", "#MMDDCCYY-MM", FieldType.NUMERIC, 2);
        pnd_Mmddccyy_Pnd_Mmddccyy_Dd = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Dd", "#MMDDCCYY-DD", FieldType.NUMERIC, 2);
        pnd_Mmddccyy_Pnd_Mmddccyy_Cc = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Cc", "#MMDDCCYY-CC", FieldType.NUMERIC, 2);
        pnd_Mmddccyy_Pnd_Mmddccyy_Yy = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Yy", "#MMDDCCYY-YY", FieldType.NUMERIC, 2);
        pnd_Unmatch_Rec_Read = localVariables.newFieldInRecord("pnd_Unmatch_Rec_Read", "#UNMATCH-REC-READ", FieldType.NUMERIC, 7);
        pnd_Old_Ap_Status = localVariables.newFieldInRecord("pnd_Old_Ap_Status", "#OLD-AP-STATUS", FieldType.STRING, 1);
        pnd_New_Ap_Status = localVariables.newFieldInRecord("pnd_New_Ap_Status", "#NEW-AP-STATUS", FieldType.STRING, 1);
        pnd_Date_D_1 = localVariables.newFieldInRecord("pnd_Date_D_1", "#DATE-D-1", FieldType.DATE);
        pnd_Date_D_2 = localVariables.newFieldInRecord("pnd_Date_D_2", "#DATE-D-2", FieldType.DATE);
        pnd_Check_Date = localVariables.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl301.initializeValues();
        ldaAppl3100.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb3300() throws Exception
    {
        super("Appb3300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 52
                                                                                                                                                                          //Natural: PERFORM RTN-1B-GET-CONTROL-DATE
        sub_Rtn_1b_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
        //*   ----------------------
        //*   PROCESS UNMATCHED DATA
        //*   ----------------------
        //*  OIA
        //*  IISG
        //*  IISG
        //*  OIA
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 3 #AP-DATA-1 #AP-DATA-2 #AP-DATA-3 #AP-DATA-4 #AP-DATA-5 #AP-DATA-6 #AP-DATA-7 #AP-DATA-8 #AP-DATA-9 #AP-DATA-10 #AP-DATA-11 #AP-DATA-12 #AP-DATA-13 #AP-DATA-14 #AP-DATA-15 #AP-DATA-16 #AP-DATA-17 #AP-DATA-18 #AP-ADD-DATA-1 #AP-KEY
        while (condition(getWorkFiles().read(3, ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_1(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_2(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_3(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_4(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_5(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_6(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_7(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_8(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_9(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_10(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_11(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_12(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_13(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_14(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_15(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_16(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_17(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_18(), 
            ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1(), ldaAppl3100.getPnd_Ap_Key())))
        {
            //*  CUSPORT
            if (condition(ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type().equals(1)))                                                                             //Natural: IF #AP-AP-RECORD-TYPE = 1
            {
                pnd_Unmatch_Rec_Read.nadd(1);                                                                                                                             //Natural: ADD 1 TO #UNMATCH-REC-READ
                                                                                                                                                                          //Natural: PERFORM RTN-5-UPDATE-PRAP-UNMATCH
                sub_Rtn_5_Update_Prap_Unmatch();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM RTN-92-WRITE-UNMATCH-APPLICATION
                sub_Rtn_92_Write_Unmatch_Application();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CUSTPORT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CATCH ALL PENDING UPDATES /* KG AND BE 09/26/01
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, "********************************************",NEWLINE,"PROGRAM",Global.getPROGRAM(),new ColumnSpacing(5),"DATE",Global.getDATU(),NEWLINE,"PRAP WORKFILE",NEWLINE,"  TOTAL UNMATCHED READ : ",pnd_Unmatch_Rec_Read,  //Natural: WRITE ( 1 ) '********************************************' / 'PROGRAM' *PROGRAM 5X 'DATE' *DATU / 'PRAP WORKFILE' / '  TOTAL UNMATCHED READ : ' #UNMATCH-REC-READ ( EM = Z,ZZZ,ZZ9 ) / '********************************************'
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"********************************************");
        if (Global.isEscape()) return;
        //*  END OF MAINLINE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-1B-GET-CONTROL-DATE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-5-UPDATE-PRAP-UNMATCH
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-5A-NEW-OVERDUE-STATUS
        //* *---------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-92-WRITE-UNMATCH-APPLICATION
    }
    private void sub_Rtn_1b_Get_Control_Date() throws Exception                                                                                                           //Natural: RTN-1B-GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*  LS CUSPORT- CHANGED CONTROL DATE LOGIC TO USE CURRENT PROCESS DATE
        pnd_Today_Ccyymmdd.setValue(Global.getDATN());                                                                                                                    //Natural: MOVE *DATN TO #TODAY-CCYYMMDD
        //*  CUSPORT
        getReports().write(0, Global.getPROGRAM(),"(1050) CURRENT-DATE"," =",pnd_Today_Ccyymmdd);                                                                         //Natural: WRITE *PROGRAM '(1050) CURRENT-DATE' ' =' #TODAY-CCYYMMDD
        if (Global.isEscape()) return;
        //*  RTN-1B-GET-CONTROL-DATE
    }
    private void sub_Rtn_5_Update_Prap_Unmatch() throws Exception                                                                                                         //Natural: RTN-5-UPDATE-PRAP-UNMATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM RTN-5A-NEW-OVERDUE-STATUS
        sub_Rtn_5a_New_Overdue_Status();
        if (condition(Global.isEscape())) {return;}
        if (condition((pnd_New_Ap_Status.notEquals(pnd_Old_Ap_Status))))                                                                                                  //Natural: IF ( #NEW-AP-STATUS NE #OLD-AP-STATUS )
        {
            PND_PND_L1220:                                                                                                                                                //Natural: GET PRAP #AP-ISN
            ldaAppl301.getVw_prap().readByID(ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Isn().getLong(), "PND_PND_L1220");
            ldaAppl301.getPrap_Ap_Status().setValue(pnd_New_Ap_Status);                                                                                                   //Natural: MOVE #NEW-AP-STATUS TO AP-STATUS
            ldaAppl301.getVw_prap().updateDBRow("PND_PND_L1220");                                                                                                         //Natural: UPDATE ( ##L1220. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-5-UPDATE-PRAP-UNMATCH
    }
    private void sub_Rtn_5a_New_Overdue_Status() throws Exception                                                                                                         //Natural: RTN-5A-NEW-OVERDUE-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Old_Ap_Status.setValue(ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Status());                                                                                   //Natural: MOVE #AP-AP-STATUS TO #OLD-AP-STATUS #NEW-AP-STATUS
        pnd_New_Ap_Status.setValue(ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Status());
        //*  REMOVE DECIDE STATEMENT          /* CUSPORT
        pdaAciadate.getAciadate_Pnd_Date_N_1().setValue(ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Dt_App_Recvd());                                                        //Natural: MOVE #AP-AP-DT-APP-RECVD TO #DATE-N-1
        if (condition(! (DbsUtil.maskMatches(pdaAciadate.getAciadate_Pnd_Date_N_1(),"YYYYMMDD"))))                                                                        //Natural: IF #DATE-N-1 NE MASK ( YYYYMMDD )
        {
            getReports().write(0, Global.getPROGRAM(),"(1350) INVALID DATE FOR CALC OF OVERDUE STATUS");                                                                  //Natural: WRITE *PROGRAM '(1350) INVALID DATE FOR CALC OF OVERDUE STATUS'
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec());                                                                               //Natural: WRITE '=' #AP-AP-SOC-SEC
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract());                                                                            //Natural: WRITE '=' #AP-AP-T-CONTRACT
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type());                                                                           //Natural: WRITE '=' #AP-AP-RECORD-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Date_N_1());                                                                                            //Natural: WRITE '=' #DATE-N-1
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAciadate.getAciadate_Pnd_Date_N_2().setValue(pnd_Today_Ccyymmdd);                                                                                              //Natural: MOVE #TODAY-CCYYMMDD TO ACIADATE.#DATE-N-2
        pdaAciadate.getAciadate_Pnd_Function().setValue("01");                                                                                                            //Natural: MOVE '01' TO ACIADATE.#FUNCTION
        DbsUtil.callnat(Acindate.class , getCurrentProcessState(), pdaAciadate.getAciadate());                                                                            //Natural: CALLNAT 'ACINDATE' ACIADATE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAciadate.getAciadate_Pnd_Error_Code().notEquals(getZero())))                                                                                     //Natural: IF ACIADATE.#ERROR-CODE NE 0
        {
            getReports().write(0, Global.getPROGRAM(),"(1490) INVALID DATES FOR CALC OF OVERDUE STATUS");                                                                 //Natural: WRITE *PROGRAM '(1490) INVALID DATES FOR CALC OF OVERDUE STATUS'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Error_Code());                                                                                          //Natural: WRITE '=' ACIADATE.#ERROR-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Error_Msg());                                                                                           //Natural: WRITE '=' ACIADATE.#ERROR-MSG
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Isn());                                                                                  //Natural: WRITE '=' #AP-ISN
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Soc_Sec());                                                                               //Natural: WRITE '=' #AP-AP-SOC-SEC
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_T_Contract());                                                                            //Natural: WRITE '=' #AP-AP-T-CONTRACT
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Ap_Record_Type());                                                                           //Natural: WRITE '=' #AP-AP-RECORD-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Date_N_1());                                                                                            //Natural: WRITE '=' #DATE-N-1
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Date_N_2());                                                                                            //Natural: WRITE '=' #DATE-N-2
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAciadate.getAciadate_Pnd_Diff().less(getZero())))                                                                                                //Natural: IF #DIFF LT 0
        {
            pdaAciadate.getAciadate_Pnd_Diff().setValue(0);                                                                                                               //Natural: MOVE 0 TO #DIFF
        }                                                                                                                                                                 //Natural: END-IF
        //*  REMOVE DECIDE STATEMENT  /* CUSPORT
        //*  REMOVE CALLING APPB3310 AS IT CHECK FOR NON IRA & NOT A SGRD CONTRACT
        //*  WE DO NOT HAVE A NON IRA LEGACY CONTRACT IN PRODUCTION
        //*  OVERDUE 1
        //*  OVERDUE 2
        //*  OVERDUE 3
        short decideConditionsMet1971 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #DIFF;//Natural: VALUE 0:27
        if (condition(((pdaAciadate.getAciadate_Pnd_Diff().greaterOrEqual(0) && pdaAciadate.getAciadate_Pnd_Diff().lessOrEqual(27)))))
        {
            decideConditionsMet1971++;
            ignore();
        }                                                                                                                                                                 //Natural: VALUE 28:60
        else if (condition(((pdaAciadate.getAciadate_Pnd_Diff().greaterOrEqual(28) && pdaAciadate.getAciadate_Pnd_Diff().lessOrEqual(60)))))
        {
            decideConditionsMet1971++;
            pnd_New_Ap_Status.setValue("G");                                                                                                                              //Natural: MOVE 'G' TO #NEW-AP-STATUS
        }                                                                                                                                                                 //Natural: VALUE 61:90
        else if (condition(((pdaAciadate.getAciadate_Pnd_Diff().greaterOrEqual(61) && pdaAciadate.getAciadate_Pnd_Diff().lessOrEqual(90)))))
        {
            decideConditionsMet1971++;
            pnd_New_Ap_Status.setValue("H");                                                                                                                              //Natural: MOVE 'H' TO #NEW-AP-STATUS
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            //*  OVERDUE 3
            pnd_New_Ap_Status.setValue("I");                                                                                                                              //Natural: MOVE 'I' TO #NEW-AP-STATUS
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  RTN-5A-NEW-OVERDUE-STATUS
    }
    private void sub_Rtn_92_Write_Unmatch_Application() throws Exception                                                                                                  //Natural: RTN-92-WRITE-UNMATCH-APPLICATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------------------------------
        //*  OIA
        //*  IISG
        //*  IISG
        //*  OIA
        getWorkFiles().write(4, false, ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_1(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_2(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_3(),  //Natural: WRITE WORK FILE 4 #AP-DATA-1 #AP-DATA-2 #AP-DATA-3 #AP-DATA-4 #AP-DATA-5 #AP-DATA-6 #AP-DATA-7 #AP-DATA-8 #AP-DATA-9 #AP-DATA-10 #AP-DATA-11 #AP-DATA-12 #AP-DATA-13 #AP-DATA-14 #AP-DATA-15 #AP-DATA-16 #AP-DATA-17 #AP-DATA-18 #AP-ADD-DATA-1 #AP-KEY
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_4(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_5(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_6(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_7(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_8(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_9(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_10(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_11(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_12(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_13(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_14(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_15(), 
            ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_16(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_17(), ldaAppl3100.getPnd_Ap_Work_File_Pnd_Ap_Data_18(), 
            ldaAppl3100.getPnd_Ap_Add_Work_File_Pnd_Ap_Add_Data_1(), ldaAppl3100.getPnd_Ap_Key());
        //*  RTN-92-WRITE-UNMATCH-APPLICATION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=52");
    }
}
