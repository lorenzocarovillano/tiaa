/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:58 PM
**        * FROM NATURAL PROGRAM : Appb2091
************************************************************
**        * FILE NAME            : Appb2091.java
**        * CLASS NAME           : Appb2091
**        * INSTANCE NAME        : Appb2091
************************************************************
************************************************************************
* PROGRAM  : APPB2091 - ACIS MATCH AND RELEASE DAILY REPORT            *
* FUNCTION : READS EXTRACT FILE FROM APPB2090 AND CREATES A REPORT     *
*            SORTED BY PLAN OF ALL OMNI APPLICATION RECORDS ON THE     *
*            ACIS PRAP FILE WHERE MONEY WAS APPLIED TO THE CONTRACT IN *
*            OMNI BUT THE STATUS OF THE CONTRACT ON THE ACIS PRAP FILE *
*            IS NOT MATCHED/RELEASED. THE START DATE FOR THE REPORT    *
*            IS APRIL 1, 2008.                                         *
* UPDATED  : 04/22/08 K.GATES   - PROGRAM CREATED                      *
*          : 03/10/10 C.AVE     - INCLUDED IRA INDEXED PRODUCTS-TIGR   *
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2091 extends BLNatBase
{
    // Data Areas
    private PdaAciadate pdaAciadate;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Match_Release_File;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_No;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Soc_Sec;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Tiaa_Cntrct;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Cref_Cert;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Pin_Nbr;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Date_Entered;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Cor_Last_Name;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Cor_First_Name;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Lob;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type;
    private DbsField pnd_Input_Match_Release_File_Pnd_Mr_Filler;

    private DbsGroup pnd_Pnd_Miscellanous_Variables;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Read_Count;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Plan_Total;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Error_Count;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Write_Cnt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Ap_Name;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered;

    private DbsGroup pnd_Pnd_Miscellanous_Variables__R_Field_1;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_N;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Full;

    private DbsGroup pnd_Pnd_Miscellanous_Variables__R_Field_2;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Ccyy;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Mm;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Dd;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Rev;

    private DbsGroup pnd_Pnd_Miscellanous_Variables__R_Field_3;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Mm;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Dd;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Ccyy;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Date_Entered;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Remarks;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Ssn_N;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Old_Plan;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Debug;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Mr_Sgrd_Plan_NoOld;
    private DbsField readWork01Pnd_Mr_Sgrd_Plan_NoCount119;
    private DbsField readWork01Pnd_Mr_Sgrd_Plan_NoCount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAciadate = new PdaAciadate(localVariables);

        // Local Variables

        pnd_Input_Match_Release_File = localVariables.newGroupInRecord("pnd_Input_Match_Release_File", "#INPUT-MATCH-RELEASE-FILE");
        pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_No = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_No", 
            "#MR-SGRD-PLAN-NO", FieldType.STRING, 6);
        pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No", 
            "#MR-SGRD-SUBPLAN-NO", FieldType.STRING, 6);
        pnd_Input_Match_Release_File_Pnd_Mr_Soc_Sec = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Soc_Sec", "#MR-SOC-SEC", 
            FieldType.NUMERIC, 9);
        pnd_Input_Match_Release_File_Pnd_Mr_Tiaa_Cntrct = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Tiaa_Cntrct", 
            "#MR-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Input_Match_Release_File_Pnd_Mr_Cref_Cert = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Cref_Cert", 
            "#MR-CREF-CERT", FieldType.STRING, 10);
        pnd_Input_Match_Release_File_Pnd_Mr_Pin_Nbr = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Pin_Nbr", "#MR-PIN-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Input_Match_Release_File_Pnd_Mr_Date_Entered = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Date_Entered", 
            "#MR-DATE-ENTERED", FieldType.NUMERIC, 8);
        pnd_Input_Match_Release_File_Pnd_Mr_Cor_Last_Name = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Cor_Last_Name", 
            "#MR-COR-LAST-NAME", FieldType.STRING, 30);
        pnd_Input_Match_Release_File_Pnd_Mr_Cor_First_Name = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Cor_First_Name", 
            "#MR-COR-FIRST-NAME", FieldType.STRING, 30);
        pnd_Input_Match_Release_File_Pnd_Mr_Lob = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Lob", "#MR-LOB", FieldType.STRING, 
            1);
        pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type", "#MR-LOB-TYPE", 
            FieldType.STRING, 1);
        pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type", 
            "#MR-APPLCNT-REQ-TYPE", FieldType.STRING, 1);
        pnd_Input_Match_Release_File_Pnd_Mr_Filler = pnd_Input_Match_Release_File.newFieldInGroup("pnd_Input_Match_Release_File_Pnd_Mr_Filler", "#MR-FILLER", 
            FieldType.STRING, 81);

        pnd_Pnd_Miscellanous_Variables = localVariables.newGroupInRecord("pnd_Pnd_Miscellanous_Variables", "##MISCELLANOUS-VARIABLES");
        pnd_Pnd_Miscellanous_Variables_Pnd_Read_Count = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Read_Count", 
            "#READ-COUNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Plan_Total = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Plan_Total", 
            "#PLAN-TOTAL", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Error_Count = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Error_Count", 
            "##ERROR-COUNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Write_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Write_Cnt", 
            "#WRITE-CNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Ap_Name = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Ap_Name", "#AP-NAME", 
            FieldType.STRING, 40);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered", 
            "#DT-ENTERED", FieldType.STRING, 8);

        pnd_Pnd_Miscellanous_Variables__R_Field_1 = pnd_Pnd_Miscellanous_Variables.newGroupInGroup("pnd_Pnd_Miscellanous_Variables__R_Field_1", "REDEFINE", 
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_N = pnd_Pnd_Miscellanous_Variables__R_Field_1.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_N", 
            "#DT-ENTERED-N", FieldType.NUMERIC, 8);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Full = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Full", 
            "#DT-ENTERED-FULL", FieldType.STRING, 8);

        pnd_Pnd_Miscellanous_Variables__R_Field_2 = pnd_Pnd_Miscellanous_Variables.newGroupInGroup("pnd_Pnd_Miscellanous_Variables__R_Field_2", "REDEFINE", 
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Full);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Ccyy = pnd_Pnd_Miscellanous_Variables__R_Field_2.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Ccyy", 
            "#DT-ENTERED-F-CCYY", FieldType.STRING, 4);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Mm = pnd_Pnd_Miscellanous_Variables__R_Field_2.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Mm", 
            "#DT-ENTERED-F-MM", FieldType.STRING, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Dd = pnd_Pnd_Miscellanous_Variables__R_Field_2.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Dd", 
            "#DT-ENTERED-F-DD", FieldType.STRING, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Rev = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Rev", 
            "#DT-ENTERED-REV", FieldType.STRING, 8);

        pnd_Pnd_Miscellanous_Variables__R_Field_3 = pnd_Pnd_Miscellanous_Variables.newGroupInGroup("pnd_Pnd_Miscellanous_Variables__R_Field_3", "REDEFINE", 
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Rev);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Mm = pnd_Pnd_Miscellanous_Variables__R_Field_3.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Mm", 
            "#DT-ENTERED-R-MM", FieldType.STRING, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Dd = pnd_Pnd_Miscellanous_Variables__R_Field_3.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Dd", 
            "#DT-ENTERED-R-DD", FieldType.STRING, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Ccyy = pnd_Pnd_Miscellanous_Variables__R_Field_3.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Ccyy", 
            "#DT-ENTERED-R-CCYY", FieldType.STRING, 4);
        pnd_Pnd_Miscellanous_Variables_Pnd_Date_Entered = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Date_Entered", 
            "#DATE-ENTERED", FieldType.DATE);
        pnd_Pnd_Miscellanous_Variables_Pnd_Remarks = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Remarks", "#REMARKS", 
            FieldType.STRING, 8);
        pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type", 
            "#CONTRACT-TYPE", FieldType.STRING, 7);
        pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count", 
            "#LINE-COUNT", FieldType.NUMERIC, 3);
        pnd_Pnd_Miscellanous_Variables_Pnd_Ssn_N = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Ssn_N", "#SSN-N", 
            FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Old_Plan = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Old_Plan", "#OLD-PLAN", 
            FieldType.STRING, 6);
        pnd_Pnd_Miscellanous_Variables_Pnd_Debug = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Debug", "#DEBUG", 
            FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found", 
            "#PREM-FOUND", FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found", 
            "#EXCEPT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason", 
            "#EXCEPT-REASON", FieldType.STRING, 35);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt", 
            "#EXCEPTION-CNT", FieldType.NUMERIC, 6);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt", 
            "#EXC-LINE-CNT", FieldType.NUMERIC, 3);
        pnd_Pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt", 
            "#MATCH-LINE-CNT", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Mr_Sgrd_Plan_NoOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Mr_Sgrd_Plan_No_OLD", "Pnd_Mr_Sgrd_Plan_No_OLD", FieldType.STRING, 
            6);
        readWork01Pnd_Mr_Sgrd_Plan_NoCount119 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Mr_Sgrd_Plan_No_COUNT_119", "Pnd_Mr_Sgrd_Plan_No_COUNT_119", 
            FieldType.NUMERIC, 9);
        readWork01Pnd_Mr_Sgrd_Plan_NoCount = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Mr_Sgrd_Plan_No_COUNT", "Pnd_Mr_Sgrd_Plan_No_COUNT", 
            FieldType.NUMERIC, 9);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2091() throws Exception
    {
        super("Appb2091");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB2091", onError);
        setupReports();
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 RECORD #INPUT-MATCH-RELEASE-FILE
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Match_Release_File)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Pnd_Miscellanous_Variables_Pnd_Read_Count.nadd(1);                                                                                                        //Natural: ADD 1 TO #READ-COUNT
            if (condition(pnd_Pnd_Miscellanous_Variables_Pnd_Read_Count.equals(1)))                                                                                       //Natural: IF #READ-COUNT = 1
            {
                                                                                                                                                                          //Natural: PERFORM REPORT-HEADING
                sub_Report_Heading();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count.greater(46)))                                                                                     //Natural: IF #LINE-COUNT > 46
            {
                                                                                                                                                                          //Natural: PERFORM REPORT-HEADING
                sub_Report_Heading();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
            sub_Write_Detail_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #MR-SGRD-PLAN-NO
            //*                                                                                                                                                           //Natural: AT END OF DATA
            readWork01Pnd_Mr_Sgrd_Plan_NoOld.setValue(pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_No);                                                                  //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            readWork01Pnd_Mr_Sgrd_Plan_NoCount119.resetBreak();
            readWork01Pnd_Mr_Sgrd_Plan_NoCount.resetBreak();
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GRAND-TOTAL
            sub_Grand_Total();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-HEADING
        //*  31T 'TIAA #'   42T 'CREF #' 53T 'LOB'   61T 'ENTER DATE'
        //*  74T 'POLICYHOLDER NAME'  115T 'REMARKS' /
        //*  1T '-' (8) 10T '-' (9)  22T '-' (7) 31T '-' (10) 42T '-' (10)
        //*  53T '-' (7) 61T '-' (12)
        //*  74T '-' (40)       115T '-' (8)
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-LINE
        //*  31T  #MR-TIAA-CNTRCT
        //*  42T  #MR-CREF-CERT
        //*  53T  #CONTRACT-TYPE
        //*  61T  #DATE-ENTERED (EM=MM/DD/YYYY)
        //*  74T  #AP-NAME
        //*  115T  #REMARKS
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-BY-PLAN
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRAND-TOTAL
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Report_Heading() throws Exception                                                                                                                    //Natural: REPORT-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count.reset();                                                                                                            //Natural: RESET #LINE-COUNT
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(24),"TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA",new  //Natural: WRITE ( 1 ) NOTITLE // 1T *PROGRAM 24T 'TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA' 102T 'RUN DATE' *DATU / 35T 'COLLEGE RETIREMENT  EQUITIES FUND' 102T 'PAGE NO.' *PAGE-NUMBER ( 1 ) // 33T 'DAILY PRAP MATCH RELEASE ERROR REPORT' // 1T 'PLAN NUMBER - ' #MR-SGRD-PLAN-NO // 1T 'SUB PLAN' 10T 'SOC SEC' 22T 'PIN #' 36T 'TIAA #' 47T 'CREF #' 58T 'LOB' 66T 'ENTER DATE' 79T 'POLICYHOLDER NAME' 120T 'REMARKS' / 1T '-' ( 8 ) 10T '-' ( 9 ) 22T '-' ( 12 ) 36T '-' ( 10 ) 47T '-' ( 10 ) 58T '-' ( 7 ) 66T '-' ( 12 ) 79T '-' ( 40 ) 120T '-' ( 8 )
            TabSetting(102),"RUN DATE",Global.getDATU(),NEWLINE,new TabSetting(35),"COLLEGE RETIREMENT  EQUITIES FUND",new TabSetting(102),"PAGE NO.",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
            TabSetting(33),"DAILY PRAP MATCH RELEASE ERROR REPORT",NEWLINE,NEWLINE,new TabSetting(1),"PLAN NUMBER - ",pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_No,NEWLINE,NEWLINE,new 
            TabSetting(1),"SUB PLAN",new TabSetting(10),"SOC SEC",new TabSetting(22),"PIN #",new TabSetting(36),"TIAA #",new TabSetting(47),"CREF #",new 
            TabSetting(58),"LOB",new TabSetting(66),"ENTER DATE",new TabSetting(79),"POLICYHOLDER NAME",new TabSetting(120),"REMARKS",NEWLINE,new TabSetting(1),"-",new 
            RepeatItem(8),new TabSetting(10),"-",new RepeatItem(9),new TabSetting(22),"-",new RepeatItem(12),new TabSetting(36),"-",new RepeatItem(10),new 
            TabSetting(47),"-",new RepeatItem(10),new TabSetting(58),"-",new RepeatItem(7),new TabSetting(66),"-",new RepeatItem(12),new TabSetting(79),"-",new 
            RepeatItem(40),new TabSetting(120),"-",new RepeatItem(8));
        if (Global.isEscape()) return;
        //*  REPORT-HEADING
    }
    private void sub_Write_Detail_Line() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        pnd_Pnd_Miscellanous_Variables_Pnd_Write_Cnt.nadd(1);                                                                                                             //Natural: ADD 1 TO #WRITE-CNT
        pnd_Pnd_Miscellanous_Variables_Pnd_Ap_Name.reset();                                                                                                               //Natural: RESET #AP-NAME #DATE-ENTERED
        pnd_Pnd_Miscellanous_Variables_Pnd_Date_Entered.reset();
        pnd_Pnd_Miscellanous_Variables_Pnd_Ap_Name.setValue(DbsUtil.compress(pnd_Input_Match_Release_File_Pnd_Mr_Cor_Last_Name, ",", pnd_Input_Match_Release_File_Pnd_Mr_Cor_First_Name)); //Natural: COMPRESS #MR-COR-LAST-NAME ',' #MR-COR-FIRST-NAME INTO #AP-NAME
        if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Date_Entered.notEquals(getZero())))                                                                             //Natural: IF #MR-DATE-ENTERED NE 0
        {
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_N.setValue(pnd_Input_Match_Release_File_Pnd_Mr_Date_Entered);                                                   //Natural: ASSIGN #DT-ENTERED-N := #MR-DATE-ENTERED
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Full.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered);                                                   //Natural: ASSIGN #DT-ENTERED-FULL := #DT-ENTERED
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Ccyy.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Ccyy);                                          //Natural: ASSIGN #DT-ENTERED-R-CCYY := #DT-ENTERED-F-CCYY
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Mm.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Mm);                                              //Natural: ASSIGN #DT-ENTERED-R-MM := #DT-ENTERED-F-MM
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_R_Dd.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_F_Dd);                                              //Natural: ASSIGN #DT-ENTERED-R-DD := #DT-ENTERED-F-DD
            pnd_Pnd_Miscellanous_Variables_Pnd_Date_Entered.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered_Rev);             //Natural: MOVE EDITED #DT-ENTERED-REV TO #DATE-ENTERED ( EM = MMDDYYYY )
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet199 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #MR-APPLCNT-REQ-TYPE;//Natural: VALUE 'P'
        if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("P"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("PAPER   ");                                                                                              //Natural: ASSIGN #REMARKS = 'PAPER   '
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("I"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("INTERNET");                                                                                              //Natural: ASSIGN #REMARKS = 'INTERNET'
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("R"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("REGULAR");                                                                                               //Natural: ASSIGN #REMARKS = 'REGULAR'
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("N"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("NEGATIVE");                                                                                              //Natural: ASSIGN #REMARKS = 'NEGATIVE'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("X"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("REMT/ENR");                                                                                              //Natural: ASSIGN #REMARKS = 'REMT/ENR'
        }                                                                                                                                                                 //Natural: VALUE 'O'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("O"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("ERS/ODE ");                                                                                              //Natural: ASSIGN #REMARKS = 'ERS/ODE '
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("L"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("LOAN/ENR");                                                                                              //Natural: ASSIGN #REMARKS = 'LOAN/ENR'
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("E"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("EAS/ENR ");                                                                                              //Natural: ASSIGN #REMARKS = 'EAS/ENR '
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("A"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("ADM/INET");                                                                                              //Natural: ASSIGN #REMARKS = 'ADM/INET'
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("B"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("BULK/ENR");                                                                                              //Natural: ASSIGN #REMARKS = 'BULK/ENR'
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pnd_Input_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.equals("C"))))
        {
            decideConditionsMet199++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("IND/INET");                                                                                              //Natural: ASSIGN #REMARKS = 'IND/INET'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Pnd_Miscellanous_Variables_Pnd_Remarks.setValue("UNKNOWN");                                                                                               //Natural: ASSIGN #REMARKS = 'UNKNOWN'
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet225 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MR-LOB = 'D'
        if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob.equals("D")))
        {
            decideConditionsMet225++;
            short decideConditionsMet227 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MR-LOB-TYPE = '2' OR = '8'
            if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("2") || pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("8")))
            {
                decideConditionsMet227++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("RA");                                                                                          //Natural: ASSIGN #CONTRACT-TYPE = 'RA'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '7' OR = '9'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("7") || pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("9")))
            {
                decideConditionsMet227++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("GRA");                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'GRA'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE EQ '5'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("5")))
            {
                decideConditionsMet227++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("RS");                                                                                          //Natural: ASSIGN #CONTRACT-TYPE = 'RS'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE EQ '6'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("6")))
            {
                decideConditionsMet227++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("RL");                                                                                          //Natural: ASSIGN #CONTRACT-TYPE = 'RL'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE EQ 'A'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("A")))
            {
                decideConditionsMet227++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("RC");                                                                                          //Natural: ASSIGN #CONTRACT-TYPE = 'RC'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("UNKNOWN");                                                                                     //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #MR-LOB = 'S'
        else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob.equals("S")))
        {
            decideConditionsMet225++;
            short decideConditionsMet242 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MR-LOB-TYPE = '2' OR = '8'
            if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("2") || pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("8")))
            {
                decideConditionsMet242++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("SRA");                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'SRA'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '3'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("3")))
            {
                decideConditionsMet242++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("GSRA");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'GSRA'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '4'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("4")))
            {
                decideConditionsMet242++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("GA");                                                                                          //Natural: ASSIGN #CONTRACT-TYPE = 'GA'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '5'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("5")))
            {
                decideConditionsMet242++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("RSP");                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'RSP'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '6'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("6")))
            {
                decideConditionsMet242++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("RSP2");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'RSP2'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '7'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("7")))
            {
                decideConditionsMet242++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("RCP");                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'RCP'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '9'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("9")))
            {
                decideConditionsMet242++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("GA5");                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'GA5'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("UNKNOWN");                                                                                     //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #MR-LOB = 'I'
        else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob.equals("I")))
        {
            decideConditionsMet225++;
            short decideConditionsMet261 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MR-LOB-TYPE = '1' OR = '2'
            if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("1") || pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("2")))
            {
                decideConditionsMet261++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("IRA");                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'IRA'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '3'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("3")))
            {
                decideConditionsMet261++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("IRAR");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'IRAR'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '4'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("4")))
            {
                decideConditionsMet261++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("IRAC");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'IRAC'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '6'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("6")))
            {
                decideConditionsMet261++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("IRAS");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'IRAS'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '5'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("5")))
            {
                decideConditionsMet261++;
                //*  TIGR-START
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("KEOG");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'KEOG'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '7'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("7")))
            {
                decideConditionsMet261++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("IRIR");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'IRIR'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '8'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("8")))
            {
                decideConditionsMet261++;
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("IRIC");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'IRIC'
            }                                                                                                                                                             //Natural: WHEN #MR-LOB-TYPE = '9'
            else if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Lob_Type.equals("9")))
            {
                decideConditionsMet261++;
                //*  TIGR-END
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("IRIS");                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'IRIS'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("UNKNOWN");                                                                                     //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type.setValue("UNKNOWN");                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No,new TabSetting(9),pnd_Input_Match_Release_File_Pnd_Mr_Soc_Sec,new  //Natural: WRITE ( 1 ) / 2T #MR-SGRD-SUBPLAN-NO 9T #MR-SOC-SEC 21T #MR-PIN-NBR 36T #MR-TIAA-CNTRCT 47T #MR-CREF-CERT 58T #CONTRACT-TYPE 66T #DATE-ENTERED ( EM = MM/DD/YYYY ) 79T #AP-NAME 120T #REMARKS
            TabSetting(21),pnd_Input_Match_Release_File_Pnd_Mr_Pin_Nbr,new TabSetting(36),pnd_Input_Match_Release_File_Pnd_Mr_Tiaa_Cntrct,new TabSetting(47),pnd_Input_Match_Release_File_Pnd_Mr_Cref_Cert,new 
            TabSetting(58),pnd_Pnd_Miscellanous_Variables_Pnd_Contract_Type,new TabSetting(66),pnd_Pnd_Miscellanous_Variables_Pnd_Date_Entered, new ReportEditMask 
            ("MM/DD/YYYY"),new TabSetting(79),pnd_Pnd_Miscellanous_Variables_Pnd_Ap_Name,new TabSetting(120),pnd_Pnd_Miscellanous_Variables_Pnd_Remarks);
        if (Global.isEscape()) return;
        pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count.nadd(3);                                                                                                            //Natural: ADD 3 TO #LINE-COUNT
        //*  WRITE-DETAIL-RECORD
    }
    private void sub_Total_By_Plan() throws Exception                                                                                                                     //Natural: TOTAL-BY-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        if (condition(pnd_Pnd_Miscellanous_Variables_Pnd_Line_Count.greater(45)))                                                                                         //Natural: IF #LINE-COUNT > 45
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(24),"TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA",new  //Natural: WRITE ( 1 ) // 1T *PROGRAM 24T 'TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA' 102T 'RUN DATE' *DATU / 35T 'COLLEGE RETIREMENT  EQUITIES FUND' 102T 'PAGE NO.' *PAGE-NUMBER ( 1 ) // 33T 'DAILY PRAP MATCH RELEASE ERROR REPORT' // 1T 'PLAN NUMBER - ' #OLD-PLAN //
                TabSetting(102),"RUN DATE",Global.getDATU(),NEWLINE,new TabSetting(35),"COLLEGE RETIREMENT  EQUITIES FUND",new TabSetting(102),"PAGE NO.",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
                TabSetting(33),"DAILY PRAP MATCH RELEASE ERROR REPORT",NEWLINE,NEWLINE,new TabSetting(1),"PLAN NUMBER - ",pnd_Pnd_Miscellanous_Variables_Pnd_Old_Plan,
                NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(10),"TOTAL BY PLAN         : ",pnd_Pnd_Miscellanous_Variables_Pnd_Plan_Total,NEWLINE,new  //Natural: WRITE ( 1 ) /// 10T 'TOTAL BY PLAN         : ' #PLAN-TOTAL / 10T '---------------------------------'
            TabSetting(10),"---------------------------------");
        if (Global.isEscape()) return;
        pnd_Pnd_Miscellanous_Variables_Pnd_Plan_Total.reset();                                                                                                            //Natural: RESET #PLAN-TOTAL
        //*  TOTAL-BY-PLAN
    }
    private void sub_Grand_Total() throws Exception                                                                                                                       //Natural: GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(24),"TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA",new  //Natural: WRITE ( 1 ) // 1T *PROGRAM 24T 'TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA' 102T 'RUN DATE' *DATU / 35T 'COLLEGE RETIREMENT  EQUITIES FUND' 102T 'PAGE NO.' *PAGE-NUMBER ( 1 ) // 33T 'DAILY PRAP MATCH RELEASE ERROR REPORT' // //// 10T '        CONTROL TOTALS ' / 10T '-----------------------------------' // 10T 'TOTAL MATCH/RELEASE RECORDS : ' #READ-COUNT
            TabSetting(102),"RUN DATE",Global.getDATU(),NEWLINE,new TabSetting(35),"COLLEGE RETIREMENT  EQUITIES FUND",new TabSetting(102),"PAGE NO.",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
            TabSetting(33),"DAILY PRAP MATCH RELEASE ERROR REPORT",NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(10),"        CONTROL TOTALS ",NEWLINE,new 
            TabSetting(10),"-----------------------------------",NEWLINE,NEWLINE,new TabSetting(10),"TOTAL MATCH/RELEASE RECORDS : ",pnd_Pnd_Miscellanous_Variables_Pnd_Read_Count);
        if (Global.isEscape()) return;
        //*  GRAND-TOTAL
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE // 'CONTRACT:' 25T #MR-TIAA-CNTRCT / 'PIN:' 25T #MR-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),pnd_Input_Match_Release_File_Pnd_Mr_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),pnd_Input_Match_Release_File_Pnd_Mr_Pin_Nbr);
        getReports().write(0, "MATCH RELEASE RECS WRITTEN:",pnd_Pnd_Miscellanous_Variables_Pnd_Write_Cnt, new ReportEditMask ("ZZZ,ZZ9"));                                //Natural: WRITE 'MATCH RELEASE RECS WRITTEN:' #WRITE-CNT ( EM = ZZZ,ZZ9 )
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_NoIsBreak = pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_No.isBreak(endOfData);
        if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_NoIsBreak))
        {
            pnd_Pnd_Miscellanous_Variables_Pnd_Old_Plan.setValue(readWork01Pnd_Mr_Sgrd_Plan_NoOld);                                                                       //Natural: MOVE OLD ( #MR-SGRD-PLAN-NO ) TO #OLD-PLAN
            pnd_Pnd_Miscellanous_Variables_Pnd_Plan_Total.setValue(readWork01Pnd_Mr_Sgrd_Plan_NoCount119);                                                                //Natural: MOVE COUNT ( #MR-SGRD-PLAN-NO ) TO #PLAN-TOTAL
                                                                                                                                                                          //Natural: PERFORM TOTAL-BY-PLAN
            sub_Total_By_Plan();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Input_Match_Release_File_Pnd_Mr_Sgrd_Plan_No.equals(" ")))                                                                                  //Natural: IF #MR-SGRD-PLAN-NO EQ ' '
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM REPORT-HEADING
                sub_Report_Heading();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            readWork01Pnd_Mr_Sgrd_Plan_NoCount119.setDec(new DbsDecimal(0));                                                                                              //Natural: END-BREAK
        }
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52");
    }
}
