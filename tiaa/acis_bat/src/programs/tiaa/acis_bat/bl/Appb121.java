/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:09 PM
**        * FROM NATURAL PROGRAM : Appb121
************************************************************
**        * FILE NAME            : Appb121.java
**        * CLASS NAME           : Appb121
**        * INSTANCE NAME        : Appb121
************************************************************
* *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
* TO CREATE A RECORD FOR POST, DO THE FOLLOWING IN ORDER:
* 1.0  POST OPEN
* 2.0  STORE-RECORD
* 2.5  POWER-IMAGE - THIS IS OPTIONAL
* 2.6  STORE-RECORD - ADDITIONAL STORE FOR POWER-IMAGE
* 3.0  POST CLOSE
* 4.0  END TRANSACTION - TO PREVENT HOLDING RECORDS WHEN OTHER JOBS ARE
*                        ARE USING POST
*
* **YOU MUST DO THIS FOR EACH RECORD!!
*
* REPORTS
* -------
* NI9705M1 PURDUE FOLLOWUP LETTER STATISTICS
* NI9705M2 PURDUE FOLLOWUP LETTERS BYPASSED
* NI9705M3 PURDUE FOLLOWUP LETTERS PROCESSED
*
* 08/10/2013 B. NEWSOM THE ACIS ONLINE COMPONENTS FOR ENTRY INTO ACIS
*                      WILL BE MODIFIED TO REMOVE THE USE OF THE PDQ
*                      INTERFACE WHEN ENTERING ACIS. THE ACIS BATCH
*                      MODULES WILL BE CHANGED TO NOT REFERENCE THE PDQ
*                      INTERFACE.  THE ACIS BATCH COMPONENTS WILL BE
*                      CHANGE TO USE THE CURRENT PROCESSING DATE INSTEAD
*                      OF THE PREMIUM CONTROL BUSINESS DATE. (CUSPORT)
* 06/15/17 BARUA - PIN EXPANSION CHANGES. (CHG425939)  PINE
* *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb121 extends BLNatBase
{
    // Data Areas
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private LdaPstl6765 ldaPstl6765;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_File;
    private DbsField pnd_Input_File_Pnd_Alp_Plan_Num;
    private DbsField pnd_Input_File_Pnd_Alp_Ssn;
    private DbsField pnd_Input_File_Pnd_Alp_Part_Name;
    private DbsField pnd_Input_File_Pnd_Alp_Mail_Addr_L1;
    private DbsField pnd_Input_File_Pnd_Alp_Mail_Addr_L2;
    private DbsField pnd_Input_File_Pnd_Alp_Mail_City;
    private DbsField pnd_Input_File_Pnd_Alp_Mail_State;
    private DbsField pnd_Input_File_Pnd_Alp_Mail_Zip;

    private DbsGroup pnd_Input_File__R_Field_1;
    private DbsField pnd_Input_File_Pnd_Alp_Mail_Zip5;
    private DbsField pnd_Input_File_Pnd_Alp_Mail_Zip4;
    private DbsField pnd_Input_File_Pnd_Alp_Part_Sub_Plan;
    private DbsField pnd_Input_File_Pnd_Alp_Brth_Date;

    private DbsGroup pnd_Input_File__R_Field_2;
    private DbsField pnd_Input_File_Pnd_Alp_Brth_Mm;
    private DbsField pnd_Input_File_Pnd_Alp_Brth_Dd;
    private DbsField pnd_Input_File_Pnd_Alp_Brth_Ccyy;
    private DbsField pnd_Input_File_Pnd_Alp_Default_Investment;
    private DbsField pnd_Input_File_Pnd_Alp_Days_In_Default;
    private DbsField pnd_Input_File_Pnd_Alp_Pin_Num;
    private DbsField pnd_Input_File_Pnd_Alp_Deflt_Enr_Type;
    private DbsField pnd_Input_File_Pnd_Alp_Plent_Date;

    private DbsGroup pnd_Input_File__R_Field_3;
    private DbsField pnd_Input_File_Pnd_Alp_Plent_Mm;
    private DbsField pnd_Input_File_Pnd_Alp_Plent_Dd;
    private DbsField pnd_Input_File_Pnd_Alp_Plent_Ccyy;
    private DbsField pnd_Input_File_Pnd_Filler;

    private DbsGroup parm_Employee_Info;
    private DbsField parm_Employee_Info_Pnd_Pnd_Racf_Id;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_Nme;

    private DbsGroup parm_Employee_Info__R_Field_4;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_First_Nme;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_Last_Nme;
    private DbsField parm_Employee_Info_Pnd_Pnd_Empl_Table_Updte_Authrty_Cde;

    private DbsGroup parm_Unit_Info;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Cde;

    private DbsGroup parm_Unit_Info__R_Field_5;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Id_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Rgn_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Spcl_Dsgntn_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Brnch_Cde;
    private DbsField parm_Unit_Info_Pnd_Pnd_Unit_Name_Long_Txt;
    private DbsField pnd_Error_String;
    private DbsField pnd_Save_Rqst_Id;
    private DbsField pnd_Win_Title;
    private DbsField pnd_Error_Temp1;
    private DbsField pnd_Error_Temp2;
    private DbsField pnd_Error_Title;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Under_Cnt;
    private DbsField pnd_Over_Cnt;
    private DbsField pnd_30_Cnt;
    private DbsField pnd_60_Cnt;
    private DbsField pnd_90_Cnt;
    private DbsField pnd_Reject_Cnt;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Pnd_Wk_F_Name;
    private DbsField pnd_Work_Pnd_Wk_L_Name;
    private DbsField pnd_Work_Pnd_Wk_Full_Name;
    private DbsField pnd_Work_Pnd_Idx1;
    private DbsField pnd_Work_Pnd_Idx2;
    private DbsField pnd_Work_Pnd_Idx3;
    private DbsField pnd_Work_Pnd_Spc;
    private DbsField pnd_Work_Pnd_Tmp_Name;

    private DbsGroup pnd_Work__R_Field_6;
    private DbsField pnd_Work_Pnd_Tmp_Arr;
    private DbsField pnd_Work_Pnd_Wk_City;

    private DbsGroup pnd_Work__R_Field_7;
    private DbsField pnd_Work_Pnd_Wk_City_Array;
    private DbsField pnd_Work_Pnd_Wk_Zip;
    private DbsField pnd_Wk_Date;
    private DbsField pnd_Wk_Time;
    private DbsField pnd_U_Limit;
    private DbsField pnd_Days;
    private DbsField pnd_Ctrl_Date;
    private DbsField pnd_Ctrl_Date_D;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        ldaPstl6765 = new LdaPstl6765();
        registerRecord(ldaPstl6765);

        // Local Variables

        pnd_Input_File = localVariables.newGroupInRecord("pnd_Input_File", "#INPUT-FILE");
        pnd_Input_File_Pnd_Alp_Plan_Num = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Plan_Num", "#ALP-PLAN-NUM", FieldType.STRING, 6);
        pnd_Input_File_Pnd_Alp_Ssn = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Ssn", "#ALP-SSN", FieldType.STRING, 9);
        pnd_Input_File_Pnd_Alp_Part_Name = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Part_Name", "#ALP-PART-NAME", FieldType.STRING, 30);
        pnd_Input_File_Pnd_Alp_Mail_Addr_L1 = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Mail_Addr_L1", "#ALP-MAIL-ADDR-L1", FieldType.STRING, 
            35);
        pnd_Input_File_Pnd_Alp_Mail_Addr_L2 = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Mail_Addr_L2", "#ALP-MAIL-ADDR-L2", FieldType.STRING, 
            35);
        pnd_Input_File_Pnd_Alp_Mail_City = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Mail_City", "#ALP-MAIL-CITY", FieldType.STRING, 27);
        pnd_Input_File_Pnd_Alp_Mail_State = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Mail_State", "#ALP-MAIL-STATE", FieldType.STRING, 2);
        pnd_Input_File_Pnd_Alp_Mail_Zip = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Mail_Zip", "#ALP-MAIL-ZIP", FieldType.STRING, 9);

        pnd_Input_File__R_Field_1 = pnd_Input_File.newGroupInGroup("pnd_Input_File__R_Field_1", "REDEFINE", pnd_Input_File_Pnd_Alp_Mail_Zip);
        pnd_Input_File_Pnd_Alp_Mail_Zip5 = pnd_Input_File__R_Field_1.newFieldInGroup("pnd_Input_File_Pnd_Alp_Mail_Zip5", "#ALP-MAIL-ZIP5", FieldType.STRING, 
            5);
        pnd_Input_File_Pnd_Alp_Mail_Zip4 = pnd_Input_File__R_Field_1.newFieldInGroup("pnd_Input_File_Pnd_Alp_Mail_Zip4", "#ALP-MAIL-ZIP4", FieldType.STRING, 
            4);
        pnd_Input_File_Pnd_Alp_Part_Sub_Plan = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Part_Sub_Plan", "#ALP-PART-SUB-PLAN", FieldType.STRING, 
            6);
        pnd_Input_File_Pnd_Alp_Brth_Date = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Brth_Date", "#ALP-BRTH-DATE", FieldType.NUMERIC, 8);

        pnd_Input_File__R_Field_2 = pnd_Input_File.newGroupInGroup("pnd_Input_File__R_Field_2", "REDEFINE", pnd_Input_File_Pnd_Alp_Brth_Date);
        pnd_Input_File_Pnd_Alp_Brth_Mm = pnd_Input_File__R_Field_2.newFieldInGroup("pnd_Input_File_Pnd_Alp_Brth_Mm", "#ALP-BRTH-MM", FieldType.NUMERIC, 
            2);
        pnd_Input_File_Pnd_Alp_Brth_Dd = pnd_Input_File__R_Field_2.newFieldInGroup("pnd_Input_File_Pnd_Alp_Brth_Dd", "#ALP-BRTH-DD", FieldType.NUMERIC, 
            2);
        pnd_Input_File_Pnd_Alp_Brth_Ccyy = pnd_Input_File__R_Field_2.newFieldInGroup("pnd_Input_File_Pnd_Alp_Brth_Ccyy", "#ALP-BRTH-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Input_File_Pnd_Alp_Default_Investment = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Default_Investment", "#ALP-DEFAULT-INVESTMENT", 
            FieldType.STRING, 2);
        pnd_Input_File_Pnd_Alp_Days_In_Default = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Days_In_Default", "#ALP-DAYS-IN-DEFAULT", FieldType.NUMERIC, 
            4);
        pnd_Input_File_Pnd_Alp_Pin_Num = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Pin_Num", "#ALP-PIN-NUM", FieldType.STRING, 12);
        pnd_Input_File_Pnd_Alp_Deflt_Enr_Type = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Deflt_Enr_Type", "#ALP-DEFLT-ENR-TYPE", FieldType.STRING, 
            1);
        pnd_Input_File_Pnd_Alp_Plent_Date = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Alp_Plent_Date", "#ALP-PLENT-DATE", FieldType.STRING, 8);

        pnd_Input_File__R_Field_3 = pnd_Input_File.newGroupInGroup("pnd_Input_File__R_Field_3", "REDEFINE", pnd_Input_File_Pnd_Alp_Plent_Date);
        pnd_Input_File_Pnd_Alp_Plent_Mm = pnd_Input_File__R_Field_3.newFieldInGroup("pnd_Input_File_Pnd_Alp_Plent_Mm", "#ALP-PLENT-MM", FieldType.STRING, 
            2);
        pnd_Input_File_Pnd_Alp_Plent_Dd = pnd_Input_File__R_Field_3.newFieldInGroup("pnd_Input_File_Pnd_Alp_Plent_Dd", "#ALP-PLENT-DD", FieldType.STRING, 
            2);
        pnd_Input_File_Pnd_Alp_Plent_Ccyy = pnd_Input_File__R_Field_3.newFieldInGroup("pnd_Input_File_Pnd_Alp_Plent_Ccyy", "#ALP-PLENT-CCYY", FieldType.STRING, 
            4);
        pnd_Input_File_Pnd_Filler = pnd_Input_File.newFieldInGroup("pnd_Input_File_Pnd_Filler", "#FILLER", FieldType.STRING, 11);

        parm_Employee_Info = localVariables.newGroupInRecord("parm_Employee_Info", "PARM-EMPLOYEE-INFO");
        parm_Employee_Info_Pnd_Pnd_Racf_Id = parm_Employee_Info.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Racf_Id", "##RACF-ID", FieldType.STRING, 8);
        parm_Employee_Info_Pnd_Pnd_Empl_Nme = parm_Employee_Info.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_Nme", "##EMPL-NME", FieldType.STRING, 
            40);

        parm_Employee_Info__R_Field_4 = parm_Employee_Info.newGroupInGroup("parm_Employee_Info__R_Field_4", "REDEFINE", parm_Employee_Info_Pnd_Pnd_Empl_Nme);
        parm_Employee_Info_Pnd_Pnd_Empl_First_Nme = parm_Employee_Info__R_Field_4.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_First_Nme", "##EMPL-FIRST-NME", 
            FieldType.STRING, 20);
        parm_Employee_Info_Pnd_Pnd_Empl_Last_Nme = parm_Employee_Info__R_Field_4.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_Last_Nme", "##EMPL-LAST-NME", 
            FieldType.STRING, 20);
        parm_Employee_Info_Pnd_Pnd_Empl_Table_Updte_Authrty_Cde = parm_Employee_Info.newFieldInGroup("parm_Employee_Info_Pnd_Pnd_Empl_Table_Updte_Authrty_Cde", 
            "##EMPL-TABLE-UPDTE-AUTHRTY-CDE", FieldType.STRING, 2);

        parm_Unit_Info = localVariables.newGroupInRecord("parm_Unit_Info", "PARM-UNIT-INFO");
        parm_Unit_Info_Pnd_Pnd_Unit_Cde = parm_Unit_Info.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Cde", "##UNIT-CDE", FieldType.STRING, 8);

        parm_Unit_Info__R_Field_5 = parm_Unit_Info.newGroupInGroup("parm_Unit_Info__R_Field_5", "REDEFINE", parm_Unit_Info_Pnd_Pnd_Unit_Cde);
        parm_Unit_Info_Pnd_Pnd_Unit_Id_Cde = parm_Unit_Info__R_Field_5.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Id_Cde", "##UNIT-ID-CDE", FieldType.STRING, 
            5);
        parm_Unit_Info_Pnd_Pnd_Unit_Rgn_Cde = parm_Unit_Info__R_Field_5.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Rgn_Cde", "##UNIT-RGN-CDE", FieldType.STRING, 
            1);
        parm_Unit_Info_Pnd_Pnd_Unit_Spcl_Dsgntn_Cde = parm_Unit_Info__R_Field_5.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Spcl_Dsgntn_Cde", "##UNIT-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        parm_Unit_Info_Pnd_Pnd_Unit_Brnch_Cde = parm_Unit_Info__R_Field_5.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Brnch_Cde", "##UNIT-BRNCH-CDE", 
            FieldType.STRING, 1);
        parm_Unit_Info_Pnd_Pnd_Unit_Name_Long_Txt = parm_Unit_Info.newFieldInGroup("parm_Unit_Info_Pnd_Pnd_Unit_Name_Long_Txt", "##UNIT-NAME-LONG-TXT", 
            FieldType.STRING, 45);
        pnd_Error_String = localVariables.newFieldInRecord("pnd_Error_String", "#ERROR-STRING", FieldType.STRING, 20);
        pnd_Save_Rqst_Id = localVariables.newFieldInRecord("pnd_Save_Rqst_Id", "#SAVE-RQST-ID", FieldType.STRING, 11);
        pnd_Win_Title = localVariables.newFieldInRecord("pnd_Win_Title", "#WIN-TITLE", FieldType.STRING, 40);
        pnd_Error_Temp1 = localVariables.newFieldInRecord("pnd_Error_Temp1", "#ERROR-TEMP1", FieldType.STRING, 40);
        pnd_Error_Temp2 = localVariables.newFieldInRecord("pnd_Error_Temp2", "#ERROR-TEMP2", FieldType.STRING, 40);
        pnd_Error_Title = localVariables.newFieldInRecord("pnd_Error_Title", "#ERROR-TITLE", FieldType.STRING, 40);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.INTEGER, 4);
        pnd_Under_Cnt = localVariables.newFieldInRecord("pnd_Under_Cnt", "#UNDER-CNT", FieldType.INTEGER, 4);
        pnd_Over_Cnt = localVariables.newFieldInRecord("pnd_Over_Cnt", "#OVER-CNT", FieldType.INTEGER, 4);
        pnd_30_Cnt = localVariables.newFieldInRecord("pnd_30_Cnt", "#30-CNT", FieldType.INTEGER, 4);
        pnd_60_Cnt = localVariables.newFieldInRecord("pnd_60_Cnt", "#60-CNT", FieldType.INTEGER, 4);
        pnd_90_Cnt = localVariables.newFieldInRecord("pnd_90_Cnt", "#90-CNT", FieldType.INTEGER, 4);
        pnd_Reject_Cnt = localVariables.newFieldInRecord("pnd_Reject_Cnt", "#REJECT-CNT", FieldType.INTEGER, 4);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Pnd_Wk_F_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_F_Name", "#WK-F-NAME", FieldType.STRING, 30);
        pnd_Work_Pnd_Wk_L_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_L_Name", "#WK-L-NAME", FieldType.STRING, 30);
        pnd_Work_Pnd_Wk_Full_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Full_Name", "#WK-FULL-NAME", FieldType.STRING, 30);
        pnd_Work_Pnd_Idx1 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Idx1", "#IDX1", FieldType.INTEGER, 4);
        pnd_Work_Pnd_Idx2 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Idx2", "#IDX2", FieldType.INTEGER, 4);
        pnd_Work_Pnd_Idx3 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Idx3", "#IDX3", FieldType.INTEGER, 2);
        pnd_Work_Pnd_Spc = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Spc", "#SPC", FieldType.BOOLEAN, 1);
        pnd_Work_Pnd_Tmp_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Tmp_Name", "#TMP-NAME", FieldType.STRING, 30);

        pnd_Work__R_Field_6 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_6", "REDEFINE", pnd_Work_Pnd_Tmp_Name);
        pnd_Work_Pnd_Tmp_Arr = pnd_Work__R_Field_6.newFieldArrayInGroup("pnd_Work_Pnd_Tmp_Arr", "#TMP-ARR", FieldType.STRING, 1, new DbsArrayController(1, 
            30));
        pnd_Work_Pnd_Wk_City = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City", "#WK-CITY", FieldType.STRING, 28);

        pnd_Work__R_Field_7 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_7", "REDEFINE", pnd_Work_Pnd_Wk_City);
        pnd_Work_Pnd_Wk_City_Array = pnd_Work__R_Field_7.newFieldArrayInGroup("pnd_Work_Pnd_Wk_City_Array", "#WK-CITY-ARRAY", FieldType.STRING, 1, new 
            DbsArrayController(1, 28));
        pnd_Work_Pnd_Wk_Zip = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip", "#WK-ZIP", FieldType.STRING, 10);
        pnd_Wk_Date = localVariables.newFieldInRecord("pnd_Wk_Date", "#WK-DATE", FieldType.STRING, 8);
        pnd_Wk_Time = localVariables.newFieldInRecord("pnd_Wk_Time", "#WK-TIME", FieldType.STRING, 8);
        pnd_U_Limit = localVariables.newFieldInRecord("pnd_U_Limit", "#U-LIMIT", FieldType.NUMERIC, 4);
        pnd_Days = localVariables.newFieldInRecord("pnd_Days", "#DAYS", FieldType.NUMERIC, 4);
        pnd_Ctrl_Date = localVariables.newFieldInRecord("pnd_Ctrl_Date", "#CTRL-DATE", FieldType.STRING, 8);
        pnd_Ctrl_Date_D = localVariables.newFieldInRecord("pnd_Ctrl_Date_D", "#CTRL-DATE-D", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaPstl6765.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb121() throws Exception
    {
        super("Appb121");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 80 PS = 60 ZP = ON;//Natural: FORMAT ( 1 ) LS = 80 PS = 60 ZP = ON;//Natural: FORMAT ( 2 ) LS = 80 PS = 60 ZP = ON
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*  SET DATE AND TIME FOR POWERIMAGE
        //*  --------------------------------
        pnd_Wk_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                      //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #WK-DATE
        pnd_Wk_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:MM:SS"));                                                                                      //Natural: MOVE EDITED *TIMX ( EM = HH:MM:SS ) TO #WK-TIME
        //*  DETERMINE UPPER LIMIT OF LETTERS TO PRINT
        //*  -----------------------------------------
        //*  LAST RUN DATE
        getWorkFiles().read(1, pnd_Ctrl_Date);                                                                                                                            //Natural: READ WORK FILE 1 ONCE #CTRL-DATE
        pnd_Ctrl_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ctrl_Date);                                                                                     //Natural: MOVE EDITED #CTRL-DATE TO #CTRL-DATE-D ( EM = YYYYMMDD )
        pnd_Days.compute(new ComputeParameters(false, pnd_Days), (Global.getDATX().subtract(pnd_Ctrl_Date_D)));                                                           //Natural: COMPUTE #DAYS = ( *DATX - #CTRL-DATE-D )
        getReports().write(1, "LAST RUN:",pnd_Ctrl_Date,"TODAY:",pnd_Wk_Date);                                                                                            //Natural: WRITE ( 1 ) 'LAST RUN:' #CTRL-DATE 'TODAY:' #WK-DATE
        if (Global.isEscape()) return;
        getReports().write(1, "NUMBER OF DAYS BETWEEN RUNS:",pnd_Days);                                                                                                   //Natural: WRITE ( 1 ) 'NUMBER OF DAYS BETWEEN RUNS:' #DAYS
        if (Global.isEscape()) return;
        pnd_U_Limit.compute(new ComputeParameters(false, pnd_U_Limit), pnd_Days.add(91));                                                                                 //Natural: COMPUTE #U-LIMIT = #DAYS + 91
        getReports().write(1, "LETTERS THAT ARE",pnd_U_Limit,"DAYS OR OLDER WILL NOT BE PRINTED.");                                                                       //Natural: WRITE ( 1 ) 'LETTERS THAT ARE' #U-LIMIT 'DAYS OR OLDER WILL NOT BE PRINTED.'
        if (Global.isEscape()) return;
        R1:                                                                                                                                                               //Natural: READ WORK FILE 2 #INPUT-FILE
        while (condition(getWorkFiles().read(2, pnd_Input_File)))
        {
            pdaPsta9500.getPsta9500().reset();                                                                                                                            //Natural: RESET PSTA9500 #WORK
            pnd_Work.reset();
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            //*  DETERMINE PLANS THAT CAN BE PROCESSED
            //*  -------------------------------------
            short decideConditionsMet739 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #ALP-PLAN-NUM;//Natural: VALUE '100100','100101','100102','100103','100104','100105'
            if (condition((pnd_Input_File_Pnd_Alp_Plan_Num.equals("100100") || pnd_Input_File_Pnd_Alp_Plan_Num.equals("100101") || pnd_Input_File_Pnd_Alp_Plan_Num.equals("100102") 
                || pnd_Input_File_Pnd_Alp_Plan_Num.equals("100103") || pnd_Input_File_Pnd_Alp_Plan_Num.equals("100104") || pnd_Input_File_Pnd_Alp_Plan_Num.equals("100105"))))
            {
                decideConditionsMet739++;
                ignore();
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(2, "PLAN NOT ALLOWED FOR PROCESSING FOR PARTICIPANT:",pnd_Input_File_Pnd_Alp_Ssn,"PLAN:",pnd_Input_File_Pnd_Alp_Plan_Num);             //Natural: WRITE ( 2 ) 'PLAN NOT ALLOWED FOR PROCESSING FOR PARTICIPANT:' #ALP-SSN 'PLAN:' #ALP-PLAN-NUM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Reject_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REJECT-CNT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  DETERMINE WHICH LETTER TO SEND
            //*  ------------------------------
            //*  DISCARD LESS THAN 31 DAYS
            short decideConditionsMet751 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ALP-DAYS-IN-DEFAULT < 31
            if (condition(pnd_Input_File_Pnd_Alp_Days_In_Default.less(31)))
            {
                decideConditionsMet751++;
                pnd_Under_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #UNDER-CNT
                getReports().write(2, "UNDER 30 DAYS-BYPASSING FOR PARTICIPANT:",pnd_Input_File_Pnd_Alp_Ssn,"- NUMBER OF DAYS",pnd_Input_File_Pnd_Alp_Days_In_Default);   //Natural: WRITE ( 2 ) 'UNDER 30 DAYS-BYPASSING FOR PARTICIPANT:' #ALP-SSN '- NUMBER OF DAYS' #ALP-DAYS-IN-DEFAULT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  DISCARD-PROCESSED EARLIER
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN #ALP-DAYS-IN-DEFAULT >= #U-LIMIT
            else if (condition(pnd_Input_File_Pnd_Alp_Days_In_Default.greaterOrEqual(pnd_U_Limit)))
            {
                decideConditionsMet751++;
                pnd_Over_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #OVER-CNT
                getReports().write(2, "OVER DAYS LIMIT-BYPASSING FOR PARTICIPANT:",pnd_Input_File_Pnd_Alp_Ssn,"- NUMBER OF DAYS",pnd_Input_File_Pnd_Alp_Days_In_Default); //Natural: WRITE ( 2 ) 'OVER DAYS LIMIT-BYPASSING FOR PARTICIPANT:' #ALP-SSN '- NUMBER OF DAYS' #ALP-DAYS-IN-DEFAULT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ASSIGN 90 DAY LETTER
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN #ALP-DAYS-IN-DEFAULT > 90
            else if (condition(pnd_Input_File_Pnd_Alp_Days_In_Default.greater(90)))
            {
                decideConditionsMet751++;
                //*  (91 OR MORE DAYS)
                pnd_90_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #90-CNT
                //*  ASSIGN 60 DAY LETTER
                ldaPstl6765.getPstl6765_Data_Letter_Type().setValue("PULET90");                                                                                           //Natural: MOVE 'PULET90' TO LETTER-TYPE
                getReports().write(3, "90 day letter for participant:",pnd_Input_File_Pnd_Alp_Ssn,"- NUMBER OF DAYS",pnd_Input_File_Pnd_Alp_Days_In_Default);             //Natural: WRITE ( 3 ) '90 day letter for participant:' #ALP-SSN '- NUMBER OF DAYS' #ALP-DAYS-IN-DEFAULT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #ALP-DAYS-IN-DEFAULT > 60
            else if (condition(pnd_Input_File_Pnd_Alp_Days_In_Default.greater(60)))
            {
                decideConditionsMet751++;
                //*  (61 TO 90 DAYS)
                pnd_60_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #60-CNT
                //*  ASSIGN 30 DAY LETTER
                ldaPstl6765.getPstl6765_Data_Letter_Type().setValue("PULET60");                                                                                           //Natural: MOVE 'PULET60' TO LETTER-TYPE
                getReports().write(3, "60 day letter for participant:",pnd_Input_File_Pnd_Alp_Ssn,"- NUMBER OF DAYS",pnd_Input_File_Pnd_Alp_Days_In_Default);             //Natural: WRITE ( 3 ) '60 day letter for participant:' #ALP-SSN '- NUMBER OF DAYS' #ALP-DAYS-IN-DEFAULT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #ALP-DAYS-IN-DEFAULT > 30
            else if (condition(pnd_Input_File_Pnd_Alp_Days_In_Default.greater(30)))
            {
                decideConditionsMet751++;
                //*  (31 TO 60 DAYS)
                pnd_30_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #30-CNT
                ldaPstl6765.getPstl6765_Data_Letter_Type().setValue("PULET30");                                                                                           //Natural: MOVE 'PULET30' TO LETTER-TYPE
                getReports().write(3, "30 day letter for participant:",pnd_Input_File_Pnd_Alp_Ssn,"- NUMBER OF DAYS",pnd_Input_File_Pnd_Alp_Days_In_Default);             //Natural: WRITE ( 3 ) '30 day letter for participant:' #ALP-SSN '- NUMBER OF DAYS' #ALP-DAYS-IN-DEFAULT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(0, "COULD NOT DETERMINE WHICH KIND OF LETTER TO PRINT-",NEWLINE,"TERMINATING!!","=",pnd_Input_File_Pnd_Alp_Days_In_Default);           //Natural: WRITE 'COULD NOT DETERMINE WHICH KIND OF LETTER TO PRINT-' / 'TERMINATING!!' '=' #ALP-DAYS-IN-DEFAULT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(16);  if (true) return;                                                                                                                 //Natural: TERMINATE 16
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  PARSE THE PARTICIPANT NAME
            //*  --------------------------
            pnd_Input_File_Pnd_Alp_Part_Name.separate(SeparateOption.WithAnyDelimiters, ",", pnd_Work_Pnd_Wk_L_Name, pnd_Work_Pnd_Wk_F_Name);                             //Natural: SEPARATE #ALP-PART-NAME INTO #WK-L-NAME #WK-F-NAME WITH DELIMITER ','
            //*  FORMAT FIRST NAME
            //*  -----------------
            pnd_Work_Pnd_Tmp_Name.setValue(pnd_Work_Pnd_Wk_L_Name);                                                                                                       //Natural: MOVE #WK-L-NAME TO #TMP-NAME
                                                                                                                                                                          //Natural: PERFORM FORMAT-NAME
            sub_Format_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Work_Pnd_Wk_L_Name.setValue(pnd_Work_Pnd_Tmp_Name);                                                                                                       //Natural: MOVE #TMP-NAME TO #WK-L-NAME
            //*  FORMAT LAST NAME
            //*  ----------------
            pnd_Work_Pnd_Tmp_Name.setValue(pnd_Work_Pnd_Wk_F_Name);                                                                                                       //Natural: MOVE #WK-F-NAME TO #TMP-NAME
                                                                                                                                                                          //Natural: PERFORM FORMAT-NAME
            sub_Format_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Work_Pnd_Wk_F_Name.setValue(pnd_Work_Pnd_Tmp_Name);                                                                                                       //Natural: MOVE #TMP-NAME TO #WK-F-NAME
            //*  BUILD FULL NAME
            //*  ---------------
            pnd_Work_Pnd_Wk_Full_Name.setValue(DbsUtil.compress(pnd_Work_Pnd_Wk_F_Name, pnd_Work_Pnd_Wk_L_Name));                                                         //Natural: COMPRESS #WK-F-NAME #WK-L-NAME INTO #WK-FULL-NAME
            //*  BUILD CITY, ST  ZIP9
            //*  --------------------
            //*  PLACE COMMA
            pnd_Work_Pnd_Wk_City.setValue(pnd_Input_File_Pnd_Alp_Mail_City);                                                                                              //Natural: MOVE #ALP-MAIL-CITY TO #WK-CITY
            FOR01:                                                                                                                                                        //Natural: FOR #IDX3 28 TO 1 STEP -1
            for (pnd_Work_Pnd_Idx3.setValue(28); condition(pnd_Work_Pnd_Idx3.greaterOrEqual(1)); pnd_Work_Pnd_Idx3.nsubtract(1))
            {
                //*  AFTER CITY
                if (condition(pnd_Work_Pnd_Wk_City_Array.getValue(pnd_Work_Pnd_Idx3).notEquals(" ")))                                                                     //Natural: IF #WK-CITY-ARRAY ( #IDX3 ) NE ' '
                {
                    pnd_Work_Pnd_Idx3.nadd(1);                                                                                                                            //Natural: ADD 1 TO #IDX3
                    pnd_Work_Pnd_Wk_City_Array.getValue(pnd_Work_Pnd_Idx3).setValue(",");                                                                                 //Natural: MOVE ',' TO #WK-CITY-ARRAY ( #IDX3 )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FORMAT ZIP9
            if (condition(pnd_Input_File_Pnd_Alp_Mail_Zip4.equals(" ")))                                                                                                  //Natural: IF #ALP-MAIL-ZIP4 = ' '
            {
                pnd_Input_File_Pnd_Alp_Mail_Zip4.setValue("0000");                                                                                                        //Natural: MOVE '0000' TO #ALP-MAIL-ZIP4
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Pnd_Wk_Zip.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_File_Pnd_Alp_Mail_Zip5, "-", pnd_Input_File_Pnd_Alp_Mail_Zip4));       //Natural: COMPRESS #ALP-MAIL-ZIP5 '-' #ALP-MAIL-ZIP4 INTO #WK-ZIP LEAVING NO SPACE
            //*  CITY, ST  ZIP9
            pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(DbsUtil.compress(pnd_Work_Pnd_Wk_City, pnd_Input_File_Pnd_Alp_Mail_State, pnd_Work_Pnd_Wk_Zip));             //Natural: COMPRESS #WK-CITY #ALP-MAIL-STATE #WK-ZIP INTO ADDRSS-LINE-2
            pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO ADDRSS-LINE-3
            pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO ADDRSS-LINE-4
            pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO ADDRSS-LINE-5
            pdaPsta9612.getPsta9612_Addrss_Line_6().setValue(" ");                                                                                                        //Natural: MOVE ' ' TO ADDRSS-LINE-6
            //*  CREATE PARTICIPANT INFO FOR POST OPEN
            //*  -------------------------------------
            pdaPsta9612.getPsta9612_Ssn_Nbr().setValueEdited(new ReportEditMask("999999999"),pnd_Input_File_Pnd_Alp_Ssn);                                                 //Natural: MOVE EDITED #ALP-SSN TO PSTA9612.SSN-NBR ( EM = 999999999 )
            pdaPsta9612.getPsta9612_Univ_Id().setValue(pnd_Input_File_Pnd_Alp_Pin_Num);                                                                                   //Natural: ASSIGN PSTA9612.UNIV-ID := #ALP-PIN-NUM
            pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                          //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
            pdaPsta9612.getPsta9612_Full_Nme().setValue(pnd_Work_Pnd_Wk_Full_Name);                                                                                       //Natural: ASSIGN FULL-NME := #WK-FULL-NAME
            pdaPsta9612.getPsta9612_Last_Nme().setValue(pnd_Work_Pnd_Wk_L_Name);                                                                                          //Natural: ASSIGN LAST-NME := #WK-L-NAME
            pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                          //Natural: ASSIGN UNIV-ID-TYP := 'P'
            ldaPstl6765.getPstl6765_Data_Pstl6765_Rec_Id().setValue("AA");                                                                                                //Natural: ASSIGN PSTL6765-REC-ID := 'AA'
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("U");                                                                                                       //Natural: ASSIGN ADDRSS-TYP-CDE := 'U'
            pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(pnd_Input_File_Pnd_Alp_Mail_Addr_L1);                                                                        //Natural: ASSIGN ADDRSS-LINE-1 := #ALP-MAIL-ADDR-L1
            pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(pnd_Input_File_Pnd_Alp_Mail_Zip);                                                                           //Natural: ASSIGN ADDRSS-ZP9-NBR := #ALP-MAIL-ZIP
            pdaPsta9612.getPsta9612_Dont_Use_Finalist().setValue(true);                                                                                                   //Natural: ASSIGN DONT-USE-FINALIST := TRUE
            pdaPsta9612.getPsta9612_Addrss_Lines_Rule().setValue(false);                                                                                                  //Natural: ASSIGN ADDRSS-LINES-RULE := FALSE
            pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                 //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := TRUE
            pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                 //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := TRUE
            //*  CREATE LETTER ON POST TABLE
                                                                                                                                                                          //Natural: PERFORM POST-OPEN
            sub_Post_Open();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM STORE-RECORD
            sub_Store_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CREATE POWERIMAGE RECORD
                                                                                                                                                                          //Natural: PERFORM POWER-IMAGE-DATA
            sub_Power_Image_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM STORE-RECORD
            sub_Store_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM POST-CLOSE
            sub_Post_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OPEN
            //*  **********************************************************************
            //*  SET UP VARS IN PSTA9612 FOR POST OPEN
            //*  -------------------------------------
            //*  **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-RECORD
            //*  **********************************************************************
            //*  **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POWER-IMAGE-DATA
            //*  **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-CLOSE
            //*  **********************************************************************
            //*  CALL POST CLOSE TO FINALISE THE REQUEST
            //*  ---------------------------------------
            //*  **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-NAME
            //*  **********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR
        }                                                                                                                                                                 //Natural: END-WORK
        R1_Exit:
        if (Global.isEscape()) return;
        //*  WRITE RUN DATE TO FILE - CONTROL RECORD
        //*  ----------------------
        getWorkFiles().write(3, false, pnd_Wk_Date);                                                                                                                      //Natural: WRITE WORK FILE 3 #WK-DATE
        getReports().write(1, "NUMBER OF RECORDS READ:                               ",pnd_Read_Cnt,NEWLINE,"NUMBER OF RECORDS bypassed(less than 31 days):        ",     //Natural: WRITE ( 1 ) 'NUMBER OF RECORDS READ:                               ' #READ-CNT / 'NUMBER OF RECORDS bypassed(less than 31 days):        ' #UNDER-CNT / 'NUMBER OF RECORDS bypassed(over days limit:)          ' #OVER-CNT / 'NUMBER OF RECORDS BYPASSED(PLAN NOT 100100 - 100105): ' #REJECT-CNT / '**' / 'NUMBER OF 30 DAY LETTERS - (31 TO 60 DAYS)            ' #30-CNT / 'NUMBER OF 60 DAY LETTERS - (61 TO 90 DAYS)            ' #60-CNT / 'NUMBER OF 90 DAY LETTERS - (91 OR MORE DAYS)          ' #90-CNT /
            pnd_Under_Cnt,NEWLINE,"NUMBER OF RECORDS bypassed(over days limit:)          ",pnd_Over_Cnt,NEWLINE,"NUMBER OF RECORDS BYPASSED(PLAN NOT 100100 - 100105): ",
            pnd_Reject_Cnt,NEWLINE,"**",NEWLINE,"NUMBER OF 30 DAY LETTERS - (31 TO 60 DAYS)            ",pnd_30_Cnt,NEWLINE,"NUMBER OF 60 DAY LETTERS - (61 TO 90 DAYS)            ",
            pnd_60_Cnt,NEWLINE,"NUMBER OF 90 DAY LETTERS - (91 OR MORE DAYS)          ",pnd_90_Cnt,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Post_Open() throws Exception                                                                                                                         //Natural: POST-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("BATCHACIS");                                                                                                     //Natural: ASSIGN SYSTM-ID-CDE := 'BATCHACIS'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTRKADEC");                                                                                                         //Natural: ASSIGN PCKGE-CDE := 'PTRKADEC'
        pnd_Error_String.setValue("Post OPEN");                                                                                                                           //Natural: ASSIGN #ERROR-STRING := 'Post OPEN'
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO PARM-UNIT-INFO
            parm_Employee_Info, parm_Unit_Info);
        if (condition(Global.isEscape())) return;
        //*  CHECK FOR ERRORS IN POST OPEN
        //*  -----------------------------
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Save_Rqst_Id.setValue(pdaPsta9610.getPsta9610_Pst_Rqst_Id());                                                                                                 //Natural: ASSIGN #SAVE-RQST-ID := PSTA9610.PST-RQST-ID
        //*  (POST-OPEN)
    }
    private void sub_Store_Record() throws Exception                                                                                                                      //Natural: STORE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL6765-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6765.getSort_Key(), ldaPstl6765.getPstl6765_Data());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  (STORE-RECORD)
    }
    private void sub_Power_Image_Data() throws Exception                                                                                                                  //Natural: POWER-IMAGE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  **********************************************************************
        ldaPstl6765.getPstl6765_Data_Power_Image_Task_Information().reset();                                                                                              //Natural: RESET POWER-IMAGE-TASK-INFORMATION
        ldaPstl6765.getPstl6765_Data_Pstl6765_Rec_Id().setValue("PI");                                                                                                    //Natural: ASSIGN PSTL6765-REC-ID := 'PI'
        ldaPstl6765.getPstl6765_Data_Pi_Export_Ind().setValue("T");                                                                                                       //Natural: ASSIGN PI-EXPORT-IND := 'T'
        ldaPstl6765.getPstl6765_Data_Pi_Task_Type().setValue("ENRSYSODLT");                                                                                               //Natural: ASSIGN PI-TASK-TYPE := 'ENRSYSODLT'
        ldaPstl6765.getPstl6765_Data_Pi_Action_Step().setValue("NONE");                                                                                                   //Natural: ASSIGN PI-ACTION-STEP := 'NONE'
        ldaPstl6765.getPstl6765_Data_Pi_Tiaa_Full_Date().setValue(pnd_Wk_Date);                                                                                           //Natural: ASSIGN PI-TIAA-FULL-DATE := #WK-DATE
        ldaPstl6765.getPstl6765_Data_Pi_Tiaa_Time().setValue(pnd_Wk_Time);                                                                                                //Natural: ASSIGN PI-TIAA-TIME := #WK-TIME
        ldaPstl6765.getPstl6765_Data_Pi_Task_Status().setValue("C");                                                                                                      //Natural: ASSIGN PI-TASK-STATUS := 'C'
        ldaPstl6765.getPstl6765_Data_Pi_Ssn().setValue(pnd_Input_File_Pnd_Alp_Ssn);                                                                                       //Natural: ASSIGN PI-SSN := #ALP-SSN
        ldaPstl6765.getPstl6765_Data_Pi_Pin_Npin_Ppg().setValue(pnd_Input_File_Pnd_Alp_Pin_Num);                                                                          //Natural: ASSIGN PI-PIN-NPIN-PPG := #ALP-PIN-NUM
        ldaPstl6765.getPstl6765_Data_Pi_Pin_Type().setValue("P");                                                                                                         //Natural: ASSIGN PI-PIN-TYPE := 'P'
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(1).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 1 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(2).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 2 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(3).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 3 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(4).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 4 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(5).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 5 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(6).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 6 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(7).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 7 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(8).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 8 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(9).setValue(" ");                                                                                           //Natural: ASSIGN PI-CONTRACT-A ( 9 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Contract_A().getValue(10).setValue(" ");                                                                                          //Natural: ASSIGN PI-CONTRACT-A ( 10 ) := ' '
        ldaPstl6765.getPstl6765_Data_Pi_Plan_Id().setValue(pnd_Input_File_Pnd_Alp_Plan_Num);                                                                              //Natural: ASSIGN PI-PLAN-ID := #ALP-PLAN-NUM
        ldaPstl6765.getPstl6765_Data_Pi_Doc_Content().setValue("POST LETTER");                                                                                            //Natural: ASSIGN PI-DOC-CONTENT := 'POST LETTER'
        //*  (POWER-IMAGE-DATA)
    }
    private void sub_Post_Close() throws Exception                                                                                                                        //Natural: POST-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Error_String.setValue("POST CLOSE");                                                                                                                          //Natural: ASSIGN #ERROR-STRING := 'POST CLOSE'
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO PARM-UNIT-INFO
            parm_Employee_Info, parm_Unit_Info);
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE <> ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Error_String.setValue("Request ADDED!");                                                                                                                      //Natural: ASSIGN #ERROR-STRING := 'Request ADDED!'
        getReports().write(0, "MAIL HEADER AND DATA:",pnd_Save_Rqst_Id,"ADDED SUCCESSFULLY",NEWLINE,"   RQST LOG-DATE TME:",pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1)); //Natural: WRITE 'MAIL HEADER AND DATA:' #SAVE-RQST-ID 'ADDED SUCCESSFULLY' / '   RQST LOG-DATE TME:' PSTA9612.RQST-LOG-DTE-TME ( 1 )
        if (Global.isEscape()) return;
        //*  (POST-CLOSE)
    }
    private void sub_Format_Name() throws Exception                                                                                                                       //Natural: FORMAT-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //*  **********************************************************************
        DbsUtil.examine(new ExamineSource(pnd_Work_Pnd_Tmp_Name), new ExamineTranslate(TranslateOption.Lower));                                                           //Natural: EXAMINE #TMP-NAME TRANSLATE INTO LOWER CASE
        FOR02:                                                                                                                                                            //Natural: FOR #IDX1 = 1 TO 30
        for (pnd_Work_Pnd_Idx1.setValue(1); condition(pnd_Work_Pnd_Idx1.lessOrEqual(30)); pnd_Work_Pnd_Idx1.nadd(1))
        {
            //*  FIRST CHARACTER
            if (condition(pnd_Work_Pnd_Idx1.equals(1)))                                                                                                                   //Natural: IF #IDX1 = 1
            {
                //*  TO UPPERCASE
                DbsUtil.examine(new ExamineSource(pnd_Work_Pnd_Tmp_Arr.getValue(1)), new ExamineTranslate(TranslateOption.Upper));                                        //Natural: EXAMINE #TMP-ARR ( 1 ) TRANSLATE INTO UPPER CASE
            }                                                                                                                                                             //Natural: END-IF
            //*  FIRST CHARACTER
            //*  AFTER HYPHEN
            if (condition(pnd_Work_Pnd_Tmp_Arr.getValue(pnd_Work_Pnd_Idx1).equals("-")))                                                                                  //Natural: IF #TMP-ARR ( #IDX1 ) = '-'
            {
                pnd_Work_Pnd_Idx2.compute(new ComputeParameters(false, pnd_Work_Pnd_Idx2), pnd_Work_Pnd_Idx1.add(1));                                                     //Natural: ASSIGN #IDX2 := #IDX1 + 1
                //*  INTO UPPERCASE
                DbsUtil.examine(new ExamineSource(pnd_Work_Pnd_Tmp_Arr.getValue(pnd_Work_Pnd_Idx2)), new ExamineTranslate(TranslateOption.Upper));                        //Natural: EXAMINE #TMP-ARR ( #IDX2 ) TRANSLATE INTO UPPER CASE
                pnd_Work_Pnd_Idx2.reset();                                                                                                                                //Natural: RESET #IDX2
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Pnd_Tmp_Arr.getValue(pnd_Work_Pnd_Idx1).equals(" ")))                                                                                  //Natural: IF #TMP-ARR ( #IDX1 ) = ' '
            {
                pnd_Work_Pnd_Spc.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #SPC
            }                                                                                                                                                             //Natural: END-IF
            //*  FIRST CHARACTER
            if (condition(pnd_Work_Pnd_Tmp_Arr.getValue(pnd_Work_Pnd_Idx1).notEquals(" ") && pnd_Work_Pnd_Spc.getBoolean()))                                              //Natural: IF #TMP-ARR ( #IDX1 ) NE ' ' AND #SPC
            {
                //*  AFTER ' ' INTO
                //*  UPPERCASE
                DbsUtil.examine(new ExamineSource(pnd_Work_Pnd_Tmp_Arr.getValue(pnd_Work_Pnd_Idx1)), new ExamineTranslate(TranslateOption.Upper));                        //Natural: EXAMINE #TMP-ARR ( #IDX1 ) TRANSLATE INTO UPPER CASE
                pnd_Work_Pnd_Spc.reset();                                                                                                                                 //Natural: RESET #SPC
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  (FORMAT-NAME)
    }
    private void sub_Error() throws Exception                                                                                                                             //Natural: ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  **********************************************************************
        //*  FORMAT ##MSG
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        pnd_Error_String.setValue(pnd_Win_Title);                                                                                                                         //Natural: ASSIGN #ERROR-STRING := #WIN-TITLE
        pnd_Error_Temp1.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                              //Natural: ASSIGN #ERROR-TEMP1 := MSG-INFO-SUB.##MSG
        pnd_Error_Temp2.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().getSubstring(41,39));                                                                          //Natural: ASSIGN #ERROR-TEMP2 := SUBSTRING ( MSG-INFO-SUB.##MSG,41,39 )
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pnd_Error_Title.setValue(DbsUtil.compress("Error from:", pnd_Error_String));                                                                                      //Natural: COMPRESS 'Error from:' #ERROR-STRING INTO #ERROR-TITLE
        getReports().write(0, " Error Title:",pnd_Error_Title,NEWLINE," Error Field:",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(),NEWLINE,"         MSG:",          //Natural: WRITE ' Error Title:' #ERROR-TITLE / ' Error Field:' MSG-INFO-SUB.##ERROR-FIELD / '         MSG:' #ERROR-TEMP1 / '             ' #ERROR-TEMP2
            pnd_Error_Temp1,NEWLINE,"             ",pnd_Error_Temp2);
        if (Global.isEscape()) return;
        //*  (ERROR)
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(63),Global.getDATU(),NEWLINE,new ColumnSpacing(34),"PROCESSING STATISTICS",NEWLINE,new    //Natural: WRITE ( 1 ) *PROGRAM 63X *DATU / 34X'PROCESSING STATISTICS'/ 27X'Purdue Followup Letter Statistics'
                        ColumnSpacing(27),"Purdue Followup Letter Statistics");
                    getReports().write(1, NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE ( 1 ) //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, Global.getPROGRAM(),new ColumnSpacing(63),Global.getDATU(),NEWLINE,new ColumnSpacing(35),"EXCEPTION REPORT",NEWLINE,new         //Natural: WRITE ( 2 ) *PROGRAM 63X *DATU / 35X'EXCEPTION REPORT'/ 27X'Purdue Followup Letters Bypassed'
                        ColumnSpacing(27),"Purdue Followup Letters Bypassed");
                    getReports().write(2, NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE ( 2 ) //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, Global.getPROGRAM(),new ColumnSpacing(63),Global.getDATU(),NEWLINE,new ColumnSpacing(34),"PROCESSING REPORT",NEWLINE,new        //Natural: WRITE ( 3 ) *PROGRAM 63X *DATU / 34X'PROCESSING REPORT'/ 26X'Purdue Followup Letters Processed'
                        ColumnSpacing(26),"Purdue Followup Letters Processed");
                    getReports().write(3, NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE ( 3 ) //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=60 ZP=ON");
        Global.format(1, "LS=80 PS=60 ZP=ON");
        Global.format(2, "LS=80 PS=60 ZP=ON");
    }
}
