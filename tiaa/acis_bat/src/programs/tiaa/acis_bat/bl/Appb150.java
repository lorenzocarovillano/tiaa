/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:11 PM
**        * FROM NATURAL PROGRAM : Appb150
************************************************************
**        * FILE NAME            : Appb150.java
**        * CLASS NAME           : Appb150
**        * INSTANCE NAME        : Appb150
************************************************************
************************************************************************
* PROGRAM  : APPB150 - DEFAULT ENROLLMENT PROCESSING                   *
* FUNCTION : READS ACIS PRAP FILE AND EXTRACTS DEFAULT ENROLLMENT      *
*            CONTRACTS RECORDS.  UPDATES STATUS ON PRAP BASED ON       *
*            WHETHER THE CONTRACT IS STILL A DEFAULT ENROLLMENT IN     *
*            COR.  PROCESSING SUPPRESSES PRINTING OF A LEGAL PACKAGE   *
*            UNTIL AN APPLICATION IS RECEIVED.                         *
* UPDATED  : 08/23/04 K.GATES   - PROGRAM CREATED                      *
*   (RW1)  : 05/03/05 R.WILLIS  - REDEFINE AP-BANK-PYMNT-ACCT-NMBR     *
*   (KG1)  : 03/19/07 K.GATES   - CORRECTED PAGE CNT FOR EXCEPTION RPT *
*          : 07/18/13 L.SHU     - UPDATE DATE OF ISSUE WITH AP-TIAA-DOI*
* (TNGSUB)                        AND AP-CREF-DOI FOR IRA SUBSTITUTION *
*   (CNR)  : 09/01/15 L.SHU     - COR NAAD RETIREMENT                  *
*                               - READ MDM WORKFILE FOR DEFAULT ENROL- *
*                                 MENT IND AND UPDATE PRAP FILE.       *
*                               - READ PRAP WITH AP-RELEASE-IND OF 4   *
*                                 AND UPDATE IT TO 7 IF CONTRACT IS    *
*                                 STILL DEFAULT ENROLLED (OMNI-ISSUE-  *
*                                 IND = '2').                          *
*                               - THE RQST-LOG-DTE-TME OF THE PRAP REC *
*                                 IS UPDATED TO REFLECT THE DATE OF    *
*                                 WHEN THE AP-RELEASE-IND IS UPDATED TO*
*                                 '7'.                                 *
* CHG362343: 09/15/15 B.NEWSOM  - FIX A 3041 ERROR:                    *
*              MAXIMUM VALUE FOR ADABAS PARAMETER NISNHQ WAS EXCEEDED. *
* 06/13/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb150 extends BLNatBase
{
    // Data Areas
    private PdaAciadate pdaAciadate;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_prap_View;
    private DbsField prap_View_Ap_Coll_Code;
    private DbsField prap_View_Ap_Soc_Sec;
    private DbsField prap_View_Ap_Lob;
    private DbsField prap_View_Ap_T_Doi;
    private DbsField prap_View_Ap_C_Doi;
    private DbsField prap_View_Ap_Release_Ind;
    private DbsField prap_View_Ap_Record_Type;
    private DbsField prap_View_Ap_Lob_Type;
    private DbsField prap_View_Ap_Cor_Last_Nme;
    private DbsField prap_View_Ap_Cor_First_Nme;
    private DbsField prap_View_Ap_Cor_Mddle_Nme;
    private DbsField prap_View_Ap_Pin_Nbr;
    private DbsField prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup prap_View__R_Field_1;
    private DbsField prap_View_Bank_Home_Ac;
    private DbsField prap_View_Bank_Home_A3;
    private DbsField prap_View_Bank_Home_A4;
    private DbsField prap_View_Bank_Filler_4;
    private DbsField prap_View_Oia_Indicator;
    private DbsField prap_View_Erisa_Ind;
    private DbsField prap_View_Cai_Ind;
    private DbsField prap_View_Cip_Ind;
    private DbsField prap_View_Same_Addr;
    private DbsField prap_View_Omni_Issue_Ind;
    private DbsField prap_View_Ap_Tiaa_Cntrct;
    private DbsField prap_View_Ap_Cref_Cert;
    private DbsField prap_View_Ap_Sgrd_Plan_No;
    private DbsField prap_View_Ap_Sgrd_Subplan_No;
    private DbsField prap_View_Ap_Sgrd_Part_Ext;

    private DbsGroup prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField prap_View_Ap_Tiaa_Doi;
    private DbsField prap_View_Ap_Cref_Doi;
    private DbsField prap_View_Ap_Substitution_Contract_Ind;
    private DbsField prap_View_Ap_Conv_Issue_State;
    private DbsField prap_View_Ap_Deceased_Ind;
    private DbsField pnd_Ap_Name;
    private DbsField pnd_Cor_Full_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_2;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_3;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_4;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_5;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_6;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20;
    private DbsField pnd_Hold_Nme_Prfx;

    private DbsGroup pnd_Mdm_Input_Rec;
    private DbsField pnd_Mdm_Input_Rec_Pnd_Tiaa_Cntrct_No;
    private DbsField pnd_Mdm_Input_Rec_Pnd_Cref_Cntrct_No;
    private DbsField pnd_Mdm_Input_Rec_Pnd_Ap_Isn;
    private DbsField pnd_Mdm_Input_Rec_Pnd_Ap_Release_Ind;
    private DbsField pnd_Mdm_Input_Rec_Pnd_Cntrct_Found_Ind;
    private DbsField pnd_Mdm_Input_Rec_Pnd_Omni_Issue_Ind;

    private DbsGroup pnd_Miscellanous_Variables;
    private DbsField pnd_Miscellanous_Variables_Pnd_Recs_Selected_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Wk_Read_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Wk_Update_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count2;
    private DbsField pnd_Miscellanous_Variables_Pnd_Update_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Not_Found_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Hold_Date;
    private DbsField pnd_Miscellanous_Variables_Pnd_No_Issue_Dt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Comma;
    private DbsField pnd_Miscellanous_Variables_Pnd_Space;
    private DbsField pnd_Miscellanous_Variables_Pnd_Dash;
    private DbsField pnd_Miscellanous_Variables_Pnd_Del;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_7;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time;
    private DbsField pnd_Miscellanous_Variables_Pnd_Debug;
    private DbsField pnd_Miscellanous_Variables_Pnd_Except_Reason;
    private DbsField pnd_Miscellanous_Variables_Pnd_Exception_Cnt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Dflt_Line_Cnt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_8;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Mm;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_9;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Yy;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Mm;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Dd;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_A;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_10;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_N;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt;
    private DbsField pnd_I;
    private DbsField pnd_Rc;
    private DbsField pnd_Get_Cntr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAciadate = new PdaAciadate(localVariables);

        // Local Variables

        vw_prap_View = new DataAccessProgramView(new NameInfo("vw_prap_View", "PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        prap_View_Ap_Coll_Code = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_View_Ap_Soc_Sec = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_View_Ap_Lob = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB");
        prap_View_Ap_Lob.setDdmHeader("LOB");
        prap_View_Ap_T_Doi = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_DOI");
        prap_View_Ap_C_Doi = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_C_DOI");
        prap_View_Ap_Release_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        prap_View_Ap_Record_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_View_Ap_Lob_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB_TYPE");
        prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        prap_View_Ap_Cor_Last_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        prap_View_Ap_Cor_First_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        prap_View_Ap_Cor_Mddle_Nme = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        prap_View_Ap_Pin_Nbr = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AP_PIN_NBR");
        prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Bank_Pymnt_Acct_Nmbr", "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 
            21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        prap_View__R_Field_1 = vw_prap_View.getRecord().newGroupInGroup("prap_View__R_Field_1", "REDEFINE", prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        prap_View_Bank_Home_Ac = prap_View__R_Field_1.newFieldInGroup("prap_View_Bank_Home_Ac", "BANK-HOME-AC", FieldType.STRING, 3);
        prap_View_Bank_Home_A3 = prap_View__R_Field_1.newFieldInGroup("prap_View_Bank_Home_A3", "BANK-HOME-A3", FieldType.STRING, 3);
        prap_View_Bank_Home_A4 = prap_View__R_Field_1.newFieldInGroup("prap_View_Bank_Home_A4", "BANK-HOME-A4", FieldType.STRING, 4);
        prap_View_Bank_Filler_4 = prap_View__R_Field_1.newFieldInGroup("prap_View_Bank_Filler_4", "BANK-FILLER-4", FieldType.STRING, 4);
        prap_View_Oia_Indicator = prap_View__R_Field_1.newFieldInGroup("prap_View_Oia_Indicator", "OIA-INDICATOR", FieldType.STRING, 2);
        prap_View_Erisa_Ind = prap_View__R_Field_1.newFieldInGroup("prap_View_Erisa_Ind", "ERISA-IND", FieldType.STRING, 1);
        prap_View_Cai_Ind = prap_View__R_Field_1.newFieldInGroup("prap_View_Cai_Ind", "CAI-IND", FieldType.STRING, 1);
        prap_View_Cip_Ind = prap_View__R_Field_1.newFieldInGroup("prap_View_Cip_Ind", "CIP-IND", FieldType.STRING, 1);
        prap_View_Same_Addr = prap_View__R_Field_1.newFieldInGroup("prap_View_Same_Addr", "SAME-ADDR", FieldType.STRING, 1);
        prap_View_Omni_Issue_Ind = prap_View__R_Field_1.newFieldInGroup("prap_View_Omni_Issue_Ind", "OMNI-ISSUE-IND", FieldType.STRING, 1);
        prap_View_Ap_Tiaa_Cntrct = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_View_Ap_Cref_Cert = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        prap_View_Ap_Sgrd_Plan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_View_Ap_Sgrd_Subplan_No = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        prap_View_Ap_Sgrd_Part_Ext = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_SGRD_PART_EXT");

        prap_View_Ap_Rqst_Log_Dte_Tm = vw_prap_View.getRecord().newGroupInGroup("prap_View_Ap_Rqst_Log_Dte_Tm", "AP-RQST-LOG-DTE-TM", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        prap_View_Ap_Rqst_Log_Dte_Time = prap_View_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("prap_View_Ap_Rqst_Log_Dte_Time", "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 
            15, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        prap_View_Ap_Tiaa_Doi = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", FieldType.DATE, RepeatingFieldStrategy.None, 
            "AP_TIAA_DOI");
        prap_View_Ap_Cref_Doi = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cref_Doi", "AP-CREF-DOI", FieldType.DATE, RepeatingFieldStrategy.None, 
            "AP_CREF_DOI");
        prap_View_Ap_Substitution_Contract_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Substitution_Contract_Ind", "AP-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        prap_View_Ap_Conv_Issue_State = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Conv_Issue_State", "AP-CONV-ISSUE-STATE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_CONV_ISSUE_STATE");
        prap_View_Ap_Deceased_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Deceased_Ind", "AP-DECEASED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_DECEASED_IND");
        registerRecord(vw_prap_View);

        pnd_Ap_Name = localVariables.newFieldInRecord("pnd_Ap_Name", "#AP-NAME", FieldType.STRING, 30);
        pnd_Cor_Full_Nme = localVariables.newFieldInRecord("pnd_Cor_Full_Nme", "#COR-FULL-NME", FieldType.STRING, 90);

        pnd_Cor_Full_Nme__R_Field_2 = localVariables.newGroupInRecord("pnd_Cor_Full_Nme__R_Field_2", "REDEFINE", pnd_Cor_Full_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme = pnd_Cor_Full_Nme__R_Field_2.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme", "#COR-LAST-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_3 = pnd_Cor_Full_Nme__R_Field_2.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_3", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16 = pnd_Cor_Full_Nme__R_Field_3.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16", "#COR-LAST-NME-16", 
            FieldType.STRING, 16);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14 = pnd_Cor_Full_Nme__R_Field_3.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14", "#COR-LAST-NME-14", 
            FieldType.STRING, 14);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme = pnd_Cor_Full_Nme__R_Field_2.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme", "#COR-FIRST-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_4 = pnd_Cor_Full_Nme__R_Field_2.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_4", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_First_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10 = pnd_Cor_Full_Nme__R_Field_4.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10", "#COR-FIRST-NME-10", 
            FieldType.STRING, 10);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20 = pnd_Cor_Full_Nme__R_Field_4.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20", "#COR-FIRST-NME-20", 
            FieldType.STRING, 20);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme = pnd_Cor_Full_Nme__R_Field_2.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme", "#COR-MDDLE-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_5 = pnd_Cor_Full_Nme__R_Field_2.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_5", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10 = pnd_Cor_Full_Nme__R_Field_5.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10", "#COR-MDDLE-NME-10", 
            FieldType.STRING, 10);

        pnd_Cor_Full_Nme__R_Field_6 = pnd_Cor_Full_Nme__R_Field_5.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_6", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1 = pnd_Cor_Full_Nme__R_Field_6.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1", "#COR-MDDLE-NME-1", 
            FieldType.STRING, 1);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9 = pnd_Cor_Full_Nme__R_Field_6.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9", "#COR-MDDLE-NME-9", 
            FieldType.STRING, 9);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20 = pnd_Cor_Full_Nme__R_Field_5.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20", "#COR-MDDLE-NME-20", 
            FieldType.STRING, 20);
        pnd_Hold_Nme_Prfx = localVariables.newFieldInRecord("pnd_Hold_Nme_Prfx", "#HOLD-NME-PRFX", FieldType.STRING, 8);

        pnd_Mdm_Input_Rec = localVariables.newGroupInRecord("pnd_Mdm_Input_Rec", "#MDM-INPUT-REC");
        pnd_Mdm_Input_Rec_Pnd_Tiaa_Cntrct_No = pnd_Mdm_Input_Rec.newFieldInGroup("pnd_Mdm_Input_Rec_Pnd_Tiaa_Cntrct_No", "#TIAA-CNTRCT-NO", FieldType.STRING, 
            10);
        pnd_Mdm_Input_Rec_Pnd_Cref_Cntrct_No = pnd_Mdm_Input_Rec.newFieldInGroup("pnd_Mdm_Input_Rec_Pnd_Cref_Cntrct_No", "#CREF-CNTRCT-NO", FieldType.STRING, 
            10);
        pnd_Mdm_Input_Rec_Pnd_Ap_Isn = pnd_Mdm_Input_Rec.newFieldInGroup("pnd_Mdm_Input_Rec_Pnd_Ap_Isn", "#AP-ISN", FieldType.NUMERIC, 10);
        pnd_Mdm_Input_Rec_Pnd_Ap_Release_Ind = pnd_Mdm_Input_Rec.newFieldInGroup("pnd_Mdm_Input_Rec_Pnd_Ap_Release_Ind", "#AP-RELEASE-IND", FieldType.STRING, 
            1);
        pnd_Mdm_Input_Rec_Pnd_Cntrct_Found_Ind = pnd_Mdm_Input_Rec.newFieldInGroup("pnd_Mdm_Input_Rec_Pnd_Cntrct_Found_Ind", "#CNTRCT-FOUND-IND", FieldType.STRING, 
            1);
        pnd_Mdm_Input_Rec_Pnd_Omni_Issue_Ind = pnd_Mdm_Input_Rec.newFieldInGroup("pnd_Mdm_Input_Rec_Pnd_Omni_Issue_Ind", "#OMNI-ISSUE-IND", FieldType.STRING, 
            1);

        pnd_Miscellanous_Variables = localVariables.newGroupInRecord("pnd_Miscellanous_Variables", "#MISCELLANOUS-VARIABLES");
        pnd_Miscellanous_Variables_Pnd_Recs_Selected_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Recs_Selected_Count", 
            "#RECS-SELECTED-COUNT", FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Wk_Read_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Wk_Read_Count", "#WK-READ-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Wk_Update_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Wk_Update_Count", 
            "#WK-UPDATE-COUNT", FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count", 
            "#UPDATE-RELEASE-IND-COUNT", FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count2 = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count2", 
            "#UPDATE-RELEASE-IND-COUNT2", FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Update_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Update_Count", "#UPDATE-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Not_Found_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Not_Found_Count", 
            "#NOT-FOUND-COUNT", FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Hold_Date = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Hold_Date", "#HOLD-DATE", 
            FieldType.DATE);
        pnd_Miscellanous_Variables_Pnd_No_Issue_Dt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_No_Issue_Dt", "#NO-ISSUE-DT", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_Comma = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Comma", "#COMMA", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Space = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Space", "#SPACE", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Dash = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Dash", "#DASH", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Del = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Del", "#DEL", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time", 
            "#RQST-LOG-DTE-TIME", FieldType.STRING, 15);

        pnd_Miscellanous_Variables__R_Field_7 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_7", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date = pnd_Miscellanous_Variables__R_Field_7.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date", 
            "#RQST-LOG-DATE", FieldType.STRING, 8);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time = pnd_Miscellanous_Variables__R_Field_7.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time", 
            "#RQST-LOG-TIME", FieldType.STRING, 7);
        pnd_Miscellanous_Variables_Pnd_Debug = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 
            1);
        pnd_Miscellanous_Variables_Pnd_Except_Reason = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Except_Reason", "#EXCEPT-REASON", 
            FieldType.STRING, 35);
        pnd_Miscellanous_Variables_Pnd_Exception_Cnt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Exception_Cnt", "#EXCEPTION-CNT", 
            FieldType.NUMERIC, 6);
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt", "#EXC-LINE-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Miscellanous_Variables_Pnd_Dflt_Line_Cnt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Dflt_Line_Cnt", "#DFLT-LINE-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short", "#ISSUE-DT-SHORT", 
            FieldType.NUMERIC, 4);

        pnd_Miscellanous_Variables__R_Field_8 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_8", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Mm = pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Mm", 
            "#ISSUE-DT-S-MM", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy = pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy", 
            "#ISSUE-DT-S-YY", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full", "#ISSUE-DT-FULL", 
            FieldType.NUMERIC, 8);

        pnd_Miscellanous_Variables__R_Field_9 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_9", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc = pnd_Miscellanous_Variables__R_Field_9.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc", 
            "#ISSUE-DT-CC", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Yy = pnd_Miscellanous_Variables__R_Field_9.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Yy", 
            "#ISSUE-DT-YY", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Mm = pnd_Miscellanous_Variables__R_Field_9.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Mm", 
            "#ISSUE-DT-MM", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Dd = pnd_Miscellanous_Variables__R_Field_9.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Dd", 
            "#ISSUE-DT-DD", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_A = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_A", "#ISSUE-DT-A", 
            FieldType.STRING, 8);

        pnd_Miscellanous_Variables__R_Field_10 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_10", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Issue_Dt_A);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_N = pnd_Miscellanous_Variables__R_Field_10.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_N", 
            "#ISSUE-DT-N", FieldType.NUMERIC, 8);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt", "#ISSUE-DT", FieldType.DATE);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_Get_Cntr = localVariables.newFieldInRecord("pnd_Get_Cntr", "#GET-CNTR", FieldType.NUMERIC, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap_View.reset();

        localVariables.reset();
        pnd_Miscellanous_Variables_Pnd_Comma.setInitialValue(",");
        pnd_Miscellanous_Variables_Pnd_Space.setInitialValue(" ");
        pnd_Miscellanous_Variables_Pnd_Dash.setInitialValue("-");
        pnd_Miscellanous_Variables_Pnd_Del.setInitialValue("+");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb150() throws Exception
    {
        super("Appb150");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52;//Natural: FORMAT ( 2 ) LS = 133 PS = 52;//Natural: FORMAT ( 3 ) LS = 133 PS = 52
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*  TO READ PRAP FILE AND UPDATE ALL DEFAULED ENROLLED CONTRACTS
        //*  WITH AP-RELEASE-IND = '4' AND OMNI-ISSUE-IND = '2' AND CHANGE THE
        //*  AP-RELEASE-IND TO '7' SO THEY CAN BE EXCLUDED IN THE LEGAL PACKAGE
        //*  GENERATION.
        //*  CNR
                                                                                                                                                                          //Natural: PERFORM UPDT-REL-IND-FOR-DFLT-ENROLLED
        sub_Updt_Rel_Ind_For_Dflt_Enrolled();
        if (condition(Global.isEscape())) {return;}
        //*  READ MDM FILE W/C CONTAINS DEFAULT ENROLLMENT IND AND UPDATE PRAP FILE
        //*  WITH OMNI-ISSUE-IND AND SET AP-RELEASE-IND TO 4 IF IT IS 7
        //*  CNR
                                                                                                                                                                          //Natural: PERFORM UPDATE-DEFAULT-ENROLMENT-IND
        sub_Update_Default_Enrolment_Ind();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT
        sub_Write_Control_Report();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-DEFAULT-ENROLMENT-IND
        //* ***********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDT-REL-IND-FOR-DFLT-ENROLLED
        //* *---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REPORT
        //* *---------------------------------------
        //*  WRITE(1) 14X  'TOTAL PRAP RECORDS READ               ' #ALL-READ-COUNT
        //*    /
        //*  WRITE(1) 14X  'TOTAL PRAP DEFAULT ENROLL RECORDS READ' #READ-COUNT
        //*    /
        //*  WRITE(1) 14X  'TOTAL DEFAULT RECORDS CONVERTED       ' #WRITE-COUNT
        //*    /
        //*  WRITE(1) 14X  'TOTAL DEFAULT RECORDS UPDATED TO PRAP ' #UPDATE-COUNT
        //*    /
        //* *WRITE(1) 14X  'TOTAL RECORDS IN ERROR                ' #ERROR-COUNT
        //*  ///////
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT
        //*  7X  AP-TIAA-CNTRCT (EM=XXXXXXX-X)
        //*  7X  AP-CREF-CERT   (EM=XXXXXXX-X)
        //*   8X  AP-SGRD-PLAN-NO
        //*   11X  AP-SGRD-SUBPLAN-NO
        //* *----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT-WRITE-HEADER
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DEFLT-ENROLL-REPORT
        //* *----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DEFLT-REPORT-WRITE-HEADER
        //*  64X  'SOCIAL'/
        //*  43X  'SECURITY'
        //*  6X  'NUMBER'
        //*  2X  '-------'
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-RTN
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUE-DATE-RTN
    }
    //*  CNR
    private void sub_Update_Default_Enrolment_Ind() throws Exception                                                                                                      //Natural: UPDATE-DEFAULT-ENROLMENT-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        //*  READ MDM WORKFILE FOR DEFAULT-ENROLLMENT-IND
        //*  INPUT FROM MDM
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #MDM-INPUT-REC
        while (condition(getWorkFiles().read(1, pnd_Mdm_Input_Rec)))
        {
            pnd_Miscellanous_Variables_Pnd_Wk_Read_Count.nadd(1);                                                                                                         //Natural: ADD 1 TO #WK-READ-COUNT
            if (condition(pnd_Mdm_Input_Rec_Pnd_Cntrct_Found_Ind.equals("N")))                                                                                            //Natural: IF #CNTRCT-FOUND-IND = 'N'
            {
                pnd_Miscellanous_Variables_Pnd_Not_Found_Count.nadd(1);                                                                                                   //Natural: ADD 1 TO #NOT-FOUND-COUNT
                pnd_Miscellanous_Variables_Pnd_Except_Reason.setValue("CONTRACT NOT FOUND ON MDM");                                                                       //Natural: MOVE 'CONTRACT NOT FOUND ON MDM' TO #EXCEPT-REASON
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT
                sub_Exception_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  CHG362343
            //*  CHG362343
            //*  CHG362343
            if (condition(pnd_Mdm_Input_Rec_Pnd_Omni_Issue_Ind.equals(" ") || pnd_Mdm_Input_Rec_Pnd_Omni_Issue_Ind.equals("0")))                                          //Natural: IF #OMNI-ISSUE-IND = ' ' OR = '0'
            {
                PND_PND_L2080:                                                                                                                                            //Natural: GET PRAP-VIEW #AP-ISN
                vw_prap_View.readByID(pnd_Mdm_Input_Rec_Pnd_Ap_Isn.getLong(), "PND_PND_L2080");
                pnd_Get_Cntr.nadd(1);                                                                                                                                     //Natural: ASSIGN #GET-CNTR := #GET-CNTR + 1
                //*  CHG362343
                if (condition(prap_View_Ap_Tiaa_Cntrct.notEquals(pnd_Mdm_Input_Rec_Pnd_Tiaa_Cntrct_No)))                                                                  //Natural: IF AP-TIAA-CNTRCT NE #TIAA-CNTRCT-NO
                {
                    //*  CHG362343
                    //*  CHG362343
                    getReports().write(0, "**** ERROR NOT SAME CONTRACT IN PRAP AND MDM ",NEWLINE,prap_View_Ap_Tiaa_Cntrct," / ",pnd_Mdm_Input_Rec_Pnd_Tiaa_Cntrct_No);   //Natural: WRITE '**** ERROR NOT SAME CONTRACT IN PRAP AND MDM ' / AP-TIAA-CNTRCT ' / ' #TIAA-CNTRCT-NO
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  CHG362343
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  CHG362343
                    if (condition(prap_View_Ap_Release_Ind.equals(7)))                                                                                                    //Natural: IF AP-RELEASE-IND = 7
                    {
                        //*  CHG362343
                        prap_View_Ap_Release_Ind.setValue(4);                                                                                                             //Natural: MOVE 4 TO AP-RELEASE-IND
                        //*  CHG362343
                        //*  CHG362343
                        pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count.nadd(1);                                                                                  //Natural: ADD 1 TO #UPDATE-RELEASE-IND-COUNT
                        vw_prap_View.updateDBRow("PND_PND_L2080");                                                                                                        //Natural: UPDATE ( ##L2080. )
                        //*  CHG362343
                                                                                                                                                                          //Natural: PERFORM WRITE-DEFLT-ENROLL-REPORT
                        sub_Write_Deflt_Enroll_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  CHG362343
                        pnd_Miscellanous_Variables_Pnd_Wk_Update_Count.nadd(1);                                                                                           //Natural: ADD 1 TO #WK-UPDATE-COUNT
                        //*  CHG362343
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CHG362343
                }                                                                                                                                                         //Natural: END-IF
                //*  CHG362343
            }                                                                                                                                                             //Natural: END-IF
            //*  CHG362343
            if (condition(pnd_Get_Cntr.greater(50)))                                                                                                                      //Natural: IF #GET-CNTR > 50
            {
                //*  CHG362343
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  CHG362343
                pnd_Get_Cntr.reset();                                                                                                                                     //Natural: RESET #GET-CNTR
                //*  CHG362343
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CHG362343
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    //*  CNR
    private void sub_Updt_Rel_Ind_For_Dflt_Enrolled() throws Exception                                                                                                    //Natural: UPDT-REL-IND-FOR-DFLT-ENROLLED
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************
        vw_prap_View.startDatabaseRead                                                                                                                                    //Natural: READ PRAP-VIEW BY AP-RELEASE-IND STARTING FROM 4
        (
        "L_R2",
        new Wc[] { new Wc("AP_RELEASE_IND", ">=", 4, WcType.BY) },
        new Oc[] { new Oc("AP_RELEASE_IND", "ASC") }
        );
        L_R2:
        while (condition(vw_prap_View.readNextRow("L_R2")))
        {
            if (condition(prap_View_Ap_Release_Ind.greater(4)))                                                                                                           //Natural: IF AP-RELEASE-IND GT 4
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(prap_View_Omni_Issue_Ind.notEquals("2")))                                                                                                       //Natural: IF OMNI-ISSUE-IND NE '2'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            L_GET:                                                                                                                                                        //Natural: GET PRAP-VIEW *ISN ( L-R2. )
            vw_prap_View.readByID(vw_prap_View.getAstISN("L_R2"), "L_GET");
            pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date.reset();                                                                                                         //Natural: RESET #RQST-LOG-DATE
            pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #RQST-LOG-DATE
            prap_View_Ap_Release_Ind.setValue(7);                                                                                                                         //Natural: MOVE 7 TO AP-RELEASE-IND
            prap_View_Ap_Rqst_Log_Dte_Time.getValue(5).setValue(pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time);                                                        //Natural: MOVE #RQST-LOG-DTE-TIME TO AP-RQST-LOG-DTE-TIME ( 5 )
            pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count2.nadd(1);                                                                                             //Natural: ADD 1 TO #UPDATE-RELEASE-IND-COUNT2
            vw_prap_View.updateDBRow("L_GET");                                                                                                                            //Natural: UPDATE ( L-GET. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  TITLE LEFT JUSTIFIED
    private void sub_Write_Control_Report() throws Exception                                                                                                              //Natural: WRITE-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. DEFAULT ENROLL CONTROL TOTALS    ",new ColumnSpacing(16),Global.getDATU());              //Natural: WRITE ( 1 ) *PROGRAM 5X 'A.C.I.S. DEFAULT ENROLL CONTROL TOTALS    ' 16X *DATU
        if (Global.isEscape()) return;
        getReports().write(1, Global.getTIMX(),new ColumnSpacing(58),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);                                                            //Natural: WRITE ( 1 ) *TIMX 58X /////
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL MDM RECORDS READ                ",pnd_Miscellanous_Variables_Pnd_Wk_Read_Count,NEWLINE);                       //Natural: WRITE ( 1 ) 14X 'TOTAL MDM RECORDS READ                ' #WK-READ-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL PRAP NOT FOUND ON MDM           ",pnd_Miscellanous_Variables_Pnd_Not_Found_Count,NEWLINE);                     //Natural: WRITE ( 1 ) 14X 'TOTAL PRAP NOT FOUND ON MDM           ' #NOT-FOUND-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL DEFAULT ENROLL UPDATED TO PRAP  ",pnd_Miscellanous_Variables_Pnd_Wk_Update_Count,NEWLINE);                     //Natural: WRITE ( 1 ) 14X 'TOTAL DEFAULT ENROLL UPDATED TO PRAP  ' #WK-UPDATE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL RELEASE INDICATOR CONVERTED TO 4",pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count,NEWLINE);            //Natural: WRITE ( 1 ) 14X 'TOTAL RELEASE INDICATOR CONVERTED TO 4' #UPDATE-RELEASE-IND-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL RELEASE INDICATOR CONVERTED TO 7",pnd_Miscellanous_Variables_Pnd_Update_Release_Ind_Count2,                    //Natural: WRITE ( 1 ) 14X 'TOTAL RELEASE INDICATOR CONVERTED TO 7' #UPDATE-RELEASE-IND-COUNT2 ///
            NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(16),"**    END    OF   REPORT   **");                                                                                     //Natural: WRITE ( 1 ) 16X '**    END    OF   REPORT   **'
        if (Global.isEscape()) return;
    }
    private void sub_Exception_Report() throws Exception                                                                                                                  //Natural: EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //* * WRITE (2) / 2X AP-SOC-SEC (EM=999-99-9999)
        //*  CNR
        //*  CNR
        getReports().write(2, NEWLINE,new ColumnSpacing(2),pnd_Mdm_Input_Rec_Pnd_Tiaa_Cntrct_No, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(7),pnd_Mdm_Input_Rec_Pnd_Cref_Cntrct_No,  //Natural: WRITE ( 2 ) / 2X #TIAA-CNTRCT-NO ( EM = XXXXXXX-X ) 7X #CREF-CNTRCT-NO ( EM = XXXXXXX-X ) 7X #EXCEPT-REASON
            new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(7),pnd_Miscellanous_Variables_Pnd_Except_Reason);
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #EXC-LINE-CNT
        //*  EXCEPTION-REPORT
    }
    private void sub_Exception_Report_Write_Header() throws Exception                                                                                                     //Natural: EXCEPTION-REPORT-WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        //*  TITLE LEFT JUSTIFIED
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.reset();                                                                                                              //Natural: RESET #EXC-LINE-CNT
        getReports().write(2, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. DEFAULT ENROLL DAILY EXCEPTION REPORT",new ColumnSpacing(10),Global.getDATU());          //Natural: WRITE ( 2 ) *PROGRAM 5X 'A.C.I.S. DEFAULT ENROLL DAILY EXCEPTION REPORT' 10X *DATU
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(69),Global.getTIMX(),NEWLINE,NEWLINE);                                                                                    //Natural: WRITE ( 2 ) 69X *TIMX //
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(2),"TIAA CONTRACT",new ColumnSpacing(3),"CREF CONTRACT",new ColumnSpacing(3),"EXCEPTION",NEWLINE);                        //Natural: WRITE ( 2 ) 2X 'TIAA CONTRACT' 3X 'CREF CONTRACT' 3X 'EXCEPTION'/
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(6),"NUMBER",new ColumnSpacing(10),"NUMBER",new ColumnSpacing(7),"REASON",NEWLINE);                                        //Natural: WRITE ( 2 ) 6X 'NUMBER' 10X 'NUMBER' 7X 'REASON' /
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(2),"-------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"------------------------------------------", //Natural: WRITE ( 2 ) 2X '-------------' 3X '-------------' 3X '------------------------------------------'/
            NEWLINE);
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.nadd(5);                                                                                                              //Natural: ADD 5 TO #EXC-LINE-CNT
        //*  EXCEPTION-REPORT-WRITE-HEADER
    }
    private void sub_Write_Deflt_Enroll_Report() throws Exception                                                                                                         //Natural: WRITE-DEFLT-ENROLL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
                                                                                                                                                                          //Natural: PERFORM NAME-RTN
        sub_Name_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ISSUE-DATE-RTN
        sub_Issue_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
        getReports().write(3, NEWLINE,new ColumnSpacing(2),prap_View_Ap_Tiaa_Cntrct, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),prap_View_Ap_Cref_Cert,        //Natural: WRITE ( 3 ) / 2X AP-TIAA-CNTRCT ( EM = XXXXXXX-X ) 3X AP-CREF-CERT ( EM = XXXXXXX-X ) 2X AP-SGRD-PLAN-NO 2X AP-SGRD-SUBPLAN-NO 3X #AP-NAME 1X AP-PIN-NBR 2X AP-SOC-SEC ( EM = 999-99-9999 ) 2X #ISSUE-DT ( EM = MM/DD/YYYY )
            new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(2),prap_View_Ap_Sgrd_Plan_No,new ColumnSpacing(2),prap_View_Ap_Sgrd_Subplan_No,new ColumnSpacing(3),pnd_Ap_Name,new 
            ColumnSpacing(1),prap_View_Ap_Pin_Nbr,new ColumnSpacing(2),prap_View_Ap_Soc_Sec, new ReportEditMask ("999-99-9999"),new ColumnSpacing(2),pnd_Miscellanous_Variables_Pnd_Issue_Dt, 
            new ReportEditMask ("MM/DD/YYYY"));
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Dflt_Line_Cnt.nadd(1);                                                                                                             //Natural: ADD 1 TO #DFLT-LINE-CNT
        //* * ADD 1 TO #WRITE-COUNT                                  /* CNR
        //*  WRITE-DEFLT-ENROLL-REPORT
    }
    private void sub_Deflt_Report_Write_Header() throws Exception                                                                                                         //Natural: DEFLT-REPORT-WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        //*  TITLE LEFT JUSTIFIED
        pnd_Miscellanous_Variables_Pnd_Dflt_Line_Cnt.reset();                                                                                                             //Natural: RESET #DFLT-LINE-CNT
        getReports().write(3, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. DEFAULT ENROLLMENT DAILY CONVERSION REPORT",new ColumnSpacing(9),Global.getDATU());      //Natural: WRITE ( 3 ) *PROGRAM 5X 'A.C.I.S. DEFAULT ENROLLMENT DAILY CONVERSION REPORT' 9X *DATU
        if (Global.isEscape()) return;
        getReports().write(3, new ColumnSpacing(73),Global.getTIMX(),NEWLINE,NEWLINE);                                                                                    //Natural: WRITE ( 3 ) 73X *TIMX //
        if (Global.isEscape()) return;
        //* PINE
        getReports().write(3, new ColumnSpacing(4),"TIAA",new ColumnSpacing(8),"CREF",new ColumnSpacing(68),"SOCIAL",NEWLINE);                                            //Natural: WRITE ( 3 ) 4X 'TIAA' 8X 'CREF' 68X 'SOCIAL'/
        if (Global.isEscape()) return;
        //* PINE
        getReports().write(3, new ColumnSpacing(2),"CONTRACT",new ColumnSpacing(4),"CONTRACT",new ColumnSpacing(4),"PLAN",new ColumnSpacing(3),"SUBPLAN",new              //Natural: WRITE ( 3 ) 2X 'CONTRACT' 4X 'CONTRACT' 4X 'PLAN' 3X 'SUBPLAN' 47X 'SECURITY' 7X 'ISSUE'/
            ColumnSpacing(47),"SECURITY",new ColumnSpacing(7),"ISSUE",NEWLINE);
        if (Global.isEscape()) return;
        //* PINE
        getReports().write(3, new ColumnSpacing(3),"NUMBER",new ColumnSpacing(6),"NUMBER",new ColumnSpacing(4),"NUMBER",new ColumnSpacing(2),"NUMBER",new                 //Natural: WRITE ( 3 ) 3X 'NUMBER' 6X 'NUMBER' 4X 'NUMBER' 2X 'NUMBER' 9X 'PARTICIPANT NAME' 11X 'PIN' 10X 'NUMBER' 8X 'DATE' /
            ColumnSpacing(9),"PARTICIPANT NAME",new ColumnSpacing(11),"PIN",new ColumnSpacing(10),"NUMBER",new ColumnSpacing(8),"DATE",NEWLINE);
        if (Global.isEscape()) return;
        //* PINE
        getReports().write(3, new ColumnSpacing(1),"----------",new ColumnSpacing(2),"----------",new ColumnSpacing(2),"------",new ColumnSpacing(2),"-------",new        //Natural: WRITE ( 3 ) 1X '----------' 2X '----------' 2X '------' 2X '-------' 2X '------------------------------' 2X '------------' 2X '-----------' 2X '----------' /
            ColumnSpacing(2),"------------------------------",new ColumnSpacing(2),"------------",new ColumnSpacing(2),"-----------",new ColumnSpacing(2),
            "----------",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Dflt_Line_Cnt.nadd(6);                                                                                                             //Natural: ADD 6 TO #DFLT-LINE-CNT
        //*  DEFLT-REPORT-WRITE-HEADER
    }
    private void sub_Name_Rtn() throws Exception                                                                                                                          //Natural: NAME-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Ap_Name.reset();                                                                                                                                              //Natural: RESET #AP-NAME
        //*  CNR
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme.setValue(prap_View_Ap_Cor_Last_Nme);                                                                                            //Natural: MOVE AP-COR-LAST-NME TO #COR-LAST-NME
        //*  CNR
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme.setValue(prap_View_Ap_Cor_First_Nme);                                                                                          //Natural: MOVE AP-COR-FIRST-NME TO #COR-FIRST-NME
        //*  CNR
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme.setValue(prap_View_Ap_Cor_Mddle_Nme);                                                                                          //Natural: MOVE AP-COR-MDDLE-NME TO #COR-MDDLE-NME
        if (condition(pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10.equals(" ")))                                                                                                 //Natural: IF #COR-MDDLE-NME-10 = ' '
        {
            pnd_Ap_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16, pnd_Miscellanous_Variables_Pnd_Comma,              //Natural: COMPRESS #COR-LAST-NME-16 #COMMA #DEL #COR-FIRST-NME-10 INTO #AP-NAME LEAVING NO SPACE
                pnd_Miscellanous_Variables_Pnd_Del, pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ap_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16, pnd_Miscellanous_Variables_Pnd_Comma,              //Natural: COMPRESS #COR-LAST-NME-16 #COMMA #DEL #COR-FIRST-NME-10 #COMMA #DEL #COR-MDDLE-NME-1 INTO #AP-NAME LEAVING NO SPACE
                pnd_Miscellanous_Variables_Pnd_Del, pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10, pnd_Miscellanous_Variables_Pnd_Comma, pnd_Miscellanous_Variables_Pnd_Del, 
                pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1));
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ap_Name,true), new ExamineSearch("+"), new ExamineReplace(" "));                                                            //Natural: EXAMINE FULL #AP-NAME '+' REPLACE WITH ' '
        //*  NAME-RTN
    }
    private void sub_Issue_Date_Rtn() throws Exception                                                                                                                    //Natural: ISSUE-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        if (condition(prap_View_Ap_T_Doi.greater(getZero())))                                                                                                             //Natural: IF AP-T-DOI GT 0
        {
            pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short.setValue(prap_View_Ap_T_Doi);                                                                                   //Natural: MOVE AP-T-DOI TO #ISSUE-DT-SHORT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(prap_View_Ap_C_Doi.greater(getZero())))                                                                                                         //Natural: IF AP-C-DOI GT 0
            {
                pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short.setValue(prap_View_Ap_C_Doi);                                                                               //Natural: MOVE AP-C-DOI TO #ISSUE-DT-SHORT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Miscellanous_Variables_Pnd_No_Issue_Dt.setValue(true);                                                                                                //Natural: ASSIGN #NO-ISSUE-DT := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Miscellanous_Variables_Pnd_No_Issue_Dt.getBoolean())))                                                                                       //Natural: IF NOT #NO-ISSUE-DT
        {
            //*  TNGSUB
            //*  TNGSUB
            if (condition(prap_View_Ap_Substitution_Contract_Ind.greater(" ") && prap_View_Ap_Tiaa_Doi.greater(getZero())))                                               //Natural: IF AP-SUBSTITUTION-CONTRACT-IND > ' ' AND AP-TIAA-DOI > 0
            {
                //*  TNGSUB
                //*  TNGSUB
                pnd_Miscellanous_Variables_Pnd_Issue_Dt_A.setValueEdited(prap_View_Ap_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED AP-TIAA-DOI ( EM = YYYYMMDD ) TO #ISSUE-DT-A
                //*  TNGSUB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Miscellanous_Variables_Pnd_Issue_Dt_Mm.setValue(pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Mm);                                                        //Natural: MOVE #ISSUE-DT-S-MM TO #ISSUE-DT-MM
                pnd_Miscellanous_Variables_Pnd_Issue_Dt_Yy.setValue(pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy);                                                        //Natural: MOVE #ISSUE-DT-S-YY TO #ISSUE-DT-YY
                pnd_Miscellanous_Variables_Pnd_Issue_Dt_Dd.setValue(1);                                                                                                   //Natural: MOVE 01 TO #ISSUE-DT-DD
                if (condition(pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy.greater(40)))                                                                                  //Natural: IF #ISSUE-DT-S-YY GT 40
                {
                    pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc.setValue(19);                                                                                              //Natural: MOVE 19 TO #ISSUE-DT-CC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc.setValue(20);                                                                                              //Natural: MOVE 20 TO #ISSUE-DT-CC
                }                                                                                                                                                         //Natural: END-IF
                pnd_Miscellanous_Variables_Pnd_Issue_Dt_N.setValue(pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full);                                                         //Natural: ASSIGN #ISSUE-DT-N = #ISSUE-DT-FULL
                //*  TNGSUB
            }                                                                                                                                                             //Natural: END-IF
            pnd_Miscellanous_Variables_Pnd_Issue_Dt.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Miscellanous_Variables_Pnd_Issue_Dt_A);                             //Natural: MOVE EDITED #ISSUE-DT-A TO #ISSUE-DT ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Miscellanous_Variables_Pnd_Issue_Dt.reset();                                                                                                              //Natural: RESET #ISSUE-DT
            pnd_Miscellanous_Variables_Pnd_No_Issue_Dt.reset();                                                                                                           //Natural: RESET #NO-ISSUE-DT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ISSUE-DATE-RTN
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT-WRITE-HEADER
                    sub_Exception_Report_Write_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM DEFLT-REPORT-WRITE-HEADER
                    sub_Deflt_Report_Write_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52");
        Global.format(2, "LS=133 PS=52");
        Global.format(3, "LS=133 PS=52");
    }
}
