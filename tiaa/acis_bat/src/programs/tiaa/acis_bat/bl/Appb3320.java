/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:34 PM
**        * FROM NATURAL PROGRAM : Appb3320
************************************************************
**        * FILE NAME            : Appb3320.java
**        * CLASS NAME           : Appb3320
**        * INSTANCE NAME        : Appb3320
************************************************************
**********************************************************************
* PROGRAM    : APPB3320
* WRITTEN ON : 03/31/04
* PURPOSE    : THIS PROGRAM WILL EXTRACT SUNGARD APPLICATIONS THAT ARE
*              OVER 180 DAYS OLD FOR A REPORT.
**********************************************************************
* 03/01/06   : PROGRAM RESTOWED FOR DIV/SUB FOR APPL301  KG
* 07/14/06   : PROGRAM RESTOWED FOR ARR PROJECT FOR APPL301  KG
* 05/03/07   : PROGRAM RESTOWED FOR ARR2 PROJECT FOR APPL301  KG
* 09/23/08   : PROGRAM RESTOWED FOR AUTO ENROLL PROJECT FOR APPL301
* 08/12/09 C. AVE    - RE-STOW NEW LAYOUT OF APPL301 FOR ACIS PERF
* 05/02/11 C. SCHNEIDER - RE-STOW NEW LAYOUT OF APPL301 (JHU)
* 06/27/11 C. SCHNEIDER - RE-STOW NEW LAYOUT OF APPL301 (TIC)
* 11/17/12 L. SHU    - RE-STOW NEW LAYOUT OF APPL301    (LPAO)
* 07/18/13 L. SHU    - RE-STOW FOR APPL301 FOR IRA SUBSTITUTION
* 09/12/13 B.NEWSOM - RESTOW FOR AP-NON-PROPRIETARY-PKG-IND ADDED TO
*                     ADDED TO APPL301.                      (MTSIN)
* 12/07/13 L. SHU   - USE CURRENT DATE INSTEAD OF FROM PREMIUM CONTROL
*                     FILE                                   (CUSPORT)
* 09/01/15 L. SHU   - RESTOW FOR NEW FIELDS IN APPL301 FOR BENE IN
*                     THE PLAN.                                 (BIP)
* 11/01/16 L. SHU   - RESTOW FOR NEW FIELDS IN APPL301 FOR 1IRA
* 06/19/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)(RESTOW) PINE.
* 11/09/19 L. SHU   - RESTOW FOR NEW FIELDS IN APPL301 FOR IISG
**********************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb3320 extends BLNatBase
{
    // Data Areas
    private LdaAppl301 ldaAppl301;
    private PdaAciadate pdaAciadate;
    private PdaAcia1050 pdaAcia1050;
    private PdaAcia2100 pdaAcia2100;
    private LdaAppl3320 ldaAppl3320;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Today_Ccyymmdd;
    private DbsField pnd_Ccyymmdd;

    private DbsGroup pnd_Ccyymmdd__R_Field_1;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Cc;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Yy;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Mm;
    private DbsField pnd_Ccyymmdd_Pnd_Ccyymmdd_Dd;
    private DbsField pnd_Mmddccyy;

    private DbsGroup pnd_Mmddccyy__R_Field_2;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Mm;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Dd;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Cc;
    private DbsField pnd_Mmddccyy_Pnd_Mmddccyy_Yy;
    private DbsField pnd_Mmyy;

    private DbsGroup pnd_Mmyy__R_Field_3;
    private DbsField pnd_Mmyy_Pnd_Mmyy_Mm;
    private DbsField pnd_Mmyy_Pnd_Mmyy_Yy;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Found;
    private DbsField pnd_Debug;
    private DbsField pnd_Debug_1;
    private DbsField pnd_Display_Flag;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl301 = new LdaAppl301();
        registerRecord(ldaAppl301);
        registerRecord(ldaAppl301.getVw_prap());
        localVariables = new DbsRecord();
        pdaAciadate = new PdaAciadate(localVariables);
        pdaAcia1050 = new PdaAcia1050(localVariables);
        pdaAcia2100 = new PdaAcia2100(localVariables);
        ldaAppl3320 = new LdaAppl3320();
        registerRecord(ldaAppl3320);

        // Local Variables
        pnd_Today_Ccyymmdd = localVariables.newFieldInRecord("pnd_Today_Ccyymmdd", "#TODAY-CCYYMMDD", FieldType.NUMERIC, 8);
        pnd_Ccyymmdd = localVariables.newFieldInRecord("pnd_Ccyymmdd", "#CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Ccyymmdd__R_Field_1 = localVariables.newGroupInRecord("pnd_Ccyymmdd__R_Field_1", "REDEFINE", pnd_Ccyymmdd);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Cc = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Cc", "#CCYYMMDD-CC", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Yy = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Yy", "#CCYYMMDD-YY", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Mm = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Mm", "#CCYYMMDD-MM", FieldType.NUMERIC, 2);
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Dd = pnd_Ccyymmdd__R_Field_1.newFieldInGroup("pnd_Ccyymmdd_Pnd_Ccyymmdd_Dd", "#CCYYMMDD-DD", FieldType.NUMERIC, 2);
        pnd_Mmddccyy = localVariables.newFieldInRecord("pnd_Mmddccyy", "#MMDDCCYY", FieldType.NUMERIC, 8);

        pnd_Mmddccyy__R_Field_2 = localVariables.newGroupInRecord("pnd_Mmddccyy__R_Field_2", "REDEFINE", pnd_Mmddccyy);
        pnd_Mmddccyy_Pnd_Mmddccyy_Mm = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Mm", "#MMDDCCYY-MM", FieldType.NUMERIC, 2);
        pnd_Mmddccyy_Pnd_Mmddccyy_Dd = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Dd", "#MMDDCCYY-DD", FieldType.NUMERIC, 2);
        pnd_Mmddccyy_Pnd_Mmddccyy_Cc = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Cc", "#MMDDCCYY-CC", FieldType.NUMERIC, 2);
        pnd_Mmddccyy_Pnd_Mmddccyy_Yy = pnd_Mmddccyy__R_Field_2.newFieldInGroup("pnd_Mmddccyy_Pnd_Mmddccyy_Yy", "#MMDDCCYY-YY", FieldType.NUMERIC, 2);
        pnd_Mmyy = localVariables.newFieldInRecord("pnd_Mmyy", "#MMYY", FieldType.NUMERIC, 4);

        pnd_Mmyy__R_Field_3 = localVariables.newGroupInRecord("pnd_Mmyy__R_Field_3", "REDEFINE", pnd_Mmyy);
        pnd_Mmyy_Pnd_Mmyy_Mm = pnd_Mmyy__R_Field_3.newFieldInGroup("pnd_Mmyy_Pnd_Mmyy_Mm", "#MMYY-MM", FieldType.NUMERIC, 2);
        pnd_Mmyy_Pnd_Mmyy_Yy = pnd_Mmyy__R_Field_3.newFieldInGroup("pnd_Mmyy_Pnd_Mmyy_Yy", "#MMYY-YY", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Debug_1 = localVariables.newFieldInRecord("pnd_Debug_1", "#DEBUG-1", FieldType.BOOLEAN, 1);
        pnd_Display_Flag = localVariables.newFieldInRecord("pnd_Display_Flag", "#DISPLAY-FLAG", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl301.initializeValues();
        ldaAppl3320.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb3320() throws Exception
    {
        super("Appb3320");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 HW = OFF PS = 52
                                                                                                                                                                          //Natural: PERFORM GET-CONTROL-DATE
        sub_Get_Control_Date();
        if (condition(Global.isEscape())) {return;}
        ldaAppl301.getVw_prap().startDatabaseRead                                                                                                                         //Natural: READ PRAP BY AP-PRAP-KEY = 'SGRD'
        (
        "READ01",
        new Wc[] { new Wc("AP_PRAP_KEY", ">=", "SGRD", WcType.BY) },
        new Oc[] { new Oc("AP_PRAP_KEY", "ASC") }
        );
        READ01:
        while (condition(ldaAppl301.getVw_prap().readNextRow("READ01")))
        {
            if (condition(ldaAppl301.getPrap_Ap_Coll_Code().notEquals("SGRD")))                                                                                           //Natural: IF AP-COLL-CODE NE 'SGRD'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl301.getPrap_Ap_Record_Type().notEquals(1)))                                                                                              //Natural: IF AP-RECORD-TYPE NE 1
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FIND-DAYS-DIFF
            sub_Find_Days_Diff();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaAciadate.getAciadate_Pnd_Diff().greater(180)))                                                                                               //Natural: IF #DIFF GT 180
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORKFILE
                sub_Write_Workfile();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDCY-TO-CYMD
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROL-DATE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-DAYS-DIFF
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORKFILE
        //*  WORK FILE 1
        //* *----------------------------------------------------------------------
    }
    private void sub_Mdcy_To_Cymd() throws Exception                                                                                                                      //Natural: MDCY-TO-CYMD
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Cc.setValue(pnd_Mmddccyy_Pnd_Mmddccyy_Cc);                                                                                              //Natural: MOVE #MMDDCCYY-CC TO #CCYYMMDD-CC
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Yy.setValue(pnd_Mmddccyy_Pnd_Mmddccyy_Yy);                                                                                              //Natural: MOVE #MMDDCCYY-YY TO #CCYYMMDD-YY
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Mm.setValue(pnd_Mmddccyy_Pnd_Mmddccyy_Mm);                                                                                              //Natural: MOVE #MMDDCCYY-MM TO #CCYYMMDD-MM
        pnd_Ccyymmdd_Pnd_Ccyymmdd_Dd.setValue(pnd_Mmddccyy_Pnd_Mmddccyy_Dd);                                                                                              //Natural: MOVE #MMDDCCYY-DD TO #CCYYMMDD-DD
        //*  CYMD-TO-MDCY
    }
    private void sub_Get_Control_Date() throws Exception                                                                                                                  //Natural: GET-CONTROL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //* * MOVE 'C' TO ACIA3521.#AA-PHYSICAL-RECORD-CD
        //* * RESET       ACIA3521.#CNTRL-TODAYS-RECIPROCAL-DT
        //* * CALLNAT 'ACIN3521' ACIA3521
        //* *  #DEBUG
        //* * MOVE ACIA3521.#CNTRL-TODAYS-BUSINESS-DT-CYMD  TO  #TODAY-CCYYMMDD
        //*  LS CUSPORT- CHANGED CONTROL DATE LOGIC TO USE CURRENT PROCESS DATE
        pnd_Today_Ccyymmdd.setValue(Global.getDATN());                                                                                                                    //Natural: MOVE *DATN TO #TODAY-CCYYMMDD
        getReports().write(0, Global.getPROGRAM(),"(1080) CURRENT-DATE"," =",pnd_Today_Ccyymmdd);                                                                         //Natural: WRITE *PROGRAM '(1080) CURRENT-DATE' ' =' #TODAY-CCYYMMDD
        if (Global.isEscape()) return;
        //*  GET-CONTROL-DATE
    }
    private void sub_Find_Days_Diff() throws Exception                                                                                                                    //Natural: FIND-DAYS-DIFF
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Mmddccyy.setValue(ldaAppl301.getPrap_Ap_Dt_App_Recvd());                                                                                                      //Natural: MOVE AP-DT-APP-RECVD TO #MMDDCCYY
                                                                                                                                                                          //Natural: PERFORM MDCY-TO-CYMD
        sub_Mdcy_To_Cymd();
        if (condition(Global.isEscape())) {return;}
        pdaAciadate.getAciadate_Pnd_Date_N_1().setValue(pnd_Ccyymmdd);                                                                                                    //Natural: MOVE #CCYYMMDD TO ACIADATE.#DATE-N-1
        pdaAciadate.getAciadate_Pnd_Date_N_2().setValue(pnd_Today_Ccyymmdd);                                                                                              //Natural: MOVE #TODAY-CCYYMMDD TO ACIADATE.#DATE-N-2
        pdaAciadate.getAciadate_Pnd_Function().setValue("01");                                                                                                            //Natural: MOVE '01' TO ACIADATE.#FUNCTION
        DbsUtil.callnat(Acindate.class , getCurrentProcessState(), pdaAciadate.getAciadate());                                                                            //Natural: CALLNAT 'ACINDATE' ACIADATE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAciadate.getAciadate_Pnd_Error_Code().notEquals(getZero())))                                                                                     //Natural: IF ACIADATE.#ERROR-CODE NE 0
        {
            getReports().write(0, Global.getPROGRAM(),"(4650) INVALID DATES FOR CALC OF OVERDUE STATUS");                                                                 //Natural: WRITE *PROGRAM '(4650) INVALID DATES FOR CALC OF OVERDUE STATUS'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Error_Code());                                                                                          //Natural: WRITE '=' ACIADATE.#ERROR-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Error_Msg());                                                                                           //Natural: WRITE '=' ACIADATE.#ERROR-MSG
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl301.getPrap_Ap_Soc_Sec());                                                                                                   //Natural: WRITE '=' AP-SOC-SEC
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl301.getPrap_Ap_Tiaa_Cntrct());                                                                                               //Natural: WRITE '=' AP-TIAA-CNTRCT
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaAppl301.getPrap_Ap_Record_Type());                                                                                               //Natural: WRITE '=' AP-RECORD-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Date_N_1());                                                                                            //Natural: WRITE '=' #DATE-N-1
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAciadate.getAciadate_Pnd_Date_N_2());                                                                                            //Natural: WRITE '=' #DATE-N-2
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAciadate.getAciadate_Pnd_Diff().less(getZero())))                                                                                                //Natural: IF #DIFF LT 0
        {
            pdaAciadate.getAciadate_Pnd_Diff().setValue(0);                                                                                                               //Natural: MOVE 0 TO #DIFF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Workfile() throws Exception                                                                                                                    //Natural: WRITE-WORKFILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  DCA PROCESS - DTM
        //*  DCA PROCESS - DTM
        getReports().write(0, "---- WRITING DAYS DIFF  -----------------------------------",NEWLINE,"=",ldaAppl301.getPrap_Ap_Sgrd_Plan_No(),"=",ldaAppl301.getPrap_Ap_Sgrd_Subplan_No(), //Natural: WRITE '---- WRITING DAYS DIFF  -----------------------------------' / '=' AP-SGRD-PLAN-NO '=' AP-SGRD-SUBPLAN-NO / '=' AP-SGRD-PART-EXT '=' AP-SGRD-DIVSUB / '=' AP-COR-LAST-NME / '=' AP-COR-FIRST-NME / '=' AP-SOC-SEC '=' AP-TIAA-CNTRCT '=' AP-CREF-CERT / '=' #DIFF '=' AP-DT-APP-RECVD '=' AP-DOB
            NEWLINE,"=",ldaAppl301.getPrap_Ap_Sgrd_Part_Ext(),"=",ldaAppl301.getPrap_Ap_Sgrd_Divsub(),NEWLINE,"=",ldaAppl301.getPrap_Ap_Cor_Last_Nme(),NEWLINE,
            "=",ldaAppl301.getPrap_Ap_Cor_First_Nme(),NEWLINE,"=",ldaAppl301.getPrap_Ap_Soc_Sec(),"=",ldaAppl301.getPrap_Ap_Tiaa_Cntrct(),"=",ldaAppl301.getPrap_Ap_Cref_Cert(),
            NEWLINE,"=",pdaAciadate.getAciadate_Pnd_Diff(),"=",ldaAppl301.getPrap_Ap_Dt_App_Recvd(),"=",ldaAppl301.getPrap_Ap_Dob());
        if (Global.isEscape()) return;
        ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Plan_No().setValue(ldaAppl301.getPrap_Ap_Sgrd_Plan_No());                                                                    //Natural: ASSIGN EX-SGRD-PLAN-NO := AP-SGRD-PLAN-NO
        ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Subplan_No().setValue(ldaAppl301.getPrap_Ap_Sgrd_Subplan_No());                                                              //Natural: ASSIGN EX-SGRD-SUBPLAN-NO := AP-SGRD-SUBPLAN-NO
        ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Part_Ext().setValue(ldaAppl301.getPrap_Ap_Sgrd_Part_Ext());                                                                  //Natural: ASSIGN EX-SGRD-PART-EXT := AP-SGRD-PART-EXT
        ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Divsub().setValue(ldaAppl301.getPrap_Ap_Sgrd_Divsub());                                                                      //Natural: ASSIGN EX-SGRD-DIVSUB := AP-SGRD-DIVSUB
        ldaAppl3320.getSgrd_Ext_File_Ex_Cor_Last_Name().setValue(ldaAppl301.getPrap_Ap_Cor_Last_Nme());                                                                   //Natural: ASSIGN EX-COR-LAST-NAME := AP-COR-LAST-NME
        ldaAppl3320.getSgrd_Ext_File_Ex_Cor_First_Name().setValue(ldaAppl301.getPrap_Ap_Cor_First_Nme());                                                                 //Natural: ASSIGN EX-COR-FIRST-NAME := AP-COR-FIRST-NME
        ldaAppl3320.getSgrd_Ext_File_Ex_Soc_Sec().setValue(ldaAppl301.getPrap_Ap_Soc_Sec());                                                                              //Natural: ASSIGN EX-SOC-SEC := AP-SOC-SEC
        ldaAppl3320.getSgrd_Ext_File_Ex_Tiaa_Cntrct().setValue(ldaAppl301.getPrap_Ap_Tiaa_Cntrct());                                                                      //Natural: ASSIGN EX-TIAA-CNTRCT := AP-TIAA-CNTRCT
        ldaAppl3320.getSgrd_Ext_File_Ex_Cref_Cert().setValue(ldaAppl301.getPrap_Ap_Cref_Cert());                                                                          //Natural: ASSIGN EX-CREF-CERT := AP-CREF-CERT
        ldaAppl3320.getSgrd_Ext_File_Ex_Day_Diff().setValue(pdaAciadate.getAciadate_Pnd_Diff());                                                                          //Natural: ASSIGN EX-DAY-DIFF := #DIFF
        ldaAppl3320.getSgrd_Ext_File_Ex_Pin_Nbr().setValue(ldaAppl301.getPrap_Ap_Pin_Nbr());                                                                              //Natural: ASSIGN EX-PIN-NBR := AP-PIN-NBR
        ldaAppl3320.getSgrd_Ext_File_Ex_Lob().setValue(ldaAppl301.getPrap_Ap_Lob());                                                                                      //Natural: ASSIGN EX-LOB := AP-LOB
        ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().setValue(ldaAppl301.getPrap_Ap_Lob_Type());                                                                            //Natural: ASSIGN EX-LOB-TYPE := AP-LOB-TYPE
        ldaAppl3320.getSgrd_Ext_File_Ex_Dob().setValue(ldaAppl301.getPrap_Ap_Dob());                                                                                      //Natural: ASSIGN EX-DOB := AP-DOB
        ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().setValue(ldaAppl301.getPrap_Ap_Applcnt_Req_Type());                                                            //Natural: ASSIGN EX-APPLCNT-REQ-TYPE := AP-APPLCNT-REQ-TYPE
        getWorkFiles().write(1, false, ldaAppl3320.getSgrd_Ext_File());                                                                                                   //Natural: WRITE WORK FILE 1 SGRD-EXT-FILE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 HW=OFF PS=52");
    }
}
