/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:20 PM
**        * FROM NATURAL PROGRAM : Acip9005
************************************************************
**        * FILE NAME            : Acip9005.java
**        * CLASS NAME           : Acip9005
**        * INSTANCE NAME        : Acip9005
************************************************************
******************************************************************
* YR2000 COMPLIANT FIX APPLIED  ---> M NACHBER 12/28/99          *
*                      LAST PCY STOW        ON 10/07/99 15:46:31 *
*                      REVIEWED BY   SR     ON 12/29/99          *
*                                                                *
* V.RAQUENO - 12/12/01 - IOES CHANGES                 /* IOES VR *
* K.GATES   - 03/21/04 - RESTOW FOR APPL220 SUNGARD RELEASE 2    *
* R.WILLIS  - 12/22/04 - TNT/REL4.                               *
* R.WILLIS  - 03/21/05 - TNT/REL5.                               *
* D.MARPURI - 09/09/05 - IRA/REL6.                               *
* K.GATES   - 10/05/06 - DCA/DEPOSIT FUND CHANGES - DCA KG       *
* N.CVETKOVIC  8/28/08 - RHSP PRODUCT CHANGES - RHSP NBC         *
* C.AVE     - 03/08/10 - INCLUDED IRA INDEXED PRODUCTS - TIGR    *
* MITRPAU   - 06/21/17 - PIN EXPANSION (STOW ONLY) (C425939) PINE*
******************************************************************
**Y2CHMN CP ANNACI (AD1199)
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Acip9005 extends BLNatBase
{
    // Data Areas
    private LdaAppl220 ldaAppl220;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rcd;

    private DbsGroup pnd_Rcd__R_Field_1;
    private DbsField pnd_Rcd_Pnd_Rcd_Mm;
    private DbsField pnd_Rcd_Pnd_Rcd_Fil1;
    private DbsField pnd_Rcd_Pnd_Rcd_Dd;
    private DbsField pnd_Rcd_Pnd_Rcd_Fil2;
    private DbsField pnd_Rcd_Pnd_Rcd_Cc;
    private DbsField pnd_Rcd_Pnd_Rcd_Yy;
    private DbsField pnd_Rcdn8;

    private DbsGroup pnd_Rcdn8__R_Field_2;
    private DbsField pnd_Rcdn8_Pnd_Rcdn8_Mm;
    private DbsField pnd_Rcdn8_Pnd_Rcdn8_Dd;
    private DbsField pnd_Rcdn8_Pnd_Rcdn8_Cc;
    private DbsField pnd_Rcdn8_Pnd_Rcdn8_Yy;
    private DbsField pnd_Unit_Total;
    private DbsField pnd_Grand_Total_Inst;
    private DbsField pnd_Grand_Total_Indv;
    private DbsField pnd_Break_Unit;
    private DbsField pnd_Ex_Dob;

    private DbsGroup pnd_Ex_Dob__R_Field_3;
    private DbsField pnd_Ex_Dob_Pnd_Ex_Dob_Mm;
    private DbsField pnd_Ex_Dob_Pnd_Ex_Dob_Dd;
    private DbsField pnd_Ex_Dob_Pnd_Ex_Dob_Cc;
    private DbsField pnd_Ex_Dob_Pnd_Ex_Dob_Yy;
    private DbsField pnd_Dob;

    private DbsGroup pnd_Dob__R_Field_4;
    private DbsField pnd_Dob_Pnd_Dob_Mm;
    private DbsField pnd_Dob_Pnd_Dob_Fil1;
    private DbsField pnd_Dob_Pnd_Dob_Dd;
    private DbsField pnd_Dob_Pnd_Dob_Fil2;
    private DbsField pnd_Dob_Pnd_Dob_Cc;
    private DbsField pnd_Dob_Pnd_Dob_Yy;
    private DbsField pnd_Dobn8;

    private DbsGroup pnd_Dobn8__R_Field_5;
    private DbsField pnd_Dobn8_Pnd_Dobn8_Mm;
    private DbsField pnd_Dobn8_Pnd_Dobn8_Dd;
    private DbsField pnd_Dobn8_Pnd_Dobn8_Cc;
    private DbsField pnd_Dobn8_Pnd_Dobn8_Yy;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_6;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Date_Pnd_Date_Cc;
    private DbsField pnd_Date_Pnd_Date_Yy;
    private DbsField pnd_Date_Yyyymmdd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_7;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Cc_1;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yy_1;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Mm_1;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Dd_1;
    private DbsField pnd_Name;
    private DbsField pnd_Address;
    private DbsField pnd_I;
    private DbsField pnd_L;
    private DbsField pnd_Isn;
    private DbsField pnd_Product;
    private DbsField pnd_Prap_Premium_Found;
    private DbsField pnd_City_State;
    private DbsField pnd_Filler_Lcl;

    private DbsGroup pnd_Filler_Lcl__R_Field_8;
    private DbsField pnd_Filler_Lcl_Pnd_App_Inet;
    private DbsField pnd_Filler_Lcl_Pnd_App_Ppg_Team;
    private DbsField pnd_Filler_Lcl_Pnd_App_Filler;
    private DbsField pnd_Y2_Ccyymmdd_Area1;

    private DbsGroup pnd_Y2_Ccyymmdd_Area1__R_Field_9;
    private DbsField pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Cc_Area1;
    private DbsField pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Yy_Area1;
    private DbsField pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Mm_Area1;
    private DbsField pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Dd_Area1;
    private DbsField pnd_Y2_Ccyymmdd_Area2;

    private DbsGroup pnd_Y2_Ccyymmdd_Area2__R_Field_10;
    private DbsField pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Cc_Area2;
    private DbsField pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Yy_Area2;
    private DbsField pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Mm_Area2;
    private DbsField pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Dd_Area2;
    private DbsField pnd_Ira_Unit;
    private DbsField pnd_Da_Unit;
    private DbsField pnd_Break_Lob;
    private DbsField pnd_Break_Lob_Type;
    private DbsField pnd_At_Start;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl220 = new LdaAppl220();
        registerRecord(ldaAppl220);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Rcd = localVariables.newFieldInRecord("pnd_Rcd", "#RCD", FieldType.STRING, 10);

        pnd_Rcd__R_Field_1 = localVariables.newGroupInRecord("pnd_Rcd__R_Field_1", "REDEFINE", pnd_Rcd);
        pnd_Rcd_Pnd_Rcd_Mm = pnd_Rcd__R_Field_1.newFieldInGroup("pnd_Rcd_Pnd_Rcd_Mm", "#RCD-MM", FieldType.NUMERIC, 2);
        pnd_Rcd_Pnd_Rcd_Fil1 = pnd_Rcd__R_Field_1.newFieldInGroup("pnd_Rcd_Pnd_Rcd_Fil1", "#RCD-FIL1", FieldType.STRING, 1);
        pnd_Rcd_Pnd_Rcd_Dd = pnd_Rcd__R_Field_1.newFieldInGroup("pnd_Rcd_Pnd_Rcd_Dd", "#RCD-DD", FieldType.NUMERIC, 2);
        pnd_Rcd_Pnd_Rcd_Fil2 = pnd_Rcd__R_Field_1.newFieldInGroup("pnd_Rcd_Pnd_Rcd_Fil2", "#RCD-FIL2", FieldType.STRING, 1);
        pnd_Rcd_Pnd_Rcd_Cc = pnd_Rcd__R_Field_1.newFieldInGroup("pnd_Rcd_Pnd_Rcd_Cc", "#RCD-CC", FieldType.NUMERIC, 2);
        pnd_Rcd_Pnd_Rcd_Yy = pnd_Rcd__R_Field_1.newFieldInGroup("pnd_Rcd_Pnd_Rcd_Yy", "#RCD-YY", FieldType.NUMERIC, 2);
        pnd_Rcdn8 = localVariables.newFieldInRecord("pnd_Rcdn8", "#RCDN8", FieldType.NUMERIC, 8);

        pnd_Rcdn8__R_Field_2 = localVariables.newGroupInRecord("pnd_Rcdn8__R_Field_2", "REDEFINE", pnd_Rcdn8);
        pnd_Rcdn8_Pnd_Rcdn8_Mm = pnd_Rcdn8__R_Field_2.newFieldInGroup("pnd_Rcdn8_Pnd_Rcdn8_Mm", "#RCDN8-MM", FieldType.NUMERIC, 2);
        pnd_Rcdn8_Pnd_Rcdn8_Dd = pnd_Rcdn8__R_Field_2.newFieldInGroup("pnd_Rcdn8_Pnd_Rcdn8_Dd", "#RCDN8-DD", FieldType.NUMERIC, 2);
        pnd_Rcdn8_Pnd_Rcdn8_Cc = pnd_Rcdn8__R_Field_2.newFieldInGroup("pnd_Rcdn8_Pnd_Rcdn8_Cc", "#RCDN8-CC", FieldType.NUMERIC, 2);
        pnd_Rcdn8_Pnd_Rcdn8_Yy = pnd_Rcdn8__R_Field_2.newFieldInGroup("pnd_Rcdn8_Pnd_Rcdn8_Yy", "#RCDN8-YY", FieldType.NUMERIC, 2);
        pnd_Unit_Total = localVariables.newFieldInRecord("pnd_Unit_Total", "#UNIT-TOTAL", FieldType.NUMERIC, 9);
        pnd_Grand_Total_Inst = localVariables.newFieldInRecord("pnd_Grand_Total_Inst", "#GRAND-TOTAL-INST", FieldType.NUMERIC, 9);
        pnd_Grand_Total_Indv = localVariables.newFieldInRecord("pnd_Grand_Total_Indv", "#GRAND-TOTAL-INDV", FieldType.NUMERIC, 9);
        pnd_Break_Unit = localVariables.newFieldInRecord("pnd_Break_Unit", "#BREAK-UNIT", FieldType.STRING, 10);
        pnd_Ex_Dob = localVariables.newFieldInRecord("pnd_Ex_Dob", "#EX-DOB", FieldType.NUMERIC, 8);

        pnd_Ex_Dob__R_Field_3 = localVariables.newGroupInRecord("pnd_Ex_Dob__R_Field_3", "REDEFINE", pnd_Ex_Dob);
        pnd_Ex_Dob_Pnd_Ex_Dob_Mm = pnd_Ex_Dob__R_Field_3.newFieldInGroup("pnd_Ex_Dob_Pnd_Ex_Dob_Mm", "#EX-DOB-MM", FieldType.NUMERIC, 2);
        pnd_Ex_Dob_Pnd_Ex_Dob_Dd = pnd_Ex_Dob__R_Field_3.newFieldInGroup("pnd_Ex_Dob_Pnd_Ex_Dob_Dd", "#EX-DOB-DD", FieldType.NUMERIC, 2);
        pnd_Ex_Dob_Pnd_Ex_Dob_Cc = pnd_Ex_Dob__R_Field_3.newFieldInGroup("pnd_Ex_Dob_Pnd_Ex_Dob_Cc", "#EX-DOB-CC", FieldType.NUMERIC, 2);
        pnd_Ex_Dob_Pnd_Ex_Dob_Yy = pnd_Ex_Dob__R_Field_3.newFieldInGroup("pnd_Ex_Dob_Pnd_Ex_Dob_Yy", "#EX-DOB-YY", FieldType.NUMERIC, 2);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 10);

        pnd_Dob__R_Field_4 = localVariables.newGroupInRecord("pnd_Dob__R_Field_4", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_Mm = pnd_Dob__R_Field_4.newFieldInGroup("pnd_Dob_Pnd_Dob_Mm", "#DOB-MM", FieldType.NUMERIC, 2);
        pnd_Dob_Pnd_Dob_Fil1 = pnd_Dob__R_Field_4.newFieldInGroup("pnd_Dob_Pnd_Dob_Fil1", "#DOB-FIL1", FieldType.STRING, 1);
        pnd_Dob_Pnd_Dob_Dd = pnd_Dob__R_Field_4.newFieldInGroup("pnd_Dob_Pnd_Dob_Dd", "#DOB-DD", FieldType.NUMERIC, 2);
        pnd_Dob_Pnd_Dob_Fil2 = pnd_Dob__R_Field_4.newFieldInGroup("pnd_Dob_Pnd_Dob_Fil2", "#DOB-FIL2", FieldType.STRING, 1);
        pnd_Dob_Pnd_Dob_Cc = pnd_Dob__R_Field_4.newFieldInGroup("pnd_Dob_Pnd_Dob_Cc", "#DOB-CC", FieldType.NUMERIC, 2);
        pnd_Dob_Pnd_Dob_Yy = pnd_Dob__R_Field_4.newFieldInGroup("pnd_Dob_Pnd_Dob_Yy", "#DOB-YY", FieldType.NUMERIC, 2);
        pnd_Dobn8 = localVariables.newFieldInRecord("pnd_Dobn8", "#DOBN8", FieldType.NUMERIC, 8);

        pnd_Dobn8__R_Field_5 = localVariables.newGroupInRecord("pnd_Dobn8__R_Field_5", "REDEFINE", pnd_Dobn8);
        pnd_Dobn8_Pnd_Dobn8_Mm = pnd_Dobn8__R_Field_5.newFieldInGroup("pnd_Dobn8_Pnd_Dobn8_Mm", "#DOBN8-MM", FieldType.NUMERIC, 2);
        pnd_Dobn8_Pnd_Dobn8_Dd = pnd_Dobn8__R_Field_5.newFieldInGroup("pnd_Dobn8_Pnd_Dobn8_Dd", "#DOBN8-DD", FieldType.NUMERIC, 2);
        pnd_Dobn8_Pnd_Dobn8_Cc = pnd_Dobn8__R_Field_5.newFieldInGroup("pnd_Dobn8_Pnd_Dobn8_Cc", "#DOBN8-CC", FieldType.NUMERIC, 2);
        pnd_Dobn8_Pnd_Dobn8_Yy = pnd_Dobn8__R_Field_5.newFieldInGroup("pnd_Dobn8_Pnd_Dobn8_Yy", "#DOBN8-YY", FieldType.NUMERIC, 2);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Date__R_Field_6", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Cc = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Yy = pnd_Date__R_Field_6.newFieldInGroup("pnd_Date_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 2);
        pnd_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Date_Yyyymmdd__R_Field_7 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_7", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Cc_1 = pnd_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Cc_1", "#DATE-CC-1", FieldType.NUMERIC, 
            2);
        pnd_Date_Yyyymmdd_Pnd_Date_Yy_1 = pnd_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yy_1", "#DATE-YY-1", FieldType.NUMERIC, 
            2);
        pnd_Date_Yyyymmdd_Pnd_Date_Mm_1 = pnd_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Mm_1", "#DATE-MM-1", FieldType.NUMERIC, 
            2);
        pnd_Date_Yyyymmdd_Pnd_Date_Dd_1 = pnd_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Dd_1", "#DATE-DD-1", FieldType.NUMERIC, 
            2);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 30);
        pnd_Address = localVariables.newFieldArrayInRecord("pnd_Address", "#ADDRESS", FieldType.STRING, 35, new DbsArrayController(1, 5));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 3);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.NUMERIC, 10);
        pnd_Product = localVariables.newFieldInRecord("pnd_Product", "#PRODUCT", FieldType.STRING, 10);
        pnd_Prap_Premium_Found = localVariables.newFieldInRecord("pnd_Prap_Premium_Found", "#PRAP-PREMIUM-FOUND", FieldType.BOOLEAN, 1);
        pnd_City_State = localVariables.newFieldInRecord("pnd_City_State", "#CITY-STATE", FieldType.STRING, 40);
        pnd_Filler_Lcl = localVariables.newFieldInRecord("pnd_Filler_Lcl", "#FILLER-LCL", FieldType.STRING, 167);

        pnd_Filler_Lcl__R_Field_8 = localVariables.newGroupInRecord("pnd_Filler_Lcl__R_Field_8", "REDEFINE", pnd_Filler_Lcl);
        pnd_Filler_Lcl_Pnd_App_Inet = pnd_Filler_Lcl__R_Field_8.newFieldInGroup("pnd_Filler_Lcl_Pnd_App_Inet", "#APP-INET", FieldType.STRING, 1);
        pnd_Filler_Lcl_Pnd_App_Ppg_Team = pnd_Filler_Lcl__R_Field_8.newFieldInGroup("pnd_Filler_Lcl_Pnd_App_Ppg_Team", "#APP-PPG-TEAM", FieldType.STRING, 
            8);
        pnd_Filler_Lcl_Pnd_App_Filler = pnd_Filler_Lcl__R_Field_8.newFieldInGroup("pnd_Filler_Lcl_Pnd_App_Filler", "#APP-FILLER", FieldType.STRING, 138);
        pnd_Y2_Ccyymmdd_Area1 = localVariables.newFieldInRecord("pnd_Y2_Ccyymmdd_Area1", "#Y2-CCYYMMDD-AREA1", FieldType.NUMERIC, 8);

        pnd_Y2_Ccyymmdd_Area1__R_Field_9 = localVariables.newGroupInRecord("pnd_Y2_Ccyymmdd_Area1__R_Field_9", "REDEFINE", pnd_Y2_Ccyymmdd_Area1);
        pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Cc_Area1 = pnd_Y2_Ccyymmdd_Area1__R_Field_9.newFieldInGroup("pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Cc_Area1", "#Y2-CC-AREA1", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Yy_Area1 = pnd_Y2_Ccyymmdd_Area1__R_Field_9.newFieldInGroup("pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Yy_Area1", "#Y2-YY-AREA1", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Mm_Area1 = pnd_Y2_Ccyymmdd_Area1__R_Field_9.newFieldInGroup("pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Mm_Area1", "#Y2-MM-AREA1", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Dd_Area1 = pnd_Y2_Ccyymmdd_Area1__R_Field_9.newFieldInGroup("pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Dd_Area1", "#Y2-DD-AREA1", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Ccyymmdd_Area2 = localVariables.newFieldInRecord("pnd_Y2_Ccyymmdd_Area2", "#Y2-CCYYMMDD-AREA2", FieldType.NUMERIC, 8);

        pnd_Y2_Ccyymmdd_Area2__R_Field_10 = localVariables.newGroupInRecord("pnd_Y2_Ccyymmdd_Area2__R_Field_10", "REDEFINE", pnd_Y2_Ccyymmdd_Area2);
        pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Cc_Area2 = pnd_Y2_Ccyymmdd_Area2__R_Field_10.newFieldInGroup("pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Cc_Area2", "#Y2-CC-AREA2", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Yy_Area2 = pnd_Y2_Ccyymmdd_Area2__R_Field_10.newFieldInGroup("pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Yy_Area2", "#Y2-YY-AREA2", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Mm_Area2 = pnd_Y2_Ccyymmdd_Area2__R_Field_10.newFieldInGroup("pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Mm_Area2", "#Y2-MM-AREA2", 
            FieldType.NUMERIC, 2);
        pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Dd_Area2 = pnd_Y2_Ccyymmdd_Area2__R_Field_10.newFieldInGroup("pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Dd_Area2", "#Y2-DD-AREA2", 
            FieldType.NUMERIC, 2);
        pnd_Ira_Unit = localVariables.newFieldInRecord("pnd_Ira_Unit", "#IRA-UNIT", FieldType.STRING, 10);
        pnd_Da_Unit = localVariables.newFieldInRecord("pnd_Da_Unit", "#DA-UNIT", FieldType.STRING, 10);
        pnd_Break_Lob = localVariables.newFieldInRecord("pnd_Break_Lob", "#BREAK-LOB", FieldType.STRING, 1);
        pnd_Break_Lob_Type = localVariables.newFieldInRecord("pnd_Break_Lob_Type", "#BREAK-LOB-TYPE", FieldType.STRING, 1);
        pnd_At_Start = localVariables.newFieldInRecord("pnd_At_Start", "#AT-START", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl220.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Acip9005() throws Exception
    {
        super("Acip9005");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON;//Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = ON;//Natural: READ WORK FILE 1 PRAP-EXTRACT-FILE.EX-DATA-1 PRAP-EXTRACT-FILE.EX-DATA-2 PRAP-EXTRACT-FILE.EX-DATA-3 PRAP-EXTRACT-FILE.EX-DATA-4 PRAP-EXTRACT-FILE.EX-DATA-5 PRAP-EXTRACT-FILE.EX-DATA-6 PRAP-EXTRACT-FILE.EX-DATA-7 PRAP-EXTRACT-FILE.EX-DATA-8 PRAP-EXTRACT-FILE.EX-DATA-9 PRAP-EXTRACT-FILE.EX-DATA-10 PRAP-EXTRACT-FILE.EX-DATA-11 PRAP-EXTRACT-FILE.EX-DATA-12 PRAP-EXTRACT-FILE.EX-DATA-13 PRAP-EXTRACT-FILE.EX-DATA-14 PRAP-EXTRACT-FILE.EX-DATA-15 PRAP-EXTRACT-FILE.EX-DATA-16
        while (condition(getWorkFiles().read(1, ldaAppl220.getPrap_Extract_File_Ex_Data_1(), ldaAppl220.getPrap_Extract_File_Ex_Data_2(), ldaAppl220.getPrap_Extract_File_Ex_Data_3(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_4(), ldaAppl220.getPrap_Extract_File_Ex_Data_5(), ldaAppl220.getPrap_Extract_File_Ex_Data_6(), ldaAppl220.getPrap_Extract_File_Ex_Data_7(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_8(), ldaAppl220.getPrap_Extract_File_Ex_Data_9(), ldaAppl220.getPrap_Extract_File_Ex_Data_10(), ldaAppl220.getPrap_Extract_File_Ex_Data_11(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_12(), ldaAppl220.getPrap_Extract_File_Ex_Data_13(), ldaAppl220.getPrap_Extract_File_Ex_Data_14(), ldaAppl220.getPrap_Extract_File_Ex_Data_15(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_16())))
        {
            //* *Y2NCMN
            pnd_Dob.reset();                                                                                                                                              //Natural: RESET #DOB #DOBN8 #RCD #RCDN8 #PRAP-PREMIUM-FOUND #FILLER-LCL
            pnd_Dobn8.reset();
            pnd_Rcd.reset();
            pnd_Rcdn8.reset();
            pnd_Prap_Premium_Found.reset();
            pnd_Filler_Lcl.reset();
            //*  #FILLER-LCL := PRAP-EXTRACT-FILE.EX-FILLER
            pnd_Filler_Lcl_Pnd_App_Ppg_Team.setValue(ldaAppl220.getPrap_Extract_File_Ex_Ppg_Team());                                                                      //Natural: ASSIGN #APP-PPG-TEAM := PRAP-EXTRACT-FILE.EX-PPG-TEAM
            if (condition(ldaAppl220.getPrap_Extract_File_Ex_Status().notEquals("C")))                                                                                    //Natural: REJECT IF PRAP-EXTRACT-FILE.EX-STATUS NE 'C'
            {
                continue;
            }
            //*  IOES VR
            //*  IOES VR
            if (condition(ldaAppl220.getPrap_Extract_File_Ex_Net_Ind().notEquals("I") && ldaAppl220.getPrap_Extract_File_Ex_Net_Ind().notEquals("A") &&                   //Natural: REJECT IF PRAP-EXTRACT-FILE.EX-NET-IND NE 'I' AND PRAP-EXTRACT-FILE.EX-NET-IND NE 'A' AND PRAP-EXTRACT-FILE.EX-NET-IND NE 'C'
                ldaAppl220.getPrap_Extract_File_Ex_Net_Ind().notEquals("C")))
            {
                continue;
            }
            if (condition(ldaAppl220.getPrap_Extract_File_Ex_Record_Type().notEquals(1)))                                                                                 //Natural: REJECT IF PRAP-EXTRACT-FILE.EX-RECORD-TYPE NE 1
            {
                continue;
            }
            //*  MOVE BY NAME ANNTY-ACTVTY-PRAP-REC TO
            //*    ANNTY-ACTVTY-PRAP-REC-LCL
            getSort().writeSortInData(pnd_Filler_Lcl_Pnd_App_Ppg_Team, ldaAppl220.getPrap_Extract_File_Ex_Lob(), ldaAppl220.getPrap_Extract_File_Ex_Lob_Type(),           //Natural: END-ALL
                ldaAppl220.getPrap_Extract_File_Ex_Data_1(), ldaAppl220.getPrap_Extract_File_Ex_Data_2(), ldaAppl220.getPrap_Extract_File_Ex_Data_3(), ldaAppl220.getPrap_Extract_File_Ex_Data_4(), 
                ldaAppl220.getPrap_Extract_File_Ex_Data_5(), ldaAppl220.getPrap_Extract_File_Ex_Data_6(), ldaAppl220.getPrap_Extract_File_Ex_Data_7(), ldaAppl220.getPrap_Extract_File_Ex_Data_8(), 
                ldaAppl220.getPrap_Extract_File_Ex_Data_9(), ldaAppl220.getPrap_Extract_File_Ex_Data_10(), ldaAppl220.getPrap_Extract_File_Ex_Data_11(), 
                ldaAppl220.getPrap_Extract_File_Ex_Data_12(), ldaAppl220.getPrap_Extract_File_Ex_Data_13(), ldaAppl220.getPrap_Extract_File_Ex_Data_14(), 
                ldaAppl220.getPrap_Extract_File_Ex_Data_15(), ldaAppl220.getPrap_Extract_File_Ex_Data_16());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Filler_Lcl_Pnd_App_Ppg_Team, ldaAppl220.getPrap_Extract_File_Ex_Lob(), ldaAppl220.getPrap_Extract_File_Ex_Lob_Type());                     //Natural: SORT #APP-PPG-TEAM PRAP-EXTRACT-FILE.EX-LOB PRAP-EXTRACT-FILE.EX-LOB-TYPE USING PRAP-EXTRACT-FILE.EX-DATA-1 PRAP-EXTRACT-FILE.EX-DATA-2 PRAP-EXTRACT-FILE.EX-DATA-3 PRAP-EXTRACT-FILE.EX-DATA-4 PRAP-EXTRACT-FILE.EX-DATA-5 PRAP-EXTRACT-FILE.EX-DATA-6 PRAP-EXTRACT-FILE.EX-DATA-7 PRAP-EXTRACT-FILE.EX-DATA-8 PRAP-EXTRACT-FILE.EX-DATA-9 PRAP-EXTRACT-FILE.EX-DATA-10 PRAP-EXTRACT-FILE.EX-DATA-11 PRAP-EXTRACT-FILE.EX-DATA-12 PRAP-EXTRACT-FILE.EX-DATA-13 PRAP-EXTRACT-FILE.EX-DATA-14 PRAP-EXTRACT-FILE.EX-DATA-15 PRAP-EXTRACT-FILE.EX-DATA-16
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Filler_Lcl_Pnd_App_Ppg_Team, ldaAppl220.getPrap_Extract_File_Ex_Lob(), ldaAppl220.getPrap_Extract_File_Ex_Lob_Type(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_1(), ldaAppl220.getPrap_Extract_File_Ex_Data_2(), ldaAppl220.getPrap_Extract_File_Ex_Data_3(), ldaAppl220.getPrap_Extract_File_Ex_Data_4(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_5(), ldaAppl220.getPrap_Extract_File_Ex_Data_6(), ldaAppl220.getPrap_Extract_File_Ex_Data_7(), ldaAppl220.getPrap_Extract_File_Ex_Data_8(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_9(), ldaAppl220.getPrap_Extract_File_Ex_Data_10(), ldaAppl220.getPrap_Extract_File_Ex_Data_11(), ldaAppl220.getPrap_Extract_File_Ex_Data_12(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_13(), ldaAppl220.getPrap_Extract_File_Ex_Data_14(), ldaAppl220.getPrap_Extract_File_Ex_Data_15(), ldaAppl220.getPrap_Extract_File_Ex_Data_16())))
        {
            CheckAtStartofData506();

            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 2 )
            //*                                                                                                                                                           //Natural: AT START OF DATA
            pnd_At_Start.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #AT-START
            if (condition(pnd_At_Start.equals(1)))                                                                                                                        //Natural: IF #AT-START EQ 1
            {
                pnd_Break_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                                 //Natural: ASSIGN #BREAK-UNIT := #APP-PPG-TEAM
                pnd_Break_Lob.setValue(ldaAppl220.getPrap_Extract_File_Ex_Lob());                                                                                         //Natural: ASSIGN #BREAK-LOB := PRAP-EXTRACT-FILE.EX-LOB
                pnd_Break_Lob_Type.setValue(ldaAppl220.getPrap_Extract_File_Ex_Lob_Type());                                                                               //Natural: ASSIGN #BREAK-LOB-TYPE := PRAP-EXTRACT-FILE.EX-LOB-TYPE
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I")))                                                                                      //Natural: IF PRAP-EXTRACT-FILE.EX-LOB EQ 'I'
                {
                    pnd_Ira_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                               //Natural: ASSIGN #IRA-UNIT := #APP-PPG-TEAM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().notEquals("I")))                                                                                   //Natural: IF PRAP-EXTRACT-FILE.EX-LOB NE 'I'
                {
                    pnd_Da_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                                //Natural: ASSIGN #DA-UNIT := #APP-PPG-TEAM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Break_Unit.notEquals(pnd_Filler_Lcl_Pnd_App_Ppg_Team)))                                                                                     //Natural: IF #BREAK-UNIT NE #APP-PPG-TEAM
            {
                                                                                                                                                                          //Natural: PERFORM UNIT-TOTAL
                sub_Unit_Total();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().notEquals("I")))                                                                                   //Natural: IF PRAP-EXTRACT-FILE.EX-LOB NE 'I'
                {
                    pnd_Da_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                                //Natural: ASSIGN #DA-UNIT := #APP-PPG-TEAM
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I")))                                                                                      //Natural: IF PRAP-EXTRACT-FILE.EX-LOB EQ 'I'
                {
                    pnd_Ira_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                               //Natural: ASSIGN #IRA-UNIT := #APP-PPG-TEAM
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Break_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                                 //Natural: ASSIGN #BREAK-UNIT := #APP-PPG-TEAM
                pnd_Break_Lob.setValue(ldaAppl220.getPrap_Extract_File_Ex_Lob());                                                                                         //Natural: ASSIGN #BREAK-LOB := PRAP-EXTRACT-FILE.EX-LOB
                pnd_Break_Lob_Type.setValue(ldaAppl220.getPrap_Extract_File_Ex_Lob_Type());                                                                               //Natural: ASSIGN #BREAK-LOB-TYPE := PRAP-EXTRACT-FILE.EX-LOB-TYPE
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I")))                                                                                      //Natural: IF PRAP-EXTRACT-FILE.EX-LOB EQ 'I'
                {
                    pnd_Ira_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                               //Natural: ASSIGN #IRA-UNIT := #APP-PPG-TEAM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().notEquals("I")))                                                                                   //Natural: IF PRAP-EXTRACT-FILE.EX-LOB NE 'I'
                {
                    pnd_Da_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                                //Natural: ASSIGN #DA-UNIT := #APP-PPG-TEAM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Break_Unit.notEquals(pnd_Filler_Lcl_Pnd_App_Ppg_Team)))                                                                                     //Natural: IF #BREAK-UNIT NE #APP-PPG-TEAM
            {
                                                                                                                                                                          //Natural: PERFORM UNIT-TOTAL
                sub_Unit_Total();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("D")))                                                                                      //Natural: IF PRAP-EXTRACT-FILE.EX-LOB EQ 'D'
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I")))                                                                                      //Natural: IF PRAP-EXTRACT-FILE.EX-LOB EQ 'I'
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Break_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                                 //Natural: ASSIGN #BREAK-UNIT := #APP-PPG-TEAM
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //* *Y2NCMN N6
            pnd_Ex_Dob.setValue(ldaAppl220.getPrap_Extract_File_Ex_Date_Birth());                                                                                         //Natural: ASSIGN #EX-DOB := PRAP-EXTRACT-FILE.EX-DATE-BIRTH
            //* *Y2NCMN
            pnd_Date_Yyyymmdd.setValue(Global.getDATN());                                                                                                                 //Natural: ASSIGN #DATE-YYYYMMDD := *DATN
            pnd_Dob_Pnd_Dob_Mm.setValue(pnd_Ex_Dob_Pnd_Ex_Dob_Mm);                                                                                                        //Natural: ASSIGN #DOB-MM := #EX-DOB-MM
            pnd_Dob_Pnd_Dob_Dd.setValue(pnd_Ex_Dob_Pnd_Ex_Dob_Dd);                                                                                                        //Natural: ASSIGN #DOB-DD := #EX-DOB-DD
            //* *********************************
            //* * YEAR 2000 FIX START          **
            //* *********************************
            //* *Y2CHMN  REMEDIATION IS BELOW
            //* *#DOB-CC := #DATE-CC-1
            //* *********************************
            //* * YEAR 2000 FIX END            **
            //* *********************************
            //* *Y2NCMN
            pnd_Dob_Pnd_Dob_Yy.setValue(pnd_Ex_Dob_Pnd_Ex_Dob_Yy);                                                                                                        //Natural: ASSIGN #DOB-YY := #EX-DOB-YY
            pnd_Dob_Pnd_Dob_Fil1.setValue("/");                                                                                                                           //Natural: ASSIGN #DOB-FIL1 := '/'
            pnd_Dob_Pnd_Dob_Fil2.setValue("/");                                                                                                                           //Natural: ASSIGN #DOB-FIL2 := '/'
            //* *Y2NCMN N6
            pnd_Rcdn8.setValue(ldaAppl220.getPrap_Extract_File_Ex_Dt_Matched());                                                                                          //Natural: ASSIGN #RCDN8 := PRAP-EXTRACT-FILE.EX-DT-MATCHED
            pnd_Rcd_Pnd_Rcd_Mm.setValue(pnd_Rcdn8_Pnd_Rcdn8_Mm);                                                                                                          //Natural: ASSIGN #RCD-MM := #RCDN8-MM
            pnd_Rcd_Pnd_Rcd_Dd.setValue(pnd_Rcdn8_Pnd_Rcdn8_Dd);                                                                                                          //Natural: ASSIGN #RCD-DD := #RCDN8-DD
            //* *********************************
            //* * YEAR 2000 FIX START          **
            //* *********************************
            //* *Y2CHMN
            //* *#RCD-CC := #DATE-CC-1
            pnd_Y2_Ccyymmdd_Area1.reset();                                                                                                                                //Natural: RESET #Y2-CCYYMMDD-AREA1 #Y2-CCYYMMDD-AREA2
            pnd_Y2_Ccyymmdd_Area2.reset();
            pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Mm_Area1.setValue(1);                                                                                                            //Natural: ASSIGN #Y2-MM-AREA1 := 01
            pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Mm_Area2.setValue(1);                                                                                                            //Natural: ASSIGN #Y2-MM-AREA2 := 01
            pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Dd_Area1.setValue(1);                                                                                                            //Natural: ASSIGN #Y2-DD-AREA1 := 01
            pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Dd_Area2.setValue(1);                                                                                                            //Natural: ASSIGN #Y2-DD-AREA2 := 01
            pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Yy_Area1.setValue(pnd_Ex_Dob_Pnd_Ex_Dob_Yy);                                                                                     //Natural: ASSIGN #Y2-YY-AREA1 := #EX-DOB-YY
            pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Yy_Area2.setValue(pnd_Rcdn8_Pnd_Rcdn8_Yy);                                                                                       //Natural: ASSIGN #Y2-YY-AREA2 := #RCDN8-YY
            DbsUtil.callnat(Y2dat1bn.class , getCurrentProcessState(), pnd_Y2_Ccyymmdd_Area1, pnd_Y2_Ccyymmdd_Area2);                                                     //Natural: CALLNAT 'Y2DAT1BN' #Y2-CCYYMMDD-AREA1 #Y2-CCYYMMDD-AREA2
            if (condition(Global.isEscape())) return;
            pnd_Dob_Pnd_Dob_Cc.setValue(pnd_Y2_Ccyymmdd_Area1_Pnd_Y2_Cc_Area1);                                                                                           //Natural: ASSIGN #DOB-CC := #Y2-CC-AREA1
            pnd_Rcd_Pnd_Rcd_Cc.setValue(pnd_Y2_Ccyymmdd_Area2_Pnd_Y2_Cc_Area2);                                                                                           //Natural: ASSIGN #RCD-CC := #Y2-CC-AREA2
            //* *********************************
            //* * YEAR 2000 FIX END            **
            //* *********************************
            //* *Y2NCMN
            pnd_Rcd_Pnd_Rcd_Yy.setValue(pnd_Rcdn8_Pnd_Rcdn8_Yy);                                                                                                          //Natural: ASSIGN #RCD-YY := #RCDN8-YY
            pnd_Dob_Pnd_Dob_Fil1.setValue("/");                                                                                                                           //Natural: ASSIGN #DOB-FIL1 := '/'
            pnd_Dob_Pnd_Dob_Fil2.setValue("/");                                                                                                                           //Natural: ASSIGN #DOB-FIL2 := '/'
            pnd_Rcd_Pnd_Rcd_Fil1.setValue("/");                                                                                                                           //Natural: ASSIGN #RCD-FIL1 := '/'
            pnd_Rcd_Pnd_Rcd_Fil2.setValue("/");                                                                                                                           //Natural: ASSIGN #RCD-FIL2 := '/'
            pnd_Unit_Total.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #UNIT-TOTAL
            if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().notEquals("I")))                                                                                       //Natural: IF PRAP-EXTRACT-FILE.EX-LOB NE 'I'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-INSTITUTIONAL
                sub_Write_Report_Institutional();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I")))                                                                                          //Natural: IF PRAP-EXTRACT-FILE.EX-LOB EQ 'I'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-IRA
                sub_Write_Report_Ira();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAtEndOfData()))
        {
            if (condition(pnd_Unit_Total.greater(getZero())))                                                                                                             //Natural: IF #UNIT-TOTAL GT 0
            {
                                                                                                                                                                          //Natural: PERFORM UNIT-TOTAL
                sub_Unit_Total();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,"Grand Total =======>",pnd_Grand_Total_Inst);                                                                      //Natural: WRITE ( 1 ) 'Grand Total =======>' #GRAND-TOTAL-INST
            if (condition(Global.isEscape())) return;
            getReports().write(2, ReportOption.NOTITLE,"Grand Total =======>",pnd_Grand_Total_Indv);                                                                      //Natural: WRITE ( 2 ) 'Grand Total =======>' #GRAND-TOTAL-INDV
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(40),"************ END OF REPORT ************");                                                     //Natural: WRITE ( 1 ) 40T '************ END OF REPORT ************'
            if (condition(Global.isEscape())) return;
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(40),"************ END OF REPORT ************");                                                     //Natural: WRITE ( 2 ) 40T '************ END OF REPORT ************'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        //* *------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-INSTITUTIONAL
        //* *Y2NCMN
        //* *------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-IRA
        //* *Y2NCMN
        //* *------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PRODUCT
        //* *------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UNIT-TOTAL
    }
    private void sub_Write_Report_Institutional() throws Exception                                                                                                        //Natural: WRITE-REPORT-INSTITUTIONAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        pnd_Name.reset();                                                                                                                                                 //Natural: RESET #NAME #ADDRESS ( * ) #I #L
        pnd_Address.getValue("*").reset();
        pnd_I.reset();
        pnd_L.reset();
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(1).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 1 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(1),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 1 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(2).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 2 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(2),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 2 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(3).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 3 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(3),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 3 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(4).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 4 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(4),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 4 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(5).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 5 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(5),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 5 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Name.setValue(DbsUtil.compress(ldaAppl220.getPrap_Extract_File_Ex_Cor_Prefix(), " ", ldaAppl220.getPrap_Extract_File_Ex_Cor_Last_Name(), " ",                 //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-COR-PREFIX ' ' PRAP-EXTRACT-FILE.EX-COR-LAST-NAME ' ' PRAP-EXTRACT-FILE.EX-COR-FIRST-NAME ' ' PRAP-EXTRACT-FILE.EX-COR-MDDLE-NAME ' ' PRAP-EXTRACT-FILE.EX-COR-SUFFIX INTO #NAME
            ldaAppl220.getPrap_Extract_File_Ex_Cor_First_Name(), " ", ldaAppl220.getPrap_Extract_File_Ex_Cor_Mddle_Name(), " ", ldaAppl220.getPrap_Extract_File_Ex_Cor_Suffix()));
        pnd_Product.reset();                                                                                                                                              //Natural: RESET #PRODUCT
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PRODUCT
        sub_Determine_Product();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Product.equals(" ")))                                                                                                                           //Natural: IF #PRODUCT EQ ' '
        {
            pnd_Product.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Lob(), ldaAppl220.getPrap_Extract_File_Ex_Lob_Type())); //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-LOB PRAP-EXTRACT-FILE.EX-LOB-TYPE INTO #PRODUCT LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Name,new TabSetting(33),ldaAppl220.getPrap_Extract_File_Ex_Soc_Sec(), new ReportEditMask         //Natural: WRITE ( 1 ) 1T #NAME 33T PRAP-EXTRACT-FILE.EX-SOC-SEC ( EM = 999999999 ) 44T #DOB 56T #PRODUCT 1X #ADDRESS ( 1 ) 3X #RCD / 67T #ADDRESS ( 2 )
            ("999999999"),new TabSetting(44),pnd_Dob,new TabSetting(56),pnd_Product,new ColumnSpacing(1),pnd_Address.getValue(1),new ColumnSpacing(3),pnd_Rcd,NEWLINE,new 
            TabSetting(67),pnd_Address.getValue(2));
        if (Global.isEscape()) return;
        if (condition(pnd_Address.getValue(3).notEquals(" ")))                                                                                                            //Natural: IF #ADDRESS ( 3 ) NE ' '
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(67),pnd_Address.getValue(3));                                                                       //Natural: WRITE ( 1 ) 67T #ADDRESS ( 3 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Report_Ira() throws Exception                                                                                                                  //Natural: WRITE-REPORT-IRA
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        pnd_Name.reset();                                                                                                                                                 //Natural: RESET #NAME #ADDRESS ( * ) #I #L
        pnd_Address.getValue("*").reset();
        pnd_I.reset();
        pnd_L.reset();
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(1).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 1 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(1),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 1 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(2).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 2 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(2),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 2 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(3).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 3 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(3),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 3 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(4).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 4 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(4),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 4 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(5).notEquals(" ")))                                                                     //Natural: IF PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 5 ) NE ' '
        {
            pnd_L.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #L
            pnd_Address.getValue(pnd_L).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(5),          //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-ADDRESS-FIELD ( 5 ) ' ' INTO #ADDRESS ( #L ) LEAVING NO SPACE
                " "));
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Name.setValue(DbsUtil.compress(ldaAppl220.getPrap_Extract_File_Ex_Cor_Prefix(), " ", ldaAppl220.getPrap_Extract_File_Ex_Cor_Last_Name(), " ",                 //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-COR-PREFIX ' ' PRAP-EXTRACT-FILE.EX-COR-LAST-NAME ' ' PRAP-EXTRACT-FILE.EX-COR-FIRST-NAME ' ' PRAP-EXTRACT-FILE.EX-COR-MDDLE-NAME ' ' PRAP-EXTRACT-FILE.EX-COR-SUFFIX INTO #NAME
            ldaAppl220.getPrap_Extract_File_Ex_Cor_First_Name(), " ", ldaAppl220.getPrap_Extract_File_Ex_Cor_Mddle_Name(), " ", ldaAppl220.getPrap_Extract_File_Ex_Cor_Suffix()));
        pnd_Product.reset();                                                                                                                                              //Natural: RESET #PRODUCT
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PRODUCT
        sub_Determine_Product();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Product.equals(" ")))                                                                                                                           //Natural: IF #PRODUCT EQ ' '
        {
            pnd_Product.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl220.getPrap_Extract_File_Ex_Lob(), ldaAppl220.getPrap_Extract_File_Ex_Lob_Type())); //Natural: COMPRESS PRAP-EXTRACT-FILE.EX-LOB PRAP-EXTRACT-FILE.EX-LOB-TYPE INTO #PRODUCT LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Name,new TabSetting(33),ldaAppl220.getPrap_Extract_File_Ex_Soc_Sec(), new ReportEditMask         //Natural: WRITE ( 2 ) 1T #NAME 33T PRAP-EXTRACT-FILE.EX-SOC-SEC ( EM = 999999999 ) 44T #DOB 56T #PRODUCT 1X #ADDRESS ( 1 ) 3X #RCD / 67T #ADDRESS ( 2 )
            ("999999999"),new TabSetting(44),pnd_Dob,new TabSetting(56),pnd_Product,new ColumnSpacing(1),pnd_Address.getValue(1),new ColumnSpacing(3),pnd_Rcd,NEWLINE,new 
            TabSetting(67),pnd_Address.getValue(2));
        if (Global.isEscape()) return;
        if (condition(pnd_Address.getValue(3).notEquals(" ")))                                                                                                            //Natural: IF #ADDRESS ( 3 ) NE ' '
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(67),pnd_Address.getValue(3));                                                                       //Natural: WRITE ( 1 ) 67T #ADDRESS ( 3 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Product() throws Exception                                                                                                                 //Natural: DETERMINE-PRODUCT
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        //*  457(B) JR
        //*  457(B) JR
        //*  457(B) JR
        //*  IRA/REL6.
        //*  IRA/REL6.
        //*  IRA/REL6.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL4.
        //*  TNT/REL5.
        //*  TNT/REL5.
        //*  TNT/REL5.
        //*  TNT/REL5.
        //*  TNT/REL5.
        //*  TNT/REL5.
        //*  DCA KG
        //*  DCA KG
        //*  DCA KG
        //*  RHSP NBC
        //*  RHSP NBC
        //*  RHSP NBC
        //*  TIGR-START
        //*  ROTH INDEXED IRA
        //*  TRAD INDEXED IRA
        //*  SEP INDEXED IRA - TIGR-END
        short decideConditionsMet757 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'D' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '2'
        if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("D") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("2")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("RA");                                                                                                                                   //Natural: ASSIGN #PRODUCT := 'RA'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'D' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '7'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("D") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("7")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("GRA");                                                                                                                                  //Natural: ASSIGN #PRODUCT := 'GRA'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'S' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '3'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("3")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("GSRA");                                                                                                                                 //Natural: ASSIGN #PRODUCT := 'GSRA'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'S' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '4'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("4")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("GA");                                                                                                                                   //Natural: ASSIGN #PRODUCT := 'GA'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'S' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '2'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("2")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("SRA");                                                                                                                                  //Natural: ASSIGN #PRODUCT := 'SRA'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'I' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '3'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("3")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("IRAROTH");                                                                                                                              //Natural: ASSIGN #PRODUCT := 'IRAROTH'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'I' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '4'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("4")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("IRACLAS");                                                                                                                              //Natural: ASSIGN #PRODUCT := 'IRACLAS'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'I' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '5'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("5")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("KEOGH");                                                                                                                                //Natural: ASSIGN #PRODUCT := 'KEOGH'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'I' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '6'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("6")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("IRA SEP");                                                                                                                              //Natural: ASSIGN #PRODUCT := 'IRA SEP'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'D' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '5'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("D") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("5")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("RS");                                                                                                                                   //Natural: ASSIGN #PRODUCT := 'RS'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'S' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '5'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("5")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("RSP");                                                                                                                                  //Natural: ASSIGN #PRODUCT := 'RSP'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'S' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '6'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("6")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("RSP2");                                                                                                                                 //Natural: ASSIGN #PRODUCT := 'RSP2'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'D' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ 'A'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("D") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("A")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("RC");                                                                                                                                   //Natural: ASSIGN #PRODUCT := 'RC'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'S' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '7'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("7")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("RCP");                                                                                                                                  //Natural: ASSIGN #PRODUCT := 'RCP'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'S' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '9'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("9")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("TGA");                                                                                                                                  //Natural: ASSIGN #PRODUCT := 'TGA'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'D' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ 'B'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("D") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("B")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("RHSP");                                                                                                                                 //Natural: ASSIGN #PRODUCT := 'RHSP'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'I' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '7'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("7")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("IRIR");                                                                                                                                 //Natural: ASSIGN #PRODUCT := 'IRIR'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'I' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '8'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("8")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("IRIC");                                                                                                                                 //Natural: ASSIGN #PRODUCT := 'IRIC'
        }                                                                                                                                                                 //Natural: WHEN PRAP-EXTRACT-FILE.EX-LOB EQ 'I' AND PRAP-EXTRACT-FILE.EX-LOB-TYPE EQ '9'
        else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I") && ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("9")))
        {
            decideConditionsMet757++;
            pnd_Product.setValue("IRIS");                                                                                                                                 //Natural: ASSIGN #PRODUCT := 'IRIS'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Unit_Total() throws Exception                                                                                                                        //Natural: UNIT-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        if (condition(pnd_Break_Lob.notEquals("I")))                                                                                                                      //Natural: IF #BREAK-LOB NE 'I'
        {
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"Total for Unit ",pnd_Break_Unit,"====>",pnd_Unit_Total);                                                          //Natural: WRITE ( 1 ) 'Total for Unit ' #BREAK-UNIT '====>' #UNIT-TOTAL
            if (Global.isEscape()) return;
            pnd_Grand_Total_Inst.nadd(pnd_Unit_Total);                                                                                                                    //Natural: ADD #UNIT-TOTAL TO #GRAND-TOTAL-INST
            pnd_Unit_Total.reset();                                                                                                                                       //Natural: RESET #UNIT-TOTAL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Break_Lob.equals("I")))                                                                                                                         //Natural: IF #BREAK-LOB EQ 'I'
        {
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"Total for Unit ",pnd_Break_Unit,"====>",pnd_Unit_Total);                                                          //Natural: WRITE ( 2 ) 'Total for Unit ' #BREAK-UNIT '====>' #UNIT-TOTAL
            if (Global.isEscape()) return;
            pnd_Grand_Total_Indv.nadd(pnd_Unit_Total);                                                                                                                    //Natural: ADD #UNIT-TOTAL TO #GRAND-TOTAL-INDV
            pnd_Unit_Total.reset();                                                                                                                                       //Natural: RESET #UNIT-TOTAL
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *Y2NCMN
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(120),Global.getDATU(),NEWLINE,NEWLINE,new TabSetting(42),"Internet Application Matched & Released",NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 120T *DATU // 42T 'Internet Application Matched & Released' / 38T 'Institutional Products Detail Report' / 1T #APP-PPG-TEAM // 1T 'Name' 38T 'SSN' 44T 'Birth Date' 56T 'Product' 75T 'Address' 105T 'Date Matched' / 1T '-' ( 30 ) 33T '-' ( 10 ) 44T '-' ( 10 ) 56T '-' ( 10 ) 67T '-' ( 35 ) 105T '-' ( 13 ) /
                        TabSetting(38),"Institutional Products Detail Report",NEWLINE,new TabSetting(1),pnd_Filler_Lcl_Pnd_App_Ppg_Team,NEWLINE,NEWLINE,new 
                        TabSetting(1),"Name",new TabSetting(38),"SSN",new TabSetting(44),"Birth Date",new TabSetting(56),"Product",new TabSetting(75),"Address",new 
                        TabSetting(105),"Date Matched",NEWLINE,new TabSetting(1),"-",new RepeatItem(30),new TabSetting(33),"-",new RepeatItem(10),new TabSetting(44),"-",new 
                        RepeatItem(10),new TabSetting(56),"-",new RepeatItem(10),new TabSetting(67),"-",new RepeatItem(35),new TabSetting(105),"-",new RepeatItem(13),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *Y2NCMN
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(120),Global.getDATU(),NEWLINE,NEWLINE,new TabSetting(42),"Internet Application Matched & Released",NEWLINE,new  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 120T *DATU // 42T 'Internet Application Matched & Released' / 38T 'Individual Products Detail Report' / 1T #APP-PPG-TEAM // 1T 'Name' 38T 'SSN' 44T 'Birth Date' 56T 'Product' 75T 'Address' 105T 'Date Matched' / 1T '-' ( 30 ) 33T '-' ( 10 ) 44T '-' ( 10 ) 56T '-' ( 10 ) 67T '-' ( 35 ) 105T '-' ( 13 ) /
                        TabSetting(38),"Individual Products Detail Report",NEWLINE,new TabSetting(1),pnd_Filler_Lcl_Pnd_App_Ppg_Team,NEWLINE,NEWLINE,new 
                        TabSetting(1),"Name",new TabSetting(38),"SSN",new TabSetting(44),"Birth Date",new TabSetting(56),"Product",new TabSetting(75),"Address",new 
                        TabSetting(105),"Date Matched",NEWLINE,new TabSetting(1),"-",new RepeatItem(30),new TabSetting(33),"-",new RepeatItem(10),new TabSetting(44),"-",new 
                        RepeatItem(10),new TabSetting(56),"-",new RepeatItem(10),new TabSetting(67),"-",new RepeatItem(35),new TabSetting(105),"-",new RepeatItem(13),
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60 ZP=ON");
        Global.format(2, "LS=133 PS=60 ZP=ON");
    }
    private void CheckAtStartofData506() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            pnd_Break_Unit.setValue(pnd_Filler_Lcl_Pnd_App_Ppg_Team);                                                                                                     //Natural: ASSIGN #BREAK-UNIT := #APP-PPG-TEAM
        }                                                                                                                                                                 //Natural: END-START
    }
}
