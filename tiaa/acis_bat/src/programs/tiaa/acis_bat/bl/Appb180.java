/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:37 PM
**        * FROM NATURAL PROGRAM : Appb180
************************************************************
**        * FILE NAME            : Appb180.java
**        * CLASS NAME           : Appb180
**        * INSTANCE NAME        : Appb180
************************************************************
************************************************************************
* PROGRAM  : APPB180 - ACIS/MDM EXTRACT PROCESS
* FUNCTION : READS DATA FROM OMNI FILE FOR ACIS AND EXTRACT DATA TO
*            FILE TO LOAD ON MDM
*
* CREATED  : 10/29/09 N. CVETKOVIC/C. MASON
* HISTORY:
* 12/15/09   CORRECTED SEPARATE OF FIRST-MIDDLE NAME FIELD
* 12/22/09   RESET #NEG-ENROLL INDICATOR FOR EVERY HEADER RECORD. SOME
*            PARTICIPANTS DO NOT HAVE IT SET CORRECTLY WHEN NO REC IS
*            FOUND THAT HAS #DET-TRAILER-ID = '$PTDE1 637'. SEE - GGG.
* 09/27/10   ADD PREFIX ($NOTE  140) TO MDM FEED.  SEE - DG
* 10/19/10   VALIDATE PREFIX ($NOTE  140).             - DG
* 10/22/10   ADD 'SIR', 'SIR.' TO PREFIX VALID VALUES  - DG
* 04/12/11   EXTRACT ADDRESS INFORMATION FROM THE OMNI FILES - F.ALLIE
* 05/01/11   ADD EDIT TO CHECK IF T801 IS A CHANGE & TO DISALLOW.
*            SEE - GGG0511
* 05/20/11   REMOVED TEMPORARY PREFIX EDITS - VALIDATE_PREFIX SUBROUTINE
*            ADDED CHECK TO VERIFY PREFIX IS NOT BLANK BEFORE SENDING
*            SEE - GGG052011
* 05/21/2014 ADDED VALIDATION CHECKS TO SSN,NAME,DOB,ADDRESS LINE1
*            SEE - 05/21/2014
* 09/01/15   ADD SSN/TIN INDICATOR IN THE MDM INTERFACE FILE.
*            INCLUDE NEGATIVE ENROLLMENT TO THE EXTRACT FILE TO MDM.
*              (MDM WILL PASS DATA BACK FOR PINS THAT ALREADY EXISTS)
*            BYPASS RECORDS IN FOLDERS ON ADMIN. HOLD.
*            RENAMED INPUT RECORD AS NOW NEED TO CHECK '02' FOLDER
*            HEADER RECORDS AS WELL AS T801 RECORDS.
*            FOLDER ADMIN HOLD INDICATOR  PER JOE PELLEGRINI:
*            '00204' HEADER WITH 'Y' IN BYTE 60.
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
* 04/04/19  B.NEWSOM  BREAKING ACIS DEPENDENCIES OF THE BENE LEGACY
*                     SYSTEM                             (BADOTBLS)
************************************************************************
* OMNI FILE DETAILS PROVIDED BY PARIJAT
*
* SSN = WHEN COLS 1-5 = 80100, SSN IN COLS 22-30
* LAST,FIRST,MIDDLE,SUFFIX COLS 9 - 18 = $PTEN1 011
*    WHOLE NAME IN COLS 19 THRU 49 IN FORMAT LAST SUFFIX, FIRST MIDDLE
* DATE_OF_BIRTH = WHEN COLS 1-3 = 801, COLS 9-18 = $PTEN1 050,
*    DOB IN COLS 19-26 IN FORMAT CCYYMMDD
* FOREIGN SSN = NOT NEEDED
* SEX_CODE = WHEN COLS 1-3 = 801, COLS 9-18 = $PTEN1 030, SEX CODE
*    IN COLS 19 , 1=MALE, 2=FEMALE
* DATE_OF_DEATH = WHEN COLS 1-3 = 801, COLS 9-18 = $PTDE1 051,
*    DOD IN COLS 19-26 IN FORMAT CCYYMMDD
* NEGATIVE ENROLLMENTS WHEN COLS 1-3 = 801, COLS 9-18 = $PTDE1 637
*    COL 19 = N
********************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb180 extends BLNatBase
{
    // Data Areas
    private LdaAppbl180 ldaAppbl180;
    private PdaScia1103 pdaScia1103;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Write_Count;
    private DbsField pnd_Folder_On_Hold;
    private DbsField pnd_Input_Rec;

    private DbsGroup pnd_Input_Rec__R_Field_1;
    private DbsField pnd_Input_Rec_Pnd_Tran_Code;
    private DbsField pnd_Input_Rec_Pnd_Seq_Code;
    private DbsField pnd_Input_Rec_Pnd_Filler1;
    private DbsField pnd_Input_Rec_Pnd_Function;
    private DbsField pnd_Input_Rec_Pnd_Filler2;
    private DbsField pnd_Input_Rec_Pnd_Folder_Admin_Hold_Ind;

    private DbsGroup pnd_Input_Rec__R_Field_2;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Tran_Code;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Seq_Code;
    private DbsField pnd_Input_Rec_Pnd_Notused1;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Rec_Type;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Plan_No;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Part_Id;

    private DbsGroup pnd_Input_Rec__R_Field_3;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Ssn;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Subplan_No;
    private DbsField pnd_Input_Rec_Pnd_Subplan_Ext;
    private DbsField pnd_Input_Rec_Pnd_Notused2;
    private DbsField pnd_Input_Rec_Pnd_Hdr_Trade_Date;

    private DbsGroup pnd_Input_Rec__R_Field_4;
    private DbsField pnd_Input_Rec_Pnd_Det_Tran_Code;
    private DbsField pnd_Input_Rec_Pnd_Det_Seq_Code;
    private DbsField pnd_Input_Rec_Pnd_Det_Filler1;
    private DbsField pnd_Input_Rec_Pnd_Det_Trailer_Id;
    private DbsField pnd_Input_Rec_Pnd_Det_Value;

    private DbsGroup pnd_Input_Rec__R_Field_5;
    private DbsField pnd_Input_Rec_Pnd_Det_Val_Fil1;
    private DbsField pnd_Input_Rec_Pnd_Det_Val_Admin_Hold;
    private DbsField pnd_Input_Rec_Pnd_Det_Val_Fil2;
    private DbsField pnd_Chg_Function;
    private DbsField pnd_Hold_Ssn;
    private DbsField pnd_Last_Suffix;
    private DbsField pnd_First_Middle;
    private DbsField pnd_Skipped;
    private DbsField pnd_Folder_Skipped;
    private DbsField pnd_Neg_Enroll;
    private DbsField pnd_Neg_Enrollment_Cnt;
    private DbsField pnd_Total_Count;
    private DbsField pnd_Total_T801_Recs;
    private DbsField pnd_Blank;
    private DbsField pnd_I;
    private DbsField pnd_Pos;
    private DbsField pnd_Len;
    private DbsField pnd_Error_Ssn;
    private DbsField pnd_Buffer;
    private DbsField pnd_Last4_Ssn;
    private DbsField pnd_Skip_Rec;
    private DbsField pnd_P;
    private DbsField pnd_Valid_Characters;
    private DbsField pnd_Valid_Characters_First;
    private DbsField pnd_Last_Name;

    private DbsGroup pnd_Last_Name__R_Field_6;
    private DbsField pnd_Last_Name_Pnd_Name_L;
    private DbsField pnd_First_Name;

    private DbsGroup pnd_First_Name__R_Field_7;
    private DbsField pnd_First_Name_Pnd_Name_F;
    private DbsField pnd_Middle_Name;

    private DbsGroup pnd_Middle_Name__R_Field_8;
    private DbsField pnd_Middle_Name_Pnd_Name_M;
    private DbsField pnd_Dob;

    private DbsGroup pnd_Dob__R_Field_9;
    private DbsField pnd_Dob_Pnd_Dob_N8;
    private DbsField pnd_Compare_Dob;
    private DbsField pnd_Dot;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppbl180 = new LdaAppbl180();
        registerRecord(ldaAppbl180);
        localVariables = new DbsRecord();
        pdaScia1103 = new PdaScia1103(localVariables);

        // Local Variables
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Folder_On_Hold = localVariables.newFieldInRecord("pnd_Folder_On_Hold", "#FOLDER-ON-HOLD", FieldType.BOOLEAN, 1);
        pnd_Input_Rec = localVariables.newFieldInRecord("pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 80);

        pnd_Input_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Rec__R_Field_1", "REDEFINE", pnd_Input_Rec);
        pnd_Input_Rec_Pnd_Tran_Code = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Tran_Code", "#TRAN-CODE", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Seq_Code = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Seq_Code", "#SEQ-CODE", FieldType.STRING, 2);
        pnd_Input_Rec_Pnd_Filler1 = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Function = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Function", "#FUNCTION", FieldType.STRING, 15);
        pnd_Input_Rec_Pnd_Filler2 = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Filler2", "#FILLER2", FieldType.STRING, 36);
        pnd_Input_Rec_Pnd_Folder_Admin_Hold_Ind = pnd_Input_Rec__R_Field_1.newFieldInGroup("pnd_Input_Rec_Pnd_Folder_Admin_Hold_Ind", "#FOLDER-ADMIN-HOLD-IND", 
            FieldType.STRING, 1);

        pnd_Input_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Rec__R_Field_2", "REDEFINE", pnd_Input_Rec);
        pnd_Input_Rec_Pnd_Hdr_Tran_Code = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Tran_Code", "#HDR-TRAN-CODE", FieldType.STRING, 
            3);
        pnd_Input_Rec_Pnd_Hdr_Seq_Code = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Seq_Code", "#HDR-SEQ-CODE", FieldType.STRING, 
            2);
        pnd_Input_Rec_Pnd_Notused1 = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Notused1", "#NOTUSED1", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Hdr_Rec_Type = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Rec_Type", "#HDR-REC-TYPE", FieldType.STRING, 
            7);
        pnd_Input_Rec_Pnd_Hdr_Plan_No = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Plan_No", "#HDR-PLAN-NO", FieldType.STRING, 6);
        pnd_Input_Rec_Pnd_Hdr_Part_Id = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Part_Id", "#HDR-PART-ID", FieldType.STRING, 17);

        pnd_Input_Rec__R_Field_3 = pnd_Input_Rec__R_Field_2.newGroupInGroup("pnd_Input_Rec__R_Field_3", "REDEFINE", pnd_Input_Rec_Pnd_Hdr_Part_Id);
        pnd_Input_Rec_Pnd_Hdr_Ssn = pnd_Input_Rec__R_Field_3.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Ssn", "#HDR-SSN", FieldType.STRING, 9);
        pnd_Input_Rec_Pnd_Hdr_Subplan_No = pnd_Input_Rec__R_Field_3.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Subplan_No", "#HDR-SUBPLAN-NO", FieldType.STRING, 
            6);
        pnd_Input_Rec_Pnd_Subplan_Ext = pnd_Input_Rec__R_Field_3.newFieldInGroup("pnd_Input_Rec_Pnd_Subplan_Ext", "#SUBPLAN-EXT", FieldType.STRING, 2);
        pnd_Input_Rec_Pnd_Notused2 = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Notused2", "#NOTUSED2", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Hdr_Trade_Date = pnd_Input_Rec__R_Field_2.newFieldInGroup("pnd_Input_Rec_Pnd_Hdr_Trade_Date", "#HDR-TRADE-DATE", FieldType.NUMERIC, 
            8);

        pnd_Input_Rec__R_Field_4 = localVariables.newGroupInRecord("pnd_Input_Rec__R_Field_4", "REDEFINE", pnd_Input_Rec);
        pnd_Input_Rec_Pnd_Det_Tran_Code = pnd_Input_Rec__R_Field_4.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Tran_Code", "#DET-TRAN-CODE", FieldType.STRING, 
            3);
        pnd_Input_Rec_Pnd_Det_Seq_Code = pnd_Input_Rec__R_Field_4.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Seq_Code", "#DET-SEQ-CODE", FieldType.STRING, 
            2);
        pnd_Input_Rec_Pnd_Det_Filler1 = pnd_Input_Rec__R_Field_4.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Filler1", "#DET-FILLER1", FieldType.STRING, 3);
        pnd_Input_Rec_Pnd_Det_Trailer_Id = pnd_Input_Rec__R_Field_4.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Trailer_Id", "#DET-TRAILER-ID", FieldType.STRING, 
            10);
        pnd_Input_Rec_Pnd_Det_Value = pnd_Input_Rec__R_Field_4.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Value", "#DET-VALUE", FieldType.STRING, 40);

        pnd_Input_Rec__R_Field_5 = pnd_Input_Rec__R_Field_4.newGroupInGroup("pnd_Input_Rec__R_Field_5", "REDEFINE", pnd_Input_Rec_Pnd_Det_Value);
        pnd_Input_Rec_Pnd_Det_Val_Fil1 = pnd_Input_Rec__R_Field_5.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Val_Fil1", "#DET-VAL-FIL1", FieldType.STRING, 
            36);
        pnd_Input_Rec_Pnd_Det_Val_Admin_Hold = pnd_Input_Rec__R_Field_5.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Val_Admin_Hold", "#DET-VAL-ADMIN-HOLD", 
            FieldType.STRING, 1);
        pnd_Input_Rec_Pnd_Det_Val_Fil2 = pnd_Input_Rec__R_Field_5.newFieldInGroup("pnd_Input_Rec_Pnd_Det_Val_Fil2", "#DET-VAL-FIL2", FieldType.STRING, 
            3);
        pnd_Chg_Function = localVariables.newFieldInRecord("pnd_Chg_Function", "#CHG-FUNCTION", FieldType.BOOLEAN, 1);
        pnd_Hold_Ssn = localVariables.newFieldInRecord("pnd_Hold_Ssn", "#HOLD-SSN", FieldType.STRING, 9);
        pnd_Last_Suffix = localVariables.newFieldInRecord("pnd_Last_Suffix", "#LAST-SUFFIX", FieldType.STRING, 44);
        pnd_First_Middle = localVariables.newFieldInRecord("pnd_First_Middle", "#FIRST-MIDDLE", FieldType.STRING, 70);
        pnd_Skipped = localVariables.newFieldInRecord("pnd_Skipped", "#SKIPPED", FieldType.NUMERIC, 7);
        pnd_Folder_Skipped = localVariables.newFieldInRecord("pnd_Folder_Skipped", "#FOLDER-SKIPPED", FieldType.NUMERIC, 7);
        pnd_Neg_Enroll = localVariables.newFieldInRecord("pnd_Neg_Enroll", "#NEG-ENROLL", FieldType.BOOLEAN, 1);
        pnd_Neg_Enrollment_Cnt = localVariables.newFieldInRecord("pnd_Neg_Enrollment_Cnt", "#NEG-ENROLLMENT-CNT", FieldType.NUMERIC, 7);
        pnd_Total_Count = localVariables.newFieldInRecord("pnd_Total_Count", "#TOTAL-COUNT", FieldType.NUMERIC, 9);
        pnd_Total_T801_Recs = localVariables.newFieldInRecord("pnd_Total_T801_Recs", "#TOTAL-T801-RECS", FieldType.NUMERIC, 9);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Pos = localVariables.newFieldInRecord("pnd_Pos", "#POS", FieldType.NUMERIC, 3);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.NUMERIC, 3);
        pnd_Error_Ssn = localVariables.newFieldInRecord("pnd_Error_Ssn", "#ERROR-SSN", FieldType.STRING, 9);
        pnd_Buffer = localVariables.newFieldInRecord("pnd_Buffer", "#BUFFER", FieldType.STRING, 80);
        pnd_Last4_Ssn = localVariables.newFieldInRecord("pnd_Last4_Ssn", "#LAST4-SSN", FieldType.STRING, 14);
        pnd_Skip_Rec = localVariables.newFieldInRecord("pnd_Skip_Rec", "#SKIP-REC", FieldType.BOOLEAN, 1);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 2);
        pnd_Valid_Characters = localVariables.newFieldInRecord("pnd_Valid_Characters", "#VALID-CHARACTERS", FieldType.STRING, 56);
        pnd_Valid_Characters_First = localVariables.newFieldInRecord("pnd_Valid_Characters_First", "#VALID-CHARACTERS-FIRST", FieldType.STRING, 52);
        pnd_Last_Name = localVariables.newFieldInRecord("pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 35);

        pnd_Last_Name__R_Field_6 = localVariables.newGroupInRecord("pnd_Last_Name__R_Field_6", "REDEFINE", pnd_Last_Name);
        pnd_Last_Name_Pnd_Name_L = pnd_Last_Name__R_Field_6.newFieldArrayInGroup("pnd_Last_Name_Pnd_Name_L", "#NAME-L", FieldType.STRING, 1, new DbsArrayController(1, 
            30));
        pnd_First_Name = localVariables.newFieldInRecord("pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);

        pnd_First_Name__R_Field_7 = localVariables.newGroupInRecord("pnd_First_Name__R_Field_7", "REDEFINE", pnd_First_Name);
        pnd_First_Name_Pnd_Name_F = pnd_First_Name__R_Field_7.newFieldArrayInGroup("pnd_First_Name_Pnd_Name_F", "#NAME-F", FieldType.STRING, 1, new DbsArrayController(1, 
            30));
        pnd_Middle_Name = localVariables.newFieldInRecord("pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 30);

        pnd_Middle_Name__R_Field_8 = localVariables.newGroupInRecord("pnd_Middle_Name__R_Field_8", "REDEFINE", pnd_Middle_Name);
        pnd_Middle_Name_Pnd_Name_M = pnd_Middle_Name__R_Field_8.newFieldArrayInGroup("pnd_Middle_Name_Pnd_Name_M", "#NAME-M", FieldType.STRING, 1, new 
            DbsArrayController(1, 30));
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 8);

        pnd_Dob__R_Field_9 = localVariables.newGroupInRecord("pnd_Dob__R_Field_9", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_N8 = pnd_Dob__R_Field_9.newFieldInGroup("pnd_Dob_Pnd_Dob_N8", "#DOB-N8", FieldType.NUMERIC, 8);
        pnd_Compare_Dob = localVariables.newFieldInRecord("pnd_Compare_Dob", "#COMPARE-DOB", FieldType.STRING, 8);
        pnd_Dot = localVariables.newFieldInRecord("pnd_Dot", "#DOT", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppbl180.initializeValues();

        localVariables.reset();
        pnd_Chg_Function.setInitialValue(false);
        pnd_Skipped.setInitialValue(0);
        pnd_Folder_Skipped.setInitialValue(0);
        pnd_Valid_Characters.setInitialValue("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz- ,'");
        pnd_Valid_Characters_First.setInitialValue("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
        pnd_Dot.setInitialValue(".");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb180() throws Exception
    {
        super("Appb180");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB180", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 132 PS = 60;//Natural: FORMAT ( 1 ) LS = 132 PS = 60
        //*  09/24/14 BEGIN
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-REC
        while (condition(getWorkFiles().read(1, pnd_Input_Rec)))
        {
            pnd_Total_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-COUNT
            if (condition(pnd_Input_Rec_Pnd_Tran_Code.equals("002")))                                                                                                     //Natural: IF #TRAN-CODE = '002'
            {
                //*  RESET FOLDER HOLD
                //*  AT FIRST NEW FOLDER
                if (condition(pnd_Input_Rec_Pnd_Seq_Code.equals("00")))                                                                                                   //Natural: IF #SEQ-CODE = '00'
                {
                    pnd_Folder_On_Hold.setValue(false);                                                                                                                   //Natural: ASSIGN #FOLDER-ON-HOLD := FALSE
                    //*  RECORD. IF THERE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IS AN 04 HEADER,
                    //*  CHECK FOR FOLDER-
                    //*  LEVEL HOLD IND.
                    if (condition(pnd_Input_Rec_Pnd_Seq_Code.equals("04") && pnd_Input_Rec_Pnd_Folder_Admin_Hold_Ind.equals("Y")))                                        //Natural: IF #SEQ-CODE = '04' AND #FOLDER-ADMIN-HOLD-IND = 'Y'
                    {
                        pnd_Folder_On_Hold.setValue(true);                                                                                                                //Natural: ASSIGN #FOLDER-ON-HOLD := TRUE
                        pnd_Folder_Skipped.nadd(1);                                                                                                                       //Natural: ADD 1 TO #FOLDER-SKIPPED
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Rec_Pnd_Tran_Code.equals("801")))                                                                                                     //Natural: IF #TRAN-CODE = '801'
            {
                if (condition(pnd_Folder_On_Hold.getBoolean()))                                                                                                           //Natural: IF #FOLDER-ON-HOLD
                {
                    pnd_Skipped.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #SKIPPED
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  09/24/14 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Total_T801_Recs.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-T801-RECS
                    //*  '00' = HEADER RECORD
                    if (condition(pnd_Input_Rec_Pnd_Seq_Code.equals("00")))                                                                                               //Natural: IF #SEQ-CODE EQ '00'
                    {
                        //*  STARIIC
                        if (condition(pnd_Total_T801_Recs.greater(1)))                                                                                                    //Natural: IF #TOTAL-T801-RECS > 1
                        {
                            //*          IF NOT (#NEG-ENROLL) AND        /* SKIP NEG ENROLL /* CNR
                            //*  GGG0511
                            if (condition(! (pnd_Chg_Function.getBoolean())))                                                                                             //Natural: IF NOT ( #CHG-FUNCTION )
                            {
                                //*  WRITE TO EXTRACT FILE
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT-RECORD
                                sub_Write_Extract_Record();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  GGG0511
                        //*  BIP
                        //*  GGG0511
                        ldaAppbl180.getPnd_Mdm_Data().reset();                                                                                                            //Natural: RESET #MDM-DATA #HOLD-SSN
                        pnd_Hold_Ssn.reset();
                        ldaAppbl180.getPnd_Mdm_Data_Ph_Unique_Id_Nbr().setValue("000000000000");                                                                          //Natural: ASSIGN #MDM-DATA.PH_UNIQUE_ID_NBR := '000000000000'
                        ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().setValue(pnd_Input_Rec_Pnd_Hdr_Ssn);                                                          //Natural: ASSIGN #MDM-DATA.PH_SOCIAL_SECURITY_NO := #HDR-SSN
                        ldaAppbl180.getPnd_Mdm_Data_Plan_Number().setValue(pnd_Input_Rec_Pnd_Hdr_Plan_No);                                                                //Natural: ASSIGN #MDM-DATA.PLAN_NUMBER := #HDR-PLAN-NO
                        ldaAppbl180.getPnd_Mdm_Data_Subplan_Number().setValue(pnd_Input_Rec_Pnd_Hdr_Subplan_No);                                                          //Natural: ASSIGN #MDM-DATA.SUBPLAN_NUMBER := #HDR-SUBPLAN-NO
                        pnd_Hold_Ssn.setValue(pnd_Input_Rec_Pnd_Hdr_Ssn);                                                                                                 //Natural: ASSIGN #HOLD-SSN := #HDR-SSN
                        //*        #NEG-ENROLL := FALSE
                        pnd_Chg_Function.setValue(false);                                                                                                                 //Natural: ASSIGN #CHG-FUNCTION := FALSE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*      IF #DET-TRAILER-ID = '$VTD1     ' AND       /* 09/24/14
                    //*          #DET-VAL-ADMIN-HOLD = 'Y'               /* WOULD INDICATE
                    //*        ADD 1 TO #SKIPPED                         /* INDIVIDUAL
                    //*        ESCAPE TOP                                /* TRANS ON HOLD
                    //*      END-IF                                      /* 09/24/14 END
                    //*  GGG0511
                    if (condition(pnd_Input_Rec_Pnd_Seq_Code.equals("01") && DbsUtil.maskMatches(pnd_Input_Rec_Pnd_Function,"'CHG'")))                                    //Natural: IF #SEQ-CODE EQ '01' AND #FUNCTION = MASK ( 'CHG' )
                    {
                        //*    �
                        pnd_Chg_Function.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #CHG-FUNCTION
                        //*  GGG0511
                    }                                                                                                                                                     //Natural: END-IF
                    //*  GET NAME
                    short decideConditionsMet271 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #DET-TRAILER-ID;//Natural: VALUE '$PTEN1 011'
                    if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 011"))))
                    {
                        decideConditionsMet271++;
                        //*  GET DOB
                        //*  GET SEX CODE
                                                                                                                                                                          //Natural: PERFORM NAME-SPLITTER
                        sub_Name_Splitter();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 050'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 050"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Date_Of_Birth().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,8));                                              //Natural: ASSIGN #MDM-DATA.DATE_OF_BIRTH := SUBSTRING ( #DET-VALUE,1,8 )
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 030'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 030"))))
                    {
                        decideConditionsMet271++;
                        //*  '1' = MALE
                        if (condition(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,1).equals("1")))                                                                         //Natural: IF SUBSTRING ( #DET-VALUE,1,1 ) EQ '1'
                        {
                            ldaAppbl180.getPnd_Mdm_Data_Sex_Code().setValue("M");                                                                                         //Natural: MOVE 'M' TO #MDM-DATA.SEX_CODE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  '2' = FEMALE
                        if (condition(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,1).equals("2")))                                                                         //Natural: IF SUBSTRING ( #DET-VALUE,1,1 ) EQ '2'
                        {
                            ldaAppbl180.getPnd_Mdm_Data_Sex_Code().setValue("F");                                                                                         //Natural: MOVE 'F' TO #MDM-DATA.SEX_CODE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  ' ' = UNKNOWN
                        if (condition(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,1).equals(" ")))                                                                         //Natural: IF SUBSTRING ( #DET-VALUE,1,1 ) EQ ' '
                        {
                            ldaAppbl180.getPnd_Mdm_Data_Sex_Code().setValue("U");                                                                                         //Natural: MOVE 'U' TO #MDM-DATA.SEX_CODE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  STARIIC START
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 289'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 289"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Address_Line_1().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,40));                                            //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_1 := SUBSTRING ( #DET-VALUE,1,40 )
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 290'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 290"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Address_Line_2().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,40));                                            //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_2 := SUBSTRING ( #DET-VALUE,1,40 )
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 291'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 291"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Address_Line_3().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,40));                                            //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_3 := SUBSTRING ( #DET-VALUE,1,40 )
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 293'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 293"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_City().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,28));                                                      //Natural: ASSIGN #MDM-DATA.CITY := SUBSTRING ( #DET-VALUE,1,28 )
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 294'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 294"))))
                    {
                        decideConditionsMet271++;
                        if (condition(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,2).equals("FN") || pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,2).equals("FC")))          //Natural: IF SUBSTRING ( #DET-VALUE,1,2 ) = 'FN' OR = 'FC'
                        {
                            ldaAppbl180.getPnd_Mdm_Data_Addr_Type().setValue("F");                                                                                        //Natural: ASSIGN #MDM-DATA.ADDR_TYPE := 'F'
                            ldaAppbl180.getPnd_Mdm_Data_Country().setValue(ldaAppbl180.getPnd_Mdm_Data_City());                                                           //Natural: ASSIGN #MDM-DATA.COUNTRY := #MDM-DATA.CITY
                            ldaAppbl180.getPnd_Mdm_Data_City().reset();                                                                                                   //Natural: RESET #MDM-DATA.CITY #MDM-DATA.STATE #MDM-DATA.ZIP_CODE
                            ldaAppbl180.getPnd_Mdm_Data_State().reset();
                            ldaAppbl180.getPnd_Mdm_Data_Zip_Code().reset();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,2).equals("CN")))                                                                    //Natural: IF SUBSTRING ( #DET-VALUE,1,2 ) = 'CN'
                            {
                                ldaAppbl180.getPnd_Mdm_Data_Addr_Type().setValue("C");                                                                                    //Natural: ASSIGN #MDM-DATA.ADDR_TYPE := 'C'
                                ldaAppbl180.getPnd_Mdm_Data_Country().setValue("CANADA");                                                                                 //Natural: ASSIGN #MDM-DATA.COUNTRY := 'CANADA'
                                ldaAppbl180.getPnd_Mdm_Data_City().reset();                                                                                               //Natural: RESET #MDM-DATA.CITY #MDM-DATA.STATE #MDM-DATA.ZIP_CODE
                                ldaAppbl180.getPnd_Mdm_Data_State().reset();
                                ldaAppbl180.getPnd_Mdm_Data_Zip_Code().reset();
                                if (condition(ldaAppbl180.getPnd_Mdm_Data_Address_Line_3().equals("CANADA")))                                                             //Natural: IF #MDM-DATA.ADDRESS_LINE_3 = 'CANADA'
                                {
                                    ldaAppbl180.getPnd_Mdm_Data_Address_Line_3().reset();                                                                                 //Natural: RESET #MDM-DATA.ADDRESS_LINE_3
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaAppbl180.getPnd_Mdm_Data_Addr_Type().setValue("U");                                                                                    //Natural: ASSIGN #MDM-DATA.ADDR_TYPE := 'U'
                                ldaAppbl180.getPnd_Mdm_Data_Country().setValue("US");                                                                                     //Natural: ASSIGN #MDM-DATA.COUNTRY := 'US'
                                ldaAppbl180.getPnd_Mdm_Data_State().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,3));                                              //Natural: ASSIGN #MDM-DATA.STATE := SUBSTRING ( #DET-VALUE,1,3 )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE '$PTEN1 295'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTEN1 295"))))
                    {
                        decideConditionsMet271++;
                        if (condition(! (ldaAppbl180.getPnd_Mdm_Data_Addr_Type().equals("F") || ldaAppbl180.getPnd_Mdm_Data_Addr_Type().equals("C"))))                    //Natural: IF NOT ( #MDM-DATA.ADDR_TYPE = 'F' OR = 'C' )
                        {
                            ldaAppbl180.getPnd_Mdm_Data_Zip_Code().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,9));                                               //Natural: ASSIGN #MDM-DATA.ZIP_CODE := SUBSTRING ( #DET-VALUE,1,9 )
                        }                                                                                                                                                 //Natural: END-IF
                        //*  STARIIC END
                    }                                                                                                                                                     //Natural: VALUE '$PTDE1 051'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTDE1 051"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Date_Of_Death().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,8));                                              //Natural: ASSIGN #MDM-DATA.DATE_OF_DEATH := SUBSTRING ( #DET-VALUE,1,8 )
                    }                                                                                                                                                     //Natural: VALUE '$PTDE1 637'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTDE1 637"))))
                    {
                        decideConditionsMet271++;
                        //*  'N' = NEG ENROLL
                        if (condition(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,1).equals("N")))                                                                         //Natural: IF SUBSTRING ( #DET-VALUE,1,1 ) EQ 'N'
                        {
                            //*            #NEG-ENROLL := TRUE                              /* CNR
                            pnd_Neg_Enrollment_Cnt.nadd(1);                                                                                                               //Natural: ADD 1 TO #NEG-ENROLLMENT-CNT
                            //*            WRITE #HOLD-SSN 'NEGATIVE ENROLLMENT'            /* CNR
                            //*            RESET #MDM-DATA #HOLD-SSN                        /* CNR
                            //*            ESCAPE TOP          /* READ NEXT REC             /* CNR
                            //*          ELSE                                               /* CNR
                            //*            #NEG-ENROLL := FALSE                             /* CNR
                            //*  BADOTBLS - ORIG-TIAA-NUMBER
                            //*  BADOTBLS
                            //*  BADOTBLS - ORIG-CREF-NUMBER
                            //*  BADOTBLS
                            //*  BIP - SSN-TIN INDICATOR
                            //*  BIP
                            //*  DG
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE '$PTDE1 662'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTDE1 662"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Orig_Tiaa_Number().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,10));                                          //Natural: ASSIGN ORIG_TIAA_NUMBER := SUBSTRING ( #DET-VALUE,1,10 )
                    }                                                                                                                                                     //Natural: VALUE '$PTDE1 663'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTDE1 663"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Orig_Cref_Number().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,10));                                          //Natural: ASSIGN ORIG_CREF_NUMBER := SUBSTRING ( #DET-VALUE,1,10 )
                    }                                                                                                                                                     //Natural: VALUE '$PTDE1 782'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$PTDE1 782"))))
                    {
                        decideConditionsMet271++;
                        ldaAppbl180.getPnd_Mdm_Data_Ssn_Tin_Indicator().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(1,1));                                          //Natural: ASSIGN SSN_TIN_INDICATOR := SUBSTRING ( #DET-VALUE,1,1 )
                        //*        NOTE: PE140 (OMNI) = 5 BYTES
                    }                                                                                                                                                     //Natural: VALUE '$NOTE  140'
                    else if (condition((pnd_Input_Rec_Pnd_Det_Trailer_Id.equals("$NOTE  140"))))
                    {
                        decideConditionsMet271++;
                        //*  GGG052011
                        //*      �
                        if (condition(!pnd_Input_Rec_Pnd_Det_Value.getSubstring(2,5).equals(" ")))                                                                        //Natural: IF SUBSTRING ( #DET-VALUE,2,5 ) NE ' '
                        {
                            ldaAppbl180.getPnd_Mdm_Data_Prefix().setValue(pnd_Input_Rec_Pnd_Det_Value.getSubstring(2,5));                                                 //Natural: ASSIGN #MDM-DATA.PREFIX := SUBSTRING ( #DET-VALUE,2,5 )
                            //*      �
                        }                                                                                                                                                 //Natural: END-IF
                        //*          PERFORM VALIDATE_PREFIX (NOT EXECUTED)
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //*  09/24/14
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Total_T801_Recs.greater(1)))                                                                                                                    //Natural: IF #TOTAL-T801-RECS > 1
        {
            //*  IF NOT (#NEG-ENROLL) AND                              /* CNR
            if (condition(! (pnd_Chg_Function.getBoolean())))                                                                                                             //Natural: IF NOT ( #CHG-FUNCTION )
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT-RECORD
                sub_Write_Extract_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/24/14 - ADDED DISPLAY FOR SKIPPED FOLDERS, DETAIL RECORDS
        getReports().write(0, "TOTAL FOLDERS SKIPPED FOR ADMIN HOLD  ",pnd_Folder_Skipped);                                                                               //Natural: WRITE 'TOTAL FOLDERS SKIPPED FOR ADMIN HOLD  ' #FOLDER-SKIPPED
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL T801 RECS SKIPPED FOR ADMIN HOLD",pnd_Skipped);                                                                                      //Natural: WRITE 'TOTAL T801 RECS SKIPPED FOR ADMIN HOLD' #SKIPPED
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL NEG ENROLLMENTS                 ",pnd_Neg_Enrollment_Cnt);                                                                           //Natural: WRITE 'TOTAL NEG ENROLLMENTS                 ' #NEG-ENROLLMENT-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "SUCCESSFUL EOJ",pnd_Write_Count,"Records written to file");                                                                                //Natural: WRITE 'SUCCESSFUL EOJ' #WRITE-COUNT 'Records written to file'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL T801 RECS PROCESSED           ",pnd_Total_T801_Recs);                                                                                //Natural: WRITE 'TOTAL T801 RECS PROCESSED           ' #TOTAL-T801-RECS
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL RECORD READ                   ",pnd_Total_Count);                                                                                    //Natural: WRITE 'TOTAL RECORD READ                   ' #TOTAL-COUNT
        if (Global.isEscape()) return;
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-SPLITTER
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-SUFFIX
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT-RECORD
        //* *********************************
        //*    WHEN #MDM-DATA.MIDDLE_NAME EQ ' '
        //*      WRITE 'Middle name is blank:' #MDM-DATA.MIDDLE_NAME
        //*      MOVE TRUE TO #SKIP-REC
        //* *************************************
        //*  JRB2
        //* **************************************                                                                                                                        //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-MDM-DATA
        //* **************************************
        //*  / '=' PH_SOCIAL_SECURITY_NO
        //*  / '=' DATE_OF_BIRTH
        //*  / '=' FOREIGN_SOC_SEC_NBR
        //* *********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE_PREFIX
        //* *============
    }
    private void sub_Name_Splitter() throws Exception                                                                                                                     //Natural: NAME-SPLITTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  INPUT RECORD FORMAT LAST SUFFIX, FIRST MIDDLE
        //*  REMOVE LEADING BLANKS
        pnd_Input_Rec_Pnd_Det_Value.setValue(pnd_Input_Rec_Pnd_Det_Value, MoveOption.LeftJustified);                                                                      //Natural: MOVE LEFT JUSTIFIED #DET-VALUE TO #DET-VALUE
        //*  EXAMINE #DET-VALUE FOR ',' GIVING POSITION #POS
        pnd_Input_Rec_Pnd_Det_Value.separate(pnd_First_Middle, null, EnumSet.of(SeparateOption.LeftJustified,SeparateOption.WithAnyDelimiters), ",", pnd_Last_Suffix);    //Natural: SEPARATE #DET-VALUE LEFT INTO #LAST-SUFFIX REMAINDER #FIRST-MIDDLE WITH DELIMITER ','
        //*  CHECK FOR SUFFIX
        //*  SUBTRACT 1 FROM #POS    /* ',' REMOVED DURING SEPARATE
        DbsUtil.examine(new ExamineSource(pnd_Last_Suffix,true), new ExamineSearch("*"), new ExamineGivingLength(pnd_Pos));                                               //Natural: EXAMINE FULL #LAST-SUFFIX '*' GIVING LENGTH #POS
        FOR01:                                                                                                                                                            //Natural: FOR #BLANK = #POS TO 1 STEP -1
        for (pnd_Blank.setValue(pnd_Pos); condition(pnd_Blank.greaterOrEqual(1)); pnd_Blank.nsubtract(1))
        {
            //*  LENGTH OF STRING
            //*  FIRST CHAR AFTER BLANK
            if (condition(pnd_Last_Suffix.getSubstring(pnd_Blank.getInt(),1).equals(" ")))                                                                                //Natural: IF SUBSTRING ( #LAST-SUFFIX,#BLANK,1 ) EQ ' '
            {
                pnd_Len.compute(new ComputeParameters(false, pnd_Len), pnd_Pos.subtract(pnd_Blank));                                                                      //Natural: ASSIGN #LEN := #POS - #BLANK
                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Blank.add(1));                                                                                     //Natural: ASSIGN #I := #BLANK + 1
                ldaAppbl180.getPnd_Mdm_Data_Suffix().setValue(pnd_Last_Suffix.getSubstring(pnd_I.getInt(),pnd_Len.getInt()));                                             //Natural: MOVE SUBSTRING ( #LAST-SUFFIX,#I,#LEN ) TO SUFFIX
                //*  VALIDATE SUFFIX
                                                                                                                                                                          //Natural: PERFORM CHECK-SUFFIX
                sub_Check_Suffix();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaAppbl180.getPnd_Mdm_Data_Suffix().notEquals(" ")))                                                                                               //Natural: IF SUFFIX NE ' '
        {
            ldaAppbl180.getPnd_Mdm_Data_Last_Name().setValue(pnd_Last_Suffix.getSubstring(1,pnd_Blank.getInt()));                                                         //Natural: MOVE SUBSTRING ( #LAST-SUFFIX,1,#BLANK ) TO LAST_NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppbl180.getPnd_Mdm_Data_Last_Name().setValue(pnd_Last_Suffix);                                                                                            //Natural: MOVE #LAST-SUFFIX TO LAST_NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK FOR BLANKS IN FIRST NAME                      /* CM 12/15/09
        pnd_Blank.reset();                                                                                                                                                //Natural: RESET #BLANK #POS #LEN #I
        pnd_Pos.reset();
        pnd_Len.reset();
        pnd_I.reset();
        DbsUtil.examine(new ExamineSource(pnd_First_Middle,true), new ExamineSearch("*"), new ExamineGivingLength(pnd_Pos));                                              //Natural: EXAMINE FULL #FIRST-MIDDLE '*' GIVING LENGTH #POS
        FOR02:                                                                                                                                                            //Natural: FOR #BLANK = #POS TO 1 STEP -1
        for (pnd_Blank.setValue(pnd_Pos); condition(pnd_Blank.greaterOrEqual(1)); pnd_Blank.nsubtract(1))
        {
            //*  START AT FIRST CHAR OF SUBSTRING
            if (condition(pnd_First_Middle.getSubstring(pnd_Blank.getInt(),1).equals(" ")))                                                                               //Natural: IF SUBSTRING ( #FIRST-MIDDLE,#BLANK,1 ) EQ ' '
            {
                pnd_Len.compute(new ComputeParameters(false, pnd_Len), pnd_Pos.subtract(pnd_Blank));                                                                      //Natural: ASSIGN #LEN := #POS - #BLANK
                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Blank.add(1));                                                                                     //Natural: ASSIGN #I := #BLANK + 1
                ldaAppbl180.getPnd_Mdm_Data_Middle_Name().setValue(pnd_First_Middle.getSubstring(pnd_I.getInt(),pnd_Len.getInt()));                                       //Natural: MOVE SUBSTRING ( #FIRST-MIDDLE,#I,#LEN ) TO MIDDLE_NAME
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaAppbl180.getPnd_Mdm_Data_Middle_Name().notEquals(" ")))                                                                                          //Natural: IF MIDDLE_NAME NE ' '
        {
            ldaAppbl180.getPnd_Mdm_Data_First_Name().setValue(pnd_First_Middle.getSubstring(1,pnd_Blank.getInt()));                                                       //Natural: MOVE SUBSTRING ( #FIRST-MIDDLE,1,#BLANK ) TO FIRST_NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppbl180.getPnd_Mdm_Data_First_Name().setValue(pnd_First_Middle);                                                                                          //Natural: MOVE #FIRST-MIDDLE TO FIRST_NAME
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Suffix() throws Exception                                                                                                                      //Natural: CHECK-SUFFIX
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(ldaAppbl180.getPnd_Mdm_Data_Suffix().notEquals(" ")))                                                                                               //Natural: IF SUFFIX NE ' '
        {
            pdaScia1103.getScia1103().reset();                                                                                                                            //Natural: RESET SCIA1103
            pdaScia1103.getScia1103_Pnd_Entry_Actve_Ind_48().setValue("Y");                                                                                               //Natural: ASSIGN #ENTRY-ACTVE-IND-48 := 'Y'
            pdaScia1103.getScia1103_Pnd_Entry_Table_Id_Nbr_48().setValue(88);                                                                                             //Natural: ASSIGN #ENTRY-TABLE-ID-NBR-48 := 000088
            pdaScia1103.getScia1103_Pnd_Entry_Cde_48().setValue(ldaAppbl180.getPnd_Mdm_Data_Suffix());                                                                    //Natural: ASSIGN #ENTRY-CDE-48 := SUFFIX
            DbsUtil.callnat(Scin1103.class , getCurrentProcessState(), pdaScia1103.getScia1103());                                                                        //Natural: CALLNAT 'SCIN1103' SCIA1103
            if (condition(Global.isEscape())) return;
            if (condition(pdaScia1103.getScia1103_Pnd_Table_Err_48().equals("E")))                                                                                        //Natural: IF #TABLE-ERR-48 = 'E'
            {
                ldaAppbl180.getPnd_Mdm_Data_Suffix().setValue(" ");                                                                                                       //Natural: MOVE ' ' TO SUFFIX
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Extract_Record() throws Exception                                                                                                              //Natural: WRITE-EXTRACT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppbl180.getPnd_Mdm_Data_Requestor().setValue("ACIS");                                                                                                         //Natural: ASSIGN REQUESTOR := 'ACIS'
        ldaAppbl180.getPnd_Mdm_Data_Function_Code().setValue("003");                                                                                                      //Natural: ASSIGN FUNCTION_CODE := '003'
        ldaAppbl180.getPnd_Mdm_Data_Foreign_Soc_Sec_Nbr().setValue("000000000");                                                                                          //Natural: ASSIGN FOREIGN_SOC_SEC_NBR := '000000000'
        if (condition(ldaAppbl180.getPnd_Mdm_Data_Sex_Code().equals(" ")))                                                                                                //Natural: IF #MDM-DATA.SEX_CODE = ' '
        {
            ldaAppbl180.getPnd_Mdm_Data_Sex_Code().setValue("U");                                                                                                         //Natural: MOVE 'U' TO #MDM-DATA.SEX_CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD 1 TO #WRITE-COUNT /* 05/21/2014
        pnd_Skip_Rec.reset();                                                                                                                                             //Natural: RESET #SKIP-REC
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  /* 05/21/2014 STARTS
            pnd_Skip_Rec.reset();                                                                                                                                         //Natural: RESET #SKIP-REC
            pnd_Compare_Dob.setValue(Global.getDATN());                                                                                                                   //Natural: ASSIGN #COMPARE-DOB := *DATN
            short decideConditionsMet512 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #MDM-DATA.DATE_OF_BIRTH EQ '00000000' OR #MDM-DATA.DATE_OF_BIRTH NE MASK ( CCYYMMDD ) OR #COMPARE-DOB LT #MDM-DATA.DATE_OF_BIRTH
            if (condition(ldaAppbl180.getPnd_Mdm_Data_Date_Of_Birth().equals("00000000") || ! (DbsUtil.maskMatches(ldaAppbl180.getPnd_Mdm_Data_Date_Of_Birth(),"CCYYMMDD")) 
                || pnd_Compare_Dob.less(ldaAppbl180.getPnd_Mdm_Data_Date_Of_Birth())))
            {
                decideConditionsMet512++;
                getReports().write(0, "Invalid DOB:",ldaAppbl180.getPnd_Mdm_Data_Date_Of_Birth());                                                                        //Natural: WRITE 'Invalid DOB:' #MDM-DATA.DATE_OF_BIRTH
                if (Global.isEscape()) return;
                pnd_Skip_Rec.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #SKIP-REC
            }                                                                                                                                                             //Natural: WHEN #MDM-DATA.PH_SOCIAL_SECURITY_NO NE MASK ( NNNNNNNNN ) OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '000000000' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '111111111' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '222222222' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '333333333' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '444444444' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '555555555' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '666666666' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '777777777' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '888888888' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ '999999999' OR #MDM-DATA.PH_SOCIAL_SECURITY_NO EQ ' '
            if (condition(! ((DbsUtil.maskMatches(ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No(),"NNNNNNNNN")) || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("000000000") 
                || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("111111111") || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("222222222") 
                || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("333333333") || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("444444444") 
                || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("555555555") || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("666666666") 
                || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("777777777") || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("888888888") 
                || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals("999999999") || ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().equals(" "))))
            {
                decideConditionsMet512++;
                getReports().write(0, "Invalid SSN:",ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No());                                                                //Natural: WRITE 'Invalid SSN:' #MDM-DATA.PH_SOCIAL_SECURITY_NO
                if (Global.isEscape()) return;
                pnd_Skip_Rec.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #SKIP-REC
            }                                                                                                                                                             //Natural: WHEN #MDM-DATA.ADDRESS_LINE_1 EQ ' '
            if (condition(ldaAppbl180.getPnd_Mdm_Data_Address_Line_1().equals(" ")))
            {
                decideConditionsMet512++;
                getReports().write(0, "Invalid Address line1:",ldaAppbl180.getPnd_Mdm_Data_Address_Line_1());                                                             //Natural: WRITE 'Invalid Address line1:' #MDM-DATA.ADDRESS_LINE_1
                if (Global.isEscape()) return;
                pnd_Skip_Rec.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #SKIP-REC
            }                                                                                                                                                             //Natural: WHEN #MDM-DATA.LAST_NAME EQ ' '
            if (condition(ldaAppbl180.getPnd_Mdm_Data_Last_Name().equals(" ")))
            {
                decideConditionsMet512++;
                getReports().write(0, "Last name is blank:",ldaAppbl180.getPnd_Mdm_Data_Last_Name());                                                                     //Natural: WRITE 'Last name is blank:' #MDM-DATA.LAST_NAME
                if (Global.isEscape()) return;
                pnd_Skip_Rec.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #SKIP-REC
            }                                                                                                                                                             //Natural: WHEN #MDM-DATA.LAST_NAME GT ' '
            if (condition(ldaAppbl180.getPnd_Mdm_Data_Last_Name().greater(" ")))
            {
                decideConditionsMet512++;
                pnd_Last_Name.setValue(ldaAppbl180.getPnd_Mdm_Data_Last_Name());                                                                                          //Natural: ASSIGN #LAST-NAME := #MDM-DATA.LAST_NAME
                if (condition(! (pnd_Valid_Characters_First.contains (pnd_Last_Name_Pnd_Name_L.getValue(1)))))                                                            //Natural: IF #VALID-CHARACTERS-FIRST NE SCAN #NAME-L ( 1 )
                {
                    getReports().write(0, "Last name Should Start with alphabets:",ldaAppbl180.getPnd_Mdm_Data_Last_Name());                                              //Natural: WRITE 'Last name Should Start with alphabets:'#MDM-DATA.LAST_NAME
                    if (Global.isEscape()) return;
                    pnd_Skip_Rec.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #SKIP-REC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR03:                                                                                                                                                //Natural: FOR #P 1 TO 30
                    for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(30)); pnd_P.nadd(1))
                    {
                        if (condition(! (pnd_Valid_Characters.contains (pnd_Last_Name_Pnd_Name_L.getValue(pnd_P)))))                                                      //Natural: IF #VALID-CHARACTERS NE SCAN #NAME-L ( #P )
                        {
                            getReports().write(0, "Last name contains invalid characters:",ldaAppbl180.getPnd_Mdm_Data_Last_Name());                                      //Natural: WRITE 'Last name contains invalid characters:'#MDM-DATA.LAST_NAME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Skip_Rec.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #SKIP-REC
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #MDM-DATA.FIRST_NAME EQ ' '
            if (condition(ldaAppbl180.getPnd_Mdm_Data_First_Name().equals(" ")))
            {
                decideConditionsMet512++;
                getReports().write(0, "First name is blank:",ldaAppbl180.getPnd_Mdm_Data_First_Name());                                                                   //Natural: WRITE 'First name is blank:' #MDM-DATA.FIRST_NAME
                if (Global.isEscape()) return;
                pnd_Skip_Rec.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #SKIP-REC
            }                                                                                                                                                             //Natural: WHEN #MDM-DATA.FIRST_NAME GT ' '
            if (condition(ldaAppbl180.getPnd_Mdm_Data_First_Name().greater(" ")))
            {
                decideConditionsMet512++;
                pnd_First_Name.setValue(ldaAppbl180.getPnd_Mdm_Data_First_Name());                                                                                        //Natural: ASSIGN #FIRST-NAME := #MDM-DATA.FIRST_NAME
                if (condition(! (pnd_Valid_Characters_First.contains (pnd_First_Name_Pnd_Name_F.getValue(1)))))                                                           //Natural: IF #VALID-CHARACTERS-FIRST NE SCAN #NAME-F ( 1 )
                {
                    getReports().write(0, "First name Should Start with alphabets:",ldaAppbl180.getPnd_Mdm_Data_First_Name());                                            //Natural: WRITE 'First name Should Start with alphabets:'#MDM-DATA.FIRST_NAME
                    if (Global.isEscape()) return;
                    pnd_Skip_Rec.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #SKIP-REC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR04:                                                                                                                                                //Natural: FOR #P 1 TO 30
                    for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(30)); pnd_P.nadd(1))
                    {
                        if (condition(! (pnd_Valid_Characters.contains (pnd_First_Name_Pnd_Name_F.getValue(pnd_P)))))                                                     //Natural: IF #VALID-CHARACTERS NE SCAN #NAME-F ( #P )
                        {
                            getReports().write(0, "First name contains invalid characters:",ldaAppbl180.getPnd_Mdm_Data_First_Name());                                    //Natural: WRITE 'First name contains invalid characters:'#MDM-DATA.FIRST_NAME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Skip_Rec.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #SKIP-REC
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #MDM-DATA.MIDDLE_NAME GT ' '
            if (condition(ldaAppbl180.getPnd_Mdm_Data_Middle_Name().greater(" ")))
            {
                decideConditionsMet512++;
                pnd_Middle_Name.setValue(ldaAppbl180.getPnd_Mdm_Data_Middle_Name());                                                                                      //Natural: ASSIGN #MIDDLE-NAME := #MDM-DATA.MIDDLE_NAME
                if (condition(! (pnd_Valid_Characters_First.contains (pnd_Middle_Name_Pnd_Name_M.getValue(1)))))                                                          //Natural: IF #VALID-CHARACTERS-FIRST NE SCAN #NAME-M ( 1 )
                {
                    getReports().write(0, "Middle name Should Start with alphabetm:",ldaAppbl180.getPnd_Mdm_Data_Middle_Name());                                          //Natural: WRITE 'Middle name Should Start with alphabetm:'#MDM-DATA.MIDDLE_NAME
                    if (Global.isEscape()) return;
                    pnd_Skip_Rec.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #SKIP-REC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR05:                                                                                                                                                //Natural: FOR #P 1 TO 30
                    for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(30)); pnd_P.nadd(1))
                    {
                        if (condition(! (pnd_Valid_Characters.contains (pnd_Middle_Name_Pnd_Name_M.getValue(pnd_P)))))                                                    //Natural: IF #VALID-CHARACTERS NE SCAN #NAME-M ( #P )
                        {
                            getReports().write(0, "Middle name contains invalid characters:",ldaAppbl180.getPnd_Mdm_Data_Middle_Name());                                  //Natural: WRITE 'Middle name contains invalid characters:'#MDM-DATA.MIDDLE_NAME
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Skip_Rec.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #SKIP-REC
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet512 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  05/21/2014 ENDS
            if (condition(! (pnd_Skip_Rec.getBoolean())))                                                                                                                 //Natural: IF NOT #SKIP-REC
            {
                //*  05/21/2014
                pnd_Write_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WRITE-COUNT
                getWorkFiles().write(2, false, ldaAppbl180.getPnd_Mdm_Data());                                                                                            //Natural: WRITE WORK FILE 2 #MDM-DATA
                //*  05/21/2014
                                                                                                                                                                          //Natural: PERFORM PRINT-MDM-DATA
                sub_Print_Mdm_Data();
                if (condition(Global.isEscape())) {return;}
                //*  05/21/2014
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppbl180.getPnd_Mdm_Data().reset();                                                                                                                            //Natural: RESET #MDM-DATA
    }
    private void sub_Print_Mdm_Data() throws Exception                                                                                                                    //Natural: PRINT-MDM-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Last4_Ssn.setValue(ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().getSubstring(6));                                                                      //Natural: ASSIGN #LAST4-SSN := SUBSTRING ( PH_SOCIAL_SECURITY_NO,6 )
        //*  DG
        //*  DG
        //*  DG
        getReports().write(0, "Record number ",pnd_Write_Count,"written to MDM file:",NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Requestor(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Function_Code(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Foreign_Soc_Sec_Nbr(),NEWLINE,"=",pnd_Input_Rec_Pnd_Hdr_Plan_No,NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Ph_Unique_Id_Nbr(),NEWLINE,"PH_SOCIAL_SECURITY_NO:",pnd_Last4_Ssn,  //Natural: WRITE 'Record number ' #WRITE-COUNT 'written to MDM file:' / '=' REQUESTOR / '=' FUNCTION_CODE / '=' FOREIGN_SOC_SEC_NBR / '=' #HDR-PLAN-NO / '=' PH_UNIQUE_ID_NBR / 'PH_SOCIAL_SECURITY_NO:' #LAST4-SSN ( EM = ___-__-XXXX ) / '=' PREFIX / '=' LAST_NAME / '=' FIRST_NAME / '=' MIDDLE_NAME / '=' SUFFIX / '=' SEX_CODE / '=' DATE_OF_DEATH / '=' ADDR_TYPE / '=' ADDRESS_LINE_1 / '=' ADDRESS_LINE_2 / '=' ADDRESS_LINE_3 / '=' ADDRESS_LINE_4 / '=' ADDRESS_LINE_5 / '=' CITY / '=' ZIP_CODE / '=' STATE / '=' COUNTRY
            new ReportEditMask ("___-__-XXXX"),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Prefix(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Last_Name(),NEWLINE,
            "=",ldaAppbl180.getPnd_Mdm_Data_First_Name(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Middle_Name(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Suffix(),
            NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Sex_Code(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Date_Of_Death(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Addr_Type(),
            NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Address_Line_1(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Address_Line_2(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Address_Line_3(),
            NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Address_Line_4(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Address_Line_5(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_City(),
            NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Zip_Code(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_State(),NEWLINE,"=",ldaAppbl180.getPnd_Mdm_Data_Country());
        if (Global.isEscape()) return;
    }
    //*  TEMPORARY, UNTIL A ROSTER MAPPING FIX (DG)
    private void sub_Validate_Prefix() throws Exception                                                                                                                   //Natural: VALIDATE_PREFIX
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************
        //*  * * * * * * * * * NO LONGER EXECUTED - GGG052011 * * * * * * * * * *
        //*  LENGTH=8 (COR) BUT OMNI PE140 PREFIX IS (X5)
        //*  CHG191124
        //*  CHG191124
        if (condition(ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("ADM  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("ADM. ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("ATTY ")  //Natural: IF #MDM-DATA.PREFIX = 'ADM  ' OR = 'ADM. ' OR = 'ATTY ' OR = 'ATTY.' OR = 'BRO  ' OR = 'BRO. ' OR = 'BSHP ' OR = 'BSHP.' OR = 'CAPT ' OR = 'CAPT.' OR = 'CDR  ' OR = 'CDR. ' OR = 'CHANC' OR = 'COL  ' OR = 'COL. ' OR = 'DR   ' OR = 'DR.  ' OR = 'FR   ' OR = 'FR.  ' OR = 'GEN  ' OR = 'GEN. ' OR = 'HON  ' OR = 'HON. ' OR = 'LT   ' OR = 'LT.  ' OR = 'MAJ  ' OR = 'MAJ. ' OR = 'MAJOR' OR = 'MISS ' OR = 'MISS.' OR = 'MR   ' OR = 'MR.  ' OR = 'MRS  ' OR = 'MRS. ' OR = 'MS   ' OR = 'MS.  ' OR = 'MSGR ' OR = 'MSGR.' OR = 'MTHR ' OR = 'MTHR.' OR = 'PROF ' OR = 'PROF.' OR = 'PROV ' OR = 'PROV.' OR = 'RABBI' OR = 'REV  ' OR = 'REV. ' OR = 'SR   ' OR = 'SR.  ' OR = 'SIR  ' OR = 'SIR. '
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("ATTY.") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("BRO  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("BRO. ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("BSHP ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("BSHP.") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("CAPT ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("CAPT.") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("CDR  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("CDR. ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("CHANC") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("COL  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("COL. ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("DR   ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("DR.  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("FR   ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("FR.  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("GEN  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("GEN. ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("HON  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("HON. ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("LT   ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("LT.  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MAJ  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MAJ. ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MAJOR") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MISS ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MISS.") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MR   ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MR.  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MRS  ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MRS. ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MS   ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MS.  ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MSGR ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MSGR.") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MTHR ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("MTHR.") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("PROF ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("PROF.") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("PROV ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("PROV.") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("RABBI") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("REV  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("REV. ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("SR   ") 
            || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("SR.  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("SIR  ") || ldaAppbl180.getPnd_Mdm_Data_Prefix().equals("SIR. ")))
        {
            ignore();
            //*  CONSTRUCT EXCEPTION MESSAGE AND BLANK OUT THE INVALID PREFIX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Last4_Ssn.setValue(ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().getSubstring(6));                                                                  //Natural: ASSIGN #LAST4-SSN := SUBSTRING ( PH_SOCIAL_SECURITY_NO,6 )
            pnd_Last4_Ssn.setValueEdited(pnd_Last4_Ssn,new ReportEditMask("___-__-XXXX')'"));                                                                             //Natural: MOVE EDITED #LAST4-SSN ( EM = ___-__-XXXX')' ) TO #LAST4-SSN
            pnd_Buffer.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", ldaAppbl180.getPnd_Mdm_Data_Prefix(), "):"));                                        //Natural: COMPRESS '(' #MDM-DATA.PREFIX '):' INTO #BUFFER LEAVING NO SPACE
            pnd_Buffer.setValue(DbsUtil.compress(pnd_Buffer, ldaAppbl180.getPnd_Mdm_Data_First_Name(), ldaAppbl180.getPnd_Mdm_Data_Middle_Name(), ldaAppbl180.getPnd_Mdm_Data_Last_Name(),  //Natural: COMPRESS #BUFFER FIRST_NAME MIDDLE_NAME LAST_NAME '(SSN:' #LAST4-SSN INTO #BUFFER
                "(SSN:", pnd_Last4_Ssn));
            getReports().write(0, "blanking invalid Prefix",pnd_Buffer);                                                                                                  //Natural: WRITE 'blanking invalid Prefix' #BUFFER
            if (Global.isEscape()) return;
            //*  SET INVALID PREFIX TO BLANKS FOR MDM
            ldaAppbl180.getPnd_Mdm_Data_Prefix().reset();                                                                                                                 //Natural: RESET #MDM-DATA.PREFIX
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE_PREFIX
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  JRB2
        //*  JRB2
        getReports().write(0, NEWLINE,NEWLINE,"Program:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'Program:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE
            TabSetting(25),Global.getERROR_LINE());
        //*   // 'Contract:'   25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR   /* JRB2
        pnd_Error_Ssn.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "XXXXX", ldaAppbl180.getPnd_Mdm_Data_Ph_Social_Security_No().getSubstring(6,               //Natural: COMPRESS 'XXXXX' SUBSTRING ( PH_SOCIAL_SECURITY_NO,6,4 ) INTO #ERROR-SSN LEAVING NO
            4)));
        getReports().write(0, "PARTICIPANT SSN:",pnd_Error_Ssn);                                                                                                          //Natural: WRITE 'PARTICIPANT SSN:' #ERROR-SSN
        getReports().write(0, "ERROR RECORD:",pnd_Input_Rec);                                                                                                             //Natural: WRITE 'ERROR RECORD:' #INPUT-REC
        //*  JRB2
        getReports().write(0, "Extract Records Written:",pnd_Write_Count, new ReportEditMask ("ZZZ,ZZ9"));                                                                //Natural: WRITE 'Extract Records Written:' #WRITE-COUNT ( EM = ZZZ,ZZ9 )
        //*  JRB2
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");
    }
}
