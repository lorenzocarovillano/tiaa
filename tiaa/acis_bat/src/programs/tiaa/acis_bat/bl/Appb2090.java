/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:55 PM
**        * FROM NATURAL PROGRAM : Appb2090
************************************************************
**        * FILE NAME            : Appb2090.java
**        * CLASS NAME           : Appb2090
**        * INSTANCE NAME        : Appb2090
************************************************************
************************************************************************
* PROGRAM  : APPB2090 - ACIS MATCH AND RELEASE DAILY REPORT EXTRACT    *
* FUNCTION : READS PRAP FILE AND DETERMINES IF THE PARTICIPANT         *
*            CONTRACTS HAVE MONEY APPLIED TO THE CONTRACT IN OMNI.  IF *
*            MONEY WAS APPLIED BUT THE STATUS OF THE CONTRACT ON THE   *
*            ACIS PRAP FILE IS NOT MATCHED/RELEASED, THE CONTRACT IS   *
*            EXTRACTED TO BE PUT ON THE DAILY REPORT. THE DAILY REPORT *
*            ONLY STARTS FROM APRIL 1, 2008.                           *
* UPDATED  : 04/22/08 K.GATES   - PROGRAM CREATED                      *
*          : 11/20/08 K.GATES   - REMOVED APRIL 1, 2008 START DATE     *
*          : 07/14/09 G.GUERRERO- RECOMPILE FOR OMNI UPGRADE PROJECT   *
*            FOR NEW VERSION OF SCIA8250                               *
* ADDED 6/2011 GUY   - AP-LEGAL-ANN-OPTION CHECK FOR Y     - SEE JHU   *
*                      SKIP CHECKING FOR FUNDING ON THE CONTRACT       *
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2090 extends BLNatBase
{
    // Data Areas
    private PdaScia8250 pdaScia8250;
    private PdaAciadate pdaAciadate;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Divsub;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Ent_Sys;
    private DbsField annty_Actvty_Prap_View_Ap_Dob;
    private DbsField annty_Actvty_Prap_View_Ap_Applcnt_Req_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_Ann_Option;

    private DbsGroup pnd_Match_Release_File;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Sgrd_Plan_No;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Soc_Sec;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Tiaa_Cntrct;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Cref_Cert;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Pin_Nbr;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Date_Entered;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Cor_Last_Name;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Cor_First_Name;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Lob;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Lob_Type;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Applcnt_Req_Type;
    private DbsField pnd_Match_Release_File_Pnd_Mr_Filler;
    private DbsField pnd_Ap_Name;
    private DbsField pnd_Pnd_Cor_Full_Nme;

    private DbsGroup pnd_Pnd_Cor_Full_Nme__R_Field_1;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme;

    private DbsGroup pnd_Pnd_Cor_Full_Nme__R_Field_2;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme_16;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme_14;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme;

    private DbsGroup pnd_Pnd_Cor_Full_Nme__R_Field_3;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme_10;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme_20;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme;

    private DbsGroup pnd_Pnd_Cor_Full_Nme__R_Field_4;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_10;

    private DbsGroup pnd_Pnd_Cor_Full_Nme__R_Field_5;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_1;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_9;
    private DbsField pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_20;
    private DbsField pnd_Pnd_Hold_Nme_Prfx;

    private DbsGroup pnd_Pnd_Miscellanous_Variables;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Read_Count;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Match_Rel_Cnt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Error_Count;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Write_Count;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_All_Read_Count;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Hold_Date;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Hold_State;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Found;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Deflt_Change;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_No_Issue_Dt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Process_Ind;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Comma;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Space;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Dash;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Del;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time;

    private DbsGroup pnd_Pnd_Miscellanous_Variables__R_Field_6;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Ssn_N;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_St;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Debug;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered;

    private DbsGroup pnd_Pnd_Miscellanous_Variables__R_Field_7;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Ccyy;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Mm;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Dd;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt;

    private DbsGroup pnd_Pnd_Miscellanous_Variables__R_Field_8;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Mm;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Dd;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Ccyy;
    private DbsField pnd_Pnd_Miscellanous_Variables_Pnd_Int_First_Time;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaScia8250 = new PdaScia8250(localVariables);
        pdaAciadate = new PdaAciadate(localVariables);

        // Local Variables

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP");
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        annty_Actvty_Prap_View_Ap_Sgrd_Divsub = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_SGRD_DIVSUB");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Dt_Ent_Sys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        annty_Actvty_Prap_View_Ap_Dob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Applcnt_Req_Type", 
            "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        annty_Actvty_Prap_View_Ap_Legal_Ann_Option = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_Ann_Option", 
            "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Match_Release_File = localVariables.newGroupInRecord("pnd_Match_Release_File", "#MATCH-RELEASE-FILE");
        pnd_Match_Release_File_Pnd_Mr_Sgrd_Plan_No = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Sgrd_Plan_No", "#MR-SGRD-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No", "#MR-SGRD-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Match_Release_File_Pnd_Mr_Soc_Sec = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Soc_Sec", "#MR-SOC-SEC", FieldType.NUMERIC, 
            9);
        pnd_Match_Release_File_Pnd_Mr_Tiaa_Cntrct = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Tiaa_Cntrct", "#MR-TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Match_Release_File_Pnd_Mr_Cref_Cert = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Cref_Cert", "#MR-CREF-CERT", FieldType.STRING, 
            10);
        pnd_Match_Release_File_Pnd_Mr_Pin_Nbr = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Pin_Nbr", "#MR-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Match_Release_File_Pnd_Mr_Date_Entered = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Date_Entered", "#MR-DATE-ENTERED", 
            FieldType.NUMERIC, 8);
        pnd_Match_Release_File_Pnd_Mr_Cor_Last_Name = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Cor_Last_Name", "#MR-COR-LAST-NAME", 
            FieldType.STRING, 30);
        pnd_Match_Release_File_Pnd_Mr_Cor_First_Name = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Cor_First_Name", "#MR-COR-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Match_Release_File_Pnd_Mr_Lob = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Lob", "#MR-LOB", FieldType.STRING, 1);
        pnd_Match_Release_File_Pnd_Mr_Lob_Type = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Lob_Type", "#MR-LOB-TYPE", FieldType.STRING, 
            1);
        pnd_Match_Release_File_Pnd_Mr_Applcnt_Req_Type = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Applcnt_Req_Type", "#MR-APPLCNT-REQ-TYPE", 
            FieldType.STRING, 1);
        pnd_Match_Release_File_Pnd_Mr_Filler = pnd_Match_Release_File.newFieldInGroup("pnd_Match_Release_File_Pnd_Mr_Filler", "#MR-FILLER", FieldType.STRING, 
            76);
        pnd_Ap_Name = localVariables.newFieldInRecord("pnd_Ap_Name", "#AP-NAME", FieldType.STRING, 30);
        pnd_Pnd_Cor_Full_Nme = localVariables.newFieldInRecord("pnd_Pnd_Cor_Full_Nme", "##COR-FULL-NME", FieldType.STRING, 90);

        pnd_Pnd_Cor_Full_Nme__R_Field_1 = localVariables.newGroupInRecord("pnd_Pnd_Cor_Full_Nme__R_Field_1", "REDEFINE", pnd_Pnd_Cor_Full_Nme);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme = pnd_Pnd_Cor_Full_Nme__R_Field_1.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme", "##COR-LAST-NME", 
            FieldType.STRING, 30);

        pnd_Pnd_Cor_Full_Nme__R_Field_2 = pnd_Pnd_Cor_Full_Nme__R_Field_1.newGroupInGroup("pnd_Pnd_Cor_Full_Nme__R_Field_2", "REDEFINE", pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme_16 = pnd_Pnd_Cor_Full_Nme__R_Field_2.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme_16", 
            "##COR-LAST-NME-16", FieldType.STRING, 16);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme_14 = pnd_Pnd_Cor_Full_Nme__R_Field_2.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Last_Nme_14", 
            "##COR-LAST-NME-14", FieldType.STRING, 14);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme = pnd_Pnd_Cor_Full_Nme__R_Field_1.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme", "##COR-FIRST-NME", 
            FieldType.STRING, 30);

        pnd_Pnd_Cor_Full_Nme__R_Field_3 = pnd_Pnd_Cor_Full_Nme__R_Field_1.newGroupInGroup("pnd_Pnd_Cor_Full_Nme__R_Field_3", "REDEFINE", pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme_10 = pnd_Pnd_Cor_Full_Nme__R_Field_3.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme_10", 
            "##COR-FIRST-NME-10", FieldType.STRING, 10);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme_20 = pnd_Pnd_Cor_Full_Nme__R_Field_3.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_First_Nme_20", 
            "##COR-FIRST-NME-20", FieldType.STRING, 20);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme = pnd_Pnd_Cor_Full_Nme__R_Field_1.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme", "##COR-MDDLE-NME", 
            FieldType.STRING, 30);

        pnd_Pnd_Cor_Full_Nme__R_Field_4 = pnd_Pnd_Cor_Full_Nme__R_Field_1.newGroupInGroup("pnd_Pnd_Cor_Full_Nme__R_Field_4", "REDEFINE", pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_10 = pnd_Pnd_Cor_Full_Nme__R_Field_4.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_10", 
            "##COR-MDDLE-NME-10", FieldType.STRING, 10);

        pnd_Pnd_Cor_Full_Nme__R_Field_5 = pnd_Pnd_Cor_Full_Nme__R_Field_4.newGroupInGroup("pnd_Pnd_Cor_Full_Nme__R_Field_5", "REDEFINE", pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_10);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_1 = pnd_Pnd_Cor_Full_Nme__R_Field_5.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_1", 
            "##COR-MDDLE-NME-1", FieldType.STRING, 1);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_9 = pnd_Pnd_Cor_Full_Nme__R_Field_5.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_9", 
            "##COR-MDDLE-NME-9", FieldType.STRING, 9);
        pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_20 = pnd_Pnd_Cor_Full_Nme__R_Field_4.newFieldInGroup("pnd_Pnd_Cor_Full_Nme_Pnd_Pnd_Cor_Mddle_Nme_20", 
            "##COR-MDDLE-NME-20", FieldType.STRING, 20);
        pnd_Pnd_Hold_Nme_Prfx = localVariables.newFieldInRecord("pnd_Pnd_Hold_Nme_Prfx", "##HOLD-NME-PRFX", FieldType.STRING, 8);

        pnd_Pnd_Miscellanous_Variables = localVariables.newGroupInRecord("pnd_Pnd_Miscellanous_Variables", "##MISCELLANOUS-VARIABLES");
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Read_Count = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Read_Count", 
            "##READ-COUNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Match_Rel_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Match_Rel_Cnt", 
            "##MATCH-REL-CNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Error_Count = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Error_Count", 
            "##ERROR-COUNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Write_Count = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Write_Count", 
            "##WRITE-COUNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_All_Read_Count = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_All_Read_Count", 
            "#ALL-READ-COUNT", FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_Hold_Date = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Hold_Date", 
            "#HOLD-DATE", FieldType.DATE);
        pnd_Pnd_Miscellanous_Variables_Pnd_Hold_State = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Hold_State", 
            "#HOLD-STATE", FieldType.STRING, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Found = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Found", "#FOUND", 
            FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Deflt_Change = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Deflt_Change", 
            "#DEFLT-CHANGE", FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_No_Issue_Dt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_No_Issue_Dt", 
            "#NO-ISSUE-DT", FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Process_Ind = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Process_Ind", 
            "#PROCESS-IND", FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Comma = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Comma", 
            "##COMMA", FieldType.STRING, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Space = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Space", 
            "##SPACE", FieldType.STRING, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Dash = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Dash", "##DASH", 
            FieldType.STRING, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Del = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Del", "##DEL", 
            FieldType.STRING, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time", 
            "#RQST-LOG-DTE-TIME", FieldType.STRING, 15);

        pnd_Pnd_Miscellanous_Variables__R_Field_6 = pnd_Pnd_Miscellanous_Variables.newGroupInGroup("pnd_Pnd_Miscellanous_Variables__R_Field_6", "REDEFINE", 
            pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time);
        pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date = pnd_Pnd_Miscellanous_Variables__R_Field_6.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date", 
            "#RQST-LOG-DATE", FieldType.STRING, 8);
        pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time = pnd_Pnd_Miscellanous_Variables__R_Field_6.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time", 
            "#RQST-LOG-TIME", FieldType.STRING, 7);
        pnd_Pnd_Miscellanous_Variables_Pnd_Ssn_N = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Ssn_N", "#SSN-N", 
            FieldType.NUMERIC, 9);
        pnd_Pnd_Miscellanous_Variables_Pnd_St = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_St", "#ST", FieldType.PACKED_DECIMAL, 
            2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Debug = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Debug", "#DEBUG", 
            FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found", 
            "#PREM-FOUND", FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found", 
            "#EXCEPT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason", 
            "#EXCEPT-REASON", FieldType.STRING, 35);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt", 
            "#EXCEPTION-CNT", FieldType.NUMERIC, 6);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt", 
            "#EXC-LINE-CNT", FieldType.NUMERIC, 3);
        pnd_Pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt", 
            "#MATCH-LINE-CNT", FieldType.NUMERIC, 3);
        pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered", 
            "#DT-ENTERED", FieldType.NUMERIC, 8);

        pnd_Pnd_Miscellanous_Variables__R_Field_7 = pnd_Pnd_Miscellanous_Variables.newGroupInGroup("pnd_Pnd_Miscellanous_Variables__R_Field_7", "REDEFINE", 
            pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered);
        pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Ccyy = pnd_Pnd_Miscellanous_Variables__R_Field_7.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Ccyy", 
            "#ENTER-DT-CCYY", FieldType.NUMERIC, 4);
        pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Mm = pnd_Pnd_Miscellanous_Variables__R_Field_7.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Mm", 
            "#ENTER-DT-MM", FieldType.NUMERIC, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Dd = pnd_Pnd_Miscellanous_Variables__R_Field_7.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Dd", 
            "#ENTER-DT-DD", FieldType.NUMERIC, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt", "#EXIST-DT", 
            FieldType.NUMERIC, 8);

        pnd_Pnd_Miscellanous_Variables__R_Field_8 = pnd_Pnd_Miscellanous_Variables.newGroupInGroup("pnd_Pnd_Miscellanous_Variables__R_Field_8", "REDEFINE", 
            pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Mm = pnd_Pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Mm", 
            "#EXIST-DT-MM", FieldType.NUMERIC, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Dd = pnd_Pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Dd", 
            "#EXIST-DT-DD", FieldType.NUMERIC, 2);
        pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Ccyy = pnd_Pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Ccyy", 
            "#EXIST-DT-CCYY", FieldType.NUMERIC, 4);
        pnd_Pnd_Miscellanous_Variables_Pnd_Int_First_Time = pnd_Pnd_Miscellanous_Variables.newFieldInGroup("pnd_Pnd_Miscellanous_Variables_Pnd_Int_First_Time", 
            "#INT-FIRST-TIME", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        localVariables.reset();
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Comma.setInitialValue(",");
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Space.setInitialValue(" ");
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Dash.setInitialValue("-");
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Del.setInitialValue("+");
        pnd_Pnd_Miscellanous_Variables_Pnd_Int_First_Time.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2090() throws Exception
    {
        super("Appb2090");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB2090", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52;//Natural: FORMAT ( 2 ) LS = 133 PS = 52
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        vw_annty_Actvty_Prap_View.startDatabaseRead                                                                                                                       //Natural: READ ANNTY-ACTVTY-PRAP-VIEW BY AP-RELEASE-IND
        (
        "READUPD",
        new Oc[] { new Oc("AP_RELEASE_IND", "ASC") }
        );
        READUPD:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("READUPD")))
        {
            pnd_Pnd_Miscellanous_Variables_Pnd_All_Read_Count.nadd(1);                                                                                                    //Natural: ADD 1 TO #ALL-READ-COUNT
            //*  IF ##MATCH-REL-CNT > 15                 /* TEST ONLY
            //*    ESCAPE BOTTOM
            //*  END-IF
            //*  ONLY APPLICATION RECORDS
            if (condition(annty_Actvty_Prap_View_Ap_Record_Type.notEquals(1)))                                                                                            //Natural: IF AP-RECORD-TYPE NE 1
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ONLY OMNI RECORDS
            if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.notEquals("SGRD")))                                                                                         //Natural: IF AP-COLL-CODE NE 'SGRD'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  NEEDED VALUES FOR AP-STATUS
            //*  ==========================================================
            //*  D - NEW APPLICATION RELEASED
            //*  G - OVERDUE 1 (OVER 30 DAYS OVERDUE)
            //*  H - OVERDUE 2 (OVER 60 DAYS OVERDUE)
            //*  I - OVERDUE 3 (OVER 90 DAYS OVERDUE)
            //*  ==========================================================
            //*  MATCHED AND RELEASED
            //*  DELETED
            //*  NEW PREMIUM ASSIGNED
            //*  SKIP ANNUITY OPTION - JHU
            if (condition(annty_Actvty_Prap_View_Ap_Status.equals("C") || annty_Actvty_Prap_View_Ap_Status.equals("A") || annty_Actvty_Prap_View_Ap_Status.equals("B")    //Natural: IF AP-STATUS EQ 'C' OR AP-STATUS EQ 'A' OR AP-STATUS EQ 'B' OR AP-LEGAL-ANN-OPTION = 'Y'
                || annty_Actvty_Prap_View_Ap_Legal_Ann_Option.equals("Y")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  NEEDED VALUES FOR AP-RELEASE-IND
            //*  ==========================================================
            //*  1 - NEW APPLICATION
            //*  3 - APPLICATION READY FOR WELCOME PRINTING
            //*  4 - APPLICATION READY FOR LEGAL PRINTING
            //*  7 - DEFAULT ENROLLMENT AWAITING APPLICATION - SHOULD NOT BE PRINTED!
            //*  ==========================================================
            //*  NEW PREMIUM
            //*  ANY OTHERS
            if (condition(annty_Actvty_Prap_View_Ap_Release_Ind.equals(2) || annty_Actvty_Prap_View_Ap_Release_Ind.greaterOrEqual(5)))                                    //Natural: IF AP-RELEASE-IND EQ 2 OR AP-RELEASE-IND GE 5
            {
                //*      OR AP-RELEASE-IND EQ 5                   /* ILLINOIS SMP
                //*      OR AP-RELEASE-IND EQ 6                   /* ALL PACKAGES PRINTED
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt.setValue(annty_Actvty_Prap_View_Ap_Dt_Ent_Sys);                                                                   //Natural: ASSIGN #EXIST-DT := AP-DT-ENT-SYS
            pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Mm.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Mm);                                                      //Natural: ASSIGN #ENTER-DT-MM := #EXIST-DT-MM
            pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Dd.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Dd);                                                      //Natural: ASSIGN #ENTER-DT-DD := #EXIST-DT-DD
            pnd_Pnd_Miscellanous_Variables_Pnd_Enter_Dt_Ccyy.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Exist_Dt_Ccyy);                                                  //Natural: ASSIGN #ENTER-DT-CCYY := #EXIST-DT-CCYY
            //*  IF #DT-ENTERED LT 20080401              /* COMMENTED OUT 11-20-08 KG
            //*    ESCAPE TOP                            /* COMMENTED OUT 11-20-08 KG
            //*  END-IF                                  /* COMMENTED OUT 11-20-08 KG
            //*  WRITE '=' AP-TIAA-CNTRCT                    /* TEST ONLY
            pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Read_Count.nadd(1);                                                                                                    //Natural: ADD 1 TO ##READ-COUNT
                                                                                                                                                                          //Natural: PERFORM CALL-PREM-INTERFACE
            sub_Call_Prem_Interface();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READUPD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READUPD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found.getBoolean()))                                                                                    //Natural: IF #PREM-FOUND
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-MATCH-RELEASE-FILE
                sub_Write_Match_Release_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READUPD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READUPD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT
        sub_Write_Control_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaScia8250.getPpt_File_Open_Sw().equals("Y")))                                                                                                     //Natural: IF PPT-FILE-OPEN-SW = 'Y'
        {
            pdaScia8250.getPpt_Part_Fund_Rec().reset();                                                                                                                   //Natural: RESET PPT-PART-FUND-REC
            pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Eof_Sw().setValue("Y");                                                                                                  //Natural: ASSIGN PPT-EOF-SW := 'Y'
                                                                                                                                                                          //Natural: PERFORM CALL-PREM-INTERFACE
            sub_Call_Prem_Interface();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-PREM-INTERFACE
        //* *---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REPORT
        //* *---------------------------------------
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT
        //* *----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT-WRITE-HEADER
        //* *-----------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MATCH-RELEASE-FILE
        //*  #SSN-N := AP-SOC-SEC
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Call_Prem_Interface() throws Exception                                                                                                               //Natural: CALL-PREM-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        //*  WRITE 'GOT TO CALL-PREM-INTERFACE'
        pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found.reset();                                                                                                            //Natural: RESET #PREM-FOUND #EXCEPT-FOUND
        pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found.reset();
        pdaScia8250.getPpt_Part_Fund_Rec().reset();                                                                                                                       //Natural: RESET PPT-PART-FUND-REC
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Request_Type().setValue("F");                                                                                                //Natural: ASSIGN PPT-REQUEST-TYPE := 'F'
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Eof_Sw().setValue("N");                                                                                                      //Natural: ASSIGN PPT-EOF-SW := 'N'
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Plan_Num().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                 //Natural: ASSIGN PPT-PLAN-NUM := AP-SGRD-PLAN-NO
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Part_Sub_Plan().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                         //Natural: ASSIGN PPT-PART-SUB-PLAN := AP-SGRD-SUBPLAN-NO
        if (condition(annty_Actvty_Prap_View_Ap_Soc_Sec.greater(getZero())))                                                                                              //Natural: IF AP-SOC-SEC GT 0
        {
            pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Part_Number().setValueEdited(annty_Actvty_Prap_View_Ap_Soc_Sec,new ReportEditMask("999999999"));                         //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999999999 ) TO PPT-PART-NUMBER
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pnd_Miscellanous_Variables_Pnd_Int_First_Time.equals("Y")))                                                                                     //Natural: IF #INT-FIRST-TIME = 'Y'
        {
            pdaScia8250.getPpt_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN PPT-FILE-OPEN-SW := 'N'
            pnd_Pnd_Miscellanous_Variables_Pnd_Int_First_Time.setValue("N");                                                                                              //Natural: ASSIGN #INT-FIRST-TIME := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia8250.getPpt_File_Open_Sw().setValue("Y");                                                                                                              //Natural: ASSIGN PPT-FILE-OPEN-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Scin8250.class , getCurrentProcessState(), pdaScia8250.getPpt_File_Open_Sw(), pdaScia8250.getPpt_Part_Fund_Rec());                                //Natural: CALLNAT 'SCIN8250' PPT-FILE-OPEN-SW PPT-PART-FUND-REC
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Code().notEquals(getZero())))                                                                           //Natural: IF PPT-RETURN-CODE NE 0
        {
            pnd_Pnd_Miscellanous_Variables_Pnd_Except_Found.setValue(true);                                                                                               //Natural: ASSIGN #EXCEPT-FOUND = TRUE
            pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt.nadd(1);                                                                                                     //Natural: ADD 1 TO #EXCEPTION-CNT
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT
            sub_Exception_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_First_Contrib_Date().greater(getZero())))                                                                  //Natural: IF PPT-FIRST-CONTRIB-DATE GT 0
            {
                pnd_Pnd_Miscellanous_Variables_Pnd_Prem_Found.setValue(true);                                                                                             //Natural: ASSIGN #PREM-FOUND = TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-PREM-INTERFACE
    }
    //*  TITLE LEFT JUSTIFIED
    private void sub_Write_Control_Report() throws Exception                                                                                                              //Natural: WRITE-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. MATCH/RELEASE DAILY REPORT EXTRACT  ",new ColumnSpacing(16),Global.getDATU());           //Natural: WRITE ( 1 ) *PROGRAM 5X 'A.C.I.S. MATCH/RELEASE DAILY REPORT EXTRACT  ' 16X *DATU
        if (Global.isEscape()) return;
        getReports().write(1, Global.getTIMX(),new ColumnSpacing(58),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);                                                            //Natural: WRITE ( 1 ) *TIMX 58X /////
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL PRAP RECORDS READ ",new ColumnSpacing(19),pnd_Pnd_Miscellanous_Variables_Pnd_All_Read_Count,                   //Natural: WRITE ( 1 ) 14X 'TOTAL PRAP RECORDS READ ' 19X #ALL-READ-COUNT /
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL PRAP UNMATCHED/RELEASED RECORDS READ",pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Read_Count,NEWLINE);              //Natural: WRITE ( 1 ) 14X 'TOTAL PRAP UNMATCHED/RELEASED RECORDS READ' ##READ-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL APPLICATION RECORDS NEEDING M/R ",pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Match_Rel_Cnt,NEWLINE);               //Natural: WRITE ( 1 ) 14X 'TOTAL APPLICATION RECORDS NEEDING M/R ' ##MATCH-REL-CNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL RECORDS IN ERROR ",new ColumnSpacing(23),pnd_Pnd_Miscellanous_Variables_Pnd_Exception_Cnt,NEWLINE,             //Natural: WRITE ( 1 ) 14X 'TOTAL RECORDS IN ERROR ' 23X #EXCEPTION-CNT ///////
            NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(16),"**    END    OF   REPORT   **");                                                                                     //Natural: WRITE ( 1 ) 16X '**    END    OF   REPORT   **'
        if (Global.isEscape()) return;
    }
    private void sub_Exception_Report() throws Exception                                                                                                                  //Natural: EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        short decideConditionsMet360 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PPT-RETURN-MSG-MODULE EQ 'PTPTIOX'
        if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().equals("PTPTIOX")))
        {
            decideConditionsMet360++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason.setValue("PARTICIPANT NOT FOUND IN OMNI");                                                                   //Natural: MOVE 'PARTICIPANT NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN PPT-RETURN-MSG-MODULE EQ 'PLPLIO'
        else if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().equals("PLPLIO")))
        {
            decideConditionsMet360++;
            pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason.setValue("PLAN NOT FOUND IN OMNI");                                                                          //Natural: MOVE 'PLAN NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason.setValue(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Message());                                             //Natural: MOVE PPT-RETURN-MESSAGE TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().write(2, NEWLINE,new ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Soc_Sec, new ReportEditMask ("999-99-9999"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,  //Natural: WRITE ( 2 ) / 2X AP-SOC-SEC ( EM = 999-99-9999 ) 7X AP-TIAA-CNTRCT ( EM = XXXXXXX-X ) 7X AP-CREF-CERT ( EM = XXXXXXX-X ) 8X AP-SGRD-PLAN-NO 11X AP-SGRD-SUBPLAN-NO 7X #EXCEPT-REASON
            new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Cref_Cert, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(8),annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,new 
            ColumnSpacing(11),annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,new ColumnSpacing(7),pnd_Pnd_Miscellanous_Variables_Pnd_Except_Reason);
        if (Global.isEscape()) return;
        pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.nadd(1);                                                                                                          //Natural: ADD 1 TO #EXC-LINE-CNT
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().reset();                                                                                                 //Natural: RESET PPT-RETURN-MSG-MODULE
        //*  EXCEPTION-REPORT
    }
    private void sub_Exception_Report_Write_Header() throws Exception                                                                                                     //Natural: EXCEPTION-REPORT-WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        //*  TITLE LEFT JUSTIFIED
        pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.reset();                                                                                                          //Natural: RESET #EXC-LINE-CNT
        getReports().write(2, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. OMNI MATCH/RELEASE DAILY EXCEPTION REPORT",new ColumnSpacing(51),Global.getDATU());      //Natural: WRITE ( 2 ) *PROGRAM 5X 'A.C.I.S. OMNI MATCH/RELEASE DAILY EXCEPTION REPORT' 51X *DATU
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(114),Global.getTIMX(),NEWLINE,NEWLINE);                                                                                   //Natural: WRITE ( 2 ) 114X *TIMX //
        if (Global.isEscape()) return;
        getReports().write(2, "SOCIAL SECURITY",new ColumnSpacing(3),"TIAA CONTRACT",new ColumnSpacing(3),"CREF CONTRACT",new ColumnSpacing(3),"SUNGARD PLAN",new         //Natural: WRITE ( 2 ) 'SOCIAL SECURITY' 3X 'TIAA CONTRACT' 3X 'CREF CONTRACT' 3X 'SUNGARD PLAN' 3X 'SUNGARD SUBPLAN' 3X 'EXCEPTION'/
            ColumnSpacing(3),"SUNGARD SUBPLAN",new ColumnSpacing(3),"EXCEPTION",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(4),"NUMBER",new ColumnSpacing(11),"NUMBER",new ColumnSpacing(10),"NUMBER",new ColumnSpacing(10),"NUMBER",new              //Natural: WRITE ( 2 ) 4X 'NUMBER' 11X 'NUMBER' 10X 'NUMBER' 10X 'NUMBER' 11X 'NUMBER' 7X 'REASON' /
            ColumnSpacing(11),"NUMBER",new ColumnSpacing(7),"REASON",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, "---------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"------------",new         //Natural: WRITE ( 2 ) '---------------' 3X '-------------' 3X '-------------' 3X '------------' 3X '---------------' 3X '------------------------------------------'/
            ColumnSpacing(3),"---------------",new ColumnSpacing(3),"------------------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.nadd(5);                                                                                                          //Natural: ADD 5 TO #EXC-LINE-CNT
        //*  EXCEPTION-REPORT-WRITE-HEADER
    }
    private void sub_Write_Match_Release_File() throws Exception                                                                                                          //Natural: WRITE-MATCH-RELEASE-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Match_Rel_Cnt.nadd(1);                                                                                                     //Natural: ADD 1 TO ##MATCH-REL-CNT
        pnd_Match_Release_File_Pnd_Mr_Sgrd_Plan_No.setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                      //Natural: ASSIGN #MR-SGRD-PLAN-NO := AP-SGRD-PLAN-NO
        pnd_Match_Release_File_Pnd_Mr_Sgrd_Subplan_No.setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                                //Natural: ASSIGN #MR-SGRD-SUBPLAN-NO := AP-SGRD-SUBPLAN-NO
        pnd_Match_Release_File_Pnd_Mr_Soc_Sec.setValue(annty_Actvty_Prap_View_Ap_Soc_Sec);                                                                                //Natural: ASSIGN #MR-SOC-SEC := AP-SOC-SEC
        pnd_Match_Release_File_Pnd_Mr_Tiaa_Cntrct.setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                                        //Natural: ASSIGN #MR-TIAA-CNTRCT := AP-TIAA-CNTRCT
        pnd_Match_Release_File_Pnd_Mr_Cref_Cert.setValue(annty_Actvty_Prap_View_Ap_Cref_Cert);                                                                            //Natural: ASSIGN #MR-CREF-CERT := AP-CREF-CERT
        pnd_Match_Release_File_Pnd_Mr_Pin_Nbr.setValue(annty_Actvty_Prap_View_Ap_Pin_Nbr);                                                                                //Natural: ASSIGN #MR-PIN-NBR := AP-PIN-NBR
        pnd_Match_Release_File_Pnd_Mr_Date_Entered.setValue(pnd_Pnd_Miscellanous_Variables_Pnd_Dt_Entered);                                                               //Natural: ASSIGN #MR-DATE-ENTERED := #DT-ENTERED
        pnd_Match_Release_File_Pnd_Mr_Cor_Last_Name.setValue(annty_Actvty_Prap_View_Ap_Cor_Last_Nme);                                                                     //Natural: ASSIGN #MR-COR-LAST-NAME := AP-COR-LAST-NME
        pnd_Match_Release_File_Pnd_Mr_Cor_First_Name.setValue(annty_Actvty_Prap_View_Ap_Cor_First_Nme);                                                                   //Natural: ASSIGN #MR-COR-FIRST-NAME := AP-COR-FIRST-NME
        pnd_Match_Release_File_Pnd_Mr_Lob.setValue(annty_Actvty_Prap_View_Ap_Lob);                                                                                        //Natural: ASSIGN #MR-LOB := AP-LOB
        pnd_Match_Release_File_Pnd_Mr_Lob_Type.setValue(annty_Actvty_Prap_View_Ap_Lob_Type);                                                                              //Natural: ASSIGN #MR-LOB-TYPE := AP-LOB-TYPE
        pnd_Match_Release_File_Pnd_Mr_Applcnt_Req_Type.setValue(annty_Actvty_Prap_View_Ap_Applcnt_Req_Type);                                                              //Natural: ASSIGN #MR-APPLCNT-REQ-TYPE := AP-APPLCNT-REQ-TYPE
        getWorkFiles().write(1, false, pnd_Match_Release_File);                                                                                                           //Natural: WRITE WORK FILE 1 #MATCH-RELEASE-FILE
        //*  WRITE-MATCH-RELEASE-FILE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT-WRITE-HEADER
                    sub_Exception_Report_Write_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE // 'CONTRACT:' 25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),annty_Actvty_Prap_View_Ap_Pin_Nbr);
        getReports().write(0, "MATCH RELEASE RECS WRITTEN:",pnd_Pnd_Miscellanous_Variables_Pnd_Pnd_Match_Rel_Cnt, new ReportEditMask ("ZZZ,ZZ9"));                        //Natural: WRITE 'MATCH RELEASE RECS WRITTEN:' ##MATCH-REL-CNT ( EM = ZZZ,ZZ9 )
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52");
        Global.format(2, "LS=133 PS=52");
    }
}
