/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:24 PM
**        * FROM NATURAL PROGRAM : Appb166
************************************************************
**        * FILE NAME            : Appb166.java
**        * CLASS NAME           : Appb166
**        * INSTANCE NAME        : Appb166
************************************************************
************************************************************************
* PROGRAM: APPB166 - ACIS/POST RHSP LETTER PROCESS
* READ TRANSACTION FILE AND FORMAT FOR CALL TO POST TO PRINT LETTERS   *
* INPUT FILE IS SORTED BY PLAN NAME.  WRITE LETTER FOR EACH INPUT
* RECORD AND CREATE TOTALS AT THE PLAN LEVEL ON DETAIL REPORT
************************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                     *
* ----------  ----------------  -------------------------------------- *
* 09/17/2008  N. CVETKOVIC      CLONED FROM APPB135                    *
* 01/21/2008  N. CVETKOVIC      CORRECT ADDRESS FORMAT
*                               INC565126                              *
* 06/20/2014  L. SHU            RESTOW FOR NECA4000             CREA   *
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb166 extends BLNatBase
{
    // Data Areas
    private LdaAppl165 ldaAppl165;
    private LdaPstl0425 ldaPstl0425;
    private PdaScia8200 pdaScia8200;
    private PdaPsta9612 pdaPsta9612;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCdaobj pdaCdaobj;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Restart_Data;

    private DbsGroup pnd_Restart_Data__R_Field_1;
    private DbsField pnd_Restart_Data_Pnd_Restart_Program;
    private DbsField pnd_Restart_Data_Pnd_Restart_Ppg;
    private DbsField pnd_Restart_Data_Pnd_Restart_Plan;
    private DbsField pnd_Restart_Data_Pnd_Restart_Name_Key;
    private DbsField pnd_Restart_Data_Pnd_Restart_Tiaa_Contract;
    private DbsField pnd_Restart_Data_Pnd_Restart_Record_Nbr;

    private DbsGroup pnd_Restart_Data__R_Field_2;
    private DbsField pnd_Restart_Data_Pnd_Restart_Record_Nbr_N;

    private DbsGroup pnd_Restart_Data_Pnd_Restart_Date_Time;
    private DbsField pnd_Restart_Data_Pnd_Restart_Date;
    private DbsField pnd_Restart_Data_Pnd_Restart_Time;
    private DbsField pnd_Restart_Data_Pnd_Restart_Filler;
    private DbsField pnd_Restart;
    private DbsField pnd_Sgrd_Call_Cnt;
    private DbsField pnd_Record_Found;
    private DbsField pnd_Total_Plans;
    private DbsField pnd_This_Plan;
    private DbsField pnd_All_Plans;
    private DbsField pnd_Records_Read;
    private DbsField pnd_Letters_Written;
    private DbsField pnd_Post_Error_Count;
    private DbsField pnd_Omni_Error_Count;
    private DbsField pnd_Participant_Count;
    private DbsField pnd_Sub;
    private DbsField pnd_I;
    private DbsField pnd_Max_Size;
    private DbsField pnd_Post_Write_Called;
    private DbsField pnd_Post_Write_Type;
    private DbsField pnd_File_Open_Sw;
    private DbsField pnd_Univ_Id;

    private DbsGroup pnd_Univ_Id__R_Field_3;
    private DbsField pnd_Univ_Id_Pnd_Univ_Inst_Code;
    private DbsField pnd_Univ_Id_Pnd_Univ_Plan_Code;
    private DbsField pnd_Univ_Id_Pnd_Univ_Filler;
    private DbsField pnd_Error_String;
    private DbsField pnd_Error_Temp1;
    private DbsField pnd_Error_Temp2;
    private DbsField pnd_Error_Title;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Win_Title;
    private DbsField pnd_Save_Rqst_Id;
    private DbsField pnd_Plan_Name_Found;
    private DbsField pnd_Debug;
    private DbsField pnd_Call_Post;
    private DbsField pnd_Company_Name;
    private DbsField pnd_Job_Title;
    private DbsField pnd_Contact_Name;
    private DbsField pnd_Format_Field;

    private DbsGroup pnd_Format_Field__R_Field_4;
    private DbsField pnd_Format_Field_Pnd_Char;
    private DbsField pnd_Csub;
    private DbsField pnd_Prev_Char;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01PlanOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl165 = new LdaAppl165();
        registerRecord(ldaAppl165);
        ldaPstl0425 = new LdaPstl0425();
        registerRecord(ldaPstl0425);
        localVariables = new DbsRecord();
        pdaScia8200 = new PdaScia8200(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_Restart_Data = localVariables.newFieldInRecord("pnd_Restart_Data", "#RESTART-DATA", FieldType.STRING, 79);

        pnd_Restart_Data__R_Field_1 = localVariables.newGroupInRecord("pnd_Restart_Data__R_Field_1", "REDEFINE", pnd_Restart_Data);
        pnd_Restart_Data_Pnd_Restart_Program = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Program", "#RESTART-PROGRAM", 
            FieldType.STRING, 8);
        pnd_Restart_Data_Pnd_Restart_Ppg = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Ppg", "#RESTART-PPG", FieldType.STRING, 
            4);
        pnd_Restart_Data_Pnd_Restart_Plan = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Plan", "#RESTART-PLAN", FieldType.STRING, 
            6);
        pnd_Restart_Data_Pnd_Restart_Name_Key = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Name_Key", "#RESTART-NAME-KEY", 
            FieldType.STRING, 7);
        pnd_Restart_Data_Pnd_Restart_Tiaa_Contract = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Tiaa_Contract", "#RESTART-TIAA-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Restart_Data_Pnd_Restart_Record_Nbr = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Record_Nbr", "#RESTART-RECORD-NBR", 
            FieldType.STRING, 9);

        pnd_Restart_Data__R_Field_2 = pnd_Restart_Data__R_Field_1.newGroupInGroup("pnd_Restart_Data__R_Field_2", "REDEFINE", pnd_Restart_Data_Pnd_Restart_Record_Nbr);
        pnd_Restart_Data_Pnd_Restart_Record_Nbr_N = pnd_Restart_Data__R_Field_2.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Record_Nbr_N", "#RESTART-RECORD-NBR-N", 
            FieldType.NUMERIC, 9);

        pnd_Restart_Data_Pnd_Restart_Date_Time = pnd_Restart_Data__R_Field_1.newGroupInGroup("pnd_Restart_Data_Pnd_Restart_Date_Time", "#RESTART-DATE-TIME");
        pnd_Restart_Data_Pnd_Restart_Date = pnd_Restart_Data_Pnd_Restart_Date_Time.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Date", "#RESTART-DATE", 
            FieldType.STRING, 10);
        pnd_Restart_Data_Pnd_Restart_Time = pnd_Restart_Data_Pnd_Restart_Date_Time.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Time", "#RESTART-TIME", 
            FieldType.STRING, 5);
        pnd_Restart_Data_Pnd_Restart_Filler = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Filler", "#RESTART-FILLER", FieldType.STRING, 
            4);
        pnd_Restart = localVariables.newFieldInRecord("pnd_Restart", "#RESTART", FieldType.BOOLEAN, 1);
        pnd_Sgrd_Call_Cnt = localVariables.newFieldInRecord("pnd_Sgrd_Call_Cnt", "#SGRD-CALL-CNT", FieldType.INTEGER, 4);
        pnd_Record_Found = localVariables.newFieldInRecord("pnd_Record_Found", "#RECORD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Total_Plans = localVariables.newFieldInRecord("pnd_Total_Plans", "#TOTAL-PLANS", FieldType.PACKED_DECIMAL, 3);
        pnd_This_Plan = localVariables.newFieldInRecord("pnd_This_Plan", "#THIS-PLAN", FieldType.PACKED_DECIMAL, 5);
        pnd_All_Plans = localVariables.newFieldInRecord("pnd_All_Plans", "#ALL-PLANS", FieldType.PACKED_DECIMAL, 7);
        pnd_Records_Read = localVariables.newFieldInRecord("pnd_Records_Read", "#RECORDS-READ", FieldType.NUMERIC, 9);
        pnd_Letters_Written = localVariables.newFieldInRecord("pnd_Letters_Written", "#LETTERS-WRITTEN", FieldType.NUMERIC, 9);
        pnd_Post_Error_Count = localVariables.newFieldInRecord("pnd_Post_Error_Count", "#POST-ERROR-COUNT", FieldType.NUMERIC, 9);
        pnd_Omni_Error_Count = localVariables.newFieldInRecord("pnd_Omni_Error_Count", "#OMNI-ERROR-COUNT", FieldType.NUMERIC, 9);
        pnd_Participant_Count = localVariables.newFieldInRecord("pnd_Participant_Count", "#PARTICIPANT-COUNT", FieldType.NUMERIC, 9);
        pnd_Sub = localVariables.newFieldInRecord("pnd_Sub", "#SUB", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Size = localVariables.newFieldInRecord("pnd_Max_Size", "#MAX-SIZE", FieldType.NUMERIC, 3);
        pnd_Post_Write_Called = localVariables.newFieldInRecord("pnd_Post_Write_Called", "#POST-WRITE-CALLED", FieldType.BOOLEAN, 1);
        pnd_Post_Write_Type = localVariables.newFieldInRecord("pnd_Post_Write_Type", "#POST-WRITE-TYPE", FieldType.STRING, 2);
        pnd_File_Open_Sw = localVariables.newFieldInRecord("pnd_File_Open_Sw", "#FILE-OPEN-SW", FieldType.STRING, 1);
        pnd_Univ_Id = localVariables.newFieldInRecord("pnd_Univ_Id", "#UNIV-ID", FieldType.STRING, 13);

        pnd_Univ_Id__R_Field_3 = localVariables.newGroupInRecord("pnd_Univ_Id__R_Field_3", "REDEFINE", pnd_Univ_Id);
        pnd_Univ_Id_Pnd_Univ_Inst_Code = pnd_Univ_Id__R_Field_3.newFieldInGroup("pnd_Univ_Id_Pnd_Univ_Inst_Code", "#UNIV-INST-CODE", FieldType.STRING, 
            6);
        pnd_Univ_Id_Pnd_Univ_Plan_Code = pnd_Univ_Id__R_Field_3.newFieldInGroup("pnd_Univ_Id_Pnd_Univ_Plan_Code", "#UNIV-PLAN-CODE", FieldType.STRING, 
            6);
        pnd_Univ_Id_Pnd_Univ_Filler = pnd_Univ_Id__R_Field_3.newFieldInGroup("pnd_Univ_Id_Pnd_Univ_Filler", "#UNIV-FILLER", FieldType.STRING, 1);
        pnd_Error_String = localVariables.newFieldInRecord("pnd_Error_String", "#ERROR-STRING", FieldType.STRING, 20);
        pnd_Error_Temp1 = localVariables.newFieldInRecord("pnd_Error_Temp1", "#ERROR-TEMP1", FieldType.STRING, 40);
        pnd_Error_Temp2 = localVariables.newFieldInRecord("pnd_Error_Temp2", "#ERROR-TEMP2", FieldType.STRING, 40);
        pnd_Error_Title = localVariables.newFieldInRecord("pnd_Error_Title", "#ERROR-TITLE", FieldType.STRING, 40);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 40);
        pnd_Win_Title = localVariables.newFieldInRecord("pnd_Win_Title", "#WIN-TITLE", FieldType.STRING, 40);
        pnd_Save_Rqst_Id = localVariables.newFieldInRecord("pnd_Save_Rqst_Id", "#SAVE-RQST-ID", FieldType.STRING, 11);
        pnd_Plan_Name_Found = localVariables.newFieldInRecord("pnd_Plan_Name_Found", "#PLAN-NAME-FOUND", FieldType.BOOLEAN, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Call_Post = localVariables.newFieldInRecord("pnd_Call_Post", "#CALL-POST", FieldType.BOOLEAN, 1);
        pnd_Company_Name = localVariables.newFieldInRecord("pnd_Company_Name", "#COMPANY-NAME", FieldType.STRING, 35);
        pnd_Job_Title = localVariables.newFieldInRecord("pnd_Job_Title", "#JOB-TITLE", FieldType.STRING, 40);
        pnd_Contact_Name = localVariables.newFieldInRecord("pnd_Contact_Name", "#CONTACT-NAME", FieldType.STRING, 35);
        pnd_Format_Field = localVariables.newFieldInRecord("pnd_Format_Field", "#FORMAT-FIELD", FieldType.STRING, 35);

        pnd_Format_Field__R_Field_4 = localVariables.newGroupInRecord("pnd_Format_Field__R_Field_4", "REDEFINE", pnd_Format_Field);
        pnd_Format_Field_Pnd_Char = pnd_Format_Field__R_Field_4.newFieldArrayInGroup("pnd_Format_Field_Pnd_Char", "#CHAR", FieldType.STRING, 1, new DbsArrayController(1, 
            35));
        pnd_Csub = localVariables.newFieldInRecord("pnd_Csub", "#CSUB", FieldType.NUMERIC, 2);
        pnd_Prev_Char = localVariables.newFieldInRecord("pnd_Prev_Char", "#PREV-CHAR", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01PlanOld = internalLoopRecord.newFieldInRecord("ReadWork01_Plan_OLD", "Plan_OLD", FieldType.STRING, 6);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaAppl165.initializeValues();
        ldaPstl0425.initializeValues();

        localVariables.reset();
        pnd_Record_Found.setInitialValue(true);
        pnd_Max_Size.setInitialValue(75);
        pnd_File_Open_Sw.setInitialValue("N");
        pnd_Plan_Name_Found.setInitialValue(false);
        pnd_Debug.setInitialValue(false);
        pnd_Call_Post.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb166() throws Exception
    {
        super("Appb166");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB166", onError);
        setupReports();
        //*  #DEBUG := TRUE /* FOR TESTING PURPOSES
        //*  #CALL-POST := FALSE /* TESTING
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 65;//Natural: FORMAT ( 2 ) LS = 80 PS = 65;//Natural: FORMAT ( 3 ) LS = 80 PS = 65
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *PROGRAM 9X 'ACIS/POST RHSP LETTER Process' 8X *DATX ( EM = MM/DD/YYYY ) *TIMX ( EM = HH:II ) / 25X 'OMNI STATISTICS REPORT' /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT *PROGRAM 9X 'ACIS/POST RHSP LETTER Process' 8X *DATX ( EM = MM/DD/YYYY ) *TIMX ( EM = HH:II ) / 30X 'OMNI SYSTEM' // ' PLAN ' 2X 'TIAA NBR' 2X ' Participant Name' / '======' 2X '==========' / 2X '========================================'
        //*  PROGRAM WILL 'auto restart
        //*                                                                                                                                                               //Natural: GET TRANSACTION DATA #RESTART-DATA
        if (condition(pnd_Restart_Data.notEquals(" ")))                                                                                                                   //Natural: IF #RESTART-DATA NE ' '
        {
            pnd_Restart.setValue(true);                                                                                                                                   //Natural: ASSIGN #RESTART := TRUE
            getReports().write(0, "RESTART DATA - LAST RECORD STORED ON POST",NEWLINE,NEWLINE,"          PROGRAM:",pnd_Restart_Data_Pnd_Restart_Program,NEWLINE,"LAST ET DATE/TIME:",pnd_Restart_Data_Pnd_Restart_Date,pnd_Restart_Data_Pnd_Restart_Time,NEWLINE,"   LAST ET RECORD:",pnd_Restart_Data_Pnd_Restart_Record_Nbr_N,  //Natural: WRITE 'RESTART DATA - LAST RECORD STORED ON POST' // '          PROGRAM:' #RESTART-PROGRAM / 'LAST ET DATE/TIME:' #RESTART-DATE #RESTART-TIME / '   LAST ET RECORD:' #RESTART-RECORD-NBR-N ( EM = ZZZZZZZZ9 ) / '             PLAN:' #RESTART-PLAN / '         NAME KEY:' #RESTART-NAME-KEY / '    TIIA CONTRACT:' #RESTART-TIAA-CONTRACT
                new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,"             PLAN:",pnd_Restart_Data_Pnd_Restart_Plan,NEWLINE,"         NAME KEY:",pnd_Restart_Data_Pnd_Restart_Name_Key,
                NEWLINE,"    TIIA CONTRACT:",pnd_Restart_Data_Pnd_Restart_Tiaa_Contract);
            if (Global.isEscape()) return;
            pnd_Restart.setValue(true);                                                                                                                                   //Natural: ASSIGN #RESTART := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Restart_Data_Pnd_Restart_Program.setValue(Global.getPROGRAM());                                                                                               //Natural: ASSIGN #RESTART-PROGRAM := *PROGRAM
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 APPL165
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaAppl165.getAppl165())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Records_Read.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORDS-READ
            //*  ------------------------------------------------------------------- *
            //*  CHECK RESTART AND BYPASS RECORDS ALREADY PROCESSED                  *
            //*  ------------------------------------------------------------------- *
            if (condition(pnd_Restart.getBoolean()))                                                                                                                      //Natural: IF #RESTART
            {
                if (condition(ldaAppl165.getAppl165_Plan().equals(pnd_Restart_Data_Pnd_Restart_Plan) && ldaAppl165.getAppl165_Tiaa_Contract().equals(pnd_Restart_Data_Pnd_Restart_Tiaa_Contract))) //Natural: IF APPL165.PLAN = #RESTART-PLAN AND APPL165.TIAA-CONTRACT = #RESTART-TIAA-CONTRACT
                {
                    pnd_Restart.setValue(false);                                                                                                                          //Natural: ASSIGN #RESTART := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF APPL165.PLAN
            pnd_Post_Write_Called.setValue(false);                                                                                                                        //Natural: ASSIGN #POST-WRITE-CALLED := FALSE
                                                                                                                                                                          //Natural: PERFORM WRITE-POST-INFO
            sub_Write_Post_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-DETAIL
            sub_Write_Report_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM BUILD-POWER-IMAGE-INFO
            sub_Build_Power_Image_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Post_Write_Type.setValue("PI");                                                                                                                           //Natural: ASSIGN #POST-WRITE-TYPE := 'PI'
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
            sub_Post_Write();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM POST-CLOSE
            sub_Post_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Letters_Written.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #LETTERS-WRITTEN
            pnd_This_Plan.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #THIS-PLAN
            readWork01PlanOld.setValue(ldaAppl165.getAppl165_Plan());                                                                                                     //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM EOJ-PROCESSING
        sub_Eoj_Processing();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PLAN-NAME
        //*  ------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-POST-INFO
        //*  #UNIV-INST-CODE             := APPL165.PLAN-NUM
        //*  #UNIV-PLAN-CODE               := APPL165.#TRANS-SG-SUBPLAN-NO
        //* *
        //*  PSTA9612.ADDRSS-LINE-4        := APPL165.ADDRESS(4)
        //*  PSTA9612.ADDRSS-LINE-5        := APPL165.ADDRESS(5)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-POWER-IMAGE-INFO
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OPEN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-WRITE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-CLOSE
        //*   #RESTART-NAME-KEY      := APPL165.#NAME-KEY
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-DETAIL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-FUND
        //*     #P-TICKER  'not found on New Externalization'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EOJ-PROCESSING
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RUN-STATISTICS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Get_Plan_Name() throws Exception                                                                                                                     //Natural: GET-PLAN-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //*  CALL INTERFACE TO OMNI - RETRIEVE COMPANY NAME/ADDRESS INFO         *
        pdaScia8200.getCon_Part_Rec().reset();                                                                                                                            //Natural: RESET CON-PART-REC
        pnd_Sgrd_Call_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #SGRD-CALL-CNT
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            pdaScia8200.getCon_Part_Rec_Con_Company_Name().setValue(DbsUtil.compress("RHSP Plan", ldaAppl165.getAppl165_Plan(), "Test mode- no Oracle "));                //Natural: COMPRESS 'RHSP Plan' APPL165.PLAN 'Test mode- no Oracle ' INTO CON-COMPANY-NAME
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sgrd_Call_Cnt.equals(1)))                                                                                                                       //Natural: IF #SGRD-CALL-CNT = 1
        {
            pdaScia8200.getCon_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN CON-FILE-OPEN-SW := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia8200.getCon_File_Open_Sw().setValue("Y");                                                                                                              //Natural: ASSIGN CON-FILE-OPEN-SW := 'Y'
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("N");                                                                                                           //Natural: ASSIGN CON-PART-REC.CON-EOF-SW := 'N'
        pdaScia8200.getCon_Part_Rec_Con_Function_Code().getValue(1).setValue("08");                                                                                       //Natural: ASSIGN CON-PART-REC.CON-FUNCTION-CODE ( 1 ) := '08'
        pdaScia8200.getCon_Part_Rec_Con_Access_Level().setValue(" ");                                                                                                     //Natural: ASSIGN CON-PART-REC.CON-ACCESS-LEVEL := ' '
        pdaScia8200.getCon_Part_Rec_Con_Contact_Type().setValue("E");                                                                                                     //Natural: ASSIGN CON-PART-REC.CON-CONTACT-TYPE := 'E'
        pdaScia8200.getCon_Part_Rec_Con_Plan_Num().setValue(ldaAppl165.getAppl165_Plan_Num());                                                                            //Natural: ASSIGN CON-PART-REC.CON-PLAN-NUM := APPL165.PLAN-NUM
        pdaScia8200.getCon_Part_Rec_Con_Part_Divsub().setValue(ldaAppl165.getAppl165_Divsub());                                                                           //Natural: ASSIGN CON-PART-REC.CON-PART-DIVSUB := APPL165.DIVSUB
        //*  SGRD5 KG
        DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                     //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia8200.getCon_Part_Rec_Con_Return_Code().notEquals(getZero()) || pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg().notEquals(" ")))            //Natural: IF CON-PART-REC.CON-RETURN-CODE NE 0 OR CON-PART-REC.CON-CONTACT-ERR-MSG NE ' '
        {
            getReports().write(0, "*CALL ERROR (PSG9070):","RC:",pdaScia8200.getCon_Part_Rec_Con_Return_Code(),NEWLINE,pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg()); //Natural: WRITE '*CALL ERROR (PSG9070):' 'RC:' CON-PART-REC.CON-RETURN-CODE / CON-PART-REC.CON-CONTACT-ERR-MSG
            if (Global.isEscape()) return;
            getReports().write(0, "PLAN:",pdaScia8200.getCon_Part_Rec_Con_Plan_Num());                                                                                    //Natural: WRITE 'PLAN:' CON-PART-REC.CON-PLAN-NUM
            if (Global.isEscape()) return;
            pnd_Contact_Name.reset();                                                                                                                                     //Natural: RESET #CONTACT-NAME #COMPANY-NAME #JOB-TITLE CON-PART-REC
            pnd_Company_Name.reset();
            pnd_Job_Title.reset();
            pdaScia8200.getCon_Part_Rec().reset();
            pnd_Omni_Error_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #OMNI-ERROR-COUNT
            pnd_Plan_Name_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #PLAN-NAME-FOUND := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Plan_Name_Found.setValue(true);                                                                                                                           //Natural: ASSIGN #PLAN-NAME-FOUND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DEBUG DISPLAYS:
        getReports().write(0, NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Title(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Job_Title(),NEWLINE,"=",                    //Natural: WRITE / '=' CON-TITLE / '=' CON-JOB-TITLE / '=' CON-COMPANY-NAME
            pdaScia8200.getCon_Part_Rec_Con_Company_Name());
        if (Global.isEscape()) return;
    }
    private void sub_Write_Post_Info() throws Exception                                                                                                                   //Natural: WRITE-POST-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //*  RESET PSTL0425-DATA.PRIMARY-RECORD PSTA9612.PRTCPNT-DATA
        //*  PRIMARY RECORD
        //*  PARTIPANT
        pdaPsta9612.getPsta9612_Prtcpnt_Data().reset();                                                                                                                   //Natural: RESET PSTA9612.PRTCPNT-DATA
        ldaPstl0425.getPstl0425_Data_Pstl0425_Rec_Id().setValue("RH");                                                                                                    //Natural: ASSIGN PSTL0425-DATA.PSTL0425-REC-ID := 'RH'
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("BATCHACIS");                                                                                                     //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'BATCHACIS'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTRKRHSP");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PTRKRHSP'
        pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaAppl165.getAppl165_Pin());                                                                                          //Natural: ASSIGN PSTA9612.UNIV-ID := APPL165.PIN
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                     //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-MIT-IND := TRUE
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                     //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-EFM-IND := TRUE
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                     //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-EFM-IND := TRUE
        pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("Richard Ward");                                                                                               //Natural: ASSIGN PSTA9612.EMPL-SGNTRY-NME := 'Richard Ward'
        pdaPsta9612.getPsta9612_Empl_Unit_Work_Nme().setValue("Director, Institutional Product Management");                                                              //Natural: ASSIGN PSTA9612.EMPL-UNIT-WORK-NME := 'Director, Institutional Product Management'
        //*  DOMESTIC
        if (condition(ldaAppl165.getAppl165_Address().getValue(1).notEquals(" ")))                                                                                        //Natural: IF APPL165.ADDRESS ( 1 ) NE ' '
        {
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("U");                                                                                                       //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := 'U'
            //*  BAD ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue("B");                                                                                                       //Natural: ASSIGN PSTA9612.ADDRSS-TYP-CDE := 'B'
            getReports().write(0, "INVALID CONTACT INFO  -","pin",ldaAppl165.getAppl165_Pin(),"ADDRSS-TYP-CDE:",pdaPsta9612.getPsta9612_Addrss_Typ_Cde());                //Natural: WRITE 'INVALID CONTACT INFO  -' 'pin' APPL165.PIN 'ADDRSS-TYP-CDE:' PSTA9612.ADDRSS-TYP-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Full_Nme().setValue(ldaAppl165.getAppl165_Full_Name());                                                                                   //Natural: ASSIGN PSTA9612.FULL-NME := APPL165.FULL-NAME
        pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(ldaAppl165.getAppl165_Address().getValue(1));                                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-1 := APPL165.ADDRESS ( 1 )
        pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(ldaAppl165.getAppl165_Address().getValue(2));                                                                    //Natural: ASSIGN PSTA9612.ADDRSS-LINE-2 := APPL165.ADDRESS ( 2 )
        //*  INC565126
        //*  CORRECT ADDRESS IMPROPERLY SET UP IN APPB165
        //*  CITY AND STATE MUST BE ON THE SAME LINE
        ldaAppl165.getAppl165_Address().getValue(3).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl165.getAppl165_Address().getValue(3),                 //Natural: COMPRESS APPL165.ADDRESS ( 3 ) ',' INTO APPL165.ADDRESS ( 3 ) LEAVING NO
            ","));
        pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(DbsUtil.compress(ldaAppl165.getAppl165_Address().getValue(3), ldaAppl165.getAppl165_Address().getValue(4)));     //Natural: COMPRESS APPL165.ADDRESS ( 3 ) APPL165.ADDRESS ( 4 ) INTO PSTA9612.ADDRSS-LINE-3
        pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(" ");                                                                                                            //Natural: ASSIGN PSTA9612.ADDRSS-LINE-4 := ' '
        pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(" ");                                                                                                            //Natural: ASSIGN PSTA9612.ADDRSS-LINE-5 := ' '
        pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(ldaAppl165.getAppl165_Zip());                                                                                   //Natural: ASSIGN PSTA9612.ADDRSS-ZP9-NBR := APPL165.ZIP
        pdaPsta9612.getPsta9612_Alt_Dlvry_Addr().setValue(ldaAppl165.getAppl165_Email_Address());                                                                         //Natural: ASSIGN PSTA9612.ALT-DLVRY-ADDR := APPL165.EMAIL-ADDRESS
        //*  INC565126
        //*      - RESET PACKAGE DELIVERY TYPE FIELD
        //*        BEFORE EACH CALL TO POST
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().reset();                                                                                                                //Natural: RESET PSTA9612.PCKGE-DLVRY-TYP
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, "=",pdaPsta9612.getPsta9612_Addrss_Typ_Cde(),NEWLINE,pdaPsta9612.getPsta9612_Full_Nme(),NEWLINE,pdaPsta9612.getPsta9612_Addrss_Line_1(), //Natural: WRITE '=' PSTA9612.ADDRSS-TYP-CDE / PSTA9612.FULL-NME / PSTA9612.ADDRSS-LINE-1 / PSTA9612.ADDRSS-LINE-2 / PSTA9612.ADDRSS-LINE-3 / PSTA9612.ADDRSS-LINE-4 / PSTA9612.ADDRSS-LINE-5 / PSTA9612.ADDRSS-ZP9-NBR / APPL165.EMAIL-ADDRESS
                NEWLINE,pdaPsta9612.getPsta9612_Addrss_Line_2(),NEWLINE,pdaPsta9612.getPsta9612_Addrss_Line_3(),NEWLINE,pdaPsta9612.getPsta9612_Addrss_Line_4(),
                NEWLINE,pdaPsta9612.getPsta9612_Addrss_Line_5(),NEWLINE,pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr(),NEWLINE,ldaAppl165.getAppl165_Email_Address());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  INC - WRITE PACKAGE DELIVERY TYPE GOING IN AND COMING BACK
        getReports().write(0, "=",pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ()," BEFORE CALLING POST");                                                                      //Natural: WRITE '=' PSTA9612.PCKGE-DLVRY-TYP ' BEFORE CALLING POST'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM POST-OPEN
        sub_Post_Open();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "=",pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ()," AFTER RETURN FROM POST");                                                                   //Natural: WRITE '=' PSTA9612.PCKGE-DLVRY-TYP ' AFTER RETURN FROM POST'
        if (Global.isEscape()) return;
        //*  PERFORM POST-WRITE              /* FOR PARTICIPANT INFORMATION
        //*  PRIMARY RECORD
        ldaPstl0425.getPstl0425_Data_Pstl0425_Data_Array().getValue("*").reset();                                                                                         //Natural: RESET PSTL0425-DATA-ARRAY ( * )
        ldaPstl0425.getPstl0425_Data_Pstl0425_Rec_Id().setValue("RH");                                                                                                    //Natural: ASSIGN PSTL0425-DATA.PSTL0425-REC-ID := 'RH'
                                                                                                                                                                          //Natural: PERFORM GET-PLAN-NAME
        sub_Get_Plan_Name();
        if (condition(Global.isEscape())) {return;}
        ldaPstl0425.getPstl0425_Data_Rh_Employer().setValue(pdaScia8200.getCon_Part_Rec_Con_Company_Name());                                                              //Natural: ASSIGN RH-EMPLOYER := CON-COMPANY-NAME
        ldaPstl0425.getPstl0425_Data_Rh_Date_Of_Birth().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl165.getAppl165_Birthdate_Mmddyyyy().getSubstring(1,2),  //Natural: COMPRESS SUBSTR ( APPL165.BIRTHDATE-MMDDYYYY,1,2 ) '/' SUBSTR ( APPL165.BIRTHDATE-MMDDYYYY,3,2 ) '/' SUBSTR ( APPL165.BIRTHDATE-MMDDYYYY,5,4 ) INTO RH-DATE-OF-BIRTH LEAVING NO
            "/", ldaAppl165.getAppl165_Birthdate_Mmddyyyy().getSubstring(3,2), "/", ldaAppl165.getAppl165_Birthdate_Mmddyyyy().getSubstring(5,4)));
        ldaPstl0425.getPstl0425_Data_Rh_Account_Number().setValue(ldaAppl165.getAppl165_Tiaa_Contract());                                                                 //Natural: ASSIGN RH-ACCOUNT-NUMBER := APPL165.TIAA-CONTRACT
        //*  FOR PARTICIPANT INFORMATION
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
        sub_Post_Write();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, "=",ldaPstl0425.getPstl0425_Data_Rh_Employer(),"=",ldaPstl0425.getPstl0425_Data_Rh_Date_Of_Birth(),"=",ldaPstl0425.getPstl0425_Data_Rh_Account_Number()); //Natural: WRITE '=' RH-EMPLOYER '=' RH-DATE-OF-BIRTH '=' RH-ACCOUNT-NUMBER
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRIMARY RECORD
        ldaPstl0425.getPstl0425_Data_Pstl0425_Data_Array().getValue("*").reset();                                                                                         //Natural: RESET PSTL0425-DATA-ARRAY ( * )
        ldaPstl0425.getPstl0425_Data_Pstl0425_Rec_Id().setValue("AA");                                                                                                    //Natural: ASSIGN PSTL0425-DATA.PSTL0425-REC-ID := 'AA'
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            if (condition(ldaAppl165.getAppl165_Alloc_Fund_Cde().getValue(pnd_I).equals(" ")))                                                                            //Natural: IF ALLOC-FUND-CDE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOOKUP-FUND
            sub_Lookup_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaPstl0425.getPstl0425_Data_Rh_Aa_Fund_Name().getValue(pnd_I).setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1));                                  //Natural: ASSIGN RH-AA-FUND-NAME ( #I ) := NECA4000.FDS-FUND-NME ( 1 )
            ldaPstl0425.getPstl0425_Data_Rh_Aa_Percent_Amount().getValue(pnd_I).setValue(ldaAppl165.getAppl165_Alloc_Pct().getValue(pnd_I));                              //Natural: ASSIGN RH-AA-PERCENT-AMOUNT ( #I ) := ALLOC-PCT ( #I )
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "=",ldaPstl0425.getPstl0425_Data_Rh_Aa_Fund_Name().getValue(pnd_I),"=",ldaPstl0425.getPstl0425_Data_Rh_Aa_Percent_Amount().getValue(pnd_I)); //Natural: WRITE '=' RH-AA-FUND-NAME ( #I ) '=' RH-AA-PERCENT-AMOUNT ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FOR PARTICIPANT INFORMATION
                                                                                                                                                                          //Natural: PERFORM POST-WRITE
        sub_Post_Write();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Build_Power_Image_Info() throws Exception                                                                                                            //Natural: BUILD-POWER-IMAGE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //*  POWER IMAGE
        //*  NOT USED
        //*  TASK IS A TASK TYPE
        //*  PI-EXPORT-IND 'I'
        //*  PI-EXPORT-IND 'T'
        //*  PI-EXPORT-IND 'G'
        //*  CLOSE
        ldaPstl0425.getPstl0425_Data_Power_Image_Task_Information().reset();                                                                                              //Natural: RESET POWER-IMAGE-TASK-INFORMATION
        ldaPstl0425.getPstl0425_Data_Pstl0425_Rec_Id().setValue("PI");                                                                                                    //Natural: ASSIGN PSTL0425-DATA.PSTL0425-REC-ID := 'PI'
        ldaPstl0425.getPstl0425_Data_Pi_Begin_Literal().setValue(" ");                                                                                                    //Natural: ASSIGN PSTL0425-DATA.PI-BEGIN-LITERAL := ' '
        ldaPstl0425.getPstl0425_Data_Pi_Export_Ind().setValue("T");                                                                                                       //Natural: ASSIGN PSTL0425-DATA.PI-EXPORT-IND := 'T'
        ldaPstl0425.getPstl0425_Data_Pi_Task_Id().setValue(" ");                                                                                                          //Natural: ASSIGN PSTL0425-DATA.PI-TASK-ID := ' '
        ldaPstl0425.getPstl0425_Data_Pi_Task_Type().setValue("INCENRRHSP");                                                                                               //Natural: ASSIGN PSTL0425-DATA.PI-TASK-TYPE := 'INCENRRHSP'
        ldaPstl0425.getPstl0425_Data_Pi_Task_Guid().setValue(" ");                                                                                                        //Natural: ASSIGN PSTL0425-DATA.PI-TASK-GUID := ' '
        ldaPstl0425.getPstl0425_Data_Pi_Action_Step().setValue("NONE");                                                                                                   //Natural: ASSIGN PSTL0425-DATA.PI-ACTION-STEP := 'NONE'
        ldaPstl0425.getPstl0425_Data_Pi_Task_Status().setValue("C");                                                                                                      //Natural: ASSIGN PSTL0425-DATA.PI-TASK-STATUS := 'C'
        ldaPstl0425.getPstl0425_Data_Pi_Ssn().setValue(ldaAppl165.getAppl165_Ssn_Alpha());                                                                                //Natural: ASSIGN PSTL0425-DATA.PI-SSN := APPL165.SSN-ALPHA
        ldaPstl0425.getPstl0425_Data_Pi_Pin_Npin_Ppg().setValue(ldaAppl165.getAppl165_Pin());                                                                             //Natural: ASSIGN PSTL0425-DATA.PI-PIN-NPIN-PPG := APPL165.PIN
        ldaPstl0425.getPstl0425_Data_Pi_Pin_Type().setValue("P");                                                                                                         //Natural: ASSIGN PSTL0425-DATA.PI-PIN-TYPE := 'P'
        ldaPstl0425.getPstl0425_Data_Pi_Contract_A().getValue("*").setValue(" ");                                                                                         //Natural: ASSIGN PSTL0425-DATA.PI-CONTRACT-A ( * ) := ' '
        ldaPstl0425.getPstl0425_Data_Pi_Plan_Id().setValue(ldaAppl165.getAppl165_Plan());                                                                                 //Natural: ASSIGN PSTL0425-DATA.PI-PLAN-ID := APPL165.PLAN
        ldaPstl0425.getPstl0425_Data_Pi_Doc_Content().setValue("CONFIRMATION");                                                                                           //Natural: ASSIGN PSTL0425-DATA.PI-DOC-CONTENT := 'CONFIRMATION'
        //*  CURRENT DATE
        ldaPstl0425.getPstl0425_Data_Pi_Tiaa_Full_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO PSTL0425-DATA.PI-TIAA-FULL-DATE
        //*  CURRENT TIME
        ldaPstl0425.getPstl0425_Data_Pi_Tiaa_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:MM:SS"));                                                      //Natural: MOVE EDITED *TIMX ( EM = HH:MM:SS ) TO PSTL0425-DATA.PI-TIAA-TIME
    }
    private void sub_Post_Open() throws Exception                                                                                                                         //Natural: POST-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Call_Post.getBoolean()))                                                                                                                        //Natural: IF #CALL-POST
        {
            DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),               //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
                pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
            if (condition(Global.isEscape())) return;
            pnd_Error_String.setValue("POST OPEN");                                                                                                                       //Natural: ASSIGN #ERROR-STRING := 'POST OPEN'
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
                sub_Error_Routine();
                if (condition(Global.isEscape())) {return;}
                pnd_Post_Error_Count.nadd(1);                                                                                                                             //Natural: ADD 1 TO #POST-ERROR-COUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Rqst_Id.setValue(pdaPsta9610.getPsta9610_Pst_Rqst_Id());                                                                                         //Natural: ASSIGN #SAVE-RQST-ID := PSTA9610.PST-RQST-ID
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "=",pnd_Save_Rqst_Id);                                                                                                              //Natural: WRITE '=' #SAVE-RQST-ID
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Post_Write() throws Exception                                                                                                                        //Natural: POST-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Call_Post.getBoolean()))                                                                                                                        //Natural: IF #CALL-POST
        {
            pnd_Post_Write_Called.setValue(true);                                                                                                                         //Natural: ASSIGN #POST-WRITE-CALLED := TRUE
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                         //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
            //*  APPLICATION SPECIFIC DATA
            DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL0425-DATA
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
                ldaPstl0425.getSort_Key(), ldaPstl0425.getPstl0425_Data());
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
                sub_Error_Routine();
                if (condition(Global.isEscape())) {return;}
                pnd_Post_Error_Count.nadd(1);                                                                                                                             //Natural: ADD 1 TO #POST-ERROR-COUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Post_Close() throws Exception                                                                                                                        //Natural: POST-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Call_Post.getBoolean()))                                                                                                                        //Natural: IF #CALL-POST
        {
            pnd_Error_String.setValue("POST CLOSE");                                                                                                                      //Natural: ASSIGN #ERROR-STRING := 'POST CLOSE'
            DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),               //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
                pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
            sub_Error_Routine();
            if (condition(Global.isEscape())) {return;}
            pnd_Post_Error_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #POST-ERROR-COUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Restart_Data_Pnd_Restart_Plan.setValue(ldaAppl165.getAppl165_Plan_Num());                                                                                 //Natural: ASSIGN #RESTART-PLAN := PLAN-NUM
            pnd_Restart_Data_Pnd_Restart_Tiaa_Contract.setValue(ldaAppl165.getAppl165_Tiaa_Contract());                                                                   //Natural: ASSIGN #RESTART-TIAA-CONTRACT := APPL165.TIAA-CONTRACT
            pnd_Restart_Data_Pnd_Restart_Record_Nbr_N.compute(new ComputeParameters(false, pnd_Restart_Data_Pnd_Restart_Record_Nbr_N), pnd_Records_Read.subtract(1));     //Natural: ASSIGN #RESTART-RECORD-NBR-N := #RECORDS-READ - 1
            pnd_Restart_Data_Pnd_Restart_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                          //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #RESTART-DATE
            pnd_Restart_Data_Pnd_Restart_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II"));                                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II ) TO #RESTART-TIME
            //*  FINALIZE POST WRITES FOR PLAN
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION #RESTART-DATA
            pnd_Error_String.setValue("REQUEST ADDED!");                                                                                                                  //Natural: ASSIGN #ERROR-STRING := 'REQUEST ADDED!'
            getReports().write(0, "MAIL HEADER AND DATA:",pnd_Save_Rqst_Id,"ADDED SUCCESSFULLY",NEWLINE);                                                                 //Natural: WRITE 'MAIL HEADER AND DATA:' #SAVE-RQST-ID 'ADDED SUCCESSFULLY' /
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Report_Detail() throws Exception                                                                                                               //Natural: WRITE-REPORT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Participant_Count.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #PARTICIPANT-COUNT
        getReports().write(3, ldaAppl165.getAppl165_Plan(),new ColumnSpacing(3),ldaAppl165.getAppl165_Tiaa_Contract(),new ColumnSpacing(2),ldaAppl165.getAppl165_Full_Name()); //Natural: WRITE ( 3 ) APPL165.PLAN 3X APPL165.TIAA-CONTRACT 2X APPL165.FULL-NAME
        if (Global.isEscape()) return;
    }
    private void sub_Lookup_Fund() throws Exception                                                                                                                       //Natural: LOOKUP-FUND
    {
        if (BLNatReinput.isReinput()) return;

        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN REQUEST-IND := ' '
        pdaNeca4000.getNeca4000_Function_Cde().setValue("FDS");                                                                                                           //Natural: ASSIGN FUNCTION-CDE := 'FDS'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                     //Natural: ASSIGN INPT-KEY-OPTION-CDE := '01'
        pdaNeca4000.getNeca4000_Fds_Key_Ticker_Symbol().setValue(ldaAppl165.getAppl165_Alloc_Fund_Cde().getValue(pnd_I));                                                 //Natural: ASSIGN FDS-KEY-TICKER-SYMBOL := ALLOC-FUND-CDE ( #I )
        pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValue("16");                                                                                               //Natural: ASSIGN FDS-KEY-FUND-NAME-REF-CDE := '16'
        //*  GET FUND
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        //*  TICKER NOT FOUND
        if (condition(pdaNeca4000.getNeca4000_Output_Cnt().equals(getZero())))                                                                                            //Natural: IF NECA4000.OUTPUT-CNT = 0
        {
            pnd_Error_Msg.setValue(DbsUtil.compress("PYMNT-FUND-ACCNT-NBR: Ticker Symbol"));                                                                              //Natural: COMPRESS 'PYMNT-FUND-ACCNT-NBR: Ticker Symbol' INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Eoj_Processing() throws Exception                                                                                                                    //Natural: EOJ-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //*  CLOSE VSAM FILE - CALL TO OMNI FOR CONTACT INFORMATION              *
        //*  OMNI ADDRESS FILE OPENED    SGRD5 KG
        if (condition(pdaScia8200.getCon_File_Open_Sw().equals("Y")))                                                                                                     //Natural: IF CON-FILE-OPEN-SW = 'Y'
        {
            pdaScia8200.getCon_Part_Rec().reset();                                                                                                                        //Natural: RESET CON-PART-REC
            pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("Y");                                                                                                       //Natural: ASSIGN CON-EOF-SW := 'Y'
            //*  SGRD5 KG
            DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                 //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  NORMAL END OF JOB
        pnd_Restart_Data.reset();                                                                                                                                         //Natural: RESET #RESTART-DATA
        //*  STORE BLANK RESTART RECORD
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION #RESTART-DATA
        if (condition(pnd_Records_Read.equals(getZero())))                                                                                                                //Natural: IF #RECORDS-READ = 0
        {
            getReports().write(0, NEWLINE,"******************************",NEWLINE,"** ==> EMPTY INPUT FILE <== **",NEWLINE,"******************************");            //Natural: WRITE / '******************************' / '** ==> EMPTY INPUT FILE <== **' / '******************************'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-RUN-STATISTICS
            sub_Write_Run_Statistics();
            if (condition(Global.isEscape())) {return;}
            getReports().write(2, NEWLINE,NEWLINE,"No letters created, normal end of job");                                                                               //Natural: WRITE ( 2 ) // 'No letters created, normal end of job'
            if (Global.isEscape()) return;
            //*  'Normal EOJ but no records read'
            DbsUtil.terminate(1);  if (true) return;                                                                                                                      //Natural: TERMINATE 01
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-RUN-STATISTICS
            sub_Write_Run_Statistics();
            if (condition(Global.isEscape())) {return;}
            getReports().write(2, NEWLINE,NEWLINE,"Letters created, normal end of job");                                                                                  //Natural: WRITE ( 2 ) // 'Letters created, normal end of job'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Run_Statistics() throws Exception                                                                                                              //Natural: WRITE-RUN-STATISTICS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(2, "Total number of participants processed",pnd_All_Plans,NEWLINE,"Total number of plans processed       ",pnd_Total_Plans,NEWLINE,NEWLINE,"    RECORDS READ:",pnd_Records_Read,  //Natural: WRITE ( 2 ) 'Total number of participants processed' #ALL-PLANS / 'Total number of plans processed       ' #TOTAL-PLANS // '    RECORDS READ:' #RECORDS-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'OMNI ERROR COUNT:' #OMNI-ERROR-COUNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'POST ERROR COUNT:' #POST-ERROR-COUNT ( EM = ZZZ,ZZZ,ZZ9 ) / ' LETTERS WRITTEN:' #LETTERS-WRITTEN ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"OMNI ERROR COUNT:",pnd_Omni_Error_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"POST ERROR COUNT:",pnd_Post_Error_Count, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE," LETTERS WRITTEN:",pnd_Letters_Written, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Routine() throws Exception                                                                                                                     //Natural: ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  FORMAT ##MSG
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        pnd_Error_String.setValue(pnd_Win_Title);                                                                                                                         //Natural: ASSIGN #ERROR-STRING := #WIN-TITLE
        pnd_Error_Temp1.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                              //Natural: ASSIGN #ERROR-TEMP1 := MSG-INFO-SUB.##MSG
        pnd_Error_Temp2.setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().getSubstring(41,39));                                                                          //Natural: ASSIGN #ERROR-TEMP2 := SUBSTRING ( MSG-INFO-SUB.##MSG,41,39 )
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pnd_Error_Title.setValue(DbsUtil.compress("ERROR FROM:", pnd_Error_String));                                                                                      //Natural: COMPRESS 'ERROR FROM:' #ERROR-STRING INTO #ERROR-TITLE
        getReports().write(0, " ERROR TITLE:",pnd_Error_Title,NEWLINE," ERROR FIELD:",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(),NEWLINE,"         MSG:",          //Natural: WRITE ' ERROR TITLE:' #ERROR-TITLE / ' ERROR FIELD:' MSG-INFO-SUB.##ERROR-FIELD / '         MSG:' #ERROR-TEMP1 / '             ' #ERROR-TEMP2
            pnd_Error_Temp1,NEWLINE,"             ",pnd_Error_Temp2);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM:",Global.getPROGRAM(),NEWLINE,"ERROR LINE:",Global.getERROR_LINE(),NEWLINE,"ERROR:     ",Global.getERROR_NR());                    //Natural: WRITE 'PROGRAM:' *PROGRAM / 'ERROR LINE:' *ERROR-LINE / 'ERROR:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaAppl165_getAppl165_PlanIsBreak = ldaAppl165.getAppl165_Plan().isBreak(endOfData);
        if (condition(ldaAppl165_getAppl165_PlanIsBreak))
        {
            getReports().write(3, NEWLINE,NEWLINE,new TabSetting(20),"Total participants for Plan",readWork01PlanOld," =",pnd_This_Plan);                                 //Natural: WRITE ( 3 ) // 20T 'Total participants for Plan' OLD ( APPL165.PLAN ) ' =' #THIS-PLAN
            if (condition(Global.isEscape())) return;
            pnd_All_Plans.nadd(pnd_This_Plan);                                                                                                                            //Natural: ADD #THIS-PLAN TO #ALL-PLANS
            pnd_This_Plan.reset();                                                                                                                                        //Natural: RESET #THIS-PLAN
            pnd_Total_Plans.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-PLANS
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=65");
        Global.format(2, "LS=80 PS=65");
        Global.format(3, "LS=80 PS=65");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(9),"ACIS/POST RHSP LETTER Process",new 
            ColumnSpacing(8),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(), new ReportEditMask ("HH:II"),NEWLINE,new ColumnSpacing(25),
            "OMNI STATISTICS REPORT",NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(9),"ACIS/POST RHSP LETTER Process",new 
            ColumnSpacing(8),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(), new ReportEditMask ("HH:II"),NEWLINE,new ColumnSpacing(30),"OMNI SYSTEM",NEWLINE,NEWLINE," PLAN ",new 
            ColumnSpacing(2),"TIAA NBR",new ColumnSpacing(2)," Participant Name",NEWLINE,"======",new ColumnSpacing(2),"==========",NEWLINE,new ColumnSpacing(2),
            "========================================");
    }
}
