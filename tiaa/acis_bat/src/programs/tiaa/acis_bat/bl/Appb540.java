/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:10:01 PM
**        * FROM NATURAL PROGRAM : Appb540
************************************************************
**        * FILE NAME            : Appb540.java
**        * CLASS NAME           : Appb540
**        * INSTANCE NAME        : Appb540
************************************************************
************************************************************************
* PROGRAM  : APPB540 - GENERATES VARIOUS RECONCILIATION REPORTS        *
* FUNCTION : READS REPORT DATAFILE AND GENERATES THE FOLLOWING REPORTS:*
*            1) NEW ENROLLMENTS - NO WELCOME MAIL DATE (NI3502M1)      *
*            2) NEW ENROLLMENTS - DELETED (NI3502M2)                   *
*            3) NEW ENROLLMENTS - NOT FOUND ON ACIS (NI3502M3)         *
*            4) NEW ENROLLMENTS - NOT APPLICABLE (NI3502M4)            *
*            5) NEW ENROLLMENTS - COMPLETED (NI3502M5)                 *
*            6) NEWLY FUNDED - NO LEGAL MAIL DATE (NI3502M6)           *
*            7) NEWLY FUNDED - DELETED (NI3502M7)                      *
*            8) NEWLY FUNDED - NOT FOUND ON ACIS (NI3502M8)            *
*            9) NEWLY FUNDED - NOT APPLICABLE (NI3502M9)               *
*           10) NEWLY FUNDED - COMPLETED (NI3502MA)                    *
*           11) NEWLY FUNDED - PENDING LEGAL (NI3502MB)                *
*           12) ALL NEW ENROLLMENTS/NEWLY FUNDED FROM EDW (NI3502MC)   *
*           13) SUMMARY CONTROL REPORT OF ALL EDW RECORDS (NI3502MD)   *
*           14) ALL OUTSTANDING EXCEPTIONS (NI3502ME)                  *
*           15) ALL RESOLVED EXCEPTIONS (NI3502MF)                     *
*           16) SUMMARY CONTROL REPORT OF ALL OUTSTANDING EXCEPTIONS   *
*               (NI3502MG)                                             *
* HISTORY  :                                                           *
* 08/13/09 C.AVE     - NEW PROGRAM                                     *
* 09/04/09 C.AVE     - ASSIGN 'N/A' IN THE WELCOME MAIL DATE OR LEGAL  *
*                      MAIL DATE IF IT'S BLANK IN THE COMPLETED REPORT *
* 10/22/09 C.AVE     - CHANGE THE DISPLAY FORMAT OF DELETE DATE TO     *
*                      YYYYMMDD FOR CONSISTENCY                        *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb540 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_Rpt_File;

    private DbsGroup pnd_Work_Rpt_File__R_Field_1;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Rec_Id;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Rec_Type;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Category_Ind;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Cref_No;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Plan;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Subplan;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Ssn;

    private DbsGroup pnd_Work_Rpt_File__R_Field_2;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec;

    private DbsGroup pnd_Work_Rpt_File__R_Field_3;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec_Last4;

    private DbsGroup pnd_Work_Rpt_File__R_Field_4;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec_A5;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_5;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_6;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Reprint_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_7;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Date;

    private DbsGroup pnd_Work_Rpt_File__R_Field_8;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Mm;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Dd;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Yy;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_User;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_9;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_10;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Control_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_11;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Control_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Old_Category;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_12;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Exception_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte;

    private DbsGroup pnd_Work_Rpt_File__R_Field_13;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Resolved_Date;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days;

    private DbsGroup pnd_Work_Rpt_File__R_Field_14;
    private DbsField pnd_Work_Rpt_File_Pnd_Wk_Excptn_Age_Days;
    private DbsField pnd_Rpt_Detail;

    private DbsGroup pnd_Rpt_Detail__R_Field_15;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Tiaa_No;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Fill1;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Cref_No;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Fill2;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Ssn;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Fill3;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Plan;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Fill4;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Subplan;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Fill5;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt_Misc;

    private DbsGroup pnd_Rpt_Detail__R_Field_16;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt1_Acis_Entry_Dte;

    private DbsGroup pnd_Rpt_Detail__R_Field_17;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt2_Acis_Entry_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt2_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt2_Delete_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt2_Fill7;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt2_Delete_User;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt2_Fill8;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt2_Delete_Reason;

    private DbsGroup pnd_Rpt_Detail__R_Field_18;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt3_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt3_Fund_Ind;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt3_Fill7;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt3_Funded_Dte;

    private DbsGroup pnd_Rpt_Detail__R_Field_19;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt4_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt4_Pkg_Mail_Type;

    private DbsGroup pnd_Rpt_Detail__R_Field_20;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt5_Welcome_Mail_Dte;

    private DbsGroup pnd_Rpt_Detail__R_Field_21;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt6_Acis_Entry_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt6_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt6_Welcome_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt6_Fill7;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt6_Funded_Dte;

    private DbsGroup pnd_Rpt_Detail__R_Field_22;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt7_Acis_Entry_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt7_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt7_Delete_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt7_Fill7;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt7_Delete_User;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt7_Fill8;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt7_Delete_Reason;

    private DbsGroup pnd_Rpt_Detail__R_Field_23;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt8_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt8_Fund_Ind;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt8_Fill7;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt8_Funded_Dte;

    private DbsGroup pnd_Rpt_Detail__R_Field_24;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt9_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt9_Default_Enroll;

    private DbsGroup pnd_Rpt_Detail__R_Field_25;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt10_Welcome_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt10_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt10_Legal_Mail_Dte;

    private DbsGroup pnd_Rpt_Detail__R_Field_26;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt11_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt11_Fund_Ind;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt11_Fill7;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt11_Funded_Dte;

    private DbsGroup pnd_Rpt_Detail__R_Field_27;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill6;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fund_Ind;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill7;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Funded_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill8;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Default_Enroll;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill9;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Acis_Entry_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill10;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Welcome_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill11;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Legal_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill12;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Pkg_Mail_Type;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill13;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill14;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Deleted_By;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill15;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Rsn;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill16;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Rec_Type;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Fill17;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt12_Category;

    private DbsGroup pnd_Rpt_Detail__R_Field_28;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Funded_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill8;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Acis_Entry_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill10;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Welcome_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill11;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Legal_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill12;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill14;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Deleted_By;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill15;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Rsn;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill16;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Rec_Type;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill17;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Old_Category;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill18;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_New_Category;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill19;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Exception_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Fill20;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt14_Age_In_Days;

    private DbsGroup pnd_Rpt_Detail__R_Field_29;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Funded_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill8;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Acis_Entry_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill10;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Welcome_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill11;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Legal_Mail_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill12;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Dte;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill14;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Deleted_By;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill15;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Rsn;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill16;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Rec_Type;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill17;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Old_Category;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill18;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_New_Category;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Fill19;
    private DbsField pnd_Rpt_Detail_Pnd_Rpt15_Resolved_Dte;
    private DbsField pnd_Current_Dte;
    private DbsField pnd_Current_Dte_N;
    private DbsField pnd_Control_Dte;

    private DbsGroup pnd_Control_Dte__R_Field_30;
    private DbsField pnd_Control_Dte_Pnd_Control_Yyyy;
    private DbsField pnd_Control_Dte_Pnd_Control_Mm;
    private DbsField pnd_Control_Dte_Pnd_Control_Dd;

    private DbsGroup pnd_Control_Dte__R_Field_31;
    private DbsField pnd_Control_Dte_Pnd_Control_Dte_A;
    private DbsField pnd_Curr_Control_Dte;
    private DbsField pnd_From_Control_Dte;
    private DbsField pnd_To_Control_Dte;
    private DbsField pnd_Dted;
    private DbsField pnd_Dte;

    private DbsGroup pnd_Dte__R_Field_32;
    private DbsField pnd_Dte_Pnd_Dte_Yyyy;
    private DbsField pnd_Dte_Pnd_Dte_Mm;
    private DbsField pnd_Dte_Pnd_Dte_Dd;
    private DbsField pnd_Delete_Dte;

    private DbsGroup pnd_Delete_Dte__R_Field_33;
    private DbsField pnd_Delete_Dte_Pnd_Delete_Dte_Yyyy;
    private DbsField pnd_Delete_Dte_Pnd_Delete_Dte_Mm;
    private DbsField pnd_Delete_Dte_Pnd_Delete_Dte_Dd;
    private DbsField pnd_Dlt_Reason_Desc;
    private DbsField pnd_Amt;
    private DbsField pnd_Amt1;
    private DbsField pnd_Total_Rec;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Read;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Deleted;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Read;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Deleted;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttl_Funded_Pending;
    private DbsField pnd_Counters_Pnd_Ttl_Header_Found;
    private DbsField pnd_Counters_Pnd_Ttl_Trailer_Found;
    private DbsField pnd_Counters_Pnd_Gttl_Read;
    private DbsField pnd_Counters_Pnd_Gttl_Completed;
    private DbsField pnd_Counters_Pnd_Gttl_Notcompleted;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Read;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Deleted;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Read;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Deleted;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Not_Found;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt;
    private DbsField pnd_Counters_Pnd_Ttlx_Funded_Pending;
    private DbsField pnd_Counters_Pnd_Gttlx_Read;
    private DbsField pnd_Counters_Pnd_Gttlx_Resolved;
    private DbsField pnd_Counters_Pnd_Gttlx_Notresolved;
    private DbsField pnd_Rpt_Period_Covered;
    private DbsField pnd_Rpt_Curr_Control;
    private DbsField pnd_First_Record;
    private DbsField pnd_Resolved_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_Rpt_File = localVariables.newFieldInRecord("pnd_Work_Rpt_File", "#WORK-RPT-FILE", FieldType.STRING, 150);

        pnd_Work_Rpt_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Rpt_File__R_Field_1", "REDEFINE", pnd_Work_Rpt_File);
        pnd_Work_Rpt_File_Pnd_Wk_Rec_Id = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Rec_Id", "#WK-REC-ID", FieldType.STRING, 
            1);
        pnd_Work_Rpt_File_Pnd_Wk_Rec_Type = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Rec_Type", "#WK-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Work_Rpt_File_Pnd_Wk_Category_Ind = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Category_Ind", "#WK-CATEGORY-IND", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No", "#WK-TIAA-NO", FieldType.STRING, 
            10);
        pnd_Work_Rpt_File_Pnd_Wk_Cref_No = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Cref_No", "#WK-CREF-NO", FieldType.STRING, 
            10);
        pnd_Work_Rpt_File_Pnd_Wk_Plan = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Plan", "#WK-PLAN", FieldType.STRING, 6);
        pnd_Work_Rpt_File_Pnd_Wk_Subplan = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Subplan", "#WK-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Work_Rpt_File_Pnd_Wk_Ssn = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Ssn", "#WK-SSN", FieldType.STRING, 9);

        pnd_Work_Rpt_File__R_Field_2 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_2", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Ssn);
        pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec = pnd_Work_Rpt_File__R_Field_2.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec", "#WK-SOC-SEC", FieldType.NUMERIC, 
            9);

        pnd_Work_Rpt_File__R_Field_3 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_3", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Ssn);
        pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec_Last4 = pnd_Work_Rpt_File__R_Field_3.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec_Last4", "#WK-SOC-SEC-LAST4", 
            FieldType.STRING, 9);

        pnd_Work_Rpt_File__R_Field_4 = pnd_Work_Rpt_File__R_Field_3.newGroupInGroup("pnd_Work_Rpt_File__R_Field_4", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec_Last4);
        pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec_A5 = pnd_Work_Rpt_File__R_Field_4.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Soc_Sec_A5", "#WK-SOC-SEC-A5", FieldType.STRING, 
            5);
        pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind", "#WK-FUNDED-IND", FieldType.STRING, 
            1);
        pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte", "#WK-INIT-FUNDED-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_5 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_5", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Date = pnd_Work_Rpt_File__R_Field_5.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Date", "#WK-INIT-FUNDED-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte", "#WK-REPRINT-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_6 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_6", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Reprint_Date = pnd_Work_Rpt_File__R_Field_6.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Reprint_Date", "#WK-REPRINT-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte", "#WK-DELETE-DTE", FieldType.STRING, 
            8);

        pnd_Work_Rpt_File__R_Field_7 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_7", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Delete_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Date = pnd_Work_Rpt_File__R_Field_7.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Date", "#WK-DELETE-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Work_Rpt_File__R_Field_8 = pnd_Work_Rpt_File__R_Field_7.newGroupInGroup("pnd_Work_Rpt_File__R_Field_8", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Delete_Date);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Mm = pnd_Work_Rpt_File__R_Field_8.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Mm", "#WK-DELETE-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Dd = pnd_Work_Rpt_File__R_Field_8.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Dd", "#WK-DELETE-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Yy = pnd_Work_Rpt_File__R_Field_8.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Yy", "#WK-DELETE-DATE-YY", 
            FieldType.NUMERIC, 4);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_User = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_User", "#WK-DELETE-USER", 
            FieldType.STRING, 16);
        pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason", "#WK-DELETE-REASON", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte", "#WK-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_9 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_9", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date = pnd_Work_Rpt_File__R_Field_9.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date", "#WK-WELCOME-MAIL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte", "#WK-LEGAL-MAIL-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_10 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_10", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date = pnd_Work_Rpt_File__R_Field_10.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date", "#WK-LEGAL-MAIL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Control_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Control_Dte", "#WK-CONTROL-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_11 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_11", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Control_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Control_Date = pnd_Work_Rpt_File__R_Field_11.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Control_Date", "#WK-CONTROL-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll", "#WK-DEFAULT-ENROLL", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type", "#WK-PACKAGE-MAIL-TYPE", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Old_Category = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Old_Category", "#WK-OLD-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte", "#WK-EXCEPTION-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_12 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_12", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Exception_Date = pnd_Work_Rpt_File__R_Field_12.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Exception_Date", "#WK-EXCEPTION-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte", "#WK-RESOLVED-DTE", 
            FieldType.STRING, 8);

        pnd_Work_Rpt_File__R_Field_13 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_13", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte);
        pnd_Work_Rpt_File_Pnd_Wk_Resolved_Date = pnd_Work_Rpt_File__R_Field_13.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Resolved_Date", "#WK-RESOLVED-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days = pnd_Work_Rpt_File__R_Field_1.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days", "#WK-EXCEPTION-AGE-DAYS", 
            FieldType.STRING, 7);

        pnd_Work_Rpt_File__R_Field_14 = pnd_Work_Rpt_File__R_Field_1.newGroupInGroup("pnd_Work_Rpt_File__R_Field_14", "REDEFINE", pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days);
        pnd_Work_Rpt_File_Pnd_Wk_Excptn_Age_Days = pnd_Work_Rpt_File__R_Field_14.newFieldInGroup("pnd_Work_Rpt_File_Pnd_Wk_Excptn_Age_Days", "#WK-EXCPTN-AGE-DAYS", 
            FieldType.NUMERIC, 7);
        pnd_Rpt_Detail = localVariables.newFieldInRecord("pnd_Rpt_Detail", "#RPT-DETAIL", FieldType.STRING, 200);

        pnd_Rpt_Detail__R_Field_15 = localVariables.newGroupInRecord("pnd_Rpt_Detail__R_Field_15", "REDEFINE", pnd_Rpt_Detail);
        pnd_Rpt_Detail_Pnd_Rpt_Tiaa_No = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Tiaa_No", "#RPT-TIAA-NO", FieldType.STRING, 
            10);
        pnd_Rpt_Detail_Pnd_Rpt_Fill1 = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Fill1", "#RPT-FILL1", FieldType.STRING, 4);
        pnd_Rpt_Detail_Pnd_Rpt_Cref_No = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Cref_No", "#RPT-CREF-NO", FieldType.STRING, 
            10);
        pnd_Rpt_Detail_Pnd_Rpt_Fill2 = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Fill2", "#RPT-FILL2", FieldType.STRING, 4);
        pnd_Rpt_Detail_Pnd_Rpt_Ssn = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Ssn", "#RPT-SSN", FieldType.STRING, 10);
        pnd_Rpt_Detail_Pnd_Rpt_Fill3 = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Fill3", "#RPT-FILL3", FieldType.STRING, 2);
        pnd_Rpt_Detail_Pnd_Rpt_Plan = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Plan", "#RPT-PLAN", FieldType.STRING, 6);
        pnd_Rpt_Detail_Pnd_Rpt_Fill4 = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Fill4", "#RPT-FILL4", FieldType.STRING, 2);
        pnd_Rpt_Detail_Pnd_Rpt_Subplan = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Subplan", "#RPT-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Rpt_Detail_Pnd_Rpt_Fill5 = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Fill5", "#RPT-FILL5", FieldType.STRING, 3);
        pnd_Rpt_Detail_Pnd_Rpt_Misc = pnd_Rpt_Detail__R_Field_15.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt_Misc", "#RPT-MISC", FieldType.STRING, 140);

        pnd_Rpt_Detail__R_Field_16 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_16", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt1_Acis_Entry_Dte = pnd_Rpt_Detail__R_Field_16.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt1_Acis_Entry_Dte", "#RPT1-ACIS-ENTRY-DTE", 
            FieldType.STRING, 10);

        pnd_Rpt_Detail__R_Field_17 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_17", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt2_Acis_Entry_Dte = pnd_Rpt_Detail__R_Field_17.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt2_Acis_Entry_Dte", "#RPT2-ACIS-ENTRY-DTE", 
            FieldType.STRING, 10);
        pnd_Rpt_Detail_Pnd_Rpt2_Fill6 = pnd_Rpt_Detail__R_Field_17.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt2_Fill6", "#RPT2-FILL6", FieldType.STRING, 2);
        pnd_Rpt_Detail_Pnd_Rpt2_Delete_Dte = pnd_Rpt_Detail__R_Field_17.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt2_Delete_Dte", "#RPT2-DELETE-DTE", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt2_Fill7 = pnd_Rpt_Detail__R_Field_17.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt2_Fill7", "#RPT2-FILL7", FieldType.STRING, 3);
        pnd_Rpt_Detail_Pnd_Rpt2_Delete_User = pnd_Rpt_Detail__R_Field_17.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt2_Delete_User", "#RPT2-DELETE-USER", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt2_Fill8 = pnd_Rpt_Detail__R_Field_17.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt2_Fill8", "#RPT2-FILL8", FieldType.STRING, 2);
        pnd_Rpt_Detail_Pnd_Rpt2_Delete_Reason = pnd_Rpt_Detail__R_Field_17.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt2_Delete_Reason", "#RPT2-DELETE-REASON", 
            FieldType.STRING, 30);

        pnd_Rpt_Detail__R_Field_18 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_18", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt3_Fill6 = pnd_Rpt_Detail__R_Field_18.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt3_Fill6", "#RPT3-FILL6", FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt3_Fund_Ind = pnd_Rpt_Detail__R_Field_18.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt3_Fund_Ind", "#RPT3-FUND-IND", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt3_Fill7 = pnd_Rpt_Detail__R_Field_18.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt3_Fill7", "#RPT3-FILL7", FieldType.STRING, 6);
        pnd_Rpt_Detail_Pnd_Rpt3_Funded_Dte = pnd_Rpt_Detail__R_Field_18.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt3_Funded_Dte", "#RPT3-FUNDED-DTE", FieldType.STRING, 
            8);

        pnd_Rpt_Detail__R_Field_19 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_19", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt4_Fill6 = pnd_Rpt_Detail__R_Field_19.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt4_Fill6", "#RPT4-FILL6", FieldType.STRING, 3);
        pnd_Rpt_Detail_Pnd_Rpt4_Pkg_Mail_Type = pnd_Rpt_Detail__R_Field_19.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt4_Pkg_Mail_Type", "#RPT4-PKG-MAIL-TYPE", 
            FieldType.STRING, 1);

        pnd_Rpt_Detail__R_Field_20 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_20", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt5_Welcome_Mail_Dte = pnd_Rpt_Detail__R_Field_20.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt5_Welcome_Mail_Dte", "#RPT5-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);

        pnd_Rpt_Detail__R_Field_21 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_21", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt6_Acis_Entry_Dte = pnd_Rpt_Detail__R_Field_21.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt6_Acis_Entry_Dte", "#RPT6-ACIS-ENTRY-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt6_Fill6 = pnd_Rpt_Detail__R_Field_21.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt6_Fill6", "#RPT6-FILL6", FieldType.STRING, 4);
        pnd_Rpt_Detail_Pnd_Rpt6_Welcome_Mail_Dte = pnd_Rpt_Detail__R_Field_21.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt6_Welcome_Mail_Dte", "#RPT6-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt6_Fill7 = pnd_Rpt_Detail__R_Field_21.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt6_Fill7", "#RPT6-FILL7", FieldType.STRING, 4);
        pnd_Rpt_Detail_Pnd_Rpt6_Funded_Dte = pnd_Rpt_Detail__R_Field_21.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt6_Funded_Dte", "#RPT6-FUNDED-DTE", FieldType.STRING, 
            8);

        pnd_Rpt_Detail__R_Field_22 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_22", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt7_Acis_Entry_Dte = pnd_Rpt_Detail__R_Field_22.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt7_Acis_Entry_Dte", "#RPT7-ACIS-ENTRY-DTE", 
            FieldType.STRING, 10);
        pnd_Rpt_Detail_Pnd_Rpt7_Fill6 = pnd_Rpt_Detail__R_Field_22.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt7_Fill6", "#RPT7-FILL6", FieldType.STRING, 2);
        pnd_Rpt_Detail_Pnd_Rpt7_Delete_Dte = pnd_Rpt_Detail__R_Field_22.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt7_Delete_Dte", "#RPT7-DELETE-DTE", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt7_Fill7 = pnd_Rpt_Detail__R_Field_22.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt7_Fill7", "#RPT7-FILL7", FieldType.STRING, 3);
        pnd_Rpt_Detail_Pnd_Rpt7_Delete_User = pnd_Rpt_Detail__R_Field_22.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt7_Delete_User", "#RPT7-DELETE-USER", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt7_Fill8 = pnd_Rpt_Detail__R_Field_22.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt7_Fill8", "#RPT7-FILL8", FieldType.STRING, 2);
        pnd_Rpt_Detail_Pnd_Rpt7_Delete_Reason = pnd_Rpt_Detail__R_Field_22.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt7_Delete_Reason", "#RPT7-DELETE-REASON", 
            FieldType.STRING, 30);

        pnd_Rpt_Detail__R_Field_23 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_23", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt8_Fill6 = pnd_Rpt_Detail__R_Field_23.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt8_Fill6", "#RPT8-FILL6", FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt8_Fund_Ind = pnd_Rpt_Detail__R_Field_23.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt8_Fund_Ind", "#RPT8-FUND-IND", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt8_Fill7 = pnd_Rpt_Detail__R_Field_23.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt8_Fill7", "#RPT8-FILL7", FieldType.STRING, 6);
        pnd_Rpt_Detail_Pnd_Rpt8_Funded_Dte = pnd_Rpt_Detail__R_Field_23.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt8_Funded_Dte", "#RPT8-FUNDED-DTE", FieldType.STRING, 
            8);

        pnd_Rpt_Detail__R_Field_24 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_24", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt9_Fill6 = pnd_Rpt_Detail__R_Field_24.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt9_Fill6", "#RPT9-FILL6", FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt9_Default_Enroll = pnd_Rpt_Detail__R_Field_24.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt9_Default_Enroll", "#RPT9-DEFAULT-ENROLL", 
            FieldType.STRING, 1);

        pnd_Rpt_Detail__R_Field_25 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_25", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt10_Welcome_Mail_Dte = pnd_Rpt_Detail__R_Field_25.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt10_Welcome_Mail_Dte", "#RPT10-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt10_Fill6 = pnd_Rpt_Detail__R_Field_25.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt10_Fill6", "#RPT10-FILL6", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt10_Legal_Mail_Dte = pnd_Rpt_Detail__R_Field_25.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt10_Legal_Mail_Dte", "#RPT10-LEGAL-MAIL-DTE", 
            FieldType.STRING, 8);

        pnd_Rpt_Detail__R_Field_26 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_26", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt11_Fill6 = pnd_Rpt_Detail__R_Field_26.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt11_Fill6", "#RPT11-FILL6", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt11_Fund_Ind = pnd_Rpt_Detail__R_Field_26.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt11_Fund_Ind", "#RPT11-FUND-IND", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt11_Fill7 = pnd_Rpt_Detail__R_Field_26.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt11_Fill7", "#RPT11-FILL7", FieldType.STRING, 
            6);
        pnd_Rpt_Detail_Pnd_Rpt11_Funded_Dte = pnd_Rpt_Detail__R_Field_26.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt11_Funded_Dte", "#RPT11-FUNDED-DTE", FieldType.STRING, 
            8);

        pnd_Rpt_Detail__R_Field_27 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_27", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill6 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill6", "#RPT12-FILL6", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt12_Fund_Ind = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fund_Ind", "#RPT12-FUND-IND", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill7 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill7", "#RPT12-FILL7", FieldType.STRING, 
            6);
        pnd_Rpt_Detail_Pnd_Rpt12_Funded_Dte = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Funded_Dte", "#RPT12-FUNDED-DTE", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill8 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill8", "#RPT12-FILL8", FieldType.STRING, 
            5);
        pnd_Rpt_Detail_Pnd_Rpt12_Default_Enroll = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Default_Enroll", "#RPT12-DEFAULT-ENROLL", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill9 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill9", "#RPT12-FILL9", FieldType.STRING, 
            7);
        pnd_Rpt_Detail_Pnd_Rpt12_Acis_Entry_Dte = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Acis_Entry_Dte", "#RPT12-ACIS-ENTRY-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill10 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill10", "#RPT12-FILL10", FieldType.STRING, 
            4);
        pnd_Rpt_Detail_Pnd_Rpt12_Welcome_Mail_Dte = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Welcome_Mail_Dte", "#RPT12-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill11 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill11", "#RPT12-FILL11", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt12_Legal_Mail_Dte = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Legal_Mail_Dte", "#RPT12-LEGAL-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill12 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill12", "#RPT12-FILL12", FieldType.STRING, 
            6);
        pnd_Rpt_Detail_Pnd_Rpt12_Pkg_Mail_Type = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Pkg_Mail_Type", "#RPT12-PKG-MAIL-TYPE", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill13 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill13", "#RPT12-FILL13", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Dte = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Dte", "#RPT12-DELETED-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill14 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill14", "#RPT12-FILL14", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt12_Deleted_By = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Deleted_By", "#RPT12-DELETED-BY", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill15 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill15", "#RPT12-FILL15", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Rsn = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Rsn", "#RPT12-DELETED-RSN", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill16 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill16", "#RPT12-FILL16", FieldType.STRING, 
            5);
        pnd_Rpt_Detail_Pnd_Rpt12_Rec_Type = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Rec_Type", "#RPT12-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt12_Fill17 = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Fill17", "#RPT12-FILL17", FieldType.STRING, 
            4);
        pnd_Rpt_Detail_Pnd_Rpt12_Category = pnd_Rpt_Detail__R_Field_27.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt12_Category", "#RPT12-CATEGORY", FieldType.STRING, 
            1);

        pnd_Rpt_Detail__R_Field_28 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_28", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt14_Funded_Dte = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Funded_Dte", "#RPT14-FUNDED-DTE", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill8 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill8", "#RPT14-FILL8", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt14_Acis_Entry_Dte = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Acis_Entry_Dte", "#RPT14-ACIS-ENTRY-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill10 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill10", "#RPT14-FILL10", FieldType.STRING, 
            4);
        pnd_Rpt_Detail_Pnd_Rpt14_Welcome_Mail_Dte = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Welcome_Mail_Dte", "#RPT14-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill11 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill11", "#RPT14-FILL11", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt14_Legal_Mail_Dte = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Legal_Mail_Dte", "#RPT14-LEGAL-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill12 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill12", "#RPT14-FILL12", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Dte = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Dte", "#RPT14-DELETED-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill14 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill14", "#RPT14-FILL14", FieldType.STRING, 
            2);
        pnd_Rpt_Detail_Pnd_Rpt14_Deleted_By = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Deleted_By", "#RPT14-DELETED-BY", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill15 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill15", "#RPT14-FILL15", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Rsn = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Rsn", "#RPT14-DELETED-RSN", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill16 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill16", "#RPT14-FILL16", FieldType.STRING, 
            5);
        pnd_Rpt_Detail_Pnd_Rpt14_Rec_Type = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Rec_Type", "#RPT14-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill17 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill17", "#RPT14-FILL17", FieldType.STRING, 
            5);
        pnd_Rpt_Detail_Pnd_Rpt14_Old_Category = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Old_Category", "#RPT14-OLD-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill18 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill18", "#RPT14-FILL18", FieldType.STRING, 
            6);
        pnd_Rpt_Detail_Pnd_Rpt14_New_Category = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_New_Category", "#RPT14-NEW-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill19 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill19", "#RPT14-FILL19", FieldType.STRING, 
            4);
        pnd_Rpt_Detail_Pnd_Rpt14_Exception_Dte = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Exception_Dte", "#RPT14-EXCEPTION-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt14_Fill20 = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Fill20", "#RPT14-FILL20", FieldType.STRING, 
            2);
        pnd_Rpt_Detail_Pnd_Rpt14_Age_In_Days = pnd_Rpt_Detail__R_Field_28.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt14_Age_In_Days", "#RPT14-AGE-IN-DAYS", 
            FieldType.STRING, 7);

        pnd_Rpt_Detail__R_Field_29 = pnd_Rpt_Detail__R_Field_15.newGroupInGroup("pnd_Rpt_Detail__R_Field_29", "REDEFINE", pnd_Rpt_Detail_Pnd_Rpt_Misc);
        pnd_Rpt_Detail_Pnd_Rpt15_Funded_Dte = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Funded_Dte", "#RPT15-FUNDED-DTE", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill8 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill8", "#RPT15-FILL8", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt15_Acis_Entry_Dte = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Acis_Entry_Dte", "#RPT15-ACIS-ENTRY-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill10 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill10", "#RPT15-FILL10", FieldType.STRING, 
            4);
        pnd_Rpt_Detail_Pnd_Rpt15_Welcome_Mail_Dte = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Welcome_Mail_Dte", "#RPT15-WELCOME-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill11 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill11", "#RPT15-FILL11", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt15_Legal_Mail_Dte = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Legal_Mail_Dte", "#RPT15-LEGAL-MAIL-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill12 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill12", "#RPT15-FILL12", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Dte = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Dte", "#RPT15-DELETED-DTE", 
            FieldType.STRING, 8);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill14 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill14", "#RPT15-FILL14", FieldType.STRING, 
            2);
        pnd_Rpt_Detail_Pnd_Rpt15_Deleted_By = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Deleted_By", "#RPT15-DELETED-BY", FieldType.STRING, 
            8);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill15 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill15", "#RPT15-FILL15", FieldType.STRING, 
            3);
        pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Rsn = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Rsn", "#RPT15-DELETED-RSN", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill16 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill16", "#RPT15-FILL16", FieldType.STRING, 
            5);
        pnd_Rpt_Detail_Pnd_Rpt15_Rec_Type = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Rec_Type", "#RPT15-REC-TYPE", FieldType.STRING, 
            1);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill17 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill17", "#RPT15-FILL17", FieldType.STRING, 
            5);
        pnd_Rpt_Detail_Pnd_Rpt15_Old_Category = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Old_Category", "#RPT15-OLD-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill18 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill18", "#RPT15-FILL18", FieldType.STRING, 
            6);
        pnd_Rpt_Detail_Pnd_Rpt15_New_Category = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_New_Category", "#RPT15-NEW-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Rpt_Detail_Pnd_Rpt15_Fill19 = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Fill19", "#RPT15-FILL19", FieldType.STRING, 
            4);
        pnd_Rpt_Detail_Pnd_Rpt15_Resolved_Dte = pnd_Rpt_Detail__R_Field_29.newFieldInGroup("pnd_Rpt_Detail_Pnd_Rpt15_Resolved_Dte", "#RPT15-RESOLVED-DTE", 
            FieldType.STRING, 8);
        pnd_Current_Dte = localVariables.newFieldInRecord("pnd_Current_Dte", "#CURRENT-DTE", FieldType.DATE);
        pnd_Current_Dte_N = localVariables.newFieldInRecord("pnd_Current_Dte_N", "#CURRENT-DTE-N", FieldType.NUMERIC, 8);
        pnd_Control_Dte = localVariables.newFieldInRecord("pnd_Control_Dte", "#CONTROL-DTE", FieldType.NUMERIC, 8);

        pnd_Control_Dte__R_Field_30 = localVariables.newGroupInRecord("pnd_Control_Dte__R_Field_30", "REDEFINE", pnd_Control_Dte);
        pnd_Control_Dte_Pnd_Control_Yyyy = pnd_Control_Dte__R_Field_30.newFieldInGroup("pnd_Control_Dte_Pnd_Control_Yyyy", "#CONTROL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Control_Dte_Pnd_Control_Mm = pnd_Control_Dte__R_Field_30.newFieldInGroup("pnd_Control_Dte_Pnd_Control_Mm", "#CONTROL-MM", FieldType.NUMERIC, 
            2);
        pnd_Control_Dte_Pnd_Control_Dd = pnd_Control_Dte__R_Field_30.newFieldInGroup("pnd_Control_Dte_Pnd_Control_Dd", "#CONTROL-DD", FieldType.NUMERIC, 
            2);

        pnd_Control_Dte__R_Field_31 = localVariables.newGroupInRecord("pnd_Control_Dte__R_Field_31", "REDEFINE", pnd_Control_Dte);
        pnd_Control_Dte_Pnd_Control_Dte_A = pnd_Control_Dte__R_Field_31.newFieldInGroup("pnd_Control_Dte_Pnd_Control_Dte_A", "#CONTROL-DTE-A", FieldType.STRING, 
            8);
        pnd_Curr_Control_Dte = localVariables.newFieldInRecord("pnd_Curr_Control_Dte", "#CURR-CONTROL-DTE", FieldType.STRING, 8);
        pnd_From_Control_Dte = localVariables.newFieldInRecord("pnd_From_Control_Dte", "#FROM-CONTROL-DTE", FieldType.STRING, 8);
        pnd_To_Control_Dte = localVariables.newFieldInRecord("pnd_To_Control_Dte", "#TO-CONTROL-DTE", FieldType.STRING, 8);
        pnd_Dted = localVariables.newFieldInRecord("pnd_Dted", "#DTED", FieldType.DATE);
        pnd_Dte = localVariables.newFieldInRecord("pnd_Dte", "#DTE", FieldType.STRING, 8);

        pnd_Dte__R_Field_32 = localVariables.newGroupInRecord("pnd_Dte__R_Field_32", "REDEFINE", pnd_Dte);
        pnd_Dte_Pnd_Dte_Yyyy = pnd_Dte__R_Field_32.newFieldInGroup("pnd_Dte_Pnd_Dte_Yyyy", "#DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Dte_Pnd_Dte_Mm = pnd_Dte__R_Field_32.newFieldInGroup("pnd_Dte_Pnd_Dte_Mm", "#DTE-MM", FieldType.NUMERIC, 2);
        pnd_Dte_Pnd_Dte_Dd = pnd_Dte__R_Field_32.newFieldInGroup("pnd_Dte_Pnd_Dte_Dd", "#DTE-DD", FieldType.NUMERIC, 2);
        pnd_Delete_Dte = localVariables.newFieldInRecord("pnd_Delete_Dte", "#DELETE-DTE", FieldType.STRING, 8);

        pnd_Delete_Dte__R_Field_33 = localVariables.newGroupInRecord("pnd_Delete_Dte__R_Field_33", "REDEFINE", pnd_Delete_Dte);
        pnd_Delete_Dte_Pnd_Delete_Dte_Yyyy = pnd_Delete_Dte__R_Field_33.newFieldInGroup("pnd_Delete_Dte_Pnd_Delete_Dte_Yyyy", "#DELETE-DTE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Delete_Dte_Pnd_Delete_Dte_Mm = pnd_Delete_Dte__R_Field_33.newFieldInGroup("pnd_Delete_Dte_Pnd_Delete_Dte_Mm", "#DELETE-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Delete_Dte_Pnd_Delete_Dte_Dd = pnd_Delete_Dte__R_Field_33.newFieldInGroup("pnd_Delete_Dte_Pnd_Delete_Dte_Dd", "#DELETE-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Dlt_Reason_Desc = localVariables.newFieldInRecord("pnd_Dlt_Reason_Desc", "#DLT-REASON-DESC", FieldType.STRING, 26);
        pnd_Amt = localVariables.newFieldInRecord("pnd_Amt", "#AMT", FieldType.STRING, 15);
        pnd_Amt1 = localVariables.newFieldInRecord("pnd_Amt1", "#AMT1", FieldType.PACKED_DECIMAL, 8);
        pnd_Total_Rec = localVariables.newFieldInRecord("pnd_Total_Rec", "#TOTAL-REC", FieldType.STRING, 30);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Ttl_Enroll_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Read", "#TTL-ENROLL-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt", "#TTL-ENROLL-WITH-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt", "#TTL-ENROLL-NO-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Enroll_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Deleted", "#TTL-ENROLL-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Enroll_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Not_Found", "#TTL-ENROLL-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable", "#TTL-ENROLL-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Read", "#TTL-FUNDED-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt", "#TTL-FUNDED-WITH-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt", "#TTL-FUNDED-NO-LEGAL-DT", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Deleted", "#TTL-FUNDED-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Not_Found", "#TTL-FUNDED-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Funded_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Not_Applicable", "#TTL-FUNDED-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt", "#TTL-FUNDED-DFLT-NO-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttl_Funded_Pending = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Funded_Pending", "#TTL-FUNDED-PENDING", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Header_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Header_Found", "#TTL-HEADER-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttl_Trailer_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttl_Trailer_Found", "#TTL-TRAILER-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Gttl_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gttl_Read", "#GTTL-READ", FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Gttl_Completed = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gttl_Completed", "#GTTL-COMPLETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Gttl_Notcompleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gttl_Notcompleted", "#GTTL-NOTCOMPLETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Read", "#TTLX-ENROLL-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt", "#TTLX-ENROLL-WITH-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt", "#TTLX-ENROLL-NO-WELCOME-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Enroll_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Deleted", "#TTLX-ENROLL-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Not_Found", "#TTLX-ENROLL-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable", "#TTLX-ENROLL-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Read", "#TTLX-FUNDED-READ", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt", "#TTLX-FUNDED-WITH-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt", "#TTLX-FUNDED-NO-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Deleted = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Deleted", "#TTLX-FUNDED-DELETED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Funded_Not_Found = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Not_Found", "#TTLX-FUNDED-NOT-FOUND", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable", "#TTLX-FUNDED-NOT-APPLICABLE", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt", "#TTLX-FUNDED-DFLT-NO-LEGAL-DT", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Ttlx_Funded_Pending = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ttlx_Funded_Pending", "#TTLX-FUNDED-PENDING", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Gttlx_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gttlx_Read", "#GTTLX-READ", FieldType.PACKED_DECIMAL, 8);
        pnd_Counters_Pnd_Gttlx_Resolved = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gttlx_Resolved", "#GTTLX-RESOLVED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Counters_Pnd_Gttlx_Notresolved = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Gttlx_Notresolved", "#GTTLX-NOTRESOLVED", FieldType.PACKED_DECIMAL, 
            8);
        pnd_Rpt_Period_Covered = localVariables.newFieldInRecord("pnd_Rpt_Period_Covered", "#RPT-PERIOD-COVERED", FieldType.STRING, 50);
        pnd_Rpt_Curr_Control = localVariables.newFieldInRecord("pnd_Rpt_Curr_Control", "#RPT-CURR-CONTROL", FieldType.STRING, 50);
        pnd_First_Record = localVariables.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Resolved_Ind = localVariables.newFieldInRecord("pnd_Resolved_Ind", "#RESOLVED-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb540() throws Exception
    {
        super("Appb540");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB540", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        getReports().atTopOfPage(atTopEventRpt8, 8);
        getReports().atTopOfPage(atTopEventRpt9, 9);
        getReports().atTopOfPage(atTopEventRpt10, 10);
        getReports().atTopOfPage(atTopEventRpt11, 11);
        getReports().atTopOfPage(atTopEventRpt12, 12);
        getReports().atTopOfPage(atTopEventRpt14, 14);
        getReports().atTopOfPage(atTopEventRpt15, 15);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 210 PS = 52;//Natural: FORMAT ( 2 ) LS = 210 PS = 52;//Natural: FORMAT ( 3 ) LS = 210 PS = 52;//Natural: FORMAT ( 4 ) LS = 210 PS = 52;//Natural: FORMAT ( 5 ) LS = 210 PS = 52;//Natural: FORMAT ( 6 ) LS = 210 PS = 52;//Natural: FORMAT ( 7 ) LS = 210 PS = 52;//Natural: FORMAT ( 8 ) LS = 210 PS = 52;//Natural: FORMAT ( 9 ) LS = 210 PS = 52;//Natural: FORMAT ( 10 ) LS = 210 PS = 52;//Natural: FORMAT ( 11 ) LS = 210 PS = 52;//Natural: FORMAT ( 12 ) LS = 210 PS = 52;//Natural: FORMAT ( 13 ) LS = 210 PS = 52;//Natural: FORMAT ( 14 ) LS = 210 PS = 52;//Natural: FORMAT ( 15 ) LS = 210 PS = 52;//Natural: FORMAT ( 16 ) LS = 210 PS = 52
        pnd_First_Record.setValue(true);                                                                                                                                  //Natural: ASSIGN #FIRST-RECORD := TRUE
        pnd_Current_Dte.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN #CURRENT-DTE := *DATX
        pnd_Current_Dte_N.setValue(Global.getDATN());                                                                                                                     //Natural: ASSIGN #CURRENT-DTE-N := *DATN
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 );//Natural: AT TOP OF PAGE ( 4 );//Natural: AT TOP OF PAGE ( 5 );//Natural: AT TOP OF PAGE ( 6 );//Natural: AT TOP OF PAGE ( 7 );//Natural: AT TOP OF PAGE ( 8 );//Natural: AT TOP OF PAGE ( 9 );//Natural: AT TOP OF PAGE ( 10 );//Natural: AT TOP OF PAGE ( 11 );//Natural: AT TOP OF PAGE ( 12 )
        //*  REPORT 13 IS THE SUMMARY REPORT (REGULAR RECORDS FROM EDW)
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 14 );//Natural: AT TOP OF PAGE ( 15 )
        //*  REPORT 16 IS THE SUMMARY REPORT (EXCEPTION RECORDS - AGING)
        //*  READ REPORT FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-RPT-FILE
        while (condition(getWorkFiles().read(1, pnd_Work_Rpt_File)))
        {
            if (condition(pnd_First_Record.getBoolean()))                                                                                                                 //Natural: IF #FIRST-RECORD
            {
                if (condition(pnd_Work_Rpt_File_Pnd_Wk_Rec_Id.equals("1")))                                                                                               //Natural: IF #WK-REC-ID = '1'
                {
                    pnd_Control_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Control_Date);                                                                                      //Natural: ASSIGN #CONTROL-DTE := #WK-CONTROL-DATE
                    //*  '3'- EXCEPTIONS (THERE ARE NO REGULAR RECORDS)
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Control_Dte.setValue(pnd_Current_Dte_N);                                                                                                          //Natural: ASSIGN #CONTROL-DTE := #CURRENT-DTE-N
                    //*  SET TO DAY 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Control_Dte_Pnd_Control_Dd.setValue(1);                                                                                                               //Natural: ASSIGN #CONTROL-DD := 01
                pnd_Dted.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Control_Dte_Pnd_Control_Dte_A);                                                                //Natural: MOVE EDITED #CONTROL-DTE-A TO #DTED ( EM = YYYYMMDD )
                pnd_Curr_Control_Dte.setValueEdited(pnd_Dted,new ReportEditMask("MM/DD/YY"));                                                                             //Natural: MOVE EDITED #DTED ( EM = MM/DD/YY ) TO #CURR-CONTROL-DTE
                //*    DERIVE THE "To" PERIOD
                pnd_Dted.nsubtract(1);                                                                                                                                    //Natural: SUBTRACT 1 FROM #DTED
                pnd_To_Control_Dte.setValueEdited(pnd_Dted,new ReportEditMask("MM/DD/YY"));                                                                               //Natural: MOVE EDITED #DTED ( EM = MM/DD/YY ) TO #TO-CONTROL-DTE
                //*    DERIVE THE "From" PERIOD
                pnd_Dte.setValueEdited(pnd_Dted,new ReportEditMask("YYYYMMDD"));                                                                                          //Natural: MOVE EDITED #DTED ( EM = YYYYMMDD ) TO #DTE
                pnd_Dte_Pnd_Dte_Dd.setValue(1);                                                                                                                           //Natural: ASSIGN #DTE-DD := 01
                pnd_Dted.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Dte);                                                                                          //Natural: MOVE EDITED #DTE TO #DTED ( EM = YYYYMMDD )
                pnd_From_Control_Dte.setValueEdited(pnd_Dted,new ReportEditMask("MM/DD/YY"));                                                                             //Natural: MOVE EDITED #DTED ( EM = MM/DD/YY ) TO #FROM-CONTROL-DTE
                pnd_Rpt_Period_Covered.setValue(DbsUtil.compress("Period Covered:", pnd_From_Control_Dte, "-", pnd_To_Control_Dte));                                      //Natural: COMPRESS 'Period Covered:' #FROM-CONTROL-DTE '-' #TO-CONTROL-DTE INTO #RPT-PERIOD-COVERED
                pnd_Rpt_Curr_Control.setValue(DbsUtil.compress("For Control Date:", pnd_Curr_Control_Dte));                                                               //Natural: COMPRESS 'For Control Date:' #CURR-CONTROL-DTE INTO #RPT-CURR-CONTROL
                pnd_First_Record.setValue(false);                                                                                                                         //Natural: ASSIGN #FIRST-RECORD := FALSE
                getReports().write(0, "CONTROL-DTE:",pnd_Control_Dte);                                                                                                    //Natural: WRITE 'CONTROL-DTE:' #CONTROL-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rpt_Detail.reset();                                                                                                                                       //Natural: RESET #RPT-DETAIL
            pnd_Rpt_Detail_Pnd_Rpt_Tiaa_No.setValue(pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No);                                                                                    //Natural: ASSIGN #RPT-TIAA-NO := #WK-TIAA-NO
            pnd_Rpt_Detail_Pnd_Rpt_Cref_No.setValue(pnd_Work_Rpt_File_Pnd_Wk_Cref_No);                                                                                    //Natural: ASSIGN #RPT-CREF-NO := #WK-CREF-NO
            pnd_Rpt_Detail_Pnd_Rpt_Ssn.setValue(pnd_Work_Rpt_File_Pnd_Wk_Ssn);                                                                                            //Natural: ASSIGN #RPT-SSN := #WK-SSN
            pnd_Rpt_Detail_Pnd_Rpt_Plan.setValue(pnd_Work_Rpt_File_Pnd_Wk_Plan);                                                                                          //Natural: ASSIGN #RPT-PLAN := #WK-PLAN
            pnd_Rpt_Detail_Pnd_Rpt_Subplan.setValue(pnd_Work_Rpt_File_Pnd_Wk_Subplan);                                                                                    //Natural: ASSIGN #RPT-SUBPLAN := #WK-SUBPLAN
            short decideConditionsMet399 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #WK-DELETE-REASON;//Natural: VALUE '1'
            if (condition((pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason.equals("1"))))
            {
                decideConditionsMet399++;
                pnd_Dlt_Reason_Desc.setValue("-Issued in Error-Inst.");                                                                                                   //Natural: ASSIGN #DLT-REASON-DESC := '-Issued in Error-Inst.'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason.equals("2"))))
            {
                decideConditionsMet399++;
                pnd_Dlt_Reason_Desc.setValue("-Issued in Error-Processor");                                                                                               //Natural: ASSIGN #DLT-REASON-DESC := '-Issued in Error-Processor'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason.equals("3"))))
            {
                decideConditionsMet399++;
                pnd_Dlt_Reason_Desc.setValue("-Contract already exists");                                                                                                 //Natural: ASSIGN #DLT-REASON-DESC := '-Contract already exists'
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason.equals("4"))))
            {
                decideConditionsMet399++;
                pnd_Dlt_Reason_Desc.setValue("-Test Contract");                                                                                                           //Natural: ASSIGN #DLT-REASON-DESC := '-Test Contract'
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason.equals("5"))))
            {
                decideConditionsMet399++;
                pnd_Dlt_Reason_Desc.setValue("-OMNI Delete");                                                                                                             //Natural: ASSIGN #DLT-REASON-DESC := '-OMNI Delete'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Dlt_Reason_Desc.setValue(" ");                                                                                                                        //Natural: ASSIGN #DLT-REASON-DESC := ' '
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  FOR THE DELETE DATE TO YYYYMMDD
            pnd_Delete_Dte.reset();                                                                                                                                       //Natural: RESET #DELETE-DTE
            pnd_Delete_Dte_Pnd_Delete_Dte_Yyyy.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Yy);                                                                         //Natural: ASSIGN #DELETE-DTE-YYYY := #WK-DELETE-DATE-YY
            pnd_Delete_Dte_Pnd_Delete_Dte_Mm.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Mm);                                                                           //Natural: ASSIGN #DELETE-DTE-MM := #WK-DELETE-DATE-MM
            pnd_Delete_Dte_Pnd_Delete_Dte_Dd.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_Date_Dd);                                                                           //Natural: ASSIGN #DELETE-DTE-DD := #WK-DELETE-DATE-DD
            //*  REGULAR RECORDS FROM EDW
            if (condition(pnd_Work_Rpt_File_Pnd_Wk_Rec_Id.equals("1")))                                                                                                   //Natural: IF #WK-REC-ID = '1'
            {
                //*  WELCOME PKG. (NEW ENROLLMENTS)
                short decideConditionsMet422 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #WK-REC-TYPE;//Natural: VALUE 'W'
                if (condition((pnd_Work_Rpt_File_Pnd_Wk_Rec_Type.equals("W"))))
                {
                    decideConditionsMet422++;
                    pnd_Counters_Pnd_Ttl_Enroll_Read.nadd(1);                                                                                                             //Natural: ADD 1 TO #TTL-ENROLL-READ
                    //*  REPORT 1 (EXCEPTION-NO WELCOME MAIL DATE)
                    short decideConditionsMet426 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE #WK-CATEGORY-IND;//Natural: VALUE 'E'
                    if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("E"))))
                    {
                        decideConditionsMet426++;
                        pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt.nadd(1);                                                                                                //Natural: ADD 1 TO #TTL-ENROLL-NO-WELCOME-DT
                        pnd_Rpt_Detail_Pnd_Rpt1_Acis_Entry_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);                                                            //Natural: ASSIGN #RPT1-ACIS-ENTRY-DTE := #WK-REPRINT-DTE
                        //*  REPORT 2 (DELETED)
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 1 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'D'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("D"))))
                    {
                        decideConditionsMet426++;
                        pnd_Counters_Pnd_Ttl_Enroll_Deleted.nadd(1);                                                                                                      //Natural: ADD 1 TO #TTL-ENROLL-DELETED
                        pnd_Rpt_Detail_Pnd_Rpt2_Acis_Entry_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);                                                            //Natural: ASSIGN #RPT2-ACIS-ENTRY-DTE := #WK-REPRINT-DTE
                        pnd_Rpt_Detail_Pnd_Rpt2_Delete_Dte.setValue(pnd_Delete_Dte);                                                                                      //Natural: ASSIGN #RPT2-DELETE-DTE := #DELETE-DTE
                        pnd_Rpt_Detail_Pnd_Rpt2_Delete_User.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_User);                                                               //Natural: ASSIGN #RPT2-DELETE-USER := #WK-DELETE-USER
                        pnd_Rpt_Detail_Pnd_Rpt2_Delete_Reason.setValue(DbsUtil.compress(pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason, pnd_Dlt_Reason_Desc));                    //Natural: COMPRESS #WK-DELETE-REASON #DLT-REASON-DESC INTO #RPT2-DELETE-REASON
                        //*  REPORT 3 (NOT FOUND ON ACIS)
                        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 2 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'F'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("F"))))
                    {
                        decideConditionsMet426++;
                        pnd_Counters_Pnd_Ttl_Enroll_Not_Found.nadd(1);                                                                                                    //Natural: ADD 1 TO #TTL-ENROLL-NOT-FOUND
                        //* *         #RPT3-FUND-IND   := #WK-FUNDED-IND
                        //* *         #RPT3-FUNDED-DTE := #WK-INIT-FUNDED-DTE
                        //*  REPORT 4 (NOT APPLICABLE)
                        getReports().write(3, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 3 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'N'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("N"))))
                    {
                        decideConditionsMet426++;
                        pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable.nadd(1);                                                                                               //Natural: ADD 1 TO #TTL-ENROLL-NOT-APPLICABLE
                        pnd_Rpt_Detail_Pnd_Rpt4_Pkg_Mail_Type.setValue(pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type);                                                       //Natural: ASSIGN #RPT4-PKG-MAIL-TYPE := #WK-PACKAGE-MAIL-TYPE
                        //*  REPORT 5 (COMPLETED)
                        getReports().write(4, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 4 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("C"))))
                    {
                        decideConditionsMet426++;
                        pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt.nadd(1);                                                                                              //Natural: ADD 1 TO #TTL-ENROLL-WITH-WELCOME-DT
                        if (condition(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date.equals(getZero())))                                                                      //Natural: IF #WK-WELCOME-MAIL-DATE = 0
                        {
                            pnd_Rpt_Detail_Pnd_Rpt5_Welcome_Mail_Dte.setValue("  N/A   ");                                                                                //Natural: ASSIGN #RPT5-WELCOME-MAIL-DTE := '  N/A   '
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Rpt_Detail_Pnd_Rpt5_Welcome_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);                                                 //Natural: ASSIGN #RPT5-WELCOME-MAIL-DTE := #WK-WELCOME-MAIL-DTE
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(5, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 5 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                        //*  LEGAL PKG. (NEWLY FUNDED)
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'L'
                else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Rec_Type.equals("L"))))
                {
                    decideConditionsMet422++;
                    pnd_Counters_Pnd_Ttl_Funded_Read.nadd(1);                                                                                                             //Natural: ADD 1 TO #TTL-FUNDED-READ
                    //*  REPORT 6 (EXCEPTION-NO LEGAL MAIL DATE)
                    short decideConditionsMet466 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE #WK-CATEGORY-IND;//Natural: VALUE 'E'
                    if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("E"))))
                    {
                        decideConditionsMet466++;
                        pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt.nadd(1);                                                                                                  //Natural: ADD 1 TO #TTL-FUNDED-NO-LEGAL-DT
                        pnd_Rpt_Detail_Pnd_Rpt6_Acis_Entry_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);                                                            //Natural: ASSIGN #RPT6-ACIS-ENTRY-DTE := #WK-REPRINT-DTE
                        pnd_Rpt_Detail_Pnd_Rpt6_Welcome_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);                                                     //Natural: ASSIGN #RPT6-WELCOME-MAIL-DTE := #WK-WELCOME-MAIL-DTE
                        pnd_Rpt_Detail_Pnd_Rpt6_Funded_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);                                                            //Natural: ASSIGN #RPT6-FUNDED-DTE := #WK-INIT-FUNDED-DTE
                        //*  REPORT 7 (DELETED)
                        getReports().write(6, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 6 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'D'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("D"))))
                    {
                        decideConditionsMet466++;
                        pnd_Counters_Pnd_Ttl_Funded_Deleted.nadd(1);                                                                                                      //Natural: ADD 1 TO #TTL-FUNDED-DELETED
                        pnd_Rpt_Detail_Pnd_Rpt7_Acis_Entry_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);                                                            //Natural: ASSIGN #RPT7-ACIS-ENTRY-DTE := #WK-REPRINT-DTE
                        pnd_Rpt_Detail_Pnd_Rpt7_Delete_Dte.setValue(pnd_Delete_Dte);                                                                                      //Natural: ASSIGN #RPT7-DELETE-DTE := #DELETE-DTE
                        pnd_Rpt_Detail_Pnd_Rpt7_Delete_User.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_User);                                                               //Natural: ASSIGN #RPT7-DELETE-USER := #WK-DELETE-USER
                        pnd_Rpt_Detail_Pnd_Rpt7_Delete_Reason.setValue(DbsUtil.compress(pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason, pnd_Dlt_Reason_Desc));                    //Natural: COMPRESS #WK-DELETE-REASON #DLT-REASON-DESC INTO #RPT7-DELETE-REASON
                        //*  REPORT 8 (NOT FOUND ON ACIS)
                        getReports().write(7, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 7 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'F'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("F"))))
                    {
                        decideConditionsMet466++;
                        pnd_Counters_Pnd_Ttl_Funded_Not_Found.nadd(1);                                                                                                    //Natural: ADD 1 TO #TTL-FUNDED-NOT-FOUND
                        pnd_Rpt_Detail_Pnd_Rpt8_Fund_Ind.setValue(pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind);                                                                   //Natural: ASSIGN #RPT8-FUND-IND := #WK-FUNDED-IND
                        pnd_Rpt_Detail_Pnd_Rpt8_Funded_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);                                                            //Natural: ASSIGN #RPT8-FUNDED-DTE := #WK-INIT-FUNDED-DTE
                        //*  REPORT 9 (NOT APPLICABLE)
                        getReports().write(8, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 8 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'N'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("N"))))
                    {
                        decideConditionsMet466++;
                        if (condition(pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll.equals("2")))                                                                               //Natural: IF #WK-DEFAULT-ENROLL = '2'
                        {
                            pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt.nadd(1);                                                                                         //Natural: ADD 1 TO #TTL-FUNDED-DFLT-NO-LEGAL-DT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Counters_Pnd_Ttl_Funded_Not_Applicable.nadd(1);                                                                                           //Natural: ADD 1 TO #TTL-FUNDED-NOT-APPLICABLE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Rpt_Detail_Pnd_Rpt9_Default_Enroll.setValue(pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll);                                                         //Natural: ASSIGN #RPT9-DEFAULT-ENROLL := #WK-DEFAULT-ENROLL
                        //*  REPORT 10 (COMPLETED)
                        getReports().write(9, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                     //Natural: WRITE ( 9 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("C"))))
                    {
                        decideConditionsMet466++;
                        pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt.nadd(1);                                                                                                //Natural: ADD 1 TO #TTL-FUNDED-WITH-LEGAL-DT
                        if (condition(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date.equals(getZero())))                                                                      //Natural: IF #WK-WELCOME-MAIL-DATE = 0
                        {
                            pnd_Rpt_Detail_Pnd_Rpt10_Welcome_Mail_Dte.setValue("  N/A   ");                                                                               //Natural: ASSIGN #RPT10-WELCOME-MAIL-DTE := '  N/A   '
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Rpt_Detail_Pnd_Rpt10_Welcome_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);                                                //Natural: ASSIGN #RPT10-WELCOME-MAIL-DTE := #WK-WELCOME-MAIL-DTE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date.equals(getZero())))                                                                        //Natural: IF #WK-LEGAL-MAIL-DATE = 0
                        {
                            pnd_Rpt_Detail_Pnd_Rpt10_Legal_Mail_Dte.setValue("  N/A   ");                                                                                 //Natural: ASSIGN #RPT10-LEGAL-MAIL-DTE := '  N/A   '
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Rpt_Detail_Pnd_Rpt10_Legal_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte);                                                    //Natural: ASSIGN #RPT10-LEGAL-MAIL-DTE := #WK-LEGAL-MAIL-DTE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  REPORT 11 (PENDING)
                        getReports().write(10, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                    //Natural: WRITE ( 10 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'P'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("P"))))
                    {
                        decideConditionsMet466++;
                        pnd_Counters_Pnd_Ttl_Funded_Pending.nadd(1);                                                                                                      //Natural: ADD 1 TO #TTL-FUNDED-PENDING
                        pnd_Rpt_Detail_Pnd_Rpt11_Fund_Ind.setValue(pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind);                                                                  //Natural: ASSIGN #RPT11-FUND-IND := #WK-FUNDED-IND
                        pnd_Rpt_Detail_Pnd_Rpt11_Funded_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);                                                           //Natural: ASSIGN #RPT11-FUNDED-DTE := #WK-INIT-FUNDED-DTE
                        getReports().write(11, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                    //Natural: WRITE ( 11 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet422 > 0))
                {
                    pnd_Rpt_Detail_Pnd_Rpt_Misc.reset();                                                                                                                  //Natural: RESET #RPT-MISC
                    pnd_Rpt_Detail_Pnd_Rpt12_Fund_Ind.setValue(pnd_Work_Rpt_File_Pnd_Wk_Funded_Ind);                                                                      //Natural: ASSIGN #RPT12-FUND-IND := #WK-FUNDED-IND
                    pnd_Rpt_Detail_Pnd_Rpt12_Funded_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);                                                               //Natural: ASSIGN #RPT12-FUNDED-DTE := #WK-INIT-FUNDED-DTE
                    pnd_Rpt_Detail_Pnd_Rpt12_Default_Enroll.setValue(pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll);                                                            //Natural: ASSIGN #RPT12-DEFAULT-ENROLL := #WK-DEFAULT-ENROLL
                    pnd_Rpt_Detail_Pnd_Rpt12_Acis_Entry_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);                                                               //Natural: ASSIGN #RPT12-ACIS-ENTRY-DTE := #WK-REPRINT-DTE
                    pnd_Rpt_Detail_Pnd_Rpt12_Welcome_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);                                                        //Natural: ASSIGN #RPT12-WELCOME-MAIL-DTE := #WK-WELCOME-MAIL-DTE
                    pnd_Rpt_Detail_Pnd_Rpt12_Legal_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte);                                                            //Natural: ASSIGN #RPT12-LEGAL-MAIL-DTE := #WK-LEGAL-MAIL-DTE
                    pnd_Rpt_Detail_Pnd_Rpt12_Pkg_Mail_Type.setValue(pnd_Work_Rpt_File_Pnd_Wk_Package_Mail_Type);                                                          //Natural: ASSIGN #RPT12-PKG-MAIL-TYPE := #WK-PACKAGE-MAIL-TYPE
                    pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Dte.setValue(pnd_Delete_Dte);                                                                                        //Natural: ASSIGN #RPT12-DELETED-DTE := #DELETE-DTE
                    pnd_Rpt_Detail_Pnd_Rpt12_Deleted_By.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_User);                                                                   //Natural: ASSIGN #RPT12-DELETED-BY := #WK-DELETE-USER
                    pnd_Rpt_Detail_Pnd_Rpt12_Deleted_Rsn.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason);                                                                //Natural: ASSIGN #RPT12-DELETED-RSN := #WK-DELETE-REASON
                    pnd_Rpt_Detail_Pnd_Rpt12_Rec_Type.setValue(pnd_Work_Rpt_File_Pnd_Wk_Rec_Type);                                                                        //Natural: ASSIGN #RPT12-REC-TYPE := #WK-REC-TYPE
                    pnd_Rpt_Detail_Pnd_Rpt12_Category.setValue(pnd_Work_Rpt_File_Pnd_Wk_Category_Ind);                                                                    //Natural: ASSIGN #RPT12-CATEGORY := #WK-CATEGORY-IND
                    if (condition(pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("C")))                                                                                     //Natural: IF #WK-CATEGORY-IND = 'C'
                    {
                        if (condition(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Date.equals(getZero())))                                                                      //Natural: IF #WK-WELCOME-MAIL-DATE = 0
                        {
                            pnd_Rpt_Detail_Pnd_Rpt12_Welcome_Mail_Dte.setValue("  N/A   ");                                                                               //Natural: ASSIGN #RPT12-WELCOME-MAIL-DTE := '  N/A   '
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Date.equals(getZero())))                                                                        //Natural: IF #WK-LEGAL-MAIL-DATE = 0
                        {
                            pnd_Rpt_Detail_Pnd_Rpt12_Legal_Mail_Dte.setValue("  N/A   ");                                                                                 //Natural: ASSIGN #RPT12-LEGAL-MAIL-DTE := '  N/A   '
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(12, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                        //Natural: WRITE ( 12 ) 2T #RPT-DETAIL
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  EXCEPTION RECORDS
                if (condition(pnd_Work_Rpt_File_Pnd_Wk_Rec_Id.equals("3")))                                                                                               //Natural: IF #WK-REC-ID = '3'
                {
                    if (condition(pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("E") || pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("F") || pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("P"))) //Natural: IF #WK-CATEGORY-IND = 'E' OR = 'F' OR = 'P'
                    {
                        pnd_Rpt_Detail_Pnd_Rpt14_Funded_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);                                                           //Natural: ASSIGN #RPT14-FUNDED-DTE := #WK-INIT-FUNDED-DTE
                        pnd_Rpt_Detail_Pnd_Rpt14_Acis_Entry_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);                                                           //Natural: ASSIGN #RPT14-ACIS-ENTRY-DTE := #WK-REPRINT-DTE
                        pnd_Rpt_Detail_Pnd_Rpt14_Welcome_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);                                                    //Natural: ASSIGN #RPT14-WELCOME-MAIL-DTE := #WK-WELCOME-MAIL-DTE
                        pnd_Rpt_Detail_Pnd_Rpt14_Legal_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte);                                                        //Natural: ASSIGN #RPT14-LEGAL-MAIL-DTE := #WK-LEGAL-MAIL-DTE
                        pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Dte.setValue(pnd_Delete_Dte);                                                                                    //Natural: ASSIGN #RPT14-DELETED-DTE := #DELETE-DTE
                        pnd_Rpt_Detail_Pnd_Rpt14_Deleted_By.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_User);                                                               //Natural: ASSIGN #RPT14-DELETED-BY := #WK-DELETE-USER
                        pnd_Rpt_Detail_Pnd_Rpt14_Deleted_Rsn.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason);                                                            //Natural: ASSIGN #RPT14-DELETED-RSN := #WK-DELETE-REASON
                        pnd_Rpt_Detail_Pnd_Rpt14_Rec_Type.setValue(pnd_Work_Rpt_File_Pnd_Wk_Rec_Type);                                                                    //Natural: ASSIGN #RPT14-REC-TYPE := #WK-REC-TYPE
                        pnd_Rpt_Detail_Pnd_Rpt14_Old_Category.setValue(pnd_Work_Rpt_File_Pnd_Wk_Old_Category);                                                            //Natural: ASSIGN #RPT14-OLD-CATEGORY := #WK-OLD-CATEGORY
                        pnd_Rpt_Detail_Pnd_Rpt14_New_Category.setValue(pnd_Work_Rpt_File_Pnd_Wk_Category_Ind);                                                            //Natural: ASSIGN #RPT14-NEW-CATEGORY := #WK-CATEGORY-IND
                        pnd_Rpt_Detail_Pnd_Rpt14_Exception_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Exception_Dte);                                                          //Natural: ASSIGN #RPT14-EXCEPTION-DTE := #WK-EXCEPTION-DTE
                        pnd_Rpt_Detail_Pnd_Rpt14_Age_In_Days.setValue(pnd_Work_Rpt_File_Pnd_Wk_Exception_Age_Days);                                                       //Natural: ASSIGN #RPT14-AGE-IN-DAYS := #WK-EXCEPTION-AGE-DAYS
                        getReports().write(14, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                    //Natural: WRITE ( 14 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Rpt_Detail_Pnd_Rpt15_Funded_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Init_Funded_Dte);                                                           //Natural: ASSIGN #RPT15-FUNDED-DTE := #WK-INIT-FUNDED-DTE
                        pnd_Rpt_Detail_Pnd_Rpt15_Acis_Entry_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Reprint_Dte);                                                           //Natural: ASSIGN #RPT15-ACIS-ENTRY-DTE := #WK-REPRINT-DTE
                        pnd_Rpt_Detail_Pnd_Rpt15_Welcome_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Welcome_Mail_Dte);                                                    //Natural: ASSIGN #RPT15-WELCOME-MAIL-DTE := #WK-WELCOME-MAIL-DTE
                        pnd_Rpt_Detail_Pnd_Rpt15_Legal_Mail_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Legal_Mail_Dte);                                                        //Natural: ASSIGN #RPT15-LEGAL-MAIL-DTE := #WK-LEGAL-MAIL-DTE
                        pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Dte.setValue(pnd_Delete_Dte);                                                                                    //Natural: ASSIGN #RPT15-DELETED-DTE := #DELETE-DTE
                        pnd_Rpt_Detail_Pnd_Rpt15_Deleted_By.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_User);                                                               //Natural: ASSIGN #RPT15-DELETED-BY := #WK-DELETE-USER
                        pnd_Rpt_Detail_Pnd_Rpt15_Deleted_Rsn.setValue(pnd_Work_Rpt_File_Pnd_Wk_Delete_Reason);                                                            //Natural: ASSIGN #RPT15-DELETED-RSN := #WK-DELETE-REASON
                        pnd_Rpt_Detail_Pnd_Rpt15_Rec_Type.setValue(pnd_Work_Rpt_File_Pnd_Wk_Rec_Type);                                                                    //Natural: ASSIGN #RPT15-REC-TYPE := #WK-REC-TYPE
                        pnd_Rpt_Detail_Pnd_Rpt15_Old_Category.setValue(pnd_Work_Rpt_File_Pnd_Wk_Old_Category);                                                            //Natural: ASSIGN #RPT15-OLD-CATEGORY := #WK-OLD-CATEGORY
                        pnd_Rpt_Detail_Pnd_Rpt15_New_Category.setValue(pnd_Work_Rpt_File_Pnd_Wk_Category_Ind);                                                            //Natural: ASSIGN #RPT15-NEW-CATEGORY := #WK-CATEGORY-IND
                        pnd_Rpt_Detail_Pnd_Rpt15_Resolved_Dte.setValue(pnd_Work_Rpt_File_Pnd_Wk_Resolved_Dte);                                                            //Natural: ASSIGN #RPT15-RESOLVED-DTE := #WK-RESOLVED-DTE
                        getReports().write(15, ReportOption.NOTITLE,new TabSetting(2),pnd_Rpt_Detail);                                                                    //Natural: WRITE ( 15 ) 2T #RPT-DETAIL
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  WELCOME PKG. (NEW ENROLLMENTS)
                    short decideConditionsMet577 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE #WK-REC-TYPE;//Natural: VALUE 'W'
                    if (condition((pnd_Work_Rpt_File_Pnd_Wk_Rec_Type.equals("W"))))
                    {
                        decideConditionsMet577++;
                        pnd_Counters_Pnd_Ttlx_Enroll_Read.nadd(1);                                                                                                        //Natural: ADD 1 TO #TTLX-ENROLL-READ
                        short decideConditionsMet580 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE #WK-CATEGORY-IND;//Natural: VALUE 'E'
                        if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("E"))))
                        {
                            decideConditionsMet580++;
                            pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt.nadd(1);                                                                                           //Natural: ADD 1 TO #TTLX-ENROLL-NO-WELCOME-DT
                        }                                                                                                                                                 //Natural: VALUE 'D'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("D"))))
                        {
                            decideConditionsMet580++;
                            pnd_Counters_Pnd_Ttlx_Enroll_Deleted.nadd(1);                                                                                                 //Natural: ADD 1 TO #TTLX-ENROLL-DELETED
                        }                                                                                                                                                 //Natural: VALUE 'F'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("F"))))
                        {
                            decideConditionsMet580++;
                            pnd_Counters_Pnd_Ttlx_Enroll_Not_Found.nadd(1);                                                                                               //Natural: ADD 1 TO #TTLX-ENROLL-NOT-FOUND
                        }                                                                                                                                                 //Natural: VALUE 'N'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("N"))))
                        {
                            decideConditionsMet580++;
                            pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable.nadd(1);                                                                                          //Natural: ADD 1 TO #TTLX-ENROLL-NOT-APPLICABLE
                        }                                                                                                                                                 //Natural: VALUE 'C'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("C"))))
                        {
                            decideConditionsMet580++;
                            pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt.nadd(1);                                                                                         //Natural: ADD 1 TO #TTLX-ENROLL-WITH-WELCOME-DT
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                            //*  LEGAL PKG. (NEWLY FUNDED)
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: VALUE 'L'
                    else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Rec_Type.equals("L"))))
                    {
                        decideConditionsMet577++;
                        pnd_Counters_Pnd_Ttlx_Funded_Read.nadd(1);                                                                                                        //Natural: ADD 1 TO #TTLX-FUNDED-READ
                        short decideConditionsMet597 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE #WK-CATEGORY-IND;//Natural: VALUE 'E'
                        if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("E"))))
                        {
                            decideConditionsMet597++;
                            pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt.nadd(1);                                                                                             //Natural: ADD 1 TO #TTLX-FUNDED-NO-LEGAL-DT
                        }                                                                                                                                                 //Natural: VALUE 'D'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("D"))))
                        {
                            decideConditionsMet597++;
                            pnd_Counters_Pnd_Ttlx_Funded_Deleted.nadd(1);                                                                                                 //Natural: ADD 1 TO #TTLX-FUNDED-DELETED
                        }                                                                                                                                                 //Natural: VALUE 'F'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("F"))))
                        {
                            decideConditionsMet597++;
                            //*  (NOT APPLICABLE)
                            pnd_Counters_Pnd_Ttlx_Funded_Not_Found.nadd(1);                                                                                               //Natural: ADD 1 TO #TTLX-FUNDED-NOT-FOUND
                        }                                                                                                                                                 //Natural: VALUE 'N'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("N"))))
                        {
                            decideConditionsMet597++;
                            if (condition(pnd_Work_Rpt_File_Pnd_Wk_Default_Enroll.equals("2")))                                                                           //Natural: IF #WK-DEFAULT-ENROLL = '2'
                            {
                                pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt.nadd(1);                                                                                    //Natural: ADD 1 TO #TTLX-FUNDED-DFLT-NO-LEGAL-DT
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable.nadd(1);                                                                                      //Natural: ADD 1 TO #TTLX-FUNDED-NOT-APPLICABLE
                                //*  (COMPLETED)
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: VALUE 'C'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("C"))))
                        {
                            decideConditionsMet597++;
                            //*  (PENDING)
                            pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt.nadd(1);                                                                                           //Natural: ADD 1 TO #TTLX-FUNDED-WITH-LEGAL-DT
                        }                                                                                                                                                 //Natural: VALUE 'P'
                        else if (condition((pnd_Work_Rpt_File_Pnd_Wk_Category_Ind.equals("P"))))
                        {
                            decideConditionsMet597++;
                            pnd_Counters_Pnd_Ttlx_Funded_Pending.nadd(1);                                                                                                 //Natural: ADD 1 TO #TTLX-FUNDED-PENDING
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY
        sub_Write_Summary();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTROL-COUNT
        sub_Display_Control_Count();
        if (condition(Global.isEscape())) {return;}
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ENROLL-RPT1-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ENROLL-RPT2-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ENROLL-RPT3-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ENROLL-RPT4-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ENROLL-RPT5-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT6-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT7-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT8-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT9-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT10-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT11-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT12-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-RPT13
        //* ******************************************
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT14-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FUNDED-RPT15-HEADER
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-RPT16
        //* ******************************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY
        //*  REPORT 9 (NEWLY FUNDED - NOT APPLICABLE)
        //*  REPORT 12 (ALL RECORDS)
        //*  REPORT 14 (ALL OUTSTANDING EXCEPTIONS)
        //*  REPORT 15 (ALL RESOLVED EXCEPTIONS)
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-CONTROL-COUNT
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Write_Enroll_Rpt1_Header() throws Exception                                                                                                          //Natural: WRITE-ENROLL-RPT1-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZZZ"),new TabSetting(49),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 1 ) ( EM = ZZZZ ) 49T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 49T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 12T 'New Enrollment with no Welcome Package Date' / 16T #RPT-PERIOD-COVERED / 20T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub      ACIS' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date' / 1T '------------  ------------  -----------  ------  ------  ----------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(49),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(12),"New Enrollment with no Welcome Package Date",NEWLINE,new TabSetting(16),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(20),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub      ACIS",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  ----------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-ENROLL-RPT1-HEADER
    }
    private void sub_Write_Enroll_Rpt2_Header() throws Exception                                                                                                          //Natural: WRITE-ENROLL-RPT2-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZZZ"),new TabSetting(99),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 2 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 2 ) ( EM = ZZZZ ) 99T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 99T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 47T 'New Enrollments Deleted' / 41T #RPT-PERIOD-COVERED / 45T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub      ACIS        Deleted   Deleted           Deleted' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date     Date       By              Reason' / 1T '------------  ------------  -----------  ------  ------  ----------  ----------  --------  ----------------------------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(99),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(47),"New Enrollments Deleted",NEWLINE,new TabSetting(41),pnd_Rpt_Period_Covered,NEWLINE,new TabSetting(45),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new 
            TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub      ACIS        Deleted   Deleted           Deleted",NEWLINE,new TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date     Date       By              Reason",NEWLINE,new 
            TabSetting(1),"------------  ------------  -----------  ------  ------  ----------  ----------  --------  ----------------------------",NEWLINE,
            NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-ENROLL-RPT2-HEADER
    }
    private void sub_Write_Enroll_Rpt3_Header() throws Exception                                                                                                          //Natural: WRITE-ENROLL-RPT3-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZZZ"),new TabSetting(37),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 3 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 3 ) ( EM = ZZZZ ) 37T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 37T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 11T 'New Enrollments not found on ACIS' / 10T #RPT-PERIOD-COVERED / 14T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub  ' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan ' / 1T '------------  ------------  -----------  ------  ------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(37),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(11),"New Enrollments not found on ACIS",NEWLINE,new TabSetting(10),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(14),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub  ",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan ",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-ENROLL-RPT3-HEADER
    }
    private void sub_Write_Enroll_Rpt4_Header() throws Exception                                                                                                          //Natural: WRITE-ENROLL-RPT4-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZZZ"),new TabSetting(48),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 4 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 4 ) ( EM = ZZZZ ) 48T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 48T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 16T 'New Enrollments - Not Applicable' / 15T #RPT-PERIOD-COVERED / 19T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub    Pkg. Mail' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan     Type   ' / 1T '------------  ------------  -----------  ------  ------  ---------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(48),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(16),"New Enrollments - Not Applicable",NEWLINE,new TabSetting(15),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(19),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub    Pkg. Mail",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan     Type   ",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  ---------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-ENROLL-RPT4-HEADER
    }
    private void sub_Write_Enroll_Rpt5_Header() throws Exception                                                                                                          //Natural: WRITE-ENROLL-RPT5-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZZZ"),new TabSetting(49),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 5 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 5 ) ( EM = ZZZZ ) 49T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 49T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 7T 'New Enrollments with Welcome Package Date - Completed' / 16T #RPT-PERIOD-COVERED / 20T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub     Welcome' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan   Mail Date' / 1T '------------  ------------  -----------  ------  ------  ----------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(49),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(7),"New Enrollments with Welcome Package Date - Completed",NEWLINE,new TabSetting(16),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(20),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub     Welcome",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan   Mail Date",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  ----------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-ENROLL-RPT5-HEADER
    }
    private void sub_Write_Funded_Rpt6_Header() throws Exception                                                                                                          //Natural: WRITE-FUNDED-RPT6-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZZZ"),new TabSetting(73),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 6 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 6 ) ( EM = ZZZZ ) 73T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 73T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 21T 'Newly Funded Contracts with no Legal Package Date' / 27T #RPT-PERIOD-COVERED / 31T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub      ACIS       Welcome      Funded  ' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date  Mail Date      Date   ' / 1T '------------  ------------  -----------  ------  ------  ----------  ----------  ----------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(73),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(21),"Newly Funded Contracts with no Legal Package Date",NEWLINE,new TabSetting(27),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(31),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub      ACIS       Welcome      Funded  ",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date  Mail Date      Date   ",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  ----------  ----------  ----------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT6-HEADER
    }
    private void sub_Write_Funded_Rpt7_Header() throws Exception                                                                                                          //Natural: WRITE-FUNDED-RPT7-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZZZ"),new TabSetting(99),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 7 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 7 ) ( EM = ZZZZ ) 99T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 99T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 38T 'Newly Funded Contracts with Delete Reason' / 41T #RPT-PERIOD-COVERED / 45T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub      ACIS        Deleted   Deleted           Deleted' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date     Date       By              Reason' / 1T '------------  ------------  -----------  ------  ------  ----------  ----------  --------  ----------------------------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(99),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(38),"Newly Funded Contracts with Delete Reason",NEWLINE,new TabSetting(41),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(45),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub      ACIS        Deleted   Deleted           Deleted",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan   Entry Date     Date       By              Reason",NEWLINE,new TabSetting(1),
            "------------  ------------  -----------  ------  ------  ----------  ----------  --------  ----------------------------",NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT7-HEADER
    }
    private void sub_Write_Funded_Rpt8_Header() throws Exception                                                                                                          //Natural: WRITE-FUNDED-RPT8-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(8, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZZZ"),new TabSetting(57),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 8 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 8 ) ( EM = ZZZZ ) 57T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 57T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 17T 'Newly Funded Contracts not found on ACIS' / 19T #RPT-PERIOD-COVERED / 23T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub    Funded    Funded' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan    Ind.      Date' / 1T '------------  ------------  -----------  ------  ------  ------  ----------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(57),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(17),"Newly Funded Contracts not found on ACIS",NEWLINE,new TabSetting(19),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(23),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub    Funded    Funded",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan    Ind.      Date",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  ------  ----------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT8-HEADER
    }
    private void sub_Write_Funded_Rpt9_Header() throws Exception                                                                                                          //Natural: WRITE-FUNDED-RPT9-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(9, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(9), new ReportEditMask ("ZZZZ"),new TabSetting(46),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 9 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 9 ) ( EM = ZZZZ ) 46T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 46T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 12T 'Newly Funded Contracts - Not Applicable' / 14T #RPT-PERIOD-COVERED / 18T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F.       OMNI       Plan    Sub    Default' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan   Enroll ' / 1T '------------  ------------  -----------  ------  ------  -------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(46),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(12),"Newly Funded Contracts - Not Applicable",NEWLINE,new TabSetting(14),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(18),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F.       OMNI       Plan    Sub    Default",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan   Enroll ",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  -------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT9-HEADER
    }
    private void sub_Write_Funded_Rpt10_Header() throws Exception                                                                                                         //Natural: WRITE-FUNDED-RPT10-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(10, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(10), new ReportEditMask ("ZZZZ"),new TabSetting(62),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 10 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 10 ) ( EM = ZZZZ ) 62T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 62T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 11T 'Newly Funded Contracts with Legal Package Date - Completed' / 22T #RPT-PERIOD-COVERED / 26T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub     Welcome      Legal   ' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan   Mail Date   Mail Date ' / 1T '------------  ------------  -----------  ------  ------  ----------  ----------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(62),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(11),"Newly Funded Contracts with Legal Package Date - Completed",NEWLINE,new TabSetting(22),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(26),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub     Welcome      Legal   ",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan   Mail Date   Mail Date ",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  ----------  ----------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT10-HEADER
    }
    private void sub_Write_Funded_Rpt11_Header() throws Exception                                                                                                         //Natural: WRITE-FUNDED-RPT11-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(11, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(11), new ReportEditMask ("ZZZZ"),new TabSetting(57),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 11 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 11 ) ( EM = ZZZZ ) 57T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 57T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 16T 'Newly Funded Contracts - Pending Processing' / 20T #RPT-PERIOD-COVERED / 24T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F        OMNI       Plan    Sub    Funded    Funded' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan    Ind.      Date' / 1T '------------  ------------  -----------  ------  ------  ------  ----------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(57),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(16),"Newly Funded Contracts - Pending Processing",NEWLINE,new TabSetting(20),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(24),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F        OMNI       Plan    Sub    Funded    Funded",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan    Ind.      Date",NEWLINE,new TabSetting(1),"------------  ------------  -----------  ------  ------  ------  ----------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT11-HEADER
    }
    private void sub_Write_Funded_Rpt12_Header() throws Exception                                                                                                         //Natural: WRITE-FUNDED-RPT12-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(12, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(12), new ReportEditMask ("ZZZZ"),new TabSetting(150),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 12 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 12 ) ( EM = ZZZZ ) 150T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 150T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 57T 'List of All New Enrollments And Newly Funded Contracts' / 63T 'Reconciliation Report Between EDW and ACIS'/ 67T #RPT-PERIOD-COVERED / 71T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F.       OMNI       Plan    Sub    Funded    Funded    Default    ACIS       Welcome     Legal     Pkg. Mail    Deleted   Deleted   Dltd  Rec  Cat' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan    Ind.      Date     Enroll   Entry Date  Mail Date  Mail Date     Type        Date       By      Rsn   Ind  Cde' / 1T '------------  ------------  -----------  ------  ------  ------  ----------  -------  ----------  ---------- ----------  ---------  ----------  --------  ----  ---  ---' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(150),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(57),"List of All New Enrollments And Newly Funded Contracts",NEWLINE,new TabSetting(63),"Reconciliation Report Between EDW and ACIS",NEWLINE,new 
            TabSetting(67),pnd_Rpt_Period_Covered,NEWLINE,new TabSetting(71),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F.       OMNI       Plan    Sub    Funded    Funded    Default    ACIS       Welcome     Legal     Pkg. Mail    Deleted   Deleted   Dltd  Rec  Cat",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan    Ind.      Date     Enroll   Entry Date  Mail Date  Mail Date     Type        Date       By      Rsn   Ind  Cde",NEWLINE,new 
            TabSetting(1),"------------  ------------  -----------  ------  ------  ------  ----------  -------  ----------  ---------- ----------  ---------  ----------  --------  ----  ---  ---",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT12-HEADER
    }
    private void sub_Write_Summary_Rpt13() throws Exception                                                                                                               //Natural: WRITE-SUMMARY-RPT13
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Gttl_Read.compute(new ComputeParameters(false, pnd_Counters_Pnd_Gttl_Read), (pnd_Counters_Pnd_Ttl_Funded_Read.add(pnd_Counters_Pnd_Ttl_Enroll_Read))); //Natural: ASSIGN #GTTL-READ := ( #TTL-FUNDED-READ + #TTL-ENROLL-READ )
        pnd_Counters_Pnd_Gttl_Completed.compute(new ComputeParameters(false, pnd_Counters_Pnd_Gttl_Completed), (pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt.add(pnd_Counters_Pnd_Ttl_Enroll_Deleted).add(pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable).add(pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt).add(pnd_Counters_Pnd_Ttl_Funded_Deleted).add(pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt).add(pnd_Counters_Pnd_Ttl_Funded_Not_Applicable))); //Natural: ASSIGN #GTTL-COMPLETED := ( #TTL-ENROLL-WITH-WELCOME-DT + #TTL-ENROLL-DELETED + #TTL-ENROLL-NOT-APPLICABLE + #TTL-FUNDED-WITH-LEGAL-DT + #TTL-FUNDED-DELETED + #TTL-FUNDED-DFLT-NO-LEGAL-DT + #TTL-FUNDED-NOT-APPLICABLE )
        pnd_Counters_Pnd_Gttl_Notcompleted.compute(new ComputeParameters(false, pnd_Counters_Pnd_Gttl_Notcompleted), (pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt.add(pnd_Counters_Pnd_Ttl_Enroll_Not_Found).add(pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt).add(pnd_Counters_Pnd_Ttl_Funded_Not_Found).add(pnd_Counters_Pnd_Ttl_Funded_Pending))); //Natural: ASSIGN #GTTL-NOTCOMPLETED := ( #TTL-ENROLL-NO-WELCOME-DT + #TTL-ENROLL-NOT-FOUND + #TTL-FUNDED-NO-LEGAL-DT + #TTL-FUNDED-NOT-FOUND + #TTL-FUNDED-PENDING )
        getReports().write(13, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(12), new ReportEditMask ("ZZZZ"),new TabSetting(62),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 13 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 12 ) ( EM = ZZZZ ) 62T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 62T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 14T 'Summary Reconciliation Report Between EDW and ACIS' / 22T #RPT-PERIOD-COVERED / 26T #RPT-CURR-CONTROL ///// // 2T 'New Enrollment Reconciliation:' / 2T '==============================' / 2T 'Number of New Enrollments during the period              =' 61T #TTL-ENROLL-READ ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments with Welcome Date Populated    =' 61T #TTL-ENROLL-WITH-WELCOME-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments without Welcome Date Populated =' 61T #TTL-ENROLL-NO-WELCOME-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments Deleted                        =' 61T #TTL-ENROLL-DELETED ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments Not Found on ACIS              =' 61T #TTL-ENROLL-NOT-FOUND ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments Not Applicable                 =' 61T #TTL-ENROLL-NOT-APPLICABLE ( EM = ZZZ,ZZ9 ) // 2T 'Newly Funded Contract Reconciliation:' / 2T '=====================================' // 2T 'Number of Newly Funded Contracts during the period       =' 61T #TTL-FUNDED-READ ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts with Legal Package Date =' 61T #TTL-FUNDED-WITH-LEGAL-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts without Legal Pkg. Date =' 61T #TTL-FUNDED-NO-LEGAL-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts Deleted                 =' 61T #TTL-FUNDED-DELETED ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts Not Found on ACIS       =' 61T #TTL-FUNDED-NOT-FOUND ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Dflt. Enroll. No Legal Pkg. Date  =' 61T #TTL-FUNDED-DFLT-NO-LEGAL-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Pending Processing                =' 61T #TTL-FUNDED-PENDING ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Not Applicable                    =' 61T #TTL-FUNDED-NOT-APPLICABLE ( EM = ZZZ,ZZ9 ) /// 2T 'Grand Totals:' / 2T '============' // 2T 'Number of EDW Records Read                               =' 61T #GTTL-READ ( EM = ZZZ,ZZ9 ) / 2T 'Number of Records Categorized as Completed/Non-Issue     =' 61T #GTTL-COMPLETED ( EM = ZZZ,ZZ9 ) / 2T 'Number of Records Categorized as Not Completed/Exception =' 61T #GTTL-NOTCOMPLETED ( EM = ZZZ,ZZ9 ) /// 29T '<<< End of Report >>>'
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(62),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(14),"Summary Reconciliation Report Between EDW and ACIS",NEWLINE,new TabSetting(22),pnd_Rpt_Period_Covered,NEWLINE,new 
            TabSetting(26),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"New Enrollment Reconciliation:",NEWLINE,new 
            TabSetting(2),"==============================",NEWLINE,new TabSetting(2),"Number of New Enrollments during the period              =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Enroll_Read, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments with Welcome Date Populated    =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments without Welcome Date Populated =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments Deleted                        =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Enroll_Deleted, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments Not Found on ACIS              =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Enroll_Not_Found, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments Not Applicable                 =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(2),"Newly Funded Contract Reconciliation:",NEWLINE,new TabSetting(2),"=====================================",NEWLINE,NEWLINE,new 
            TabSetting(2),"Number of Newly Funded Contracts during the period       =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_Read, new ReportEditMask 
            ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts with Legal Package Date =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts without Legal Pkg. Date =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts Deleted                 =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_Deleted, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts Not Found on ACIS       =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_Not_Found, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Dflt. Enroll. No Legal Pkg. Date  =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Pending Processing                =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_Pending, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Not Applicable                    =",new TabSetting(61),pnd_Counters_Pnd_Ttl_Funded_Not_Applicable, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"Grand Totals:",NEWLINE,new TabSetting(2),"============",NEWLINE,NEWLINE,new 
            TabSetting(2),"Number of EDW Records Read                               =",new TabSetting(61),pnd_Counters_Pnd_Gttl_Read, new ReportEditMask 
            ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Records Categorized as Completed/Non-Issue     =",new TabSetting(61),pnd_Counters_Pnd_Gttl_Completed, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Records Categorized as Not Completed/Exception =",new TabSetting(61),pnd_Counters_Pnd_Gttl_Notcompleted, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(29),"<<< End of Report >>>");
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT13-HEADER
    }
    private void sub_Write_Funded_Rpt14_Header() throws Exception                                                                                                         //Natural: WRITE-FUNDED-RPT14-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(14, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(14), new ReportEditMask ("ZZZZ"),new TabSetting(150),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 14 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 14 ) ( EM = ZZZZ ) 150T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 150T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 57T 'List of All Outstanding Exceptions and their Current Status' / 63T 'Reconciliation Report Between EDW and ACIS'/ 70T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F.       OMNI       Plan    Sub      Funded      ACIS       Welcome     Legal     Deleted  Deleted   Dltd  Rec  OldCat NewCat Exception Age In ' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan      Date    Entry Date  Mail Date  Mail Date     Date      By      Rsn   Ind   Cde    Cde     Date     Days  ' / 1T '------------  ------------  -----------  ------  ------   --------  ----------  ---------- ----------  --------  --------  ----  ---  ------ ------ --------  -------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(150),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(57),"List of All Outstanding Exceptions and their Current Status",NEWLINE,new TabSetting(63),"Reconciliation Report Between EDW and ACIS",NEWLINE,new 
            TabSetting(70),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F.       OMNI       Plan    Sub      Funded      ACIS       Welcome     Legal     Deleted  Deleted   Dltd  Rec  OldCat NewCat Exception Age In ",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan      Date    Entry Date  Mail Date  Mail Date     Date      By      Rsn   Ind   Cde    Cde     Date     Days  ",NEWLINE,new 
            TabSetting(1),"------------  ------------  -----------  ------  ------   --------  ----------  ---------- ----------  --------  --------  ----  ---  ------ ------ --------  -------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT14-HEADER
    }
    private void sub_Write_Funded_Rpt15_Header() throws Exception                                                                                                         //Natural: WRITE-FUNDED-RPT15-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(15, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZZZ"),new TabSetting(150),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 15 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 15 ) ( EM = ZZZZ ) 150T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 150T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 70T 'List of Resolved Exceptions' / 63T 'Reconciliation Report Between EDW and ACIS'/ 70T #RPT-CURR-CONTROL // 1T '  T I A A       C R E F.       OMNI       Plan    Sub      Funded      ACIS       Welcome     Legal     Deleted  Deleted   Dltd  Rec  OldCat NewCat Resolved' / 1T 'Contract No.  Contract No.   Part. Id     Code    Plan      Date    Entry Date  Mail Date  Mail Date     Date      By      Rsn   Ind   Cde    Cde     Date  ' / 1T '------------  ------------  -----------  ------  ------   --------  ----------  ---------- ----------  --------  --------  ----  ---  ------ ------ --------' //
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(150),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(70),"List of Resolved Exceptions",NEWLINE,new TabSetting(63),"Reconciliation Report Between EDW and ACIS",NEWLINE,new 
            TabSetting(70),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,new TabSetting(1),"  T I A A       C R E F.       OMNI       Plan    Sub      Funded      ACIS       Welcome     Legal     Deleted  Deleted   Dltd  Rec  OldCat NewCat Resolved",NEWLINE,new 
            TabSetting(1),"Contract No.  Contract No.   Part. Id     Code    Plan      Date    Entry Date  Mail Date  Mail Date     Date      By      Rsn   Ind   Cde    Cde     Date  ",NEWLINE,new 
            TabSetting(1),"------------  ------------  -----------  ------  ------   --------  ----------  ---------- ----------  --------  --------  ----  ---  ------ ------ --------",
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //*  WRITE-FUNDED-RPT15-HEADER
    }
    private void sub_Write_Summary_Rpt16() throws Exception                                                                                                               //Natural: WRITE-SUMMARY-RPT16
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counters_Pnd_Gttlx_Read.compute(new ComputeParameters(false, pnd_Counters_Pnd_Gttlx_Read), (pnd_Counters_Pnd_Ttlx_Funded_Read.add(pnd_Counters_Pnd_Ttlx_Enroll_Read))); //Natural: ASSIGN #GTTLX-READ := ( #TTLX-FUNDED-READ + #TTLX-ENROLL-READ )
        pnd_Counters_Pnd_Gttlx_Resolved.compute(new ComputeParameters(false, pnd_Counters_Pnd_Gttlx_Resolved), (pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt.add(pnd_Counters_Pnd_Ttlx_Enroll_Deleted).add(pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable).add(pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt).add(pnd_Counters_Pnd_Ttlx_Funded_Deleted).add(pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt).add(pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable))); //Natural: ASSIGN #GTTLX-RESOLVED := ( #TTLX-ENROLL-WITH-WELCOME-DT + #TTLX-ENROLL-DELETED + #TTLX-ENROLL-NOT-APPLICABLE + #TTLX-FUNDED-WITH-LEGAL-DT + #TTLX-FUNDED-DELETED + #TTLX-FUNDED-DFLT-NO-LEGAL-DT + #TTLX-FUNDED-NOT-APPLICABLE )
        pnd_Counters_Pnd_Gttlx_Notresolved.compute(new ComputeParameters(false, pnd_Counters_Pnd_Gttlx_Notresolved), (pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt.add(pnd_Counters_Pnd_Ttlx_Enroll_Not_Found).add(pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt).add(pnd_Counters_Pnd_Ttlx_Funded_Not_Found).add(pnd_Counters_Pnd_Ttlx_Funded_Pending))); //Natural: ASSIGN #GTTLX-NOTRESOLVED := ( #TTLX-ENROLL-NO-WELCOME-DT + #TTLX-ENROLL-NOT-FOUND + #TTLX-FUNDED-NO-LEGAL-DT + #TTLX-FUNDED-NOT-FOUND + #TTLX-FUNDED-PENDING )
        getReports().write(16, ReportOption.NOTITLE,new TabSetting(1),"Page : ",getReports().getPageNumberDbs(16), new ReportEditMask ("ZZZZ"),new TabSetting(62),"Program : ",Global.getPROGRAM(),NEWLINE,new  //Natural: WRITE ( 16 ) NOTITLE 1T 'Page : ' *PAGE-NUMBER ( 16 ) ( EM = ZZZZ ) 62T 'Program : ' *PROGRAM / 1T 'Date : ' *DATX ( EM = MM/DD/YY ) 62T 'Time    : ' *TIME ( EM = XXXXXXXX ) // 14T 'Summary of Outstanding Exceptions and their Status' / 26T #RPT-CURR-CONTROL ///// // 2T 'New Enrollment Exceptions:' / 2T '=========================' / 2T 'Number of New Enrollment Outstanding Exceptions          =' 61T #TTLX-ENROLL-READ ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments with Welcome Date Populated    =' 61T #TTLX-ENROLL-WITH-WELCOME-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments without Welcome Date Populated =' 61T #TTLX-ENROLL-NO-WELCOME-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments Deleted                        =' 61T #TTLX-ENROLL-DELETED ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments Not Found on ACIS              =' 61T #TTLX-ENROLL-NOT-FOUND ( EM = ZZZ,ZZ9 ) / 2T 'Number of New Enrollments Not Applicable                 =' 61T #TTLX-ENROLL-NOT-APPLICABLE ( EM = ZZZ,ZZ9 ) // 2T 'Newly Funded Contract Exceptions:' / 2T '================================' // 2T 'Number of Newly Funded Outstanding Exceptions            =' 61T #TTLX-FUNDED-READ ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts with Legal Package Date =' 61T #TTLX-FUNDED-WITH-LEGAL-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts without Legal Pkg. Date =' 61T #TTLX-FUNDED-NO-LEGAL-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts Deleted                 =' 61T #TTLX-FUNDED-DELETED ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Contracts Not Found on ACIS       =' 61T #TTLX-FUNDED-NOT-FOUND ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Dflt. Enroll. No Legal Pkg. Date  =' 61T #TTLX-FUNDED-DFLT-NO-LEGAL-DT ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Pending Processing                =' 61T #TTLX-FUNDED-PENDING ( EM = ZZZ,ZZ9 ) / 2T 'Number of Newly Funded Not Applicable                    =' 61T #TTLX-FUNDED-NOT-APPLICABLE ( EM = ZZZ,ZZ9 ) /// 2T 'Grand Totals:' / 2T '============' // 2T 'Number of Outstanding Exceptions Read                    =' 61T #GTTLX-READ ( EM = ZZZ,ZZ9 ) / 2T 'Number of Outstanding Exceptions Resolved                =' 61T #GTTLX-RESOLVED ( EM = ZZZ,ZZ9 ) / 2T 'Number of Outstanding Exceptions Not Resolved            =' 61T #GTTLX-NOTRESOLVED ( EM = ZZZ,ZZ9 ) /// 29T '<<< End of Report >>>'
            TabSetting(1),"Date : ",Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new TabSetting(62),"Time    : ",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,NEWLINE,new TabSetting(14),"Summary of Outstanding Exceptions and their Status",NEWLINE,new TabSetting(26),pnd_Rpt_Curr_Control,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new 
            TabSetting(2),"New Enrollment Exceptions:",NEWLINE,new TabSetting(2),"=========================",NEWLINE,new TabSetting(2),"Number of New Enrollment Outstanding Exceptions          =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Enroll_Read, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments with Welcome Date Populated    =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments without Welcome Date Populated =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments Deleted                        =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Enroll_Deleted, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments Not Found on ACIS              =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Enroll_Not_Found, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of New Enrollments Not Applicable                 =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(2),"Newly Funded Contract Exceptions:",NEWLINE,new 
            TabSetting(2),"================================",NEWLINE,NEWLINE,new TabSetting(2),"Number of Newly Funded Outstanding Exceptions            =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_Read, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts with Legal Package Date =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts without Legal Pkg. Date =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts Deleted                 =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_Deleted, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Contracts Not Found on ACIS       =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_Not_Found, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Dflt. Enroll. No Legal Pkg. Date  =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Pending Processing                =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_Pending, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Newly Funded Not Applicable                    =",new 
            TabSetting(61),pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(2),"Grand Totals:",NEWLINE,new 
            TabSetting(2),"============",NEWLINE,NEWLINE,new TabSetting(2),"Number of Outstanding Exceptions Read                    =",new TabSetting(61),pnd_Counters_Pnd_Gttlx_Read, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Outstanding Exceptions Resolved                =",new TabSetting(61),pnd_Counters_Pnd_Gttlx_Resolved, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new TabSetting(2),"Number of Outstanding Exceptions Not Resolved            =",new TabSetting(61),pnd_Counters_Pnd_Gttlx_Notresolved, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(29),"<<< End of Report >>>");
        if (Global.isEscape()) return;
        //*  WRITE-SUMMARY-RPT16
    }
    private void sub_Write_Summary() throws Exception                                                                                                                     //Natural: WRITE-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  REPORT 1 (NEW ENROLLMENTS - NO WELCOME MAIL DATE)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                              //Natural: MOVE EDITED #TTL-ENROLL-NO-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(23),"<<< End of Report >>>");           //Natural: WRITE ( 1 ) / 2T #TOTAL-REC /// 23T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 2 (NEW ENROLLMENTS - DELETED)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Enroll_Deleted,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                    //Natural: MOVE EDITED #TTL-ENROLL-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),"<<< End of Report >>>");           //Natural: WRITE ( 2 ) / 2T #TOTAL-REC /// 48T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 3 (NEW ENROLLMENTS - NOT FOUND IN ACIS)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Enroll_Not_Found,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                  //Natural: MOVE EDITED #TTL-ENROLL-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"<<< End of Report >>>");           //Natural: WRITE ( 3 ) / 2T #TOTAL-REC /// 17T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 4 (NEW ENROLLMENTS - NOT APPLICABLE)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                             //Natural: MOVE EDITED #TTL-ENROLL-NOT-APPLICABLE ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"<<< End of Report >>>");           //Natural: WRITE ( 4 ) / 2T #TOTAL-REC /// 17T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 5 (NEW ENROLLMENTS - COMPLETED)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                            //Natural: MOVE EDITED #TTL-ENROLL-WITH-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(5, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(23),"<<< End of Report >>>");           //Natural: WRITE ( 5 ) / 2T #TOTAL-REC /// 23T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 6 (NEWLY FUNDED - NO LEGAL MAIL DATE)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                //Natural: MOVE EDITED #TTL-FUNDED-NO-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(6, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(35),"<<< End of Report >>>");           //Natural: WRITE ( 6 ) / 2T #TOTAL-REC /// 35T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 7 (NEWLY FUNDED - DELEDED)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Funded_Deleted,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                    //Natural: MOVE EDITED #TTL-FUNDED-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(7, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),"<<< End of Report >>>");           //Natural: WRITE ( 7 ) / 2T #TOTAL-REC /// 48T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 8 (NEWLY FUNDED - NOT FOUND IN ACIS)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Funded_Not_Found,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                  //Natural: MOVE EDITED #TTL-FUNDED-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(8, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(27),"<<< End of Report >>>");           //Natural: WRITE ( 8 ) / 2T #TOTAL-REC /// 27T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        pnd_Amt1.compute(new ComputeParameters(false, pnd_Amt1), pnd_Counters_Pnd_Ttl_Funded_Not_Applicable.add(pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt));           //Natural: ASSIGN #AMT1 := #TTL-FUNDED-NOT-APPLICABLE + #TTL-FUNDED-DFLT-NO-LEGAL-DT
        pnd_Amt.setValueEdited(pnd_Amt1,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                                               //Natural: MOVE EDITED #AMT1 ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(9, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(21),"<<< End of Report >>>");           //Natural: WRITE ( 9 ) / 2T #TOTAL-REC /// 21T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 10 (NEWLY FUNDED - COMPLETED)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                              //Natural: MOVE EDITED #TTL-FUNDED-WITH-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(10, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(29),"<<< End of Report >>>");          //Natural: WRITE ( 10 ) / 2T #TOTAL-REC /// 29T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        //*  REPORT 11 (NEWLY FUNDED - PENDING)
        pnd_Amt.setValueEdited(pnd_Counters_Pnd_Ttl_Funded_Pending,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                    //Natural: MOVE EDITED #TTL-FUNDED-PENDING ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(11, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,NEWLINE,new TabSetting(28),"<<< End of Report >>>");          //Natural: WRITE ( 11 ) / 2T #TOTAL-REC /// 28T '<<< End of Report >>>'
        if (Global.isEscape()) return;
        pnd_Amt1.compute(new ComputeParameters(false, pnd_Amt1), pnd_Counters_Pnd_Ttl_Enroll_Read.add(pnd_Counters_Pnd_Ttl_Funded_Read));                                 //Natural: ASSIGN #AMT1 := #TTL-ENROLL-READ + #TTL-FUNDED-READ
        pnd_Amt.setValueEdited(pnd_Amt1,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                                               //Natural: MOVE EDITED #AMT1 ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(12, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,"Legend:",NEWLINE,"Dltd Rsn (Deleted Reason)     Rec Ind         Cat Cde (Category)",NEWLINE,"----------------------------  --------------  ------------------",NEWLINE,"1-Issued in Error (Inst.)     W-Enrollment    C-Completed       ",NEWLINE,"2-Issued in Error (Processor) L-Newly Funded  E-Exception       ",NEWLINE,"3-Contract Already Exist                      D-Deleted         ",NEWLINE,"4-Test Contract                               F-Not Found       ",NEWLINE,"5-OMNI Delete                                 N-Not Applicable  ",NEWLINE,"                                              P-Pending         ",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 12 ) / 2T #TOTAL-REC // 'Legend:' / 'Dltd Rsn (Deleted Reason)     Rec Ind         Cat Cde (Category)' / '----------------------------  --------------  ------------------' / '1-Issued in Error (Inst.)     W-Enrollment    C-Completed       ' / '2-Issued in Error (Processor) L-Newly Funded  E-Exception       ' / '3-Contract Already Exist                      D-Deleted         ' / '4-Test Contract                               F-Not Found       ' / '5-OMNI Delete                                 N-Not Applicable  ' / '                                              P-Pending         ' /// 73T '<<< End of Report >>>'
            TabSetting(73),"<<< End of Report >>>");
        if (Global.isEscape()) return;
        //*  REPORT 13 (REGULAR RECORDS)
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-RPT13
        sub_Write_Summary_Rpt13();
        if (condition(Global.isEscape())) {return;}
        pnd_Amt1.compute(new ComputeParameters(false, pnd_Amt1), (pnd_Counters_Pnd_Ttlx_Enroll_No_Welcome_Dt.add(pnd_Counters_Pnd_Ttlx_Enroll_Not_Found).add(pnd_Counters_Pnd_Ttlx_Funded_No_Legal_Dt).add(pnd_Counters_Pnd_Ttlx_Funded_Not_Found).add(pnd_Counters_Pnd_Ttlx_Funded_Pending))); //Natural: ASSIGN #AMT1 := ( #TTLX-ENROLL-NO-WELCOME-DT + #TTLX-ENROLL-NOT-FOUND + #TTLX-FUNDED-NO-LEGAL-DT + #TTLX-FUNDED-NOT-FOUND + #TTLX-FUNDED-PENDING )
        pnd_Amt.setValueEdited(pnd_Amt1,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                                               //Natural: MOVE EDITED #AMT1 ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(14, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,"Legend:",NEWLINE,"Dltd Rsn (Deleted Reason)     Rec Ind         Category Code     ",NEWLINE,"----------------------------  --------------  ------------------",NEWLINE,"1-Issued in Error (Inst.)     W-Enrollment    C-Completed       ",NEWLINE,"2-Issued in Error (Processor) L-Newly Funded  E-Exception       ",NEWLINE,"3-Contract Already Exist                      D-Deleted         ",NEWLINE,"4-Test Contract                               F-Not Found       ",NEWLINE,"5-OMNI Delete                                 N-Not Applicable  ",NEWLINE,"                                              P-Pending         ",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 14 ) / 2T #TOTAL-REC // 'Legend:' / 'Dltd Rsn (Deleted Reason)     Rec Ind         Category Code     ' / '----------------------------  --------------  ------------------' / '1-Issued in Error (Inst.)     W-Enrollment    C-Completed       ' / '2-Issued in Error (Processor) L-Newly Funded  E-Exception       ' / '3-Contract Already Exist                      D-Deleted         ' / '4-Test Contract                               F-Not Found       ' / '5-OMNI Delete                                 N-Not Applicable  ' / '                                              P-Pending         ' /// 73T '<<< End of Report >>>'
            TabSetting(73),"<<< End of Report >>>");
        if (Global.isEscape()) return;
        pnd_Amt1.compute(new ComputeParameters(false, pnd_Amt1), (pnd_Counters_Pnd_Ttlx_Enroll_With_Welcome_Dt.add(pnd_Counters_Pnd_Ttlx_Enroll_Deleted).add(pnd_Counters_Pnd_Ttlx_Enroll_Not_Applicable).add(pnd_Counters_Pnd_Ttlx_Funded_With_Legal_Dt).add(pnd_Counters_Pnd_Ttlx_Funded_Deleted).add(pnd_Counters_Pnd_Ttlx_Funded_Dflt_No_Legal_Dt).add(pnd_Counters_Pnd_Ttlx_Funded_Not_Applicable))); //Natural: ASSIGN #AMT1 := ( #TTLX-ENROLL-WITH-WELCOME-DT + #TTLX-ENROLL-DELETED + #TTLX-ENROLL-NOT-APPLICABLE + #TTLX-FUNDED-WITH-LEGAL-DT + #TTLX-FUNDED-DELETED + #TTLX-FUNDED-DFLT-NO-LEGAL-DT + #TTLX-FUNDED-NOT-APPLICABLE )
        pnd_Amt.setValueEdited(pnd_Amt1,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                                                               //Natural: MOVE EDITED #AMT1 ( EM = ZZZ,ZZZ,ZZ9 ) TO #AMT
        DbsUtil.examine(new ExamineSource(pnd_Amt), new ExamineSearch(" "), new ExamineDelete());                                                                         //Natural: EXAMINE #AMT FOR ' ' DELETE
        pnd_Total_Rec.setValue(DbsUtil.compress("Total No. of Records:", pnd_Amt));                                                                                       //Natural: COMPRESS 'Total No. of Records:' #AMT INTO #TOTAL-REC
        getReports().write(15, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),pnd_Total_Rec,NEWLINE,NEWLINE,"Legend:",NEWLINE,"Dltd Rsn (Deleted Reason)     Rec Ind         Category Code     ",NEWLINE,"----------------------------  --------------  ------------------",NEWLINE,"1-Issued in Error (Inst.)     W-Enrollment    C-Completed       ",NEWLINE,"2-Issued in Error (Processor) L-Newly Funded  E-Exception       ",NEWLINE,"3-Contract Already Exist                      D-Deleted         ",NEWLINE,"4-Test Contract                               F-Not Found       ",NEWLINE,"5-OMNI Delete                                 N-Not Applicable  ",NEWLINE,"                                              P-Pending         ",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 15 ) / 2T #TOTAL-REC // 'Legend:' / 'Dltd Rsn (Deleted Reason)     Rec Ind         Category Code     ' / '----------------------------  --------------  ------------------' / '1-Issued in Error (Inst.)     W-Enrollment    C-Completed       ' / '2-Issued in Error (Processor) L-Newly Funded  E-Exception       ' / '3-Contract Already Exist                      D-Deleted         ' / '4-Test Contract                               F-Not Found       ' / '5-OMNI Delete                                 N-Not Applicable  ' / '                                              P-Pending         ' /// 73T '<<< End of Report >>>'
            TabSetting(73),"<<< End of Report >>>");
        if (Global.isEscape()) return;
        //*  REPORT 16 (EXCEPTIONS)
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-RPT16
        sub_Write_Summary_Rpt16();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE-SUMMARY
    }
    private void sub_Display_Control_Count() throws Exception                                                                                                             //Natural: DISPLAY-CONTROL-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getReports().write(0, "<<< REGULAR CONTROL SUMMARY >>>",NEWLINE,NEWLINE,"NO. OF NEW ENROLLMENT RECORDS READ:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Read,  //Natural: WRITE '<<< REGULAR CONTROL SUMMARY >>>' // 'NO. OF NEW ENROLLMENT RECORDS READ:' 47T #TTL-ENROLL-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS WITH WELCOME PKG.:' 47T #TTL-ENROLL-WITH-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS WITHOUT WELCOME PKG.:' 47T #TTL-ENROLL-NO-WELCOME-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS DELETED:' 47T #TTL-ENROLL-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS NOT FOUND:' 47T #TTL-ENROLL-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW ENROLL. RECS NOT APPLICABLE:' 47T #TTL-ENROLL-NOT-APPLICABLE ( EM = ZZZ,ZZZ,ZZ9 ) // 'NO. OF NEW FUNDED RECORDS READ:' 47T #TTL-FUNDED-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS WITH LEGAL PKG.:' 47T #TTL-FUNDED-WITH-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS PENDING:' 47T #TTL-FUNDED-PENDING ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS WITHOUT LEGAL PKG.:' 47T #TTL-FUNDED-NO-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS DELETED:' 47T #TTL-FUNDED-DELETED ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS NOT FOUND:' 47T #TTL-FUNDED-NOT-FOUND ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS NOT APPLICABLE:' 47T #TTL-FUNDED-NOT-APPLICABLE ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF NEW FUNDED RECS DEFAULT ENROLL:' 47T #TTL-FUNDED-DFLT-NO-LEGAL-DT ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS WITH WELCOME PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_With_Welcome_Dt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS WITHOUT WELCOME PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_No_Welcome_Dt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS DELETED:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Deleted, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS NOT FOUND:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Not_Found, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW ENROLL. RECS NOT APPLICABLE:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Enroll_Not_Applicable, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"NO. OF NEW FUNDED RECORDS READ:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Read, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS WITH LEGAL PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_With_Legal_Dt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS PENDING:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Pending, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS WITHOUT LEGAL PKG.:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_No_Legal_Dt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS DELETED:",new TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Deleted, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS NOT FOUND:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Not_Found, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS NOT APPLICABLE:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Not_Applicable, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF NEW FUNDED RECS DEFAULT ENROLL:",new 
            TabSetting(47),pnd_Counters_Pnd_Ttl_Funded_Dflt_No_Legal_Dt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,"<<< OUTSTANDING EXCEPTION CONTROL SUMMARY >>>",NEWLINE,NEWLINE,"NO. OF OUTSTANDING EXCEPTIONS READ:",new           //Natural: WRITE /// '<<< OUTSTANDING EXCEPTION CONTROL SUMMARY >>>' // 'NO. OF OUTSTANDING EXCEPTIONS READ:' 47T #GTTLX-READ ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF OUTSTANDING EXCEPTIONS RESOLVED:' 47T #GTTLX-RESOLVED ( EM = ZZZ,ZZZ,ZZ9 ) / 'NO. OF OUTSTANDING EXCEPTIONS NOT RESOLVED:' 47T #GTTLX-NOTRESOLVED ( EM = ZZZ,ZZZ,ZZ9 )
            TabSetting(47),pnd_Counters_Pnd_Gttlx_Read, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF OUTSTANDING EXCEPTIONS RESOLVED:",new TabSetting(47),pnd_Counters_Pnd_Gttlx_Resolved, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"NO. OF OUTSTANDING EXCEPTIONS NOT RESOLVED:",new TabSetting(47),pnd_Counters_Pnd_Gttlx_Notresolved, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  DISPLAY-CONTROL-COUNT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-ENROLL-RPT1-HEADER
                    sub_Write_Enroll_Rpt1_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-ENROLL-RPT2-HEADER
                    sub_Write_Enroll_Rpt2_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-ENROLL-RPT3-HEADER
                    sub_Write_Enroll_Rpt3_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-ENROLL-RPT4-HEADER
                    sub_Write_Enroll_Rpt4_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-ENROLL-RPT5-HEADER
                    sub_Write_Enroll_Rpt5_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT6-HEADER
                    sub_Write_Funded_Rpt6_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT7-HEADER
                    sub_Write_Funded_Rpt7_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt8 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT8-HEADER
                    sub_Write_Funded_Rpt8_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt9 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT9-HEADER
                    sub_Write_Funded_Rpt9_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt10 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT10-HEADER
                    sub_Write_Funded_Rpt10_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt11 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT11-HEADER
                    sub_Write_Funded_Rpt11_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt12 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT12-HEADER
                    sub_Write_Funded_Rpt12_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt14 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT14-HEADER
                    sub_Write_Funded_Rpt14_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt15 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-FUNDED-RPT15-HEADER
                    sub_Write_Funded_Rpt15_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE // 'CONTRACT:' 25T #WK-TIAA-NO / 'SSN:' 25T #WK-SSN
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),pnd_Work_Rpt_File_Pnd_Wk_Tiaa_No,NEWLINE,"SSN:",new TabSetting(25),
            pnd_Work_Rpt_File_Pnd_Wk_Ssn);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=210 PS=52");
        Global.format(2, "LS=210 PS=52");
        Global.format(3, "LS=210 PS=52");
        Global.format(4, "LS=210 PS=52");
        Global.format(5, "LS=210 PS=52");
        Global.format(6, "LS=210 PS=52");
        Global.format(7, "LS=210 PS=52");
        Global.format(8, "LS=210 PS=52");
        Global.format(9, "LS=210 PS=52");
        Global.format(10, "LS=210 PS=52");
        Global.format(11, "LS=210 PS=52");
        Global.format(12, "LS=210 PS=52");
        Global.format(13, "LS=210 PS=52");
        Global.format(14, "LS=210 PS=52");
        Global.format(15, "LS=210 PS=52");
        Global.format(16, "LS=210 PS=52");
    }
}
