/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:39 PM
**        * FROM NATURAL PROGRAM : Appb4100
************************************************************
**        * FILE NAME            : Appb4100.java
**        * CLASS NAME           : Appb4100
**        * INSTANCE NAME        : Appb4100
************************************************************
************************************************************************
* PROGRAM  : APPB4100 - EXTRACT DATA FROM PRAP FILE                    *
* FUNCTION : READS PRAP FILE AND EXTRACTS DATA FOR CIP WHICH IS THEN   *
*            WRITTEN TO TWO OUTPUT FILES.  WRITE OUT A THIRD WORK FILE *
*            WHICH WILL BE INPUT TO APPB4101 WHICH WILL THEN PRODUCE A *
*            CONTROL REPORT                                            *
* DATE     : 07/17/03                                                  *
* **********************************************************************
* UPDATED  : 10.14.2003 JR-1 REMOVED MIDDLE INITIAL FROM DOWNLOAD
* **********************************************************************
* UPDATED  : 11.08.2004 MS-1 FORCE OUTPUT FILES TO CONTAIN A 5 CHAR ZIP*
* **********************************************************************
* UPDATED  : 11-16-2005 J CAMPBELL - CIP ENHANCEMENT, PROJ REQ. # 21:
*            1) IF AP-NAME-ADDR-CD CONTAINS 2 CHAR COUNTRY CODE, SET
*               #C-COUNTRY = AP-NAME-ADDR-CD INSTEAD OF DEFAULT 'US'.
*               DO NOT SET #C-STATE OR #C-ZIP FOR A FOREIGN ADDRESS.
*            2) INSERT ' ' IN BETWEEN LINES IN #C-ADDRESS.  (JC1)
* UPDATED  : 03-23-2006 J CAMPBELL - ATRA ACTIVITY FOR COMPLIANCE
*              MANAGER, PROJECT REQUEST # 513: SEPARATE ATRA CONTRACTS
*              FROM OTHER RA CONTRACTS IN WORK FILES.    (JC2)
* UPDATED  : 04-21-2006 J CAMPBELL - OMNI CHANGES: USE ONLY OCCURRENCE
*              (1) OF AP-ADDRESS-TXT FOR STREET ADDRESS.  DO NOT PERFORM
*              FIND-FOREIGN-CITY.  (CITY WILL BE IN AP-ADDRESS-TXT (4))
*              ALSO, REINSTATE UPDATE.                 (JC3)
* UPDATED  : 02-21-2007 K GATES - ADDED IDENTIFICATON OF OMNI ATRAS BY
* UPDATED  : 09-10-2008 N CVETKOVIC RHSP CHANGES (IGNORE THEM) NBC
* AML      : 08-01-2012 BUDDY NEWSOM REMOVE 'INDIVIDUAL' FROM FIELD 47.
* 06/20/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.  *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb4100 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_G_Key;
    private DbsField annty_Actvty_Prap_View_Ap_G_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Dob;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Bill_Code;

    private DbsGroup annty_Actvty_Prap_View_Ap_Tiaa_Contr;
    private DbsField annty_Actvty_Prap_View_App_Pref;
    private DbsField annty_Actvty_Prap_View_App_Cont;

    private DbsGroup annty_Actvty_Prap_View_Ap_Cref_Contr;
    private DbsField annty_Actvty_Prap_View_App_C_Pref;
    private DbsField annty_Actvty_Prap_View_App_C_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Curr;
    private DbsField annty_Actvty_Prap_View_Ap_T_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_C_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership;
    private DbsField annty_Actvty_Prap_View_Ap_Sex;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Zip;
    private DbsField annty_Actvty_Prap_View_Ap_Name_Addr_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Ent_Sys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Released;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Matched;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Deleted;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Withdrawn;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Discr;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Other_Pols;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Numb_Dailys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_App_Recvd;
    private DbsField annty_Actvty_Prap_View_Ap_App_Source;
    private DbsField annty_Actvty_Prap_View_Ap_Region_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Split_Code;

    private DbsGroup annty_Actvty_Prap_View_Ap_Address_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Line;
    private DbsField annty_Actvty_Prap_View_Ap_City;
    private DbsField annty_Actvty_Prap_View_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Annuity_Start_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Info_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Primary_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Contingent_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Estate;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Trust;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Category;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Instructions;
    private DbsField annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Process_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_St_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Csm_Sec_Seg;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_College;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Ph_Hist_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField annty_Actvty_Prap_View_Ap_Contact_Mode;
    private DbsField annty_Actvty_Prap_View_Ap_Racf_Id;

    private DbsGroup annty_Actvty_Prap_View__R_Field_1;
    private DbsField annty_Actvty_Prap_View_Pnd_Ap_Filler;
    private DbsField annty_Actvty_Prap_View_Pnd_Ap_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_View_Ap_Sync_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Doi;

    private DbsGroup annty_Actvty_Prap_View_Ap_Mit_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Unit;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Wpid;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Rollover_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Ppg;
    private DbsField annty_Actvty_Prap_View_Ap_Print_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Cntrct_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt;

    private DbsGroup annty_Actvty_Prap_View_Ap_Addr_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Txt;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Dest_Name;
    private DbsField annty_Actvty_Prap_View_Ap_Zip_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup annty_Actvty_Prap_View__R_Field_2;
    private DbsField annty_Actvty_Prap_View_Pnd_Evening_Phone_Number;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Usage_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Init_Paymt_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_Init_Paymt_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_1;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_2;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_3;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_4;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_5;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Inst_Link_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Applcnt_Req_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Sync_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent;
    private DbsField annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Ppg_Team_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Model_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Divorce_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Phone_No;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Fmt;
    private DbsField annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Eft_Requested_Ind;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField pnd_Cip_Customer_File;

    private DbsGroup pnd_Cip_Customer_File__R_Field_3;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Pin;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Parent;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Name;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Dba_Sic;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Address;

    private DbsGroup pnd_Cip_Customer_File__R_Field_4;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Addr;
    private DbsField pnd_Cip_Customer_File_Pnd_C_City;

    private DbsGroup pnd_Cip_Customer_File__R_Field_5;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Cty;
    private DbsField pnd_Cip_Customer_File_Pnd_C_State;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Zip;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Country;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Evening_Phone;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Email;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Tin;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Passport;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Dob;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Typeofbusiness;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Filler1;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Ownerbranch;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Ownerdept;

    private DbsGroup pnd_Cip_Customer_File__R_Field_6;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Od;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Owneroper;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Filler2;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Sex;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Customer_Type;
    private DbsField pnd_Cip_Customer_File_Pnd_C_Filler3;
    private DbsField pnd_Compressed_A;

    private DbsGroup pnd_Cip_Account_File;
    private DbsField pnd_Cip_Account_File_Pnd_A_Version;
    private DbsField pnd_Cip_Account_File_Pnd_A_Id;
    private DbsField pnd_Cip_Account_File_Pnd_A_Name;
    private DbsField pnd_Cip_Account_File_Pnd_A_Monitor;
    private DbsField pnd_Cip_Account_File_Pnd_A_Ex_Status;
    private DbsField pnd_Cip_Account_File_Pnd_A_Notes;
    private DbsField pnd_Cip_Account_File_Pnd_A_Branch;
    private DbsField pnd_Cip_Account_File_Pnd_A_Open_Date;
    private DbsField pnd_Cip_Account_File_Pnd_A_Owner_Branch;
    private DbsField pnd_Cip_Account_File_Pnd_A_Ownerdept;

    private DbsGroup pnd_Cip_Account_File__R_Field_7;
    private DbsField pnd_Cip_Account_File_Pnd_A_Od;
    private DbsField pnd_Cip_Account_File_Pnd_A_Owneroper;
    private DbsField pnd_Cip_Account_File_Pnd_A_Closed;
    private DbsField pnd_Cip_Account_File_Pnd_A_Pin;
    private DbsField pnd_Compressed_B;
    private DbsField pnd_Work_Ownerdept;

    private DbsGroup pnd_Work_Ownerdept__R_Field_8;
    private DbsField pnd_Work_Ownerdept_Pnd_W_Od;
    private DbsField pnd_Work_City;
    private DbsField pnd_Work_State;
    private DbsField pnd_Work_Name;

    private DbsGroup pnd_Work_Name__R_Field_9;
    private DbsField pnd_Work_Name_Pnd_Work_Name_39;
    private DbsField pnd_Work_Dob;
    private DbsField pnd_Ap_Dob;

    private DbsGroup pnd_Ap_Dob__R_Field_10;
    private DbsField pnd_Ap_Dob_Pnd_Work_Mm;
    private DbsField pnd_Ap_Dob_Pnd_Work_Dd;
    private DbsField pnd_Ap_Dob_Pnd_Work_Yyyy;
    private DbsField pnd_Op_Dt;

    private DbsGroup pnd_Op_Dt__R_Field_11;
    private DbsField pnd_Op_Dt_Pnd_Work_Mmo;
    private DbsField pnd_Op_Dt_Pnd_Work_Ddo;
    private DbsField pnd_Op_Dt_Pnd_Work_Yyyyo;
    private DbsField pnd_Prt_Date;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_Is;
    private DbsField pnd_How_Many;
    private DbsField pnd_Update_Isn;
    private DbsField pnd_Geo_Grp;

    private DbsGroup pnd_Geo_Grp__R_Field_12;
    private DbsField pnd_Geo_Grp_Pnd_State;
    private DbsField pnd_Geo_Grp_Pnd_Code;
    private DbsField pnd_Geo_State_Tbl;

    private DbsGroup pnd_Geo_State_Tbl__R_Field_13;
    private DbsField pnd_Geo_State_Tbl_Pnd_State_Code;
    private DbsField pnd_Ap_Soc_Sec;
    private DbsField pnd_Work_Area;
    private DbsField pnd_Save_Area;
    private DbsField pnd_Remainder;
    private DbsField pnd_Number;
    private DbsField pnd_Length;
    private DbsField pnd_Foreign_Addr;
    private DbsField pnd_Atra;

    private DbsGroup pnd_Atra__R_Field_14;
    private DbsField pnd_Atra_Pnd_Atra_A2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_G_Key = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_G_KEY");
        annty_Actvty_Prap_View_Ap_G_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_G_IND");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Dob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_Bill_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bill_Code", "AP-BILL-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BILL_CODE");

        annty_Actvty_Prap_View_Ap_Tiaa_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_TIAA_CONTR", "AP-TIAA-CONTR");
        annty_Actvty_Prap_View_App_Pref = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Pref", "APP-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_PREF");
        annty_Actvty_Prap_View_App_Cont = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Cont", "APP-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_CONT");

        annty_Actvty_Prap_View_Ap_Cref_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_CREF_CONTR", "AP-CREF-CONTR");
        annty_Actvty_Prap_View_App_C_Pref = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Pref", "APP-C-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_C_PREF");
        annty_Actvty_Prap_View_App_C_Cont = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Cont", "APP-C-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_C_CONT");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Curr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_CURR");
        annty_Actvty_Prap_View_Ap_T_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Age_1st", "AP-T-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_T_AGE_1ST");
        annty_Actvty_Prap_View_Ap_C_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Age_1st", "AP-C-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_C_AGE_1ST");
        annty_Actvty_Prap_View_Ap_Ownership = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership", "AP-OWNERSHIP", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Sex = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sex", "AP-SEX", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SEX");
        annty_Actvty_Prap_View_Ap_Sex.setDdmHeader("SEX CDE");
        annty_Actvty_Prap_View_Ap_Mail_Zip = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Zip", "AP-MAIL-ZIP", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        annty_Actvty_Prap_View_Ap_Name_Addr_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NAME_ADDR_CD");
        annty_Actvty_Prap_View_Ap_Dt_Ent_Sys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        annty_Actvty_Prap_View_Ap_Dt_Released = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Released", "AP-DT-RELEASED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_RELEASED");
        annty_Actvty_Prap_View_Ap_Dt_Matched = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Matched", "AP-DT-MATCHED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_MATCHED");
        annty_Actvty_Prap_View_Ap_Dt_Deleted = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Deleted", "AP-DT-DELETED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_DELETED");
        annty_Actvty_Prap_View_Ap_Dt_Withdrawn = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Withdrawn", "AP-DT-WITHDRAWN", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_WITHDRAWN");
        annty_Actvty_Prap_View_Ap_Alloc_Discr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Discr", "AP-ALLOC-DISCR", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_ALLOC_DISCR");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Type", "AP-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_TYPE");
        annty_Actvty_Prap_View_Ap_Other_Pols = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Other_Pols", "AP-OTHER-POLS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OTHER_POLS");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Numb_Dailys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Numb_Dailys", "AP-NUMB-DAILYS", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_NUMB_DAILYS");
        annty_Actvty_Prap_View_Ap_Dt_App_Recvd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");
        annty_Actvty_Prap_View_Ap_App_Source = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_App_Source", "AP-APP-SOURCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APP_SOURCE");
        annty_Actvty_Prap_View_Ap_App_Source.setDdmHeader("SOURCE");
        annty_Actvty_Prap_View_Ap_Region_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Region_Code", "AP-REGION-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_REGION_CODE");
        annty_Actvty_Prap_View_Ap_Orig_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orig_Issue_State", 
            "AP-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Ownership_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP_TYPE");
        annty_Actvty_Prap_View_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_View_Ap_Split_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Split_Code", "AP-SPLIT-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SPLIT_CODE");

        annty_Actvty_Prap_View_Ap_Address_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Address_Info", 
            "AP-ADDRESS-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_Address_Line = annty_Actvty_Prap_View_Ap_Address_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Line", "AP-ADDRESS-LINE", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_City = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_City", "AP-CITY", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "AP_CITY");
        annty_Actvty_Prap_View_Ap_Current_State_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Current_State_Code", 
            "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd", 
            "AP-DANA-TRANSACTION-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_TRANSACTION_CD");
        annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd", 
            "AP-DANA-STNDRD-RTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_RTN_CD");
        annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd", 
            "AP-DANA-FINALIST-REASON-CD", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DANA_FINALIST_REASON_CD");
        annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code", 
            "AP-DANA-ADDR-STNDRD-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_ADDR_STNDRD_CODE");
        annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide", 
            "AP-DANA-STNDRD-OVERIDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_OVERIDE");
        annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields", 
            "AP-DANA-POSTAL-DATA-FIELDS", FieldType.STRING, 44, RepeatingFieldStrategy.None, "AP_DANA_POSTAL_DATA_FIELDS");
        annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code", 
            "AP-DANA-ADDR-GEOGRAPHIC-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_ADDR_GEOGRAPHIC_CODE");
        annty_Actvty_Prap_View_Ap_Annuity_Start_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Annuity_Start_Date", 
            "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");

        annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct", 
            "AP-MAXIMUM-ALLOC-PCT", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct", 
            "AP-MAX-ALLOC-PCT", FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign", 
            "AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Bene_Info_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Info_Type", "AP-BENE-INFO-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_INFO_TYPE");
        annty_Actvty_Prap_View_Ap_Primary_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Std_Ent", 
            "AP-PRIMARY-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Primary_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Primary_Info", 
            "AP-BENE-PRIMARY-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Primary_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Primary_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Bene_Info", 
            "AP-PRIMARY-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Std_Ent", 
            "AP-CONTINGENT-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Contingent_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Bene_Contingent_Info", 
            "AP-BENE-CONTINGENT-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Contingent_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Bene_Info", 
            "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 72, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Bene_Estate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Estate", "AP-BENE-ESTATE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_ESTATE");
        annty_Actvty_Prap_View_Ap_Bene_Trust = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Trust", "AP-BENE-TRUST", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_TRUST");
        annty_Actvty_Prap_View_Ap_Bene_Category = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Category", "AP-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_CATEGORY");
        annty_Actvty_Prap_View_Ap_Mail_Instructions = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Instructions", 
            "AP-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request", 
            "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        annty_Actvty_Prap_View_Ap_Process_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Process_Status", "AP-PROCESS-STATUS", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_PROCESS_STATUS");
        annty_Actvty_Prap_View_Ap_Coll_St_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_St_Cd", "AP-COLL-ST-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        annty_Actvty_Prap_View_Ap_Csm_Sec_Seg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Csm_Sec_Seg", "AP-CSM-SEC-SEG", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_CSM_SEC_SEG");
        annty_Actvty_Prap_View_Ap_Rlc_College = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_College", "AP-RLC-COLLEGE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_RLC_COLLEGE");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref", "AP-RLC-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_RLC_CREF_PREF");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont", "AP-RLC-CREF-CONT", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "AP_RLC_CREF_CONT");
        annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        annty_Actvty_Prap_View_Ap_Ph_Hist_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ph_Hist_Ind", "AP-PH-HIST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PH_HIST_IND");
        annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp", 
            "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        annty_Actvty_Prap_View_Ap_Contact_Mode = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contact_Mode", "AP-CONTACT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_CONTACT_MODE");
        annty_Actvty_Prap_View_Ap_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");

        annty_Actvty_Prap_View__R_Field_1 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_1", "REDEFINE", annty_Actvty_Prap_View_Ap_Racf_Id);
        annty_Actvty_Prap_View_Pnd_Ap_Filler = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Pnd_Ap_Filler", "#AP-FILLER", 
            FieldType.STRING, 8);
        annty_Actvty_Prap_View_Pnd_Ap_Racf_Id = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Pnd_Ap_Racf_Id", "#AP-RACF-ID", 
            FieldType.STRING, 8);
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Sync_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sync_Ind", "AP-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SYNC_IND");
        annty_Actvty_Prap_View_Ap_Cor_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_COR_IND");
        annty_Actvty_Prap_View_Ap_Alloc_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Ind", "AP-ALLOC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOC_IND");
        annty_Actvty_Prap_View_Ap_Tiaa_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_View_Ap_Cref_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Doi", "AP-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");

        annty_Actvty_Prap_View_Ap_Mit_Request = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Mit_Request", "AP-MIT-REQUEST", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Unit = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Unit", "AP-MIT-UNIT", 
            FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Wpid = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldInGroup("annty_Actvty_Prap_View_Ap_Mit_Wpid", "AP-MIT-WPID", 
            FieldType.STRING, 6, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Ira_Rollover_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Rollover_Type", 
            "AP-IRA-ROLLOVER-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_ROLLOVER_TYPE");
        annty_Actvty_Prap_View_Ap_Ira_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Record_Type", 
            "AP-IRA-RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Status", 
            "AP-MULT-APP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_STATUS");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type", 
            "AP-MULT-APP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Ppg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_MULT_APP_PPG");
        annty_Actvty_Prap_View_Ap_Print_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Print_Date", "AP-PRINT-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_PRINT_DATE");

        annty_Actvty_Prap_View_Ap_Cntrct_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Info", "AP-CNTRCT-INFO", 
            new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Type = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Type", "AP-CNTRCT-TYPE", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_TYPE", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Nbr = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Nbr", "AP-CNTRCT-NBR", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_NBR", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt", 
            "AP-CNTRCT-PROCEEDS-AMT", FieldType.NUMERIC, 10, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_PROCEEDS_AMT", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");

        annty_Actvty_Prap_View_Ap_Addr_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Addr_Info", "AP-ADDR-INFO", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Txt = annty_Actvty_Prap_View_Ap_Addr_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Txt", "AP-ADDRESS-TXT", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Dest_Name = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Dest_Name", 
            "AP-ADDRESS-DEST-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "AP_ADDRESS_DEST_NAME");
        annty_Actvty_Prap_View_Ap_Zip_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Zip_Code", "AP-ZIP-CODE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        annty_Actvty_Prap_View__R_Field_2 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_2", "REDEFINE", annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        annty_Actvty_Prap_View_Pnd_Evening_Phone_Number = annty_Actvty_Prap_View__R_Field_2.newFieldInGroup("annty_Actvty_Prap_View_Pnd_Evening_Phone_Number", 
            "#EVENING-PHONE-NUMBER", FieldType.STRING, 10);
        annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr", 
            "AP-BANK-ABA-ACCT-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_BANK_ABA_ACCT_NMBR");
        annty_Actvty_Prap_View_Ap_Addr_Usage_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Usage_Code", 
            "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_USAGE_CODE");
        annty_Actvty_Prap_View_Ap_Init_Paymt_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Init_Paymt_Amt", "AP-INIT-PAYMT-AMT", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_AMT");
        annty_Actvty_Prap_View_Ap_Init_Paymt_Pct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Init_Paymt_Pct", "AP-INIT-PAYMT-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_PCT");
        annty_Actvty_Prap_View_Ap_Financial_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_1", "AP-FINANCIAL-1", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_1");
        annty_Actvty_Prap_View_Ap_Financial_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_2", "AP-FINANCIAL-2", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_2");
        annty_Actvty_Prap_View_Ap_Financial_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_3", "AP-FINANCIAL-3", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_3");
        annty_Actvty_Prap_View_Ap_Financial_4 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_4", "AP-FINANCIAL-4", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_4");
        annty_Actvty_Prap_View_Ap_Financial_5 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_5", "AP-FINANCIAL-5", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_5");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde", 
            "AP-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_GRP_CDE");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_CDE");
        annty_Actvty_Prap_View_Ap_Inst_Link_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "AP_INST_LINK_CDE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Applcnt_Req_Type", 
            "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        annty_Actvty_Prap_View_Ap_Addr_Sync_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Sync_Ind", "AP-ADDR-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_SYNC_IND");
        annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent", 
            "AP-TIAA-SERVICE-AGENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIAA_SERVICE_AGENT");
        annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind", 
            "AP-PRAP-RSCH-MIT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PRAP_RSCH_MIT_IND");
        annty_Actvty_Prap_View_Ap_Ppg_Team_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_PPG_TEAM_CDE");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_RLC_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Allocation_Model_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Model_Type", 
            "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        annty_Actvty_Prap_View_Ap_Divorce_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Divorce_Ind", "AP-DIVORCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DIVORCE_IND");
        annty_Actvty_Prap_View_Ap_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Phone_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Phone_No", "AP-PHONE-NO", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "AP_PHONE_NO");
        annty_Actvty_Prap_View_Ap_Allocation_Fmt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOCATION_FMT");
        annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind", 
            "AP-E-SIGNED-APPL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        annty_Actvty_Prap_View_Ap_Eft_Requested_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eft_Requested_Ind", 
            "AP-EFT-REQUESTED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");

        annty_Actvty_Prap_View_Ap_Fund_Identifier = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier", 
            "AP-FUND-IDENTIFIER", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Fund_Cde = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde", "AP-FUND-CDE", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Allocation_Pct = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct", 
            "AP-ALLOCATION-PCT", FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Cip_Customer_File = localVariables.newFieldArrayInRecord("pnd_Cip_Customer_File", "#CIP-CUSTOMER-FILE", FieldType.STRING, 1, new DbsArrayController(1, 
            500));

        pnd_Cip_Customer_File__R_Field_3 = localVariables.newGroupInRecord("pnd_Cip_Customer_File__R_Field_3", "REDEFINE", pnd_Cip_Customer_File);
        pnd_Cip_Customer_File_Pnd_C_Pin = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Pin", "#C-PIN", FieldType.STRING, 
            13);
        pnd_Cip_Customer_File_Pnd_C_Parent = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Parent", "#C-PARENT", FieldType.STRING, 
            1);
        pnd_Cip_Customer_File_Pnd_C_Name = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Name", "#C-NAME", FieldType.STRING, 
            41);
        pnd_Cip_Customer_File_Pnd_C_Dba_Sic = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Dba_Sic", "#C-DBA-SIC", FieldType.STRING, 
            2);
        pnd_Cip_Customer_File_Pnd_C_Address = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Address", "#C-ADDRESS", FieldType.STRING, 
            121);

        pnd_Cip_Customer_File__R_Field_4 = pnd_Cip_Customer_File__R_Field_3.newGroupInGroup("pnd_Cip_Customer_File__R_Field_4", "REDEFINE", pnd_Cip_Customer_File_Pnd_C_Address);
        pnd_Cip_Customer_File_Pnd_C_Addr = pnd_Cip_Customer_File__R_Field_4.newFieldArrayInGroup("pnd_Cip_Customer_File_Pnd_C_Addr", "#C-ADDR", FieldType.STRING, 
            1, new DbsArrayController(1, 121));
        pnd_Cip_Customer_File_Pnd_C_City = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_City", "#C-CITY", FieldType.STRING, 
            41);

        pnd_Cip_Customer_File__R_Field_5 = pnd_Cip_Customer_File__R_Field_3.newGroupInGroup("pnd_Cip_Customer_File__R_Field_5", "REDEFINE", pnd_Cip_Customer_File_Pnd_C_City);
        pnd_Cip_Customer_File_Pnd_C_Cty = pnd_Cip_Customer_File__R_Field_5.newFieldArrayInGroup("pnd_Cip_Customer_File_Pnd_C_Cty", "#C-CTY", FieldType.STRING, 
            1, new DbsArrayController(1, 41));
        pnd_Cip_Customer_File_Pnd_C_State = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_State", "#C-STATE", FieldType.STRING, 
            3);
        pnd_Cip_Customer_File_Pnd_C_Zip = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Zip", "#C-ZIP", FieldType.STRING, 
            15);
        pnd_Cip_Customer_File_Pnd_C_Country = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Country", "#C-COUNTRY", FieldType.STRING, 
            11);
        pnd_Cip_Customer_File_Pnd_C_Evening_Phone = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Evening_Phone", "#C-EVENING-PHONE", 
            FieldType.STRING, 20);
        pnd_Cip_Customer_File_Pnd_C_Email = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Email", "#C-EMAIL", FieldType.STRING, 
            1);
        pnd_Cip_Customer_File_Pnd_C_Tin = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Tin", "#C-TIN", FieldType.STRING, 
            20);
        pnd_Cip_Customer_File_Pnd_C_Passport = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Passport", "#C-PASSPORT", 
            FieldType.STRING, 1);
        pnd_Cip_Customer_File_Pnd_C_Dob = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Dob", "#C-DOB", FieldType.STRING, 
            9);
        pnd_Cip_Customer_File_Pnd_C_Typeofbusiness = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Typeofbusiness", "#C-TYPEOFBUSINESS", 
            FieldType.STRING, 40);
        pnd_Cip_Customer_File_Pnd_C_Filler1 = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Filler1", "#C-FILLER1", FieldType.STRING, 
            10);
        pnd_Cip_Customer_File_Pnd_C_Ownerbranch = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Ownerbranch", "#C-OWNERBRANCH", 
            FieldType.STRING, 11);
        pnd_Cip_Customer_File_Pnd_C_Ownerdept = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Ownerdept", "#C-OWNERDEPT", 
            FieldType.STRING, 11);

        pnd_Cip_Customer_File__R_Field_6 = pnd_Cip_Customer_File__R_Field_3.newGroupInGroup("pnd_Cip_Customer_File__R_Field_6", "REDEFINE", pnd_Cip_Customer_File_Pnd_C_Ownerdept);
        pnd_Cip_Customer_File_Pnd_C_Od = pnd_Cip_Customer_File__R_Field_6.newFieldArrayInGroup("pnd_Cip_Customer_File_Pnd_C_Od", "#C-OD", FieldType.STRING, 
            1, new DbsArrayController(1, 11));
        pnd_Cip_Customer_File_Pnd_C_Owneroper = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Owneroper", "#C-OWNEROPER", 
            FieldType.STRING, 11);
        pnd_Cip_Customer_File_Pnd_C_Filler2 = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Filler2", "#C-FILLER2", FieldType.STRING, 
            16);
        pnd_Cip_Customer_File_Pnd_C_Sex = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Sex", "#C-SEX", FieldType.STRING, 
            1);
        pnd_Cip_Customer_File_Pnd_C_Customer_Type = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Customer_Type", "#C-CUSTOMER-TYPE", 
            FieldType.STRING, 12);
        pnd_Cip_Customer_File_Pnd_C_Filler3 = pnd_Cip_Customer_File__R_Field_3.newFieldInGroup("pnd_Cip_Customer_File_Pnd_C_Filler3", "#C-FILLER3", FieldType.STRING, 
            9);
        pnd_Compressed_A = localVariables.newFieldArrayInRecord("pnd_Compressed_A", "#COMPRESSED-A", FieldType.STRING, 1, new DbsArrayController(1, 500));

        pnd_Cip_Account_File = localVariables.newGroupInRecord("pnd_Cip_Account_File", "#CIP-ACCOUNT-FILE");
        pnd_Cip_Account_File_Pnd_A_Version = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Version", "#A-VERSION", FieldType.STRING, 
            10);
        pnd_Cip_Account_File_Pnd_A_Id = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Id", "#A-ID", FieldType.STRING, 11);
        pnd_Cip_Account_File_Pnd_A_Name = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Name", "#A-NAME", FieldType.STRING, 40);
        pnd_Cip_Account_File_Pnd_A_Monitor = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Monitor", "#A-MONITOR", FieldType.STRING, 
            2);
        pnd_Cip_Account_File_Pnd_A_Ex_Status = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Ex_Status", "#A-EX-STATUS", FieldType.STRING, 
            11);
        pnd_Cip_Account_File_Pnd_A_Notes = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Notes", "#A-NOTES", FieldType.STRING, 3);
        pnd_Cip_Account_File_Pnd_A_Branch = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Branch", "#A-BRANCH", FieldType.STRING, 11);
        pnd_Cip_Account_File_Pnd_A_Open_Date = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Open_Date", "#A-OPEN-DATE", FieldType.STRING, 
            9);
        pnd_Cip_Account_File_Pnd_A_Owner_Branch = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Owner_Branch", "#A-OWNER-BRANCH", FieldType.STRING, 
            11);
        pnd_Cip_Account_File_Pnd_A_Ownerdept = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Ownerdept", "#A-OWNERDEPT", FieldType.STRING, 
            11);

        pnd_Cip_Account_File__R_Field_7 = pnd_Cip_Account_File.newGroupInGroup("pnd_Cip_Account_File__R_Field_7", "REDEFINE", pnd_Cip_Account_File_Pnd_A_Ownerdept);
        pnd_Cip_Account_File_Pnd_A_Od = pnd_Cip_Account_File__R_Field_7.newFieldArrayInGroup("pnd_Cip_Account_File_Pnd_A_Od", "#A-OD", FieldType.STRING, 
            1, new DbsArrayController(1, 11));
        pnd_Cip_Account_File_Pnd_A_Owneroper = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Owneroper", "#A-OWNEROPER", FieldType.STRING, 
            11);
        pnd_Cip_Account_File_Pnd_A_Closed = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Closed", "#A-CLOSED", FieldType.STRING, 1);
        pnd_Cip_Account_File_Pnd_A_Pin = pnd_Cip_Account_File.newFieldInGroup("pnd_Cip_Account_File_Pnd_A_Pin", "#A-PIN", FieldType.STRING, 12);
        pnd_Compressed_B = localVariables.newFieldInRecord("pnd_Compressed_B", "#COMPRESSED-B", FieldType.STRING, 250);
        pnd_Work_Ownerdept = localVariables.newFieldInRecord("pnd_Work_Ownerdept", "#WORK-OWNERDEPT", FieldType.STRING, 11);

        pnd_Work_Ownerdept__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Ownerdept__R_Field_8", "REDEFINE", pnd_Work_Ownerdept);
        pnd_Work_Ownerdept_Pnd_W_Od = pnd_Work_Ownerdept__R_Field_8.newFieldArrayInGroup("pnd_Work_Ownerdept_Pnd_W_Od", "#W-OD", FieldType.STRING, 1, 
            new DbsArrayController(1, 11));
        pnd_Work_City = localVariables.newFieldInRecord("pnd_Work_City", "#WORK-CITY", FieldType.STRING, 40);
        pnd_Work_State = localVariables.newFieldInRecord("pnd_Work_State", "#WORK-STATE", FieldType.STRING, 2);
        pnd_Work_Name = localVariables.newFieldInRecord("pnd_Work_Name", "#WORK-NAME", FieldType.STRING, 40);

        pnd_Work_Name__R_Field_9 = localVariables.newGroupInRecord("pnd_Work_Name__R_Field_9", "REDEFINE", pnd_Work_Name);
        pnd_Work_Name_Pnd_Work_Name_39 = pnd_Work_Name__R_Field_9.newFieldInGroup("pnd_Work_Name_Pnd_Work_Name_39", "#WORK-NAME-39", FieldType.STRING, 
            39);
        pnd_Work_Dob = localVariables.newFieldInRecord("pnd_Work_Dob", "#WORK-DOB", FieldType.NUMERIC, 8);
        pnd_Ap_Dob = localVariables.newFieldInRecord("pnd_Ap_Dob", "#AP-DOB", FieldType.NUMERIC, 8);

        pnd_Ap_Dob__R_Field_10 = localVariables.newGroupInRecord("pnd_Ap_Dob__R_Field_10", "REDEFINE", pnd_Ap_Dob);
        pnd_Ap_Dob_Pnd_Work_Mm = pnd_Ap_Dob__R_Field_10.newFieldInGroup("pnd_Ap_Dob_Pnd_Work_Mm", "#WORK-MM", FieldType.STRING, 2);
        pnd_Ap_Dob_Pnd_Work_Dd = pnd_Ap_Dob__R_Field_10.newFieldInGroup("pnd_Ap_Dob_Pnd_Work_Dd", "#WORK-DD", FieldType.STRING, 2);
        pnd_Ap_Dob_Pnd_Work_Yyyy = pnd_Ap_Dob__R_Field_10.newFieldInGroup("pnd_Ap_Dob_Pnd_Work_Yyyy", "#WORK-YYYY", FieldType.STRING, 4);
        pnd_Op_Dt = localVariables.newFieldInRecord("pnd_Op_Dt", "#OP-DT", FieldType.NUMERIC, 8);

        pnd_Op_Dt__R_Field_11 = localVariables.newGroupInRecord("pnd_Op_Dt__R_Field_11", "REDEFINE", pnd_Op_Dt);
        pnd_Op_Dt_Pnd_Work_Mmo = pnd_Op_Dt__R_Field_11.newFieldInGroup("pnd_Op_Dt_Pnd_Work_Mmo", "#WORK-MMO", FieldType.STRING, 2);
        pnd_Op_Dt_Pnd_Work_Ddo = pnd_Op_Dt__R_Field_11.newFieldInGroup("pnd_Op_Dt_Pnd_Work_Ddo", "#WORK-DDO", FieldType.STRING, 2);
        pnd_Op_Dt_Pnd_Work_Yyyyo = pnd_Op_Dt__R_Field_11.newFieldInGroup("pnd_Op_Dt_Pnd_Work_Yyyyo", "#WORK-YYYYO", FieldType.STRING, 4);
        pnd_Prt_Date = localVariables.newFieldInRecord("pnd_Prt_Date", "#PRT-DATE", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.NUMERIC, 3);
        pnd_Is = localVariables.newFieldInRecord("pnd_Is", "#IS", FieldType.NUMERIC, 3);
        pnd_How_Many = localVariables.newFieldInRecord("pnd_How_Many", "#HOW-MANY", FieldType.NUMERIC, 9);
        pnd_Update_Isn = localVariables.newFieldInRecord("pnd_Update_Isn", "#UPDATE-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Geo_Grp = localVariables.newFieldInRecord("pnd_Geo_Grp", "#GEO-GRP", FieldType.STRING, 4);

        pnd_Geo_Grp__R_Field_12 = localVariables.newGroupInRecord("pnd_Geo_Grp__R_Field_12", "REDEFINE", pnd_Geo_Grp);
        pnd_Geo_Grp_Pnd_State = pnd_Geo_Grp__R_Field_12.newFieldInGroup("pnd_Geo_Grp_Pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Geo_Grp_Pnd_Code = pnd_Geo_Grp__R_Field_12.newFieldInGroup("pnd_Geo_Grp_Pnd_Code", "#CODE", FieldType.STRING, 2);
        pnd_Geo_State_Tbl = localVariables.newFieldInRecord("pnd_Geo_State_Tbl", "#GEO-STATE-TBL", FieldType.STRING, 228);

        pnd_Geo_State_Tbl__R_Field_13 = localVariables.newGroupInRecord("pnd_Geo_State_Tbl__R_Field_13", "REDEFINE", pnd_Geo_State_Tbl);
        pnd_Geo_State_Tbl_Pnd_State_Code = pnd_Geo_State_Tbl__R_Field_13.newFieldArrayInGroup("pnd_Geo_State_Tbl_Pnd_State_Code", "#STATE-CODE", FieldType.STRING, 
            4, new DbsArrayController(1, 57));
        pnd_Ap_Soc_Sec = localVariables.newFieldInRecord("pnd_Ap_Soc_Sec", "#AP-SOC-SEC", FieldType.STRING, 9);
        pnd_Work_Area = localVariables.newFieldInRecord("pnd_Work_Area", "#WORK-AREA", FieldType.STRING, 35);
        pnd_Save_Area = localVariables.newFieldInRecord("pnd_Save_Area", "#SAVE-AREA", FieldType.STRING, 35);
        pnd_Remainder = localVariables.newFieldInRecord("pnd_Remainder", "#REMAINDER", FieldType.STRING, 35);
        pnd_Number = localVariables.newFieldInRecord("pnd_Number", "#NUMBER", FieldType.PACKED_DECIMAL, 3);
        pnd_Length = localVariables.newFieldInRecord("pnd_Length", "#LENGTH", FieldType.PACKED_DECIMAL, 3);
        pnd_Foreign_Addr = localVariables.newFieldInRecord("pnd_Foreign_Addr", "#FOREIGN-ADDR", FieldType.BOOLEAN, 1);
        pnd_Atra = localVariables.newFieldInRecord("pnd_Atra", "#ATRA", FieldType.STRING, 5);

        pnd_Atra__R_Field_14 = localVariables.newGroupInRecord("pnd_Atra__R_Field_14", "REDEFINE", pnd_Atra);
        pnd_Atra_Pnd_Atra_A2 = pnd_Atra__R_Field_14.newFieldInGroup("pnd_Atra_Pnd_Atra_A2", "#ATRA-A2", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        localVariables.reset();
        pnd_Foreign_Addr.setInitialValue(false);
        pnd_Atra.setInitialValue("ATRA~");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb4100() throws Exception
    {
        super("Appb4100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) LS = 132 PS = 60
        //*  WITH AP-RELEASE-IND EQ 1
        pnd_Geo_State_Tbl.setValue("AL01AK02AR04AZ03CA05CO07CT08DC10DE09FL11GA12GU97HI14IA18ID15IL16IN17KS19KY20LA21MA24MD23ME22MI25MN26MO28MS27MT29NE30NC36ND37NH32NJ33NM34NV31NY35OH38OK39OR40PA41PR42RI43SC45SD46TN47TX48UT49VT50VA51VI52WA54WV55WI56WY57AS97CM97TT97"); //Natural: ASSIGN #GEO-STATE-TBL = 'AL01AK02AR04AZ03CA05CO07CT08DC10DE09FL11GA12GU97HI14IA18ID15IL16IN17KS19KY20LA21MA24MD23ME22MI25MN26MO28MS27MT29NE30NC36ND37NH32NJ33NM34NV31NY35OH38OK39OR40PA41PR42RI43SC45SD46TN47TX48UT49VT50VA51VI52WA54WV55WI56WY57AS97CM97TT97'
        vw_annty_Actvty_Prap_View.startDatabaseRead                                                                                                                       //Natural: READ ANNTY-ACTVTY-PRAP-VIEW WHERE AP-RECORD-TYPE EQ 1
        (
        "PND_PND_L2920",
        new Wc[] { new Wc("AP_RECORD_TYPE", "=", 1, WcType.WHERE) },
        new Oc[] { new Oc("ISN", "ASC") }
        );
        PND_PND_L2920:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("PND_PND_L2920")))
        {
            //* *WRITE AP-PIN-NBR '=' AP-SOC-SEC
            if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("N")))                                                                                              //Natural: REJECT IF AP-APP-SOURCE EQ 'N'
            {
                continue;
            }
            //*  NBC RHSP CHANGES
            if (condition(annty_Actvty_Prap_View_App_Pref.equals("W") && annty_Actvty_Prap_View_App_Cont.greaterOrEqual(100000) && annty_Actvty_Prap_View_App_Cont.lessOrEqual(499999))) //Natural: REJECT IF APP-PREF = 'W' AND ( APP-CONT GE 100000 AND APP-CONT LE 499999 )
            {
                continue;
            }
            //*  RHSP CHANGES NBC
            if (condition(annty_Actvty_Prap_View_Ap_App_Source.equals("S")))                                                                                              //Natural: REJECT IF AP-APP-SOURCE EQ 'S'
            {
                continue;
            }
            if (condition(!(annty_Actvty_Prap_View_Ap_Addr_Usage_Code.equals("1"))))                                                                                      //Natural: ACCEPT IF AP-ADDR-USAGE-CODE EQ '1'
            {
                continue;
            }
            pnd_Update_Isn.setValue(vw_annty_Actvty_Prap_View.getAstISN("PND_PND_L2920"));                                                                                //Natural: ASSIGN #UPDATE-ISN := *ISN ( ##L2920. )
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 500
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(500)); pnd_I.nadd(1))
            {
                pnd_Cip_Customer_File.getValue(pnd_I).setValue(" ");                                                                                                      //Natural: MOVE ' ' TO #CIP-CUSTOMER-FILE ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO 500
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(500)); pnd_I.nadd(1))
            {
                pnd_Compressed_A.getValue(pnd_I).setValue(" ");                                                                                                           //Natural: MOVE ' ' TO #COMPRESSED-A ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cip_Customer_File_Pnd_C_Pin.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Pin_Nbr, "~"));                            //Natural: COMPRESS AP-PIN-NBR '~' INTO #C-PIN LEAVING NO
            pnd_Cip_Customer_File_Pnd_C_Parent.setValue("~");                                                                                                             //Natural: ASSIGN #C-PARENT := '~'
            //*  IF AP-COR-MDDLE-NME > ' '                         /* JR-1
            //*    COMPRESS                                        /* JR-1
            //*      AP-COR-FIRST-NME '@'                          /* JR-1
            //*      AP-COR-MDDLE-NME '@'                          /* JR-1
            //*      AP-COR-LAST-NME INTO #WORK-NAME LEAVING NO    /* JR-1
            //*  ELSE                                              /* JR-1
            pnd_Work_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Cor_First_Nme, "@", annty_Actvty_Prap_View_Ap_Cor_Last_Nme)); //Natural: COMPRESS AP-COR-FIRST-NME '@' AP-COR-LAST-NME INTO #WORK-NAME LEAVING NO
            //*   END-IF                                           /* JR-1
            pnd_Cip_Customer_File_Pnd_C_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Name, "~"));                                               //Natural: COMPRESS #WORK-NAME '~' INTO #C-NAME LEAVING NO
            pnd_Cip_Customer_File_Pnd_C_Dba_Sic.setValue("~~");                                                                                                           //Natural: ASSIGN #C-DBA-SIC := '~~'
            //*   COMPRESS
            //*     AP-ADDRESS-LINE (1)
            //*     AP-ADDRESS-LINE (2)
            //*     AP-ADDRESS-LINE (3)
            //*     AP-ADDRESS-LINE (4)
            //*     AP-ADDRESS-LINE (5) INTO #C-ADDRESS LEAVING NO
            //*  (JC1) BEGIN
            pnd_Cip_Customer_File_Pnd_C_City.reset();                                                                                                                     //Natural: RESET #C-CITY
            pnd_Foreign_Addr.setValue(false);                                                                                                                             //Natural: ASSIGN #FOREIGN-ADDR := FALSE
            DbsUtil.examine(new ExamineSource(annty_Actvty_Prap_View_Ap_Name_Addr_Cd), new ExamineSearch("A"), new ExamineGivingLength(pnd_Length));                      //Natural: EXAMINE AP-NAME-ADDR-CD FOR 'A' GIVING LENGTH IN #LENGTH
            short decideConditionsMet350 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-NAME-ADDR-CD EQ 'US'
            if (condition(annty_Actvty_Prap_View_Ap_Name_Addr_Cd.equals("US")))
            {
                decideConditionsMet350++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN #LENGTH EQ 2
            else if (condition(pnd_Length.equals(2)))
            {
                decideConditionsMet350++;
                pnd_Foreign_Addr.setValue(true);                                                                                                                          //Natural: ASSIGN #FOREIGN-ADDR := TRUE
                //*  (JC3)
                if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.notEquals("SGRD")))                                                                                     //Natural: IF AP-COLL-CODE NE 'SGRD'
                {
                    //*  SETS #C-CITY
                                                                                                                                                                          //Natural: PERFORM FIND-FOREIGN-CITY
                    sub_Find_Foreign_City();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  (JC3)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                annty_Actvty_Prap_View_Ap_Name_Addr_Cd.reset();                                                                                                           //Natural: RESET AP-NAME-ADDR-CD
                //*  (JC1) END
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  (JC1)
            pnd_Cip_Customer_File_Pnd_C_Address.setValue(DbsUtil.compress(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(1), annty_Actvty_Prap_View_Ap_Address_Txt.getValue(2),  //Natural: COMPRESS AP-ADDRESS-TXT ( 1 ) AP-ADDRESS-TXT ( 2 ) AP-ADDRESS-TXT ( 3 ) INTO #C-ADDRESS
                annty_Actvty_Prap_View_Ap_Address_Txt.getValue(3)));
            //*    AP-ADDRESS-TXT (3) INTO #C-ADDRESS LEAVING NO     /* (JC1)
            //*  (JC3)
            //*  (JC3)
            if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.equals("SGRD")))                                                                                            //Natural: IF AP-COLL-CODE EQ 'SGRD'
            {
                pnd_Cip_Customer_File_Pnd_C_Address.setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(1));                                                          //Natural: ASSIGN #C-ADDRESS := AP-ADDRESS-TXT ( 1 )
                //*  (JC3)
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #I 121 TO 1 STEP -1
            for (pnd_I.setValue(121); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
            {
                if (condition(pnd_Cip_Customer_File_Pnd_C_Addr.getValue(pnd_I).notEquals(" ") && pnd_I.equals(121)))                                                      //Natural: IF #C-ADDR ( #I ) NE ' ' AND #I EQ 121
                {
                    pnd_Cip_Customer_File_Pnd_C_Addr.getValue(pnd_I).setValue("~");                                                                                       //Natural: ASSIGN #C-ADDR ( #I ) := '~'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Cip_Customer_File_Pnd_C_Addr.getValue(pnd_I).notEquals(" ")))                                                                           //Natural: IF #C-ADDR ( #I ) NE ' '
                {
                    pnd_I2.compute(new ComputeParameters(false, pnd_I2), pnd_I.add(1));                                                                                   //Natural: COMPUTE #I2 = #I + 1
                    pnd_Cip_Customer_File_Pnd_C_Addr.getValue(pnd_I2).setValue("~");                                                                                      //Natural: ASSIGN #C-ADDR ( #I2 ) := '~'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Cip_Customer_File_Pnd_C_Address.equals(" ")))                                                                                               //Natural: IF #C-ADDRESS EQ ' '
            {
                pnd_Cip_Customer_File_Pnd_C_Addr.getValue(1).setValue("~");                                                                                               //Natural: ASSIGN #C-ADDR ( 1 ) := '~'
            }                                                                                                                                                             //Natural: END-IF
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO 121
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(121)); pnd_I.nadd(1))
            {
                if (condition(pnd_Cip_Customer_File_Pnd_C_Addr.getValue(pnd_I).equals("~")))                                                                              //Natural: IF #C-ADDR ( #I ) EQ '~'
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Cip_Customer_File_Pnd_C_Addr.getValue(pnd_I).equals(" ")))                                                                              //Natural: IF #C-ADDR ( #I ) EQ ' '
                {
                    pnd_Cip_Customer_File_Pnd_C_Addr.getValue(pnd_I).setValue("@");                                                                                       //Natural: MOVE '@' TO #C-ADDR ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   ASSIGN #WORK-CITY := AP-CITY
            pnd_Work_City.setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(4));                                                                                    //Natural: ASSIGN #WORK-CITY := AP-ADDRESS-TXT ( 4 )
            //*  (JC1)
            if (condition(pnd_Cip_Customer_File_Pnd_C_City.equals(" ")))                                                                                                  //Natural: IF #C-CITY EQ ' '
            {
                pnd_Cip_Customer_File_Pnd_C_City.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_City, "~"));                                           //Natural: COMPRESS #WORK-CITY '~' INTO #C-CITY LEAVING NO
                //*  (JC1)
            }                                                                                                                                                             //Natural: END-IF
            FOR05:                                                                                                                                                        //Natural: FOR #I 1 TO 41
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(41)); pnd_I.nadd(1))
            {
                if (condition(pnd_Cip_Customer_File_Pnd_C_Cty.getValue(pnd_I).equals("~")))                                                                               //Natural: IF #C-CTY ( #I ) EQ '~'
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Cip_Customer_File_Pnd_C_Cty.getValue(pnd_I).equals(" ")))                                                                               //Natural: IF #C-CTY ( #I ) EQ ' '
                {
                    pnd_Cip_Customer_File_Pnd_C_Cty.getValue(pnd_I).setValue("@");                                                                                        //Natural: MOVE '@' TO #C-CTY ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   IF AP-CURRENT-STATE-CODE EQ 'FN'
            //*     ASSIGN #WORK-STATE := 'FN'
            //*   ELSE
            //*     EXAMINE #STATE-CODE(*) FOR AP-CURRENT-STATE-CODE GIVING INDEX #IS
            //*     IF #IS > 0 AND #IS < 58
            //*       MOVE #STATE-CODE (#IS) TO #GEO-GRP
            //*       MOVE #STATE           TO #WORK-STATE
            //*     ELSE
            //*       MOVE ' ' TO #WORK-STATE
            //*     END-IF
            //*   END-IF
            //*   COMPRESS #WORK-STATE '~' INTO #C-STATE LEAVING NO
            pnd_Cip_Customer_File_Pnd_C_State.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Address_Txt.getValue(5),                 //Natural: COMPRESS AP-ADDRESS-TXT ( 5 ) '~' INTO #C-STATE LEAVING NO
                "~"));
            //*  MS-1
            pnd_Cip_Customer_File_Pnd_C_Zip.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Zip_Code.getSubstring(1,5),                //Natural: COMPRESS SUBSTRING ( AP-ZIP-CODE,1,5 ) '~' INTO #C-ZIP LEAVING NO
                "~"));
            //*  MS-1
            if (condition(pnd_Work_State.equals("FN")))                                                                                                                   //Natural: IF #WORK-STATE EQ 'FN'
            {
                pnd_Cip_Customer_File_Pnd_C_Country.setValue("FN~");                                                                                                      //Natural: ASSIGN #C-COUNTRY := 'FN~'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cip_Customer_File_Pnd_C_Country.setValue("US~");                                                                                                      //Natural: ASSIGN #C-COUNTRY := 'US~'
            }                                                                                                                                                             //Natural: END-IF
            //*                                                         (JC1) BEGIN
            if (condition(annty_Actvty_Prap_View_Ap_Name_Addr_Cd.notEquals(" ")))                                                                                         //Natural: IF AP-NAME-ADDR-CD NE ' '
            {
                pnd_Cip_Customer_File_Pnd_C_Country.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Name_Addr_Cd, "~"));               //Natural: COMPRESS AP-NAME-ADDR-CD '~' INTO #C-COUNTRY LEAVING NO SPACE
                if (condition(pnd_Foreign_Addr.getBoolean()))                                                                                                             //Natural: IF #FOREIGN-ADDR
                {
                    pnd_Cip_Customer_File_Pnd_C_State.setValue("~");                                                                                                      //Natural: ASSIGN #C-STATE := '~'
                    pnd_Cip_Customer_File_Pnd_C_Zip.setValue("~");                                                                                                        //Natural: ASSIGN #C-ZIP := '~'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                                         (JC1) END
            pnd_Cip_Customer_File_Pnd_C_Evening_Phone.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Pnd_Evening_Phone_Number,           //Natural: COMPRESS #EVENING-PHONE-NUMBER '~' INTO #C-EVENING-PHONE LEAVING NO
                "~"));
            pnd_Cip_Customer_File_Pnd_C_Email.setValue("~");                                                                                                              //Natural: ASSIGN #C-EMAIL := '~'
            pnd_Ap_Soc_Sec.setValueEdited(annty_Actvty_Prap_View_Ap_Soc_Sec,new ReportEditMask("999999999"));                                                             //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999999999 ) TO #AP-SOC-SEC
            pnd_Cip_Customer_File_Pnd_C_Tin.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ap_Soc_Sec, "~"));                                               //Natural: COMPRESS #AP-SOC-SEC '~' INTO #C-TIN LEAVING NO
            pnd_Cip_Customer_File_Pnd_C_Passport.setValue("~");                                                                                                           //Natural: ASSIGN #C-PASSPORT := '~'
            //*  ASSIGN #WORK-DOB := AP-DOB
            pnd_Ap_Dob.setValue(annty_Actvty_Prap_View_Ap_Dob);                                                                                                           //Natural: ASSIGN #AP-DOB := AP-DOB
            pnd_Cip_Customer_File_Pnd_C_Dob.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ap_Dob_Pnd_Work_Yyyy, pnd_Ap_Dob_Pnd_Work_Mm,                    //Natural: COMPRESS #WORK-YYYY #WORK-MM #WORK-DD '~' INTO #C-DOB LEAVING NO
                pnd_Ap_Dob_Pnd_Work_Dd, "~"));
            pnd_Cip_Customer_File_Pnd_C_Typeofbusiness.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Lob, annty_Actvty_Prap_View_Ap_Lob_Type,  //Natural: COMPRESS AP-LOB AP-LOB-TYPE '~' INTO #C-TYPEOFBUSINESS LEAVING NO
                "~"));
            //*                                                         (JC2) BEGIN
            //*  RA
            if (condition((annty_Actvty_Prap_View_Ap_Lob.equals("D") && (annty_Actvty_Prap_View_Ap_Lob_Type.equals("2") || annty_Actvty_Prap_View_Ap_Lob_Type.equals("8"))))) //Natural: IF AP-LOB EQ 'D' AND ( AP-LOB-TYPE EQ '2' OR EQ '8' )
            {
                if (condition(((DbsUtil.maskMatches(annty_Actvty_Prap_View_Ap_Coll_Code,"NNNN") && annty_Actvty_Prap_View_Ap_Coll_Code.equals("9004"))                    //Natural: IF AP-COLL-CODE = MASK ( NNNN ) AND AP-COLL-CODE = '9004' OR ( AP-COLL-CODE = '9006' THRU '9020' BUT NOT '9009' )
                    || ((annty_Actvty_Prap_View_Ap_Coll_Code.greaterOrEqual("9006") && annty_Actvty_Prap_View_Ap_Coll_Code.lessOrEqual("9020")) && annty_Actvty_Prap_View_Ap_Coll_Code.notEquals("9009")))))
                {
                    pnd_Cip_Customer_File_Pnd_C_Typeofbusiness.setValue(pnd_Atra);                                                                                        //Natural: ASSIGN #C-TYPEOFBUSINESS := #ATRA
                }                                                                                                                                                         //Natural: END-IF
                //*  ATRA KG START
                if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,2).equals("AA")))                                                                  //Natural: IF SUBSTRING ( AP-SGRD-SUBPLAN-NO,1,2 ) = 'AA'
                {
                    pnd_Cip_Customer_File_Pnd_C_Typeofbusiness.setValue(pnd_Atra);                                                                                        //Natural: ASSIGN #C-TYPEOFBUSINESS := #ATRA
                    //*  ATRA KG END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                                         (JC2) END
            pnd_Cip_Customer_File_Pnd_C_Filler1.setValue("~~~~~~~~~~");                                                                                                   //Natural: ASSIGN #C-FILLER1 := '~~~~~~~~~~'
            pnd_Cip_Customer_File_Pnd_C_Ownerbranch.setValue("RETSERVICE~");                                                                                              //Natural: ASSIGN #C-OWNERBRANCH := 'RETSERVICE~'
            pnd_Cip_Customer_File_Pnd_C_Ownerdept.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Ppg_Team_Cde, "~"));                 //Natural: COMPRESS AP-PPG-TEAM-CDE '~' INTO #C-OWNERDEPT LEAVING NO
            pnd_I2.reset();                                                                                                                                               //Natural: RESET #I2 #WORK-OWNERDEPT
            pnd_Work_Ownerdept.reset();
            FOR06:                                                                                                                                                        //Natural: FOR #I 1 TO 11
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(11)); pnd_I.nadd(1))
            {
                if (condition(pnd_Cip_Customer_File_Pnd_C_Od.getValue(pnd_I).notEquals(" ")))                                                                             //Natural: IF #C-OD ( #I ) NE ' '
                {
                    pnd_I2.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #I2
                    pnd_Work_Ownerdept_Pnd_W_Od.getValue(pnd_I2).setValue(pnd_Cip_Customer_File_Pnd_C_Od.getValue(pnd_I));                                                //Natural: MOVE #C-OD ( #I ) TO #W-OD ( #I2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cip_Customer_File_Pnd_C_Ownerdept.setValue(pnd_Work_Ownerdept);                                                                                           //Natural: ASSIGN #C-OWNERDEPT := #WORK-OWNERDEPT
            pnd_Cip_Customer_File_Pnd_C_Owneroper.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Pnd_Ap_Racf_Id, "~"));                  //Natural: COMPRESS #AP-RACF-ID '~' INTO #C-OWNEROPER LEAVING NO
            pnd_Cip_Customer_File_Pnd_C_Filler2.setValue("~~~~~~~~~~~~~~~~");                                                                                             //Natural: ASSIGN #C-FILLER2 := '~~~~~~~~~~~~~~~~'
            if (condition(annty_Actvty_Prap_View_Ap_Sex.equals("U")))                                                                                                     //Natural: IF AP-SEX = 'U'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cip_Customer_File_Pnd_C_Sex.setValue(annty_Actvty_Prap_View_Ap_Sex);                                                                                  //Natural: ASSIGN #C-SEX := AP-SEX
            }                                                                                                                                                             //Natural: END-IF
            //*  AML
            pnd_Cip_Customer_File_Pnd_C_Customer_Type.setValue("~");                                                                                                      //Natural: ASSIGN #C-CUSTOMER-TYPE := '~'
            pnd_Cip_Customer_File_Pnd_C_Filler3.setValue("~~~~~~~~~");                                                                                                    //Natural: ASSIGN #C-FILLER3 := '~~~~~~~~~'
            pnd_I2.reset();                                                                                                                                               //Natural: RESET #I2
            FOR07:                                                                                                                                                        //Natural: FOR #I 1 TO 500
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(500)); pnd_I.nadd(1))
            {
                if (condition(pnd_Cip_Customer_File.getValue(pnd_I).notEquals(" ")))                                                                                      //Natural: IF #CIP-CUSTOMER-FILE ( #I ) NE ' '
                {
                    pnd_I2.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #I2
                    pnd_Compressed_A.getValue(pnd_I2).setValue(pnd_Cip_Customer_File.getValue(pnd_I));                                                                    //Natural: MOVE #CIP-CUSTOMER-FILE ( #I ) TO #COMPRESSED-A ( #I2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR08:                                                                                                                                                        //Natural: FOR #I 1 TO 500
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(500)); pnd_I.nadd(1))
            {
                if (condition(pnd_Compressed_A.getValue(pnd_I).equals("@")))                                                                                              //Natural: IF #COMPRESSED-A ( #I ) EQ '@'
                {
                    pnd_Compressed_A.getValue(pnd_I).setValue(" ");                                                                                                       //Natural: MOVE ' ' TO #COMPRESSED-A ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(1, false, pnd_Compressed_A.getValue("*"));                                                                                               //Natural: WRITE WORK FILE 1 #COMPRESSED-A ( * )
            pnd_How_Many.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #HOW-MANY
            pnd_Cip_Account_File.reset();                                                                                                                                 //Natural: RESET #CIP-ACCOUNT-FILE
            pnd_Cip_Account_File_Pnd_A_Version.setValue("PRIME-5.2~");                                                                                                    //Natural: ASSIGN #A-VERSION := 'PRIME-5.2~'
            pnd_Cip_Account_File_Pnd_A_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct, "~"));                          //Natural: COMPRESS AP-TIAA-CNTRCT '~' INTO #A-ID LEAVING NO
            pnd_Cip_Account_File_Pnd_A_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Lob, annty_Actvty_Prap_View_Ap_Lob_Type,   //Natural: COMPRESS AP-LOB AP-LOB-TYPE '~' INTO #A-NAME LEAVING NO
                "~"));
            //*                                                         (JC2) BEGIN
            if (condition(pnd_Cip_Customer_File_Pnd_C_Typeofbusiness.equals(pnd_Atra)))                                                                                   //Natural: IF #C-TYPEOFBUSINESS = #ATRA
            {
                pnd_Cip_Account_File_Pnd_A_Name.setValue(pnd_Atra);                                                                                                       //Natural: ASSIGN #A-NAME := #ATRA
            }                                                                                                                                                             //Natural: END-IF
            //*                                                         (JC2) END
            pnd_Cip_Account_File_Pnd_A_Monitor.setValue("1~");                                                                                                            //Natural: ASSIGN #A-MONITOR := '1~'
            pnd_Cip_Account_File_Pnd_A_Ex_Status.setValue("~");                                                                                                           //Natural: ASSIGN #A-EX-STATUS := '~'
            pnd_Cip_Account_File_Pnd_A_Notes.setValue("~");                                                                                                               //Natural: ASSIGN #A-NOTES := '~'
            pnd_Cip_Account_File_Pnd_A_Branch.setValue("RETSERVICE~");                                                                                                    //Natural: ASSIGN #A-BRANCH := 'RETSERVICE~'
            pnd_Op_Dt.setValue(annty_Actvty_Prap_View_Ap_Dt_Ent_Sys);                                                                                                     //Natural: ASSIGN #OP-DT := AP-DT-ENT-SYS
            pnd_Cip_Account_File_Pnd_A_Open_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Op_Dt_Pnd_Work_Yyyyo, pnd_Op_Dt_Pnd_Work_Mmo,               //Natural: COMPRESS #WORK-YYYYO #WORK-MMO #WORK-DDO '~' INTO #A-OPEN-DATE LEAVING NO
                pnd_Op_Dt_Pnd_Work_Ddo, "~"));
            pnd_Cip_Account_File_Pnd_A_Owner_Branch.setValue("RETSERVICE~");                                                                                              //Natural: ASSIGN #A-OWNER-BRANCH := 'RETSERVICE~'
            pnd_Cip_Account_File_Pnd_A_Ownerdept.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Ppg_Team_Cde, "~"));                  //Natural: COMPRESS AP-PPG-TEAM-CDE '~' INTO #A-OWNERDEPT LEAVING NO
            pnd_I2.reset();                                                                                                                                               //Natural: RESET #I2 #WORK-OWNERDEPT
            pnd_Work_Ownerdept.reset();
            FOR09:                                                                                                                                                        //Natural: FOR #I 1 TO 11
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(11)); pnd_I.nadd(1))
            {
                if (condition(pnd_Cip_Account_File_Pnd_A_Od.getValue(pnd_I).notEquals(" ")))                                                                              //Natural: IF #A-OD ( #I ) NE ' '
                {
                    pnd_I2.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #I2
                    pnd_Work_Ownerdept_Pnd_W_Od.getValue(pnd_I2).setValue(pnd_Cip_Account_File_Pnd_A_Od.getValue(pnd_I));                                                 //Natural: MOVE #A-OD ( #I ) TO #W-OD ( #I2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cip_Account_File_Pnd_A_Ownerdept.setValue(pnd_Work_Ownerdept);                                                                                            //Natural: ASSIGN #A-OWNERDEPT := #WORK-OWNERDEPT
            DbsUtil.examine(new ExamineSource(pnd_Cip_Account_File_Pnd_A_Ownerdept), new ExamineSearch(" "), new ExamineReplace("-", true));                              //Natural: EXAMINE #A-OWNERDEPT FOR ' ' REPLACE FIRST WITH '-'
            pnd_Cip_Account_File_Pnd_A_Owneroper.setValue("~");                                                                                                           //Natural: ASSIGN #A-OWNEROPER := '~'
            pnd_Cip_Account_File_Pnd_A_Closed.setValue("~");                                                                                                              //Natural: ASSIGN #A-CLOSED := '~'
            pnd_Cip_Account_File_Pnd_A_Pin.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, annty_Actvty_Prap_View_Ap_Pin_Nbr));                                  //Natural: COMPRESS AP-PIN-NBR INTO #A-PIN LEAVING NO
            pnd_Compressed_B.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Cip_Account_File_Pnd_A_Version, pnd_Cip_Account_File_Pnd_A_Id,                  //Natural: COMPRESS #A-VERSION #A-ID #A-NAME #A-MONITOR #A-EX-STATUS #A-NOTES #A-BRANCH #A-OPEN-DATE #A-OWNER-BRANCH #A-OWNERDEPT #A-OWNEROPER #A-CLOSED #A-PIN INTO #COMPRESSED-B LEAVING NO
                pnd_Cip_Account_File_Pnd_A_Name, pnd_Cip_Account_File_Pnd_A_Monitor, pnd_Cip_Account_File_Pnd_A_Ex_Status, pnd_Cip_Account_File_Pnd_A_Notes, 
                pnd_Cip_Account_File_Pnd_A_Branch, pnd_Cip_Account_File_Pnd_A_Open_Date, pnd_Cip_Account_File_Pnd_A_Owner_Branch, pnd_Cip_Account_File_Pnd_A_Ownerdept, 
                pnd_Cip_Account_File_Pnd_A_Owneroper, pnd_Cip_Account_File_Pnd_A_Closed, pnd_Cip_Account_File_Pnd_A_Pin));
            getWorkFiles().write(2, false, pnd_Compressed_B);                                                                                                             //Natural: WRITE WORK FILE 2 #COMPRESSED-B
            pnd_Work_Name.setValue(DbsUtil.compress(annty_Actvty_Prap_View_Ap_Cor_First_Nme, annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme, annty_Actvty_Prap_View_Ap_Cor_Last_Nme)); //Natural: COMPRESS AP-COR-FIRST-NME AP-COR-MDDLE-NME AP-COR-LAST-NME INTO #WORK-NAME
            pnd_Ap_Dob.setValue(annty_Actvty_Prap_View_Ap_Dob);                                                                                                           //Natural: ASSIGN #AP-DOB := AP-DOB
            pnd_Prt_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Ap_Dob_Pnd_Work_Yyyy, pnd_Ap_Dob_Pnd_Work_Mm, pnd_Ap_Dob_Pnd_Work_Dd));             //Natural: COMPRESS #WORK-YYYY #WORK-MM #WORK-DD INTO #PRT-DATE LEAVING NO
            //*                                                         (JC2) BEGIN
            if (condition(pnd_Cip_Customer_File_Pnd_C_Typeofbusiness.equals(pnd_Atra)))                                                                                   //Natural: IF #C-TYPEOFBUSINESS = #ATRA
            {
                getWorkFiles().write(3, false, annty_Actvty_Prap_View_Ap_Pin_Nbr, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct, pnd_Prt_Date, pnd_Atra_Pnd_Atra_A2,              //Natural: WRITE WORK FILE 3 AP-PIN-NBR AP-TIAA-CNTRCT #PRT-DATE #ATRA-A2 #WORK-NAME-39
                    pnd_Work_Name_Pnd_Work_Name_39);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*                                                         (JC2) END
                getWorkFiles().write(3, false, annty_Actvty_Prap_View_Ap_Pin_Nbr, annty_Actvty_Prap_View_Ap_Tiaa_Cntrct, pnd_Prt_Date, annty_Actvty_Prap_View_Ap_Lob,     //Natural: WRITE WORK FILE 3 AP-PIN-NBR AP-TIAA-CNTRCT #PRT-DATE AP-LOB AP-LOB-TYPE #WORK-NAME-39
                    annty_Actvty_Prap_View_Ap_Lob_Type, pnd_Work_Name_Pnd_Work_Name_39);
                //*  (JC2)
            }                                                                                                                                                             //Natural: END-IF
            PND_PND_L5430:                                                                                                                                                //Natural: GET ANNTY-ACTVTY-PRAP-VIEW #UPDATE-ISN
            vw_annty_Actvty_Prap_View.readByID(pnd_Update_Isn.getLong(), "PND_PND_L5430");
            annty_Actvty_Prap_View_Ap_Addr_Usage_Code.setValue("2");                                                                                                      //Natural: MOVE '2' TO AP-ADDR-USAGE-CODE
            //*  (JC3)
            vw_annty_Actvty_Prap_View.updateDBRow("PND_PND_L5430");                                                                                                       //Natural: UPDATE RECORD IN STATEMENT ( ##L5430. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        if (condition(pnd_How_Many.equals(getZero())))                                                                                                                    //Natural: IF #HOW-MANY EQ 0
        {
            FOR10:                                                                                                                                                        //Natural: FOR #I 1 TO 56
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(56)); pnd_I.nadd(1))
            {
                pnd_Compressed_A.getValue(pnd_I).setValue("~");                                                                                                           //Natural: MOVE '~' TO #COMPRESSED-A ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Compressed_B.setValue("~~~~~~~~~~~~~");                                                                                                                   //Natural: ASSIGN #COMPRESSED-B := '~~~~~~~~~~~~~'
            getWorkFiles().write(1, false, pnd_Compressed_A.getValue("*"));                                                                                               //Natural: WRITE WORK FILE 1 #COMPRESSED-A ( * )
            getWorkFiles().write(2, false, pnd_Compressed_B);                                                                                                             //Natural: WRITE WORK FILE 2 #COMPRESSED-B
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-FOREIGN-CITY
        //*  CHECK EACH POSITION OF EACH WORD FOR NUMERICS.  IF FOUND, DISREGARD
        //*  THE ENTIRE WORD.
    }
    //*  (JC1)
    private void sub_Find_Foreign_City() throws Exception                                                                                                                 //Natural: FIND-FOREIGN-CITY
    {
        if (BLNatReinput.isReinput()) return;

        //*  THIS ROUTINE WILL EXTRACT THE CITY NAME ONLY FROM AP-ADDRESS-TXT (*).
        //*  IT WILL DISCARD ANY ADDITIONAL INFORMATION CONTAINING NUMERICS.
        //*  THE CURRENT PROCEDURE FOR ENTERING FOREIGN ADDRESSES IN ACIS IS:
        //*  1. ENTER CITY IN THE SECOND OR THIRD OF THE THREE LINES MARKED AS
        //*     RESIDENCE ADDR.  FOR CIP, THIS CORRESPONDS TO AP-ADDRESS-TXT (2-3).
        //*  2. ENTER THE FULL COUNTRY NAME IN THE MAP FIELD MARKED CITY.
        //*     FOR CIP, THIS CORRESPONDS TO AP-ADDRESS-TXT (4). (COUNTRY NAME IS
        //*     NOT EXTRACTED BY THIS PROGRAM.)
        //*  THIS ROUTINE NOW APPLIES ONLY TO LEGACY.            /* (JC3)
        if (condition(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(3).notEquals(" ")))                                                                                  //Natural: IF AP-ADDRESS-TXT ( 3 ) NE ' '
        {
            pnd_Work_Area.setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(3));                                                                                    //Natural: MOVE AP-ADDRESS-TXT ( 3 ) TO #WORK-AREA
            annty_Actvty_Prap_View_Ap_Address_Txt.getValue(3).reset();                                                                                                    //Natural: RESET AP-ADDRESS-TXT ( 3 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Work_Area.setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(2));                                                                                    //Natural: MOVE AP-ADDRESS-TXT ( 2 ) TO #WORK-AREA
            annty_Actvty_Prap_View_Ap_Address_Txt.getValue(2).reset();                                                                                                    //Natural: RESET AP-ADDRESS-TXT ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  SEPARATE CITY FIELD INTO WORDS AS DELIMITED BY BLANKS.
        pnd_Save_Area.reset();                                                                                                                                            //Natural: RESET #SAVE-AREA
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  WRITE AP-TIAA-CNTRCT 'checking:' '=' #WORK-AREA
            pnd_Work_Area.separate(pnd_Remainder, null, SeparateOption.WithAnyDelimiters, " ", pnd_Cip_Customer_File_Pnd_C_City);                                         //Natural: SEPARATE #WORK-AREA INTO #C-CITY REMAINDER #REMAINDER WITH DELIMITER ' ' GIVING NUMBER IN #NUMBER
            //*  DONE CHECKING FOR BLANKS
            if (condition(pnd_Number.equals(getZero())))                                                                                                                  //Natural: IF #NUMBER EQ 0
            {
                pnd_Save_Area.setValue(DbsUtil.compress(pnd_Save_Area, pnd_Cip_Customer_File_Pnd_C_City));                                                                //Natural: COMPRESS #SAVE-AREA #C-CITY INTO #SAVE-AREA
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            FOR11:                                                                                                                                                        //Natural: FOR #I EQ 1 TO 35
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(35)); pnd_I.nadd(1))
            {
                if (condition(pnd_Cip_Customer_File_Pnd_C_Cty.getValue(pnd_I).equals(" ") || DbsUtil.maskMatches(pnd_Cip_Customer_File_Pnd_C_Cty.getValue(pnd_I),         //Natural: IF #C-CTY ( #I ) EQ ' ' OR #C-CTY ( #I ) EQ MASK ( 9 )
                    "9")))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(DbsUtil.maskMatches(pnd_Cip_Customer_File_Pnd_C_Cty.getValue(pnd_I),"9")))                                                                      //Natural: IF #C-CTY ( #I ) EQ MASK ( 9 )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Save_Area.setValue(DbsUtil.compress(pnd_Save_Area, pnd_Cip_Customer_File_Pnd_C_City));                                                                //Natural: COMPRESS #SAVE-AREA #C-CITY INTO #SAVE-AREA
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Area.setValue(pnd_Remainder);                                                                                                                        //Natural: ASSIGN #WORK-AREA := #REMAINDER
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Cip_Customer_File_Pnd_C_City.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Save_Area, "~"));                                                   //Natural: COMPRESS #SAVE-AREA '~' INTO #C-CITY LEAVING NO SPACE
        //*  WRITE AP-TIAA-CNTRCT 'Done:       ' '=' #C-CITY
        //*  FIND-FOREIGN-CITY
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
    }
}
