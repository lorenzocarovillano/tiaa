/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:52 PM
**        * FROM NATURAL PROGRAM : Appb2080
************************************************************
**        * FILE NAME            : Appb2080.java
**        * CLASS NAME           : Appb2080
**        * INSTANCE NAME        : Appb2080
************************************************************
***********************************************************************
* APPB2080 - DUPLICATE G-KEY REPORT/EXTRACT                           *
* READS THE PRAP FILE AND IDENTIFIES ALL SSN'S WITH DUPLICATE G-KEYS  *
* WRITES REPORT AND WRITES WORK FILE OF THE SSN NUMBERS               *
***********************************************************************
*
***********************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                    *
**--------------------------------------------------------------------*
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.**
***********************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2080 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_prap_View;
    private DbsField prap_View_Ap_Soc_Sec;
    private DbsField prap_View_Ap_Pin_Nbr;
    private DbsField prap_View_Ap_Tiaa_Cntrct;
    private DbsField prap_View_Ap_Cref_Cert;
    private DbsField prap_View_Ap_Status;
    private DbsField prap_View_Ap_Dt_Ent_Sys;
    private DbsField prap_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField prap_View_Ap_Lob;
    private DbsField prap_View_Ap_Lob_Type;
    private DbsField prap_View_Ap_Coll_Code;
    private DbsField prap_View_Ap_G_Key;
    private DbsField prap_View_Ap_G_Ind;

    private DbsGroup pnd_Contract_Table;
    private DbsField pnd_Contract_Table_Pnd_Isn;
    private DbsField pnd_Contract_Table_Pnd_Pin;
    private DbsField pnd_Contract_Table_Pnd_Ppg_Code;
    private DbsField pnd_Contract_Table_Pnd_Contract_Type;
    private DbsField pnd_Contract_Table_Pnd_Tiaa_Contract;
    private DbsField pnd_Contract_Table_Pnd_Cref_Contract;
    private DbsField pnd_Contract_Table_Pnd_Status;
    private DbsField pnd_Contract_Table_Pnd_Date;
    private DbsField pnd_Contract_Table_Pnd_Time;
    private DbsField pnd_Contract_Table_Pnd_G_Key;
    private DbsField pnd_Contract_Table_Pnd_G_Ind;
    private DbsField pnd_Contract_Table_Pnd_Dup_Ind;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_Tbl_Cnt;
    private DbsField pnd_Tbl_Max;
    private DbsField pnd_Hold_Ssn;
    private DbsField pnd_Hold_Ppg;
    private DbsField pnd_Dup_Ppg;
    private DbsField pnd_Dup_Count;
    private DbsField pnd_Zero_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_prap_View = new DataAccessProgramView(new NameInfo("vw_prap_View", "PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        prap_View_Ap_Soc_Sec = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, 
            "AP_SOC_SEC");
        prap_View_Ap_Pin_Nbr = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "AP_PIN_NBR");
        prap_View_Ap_Tiaa_Cntrct = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_View_Ap_Cref_Cert = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        prap_View_Ap_Status = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_STATUS");
        prap_View_Ap_Dt_Ent_Sys = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_ENT_SYS");
        prap_View_Ap_Rcrd_Updt_Tm_Stamp = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Rcrd_Updt_Tm_Stamp", "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        prap_View_Ap_Lob = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB");
        prap_View_Ap_Lob.setDdmHeader("LOB");
        prap_View_Ap_Lob_Type = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB_TYPE");
        prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        prap_View_Ap_Coll_Code = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_View_Ap_G_Key = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_G_KEY");
        prap_View_Ap_G_Ind = vw_prap_View.getRecord().newFieldInGroup("prap_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_G_IND");
        registerRecord(vw_prap_View);

        pnd_Contract_Table = localVariables.newGroupArrayInRecord("pnd_Contract_Table", "#CONTRACT-TABLE", new DbsArrayController(1, 50));
        pnd_Contract_Table_Pnd_Isn = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Isn", "#ISN", FieldType.NUMERIC, 9);
        pnd_Contract_Table_Pnd_Pin = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Contract_Table_Pnd_Ppg_Code = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Ppg_Code", "#PPG-CODE", FieldType.STRING, 4);
        pnd_Contract_Table_Pnd_Contract_Type = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Contract_Table_Pnd_Tiaa_Contract = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Tiaa_Contract", "#TIAA-CONTRACT", FieldType.STRING, 
            8);
        pnd_Contract_Table_Pnd_Cref_Contract = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Cref_Contract", "#CREF-CONTRACT", FieldType.STRING, 
            8);
        pnd_Contract_Table_Pnd_Status = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Status", "#STATUS", FieldType.STRING, 5);
        pnd_Contract_Table_Pnd_Date = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Date", "#DATE", FieldType.NUMERIC, 8);
        pnd_Contract_Table_Pnd_Time = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Time", "#TIME", FieldType.STRING, 8);
        pnd_Contract_Table_Pnd_G_Key = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_G_Key", "#G-KEY", FieldType.NUMERIC, 4);
        pnd_Contract_Table_Pnd_G_Ind = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_G_Ind", "#G-IND", FieldType.NUMERIC, 1);
        pnd_Contract_Table_Pnd_Dup_Ind = pnd_Contract_Table.newFieldInGroup("pnd_Contract_Table_Pnd_Dup_Ind", "#DUP-IND", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 3);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 3);
        pnd_Tbl_Cnt = localVariables.newFieldInRecord("pnd_Tbl_Cnt", "#TBL-CNT", FieldType.NUMERIC, 3);
        pnd_Tbl_Max = localVariables.newFieldInRecord("pnd_Tbl_Max", "#TBL-MAX", FieldType.NUMERIC, 3);
        pnd_Hold_Ssn = localVariables.newFieldInRecord("pnd_Hold_Ssn", "#HOLD-SSN", FieldType.NUMERIC, 9);
        pnd_Hold_Ppg = localVariables.newFieldInRecord("pnd_Hold_Ppg", "#HOLD-PPG", FieldType.STRING, 4);
        pnd_Dup_Ppg = localVariables.newFieldInRecord("pnd_Dup_Ppg", "#DUP-PPG", FieldType.BOOLEAN, 1);
        pnd_Dup_Count = localVariables.newFieldInRecord("pnd_Dup_Count", "#DUP-COUNT", FieldType.NUMERIC, 9);
        pnd_Zero_Count = localVariables.newFieldInRecord("pnd_Zero_Count", "#ZERO-COUNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap_View.reset();

        localVariables.reset();
        pnd_Tbl_Max.setInitialValue(50);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2080() throws Exception
    {
        super("Appb2080");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        //* PINE                                                                                                                                                          //Natural: FORMAT ( 1 ) LS = 80 PS = 65
        //* PINE
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 15X 'CONTRACTS WITH DUPLICATE G-KEYS' // '              PPG  CNT    TIAA      CREF' / '     PIN      CODE TYP  CONTRACT  CONTRACT STATUS' 4X 'DATE    TIME       G-KEY' /
        //*   // '            PPG  CNT    TIAA      CREF'
        //*    / '     PIN    CODE TYP  CONTRACT  CONTRACT STATUS'
        vw_prap_View.startDatabaseRead                                                                                                                                    //Natural: READ PRAP-VIEW BY AP-PRAP-SOCSEC-KEY
        (
        "READ01",
        new Oc[] { new Oc("AP_PRAP_SOCSEC_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_prap_View.readNextRow("READ01")))
        {
            CheckAtStartofData72();

            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(prap_View_Ap_Soc_Sec.equals(getZero())))                                                                                                        //Natural: REJECT IF AP-SOC-SEC = 0
            {
                continue;
            }
            if (condition(prap_View_Ap_Soc_Sec.notEquals(pnd_Hold_Ssn) || prap_View_Ap_Coll_Code.notEquals(pnd_Hold_Ppg)))                                                //Natural: IF AP-SOC-SEC NE #HOLD-SSN OR AP-COLL-CODE NE #HOLD-PPG
            {
                if (condition(pnd_Dup_Ppg.getBoolean()))                                                                                                                  //Natural: IF #DUP-PPG
                {
                                                                                                                                                                          //Natural: PERFORM DUP-PROCESSING
                    sub_Dup_Processing();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(prap_View_Ap_Soc_Sec.notEquals(pnd_Hold_Ssn)))                                                                                          //Natural: IF AP-SOC-SEC NE #HOLD-SSN
                    {
                        pnd_Dup_Count.nadd(1);                                                                                                                            //Natural: ADD 1 TO #DUP-COUNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Hold_Ssn.setValue(prap_View_Ap_Soc_Sec);                                                                                                              //Natural: MOVE AP-SOC-SEC TO #HOLD-SSN
                pnd_Hold_Ppg.setValue(prap_View_Ap_Coll_Code);                                                                                                            //Natural: MOVE AP-COLL-CODE TO #HOLD-PPG
                pnd_Dup_Ppg.reset();                                                                                                                                      //Natural: RESET #DUP-PPG #CONTRACT-TABLE ( * ) #X
                pnd_Contract_Table.getValue("*").reset();
                pnd_X.reset();
            }                                                                                                                                                             //Natural: END-IF
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
            if (condition(pnd_X.greater(pnd_Tbl_Max)))                                                                                                                    //Natural: IF #X > #TBL-MAX
            {
                getReports().write(1, NEWLINE,"TABLE SIZE OF",pnd_Tbl_Max,"EXCEEDED",NEWLINE,"INCREASE TABLE SIZE",NEWLINE,"JOB ABENDING WITH RC 32");                    //Natural: WRITE ( 1 ) / 'TABLE SIZE OF' #TBL-MAX 'EXCEEDED' / 'INCREASE TABLE SIZE' / 'JOB ABENDING WITH RC 32'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(32);  if (true) return;                                                                                                                 //Natural: TERMINATE 32
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Table_Pnd_Isn.getValue(pnd_X).setValue(vw_prap_View.getAstISN("Read01"));                                                                        //Natural: MOVE *ISN TO #ISN ( #X )
            pnd_Contract_Table_Pnd_Pin.getValue(pnd_X).setValue(prap_View_Ap_Pin_Nbr);                                                                                    //Natural: MOVE AP-PIN-NBR TO #PIN ( #X )
            pnd_Contract_Table_Pnd_Ppg_Code.getValue(pnd_X).setValue(prap_View_Ap_Coll_Code);                                                                             //Natural: MOVE AP-COLL-CODE TO #PPG-CODE ( #X )
            pnd_Contract_Table_Pnd_Date.getValue(pnd_X).setValue(prap_View_Ap_Dt_Ent_Sys);                                                                                //Natural: MOVE AP-DT-ENT-SYS TO #DATE ( #X )
            pnd_Contract_Table_Pnd_Time.getValue(pnd_X).setValue(prap_View_Ap_Rcrd_Updt_Tm_Stamp);                                                                        //Natural: MOVE AP-RCRD-UPDT-TM-STAMP TO #TIME ( #X )
            pnd_Contract_Table_Pnd_G_Key.getValue(pnd_X).setValue(prap_View_Ap_G_Key);                                                                                    //Natural: MOVE AP-G-KEY TO #G-KEY ( #X )
            pnd_Contract_Table_Pnd_G_Ind.getValue(pnd_X).setValue(prap_View_Ap_G_Ind);                                                                                    //Natural: MOVE AP-G-IND TO #G-IND ( #X )
            pnd_Contract_Table_Pnd_Tiaa_Contract.getValue(pnd_X).setValue(prap_View_Ap_Tiaa_Cntrct, MoveOption.LeftJustified);                                            //Natural: MOVE LEFT AP-TIAA-CNTRCT TO #TIAA-CONTRACT ( #X )
            pnd_Contract_Table_Pnd_Cref_Contract.getValue(pnd_X).setValue(prap_View_Ap_Cref_Cert, MoveOption.LeftJustified);                                              //Natural: MOVE LEFT AP-CREF-CERT TO #CREF-CONTRACT ( #X )
            pnd_Contract_Table_Pnd_Contract_Type.getValue(pnd_X).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, prap_View_Ap_Lob, prap_View_Ap_Lob_Type));      //Natural: COMPRESS AP-LOB AP-LOB-TYPE INTO #CONTRACT-TYPE ( #X ) LEAVING NO
            short decideConditionsMet109 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE AP-STATUS;//Natural: VALUE 'A'
            if (condition((prap_View_Ap_Status.equals("A"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("DEL");                                                                                            //Natural: ASSIGN #STATUS ( #X ) = 'DEL'
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((prap_View_Ap_Status.equals("B"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("ASSGN");                                                                                          //Natural: ASSIGN #STATUS ( #X ) = 'ASSGN'
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((prap_View_Ap_Status.equals("C"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("M & R");                                                                                          //Natural: ASSIGN #STATUS ( #X ) = 'M & R'
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((prap_View_Ap_Status.equals("D"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("REL");                                                                                            //Natural: ASSIGN #STATUS ( #X ) = 'REL'
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((prap_View_Ap_Status.equals("E"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("UNASG");                                                                                          //Natural: ASSIGN #STATUS ( #X ) = 'UNASG'
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((prap_View_Ap_Status.equals("F"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("SUSPN");                                                                                          //Natural: ASSIGN #STATUS ( #X ) = 'SUSPN'
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((prap_View_Ap_Status.equals("G"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("OVD 1");                                                                                          //Natural: ASSIGN #STATUS ( #X ) = 'OVD 1'
            }                                                                                                                                                             //Natural: VALUE 'H'
            else if (condition((prap_View_Ap_Status.equals("H"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("OVD 2");                                                                                          //Natural: ASSIGN #STATUS ( #X ) = 'OVD 2'
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((prap_View_Ap_Status.equals("I"))))
            {
                decideConditionsMet109++;
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue("OVD 3");                                                                                          //Natural: ASSIGN #STATUS ( #X ) = 'OVD 3'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Contract_Table_Pnd_Status.getValue(pnd_X).setValue(prap_View_Ap_Status);                                                                              //Natural: ASSIGN #STATUS ( #X ) = AP-STATUS
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_X.greater(1)))                                                                                                                              //Natural: IF #X > 1
            {
                pnd_Z.compute(new ComputeParameters(false, pnd_Z), pnd_X.subtract(1));                                                                                    //Natural: COMPUTE #Z = #X - 1
                FOR01:                                                                                                                                                    //Natural: FOR #Y = 1 TO #Z
                for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_Z)); pnd_Y.nadd(1))
                {
                    if (condition(pnd_Contract_Table_Pnd_G_Key.getValue(pnd_X).equals(pnd_Contract_Table_Pnd_G_Key.getValue(pnd_Y)) && pnd_Contract_Table_Pnd_G_Ind.getValue(pnd_X).equals(pnd_Contract_Table_Pnd_G_Ind.getValue(pnd_Y)))) //Natural: IF #G-KEY ( #X ) = #G-KEY ( #Y ) AND #G-IND ( #X ) = #G-IND ( #Y )
                    {
                        pnd_Contract_Table_Pnd_Dup_Ind.getValue(pnd_X).setValue("Y");                                                                                     //Natural: MOVE 'Y' TO #DUP-IND ( #X ) #DUP-IND ( #Y )
                        pnd_Contract_Table_Pnd_Dup_Ind.getValue(pnd_Y).setValue("Y");
                        pnd_Dup_Ppg.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #DUP-PPG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, "SSNGS WITH DUPLICATE G-KEYS COUNT",pnd_Dup_Count);                                                                                         //Natural: WRITE ( 1 ) 'SSN''S WITH DUPLICATE G-KEYS COUNT' #DUP-COUNT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUP-PROCESSING
        //* ***********************************************************************
    }
    private void sub_Dup_Processing() throws Exception                                                                                                                    //Natural: DUP-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Cnt.setValue(pnd_X);                                                                                                                                      //Natural: ASSIGN #TBL-CNT := #X
        FOR02:                                                                                                                                                            //Natural: FOR #X = 1 TO #TBL-CNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Tbl_Cnt)); pnd_X.nadd(1))
        {
            if (condition(pnd_Contract_Table_Pnd_Dup_Ind.getValue(pnd_X).equals("Y")))                                                                                    //Natural: IF #DUP-IND ( #X ) = 'Y'
            {
                getReports().write(1, pnd_Contract_Table_Pnd_Pin.getValue(pnd_X),new ColumnSpacing(2),pnd_Contract_Table_Pnd_Ppg_Code.getValue(pnd_X),new                 //Natural: WRITE ( 1 ) #PIN ( #X ) 2X #PPG-CODE ( #X ) 2X #CONTRACT-TYPE ( #X ) 2X #TIAA-CONTRACT ( #X ) 2X #CREF-CONTRACT ( #X ) #STATUS ( #X ) #DATE ( #X ) #TIME ( #X ) #G-KEY ( #X ) #G-IND ( #X )
                    ColumnSpacing(2),pnd_Contract_Table_Pnd_Contract_Type.getValue(pnd_X),new ColumnSpacing(2),pnd_Contract_Table_Pnd_Tiaa_Contract.getValue(pnd_X),new 
                    ColumnSpacing(2),pnd_Contract_Table_Pnd_Cref_Contract.getValue(pnd_X),pnd_Contract_Table_Pnd_Status.getValue(pnd_X),pnd_Contract_Table_Pnd_Date.getValue(pnd_X),
                    pnd_Contract_Table_Pnd_Time.getValue(pnd_X),pnd_Contract_Table_Pnd_G_Key.getValue(pnd_X),pnd_Contract_Table_Pnd_G_Ind.getValue(pnd_X));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Contract_Table_Pnd_Isn.getValue(pnd_X), pnd_Contract_Table_Pnd_Pin.getValue(pnd_X), pnd_Contract_Table_Pnd_Ppg_Code.getValue(pnd_X),  //Natural: WRITE WORK FILE 01 #ISN ( #X ) #PIN ( #X ) #PPG-CODE ( #X ) #DATE ( #X ) #TIME ( #X )
                pnd_Contract_Table_Pnd_Date.getValue(pnd_X), pnd_Contract_Table_Pnd_Time.getValue(pnd_X));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=65");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(15),"CONTRACTS WITH DUPLICATE G-KEYS",NEWLINE,NEWLINE,"              PPG  CNT    TIAA      CREF",NEWLINE,"     PIN      CODE TYP  CONTRACT  CONTRACT STATUS",new 
            ColumnSpacing(4),"DATE    TIME       G-KEY",NEWLINE);
    }
    private void CheckAtStartofData72() throws Exception
    {
        if (condition(vw_prap_View.getAtStartOfData()))
        {
            pnd_Hold_Ssn.setValue(prap_View_Ap_Soc_Sec);                                                                                                                  //Natural: MOVE AP-SOC-SEC TO #HOLD-SSN
            pnd_Hold_Ppg.setValue(prap_View_Ap_Coll_Code);                                                                                                                //Natural: MOVE AP-COLL-CODE TO #HOLD-PPG
        }                                                                                                                                                                 //Natural: END-START
    }
}
