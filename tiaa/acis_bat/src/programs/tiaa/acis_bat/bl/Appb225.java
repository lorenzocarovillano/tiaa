/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:05 PM
**        * FROM NATURAL PROGRAM : Appb225
************************************************************
**        * FILE NAME            : Appb225.java
**        * CLASS NAME           : Appb225
**        * INSTANCE NAME        : Appb225
************************************************************
************************************************************************
* PROGRAM  : APPB225                                                   *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : MARCH 9, 2000                                             *
* FUNCTION : READS THE WORK FILE THAT FEEDS INTO P1040NID AND CREATES  *
*   A DETAIL REPORT AND CONTROL TOTALS                                 *
*                                                                      *
* MODIFIED : ADDED ADDITIONAL LOB/TYPE 'D,6','D,9' FOR NAME/ADDRESS    *
*          : CONTROL REPORT.                                (SINGLETON)*
*          : 457(B) PROJECT CHANGES (11/08/01 K GATES) SEE 457(B) KG   *
*          : FIXED DOB DISPLAY      (09/12/02 L DEDIOS) SEE DOB LD     *
*          : RESTOW FOR SGRD PLAN AND SUBPLAN - APPL220 (03/01/04 KG)  *
*          : ADD NEW TNT PRODUCTS/REL4. (RW1)                          *
*          : ADD NEW  ICAP PRODUCTS/REL4. (RW2) - 03/16/05             *
*          : ADD NEW  ICAP PRODUCTS/REL6. (DM6) - 09/09/05 (SEP)       *
*          : ADD NEW GA PRODUCT FOR DCA. (DCA KG) - 10/03/06           *
*          : ADD NEW RHSP PRODUCT       (RHSP NBC)-  8/28/08           *
* 03/10/10 : C. AVE - INCLUDED IRA INDEXED PRODUCRS (TIGR)             *
* 06/20/17 : BABRE    PIN EXPANSION CHANGES. CHG425939 STOW ONLY
* -------------------------------------------------------------------- *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb225 extends BLNatBase
{
    // Data Areas
    private LdaAppl220 ldaAppl220;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Online_Batch;
    private DbsField pnd_Lob;
    private DbsField pnd_Total_Batch;
    private DbsField pnd_Total_Online;
    private DbsField pnd_Total_Unknown;
    private DbsField pnd_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl220 = new LdaAppl220();
        registerRecord(ldaAppl220);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Online_Batch = localVariables.newFieldInRecord("pnd_Online_Batch", "#ONLINE-BATCH", FieldType.STRING, 7);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 4);
        pnd_Total_Batch = localVariables.newFieldInRecord("pnd_Total_Batch", "#TOTAL-BATCH", FieldType.NUMERIC, 6);
        pnd_Total_Online = localVariables.newFieldInRecord("pnd_Total_Online", "#TOTAL-ONLINE", FieldType.NUMERIC, 6);
        pnd_Total_Unknown = localVariables.newFieldInRecord("pnd_Total_Unknown", "#TOTAL-UNKNOWN", FieldType.NUMERIC, 6);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl220.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Appb225() throws Exception
    {
        super("Appb225");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB225", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 58
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 EX-DATA-1 EX-DATA-2 EX-DATA-3 EX-DATA-4 EX-DATA-5 EX-DATA-6 EX-DATA-7 EX-DATA-8 EX-DATA-9 EX-DATA-10 EX-DATA-11 EX-DATA-12 EX-DATA-13 EX-DATA-14 EX-DATA-15 EX-DATA-16
        while (condition(getWorkFiles().read(1, ldaAppl220.getPrap_Extract_File_Ex_Data_1(), ldaAppl220.getPrap_Extract_File_Ex_Data_2(), ldaAppl220.getPrap_Extract_File_Ex_Data_3(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_4(), ldaAppl220.getPrap_Extract_File_Ex_Data_5(), ldaAppl220.getPrap_Extract_File_Ex_Data_6(), ldaAppl220.getPrap_Extract_File_Ex_Data_7(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_8(), ldaAppl220.getPrap_Extract_File_Ex_Data_9(), ldaAppl220.getPrap_Extract_File_Ex_Data_10(), ldaAppl220.getPrap_Extract_File_Ex_Data_11(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_12(), ldaAppl220.getPrap_Extract_File_Ex_Data_13(), ldaAppl220.getPrap_Extract_File_Ex_Data_14(), ldaAppl220.getPrap_Extract_File_Ex_Data_15(), 
            ldaAppl220.getPrap_Extract_File_Ex_Data_16())))
        {
            getSort().writeSortInData(ldaAppl220.getPrap_Extract_File_Ex_Addr_Sync_Ind(), ldaAppl220.getPrap_Extract_File_Ex_Name(), ldaAppl220.getPrap_Extract_File_Ex_Soc_Sec(),  //Natural: END-ALL
                ldaAppl220.getPrap_Extract_File_Ex_Date_Birth(), ldaAppl220.getPrap_Extract_File_Ex_T_Contract(), ldaAppl220.getPrap_Extract_File_Ex_C_Contract(), 
                ldaAppl220.getPrap_Extract_File_Ex_Cor_Unique_Id_Nbr(), ldaAppl220.getPrap_Extract_File_Ex_Lob(), ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(1), 
                ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(2), ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(3), ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(4), 
                ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(5), ldaAppl220.getPrap_Extract_File_Ex_Lob_Type(), ldaAppl220.getPrap_Extract_File_Ex_Tiaa_Doi(), 
                ldaAppl220.getPrap_Extract_File_Ex_Cref_Doi());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(ldaAppl220.getPrap_Extract_File_Ex_Addr_Sync_Ind(), ldaAppl220.getPrap_Extract_File_Ex_Name());                                                //Natural: SORT EX-ADDR-SYNC-IND EX-NAME USING EX-SOC-SEC EX-DATE-BIRTH EX-T-CONTRACT EX-C-CONTRACT EX-COR-UNIQUE-ID-NBR EX-LOB EX-ADDRESS-FIELD ( 1:5 ) EX-LOB-TYPE EX-TIAA-DOI EX-CREF-DOI
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaAppl220.getPrap_Extract_File_Ex_Addr_Sync_Ind(), ldaAppl220.getPrap_Extract_File_Ex_Name(), ldaAppl220.getPrap_Extract_File_Ex_Soc_Sec(), 
            ldaAppl220.getPrap_Extract_File_Ex_Date_Birth(), ldaAppl220.getPrap_Extract_File_Ex_T_Contract(), ldaAppl220.getPrap_Extract_File_Ex_C_Contract(), 
            ldaAppl220.getPrap_Extract_File_Ex_Cor_Unique_Id_Nbr(), ldaAppl220.getPrap_Extract_File_Ex_Lob(), ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(1), 
            ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(2), ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(3), ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(4), 
            ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(5), ldaAppl220.getPrap_Extract_File_Ex_Lob_Type(), ldaAppl220.getPrap_Extract_File_Ex_Tiaa_Doi(), 
            ldaAppl220.getPrap_Extract_File_Ex_Cref_Doi())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            getReports().write(0, "EX-COR-UNIQUE-ID-NBR  :",ldaAppl220.getPrap_Extract_File_Ex_Cor_Unique_Id_Nbr());                                                      //Natural: WRITE 'EX-COR-UNIQUE-ID-NBR  :' EX-COR-UNIQUE-ID-NBR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "EX-ADDR-SYNC-IND  :",ldaAppl220.getPrap_Extract_File_Ex_Addr_Sync_Ind());                                                              //Natural: WRITE 'EX-ADDR-SYNC-IND  :' EX-ADDR-SYNC-IND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet251 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF EX-ADDR-SYNC-IND;//Natural: VALUE 'B'
            if (condition((ldaAppl220.getPrap_Extract_File_Ex_Addr_Sync_Ind().equals("B"))))
            {
                decideConditionsMet251++;
                pnd_Online_Batch.setValue("Batch");                                                                                                                       //Natural: MOVE 'Batch' TO #ONLINE-BATCH
                pnd_Total_Batch.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-BATCH
            }                                                                                                                                                             //Natural: VALUE 'O'
            else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Addr_Sync_Ind().equals("O"))))
            {
                decideConditionsMet251++;
                pnd_Online_Batch.setValue("Online");                                                                                                                      //Natural: MOVE 'Online' TO #ONLINE-BATCH
                pnd_Total_Online.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #TOTAL-ONLINE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Online_Batch.setValue("Unknown");                                                                                                                     //Natural: MOVE 'Unknown' TO #ONLINE-BATCH
                pnd_Total_Unknown.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-UNKNOWN
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  RW1
            //*  SINGLETON
            //*  SINGLETON
            //*  RW2
            //*  RHSP NBC
            short decideConditionsMet268 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF EX-LOB;//Natural: VALUE 'D'
            if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("D"))))
            {
                decideConditionsMet268++;
                short decideConditionsMet270 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF EX-LOB-TYPE;//Natural: VALUE '2'
                if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("2"))))
                {
                    decideConditionsMet270++;
                    pnd_Lob.setValue("RA");                                                                                                                               //Natural: MOVE 'RA' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '5'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("5"))))
                {
                    decideConditionsMet270++;
                    pnd_Lob.setValue("RS");                                                                                                                               //Natural: MOVE 'RS' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '6'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("6"))))
                {
                    decideConditionsMet270++;
                    pnd_Lob.setValue("RL");                                                                                                                               //Natural: MOVE 'RL' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '7'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("7"))))
                {
                    decideConditionsMet270++;
                    pnd_Lob.setValue("GRA");                                                                                                                              //Natural: MOVE 'GRA' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '9'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("9"))))
                {
                    decideConditionsMet270++;
                    pnd_Lob.setValue("GRA-II");                                                                                                                           //Natural: MOVE 'GRA-II' TO #LOB
                }                                                                                                                                                         //Natural: VALUE 'A'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("A"))))
                {
                    decideConditionsMet270++;
                    pnd_Lob.setValue("RC");                                                                                                                               //Natural: MOVE 'RC' TO #LOB
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("B"))))
                {
                    decideConditionsMet270++;
                    pnd_Lob.setValue("RHSP");                                                                                                                             //Natural: MOVE 'RHSP' TO #LOB
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Lob.reset();                                                                                                                                      //Natural: RESET #LOB
                    //*  DM6
                    //*  TIGR
                    //*  TIGR
                    //*  TIGR
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("I"))))
            {
                decideConditionsMet268++;
                short decideConditionsMet293 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF EX-LOB-TYPE;//Natural: VALUE '3'
                if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("3"))))
                {
                    decideConditionsMet293++;
                    pnd_Lob.setValue("IRAR");                                                                                                                             //Natural: MOVE 'IRAR' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '4'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("4"))))
                {
                    decideConditionsMet293++;
                    pnd_Lob.setValue("IRAC");                                                                                                                             //Natural: MOVE 'IRAC' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '5'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("5"))))
                {
                    decideConditionsMet293++;
                    pnd_Lob.setValue("KEOG");                                                                                                                             //Natural: MOVE 'KEOG' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '6'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("6"))))
                {
                    decideConditionsMet293++;
                    pnd_Lob.setValue("IRAS");                                                                                                                             //Natural: MOVE 'IRAS' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '7'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("7"))))
                {
                    decideConditionsMet293++;
                    pnd_Lob.setValue("IRIR");                                                                                                                             //Natural: MOVE 'IRIR' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '8'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("8"))))
                {
                    decideConditionsMet293++;
                    pnd_Lob.setValue("IRIC");                                                                                                                             //Natural: MOVE 'IRIC' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '9'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("9"))))
                {
                    decideConditionsMet293++;
                    pnd_Lob.setValue("IRIS");                                                                                                                             //Natural: MOVE 'IRIS' TO #LOB
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Lob.reset();                                                                                                                                      //Natural: RESET #LOB
                    //*  457(B) KG
                    //*  RW2
                    //*  DCA KG
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob().equals("S"))))
            {
                decideConditionsMet268++;
                short decideConditionsMet315 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF EX-LOB-TYPE;//Natural: VALUE '2'
                if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("2"))))
                {
                    decideConditionsMet315++;
                    pnd_Lob.setValue("SRA");                                                                                                                              //Natural: MOVE 'SRA' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '3'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("3"))))
                {
                    decideConditionsMet315++;
                    pnd_Lob.setValue("GSRA");                                                                                                                             //Natural: MOVE 'GSRA' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '4'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("4"))))
                {
                    decideConditionsMet315++;
                    pnd_Lob.setValue("GA");                                                                                                                               //Natural: MOVE 'GA' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '5'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("5"))))
                {
                    decideConditionsMet315++;
                    pnd_Lob.setValue("RSP");                                                                                                                              //Natural: MOVE 'RSP' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '6'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("6"))))
                {
                    decideConditionsMet315++;
                    pnd_Lob.setValue("RSP2");                                                                                                                             //Natural: MOVE 'RSP2' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '7'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("7"))))
                {
                    decideConditionsMet315++;
                    pnd_Lob.setValue("RCP");                                                                                                                              //Natural: MOVE 'RCP' TO #LOB
                }                                                                                                                                                         //Natural: VALUE '9'
                else if (condition((ldaAppl220.getPrap_Extract_File_Ex_Lob_Type().equals("9"))))
                {
                    decideConditionsMet315++;
                    pnd_Lob.setValue("TGA");                                                                                                                              //Natural: MOVE 'TGA' TO #LOB
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Lob.reset();                                                                                                                                      //Natural: RESET #LOB
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Lob.reset();                                                                                                                                          //Natural: RESET #LOB
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  **********************************************************************
            //*  1.IF TIAA ISSUE DATE IS PRESENT-USE TIAA CONTRACT# AND TIAA ISSUE DATE
            //*  2.IF CREF ISSUE DATE IS PRESENT-USE CREF CONTRACT# AND CREF ISSUE DATE
            //*  3.IF BOTH ISSUE DATES ARE PRESENT - USE TIAA CONTRACT# AND ISSUE DATE
            //*  **********************************************************************
            //*  REL5.
            //*  REL5.
            //*                                                                                                                                                           //Natural: DECIDE FOR FIRST CONDITION
            //*  TIAA ISSUE DATE NOT PRESENT
            //*  ---------------------------
            short decideConditionsMet343 = 0;                                                                                                                             //Natural: WHEN EX-TIAA-DOI = 0
            if (condition(ldaAppl220.getPrap_Extract_File_Ex_Tiaa_Doi().equals(getZero())))
            {
                decideConditionsMet343++;
                //*  REL5.
                //*  REL5.
                ldaAppl220.getPrap_Extract_File_Ex_T_Contract().reset();                                                                                                  //Natural: RESET EX-T-CONTRACT
                //*  CREF ISSUE DATE NOT PRESENT
                //*  ---------------------------
            }                                                                                                                                                             //Natural: WHEN EX-CREF-DOI = 0
            else if (condition(ldaAppl220.getPrap_Extract_File_Ex_Cref_Doi().equals(getZero())))
            {
                decideConditionsMet343++;
                //*  REL5.
                //*  REL5.
                //*  REL5.
                ldaAppl220.getPrap_Extract_File_Ex_C_Contract().reset();                                                                                                  //Natural: RESET EX-C-CONTRACT
                //*  BOTH ISSUE DATES ARE PRESENT
                //*  ----------------------------
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
                //*  REL5.
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  DOB LD
            getReports().display(1, "TIAA/Contract",                                                                                                                      //Natural: DISPLAY ( 1 ) 'TIAA/Contract' EX-T-CONTRACT ( EM = XXXXXXX-X ) 'CREF/Contract' EX-C-CONTRACT ( EM = XXXXXXX-X ) 'Name\' EX-NAME / 'Address' EX-ADDRESS-FIELD ( 1:5 ) 'Soc/Sec' EX-SOC-SEC ( EM = 999-99-9999 ) 'DOB' EX-DATE-BIRTH ( EM = 99/99/9999 ) 'Pin/Nbr' EX-COR-UNIQUE-ID-NBR ( ZP = OFF ) 'LOB' #LOB 'ONLINE/BATCH' #ONLINE-BATCH
            		ldaAppl220.getPrap_Extract_File_Ex_T_Contract(), new ReportEditMask ("XXXXXXX-X"),"CREF/Contract",
            		ldaAppl220.getPrap_Extract_File_Ex_C_Contract(), new ReportEditMask ("XXXXXXX-X"),"Name\\",
            		ldaAppl220.getPrap_Extract_File_Ex_Name(),NEWLINE,"Address",
            		ldaAppl220.getPrap_Extract_File_Ex_Address_Field().getValue(1,":",5),"Soc/Sec",
            		ldaAppl220.getPrap_Extract_File_Ex_Soc_Sec(), new ReportEditMask ("999-99-9999"),"DOB",
            		ldaAppl220.getPrap_Extract_File_Ex_Date_Birth(), new ReportEditMask ("99/99/9999"),"Pin/Nbr",
            		ldaAppl220.getPrap_Extract_File_Ex_Cor_Unique_Id_Nbr(), new ReportZeroPrint (false),"LOB",
            		pnd_Lob,"ONLINE/BATCH",
            		pnd_Online_Batch);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF EX-ADDR-SYNC-IND
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Total_Batch.add(pnd_Total_Online).add(pnd_Total_Unknown));                                         //Natural: COMPUTE #TOTAL = #TOTAL-BATCH + #TOTAL-ONLINE + #TOTAL-UNKNOWN
        getReports().write(1, ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,"Total contracts added to NAD through:                 ",NEWLINE,"                                       Batch process  ",pnd_Total_Batch,  //Natural: WRITE ( 1 ) NOHDR /// 'Total contracts added to NAD through:                 ' / '                                       Batch process  ' #TOTAL-BATCH ( EM = ZZZ,ZZ9 ) / '                                       Online process ' #TOTAL-ONLINE ( EM = ZZZ,ZZ9 ) / '                                       Unknown process' #TOTAL-UNKNOWN ( EM = ZZZ,ZZ9 ) // 'Total contracts added to NAD system:                  ' #TOTAL ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,"                                       Online process ",pnd_Total_Online, new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,"                                       Unknown process",pnd_Total_Unknown, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,"Total contracts added to NAD system:                  ",pnd_Total, new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 120T *DATU / 'PAGE' *PAGE-NUMBER ( 1 ) 120T *TIMX // 46T 'Name and Address Add - Control Report' /// '-' ( 131 ) //
        //* *======
    }                                                                                                                                                                     //Natural: ON ERROR

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "PROGRAM:   ",Global.getPROGRAM(),NEWLINE,"ERROR LINE:",Global.getERROR_LINE(),NEWLINE,"ERROR:     ",Global.getERROR_NR());                 //Natural: WRITE 'PROGRAM:   ' *PROGRAM / 'ERROR LINE:' *ERROR-LINE / 'ERROR:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaAppl220_getPrap_Extract_File_Ex_Addr_Sync_IndIsBreak = ldaAppl220.getPrap_Extract_File_Ex_Addr_Sync_Ind().isBreak(endOfData);
        if (condition(ldaAppl220_getPrap_Extract_File_Ex_Addr_Sync_IndIsBreak))
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(120),Global.getDATU(),NEWLINE,"PAGE",getReports().getPageNumberDbs(1),new 
            TabSetting(120),Global.getTIMX(),NEWLINE,NEWLINE,new TabSetting(46),"Name and Address Add - Control Report",NEWLINE,NEWLINE,NEWLINE,"-",new 
            RepeatItem(131),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "TIAA/Contract",
        		ldaAppl220.getPrap_Extract_File_Ex_T_Contract(), new ReportEditMask ("XXXXXXX-X"),"CREF/Contract",
        		ldaAppl220.getPrap_Extract_File_Ex_C_Contract(), new ReportEditMask ("XXXXXXX-X"),"Name\\",
        		ldaAppl220.getPrap_Extract_File_Ex_Name(),NEWLINE,"Address",
        		ldaAppl220.getPrap_Extract_File_Ex_Address_Field(),"Soc/Sec",
        		ldaAppl220.getPrap_Extract_File_Ex_Soc_Sec(), new ReportEditMask ("999-99-9999"),"DOB",
        		ldaAppl220.getPrap_Extract_File_Ex_Date_Birth(), new ReportEditMask ("99/99/9999"),"Pin/Nbr",
        		ldaAppl220.getPrap_Extract_File_Ex_Cor_Unique_Id_Nbr(), new ReportZeroPrint (false),"LOB",
        		pnd_Lob,"ONLINE/BATCH",
        		pnd_Online_Batch);
    }
}
