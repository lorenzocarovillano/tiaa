/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:04 PM
**        * FROM NATURAL PROGRAM : Appb1155
************************************************************
**        * FILE NAME            : Appb1155.java
**        * CLASS NAME           : Appb1155
**        * INSTANCE NAME        : Appb1155
************************************************************
************************************************************************
* PROGRAM  : APPB1155 - PROGRAM WILL READ CONTRACT EXTRACT FILE        *
*          : AND EXPAND WITH AGENT INFORMATION IF APPLICABLE           *
* FUNCTION : READS CONTRACT EXTRACT FILE AND PERFORMS THE FOLLOWING:   *
*            1. ACCEPT ONLY RECORDS WITH DX-TYPE-CODE = 'L' LEGAL      *
*            2. IF SUBPLAN = 'RA' OR= 'SR' OR'= 'IR' OR= 'AR'          *
*                 CHECK IF DX-AGENT-CRD-NO IS FILLED                   *
*                   IF YES.  CALL COBOL PROGRAM PSG9065 TO CALL A      *
*                      STORED PROCEDURE TO OBTAIN AGENT INFO. GIVEN A  *
*                      CRD NO.                                         *
*                   IF NO.  ACCESS ACIS APP-TABLE-ENTRY AND OBTAIN THE *
*                      DEFAULT CRD NO. FOR THE STATE. CALL THE COBOL   *
*                      PROGRAM PSG9065 TO CALL A STORED PROCEDURE TO   *
*                      OBTAIN AGENT INFO. GIVEN THE DEFAULT CRD NO.    *
*            3.  WRITE WORKFILE 2 USING THE LAYOUT OF THE INPUT FILE + *
*                   THE ADDITIONAL AGENT INFORMATION.                  *
*                                                                      *
* UPDATED  : 03/09/11 B.ELLO    - INITIAL PROGRAM                      *
* UPDATED  : 09/22/11 C.SCHNEIDER - RESTOWED FOR JHU AND TIC CHANGES TO*
*                                   APPL160.                           *
* (MOBKEY) :10/16/11 C.SCHNEIDER - SRK CHANGES FOR MOBIUS KEY         *
* (TNGSUB) :07/18/13 L SHU     -  CHANGES FOR IRA SUBSTITUTION        *
*                                 POPULATE SUBSTITUTION CONTRACT IND  *
*                              -  ACCEPT WELCOME PACKAGE DATA TO HAVE *
*                                 SAME RECORD LAYOUT AS LEGAL         *
* (MTSIN)   09/13/13 L SHU     -  CHANGES FOR MT SINAI                *
*                                 POPULATING NON PROPRIETARY PKG IND  *
* (ACCRC)   09/12/14 B. NEWSOM - ACIS/CIS CREF REDESIGN COMMUNICATIONS*
*                                MOVE MULTIPLE PLAN ARRAY TO OUTPUT   *
*                                FILE                                 *
* 06/21/17  (MITRAPU) PIN EXPANSION CHANGES. (C425939) STOW ONLY      *
* (ONEIRA2) 01/20/18 ELLO      - CHANGES TO INCLUDE THE FOLLOWING     *
*                                FIELDS FOR ONEIRA                    *
*                                DX-TIAA-INIT-PREM = TIAA INIT. PREM  *
*                                DX-CREF-INIT-PREM = CREF INIT. PREM  *
* (CNTRSTRG) 11/17/19 BISWP    - INCLUDE ADDITIONAL FIELDS FOR        *
* (IISG)              L SHU      CONTRACT STRATEGY AND IISG           *
***********************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb1155 extends BLNatBase
{
    // Data Areas
    private LdaAppl160 ldaAppl160;
    private LdaAppl1157 ldaAppl1157;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;

    private DbsGroup app_Table_Entry__R_Field_1;
    private DbsField app_Table_Entry_Entry_State_Code;
    private DbsField app_Table_Entry_Filler;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup app_Table_Entry__R_Field_2;
    private DbsField app_Table_Entry_Entry_Agent_Crd_No;
    private DbsField app_Table_Entry_Filler_2;
    private DbsField pnd_Search_Key;

    private DbsGroup pnd_Search_Key__R_Field_3;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id;
    private DbsField pnd_Search_Key_Pnd_Ky_Entry_Cde;

    private DbsGroup pnd_Search_Key__R_Field_4;
    private DbsField pnd_Search_Key_Pnd_Ky_Plan_Number;
    private DbsField pnd_Search_Key_Pnd_Ky_Subplan_Number;
    private DbsField pnd_Search_Key_Pnd_Ky_Addl_Cde;

    private DbsGroup pnd_Search_Key__R_Field_5;
    private DbsField pnd_Search_Key_Pnd_Ky_Pkg_Cntrl_Type;
    private DbsField pnd_Search_Key_Pnd_Ky_Addl_Cde_2;
    private DbsField pnd_Cnt;
    private DbsField pnd_Write_Count;
    private DbsField pnd_Index;
    private DbsField pnd_Middle_Name;

    private DbsGroup pnd_States_Lgl_Coverpage;
    private DbsField pnd_States_Lgl_Coverpage_Pnd_State_Code_Lgl_Cover;
    private DbsField pnd_States_Lgl_Coverpage_Pnd_State_Def_Crd_No;
    private DbsField pnd_Ws_Phone;

    private DbsGroup pnd_Psg9065;
    private DbsField pnd_Psg9065_Pnd_Agent_Crd_No;
    private DbsField pnd_Psg9065_Pnd_Return_Code;
    private DbsField pnd_Psg9065_Pnd_Return_Message;
    private DbsField pnd_Psg9065_Pnd_Agent_First_Name;
    private DbsField pnd_Psg9065_Pnd_Agent_Middle_Name;
    private DbsField pnd_Psg9065_Pnd_Agent_Last_Name;
    private DbsField pnd_Psg9065_Pnd_Agent_Work_Addr1;
    private DbsField pnd_Psg9065_Pnd_Agent_Work_Addr2;
    private DbsField pnd_Psg9065_Pnd_Agent_Work_City;
    private DbsField pnd_Psg9065_Pnd_Agent_Work_State;
    private DbsField pnd_Psg9065_Pnd_Agent_Work_Zip;
    private DbsField pnd_Psg9065_Pnd_Agent_Work_Phone;
    private int psg9065ReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl160 = new LdaAppl160();
        registerRecord(ldaAppl160);
        ldaAppl1157 = new LdaAppl1157();
        registerRecord(ldaAppl1157);

        // Local Variables
        localVariables = new DbsRecord();

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        app_Table_Entry__R_Field_1 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_1", "REDEFINE", app_Table_Entry_Entry_Cde);
        app_Table_Entry_Entry_State_Code = app_Table_Entry__R_Field_1.newFieldInGroup("app_Table_Entry_Entry_State_Code", "ENTRY-STATE-CODE", FieldType.STRING, 
            2);
        app_Table_Entry_Filler = app_Table_Entry__R_Field_1.newFieldInGroup("app_Table_Entry_Filler", "FILLER", FieldType.STRING, 18);
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        app_Table_Entry__R_Field_2 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_2", "REDEFINE", app_Table_Entry_Entry_Dscrptn_Txt);
        app_Table_Entry_Entry_Agent_Crd_No = app_Table_Entry__R_Field_2.newFieldInGroup("app_Table_Entry_Entry_Agent_Crd_No", "ENTRY-AGENT-CRD-NO", FieldType.STRING, 
            15);
        app_Table_Entry_Filler_2 = app_Table_Entry__R_Field_2.newFieldInGroup("app_Table_Entry_Filler_2", "FILLER-2", FieldType.STRING, 45);
        registerRecord(vw_app_Table_Entry);

        pnd_Search_Key = localVariables.newFieldInRecord("pnd_Search_Key", "#SEARCH-KEY", FieldType.STRING, 29);

        pnd_Search_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Search_Key__R_Field_3", "REDEFINE", pnd_Search_Key);
        pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind", "#KY-ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr", "#KY-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id", "#KY-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Search_Key_Pnd_Ky_Entry_Cde = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Entry_Cde", "#KY-ENTRY-CDE", FieldType.STRING, 
            20);

        pnd_Search_Key__R_Field_4 = pnd_Search_Key__R_Field_3.newGroupInGroup("pnd_Search_Key__R_Field_4", "REDEFINE", pnd_Search_Key_Pnd_Ky_Entry_Cde);
        pnd_Search_Key_Pnd_Ky_Plan_Number = pnd_Search_Key__R_Field_4.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Plan_Number", "#KY-PLAN-NUMBER", FieldType.STRING, 
            6);
        pnd_Search_Key_Pnd_Ky_Subplan_Number = pnd_Search_Key__R_Field_4.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Subplan_Number", "#KY-SUBPLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Search_Key_Pnd_Ky_Addl_Cde = pnd_Search_Key__R_Field_4.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Addl_Cde", "#KY-ADDL-CDE", FieldType.STRING, 
            8);

        pnd_Search_Key__R_Field_5 = pnd_Search_Key__R_Field_4.newGroupInGroup("pnd_Search_Key__R_Field_5", "REDEFINE", pnd_Search_Key_Pnd_Ky_Addl_Cde);
        pnd_Search_Key_Pnd_Ky_Pkg_Cntrl_Type = pnd_Search_Key__R_Field_5.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Pkg_Cntrl_Type", "#KY-PKG-CNTRL-TYPE", 
            FieldType.STRING, 1);
        pnd_Search_Key_Pnd_Ky_Addl_Cde_2 = pnd_Search_Key__R_Field_5.newFieldInGroup("pnd_Search_Key_Pnd_Ky_Addl_Cde_2", "#KY-ADDL-CDE-2", FieldType.STRING, 
            7);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.INTEGER, 2);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.NUMERIC, 6);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.INTEGER, 2);
        pnd_Middle_Name = localVariables.newFieldInRecord("pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 20);

        pnd_States_Lgl_Coverpage = localVariables.newGroupArrayInRecord("pnd_States_Lgl_Coverpage", "#STATES-LGL-COVERPAGE", new DbsArrayController(1, 
            52));
        pnd_States_Lgl_Coverpage_Pnd_State_Code_Lgl_Cover = pnd_States_Lgl_Coverpage.newFieldInGroup("pnd_States_Lgl_Coverpage_Pnd_State_Code_Lgl_Cover", 
            "#STATE-CODE-LGL-COVER", FieldType.STRING, 2);
        pnd_States_Lgl_Coverpage_Pnd_State_Def_Crd_No = pnd_States_Lgl_Coverpage.newFieldInGroup("pnd_States_Lgl_Coverpage_Pnd_State_Def_Crd_No", "#STATE-DEF-CRD-NO", 
            FieldType.STRING, 15);
        pnd_Ws_Phone = localVariables.newFieldInRecord("pnd_Ws_Phone", "#WS-PHONE", FieldType.STRING, 25);

        pnd_Psg9065 = localVariables.newGroupInRecord("pnd_Psg9065", "#PSG9065");
        pnd_Psg9065_Pnd_Agent_Crd_No = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Crd_No", "#AGENT-CRD-NO", FieldType.STRING, 15);
        pnd_Psg9065_Pnd_Return_Code = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 4);
        pnd_Psg9065_Pnd_Return_Message = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Return_Message", "#RETURN-MESSAGE", FieldType.STRING, 80);
        pnd_Psg9065_Pnd_Agent_First_Name = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_First_Name", "#AGENT-FIRST-NAME", FieldType.STRING, 80);
        pnd_Psg9065_Pnd_Agent_Middle_Name = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Middle_Name", "#AGENT-MIDDLE-NAME", FieldType.STRING, 30);
        pnd_Psg9065_Pnd_Agent_Last_Name = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Last_Name", "#AGENT-LAST-NAME", FieldType.STRING, 80);
        pnd_Psg9065_Pnd_Agent_Work_Addr1 = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Work_Addr1", "#AGENT-WORK-ADDR1", FieldType.STRING, 60);
        pnd_Psg9065_Pnd_Agent_Work_Addr2 = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Work_Addr2", "#AGENT-WORK-ADDR2", FieldType.STRING, 60);
        pnd_Psg9065_Pnd_Agent_Work_City = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Work_City", "#AGENT-WORK-CITY", FieldType.STRING, 60);
        pnd_Psg9065_Pnd_Agent_Work_State = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Work_State", "#AGENT-WORK-STATE", FieldType.STRING, 2);
        pnd_Psg9065_Pnd_Agent_Work_Zip = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Work_Zip", "#AGENT-WORK-ZIP", FieldType.STRING, 10);
        pnd_Psg9065_Pnd_Agent_Work_Phone = pnd_Psg9065.newFieldInGroup("pnd_Psg9065_Pnd_Agent_Work_Phone", "#AGENT-WORK-PHONE", FieldType.STRING, 25);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_app_Table_Entry.reset();

        ldaAppl160.initializeValues();
        ldaAppl1157.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb1155() throws Exception
    {
        super("Appb1155");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB1155", onError);
        //*  LOAD STATES THAT REQUIRE COVER PAGE ON LGL
                                                                                                                                                                          //Natural: PERFORM READ-TABLE-ENTRY
        sub_Read_Table_Entry();
        if (condition(Global.isEscape())) {return;}
        //*  CS (TIC)
        //*  ONEIRA2
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 DOC-EXT-DATA-1 DOC-EXT-DATA-2 DOC-EXT-DATA-3 DOC-EXT-DATA-4 DOC-EXT-DATA-5 DOC-EXT-DATA-6 DOC-EXT-DATA-7 DOC-EXT-DATA-8 DOC-EXT-DATA-9 DOC-EXT-DATA-10 DOC-EXT-DATA-11 DOC-EXT-DATA-12 DOC-EXT-DATA-13 DOC-EXT-DATA-14 DOC-EXT-DATA-15 DOC-EXT-DATA-16 DOC-EXT-DATA-17 DOC-EXT-DATA-18 DOC-EXT-DATA-19 DOC-EXT-DATA-20 DOC-EXT-DATA-21 DOC-EXT-DATA-22 DOC-EXT-DATA-23 DOC-EXT-DATA-24 DOC-EXT-DATA-25 DOC-EXT-DATA-26
        while (condition(getWorkFiles().read(1, ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_1(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_2(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_3(), 
            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_4(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_5(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_6(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_7(), 
            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_8(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_9(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_10(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_11(), 
            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_12(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_13(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_14(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_15(), 
            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_16(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_17(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_18(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_19(), 
            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_20(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_21(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_22(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_23(), 
            ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_24(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_25(), ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_26())))
        {
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_1().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_1());                                                        //Natural: ASSIGN DOC-EXT2-DATA-1 := DOC-EXT-DATA-1
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_2().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_2());                                                        //Natural: ASSIGN DOC-EXT2-DATA-2 := DOC-EXT-DATA-2
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_3().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_3());                                                        //Natural: ASSIGN DOC-EXT2-DATA-3 := DOC-EXT-DATA-3
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_4().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_4());                                                        //Natural: ASSIGN DOC-EXT2-DATA-4 := DOC-EXT-DATA-4
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_5().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_5());                                                        //Natural: ASSIGN DOC-EXT2-DATA-5 := DOC-EXT-DATA-5
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_6().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_6());                                                        //Natural: ASSIGN DOC-EXT2-DATA-6 := DOC-EXT-DATA-6
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_7().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_7());                                                        //Natural: ASSIGN DOC-EXT2-DATA-7 := DOC-EXT-DATA-7
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_8().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_8());                                                        //Natural: ASSIGN DOC-EXT2-DATA-8 := DOC-EXT-DATA-8
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_9().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_9());                                                        //Natural: ASSIGN DOC-EXT2-DATA-9 := DOC-EXT-DATA-9
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_10().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_10());                                                      //Natural: ASSIGN DOC-EXT2-DATA-10 := DOC-EXT-DATA-10
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_11().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_11());                                                      //Natural: ASSIGN DOC-EXT2-DATA-11 := DOC-EXT-DATA-11
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_12().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_12());                                                      //Natural: ASSIGN DOC-EXT2-DATA-12 := DOC-EXT-DATA-12
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_13().setValue(ldaAppl160.getDoc_Ext_File_Doc_Ext_Data_13());                                                      //Natural: ASSIGN DOC-EXT2-DATA-13 := DOC-EXT-DATA-13
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (TIC)
            //*  CS (MOBKEY)
            //* TNGSUB
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_14().reset();                                                                                                     //Natural: RESET DOC-EXT2-DATA-14 DOC-EXT2-DATA-15 DOC-EXT2-DATA-16 DOC-EXT2-DATA-17 DOC-EXT2-DATA-18 DOC-EXT2-DATA-19 DOC-EXT2-DATA-20 DOC-EXT2-DATA-21 DOC-EXT2-DATA-22 DOC-EXT2-DATA-23 DOC-EXT2-DATA-24 DOC-EXT2-DATA-25 DOC-EXT2-DATA-26 DOC-EXT2-DATA-27 DX2-CRD-PULL-IND DX2-AGENT-NAME
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_15().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_16().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_17().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_18().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_19().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_20().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_21().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_22().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_23().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_24().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_25().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_26().reset();
            ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_27().reset();
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Crd_Pull_Ind().reset();
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Name().reset();
            //*  DOC-EXT2-DATA-14 := DOC-EXT-DATA-14
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Ind().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Ind());                                                                //Natural: ASSIGN DX2-TIC-IND := DX-TIC-IND
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Startdate().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Startdate());                                                    //Natural: ASSIGN DX2-TIC-STARTDATE := DX-TIC-STARTDATE
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Enddate().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Enddate());                                                        //Natural: ASSIGN DX2-TIC-ENDDATE := DX-TIC-ENDDATE
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Percentage().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Percentage());                                                  //Natural: ASSIGN DX2-TIC-PERCENTAGE := DX-TIC-PERCENTAGE
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Postdays().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Postdays());                                                      //Natural: ASSIGN DX2-TIC-POSTDAYS := DX-TIC-POSTDAYS
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Limit().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Limit());                                                            //Natural: ASSIGN DX2-TIC-LIMIT := DX-TIC-LIMIT
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Postfreq().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Postfreq());                                                      //Natural: ASSIGN DX2-TIC-POSTFREQ := DX-TIC-POSTFREQ
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Pl_Level().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Pl_Level());                                                      //Natural: ASSIGN DX2-TIC-PL-LEVEL := DX-TIC-PL-LEVEL
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Windowdays().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Windowdays());                                                  //Natural: ASSIGN DX2-TIC-WINDOWDAYS := DX-TIC-WINDOWDAYS
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Reqdlywindow().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Reqdlywindow());                                              //Natural: ASSIGN DX2-TIC-REQDLYWINDOW := DX-TIC-REQDLYWINDOW
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tic_Recap_Prov().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tic_Recap_Prov());                                                  //Natural: ASSIGN DX2-TIC-RECAP-PROV := DX-TIC-RECAP-PROV
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Ecs_Dcs_E_Dlvry_Ind().setValue(ldaAppl160.getDoc_Ext_File_Dx_Ecs_Dcs_E_Dlvry_Ind());                                        //Natural: ASSIGN DX2-ECS-DCS-E-DLVRY-IND := DX-ECS-DCS-E-DLVRY-IND
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Ecs_Dcs_Annty_Optn_Ind().setValue(ldaAppl160.getDoc_Ext_File_Dx_Ecs_Dcs_Annty_Optn_Ind());                                  //Natural: ASSIGN DX2-ECS-DCS-ANNTY-OPTN-IND := DX-ECS-DCS-ANNTY-OPTN-IND
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Full_Middle_Name().setValue(ldaAppl160.getDoc_Ext_File_Dx_Full_Middle_Name());                                              //Natural: ASSIGN DX2-FULL-MIDDLE-NAME := DX-FULL-MIDDLE-NAME
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Substitution_Contract_Ind().setValue(ldaAppl160.getDoc_Ext_File_Dx_Substitution_Contract_Ind());                            //Natural: ASSIGN DX2-SUBSTITUTION-CONTRACT-IND := DX-SUBSTITUTION-CONTRACT-IND
            //*  TNGSUB START - TO WRITE OUT WELCOME DATA WITH EXPANDED FILE LAYOUT
            if (condition(ldaAppl160.getDoc_Ext_File_Dx_Request_Package_Type().equals("W") || ldaAppl160.getDoc_Ext_File_Dx_Print_Package_Type().equals("W")))            //Natural: IF DX-REQUEST-PACKAGE-TYPE = 'W' OR DX-PRINT-PACKAGE-TYPE = 'W'
            {
                ignore();
                //*  TNGSUB END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*   IF LEGAL PACKAGE FOR AN INDIVIDUAL PRODUCT
                //*     AND STATE REQUIRES NEW COVER PAGE
                if (condition(((ldaAppl160.getDoc_Ext_File_Dx_Request_Package_Type().equals("L") || ldaAppl160.getDoc_Ext_File_Dx_Print_Package_Type().equals("L"))       //Natural: IF ( DX-REQUEST-PACKAGE-TYPE = 'L' OR DX-PRINT-PACKAGE-TYPE = 'L' ) AND ( SUBSTR ( DX-SG-SUBPLAN-NO,1,2 ) = 'RA' OR = 'SR' OR = 'IR' OR = 'AA' )
                    && (((ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_No().getSubstring(1,2).equals("RA") || ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_No().getSubstring(1,2).equals("SR")) 
                    || ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_No().getSubstring(1,2).equals("IR")) || ldaAppl160.getDoc_Ext_File_Dx_Sg_Subplan_No().getSubstring(1,
                    2).equals("AA")))))
                {
                    if (condition(ldaAppl160.getDoc_Ext_File_Dx_Current_Iss_State().greater(" ") && ldaAppl160.getDoc_Ext_File_Dx_Current_Iss_State().equals(pnd_States_Lgl_Coverpage_Pnd_State_Code_Lgl_Cover.getValue("*")))) //Natural: IF DX-CURRENT-ISS-STATE GT ' ' AND DX-CURRENT-ISS-STATE = #STATE-CODE-LGL-COVER ( * )
                    {
                        DbsUtil.examine(new ExamineSource(pnd_States_Lgl_Coverpage_Pnd_State_Code_Lgl_Cover.getValue("*"),true), new ExamineSearch(ldaAppl160.getDoc_Ext_File_Dx_Current_Iss_State()),  //Natural: EXAMINE FULL #STATE-CODE-LGL-COVER ( * ) FOR DX-CURRENT-ISS-STATE GIVING INDEX #INDEX
                            new ExamineGivingIndex(pnd_Index));
                        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Agent_Crd_No().equals(" ")))                                                                          //Natural: IF DX-AGENT-CRD-NO = ' '
                        {
                            if (condition(pnd_Index.greater(getZero())))                                                                                                  //Natural: IF #INDEX GT 0
                            {
                                pnd_Psg9065_Pnd_Agent_Crd_No.setValue(pnd_States_Lgl_Coverpage_Pnd_State_Def_Crd_No.getValue(pnd_Index));                                 //Natural: ASSIGN #PSG9065.#AGENT-CRD-NO := #STATE-DEF-CRD-NO ( #INDEX )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Psg9065_Pnd_Agent_Crd_No.setValue(ldaAppl160.getDoc_Ext_File_Dx_Agent_Crd_No());                                                          //Natural: ASSIGN #PSG9065.#AGENT-CRD-NO := DX-AGENT-CRD-NO
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-COBOL-FOR-AGENT-INFO
                        sub_Call_Cobol_For_Agent_Info();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  (1670)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (1640)
                }                                                                                                                                                         //Natural: END-IF
                //*  (1570) TNGSUB
                //*  MTSIN
                //*  ACCRC
                //*  ACCRC
                //*  ONEIRA2
                //*  ONEIRA2
                //* CNTRSTRG
                //*  IISG
                //*  IISG
                //*  IISG
                //*  IISG
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Non_Proprietary_Pgk_Ind().setValue(ldaAppl160.getDoc_Ext_File_Dx_Non_Proprietary_Pkg_Ind());                                //Natural: ASSIGN DX2-NON-PROPRIETARY-PGK-IND := DX-NON-PROPRIETARY-PKG-IND
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Multi_Plan_No().getValue("*").setValue(ldaAppl160.getDoc_Ext_File_Dx_Multi_Plan_No().getValue("*"));                        //Natural: ASSIGN DX2-MULTI-PLAN-NO ( * ) := DX-MULTI-PLAN-NO ( * )
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Multi_Sub_Plan().getValue("*").setValue(ldaAppl160.getDoc_Ext_File_Dx_Multi_Sub_Plan().getValue("*"));                      //Natural: ASSIGN DX2-MULTI-SUB-PLAN ( * ) := DX-MULTI-SUB-PLAN ( * )
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Tiaa_Init_Prem().setValue(ldaAppl160.getDoc_Ext_File_Dx_Tiaa_Init_Prem());                                                  //Natural: ASSIGN DX2-TIAA-INIT-PREM := DX-TIAA-INIT-PREM
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Cref_Init_Prem().setValue(ldaAppl160.getDoc_Ext_File_Dx_Cref_Init_Prem());                                                  //Natural: ASSIGN DX2-CREF-INIT-PREM := DX-CREF-INIT-PREM
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Related_Contract_Info().getValue("*").setValue(ldaAppl160.getDoc_Ext_File_Dx_Related_Contract_Info().getValue("*"));        //Natural: ASSIGN DX2-RELATED-CONTRACT-INFO ( * ) := DX-RELATED-CONTRACT-INFO ( * )
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Fund_Source_1().setValue(ldaAppl160.getDoc_Ext_File_Dx_Fund_Source_Cde_1());                                                //Natural: ASSIGN DX2-FUND-SOURCE-1 := DX-FUND-SOURCE-CDE-1
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Fund_Source_2().setValue(ldaAppl160.getDoc_Ext_File_Dx_Fund_Source_Cde_2());                                                //Natural: ASSIGN DX2-FUND-SOURCE-2 := DX-FUND-SOURCE-CDE-2
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Fund_Cde_2().getValue("*").setValue(ldaAppl160.getDoc_Ext_File_Dx_Fund_Cde_2().getValue("*"));                              //Natural: ASSIGN DX2-FUND-CDE-2 ( * ) := DX-FUND-CDE-2 ( * )
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Alloc_Pct_2().getValue("*").setValue(ldaAppl160.getDoc_Ext_File_Dx_Alloc_Pct_2().getValue("*"));                            //Natural: ASSIGN DX2-ALLOC-PCT-2 ( * ) := DX-ALLOC-PCT-2 ( * )
            //*  ONEIRA
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            //*  IISG
            getWorkFiles().write(2, false, ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_1(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_2(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_3(),  //Natural: WRITE WORK FILE 2 DOC-EXT2-DATA-1 DOC-EXT2-DATA-2 DOC-EXT2-DATA-3 DOC-EXT2-DATA-4 DOC-EXT2-DATA-5 DOC-EXT2-DATA-6 DOC-EXT2-DATA-7 DOC-EXT2-DATA-8 DOC-EXT2-DATA-9 DOC-EXT2-DATA-10 DOC-EXT2-DATA-11 DOC-EXT2-DATA-12 DOC-EXT2-DATA-13 DOC-EXT2-DATA-14 DOC-EXT2-DATA-15 DOC-EXT2-DATA-16 DOC-EXT2-DATA-17 DOC-EXT2-DATA-18 DOC-EXT2-DATA-19 DOC-EXT2-DATA-20 DOC-EXT2-DATA-21 DOC-EXT2-DATA-22 DOC-EXT2-DATA-23 DOC-EXT2-DATA-24 DOC-EXT2-DATA-25 DOC-EXT2-DATA-26 DOC-EXT2-DATA-27
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_4(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_5(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_6(), 
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_7(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_8(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_9(), 
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_10(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_11(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_12(), 
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_13(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_14(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_15(), 
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_16(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_17(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_18(), 
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_19(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_20(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_21(), 
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_22(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_23(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_24(), 
                ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_25(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_26(), ldaAppl1157.getDoc_Ext_File_2_Doc_Ext2_Data_27());
            pnd_Write_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #WRITE-COUNT
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  TNGSUB
        //*  TNGSUB
        if (condition(ldaAppl160.getDoc_Ext_File_Dx_Request_Package_Type().equals("L") || ldaAppl160.getDoc_Ext_File_Dx_Print_Package_Type().equals("L")))                //Natural: IF DX-REQUEST-PACKAGE-TYPE = 'L' OR DX-PRINT-PACKAGE-TYPE = 'L'
        {
            getReports().write(0, pnd_States_Lgl_Coverpage_Pnd_State_Code_Lgl_Cover.getValue("*"));                                                                       //Natural: WRITE #STATE-CODE-LGL-COVER ( * )
            if (Global.isEscape()) return;
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-COBOL-FOR-AGENT-INFO
        //*  CALL COBOL PROGRAM 'PSG9065'.  THE COBOL PROGRAM WILL ACCEPT AN INPUT
        //*  OF CRD NO. AND CALL AN AODM ORACLE STORED PROCEDURE TO RETRIEVE THE
        //*  AGENT INFO..  THE INFO. IS THEN RETURNED TO THIS PROGRAM AND WRITTEN
        //*  INTO THE EXTRACT FILE.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-TABLE-ENTRY
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Call_Cobol_For_Agent_Info() throws Exception                                                                                                         //Natural: CALL-COBOL-FOR-AGENT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*    A1
        //*    A90
        //*    A35
        //*    A35
        //*    A35
        //*    A2
        //*    A10
        //*    A25
        //*  SET TO 8888 PRIOR TO CALL
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Crd_Pull_Ind().reset();                                                                                                         //Natural: RESET DX2-CRD-PULL-IND DX2-AGENT-NAME DX2-AGENT-WORK-ADDR1 DX2-AGENT-WORK-ADDR2 DX2-AGENT-WORK-CITY DX2-AGENT-WORK-STATE DX2-AGENT-WORK-ZIP DX2-AGENT-WORK-PHONE
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Name().reset();
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Addr1().reset();
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Addr2().reset();
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_City().reset();
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_State().reset();
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Zip().reset();
        ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Phone().reset();
        pnd_Psg9065_Pnd_Return_Code.setValue(8888);                                                                                                                       //Natural: ASSIGN #RETURN-CODE := 8888
        psg9065ReturnCode = DbsUtil.callExternalProgram("PSG9065",pnd_Psg9065_Pnd_Agent_Crd_No,pnd_Psg9065_Pnd_Return_Code,pnd_Psg9065_Pnd_Return_Message,                //Natural: CALL 'PSG9065' #PSG9065
            pnd_Psg9065_Pnd_Agent_First_Name,pnd_Psg9065_Pnd_Agent_Middle_Name,pnd_Psg9065_Pnd_Agent_Last_Name,pnd_Psg9065_Pnd_Agent_Work_Addr1,pnd_Psg9065_Pnd_Agent_Work_Addr2,
            pnd_Psg9065_Pnd_Agent_Work_City,pnd_Psg9065_Pnd_Agent_Work_State,pnd_Psg9065_Pnd_Agent_Work_Zip,pnd_Psg9065_Pnd_Agent_Work_Phone);
        getReports().write(0, "=",pnd_Psg9065_Pnd_Agent_Crd_No,NEWLINE,"=",pnd_Psg9065_Pnd_Agent_First_Name, new AlphanumericLength (30),NEWLINE,"=",pnd_Psg9065_Pnd_Agent_Middle_Name,  //Natural: WRITE '=' #AGENT-CRD-NO / '=' #AGENT-FIRST-NAME ( AL = 30 ) / '=' #AGENT-MIDDLE-NAME ( AL = 30 ) / '=' #AGENT-LAST-NAME ( AL = 30 ) / '=' #AGENT-WORK-ADDR1 / '=' #AGENT-WORK-ADDR2 / '=' #AGENT-WORK-CITY / '=' #AGENT-WORK-STATE / '=' #AGENT-WORK-ZIP / '=' #AGENT-WORK-PHONE / '=' #RETURN-CODE / '**************************' /
            new AlphanumericLength (30),NEWLINE,"=",pnd_Psg9065_Pnd_Agent_Last_Name, new AlphanumericLength (30),NEWLINE,"=",pnd_Psg9065_Pnd_Agent_Work_Addr1,
            NEWLINE,"=",pnd_Psg9065_Pnd_Agent_Work_Addr2,NEWLINE,"=",pnd_Psg9065_Pnd_Agent_Work_City,NEWLINE,"=",pnd_Psg9065_Pnd_Agent_Work_State,NEWLINE,
            "=",pnd_Psg9065_Pnd_Agent_Work_Zip,NEWLINE,"=",pnd_Psg9065_Pnd_Agent_Work_Phone,NEWLINE,"=",pnd_Psg9065_Pnd_Return_Code,NEWLINE,"**************************",
            NEWLINE);
        if (Global.isEscape()) return;
        //*  AGENT INFO. IS PULLED OR RETURNED
        if (condition(pnd_Psg9065_Pnd_Return_Code.equals(getZero())))                                                                                                     //Natural: IF #RETURN-CODE = 0
        {
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Crd_Pull_Ind().setValue("Y");                                                                                               //Natural: ASSIGN DX2-CRD-PULL-IND := 'Y'
            pnd_Middle_Name.reset();                                                                                                                                      //Natural: RESET #MIDDLE-NAME
            short decideConditionsMet931 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #AGENT-FIRST-NAME GT ' '
            if (condition(pnd_Psg9065_Pnd_Agent_First_Name.greater(" ")))
            {
                decideConditionsMet931++;
                ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Name().setValue(DbsUtil.compress(pnd_Psg9065_Pnd_Agent_First_Name));                                              //Natural: COMPRESS #AGENT-FIRST-NAME INTO DX2-AGENT-NAME
            }                                                                                                                                                             //Natural: WHEN #AGENT-MIDDLE-NAME GT ' '
            if (condition(pnd_Psg9065_Pnd_Agent_Middle_Name.greater(" ")))
            {
                decideConditionsMet931++;
                pnd_Middle_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Psg9065_Pnd_Agent_Middle_Name.getSubstring(1,1), "."));                      //Natural: COMPRESS SUBSTRING ( #AGENT-MIDDLE-NAME,1,1 ) '.' INTO #MIDDLE-NAME LEAVING NO SPACE
                ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Name().setValue(DbsUtil.compress(ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Name(), pnd_Middle_Name));               //Natural: COMPRESS DX2-AGENT-NAME #MIDDLE-NAME INTO DX2-AGENT-NAME
            }                                                                                                                                                             //Natural: WHEN #AGENT-LAST-NAME GT ' '
            if (condition(pnd_Psg9065_Pnd_Agent_Last_Name.greater(" ")))
            {
                decideConditionsMet931++;
                ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Name().setValue(DbsUtil.compress(ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Name(), pnd_Psg9065_Pnd_Agent_Last_Name)); //Natural: COMPRESS DX2-AGENT-NAME #AGENT-LAST-NAME INTO DX2-AGENT-NAME
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet931 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Addr1().setValue(pnd_Psg9065_Pnd_Agent_Work_Addr1);                                                              //Natural: ASSIGN DX2-AGENT-WORK-ADDR1 := #AGENT-WORK-ADDR1
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Addr2().setValue(pnd_Psg9065_Pnd_Agent_Work_Addr2);                                                              //Natural: ASSIGN DX2-AGENT-WORK-ADDR2 := #AGENT-WORK-ADDR2
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_City().setValue(pnd_Psg9065_Pnd_Agent_Work_City);                                                                //Natural: ASSIGN DX2-AGENT-WORK-CITY := #AGENT-WORK-CITY
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_State().setValue(pnd_Psg9065_Pnd_Agent_Work_State);                                                              //Natural: ASSIGN DX2-AGENT-WORK-STATE := #AGENT-WORK-STATE
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Zip().setValue(pnd_Psg9065_Pnd_Agent_Work_Zip);                                                                  //Natural: ASSIGN DX2-AGENT-WORK-ZIP := #AGENT-WORK-ZIP
            if (condition(DbsUtil.maskMatches(pnd_Psg9065_Pnd_Agent_Work_Phone,"NNNNNNNNNN")))                                                                            //Natural: IF #AGENT-WORK-PHONE = MASK ( NNNNNNNNNN )
            {
                ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Phone().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "(", pnd_Psg9065_Pnd_Agent_Work_Phone.getSubstring(1,3),  //Natural: COMPRESS '(' SUBSTR ( #AGENT-WORK-PHONE,1,3 ) ')' INTO DX2-AGENT-WORK-PHONE LEAVING NO SPACE
                    ")"));
                ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Phone().setValue(DbsUtil.compress(ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Phone(), pnd_Psg9065_Pnd_Agent_Work_Phone.getSubstring(4, //Natural: COMPRESS DX2-AGENT-WORK-PHONE SUBSTR ( #AGENT-WORK-PHONE,4,3 ) INTO DX2-AGENT-WORK-PHONE
                    3)));
                ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Phone().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Phone(),  //Natural: COMPRESS DX2-AGENT-WORK-PHONE '-' SUBSTR ( #AGENT-WORK-PHONE,7,4 ) INTO DX2-AGENT-WORK-PHONE LEAVING NO SPACE
                    "-", pnd_Psg9065_Pnd_Agent_Work_Phone.getSubstring(7,4)));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl1157.getDoc_Ext_File_2_Dx2_Agent_Work_Phone().setValue(pnd_Psg9065_Pnd_Agent_Work_Phone);                                                          //Natural: ASSIGN DX2-AGENT-WORK-PHONE := #AGENT-WORK-PHONE
            }                                                                                                                                                             //Natural: END-IF
            //*  AGENT INFO. IS NOT PULLED OR RETURNED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1157.getDoc_Ext_File_2_Dx2_Crd_Pull_Ind().setValue("N");                                                                                               //Natural: ASSIGN DX2-CRD-PULL-IND := 'N'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Table_Entry() throws Exception                                                                                                                  //Natural: READ-TABLE-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Search_Key.reset();                                                                                                                                           //Natural: RESET #SEARCH-KEY
        pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind.setValue("Y");                                                                                                              //Natural: ASSIGN #KY-ENTRY-ACTVE-IND := 'Y'
        pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr.setValue(100);                                                                                                           //Natural: ASSIGN #KY-ENTRY-TABLE-ID-NBR := 100
        pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id.setValue("AG");                                                                                                          //Natural: ASSIGN #KY-ENTRY-TABLE-SUB-ID := 'AG'
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE FROM #SEARCH-KEY
        (
        "READ02",
        new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Search_Key, WcType.BY) },
        new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ02:
        while (condition(vw_app_Table_Entry.readNextRow("READ02")))
        {
            if (condition(app_Table_Entry_Entry_Actve_Ind.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Actve_Ind) || app_Table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Table_Id_Nbr)  //Natural: IF ENTRY-ACTVE-IND NE #KY-ENTRY-ACTVE-IND OR ENTRY-TABLE-ID-NBR NE #KY-ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #KY-ENTRY-TABLE-SUB-ID
                || app_Table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Search_Key_Pnd_Ky_Entry_Table_Sub_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Index.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #INDEX
            pnd_States_Lgl_Coverpage_Pnd_State_Code_Lgl_Cover.getValue(pnd_Index).setValue(app_Table_Entry_Entry_State_Code);                                             //Natural: ASSIGN #STATE-CODE-LGL-COVER ( #INDEX ) := ENTRY-STATE-CODE
            pnd_States_Lgl_Coverpage_Pnd_State_Def_Crd_No.getValue(pnd_Index).setValue(app_Table_Entry_Entry_Agent_Crd_No);                                               //Natural: ASSIGN #STATE-DEF-CRD-NO ( #INDEX ) := ENTRY-AGENT-CRD-NO
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  CHECK-TABLE-ENTRY
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE
            TabSetting(25),Global.getERROR_LINE());
        getReports().write(0, "EXTRACT RECORDS WRITTEN:",pnd_Write_Count, new ReportEditMask ("ZZZ,ZZ9"));                                                                //Natural: WRITE 'EXTRACT RECORDS WRITTEN:' #WRITE-COUNT ( EM = ZZZ,ZZ9 )
    };                                                                                                                                                                    //Natural: END-ERROR
}
