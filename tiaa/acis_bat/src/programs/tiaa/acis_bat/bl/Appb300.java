/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:09 PM
**        * FROM NATURAL PROGRAM : Appb300
************************************************************
**        * FILE NAME            : Appb300.java
**        * CLASS NAME           : Appb300
**        * INSTANCE NAME        : Appb300
************************************************************
************************************************************************
* PROGRAM  : APPB300 - PRAP PURGE PROCESS                              *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : APRIL 28, 1999                                            *
* FUNCTION : READS THE PRAP FILE AND CREATES A WORK FILE FROM IT TO BE *
*            READ BY APPB301                                           *
*                                                                      *
* 08/01/01 K.GATES - NEW FIELD ADDED TO PRAP FILE AND WORK FILE,       *
*                    AP-PRAP-PREM-RSCH-IND AND CV-PRAP-PREM-RSCH-IND.  *
* 02/16/02 K.GATES - NEW LAYOUT OF APPL300 AND APPL301 FOR OIA         *
*                                                                      *
* 02/10/04 SINGLETON - NEW LAYOUT OF APPL300 AND APPL301 FOR SGRD      *
* 12/09/05 D.MARPURI - NEW LAYOUT OF APPL300 AND APPL301 FOR SGRD (RL8)*
* 07/14/06 K.GATES   - NEW LAYOUT OF APPL300 AND APPL301 FOR ARR  (ARR)*
* 05/03/07 K.GATES   - NEW LAYOUT OF APPL300 & APPL301 FOR ARR2 (ARR2) *
* 09/16/08 J BERGHEISER - ADD NEW DATABASE FIELDS TO STORE      (JRB1) *
*                         AND ADD CV-RULE-DATA-17 TO RESET      (JRB1) *
* 08/05/09 C.AVE - ADDED NEW DELETE FIELDS-CHANGES MARKED BY CA-082009 *
* 05/02/11 C.SCHNEIDER- NEW FIELDS THAT WERE ADDED TO PRAP FILE MOVED  *
*          TO WORK FILE.                                        (JHU)  *
* 06/27/11 C.SCHNEIDER- NEW FIELDS THAT WERE ADDED TO PRAP FILE MOVED  *
*          TO WORK FILE.                                        (TIC)  *
* 11/17/12 L.SHU - NEW FIELDS THAT WERE ADDED TO PRAP FILE MOVED       *
*          TO WORK FILE FOR LEGAL PKG ANNUITY OPTION.           (LPAO) *
* 07/18/13 L.SHU - RESTOW FOR APPL300 AND APPL301 FOR THE NEW IRA      *
*          SUBSTITUTION FIELDS                                         *
* 09/12/13 B.NEWSOM - EXTRACTS ALL PRAP DATA TO A FILE FOR PRAP PURGE  *
*                     PROCESSING.  MOVE AP-NON-PROPRIETARY-PGK-IND     *
*                     FROM APPL301 LDA TO APPL300 LDA.         (MTSIN) *
* 09/01/15 L SHU    - MOVE 3 FIELDS FOR BENE IN THE PLAN        (BIP)  *
*                     FROM APPL301 LDA TO APPL300 LDA.                 *
* 09/01/16 L SHU    - MOVE 1 FIELDS FOR ONEIRA FROM APPL301     (1IRA) *
*                     LDA TO APPL300 LDA.                              *
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)(RESTOW) PINE *
* 11/09/19 L SHU    - MOVE 4 FIELDS FOR IISG FROM APPL301 LDA   (IISG) *
*                     TO APPL300 LDA.                                  *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb300 extends BLNatBase
{
    // Data Areas
    private LdaAppl300 ldaAppl300;
    private LdaAppl301 ldaAppl301;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl300 = new LdaAppl300();
        registerRecord(ldaAppl300);
        ldaAppl301 = new LdaAppl301();
        registerRecord(ldaAppl301);
        registerRecord(ldaAppl301.getVw_prap());
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl300.initializeValues();
        ldaAppl301.initializeValues();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb300() throws Exception
    {
        super("Appb300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB300", onError);
        ldaAppl301.getVw_prap().startDatabaseRead                                                                                                                         //Natural: READ PRAP
        (
        "READ01",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ01:
        while (condition(ldaAppl301.getVw_prap().readNextRow("READ01")))
        {
            //*  OIA BE
            //*  OIA BE
            //*  JRB1
            //*  TIC
            //*  IISG
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_1().reset();                                                                                                          //Natural: RESET CV-RULE-DATA-1 CV-RULE-DATA-2 CV-RULE-DATA-3 CV-RULE-DATA-4 CV-RULE-DATA-5 CV-RULE-DATA-6 CV-RULE-DATA-7 CV-RULE-DATA-8 CV-RULE-DATA-9 CV-RULE-DATA-10 CV-RULE-DATA-11 CV-RULE-DATA-12 CV-RULE-DATA-13 CV-RULE-DATA-14 CV-RULE-DATA-15 CV-RULE-DATA-16 CV-RULE-DATA-17 CV-RULE-DATA-18 CV-RULE-DATA-19
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_2().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_3().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_4().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_5().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_6().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_7().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_8().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_9().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_10().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_11().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_12().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_13().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_14().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_15().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_16().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_17().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_18().reset();
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_19().reset();
                                                                                                                                                                          //Natural: PERFORM CREATE-NEW-PRAP
            sub_Create_New_Prap();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-NEW-PRAP
        //* *======
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Create_New_Prap() throws Exception                                                                                                                   //Natural: CREATE-NEW-PRAP
    {
        if (BLNatReinput.isReinput()) return;

        //*  EAC (RODGER
        //*  EAC (RODGER
        //*  EAC (RODGER
        //*  EAC (RODGER
        //*  OIA BE
        //*  OIA BE
        //*  OIA BE
        //*  OIA BE
        //*  SGRS CRS
        //*  SGRD CRS
        //*  SGRD KG
        //*  DTM - RL8
        //*  DTM - RL8
        //*  DTM - RL8
        //*  DTM - RL8
        //*  ARR - KG
        //*  ARR - KG
        //*  ARR - KG
        //*  ARR - KG
        //*  ARR2 - KG
        //*  ARR2 - KG
        //*  ARR2 - KG
        //*  ARR2 - KG
        //*  ARR2 - KG
        //*  ARR2 - KG
        //*  JRB1
        //*  JRB1
        //*  JRB1
        //*  JRB1
        //*  JRB1
        //*  JRB1
        //*  JRB1
        //*  JRB1
        //*  CA-082009
        //*  CA-082009
        //*  JHU
        //*  JHU
        //*  JHU
        //*  JHU
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  TIC
        //*  LPAO
        //*  LPAO
        //*  LPAO
        //*  TNGSUB
        //*  TNGSUB
        //*  TNGSUB
        //*  MTSIN
        //*  BIP
        //*  BIP
        //*  BIP
        //*  1IRA
        //*  IISG
        //*  IISG
        //*  IISG
        //*  IISG
        ldaAppl300.getCv_Prap_Rule().reset();                                                                                                                             //Natural: RESET CV-PRAP-RULE
        ldaAppl300.getCv_Prap_Rule_Cv_Coll_Code().setValue(ldaAppl301.getPrap_Ap_Coll_Code());                                                                            //Natural: ASSIGN CV-COLL-CODE := AP-COLL-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_G_Key().setValue(ldaAppl301.getPrap_Ap_G_Key());                                                                                    //Natural: ASSIGN CV-G-KEY := AP-G-KEY
        ldaAppl300.getCv_Prap_Rule_Cv_G_Ind().setValue(ldaAppl301.getPrap_Ap_G_Ind());                                                                                    //Natural: ASSIGN CV-G-IND := AP-G-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Soc_Sec().setValue(ldaAppl301.getPrap_Ap_Soc_Sec());                                                                                //Natural: ASSIGN CV-SOC-SEC := AP-SOC-SEC
        ldaAppl300.getCv_Prap_Rule_Cv_Lob().setValue(ldaAppl301.getPrap_Ap_Lob());                                                                                        //Natural: ASSIGN CV-LOB := AP-LOB
        ldaAppl300.getCv_Prap_Rule_Cv_Bill_Code().setValue(ldaAppl301.getPrap_Ap_Bill_Code());                                                                            //Natural: ASSIGN CV-BILL-CODE := AP-BILL-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Pref().setValue(ldaAppl301.getPrap_App_Pref());                                                                                     //Natural: ASSIGN CV-PREF := APP-PREF
        ldaAppl300.getCv_Prap_Rule_Cv_Cont().setValue(ldaAppl301.getPrap_App_Cont());                                                                                     //Natural: ASSIGN CV-CONT := APP-CONT
        ldaAppl300.getCv_Prap_Rule_Cv_C_Pref().setValue(ldaAppl301.getPrap_App_C_Pref());                                                                                 //Natural: ASSIGN CV-C-PREF := APP-C-PREF
        ldaAppl300.getCv_Prap_Rule_Cv_C_Cont().setValue(ldaAppl301.getPrap_App_C_Cont());                                                                                 //Natural: ASSIGN CV-C-CONT := APP-C-CONT
        ldaAppl300.getCv_Prap_Rule_Cv_Tiaa_Cntrct().setValue(ldaAppl301.getPrap_Ap_Tiaa_Cntrct());                                                                        //Natural: ASSIGN CV-TIAA-CNTRCT := AP-TIAA-CNTRCT
        ldaAppl300.getCv_Prap_Rule_Cv_Cref_Cert().setValue(ldaAppl301.getPrap_Ap_Cref_Cert());                                                                            //Natural: ASSIGN CV-CREF-CERT := AP-CREF-CERT
        ldaAppl300.getCv_Prap_Rule_Cv_T_Doi().setValue(ldaAppl301.getPrap_Ap_T_Doi());                                                                                    //Natural: ASSIGN CV-T-DOI := AP-T-DOI
        ldaAppl300.getCv_Prap_Rule_Cv_C_Doi().setValue(ldaAppl301.getPrap_Ap_C_Doi());                                                                                    //Natural: ASSIGN CV-C-DOI := AP-C-DOI
        ldaAppl300.getCv_Prap_Rule_Cv_Curr().setValue(ldaAppl301.getPrap_Ap_Curr());                                                                                      //Natural: ASSIGN CV-CURR := AP-CURR
        ldaAppl300.getCv_Prap_Rule_Cv_T_Age_1st().setValue(ldaAppl301.getPrap_Ap_T_Age_1st());                                                                            //Natural: ASSIGN CV-T-AGE-1ST := AP-T-AGE-1ST
        ldaAppl300.getCv_Prap_Rule_Cv_C_Age_1st().setValue(ldaAppl301.getPrap_Ap_C_Age_1st());                                                                            //Natural: ASSIGN CV-C-AGE-1ST := AP-C-AGE-1ST
        ldaAppl300.getCv_Prap_Rule_Cv_Sex().setValue(ldaAppl301.getPrap_Ap_Sex());                                                                                        //Natural: ASSIGN CV-SEX := AP-SEX
        ldaAppl300.getCv_Prap_Rule_Cv_Mail_Zip().setValue(ldaAppl301.getPrap_Ap_Mail_Zip());                                                                              //Natural: ASSIGN CV-MAIL-ZIP := AP-MAIL-ZIP
        ldaAppl300.getCv_Prap_Rule_Cv_Name_Addr_Cd().setValue(ldaAppl301.getPrap_Ap_Name_Addr_Cd());                                                                      //Natural: ASSIGN CV-NAME-ADDR-CD := AP-NAME-ADDR-CD
        ldaAppl300.getCv_Prap_Rule_Cv_Alloc_Discr().setValue(ldaAppl301.getPrap_Ap_Alloc_Discr());                                                                        //Natural: ASSIGN CV-ALLOC-DISCR := AP-ALLOC-DISCR
        ldaAppl300.getCv_Prap_Rule_Cv_Release_Ind().setValue(ldaAppl301.getPrap_Ap_Release_Ind());                                                                        //Natural: ASSIGN CV-RELEASE-IND := AP-RELEASE-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Type().setValue(ldaAppl301.getPrap_Ap_Type());                                                                                      //Natural: ASSIGN CV-TYPE := AP-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Other_Pols().setValue(ldaAppl301.getPrap_Ap_Other_Pols());                                                                          //Natural: ASSIGN CV-OTHER-POLS := AP-OTHER-POLS
        ldaAppl300.getCv_Prap_Rule_Cv_Record_Type().setValue(ldaAppl301.getPrap_Ap_Record_Type());                                                                        //Natural: ASSIGN CV-RECORD-TYPE := AP-RECORD-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Status().setValue(ldaAppl301.getPrap_Ap_Status());                                                                                  //Natural: ASSIGN CV-STATUS := AP-STATUS
        ldaAppl300.getCv_Prap_Rule_Cv_Numb_Dailys().setValue(ldaAppl301.getPrap_Ap_Numb_Dailys());                                                                        //Natural: ASSIGN CV-NUMB-DAILYS := AP-NUMB-DAILYS
        ldaAppl300.getCv_Prap_Rule_Cv_Allocation().getValue("*").setValue(ldaAppl301.getPrap_Ap_Allocation().getValue("*"));                                              //Natural: ASSIGN CV-ALLOCATION ( * ) := AP-ALLOCATION ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_App_Source().setValue(ldaAppl301.getPrap_Ap_App_Source());                                                                          //Natural: ASSIGN CV-APP-SOURCE := AP-APP-SOURCE
        ldaAppl300.getCv_Prap_Rule_Cv_Region_Code().setValue(ldaAppl301.getPrap_Ap_Region_Code());                                                                        //Natural: ASSIGN CV-REGION-CODE := AP-REGION-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Orig_Issue_State().setValue(ldaAppl301.getPrap_Ap_Orig_Issue_State());                                                              //Natural: ASSIGN CV-ORIG-ISSUE-STATE := AP-ORIG-ISSUE-STATE
        ldaAppl300.getCv_Prap_Rule_Cv_Current_State_Code().setValue(ldaAppl301.getPrap_Ap_Current_State_Code());                                                          //Natural: ASSIGN CV-CURRENT-STATE-CODE := AP-CURRENT-STATE-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Ownership_Type().setValue(ldaAppl301.getPrap_Ap_Ownership_Type());                                                                  //Natural: ASSIGN CV-OWNERSHIP-TYPE := AP-OWNERSHIP-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Lob_Type().setValue(ldaAppl301.getPrap_Ap_Lob_Type());                                                                              //Natural: ASSIGN CV-LOB-TYPE := AP-LOB-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Split_Code().setValue(ldaAppl301.getPrap_Ap_Split_Code());                                                                          //Natural: ASSIGN CV-SPLIT-CODE := AP-SPLIT-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Dana_Transaction_Cd().setValue(ldaAppl301.getPrap_Ap_Dana_Transaction_Cd());                                                        //Natural: ASSIGN CV-DANA-TRANSACTION-CD := AP-DANA-TRANSACTION-CD
        ldaAppl300.getCv_Prap_Rule_Cv_Dana_Stndrd_Rtn_Cd().setValue(ldaAppl301.getPrap_Ap_Dana_Stndrd_Rtn_Cd());                                                          //Natural: ASSIGN CV-DANA-STNDRD-RTN-CD := AP-DANA-STNDRD-RTN-CD
        ldaAppl300.getCv_Prap_Rule_Cv_Dana_Finalist_Reason_Cd().setValue(ldaAppl301.getPrap_Ap_Dana_Finalist_Reason_Cd());                                                //Natural: ASSIGN CV-DANA-FINALIST-REASON-CD := AP-DANA-FINALIST-REASON-CD
        ldaAppl300.getCv_Prap_Rule_Cv_Dana_Addr_Stndrd_Code().setValue(ldaAppl301.getPrap_Ap_Dana_Addr_Stndrd_Code());                                                    //Natural: ASSIGN CV-DANA-ADDR-STNDRD-CODE := AP-DANA-ADDR-STNDRD-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Dana_Stndrd_Overide().setValue(ldaAppl301.getPrap_Ap_Dana_Stndrd_Overide());                                                        //Natural: ASSIGN CV-DANA-STNDRD-OVERIDE := AP-DANA-STNDRD-OVERIDE
        ldaAppl300.getCv_Prap_Rule_Cv_Dana_Postal_Data_Fields().setValue(ldaAppl301.getPrap_Ap_Dana_Postal_Data_Fields());                                                //Natural: ASSIGN CV-DANA-POSTAL-DATA-FIELDS := AP-DANA-POSTAL-DATA-FIELDS
        ldaAppl300.getCv_Prap_Rule_Cv_Dana_Addr_Geographic_Code().setValue(ldaAppl301.getPrap_Ap_Dana_Addr_Geographic_Code());                                            //Natural: ASSIGN CV-DANA-ADDR-GEOGRAPHIC-CODE := AP-DANA-ADDR-GEOGRAPHIC-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Annuity_Start_Date().setValue(ldaAppl301.getPrap_Ap_Annuity_Start_Date());                                                          //Natural: ASSIGN CV-ANNUITY-START-DATE := AP-ANNUITY-START-DATE
        ldaAppl300.getCv_Prap_Rule_Cv_Max_Alloc_Pct().getValue("*").setValue(ldaAppl301.getPrap_Ap_Max_Alloc_Pct().getValue("*"));                                        //Natural: ASSIGN CV-MAX-ALLOC-PCT ( * ) := AP-MAX-ALLOC-PCT ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Max_Alloc_Pct_Sign().getValue("*").setValue(ldaAppl301.getPrap_Ap_Max_Alloc_Pct_Sign().getValue("*"));                              //Natural: ASSIGN CV-MAX-ALLOC-PCT-SIGN ( * ) := AP-MAX-ALLOC-PCT-SIGN ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Bene_Info_Type().setValue(ldaAppl301.getPrap_Ap_Bene_Info_Type());                                                                  //Natural: ASSIGN CV-BENE-INFO-TYPE := AP-BENE-INFO-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Primary_Std_Ent().setValue(ldaAppl301.getPrap_Ap_Primary_Std_Ent());                                                                //Natural: ASSIGN CV-PRIMARY-STD-ENT := AP-PRIMARY-STD-ENT
        ldaAppl300.getCv_Prap_Rule_Cv_Primary_Bene_Info().getValue("*").setValue(ldaAppl301.getPrap_Ap_Primary_Bene_Info().getValue("*"));                                //Natural: ASSIGN CV-PRIMARY-BENE-INFO ( * ) := AP-PRIMARY-BENE-INFO ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Contingent_Std_Ent().setValue(ldaAppl301.getPrap_Ap_Contingent_Std_Ent());                                                          //Natural: ASSIGN CV-CONTINGENT-STD-ENT := AP-CONTINGENT-STD-ENT
        ldaAppl300.getCv_Prap_Rule_Cv_Contingent_Bene_Info().getValue("*").setValue(ldaAppl301.getPrap_Ap_Contingent_Bene_Info().getValue("*"));                          //Natural: ASSIGN CV-CONTINGENT-BENE-INFO ( * ) := AP-CONTINGENT-BENE-INFO ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Bene_Estate().setValue(ldaAppl301.getPrap_Ap_Bene_Estate());                                                                        //Natural: ASSIGN CV-BENE-ESTATE := AP-BENE-ESTATE
        ldaAppl300.getCv_Prap_Rule_Cv_Bene_Trust().setValue(ldaAppl301.getPrap_Ap_Bene_Trust());                                                                          //Natural: ASSIGN CV-BENE-TRUST := AP-BENE-TRUST
        ldaAppl300.getCv_Prap_Rule_Cv_Bene_Category().setValue(ldaAppl301.getPrap_Ap_Bene_Category());                                                                    //Natural: ASSIGN CV-BENE-CATEGORY := AP-BENE-CATEGORY
        ldaAppl300.getCv_Prap_Rule_Cv_Mail_Instructions().setValue(ldaAppl301.getPrap_Ap_Mail_Instructions());                                                            //Natural: ASSIGN CV-MAIL-INSTRUCTIONS := AP-MAIL-INSTRUCTIONS
        ldaAppl300.getCv_Prap_Rule_Cv_Eop_Addl_Cref_Request().setValue(ldaAppl301.getPrap_Ap_Eop_Addl_Cref_Request());                                                    //Natural: ASSIGN CV-EOP-ADDL-CREF-REQUEST := AP-EOP-ADDL-CREF-REQUEST
        ldaAppl300.getCv_Prap_Rule_Cv_Process_Status().setValue(ldaAppl301.getPrap_Ap_Process_Status());                                                                  //Natural: ASSIGN CV-PROCESS-STATUS := AP-PROCESS-STATUS
        ldaAppl300.getCv_Prap_Rule_Cv_Coll_St_Cd().setValue(ldaAppl301.getPrap_Ap_Coll_St_Cd());                                                                          //Natural: ASSIGN CV-COLL-ST-CD := AP-COLL-ST-CD
        ldaAppl300.getCv_Prap_Rule_Cv_Csm_Sec_Seg().setValue(ldaAppl301.getPrap_Ap_Csm_Sec_Seg());                                                                        //Natural: ASSIGN CV-CSM-SEC-SEG := AP-CSM-SEC-SEG
        ldaAppl300.getCv_Prap_Rule_Cv_Rlc_College().setValue(ldaAppl301.getPrap_Ap_Rlc_College());                                                                        //Natural: ASSIGN CV-RLC-COLLEGE := AP-RLC-COLLEGE
        ldaAppl300.getCv_Prap_Rule_Cv_Rlc_Cref_Pref().setValue(ldaAppl301.getPrap_Ap_Rlc_Cref_Pref());                                                                    //Natural: ASSIGN CV-RLC-CREF-PREF := AP-RLC-CREF-PREF
        ldaAppl300.getCv_Prap_Rule_Cv_Rlc_Cref_Cont().setValue(ldaAppl301.getPrap_Ap_Rlc_Cref_Cont());                                                                    //Natural: ASSIGN CV-RLC-CREF-CONT := AP-RLC-CREF-CONT
        ldaAppl300.getCv_Prap_Rule_Cv_Cor_Prfx_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Prfx_Nme());                                                                      //Natural: ASSIGN CV-COR-PRFX-NME := AP-COR-PRFX-NME
        ldaAppl300.getCv_Prap_Rule_Cv_Cor_Last_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Last_Nme());                                                                      //Natural: ASSIGN CV-COR-LAST-NME := AP-COR-LAST-NME
        ldaAppl300.getCv_Prap_Rule_Cv_Cor_First_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_First_Nme());                                                                    //Natural: ASSIGN CV-COR-FIRST-NME := AP-COR-FIRST-NME
        ldaAppl300.getCv_Prap_Rule_Cv_Cor_Mddle_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Mddle_Nme());                                                                    //Natural: ASSIGN CV-COR-MDDLE-NME := AP-COR-MDDLE-NME
        ldaAppl300.getCv_Prap_Rule_Cv_Cor_Sffx_Nme().setValue(ldaAppl301.getPrap_Ap_Cor_Sffx_Nme());                                                                      //Natural: ASSIGN CV-COR-SFFX-NME := AP-COR-SFFX-NME
        ldaAppl300.getCv_Prap_Rule_Cv_Dob().setValue(ldaAppl301.getPrap_Ap_Dob());                                                                                        //Natural: ASSIGN CV-DOB := AP-DOB
        ldaAppl300.getCv_Prap_Rule_Cv_Dt_Ent_Sys().setValue(ldaAppl301.getPrap_Ap_Dt_Ent_Sys());                                                                          //Natural: ASSIGN CV-DT-ENT-SYS := AP-DT-ENT-SYS
        ldaAppl300.getCv_Prap_Rule_Cv_Dt_Released().setValue(ldaAppl301.getPrap_Ap_Dt_Released());                                                                        //Natural: ASSIGN CV-DT-RELEASED := AP-DT-RELEASED
        ldaAppl300.getCv_Prap_Rule_Cv_Dt_Matched().setValue(ldaAppl301.getPrap_Ap_Dt_Matched());                                                                          //Natural: ASSIGN CV-DT-MATCHED := AP-DT-MATCHED
        ldaAppl300.getCv_Prap_Rule_Cv_Dt_Deleted().setValue(ldaAppl301.getPrap_Ap_Dt_Deleted());                                                                          //Natural: ASSIGN CV-DT-DELETED := AP-DT-DELETED
        ldaAppl300.getCv_Prap_Rule_Cv_Dt_Withdrawn().setValue(ldaAppl301.getPrap_Ap_Dt_Withdrawn());                                                                      //Natural: ASSIGN CV-DT-WITHDRAWN := AP-DT-WITHDRAWN
        ldaAppl300.getCv_Prap_Rule_Cv_Dt_App_Recvd().setValue(ldaAppl301.getPrap_Ap_Dt_App_Recvd());                                                                      //Natural: ASSIGN CV-DT-APP-RECVD := AP-DT-APP-RECVD
        ldaAppl300.getCv_Prap_Rule_Cv_Address_Line().getValue("*").setValue(ldaAppl301.getPrap_Ap_Address_Line().getValue("*"));                                          //Natural: ASSIGN CV-ADDRESS-LINE ( * ) := AP-ADDRESS-LINE ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Pin_Nbr().setValue(ldaAppl301.getPrap_Ap_Pin_Nbr());                                                                                //Natural: ASSIGN CV-PIN-NBR := AP-PIN-NBR
        ldaAppl300.getCv_Prap_Rule_Cv_Ownership().setValue(ldaAppl301.getPrap_Ap_Ownership());                                                                            //Natural: ASSIGN CV-OWNERSHIP := AP-OWNERSHIP
        ldaAppl300.getCv_Prap_Rule_Cv_Ph_Hist_Ind().setValue(ldaAppl301.getPrap_Ap_Ph_Hist_Ind());                                                                        //Natural: ASSIGN CV-PH-HIST-IND := AP-PH-HIST-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Rcrd_Updt_Tm_Stamp().setValue(ldaAppl301.getPrap_Ap_Rcrd_Updt_Tm_Stamp());                                                          //Natural: ASSIGN CV-RCRD-UPDT-TM-STAMP := AP-RCRD-UPDT-TM-STAMP
        ldaAppl300.getCv_Prap_Rule_Cv_Contact_Mode().setValue(ldaAppl301.getPrap_Ap_Contact_Mode());                                                                      //Natural: ASSIGN CV-CONTACT-MODE := AP-CONTACT-MODE
        ldaAppl300.getCv_Prap_Rule_Cv_Racf_Id().setValue(ldaAppl301.getPrap_Ap_Racf_Id());                                                                                //Natural: ASSIGN CV-RACF-ID := AP-RACF-ID
        ldaAppl300.getCv_Prap_Rule_Cv_Rqst_Log_Dte_Time().getValue("*").setValue(ldaAppl301.getPrap_Ap_Rqst_Log_Dte_Time().getValue("*"));                                //Natural: ASSIGN CV-RQST-LOG-DTE-TIME ( * ) := AP-RQST-LOG-DTE-TIME ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Sync_Ind().setValue(ldaAppl301.getPrap_Ap_Sync_Ind());                                                                              //Natural: ASSIGN CV-SYNC-IND := AP-SYNC-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Cor_Ind().setValue(ldaAppl301.getPrap_Ap_Cor_Ind());                                                                                //Natural: ASSIGN CV-COR-IND := AP-COR-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Alloc_Ind().setValue(ldaAppl301.getPrap_Ap_Alloc_Ind());                                                                            //Natural: ASSIGN CV-ALLOC-IND := AP-ALLOC-IND
        ldaAppl300.getCv_Prap_Rule_Cv_City().setValue(ldaAppl301.getPrap_Ap_City());                                                                                      //Natural: ASSIGN CV-CITY := AP-CITY
        ldaAppl300.getCv_Prap_Rule_Cv_Tiaa_Doi().setValue(ldaAppl301.getPrap_Ap_Tiaa_Doi());                                                                              //Natural: ASSIGN CV-TIAA-DOI := AP-TIAA-DOI
        ldaAppl300.getCv_Prap_Rule_Cv_Cref_Doi().setValue(ldaAppl301.getPrap_Ap_Cref_Doi());                                                                              //Natural: ASSIGN CV-CREF-DOI := AP-CREF-DOI
        ldaAppl300.getCv_Prap_Rule_Cv_Mit_Unit().getValue("*").setValue(ldaAppl301.getPrap_Ap_Mit_Unit().getValue("*"));                                                  //Natural: ASSIGN CV-MIT-UNIT ( * ) := AP-MIT-UNIT ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Mit_Wpid().getValue("*").setValue(ldaAppl301.getPrap_Ap_Mit_Wpid().getValue("*"));                                                  //Natural: ASSIGN CV-MIT-WPID ( * ) := AP-MIT-WPID ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Ira_Rollover_Type().setValue(ldaAppl301.getPrap_Ap_Ira_Rollover_Type());                                                            //Natural: ASSIGN CV-IRA-ROLLOVER-TYPE := AP-IRA-ROLLOVER-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Ira_Record_Type().setValue(ldaAppl301.getPrap_Ap_Ira_Record_Type());                                                                //Natural: ASSIGN CV-IRA-RECORD-TYPE := AP-IRA-RECORD-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Mult_App_Status().setValue(ldaAppl301.getPrap_Ap_Mult_App_Status());                                                                //Natural: ASSIGN CV-MULT-APP-STATUS := AP-MULT-APP-STATUS
        ldaAppl300.getCv_Prap_Rule_Cv_Mult_App_Lob().setValue(ldaAppl301.getPrap_Ap_Mult_App_Lob());                                                                      //Natural: ASSIGN CV-MULT-APP-LOB := AP-MULT-APP-LOB
        ldaAppl300.getCv_Prap_Rule_Cv_Mult_App_Lob_Type().setValue(ldaAppl301.getPrap_Ap_Mult_App_Lob_Type());                                                            //Natural: ASSIGN CV-MULT-APP-LOB-TYPE := AP-MULT-APP-LOB-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Mult_App_Ppg().setValue(ldaAppl301.getPrap_Ap_Mult_App_Ppg());                                                                      //Natural: ASSIGN CV-MULT-APP-PPG := AP-MULT-APP-PPG
        ldaAppl300.getCv_Prap_Rule_Cv_Print_Date().setValue(ldaAppl301.getPrap_Ap_Print_Date());                                                                          //Natural: ASSIGN CV-PRINT-DATE := AP-PRINT-DATE
        ldaAppl300.getCv_Prap_Rule_Cv_Cntrct_Type().getValue("*").setValue(ldaAppl301.getPrap_Ap_Cntrct_Type().getValue("*"));                                            //Natural: ASSIGN CV-CNTRCT-TYPE ( * ) := AP-CNTRCT-TYPE ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Cntrct_Nbr().getValue("*").setValue(ldaAppl301.getPrap_Ap_Cntrct_Nbr().getValue("*"));                                              //Natural: ASSIGN CV-CNTRCT-NBR ( * ) := AP-CNTRCT-NBR ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Cntrct_Proceeds_Amt().getValue("*").setValue(ldaAppl301.getPrap_Ap_Cntrct_Proceeds_Amt().getValue("*"));                            //Natural: ASSIGN CV-CNTRCT-PROCEEDS-AMT ( * ) := AP-CNTRCT-PROCEEDS-AMT ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Address_Txt().getValue("*").setValue(ldaAppl301.getPrap_Ap_Address_Txt().getValue("*"));                                            //Natural: ASSIGN CV-ADDRESS-TXT ( * ) := AP-ADDRESS-TXT ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Address_Dest_Name().setValue(ldaAppl301.getPrap_Ap_Address_Dest_Name());                                                            //Natural: ASSIGN CV-ADDRESS-DEST-NAME := AP-ADDRESS-DEST-NAME
        ldaAppl300.getCv_Prap_Rule_Cv_Zip_Code().setValue(ldaAppl301.getPrap_Ap_Zip_Code());                                                                              //Natural: ASSIGN CV-ZIP-CODE := AP-ZIP-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Bank_Pymnt_Acct_Nmbr().setValue(ldaAppl301.getPrap_Ap_Bank_Pymnt_Acct_Nmbr());                                                      //Natural: ASSIGN CV-BANK-PYMNT-ACCT-NMBR := AP-BANK-PYMNT-ACCT-NMBR
        ldaAppl300.getCv_Prap_Rule_Cv_Bank_Aba_Acct_Nmbr().setValue(ldaAppl301.getPrap_Ap_Bank_Aba_Acct_Nmbr());                                                          //Natural: ASSIGN CV-BANK-ABA-ACCT-NMBR := AP-BANK-ABA-ACCT-NMBR
        ldaAppl300.getCv_Prap_Rule_Cv_Addr_Usage_Code().setValue(ldaAppl301.getPrap_Ap_Addr_Usage_Code());                                                                //Natural: ASSIGN CV-ADDR-USAGE-CODE := AP-ADDR-USAGE-CODE
        ldaAppl300.getCv_Prap_Rule_Cv_Init_Paymt_Amt().setValue(ldaAppl301.getPrap_Ap_Init_Paymt_Amt());                                                                  //Natural: ASSIGN CV-INIT-PAYMT-AMT := AP-INIT-PAYMT-AMT
        ldaAppl300.getCv_Prap_Rule_Cv_Init_Paymt_Pct().setValue(ldaAppl301.getPrap_Ap_Init_Paymt_Pct());                                                                  //Natural: ASSIGN CV-INIT-PAYMT-PCT := AP-INIT-PAYMT-PCT
        ldaAppl300.getCv_Prap_Rule_Cv_Financial_1().setValue(ldaAppl301.getPrap_Ap_Financial_1());                                                                        //Natural: ASSIGN CV-FINANCIAL-1 := AP-FINANCIAL-1
        ldaAppl300.getCv_Prap_Rule_Cv_Financial_2().setValue(ldaAppl301.getPrap_Ap_Financial_2());                                                                        //Natural: ASSIGN CV-FINANCIAL-2 := AP-FINANCIAL-2
        ldaAppl300.getCv_Prap_Rule_Cv_Financial_3().setValue(ldaAppl301.getPrap_Ap_Financial_3());                                                                        //Natural: ASSIGN CV-FINANCIAL-3 := AP-FINANCIAL-3
        ldaAppl300.getCv_Prap_Rule_Cv_Financial_4().setValue(ldaAppl301.getPrap_Ap_Financial_4());                                                                        //Natural: ASSIGN CV-FINANCIAL-4 := AP-FINANCIAL-4
        ldaAppl300.getCv_Prap_Rule_Cv_Financial_5().setValue(ldaAppl301.getPrap_Ap_Financial_5());                                                                        //Natural: ASSIGN CV-FINANCIAL-5 := AP-FINANCIAL-5
        ldaAppl300.getCv_Prap_Rule_Cv_Irc_Sectn_Grp_Cde().setValue(ldaAppl301.getPrap_Ap_Irc_Sectn_Grp_Cde());                                                            //Natural: ASSIGN CV-IRC-SECTN-GRP-CDE := AP-IRC-SECTN-GRP-CDE
        ldaAppl300.getCv_Prap_Rule_Cv_Irc_Sectn_Cde().setValue(ldaAppl301.getPrap_Ap_Irc_Sectn_Cde());                                                                    //Natural: ASSIGN CV-IRC-SECTN-CDE := AP-IRC-SECTN-CDE
        ldaAppl300.getCv_Prap_Rule_Cv_Inst_Link_Cde().setValue(ldaAppl301.getPrap_Ap_Inst_Link_Cde());                                                                    //Natural: ASSIGN CV-INST-LINK-CDE := AP-INST-LINK-CDE
        ldaAppl300.getCv_Prap_Rule_Cv_Applcnt_Req_Type().setValue(ldaAppl301.getPrap_Ap_Applcnt_Req_Type());                                                              //Natural: ASSIGN CV-APPLCNT-REQ-TYPE := AP-APPLCNT-REQ-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Addr_Sync_Ind().setValue(ldaAppl301.getPrap_Ap_Addr_Sync_Ind());                                                                    //Natural: ASSIGN CV-ADDR-SYNC-IND := AP-ADDR-SYNC-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Tiaa_Service_Agent().setValue(ldaAppl301.getPrap_Ap_Tiaa_Service_Agent());                                                          //Natural: ASSIGN CV-TIAA-SERVICE-AGENT := AP-TIAA-SERVICE-AGENT
        ldaAppl300.getCv_Prap_Rule_Cv_Prap_Rsch_Mit_Ind().setValue(ldaAppl301.getPrap_Ap_Prap_Rsch_Mit_Ind());                                                            //Natural: ASSIGN CV-PRAP-RSCH-MIT-IND := AP-PRAP-RSCH-MIT-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Ppg_Team_Cde().setValue(ldaAppl301.getPrap_Ap_Ppg_Team_Cde());                                                                      //Natural: ASSIGN CV-PPG-TEAM-CDE := AP-PPG-TEAM-CDE
        ldaAppl300.getCv_Prap_Rule_Cv_Tiaa_Cntrct().setValue(ldaAppl301.getPrap_Ap_Tiaa_Cntrct());                                                                        //Natural: ASSIGN CV-TIAA-CNTRCT := AP-TIAA-CNTRCT
        ldaAppl300.getCv_Prap_Rule_Cv_Cref_Cert().setValue(ldaAppl301.getPrap_Ap_Cref_Cert());                                                                            //Natural: ASSIGN CV-CREF-CERT := AP-CREF-CERT
        ldaAppl300.getCv_Prap_Rule_Cv_Rlc_Cref_Cert().setValue(ldaAppl301.getPrap_Ap_Rlc_Cref_Cert());                                                                    //Natural: ASSIGN CV-RLC-CREF-CERT := AP-RLC-CREF-CERT
        ldaAppl300.getCv_Prap_Rule_Cv_Allocation_Model_Type().setValue(ldaAppl301.getPrap_Ap_Allocation_Model_Type());                                                    //Natural: ASSIGN CV-ALLOCATION-MODEL-TYPE := AP-ALLOCATION-MODEL-TYPE
        ldaAppl300.getCv_Prap_Rule_Cv_Divorce_Ind().setValue(ldaAppl301.getPrap_Ap_Divorce_Ind());                                                                        //Natural: ASSIGN CV-DIVORCE-IND := AP-DIVORCE-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Email_Address().setValue(ldaAppl301.getPrap_Ap_Email_Address());                                                                    //Natural: ASSIGN CV-EMAIL-ADDRESS := AP-EMAIL-ADDRESS
        ldaAppl300.getCv_Prap_Rule_Cv_E_Signed_Appl_Ind().setValue(ldaAppl301.getPrap_Ap_E_Signed_Appl_Ind());                                                            //Natural: ASSIGN CV-E-SIGNED-APPL-IND := AP-E-SIGNED-APPL-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Eft_Requested_Ind().setValue(ldaAppl301.getPrap_Ap_Eft_Requested_Ind());                                                            //Natural: ASSIGN CV-EFT-REQUESTED-IND := AP-EFT-REQUESTED-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Prap_Prem_Rsch_Ind().setValue(ldaAppl301.getPrap_Ap_Prap_Prem_Rsch_Ind());                                                          //Natural: ASSIGN CV-PRAP-PREM-RSCH-IND := AP-PRAP-PREM-RSCH-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Address_Change_Ind().setValue(ldaAppl301.getPrap_Ap_Address_Change_Ind());                                                          //Natural: ASSIGN CV-ADDRESS-CHANGE-IND := AP-ADDRESS-CHANGE-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Allocation_Fmt().setValue(ldaAppl301.getPrap_Ap_Allocation_Fmt());                                                                  //Natural: ASSIGN CV-ALLOCATION-FMT := AP-ALLOCATION-FMT
        ldaAppl300.getCv_Prap_Rule_Cv_Phone_No().setValue(ldaAppl301.getPrap_Ap_Phone_No());                                                                              //Natural: ASSIGN CV-PHONE-NO := AP-PHONE-NO
        ldaAppl300.getCv_Prap_Rule_Cv_Fund_Cde().getValue("*").setValue(ldaAppl301.getPrap_Ap_Fund_Cde().getValue("*"));                                                  //Natural: ASSIGN CV-FUND-CDE ( * ) := AP-FUND-CDE ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Allocation_Pct().getValue("*").setValue(ldaAppl301.getPrap_Ap_Allocation_Pct().getValue("*"));                                      //Natural: ASSIGN CV-ALLOCATION-PCT ( * ) := AP-ALLOCATION-PCT ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Plan_No().setValue(ldaAppl301.getPrap_Ap_Sgrd_Plan_No());                                                                      //Natural: ASSIGN CV-SGRD-PLAN-NO := AP-SGRD-PLAN-NO
        ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Subplan_No().setValue(ldaAppl301.getPrap_Ap_Sgrd_Subplan_No());                                                                //Natural: ASSIGN CV-SGRD-SUBPLAN-NO := AP-SGRD-SUBPLAN-NO
        ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Part_Ext().setValue(ldaAppl301.getPrap_Ap_Sgrd_Part_Ext());                                                                    //Natural: ASSIGN CV-SGRD-PART-EXT := AP-SGRD-PART-EXT
        ldaAppl300.getCv_Prap_Rule_Cv_Sgrd_Divsub().setValue(ldaAppl301.getPrap_Ap_Sgrd_Divsub());                                                                        //Natural: ASSIGN CV-SGRD-DIVSUB := AP-SGRD-DIVSUB
        ldaAppl300.getCv_Prap_Rule_Cv_Text_Udf_1().setValue(ldaAppl301.getPrap_Ap_Text_Udf_1());                                                                          //Natural: ASSIGN CV-TEXT-UDF-1 := AP-TEXT-UDF-1
        ldaAppl300.getCv_Prap_Rule_Cv_Text_Udf_2().setValue(ldaAppl301.getPrap_Ap_Text_Udf_2());                                                                          //Natural: ASSIGN CV-TEXT-UDF-2 := AP-TEXT-UDF-2
        ldaAppl300.getCv_Prap_Rule_Cv_Text_Udf_3().setValue(ldaAppl301.getPrap_Ap_Text_Udf_3());                                                                          //Natural: ASSIGN CV-TEXT-UDF-3 := AP-TEXT-UDF-3
        ldaAppl300.getCv_Prap_Rule_Cv_Replacement_Ind().setValue(ldaAppl301.getPrap_Ap_Replacement_Ind());                                                                //Natural: ASSIGN CV-REPLACEMENT-IND := AP-REPLACEMENT-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Register_Id().setValue(ldaAppl301.getPrap_Ap_Register_Id());                                                                        //Natural: ASSIGN CV-REGISTER-ID := AP-REGISTER-ID
        ldaAppl300.getCv_Prap_Rule_Cv_Agent_Or_Racf_Id().setValue(ldaAppl301.getPrap_Ap_Agent_Or_Racf_Id());                                                              //Natural: ASSIGN CV-AGENT-OR-RACF-ID := AP-AGENT-OR-RACF-ID
        ldaAppl300.getCv_Prap_Rule_Cv_Exempt_Ind().setValue(ldaAppl301.getPrap_Ap_Exempt_Ind());                                                                          //Natural: ASSIGN CV-EXEMPT-IND := AP-EXEMPT-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Arr_Insurer_1().setValue(ldaAppl301.getPrap_Ap_Arr_Insurer_1());                                                                    //Natural: ASSIGN CV-ARR-INSURER-1 := AP-ARR-INSURER-1
        ldaAppl300.getCv_Prap_Rule_Cv_Arr_Insurer_2().setValue(ldaAppl301.getPrap_Ap_Arr_Insurer_2());                                                                    //Natural: ASSIGN CV-ARR-INSURER-2 := AP-ARR-INSURER-2
        ldaAppl300.getCv_Prap_Rule_Cv_Arr_Insurer_3().setValue(ldaAppl301.getPrap_Ap_Arr_Insurer_3());                                                                    //Natural: ASSIGN CV-ARR-INSURER-3 := AP-ARR-INSURER-3
        ldaAppl300.getCv_Prap_Rule_Cv_Arr_1035_Exch_Ind_1().setValue(ldaAppl301.getPrap_Ap_Arr_1035_Exch_Ind_1());                                                        //Natural: ASSIGN CV-ARR-1035-EXCH-IND-1 := AP-ARR-1035-EXCH-IND-1
        ldaAppl300.getCv_Prap_Rule_Cv_Arr_1035_Exch_Ind_2().setValue(ldaAppl301.getPrap_Ap_Arr_1035_Exch_Ind_2());                                                        //Natural: ASSIGN CV-ARR-1035-EXCH-IND-2 := AP-ARR-1035-EXCH-IND-2
        ldaAppl300.getCv_Prap_Rule_Cv_Arr_1035_Exch_Ind_3().setValue(ldaAppl301.getPrap_Ap_Arr_1035_Exch_Ind_3());                                                        //Natural: ASSIGN CV-ARR-1035-EXCH-IND-3 := AP-ARR-1035-EXCH-IND-3
        ldaAppl300.getCv_Prap_Rule_Cv_Autosave_Ind().setValue(ldaAppl301.getPrap_Ap_Autosave_Ind());                                                                      //Natural: ASSIGN CV-AUTOSAVE-IND := AP-AUTOSAVE-IND
        ldaAppl300.getCv_Prap_Rule_Cv_As_Cur_Dflt_Opt().setValue(ldaAppl301.getPrap_Ap_As_Cur_Dflt_Opt());                                                                //Natural: ASSIGN CV-AS-CUR-DFLT-OPT := AP-AS-CUR-DFLT-OPT
        ldaAppl300.getCv_Prap_Rule_Cv_As_Cur_Dflt_Amt().setValue(ldaAppl301.getPrap_Ap_As_Cur_Dflt_Amt());                                                                //Natural: ASSIGN CV-AS-CUR-DFLT-AMT := AP-AS-CUR-DFLT-AMT
        ldaAppl300.getCv_Prap_Rule_Cv_As_Incr_Opt().setValue(ldaAppl301.getPrap_Ap_As_Incr_Opt());                                                                        //Natural: ASSIGN CV-AS-INCR-OPT := AP-AS-INCR-OPT
        ldaAppl300.getCv_Prap_Rule_Cv_As_Incr_Amt().setValue(ldaAppl301.getPrap_Ap_As_Incr_Amt());                                                                        //Natural: ASSIGN CV-AS-INCR-AMT := AP-AS-INCR-AMT
        ldaAppl300.getCv_Prap_Rule_Cv_As_Max_Pct().setValue(ldaAppl301.getPrap_Ap_As_Max_Pct());                                                                          //Natural: ASSIGN CV-AS-MAX-PCT := AP-AS-MAX-PCT
        ldaAppl300.getCv_Prap_Rule_Cv_Ae_Opt_Out_Days().setValue(ldaAppl301.getPrap_Ap_Ae_Opt_Out_Days());                                                                //Natural: ASSIGN CV-AE-OPT-OUT-DAYS := AP-AE-OPT-OUT-DAYS
        ldaAppl300.getCv_Prap_Rule_Cv_Incmpl_Acct_Ind().setValue(ldaAppl301.getPrap_Ap_Incmpl_Acct_Ind());                                                                //Natural: ASSIGN CV-INCMPL-ACCT-IND := AP-INCMPL-ACCT-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Delete_User_Id().setValue(ldaAppl301.getPrap_Ap_Delete_User_Id());                                                                  //Natural: ASSIGN CV-DELETE-USER-ID := AP-DELETE-USER-ID
        ldaAppl300.getCv_Prap_Rule_Cv_Delete_Reason_Cd().setValue(ldaAppl301.getPrap_Ap_Delete_Reason_Cd());                                                              //Natural: ASSIGN CV-DELETE-REASON-CD := AP-DELETE-REASON-CD
        ldaAppl300.getCv_Prap_Rule_Cv_Consent_Email_Address().setValue(ldaAppl301.getPrap_Ap_Consent_Email_Address());                                                    //Natural: ASSIGN CV-CONSENT-EMAIL-ADDRESS := AP-CONSENT-EMAIL-ADDRESS
        ldaAppl300.getCv_Prap_Rule_Cv_Welc_E_Delivery_Ind().setValue(ldaAppl301.getPrap_Ap_Welc_E_Delivery_Ind());                                                        //Natural: ASSIGN CV-WELC-E-DELIVERY-IND := AP-WELC-E-DELIVERY-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Legal_E_Delivery_Ind().setValue(ldaAppl301.getPrap_Ap_Legal_E_Delivery_Ind());                                                      //Natural: ASSIGN CV-LEGAL-E-DELIVERY-IND := AP-LEGAL-E-DELIVERY-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Legal_Ann_Option().setValue(ldaAppl301.getPrap_Ap_Legal_Ann_Option());                                                              //Natural: ASSIGN CV-LEGAL-ANN-OPTION := AP-LEGAL-ANN-OPTION
        ldaAppl300.getCv_Prap_Rule_Cv_Orchestration_Id().setValue(ldaAppl301.getPrap_Ap_Orchestration_Id());                                                              //Natural: ASSIGN CV-ORCHESTRATION-ID := AP-ORCHESTRATION-ID
        ldaAppl300.getCv_Prap_Rule_Cv_Mail_Addr_Country_Cd().setValue(ldaAppl301.getPrap_Ap_Mail_Addr_Country_Cd());                                                      //Natural: ASSIGN CV-MAIL-ADDR-COUNTRY-CD := AP-MAIL-ADDR-COUNTRY-CD
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Startdate().setValue(ldaAppl301.getPrap_Ap_Tic_Startdate());                                                                    //Natural: ASSIGN CV-TIC-STARTDATE := AP-TIC-STARTDATE
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Enddate().setValue(ldaAppl301.getPrap_Ap_Tic_Enddate());                                                                        //Natural: ASSIGN CV-TIC-ENDDATE := AP-TIC-ENDDATE
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Percentage().setValue(ldaAppl301.getPrap_Ap_Tic_Percentage());                                                                  //Natural: ASSIGN CV-TIC-PERCENTAGE := AP-TIC-PERCENTAGE
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Postdays().setValue(ldaAppl301.getPrap_Ap_Tic_Postdays());                                                                      //Natural: ASSIGN CV-TIC-POSTDAYS := AP-TIC-POSTDAYS
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Limit().setValue(ldaAppl301.getPrap_Ap_Tic_Limit());                                                                            //Natural: ASSIGN CV-TIC-LIMIT := AP-TIC-LIMIT
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Postfreq().setValue(ldaAppl301.getPrap_Ap_Tic_Postfreq());                                                                      //Natural: ASSIGN CV-TIC-POSTFREQ := AP-TIC-POSTFREQ
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Pl_Level().setValue(ldaAppl301.getPrap_Ap_Tic_Pl_Level());                                                                      //Natural: ASSIGN CV-TIC-PL-LEVEL := AP-TIC-PL-LEVEL
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Windowdays().setValue(ldaAppl301.getPrap_Ap_Tic_Windowdays());                                                                  //Natural: ASSIGN CV-TIC-WINDOWDAYS := AP-TIC-WINDOWDAYS
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Reqdlywindow().setValue(ldaAppl301.getPrap_Ap_Tic_Reqdlywindow());                                                              //Natural: ASSIGN CV-TIC-REQDLYWINDOW := AP-TIC-REQDLYWINDOW
        ldaAppl300.getCv_Prap_Rule_Cv_Tic_Recap_Prov().setValue(ldaAppl301.getPrap_Ap_Tic_Recap_Prov());                                                                  //Natural: ASSIGN CV-TIC-RECAP-PROV := AP-TIC-RECAP-PROV
        ldaAppl300.getCv_Prap_Rule_Cv_Ann_Funding_Dt().setValue(ldaAppl301.getPrap_Ap_Ann_Funding_Dt());                                                                  //Natural: ASSIGN CV-ANN-FUNDING-DT := AP-ANN-FUNDING-DT
        ldaAppl300.getCv_Prap_Rule_Cv_Tiaa_Ann_Issue_Dt().setValue(ldaAppl301.getPrap_Ap_Tiaa_Ann_Issue_Dt());                                                            //Natural: ASSIGN CV-TIAA-ANN-ISSUE-DT := AP-TIAA-ANN-ISSUE-DT
        ldaAppl300.getCv_Prap_Rule_Cv_Cref_Ann_Issue_Dt().setValue(ldaAppl301.getPrap_Ap_Cref_Ann_Issue_Dt());                                                            //Natural: ASSIGN CV-CREF-ANN-ISSUE-DT := AP-CREF-ANN-ISSUE-DT
        ldaAppl300.getCv_Prap_Rule_Cv_Substitution_Contract_Ind().setValue(ldaAppl301.getPrap_Ap_Substitution_Contract_Ind());                                            //Natural: ASSIGN CV-SUBSTITUTION-CONTRACT-IND := AP-SUBSTITUTION-CONTRACT-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Conv_Issue_State().setValue(ldaAppl301.getPrap_Ap_Conv_Issue_State());                                                              //Natural: ASSIGN CV-CONV-ISSUE-STATE := AP-CONV-ISSUE-STATE
        ldaAppl300.getCv_Prap_Rule_Cv_Deceased_Ind().setValue(ldaAppl301.getPrap_Ap_Deceased_Ind());                                                                      //Natural: ASSIGN CV-DECEASED-IND := AP-DECEASED-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Non_Proprietary_Pkg_Ind().setValue(ldaAppl301.getPrap_Ap_Non_Proprietary_Pkg_Ind());                                                //Natural: ASSIGN CV-NON-PROPRIETARY-PKG-IND := AP-NON-PROPRIETARY-PKG-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Ap_Decedent_Contract().setValue(ldaAppl301.getPrap_Ap_Decedent_Contract());                                                         //Natural: ASSIGN CV-AP-DECEDENT-CONTRACT := AP-DECEDENT-CONTRACT
        ldaAppl300.getCv_Prap_Rule_Cv_Relation_To_Decedent().setValue(ldaAppl301.getPrap_Ap_Relation_To_Decedent());                                                      //Natural: ASSIGN CV-RELATION-TO-DECEDENT := AP-RELATION-TO-DECEDENT
        ldaAppl300.getCv_Prap_Rule_Cv_Ssn_Tin_Ind().setValue(ldaAppl301.getPrap_Ap_Ssn_Tin_Ind());                                                                        //Natural: ASSIGN CV-SSN-TIN-IND := AP-SSN-TIN-IND
        ldaAppl300.getCv_Prap_Rule_Cv_Oneira_Acct_No().setValue(ldaAppl301.getPrap_Ap_Oneira_Acct_No());                                                                  //Natural: ASSIGN CV-ONEIRA-ACCT-NO := AP-ONEIRA-ACCT-NO
        ldaAppl300.getCv_Prap_Rule_Cv_Fund_Source_Cde_1().setValue(ldaAppl301.getPrap_Ap_Fund_Source_Cde_1());                                                            //Natural: ASSIGN CV-FUND-SOURCE-CDE-1 := AP-FUND-SOURCE-CDE-1
        ldaAppl300.getCv_Prap_Rule_Cv_Fund_Source_Cde_2().setValue(ldaAppl301.getPrap_Ap_Fund_Source_Cde_2());                                                            //Natural: ASSIGN CV-FUND-SOURCE-CDE-2 := AP-FUND-SOURCE-CDE-2
        ldaAppl300.getCv_Prap_Rule_Cv_Fund_Cde_2().getValue("*").setValue(ldaAppl301.getPrap_Ap_Fund_Cde_2().getValue("*"));                                              //Natural: ASSIGN CV-FUND-CDE-2 ( * ) := AP-FUND-CDE-2 ( * )
        ldaAppl300.getCv_Prap_Rule_Cv_Allocation_Pct_2().getValue("*").setValue(ldaAppl301.getPrap_Ap_Allocation_Pct_2().getValue("*"));                                  //Natural: ASSIGN CV-ALLOCATION-PCT-2 ( * ) := AP-ALLOCATION-PCT-2 ( * )
        //*  OIA BE
        //*  OIA BE
        //*  OIA BE
        //*  OIA BE
        //*  OIA BE
        //*  OIA BE
        //*  ARR KG
        //*  TIC
        //*  IISG
        getWorkFiles().write(1, false, ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_1(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_2(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_3(),  //Natural: WRITE WORK FILE 1 CV-RULE-DATA-1 CV-RULE-DATA-2 CV-RULE-DATA-3 CV-RULE-DATA-4 CV-RULE-DATA-5 CV-RULE-DATA-6 CV-RULE-DATA-7 CV-RULE-DATA-8 CV-RULE-DATA-9 CV-RULE-DATA-10 CV-RULE-DATA-11 CV-RULE-DATA-12 CV-RULE-DATA-13 CV-RULE-DATA-14 CV-RULE-DATA-15 CV-RULE-DATA-16 CV-RULE-DATA-17 CV-RULE-DATA-18 CV-RULE-DATA-19
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_4(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_5(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_6(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_7(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_8(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_9(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_10(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_11(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_12(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_13(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_14(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_15(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_16(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_17(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_18(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_19());
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"Error:     ",Global.getERROR_NR());                 //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'Error:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR
}
