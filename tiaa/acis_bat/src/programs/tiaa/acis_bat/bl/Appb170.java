/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:26 PM
**        * FROM NATURAL PROGRAM : Appb170
************************************************************
**        * FILE NAME            : Appb170.java
**        * CLASS NAME           : Appb170
**        * INSTANCE NAME        : Appb170
************************************************************
************************************************************************
* PROGRAM : APPB170 - ACIS/MDM EXTRACT PROCESS
* FUNCTION: READS NEW CONTRACTS FROM PRAP FILE AND EXTRACTS THEM TO
*           FILE TO LOAD ON MDM
*
* CREATED : 10/08/09 C. MASON
* HISTORY : 02-08-10 B.HOLLOWAY - OASIS TO SIEBEL MIGRATION - PASS PHONE
*                       NUMBERS FOR UPDATE TO MDM
*           11-10-11 C.SCHNEIDER- CORRECT NDM EXTRACTION ERROR- REPLACE
*                    CLS01
*              DATE LOGIC WITH INTERROGATION OF AP-EFT-REQUESTED-IND
*                    - COMMENTED OUT CALL TO UPDATE-NEXT-EXTRACT-DATE
*                    - COMMENTED OUT READ TO WORK 3
*                    - UPDATE AP-EFT-REQUESTED-IND ON ANNTY-ACTVTY-PRAP
*           12-01-11 C.SCHNEIDER   (CLS02)
*                    - ADD TOTALS FOR MAYO CNTRCTS VIA CALL TO SCIN8540
*                    - ADDED CONTRACT # TO DISPLAY FOR EXTRACTED RECS
*           12-09-11 B. ELLO - RESIDENTIAL ADDRESS PROJECT
*                    EXTRACT RESIDENCE ADDR IN ANNTY-ACTVTY-PRAP FILE
*                    WRITE THE INFO.IN THE ACIS TO MDM INTERFACE USING
*                    NEW APPBL170 LAYOUT FOR UPDATE TO MDM (BE1.)
*           08-06-12 B. NEWSOM - PRODUCTION FIX
*                    FIX STATE AND ZIP GO TO MDM.           (PF1)
*           03-14-13 B. NEWSOM - DATASHARE PHASE 4          (DSP4)
*                    THIS SUBPROGRAM WHICH CALLS THE UPDATE MODULE
*                    FOR THE NEWLY ISSUED CONTRACT WILL BE CHANGED
*                    TO BYPASS THE LOGIC FOR THE MAYO PLAN.
*                    THE 3PRK ACCOUNTS ARE NOT GOING TO BE STORED IN THE
*                    FIDELITY ADABAS FILE.
*
*                    THE FOLLOWING ARE THE CHANGES * ADD A NEW COUNTER
*                    FOR DATASHARE 3PRK ACCOUNT:
*                    * #DS-3PRK-CNT             (P7)
*                    * ADD A NEW DISPLAY STATEMENT AS HIGHLIGHTED BELOW:
*                      WRITE #MAYO-CNT 'MAYO RECORDS ENROLLED'
*                      WRITE #DS-3PRK-CNT ?DATASHARE RECORDS ENROLLED?
*                    * UPDATE THE COUNTER FOR DATASHARE FIELD (#DS-3RPK)
*           07-18-13 C. LAEVEY - SKIP RECORD IF             (TNGSUB)
*                    AP-SUBSTITUTION-CONTRACT-IND NE ' '
*           08-15-15 L SHU - SOR OF RECORD APPLICATION    (SOR)
*                    PASS EMAIL ADDRESS TO MDM THRU ACIS DAILY CONTRACT
*                    FEED
*                    REPLACE COR CALL TO MDM TO GET ORIG CONTRACT ISSUE
*                    DATE                                     (CORMDM)
*           08-15-15 L SHU - SOR OF RECORD APPLICATION    (SOR)
*                    PASS EMAIL ADDRESS TO MDM THRU ACIS DAILY CONTRACT
*                    FEED
*                    REPLACE COR CALL TO MDM TO GET ORIG CONTRACT ISSUE
*                    DATE                                     (CORMDM)
*           09-01-15 B ELLO- SET NEW BENE FIELDS IN THE UPDATED MDM
*                    INTERFACE LAYOUT.                        (BIP)
*           03-17-17 L SHU - ADDED LOGIC NOT TO EXTRACT ONEIRA CONTRACT
*                                                             (ONEIRA)
* 06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
***********************************************FILE*********************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb170 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaAppbl170 ldaAppbl170;
    private LdaY2datebw ldaY2datebw;
    private LdaAcil1080 ldaAcil1080;
    private PdaMdma211 pdaMdma211;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_G_Key;
    private DbsField annty_Actvty_Prap_View_Ap_G_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Dob;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Bill_Code;

    private DbsGroup annty_Actvty_Prap_View_Ap_Tiaa_Contr;
    private DbsField annty_Actvty_Prap_View_App_Pref;
    private DbsField annty_Actvty_Prap_View_App_Cont;

    private DbsGroup annty_Actvty_Prap_View_Ap_Cref_Contr;
    private DbsField annty_Actvty_Prap_View_App_C_Pref;
    private DbsField annty_Actvty_Prap_View_App_C_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Curr;
    private DbsField annty_Actvty_Prap_View_Ap_T_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_C_Age_1st;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership;
    private DbsField annty_Actvty_Prap_View_Ap_Sex;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Zip;
    private DbsField annty_Actvty_Prap_View_Ap_Name_Addr_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Ent_Sys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Released;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Matched;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Deleted;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Withdrawn;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Discr;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Other_Pols;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Numb_Dailys;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_App_Recvd;

    private DbsGroup annty_Actvty_Prap_View_Ap_Allocation_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation;
    private DbsField annty_Actvty_Prap_View_Ap_App_Source;
    private DbsField annty_Actvty_Prap_View_Ap_Region_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Ownership_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Split_Code;

    private DbsGroup annty_Actvty_Prap_View_Ap_Address_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Line;
    private DbsField annty_Actvty_Prap_View_Ap_City;
    private DbsField annty_Actvty_Prap_View_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields;
    private DbsField annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Annuity_Start_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Info_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Primary_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Primary_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Std_Ent;

    private DbsGroup annty_Actvty_Prap_View_Ap_Bene_Contingent_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Contingent_Bene_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Estate;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Trust;
    private DbsField annty_Actvty_Prap_View_Ap_Bene_Category;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Instructions;
    private DbsField annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Process_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_St_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Csm_Sec_Seg;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_College;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Ph_Hist_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp;
    private DbsField annty_Actvty_Prap_View_Ap_Contact_Mode;
    private DbsField annty_Actvty_Prap_View_Ap_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_View_Ap_Sync_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Alloc_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Doi;

    private DbsGroup annty_Actvty_Prap_View_Ap_Mit_Request;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Unit;
    private DbsField annty_Actvty_Prap_View_Ap_Mit_Wpid;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Rollover_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Ira_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Mult_App_Ppg;
    private DbsField annty_Actvty_Prap_View_Ap_Print_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Cntrct_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt;

    private DbsGroup annty_Actvty_Prap_View_Ap_Addr_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Txt;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Dest_Name;
    private DbsField annty_Actvty_Prap_View_Ap_Zip_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;

    private DbsGroup annty_Actvty_Prap_View__R_Field_1;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Home_Tel;
    private DbsField annty_Actvty_Prap_View_Ap_Oia_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Erisa_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Cai_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Cip;
    private DbsField annty_Actvty_Prap_View_Ap_Same_Addr;
    private DbsField annty_Actvty_Prap_View_Ap_Omni_Account_Issuance;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Usage_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Init_Paymt_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_Init_Paymt_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_1;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_2;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_3;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_4;
    private DbsField annty_Actvty_Prap_View_Ap_Financial_5;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Inst_Link_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Applcnt_Req_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Addr_Sync_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent;
    private DbsField annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Ppg_Team_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Model_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Divorce_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Eft_Requested_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Prap_Prem_Rsch_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Change_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Fmt;
    private DbsField annty_Actvty_Prap_View_Ap_Phone_No;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Divsub;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_1;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_2;
    private DbsField annty_Actvty_Prap_View_Ap_Text_Udf_3;
    private DbsField annty_Actvty_Prap_View_Ap_Replacement_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Register_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Exempt_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Arr_Insurer_1;
    private DbsField annty_Actvty_Prap_View_Ap_Arr_Insurer_2;
    private DbsField annty_Actvty_Prap_View_Ap_Arr_Insurer_3;
    private DbsField annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_1;
    private DbsField annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_2;
    private DbsField annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_3;
    private DbsField annty_Actvty_Prap_View_Ap_Autosave_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Opt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Incr_Amt;
    private DbsField annty_Actvty_Prap_View_Ap_As_Max_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days;
    private DbsField annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Delete_User_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Delete_Reason_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Consent_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_Ann_Option;
    private DbsField annty_Actvty_Prap_View_Ap_Orchestration_Id;
    private DbsField annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Startdate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Enddate;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Percentage;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postdays;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Limit;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Postfreq;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Pl_Level;
    private DbsField annty_Actvty_Prap_View_Ap_Tic_Windowdays;
    private DbsField annty_Actvty_Prap_View_Ap_Decedent_Contract;
    private DbsField annty_Actvty_Prap_View_Ap_Relation_To_Decedent;
    private DbsField annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Oneira_Acct_No;
    private DbsField pnd_Ds_3prk_Cnt;
    private DbsField pnd_Write_Count;
    private DbsField logically_Deleted_Count;
    private DbsField pnd_Mayo_Cnt;
    private DbsField pnd_Field;

    private DbsGroup pnd_Field__R_Field_2;
    private DbsField pnd_Field_Pnd_N8;

    private DbsGroup pnd_Field__R_Field_3;
    private DbsField pnd_Field_Pnd_A8;
    private DbsField pnd_Comparison_Date_Time;

    private DbsGroup pnd_Comparison_Date_Time__R_Field_4;
    private DbsField pnd_Comparison_Date_Time_Pnd_Comparison_Date;
    private DbsField pnd_Comparison_Date_Time_Pnd_Blank;
    private DbsField pnd_Comparison_Date_Time_Pnd_Comparison_Time;

    private DbsGroup pnd_Comparison_Date_Time__R_Field_5;
    private DbsField pnd_Comparison_Date_Time_Pnd_Comparison_Datex;
    private DbsField pnd_Comparison_Date_Time_Pnd_Blankx;
    private DbsField pnd_Comparison_Date_Time_Pnd_Comparison_Timea;
    private DbsField pnd_File_Date;
    private DbsField pnd_Blank_Trigger;
    private DbsField pnd_Extract_Date;
    private DbsField pnd_Next_Extract_Date_Time;

    private DbsGroup pnd_Next_Extract_Date_Time__R_Field_6;
    private DbsField pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Date;
    private DbsField pnd_Next_Extract_Date_Time_Pnd_Blank;
    private DbsField pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Time;
    private DbsField pnd_Cor_Datea;
    private DbsField pnd_Cor_Date;
    private DbsField pnd_Cor_Time;
    private DbsField pnd_Extr_Date;
    private DbsField pnd_Extractable;
    private DbsField pnd_Total_Extracted;
    private DbsField pnd_D;
    private DbsField pnd_Hold_Isn;
    private DbsField pnd_Non_T_C_Administered_Plan;
    private DbsField pnd_State;
    private DbsField pnd_I;
    private DbsField pnd_W_State_Zip_Idx;

    private DbsGroup pnd_W_State_Zip_Idx__R_Field_7;
    private DbsField pnd_W_State_Zip_Idx_Pnd_W_State_Abrv;
    private DbsField pnd_W_State_Zip_Idx_Pnd_W_State_Code;

    private DbsGroup pnd_W_State_Zip_Idx__R_Field_8;
    private DbsField pnd_W_State_Zip_Idx_Pnd_W_State_Code_A;
    private DbsField pnd_W_State_Zip_Idx_Pnd_W_Zip_Code_Range_Start;
    private DbsField pnd_W_State_Zip_Idx_Pnd_W_Zip_Code_Range_End;
    private DbsField pnd_Orig_Cor_Issue_Date;
    private DbsField pnd_Hold_T_Doi;

    private DbsGroup pnd_Hold_T_Doi__R_Field_9;
    private DbsField pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Mm;
    private DbsField pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Yy;
    private DbsField pnd_Ap_T_Doi;

    private DbsGroup pnd_Ap_T_Doi__R_Field_10;
    private DbsField pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Cc;
    private DbsField pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Yy;
    private DbsField pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Mm;
    private DbsField pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Dd;

    private DbsGroup pnd_Ap_T_Doi__R_Field_11;
    private DbsField pnd_Ap_T_Doi_Pnd_Ap_T_Doi_A;
    private DbsField pnd_Hold_C_Doi;

    private DbsGroup pnd_Hold_C_Doi__R_Field_12;
    private DbsField pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Mm;
    private DbsField pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Yy;
    private DbsField pnd_Ap_C_Doi;

    private DbsGroup pnd_Ap_C_Doi__R_Field_13;
    private DbsField pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Cc;
    private DbsField pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Yy;
    private DbsField pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Mm;
    private DbsField pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Dd;

    private DbsGroup pnd_Ap_C_Doi__R_Field_14;
    private DbsField pnd_Ap_C_Doi_Pnd_Ap_C_Doi_A;
    private DbsField pnd_Datd;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_15;
    private DbsField pnd_Date_Pnd_Datn;

    private DbsGroup pnd_Date__R_Field_16;
    private DbsField pnd_Date_Pnd_Date_Ccyy;

    private DbsGroup pnd_Date__R_Field_17;
    private DbsField pnd_Date_Pnd_Date_Ccyy_N;
    private DbsField pnd_Rc;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaAppbl170 = new LdaAppbl170();
        registerRecord(ldaAppbl170);
        ldaY2datebw = new LdaY2datebw();
        registerRecord(ldaY2datebw);
        ldaAcil1080 = new LdaAcil1080();
        registerRecord(ldaAcil1080);
        localVariables = new DbsRecord();
        pdaMdma211 = new PdaMdma211(localVariables);

        // Local Variables

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_G_Key = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_G_KEY");
        annty_Actvty_Prap_View_Ap_G_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_G_IND");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Dob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_Bill_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bill_Code", "AP-BILL-CODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BILL_CODE");

        annty_Actvty_Prap_View_Ap_Tiaa_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_TIAA_CONTR", "AP-TIAA-CONTR");
        annty_Actvty_Prap_View_App_Pref = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Pref", "APP-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_PREF");
        annty_Actvty_Prap_View_App_Cont = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Cont", "APP-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_CONT");

        annty_Actvty_Prap_View_Ap_Cref_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_CREF_CONTR", "AP-CREF-CONTR");
        annty_Actvty_Prap_View_App_C_Pref = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Pref", "APP-C-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_C_PREF");
        annty_Actvty_Prap_View_App_C_Cont = annty_Actvty_Prap_View_Ap_Cref_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_C_Cont", "APP-C-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_C_CONT");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Curr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Curr", "AP-CURR", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_CURR");
        annty_Actvty_Prap_View_Ap_T_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Age_1st", "AP-T-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_T_AGE_1ST");
        annty_Actvty_Prap_View_Ap_C_Age_1st = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Age_1st", "AP-C-AGE-1ST", 
            FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_C_AGE_1ST");
        annty_Actvty_Prap_View_Ap_Ownership = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership", "AP-OWNERSHIP", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Sex = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sex", "AP-SEX", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_SEX");
        annty_Actvty_Prap_View_Ap_Sex.setDdmHeader("SEX CDE");
        annty_Actvty_Prap_View_Ap_Mail_Zip = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Zip", "AP-MAIL-ZIP", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        annty_Actvty_Prap_View_Ap_Name_Addr_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Name_Addr_Cd", "AP-NAME-ADDR-CD", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_NAME_ADDR_CD");
        annty_Actvty_Prap_View_Ap_Dt_Ent_Sys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_ENT_SYS");
        annty_Actvty_Prap_View_Ap_Dt_Released = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Released", "AP-DT-RELEASED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_RELEASED");
        annty_Actvty_Prap_View_Ap_Dt_Matched = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Matched", "AP-DT-MATCHED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_MATCHED");
        annty_Actvty_Prap_View_Ap_Dt_Deleted = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Deleted", "AP-DT-DELETED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_DELETED");
        annty_Actvty_Prap_View_Ap_Dt_Withdrawn = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Withdrawn", "AP-DT-WITHDRAWN", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_WITHDRAWN");
        annty_Actvty_Prap_View_Ap_Alloc_Discr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Discr", "AP-ALLOC-DISCR", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_ALLOC_DISCR");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Type", "AP-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_TYPE");
        annty_Actvty_Prap_View_Ap_Other_Pols = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Other_Pols", "AP-OTHER-POLS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OTHER_POLS");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Numb_Dailys = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Numb_Dailys", "AP-NUMB-DAILYS", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_NUMB_DAILYS");
        annty_Actvty_Prap_View_Ap_Dt_App_Recvd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_App_Recvd", "AP-DT-APP-RECVD", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_APP_RECVD");

        annty_Actvty_Prap_View_Ap_Allocation_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Allocation_Info", 
            "AP-ALLOCATION-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ALLOCATION_INFO");
        annty_Actvty_Prap_View_Ap_Allocation = annty_Actvty_Prap_View_Ap_Allocation_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Allocation", 
            "AP-ALLOCATION", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION", 
            "ACIS_ANNTY_PRAP_AP_ALLOCATION_INFO");
        annty_Actvty_Prap_View_Ap_App_Source = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_App_Source", "AP-APP-SOURCE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APP_SOURCE");
        annty_Actvty_Prap_View_Ap_App_Source.setDdmHeader("SOURCE");
        annty_Actvty_Prap_View_Ap_Region_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Region_Code", "AP-REGION-CODE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_REGION_CODE");
        annty_Actvty_Prap_View_Ap_Orig_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orig_Issue_State", 
            "AP-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Ownership_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ownership_Type", "AP-OWNERSHIP-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_OWNERSHIP_TYPE");
        annty_Actvty_Prap_View_Ap_Ownership_Type.setDdmHeader("CNTRCT OWNERSHIP");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_View_Ap_Split_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Split_Code", "AP-SPLIT-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SPLIT_CODE");

        annty_Actvty_Prap_View_Ap_Address_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Address_Info", "AP-ADDRESS-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_Address_Line = annty_Actvty_Prap_View_Ap_Address_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Address_Line", 
            "AP-ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_City = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_City", "AP-CITY", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "AP_CITY");
        annty_Actvty_Prap_View_Ap_Current_State_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Current_State_Code", 
            "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Transaction_Cd", 
            "AP-DANA-TRANSACTION-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_TRANSACTION_CD");
        annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Stndrd_Rtn_Cd", 
            "AP-DANA-STNDRD-RTN-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_RTN_CD");
        annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Finalist_Reason_Cd", 
            "AP-DANA-FINALIST-REASON-CD", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DANA_FINALIST_REASON_CD");
        annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Addr_Stndrd_Code", 
            "AP-DANA-ADDR-STNDRD-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_ADDR_STNDRD_CODE");
        annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Stndrd_Overide", 
            "AP-DANA-STNDRD-OVERIDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DANA_STNDRD_OVERIDE");
        annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Postal_Data_Fields", 
            "AP-DANA-POSTAL-DATA-FIELDS", FieldType.STRING, 44, RepeatingFieldStrategy.None, "AP_DANA_POSTAL_DATA_FIELDS");
        annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dana_Addr_Geographic_Code", 
            "AP-DANA-ADDR-GEOGRAPHIC-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_DANA_ADDR_GEOGRAPHIC_CODE");
        annty_Actvty_Prap_View_Ap_Annuity_Start_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Annuity_Start_Date", 
            "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");

        annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct", 
            "AP-MAXIMUM-ALLOC-PCT", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct", 
            "AP-MAX-ALLOC-PCT", FieldType.STRING, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", 
            "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign", 
            "AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 1, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", 
            "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Bene_Info_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Info_Type", "AP-BENE-INFO-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_INFO_TYPE");
        annty_Actvty_Prap_View_Ap_Primary_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Primary_Std_Ent", 
            "AP-PRIMARY-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_PRIMARY_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Primary_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Bene_Primary_Info", 
            "AP-BENE-PRIMARY-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Primary_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Primary_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Primary_Bene_Info", 
            "AP-PRIMARY-BENE-INFO", FieldType.STRING, 72, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_PRIMARY_BENE_INFO", 
            "ACIS_ANNTY_PRAP_AP_BENE_PRIMARY_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Std_Ent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contingent_Std_Ent", 
            "AP-CONTINGENT-STD-ENT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_CONTINGENT_STD_ENT");

        annty_Actvty_Prap_View_Ap_Bene_Contingent_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Bene_Contingent_Info", 
            "AP-BENE-CONTINGENT-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Contingent_Bene_Info = annty_Actvty_Prap_View_Ap_Bene_Contingent_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Contingent_Bene_Info", 
            "AP-CONTINGENT-BENE-INFO", FieldType.STRING, 72, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CONTINGENT_BENE_INFO", 
            "ACIS_ANNTY_PRAP_AP_BENE_CONTINGENT_INFO");
        annty_Actvty_Prap_View_Ap_Bene_Estate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Estate", "AP-BENE-ESTATE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_ESTATE");
        annty_Actvty_Prap_View_Ap_Bene_Trust = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Trust", "AP-BENE-TRUST", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_TRUST");
        annty_Actvty_Prap_View_Ap_Bene_Category = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bene_Category", "AP-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_BENE_CATEGORY");
        annty_Actvty_Prap_View_Ap_Mail_Instructions = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Instructions", 
            "AP-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MAIL_INSTRUCTIONS");
        annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eop_Addl_Cref_Request", 
            "AP-EOP-ADDL-CREF-REQUEST", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EOP_ADDL_CREF_REQUEST");
        annty_Actvty_Prap_View_Ap_Process_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Process_Status", "AP-PROCESS-STATUS", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_PROCESS_STATUS");
        annty_Actvty_Prap_View_Ap_Coll_St_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_St_Cd", "AP-COLL-ST-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        annty_Actvty_Prap_View_Ap_Csm_Sec_Seg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Csm_Sec_Seg", "AP-CSM-SEC-SEG", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_CSM_SEC_SEG");
        annty_Actvty_Prap_View_Ap_Rlc_College = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_College", "AP-RLC-COLLEGE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_RLC_COLLEGE");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Pref", "AP-RLC-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_RLC_CREF_PREF");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cont", "AP-RLC-CREF-CONT", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "AP_RLC_CREF_CONT");
        annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        annty_Actvty_Prap_View_Ap_Ph_Hist_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ph_Hist_Ind", "AP-PH-HIST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PH_HIST_IND");
        annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rcrd_Updt_Tm_Stamp", 
            "AP-RCRD-UPDT-TM-STAMP", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_RCRD_UPDT_TM_STAMP");
        annty_Actvty_Prap_View_Ap_Contact_Mode = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contact_Mode", "AP-CONTACT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_CONTACT_MODE");
        annty_Actvty_Prap_View_Ap_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", 
            "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Sync_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sync_Ind", "AP-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SYNC_IND");
        annty_Actvty_Prap_View_Ap_Cor_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_COR_IND");
        annty_Actvty_Prap_View_Ap_Alloc_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Alloc_Ind", "AP-ALLOC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOC_IND");
        annty_Actvty_Prap_View_Ap_Tiaa_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Doi", "AP-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_TIAA_DOI");
        annty_Actvty_Prap_View_Ap_Cref_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Doi", "AP-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_CREF_DOI");

        annty_Actvty_Prap_View_Ap_Mit_Request = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Mit_Request", "AP-MIT-REQUEST", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Unit = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Mit_Unit", "AP-MIT-UNIT", 
            FieldType.STRING, 8, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Mit_Wpid = annty_Actvty_Prap_View_Ap_Mit_Request.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Mit_Wpid", "AP-MIT-WPID", 
            FieldType.STRING, 6, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_WPID", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        annty_Actvty_Prap_View_Ap_Ira_Rollover_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Rollover_Type", 
            "AP-IRA-ROLLOVER-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_ROLLOVER_TYPE");
        annty_Actvty_Prap_View_Ap_Ira_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ira_Record_Type", 
            "AP-IRA-RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_IRA_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Status", 
            "AP-MULT-APP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_STATUS");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob", "AP-MULT-APP-LOB", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB");
        annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Lob_Type", 
            "AP-MULT-APP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_MULT_APP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Mult_App_Ppg = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mult_App_Ppg", "AP-MULT-APP-PPG", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_MULT_APP_PPG");
        annty_Actvty_Prap_View_Ap_Print_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Print_Date", "AP-PRINT-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "AP_PRINT_DATE");

        annty_Actvty_Prap_View_Ap_Cntrct_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Info", "AP-CNTRCT-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Type = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Type", "AP-CNTRCT-TYPE", 
            FieldType.STRING, 1, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_TYPE", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Nbr = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Nbr", "AP-CNTRCT-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_NBR", "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");
        annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt = annty_Actvty_Prap_View_Ap_Cntrct_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Cntrct_Proceeds_Amt", 
            "AP-CNTRCT-PROCEEDS-AMT", FieldType.NUMERIC, 10, 2, new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_CNTRCT_PROCEEDS_AMT", 
            "ACIS_ANNTY_PRAP_AP_CNTRCT_INFO");

        annty_Actvty_Prap_View_Ap_Addr_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Addr_Info", "AP-ADDR-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Txt = annty_Actvty_Prap_View_Ap_Addr_Info.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Address_Txt", "AP-ADDRESS-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Dest_Name = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Dest_Name", 
            "AP-ADDRESS-DEST-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "AP_ADDRESS_DEST_NAME");
        annty_Actvty_Prap_View_Ap_Zip_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Zip_Code", "AP-ZIP-CODE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");

        annty_Actvty_Prap_View__R_Field_1 = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View__R_Field_1", "REDEFINE", annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr);
        annty_Actvty_Prap_View_Ap_Bank_Home_Tel = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Home_Tel", "AP-BANK-HOME-TEL", 
            FieldType.STRING, 14);
        annty_Actvty_Prap_View_Ap_Oia_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Ap_Oia_Ind", "AP-OIA-IND", FieldType.STRING, 
            2);
        annty_Actvty_Prap_View_Ap_Erisa_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Ap_Erisa_Ind", "AP-ERISA-IND", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Cai_Ind = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cai_Ind", "AP-CAI-IND", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Ap_Cip = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Ap_Cip", "AP-CIP", FieldType.STRING, 
            1);
        annty_Actvty_Prap_View_Ap_Same_Addr = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Ap_Same_Addr", "AP-SAME-ADDR", 
            FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Omni_Account_Issuance = annty_Actvty_Prap_View__R_Field_1.newFieldInGroup("annty_Actvty_Prap_View_Ap_Omni_Account_Issuance", 
            "AP-OMNI-ACCOUNT-ISSUANCE", FieldType.STRING, 1);
        annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Aba_Acct_Nmbr", 
            "AP-BANK-ABA-ACCT-NMBR", FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_BANK_ABA_ACCT_NMBR");
        annty_Actvty_Prap_View_Ap_Addr_Usage_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Usage_Code", 
            "AP-ADDR-USAGE-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_USAGE_CODE");
        annty_Actvty_Prap_View_Ap_Init_Paymt_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Init_Paymt_Amt", "AP-INIT-PAYMT-AMT", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_AMT");
        annty_Actvty_Prap_View_Ap_Init_Paymt_Pct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Init_Paymt_Pct", "AP-INIT-PAYMT-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_INIT_PAYMT_PCT");
        annty_Actvty_Prap_View_Ap_Financial_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_1", "AP-FINANCIAL-1", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_1");
        annty_Actvty_Prap_View_Ap_Financial_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_2", "AP-FINANCIAL-2", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_2");
        annty_Actvty_Prap_View_Ap_Financial_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_3", "AP-FINANCIAL-3", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_3");
        annty_Actvty_Prap_View_Ap_Financial_4 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_4", "AP-FINANCIAL-4", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_4");
        annty_Actvty_Prap_View_Ap_Financial_5 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Financial_5", "AP-FINANCIAL-5", 
            FieldType.NUMERIC, 10, 2, RepeatingFieldStrategy.None, "AP_FINANCIAL_5");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Grp_Cde", 
            "AP-IRC-SECTN-GRP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_GRP_CDE");
        annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_CDE");
        annty_Actvty_Prap_View_Ap_Inst_Link_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Inst_Link_Cde", "AP-INST-LINK-CDE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "AP_INST_LINK_CDE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Applcnt_Req_Type", 
            "AP-APPLCNT-REQ-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_APPLCNT_REQ_TYPE");
        annty_Actvty_Prap_View_Ap_Applcnt_Req_Type.setDdmHeader("APP REQ TYPE");
        annty_Actvty_Prap_View_Ap_Addr_Sync_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Addr_Sync_Ind", "AP-ADDR-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDR_SYNC_IND");
        annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Service_Agent", 
            "AP-TIAA-SERVICE-AGENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIAA_SERVICE_AGENT");
        annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Prap_Rsch_Mit_Ind", 
            "AP-PRAP-RSCH-MIT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PRAP_RSCH_MIT_IND");
        annty_Actvty_Prap_View_Ap_Ppg_Team_Cde = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ppg_Team_Cde", "AP-PPG-TEAM-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_PPG_TEAM_CDE");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Rlc_Cref_Cert", "AP-RLC-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_RLC_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Allocation_Model_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Model_Type", 
            "AP-ALLOCATION-MODEL-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ALLOCATION_MODEL_TYPE");
        annty_Actvty_Prap_View_Ap_Divorce_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Divorce_Ind", "AP-DIVORCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DIVORCE_IND");
        annty_Actvty_Prap_View_Ap_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_E_Signed_Appl_Ind", 
            "AP-E-SIGNED-APPL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_E_SIGNED_APPL_IND");
        annty_Actvty_Prap_View_Ap_Eft_Requested_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Eft_Requested_Ind", 
            "AP-EFT-REQUESTED-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EFT_REQUESTED_IND");
        annty_Actvty_Prap_View_Ap_Prap_Prem_Rsch_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Prap_Prem_Rsch_Ind", 
            "AP-PRAP-PREM-RSCH-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_PRAP_PREM_RSCH_IND");
        annty_Actvty_Prap_View_Ap_Address_Change_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Change_Ind", 
            "AP-ADDRESS-CHANGE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ADDRESS_CHANGE_IND");
        annty_Actvty_Prap_View_Ap_Allocation_Fmt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Fmt", "AP-ALLOCATION-FMT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ALLOCATION_FMT");
        annty_Actvty_Prap_View_Ap_Phone_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Phone_No", "AP-PHONE-NO", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "AP_PHONE_NO");

        annty_Actvty_Prap_View_Ap_Fund_Identifier = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier", 
            "AP-FUND-IDENTIFIER", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Fund_Cde = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde", "AP-FUND-CDE", 
            FieldType.STRING, 10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Allocation_Pct = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct", 
            "AP-ALLOCATION-PCT", FieldType.NUMERIC, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", 
            "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        annty_Actvty_Prap_View_Ap_Sgrd_Divsub = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_SGRD_DIVSUB");
        annty_Actvty_Prap_View_Ap_Text_Udf_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_1", "AP-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_1");
        annty_Actvty_Prap_View_Ap_Text_Udf_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_2", "AP-TEXT-UDF-2", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_2");
        annty_Actvty_Prap_View_Ap_Text_Udf_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Text_Udf_3", "AP-TEXT-UDF-3", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TEXT_UDF_3");
        annty_Actvty_Prap_View_Ap_Replacement_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Replacement_Ind", 
            "AP-REPLACEMENT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_REPLACEMENT_IND");
        annty_Actvty_Prap_View_Ap_Register_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Register_Id", "AP-REGISTER-ID", 
            FieldType.STRING, 11, RepeatingFieldStrategy.None, "AP_REGISTER_ID");
        annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Agent_Or_Racf_Id", 
            "AP-AGENT-OR-RACF-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_AGENT_OR_RACF_ID");
        annty_Actvty_Prap_View_Ap_Exempt_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Exempt_Ind", "AP-EXEMPT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_EXEMPT_IND");
        annty_Actvty_Prap_View_Ap_Arr_Insurer_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Arr_Insurer_1", "AP-ARR-INSURER-1", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_ARR_INSURER_1");
        annty_Actvty_Prap_View_Ap_Arr_Insurer_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Arr_Insurer_2", "AP-ARR-INSURER-2", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_ARR_INSURER_2");
        annty_Actvty_Prap_View_Ap_Arr_Insurer_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Arr_Insurer_3", "AP-ARR-INSURER-3", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_ARR_INSURER_3");
        annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_1 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_1", 
            "AP-ARR-1035-EXCH-IND-1", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_1");
        annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_2 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_2", 
            "AP-ARR-1035-EXCH-IND-2", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_2");
        annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_3 = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Arr_1035_Exch_Ind_3", 
            "AP-ARR-1035-EXCH-IND-3", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_3");
        annty_Actvty_Prap_View_Ap_Autosave_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Autosave_Ind", "AP-AUTOSAVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AUTOSAVE_IND");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Opt", 
            "AP-AS-CUR-DFLT-OPT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_OPT");
        annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Cur_Dflt_Amt", 
            "AP-AS-CUR-DFLT-AMT", FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_CUR_DFLT_AMT");
        annty_Actvty_Prap_View_Ap_As_Incr_Opt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Opt", "AP-AS-INCR-OPT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_AS_INCR_OPT");
        annty_Actvty_Prap_View_Ap_As_Incr_Amt = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Incr_Amt", "AP-AS-INCR-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "AP_AS_INCR_AMT");
        annty_Actvty_Prap_View_Ap_As_Max_Pct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_As_Max_Pct", "AP-AS-MAX-PCT", 
            FieldType.NUMERIC, 7, 2, RepeatingFieldStrategy.None, "AP_AS_MAX_PCT");
        annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ae_Opt_Out_Days", 
            "AP-AE-OPT-OUT-DAYS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_AE_OPT_OUT_DAYS");
        annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Incmpl_Acct_Ind", 
            "AP-INCMPL-ACCT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_INCMPL_ACCT_IND");
        annty_Actvty_Prap_View_Ap_Delete_User_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Delete_User_Id", "AP-DELETE-USER-ID", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "AP_DELETE_USER_ID");
        annty_Actvty_Prap_View_Ap_Delete_Reason_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Delete_Reason_Cd", 
            "AP-DELETE-REASON-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_DELETE_REASON_CD");
        annty_Actvty_Prap_View_Ap_Consent_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Consent_Email_Address", 
            "AP-CONSENT-EMAIL-ADDRESS", FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_CONSENT_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind", 
            "AP-WELC-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_WELC_E_DELIVERY_IND");
        annty_Actvty_Prap_View_Ap_Welc_E_Delivery_Ind.setDdmHeader("WELC EDLVRY IND");
        annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind", 
            "AP-LEGAL-E-DELIVERY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_E_DELIVERY_IND");
        annty_Actvty_Prap_View_Ap_Legal_E_Delivery_Ind.setDdmHeader("LGL EDLVRY IND");
        annty_Actvty_Prap_View_Ap_Legal_Ann_Option = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_Ann_Option", 
            "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        annty_Actvty_Prap_View_Ap_Orchestration_Id = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orchestration_Id", 
            "AP-ORCHESTRATION-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_ORCHESTRATION_ID");
        annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind", 
            "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd", 
            "AP-MAIL-ADDR-COUNTRY-CD", FieldType.STRING, 3, RepeatingFieldStrategy.None, "AP_MAIL_ADDR_COUNTRY_CD");
        annty_Actvty_Prap_View_Ap_Tic_Startdate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Startdate", "AP-TIC-STARTDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_STARTDATE");
        annty_Actvty_Prap_View_Ap_Tic_Enddate = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Enddate", "AP-TIC-ENDDATE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "AP_TIC_ENDDATE");
        annty_Actvty_Prap_View_Ap_Tic_Percentage = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Percentage", "AP-TIC-PERCENTAGE", 
            FieldType.NUMERIC, 15, 6, RepeatingFieldStrategy.None, "AP_TIC_PERCENTAGE");
        annty_Actvty_Prap_View_Ap_Tic_Postdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postdays", "AP-TIC-POSTDAYS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_TIC_POSTDAYS");
        annty_Actvty_Prap_View_Ap_Tic_Limit = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Limit", "AP-TIC-LIMIT", 
            FieldType.NUMERIC, 13, 2, RepeatingFieldStrategy.None, "AP_TIC_LIMIT");
        annty_Actvty_Prap_View_Ap_Tic_Postfreq = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Postfreq", "AP-TIC-POSTFREQ", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_POSTFREQ");
        annty_Actvty_Prap_View_Ap_Tic_Pl_Level = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Pl_Level", "AP-TIC-PL-LEVEL", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_TIC_PL_LEVEL");
        annty_Actvty_Prap_View_Ap_Tic_Windowdays = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tic_Windowdays", "AP-TIC-WINDOWDAYS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "AP_TIC_WINDOWDAYS");
        annty_Actvty_Prap_View_Ap_Decedent_Contract = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Decedent_Contract", 
            "AP-DECEDENT-CONTRACT", FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DECEDENT_CONTRACT");
        annty_Actvty_Prap_View_Ap_Relation_To_Decedent = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Relation_To_Decedent", 
            "AP-RELATION-TO-DECEDENT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_RELATION_TO_DECEDENT");
        annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind", "AP-SSN-TIN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SSN_TIN_IND");
        annty_Actvty_Prap_View_Ap_Oneira_Acct_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Oneira_Acct_No", "AP-ONEIRA-ACCT-NO", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_ONEIRA_ACCT_NO");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Ds_3prk_Cnt = localVariables.newFieldInRecord("pnd_Ds_3prk_Cnt", "#DS-3PRK-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.PACKED_DECIMAL, 7);
        logically_Deleted_Count = localVariables.newFieldInRecord("logically_Deleted_Count", "LOGICALLY-DELETED-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Mayo_Cnt = localVariables.newFieldInRecord("pnd_Mayo_Cnt", "#MAYO-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Field = localVariables.newFieldInRecord("pnd_Field", "#FIELD", FieldType.STRING, 8);

        pnd_Field__R_Field_2 = localVariables.newGroupInRecord("pnd_Field__R_Field_2", "REDEFINE", pnd_Field);
        pnd_Field_Pnd_N8 = pnd_Field__R_Field_2.newFieldInGroup("pnd_Field_Pnd_N8", "#N8", FieldType.NUMERIC, 8);

        pnd_Field__R_Field_3 = pnd_Field__R_Field_2.newGroupInGroup("pnd_Field__R_Field_3", "REDEFINE", pnd_Field_Pnd_N8);
        pnd_Field_Pnd_A8 = pnd_Field__R_Field_3.newFieldInGroup("pnd_Field_Pnd_A8", "#A8", FieldType.STRING, 8);
        pnd_Comparison_Date_Time = localVariables.newFieldInRecord("pnd_Comparison_Date_Time", "#COMPARISON-DATE-TIME", FieldType.STRING, 16);

        pnd_Comparison_Date_Time__R_Field_4 = localVariables.newGroupInRecord("pnd_Comparison_Date_Time__R_Field_4", "REDEFINE", pnd_Comparison_Date_Time);
        pnd_Comparison_Date_Time_Pnd_Comparison_Date = pnd_Comparison_Date_Time__R_Field_4.newFieldInGroup("pnd_Comparison_Date_Time_Pnd_Comparison_Date", 
            "#COMPARISON-DATE", FieldType.STRING, 8);
        pnd_Comparison_Date_Time_Pnd_Blank = pnd_Comparison_Date_Time__R_Field_4.newFieldInGroup("pnd_Comparison_Date_Time_Pnd_Blank", "#BLANK", FieldType.STRING, 
            1);
        pnd_Comparison_Date_Time_Pnd_Comparison_Time = pnd_Comparison_Date_Time__R_Field_4.newFieldInGroup("pnd_Comparison_Date_Time_Pnd_Comparison_Time", 
            "#COMPARISON-TIME", FieldType.NUMERIC, 7);

        pnd_Comparison_Date_Time__R_Field_5 = localVariables.newGroupInRecord("pnd_Comparison_Date_Time__R_Field_5", "REDEFINE", pnd_Comparison_Date_Time);
        pnd_Comparison_Date_Time_Pnd_Comparison_Datex = pnd_Comparison_Date_Time__R_Field_5.newFieldInGroup("pnd_Comparison_Date_Time_Pnd_Comparison_Datex", 
            "#COMPARISON-DATEX", FieldType.STRING, 8);
        pnd_Comparison_Date_Time_Pnd_Blankx = pnd_Comparison_Date_Time__R_Field_5.newFieldInGroup("pnd_Comparison_Date_Time_Pnd_Blankx", "#BLANKX", FieldType.STRING, 
            1);
        pnd_Comparison_Date_Time_Pnd_Comparison_Timea = pnd_Comparison_Date_Time__R_Field_5.newFieldInGroup("pnd_Comparison_Date_Time_Pnd_Comparison_Timea", 
            "#COMPARISON-TIMEA", FieldType.STRING, 7);
        pnd_File_Date = localVariables.newFieldInRecord("pnd_File_Date", "#FILE-DATE", FieldType.DATE);
        pnd_Blank_Trigger = localVariables.newFieldInRecord("pnd_Blank_Trigger", "#BLANK-TRIGGER", FieldType.STRING, 80);
        pnd_Extract_Date = localVariables.newFieldInRecord("pnd_Extract_Date", "#EXTRACT-DATE", FieldType.DATE);
        pnd_Next_Extract_Date_Time = localVariables.newFieldInRecord("pnd_Next_Extract_Date_Time", "#NEXT-EXTRACT-DATE-TIME", FieldType.STRING, 16);

        pnd_Next_Extract_Date_Time__R_Field_6 = localVariables.newGroupInRecord("pnd_Next_Extract_Date_Time__R_Field_6", "REDEFINE", pnd_Next_Extract_Date_Time);
        pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Date = pnd_Next_Extract_Date_Time__R_Field_6.newFieldInGroup("pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Date", 
            "#NEXT-EXTRACT-DATE", FieldType.NUMERIC, 8);
        pnd_Next_Extract_Date_Time_Pnd_Blank = pnd_Next_Extract_Date_Time__R_Field_6.newFieldInGroup("pnd_Next_Extract_Date_Time_Pnd_Blank", "#BLANK", 
            FieldType.STRING, 1);
        pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Time = pnd_Next_Extract_Date_Time__R_Field_6.newFieldInGroup("pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Time", 
            "#NEXT-EXTRACT-TIME", FieldType.NUMERIC, 7);
        pnd_Cor_Datea = localVariables.newFieldInRecord("pnd_Cor_Datea", "#COR-DATEA", FieldType.STRING, 8);
        pnd_Cor_Date = localVariables.newFieldInRecord("pnd_Cor_Date", "#COR-DATE", FieldType.DATE);
        pnd_Cor_Time = localVariables.newFieldInRecord("pnd_Cor_Time", "#COR-TIME", FieldType.NUMERIC, 7);
        pnd_Extr_Date = localVariables.newFieldInRecord("pnd_Extr_Date", "#EXTR-DATE", FieldType.DATE);
        pnd_Extractable = localVariables.newFieldInRecord("pnd_Extractable", "#EXTRACTABLE", FieldType.BOOLEAN, 1);
        pnd_Total_Extracted = localVariables.newFieldInRecord("pnd_Total_Extracted", "#TOTAL-EXTRACTED", FieldType.NUMERIC, 7);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_Hold_Isn = localVariables.newFieldInRecord("pnd_Hold_Isn", "#HOLD-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Non_T_C_Administered_Plan = localVariables.newFieldInRecord("pnd_Non_T_C_Administered_Plan", "#NON-T-C-ADMINISTERED-PLAN", FieldType.BOOLEAN, 
            1);
        pnd_State = localVariables.newFieldInRecord("pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_W_State_Zip_Idx = localVariables.newFieldInRecord("pnd_W_State_Zip_Idx", "#W-STATE-ZIP-IDX", FieldType.STRING, 10);

        pnd_W_State_Zip_Idx__R_Field_7 = localVariables.newGroupInRecord("pnd_W_State_Zip_Idx__R_Field_7", "REDEFINE", pnd_W_State_Zip_Idx);
        pnd_W_State_Zip_Idx_Pnd_W_State_Abrv = pnd_W_State_Zip_Idx__R_Field_7.newFieldInGroup("pnd_W_State_Zip_Idx_Pnd_W_State_Abrv", "#W-STATE-ABRV", 
            FieldType.STRING, 2);
        pnd_W_State_Zip_Idx_Pnd_W_State_Code = pnd_W_State_Zip_Idx__R_Field_7.newFieldInGroup("pnd_W_State_Zip_Idx_Pnd_W_State_Code", "#W-STATE-CODE", 
            FieldType.NUMERIC, 2);

        pnd_W_State_Zip_Idx__R_Field_8 = pnd_W_State_Zip_Idx__R_Field_7.newGroupInGroup("pnd_W_State_Zip_Idx__R_Field_8", "REDEFINE", pnd_W_State_Zip_Idx_Pnd_W_State_Code);
        pnd_W_State_Zip_Idx_Pnd_W_State_Code_A = pnd_W_State_Zip_Idx__R_Field_8.newFieldInGroup("pnd_W_State_Zip_Idx_Pnd_W_State_Code_A", "#W-STATE-CODE-A", 
            FieldType.STRING, 2);
        pnd_W_State_Zip_Idx_Pnd_W_Zip_Code_Range_Start = pnd_W_State_Zip_Idx__R_Field_7.newFieldInGroup("pnd_W_State_Zip_Idx_Pnd_W_Zip_Code_Range_Start", 
            "#W-ZIP-CODE-RANGE-START", FieldType.STRING, 3);
        pnd_W_State_Zip_Idx_Pnd_W_Zip_Code_Range_End = pnd_W_State_Zip_Idx__R_Field_7.newFieldInGroup("pnd_W_State_Zip_Idx_Pnd_W_Zip_Code_Range_End", 
            "#W-ZIP-CODE-RANGE-END", FieldType.STRING, 3);
        pnd_Orig_Cor_Issue_Date = localVariables.newFieldInRecord("pnd_Orig_Cor_Issue_Date", "#ORIG-COR-ISSUE-DATE", FieldType.NUMERIC, 8);
        pnd_Hold_T_Doi = localVariables.newFieldInRecord("pnd_Hold_T_Doi", "#HOLD-T-DOI", FieldType.NUMERIC, 4);

        pnd_Hold_T_Doi__R_Field_9 = localVariables.newGroupInRecord("pnd_Hold_T_Doi__R_Field_9", "REDEFINE", pnd_Hold_T_Doi);
        pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Mm = pnd_Hold_T_Doi__R_Field_9.newFieldInGroup("pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Mm", "#HOLD-T-DOI-MM", FieldType.NUMERIC, 
            2);
        pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Yy = pnd_Hold_T_Doi__R_Field_9.newFieldInGroup("pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Yy", "#HOLD-T-DOI-YY", FieldType.NUMERIC, 
            2);
        pnd_Ap_T_Doi = localVariables.newFieldInRecord("pnd_Ap_T_Doi", "#AP-T-DOI", FieldType.NUMERIC, 8);

        pnd_Ap_T_Doi__R_Field_10 = localVariables.newGroupInRecord("pnd_Ap_T_Doi__R_Field_10", "REDEFINE", pnd_Ap_T_Doi);
        pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Cc = pnd_Ap_T_Doi__R_Field_10.newFieldInGroup("pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Cc", "#AP-T-DOI-CC", FieldType.NUMERIC, 2);
        pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Yy = pnd_Ap_T_Doi__R_Field_10.newFieldInGroup("pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Yy", "#AP-T-DOI-YY", FieldType.NUMERIC, 2);
        pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Mm = pnd_Ap_T_Doi__R_Field_10.newFieldInGroup("pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Mm", "#AP-T-DOI-MM", FieldType.NUMERIC, 2);
        pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Dd = pnd_Ap_T_Doi__R_Field_10.newFieldInGroup("pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Dd", "#AP-T-DOI-DD", FieldType.NUMERIC, 2);

        pnd_Ap_T_Doi__R_Field_11 = localVariables.newGroupInRecord("pnd_Ap_T_Doi__R_Field_11", "REDEFINE", pnd_Ap_T_Doi);
        pnd_Ap_T_Doi_Pnd_Ap_T_Doi_A = pnd_Ap_T_Doi__R_Field_11.newFieldInGroup("pnd_Ap_T_Doi_Pnd_Ap_T_Doi_A", "#AP-T-DOI-A", FieldType.STRING, 8);
        pnd_Hold_C_Doi = localVariables.newFieldInRecord("pnd_Hold_C_Doi", "#HOLD-C-DOI", FieldType.NUMERIC, 4);

        pnd_Hold_C_Doi__R_Field_12 = localVariables.newGroupInRecord("pnd_Hold_C_Doi__R_Field_12", "REDEFINE", pnd_Hold_C_Doi);
        pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Mm = pnd_Hold_C_Doi__R_Field_12.newFieldInGroup("pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Mm", "#HOLD-C-DOI-MM", FieldType.NUMERIC, 
            2);
        pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Yy = pnd_Hold_C_Doi__R_Field_12.newFieldInGroup("pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Yy", "#HOLD-C-DOI-YY", FieldType.NUMERIC, 
            2);
        pnd_Ap_C_Doi = localVariables.newFieldInRecord("pnd_Ap_C_Doi", "#AP-C-DOI", FieldType.NUMERIC, 8);

        pnd_Ap_C_Doi__R_Field_13 = localVariables.newGroupInRecord("pnd_Ap_C_Doi__R_Field_13", "REDEFINE", pnd_Ap_C_Doi);
        pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Cc = pnd_Ap_C_Doi__R_Field_13.newFieldInGroup("pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Cc", "#AP-C-DOI-CC", FieldType.NUMERIC, 2);
        pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Yy = pnd_Ap_C_Doi__R_Field_13.newFieldInGroup("pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Yy", "#AP-C-DOI-YY", FieldType.NUMERIC, 2);
        pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Mm = pnd_Ap_C_Doi__R_Field_13.newFieldInGroup("pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Mm", "#AP-C-DOI-MM", FieldType.NUMERIC, 2);
        pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Dd = pnd_Ap_C_Doi__R_Field_13.newFieldInGroup("pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Dd", "#AP-C-DOI-DD", FieldType.NUMERIC, 2);

        pnd_Ap_C_Doi__R_Field_14 = localVariables.newGroupInRecord("pnd_Ap_C_Doi__R_Field_14", "REDEFINE", pnd_Ap_C_Doi);
        pnd_Ap_C_Doi_Pnd_Ap_C_Doi_A = pnd_Ap_C_Doi__R_Field_14.newFieldInGroup("pnd_Ap_C_Doi_Pnd_Ap_C_Doi_A", "#AP-C-DOI-A", FieldType.STRING, 8);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_15 = localVariables.newGroupInRecord("pnd_Date__R_Field_15", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Datn = pnd_Date__R_Field_15.newFieldInGroup("pnd_Date_Pnd_Datn", "#DATN", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_16 = localVariables.newGroupInRecord("pnd_Date__R_Field_16", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Ccyy = pnd_Date__R_Field_16.newFieldInGroup("pnd_Date_Pnd_Date_Ccyy", "#DATE-CCYY", FieldType.STRING, 4);

        pnd_Date__R_Field_17 = pnd_Date__R_Field_16.newGroupInGroup("pnd_Date__R_Field_17", "REDEFINE", pnd_Date_Pnd_Date_Ccyy);
        pnd_Date_Pnd_Date_Ccyy_N = pnd_Date__R_Field_17.newFieldInGroup("pnd_Date_Pnd_Date_Ccyy_N", "#DATE-CCYY-N", FieldType.NUMERIC, 4);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        ldaAppbl170.initializeValues();
        ldaY2datebw.initializeValues();
        ldaAcil1080.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb170() throws Exception
    {
        super("Appb170");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB170", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 52
        getWorkFiles().write(2, false, pnd_Blank_Trigger);                                                                                                                //Natural: WRITE WORK FILE 2 #BLANK-TRIGGER
        //*  CORMDM
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        vw_annty_Actvty_Prap_View.startDatabaseRead                                                                                                                       //Natural: READ ANNTY-ACTVTY-PRAP-VIEW
        (
        "PND_PND_L3700",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        PND_PND_L3700:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("PND_PND_L3700")))
        {
            //*  REJECT IF AP-SUBSTITUTION-CONTRACT-IND GT ' '      /* TNGSUB /* BIP
            //*  ONEIRA
            if (condition(annty_Actvty_Prap_View_Ap_Oneira_Acct_No.greater(" ")))                                                                                         //Natural: REJECT IF AP-ONEIRA-ACCT-NO GT ' '
            {
                continue;
            }
            pnd_Field_Pnd_N8.setValue(annty_Actvty_Prap_View_Ap_Dt_Ent_Sys);                                                                                              //Natural: MOVE AP-DT-ENT-SYS TO #N8
            pnd_File_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Field_Pnd_A8);                                                                                //Natural: MOVE EDITED #A8 TO #FILE-DATE ( EM = MMDDYYYY )
            //*  CLS01
            pnd_Hold_Isn.setValue(vw_annty_Actvty_Prap_View.getAstISN("PND_PND_L3700"));                                                                                  //Natural: MOVE *ISN ( ##L3700. ) TO #HOLD-ISN
            //*  CLS01
            if (condition(annty_Actvty_Prap_View_Ap_Eft_Requested_Ind.notEquals(" ") || annty_Actvty_Prap_View_Ap_Pin_Nbr.equals(getZero()) || annty_Actvty_Prap_View_Ap_Status.equals("A"))) //Natural: IF AP-EFT-REQUESTED-IND NE ' ' OR AP-PIN-NBR = 0 OR AP-STATUS = 'A'
            {
                if (condition(annty_Actvty_Prap_View_Ap_Status.equals("A")))                                                                                              //Natural: IF AP-STATUS = 'A'
                {
                    getReports().write(0, "Logically deleted record skipped","=",annty_Actvty_Prap_View_Ap_Pin_Nbr,"=",annty_Actvty_Prap_View_Ap_Soc_Sec);                //Natural: WRITE 'Logically deleted record skipped' '=' AP-PIN-NBR '=' AP-SOC-SEC
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L3700"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3700"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    logically_Deleted_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO LOGICALLY-DELETED-COUNT
                }                                                                                                                                                         //Natural: END-IF
                //*  CLS01
                if (condition(annty_Actvty_Prap_View_Ap_Eft_Requested_Ind.notEquals(" ") && annty_Actvty_Prap_View_Ap_Pin_Nbr.equals(getZero())))                         //Natural: IF AP-EFT-REQUESTED-IND NE ' ' AND AP-PIN-NBR = 0
                {
                    //*  PRINT OUT VALUES FOR ANY SKIPPED RECORDS AND THE REASON
                    //*  CLS01
                    getReports().write(0, "RECORD SKIPPED PIN","=",annty_Actvty_Prap_View_Ap_Pin_Nbr,"CNTRCT",annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,"=",                  //Natural: WRITE 'RECORD SKIPPED PIN' '=' AP-PIN-NBR 'CNTRCT' AP-TIAA-CNTRCT '=' AP-STATUS
                        annty_Actvty_Prap_View_Ap_Status);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L3700"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3700"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  CLS02
            getReports().write(0, "Extracted PIN",annty_Actvty_Prap_View_Ap_Pin_Nbr,"SSN",annty_Actvty_Prap_View_Ap_Soc_Sec,"CNTRCT",annty_Actvty_Prap_View_Ap_Tiaa_Cntrct); //Natural: WRITE 'Extracted PIN' AP-PIN-NBR 'SSN' AP-SOC-SEC 'CNTRCT' AP-TIAA-CNTRCT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L3700"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3700"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  BE1. RAC PROJ 8/16/11
                                                                                                                                                                          //Natural: PERFORM MOVE-PRAP-FIELDS-TO-MDM
            sub_Move_Prap_Fields_To_Mdm();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L3700"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3700"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAppbl170.getPnd_Mdm_Data_Ph_Unique_Id_Nbr().notEquals(" ") && ldaAppbl170.getPnd_Mdm_Data_Ph_Social_Security_No().notEquals(" ")))           //Natural: IF PH_UNIQUE_ID_NBR NE ' ' AND PH_SOCIAL_SECURITY_NO NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT-RECORD
                sub_Write_Extract_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L3700"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3700"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  11-10-11 C.SCHNEIDER
                                                                                                                                                                          //Natural: PERFORM UPDATE-PRAP-FILE
                sub_Update_Prap_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L3700"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3700"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "SKIPPED BLANK RECORD WITH ","=",annty_Actvty_Prap_View_Ap_Pin_Nbr);                                                                //Natural: WRITE 'SKIPPED BLANK RECORD WITH ' '=' AP-PIN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L3700"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3700"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "SUCCESSFUL EOJ",pnd_Write_Count,"Records written to file");                                                                                //Natural: WRITE 'SUCCESSFUL EOJ' #WRITE-COUNT 'Records written to file'
        if (Global.isEscape()) return;
        //*  CLS02
        getReports().write(0, pnd_Mayo_Cnt,"MAYO records enrolled");                                                                                                      //Natural: WRITE #MAYO-CNT 'MAYO records enrolled'
        if (Global.isEscape()) return;
        //*  DSP4
        getReports().write(0, pnd_Ds_3prk_Cnt,"DATASHARE RECORDS ENROLLED");                                                                                              //Natural: WRITE #DS-3PRK-CNT 'DATASHARE RECORDS ENROLLED'
        if (Global.isEscape()) return;
        getReports().write(0, "              ",logically_Deleted_Count,"Logically deleted","records not extracted");                                                      //Natural: WRITE '              ' LOGICALLY-DELETED-COUNT 'Logically deleted' 'records not extracted'
        if (Global.isEscape()) return;
        //*  CLOSE MQ.  CORMDM
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "LEAVING APPB170 :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                   //Natural: WRITE 'LEAVING APPB170 :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-PRAP-FIELDS-TO-MDM
        //*  #MDM-DATA.R_ADDRESS_LINE_4  := AP-ADDRESS-TXT  (4)
        //*  #MDM-DATA.R_ADDRESS_LINE_5  := AP-ADDRESS-TXT  (5)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT-RECORD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-MDM-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PLAN-IF-NON-TIAA-ADMIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-ISSUE-DATE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ORIGINAL-COR-ISSUE-DATE
        //*    #MDMA210.#I-CONTRACT-NUMBER := AP-TIAA-CNTRCT
        //*    #MDMA210.#I-TIAA-CREF-IND   := 'T'
        //*    #MDMA210.#I-PAYEE-CODE := 01
        //*      #ORIG-COR-ISSUE-DATE := #MDMA210.#O-CONTRACT-ISSUE-DATE
        //*    #MDMA210.#I-CONTRACT-NUMBER := AP-CREF-CERT
        //*    #MDMA210.#I-TIAA-CREF-IND   := 'C'
        //*    #MDMA210.#I-PAYEE-CODE := 01
        //*      #ORIG-COR-ISSUE-DATE := #MDMA210.#O-CONTRACT-ISSUE-DATE
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-NEXT-EXTRACT-DATE
        //*     ROUTINE ADDED /* CLS01
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PRAP-FILE
        //* *******************************************
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  JRB2
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    //*  BE1. RAC PROJ 8/16/11
    private void sub_Move_Prap_Fields_To_Mdm() throws Exception                                                                                                           //Natural: MOVE-PRAP-FIELDS-TO-MDM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAppbl170.getPnd_Mdm_Data().reset();                                                                                                                            //Natural: RESET #MDM-DATA
        pnd_Extractable.setValue(true);                                                                                                                                   //Natural: ASSIGN #EXTRACTABLE := TRUE
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Plan_Cde().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                   //Natural: ASSIGN #MDM-DATA.CNTRCT_PLAN_CDE := AP-SGRD-PLAN-NO
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Fund_Cde().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                                //Natural: ASSIGN #MDM-DATA.CNTRCT_FUND_CDE := AP-SGRD-SUBPLAN-NO
        if (condition(annty_Actvty_Prap_View_Ap_Pin_Nbr.greater(getZero())))                                                                                              //Natural: IF AP-PIN-NBR GT 0
        {
            //*  MOVE EDITED AP-PIN-NBR (EM=9999999) TO                   /* PINE >>
            ldaAppbl170.getPnd_Mdm_Data_Ph_Unique_Id_Nbr().setValueEdited(annty_Actvty_Prap_View_Ap_Pin_Nbr,new ReportEditMask("ZZZZZ9999999"));                          //Natural: MOVE EDITED AP-PIN-NBR ( EM = ZZZZZ9999999 ) TO #MDM-DATA.PH_UNIQUE_ID_NBR
            //*  PINE <<
            ldaAppbl170.getPnd_Mdm_Data_Ph_Unique_Id_Nbr().setValue(ldaAppbl170.getPnd_Mdm_Data_Ph_Unique_Id_Nbr(), MoveOption.LeftJustified);                            //Natural: MOVE LEFT JUSTIFIED #MDM-DATA.PH_UNIQUE_ID_NBR TO #MDM-DATA.PH_UNIQUE_ID_NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Soc_Sec.greater(getZero())))                                                                                              //Natural: IF AP-SOC-SEC GT 0
        {
            ldaAppbl170.getPnd_Mdm_Data_Ph_Social_Security_No().setValueEdited(annty_Actvty_Prap_View_Ap_Soc_Sec,new ReportEditMask("999999999"));                        //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999999999 ) TO #MDM-DATA.PH_SOCIAL_SECURITY_NO
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Nbr().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                                         //Natural: ASSIGN #MDM-DATA.CNTRCT_NBR := AP-TIAA-CNTRCT
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Cref_Nbr().setValue(annty_Actvty_Prap_View_Ap_Cref_Cert);                                                                      //Natural: ASSIGN #MDM-DATA.CNTRCT_CREF_NBR := AP-CREF-CERT
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Payee_Cde().setValue("01");                                                                                                    //Natural: ASSIGN #MDM-DATA.CNTRCT_PAYEE_CDE := '01'
        ldaAppbl170.getPnd_Mdm_Data_Contract_Party_Role().setValue(" ");                                                                                                  //Natural: ASSIGN #MDM-DATA.CONTRACT_PARTY_ROLE := ' '
                                                                                                                                                                          //Natural: PERFORM CHECK-PLAN-IF-NON-TIAA-ADMIN
        sub_Check_Plan_If_Non_Tiaa_Admin();
        if (condition(Global.isEscape())) {return;}
        //*  CURRENTLY LIMITED TO MAYO PLANS
        if (condition(pnd_Non_T_C_Administered_Plan.getBoolean()))                                                                                                        //Natural: IF #NON-T-C-ADMINISTERED-PLAN
        {
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Status_Cde().setValue("Y");                                                                                                //Natural: ASSIGN #MDM-DATA.CNTRCT_STATUS_CDE := 'Y'
            //*  DSP4 - 3PRK PLAN
            if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No.getSubstring(1,1).equals("M")))                                                                          //Natural: IF SUBSTR ( AP-SGRD-PLAN-NO,1,1 ) = 'M'
            {
                //*  DSP4
                pnd_Ds_3prk_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #DS-3PRK-CNT
                //*  DSP4
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  CLS02
                pnd_Mayo_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #MAYO-CNT
                //*  DSP4
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Status_Cde().setValue("H");                                                                                                //Natural: ASSIGN #MDM-DATA.CNTRCT_STATUS_CDE := 'H'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SET-ISSUE-DATE
        sub_Set_Issue_Date();
        if (condition(Global.isEscape())) {return;}
        //*  SET TIAA AND CREF MAILED IND.
        if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("RS1") || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("RS2")  //Natural: IF SUBSTRING ( AP-SGRD-SUBPLAN-NO,1,3 ) = 'RS1' OR = 'RS2' OR = 'RS3' OR = 'RP1' OR = 'RP2' OR = 'RP3'
            || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("RS3") || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("RP1") 
            || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("RP2") || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,3).equals("RP3")))
        {
            //*  TIAA CONTRACT MAILED IND
            //*  CREF CONTRACT MAILED IND
            //*  TIAA CONTRACT MAILED IND
            //*  CREF CONTRACT MAILED IND
            //*  TIAA CONTRACT MAILED IND
            //*  CREF CONTRACT MAILED IND
            short decideConditionsMet1112 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-OIA-IND = '10'
            if (condition(annty_Actvty_Prap_View_Ap_Oia_Ind.equals("10")))
            {
                decideConditionsMet1112++;
                ldaAppbl170.getPnd_Mdm_Data_Tiaa_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.TIAA_MAILED_IND := 'Y'
                ldaAppbl170.getPnd_Mdm_Data_Cref_Mailed_Ind().setValue("N");                                                                                              //Natural: ASSIGN #MDM-DATA.CREF_MAILED_IND := 'N'
            }                                                                                                                                                             //Natural: WHEN AP-OIA-IND = '11'
            else if (condition(annty_Actvty_Prap_View_Ap_Oia_Ind.equals("11")))
            {
                decideConditionsMet1112++;
                ldaAppbl170.getPnd_Mdm_Data_Tiaa_Mailed_Ind().setValue("N");                                                                                              //Natural: ASSIGN #MDM-DATA.TIAA_MAILED_IND := 'N'
                ldaAppbl170.getPnd_Mdm_Data_Cref_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.CREF_MAILED_IND := 'Y'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ldaAppbl170.getPnd_Mdm_Data_Tiaa_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.TIAA_MAILED_IND := 'Y'
                ldaAppbl170.getPnd_Mdm_Data_Cref_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.CREF_MAILED_IND := 'Y'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_C_Doi.greater(getZero())))                                                                                                //Natural: IF AP-C-DOI GT 0
        {
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Cref_Issued_Ind().setValue("Y");                                                                                           //Natural: ASSIGN #MDM-DATA.CNTRCT_CREF_ISSUED_IND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Cref_Issued_Ind().setValue("N");                                                                                           //Natural: ASSIGN #MDM-DATA.CNTRCT_CREF_ISSUED_IND := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1131 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-OIA-IND = '10'
        if (condition(annty_Actvty_Prap_View_Ap_Oia_Ind.equals("10")))
        {
            decideConditionsMet1131++;
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Ap_T_Doi);                                                                                        //Natural: MOVE #AP-T-DOI TO #MDM-DATA.CNTRCT_ISSUE_DTE
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Cref_Issued_Ind().setValue("N");                                                                                           //Natural: ASSIGN #MDM-DATA.CNTRCT_CREF_ISSUED_IND := 'N'
                                                                                                                                                                          //Natural: PERFORM GET-ORIGINAL-COR-ISSUE-DATE
            sub_Get_Original_Cor_Issue_Date();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Orig_Cor_Issue_Date.notEquals(getZero())))                                                                                                  //Natural: IF #ORIG-COR-ISSUE-DATE NE 0
            {
                //*  CREF CONTRACT MAILED IND
                ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Orig_Cor_Issue_Date);                                                                         //Natural: MOVE #ORIG-COR-ISSUE-DATE TO #MDM-DATA.CNTRCT_ISSUE_DTE
                ldaAppbl170.getPnd_Mdm_Data_Cntrct_Cref_Issued_Ind().setValue("Y");                                                                                       //Natural: ASSIGN #MDM-DATA.CNTRCT_CREF_ISSUED_IND := 'Y'
                ldaAppbl170.getPnd_Mdm_Data_Cref_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.CREF_MAILED_IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN AP-OIA-IND = '11'
        else if (condition(annty_Actvty_Prap_View_Ap_Oia_Ind.equals("11")))
        {
            decideConditionsMet1131++;
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Ap_C_Doi);                                                                                        //Natural: MOVE #AP-C-DOI TO #MDM-DATA.CNTRCT_ISSUE_DTE
                                                                                                                                                                          //Natural: PERFORM GET-ORIGINAL-COR-ISSUE-DATE
            sub_Get_Original_Cor_Issue_Date();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Orig_Cor_Issue_Date.notEquals(getZero())))                                                                                                  //Natural: IF #ORIG-COR-ISSUE-DATE NE 0
            {
                //*  TIAA CONTRACT MAILED IND
                ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Orig_Cor_Issue_Date);                                                                         //Natural: MOVE #ORIG-COR-ISSUE-DATE TO #MDM-DATA.CNTRCT_ISSUE_DTE
                ldaAppbl170.getPnd_Mdm_Data_Tiaa_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.TIAA_MAILED_IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN AP-T-DOI GT 0 AND AP-C-DOI GT 0
        else if (condition(annty_Actvty_Prap_View_Ap_T_Doi.greater(getZero()) && annty_Actvty_Prap_View_Ap_C_Doi.greater(getZero())))
        {
            decideConditionsMet1131++;
            //*  NON-RS/RSP COMPANION
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Ap_T_Doi);                                                                                        //Natural: MOVE #AP-T-DOI TO #MDM-DATA.CNTRCT_ISSUE_DTE
        }                                                                                                                                                                 //Natural: WHEN AP-T-DOI GT 0 AND AP-C-DOI = 0
        else if (condition(annty_Actvty_Prap_View_Ap_T_Doi.greater(getZero()) && annty_Actvty_Prap_View_Ap_C_Doi.equals(getZero())))
        {
            decideConditionsMet1131++;
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Ap_T_Doi);                                                                                        //Natural: MOVE #AP-T-DOI TO #MDM-DATA.CNTRCT_ISSUE_DTE
                                                                                                                                                                          //Natural: PERFORM GET-ORIGINAL-COR-ISSUE-DATE
            sub_Get_Original_Cor_Issue_Date();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Orig_Cor_Issue_Date.notEquals(getZero())))                                                                                                  //Natural: IF #ORIG-COR-ISSUE-DATE NE 0
            {
                //*  CREF CONTRACT MAILED IND
                ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Orig_Cor_Issue_Date);                                                                         //Natural: MOVE #ORIG-COR-ISSUE-DATE TO #MDM-DATA.CNTRCT_ISSUE_DTE
                ldaAppbl170.getPnd_Mdm_Data_Cref_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.CREF_MAILED_IND := 'Y'
                //*  NON-RS/RSP COMPANION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN AP-T-DOI = 0 AND AP-C-DOI GT 0
        else if (condition(annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero()) && annty_Actvty_Prap_View_Ap_C_Doi.greater(getZero())))
        {
            decideConditionsMet1131++;
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Ap_C_Doi);                                                                                        //Natural: MOVE #AP-C-DOI TO #MDM-DATA.CNTRCT_ISSUE_DTE
                                                                                                                                                                          //Natural: PERFORM GET-ORIGINAL-COR-ISSUE-DATE
            sub_Get_Original_Cor_Issue_Date();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Orig_Cor_Issue_Date.notEquals(getZero())))                                                                                                  //Natural: IF #ORIG-COR-ISSUE-DATE NE 0
            {
                //*  TIAA CONTRACT MAILED IND
                ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte().setValue(pnd_Orig_Cor_Issue_Date);                                                                         //Natural: MOVE #ORIG-COR-ISSUE-DATE TO #MDM-DATA.CNTRCT_ISSUE_DTE
                ldaAppbl170.getPnd_Mdm_Data_Tiaa_Mailed_Ind().setValue("Y");                                                                                              //Natural: ASSIGN #MDM-DATA.TIAA_MAILED_IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
            //*  DA CONTRACT
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Lob_Cde().setValue("D");                                                                                                       //Natural: ASSIGN #MDM-DATA.CNTRCT_LOB_CDE := 'D'
        //*  PARTICIPANT OWNED
        //*  DELAYED VESTED
        //*  INSTITUTIONALLY OWNED
        short decideConditionsMet1179 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF AP-OWNERSHIP;//Natural: VALUE 1
        if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(1))))
        {
            decideConditionsMet1179++;
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Indctrs_And_Codes().setValue("0");                                                                                         //Natural: ASSIGN #MDM-DATA.CNTRCT_INDCTRS_AND_CODES := '0'
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(2))))
        {
            decideConditionsMet1179++;
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Indctrs_And_Codes().setValue("2");                                                                                         //Natural: ASSIGN #MDM-DATA.CNTRCT_INDCTRS_AND_CODES := '2'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(3))))
        {
            decideConditionsMet1179++;
            ldaAppbl170.getPnd_Mdm_Data_Cntrct_Indctrs_And_Codes().setValue("1");                                                                                         //Natural: ASSIGN #MDM-DATA.CNTRCT_INDCTRS_AND_CODES := '1'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
            //*  SET TO BLANK
            //*  SET TO BLANK
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Optn_Cde().setValue(" ");                                                                                                      //Natural: ASSIGN #MDM-DATA.CNTRCT_OPTN_CDE := ' '
        ldaAppbl170.getPnd_Mdm_Data_Cntrct_Social_Cde().setValue(" ");                                                                                                    //Natural: ASSIGN #MDM-DATA.CNTRCT_SOCIAL_CDE := ' '
        if (condition(annty_Actvty_Prap_View_Ap_Cai_Ind.equals("Y")))                                                                                                     //Natural: IF AP-CAI-IND = 'Y'
        {
            ldaAppbl170.getPnd_Mdm_Data_Cust_Agreement_Received_Ind().setValue(annty_Actvty_Prap_View_Ap_Cai_Ind);                                                        //Natural: ASSIGN #MDM-DATA.CUST_AGREEMENT_RECEIVED_IND := AP-CAI-IND
            //*  SET TO BLANK
            //*  SET TO BLANK
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppbl170.getPnd_Mdm_Data_Asset_Allocation_Service_Ind().setValue(" ");                                                                                         //Natural: ASSIGN #MDM-DATA.ASSET_ALLOCATION_SERVICE_IND := ' '
        ldaAppbl170.getPnd_Mdm_Data_Platform_Ind().setValue("S");                                                                                                         //Natural: ASSIGN #MDM-DATA.PLATFORM_IND := 'S'
        ldaAppbl170.getPnd_Mdm_Data_Multi_Source_Ind().setValue(" ");                                                                                                     //Natural: ASSIGN #MDM-DATA.MULTI_SOURCE_IND := ' '
        if (condition(annty_Actvty_Prap_View_Ap_Omni_Account_Issuance.equals("1") || annty_Actvty_Prap_View_Ap_Omni_Account_Issuance.equals("2") || annty_Actvty_Prap_View_Ap_Omni_Account_Issuance.equals("3"))) //Natural: IF AP-OMNI-ACCOUNT-ISSUANCE = '1' OR = '2' OR = '3'
        {
            ldaAppbl170.getPnd_Mdm_Data_Online_Enrollment_Ind().setValue(annty_Actvty_Prap_View_Ap_Omni_Account_Issuance);                                                //Natural: ASSIGN #MDM-DATA.ONLINE_ENROLLMENT_IND := AP-OMNI-ACCOUNT-ISSUANCE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppbl170.getPnd_Mdm_Data_Plan_Nbr().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                          //Natural: ASSIGN #MDM-DATA.PLAN_NBR := AP-SGRD-PLAN-NO
        ldaAppbl170.getPnd_Mdm_Data_Sub_Plan_Nbr().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                                   //Natural: ASSIGN #MDM-DATA.SUB_PLAN_NBR := AP-SGRD-SUBPLAN-NO
        ldaAppbl170.getPnd_Mdm_Data_Address_Line_1().setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(1));                                                        //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_1 := AP-ADDRESS-LINE ( 1 )
        ldaAppbl170.getPnd_Mdm_Data_Address_Line_2().setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(2));                                                        //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_2 := AP-ADDRESS-LINE ( 2 )
        ldaAppbl170.getPnd_Mdm_Data_Address_Line_3().setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(3));                                                        //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_3 := AP-ADDRESS-LINE ( 3 )
        ldaAppbl170.getPnd_Mdm_Data_Address_Line_4().setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(4));                                                        //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_4 := AP-ADDRESS-LINE ( 4 )
        ldaAppbl170.getPnd_Mdm_Data_Address_Line_5().setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(5));                                                        //Natural: ASSIGN #MDM-DATA.ADDRESS_LINE_5 := AP-ADDRESS-LINE ( 5 )
        ldaAppbl170.getPnd_Mdm_Data_Zip_Code().setValue(annty_Actvty_Prap_View_Ap_Mail_Zip);                                                                              //Natural: ASSIGN #MDM-DATA.ZIP_CODE := AP-MAIL-ZIP
        ldaAppbl170.getPnd_Mdm_Data_City().setValue(annty_Actvty_Prap_View_Ap_City);                                                                                      //Natural: ASSIGN #MDM-DATA.CITY := AP-CITY
        pnd_State.reset();                                                                                                                                                //Natural: RESET #STATE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 58
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(58)); pnd_I.nadd(1))
        {
            pnd_W_State_Zip_Idx.setValue(ldaAcil1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue(pnd_I));                                                     //Natural: ASSIGN #W-STATE-ZIP-IDX := #STATE-ZIP-IDX ( #I )
            if (condition(pnd_W_State_Zip_Idx_Pnd_W_State_Code_A.equals(annty_Actvty_Prap_View_Ap_Current_State_Code)))                                                   //Natural: IF #W-STATE-CODE-A = AP-CURRENT-STATE-CODE
            {
                pnd_State.setValue(pnd_W_State_Zip_Idx_Pnd_W_State_Abrv);                                                                                                 //Natural: ASSIGN #STATE := #W-STATE-ABRV
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppbl170.getPnd_Mdm_Data_State().setValue(pnd_State);                                                                                                          //Natural: ASSIGN #MDM-DATA.STATE := #STATE
        if (condition(ldaAppbl170.getPnd_Mdm_Data_State().greater(" ")))                                                                                                  //Natural: IF #MDM-DATA.STATE GT ' '
        {
            //*  FOREIGN
            //*  CANADA
            short decideConditionsMet1226 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #MDM-DATA.STATE;//Natural: VALUE 'FN'
            if (condition((ldaAppbl170.getPnd_Mdm_Data_State().equals("FN"))))
            {
                decideConditionsMet1226++;
                ldaAppbl170.getPnd_Mdm_Data_Mail_Type().setValue("F");                                                                                                    //Natural: ASSIGN #MDM-DATA.MAIL_TYPE := 'F'
            }                                                                                                                                                             //Natural: VALUE 'CN'
            else if (condition((ldaAppbl170.getPnd_Mdm_Data_State().equals("CN"))))
            {
                decideConditionsMet1226++;
                ldaAppbl170.getPnd_Mdm_Data_Mail_Type().setValue("C");                                                                                                    //Natural: ASSIGN #MDM-DATA.MAIL_TYPE := 'C'
                //*  CODING STARTS(PF1)
                if (condition(annty_Actvty_Prap_View_Ap_Orchestration_Id.greater(" ")))                                                                                   //Natural: IF AP-ORCHESTRATION-ID > ' '
                {
                    if (condition(ldaAppbl170.getPnd_Mdm_Data_Address_Line_3().getSubstring(3,1).equals(",")))                                                            //Natural: IF SUBSTR ( #MDM-DATA.ADDRESS_LINE_3,3,1 ) = ','
                    {
                        ldaAppbl170.getPnd_Mdm_Data_Address_Line_3().separate(SeparateOption.WithAnyDelimiters, ",", ldaAppbl170.getPnd_Mdm_Data_State(),                 //Natural: SEPARATE #MDM-DATA.ADDRESS_LINE_3 INTO #MDM-DATA.STATE #MDM-DATA.ZIP_CODE WITH DELIMITER ','
                            ldaAppbl170.getPnd_Mdm_Data_Zip_Code());
                        ldaAppbl170.getPnd_Mdm_Data_Address_Line_3().reset();                                                                                             //Natural: RESET #MDM-DATA.ADDRESS_LINE_3
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CODING ENDED(PF1)
                    //*  US
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ldaAppbl170.getPnd_Mdm_Data_Mail_Type().setValue("U");                                                                                                    //Natural: ASSIGN #MDM-DATA.MAIL_TYPE := 'U'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppbl170.getPnd_Mdm_Data_R_Address_Line_1().setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(1));                                                       //Natural: ASSIGN #MDM-DATA.R_ADDRESS_LINE_1 := AP-ADDRESS-TXT ( 1 )
        ldaAppbl170.getPnd_Mdm_Data_R_Address_Line_2().setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(2));                                                       //Natural: ASSIGN #MDM-DATA.R_ADDRESS_LINE_2 := AP-ADDRESS-TXT ( 2 )
        ldaAppbl170.getPnd_Mdm_Data_R_Address_Line_3().setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(3));                                                       //Natural: ASSIGN #MDM-DATA.R_ADDRESS_LINE_3 := AP-ADDRESS-TXT ( 3 )
        ldaAppbl170.getPnd_Mdm_Data_R_Zip_Code().setValue(annty_Actvty_Prap_View_Ap_Zip_Code);                                                                            //Natural: ASSIGN #MDM-DATA.R_ZIP_CODE := AP-ZIP-CODE
        ldaAppbl170.getPnd_Mdm_Data_R_City().setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(4));                                                                 //Natural: ASSIGN #MDM-DATA.R_CITY := AP-ADDRESS-TXT ( 4 )
        ldaAppbl170.getPnd_Mdm_Data_R_State().setValue(annty_Actvty_Prap_View_Ap_Address_Txt.getValue(5));                                                                //Natural: ASSIGN #MDM-DATA.R_STATE := AP-ADDRESS-TXT ( 5 )
        if (condition(ldaAppbl170.getPnd_Mdm_Data_R_City().equals("APO") || ldaAppbl170.getPnd_Mdm_Data_R_City().equals("DPO") || ldaAppbl170.getPnd_Mdm_Data_R_City().equals("FPO"))) //Natural: IF #MDM-DATA.R_CITY = 'APO' OR = 'DPO' OR = 'FPO'
        {
            short decideConditionsMet1251 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #MDM-DATA.R_STATE;//Natural: VALUE 'NY'
            if (condition((ldaAppbl170.getPnd_Mdm_Data_R_State().equals("NY"))))
            {
                decideConditionsMet1251++;
                ldaAppbl170.getPnd_Mdm_Data_R_State().setValue("AE");                                                                                                     //Natural: MOVE 'AE' TO #MDM-DATA.R_STATE
            }                                                                                                                                                             //Natural: VALUE 'CA'
            else if (condition((ldaAppbl170.getPnd_Mdm_Data_R_State().equals("CA"))))
            {
                decideConditionsMet1251++;
                ldaAppbl170.getPnd_Mdm_Data_R_State().setValue("AP");                                                                                                     //Natural: MOVE 'AP' TO #MDM-DATA.R_STATE
            }                                                                                                                                                             //Natural: VALUE 'FL'
            else if (condition((ldaAppbl170.getPnd_Mdm_Data_R_State().equals("FL"))))
            {
                decideConditionsMet1251++;
                ldaAppbl170.getPnd_Mdm_Data_R_State().setValue("AA");                                                                                                     //Natural: MOVE 'AA' TO #MDM-DATA.R_STATE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppbl170.getPnd_Mdm_Data_R_State().greater(" ")))                                                                                                //Natural: IF #MDM-DATA.R_STATE GT ' '
        {
            //*  FOREIGN
            //*  CANADA
            short decideConditionsMet1265 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #MDM-DATA.R_STATE;//Natural: VALUE 'FN'
            if (condition((ldaAppbl170.getPnd_Mdm_Data_R_State().equals("FN"))))
            {
                decideConditionsMet1265++;
                ldaAppbl170.getPnd_Mdm_Data_R_Mail_Type().setValue("F");                                                                                                  //Natural: ASSIGN #MDM-DATA.R_MAIL_TYPE := 'F'
            }                                                                                                                                                             //Natural: VALUE 'CN'
            else if (condition((ldaAppbl170.getPnd_Mdm_Data_R_State().equals("CN"))))
            {
                decideConditionsMet1265++;
                ldaAppbl170.getPnd_Mdm_Data_R_Mail_Type().setValue("C");                                                                                                  //Natural: ASSIGN #MDM-DATA.R_MAIL_TYPE := 'C'
                //*  CODING STARTS(PF1)
                if (condition(annty_Actvty_Prap_View_Ap_Orchestration_Id.greater(" ")))                                                                                   //Natural: IF AP-ORCHESTRATION-ID > ' '
                {
                    if (condition(ldaAppbl170.getPnd_Mdm_Data_R_Address_Line_3().getSubstring(3,1).equals(",")))                                                          //Natural: IF SUBSTR ( #MDM-DATA.R_ADDRESS_LINE_3,3,1 ) = ','
                    {
                        ldaAppbl170.getPnd_Mdm_Data_R_Address_Line_3().separate(SeparateOption.WithAnyDelimiters, ",", ldaAppbl170.getPnd_Mdm_Data_R_State(),             //Natural: SEPARATE #MDM-DATA.R_ADDRESS_LINE_3 INTO #MDM-DATA.R_STATE #MDM-DATA.R_ZIP_CODE WITH DELIMITER ','
                            ldaAppbl170.getPnd_Mdm_Data_R_Zip_Code());
                        ldaAppbl170.getPnd_Mdm_Data_R_Address_Line_3().reset();                                                                                           //Natural: RESET #MDM-DATA.R_ADDRESS_LINE_3
                    }                                                                                                                                                     //Natural: END-IF
                    //*  CODING ENDED(PF1)
                    //*  US
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ldaAppbl170.getPnd_Mdm_Data_R_Mail_Type().setValue("U");                                                                                                  //Natural: ASSIGN #MDM-DATA.R_MAIL_TYPE := 'U'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Name_Addr_Cd.greater(" ")))                                                                                               //Natural: IF AP-NAME-ADDR-CD GT ' '
        {
            ldaAppbl170.getPnd_Mdm_Data_R_Country_Code().setValueEdited(annty_Actvty_Prap_View_Ap_Name_Addr_Cd,new ReportEditMask("XXX"));                                //Natural: MOVE EDITED AP-NAME-ADDR-CD ( EM = XXX ) TO #MDM-DATA.R_COUNTRY_CODE
            //*  ISO
            if (condition(annty_Actvty_Prap_View_Ap_Orchestration_Id.greater(" ")))                                                                                       //Natural: IF AP-ORCHESTRATION-ID > ' '
            {
                ldaAppbl170.getPnd_Mdm_Data_R_Country_Code_Type().setValue("1");                                                                                          //Natural: ASSIGN #MDM-DATA.R_COUNTRY_CODE_TYPE := '1'
                //*  IRS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppbl170.getPnd_Mdm_Data_R_Country_Code_Type().setValue("2");                                                                                          //Natural: ASSIGN #MDM-DATA.R_COUNTRY_CODE_TYPE := '2'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd.greater(" ")))                                                                                       //Natural: IF AP-MAIL-ADDR-COUNTRY-CD GT ' '
        {
            ldaAppbl170.getPnd_Mdm_Data_Country_Code().setValue(annty_Actvty_Prap_View_Ap_Mail_Addr_Country_Cd);                                                          //Natural: ASSIGN #MDM-DATA.COUNTRY_CODE := AP-MAIL-ADDR-COUNTRY-CD
            //*  ISO
            if (condition(annty_Actvty_Prap_View_Ap_Orchestration_Id.greater(" ")))                                                                                       //Natural: IF AP-ORCHESTRATION-ID > ' '
            {
                ldaAppbl170.getPnd_Mdm_Data_Country_Code_Type().setValue("1");                                                                                            //Natural: ASSIGN #MDM-DATA.COUNTRY_CODE_TYPE := '1'
                //*  IRS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppbl170.getPnd_Mdm_Data_Country_Code_Type().setValue("2");                                                                                            //Natural: ASSIGN #MDM-DATA.COUNTRY_CODE_TYPE := '2'
            }                                                                                                                                                             //Natural: END-IF
            //*  SOR
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppbl170.getPnd_Mdm_Data_Home_Phone_Nbr().setValue(annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr);                                                            //Natural: ASSIGN #MDM-DATA.HOME_PHONE_NBR := AP-BANK-PYMNT-ACCT-NMBR
        ldaAppbl170.getPnd_Mdm_Data_Work_Phone_Nbr().setValue(annty_Actvty_Prap_View_Ap_Phone_No);                                                                        //Natural: ASSIGN #MDM-DATA.WORK_PHONE_NBR := AP-PHONE-NO
        ldaAppbl170.getPnd_Mdm_Data_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Email_Address);                                                                    //Natural: ASSIGN #MDM-DATA.EMAIL_ADDRESS := AP-EMAIL-ADDRESS
        //*  ADDED FOR BENE IN THE PLAN (BIP) STARTS HERE....
        if (condition(annty_Actvty_Prap_View_Ap_Substitution_Contract_Ind.greater(" ")))                                                                                  //Natural: IF AP-SUBSTITUTION-CONTRACT-IND GT ' '
        {
            ldaAppbl170.getPnd_Mdm_Data_Last_Name().setValue(annty_Actvty_Prap_View_Ap_Cor_Last_Nme);                                                                     //Natural: ASSIGN #MDM-DATA.LAST_NAME := AP-COR-LAST-NME
            pnd_Date_Pnd_Datn.setValue(annty_Actvty_Prap_View_Ap_Dob);                                                                                                    //Natural: ASSIGN #DATN := AP-DOB
            pnd_Datd.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Date);                                                                                             //Natural: MOVE EDITED #DATE TO #DATD ( EM = MMDDYYYY )
            pnd_Date.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                             //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #DATE
            ldaAppbl170.getPnd_Mdm_Data_Date_Of_Birth().setValue(pnd_Date_Pnd_Datn);                                                                                      //Natural: ASSIGN #MDM-DATA.DATE_OF_BIRTH := #DATN
            if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,2).equals("IR")))                                                                      //Natural: IF SUBSTR ( AP-SGRD-SUBPLAN-NO,1,2 ) = 'IR'
            {
                ldaAppbl170.getPnd_Mdm_Data_Conversion_Type().setValue("1");                                                                                              //Natural: ASSIGN #MDM-DATA.CONVERSION_TYPE := '1'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,2).equals("RS") || annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No.getSubstring(1,       //Natural: IF SUBSTR ( AP-SGRD-SUBPLAN-NO,1,2 ) = 'RS' OR = 'RP'
                    2).equals("RP")))
                {
                    ldaAppbl170.getPnd_Mdm_Data_Conversion_Type().setValue("2");                                                                                          //Natural: ASSIGN #MDM-DATA.CONVERSION_TYPE := '2'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Date.setValueEdited(annty_Actvty_Prap_View_Ap_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED AP-TIAA-DOI ( EM = YYYYMMDD ) TO #DATE
            ldaAppbl170.getPnd_Mdm_Data_Status_Year().setValue(pnd_Date_Pnd_Date_Ccyy_N);                                                                                 //Natural: ASSIGN #MDM-DATA.STATUS_YEAR := #DATE-CCYY-N
            ldaAppbl170.getPnd_Mdm_Data_Default_Enroll().setValue("0");                                                                                                   //Natural: ASSIGN #MDM-DATA.DEFAULT_ENROLL := '0'
            ldaAppbl170.getPnd_Mdm_Data_Orig_Contract_Num().setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(4));                                         //Natural: ASSIGN #MDM-DATA.ORIG_CONTRACT_NUM := AP-CONTINGENT-BENE-INFO ( 4 )
            ldaAppbl170.getPnd_Mdm_Data_Orig_Cref_Num().setValue(annty_Actvty_Prap_View_Ap_Contingent_Bene_Info.getValue(5));                                             //Natural: ASSIGN #MDM-DATA.ORIG_CREF_NUM := AP-CONTINGENT-BENE-INFO ( 5 )
            ldaAppbl170.getPnd_Mdm_Data_Divsub_Num().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Divsub);                                                                     //Natural: ASSIGN #MDM-DATA.DIVSUB_NUM := AP-SGRD-DIVSUB
            //*  PARTICIPANT OWNED
            //*  DELAYED VESTED
            //*  INSTITUTIONALLY OWNED
            short decideConditionsMet1332 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF AP-OWNERSHIP;//Natural: VALUE 1
            if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(1))))
            {
                decideConditionsMet1332++;
                ldaAppbl170.getPnd_Mdm_Data_Ownership().setValue("0");                                                                                                    //Natural: ASSIGN #MDM-DATA.OWNERSHIP := '0'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(2))))
            {
                decideConditionsMet1332++;
                ldaAppbl170.getPnd_Mdm_Data_Ownership().setValue("2");                                                                                                    //Natural: ASSIGN #MDM-DATA.OWNERSHIP := '2'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((annty_Actvty_Prap_View_Ap_Ownership.equals(3))))
            {
                decideConditionsMet1332++;
                ldaAppbl170.getPnd_Mdm_Data_Ownership().setValue("1");                                                                                                    //Natural: ASSIGN #MDM-DATA.OWNERSHIP := '1'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET TO 'N'= NO
        if (condition(annty_Actvty_Prap_View_Ap_Decedent_Contract.greater(" ")))                                                                                          //Natural: IF AP-DECEDENT-CONTRACT GT ' '
        {
            ldaAppbl170.getPnd_Mdm_Data_Bip_Decedent_Contract().setValue(annty_Actvty_Prap_View_Ap_Decedent_Contract);                                                    //Natural: ASSIGN BIP_DECEDENT_CONTRACT := AP-DECEDENT-CONTRACT
            ldaAppbl170.getPnd_Mdm_Data_Bip_Relation_To_Decedent().setValue(annty_Actvty_Prap_View_Ap_Relation_To_Decedent);                                              //Natural: ASSIGN BIP_RELATION_TO_DECEDENT := AP-RELATION-TO-DECEDENT
            ldaAppbl170.getPnd_Mdm_Data_Bip_Ssn_Tin_Ind().setValue(annty_Actvty_Prap_View_Ap_Ssn_Tin_Ind);                                                                //Natural: ASSIGN BIP_SSN_TIN_IND := AP-SSN-TIN-IND
            ldaAppbl170.getPnd_Mdm_Data_Bip_Acceptance_Ind().setValue("N");                                                                                               //Natural: ASSIGN BIP_ACCEPTANCE_IND := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED FOR BENE IN THE PLAN (BIP) ENDS HERE.
    }
    private void sub_Write_Extract_Record() throws Exception                                                                                                              //Natural: WRITE-EXTRACT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppbl170.getPnd_Mdm_Data_Requestor().setValue("ACIS");                                                                                                         //Natural: ASSIGN REQUESTOR := 'ACIS'
        ldaAppbl170.getPnd_Mdm_Data_Function_Code().setValue("005");                                                                                                      //Natural: ASSIGN FUNCTION_CODE := '005'
        ldaAppbl170.getPnd_Mdm_Data_Source_System_Nm().setValue("ACIS");                                                                                                  //Natural: ASSIGN SOURCE_SYSTEM_NM := 'ACIS'
        ldaAppbl170.getPnd_Mdm_Data_Foreign_Soc_Sec_Nbr().setValue("000000000");                                                                                          //Natural: ASSIGN FOREIGN_SOC_SEC_NBR := '000000000'
        ldaAppbl170.getPnd_Mdm_Data_Contract_Party_Role().setValue(" ");                                                                                                  //Natural: ASSIGN CONTRACT_PARTY_ROLE := ' '
        pnd_Write_Count.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #WRITE-COUNT
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            getWorkFiles().write(1, false, ldaAppbl170.getPnd_Mdm_Data());                                                                                                //Natural: WRITE WORK FILE 1 #MDM-DATA
        }                                                                                                                                                                 //Natural: END-IF
        //*  PERFORM PRINT-MDM-DATA
    }
    private void sub_Print_Mdm_Data() throws Exception                                                                                                                    //Natural: PRINT-MDM-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "Record number ",pnd_Write_Count,"written to MDM file:",NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Requestor(),NEWLINE,"=",                    //Natural: WRITE 'Record number ' #WRITE-COUNT 'written to MDM file:' / '=' REQUESTOR / '=' FUNCTION_CODE / '=' SOURCE_SYSTEM_NM / '=' FOREIGN_SOC_SEC_NBR / '=' PH_UNIQUE_ID_NBR / '=' PH_SOCIAL_SECURITY_NO / '=' CNTRCT_NBR / '=' CNTRCT_PAYEE_CDE / '=' CONTRACT_PARTY_ROLE / '=' CNTRCT_STATUS_CDE / '=' CNTRCT_ISSUE_DTE / '=' CNTRCT_LOB_CDE / '=' CNTRCT_CREF_NBR / '=' CNTRCT_CREF_ISSUED_IND / '=' CNTRCT_INDCTRS_AND_CODES / '=' CNTRCT_OPTN_CDE / '=' CNTRCT_PLAN_CDE / '=' CNTRCT_FUND_CDE / '=' CNTRCT_SOCIAL_CDE / '=' CUST_AGREEMENT_RECEIVED_IND / '=' ASSET_ALLOCATION_SERVICE_IND
            ldaAppbl170.getPnd_Mdm_Data_Function_Code(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Source_System_Nm(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Foreign_Soc_Sec_Nbr(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Ph_Unique_Id_Nbr(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Ph_Social_Security_No(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Nbr(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Payee_Cde(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Contract_Party_Role(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Status_Cde(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Issue_Dte(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Lob_Cde(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Cref_Nbr(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Cref_Issued_Ind(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Indctrs_And_Codes(),NEWLINE,
            "=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Optn_Cde(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Plan_Cde(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Fund_Cde(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cntrct_Social_Cde(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cust_Agreement_Received_Ind(),NEWLINE,"=",
            ldaAppbl170.getPnd_Mdm_Data_Asset_Allocation_Service_Ind());
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaAppbl170.getPnd_Mdm_Data_Platform_Ind(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Multi_Source_Ind(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Tiaa_Mailed_Ind(), //Natural: WRITE '=' PLATFORM_IND / '=' MULTI_SOURCE_IND / '=' TIAA_MAILED_IND / '=' CREF_MAILED_IND / '=' ONLINE_ENROLLMENT_IND / '=' ADDRESS_LINE_1 / '=' ADDRESS_LINE_2 / '=' ADDRESS_LINE_3 / '=' ADDRESS_LINE_4 / '=' ADDRESS_LINE_5 / '=' ZIP_CODE / '=' #MDM-DATA.PLAN_NBR / '=' #MDM-DATA.SUB_PLAN_NBR
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Cref_Mailed_Ind(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Online_Enrollment_Ind(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Address_Line_1(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Address_Line_2(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Address_Line_3(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Address_Line_4(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Address_Line_5(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Zip_Code(),NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Plan_Nbr(),
            NEWLINE,"=",ldaAppbl170.getPnd_Mdm_Data_Sub_Plan_Nbr());
        if (Global.isEscape()) return;
    }
    private void sub_Check_Plan_If_Non_Tiaa_Admin() throws Exception                                                                                                      //Natural: CHECK-PLAN-IF-NON-TIAA-ADMIN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Non_T_C_Administered_Plan.reset();                                                                                                                            //Natural: RESET #NON-T-C-ADMINISTERED-PLAN
        DbsUtil.callnat(Scin8540.class , getCurrentProcessState(), annty_Actvty_Prap_View_Ap_Sgrd_Plan_No, pnd_Non_T_C_Administered_Plan);                                //Natural: CALLNAT 'SCIN8540' AP-SGRD-PLAN-NO #NON-T-C-ADMINISTERED-PLAN
        if (condition(Global.isEscape())) return;
        //*  MAYO-PPG
    }
    private void sub_Set_Issue_Date() throws Exception                                                                                                                    //Natural: SET-ISSUE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(annty_Actvty_Prap_View_Ap_T_Doi.greater(getZero())))                                                                                                //Natural: IF AP-T-DOI GT 0
        {
            pnd_Hold_T_Doi.setValue(annty_Actvty_Prap_View_Ap_T_Doi);                                                                                                     //Natural: MOVE AP-T-DOI TO #HOLD-T-DOI
            if (condition(pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Yy.less(30)))                                                                                                     //Natural: IF #HOLD-T-DOI-YY < 30
            {
                pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Cc.setValue(20);                                                                                                                //Natural: ASSIGN #AP-T-DOI-CC := 20
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Cc.setValue(19);                                                                                                                //Natural: ASSIGN #AP-T-DOI-CC := 19
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Yy.setValue(pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Yy);                                                                                      //Natural: MOVE #HOLD-T-DOI-YY TO #AP-T-DOI-YY
            pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Mm.setValue(pnd_Hold_T_Doi_Pnd_Hold_T_Doi_Mm);                                                                                      //Natural: MOVE #HOLD-T-DOI-MM TO #AP-T-DOI-MM
            pnd_Ap_T_Doi_Pnd_Ap_T_Doi_Dd.setValue(1);                                                                                                                     //Natural: MOVE 01 TO #AP-T-DOI-DD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(annty_Actvty_Prap_View_Ap_C_Doi.greater(getZero())))                                                                                                //Natural: IF AP-C-DOI GT 0
        {
            pnd_Hold_C_Doi.setValue(annty_Actvty_Prap_View_Ap_C_Doi);                                                                                                     //Natural: MOVE AP-C-DOI TO #HOLD-C-DOI
            if (condition(pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Yy.less(30)))                                                                                                     //Natural: IF #HOLD-C-DOI-YY < 30
            {
                pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Cc.setValue(20);                                                                                                                //Natural: ASSIGN #AP-C-DOI-CC := 20
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Cc.setValue(19);                                                                                                                //Natural: ASSIGN #AP-C-DOI-CC := 19
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Yy.setValue(pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Yy);                                                                                      //Natural: MOVE #HOLD-C-DOI-YY TO #AP-C-DOI-YY
            pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Mm.setValue(pnd_Hold_C_Doi_Pnd_Hold_C_Doi_Mm);                                                                                      //Natural: MOVE #HOLD-C-DOI-MM TO #AP-C-DOI-MM
            pnd_Ap_C_Doi_Pnd_Ap_C_Doi_Dd.setValue(1);                                                                                                                     //Natural: MOVE 01 TO #AP-C-DOI-DD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Original_Cor_Issue_Date() throws Exception                                                                                                       //Natural: GET-ORIGINAL-COR-ISSUE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  RESET #MDMA210                           /* CORMDM          /*PINE
        //*  CORMDM          /*PINE
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        pnd_Orig_Cor_Issue_Date.reset();                                                                                                                                  //Natural: RESET #ORIG-COR-ISSUE-DATE
        //*  RS/RSP COMPANION
        //*  NON RS/RSP COMPANION
        //* PINE
        //* PINE
        //* PINE
        short decideConditionsMet1421 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN AP-OIA-IND = '10' OR ( AP-T-DOI GT 0 AND AP-C-DOI = 0 )
        if (condition((annty_Actvty_Prap_View_Ap_Oia_Ind.equals("10") || (annty_Actvty_Prap_View_Ap_T_Doi.greater(getZero()) && annty_Actvty_Prap_View_Ap_C_Doi.equals(getZero())))))
        {
            decideConditionsMet1421++;
            pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                            //Natural: ASSIGN #MDMA211.#I-CONTRACT-NUMBER := AP-TIAA-CNTRCT
            pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                //Natural: ASSIGN #MDMA211.#I-TIAA-CREF-IND := 'T'
            pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(1);                                                                                                     //Natural: ASSIGN #MDMA211.#I-PAYEE-CODE := 01
            //*    WRITE 'GOING TO MDMN210A' *TIMX                          /*PINE
            //* PINE
            getReports().write(0, "GOING TO MDMN211A",Global.getTIMX());                                                                                                  //Natural: WRITE 'GOING TO MDMN211A' *TIMX
            if (Global.isEscape()) return;
            //*    CALLNAT 'MDMN210A' #MDMA210                              /*PINE
            //* PINE
            DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                      //Natural: CALLNAT 'MDMN211A' #MDMA211
            if (condition(Global.isEscape())) return;
            //*    WRITE 'BACK FROM MDMN210A' *TIMX                         /*PINE
            //* PINE
            getReports().write(0, "BACK FROM MDMN211A",Global.getTIMX());                                                                                                 //Natural: WRITE 'BACK FROM MDMN211A' *TIMX
            if (Global.isEscape()) return;
            //*    IF #MDMA210.#O-RETURN-CODE NE '0000'                     /*PINE
            //* PINE
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
            {
                ignore();
                //* PIN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Orig_Cor_Issue_Date.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Issue_Date());                                                                  //Natural: ASSIGN #ORIG-COR-ISSUE-DATE := #MDMA211.#O-CONTRACT-ISSUE-DATE
                //*  END CORMDM
                //*  RS/RSP COMPANION
                //*  NON RS/RSP COMPANION
                //* PINE
                //* PINE
                //* PINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN AP-OIA-IND = '11' OR ( AP-T-DOI = 0 AND AP-C-DOI GT 0 )
        else if (condition((annty_Actvty_Prap_View_Ap_Oia_Ind.equals("11") || (annty_Actvty_Prap_View_Ap_T_Doi.equals(getZero()) && annty_Actvty_Prap_View_Ap_C_Doi.greater(getZero())))))
        {
            decideConditionsMet1421++;
            pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(annty_Actvty_Prap_View_Ap_Cref_Cert);                                                              //Natural: ASSIGN #MDMA211.#I-CONTRACT-NUMBER := AP-CREF-CERT
            pdaMdma211.getPnd_Mdma211_Pnd_I_Tiaa_Cref_Ind().setValue("C");                                                                                                //Natural: ASSIGN #MDMA211.#I-TIAA-CREF-IND := 'C'
            pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code().setValue(1);                                                                                                     //Natural: ASSIGN #MDMA211.#I-PAYEE-CODE := 01
            //*    WRITE 'GOING TO MDMN210A' *TIMX                           /*PINE
            //* PINE
            getReports().write(0, "GOING TO MDMN211A",Global.getTIMX());                                                                                                  //Natural: WRITE 'GOING TO MDMN211A' *TIMX
            if (Global.isEscape()) return;
            //*    CALLNAT 'MDMN210A' #MDMA210                               /*PINE
            //* PINE
            DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                      //Natural: CALLNAT 'MDMN211A' #MDMA211
            if (condition(Global.isEscape())) return;
            //*    WRITE 'BACK FROM MDMN210A' *TIMX                          /*PINE
            //* PINE
            getReports().write(0, "BACK FROM MDMN211A",Global.getTIMX());                                                                                                 //Natural: WRITE 'BACK FROM MDMN211A' *TIMX
            if (Global.isEscape()) return;
            //*    IF #MDMA210.#O-RETURN-CODE NE '0000'                      /*PINE
            //* PINE
            if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
            {
                ignore();
                //* PINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Orig_Cor_Issue_Date.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Issue_Date());                                                                  //Natural: ASSIGN #ORIG-COR-ISSUE-DATE := #MDMA211.#O-CONTRACT-ISSUE-DATE
                //*  END CORMDM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  GET-ORIGINAL-COR-ISSUE-DATE
    }
    private void sub_Update_Next_Extract_Date() throws Exception                                                                                                          //Natural: UPDATE-NEXT-EXTRACT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        //*  MOVE NEXT EXTRACT DATE/TIME TO WORK FILE
        pnd_Next_Extract_Date_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYYMMDD' 'HHIISST"));                                                             //Natural: MOVE EDITED *TIMX ( EM = YYYYMMDD' 'HHIISST ) TO #NEXT-EXTRACT-DATE-TIME
        getWorkFiles().write(4, false, pnd_Next_Extract_Date_Time);                                                                                                       //Natural: WRITE WORK FILE 4 #NEXT-EXTRACT-DATE-TIME
        getReports().write(0, "OUTPUT",pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Date,pnd_Next_Extract_Date_Time_Pnd_Next_Extract_Time, new ReportEditMask              //Natural: WRITE 'OUTPUT' #NEXT-EXTRACT-DATE #NEXT-EXTRACT-TIME ( EM = 99:99:99:9 ) ' AS THE NEXT DATE & TIME TO GO AGAINST PRAP'
            ("99:99:99:9")," AS THE NEXT DATE & TIME TO GO AGAINST PRAP");
        if (Global.isEscape()) return;
    }
    private void sub_Update_Prap_File() throws Exception                                                                                                                  //Natural: UPDATE-PRAP-FILE
    {
        if (BLNatReinput.isReinput()) return;

        L_GET:                                                                                                                                                            //Natural: GET ANNTY-ACTVTY-PRAP-VIEW #HOLD-ISN
        vw_annty_Actvty_Prap_View.readByID(pnd_Hold_Isn.getLong(), "L_GET");
        annty_Actvty_Prap_View_Ap_Eft_Requested_Ind.setValue("Y");                                                                                                        //Natural: ASSIGN AP-EFT-REQUESTED-IND := 'Y'
        vw_annty_Actvty_Prap_View.updateDBRow("L_GET");                                                                                                                   //Natural: UPDATE ( L-GET. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    //*  CORMDM
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        getReports().write(0, "INVOKING OPEN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                     //Natural: WRITE 'INVOKING OPEN :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "FINISHED OPEN:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                      //Natural: WRITE 'FINISHED OPEN:' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  JRB2
        //*  JRB2
        //*  JRB2
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE // 'CONTRACT:' 25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),annty_Actvty_Prap_View_Ap_Pin_Nbr);
        //*  JRB2
        getReports().write(0, "EXTRACT RECORDS WRITTEN:",pnd_Write_Count, new ReportEditMask ("ZZZ,ZZ9"));                                                                //Natural: WRITE 'EXTRACT RECORDS WRITTEN:' #WRITE-COUNT ( EM = ZZZ,ZZ9 )
        //*  JRB2
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=52");
    }
}
