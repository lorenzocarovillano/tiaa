/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:35 PM
**        * FROM NATURAL PROGRAM : Appb3330
************************************************************
**        * FILE NAME            : Appb3330.java
**        * CLASS NAME           : Appb3330
**        * INSTANCE NAME        : Appb3330
************************************************************
**********************************************************************
* PROGRAM NAME : APPB3330                                            *
* DATE WRITTEN : APRIL 26, 2004                                      *
* DESCRIPTION  : THIS PROGRAM WILL READ THE EXTRACT FILE OF SUNGARD  *
*                CONTRACT OVER 180 DAYS OLD AND CREATE A REPORT.     *
* UPDATED      :                                                     *
* 04/26/04 K.GATES - PROGRAM CREATION                                *
* 08/28/08 N.CVETKOVIC- RHSP CHANGES NBC                             *
* 03/10/10 C.AVE   - INCLUDED IRA INDEXED PRODUCTS - TIGR            *
* 06/19/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.*
**********************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb3330 extends BLNatBase
{
    // Data Areas
    private LdaAppl3320 ldaAppl3320;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Plan_Total;
    private DbsField pnd_Debug;
    private DbsField pnd_Prev_Plan_No;
    private DbsField pnd_Prev_Subplan_No;
    private DbsField pnd_Prev_Soc_Sec;
    private DbsField pnd_Ap_Name;
    private DbsField pnd_Date_Of_Birth;

    private DbsGroup pnd_Date_Of_Birth__R_Field_1;
    private DbsField pnd_Date_Of_Birth_Pnd_Date_Of_Birth_N;
    private DbsField pnd_Ap_Date_Of_Birth;
    private DbsField pnd_Remarks;
    private DbsField pnd_Contract_Type;
    private DbsField pnd_Line_Count;
    private DbsField pnd_Total_Recs_Read;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl3320 = new LdaAppl3320();
        registerRecord(ldaAppl3320);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Plan_Total = localVariables.newFieldInRecord("pnd_Plan_Total", "#PLAN-TOTAL", FieldType.NUMERIC, 5);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Prev_Plan_No = localVariables.newFieldInRecord("pnd_Prev_Plan_No", "#PREV-PLAN-NO", FieldType.STRING, 6);
        pnd_Prev_Subplan_No = localVariables.newFieldInRecord("pnd_Prev_Subplan_No", "#PREV-SUBPLAN-NO", FieldType.STRING, 6);
        pnd_Prev_Soc_Sec = localVariables.newFieldInRecord("pnd_Prev_Soc_Sec", "#PREV-SOC-SEC", FieldType.NUMERIC, 9);
        pnd_Ap_Name = localVariables.newFieldInRecord("pnd_Ap_Name", "#AP-NAME", FieldType.STRING, 40);
        pnd_Date_Of_Birth = localVariables.newFieldInRecord("pnd_Date_Of_Birth", "#DATE-OF-BIRTH", FieldType.STRING, 8);

        pnd_Date_Of_Birth__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_Of_Birth__R_Field_1", "REDEFINE", pnd_Date_Of_Birth);
        pnd_Date_Of_Birth_Pnd_Date_Of_Birth_N = pnd_Date_Of_Birth__R_Field_1.newFieldInGroup("pnd_Date_Of_Birth_Pnd_Date_Of_Birth_N", "#DATE-OF-BIRTH-N", 
            FieldType.NUMERIC, 8);
        pnd_Ap_Date_Of_Birth = localVariables.newFieldInRecord("pnd_Ap_Date_Of_Birth", "#AP-DATE-OF-BIRTH", FieldType.DATE);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 8);
        pnd_Contract_Type = localVariables.newFieldInRecord("pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 7);
        pnd_Line_Count = localVariables.newFieldInRecord("pnd_Line_Count", "#LINE-COUNT", FieldType.NUMERIC, 3);
        pnd_Total_Recs_Read = localVariables.newFieldInRecord("pnd_Total_Recs_Read", "#TOTAL-RECS-READ", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl3320.initializeValues();

        localVariables.reset();
        pnd_Line_Count.setInitialValue(0);
        pnd_Total_Recs_Read.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb3330() throws Exception
    {
        super("Appb3330");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 58 ZP = OFF
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 SGRD-EXT-FILE
        while (condition(getWorkFiles().read(1, ldaAppl3320.getSgrd_Ext_File())))
        {
            pnd_Total_Recs_Read.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-RECS-READ
            if (condition(pnd_Total_Recs_Read.equals(1)))                                                                                                                 //Natural: IF #TOTAL-RECS-READ = 1
            {
                pnd_Prev_Plan_No.setValue(ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Plan_No());                                                                                //Natural: ASSIGN #PREV-PLAN-NO = EX-SGRD-PLAN-NO
                pnd_Prev_Subplan_No.setValue(ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Subplan_No());                                                                          //Natural: ASSIGN #PREV-SUBPLAN-NO = EX-SGRD-SUBPLAN-NO
                                                                                                                                                                          //Natural: PERFORM REPORT-HEADING
                sub_Report_Heading();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Plan_No().equals(pnd_Prev_Plan_No)))                                                                       //Natural: IF EX-SGRD-PLAN-NO = #PREV-PLAN-NO
            {
                pnd_Plan_Total.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PLAN-TOTAL
                if (condition(pnd_Line_Count.greater(46)))                                                                                                                //Natural: IF #LINE-COUNT > 46
                {
                                                                                                                                                                          //Natural: PERFORM REPORT-HEADING
                    sub_Report_Heading();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-RECORD
                sub_Write_Detail_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM TOTAL-BY-PLAN
                sub_Total_By_Plan();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Plan_Total.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PLAN-TOTAL
                                                                                                                                                                          //Natural: PERFORM REPORT-HEADING
                sub_Report_Heading();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-RECORD
                sub_Write_Detail_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Plan_No.setValue(ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Plan_No());                                                                                //Natural: ASSIGN #PREV-PLAN-NO = EX-SGRD-PLAN-NO
                pnd_Prev_Subplan_No.setValue(ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Subplan_No());                                                                          //Natural: ASSIGN #PREV-SUBPLAN-NO = EX-SGRD-SUBPLAN-NO
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //*  PRAP APPL
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM TOTAL-BY-PLAN
            sub_Total_By_Plan();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GRAND-TOTAL
            sub_Grand_Total();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-HEADING
        //*  37T 'CONTRACT #'   48T 'LOB'        57T 'DATE OF BIRTH'
        //*  71T 'POLICYHOLDER NAME'            113T 'REMARKS' /
        //*  1T '--------'  10T '-------'  18T '---------'  28T '-------'
        //*  37T '----------'   48T '-------'    57T '-' (13)
        //*  71T '-' (40)       113T '--------'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-RECORD
        //*  37T  EX-TIAA-CNTRCT
        //*  48T  #CONTRACT-TYPE
        //*  57T  #AP-DATE-OF-BIRTH (EM=MM/DD/YYYY)
        //*  71T  #AP-NAME
        //*  113T  #REMARKS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-BY-PLAN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRAND-TOTAL
    }
    private void sub_Report_Heading() throws Exception                                                                                                                    //Natural: REPORT-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Line_Count.reset();                                                                                                                                           //Natural: RESET #LINE-COUNT
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(24),"TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA",new  //Natural: WRITE ( 1 ) NOTITLE // 1T *PROGRAM 24T 'TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA' 102T 'RUN DATE' *DATU / 35T 'COLLEGE RETIREMENT  EQUITIES FUND' 102T 'PAGE NO.' *PAGE-NUMBER ( 1 ) // 31T 'DAILY PRAP SUNGARD APPLICATION WITHDRAWAL' // 1T 'PLAN NUMBER - ' EX-SGRD-PLAN-NO // 1T 'SUB PLAN' 10T 'DIV SUB' 18T 'SOC SEC' 28T 'PIN #' 42T 'CONTRACT #' 53T 'LOB' 62T 'DATE OF BIRTH' 76T 'POLICYHOLDER NAME' 118T 'REMARKS' / 1T '--------' 10T '-------' 18T '---------' 28T '------------' 42T '----------' 53T '-------' 62T '-' ( 13 ) 76T '-' ( 40 ) 118T '--------'
            TabSetting(102),"RUN DATE",Global.getDATU(),NEWLINE,new TabSetting(35),"COLLEGE RETIREMENT  EQUITIES FUND",new TabSetting(102),"PAGE NO.",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
            TabSetting(31),"DAILY PRAP SUNGARD APPLICATION WITHDRAWAL",NEWLINE,NEWLINE,new TabSetting(1),"PLAN NUMBER - ",ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Plan_No(),NEWLINE,NEWLINE,new 
            TabSetting(1),"SUB PLAN",new TabSetting(10),"DIV SUB",new TabSetting(18),"SOC SEC",new TabSetting(28),"PIN #",new TabSetting(42),"CONTRACT #",new 
            TabSetting(53),"LOB",new TabSetting(62),"DATE OF BIRTH",new TabSetting(76),"POLICYHOLDER NAME",new TabSetting(118),"REMARKS",NEWLINE,new TabSetting(1),"--------",new 
            TabSetting(10),"-------",new TabSetting(18),"---------",new TabSetting(28),"------------",new TabSetting(42),"----------",new TabSetting(53),"-------",new 
            TabSetting(62),"-",new RepeatItem(13),new TabSetting(76),"-",new RepeatItem(40),new TabSetting(118),"--------");
        if (Global.isEscape()) return;
    }
    private void sub_Write_Detail_Record() throws Exception                                                                                                               //Natural: WRITE-DETAIL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_Ap_Name.reset();                                                                                                                                              //Natural: RESET #AP-NAME #AP-DATE-OF-BIRTH
        pnd_Ap_Date_Of_Birth.reset();
        pnd_Ap_Name.setValue(DbsUtil.compress(ldaAppl3320.getSgrd_Ext_File_Ex_Cor_Last_Name(), ",", ldaAppl3320.getSgrd_Ext_File_Ex_Cor_First_Name()));                   //Natural: COMPRESS EX-COR-LAST-NAME ',' EX-COR-FIRST-NAME INTO #AP-NAME
        if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Dob().notEquals(getZero())))                                                                                        //Natural: IF EX-DOB NE 0
        {
            pnd_Date_Of_Birth_Pnd_Date_Of_Birth_N.setValue(ldaAppl3320.getSgrd_Ext_File_Ex_Dob());                                                                        //Natural: ASSIGN #DATE-OF-BIRTH-N = EX-DOB
            pnd_Ap_Date_Of_Birth.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Date_Of_Birth);                                                                        //Natural: MOVE EDITED #DATE-OF-BIRTH TO #AP-DATE-OF-BIRTH ( EM = MMDDYYYY )
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet155 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF EX-APPLCNT-REQ-TYPE;//Natural: VALUE 'P'
        if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("P"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("PAPER   ");                                                                                                                             //Natural: ASSIGN #REMARKS = 'PAPER   '
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("I"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("INTERNET");                                                                                                                             //Natural: ASSIGN #REMARKS = 'INTERNET'
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("R"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("REGULAR");                                                                                                                              //Natural: ASSIGN #REMARKS = 'REGULAR'
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("N"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("NEGATIVE");                                                                                                                             //Natural: ASSIGN #REMARKS = 'NEGATIVE'
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("X"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("REMT/ENR");                                                                                                                             //Natural: ASSIGN #REMARKS = 'REMT/ENR'
        }                                                                                                                                                                 //Natural: VALUE 'O'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("O"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("ERS/ODE ");                                                                                                                             //Natural: ASSIGN #REMARKS = 'ERS/ODE '
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("L"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("LOAN/ENR");                                                                                                                             //Natural: ASSIGN #REMARKS = 'LOAN/ENR'
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("E"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("EAS/ENR ");                                                                                                                             //Natural: ASSIGN #REMARKS = 'EAS/ENR '
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("A"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("ADM/INET");                                                                                                                             //Natural: ASSIGN #REMARKS = 'ADM/INET'
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("B"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("BULK/ENR");                                                                                                                             //Natural: ASSIGN #REMARKS = 'BULK/ENR'
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((ldaAppl3320.getSgrd_Ext_File_Ex_Applcnt_Req_Type().equals("C"))))
        {
            decideConditionsMet155++;
            pnd_Remarks.setValue("IND/INET");                                                                                                                             //Natural: ASSIGN #REMARKS = 'IND/INET'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Remarks.setValue("UNKNOWN");                                                                                                                              //Natural: ASSIGN #REMARKS = 'UNKNOWN'
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet182 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EX-LOB = 'D'
        if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob().equals("D")))
        {
            decideConditionsMet182++;
            short decideConditionsMet184 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EX-LOB-TYPE = '2' OR = '8'
            if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("2") || ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("8")))
            {
                decideConditionsMet184++;
                pnd_Contract_Type.setValue("RA");                                                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'RA'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '7' OR = '9'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("7") || ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("9")))
            {
                decideConditionsMet184++;
                pnd_Contract_Type.setValue("GRA");                                                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'GRA'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE EQ '5'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("5")))
            {
                decideConditionsMet184++;
                pnd_Contract_Type.setValue("RS");                                                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'RS'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE EQ '6'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("6")))
            {
                decideConditionsMet184++;
                pnd_Contract_Type.setValue("RL");                                                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'RL'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE EQ 'A'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("A")))
            {
                decideConditionsMet184++;
                pnd_Contract_Type.setValue("RC");                                                                                                                         //Natural: ASSIGN #CONTRACT-TYPE = 'RC'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE EQ 'B'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("B")))
            {
                decideConditionsMet184++;
                //*  NBC
                pnd_Contract_Type.setValue("RHSP");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'RHSP'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Contract_Type.setValue("UNKNOWN");                                                                                                                    //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN EX-LOB = 'S'
        else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob().equals("S")))
        {
            decideConditionsMet182++;
            short decideConditionsMet202 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EX-LOB-TYPE = '2' OR = '8'
            if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("2") || ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("8")))
            {
                decideConditionsMet202++;
                pnd_Contract_Type.setValue("SRA");                                                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'SRA'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '3'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("3")))
            {
                decideConditionsMet202++;
                pnd_Contract_Type.setValue("GSRA");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'GSRA'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '5'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("5")))
            {
                decideConditionsMet202++;
                pnd_Contract_Type.setValue("RSP");                                                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'RSP'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '6'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("6")))
            {
                decideConditionsMet202++;
                pnd_Contract_Type.setValue("RSP2");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'RSP2'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '7'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("7")))
            {
                decideConditionsMet202++;
                pnd_Contract_Type.setValue("RCP");                                                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'RCP'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Contract_Type.setValue("UNKNOWN");                                                                                                                    //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN EX-LOB = 'I'
        else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob().equals("I")))
        {
            decideConditionsMet182++;
            short decideConditionsMet217 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EX-LOB-TYPE = '1' OR = '2'
            if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("1") || ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("2")))
            {
                decideConditionsMet217++;
                pnd_Contract_Type.setValue("IRA");                                                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'IRA'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '3'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("3")))
            {
                decideConditionsMet217++;
                pnd_Contract_Type.setValue("IRAR");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'IRAR'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '4'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("4")))
            {
                decideConditionsMet217++;
                pnd_Contract_Type.setValue("IRAC");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'IRAC'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '6'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("6")))
            {
                decideConditionsMet217++;
                pnd_Contract_Type.setValue("IRAS");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'IRAS'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '5'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("5")))
            {
                decideConditionsMet217++;
                //*  TIGR-START
                pnd_Contract_Type.setValue("KEOG");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'KEOG'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '7'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("7")))
            {
                decideConditionsMet217++;
                pnd_Contract_Type.setValue("IRIR");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'IRIR'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '8'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("8")))
            {
                decideConditionsMet217++;
                pnd_Contract_Type.setValue("IRIC");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'IRIC'
            }                                                                                                                                                             //Natural: WHEN EX-LOB-TYPE = '9'
            else if (condition(ldaAppl3320.getSgrd_Ext_File_Ex_Lob_Type().equals("9")))
            {
                decideConditionsMet217++;
                //*  TIGR-END
                pnd_Contract_Type.setValue("IRIS");                                                                                                                       //Natural: ASSIGN #CONTRACT-TYPE = 'IRIS'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Contract_Type.setValue("UNKNOWN");                                                                                                                    //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Contract_Type.setValue("UNKNOWN");                                                                                                                        //Natural: ASSIGN #CONTRACT-TYPE = 'UNKNOWN'
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        //* PINE
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Subplan_No(),new TabSetting(12),ldaAppl3320.getSgrd_Ext_File_Ex_Sgrd_Divsub(),new  //Natural: WRITE ( 1 ) / 2T EX-SGRD-SUBPLAN-NO 12T EX-SGRD-DIVSUB 18T EX-SOC-SEC 28T EX-PIN-NBR 42T EX-TIAA-CNTRCT 53T #CONTRACT-TYPE 62T #AP-DATE-OF-BIRTH ( EM = MM/DD/YYYY ) 76T #AP-NAME 118T #REMARKS
            TabSetting(18),ldaAppl3320.getSgrd_Ext_File_Ex_Soc_Sec(),new TabSetting(28),ldaAppl3320.getSgrd_Ext_File_Ex_Pin_Nbr(),new TabSetting(42),ldaAppl3320.getSgrd_Ext_File_Ex_Tiaa_Cntrct(),new 
            TabSetting(53),pnd_Contract_Type,new TabSetting(62),pnd_Ap_Date_Of_Birth, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(76),pnd_Ap_Name,new 
            TabSetting(118),pnd_Remarks);
        if (Global.isEscape()) return;
        pnd_Line_Count.nadd(3);                                                                                                                                           //Natural: ADD 3 TO #LINE-COUNT
    }
    private void sub_Total_By_Plan() throws Exception                                                                                                                     //Natural: TOTAL-BY-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(pnd_Line_Count.greater(45)))                                                                                                                        //Natural: IF #LINE-COUNT > 45
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(24),"TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA",new  //Natural: WRITE ( 1 ) // 1T *PROGRAM 24T 'TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA' 102T 'RUN DATE' *DATU / 35T 'COLLEGE RETIREMENT  EQUITIES FUND' 102T 'PAGE NO.' *PAGE-NUMBER ( 1 ) // 31T 'DAILY PRAP SUNGARD APPLICATION WITHDRAWAL' // 1T 'PLAN NUMBER - ' #PREV-PLAN-NO //
                TabSetting(102),"RUN DATE",Global.getDATU(),NEWLINE,new TabSetting(35),"COLLEGE RETIREMENT  EQUITIES FUND",new TabSetting(102),"PAGE NO.",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
                TabSetting(31),"DAILY PRAP SUNGARD APPLICATION WITHDRAWAL",NEWLINE,NEWLINE,new TabSetting(1),"PLAN NUMBER - ",pnd_Prev_Plan_No,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(10),"TOTAL BY PLAN         : ",pnd_Plan_Total,NEWLINE,new TabSetting(10),       //Natural: WRITE ( 1 ) /// 10T 'TOTAL BY PLAN         : ' #PLAN-TOTAL / 10T '---------------------------------'
            "---------------------------------");
        if (Global.isEscape()) return;
        pnd_Plan_Total.reset();                                                                                                                                           //Natural: RESET #PLAN-TOTAL
    }
    private void sub_Grand_Total() throws Exception                                                                                                                       //Natural: GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(24),"TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA",new  //Natural: WRITE ( 1 ) // 1T *PROGRAM 24T 'TEACHERS INSURANCE AND ANNUITY ASSOCIATION OF AMERICA' 102T 'RUN DATE' *DATU / 35T 'COLLEGE RETIREMENT  EQUITIES FUND' 102T 'PAGE NO.' *PAGE-NUMBER ( 1 ) // 31T 'DAILY PRAP SUNGARD APPLICATION WITHDRAWAL' //// 10T '        CONTROL TOTALS ' / 10T '-----------------------------------' // 10T 'TOTAL PRAP APPL READ  : ' #TOTAL-RECS-READ
            TabSetting(102),"RUN DATE",Global.getDATU(),NEWLINE,new TabSetting(35),"COLLEGE RETIREMENT  EQUITIES FUND",new TabSetting(102),"PAGE NO.",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
            TabSetting(31),"DAILY PRAP SUNGARD APPLICATION WITHDRAWAL",NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(10),"        CONTROL TOTALS ",NEWLINE,new 
            TabSetting(10),"-----------------------------------",NEWLINE,NEWLINE,new TabSetting(10),"TOTAL PRAP APPL READ  : ",pnd_Total_Recs_Read);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=58 ZP=OFF");
    }
}
