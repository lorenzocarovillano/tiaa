/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:13 PM
**        * FROM NATURAL PROGRAM : Appb301
************************************************************
**        * FILE NAME            : Appb301.java
**        * CLASS NAME           : Appb301
**        * INSTANCE NAME        : Appb301
************************************************************
************************************************************************
* PROGRAM  : APPB301 - PRAP PURGE PROCESS                              *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : MAY 20, 1999                                              *
* FUNCTION : READS WORK FILE CREATED BY APPB300 AND ACCEPTS OPEN STATUS*
*            TO BE USED BY UTILITY P1020DBD AND OVERDUE LETTERS        *
* UPDATED  : 09/05/00 V.RAQUENO   - USE SINGLE FUNCTION TO CALL IIS(VR1)
*          : 02/16/02 K.GATES     - NEW LAYOUT OF APPL300 FOR OIA      *
*          : 02/10/04 SINGLETON   - NEW LAYOUT OF APPL300 FOR SGRD     *
*          : 03/26/04 K.GATES     - HARDCODED CONTACT INFO FOR SGRD    *
*                                   RELEASE 2                          *
*                                 - ADDED SUNGARD FIELDS TO APPL302    *
*          : 08/20/04 R.WILLIS    - CHANGES FOR RELEASE 3. API CALL    *
*                                   FOR CONTACT INFORMATION            *
*          : 10/11/04 R.WILLIS    - CHANGES FOR RELEASE 3. INCLUDE     *
*                                   RELEASE IND 7 RECORDS TO BE KEPT   *
*  (REL5.) : 04/07/05 R.WILLIS    - CHANGES FOR RELEASE 5.             *
*          : 04/07/05 K.GATES     - CHANGES FOR REL5 CONTACTS  SGRD5 KG*
* IM172577 : 07/20/05 J.BERGHEISER  CHANGED TO CHECK ISSUE DATE FOR    *
*                                   MASK MMYY BEFORE DOING MOVE EDITED *
*          : 12/09/05 K.GATES     - NEW LAYOUT OF APPL300 FOR SGRD (R8)*
*          : 07/28/06 K.GATES     - PROD FIX FOR PRAP PURGE ISSUE      *
*                                   SEE K.GATES 7/28/06                *
*          : 07/14/06 K.GATES     - NEW LAYOUT OF APPL300 FOR SGRD(ARR)*
*          : 09/26/06 K.GATES     - ADDED DIV/SUB TO LETTER FILE FOR   *
*                                   DCA      SEE DCA KG                *
*          : 12/30/06 K.GATES     - BYPASS CONTACT CALL FOR OMNI       *
*                                   CONTRACTS   SEE EMER KG            *
*          : 01/08/07 K.GATES     - CHANGES FOR SUNY CCR               *
*                                 - SEPERATED PRAP PURGE AND LETTER    *
*                                   PROCESSES BY COMMENTING OUT OVERDUE*
*                                   CODE MOVED TO APPB320  SEE SUNY KG *
*          : 05/03/07 K.GATES     - NEW LAYOUT OF APPL300 FOR ARR2     *
*          : 09/23/08 DEVELBISS   - NEW LAYOUT OF APPL300 FOR AUTO ENRL*
*          : 08/12/09 C. AVE      - RE-STOW NEW LAYOUT OF APPL301 FOR  *
*                                   ACIS PERF. MANAGEMENT CONTROL PROJ.*
*          : 03/10/10 C. AVE      - INCLUDED IRA INDEXED PRODUCTS-TIGR *
*          : 05/02/11 C. SCHNEIDER- RE-STOWED TO PICK UP CHANGES IN    *
*                                   APPL300                      (JHU) *
*          : 06/27/11 C. SCHNEIDER- RE-STOWED TO PICK UP CHANGES IN    *
*                                   APPL300                      (TIC) *
*          : 11/17/12 L. SHU      - RE-STOWED TO PICK UP CHANGES IN    *
*                                   APPL300                      (LPOA)*
*          : 07/18/13 L.SHU - RESTOW FOR APPL300 FOR THE NEW IRA       *
*                     SUBSTITUTION FIELDS                              *
*          : 09/12/13 B.NEWSOM - RESTOW FOR CV-NON-PROPRIETARY-PKG-IND *
*                                ADDED TO APPL300.              (MTSIN)*
*            09/01/15 L. SHU   - RESTOW FOR NEW FIELDS IN APPL300      *
*                                FOR BENE IN THE PLAN.          (BIP)  *
*            11/01/16 L. SHU   - RESTOW FOR CV-ONEIRA-ACCT-NO IN       *
*                                APPL300 FOR ONEIRA             (1IRA) *
*            06/19/17 BABRE - PIN EXPANSION CHANGES.CHG425939 STOW ONLY*
*            11/16/19 B.NEWSOM ADDED THE EXTENDED AREA OF APPL300- IISG*
*                              COMMENTED IIS OBSOLETE CODE         LS1 *
* **********************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb301 extends BLNatBase
{
    // Data Areas
    private PdaAciadate pdaAciadate;
    private LdaAppl300 ldaAppl300;
    private LdaAppl302 ldaAppl302;
    private PdaAcia2100 pdaAcia2100;
    private PdaScia8200 pdaScia8200;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Data;
    private DbsField pnd_Date;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt;

    private DbsGroup pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Appl;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem;
    private DbsField pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Del_Ovd;
    private DbsField pnd_Debug;
    private DbsField pnd_Contact;
    private DbsField pnd_I;
    private DbsField pnd_Temp_Cntr;

    private DbsGroup pnd_Temp_Cntr__R_Field_2;
    private DbsField pnd_Temp_Cntr_Pnd_Cntr_Pref;
    private DbsField pnd_Temp_Cntr_Pnd_Cntr_Nbr;
    private DbsField pnd_Temp_C_Cntr;

    private DbsGroup pnd_Temp_C_Cntr__R_Field_3;
    private DbsField pnd_Temp_C_Cntr_Pnd_Cntr_C_Pref;
    private DbsField pnd_Temp_C_Cntr_Pnd_Cntr_C_Nbr;
    private DbsField pnd_Sgrd;
    private DbsField pnd_Hold_Plan;
    private DbsField pnd_Sgrd_Call_Cnt;
    private DbsField pnd_File_Open_Sw;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Pnd_Wk_Compressed_Nme;
    private DbsField pnd_Work_Pnd_Wk_F_Name;
    private DbsField pnd_Work_Pnd_Wk_L_Name;
    private DbsField pnd_Work_Pnd_Wk_M_Name;
    private DbsField pnd_Work_Pnd_Wk_Zip;

    private DbsGroup pnd_Work__R_Field_4;
    private DbsField pnd_Work_Pnd_Wk_Zip5;
    private DbsField pnd_Work_Pnd_Wk_Zip4;
    private DbsField pnd_Work_Pnd_Wk_Zip9;
    private DbsField pnd_Work_Pnd_Wk_Cntr;
    private DbsField pnd_Work_Pnd_Wk_City;
    private DbsField pnd_Work_Pnd_Wk_City_State_Zip9;
    private DbsField pnd_Wk_Issue_Dt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAciadate = new PdaAciadate(localVariables);
        ldaAppl300 = new LdaAppl300();
        registerRecord(ldaAppl300);
        ldaAppl302 = new LdaAppl302();
        registerRecord(ldaAppl302);
        pdaAcia2100 = new PdaAcia2100(localVariables);
        pdaScia8200 = new PdaScia8200(localVariables);

        // Local Variables
        pnd_Data = localVariables.newFieldInRecord("pnd_Data", "#DATA", FieldType.STRING, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Ppg_Entry_Dscrptn_Txt = localVariables.newFieldInRecord("pnd_Ppg_Entry_Dscrptn_Txt", "#PPG-ENTRY-DSCRPTN-TXT", FieldType.STRING, 60);

        pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1 = localVariables.newGroupInRecord("pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1", "REDEFINE", pnd_Ppg_Entry_Dscrptn_Txt);
        pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Appl = pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1.newFieldInGroup("pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Appl", 
            "#PPG-MAIL-APPL", FieldType.STRING, 1);
        pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem = pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1.newFieldInGroup("pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Mail_Prem", 
            "#PPG-MAIL-PREM", FieldType.STRING, 1);
        pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Del_Ovd = pnd_Ppg_Entry_Dscrptn_Txt__R_Field_1.newFieldInGroup("pnd_Ppg_Entry_Dscrptn_Txt_Pnd_Ppg_Del_Ovd", 
            "#PPG-DEL-OVD", FieldType.STRING, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Contact = localVariables.newFieldInRecord("pnd_Contact", "#CONTACT", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Temp_Cntr = localVariables.newFieldInRecord("pnd_Temp_Cntr", "#TEMP-CNTR", FieldType.STRING, 10);

        pnd_Temp_Cntr__R_Field_2 = localVariables.newGroupInRecord("pnd_Temp_Cntr__R_Field_2", "REDEFINE", pnd_Temp_Cntr);
        pnd_Temp_Cntr_Pnd_Cntr_Pref = pnd_Temp_Cntr__R_Field_2.newFieldInGroup("pnd_Temp_Cntr_Pnd_Cntr_Pref", "#CNTR-PREF", FieldType.STRING, 1);
        pnd_Temp_Cntr_Pnd_Cntr_Nbr = pnd_Temp_Cntr__R_Field_2.newFieldInGroup("pnd_Temp_Cntr_Pnd_Cntr_Nbr", "#CNTR-NBR", FieldType.STRING, 9);
        pnd_Temp_C_Cntr = localVariables.newFieldInRecord("pnd_Temp_C_Cntr", "#TEMP-C-CNTR", FieldType.STRING, 10);

        pnd_Temp_C_Cntr__R_Field_3 = localVariables.newGroupInRecord("pnd_Temp_C_Cntr__R_Field_3", "REDEFINE", pnd_Temp_C_Cntr);
        pnd_Temp_C_Cntr_Pnd_Cntr_C_Pref = pnd_Temp_C_Cntr__R_Field_3.newFieldInGroup("pnd_Temp_C_Cntr_Pnd_Cntr_C_Pref", "#CNTR-C-PREF", FieldType.STRING, 
            1);
        pnd_Temp_C_Cntr_Pnd_Cntr_C_Nbr = pnd_Temp_C_Cntr__R_Field_3.newFieldInGroup("pnd_Temp_C_Cntr_Pnd_Cntr_C_Nbr", "#CNTR-C-NBR", FieldType.STRING, 
            9);
        pnd_Sgrd = localVariables.newFieldInRecord("pnd_Sgrd", "#SGRD", FieldType.BOOLEAN, 1);
        pnd_Hold_Plan = localVariables.newFieldInRecord("pnd_Hold_Plan", "#HOLD-PLAN", FieldType.STRING, 6);
        pnd_Sgrd_Call_Cnt = localVariables.newFieldInRecord("pnd_Sgrd_Call_Cnt", "#SGRD-CALL-CNT", FieldType.INTEGER, 4);
        pnd_File_Open_Sw = localVariables.newFieldInRecord("pnd_File_Open_Sw", "#FILE-OPEN-SW", FieldType.STRING, 1);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Pnd_Wk_Compressed_Nme = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Compressed_Nme", "#WK-COMPRESSED-NME", FieldType.STRING, 40);
        pnd_Work_Pnd_Wk_F_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_F_Name", "#WK-F-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_L_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_L_Name", "#WK-L-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_M_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_M_Name", "#WK-M-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_Zip = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip", "#WK-ZIP", FieldType.STRING, 9);

        pnd_Work__R_Field_4 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_4", "REDEFINE", pnd_Work_Pnd_Wk_Zip);
        pnd_Work_Pnd_Wk_Zip5 = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Pnd_Wk_Zip5", "#WK-ZIP5", FieldType.STRING, 5);
        pnd_Work_Pnd_Wk_Zip4 = pnd_Work__R_Field_4.newFieldInGroup("pnd_Work_Pnd_Wk_Zip4", "#WK-ZIP4", FieldType.STRING, 4);
        pnd_Work_Pnd_Wk_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip9", "#WK-ZIP9", FieldType.STRING, 10);
        pnd_Work_Pnd_Wk_Cntr = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Cntr", "#WK-CNTR", FieldType.INTEGER, 2);
        pnd_Work_Pnd_Wk_City = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City", "#WK-CITY", FieldType.STRING, 29);
        pnd_Work_Pnd_Wk_City_State_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City_State_Zip9", "#WK-CITY-STATE-ZIP9", FieldType.STRING, 35);
        pnd_Wk_Issue_Dt = localVariables.newFieldInRecord("pnd_Wk_Issue_Dt", "#WK-ISSUE-DT", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl300.initializeValues();
        ldaAppl302.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb301() throws Exception
    {
        super("Appb301");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB301", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60 ZP = ON;//Natural: FORMAT ( 1 ) LS = 132 PS = 60 ZP = ON;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 ZP = ON
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  ARR KG
        //*  TIC
        //*  IISG
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 CV-RULE-DATA-1 CV-RULE-DATA-2 CV-RULE-DATA-3 CV-RULE-DATA-4 CV-RULE-DATA-5 CV-RULE-DATA-6 CV-RULE-DATA-7 CV-RULE-DATA-8 CV-RULE-DATA-9 CV-RULE-DATA-10 CV-RULE-DATA-11 CV-RULE-DATA-12 CV-RULE-DATA-13 CV-RULE-DATA-14 CV-RULE-DATA-15 CV-RULE-DATA-16 CV-RULE-DATA-17 CV-RULE-DATA-18 CV-RULE-DATA-19
        while (condition(getWorkFiles().read(1, ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_1(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_2(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_3(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_4(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_5(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_6(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_7(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_8(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_9(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_10(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_11(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_12(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_13(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_14(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_15(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_16(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_17(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_18(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_19())))
        {
            if (condition(!(ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("B") || ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("D") || ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("G")  //Natural: ACCEPT IF CV-PRAP-RULE.CV-STATUS EQ 'B' OR EQ 'D' OR EQ 'G' OR EQ 'H' OR EQ 'I' OR EQ 'C'
                || ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("H") || ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("I") || ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("C"))))
            {
                continue;
            }
            //*  --------------A CURTIS 9/00---->>>>>>>>>
            //*                                   EQ 'H' OR EQ 'I'
            if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Status().equals("C")))                                                                                            //Natural: IF CV-PRAP-RULE.CV-STATUS EQ 'C'
            {
                if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Record_Type().equals(2)))                                                                                     //Natural: IF CV-PRAP-RULE.CV-RECORD-TYPE EQ 2
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pdaAciadate.getAciadate_Pnd_Date_N_1().reset();                                                                                                           //Natural: RESET #DATE-N-1
                pnd_Data.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_Dt_Ent_Sys(),new ReportEditMask("99999999"));                                                       //Natural: MOVE EDITED CV-PRAP-RULE.CV-DT-ENT-SYS ( EM = 99999999 ) TO #DATA
                                                                                                                                                                          //Natural: PERFORM DAY-DIFF-RTN2
                sub_Day_Diff_Rtn2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TIGR
                if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Lob().equals("I") && ldaAppl300.getCv_Prap_Rule_Cv_Lob_Type().lessOrEqual("9")))                              //Natural: IF CV-PRAP-RULE.CV-LOB EQ 'I' AND CV-PRAP-RULE.CV-LOB-TYPE LE '9'
                {
                    //* *      CV-PRAP-RULE.CV-LOB-TYPE LE '6'         /* K.GATES 7/28/06
                    //*      IF (CV-PRAP-RULE.CV-RELEASE-IND EQ 4)  OR /* AND CHANGED TO OR
                    //*          (CV-PRAP-RULE.CV-RELEASE-IND EQ 5)     /* K.GATES 9/26
                    //*  K.GATES 7/28/06
                    if (condition((ldaAppl300.getCv_Prap_Rule_Cv_Release_Ind().lessOrEqual(5))))                                                                          //Natural: IF ( CV-PRAP-RULE.CV-RELEASE-IND LE 5 )
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                        sub_Write_Work_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Release_Ind().equals(6) && ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().lessOrEqual(31)))                    //Natural: IF CV-PRAP-RULE.CV-RELEASE-IND EQ 6 AND CV-DAY-DIFF LE 31
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                        sub_Write_Work_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  K.GATES 7/28/06
                    //*  SGRD3 KG
                    if (condition((ldaAppl300.getCv_Prap_Rule_Cv_Release_Ind().lessOrEqual(4)) || (ldaAppl300.getCv_Prap_Rule_Cv_Release_Ind().equals(7))))               //Natural: IF ( CV-PRAP-RULE.CV-RELEASE-IND LE 4 ) OR ( CV-PRAP-RULE.CV-RELEASE-IND EQ 7 )
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
                        sub_Write_Work_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  <<<<<<<<<-----A CURTIS 9/00-------------
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE
            sub_Write_Work_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* ** START OF COMMENTED CODE FOR PRAP OVERDUE LETTERS - SUNY KG
            //*  IF CV-PRAP-RULE.CV-LOB EQ 'I' AND
            //*      CV-PRAP-RULE.CV-LOB-TYPE LE '6'          /* K.GATES 8/1/06
            //*    ESCAPE TOP
            //*  END-IF
            //*  IF CV-PRAP-RULE.CV-STATUS EQ 'G' OR EQ 'H' OR EQ 'I'
            //*    PERFORM DAY-DIFF-RTN
            //*    MOVE BY NAME CV-PRAP-RULE TO CV-PRAP-RULE1
            //*    RESET CV-PRAP-RULE1.CV-PREF CV-PRAP-RULE1.CV-CONT #TEMP-CNTR
            //*      #TEMP-C-CNTR
            //*    #TEMP-CNTR   :=  CV-PRAP-RULE.CV-TIAA-CNTRCT
            //*    CV-PRAP-RULE1.CV-PREF := #CNTR-PREF
            //*    CV-PRAP-RULE1.CV-CONT := #CNTR-NBR
            //*    #TEMP-C-CNTR := CV-PRAP-RULE.CV-CREF-CERT
            //*    CV-PRAP-RULE1.CV-C-PREF := #CNTR-C-PREF
            //*    CV-PRAP-RULE1.CV-C-CONT := #CNTR-C-NBR
            //*    PERFORM GET-MAIL-IND
            //*    IF CV-PRAP-RULE.CV-RECORD-TYPE EQ 1
            //*      MOVE #PPG-MAIL-APPL     TO CV-MAIL-IND
            //*    ELSE
            //*      MOVE #PPG-MAIL-PREM     TO CV-MAIL-IND
            //*    END-IF
            //*    IF CV-MAIL-IND EQ ' '
            //*      MOVE 'Y' TO CV-MAIL-IND
            //*    END-IF
            //*  SGRD KG - SKIP IIS CALL FOR SUNGARD CONTRACTS AND HARDCODE CONTACT
            //*            INFORMATION FOR RELEASE 2
            //*    IF CV-PRAP-RULE.CV-COLL-CODE EQ 'SGRD'             /* SGRD KG
            //* *** CHANGE TO BYPASS OMNI CONTACT CALL    EMERG KG 12-30-06
            //*      ESCAPE TOP                                      /* EMERG KG
            //*  SGRD RW - CALL TO OMNI API TO GET CONTACT INFORMATION
            //*  -----------------------------------------------------
            //*      ADD 1 TO #SGRD-CALL-CNT                            /* 08/20/04 >>>
            //*  SET FILE OPEN SWITCH ON FIRST PASS
            //*  ----------------------------------
            //*      IF #SGRD-CALL-CNT = 1
            //*          CON-FILE-OPEN-SW   := 'N'                       /* SGRD5 KG
            //*      END-IF
            //*      CON-EOF-SW           := 'N'
            //*      CON-FUNCTION-CODE(1) := '08'                      /* SGRD5 KG
            //*      CON-ACCESS-LEVEL     := ' '
            //*      CON-CONTACT-TYPE     := 'E'
            //*      CON-PLAN-NUM         := CV-PRAP-RULE.CV-SGRD-PLAN-NO
            //*      CON-PART-DIVSUB      := CV-PRAP-RULE.CV-SGRD-DIVSUB  /* DCA KG
            //*      CALLNAT 'SCIN8200'  CON-FILE-OPEN-SW CON-PART-REC   /* SGRD5 KG
            //*      IF CON-RETURN-CODE NE 0 OR CON-CONTACT-ERR-MSG NE ' '
            //*        WRITE '*CALL ERROR (PSG9070):'
            //*          CON-RETURN-CODE CON-CONTACT-ERR-MSG
            //*        WRITE 'PLAN:' CON-PLAN-NUM
            //*        MOVE 'N' TO CV-MAIL-IND
            //*        RESET CV-ADDR-INDEX
            //*        RESET #CONTACT CV-FIRST-NME CV-MDDLE-NME CV-LAST-NME
            //*      ELSE
            //*        MOVE 'Y' TO #CONTACT                             /* SGRD KG
            //*        MOVE 'ROBERTO'          TO CV-FIRST-NME          /* SGRD KG
            //*        MOVE ' '                TO CV-MDDLE-NME          /* SGRD KG
            //*        MOVE 'TORRES'           TO CV-LAST-NME           /* SGRD KG
            //*        MOVE 1                  TO CV-ADDR-INDEX         /* SGRD KG
            //*  CREATE NAME VALUES
            //*  ------------------
            //*        SEPARATE CON-CONTACT-NAME INTO
            //*          #WK-L-NAME #WK-F-NAME #WK-M-NAME
            //*          WITH DELIMITERS ','
            //*        MOVE #WK-F-NAME         TO CV-FIRST-NME
            //*        MOVE #WK-L-NAME         TO CV-LAST-NME
            //*         WRITE(2)
            //*           CV-PRAP-RULE.CV-SGRD-PLAN-NO
            //*           #SGRD-CALL-CNT
            //*           CV-FIRST-NME
            //*           CV-LAST-NME  //
            //*      END-IF
            //*      RESET CON-PART-REC #WORK                           /* 08/20/04 >>>
            //*      RESET #WK-ISSUE-DT                                      /* REL5. >
            //*      MOVE EDITED CV-PRAP-RULE.CV-T-DOI(EM=9999) TO #WK-ISSUE-DT
            //*      IF #WK-ISSUE-DT = MASK (MMYY)                     /* JRB 07/20/05
            //*        MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-TIAA-DOI (EM=MMYY)
            //*      ELSE
            //*        RESET CV-PRAP-RULE1.CV-TIAA-DOI
            //*      END-IF
            //*      RESET #WK-ISSUE-DT
            //*      MOVE EDITED CV-PRAP-RULE.CV-C-DOI(EM=9999) TO #WK-ISSUE-DT
            //*      IF #WK-ISSUE-DT = MASK (MMYY)                     /* JRB 07/20/05
            //*        MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-CREF-DOI (EM=MMYY)
            //*      ELSE
            //*        RESET CV-PRAP-RULE1.CV-CREF-DOI
            //*      END-IF
            //*                                                              /* REL5. >
            //*      WRITE WORK FILE 3 CV-RULE-DATA-L CV-RULE-DATA-M  /* SGRD KG
            //*    ELSE                                               /* SGRD KG
            //*      PERFORM GET-CONTACT
            //*      RESET #CONTACT CV-FIRST-NME CV-MDDLE-NME CV-LAST-NME
            //*      FOR #I 1 #CONTACT-COUNT
            //*        IF #LAST-NME (#I) EQ ' ' OR
            //*            #ADDRESS-LINE-1 (#I) EQ ' '
            //*          ESCAPE TOP
            //*        END-IF
            //*        MOVE 'Y'                TO #CONTACT
            //*        MOVE #FIRST-NME (#I)    TO CV-FIRST-NME
            //*        MOVE #MIDDLE-NME (#I)   TO CV-MDDLE-NME
            //*        MOVE #LAST-NME (#I)     TO CV-LAST-NME
            //*        MOVE #I                 TO CV-ADDR-INDEX
            //*      RESET #WK-ISSUE-DT                                      /* REL5. >
            //*        MOVE EDITED CV-PRAP-RULE.CV-T-DOI(EM=9999) TO #WK-ISSUE-DT
            //*        IF #WK-ISSUE-DT = MASK (MMYY)                   /* JRB 07/20/05
            //*         MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-TIAA-DOI (EM=MMYY)
            //*        ELSE
            //*          RESET CV-PRAP-RULE1.CV-TIAA-DOI
            //*        END-IF
            //*        RESET #WK-ISSUE-DT
            //*        MOVE EDITED CV-PRAP-RULE.CV-C-DOI(EM=9999) TO #WK-ISSUE-DT
            //*        IF #WK-ISSUE-DT = MASK (MMYY)                   /* JRB 07/20/05
            //*         MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-CREF-DOI (EM=MMYY)
            //*        ELSE
            //*          RESET CV-PRAP-RULE1.CV-CREF-DOI
            //*        END-IF
            //*                                                              /* REL5. >
            //*        WRITE WORK FILE 3 CV-RULE-DATA-L CV-RULE-DATA-M
            //*      END-FOR
            //*    END-IF                                             /* SGRD KG
            //*    IF #CONTACT EQ ' '
            //*      MOVE 'N' TO CV-MAIL-IND
            //*      RESET CV-ADDR-INDEX
            //*      RESET #WK-ISSUE-DT                                      /* REL5. >
            //*      MOVE EDITED CV-PRAP-RULE.CV-T-DOI(EM=9999) TO #WK-ISSUE-DT
            //*      IF #WK-ISSUE-DT = MASK (MMYY)                     /* JRB 07/20/05
            //*        MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-TIAA-DOI (EM=MMYY)
            //*      ELSE
            //*        RESET CV-PRAP-RULE1.CV-TIAA-DOI
            //*      END-IF
            //*      RESET #WK-ISSUE-DT
            //*      MOVE EDITED CV-PRAP-RULE.CV-C-DOI(EM=9999) TO #WK-ISSUE-DT
            //*      IF #WK-ISSUE-DT = MASK (MMYY)                     /* JRB 07/20/05
            //*        MOVE EDITED #WK-ISSUE-DT TO CV-PRAP-RULE1.CV-CREF-DOI (EM=MMYY)
            //*      ELSE
            //*        RESET CV-PRAP-RULE1.CV-CREF-DOI
            //*      END-IF
            //*                                                              /* REL5. >
            //*      WRITE WORK FILE 3 CV-RULE-DATA-L CV-RULE-DATA-M
            //*    END-IF
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CLOSE FILE - CALL TO OMNI FOR CONTACT INFORMATION         /* 08/20/04
        //*  ----------                                                /* 08/20/04
        //*  IF #SGRD-CALL-CNT > 0                                     /* 08/20/04
        //*  RESET CON-PART-REC                                        /* 08/20/04
        //*  CON-EOF-SW := 'Y'                                         /* 08/20/04
        //*  CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC   /* 08/20/04 SGRD5
        //*  WRITE 'NUMBER OF CALLS TO RETREIVE SUNGARD CONTACT DATA:' /* 08/20/04
        //*    #SGRD-CALL-CNT                                          /* 08/20/04
        //*  END-IF                                                    /* 08/20/04
        //* ** END OF COMMENTED PRAP OVERDUE LETTER CODE - SUNY KG
        //* *============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DAY-DIFF-RTN
        //* *============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DAY-DIFF-RTN2
        //* *===========================
        //*  DEFINE SUBROUTINE GET-CONTACT                /* LS1 >>>
        //* *===========================
        //*  RESET #AC-PDA
        //*  #PPG-CDE-IN        := CV-PRAP-RULE.CV-COLL-CODE
        //*  EXAMINE #PPG-CDE-IN TRANSLATE INTO UPPER CASE
        //*  #ADDRESS-TYPE-IN   := '01'
        //*  #CONTACT-LEVEL     := ' '
        //*                                                           /* BEGIN VR1
        //*  #FUNCT-CDE-IN(1)   := '01'
        //*  #FUNCT-CDE-IN(1)   := '01'
        //*  #FUNCT-CDE-IN(2)   := '02'
        //*  #FUNCT-CDE-IN(3)   := '03'
        //*  #FUNCT-CDE-IN(4)   := '06'
        //*  #FUNCT-CDE-IN(5)   := '07'
        //*                                                           /* END VR1
        //*  #EXT-CONTACT-COUNT := 1
        //*  #ROLE-CDE-IN       := ' '
        //*  #INT-CONTACT-COUNT := 0
        //*  CALLNAT 'IISN8000' #AC-PDA
        //*  END-SUBROUTINE                                           /* LS1 <<<
        //* *============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MAIL-IND
        //* *--------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE
        //* *======
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Day_Diff_Rtn() throws Exception                                                                                                                      //Natural: DAY-DIFF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *============================
        pdaAciadate.getAciadate_Pnd_Date_N_1().reset();                                                                                                                   //Natural: RESET #DATE-N-1
        if (condition(ldaAppl300.getCv_Prap_Rule_Cv_Record_Type().equals(1)))                                                                                             //Natural: IF CV-PRAP-RULE.CV-RECORD-TYPE EQ 1
        {
            pnd_Data.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_Dt_App_Recvd(),new ReportEditMask("99999999"));                                                         //Natural: MOVE EDITED CV-PRAP-RULE.CV-DT-APP-RECVD ( EM = 99999999 ) TO #DATA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Data.setValueEdited(ldaAppl300.getCv_Prap_Rule_Cv_Dt_Ent_Sys(),new ReportEditMask("99999999"));                                                           //Natural: MOVE EDITED CV-PRAP-RULE.CV-DT-ENT-SYS ( EM = 99999999 ) TO #DATA
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DAY-DIFF-RTN2
        sub_Day_Diff_Rtn2();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Day_Diff_Rtn2() throws Exception                                                                                                                     //Natural: DAY-DIFF-RTN2
    {
        if (BLNatReinput.isReinput()) return;

        //* *============================
        if (condition(DbsUtil.maskMatches(pnd_Data,"MMDDYYYY")))                                                                                                          //Natural: IF #DATA EQ MASK ( MMDDYYYY )
        {
            pnd_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Data);                                                                                             //Natural: MOVE EDITED #DATA TO #DATE ( EM = MMDDYYYY )
            pdaAciadate.getAciadate_Pnd_Date_A_1().setValueEdited(pnd_Date,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #DATE-A-1
        }                                                                                                                                                                 //Natural: END-IF
        pdaAciadate.getAciadate_Pnd_Date_N_2().setValue(Global.getDATN());                                                                                                //Natural: MOVE *DATN TO #DATE-N-2
        pdaAciadate.getAciadate_Pnd_Function().setValue("01");                                                                                                            //Natural: MOVE '01' TO #FUNCTION
        if (condition(pdaAciadate.getAciadate_Pnd_Date_N_1().notEquals(getZero())))                                                                                       //Natural: IF #DATE-N-1 NE 0
        {
            DbsUtil.callnat(Acindate.class , getCurrentProcessState(), pdaAciadate.getAciadate());                                                                        //Natural: CALLNAT 'ACINDATE' ACIADATE
            if (condition(Global.isEscape())) return;
            if (condition(pdaAciadate.getAciadate_Pnd_Error_Msg().notEquals(" ") || pdaAciadate.getAciadate_Pnd_Error_Code().notEquals(getZero())))                       //Natural: IF #ERROR-MSG NE ' ' OR #ERROR-CODE NE 0
            {
                ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().reset();                                                                                                        //Natural: RESET CV-DAY-DIFF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().setValue(pdaAciadate.getAciadate_Pnd_Diff());                                                                   //Natural: MOVE #DIFF TO CV-DAY-DIFF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().reset();                                                                                                            //Natural: RESET CV-DAY-DIFF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Mail_Ind() throws Exception                                                                                                                      //Natural: GET-MAIL-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *============================
        pdaAcia2100.getAcia2100().reset();                                                                                                                                //Natural: RESET ACIA2100 #PPG-ENTRY-DSCRPTN-TXT
        pnd_Ppg_Entry_Dscrptn_Txt.reset();
        //*  SEARCH BY PPG
        pdaAcia2100.getAcia2100_Pnd_Search_Mode().setValue("AW");                                                                                                         //Natural: MOVE 'AW' TO ACIA2100.#SEARCH-MODE
        pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Actve_Ind().setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO ACIA2100.#S-AW-ENTRY-ACTVE-IND
        pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Table_Id_Nbr().setValue(300);                                                                                              //Natural: MOVE 000300 TO ACIA2100.#S-AW-ENTRY-TABLE-ID-NBR
        pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Table_Sub_Id().setValue("PG");                                                                                             //Natural: MOVE 'PG' TO ACIA2100.#S-AW-ENTRY-TABLE-SUB-ID
        pdaAcia2100.getAcia2100_Pnd_S_Aw_Entry_Cde().setValue(ldaAppl300.getCv_Prap_Rule_Cv_Coll_Code());                                                                 //Natural: MOVE CV-PRAP-RULE.CV-COLL-CODE TO ACIA2100.#S-AW-ENTRY-CDE
        //*  READ APP-TABLE-ENTRY
        DbsUtil.callnat(Acin2100.class , getCurrentProcessState(), pdaAcia2100.getAcia2100(), pnd_Debug);                                                                 //Natural: CALLNAT 'ACIN2100' ACIA2100 #DEBUG
        if (condition(Global.isEscape())) return;
        if (condition(pdaAcia2100.getAcia2100_Pnd_Return_Code().equals("  ")))                                                                                            //Natural: IF ACIA2100.#RETURN-CODE = '  '
        {
            pnd_Ppg_Entry_Dscrptn_Txt.setValue(pdaAcia2100.getAcia2100_Pnd_Entry_Dscrptn_Txt());                                                                          //Natural: MOVE ACIA2100.#ENTRY-DSCRPTN-TXT TO #PPG-ENTRY-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Work_File() throws Exception                                                                                                                   //Natural: WRITE-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  OIA KG
        //*  ARR KG
        //*  TIC
        //*  IISG
        getWorkFiles().write(2, false, ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_1(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_2(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_3(),  //Natural: WRITE WORK FILE 2 CV-RULE-DATA-1 CV-RULE-DATA-2 CV-RULE-DATA-3 CV-RULE-DATA-4 CV-RULE-DATA-5 CV-RULE-DATA-6 CV-RULE-DATA-7 CV-RULE-DATA-8 CV-RULE-DATA-9 CV-RULE-DATA-10 CV-RULE-DATA-11 CV-RULE-DATA-12 CV-RULE-DATA-13 CV-RULE-DATA-14 CV-RULE-DATA-15 CV-RULE-DATA-16 CV-RULE-DATA-17 CV-RULE-DATA-18 CV-RULE-DATA-19
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_4(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_5(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_6(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_7(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_8(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_9(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_10(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_11(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_12(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_13(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_14(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_15(), 
            ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_16(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_17(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_18(), ldaAppl300.getCv_Prap_Rule_Cv_Rule_Data_19());
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"ERROR:     ",Global.getERROR_NR(),                  //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'ERROR:     ' *ERROR-NR / 'TIAA CONTRACT:' CV-PRAP-RULE1.CV-PREF CV-PRAP-RULE1.CV-CONT / 'CREF CONTRACT:' CV-PRAP-RULE1.CV-C-PREF CV-PRAP-RULE1.CV-C-CONT /
            NEWLINE,"TIAA CONTRACT:",ldaAppl302.getCv_Prap_Rule1_Cv_Pref(),ldaAppl302.getCv_Prap_Rule1_Cv_Cont(),NEWLINE,"CREF CONTRACT:",ldaAppl302.getCv_Prap_Rule1_Cv_C_Pref(),
            ldaAppl302.getCv_Prap_Rule1_Cv_C_Cont(),NEWLINE);
        //*                                                              /* REL5. >
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60 ZP=ON");
        Global.format(1, "LS=132 PS=60 ZP=ON");
        Global.format(2, "LS=132 PS=60 ZP=ON");
    }
}
