/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:03 PM
**        * FROM NATURAL PROGRAM : Appb221
************************************************************
**        * FILE NAME            : Appb221.java
**        * CLASS NAME           : Appb221
**        * INSTANCE NAME        : Appb221
************************************************************
**********************************************************************
* PROGRAM ID : APPB221
* FUNCTION   : READ WORKFILE CREATED FROM APPB220 AND PRINT DAILY
*              STATISTICAL REPORTS BY REGION/NEED AND TEAM
*
* MODIFY ON BY       DESCRIPTION
* 03/10/99  L SHU    COMPARE THE PRAP COUNT WITH ILOG COUNT      (LS1)
* 03/23/99  L SHU    INCLUDE KEOGH                               (LS2)
* 11/13/01  B.E.     SEE 457(B) CHANGES
* 03/01/04  K.G.     RESTOW FOR SGRD PLAN AND SUBPLAN (APPG225)
* 12/24/04  R.WILLIS CHANGES FOR TNT/REL4.                       (RW1)
* 03/15/05  R.WILLIS CHANGES FOR TNT/REL5.                       (RW2)
* 09/09/05  D.M.     CHANGES FOR IRA SEP/REL 6                   (DM6)
* 10/03/06  K.GATES  CHANGES FOR DCA/CUNY DEPOSIT FUNDS       (DCA KG)
*  8/28/08 N.CVETKOVIC CHANGES FOR RHSP                      (RHSP NBC)
* 03/05/10  C. AVE   TIAA INDEXED GUARANTEE RATE - IRA CHANGES  (TIGR)
* 06/22/17  BABRE    PIN EXPANSION CHANGES. CHG425939   STOW ONLY
**********************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb221 extends BLNatBase
{
    // Data Areas
    private GdaAppg225 gdaAppg225;
    private LdaAppl020 ldaAppl020;
    private PdaAppa010 pdaAppa010;
    private PdaAppa020 pdaAppa020;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Prt_Reg;
    private DbsField pnd_Prt_Need;
    private DbsField pnd_Prt_Lob;
    private DbsField pnd_Prev_Reg;
    private DbsField pnd_Prev_Need;
    private DbsField pnd_Prev_Lob;
    private DbsField pnd_Prev_Bucket;
    private DbsField pnd_Prev_Accum;
    private DbsField work_Data;

    private DbsGroup work_Data__R_Field_1;
    private DbsField work_Data_Pnd_Wk_Region_Need;

    private DbsGroup work_Data__R_Field_2;
    private DbsField work_Data_Region_Num;
    private DbsField work_Data_Region_Need;
    private DbsField work_Data_Pnd_Proc_Stat_Num;
    private DbsField work_Data_Lob;
    private DbsField work_Data_Display_Map_Date;
    private DbsField work_Data_Pnd_Contract_Num;
    private DbsField work_Data_Pnd_Team_Disc;
    private DbsField work_Data_Pnd_Bucket;
    private DbsField work_Data_Pnd_Accum;
    private DbsField work_Data_Pnd_Wk_Start_Date;
    private DbsField work_Data_Pnd_Wk_End_Date;

    private DbsGroup pnd_Bucket_Name_Array;
    private DbsField pnd_Bucket_Name_Array_Pnd_Ilog_Bucket_Name;

    private DbsGroup pnd_Bucket_Name_Array__R_Field_3;
    private DbsField pnd_Bucket_Name_Array_Pnd_Ilog_Region_Need;
    private DbsField pnd_Bucket_Name_Array_Pnd_Bucket_Name;
    private DbsField pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_F;
    private DbsField pnd_Ilog_Position;
    private DbsField pnd_Page_No;
    private DbsField pnd_Prod_Cnt;
    private DbsField pnd_Prt_Fl;
    private DbsField pnd_Cd;

    private DbsGroup pnd_Prod_Table;
    private DbsField pnd_Prod_Table_Pnd_Prod_Cd;
    private DbsField pnd_Rpt_Tot_Tbl;

    private DbsGroup pnd_Rpt_Tot_Tbl__R_Field_4;
    private DbsField pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg;
    private DbsField pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl;
    private DbsField pnd_Rpt_Total_Tbl_Err;
    private DbsField pnd_Reg_Tot_Tbl;

    private DbsGroup pnd_Reg_Tot_Tbl__R_Field_5;
    private DbsField pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl;
    private DbsField pnd_Accum_Tot_Tbl;

    private DbsGroup pnd_Accum_Tot_Tbl__R_Field_6;
    private DbsField pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl;

    private DbsGroup pnd_Rpt_Counters;
    private DbsField pnd_Rpt_Counters_Pnd_Tiaa_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Prem_App_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Cref_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Splits_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Ln_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Comp_Cnt;
    private DbsField pnd_Rpt_Counters_Pnd_Cont_Mann_Cnt;

    private DbsGroup pnd_Print_Counters;
    private DbsField pnd_Print_Counters_Pnd_Prt_Cont_Iss;
    private DbsField pnd_Print_Counters_Pnd_Prt_Tiaa;
    private DbsField pnd_Print_Counters_Pnd_Prt_Cref;
    private DbsField pnd_Print_Counters_Pnd_Prt_Prem_App;
    private DbsField pnd_Print_Counters_Pnd_Prt_Neg_Enrol;
    private DbsField pnd_Print_Counters_Pnd_Prt_Splits;
    private DbsField pnd_Print_Counters_Pnd_Prt_Ln;

    private DbsGroup pnd_Prt_Total_Counters;
    private DbsField pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total;
    private DbsField pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total;
    private DbsField pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total;
    private DbsField pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total;
    private DbsField pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total;
    private DbsField pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total;
    private DbsField pnd_Prt_Total_Counters_Pnd_Prt_Need_Total;

    private DbsGroup pnd_Accum_Counters;
    private DbsField pnd_Accum_Counters_Pnd_Cont_Iss_Accum;
    private DbsField pnd_Accum_Counters_Pnd_Tiaa_Accum;
    private DbsField pnd_Accum_Counters_Pnd_Cref_Accum;
    private DbsField pnd_Accum_Counters_Pnd_Prem_App_Accum;
    private DbsField pnd_Accum_Counters_Pnd_Neg_Enrol_Accum;
    private DbsField pnd_Accum_Counters_Pnd_Splits_Accum;
    private DbsField pnd_Accum_Counters_Pnd_Rpt_Grand_Accum;
    private DbsField pnd_Accum_Counters_Pnd_Cont_Mann_Accum;
    private DbsField pnd_Debug;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaAppg225 = GdaAppg225.getInstance(getCallnatLevel());
        registerRecord(gdaAppg225);
        if (gdaOnly) return;

        ldaAppl020 = new LdaAppl020();
        registerRecord(ldaAppl020);
        registerRecord(ldaAppl020.getVw_app_Ilog_B_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_J_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_A_Rcrd_View());
        localVariables = new DbsRecord();
        pdaAppa010 = new PdaAppa010(localVariables);
        pdaAppa020 = new PdaAppa020(localVariables);

        // Local Variables
        pnd_Prt_Reg = localVariables.newFieldInRecord("pnd_Prt_Reg", "#PRT-REG", FieldType.STRING, 3);
        pnd_Prt_Need = localVariables.newFieldInRecord("pnd_Prt_Need", "#PRT-NEED", FieldType.STRING, 8);
        pnd_Prt_Lob = localVariables.newFieldInRecord("pnd_Prt_Lob", "#PRT-LOB", FieldType.STRING, 4);
        pnd_Prev_Reg = localVariables.newFieldInRecord("pnd_Prev_Reg", "#PREV-REG", FieldType.STRING, 3);
        pnd_Prev_Need = localVariables.newFieldInRecord("pnd_Prev_Need", "#PREV-NEED", FieldType.STRING, 8);
        pnd_Prev_Lob = localVariables.newFieldInRecord("pnd_Prev_Lob", "#PREV-LOB", FieldType.STRING, 4);
        pnd_Prev_Bucket = localVariables.newFieldInRecord("pnd_Prev_Bucket", "#PREV-BUCKET", FieldType.STRING, 2);
        pnd_Prev_Accum = localVariables.newFieldInRecord("pnd_Prev_Accum", "#PREV-ACCUM", FieldType.STRING, 2);
        work_Data = localVariables.newFieldInRecord("work_Data", "WORK-DATA", FieldType.STRING, 50);

        work_Data__R_Field_1 = localVariables.newGroupInRecord("work_Data__R_Field_1", "REDEFINE", work_Data);
        work_Data_Pnd_Wk_Region_Need = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Wk_Region_Need", "#WK-REGION-NEED", FieldType.STRING, 2);

        work_Data__R_Field_2 = work_Data__R_Field_1.newGroupInGroup("work_Data__R_Field_2", "REDEFINE", work_Data_Pnd_Wk_Region_Need);
        work_Data_Region_Num = work_Data__R_Field_2.newFieldInGroup("work_Data_Region_Num", "REGION-NUM", FieldType.STRING, 1);
        work_Data_Region_Need = work_Data__R_Field_2.newFieldInGroup("work_Data_Region_Need", "REGION-NEED", FieldType.STRING, 1);
        work_Data_Pnd_Proc_Stat_Num = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Proc_Stat_Num", "#PROC-STAT-NUM", FieldType.STRING, 2);
        work_Data_Lob = work_Data__R_Field_1.newFieldInGroup("work_Data_Lob", "LOB", FieldType.STRING, 4);
        work_Data_Display_Map_Date = work_Data__R_Field_1.newFieldInGroup("work_Data_Display_Map_Date", "DISPLAY-MAP-DATE", FieldType.DATE);
        work_Data_Pnd_Contract_Num = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Contract_Num", "#CONTRACT-NUM", FieldType.STRING, 6);
        work_Data_Pnd_Team_Disc = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Team_Disc", "#TEAM-DISC", FieldType.STRING, 8);
        work_Data_Pnd_Bucket = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Bucket", "#BUCKET", FieldType.STRING, 2);
        work_Data_Pnd_Accum = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Accum", "#ACCUM", FieldType.STRING, 2);
        work_Data_Pnd_Wk_Start_Date = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Wk_Start_Date", "#WK-START-DATE", FieldType.STRING, 8);
        work_Data_Pnd_Wk_End_Date = work_Data__R_Field_1.newFieldInGroup("work_Data_Pnd_Wk_End_Date", "#WK-END-DATE", FieldType.STRING, 8);

        pnd_Bucket_Name_Array = localVariables.newGroupArrayInRecord("pnd_Bucket_Name_Array", "#BUCKET-NAME-ARRAY", new DbsArrayController(1, 10));
        pnd_Bucket_Name_Array_Pnd_Ilog_Bucket_Name = pnd_Bucket_Name_Array.newFieldInGroup("pnd_Bucket_Name_Array_Pnd_Ilog_Bucket_Name", "#ILOG-BUCKET-NAME", 
            FieldType.STRING, 15);

        pnd_Bucket_Name_Array__R_Field_3 = pnd_Bucket_Name_Array.newGroupInGroup("pnd_Bucket_Name_Array__R_Field_3", "REDEFINE", pnd_Bucket_Name_Array_Pnd_Ilog_Bucket_Name);
        pnd_Bucket_Name_Array_Pnd_Ilog_Region_Need = pnd_Bucket_Name_Array__R_Field_3.newFieldInGroup("pnd_Bucket_Name_Array_Pnd_Ilog_Region_Need", "#ILOG-REGION-NEED", 
            FieldType.STRING, 2);
        pnd_Bucket_Name_Array_Pnd_Bucket_Name = pnd_Bucket_Name_Array__R_Field_3.newFieldInGroup("pnd_Bucket_Name_Array_Pnd_Bucket_Name", "#BUCKET-NAME", 
            FieldType.STRING, 13);
        pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value = pnd_Bucket_Name_Array.newFieldInGroup("pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value", "#ILOG-ACCUM-VALUE", 
            FieldType.NUMERIC, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 4);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 4);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 4);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.INTEGER, 2);
        pnd_Ilog_Position = localVariables.newFieldInRecord("pnd_Ilog_Position", "#ILOG-POSITION", FieldType.INTEGER, 4);
        pnd_Page_No = localVariables.newFieldInRecord("pnd_Page_No", "#PAGE-NO", FieldType.NUMERIC, 3);
        pnd_Prod_Cnt = localVariables.newFieldInRecord("pnd_Prod_Cnt", "#PROD-CNT", FieldType.NUMERIC, 3);
        pnd_Prt_Fl = localVariables.newFieldArrayInRecord("pnd_Prt_Fl", "#PRT-FL", FieldType.STRING, 1, new DbsArrayController(1, 7));
        pnd_Cd = localVariables.newFieldInRecord("pnd_Cd", "#CD", FieldType.NUMERIC, 3);

        pnd_Prod_Table = localVariables.newGroupArrayInRecord("pnd_Prod_Table", "#PROD-TABLE", new DbsArrayController(1, 21));
        pnd_Prod_Table_Pnd_Prod_Cd = pnd_Prod_Table.newFieldInGroup("pnd_Prod_Table_Pnd_Prod_Cd", "#PROD-CD", FieldType.STRING, 4);
        pnd_Rpt_Tot_Tbl = localVariables.newFieldArrayInRecord("pnd_Rpt_Tot_Tbl", "#RPT-TOT-TBL", FieldType.STRING, 7, new DbsArrayController(1, 21, 1, 
            10));

        pnd_Rpt_Tot_Tbl__R_Field_4 = localVariables.newGroupInRecord("pnd_Rpt_Tot_Tbl__R_Field_4", "REDEFINE", pnd_Rpt_Tot_Tbl);
        pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg = pnd_Rpt_Tot_Tbl__R_Field_4.newFieldArrayInGroup("pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg", "#RPT-FLG", FieldType.STRING, 1, 
            new DbsArrayController(1, 21, 1, 10));
        pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl = pnd_Rpt_Tot_Tbl__R_Field_4.newFieldArrayInGroup("pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl", "#RPT-TOTAL-TBL", FieldType.NUMERIC, 
            6, new DbsArrayController(1, 21, 1, 10));
        pnd_Rpt_Total_Tbl_Err = localVariables.newFieldArrayInRecord("pnd_Rpt_Total_Tbl_Err", "#RPT-TOTAL-TBL-ERR", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            21, 1, 10));
        pnd_Reg_Tot_Tbl = localVariables.newFieldArrayInRecord("pnd_Reg_Tot_Tbl", "#REG-TOT-TBL", FieldType.STRING, 7, new DbsArrayController(1, 21, 1, 
            10));

        pnd_Reg_Tot_Tbl__R_Field_5 = localVariables.newGroupInRecord("pnd_Reg_Tot_Tbl__R_Field_5", "REDEFINE", pnd_Reg_Tot_Tbl);
        pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl = pnd_Reg_Tot_Tbl__R_Field_5.newFieldArrayInGroup("pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl", "#REG-TOTAL-TBL", FieldType.NUMERIC, 
            6, new DbsArrayController(1, 21, 1, 10));
        pnd_Accum_Tot_Tbl = localVariables.newFieldArrayInRecord("pnd_Accum_Tot_Tbl", "#ACCUM-TOT-TBL", FieldType.STRING, 7, new DbsArrayController(1, 
            21, 1, 10));

        pnd_Accum_Tot_Tbl__R_Field_6 = localVariables.newGroupInRecord("pnd_Accum_Tot_Tbl__R_Field_6", "REDEFINE", pnd_Accum_Tot_Tbl);
        pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl = pnd_Accum_Tot_Tbl__R_Field_6.newFieldArrayInGroup("pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl", "#ACCUM-TOTAL-TBL", 
            FieldType.NUMERIC, 6, new DbsArrayController(1, 21, 1, 10));

        pnd_Rpt_Counters = localVariables.newGroupInRecord("pnd_Rpt_Counters", "#RPT-COUNTERS");
        pnd_Rpt_Counters_Pnd_Tiaa_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Tiaa_Cnt", "#TIAA-CNT", FieldType.NUMERIC, 10);
        pnd_Rpt_Counters_Pnd_Prem_App_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Prem_App_Cnt", "#PREM-APP-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt", "#CONT-ISS-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rpt_Counters_Pnd_Cref_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Cref_Cnt", "#CREF-CNT", FieldType.NUMERIC, 10);
        pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt", "#NEG-ENROL-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rpt_Counters_Pnd_Splits_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Splits_Cnt", "#SPLITS-CNT", FieldType.NUMERIC, 10);
        pnd_Rpt_Counters_Pnd_Ln_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Ln_Cnt", "#LN-CNT", FieldType.NUMERIC, 10);
        pnd_Rpt_Counters_Pnd_Comp_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Comp_Cnt", "#COMP-CNT", FieldType.NUMERIC, 10);
        pnd_Rpt_Counters_Pnd_Cont_Mann_Cnt = pnd_Rpt_Counters.newFieldInGroup("pnd_Rpt_Counters_Pnd_Cont_Mann_Cnt", "#CONT-MANN-CNT", FieldType.NUMERIC, 
            10);

        pnd_Print_Counters = localVariables.newGroupInRecord("pnd_Print_Counters", "#PRINT-COUNTERS");
        pnd_Print_Counters_Pnd_Prt_Cont_Iss = pnd_Print_Counters.newFieldInGroup("pnd_Print_Counters_Pnd_Prt_Cont_Iss", "#PRT-CONT-ISS", FieldType.NUMERIC, 
            10);
        pnd_Print_Counters_Pnd_Prt_Tiaa = pnd_Print_Counters.newFieldInGroup("pnd_Print_Counters_Pnd_Prt_Tiaa", "#PRT-TIAA", FieldType.NUMERIC, 10);
        pnd_Print_Counters_Pnd_Prt_Cref = pnd_Print_Counters.newFieldInGroup("pnd_Print_Counters_Pnd_Prt_Cref", "#PRT-CREF", FieldType.NUMERIC, 10);
        pnd_Print_Counters_Pnd_Prt_Prem_App = pnd_Print_Counters.newFieldInGroup("pnd_Print_Counters_Pnd_Prt_Prem_App", "#PRT-PREM-APP", FieldType.NUMERIC, 
            10);
        pnd_Print_Counters_Pnd_Prt_Neg_Enrol = pnd_Print_Counters.newFieldInGroup("pnd_Print_Counters_Pnd_Prt_Neg_Enrol", "#PRT-NEG-ENROL", FieldType.NUMERIC, 
            10);
        pnd_Print_Counters_Pnd_Prt_Splits = pnd_Print_Counters.newFieldInGroup("pnd_Print_Counters_Pnd_Prt_Splits", "#PRT-SPLITS", FieldType.NUMERIC, 
            10);
        pnd_Print_Counters_Pnd_Prt_Ln = pnd_Print_Counters.newFieldInGroup("pnd_Print_Counters_Pnd_Prt_Ln", "#PRT-LN", FieldType.NUMERIC, 10);

        pnd_Prt_Total_Counters = localVariables.newGroupInRecord("pnd_Prt_Total_Counters", "#PRT-TOTAL-COUNTERS");
        pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total = pnd_Prt_Total_Counters.newFieldInGroup("pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total", "#PRT-ISS-TOTAL", 
            FieldType.NUMERIC, 10);
        pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total = pnd_Prt_Total_Counters.newFieldInGroup("pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total", "#PRT-TIAA-TOTAL", 
            FieldType.NUMERIC, 10);
        pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total = pnd_Prt_Total_Counters.newFieldInGroup("pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total", "#PRT-CREF-TOTAL", 
            FieldType.NUMERIC, 10);
        pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total = pnd_Prt_Total_Counters.newFieldInGroup("pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total", "#PRT-PREM-TOTAL", 
            FieldType.NUMERIC, 10);
        pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total = pnd_Prt_Total_Counters.newFieldInGroup("pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total", "#PRT-NEG-TOTAL", 
            FieldType.NUMERIC, 10);
        pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total = pnd_Prt_Total_Counters.newFieldInGroup("pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total", "#PRT-SPLITS-TOTAL", 
            FieldType.NUMERIC, 10);
        pnd_Prt_Total_Counters_Pnd_Prt_Need_Total = pnd_Prt_Total_Counters.newFieldInGroup("pnd_Prt_Total_Counters_Pnd_Prt_Need_Total", "#PRT-NEED-TOTAL", 
            FieldType.NUMERIC, 10);

        pnd_Accum_Counters = localVariables.newGroupInRecord("pnd_Accum_Counters", "#ACCUM-COUNTERS");
        pnd_Accum_Counters_Pnd_Cont_Iss_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Cont_Iss_Accum", "#CONT-ISS-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Accum_Counters_Pnd_Tiaa_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Tiaa_Accum", "#TIAA-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Accum_Counters_Pnd_Cref_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Cref_Accum", "#CREF-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Accum_Counters_Pnd_Prem_App_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Prem_App_Accum", "#PREM-APP-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Accum_Counters_Pnd_Neg_Enrol_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Neg_Enrol_Accum", "#NEG-ENROL-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Accum_Counters_Pnd_Splits_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Splits_Accum", "#SPLITS-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Accum_Counters_Pnd_Rpt_Grand_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Rpt_Grand_Accum", "#RPT-GRAND-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Accum_Counters_Pnd_Cont_Mann_Accum = pnd_Accum_Counters.newFieldInGroup("pnd_Accum_Counters_Pnd_Cont_Mann_Accum", "#CONT-MANN-ACCUM", FieldType.NUMERIC, 
            10);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl020.initializeValues();

        localVariables.reset();
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_F.setInitialValue(0);
        pnd_Ilog_Position.setInitialValue(0);
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(1).setInitialValue("RA  ");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(2).setInitialValue("GSRA");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(3).setInitialValue("IRA ");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(4).setInitialValue("IRAC");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(5).setInitialValue("IRAR");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(6).setInitialValue("IRAS");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(7).setInitialValue("GRA ");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(8).setInitialValue("LGA ");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(9).setInitialValue("SRA ");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(10).setInitialValue("KEOG");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(11).setInitialValue("GA");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(12).setInitialValue("RS");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(13).setInitialValue("RSP");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(14).setInitialValue("RSP2");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(15).setInitialValue("RC");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(16).setInitialValue("RCP");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(17).setInitialValue("TGA");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(18).setInitialValue("RHSP");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(19).setInitialValue("IRIR");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(20).setInitialValue("IRIC");
        pnd_Prod_Table_Pnd_Prod_Cd.getValue(21).setInitialValue("IRIS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb221() throws Exception
    {
        super("Appb221");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52 ZP = OFF
        pdaAppa020.getPnd_Parmdata_Pnd_End_Date().setValue(Global.getDATN());                                                                                             //Natural: MOVE *DATN TO #END-DATE
        //*  ----------- TALLY BY PRODUCT -------------
        READ3:                                                                                                                                                            //Natural: READ WORK FILE 1 WORK-DATA
        while (condition(getWorkFiles().read(1, work_Data)))
        {
            if (condition(pnd_Prev_Reg.equals(" ")))                                                                                                                      //Natural: IF #PREV-REG = ' '
            {
                pnd_Prev_Reg.setValue(work_Data_Region_Num);                                                                                                              //Natural: MOVE REGION-NUM TO #PREV-REG #PRT-REG
                pnd_Prt_Reg.setValue(work_Data_Region_Num);
                pnd_Prev_Need.setValue(work_Data_Region_Need);                                                                                                            //Natural: MOVE REGION-NEED TO #PREV-NEED
                pnd_Prev_Lob.setValue(work_Data_Lob);                                                                                                                     //Natural: MOVE LOB TO #PREV-LOB
                //*  LS1
                pnd_Bucket_Name_Array_Pnd_Ilog_Region_Need.getValue("*").setValue(work_Data_Pnd_Wk_Region_Need);                                                          //Natural: MOVE #WK-REGION-NEED TO #ILOG-REGION-NEED ( * )
                //*  LS1
                pnd_Prev_Bucket.setValue(work_Data_Pnd_Bucket);                                                                                                           //Natural: MOVE #BUCKET TO #PREV-BUCKET
                //*  LS1
                pnd_Prev_Accum.setValue(work_Data_Pnd_Accum);                                                                                                             //Natural: MOVE #ACCUM TO #PREV-ACCUM
                if (condition(work_Data_Region_Need.equals("S")))                                                                                                         //Natural: IF REGION-NEED = 'S'
                {
                    pnd_Prt_Need.setValue("SPECIAL ");                                                                                                                    //Natural: MOVE 'SPECIAL ' TO #PRT-NEED
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prt_Need.setValue(work_Data_Pnd_Team_Disc);                                                                                                       //Natural: MOVE #TEAM-DISC TO #PRT-NEED
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RPT-HEADING
                sub_Rpt_Heading();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_Reg.equals(work_Data_Region_Num)))                                                                                                     //Natural: IF #PREV-REG = REGION-NUM
            {
                if (condition(pnd_Prev_Need.equals(work_Data_Region_Need)))                                                                                               //Natural: IF #PREV-NEED = REGION-NEED
                {
                    if (condition(pnd_Prev_Lob.equals(work_Data_Lob)))                                                                                                    //Natural: IF #PREV-LOB = LOB
                    {
                                                                                                                                                                          //Natural: PERFORM PROCESS-LOB-TOTALS
                        sub_Process_Lob_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM PROD-BREAK-PROCESSING
                        sub_Prod_Break_Processing();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTALS
                        sub_Accum_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rpt_Counters.reset();                                                                                                                         //Natural: RESET #RPT-COUNTERS
                                                                                                                                                                          //Natural: PERFORM PROCESS-LOB-TOTALS
                        sub_Process_Lob_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Prev_Lob.setValue(work_Data_Lob);                                                                                                             //Natural: MOVE LOB TO #PREV-LOB
                        //*  LS1
                        pnd_Bucket_Name_Array_Pnd_Ilog_Region_Need.getValue("*").setValue(work_Data_Pnd_Wk_Region_Need);                                                  //Natural: MOVE #WK-REGION-NEED TO #ILOG-REGION-NEED ( * )
                        //*  LS1
                        pnd_Prev_Bucket.setValue(work_Data_Pnd_Bucket);                                                                                                   //Natural: MOVE #BUCKET TO #PREV-BUCKET
                        //*  LS1
                        pnd_Prev_Accum.setValue(work_Data_Pnd_Accum);                                                                                                     //Natural: MOVE #ACCUM TO #PREV-ACCUM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM NEED-BREAK-PROCESSING
                    sub_Need_Break_Processing();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTALS
                    sub_Accum_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Rpt_Counters.reset();                                                                                                                             //Natural: RESET #RPT-COUNTERS
                                                                                                                                                                          //Natural: PERFORM PROCESS-LOB-TOTALS
                    sub_Process_Lob_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prev_Lob.setValue(work_Data_Lob);                                                                                                                 //Natural: MOVE LOB TO #PREV-LOB
                    pnd_Prev_Need.setValue(work_Data_Region_Need);                                                                                                        //Natural: MOVE REGION-NEED TO #PREV-NEED
                    //*  LS1
                    pnd_Bucket_Name_Array_Pnd_Ilog_Region_Need.getValue("*").setValue(work_Data_Pnd_Wk_Region_Need);                                                      //Natural: MOVE #WK-REGION-NEED TO #ILOG-REGION-NEED ( * )
                    //*  LS1
                    pnd_Prev_Bucket.setValue(work_Data_Pnd_Bucket);                                                                                                       //Natural: MOVE #BUCKET TO #PREV-BUCKET
                    //*  LS1
                    pnd_Prev_Accum.setValue(work_Data_Pnd_Accum);                                                                                                         //Natural: MOVE #ACCUM TO #PREV-ACCUM
                    if (condition(work_Data_Region_Need.equals("S")))                                                                                                     //Natural: IF REGION-NEED = 'S'
                    {
                        pnd_Prt_Need.setValue("SPECIAL ");                                                                                                                //Natural: MOVE 'SPECIAL ' TO #PRT-NEED
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Prt_Need.setValue(work_Data_Pnd_Team_Disc);                                                                                                   //Natural: MOVE #TEAM-DISC TO #PRT-NEED
                    }                                                                                                                                                     //Natural: END-IF
                    //*       MOVE #TEAM-DISC TO #PRT-NEED
                                                                                                                                                                          //Natural: PERFORM RPT-HEADING
                    sub_Rpt_Heading();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ3"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  LS1
                    pnd_Prod_Cnt.reset();                                                                                                                                 //Natural: RESET #PROD-CNT #PRINT-COUNTERS #PRT-TOTAL-COUNTERS #RPT-TOT-TBL ( *,* ) #PRT-LOB #RPT-TOTAL-TBL-ERR ( *,* )
                    pnd_Print_Counters.reset();
                    pnd_Prt_Total_Counters.reset();
                    pnd_Rpt_Tot_Tbl.getValue("*","*").reset();
                    pnd_Prt_Lob.reset();
                    pnd_Rpt_Total_Tbl_Err.getValue("*","*").reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM REG-BREAK-PROCESSING
                sub_Reg_Break_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  LS1
                pnd_Rpt_Counters.reset();                                                                                                                                 //Natural: RESET #RPT-COUNTERS #RPT-FLG ( *,* )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue("*","*").reset();
                                                                                                                                                                          //Natural: PERFORM PROCESS-LOB-TOTALS
                sub_Process_Lob_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Lob.setValue(work_Data_Lob);                                                                                                                     //Natural: MOVE LOB TO #PREV-LOB
                pnd_Prev_Need.setValue(work_Data_Region_Need);                                                                                                            //Natural: MOVE REGION-NEED TO #PREV-NEED
                pnd_Prev_Reg.setValue(work_Data_Region_Num);                                                                                                              //Natural: MOVE REGION-NUM TO #PREV-REG #PRT-REG
                pnd_Prt_Reg.setValue(work_Data_Region_Num);
                //*  LS1
                pnd_Bucket_Name_Array_Pnd_Ilog_Region_Need.getValue("*").setValue(work_Data_Pnd_Wk_Region_Need);                                                          //Natural: MOVE #WK-REGION-NEED TO #ILOG-REGION-NEED ( * )
                //*  LS1
                pnd_Prev_Bucket.setValue(work_Data_Pnd_Bucket);                                                                                                           //Natural: MOVE #BUCKET TO #PREV-BUCKET
                //*  LS1
                pnd_Prev_Accum.setValue(work_Data_Pnd_Accum);                                                                                                             //Natural: MOVE #ACCUM TO #PREV-ACCUM
                if (condition(work_Data_Region_Need.equals("S")))                                                                                                         //Natural: IF REGION-NEED = 'S'
                {
                    pnd_Prt_Need.setValue("SPECIAL ");                                                                                                                    //Natural: MOVE 'SPECIAL ' TO #PRT-NEED
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prt_Need.setValue(work_Data_Pnd_Team_Disc);                                                                                                       //Natural: MOVE #TEAM-DISC TO #PRT-NEED
                }                                                                                                                                                         //Natural: END-IF
                //*    MOVE #TEAM-DISC TO #PRT-NEED
                                                                                                                                                                          //Natural: PERFORM RPT-HEADING
                sub_Rpt_Heading();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  LS1
                pnd_Prod_Cnt.reset();                                                                                                                                     //Natural: RESET #PROD-CNT #PRT-LOB #PRT-TOTAL-COUNTERS #PRINT-COUNTERS #RPT-TOT-TBL ( *,* ) #RPT-TOTAL-TBL-ERR ( *,* ) #REG-TOT-TBL ( *,* )
                pnd_Prt_Lob.reset();
                pnd_Prt_Total_Counters.reset();
                pnd_Print_Counters.reset();
                pnd_Rpt_Tot_Tbl.getValue("*","*").reset();
                pnd_Rpt_Total_Tbl_Err.getValue("*","*").reset();
                pnd_Reg_Tot_Tbl.getValue("*","*").reset();
            }                                                                                                                                                             //Natural: END-IF
            //*  ------------------------------------
            //*  ----------------------------
            //*  --------------------------------------
            //*  COMPUTE #COMP-CNT = (#CONT-ISS-CNT + #TIAA-CNT + #CREF-CNT
            //*  + #CONT-MANN-CNT + #PREM-APP-CNT + #NEG-ENROL-CNT + #SPLITS-CNT)  LS1
            //*  FOR #I  1 10
            //*  FOR #I  1 13
            //*  FOR #I  1 15
            //*  FOR #I  1 16
            //* *FOR #I  1 18
            //*  -----------------------------
            //*  -----------------------------
            //*  FOR #I  1 10
            //*  FOR #I  1 13
            //*  FOR #I  1 15
            //*  FOR #I  1 16
            //*  FOR #I  1 17
            //* *FOR #I  1 18
            //*  FOR #I  1 10
            //*  FOR #I  1 13
            //*  FOR #I  1 15
            //*  FOR #I  1 16
            //*  FOR #I  1 17
            //* *FOR #I  1 18
            //*  --------------------------------------
            //*  FOR #PROD-CNT 1 10
            //*  FOR #PROD-CNT 1 13
            //*  FOR #PROD-CNT 1 15
            //*  FOR #PROD-CNT 1 16
            //*  FOR #PROD-CNT 1 17
            //* *FOR #PROD-CNT 1 18
            //*  FOR #I  1 10
            //*  FOR #I  1 13
            //*  FOR #I  1 15
            //*  FOR #I  1 16
            //*  FOR #I  1 17
            //* *FOR #I  1 18
            //*  --------------------------
            //*  FOR #PROD-CNT 1 10
            //*  FOR #PROD-CNT 1 13
            //*  FOR #PROD-CNT 1 15
            //*  FOR #PROD-CNT 1 16
            //*  FOR #PROD-CNT 1 17
            //* *FOR #PROD-CNT 1 18
            //*  FOR #I  1 10
            //*  FOR #I  1 13
            //*  FOR #I  1 15
            //*  FOR #I  1 16
            //*  FOR #I  1 17
            //* *FOR #I  1 18
            //*  -------------
            //*  ==============
            //*  ----------                                                                                                                                               //Natural: AT END OF DATA ( READ3. )
            //*  READ3
        }                                                                                                                                                                 //Natural: END-WORK
        READ3_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM REG-BREAK-PROCESSING
            sub_Reg_Break_Processing();
            if (condition(Global.isEscape())) {return;}
            pnd_Prt_Reg.setValue("ALL");                                                                                                                                  //Natural: MOVE 'ALL' TO #PRT-REG
            pnd_Prt_Need.setValue("TOTAL  ");                                                                                                                             //Natural: MOVE 'TOTAL  ' TO #PRT-NEED
            //*  TIGR
                                                                                                                                                                          //Natural: PERFORM RPT-HEADING
            sub_Rpt_Heading();
            if (condition(Global.isEscape())) {return;}
            //*     FOR #PROD-CNT 1 10
            //*    FOR #PROD-CNT 1 13
            //*    FOR #PROD-CNT 1 15
            //*    FOR #PROD-CNT 1 16
            //*    FOR #PROD-CNT 1 17
            //* *  FOR #PROD-CNT 1 18
            FOR01:                                                                                                                                                        //Natural: FOR #PROD-CNT 1 21
            for (pnd_Prod_Cnt.setValue(1); condition(pnd_Prod_Cnt.lessOrEqual(21)); pnd_Prod_Cnt.nadd(1))
            {
                pnd_Prt_Lob.setValue(pnd_Prod_Table_Pnd_Prod_Cd.getValue(pnd_Prod_Cnt));                                                                                  //Natural: MOVE #PROD-CD ( #PROD-CNT ) TO #PRT-LOB
                pnd_Print_Counters_Pnd_Prt_Cont_Iss.setValue(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_Prod_Cnt,1));                                             //Natural: MOVE #ACCUM-TOTAL-TBL ( #PROD-CNT,1 ) TO #PRT-CONT-ISS
                pnd_Print_Counters_Pnd_Prt_Tiaa.setValue(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_Prod_Cnt,2));                                                 //Natural: MOVE #ACCUM-TOTAL-TBL ( #PROD-CNT,2 ) TO #PRT-TIAA
                pnd_Print_Counters_Pnd_Prt_Cref.setValue(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_Prod_Cnt,3));                                                 //Natural: MOVE #ACCUM-TOTAL-TBL ( #PROD-CNT,3 ) TO #PRT-CREF
                pnd_Print_Counters_Pnd_Prt_Prem_App.setValue(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_Prod_Cnt,4));                                             //Natural: MOVE #ACCUM-TOTAL-TBL ( #PROD-CNT,4 ) TO #PRT-PREM-APP
                pnd_Print_Counters_Pnd_Prt_Neg_Enrol.setValue(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_Prod_Cnt,5));                                            //Natural: MOVE #ACCUM-TOTAL-TBL ( #PROD-CNT,5 ) TO #PRT-NEG-ENROL
                pnd_Print_Counters_Pnd_Prt_Splits.setValue(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_Prod_Cnt,6));                                               //Natural: MOVE #ACCUM-TOTAL-TBL ( #PROD-CNT,6 ) TO #PRT-SPLITS
                pnd_Print_Counters_Pnd_Prt_Ln.setValue(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_Prod_Cnt,7));                                                   //Natural: MOVE #ACCUM-TOTAL-TBL ( #PROD-CNT,7 ) TO #PRT-LN
                //*  START LS1
                pnd_Prt_Fl.getValue("*").reset();                                                                                                                         //Natural: RESET #PRT-FL ( * )
                //*     FOR  #F = 1 TO 7
                //*       IF (#ACCUM-TOTAL-TBL(#PROD-CNT,#F) > 0
                //*       AND #ACCUM-FLG(#PROD-CNT,#F) = '*')
                //*           MOVE '*' TO #PRT-FL(#F)
                //*           ESCAPE TOP
                //*       END-IF
                //*     END-FOR                                                 /* END  LS1
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),pnd_Prt_Lob,new ColumnSpacing(6),pnd_Print_Counters_Pnd_Prt_Cont_Iss,pnd_Prt_Fl.getValue(1),new  //Natural: WRITE ( 1 ) NOTITLE 1X #PRT-LOB 6X #PRT-CONT-ISS #PRT-FL ( 1 ) 2X #PRT-TIAA #PRT-FL ( 2 ) 1X #PRT-CREF #PRT-FL ( 3 ) 1X #PRT-PREM-APP #PRT-FL ( 4 ) 1X #PRT-NEG-ENROL 1X #PRT-FL ( 5 ) #PRT-SPLITS #PRT-FL ( 6 ) #PRT-LN #PRT-FL ( 7 ) /
                    ColumnSpacing(2),pnd_Print_Counters_Pnd_Prt_Tiaa,pnd_Prt_Fl.getValue(2),new ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Cref,pnd_Prt_Fl.getValue(3),new 
                    ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Prem_App,pnd_Prt_Fl.getValue(4),new ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Neg_Enrol,new 
                    ColumnSpacing(1),pnd_Prt_Fl.getValue(5),pnd_Print_Counters_Pnd_Prt_Splits,pnd_Prt_Fl.getValue(6),pnd_Print_Counters_Pnd_Prt_Ln,pnd_Prt_Fl.getValue(7),
                    NEWLINE);
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            //*  TIGR
            pnd_Prt_Total_Counters.reset();                                                                                                                               //Natural: RESET #PRT-TOTAL-COUNTERS
            //*    FOR #I 1 10
            //*    FOR #I 1 13
            //*    FOR #I 1 15
            //*    FOR #I 1 16
            //*    FOR #I 1 17
            //* *  FOR #I 1 18
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 21
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
            {
                pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total.nadd(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,1));                                                   //Natural: ADD #ACCUM-TOTAL-TBL ( #I,1 ) TO #PRT-ISS-TOTAL
                pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total.nadd(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,2));                                                  //Natural: ADD #ACCUM-TOTAL-TBL ( #I,2 ) TO #PRT-TIAA-TOTAL
                pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total.nadd(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,3));                                                  //Natural: ADD #ACCUM-TOTAL-TBL ( #I,3 ) TO #PRT-CREF-TOTAL
                pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total.nadd(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,4));                                                  //Natural: ADD #ACCUM-TOTAL-TBL ( #I,4 ) TO #PRT-PREM-TOTAL
                pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total.nadd(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,5));                                                   //Natural: ADD #ACCUM-TOTAL-TBL ( #I,5 ) TO #PRT-NEG-TOTAL
                pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total.nadd(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,6));                                                //Natural: ADD #ACCUM-TOTAL-TBL ( #I,6 ) TO #PRT-SPLITS-TOTAL
                pnd_Prt_Total_Counters_Pnd_Prt_Need_Total.nadd(pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,7));                                                  //Natural: ADD #ACCUM-TOTAL-TBL ( #I,7 ) TO #PRT-NEED-TOTAL
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,"TOTAL",new ColumnSpacing(6),pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total,new ColumnSpacing(4),pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total,new  //Natural: WRITE ( 1 ) NOTITLE 'TOTAL' 6X #PRT-ISS-TOTAL 4X #PRT-TIAA-TOTAL 3X #PRT-CREF-TOTAL 3X #PRT-PREM-TOTAL 3X #PRT-NEG-TOTAL 3X #PRT-SPLITS-TOTAL 3X #PRT-NEED-TOTAL
                ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total,new 
                ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Need_Total);
            if (condition(Global.isEscape())) return;
            //*  ----------
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
    }
    private void sub_Process_Lob_Totals() throws Exception                                                                                                                //Natural: PROCESS-LOB-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        short decideConditionsMet819 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #PROC-STAT-NUM;//Natural: VALUE '01'
        if (condition((work_Data_Pnd_Proc_Stat_Num.equals("01"))))
        {
            decideConditionsMet819++;
            //*  LS1
            pnd_Rpt_Counters_Pnd_Prem_App_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #PREM-APP-CNT
        }                                                                                                                                                                 //Natural: VALUE '02'
        else if (condition((work_Data_Pnd_Proc_Stat_Num.equals("02"))))
        {
            decideConditionsMet819++;
            pnd_Rpt_Counters_Pnd_Tiaa_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #TIAA-CNT
        }                                                                                                                                                                 //Natural: VALUE '03'
        else if (condition((work_Data_Pnd_Proc_Stat_Num.equals("03"))))
        {
            decideConditionsMet819++;
            //*  LS1
            pnd_Rpt_Counters_Pnd_Cont_Mann_Cnt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #CONT-MANN-CNT
        }                                                                                                                                                                 //Natural: VALUE '04'
        else if (condition((work_Data_Pnd_Proc_Stat_Num.equals("04"))))
        {
            decideConditionsMet819++;
            pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CONT-ISS-CNT
        }                                                                                                                                                                 //Natural: VALUE '06'
        else if (condition((work_Data_Pnd_Proc_Stat_Num.equals("06"))))
        {
            decideConditionsMet819++;
            pnd_Rpt_Counters_Pnd_Cref_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CREF-CNT
        }                                                                                                                                                                 //Natural: VALUE '07'
        else if (condition((work_Data_Pnd_Proc_Stat_Num.equals("07"))))
        {
            decideConditionsMet819++;
            pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt.nadd(1);                                                                                                                   //Natural: ADD 1 TO #NEG-ENROL-CNT
        }                                                                                                                                                                 //Natural: VALUE '08'
        else if (condition((work_Data_Pnd_Proc_Stat_Num.equals("08"))))
        {
            decideConditionsMet819++;
            pnd_Rpt_Counters_Pnd_Splits_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #SPLITS-CNT
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Rpt_Heading() throws Exception                                                                                                                       //Natural: RPT-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------
        pnd_Page_No.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #PAGE-NO
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"PGM ",Global.getPROGRAM(),new ColumnSpacing(9),"TEACHERS INSURANCE AND ANNUITY OF AMERICA",new        //Natural: WRITE ( 1 ) NOTITLE // 'PGM ' *PROGRAM 9X 'TEACHERS INSURANCE AND ANNUITY OF AMERICA' 32X 'RUN DATE ' *DATU / 27X 'COLLEGE RETIREMENT  EQUITIES FUND' 35X 'PAGE NO.' #PAGE-NO // 'REGION ' #PRT-REG 11X 'DAILY UNIT WORK FOR APPLICATIONS PROCESSED' 30X 'START DATE' DISPLAY-MAP-DATE ( EM = MM/DD/YY ) / 'NEED ' #PRT-NEED // 15X 'CONTRACT' 11X 'TIAA' 10X 'CREF' / 'PRODUCT' 10X 'ISSUED' 6X 'COMPANION' 5X 'COMPANION' 6X 'PREM/APP' 5X 'NEG ENROLL' 7X 'SPLITS' 9X 'TOTAL' / '-------' 5X '------------' 4X '-----------' 3X '-----------' 3X'-----------' 3X '-----------' 3X '-----------' 3X '-----------' /
            ColumnSpacing(32),"RUN DATE ",Global.getDATU(),NEWLINE,new ColumnSpacing(27),"COLLEGE RETIREMENT  EQUITIES FUND",new ColumnSpacing(35),"PAGE NO.",pnd_Page_No,NEWLINE,NEWLINE,"REGION ",pnd_Prt_Reg,new 
            ColumnSpacing(11),"DAILY UNIT WORK FOR APPLICATIONS PROCESSED",new ColumnSpacing(30),"START DATE",work_Data_Display_Map_Date, new ReportEditMask 
            ("MM/DD/YY"),NEWLINE,"NEED ",pnd_Prt_Need,NEWLINE,NEWLINE,new ColumnSpacing(15),"CONTRACT",new ColumnSpacing(11),"TIAA",new ColumnSpacing(10),"CREF",NEWLINE,"PRODUCT",new 
            ColumnSpacing(10),"ISSUED",new ColumnSpacing(6),"COMPANION",new ColumnSpacing(5),"COMPANION",new ColumnSpacing(6),"PREM/APP",new ColumnSpacing(5),"NEG ENROLL",new 
            ColumnSpacing(7),"SPLITS",new ColumnSpacing(9),"TOTAL",NEWLINE,"-------",new ColumnSpacing(5),"------------",new ColumnSpacing(4),"-----------",new 
            ColumnSpacing(3),"-----------",new ColumnSpacing(3),"-----------",new ColumnSpacing(3),"-----------",new ColumnSpacing(3),"-----------",new 
            ColumnSpacing(3),"-----------",NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Prod_Break_Processing() throws Exception                                                                                                             //Natural: PROD-BREAK-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  TIGR
        pnd_Rpt_Counters_Pnd_Ln_Cnt.compute(new ComputeParameters(false, pnd_Rpt_Counters_Pnd_Ln_Cnt), (pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt.add(pnd_Rpt_Counters_Pnd_Tiaa_Cnt).add(pnd_Rpt_Counters_Pnd_Cref_Cnt).add(pnd_Rpt_Counters_Pnd_Prem_App_Cnt).add(pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt).add(pnd_Rpt_Counters_Pnd_Splits_Cnt))); //Natural: COMPUTE #LN-CNT = ( #CONT-ISS-CNT + #TIAA-CNT + #CREF-CNT + #PREM-APP-CNT + #NEG-ENROL-CNT + #SPLITS-CNT )
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            if (condition(pnd_Prod_Table_Pnd_Prod_Cd.getValue(pnd_I).equals(pnd_Prev_Lob)))                                                                               //Natural: IF #PROD-CD ( #I ) = #PREV-LOB
            {
                //*  ----------  COMPARE PROGRAM COUNTS TO ARCHIVE COUNTS -----------
                //*     IF #LN-CNT �= #COMP-CNT                                     /* LS1
                //*        MOVE '*' TO #RPT-FLG(#I,*)                               /* LS1
                //*        MOVE '*' TO #REG-FLG(#I,*)                               /* LS1
                //*        MOVE '*' TO #ACCUM-FLG(#I,*)                             /* LS1
                //*        ADD #COMP-CNT TO #LN-CNT                                 /* LS1
                //*     END-IF                                                      /* LS1
                //*  ----------END COMPARE PROGRAM COUNTS TO ARCHIVE COUNTS ----------
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,1).setValue(pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt);                                                          //Natural: MOVE #CONT-ISS-CNT TO #RPT-TOTAL-TBL ( #I,1 )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,2).setValue(pnd_Rpt_Counters_Pnd_Tiaa_Cnt);                                                              //Natural: MOVE #TIAA-CNT TO #RPT-TOTAL-TBL ( #I,2 )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,3).setValue(pnd_Rpt_Counters_Pnd_Cref_Cnt);                                                              //Natural: MOVE #CREF-CNT TO #RPT-TOTAL-TBL ( #I,3 )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,4).setValue(pnd_Rpt_Counters_Pnd_Prem_App_Cnt);                                                          //Natural: MOVE #PREM-APP-CNT TO #RPT-TOTAL-TBL ( #I,4 )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,5).setValue(pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt);                                                         //Natural: MOVE #NEG-ENROL-CNT TO #RPT-TOTAL-TBL ( #I,5 )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,6).setValue(pnd_Rpt_Counters_Pnd_Splits_Cnt);                                                            //Natural: MOVE #SPLITS-CNT TO #RPT-TOTAL-TBL ( #I,6 )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,7).setValue(pnd_Rpt_Counters_Pnd_Ln_Cnt);                                                                //Natural: MOVE #LN-CNT TO #RPT-TOTAL-TBL ( #I,7 )
                pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,8).setValue(pnd_Rpt_Counters_Pnd_Cont_Mann_Cnt);                                                         //Natural: MOVE #CONT-MANN-CNT TO #RPT-TOTAL-TBL ( #I,8 )
                //* **  RESET  #RPT-FLG (*,*)
                //*  LS1
                                                                                                                                                                          //Natural: PERFORM PROCESS-ILOG-DATA
                sub_Process_Ilog_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  START LS1
                if (condition(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,1).notEquals(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(4))))                     //Natural: IF #RPT-TOTAL-TBL ( #I,1 ) NE #ILOG-ACCUM-VALUE ( 4 )
                {
                    pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue(pnd_I,1).setValue("*");                                                                                          //Natural: MOVE '*' TO #RPT-FLG ( #I,1 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,1).setValue(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(4));                                             //Natural: MOVE #ILOG-ACCUM-VALUE ( 4 ) TO #RPT-TOTAL-TBL-ERR ( #I,1 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,7).nadd(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(4));                                                 //Natural: ADD #ILOG-ACCUM-VALUE ( 4 ) TO #RPT-TOTAL-TBL-ERR ( #I,7 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,2).notEquals(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(6))))                     //Natural: IF #RPT-TOTAL-TBL ( #I,2 ) NE #ILOG-ACCUM-VALUE ( 6 )
                {
                    pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue(pnd_I,2).setValue("*");                                                                                          //Natural: MOVE '*' TO #RPT-FLG ( #I,2 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,2).setValue(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(6));                                             //Natural: MOVE #ILOG-ACCUM-VALUE ( 6 ) TO #RPT-TOTAL-TBL-ERR ( #I,2 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,7).nadd(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(6));                                                 //Natural: ADD #ILOG-ACCUM-VALUE ( 6 ) TO #RPT-TOTAL-TBL-ERR ( #I,7 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,3).notEquals(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(2))))                     //Natural: IF #RPT-TOTAL-TBL ( #I,3 ) NE #ILOG-ACCUM-VALUE ( 2 )
                {
                    pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue(pnd_I,3).setValue("*");                                                                                          //Natural: MOVE '*' TO #RPT-FLG ( #I,3 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,3).setValue(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(2));                                             //Natural: MOVE #ILOG-ACCUM-VALUE ( 2 ) TO #RPT-TOTAL-TBL-ERR ( #I,3 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,7).nadd(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(2));                                                 //Natural: ADD #ILOG-ACCUM-VALUE ( 2 ) TO #RPT-TOTAL-TBL-ERR ( #I,7 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,4).notEquals(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(1))))                     //Natural: IF #RPT-TOTAL-TBL ( #I,4 ) NE #ILOG-ACCUM-VALUE ( 1 )
                {
                    pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue(pnd_I,4).setValue("*");                                                                                          //Natural: MOVE '*' TO #RPT-FLG ( #I,4 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,4).setValue(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(1));                                             //Natural: MOVE #ILOG-ACCUM-VALUE ( 1 ) TO #RPT-TOTAL-TBL-ERR ( #I,4 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,7).nadd(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(1));                                                 //Natural: ADD #ILOG-ACCUM-VALUE ( 1 ) TO #RPT-TOTAL-TBL-ERR ( #I,7 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,5).notEquals(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(7))))                     //Natural: IF #RPT-TOTAL-TBL ( #I,5 ) NE #ILOG-ACCUM-VALUE ( 7 )
                {
                    pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue(pnd_I,5).setValue("*");                                                                                          //Natural: MOVE '*' TO #RPT-FLG ( #I,5 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,5).setValue(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(7));                                             //Natural: MOVE #ILOG-ACCUM-VALUE ( 7 ) TO #RPT-TOTAL-TBL-ERR ( #I,5 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,7).nadd(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(7));                                                 //Natural: ADD #ILOG-ACCUM-VALUE ( 7 ) TO #RPT-TOTAL-TBL-ERR ( #I,7 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,6).notEquals(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(8))))                     //Natural: IF #RPT-TOTAL-TBL ( #I,6 ) NE #ILOG-ACCUM-VALUE ( 8 )
                {
                    pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue(pnd_I,6).setValue("*");                                                                                          //Natural: MOVE '*' TO #RPT-FLG ( #I,6 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,6).setValue(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(8));                                             //Natural: MOVE #ILOG-ACCUM-VALUE ( 8 ) TO #RPT-TOTAL-TBL-ERR ( #I,6 )
                    pnd_Rpt_Total_Tbl_Err.getValue(pnd_I,7).nadd(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(8));                                                 //Natural: ADD #ILOG-ACCUM-VALUE ( 8 ) TO #RPT-TOTAL-TBL-ERR ( #I,7 )
                }                                                                                                                                                         //Natural: END-IF
                //*  #CONT-MANN-CNT  =  ASSIGNED ===>  NOT USED
                //*     IF  #RPT-TOTAL-TBL(#I,8)  NE  #ILOG-ACCUM-VALUE (3)
                //*         MOVE  '*'  TO  #RPT-FLG (#I,8)
                //*         MOVE  #ILOG-ACCUM-VALUE (3)  TO #RPT-TOTAL-TBL-ERR (#I,8)
                //*     END-IF                                                 /*  END  LS1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  TIGR
    private void sub_Accum_Totals() throws Exception                                                                                                                      //Natural: ACCUM-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I 1 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            if (condition(pnd_Prod_Table_Pnd_Prod_Cd.getValue(pnd_I).equals(pnd_Prev_Lob)))                                                                               //Natural: IF #PROD-CD ( #I ) = #PREV-LOB
            {
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,1).nadd(pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt);                                                              //Natural: ADD #CONT-ISS-CNT TO #REG-TOTAL-TBL ( #I,1 )
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,2).nadd(pnd_Rpt_Counters_Pnd_Tiaa_Cnt);                                                                  //Natural: ADD #TIAA-CNT TO #REG-TOTAL-TBL ( #I,2 )
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,3).nadd(pnd_Rpt_Counters_Pnd_Cref_Cnt);                                                                  //Natural: ADD #CREF-CNT TO #REG-TOTAL-TBL ( #I,3 )
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,4).nadd(pnd_Rpt_Counters_Pnd_Prem_App_Cnt);                                                              //Natural: ADD #PREM-APP-CNT TO #REG-TOTAL-TBL ( #I,4 )
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,5).nadd(pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt);                                                             //Natural: ADD #NEG-ENROL-CNT TO #REG-TOTAL-TBL ( #I,5 )
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,6).nadd(pnd_Rpt_Counters_Pnd_Splits_Cnt);                                                                //Natural: ADD #SPLITS-CNT TO #REG-TOTAL-TBL ( #I,6 )
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,7).nadd(pnd_Rpt_Counters_Pnd_Ln_Cnt);                                                                    //Natural: ADD #LN-CNT TO #REG-TOTAL-TBL ( #I,7 )
                pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,8).nadd(pnd_Rpt_Counters_Pnd_Cont_Mann_Cnt);                                                             //Natural: ADD #CONT-MANN-CNT TO #REG-TOTAL-TBL ( #I,8 )
            }                                                                                                                                                             //Natural: END-IF
            //*  TIGR
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            if (condition(pnd_Prod_Table_Pnd_Prod_Cd.getValue(pnd_I).equals(pnd_Prev_Lob)))                                                                               //Natural: IF #PROD-CD ( #I ) = #PREV-LOB
            {
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,1).nadd(pnd_Rpt_Counters_Pnd_Cont_Iss_Cnt);                                                          //Natural: ADD #CONT-ISS-CNT TO #ACCUM-TOTAL-TBL ( #I,1 )
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,2).nadd(pnd_Rpt_Counters_Pnd_Tiaa_Cnt);                                                              //Natural: ADD #TIAA-CNT TO #ACCUM-TOTAL-TBL ( #I,2 )
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,3).nadd(pnd_Rpt_Counters_Pnd_Cref_Cnt);                                                              //Natural: ADD #CREF-CNT TO #ACCUM-TOTAL-TBL ( #I,3 )
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,4).nadd(pnd_Rpt_Counters_Pnd_Prem_App_Cnt);                                                          //Natural: ADD #PREM-APP-CNT TO #ACCUM-TOTAL-TBL ( #I,4 )
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,5).nadd(pnd_Rpt_Counters_Pnd_Neg_Enrol_Cnt);                                                         //Natural: ADD #NEG-ENROL-CNT TO #ACCUM-TOTAL-TBL ( #I,5 )
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,6).nadd(pnd_Rpt_Counters_Pnd_Splits_Cnt);                                                            //Natural: ADD #SPLITS-CNT TO #ACCUM-TOTAL-TBL ( #I,6 )
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,7).nadd(pnd_Rpt_Counters_Pnd_Ln_Cnt);                                                                //Natural: ADD #LN-CNT TO #ACCUM-TOTAL-TBL ( #I,7 )
                pnd_Accum_Tot_Tbl_Pnd_Accum_Total_Tbl.getValue(pnd_I,8).nadd(pnd_Rpt_Counters_Pnd_Cont_Mann_Cnt);                                                         //Natural: ADD #CONT-MANN-CNT TO #ACCUM-TOTAL-TBL ( #I,8 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Need_Break_Processing() throws Exception                                                                                                             //Natural: NEED-BREAK-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  TIGR
                                                                                                                                                                          //Natural: PERFORM PROD-BREAK-PROCESSING
        sub_Prod_Break_Processing();
        if (condition(Global.isEscape())) {return;}
        FOR06:                                                                                                                                                            //Natural: FOR #PROD-CNT 1 21
        for (pnd_Prod_Cnt.setValue(1); condition(pnd_Prod_Cnt.lessOrEqual(21)); pnd_Prod_Cnt.nadd(1))
        {
            pnd_Prt_Lob.setValue(pnd_Prod_Table_Pnd_Prod_Cd.getValue(pnd_Prod_Cnt));                                                                                      //Natural: MOVE #PROD-CD ( #PROD-CNT ) TO #PRT-LOB
            pnd_Print_Counters_Pnd_Prt_Cont_Iss.setValue(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_Prod_Cnt,1));                                                     //Natural: MOVE #RPT-TOTAL-TBL ( #PROD-CNT,1 ) TO #PRT-CONT-ISS
            pnd_Print_Counters_Pnd_Prt_Tiaa.setValue(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_Prod_Cnt,2));                                                         //Natural: MOVE #RPT-TOTAL-TBL ( #PROD-CNT,2 ) TO #PRT-TIAA
            pnd_Print_Counters_Pnd_Prt_Cref.setValue(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_Prod_Cnt,3));                                                         //Natural: MOVE #RPT-TOTAL-TBL ( #PROD-CNT,3 ) TO #PRT-CREF
            pnd_Print_Counters_Pnd_Prt_Prem_App.setValue(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_Prod_Cnt,4));                                                     //Natural: MOVE #RPT-TOTAL-TBL ( #PROD-CNT,4 ) TO #PRT-PREM-APP
            pnd_Print_Counters_Pnd_Prt_Neg_Enrol.setValue(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_Prod_Cnt,5));                                                    //Natural: MOVE #RPT-TOTAL-TBL ( #PROD-CNT,5 ) TO #PRT-NEG-ENROL
            pnd_Print_Counters_Pnd_Prt_Splits.setValue(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_Prod_Cnt,6));                                                       //Natural: MOVE #RPT-TOTAL-TBL ( #PROD-CNT,6 ) TO #PRT-SPLITS
            pnd_Print_Counters_Pnd_Prt_Ln.setValue(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_Prod_Cnt,7));                                                           //Natural: MOVE #RPT-TOTAL-TBL ( #PROD-CNT,7 ) TO #PRT-LN
            pnd_Prt_Fl.getValue("*").reset();                                                                                                                             //Natural: RESET #PRT-FL ( * )
            FOR07:                                                                                                                                                        //Natural: FOR #F = 1 TO 7
            for (pnd_F.setValue(1); condition(pnd_F.lessOrEqual(7)); pnd_F.nadd(1))
            {
                //* *      IF (#RPT-TOTAL-TBL(#PROD-CNT,#F) > 0  AND                 /* LS1
                //*  LS1
                if (condition(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Flg.getValue(pnd_Prod_Cnt,pnd_F).equals("*")))                                                                      //Natural: IF #RPT-FLG ( #PROD-CNT,#F ) = '*'
                {
                    pnd_Prt_Fl.getValue(pnd_F).setValue("*");                                                                                                             //Natural: MOVE '*' TO #PRT-FL ( #F )
                    //* *          ESCAPE TOP                                            /* LS1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),pnd_Prt_Lob,new ColumnSpacing(6),pnd_Print_Counters_Pnd_Prt_Cont_Iss,pnd_Prt_Fl.getValue(1),new  //Natural: WRITE ( 1 ) NOTITLE 1X #PRT-LOB 6X #PRT-CONT-ISS #PRT-FL ( 1 ) 2X #PRT-TIAA #PRT-FL ( 2 ) 1X #PRT-CREF #PRT-FL ( 3 ) 1X #PRT-PREM-APP #PRT-FL ( 4 ) 1X #PRT-NEG-ENROL 1X #PRT-FL ( 5 ) #PRT-SPLITS #PRT-FL ( 6 ) #PRT-LN #PRT-FL ( 7 ) /
                ColumnSpacing(2),pnd_Print_Counters_Pnd_Prt_Tiaa,pnd_Prt_Fl.getValue(2),new ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Cref,pnd_Prt_Fl.getValue(3),new 
                ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Prem_App,pnd_Prt_Fl.getValue(4),new ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Neg_Enrol,new 
                ColumnSpacing(1),pnd_Prt_Fl.getValue(5),pnd_Print_Counters_Pnd_Prt_Splits,pnd_Prt_Fl.getValue(6),pnd_Print_Counters_Pnd_Prt_Ln,pnd_Prt_Fl.getValue(7),
                NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  START LS1
            pnd_Print_Counters.reset();                                                                                                                                   //Natural: RESET #PRINT-COUNTERS
            pnd_Print_Counters_Pnd_Prt_Cont_Iss.setValue(pnd_Rpt_Total_Tbl_Err.getValue(pnd_Prod_Cnt,1));                                                                 //Natural: MOVE #RPT-TOTAL-TBL-ERR ( #PROD-CNT,1 ) TO #PRT-CONT-ISS
            pnd_Print_Counters_Pnd_Prt_Tiaa.setValue(pnd_Rpt_Total_Tbl_Err.getValue(pnd_Prod_Cnt,2));                                                                     //Natural: MOVE #RPT-TOTAL-TBL-ERR ( #PROD-CNT,2 ) TO #PRT-TIAA
            pnd_Print_Counters_Pnd_Prt_Cref.setValue(pnd_Rpt_Total_Tbl_Err.getValue(pnd_Prod_Cnt,3));                                                                     //Natural: MOVE #RPT-TOTAL-TBL-ERR ( #PROD-CNT,3 ) TO #PRT-CREF
            pnd_Print_Counters_Pnd_Prt_Prem_App.setValue(pnd_Rpt_Total_Tbl_Err.getValue(pnd_Prod_Cnt,4));                                                                 //Natural: MOVE #RPT-TOTAL-TBL-ERR ( #PROD-CNT,4 ) TO #PRT-PREM-APP
            pnd_Print_Counters_Pnd_Prt_Neg_Enrol.setValue(pnd_Rpt_Total_Tbl_Err.getValue(pnd_Prod_Cnt,5));                                                                //Natural: MOVE #RPT-TOTAL-TBL-ERR ( #PROD-CNT,5 ) TO #PRT-NEG-ENROL
            pnd_Print_Counters_Pnd_Prt_Splits.setValue(pnd_Rpt_Total_Tbl_Err.getValue(pnd_Prod_Cnt,6));                                                                   //Natural: MOVE #RPT-TOTAL-TBL-ERR ( #PROD-CNT,6 ) TO #PRT-SPLITS
            pnd_Print_Counters_Pnd_Prt_Ln.setValue(pnd_Rpt_Total_Tbl_Err.getValue(pnd_Prod_Cnt,7));                                                                       //Natural: MOVE #RPT-TOTAL-TBL-ERR ( #PROD-CNT,7 ) TO #PRT-LN
            if (condition(pnd_Print_Counters_Pnd_Prt_Cont_Iss.notEquals(getZero()) || pnd_Print_Counters_Pnd_Prt_Tiaa.notEquals(getZero()) || pnd_Print_Counters_Pnd_Prt_Cref.notEquals(getZero())  //Natural: IF #PRT-CONT-ISS NE 0 OR #PRT-TIAA NE 0 OR #PRT-CREF NE 0 OR #PRT-PREM-APP NE 0 OR #PRT-NEG-ENROL NE 0 OR #PRT-SPLITS NE 0
                || pnd_Print_Counters_Pnd_Prt_Prem_App.notEquals(getZero()) || pnd_Print_Counters_Pnd_Prt_Neg_Enrol.notEquals(getZero()) || pnd_Print_Counters_Pnd_Prt_Splits.notEquals(getZero())))
            {
                pnd_Prt_Fl.getValue("*").reset();                                                                                                                         //Natural: RESET #PRT-FL ( * )
                getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(11),pnd_Print_Counters_Pnd_Prt_Cont_Iss,pnd_Prt_Fl.getValue(1),new ColumnSpacing(2),pnd_Print_Counters_Pnd_Prt_Tiaa,pnd_Prt_Fl.getValue(2),new  //Natural: WRITE ( 1 ) NOTITLE 11X #PRT-CONT-ISS #PRT-FL ( 1 ) 2X #PRT-TIAA #PRT-FL ( 2 ) 1X #PRT-CREF #PRT-FL ( 3 ) 1X #PRT-PREM-APP #PRT-FL ( 4 ) 1X #PRT-NEG-ENROL 1X #PRT-FL ( 5 ) #PRT-SPLITS #PRT-FL ( 6 ) #PRT-LN #PRT-FL ( 7 ) /
                    ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Cref,pnd_Prt_Fl.getValue(3),new ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Prem_App,pnd_Prt_Fl.getValue(4),new 
                    ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Neg_Enrol,new ColumnSpacing(1),pnd_Prt_Fl.getValue(5),pnd_Print_Counters_Pnd_Prt_Splits,
                    pnd_Prt_Fl.getValue(6),pnd_Print_Counters_Pnd_Prt_Ln,pnd_Prt_Fl.getValue(7),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    END LS1
            }                                                                                                                                                             //Natural: END-IF
            //*  TIGR
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #I 1 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total.nadd(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,1));                                                           //Natural: ADD #RPT-TOTAL-TBL ( #I,1 ) TO #PRT-ISS-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total.nadd(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,2));                                                          //Natural: ADD #RPT-TOTAL-TBL ( #I,2 ) TO #PRT-TIAA-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total.nadd(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,3));                                                          //Natural: ADD #RPT-TOTAL-TBL ( #I,3 ) TO #PRT-CREF-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total.nadd(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,4));                                                          //Natural: ADD #RPT-TOTAL-TBL ( #I,4 ) TO #PRT-PREM-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total.nadd(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,5));                                                           //Natural: ADD #RPT-TOTAL-TBL ( #I,5 ) TO #PRT-NEG-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total.nadd(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,6));                                                        //Natural: ADD #RPT-TOTAL-TBL ( #I,6 ) TO #PRT-SPLITS-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Need_Total.nadd(pnd_Rpt_Tot_Tbl_Pnd_Rpt_Total_Tbl.getValue(pnd_I,7));                                                          //Natural: ADD #RPT-TOTAL-TBL ( #I,7 ) TO #PRT-NEED-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL",new ColumnSpacing(6),pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total,new ColumnSpacing(4),pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total,new  //Natural: WRITE ( 1 ) NOTITLE 'TOTAL' 6X #PRT-ISS-TOTAL 4X #PRT-TIAA-TOTAL 3X #PRT-CREF-TOTAL 3X #PRT-PREM-TOTAL 3X #PRT-NEG-TOTAL 3X #PRT-SPLITS-TOTAL 3X #PRT-NEED-TOTAL
            ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total,new 
            ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Need_Total);
        if (Global.isEscape()) return;
    }
    private void sub_Reg_Break_Processing() throws Exception                                                                                                              //Natural: REG-BREAK-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------
                                                                                                                                                                          //Natural: PERFORM NEED-BREAK-PROCESSING
        sub_Need_Break_Processing();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCUM-TOTALS
        sub_Accum_Totals();
        if (condition(Global.isEscape())) {return;}
        pnd_Prt_Reg.setValue(pnd_Prev_Reg);                                                                                                                               //Natural: MOVE #PREV-REG TO #PRT-REG
        pnd_Prt_Need.setValue("TOTAL");                                                                                                                                   //Natural: MOVE 'TOTAL' TO #PRT-NEED
        //*  TIGR
                                                                                                                                                                          //Natural: PERFORM RPT-HEADING
        sub_Rpt_Heading();
        if (condition(Global.isEscape())) {return;}
        FOR09:                                                                                                                                                            //Natural: FOR #PROD-CNT 1 21
        for (pnd_Prod_Cnt.setValue(1); condition(pnd_Prod_Cnt.lessOrEqual(21)); pnd_Prod_Cnt.nadd(1))
        {
            pnd_Prt_Lob.setValue(pnd_Prod_Table_Pnd_Prod_Cd.getValue(pnd_Prod_Cnt));                                                                                      //Natural: MOVE #PROD-CD ( #PROD-CNT ) TO #PRT-LOB
            pnd_Print_Counters_Pnd_Prt_Cont_Iss.setValue(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_Prod_Cnt,1));                                                     //Natural: MOVE #REG-TOTAL-TBL ( #PROD-CNT,1 ) TO #PRT-CONT-ISS
            pnd_Print_Counters_Pnd_Prt_Tiaa.setValue(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_Prod_Cnt,2));                                                         //Natural: MOVE #REG-TOTAL-TBL ( #PROD-CNT,2 ) TO #PRT-TIAA
            pnd_Print_Counters_Pnd_Prt_Cref.setValue(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_Prod_Cnt,3));                                                         //Natural: MOVE #REG-TOTAL-TBL ( #PROD-CNT,3 ) TO #PRT-CREF
            pnd_Print_Counters_Pnd_Prt_Prem_App.setValue(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_Prod_Cnt,4));                                                     //Natural: MOVE #REG-TOTAL-TBL ( #PROD-CNT,4 ) TO #PRT-PREM-APP
            pnd_Print_Counters_Pnd_Prt_Neg_Enrol.setValue(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_Prod_Cnt,5));                                                    //Natural: MOVE #REG-TOTAL-TBL ( #PROD-CNT,5 ) TO #PRT-NEG-ENROL
            pnd_Print_Counters_Pnd_Prt_Splits.setValue(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_Prod_Cnt,6));                                                       //Natural: MOVE #REG-TOTAL-TBL ( #PROD-CNT,6 ) TO #PRT-SPLITS
            pnd_Print_Counters_Pnd_Prt_Ln.setValue(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_Prod_Cnt,7));                                                           //Natural: MOVE #REG-TOTAL-TBL ( #PROD-CNT,7 ) TO #PRT-LN
            pnd_Prt_Fl.getValue("*").reset();                                                                                                                             //Natural: RESET #PRT-FL ( * )
            //*   FOR  #F = 1 TO 7                                             /* LS1
            //*     IF (#REG-TOTAL-TBL(#PROD-CNT,#F) > 0                       /* LS1
            //*     AND #REG-FLG(#PROD-CNT,#F) = '*')                          /* LS1
            //*         MOVE '*' TO #PRT-FL(#F)                                /* LS1
            //*         ESCAPE TOP                                             /* LS1
            //*      END-IF                                                    /* LS1
            //*   END-FOR                                                      /* LS1
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(1),pnd_Prt_Lob,new ColumnSpacing(6),pnd_Print_Counters_Pnd_Prt_Cont_Iss,pnd_Prt_Fl.getValue(1),new  //Natural: WRITE ( 1 ) NOTITLE 1X #PRT-LOB 6X #PRT-CONT-ISS #PRT-FL ( 1 ) 2X #PRT-TIAA #PRT-FL ( 2 ) 1X #PRT-CREF #PRT-FL ( 3 ) 1X #PRT-PREM-APP #PRT-FL ( 4 ) 1X #PRT-NEG-ENROL 1X #PRT-FL ( 5 ) #PRT-SPLITS #PRT-FL ( 6 ) #PRT-LN #PRT-FL ( 7 ) /
                ColumnSpacing(2),pnd_Print_Counters_Pnd_Prt_Tiaa,pnd_Prt_Fl.getValue(2),new ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Cref,pnd_Prt_Fl.getValue(3),new 
                ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Prem_App,pnd_Prt_Fl.getValue(4),new ColumnSpacing(1),pnd_Print_Counters_Pnd_Prt_Neg_Enrol,new 
                ColumnSpacing(1),pnd_Prt_Fl.getValue(5),pnd_Print_Counters_Pnd_Prt_Splits,pnd_Prt_Fl.getValue(6),pnd_Print_Counters_Pnd_Prt_Ln,pnd_Prt_Fl.getValue(7),
                NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TIGR
        pnd_Prt_Total_Counters.reset();                                                                                                                                   //Natural: RESET #PRT-TOTAL-COUNTERS
        FOR10:                                                                                                                                                            //Natural: FOR #I 1 21
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(21)); pnd_I.nadd(1))
        {
            pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total.nadd(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,1));                                                           //Natural: ADD #REG-TOTAL-TBL ( #I,1 ) TO #PRT-ISS-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total.nadd(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,2));                                                          //Natural: ADD #REG-TOTAL-TBL ( #I,2 ) TO #PRT-TIAA-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total.nadd(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,3));                                                          //Natural: ADD #REG-TOTAL-TBL ( #I,3 ) TO #PRT-CREF-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total.nadd(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,4));                                                          //Natural: ADD #REG-TOTAL-TBL ( #I,4 ) TO #PRT-PREM-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total.nadd(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,5));                                                           //Natural: ADD #REG-TOTAL-TBL ( #I,5 ) TO #PRT-NEG-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total.nadd(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,6));                                                        //Natural: ADD #REG-TOTAL-TBL ( #I,6 ) TO #PRT-SPLITS-TOTAL
            pnd_Prt_Total_Counters_Pnd_Prt_Need_Total.nadd(pnd_Reg_Tot_Tbl_Pnd_Reg_Total_Tbl.getValue(pnd_I,7));                                                          //Natural: ADD #REG-TOTAL-TBL ( #I,7 ) TO #PRT-NEED-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL",new ColumnSpacing(6),pnd_Prt_Total_Counters_Pnd_Prt_Iss_Total,new ColumnSpacing(4),pnd_Prt_Total_Counters_Pnd_Prt_Tiaa_Total,new  //Natural: WRITE ( 1 ) NOTITLE 'TOTAL' 6X #PRT-ISS-TOTAL 4X #PRT-TIAA-TOTAL 3X #PRT-CREF-TOTAL 3X #PRT-PREM-TOTAL 3X #PRT-NEG-TOTAL 3X #PRT-SPLITS-TOTAL 3X #PRT-NEED-TOTAL
            ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Cref_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Prem_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Neg_Total,new 
            ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Splits_Total,new ColumnSpacing(3),pnd_Prt_Total_Counters_Pnd_Prt_Need_Total);
        if (Global.isEscape()) return;
        //*  -------------
    }
    //*  LS1
    private void sub_Process_Ilog_Data() throws Exception                                                                                                                 //Natural: PROCESS-ILOG-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ===================================
        pnd_Ilog_Position.reset();                                                                                                                                        //Natural: RESET #ILOG-POSITION #ILOG-ACCUM-VALUE ( * )
        pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue("*").reset();
        pnd_Bucket_Name_Array_Pnd_Bucket_Name.getValue(1).setValue("-PREMAPP/MANU");                                                                                      //Natural: ASSIGN #BUCKET-NAME ( 1 ) = '-PREMAPP/MANU'
        ldaAppl020.getVw_app_Ilog_B_Rcrd_View().startDatabaseRead                                                                                                         //Natural: READ ( 1 ) APP-ILOG-B-RCRD-VIEW BY RCRD-CDE STARTING FROM #PREV-BUCKET
        (
        "READ01",
        new Wc[] { new Wc("RCRD_CDE", ">=", pnd_Prev_Bucket, WcType.BY) },
        new Oc[] { new Oc("RCRD_CDE", "ASC") },
        1
        );
        READ01:
        while (condition(ldaAppl020.getVw_app_Ilog_B_Rcrd_View().readNextRow("READ01")))
        {
            if (condition(ldaAppl020.getApp_Ilog_B_Rcrd_View_Rcrd_Cde().notEquals(pnd_Prev_Bucket)))                                                                      //Natural: IF RCRD-CDE NE #PREV-BUCKET
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  457(B) B.E. CHANGED 80 TO 99
            }                                                                                                                                                             //Natural: END-IF
            FOR11:                                                                                                                                                        //Natural: FOR #J = 1 TO 99
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(99)); pnd_J.nadd(1))
            {
                if (condition(ldaAppl020.getApp_Ilog_B_Rcrd_View_Buckets_Nme().getValue(pnd_J).equals(pnd_Bucket_Name_Array_Pnd_Ilog_Bucket_Name.getValue(1))))           //Natural: IF BUCKETS-NME ( #J ) = #ILOG-BUCKET-NAME ( 1 )
                {
                    pnd_Ilog_Position.setValue(pnd_J);                                                                                                                    //Natural: ASSIGN #ILOG-POSITION = #J
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaAppl020.getVw_app_Ilog_A_Rcrd_View().startDatabaseRead                                                                                                         //Natural: READ APP-ILOG-A-RCRD-VIEW BY START-DTE STARTING FROM #WK-START-DATE ENDING AT #WK-END-DATE
        (
        "READ02",
        new Wc[] { new Wc("START_DTE", ">=", work_Data_Pnd_Wk_Start_Date, "And", WcType.BY) ,
        new Wc("START_DTE", "<=", work_Data_Pnd_Wk_End_Date, WcType.BY) },
        new Oc[] { new Oc("START_DTE", "ASC") }
        );
        READ02:
        while (condition(ldaAppl020.getVw_app_Ilog_A_Rcrd_View().readNextRow("READ02")))
        {
            if (condition(ldaAppl020.getApp_Ilog_A_Rcrd_View_Start_Dte().greater(work_Data_Pnd_Wk_End_Date)))                                                             //Natural: IF START-DTE > #WK-END-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl020.getApp_Ilog_A_Rcrd_View_Rcrd_Cde().notEquals(pnd_Prev_Accum) || ldaAppl020.getApp_Ilog_A_Rcrd_View_Prdct_Cde().notEquals(pnd_Prev_Lob))) //Natural: IF RCRD-CDE NE #PREV-ACCUM OR PRDCT-CDE NE #PREV-LOB
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_K.setValue(1);                                                                                                                                            //Natural: ASSIGN #K = 1
            pnd_L.compute(new ComputeParameters(false, pnd_L), pnd_Ilog_Position.add(9));                                                                                 //Natural: ASSIGN #L = #ILOG-POSITION + 9
            FOR12:                                                                                                                                                        //Natural: FOR #J = #ILOG-POSITION TO #L
            for (pnd_J.setValue(pnd_Ilog_Position); condition(pnd_J.lessOrEqual(pnd_L)); pnd_J.nadd(1))
            {
                pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(pnd_K).compute(new ComputeParameters(false, pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(pnd_K)),  //Natural: COMPUTE #ILOG-ACCUM-VALUE ( #K ) = BUCKETS ( #J ) + #ILOG-ACCUM-VALUE ( #K )
                    ldaAppl020.getApp_Ilog_A_Rcrd_View_Buckets().getValue(pnd_J).add(pnd_Bucket_Name_Array_Pnd_Ilog_Accum_Value.getValue(pnd_K)));
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  LS1
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52 ZP=OFF");
    }
}
