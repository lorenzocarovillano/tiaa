/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:27 PM
**        * FROM NATURAL PROGRAM : Appb319
************************************************************
**        * FILE NAME            : Appb319.java
**        * CLASS NAME           : Appb319
**        * INSTANCE NAME        : Appb319
************************************************************
************************************************************************
* PROGRAM:  APPB319
*
* FUNCTION: THIS PROGRAM CREATES A REPORT OF ALL CONTRACTS ISSUED
*           TO NY STATE RESIDENTS.  IT IS RUN ON A WEEKLY BASIS.
*
* CREATED:  01/20/2006    JIM CAMPBELL FOR NEW YORK REGULATION 60
*                           CONTROL REPORTS (IT PROJECT REQUEST # 465)
*
* HISTORY
*
* 02/14/2006   CAMPBELL  SEPARATE ATRA CONTRACTS FROM OTHER RAS; ADD
*                        INTERNAL SORT BY CONTRACT TYPE & NUMBER.  (JC1)
* 04/21/2006   CAMPBELL  ADD USER NAME, MIT UNIT TO REPORT (LEGACY NON-
*                        IRA CONTRACTS ONLY.)  ADD LEGACY/OMNI INDICATOR
*                        TO REPORT.  SORT BY CONTRACT TYPE, LEGACY/OMNI
*                        INDICATOR, AND CONTRACT NUMBER. (JC2)
* 02/21/2007   GATES     USED SUB PLAN TO IDENTIFY ATRAS FROM OMNI.
*                        SEE ATRA KG
* 06/19/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE. **
* 08/13/2018  L SHU      REMOVE ACIN3590 THAT READS ACIS1105    IIS1
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb319 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_prap;
    private DbsField prap_Ap_Coll_Code;
    private DbsField prap_Ap_Release_Ind;
    private DbsField prap_Ap_Lob;
    private DbsField prap_Ap_Lob_Type;
    private DbsField prap_Ap_Record_Type;
    private DbsField prap_Ap_Cor_Last_Nme;
    private DbsField prap_Ap_Cor_First_Nme;
    private DbsField prap_Ap_Cor_Mddle_Nme;
    private DbsField prap_Ap_Pin_Nbr;
    private DbsField prap_Ap_Tiaa_Cntrct;
    private DbsField prap_Ap_Dt_Ent_Sys;
    private DbsField prap_Ap_Status;
    private DbsField prap_Ap_Coll_St_Cd;
    private DbsField prap_Ap_Current_State_Code;
    private DbsField prap_Ap_T_Doi;
    private DbsField prap_Ap_Racf_Id;

    private DbsGroup prap__R_Field_1;
    private DbsField prap_Pnd_Racf_Id;

    private DbsGroup prap_Ap_Mit_Request;
    private DbsField prap_Ap_Mit_Unit;
    private DbsField prap_Ap_Sgrd_Plan_No;
    private DbsField prap_Ap_Sgrd_Subplan_No;
    private DbsField pnd_Omni_Legacy_Ind;
    private DbsField pnd_Change_By_Nme;
    private DbsField pnd_Ap_Dt_Ent_Sys;
    private DbsField pnd_Ap_T_Doi;
    private DbsField pnd_Input_Date_Range;

    private DbsGroup pnd_Input_Date_Range__R_Field_2;
    private DbsField pnd_Input_Date_Range_Pnd_Input_Start;
    private DbsField pnd_Input_Date_Range_Pnd_Input_End;
    private DbsField pnd_Full_Name;
    private DbsField pnd_Ny_Code;
    private DbsField pnd_Type;
    private DbsField pnd_State;
    private DbsField pnd_Date_Entered_Sys;

    private DbsGroup pnd_Date_Entered_Sys__R_Field_3;
    private DbsField pnd_Date_Entered_Sys_Pnd_Date_Entered_Sys_A;
    private DbsField pnd_Prap_Date;
    private DbsField pnd_Start_Date;
    private DbsField pnd_End_Date;
    private DbsField pnd_Today_Abbr;
    private DbsField pnd_Abbr_Table;
    private DbsField pnd_Count;
    private DbsField pnd_I;
    private DbsField pnd_Delim;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_prap = new DataAccessProgramView(new NameInfo("vw_prap", "PRAP"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        prap_Ap_Coll_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_COLL_CODE");
        prap_Ap_Release_Ind = vw_prap.getRecord().newFieldInGroup("prap_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        prap_Ap_Lob = vw_prap.getRecord().newFieldInGroup("prap_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB");
        prap_Ap_Lob.setDdmHeader("LOB");
        prap_Ap_Lob_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        prap_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        prap_Ap_Record_Type = vw_prap.getRecord().newFieldInGroup("prap_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RECORD_TYPE");
        prap_Ap_Cor_Last_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_LAST_NME");
        prap_Ap_Cor_First_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_FIRST_NME");
        prap_Ap_Cor_Mddle_Nme = vw_prap.getRecord().newFieldInGroup("prap_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "AP_COR_MDDLE_NME");
        prap_Ap_Pin_Nbr = vw_prap.getRecord().newFieldInGroup("prap_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        prap_Ap_Tiaa_Cntrct = vw_prap.getRecord().newFieldInGroup("prap_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        prap_Ap_Dt_Ent_Sys = vw_prap.getRecord().newFieldInGroup("prap_Ap_Dt_Ent_Sys", "AP-DT-ENT-SYS", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, 
            "AP_DT_ENT_SYS");
        prap_Ap_Status = vw_prap.getRecord().newFieldInGroup("prap_Ap_Status", "AP-STATUS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_STATUS");
        prap_Ap_Coll_St_Cd = vw_prap.getRecord().newFieldInGroup("prap_Ap_Coll_St_Cd", "AP-COLL-ST-CD", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_COLL_ST_CD");
        prap_Ap_Current_State_Code = vw_prap.getRecord().newFieldInGroup("prap_Ap_Current_State_Code", "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "AP_CURRENT_STATE_CODE");
        prap_Ap_T_Doi = vw_prap.getRecord().newFieldInGroup("prap_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "AP_T_DOI");
        prap_Ap_Racf_Id = vw_prap.getRecord().newFieldInGroup("prap_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 16, RepeatingFieldStrategy.None, "AP_RACF_ID");

        prap__R_Field_1 = vw_prap.getRecord().newGroupInGroup("prap__R_Field_1", "REDEFINE", prap_Ap_Racf_Id);
        prap_Pnd_Racf_Id = prap__R_Field_1.newFieldInGroup("prap_Pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 8);

        prap_Ap_Mit_Request = vw_prap.getRecord().newGroupInGroup("prap_Ap_Mit_Request", "AP-MIT-REQUEST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        prap_Ap_Mit_Unit = prap_Ap_Mit_Request.newFieldArrayInGroup("prap_Ap_Mit_Unit", "AP-MIT-UNIT", FieldType.STRING, 8, new DbsArrayController(1, 
            1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MIT_UNIT", "ACIS_ANNTY_PRAP_AP_MIT_REQUEST");
        prap_Ap_Sgrd_Plan_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_PLAN_NO");
        prap_Ap_Sgrd_Subplan_No = vw_prap.getRecord().newFieldInGroup("prap_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "AP_SGRD_SUBPLAN_NO");
        registerRecord(vw_prap);

        pnd_Omni_Legacy_Ind = localVariables.newFieldInRecord("pnd_Omni_Legacy_Ind", "#OMNI-LEGACY-IND", FieldType.STRING, 6);
        pnd_Change_By_Nme = localVariables.newFieldInRecord("pnd_Change_By_Nme", "#CHANGE-BY-NME", FieldType.STRING, 30);
        pnd_Ap_Dt_Ent_Sys = localVariables.newFieldInRecord("pnd_Ap_Dt_Ent_Sys", "#AP-DT-ENT-SYS", FieldType.NUMERIC, 8);
        pnd_Ap_T_Doi = localVariables.newFieldInRecord("pnd_Ap_T_Doi", "#AP-T-DOI", FieldType.NUMERIC, 4);
        pnd_Input_Date_Range = localVariables.newFieldInRecord("pnd_Input_Date_Range", "#INPUT-DATE-RANGE", FieldType.STRING, 16);

        pnd_Input_Date_Range__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Date_Range__R_Field_2", "REDEFINE", pnd_Input_Date_Range);
        pnd_Input_Date_Range_Pnd_Input_Start = pnd_Input_Date_Range__R_Field_2.newFieldInGroup("pnd_Input_Date_Range_Pnd_Input_Start", "#INPUT-START", 
            FieldType.STRING, 8);
        pnd_Input_Date_Range_Pnd_Input_End = pnd_Input_Date_Range__R_Field_2.newFieldInGroup("pnd_Input_Date_Range_Pnd_Input_End", "#INPUT-END", FieldType.STRING, 
            8);
        pnd_Full_Name = localVariables.newFieldInRecord("pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 32);
        pnd_Ny_Code = localVariables.newFieldInRecord("pnd_Ny_Code", "#NY-CODE", FieldType.STRING, 2);
        pnd_Type = localVariables.newFieldInRecord("pnd_Type", "#TYPE", FieldType.STRING, 8);
        pnd_State = localVariables.newFieldInRecord("pnd_State", "#STATE", FieldType.STRING, 2);
        pnd_Date_Entered_Sys = localVariables.newFieldInRecord("pnd_Date_Entered_Sys", "#DATE-ENTERED-SYS", FieldType.NUMERIC, 8);

        pnd_Date_Entered_Sys__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_Entered_Sys__R_Field_3", "REDEFINE", pnd_Date_Entered_Sys);
        pnd_Date_Entered_Sys_Pnd_Date_Entered_Sys_A = pnd_Date_Entered_Sys__R_Field_3.newFieldInGroup("pnd_Date_Entered_Sys_Pnd_Date_Entered_Sys_A", "#DATE-ENTERED-SYS-A", 
            FieldType.STRING, 8);
        pnd_Prap_Date = localVariables.newFieldInRecord("pnd_Prap_Date", "#PRAP-DATE", FieldType.DATE);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.DATE);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.DATE);
        pnd_Today_Abbr = localVariables.newFieldInRecord("pnd_Today_Abbr", "#TODAY-ABBR", FieldType.STRING, 3);
        pnd_Abbr_Table = localVariables.newFieldArrayInRecord("pnd_Abbr_Table", "#ABBR-TABLE", FieldType.STRING, 3, new DbsArrayController(1, 7));
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Delim = localVariables.newFieldInRecord("pnd_Delim", "#DELIM", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_prap.reset();

        localVariables.reset();
        pnd_Input_Date_Range.setInitialValue(" ");
        pnd_Ny_Code.setInitialValue("35");
        pnd_Delim.setInitialValue(";");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Appb319() throws Exception
    {
        super("Appb319");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Appb319|Main");
        OnErrorManager.pushEvent("APPB319", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: ON ERROR
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 132
                //*  OCCURRENCE 0 MUST CORRESPOND TO END DAY.
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 1 ) TITLE LEFT 'Page' *PAGE-NUMBER ( 1 ) ( EM = ZZZ9 ) 54T 'New York Regulation 60 Control Report - ACIS' 120T *DATX ( EM = MM/DD/YYYY ) / 54T '--------------------------------------------' / 50T 'For period:' #START-DATE ( EM = MM/DD/YYYY ) 'thru' #END-DATE ( EM = MM/DD/YYYY ) /
                pnd_Abbr_Table.getValue(0 + 1).setValue("SUN");                                                                                                           //Natural: ASSIGN #ABBR-TABLE ( 0 ) := 'SUN'
                //*                            SEE SUBROUTINE FIND-DATE-RANGE
                pnd_Abbr_Table.getValue(1 + 1).setValue("MON");                                                                                                           //Natural: ASSIGN #ABBR-TABLE ( 1 ) := 'MON'
                pnd_Abbr_Table.getValue(2 + 1).setValue("TUE");                                                                                                           //Natural: ASSIGN #ABBR-TABLE ( 2 ) := 'TUE'
                pnd_Abbr_Table.getValue(3 + 1).setValue("WED");                                                                                                           //Natural: ASSIGN #ABBR-TABLE ( 3 ) := 'WED'
                pnd_Abbr_Table.getValue(4 + 1).setValue("THU");                                                                                                           //Natural: ASSIGN #ABBR-TABLE ( 4 ) := 'THU'
                pnd_Abbr_Table.getValue(5 + 1).setValue("FRI");                                                                                                           //Natural: ASSIGN #ABBR-TABLE ( 5 ) := 'FRI'
                pnd_Abbr_Table.getValue(6 + 1).setValue("SAT");                                                                                                           //Natural: ASSIGN #ABBR-TABLE ( 6 ) := 'SAT'
                if (condition(Global.getSTACK().getDatacount().equals(1), INPUT_1))                                                                                       //Natural: IF *DATA EQ 1
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Date_Range);                                                                             //Natural: INPUT #INPUT-DATE-RANGE
                }                                                                                                                                                         //Natural: END-IF
                //*   CHECK INPUT PARM FOR VALID DATES.  IF FOUND, USES THIS DATE RANGE.
                //*     ELSE CALCULATE DATE RANGE.
                if (condition(DbsUtil.maskMatches(pnd_Input_Date_Range_Pnd_Input_Start,"YYYYMMDD") && DbsUtil.maskMatches(pnd_Input_Date_Range_Pnd_Input_End,"YYYYMMDD")  //Natural: IF #INPUT-START EQ MASK ( YYYYMMDD ) AND #INPUT-END EQ MASK ( YYYYMMDD ) AND #INPUT-START LT #INPUT-END
                    && pnd_Input_Date_Range_Pnd_Input_Start.less(pnd_Input_Date_Range_Pnd_Input_End)))
                {
                    pnd_Start_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Date_Range_Pnd_Input_Start);                                                   //Natural: MOVE EDITED #INPUT-START TO #START-DATE ( EM = YYYYMMDD )
                    pnd_End_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Date_Range_Pnd_Input_End);                                                       //Natural: MOVE EDITED #INPUT-END TO #END-DATE ( EM = YYYYMMDD )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM FIND-DATE-RANGE
                    sub_Find_Date_Range();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                vw_prap.startDatabaseRead                                                                                                                                 //Natural: READ PRAP BY AP-TIAA-CNTRCT
                (
                "RDPRAP",
                new Oc[] { new Oc("AP_TIAA_CNTRCT", "ASC") }
                );
                RDPRAP:
                while (condition(vw_prap.readNextRow("RDPRAP")))
                {
                    //*  IF #COUNT       GT 100                /* DEBUG ONLY
                    //*    ESCAPE BOTTOM
                    //*  END-IF
                    //*   REJECT IF 1) RECORD IS NOT AN APPLICATION OR 2) IS AN RL CONTRACT
                    //*  RL
                    if (condition((prap_Ap_Record_Type.notEquals(1) || (prap_Ap_Lob.equals("D") && prap_Ap_Lob_Type.equals("6")))))                                       //Natural: REJECT IF AP-RECORD-TYPE NE 1 OR ( AP-LOB EQ 'D' AND AP-LOB-TYPE EQ '6' )
                    {
                        continue;
                    }
                    //*   EXCLUDE RECORDS OUT OF DATE RANGE
                    pnd_Date_Entered_Sys.setValue(prap_Ap_Dt_Ent_Sys);                                                                                                    //Natural: ASSIGN #DATE-ENTERED-SYS := AP-DT-ENT-SYS
                    pnd_Prap_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Date_Entered_Sys_Pnd_Date_Entered_Sys_A);                                             //Natural: MOVE EDITED #DATE-ENTERED-SYS-A TO #PRAP-DATE ( EM = MMDDYYYY )
                    if (condition(pnd_Prap_Date.less(pnd_Start_Date) || pnd_Prap_Date.greater(pnd_End_Date)))                                                             //Natural: IF #PRAP-DATE LT #START-DATE OR #PRAP-DATE GT #END-DATE
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  DEFAULT; OVERRIDE IF INDIVIDUAL OR GSRA
                    }                                                                                                                                                     //Natural: END-IF
                    //*   DETERMINE IF CONTRACT TYPE IS GROUP OR INDIVIDUAL:
                    //*     FOR INDIVIDUAL PLANS, EXAMINE AP-CURRENT-STATE-CODE
                    //*     FOR GROUP, EXAMINE AP-COLL-ST-CD.
                    pnd_State.setValue(prap_Ap_Coll_St_Cd);                                                                                                               //Natural: ASSIGN #STATE := AP-COLL-ST-CD
                    short decideConditionsMet145 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( AP-LOB EQ 'D' AND AP-LOB-TYPE EQ 'A' )
                    if (condition(prap_Ap_Lob.equals("D") && prap_Ap_Lob_Type.equals("A")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("ICAP RC");                                                                                                                     //Natural: ASSIGN #TYPE := 'ICAP RC'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'S' AND AP-LOB-TYPE EQ '7' )
                    else if (condition(prap_Ap_Lob.equals("S") && prap_Ap_Lob_Type.equals("7")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("ICAP RCP");                                                                                                                    //Natural: ASSIGN #TYPE := 'ICAP RCP'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'D' AND AP-LOB-TYPE EQ '5' )
                    else if (condition(prap_Ap_Lob.equals("D") && prap_Ap_Lob_Type.equals("5")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("TNT RS");                                                                                                                      //Natural: ASSIGN #TYPE := 'TNT RS'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'S' AND AP-LOB-TYPE EQ '5' )
                    else if (condition(prap_Ap_Lob.equals("S") && prap_Ap_Lob_Type.equals("5")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("TNT RSP");                                                                                                                     //Natural: ASSIGN #TYPE := 'TNT RSP'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'S' AND AP-LOB-TYPE EQ '6' )
                    else if (condition(prap_Ap_Lob.equals("S") && prap_Ap_Lob_Type.equals("6")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("TNT RSP2");                                                                                                                    //Natural: ASSIGN #TYPE := 'TNT RSP2'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'S' AND AP-LOB-TYPE EQ '4' )
                    else if (condition(prap_Ap_Lob.equals("S") && prap_Ap_Lob_Type.equals("4")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("GA");                                                                                                                          //Natural: ASSIGN #TYPE := 'GA'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'D' AND AP-LOB-TYPE EQ '7' )
                    else if (condition(prap_Ap_Lob.equals("D") && prap_Ap_Lob_Type.equals("7")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("GRA");                                                                                                                         //Natural: ASSIGN #TYPE := 'GRA'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'D' AND AP-LOB-TYPE EQ '9' )
                    else if (condition(prap_Ap_Lob.equals("D") && prap_Ap_Lob_Type.equals("9")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("GRA");                                                                                                                         //Natural: ASSIGN #TYPE := 'GRA'
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'S' AND AP-LOB-TYPE EQ '3' )
                    else if (condition(prap_Ap_Lob.equals("S") && prap_Ap_Lob_Type.equals("3")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("GSRA");                                                                                                                        //Natural: ASSIGN #TYPE := 'GSRA'
                        //*   FOR GSRA CONTRACTS FOR NEW YORK RESIDENTS, ACIS POPULATES
                        //*   AP-COLL-ST-CD WITH THE VALUE '35' (NEW YORK) INSTEAD OF THE ACTUAL
                        //*   STATE CODE FOR THE INSTITUTION (SEE ACIN3500 AND APPP3100.)
                        //*   CALLNAT 'ACIN3590' FOR THESE CONTRACTS TO RETRIEVE INSTITUTION's
                        //*   STATE CODE.
                        //*      IF AP-CURRENT-STATE-CODE EQ #NY-CODE
                        //*        ACIA3590.#COLLEGE-CODE :=  AP-COLL-CODE
                        //*        CALLNAT 'ACIN3590' ACIA3590
                        //*        #STATE :=  ACIA3590.#COLLEGE-STATE-CODE
                        //*      END-IF
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'D' AND AP-LOB-TYPE EQ '2' ) OR ( AP-LOB EQ 'D' AND AP-LOB-TYPE EQ '8' )
                    else if (condition(((prap_Ap_Lob.equals("D") && prap_Ap_Lob_Type.equals("2")) || (prap_Ap_Lob.equals("D") && prap_Ap_Lob_Type.equals("8")))))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("RA");                                                                                                                          //Natural: ASSIGN #TYPE := 'RA'
                        pnd_State.setValue(prap_Ap_Current_State_Code);                                                                                                   //Natural: ASSIGN #STATE := AP-CURRENT-STATE-CODE
                        //*  (JC1) BEGIN
                        if (condition(((DbsUtil.maskMatches(prap_Ap_Coll_Code,"NNNN") && prap_Ap_Coll_Code.equals("9004")) || ((prap_Ap_Coll_Code.greaterOrEqual("9006")  //Natural: IF AP-COLL-CODE = MASK ( NNNN ) AND AP-COLL-CODE = '9004' OR ( AP-COLL-CODE = '9006' THRU '9020' BUT NOT '9009' )
                            && prap_Ap_Coll_Code.lessOrEqual("9020")) && prap_Ap_Coll_Code.notEquals("9009")))))
                        {
                            pnd_Type.setValue("ATRA");                                                                                                                    //Natural: ASSIGN #TYPE := 'ATRA'
                            //*  (JC1) END
                        }                                                                                                                                                 //Natural: END-IF
                        //*  ATRA KG START
                        if (condition(prap_Ap_Sgrd_Subplan_No.getSubstring(1,2).equals("AA")))                                                                            //Natural: IF SUBSTRING ( AP-SGRD-SUBPLAN-NO,1,2 ) = 'AA'
                        {
                            pnd_Type.setValue("ATRA");                                                                                                                    //Natural: ASSIGN #TYPE := 'ATRA'
                            //*  ATRA KG END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'S' AND AP-LOB-TYPE EQ '2' )
                    else if (condition(prap_Ap_Lob.equals("S") && prap_Ap_Lob_Type.equals("2")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("SRA");                                                                                                                         //Natural: ASSIGN #TYPE := 'SRA'
                        pnd_State.setValue(prap_Ap_Current_State_Code);                                                                                                   //Natural: ASSIGN #STATE := AP-CURRENT-STATE-CODE
                    }                                                                                                                                                     //Natural: WHEN ( AP-LOB EQ 'S' AND AP-LOB-TYPE EQ '8' )
                    else if (condition(prap_Ap_Lob.equals("S") && prap_Ap_Lob_Type.equals("8")))
                    {
                        decideConditionsMet145++;
                        pnd_Type.setValue("SRA");                                                                                                                         //Natural: ASSIGN #TYPE := 'SRA'
                        pnd_State.setValue(prap_Ap_Current_State_Code);                                                                                                   //Natural: ASSIGN #STATE := AP-CURRENT-STATE-CODE
                    }                                                                                                                                                     //Natural: WHEN AP-LOB EQ 'I'
                    else if (condition(prap_Ap_Lob.equals("I")))
                    {
                        decideConditionsMet145++;
                        pnd_State.setValue(prap_Ap_Current_State_Code);                                                                                                   //Natural: ASSIGN #STATE := AP-CURRENT-STATE-CODE
                        if (condition(prap_Ap_Lob_Type.equals("5")))                                                                                                      //Natural: IF AP-LOB-TYPE EQ '5'
                        {
                            pnd_Type.setValue("KEOGH");                                                                                                                   //Natural: ASSIGN #TYPE := 'KEOGH'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Type.setValue("IRA");                                                                                                                     //Natural: ASSIGN #TYPE := 'IRA'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_Type.setValue("N/A");                                                                                                                         //Natural: ASSIGN #TYPE := 'N/A'
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(pnd_State.equals(pnd_Ny_Code)))                                                                                                         //Natural: IF #STATE EQ #NY-CODE
                    {
                        pnd_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #COUNT
                        pnd_Full_Name.setValue(DbsUtil.compress(prap_Ap_Cor_First_Nme, prap_Ap_Cor_Mddle_Nme, prap_Ap_Cor_Last_Nme));                                     //Natural: COMPRESS AP-COR-FIRST-NME AP-COR-MDDLE-NME AP-COR-LAST-NME INTO #FULL-NAME
                        //*  (JC2) BEGIN
                        if (condition(prap_Ap_Coll_Code.equals("SGRD")))                                                                                                  //Natural: IF AP-COLL-CODE = 'SGRD'
                        {
                            pnd_Omni_Legacy_Ind.setValue("OMNI");                                                                                                         //Natural: ASSIGN #OMNI-LEGACY-IND := 'OMNI'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Omni_Legacy_Ind.setValue("Legacy");                                                                                                       //Natural: ASSIGN #OMNI-LEGACY-IND := 'Legacy'
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Type.equals("IRA") || pnd_Omni_Legacy_Ind.equals("OMNI")))                                                                      //Natural: IF #TYPE EQ 'IRA' OR #OMNI-LEGACY-IND EQ 'OMNI'
                        {
                            pnd_Change_By_Nme.reset();                                                                                                                    //Natural: RESET #CHANGE-BY-NME AP-MIT-UNIT ( 1 )
                            prap_Ap_Mit_Unit.getValue(1).reset();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            DbsUtil.callnat(Cwfn1107.class , getCurrentProcessState(), prap_Pnd_Racf_Id, pnd_Change_By_Nme);                                              //Natural: CALLNAT 'CWFN1107' #RACF-ID #CHANGE-BY-NME
                            if (condition(Global.isEscape())) return;
                            DbsUtil.examine(new ExamineSource(pnd_Change_By_Nme), new ExamineSearch("*"), new ExamineDelete());                                           //Natural: EXAMINE #CHANGE-BY-NME FOR '*' DELETE
                            pnd_Change_By_Nme.setValue(pnd_Change_By_Nme, MoveOption.LeftJustified);                                                                      //Natural: MOVE LEFT JUSTIFIED #CHANGE-BY-NME TO #CHANGE-BY-NME
                            //*  (JC2) END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (JC1)
                    getSort().writeSortInData(pnd_Type, pnd_Omni_Legacy_Ind, prap_Ap_Tiaa_Cntrct, pnd_Full_Name, prap_Ap_Dt_Ent_Sys, prap_Ap_Pin_Nbr,                     //Natural: END-ALL
                        prap_Ap_T_Doi, pnd_Change_By_Nme, prap_Ap_Mit_Unit.getValue(1));
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  SORT BY #TYPE AP-TIAA-CNTRCT                        /* (JC1)
                //*  (JC2)
                //*  (JC2)
                getSort().sortData(pnd_Type, pnd_Omni_Legacy_Ind, prap_Ap_Tiaa_Cntrct);                                                                                   //Natural: SORT BY #TYPE #OMNI-LEGACY-IND AP-TIAA-CNTRCT USING #FULL-NAME AP-DT-ENT-SYS AP-PIN-NBR AP-T-DOI #CHANGE-BY-NME AP-MIT-UNIT ( 1 )
                SORT01:
                while (condition(getSort().readSortOutData(pnd_Type, pnd_Omni_Legacy_Ind, prap_Ap_Tiaa_Cntrct, pnd_Full_Name, prap_Ap_Dt_Ent_Sys, prap_Ap_Pin_Nbr, 
                    prap_Ap_T_Doi, pnd_Change_By_Nme, prap_Ap_Mit_Unit.getValue(1))))
                {
                    pnd_Ap_T_Doi.setValue(prap_Ap_T_Doi);                                                                                                                 //Natural: MOVE AP-T-DOI TO #AP-T-DOI
                    pnd_Ap_Dt_Ent_Sys.setValue(prap_Ap_Dt_Ent_Sys);                                                                                                       //Natural: MOVE AP-DT-ENT-SYS TO #AP-DT-ENT-SYS
                    //*  WRITE WORK 1                     /* DEBUG ONLY
                    //*    #TYPE #DELIM
                    //*    #FULL-NAME #DELIM
                    //*    AP-TIAA-CNTRCT #DELIM
                    //*    #AP-DT-ENT-SYS #DELIM
                    //*    AP-PIN-NBR #DELIM
                    //*    #AP-T-DOI #DELIM
                    //*      #STATE
                    //*      'curr' AP-CURRENT-STATE-CODE
                    //*      'coll' AP-COLL-ST-CD
                    //*      #PRAP-DATE
                    getReports().display(1, ReportOption.NOTITLE,"type",                                                                                                  //Natural: DISPLAY ( 1 ) NOTITLE 'type' #TYPE 'Participant name' #FULL-NAME 'TIAA/Contract' AP-TIAA-CNTRCT 'Received/date' AP-DT-ENT-SYS ( EM = 99/99/9999 ZP = ON ) 'PIN' AP-PIN-NBR 'Issue/date' AP-T-DOI ( EM = 99/99 ZP = ON ) 'Change By Name' #CHANGE-BY-NME 'MIT Unit' AP-MIT-UNIT ( 1 ) 'OMNI or/Legacy' #OMNI-LEGACY-IND
                    		pnd_Type,"Participant name",
                    		pnd_Full_Name,"TIAA/Contract",
                    		prap_Ap_Tiaa_Cntrct,"Received/date",
                    		prap_Ap_Dt_Ent_Sys, new ReportEditMask ("99/99/9999"), new ReportZeroPrint (true),"PIN",
                    		prap_Ap_Pin_Nbr,"Issue/date",
                    		prap_Ap_T_Doi, new ReportEditMask ("99/99"), new ReportZeroPrint (true),"Change By Name",
                    		pnd_Change_By_Nme,"MIT Unit",
                    		prap_Ap_Mit_Unit.getValue(1),"OMNI or/Legacy",
                    		pnd_Omni_Legacy_Ind);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-SORT
                endSort();
                                                                                                                                                                          //Natural: PERFORM WRITE-TOTALS
                sub_Write_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ----------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-DATE-RANGE
                //*   MATCH TO #ABBR-TABLE TABLE.  WHEN FOUND, SUBTRACT INDEX OF TABLE
                //*     FROM #END-DATE. SUBTRACT SIX FROM #END-DATE TO GET #START-DATE.
                //*  ----------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TOTALS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Find_Date_Range() throws Exception                                                                                                                   //Natural: FIND-DATE-RANGE
    {
        if (BLNatReinput.isReinput()) return;

        //*   PURPOSE: TO FIND THE IDENTICAL RANGE OF DAYS IN A SEVEN-DAY PERIOD,
        //*     GIVEN THE ENDING DAY OF WEEK.  THIS IS TO HANDLE THE SITUATION IN
        //*     WHICH A JOB IS SCHEDULED TO RUN A PARTICULAR DAY, BUT ACTUALLY
        //*     RUNS ONE OR MORE (UP TO 5) DAYS LATE.
        //*   THIS PROGRAM EXTRACTS ONE WEEK OF DATA, BEGINNING WITH THE PREVIOUS
        //*     MONDAY THROUGH SUNDAY.  SINCE SUNDAY CORRESPONDS TO DESIRED
        //*     END DAY OF WEEK, SUN MUST BE OCCURRENCE 0 IN #ABBR-TABLE.
        //*   EXTRACT FIRST THREE LETTERS OF NAME OF DAY (RESULT IS IN MIXED CASE.)
        pnd_Today_Abbr.setValueEdited(Global.getDATX(),new ReportEditMask("NNN"));                                                                                        //Natural: MOVE EDITED *DATX ( EM = N ( 3 ) ) TO #TODAY-ABBR
        //*   DEFAULT #END-DATE AS SYSTEM DATE.
        DbsUtil.examine(new ExamineSource(pnd_Today_Abbr), new ExamineTranslate(TranslateOption.Upper));                                                                  //Natural: EXAMINE #TODAY-ABBR TRANSLATE INTO UPPER CASE
        pnd_End_Date.setValue(Global.getDATX());                                                                                                                          //Natural: ASSIGN #END-DATE := *DATX
        FOR01:                                                                                                                                                            //Natural: FOR #I EQ 0 TO 6
        for (pnd_I.setValue(0); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
        {
            if (condition(pnd_Today_Abbr.equals(pnd_Abbr_Table.getValue(pnd_I.getInt() + 1))))                                                                            //Natural: IF #TODAY-ABBR EQ #ABBR-TABLE ( #I )
            {
                pnd_End_Date.nsubtract(pnd_I);                                                                                                                            //Natural: SUBTRACT #I FROM #END-DATE
                pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), pnd_End_Date.subtract(6));                                                           //Natural: SUBTRACT 6 FROM #END-DATE GIVING #START-DATE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FIND-DATE-RANGE
    }
    private void sub_Write_Totals() throws Exception                                                                                                                      //Natural: WRITE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, "NY contracts:     ",pnd_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Records read:",vw_prap.getAstCOUNTER(), new                     //Natural: WRITE ( 1 ) 'NY contracts:     ' #COUNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Records read:' *COUNTER ( RDPRAP. ) ( EM = ZZZ,ZZZ,ZZ9 ) /
            ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "ELIN: ",Global.getERROR_LINE(),NEWLINE,"ERR: ",Global.getERROR_NR(),"=",pnd_Count, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"=",pnd_Start_Date,  //Natural: WRITE '=' *ERROR-LINE / '=' *ERROR-NR '=' #COUNT ( EM = ZZZ,ZZZ,ZZ9 ) / '=' #START-DATE ( EM = MM/DD/YYYY ) / 3T '=' #END-DATE ( EM = MM/DD/YYYY )
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(3),"=",pnd_End_Date, new ReportEditMask ("MM/DD/YYYY"));
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"Page",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZZ9"),new 
            TabSetting(54),"New York Regulation 60 Control Report - ACIS",new TabSetting(120),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new 
            TabSetting(54),"--------------------------------------------",NEWLINE,new TabSetting(50),"For period:",pnd_Start_Date, new ReportEditMask ("MM/DD/YYYY"),"thru",pnd_End_Date, 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE);

        getReports().setDisplayColumns(1, ReportOption.NOTITLE,"type",
        		pnd_Type,"Participant name",
        		pnd_Full_Name,"TIAA/Contract",
        		prap_Ap_Tiaa_Cntrct,"Received/date",
        		prap_Ap_Dt_Ent_Sys, new ReportEditMask ("99/99/9999"), new ReportZeroPrint (true),"PIN",
        		prap_Ap_Pin_Nbr,"Issue/date",
        		prap_Ap_T_Doi, new ReportEditMask ("99/99"), new ReportZeroPrint (true),"Change By Name",
        		pnd_Change_By_Nme,"MIT Unit",
        		prap_Ap_Mit_Unit,"OMNI or/Legacy",
        		pnd_Omni_Legacy_Ind);
    }
}
