/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:41 PM
**        * FROM NATURAL PROGRAM : Appb2070
************************************************************
**        * FILE NAME            : Appb2070.java
**        * CLASS NAME           : Appb2070
**        * INSTANCE NAME        : Appb2070
************************************************************
************************************************************************
* APPB2070 - WEEKLY CONTRACT RANGE EXTRACT                             *
* READS ILOG FILE CURRENT & NEXT JUMPER RECORD FOR ALL CONTRACT TYPES  *
* WRITES WORK FILES OF IA AND DA CONTRACT RANGES                       *
************************************************************************
*
************************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                     *
* ----------  ----------------  -------------------------------------- *
* 10/17/2005  JANET BERGHEISER  NEW                                    *
* 01/11/2007  JANET BERGHEISER  CONTRACT RANGE EXPANSION PROJECT (JB1) *
*                               CHANGE RANGE CALCULATIONS FOR ALPHAS   *
*                               PASS START DATE IN WORK FILE           *
* 06/08/2007  JANET BERGHEISER  CORRECTION FOR CALCULATIONS      (JB2) *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2070 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_da_Ilog_Curr_Jumper;
    private DbsField da_Ilog_Curr_Jumper_Start_Dte;
    private DbsField da_Ilog_Curr_Jumper_Rcrd_Cde;
    private DbsField da_Ilog_Curr_Jumper_Rcrd_Nme;
    private DbsField da_Ilog_Curr_Jumper_Prdct_Cde;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Cref_Pref;
    private DbsField da_Ilog_Curr_Jumper_Rcrd_Revision_Nbr;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Range_Start;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Range_End;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Last_Used;
    private DbsField da_Ilog_Curr_Jumper_Previous_Rcrd_Dte;
    private DbsField da_Ilog_Curr_Jumper_Current_Rcrd_Dte;

    private DataAccessProgramView vw_da_Ilog_Next_Jumper;
    private DbsField da_Ilog_Next_Jumper_Start_Dte;
    private DbsField da_Ilog_Next_Jumper_Rcrd_Cde;
    private DbsField da_Ilog_Next_Jumper_Rcrd_Nme;
    private DbsField da_Ilog_Next_Jumper_Prdct_Cde;

    private DataAccessProgramView vw_ia_Ilog_Curr_Jumper;
    private DbsField ia_Ilog_Curr_Jumper_Start_Dte;
    private DbsField ia_Ilog_Curr_Jumper_Rcrd_Cde;
    private DbsField ia_Ilog_Curr_Jumper_Rcrd_Nme;
    private DbsField ia_Ilog_Curr_Jumper_Prdct_Cde;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Cref_Pref;
    private DbsField ia_Ilog_Curr_Jumper_Rcrd_Revision_Nbr;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Range_Start;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Range_End;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Last_Used;
    private DbsField ia_Ilog_Curr_Jumper_Previous_Rcrd_Dte;
    private DbsField ia_Ilog_Curr_Jumper_Current_Rcrd_Dte;

    private DataAccessProgramView vw_ia_Ilog_Next_Jumper;
    private DbsField ia_Ilog_Next_Jumper_Start_Dte;
    private DbsField ia_Ilog_Next_Jumper_Rcrd_Cde;
    private DbsField ia_Ilog_Next_Jumper_Rcrd_Nme;
    private DbsField ia_Ilog_Next_Jumper_Prdct_Cde;
    private DbsField pnd_Da_Jump_Key;

    private DbsGroup pnd_Da_Jump_Key__R_Field_1;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Start_Dte;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde;
    private DbsField pnd_Ia_Jump_Key;

    private DbsGroup pnd_Ia_Jump_Key__R_Field_2;
    private DbsField pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme;
    private DbsField pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde;
    private DbsField pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde;
    private DbsField pnd_Header_Record;

    private DbsGroup pnd_Header_Record__R_Field_3;
    private DbsField pnd_Header_Record_Pnd_H_Record_Type;
    private DbsField pnd_Header_Record_Pnd_H_Filler_1;
    private DbsField pnd_Header_Record_Pnd_H_Contract_Type;
    private DbsField pnd_Header_Record_Pnd_H_Filler_2;
    private DbsField pnd_Header_Record_Pnd_H_Date;
    private DbsField pnd_Header_Record_Pnd_H_Filler_3;
    private DbsField pnd_Detail_Record;

    private DbsGroup pnd_Detail_Record__R_Field_4;
    private DbsField pnd_Detail_Record_Pnd_D_Record_Type;
    private DbsField pnd_Detail_Record_Pnd_D_Filler_1;
    private DbsField pnd_Detail_Record_Pnd_D_Product_Code;
    private DbsField pnd_Detail_Record_Pnd_D_Start_Contract;
    private DbsField pnd_Detail_Record_Pnd_D_End_Contract;
    private DbsField pnd_Detail_Record_Pnd_D_Next_Contract;
    private DbsField pnd_Detail_Record_Pnd_D_Range_Size;
    private DbsField pnd_Detail_Record_Pnd_D_Filler_2;
    private DbsField pnd_Detail_Record_Pnd_D_Range_Left;
    private DbsField pnd_Detail_Record_Pnd_D_Filler_3;
    private DbsField pnd_Detail_Record_Pnd_D_Next_Range;
    private DbsField pnd_Detail_Record_Pnd_D_Filler_4;
    private DbsField pnd_Detail_Record_Pnd_D_Start_Date;
    private DbsField pnd_Detail_Record_Pnd_D_Filler_5;
    private DbsField pnd_Next_Range_Found;
    private DbsField pnd_Cntrct_Range_Beg;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_5;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Prefix_1_2;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_7;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_6;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_6;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_7;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_7;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_5;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_6;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_8;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_4;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_5;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_9;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Prefix_1;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_7;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_10;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_6;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_11;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_5;

    private DbsGroup pnd_Cntrct_Range_Beg__R_Field_12;
    private DbsField pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_4;
    private DbsField pnd_Cntrct_Range_End;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_13;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Prefix_1_2;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_14;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_3_6;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Alpha_7;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_15;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_3_5;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Alpha_6;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_16;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_3_4;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Alpha_5;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_17;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Prefix_1;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_18;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_2_6;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_19;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_2_5;

    private DbsGroup pnd_Cntrct_Range_End__R_Field_20;
    private DbsField pnd_Cntrct_Range_End_Pnd_End_Nbr_2_4;
    private DbsField pnd_Cntrct_Last_Used;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_21;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Prefix_1_2;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_7;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_22;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_6;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Alpha_7;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_23;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_5;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Alpha_6;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_24;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_4;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Alpha_5;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_25;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Prefix_1;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_7;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_26;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_6;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_27;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_5;

    private DbsGroup pnd_Cntrct_Last_Used__R_Field_28;
    private DbsField pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_4;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_da_Ilog_Curr_Jumper = new DataAccessProgramView(new NameInfo("vw_da_Ilog_Curr_Jumper", "DA-ILOG-CURR-JUMPER"), "APP_ILOG_J_RCRD", "ACIS_APP_ILOG");
        da_Ilog_Curr_Jumper_Start_Dte = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        da_Ilog_Curr_Jumper_Rcrd_Cde = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        da_Ilog_Curr_Jumper_Rcrd_Nme = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        da_Ilog_Curr_Jumper_Prdct_Cde = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PRDCT_CDE");
        da_Ilog_Curr_Jumper_Cntrct_Cref_Pref = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Cref_Pref", "CNTRCT-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CREF_PREF");
        da_Ilog_Curr_Jumper_Rcrd_Revision_Nbr = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Rcrd_Revision_Nbr", "RCRD-REVISION-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RCRD_REVISION_NBR");
        da_Ilog_Curr_Jumper_Cntrct_Range_Start = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Range_Start", "CNTRCT-RANGE-START", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_RANGE_START");
        da_Ilog_Curr_Jumper_Cntrct_Range_End = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Range_End", "CNTRCT-RANGE-END", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_RANGE_END");
        da_Ilog_Curr_Jumper_Cntrct_Last_Used = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Last_Used", "CNTRCT-LAST-USED", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_LAST_USED");
        da_Ilog_Curr_Jumper_Previous_Rcrd_Dte = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Previous_Rcrd_Dte", "PREVIOUS-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PREVIOUS_RCRD_DTE");
        da_Ilog_Curr_Jumper_Current_Rcrd_Dte = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Current_Rcrd_Dte", "CURRENT-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CURRENT_RCRD_DTE");
        registerRecord(vw_da_Ilog_Curr_Jumper);

        vw_da_Ilog_Next_Jumper = new DataAccessProgramView(new NameInfo("vw_da_Ilog_Next_Jumper", "DA-ILOG-NEXT-JUMPER"), "APP_ILOG_J_RCRD", "ACIS_APP_ILOG");
        da_Ilog_Next_Jumper_Start_Dte = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        da_Ilog_Next_Jumper_Rcrd_Cde = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        da_Ilog_Next_Jumper_Rcrd_Nme = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        da_Ilog_Next_Jumper_Prdct_Cde = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PRDCT_CDE");
        registerRecord(vw_da_Ilog_Next_Jumper);

        vw_ia_Ilog_Curr_Jumper = new DataAccessProgramView(new NameInfo("vw_ia_Ilog_Curr_Jumper", "IA-ILOG-CURR-JUMPER"), "CIS_CNTRCT_J_RCRD", "CIS_CNTRCT_FILE");
        ia_Ilog_Curr_Jumper_Start_Dte = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        ia_Ilog_Curr_Jumper_Rcrd_Cde = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        ia_Ilog_Curr_Jumper_Rcrd_Nme = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        ia_Ilog_Curr_Jumper_Prdct_Cde = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PRDCT_CDE");
        ia_Ilog_Curr_Jumper_Cntrct_Cref_Pref = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Cref_Pref", "CNTRCT-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CREF_PREF");
        ia_Ilog_Curr_Jumper_Rcrd_Revision_Nbr = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Rcrd_Revision_Nbr", "RCRD-REVISION-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RCRD_REVISION_NBR");
        ia_Ilog_Curr_Jumper_Cntrct_Range_Start = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Range_Start", "CNTRCT-RANGE-START", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_RANGE_START");
        ia_Ilog_Curr_Jumper_Cntrct_Range_End = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Range_End", "CNTRCT-RANGE-END", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_RANGE_END");
        ia_Ilog_Curr_Jumper_Cntrct_Last_Used = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Last_Used", "CNTRCT-LAST-USED", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_LAST_USED");
        ia_Ilog_Curr_Jumper_Previous_Rcrd_Dte = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Previous_Rcrd_Dte", "PREVIOUS-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PREVIOUS_RCRD_DTE");
        ia_Ilog_Curr_Jumper_Current_Rcrd_Dte = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Current_Rcrd_Dte", "CURRENT-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CURRENT_RCRD_DTE");
        registerRecord(vw_ia_Ilog_Curr_Jumper);

        vw_ia_Ilog_Next_Jumper = new DataAccessProgramView(new NameInfo("vw_ia_Ilog_Next_Jumper", "IA-ILOG-NEXT-JUMPER"), "CIS_CNTRCT_J_RCRD", "CIS_CNTRCT_FILE");
        ia_Ilog_Next_Jumper_Start_Dte = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        ia_Ilog_Next_Jumper_Rcrd_Cde = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        ia_Ilog_Next_Jumper_Rcrd_Nme = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        ia_Ilog_Next_Jumper_Prdct_Cde = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PRDCT_CDE");
        registerRecord(vw_ia_Ilog_Next_Jumper);

        pnd_Da_Jump_Key = localVariables.newFieldInRecord("pnd_Da_Jump_Key", "#DA-JUMP-KEY", FieldType.STRING, 26);

        pnd_Da_Jump_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Da_Jump_Key__R_Field_1", "REDEFINE", pnd_Da_Jump_Key);
        pnd_Da_Jump_Key_Pnd_Da_Start_Dte = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Start_Dte", "#DA-START-DTE", FieldType.STRING, 
            8);
        pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde", "#DA-RCRD-CDE", FieldType.STRING, 
            2);
        pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme", "#DA-RCRD-NME", FieldType.STRING, 
            12);
        pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde", "#DA-PRDCT-CDE", FieldType.STRING, 
            4);
        pnd_Ia_Jump_Key = localVariables.newFieldInRecord("pnd_Ia_Jump_Key", "#IA-JUMP-KEY", FieldType.STRING, 24);

        pnd_Ia_Jump_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ia_Jump_Key__R_Field_2", "REDEFINE", pnd_Ia_Jump_Key);
        pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme = pnd_Ia_Jump_Key__R_Field_2.newFieldInGroup("pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme", "#IA-RCRD-NME", FieldType.STRING, 
            12);
        pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde = pnd_Ia_Jump_Key__R_Field_2.newFieldInGroup("pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde", "#IA-RCRD-CDE", FieldType.STRING, 
            2);
        pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde = pnd_Ia_Jump_Key__R_Field_2.newFieldInGroup("pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde", "#IA-PRDCT-CDE", FieldType.STRING, 
            10);
        pnd_Header_Record = localVariables.newFieldInRecord("pnd_Header_Record", "#HEADER-RECORD", FieldType.STRING, 80);

        pnd_Header_Record__R_Field_3 = localVariables.newGroupInRecord("pnd_Header_Record__R_Field_3", "REDEFINE", pnd_Header_Record);
        pnd_Header_Record_Pnd_H_Record_Type = pnd_Header_Record__R_Field_3.newFieldInGroup("pnd_Header_Record_Pnd_H_Record_Type", "#H-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Header_Record_Pnd_H_Filler_1 = pnd_Header_Record__R_Field_3.newFieldInGroup("pnd_Header_Record_Pnd_H_Filler_1", "#H-FILLER-1", FieldType.STRING, 
            1);
        pnd_Header_Record_Pnd_H_Contract_Type = pnd_Header_Record__R_Field_3.newFieldInGroup("pnd_Header_Record_Pnd_H_Contract_Type", "#H-CONTRACT-TYPE", 
            FieldType.STRING, 2);
        pnd_Header_Record_Pnd_H_Filler_2 = pnd_Header_Record__R_Field_3.newFieldInGroup("pnd_Header_Record_Pnd_H_Filler_2", "#H-FILLER-2", FieldType.STRING, 
            1);
        pnd_Header_Record_Pnd_H_Date = pnd_Header_Record__R_Field_3.newFieldInGroup("pnd_Header_Record_Pnd_H_Date", "#H-DATE", FieldType.STRING, 10);
        pnd_Header_Record_Pnd_H_Filler_3 = pnd_Header_Record__R_Field_3.newFieldInGroup("pnd_Header_Record_Pnd_H_Filler_3", "#H-FILLER-3", FieldType.STRING, 
            65);
        pnd_Detail_Record = localVariables.newFieldInRecord("pnd_Detail_Record", "#DETAIL-RECORD", FieldType.STRING, 80);

        pnd_Detail_Record__R_Field_4 = localVariables.newGroupInRecord("pnd_Detail_Record__R_Field_4", "REDEFINE", pnd_Detail_Record);
        pnd_Detail_Record_Pnd_D_Record_Type = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Record_Type", "#D-RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_D_Filler_1 = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Filler_1", "#D-FILLER-1", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_D_Product_Code = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Product_Code", "#D-PRODUCT-CODE", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_D_Start_Contract = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Start_Contract", "#D-START-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_D_End_Contract = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_End_Contract", "#D-END-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_D_Next_Contract = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Next_Contract", "#D-NEXT-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Detail_Record_Pnd_D_Range_Size = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Range_Size", "#D-RANGE-SIZE", FieldType.NUMERIC, 
            6);
        pnd_Detail_Record_Pnd_D_Filler_2 = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Filler_2", "#D-FILLER-2", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_D_Range_Left = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Range_Left", "#D-RANGE-LEFT", FieldType.NUMERIC, 
            6);
        pnd_Detail_Record_Pnd_D_Filler_3 = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Filler_3", "#D-FILLER-3", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_D_Next_Range = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Next_Range", "#D-NEXT-RANGE", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_D_Filler_4 = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Filler_4", "#D-FILLER-4", FieldType.STRING, 
            1);
        pnd_Detail_Record_Pnd_D_Start_Date = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Start_Date", "#D-START-DATE", FieldType.STRING, 
            8);
        pnd_Detail_Record_Pnd_D_Filler_5 = pnd_Detail_Record__R_Field_4.newFieldInGroup("pnd_Detail_Record_Pnd_D_Filler_5", "#D-FILLER-5", FieldType.STRING, 
            14);
        pnd_Next_Range_Found = localVariables.newFieldInRecord("pnd_Next_Range_Found", "#NEXT-RANGE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Cntrct_Range_Beg = localVariables.newFieldInRecord("pnd_Cntrct_Range_Beg", "#CNTRCT-RANGE-BEG", FieldType.STRING, 7);

        pnd_Cntrct_Range_Beg__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Range_Beg__R_Field_5", "REDEFINE", pnd_Cntrct_Range_Beg);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Prefix_1_2 = pnd_Cntrct_Range_Beg__R_Field_5.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Prefix_1_2", "#BEG-PREFIX-1-2", 
            FieldType.STRING, 2);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_7 = pnd_Cntrct_Range_Beg__R_Field_5.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_7", "#BEG-NBR-3-7", 
            FieldType.NUMERIC, 5);

        pnd_Cntrct_Range_Beg__R_Field_6 = pnd_Cntrct_Range_Beg__R_Field_5.newGroupInGroup("pnd_Cntrct_Range_Beg__R_Field_6", "REDEFINE", pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_7);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_6 = pnd_Cntrct_Range_Beg__R_Field_6.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_6", "#BEG-NBR-3-6", 
            FieldType.NUMERIC, 4);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_7 = pnd_Cntrct_Range_Beg__R_Field_6.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_7", "#BEG-ALPHA-7", 
            FieldType.STRING, 1);

        pnd_Cntrct_Range_Beg__R_Field_7 = pnd_Cntrct_Range_Beg__R_Field_5.newGroupInGroup("pnd_Cntrct_Range_Beg__R_Field_7", "REDEFINE", pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_7);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_5 = pnd_Cntrct_Range_Beg__R_Field_7.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_5", "#BEG-NBR-3-5", 
            FieldType.NUMERIC, 3);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_6 = pnd_Cntrct_Range_Beg__R_Field_7.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_6", "#BEG-ALPHA-6", 
            FieldType.STRING, 1);

        pnd_Cntrct_Range_Beg__R_Field_8 = pnd_Cntrct_Range_Beg__R_Field_5.newGroupInGroup("pnd_Cntrct_Range_Beg__R_Field_8", "REDEFINE", pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_7);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_4 = pnd_Cntrct_Range_Beg__R_Field_8.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_4", "#BEG-NBR-3-4", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_5 = pnd_Cntrct_Range_Beg__R_Field_8.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_5", "#BEG-ALPHA-5", 
            FieldType.STRING, 1);

        pnd_Cntrct_Range_Beg__R_Field_9 = localVariables.newGroupInRecord("pnd_Cntrct_Range_Beg__R_Field_9", "REDEFINE", pnd_Cntrct_Range_Beg);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Prefix_1 = pnd_Cntrct_Range_Beg__R_Field_9.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Prefix_1", "#BEG-PREFIX-1", 
            FieldType.STRING, 1);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_7 = pnd_Cntrct_Range_Beg__R_Field_9.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_7", "#BEG-NBR-2-7", 
            FieldType.NUMERIC, 6);

        pnd_Cntrct_Range_Beg__R_Field_10 = pnd_Cntrct_Range_Beg__R_Field_9.newGroupInGroup("pnd_Cntrct_Range_Beg__R_Field_10", "REDEFINE", pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_7);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_6 = pnd_Cntrct_Range_Beg__R_Field_10.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_6", "#BEG-NBR-2-6", 
            FieldType.NUMERIC, 5);

        pnd_Cntrct_Range_Beg__R_Field_11 = pnd_Cntrct_Range_Beg__R_Field_9.newGroupInGroup("pnd_Cntrct_Range_Beg__R_Field_11", "REDEFINE", pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_7);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_5 = pnd_Cntrct_Range_Beg__R_Field_11.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_5", "#BEG-NBR-2-5", 
            FieldType.NUMERIC, 4);

        pnd_Cntrct_Range_Beg__R_Field_12 = pnd_Cntrct_Range_Beg__R_Field_9.newGroupInGroup("pnd_Cntrct_Range_Beg__R_Field_12", "REDEFINE", pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_7);
        pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_4 = pnd_Cntrct_Range_Beg__R_Field_12.newFieldInGroup("pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_4", "#BEG-NBR-2-4", 
            FieldType.NUMERIC, 3);
        pnd_Cntrct_Range_End = localVariables.newFieldInRecord("pnd_Cntrct_Range_End", "#CNTRCT-RANGE-END", FieldType.STRING, 7);

        pnd_Cntrct_Range_End__R_Field_13 = localVariables.newGroupInRecord("pnd_Cntrct_Range_End__R_Field_13", "REDEFINE", pnd_Cntrct_Range_End);
        pnd_Cntrct_Range_End_Pnd_End_Prefix_1_2 = pnd_Cntrct_Range_End__R_Field_13.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Prefix_1_2", "#END-PREFIX-1-2", 
            FieldType.STRING, 2);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7 = pnd_Cntrct_Range_End__R_Field_13.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7", "#END-NBR-3-7", 
            FieldType.NUMERIC, 5);

        pnd_Cntrct_Range_End__R_Field_14 = pnd_Cntrct_Range_End__R_Field_13.newGroupInGroup("pnd_Cntrct_Range_End__R_Field_14", "REDEFINE", pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_3_6 = pnd_Cntrct_Range_End__R_Field_14.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_3_6", "#END-NBR-3-6", 
            FieldType.NUMERIC, 4);
        pnd_Cntrct_Range_End_Pnd_End_Alpha_7 = pnd_Cntrct_Range_End__R_Field_14.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Alpha_7", "#END-ALPHA-7", 
            FieldType.STRING, 1);

        pnd_Cntrct_Range_End__R_Field_15 = pnd_Cntrct_Range_End__R_Field_13.newGroupInGroup("pnd_Cntrct_Range_End__R_Field_15", "REDEFINE", pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_3_5 = pnd_Cntrct_Range_End__R_Field_15.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_3_5", "#END-NBR-3-5", 
            FieldType.NUMERIC, 3);
        pnd_Cntrct_Range_End_Pnd_End_Alpha_6 = pnd_Cntrct_Range_End__R_Field_15.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Alpha_6", "#END-ALPHA-6", 
            FieldType.STRING, 1);

        pnd_Cntrct_Range_End__R_Field_16 = pnd_Cntrct_Range_End__R_Field_13.newGroupInGroup("pnd_Cntrct_Range_End__R_Field_16", "REDEFINE", pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_3_4 = pnd_Cntrct_Range_End__R_Field_16.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_3_4", "#END-NBR-3-4", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Range_End_Pnd_End_Alpha_5 = pnd_Cntrct_Range_End__R_Field_16.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Alpha_5", "#END-ALPHA-5", 
            FieldType.STRING, 1);

        pnd_Cntrct_Range_End__R_Field_17 = localVariables.newGroupInRecord("pnd_Cntrct_Range_End__R_Field_17", "REDEFINE", pnd_Cntrct_Range_End);
        pnd_Cntrct_Range_End_Pnd_End_Prefix_1 = pnd_Cntrct_Range_End__R_Field_17.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Prefix_1", "#END-PREFIX-1", 
            FieldType.STRING, 1);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7 = pnd_Cntrct_Range_End__R_Field_17.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7", "#END-NBR-2-7", 
            FieldType.NUMERIC, 6);

        pnd_Cntrct_Range_End__R_Field_18 = pnd_Cntrct_Range_End__R_Field_17.newGroupInGroup("pnd_Cntrct_Range_End__R_Field_18", "REDEFINE", pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_2_6 = pnd_Cntrct_Range_End__R_Field_18.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_2_6", "#END-NBR-2-6", 
            FieldType.NUMERIC, 5);

        pnd_Cntrct_Range_End__R_Field_19 = pnd_Cntrct_Range_End__R_Field_17.newGroupInGroup("pnd_Cntrct_Range_End__R_Field_19", "REDEFINE", pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_2_5 = pnd_Cntrct_Range_End__R_Field_19.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_2_5", "#END-NBR-2-5", 
            FieldType.NUMERIC, 4);

        pnd_Cntrct_Range_End__R_Field_20 = pnd_Cntrct_Range_End__R_Field_17.newGroupInGroup("pnd_Cntrct_Range_End__R_Field_20", "REDEFINE", pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7);
        pnd_Cntrct_Range_End_Pnd_End_Nbr_2_4 = pnd_Cntrct_Range_End__R_Field_20.newFieldInGroup("pnd_Cntrct_Range_End_Pnd_End_Nbr_2_4", "#END-NBR-2-4", 
            FieldType.NUMERIC, 3);
        pnd_Cntrct_Last_Used = localVariables.newFieldInRecord("pnd_Cntrct_Last_Used", "#CNTRCT-LAST-USED", FieldType.STRING, 7);

        pnd_Cntrct_Last_Used__R_Field_21 = localVariables.newGroupInRecord("pnd_Cntrct_Last_Used__R_Field_21", "REDEFINE", pnd_Cntrct_Last_Used);
        pnd_Cntrct_Last_Used_Pnd_Last_Prefix_1_2 = pnd_Cntrct_Last_Used__R_Field_21.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Prefix_1_2", "#LAST-PREFIX-1-2", 
            FieldType.STRING, 2);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_7 = pnd_Cntrct_Last_Used__R_Field_21.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_7", "#LAST-NBR-3-7", 
            FieldType.NUMERIC, 5);

        pnd_Cntrct_Last_Used__R_Field_22 = pnd_Cntrct_Last_Used__R_Field_21.newGroupInGroup("pnd_Cntrct_Last_Used__R_Field_22", "REDEFINE", pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_7);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_6 = pnd_Cntrct_Last_Used__R_Field_22.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_6", "#LAST-NBR-3-6", 
            FieldType.NUMERIC, 4);
        pnd_Cntrct_Last_Used_Pnd_Last_Alpha_7 = pnd_Cntrct_Last_Used__R_Field_22.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Alpha_7", "#LAST-ALPHA-7", 
            FieldType.STRING, 1);

        pnd_Cntrct_Last_Used__R_Field_23 = pnd_Cntrct_Last_Used__R_Field_21.newGroupInGroup("pnd_Cntrct_Last_Used__R_Field_23", "REDEFINE", pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_7);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_5 = pnd_Cntrct_Last_Used__R_Field_23.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_5", "#LAST-NBR-3-5", 
            FieldType.NUMERIC, 3);
        pnd_Cntrct_Last_Used_Pnd_Last_Alpha_6 = pnd_Cntrct_Last_Used__R_Field_23.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Alpha_6", "#LAST-ALPHA-6", 
            FieldType.STRING, 1);

        pnd_Cntrct_Last_Used__R_Field_24 = pnd_Cntrct_Last_Used__R_Field_21.newGroupInGroup("pnd_Cntrct_Last_Used__R_Field_24", "REDEFINE", pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_7);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_4 = pnd_Cntrct_Last_Used__R_Field_24.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_4", "#LAST-NBR-3-4", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Last_Used_Pnd_Last_Alpha_5 = pnd_Cntrct_Last_Used__R_Field_24.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Alpha_5", "#LAST-ALPHA-5", 
            FieldType.STRING, 1);

        pnd_Cntrct_Last_Used__R_Field_25 = localVariables.newGroupInRecord("pnd_Cntrct_Last_Used__R_Field_25", "REDEFINE", pnd_Cntrct_Last_Used);
        pnd_Cntrct_Last_Used_Pnd_Last_Prefix_1 = pnd_Cntrct_Last_Used__R_Field_25.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Prefix_1", "#LAST-PREFIX-1", 
            FieldType.STRING, 1);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_7 = pnd_Cntrct_Last_Used__R_Field_25.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_7", "#LAST-NBR-2-7", 
            FieldType.NUMERIC, 6);

        pnd_Cntrct_Last_Used__R_Field_26 = pnd_Cntrct_Last_Used__R_Field_25.newGroupInGroup("pnd_Cntrct_Last_Used__R_Field_26", "REDEFINE", pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_7);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_6 = pnd_Cntrct_Last_Used__R_Field_26.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_6", "#LAST-NBR-2-6", 
            FieldType.NUMERIC, 5);

        pnd_Cntrct_Last_Used__R_Field_27 = pnd_Cntrct_Last_Used__R_Field_25.newGroupInGroup("pnd_Cntrct_Last_Used__R_Field_27", "REDEFINE", pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_7);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_5 = pnd_Cntrct_Last_Used__R_Field_27.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_5", "#LAST-NBR-2-5", 
            FieldType.NUMERIC, 4);

        pnd_Cntrct_Last_Used__R_Field_28 = pnd_Cntrct_Last_Used__R_Field_25.newGroupInGroup("pnd_Cntrct_Last_Used__R_Field_28", "REDEFINE", pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_7);
        pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_4 = pnd_Cntrct_Last_Used__R_Field_28.newFieldInGroup("pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_4", "#LAST-NBR-2-4", 
            FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_da_Ilog_Curr_Jumper.reset();
        vw_da_Ilog_Next_Jumper.reset();
        vw_ia_Ilog_Curr_Jumper.reset();
        vw_ia_Ilog_Next_Jumper.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2070() throws Exception
    {
        super("Appb2070");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        pnd_Header_Record_Pnd_H_Record_Type.setValue("H");                                                                                                                //Natural: ASSIGN #H-RECORD-TYPE := 'H'
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-RECORDS
        sub_Write_Header_Records();
        if (condition(Global.isEscape())) {return;}
        pnd_Detail_Record_Pnd_D_Record_Type.setValue("D");                                                                                                                //Natural: ASSIGN #D-RECORD-TYPE := 'D'
                                                                                                                                                                          //Natural: PERFORM READ-DA-ILOG-RECORD
        sub_Read_Da_Ilog_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-IA-ILOG-RECORD
        sub_Read_Ia_Ilog_Record();
        if (condition(Global.isEscape())) {return;}
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER-RECORDS
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DA-ILOG-RECORD
        //* *****************************************************
        //*    CHECK TO SEE IF THERE IS A NEXT AVAILABLE RANGE  *
        //* *****************************************************
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IA-ILOG-RECORD
        //* *****************************************************
        //*    CHECK TO SEE IF THERE IS A NEXT AVAILABLE RANGE  *
        //* *****************************************************
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RANGE-CALCULATIONS
    }
    private void sub_Write_Header_Records() throws Exception                                                                                                              //Natural: WRITE-HEADER-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Header_Record_Pnd_H_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                   //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #H-DATE
        pnd_Header_Record_Pnd_H_Contract_Type.setValue("DA");                                                                                                             //Natural: ASSIGN #H-CONTRACT-TYPE := 'DA'
        getWorkFiles().write(1, false, pnd_Header_Record);                                                                                                                //Natural: WRITE WORK FILE 01 #HEADER-RECORD
        pnd_Header_Record_Pnd_H_Contract_Type.setValue("IA");                                                                                                             //Natural: ASSIGN #H-CONTRACT-TYPE := 'IA'
        getWorkFiles().write(2, false, pnd_Header_Record);                                                                                                                //Natural: WRITE WORK FILE 02 #HEADER-RECORD
    }
    private void sub_Read_Da_Ilog_Record() throws Exception                                                                                                               //Natural: READ-DA-ILOG-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        vw_da_Ilog_Curr_Jumper.startDatabaseFind                                                                                                                          //Natural: FIND DA-ILOG-CURR-JUMPER WITH RCRD-NME = 'CURRENT'
        (
        "FIND01",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", WcType.WITH) }
        );
        FIND01:
        while (condition(vw_da_Ilog_Curr_Jumper.readNextRow("FIND01")))
        {
            vw_da_Ilog_Curr_Jumper.setIfNotFoundControlFlag(false);
            //*  JB2
            //*  JB2
            //*  JB2
            if (condition(da_Ilog_Curr_Jumper_Rcrd_Cde.notEquals("JU")))                                                                                                  //Natural: REJECT IF RCRD-CDE NE 'JU'
            {
                continue;
            }
            pnd_Cntrct_Range_Beg.setValue(da_Ilog_Curr_Jumper_Cntrct_Range_Start);                                                                                        //Natural: ASSIGN #CNTRCT-RANGE-BEG := CNTRCT-RANGE-START
            pnd_Cntrct_Range_End.setValue(da_Ilog_Curr_Jumper_Cntrct_Range_End);                                                                                          //Natural: ASSIGN #CNTRCT-RANGE-END := CNTRCT-RANGE-END
            pnd_Cntrct_Last_Used.setValue(da_Ilog_Curr_Jumper_Cntrct_Last_Used);                                                                                          //Natural: ASSIGN #CNTRCT-LAST-USED := CNTRCT-LAST-USED
            //*  JB2
                                                                                                                                                                          //Natural: PERFORM RANGE-CALCULATIONS
            sub_Range_Calculations();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Da_Jump_Key_Pnd_Da_Start_Dte.setValue(da_Ilog_Curr_Jumper_Start_Dte);                                                                                     //Natural: ASSIGN #DA-START-DTE := START-DTE
            pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde.setValue(da_Ilog_Curr_Jumper_Rcrd_Cde);                                                                                       //Natural: ASSIGN #DA-RCRD-CDE := RCRD-CDE
            pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme.setValue("NEXT");                                                                                                             //Natural: ASSIGN #DA-RCRD-NME := 'NEXT'
            pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde.setValue(da_Ilog_Curr_Jumper_Prdct_Cde);                                                                                     //Natural: ASSIGN #DA-PRDCT-CDE := PRDCT-CDE
            vw_da_Ilog_Next_Jumper.getTotalRowCount                                                                                                                       //Natural: FIND NUMBER DA-ILOG-NEXT-JUMPER WITH RCRD-KEY = #DA-JUMP-KEY
            (
            new Wc[] { new Wc("RCRD_KEY", "=", pnd_Da_Jump_Key, WcType.WITH) }
            );
            if (condition(vw_da_Ilog_Next_Jumper.getAstNUMBER().greater(getZero())))                                                                                      //Natural: IF *NUMBER ( ##L2100. ) > 0
            {
                pnd_Next_Range_Found.setValue(true);                                                                                                                      //Natural: ASSIGN #NEXT-RANGE-FOUND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Next_Range_Found.setValue(false);                                                                                                                     //Natural: ASSIGN #NEXT-RANGE-FOUND := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Next_Range_Found.getBoolean()))                                                                                                             //Natural: IF #NEXT-RANGE-FOUND
            {
                pnd_Detail_Record_Pnd_D_Next_Range.setValue("*");                                                                                                         //Natural: ASSIGN #D-NEXT-RANGE := '*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Detail_Record_Pnd_D_Next_Range.reset();                                                                                                               //Natural: RESET #D-NEXT-RANGE
                //*  JB1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Detail_Record_Pnd_D_Product_Code.setValue(da_Ilog_Curr_Jumper_Prdct_Cde);                                                                                 //Natural: ASSIGN #D-PRODUCT-CODE := DA-ILOG-CURR-JUMPER.PRDCT-CDE
            pnd_Detail_Record_Pnd_D_Start_Date.setValue(da_Ilog_Curr_Jumper_Start_Dte);                                                                                   //Natural: ASSIGN #D-START-DATE := DA-ILOG-CURR-JUMPER.START-DTE
            pnd_Detail_Record_Pnd_D_Start_Contract.setValue(da_Ilog_Curr_Jumper_Cntrct_Range_Start, MoveOption.LeftJustified);                                            //Natural: MOVE LEFT DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START TO #D-START-CONTRACT
            pnd_Detail_Record_Pnd_D_End_Contract.setValue(da_Ilog_Curr_Jumper_Cntrct_Range_End, MoveOption.LeftJustified);                                                //Natural: MOVE LEFT DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END TO #D-END-CONTRACT
            pnd_Detail_Record_Pnd_D_Next_Contract.setValue(da_Ilog_Curr_Jumper_Cntrct_Last_Used, MoveOption.LeftJustified);                                               //Natural: MOVE LEFT DA-ILOG-CURR-JUMPER.CNTRCT-LAST-USED TO #D-NEXT-CONTRACT
            getWorkFiles().write(1, false, pnd_Detail_Record);                                                                                                            //Natural: WRITE WORK FILE 01 #DETAIL-RECORD
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Read_Ia_Ilog_Record() throws Exception                                                                                                               //Natural: READ-IA-ILOG-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        vw_ia_Ilog_Curr_Jumper.startDatabaseFind                                                                                                                          //Natural: FIND IA-ILOG-CURR-JUMPER WITH RCRD-NME = 'CURRENT'
        (
        "FIND02",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", WcType.WITH) }
        );
        FIND02:
        while (condition(vw_ia_Ilog_Curr_Jumper.readNextRow("FIND02")))
        {
            vw_ia_Ilog_Curr_Jumper.setIfNotFoundControlFlag(false);
            //*  JB2
            //*  JB2
            //*  JB2
            if (condition(ia_Ilog_Curr_Jumper_Rcrd_Cde.notEquals("JU")))                                                                                                  //Natural: REJECT IF RCRD-CDE NE 'JU'
            {
                continue;
            }
            pnd_Cntrct_Range_Beg.setValue(ia_Ilog_Curr_Jumper_Cntrct_Range_Start);                                                                                        //Natural: ASSIGN #CNTRCT-RANGE-BEG := CNTRCT-RANGE-START
            pnd_Cntrct_Range_End.setValue(ia_Ilog_Curr_Jumper_Cntrct_Range_End);                                                                                          //Natural: ASSIGN #CNTRCT-RANGE-END := CNTRCT-RANGE-END
            pnd_Cntrct_Last_Used.setValue(ia_Ilog_Curr_Jumper_Cntrct_Last_Used);                                                                                          //Natural: ASSIGN #CNTRCT-LAST-USED := CNTRCT-LAST-USED
            //*  JB2
                                                                                                                                                                          //Natural: PERFORM RANGE-CALCULATIONS
            sub_Range_Calculations();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme.setValue("NEXT");                                                                                                             //Natural: ASSIGN #IA-RCRD-NME := 'NEXT'
            pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde.setValue(ia_Ilog_Curr_Jumper_Rcrd_Cde);                                                                                       //Natural: ASSIGN #IA-RCRD-CDE := RCRD-CDE
            pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde.setValue(ia_Ilog_Curr_Jumper_Prdct_Cde);                                                                                     //Natural: ASSIGN #IA-PRDCT-CDE := PRDCT-CDE
            vw_ia_Ilog_Next_Jumper.getTotalRowCount                                                                                                                       //Natural: FIND NUMBER IA-ILOG-NEXT-JUMPER WITH JUMP-KEY = #IA-JUMP-KEY
            (
            new Wc[] { new Wc("JUMP_KEY", "=", pnd_Ia_Jump_Key, WcType.WITH) }
            );
            if (condition(vw_ia_Ilog_Next_Jumper.getAstNUMBER().greater(getZero())))                                                                                      //Natural: IF *NUMBER ( ##L2510. ) > 0
            {
                pnd_Next_Range_Found.setValue(true);                                                                                                                      //Natural: ASSIGN #NEXT-RANGE-FOUND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Next_Range_Found.setValue(false);                                                                                                                     //Natural: ASSIGN #NEXT-RANGE-FOUND := FALSE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Next_Range_Found.getBoolean()))                                                                                                             //Natural: IF #NEXT-RANGE-FOUND
            {
                pnd_Detail_Record_Pnd_D_Next_Range.setValue("*");                                                                                                         //Natural: ASSIGN #D-NEXT-RANGE := '*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Detail_Record_Pnd_D_Next_Range.reset();                                                                                                               //Natural: RESET #D-NEXT-RANGE
                //*  JB1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Detail_Record_Pnd_D_Product_Code.setValue(ia_Ilog_Curr_Jumper_Prdct_Cde);                                                                                 //Natural: ASSIGN #D-PRODUCT-CODE := IA-ILOG-CURR-JUMPER.PRDCT-CDE
            pnd_Detail_Record_Pnd_D_Start_Date.setValue(ia_Ilog_Curr_Jumper_Start_Dte);                                                                                   //Natural: ASSIGN #D-START-DATE := IA-ILOG-CURR-JUMPER.START-DTE
            pnd_Detail_Record_Pnd_D_Start_Contract.setValue(ia_Ilog_Curr_Jumper_Cntrct_Range_Start, MoveOption.LeftJustified);                                            //Natural: MOVE LEFT IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START TO #D-START-CONTRACT
            pnd_Detail_Record_Pnd_D_End_Contract.setValue(ia_Ilog_Curr_Jumper_Cntrct_Range_End, MoveOption.LeftJustified);                                                //Natural: MOVE LEFT IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END TO #D-END-CONTRACT
            pnd_Detail_Record_Pnd_D_Next_Contract.setValue(ia_Ilog_Curr_Jumper_Cntrct_Last_Used, MoveOption.LeftJustified);                                               //Natural: MOVE LEFT IA-ILOG-CURR-JUMPER.CNTRCT-LAST-USED TO #D-NEXT-CONTRACT
            getWorkFiles().write(2, false, pnd_Detail_Record);                                                                                                            //Natural: WRITE WORK FILE 02 #DETAIL-RECORD
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    //*  JB2
    private void sub_Range_Calculations() throws Exception                                                                                                                //Natural: RANGE-CALCULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  JB2
        if (condition(DbsUtil.maskMatches(pnd_Cntrct_Range_Beg_Pnd_Beg_Prefix_1_2,"AA")))                                                                                 //Natural: IF #BEG-PREFIX-1-2 = MASK ( AA )
        {
            //*  JB2
            //*  ALPHA RANGE (POS 5) /* JB1
            //*  JB2
            //*  JB2
            //*  ALPHA RANGE (POS 6) /* JB1
            //*  JB2
            //*  JB2
            //*  ALPHA RANGE (POS 7) /* JB1
            //*  JB2
            //*  JB2
            //*  ALL NUMERICS        /* JB1
            //*  JB2
            //*  JB2
            short decideConditionsMet380 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #BEG-ALPHA-5 = MASK ( A )
            if (condition(DbsUtil.maskMatches(pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_5,"A")))
            {
                decideConditionsMet380++;
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_4.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_4)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-3-4 - #BEG-NBR-3-4
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_4.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_4)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-3-4 - #LAST-NBR-3-4
            }                                                                                                                                                             //Natural: WHEN #BEG-ALPHA-6 = MASK ( A )
            else if (condition(DbsUtil.maskMatches(pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_6,"A")))
            {
                decideConditionsMet380++;
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_5.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_5)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-3-5 - #BEG-NBR-3-5
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_5.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_5)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-3-5 - #LAST-NBR-3-5
            }                                                                                                                                                             //Natural: WHEN #BEG-ALPHA-7 = MASK ( A )
            else if (condition(DbsUtil.maskMatches(pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_7,"A")))
            {
                decideConditionsMet380++;
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_6.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_6)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-3-6 - #BEG-NBR-3-6
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_6.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_6)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-3-6 - #LAST-NBR-3-6
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_3_7)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-3-7 - #BEG-NBR-3-7
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_3_7.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_3_7)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-3-7 - #LAST-NBR-3-7
                //*  JB2
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  JB2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  JB2
            //*  ALPHA RANGE (POS 5) /* JB1
            //*  JB2
            //*  JB2
            //*  ALPHA RANGE (POS 6) /* JB1
            //*  JB2
            //*  JB2
            //*  ALPHA RANGE (POS 7) /* JB1
            //*  JB2
            //*  JB2
            //*  ALL NUMERICS        /* JB1
            //*  JB2
            //*  JB2
            short decideConditionsMet410 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #BEG-ALPHA-5 = MASK ( A )
            if (condition(DbsUtil.maskMatches(pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_5,"A")))
            {
                decideConditionsMet410++;
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_4.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_4)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-2-4 - #BEG-NBR-2-4
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_4.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_4)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-2-4 - #LAST-NBR-2-4
            }                                                                                                                                                             //Natural: WHEN #BEG-ALPHA-6 = MASK ( A )
            else if (condition(DbsUtil.maskMatches(pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_6,"A")))
            {
                decideConditionsMet410++;
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_5.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_5)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-2-5 - #BEG-NBR-2-5
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_5.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_5)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-2-5 - #LAST-NBR-2-5
            }                                                                                                                                                             //Natural: WHEN #BEG-ALPHA-7 = MASK ( A )
            else if (condition(DbsUtil.maskMatches(pnd_Cntrct_Range_Beg_Pnd_Beg_Alpha_7,"A")))
            {
                decideConditionsMet410++;
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_6.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_6)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-2-6 - #BEG-NBR-2-6
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_6.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_6)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-2-6 - #LAST-NBR-2-6
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Detail_Record_Pnd_D_Range_Size.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Size), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7.subtract(pnd_Cntrct_Range_Beg_Pnd_Beg_Nbr_2_7)); //Natural: ASSIGN #D-RANGE-SIZE := #END-NBR-2-7 - #BEG-NBR-2-7
                pnd_Detail_Record_Pnd_D_Range_Left.compute(new ComputeParameters(false, pnd_Detail_Record_Pnd_D_Range_Left), pnd_Cntrct_Range_End_Pnd_End_Nbr_2_7.subtract(pnd_Cntrct_Last_Used_Pnd_Last_Nbr_2_7)); //Natural: ASSIGN #D-RANGE-LEFT := #END-NBR-2-7 - #LAST-NBR-2-7
                //*  JB2
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  JB2
        }                                                                                                                                                                 //Natural: END-IF
        //*  JB2
    }

    //
}
