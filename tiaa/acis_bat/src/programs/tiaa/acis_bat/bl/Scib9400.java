/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:06 PM
**        * FROM NATURAL PROGRAM : Scib9400
************************************************************
**        * FILE NAME            : Scib9400.java
**        * CLASS NAME           : Scib9400
**        * INSTANCE NAME        : Scib9400
************************************************************
************************************************************************
* PROGRAM  : SCIB9400 - OMNI/ACIS MATCH AND RELEASE PROGRAM            *
* FUNCTION : READS PRAP FILE AND EXTRACTS PARTICIPANT CONTRACTS TO     *
*            DETERMINE WHETHER MONEY HAS BEEN APPLIED TO THE CONTRACT  *
*            IN OMNI.  IF MONEY WAS APPLIED, THE STATUS ON THE ACIS    *
*            PRAP FILE IS UPDATED TO MATCHED AND RELEASED.             *
* UPDATED  : 01/07/08 K.GATES   - PROGRAM CREATED                      *
*            04/09/08 K.GATES   - ADDED 5000 LIMIT PRIOR TO PROD IMPL  *
*            05/09/08 K.GATES   - ADDED SKIPPING MARKED CONTRACTS AND  *
*                                 UPDATE M/R DATE  SEE KG 5/9/08       *
*                                                                      *
*            07/14/09 G.GUERRERO- RECOMILE FOR OMNI 5.80 UPGRADE -     *
*                                 NEW VERSION OF SCIA8250              *
* ADDED 6/2011 GUY -   AP-LEGAL-ANN-OPTION CHECK FOR Y  SEE SRK        *
*                  -   SKIP CHECKING FOR FUNDING                       *
*                  -   REMOVED UPDATE COUNT LOGIC  - JHU PROJECT       *
*            04/05/17 ELLO    - ADDED NEW SUBROUTINE NEW-CALL-PREM-    *
*                               INTERFACE WHICH CALLS SCIN8251 AND THEN*
*                               CALL A COBOL PROGRAM PSG9023 TO DETER- *
*                               MINE MONEY INVESTED IN THE CONTRACT.   *
*                               THE OLD SUBROUTINE CALL-PREM-INTERFACE *
*                               WAS COMMENTED OUT.  (ONEIRA)           *
* 05/23/17 B.ELLO      PIN EXPANSION CHANGES                           *
*                      CHANGED VIEW OF PRAP WITH EXPANDED PIN.  (PINE) *
* 01/20/18 B.ELLO      BACKOUT ONEIRA LOGIC TO RETRIEVE INITIAL PREM.  *
*                      THE LOGIC IS BEING MOVED TO APPB430/PNI4300D.   *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9400 extends BLNatBase
{
    // Data Areas
    private PdaScia8250 pdaScia8250;
    private PdaAciadate pdaAciadate;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;
    private DbsField annty_Actvty_Prap_View_Ap_Status;
    private DbsField annty_Actvty_Prap_View_Ap_Dt_Matched;
    private DbsField annty_Actvty_Prap_View_Ap_Contact_Mode;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;
    private DbsField annty_Actvty_Prap_View_Ap_Legal_Ann_Option;
    private DbsField pnd_Ap_Name;
    private DbsField pnd_Cor_Full_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_1;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_2;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_3;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_4;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10;

    private DbsGroup pnd_Cor_Full_Nme__R_Field_5;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9;
    private DbsField pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20;
    private DbsField pnd_Hold_Nme_Prfx;

    private DbsGroup pnd_Miscellanous_Variables;
    private DbsField pnd_Miscellanous_Variables_Pnd_Recs_Selected_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Read_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Update_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Error_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Write_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_All_Read_Count;
    private DbsField pnd_Miscellanous_Variables_Pnd_Hold_Date;
    private DbsField pnd_Miscellanous_Variables_Pnd_Found;
    private DbsField pnd_Miscellanous_Variables_Pnd_Deflt_Change;
    private DbsField pnd_Miscellanous_Variables_Pnd_No_Issue_Dt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Process_Ind;
    private DbsField pnd_Miscellanous_Variables_Pnd_Comma;
    private DbsField pnd_Miscellanous_Variables_Pnd_Space;
    private DbsField pnd_Miscellanous_Variables_Pnd_Dash;
    private DbsField pnd_Miscellanous_Variables_Pnd_Del;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_6;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date;
    private DbsField pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time;
    private DbsField pnd_Miscellanous_Variables_Pnd_Release_Ind;
    private DbsField pnd_Miscellanous_Variables_Pnd_Debug;
    private DbsField pnd_Miscellanous_Variables_Pnd_Prem_Found;
    private DbsField pnd_Miscellanous_Variables_Pnd_Except_Found;
    private DbsField pnd_Miscellanous_Variables_Pnd_Except_Reason;
    private DbsField pnd_Miscellanous_Variables_Pnd_Exception_Cnt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_7;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Mm;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_8;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Yy;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Mm;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_Dd;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_A;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_9;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt_N;
    private DbsField pnd_Miscellanous_Variables_Pnd_Issue_Dt;
    private DbsField pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_A;

    private DbsGroup pnd_Miscellanous_Variables__R_Field_10;
    private DbsField pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_N;
    private DbsField pnd_Miscellanous_Variables_Pnd_Int_First_Time;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaScia8250 = new PdaScia8250(localVariables);
        pdaAciadate = new PdaAciadate(localVariables);

        // Local Variables

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");
        annty_Actvty_Prap_View_Ap_Dt_Matched = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dt_Matched", "AP-DT-MATCHED", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "AP_DT_MATCHED");
        annty_Actvty_Prap_View_Ap_Contact_Mode = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Contact_Mode", "AP-CONTACT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_CONTACT_MODE");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", 
            "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Legal_Ann_Option = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Legal_Ann_Option", 
            "AP-LEGAL-ANN-OPTION", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LEGAL_ANN_OPTION");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Ap_Name = localVariables.newFieldInRecord("pnd_Ap_Name", "#AP-NAME", FieldType.STRING, 30);
        pnd_Cor_Full_Nme = localVariables.newFieldInRecord("pnd_Cor_Full_Nme", "#COR-FULL-NME", FieldType.STRING, 90);

        pnd_Cor_Full_Nme__R_Field_1 = localVariables.newGroupInRecord("pnd_Cor_Full_Nme__R_Field_1", "REDEFINE", pnd_Cor_Full_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme = pnd_Cor_Full_Nme__R_Field_1.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme", "#COR-LAST-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_2 = pnd_Cor_Full_Nme__R_Field_1.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_2", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16 = pnd_Cor_Full_Nme__R_Field_2.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16", "#COR-LAST-NME-16", 
            FieldType.STRING, 16);
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14 = pnd_Cor_Full_Nme__R_Field_2.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_14", "#COR-LAST-NME-14", 
            FieldType.STRING, 14);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme = pnd_Cor_Full_Nme__R_Field_1.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme", "#COR-FIRST-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_3 = pnd_Cor_Full_Nme__R_Field_1.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_3", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_First_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10 = pnd_Cor_Full_Nme__R_Field_3.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10", "#COR-FIRST-NME-10", 
            FieldType.STRING, 10);
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20 = pnd_Cor_Full_Nme__R_Field_3.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_20", "#COR-FIRST-NME-20", 
            FieldType.STRING, 20);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme = pnd_Cor_Full_Nme__R_Field_1.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme", "#COR-MDDLE-NME", FieldType.STRING, 
            30);

        pnd_Cor_Full_Nme__R_Field_4 = pnd_Cor_Full_Nme__R_Field_1.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_4", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10 = pnd_Cor_Full_Nme__R_Field_4.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10", "#COR-MDDLE-NME-10", 
            FieldType.STRING, 10);

        pnd_Cor_Full_Nme__R_Field_5 = pnd_Cor_Full_Nme__R_Field_4.newGroupInGroup("pnd_Cor_Full_Nme__R_Field_5", "REDEFINE", pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1 = pnd_Cor_Full_Nme__R_Field_5.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1", "#COR-MDDLE-NME-1", 
            FieldType.STRING, 1);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9 = pnd_Cor_Full_Nme__R_Field_5.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_9", "#COR-MDDLE-NME-9", 
            FieldType.STRING, 9);
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20 = pnd_Cor_Full_Nme__R_Field_4.newFieldInGroup("pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_20", "#COR-MDDLE-NME-20", 
            FieldType.STRING, 20);
        pnd_Hold_Nme_Prfx = localVariables.newFieldInRecord("pnd_Hold_Nme_Prfx", "#HOLD-NME-PRFX", FieldType.STRING, 8);

        pnd_Miscellanous_Variables = localVariables.newGroupInRecord("pnd_Miscellanous_Variables", "#MISCELLANOUS-VARIABLES");
        pnd_Miscellanous_Variables_Pnd_Recs_Selected_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Recs_Selected_Count", 
            "#RECS-SELECTED-COUNT", FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Read_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Read_Count", "#READ-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Update_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Update_Count", "#UPDATE-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Error_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Error_Count", "#ERROR-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Write_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Write_Count", "#WRITE-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_All_Read_Count = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_All_Read_Count", "#ALL-READ-COUNT", 
            FieldType.NUMERIC, 9);
        pnd_Miscellanous_Variables_Pnd_Hold_Date = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Hold_Date", "#HOLD-DATE", 
            FieldType.DATE);
        pnd_Miscellanous_Variables_Pnd_Found = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Found", "#FOUND", FieldType.BOOLEAN, 
            1);
        pnd_Miscellanous_Variables_Pnd_Deflt_Change = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Deflt_Change", "#DEFLT-CHANGE", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_No_Issue_Dt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_No_Issue_Dt", "#NO-ISSUE-DT", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_Process_Ind = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Process_Ind", "#PROCESS-IND", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_Comma = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Comma", "#COMMA", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Space = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Space", "#SPACE", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Dash = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Dash", "#DASH", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Del = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Del", "#DEL", FieldType.STRING, 
            1);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time", 
            "#RQST-LOG-DTE-TIME", FieldType.STRING, 15);

        pnd_Miscellanous_Variables__R_Field_6 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_6", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Rqst_Log_Dte_Time);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date = pnd_Miscellanous_Variables__R_Field_6.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Date", 
            "#RQST-LOG-DATE", FieldType.STRING, 8);
        pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time = pnd_Miscellanous_Variables__R_Field_6.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Rqst_Log_Time", 
            "#RQST-LOG-TIME", FieldType.STRING, 7);
        pnd_Miscellanous_Variables_Pnd_Release_Ind = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Release_Ind", "#RELEASE-IND", 
            FieldType.NUMERIC, 1);
        pnd_Miscellanous_Variables_Pnd_Debug = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 
            1);
        pnd_Miscellanous_Variables_Pnd_Prem_Found = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Prem_Found", "#PREM-FOUND", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_Except_Found = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Except_Found", "#EXCEPT-FOUND", 
            FieldType.BOOLEAN, 1);
        pnd_Miscellanous_Variables_Pnd_Except_Reason = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Except_Reason", "#EXCEPT-REASON", 
            FieldType.STRING, 35);
        pnd_Miscellanous_Variables_Pnd_Exception_Cnt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Exception_Cnt", "#EXCEPTION-CNT", 
            FieldType.NUMERIC, 6);
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt", "#EXC-LINE-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt", "#MATCH-LINE-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short", "#ISSUE-DT-SHORT", 
            FieldType.NUMERIC, 4);

        pnd_Miscellanous_Variables__R_Field_7 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_7", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Issue_Dt_Short);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Mm = pnd_Miscellanous_Variables__R_Field_7.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Mm", 
            "#ISSUE-DT-S-MM", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy = pnd_Miscellanous_Variables__R_Field_7.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_S_Yy", 
            "#ISSUE-DT-S-YY", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full", "#ISSUE-DT-FULL", 
            FieldType.NUMERIC, 8);

        pnd_Miscellanous_Variables__R_Field_8 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_8", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Issue_Dt_Full);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc = pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Cc", 
            "#ISSUE-DT-CC", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Yy = pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Yy", 
            "#ISSUE-DT-YY", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Mm = pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Mm", 
            "#ISSUE-DT-MM", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_Dd = pnd_Miscellanous_Variables__R_Field_8.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_Dd", 
            "#ISSUE-DT-DD", FieldType.NUMERIC, 2);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_A = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_A", "#ISSUE-DT-A", 
            FieldType.STRING, 8);

        pnd_Miscellanous_Variables__R_Field_9 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_9", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Issue_Dt_A);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt_N = pnd_Miscellanous_Variables__R_Field_9.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt_N", 
            "#ISSUE-DT-N", FieldType.NUMERIC, 8);
        pnd_Miscellanous_Variables_Pnd_Issue_Dt = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Issue_Dt", "#ISSUE-DT", FieldType.DATE);
        pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_A = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_A", 
            "#DATE-MMDDYYYY-A", FieldType.STRING, 8);

        pnd_Miscellanous_Variables__R_Field_10 = pnd_Miscellanous_Variables.newGroupInGroup("pnd_Miscellanous_Variables__R_Field_10", "REDEFINE", pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_A);
        pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_N = pnd_Miscellanous_Variables__R_Field_10.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_N", 
            "#DATE-MMDDYYYY-N", FieldType.NUMERIC, 8);
        pnd_Miscellanous_Variables_Pnd_Int_First_Time = pnd_Miscellanous_Variables.newFieldInGroup("pnd_Miscellanous_Variables_Pnd_Int_First_Time", "#INT-FIRST-TIME", 
            FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        localVariables.reset();
        pnd_Miscellanous_Variables_Pnd_Comma.setInitialValue(",");
        pnd_Miscellanous_Variables_Pnd_Space.setInitialValue(" ");
        pnd_Miscellanous_Variables_Pnd_Dash.setInitialValue("-");
        pnd_Miscellanous_Variables_Pnd_Del.setInitialValue("+");
        pnd_Miscellanous_Variables_Pnd_Int_First_Time.setInitialValue("Y");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9400() throws Exception
    {
        super("Scib9400");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIB9400", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52;//Natural: FORMAT ( 2 ) LS = 133 PS = 52;//Natural: FORMAT ( 3 ) LS = 133 PS = 52
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        vw_annty_Actvty_Prap_View.startDatabaseRead                                                                                                                       //Natural: READ ANNTY-ACTVTY-PRAP-VIEW BY AP-RELEASE-IND
        (
        "READUPD",
        new Oc[] { new Oc("AP_RELEASE_IND", "ASC") }
        );
        READUPD:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("READUPD")))
        {
            pnd_Miscellanous_Variables_Pnd_All_Read_Count.nadd(1);                                                                                                        //Natural: ADD 1 TO #ALL-READ-COUNT
            //* ************** REMOVED COUNT LOGIC - SRK
            //* * IF #UPDATE-COUNT GT 5000                /* LIMIT TO 5000 PER DAY
            //*  IF #UPDATE-COUNT GT 5                 /* KG TEST ONLY
            //* *  ESCAPE BOTTOM
            //* * END-IF
            //*  ONLY APPLICATION RECORDS
            if (condition(annty_Actvty_Prap_View_Ap_Record_Type.notEquals(1)))                                                                                            //Natural: IF AP-RECORD-TYPE NE 1
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  NO REMEDIATION CONTRACTS
            if (condition(annty_Actvty_Prap_View_Ap_Contact_Mode.notEquals(" ")))                                                                                         //Natural: IF AP-CONTACT-MODE NE ' '
            {
                //*  KG 5/9/2008
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ONLY OMNI RECORDS
            if (condition(annty_Actvty_Prap_View_Ap_Coll_Code.notEquals("SGRD")))                                                                                         //Natural: IF AP-COLL-CODE NE 'SGRD'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  NEEDED VALUES FOR AP-STATUS
            //*  ==========================================================
            //*  D - NEW APPLICATION RELEASED
            //*  G - OVERDUE 1 (OVER 30 DAYS OVERDUE)
            //*  H - OVERDUE 2 (OVER 60 DAYS OVERDUE)
            //*  I - OVERDUE 3 (OVER 90 DAYS OVERDUE)
            //*  ==========================================================
            //*  MATCHED&RELEASED
            //*  DELETED
            //*  NEW PREMIUM ASSIGNED
            //*  SKIP- ANNUITY OPTION - SRK
            if (condition(annty_Actvty_Prap_View_Ap_Status.equals("C") || annty_Actvty_Prap_View_Ap_Status.equals("A") || annty_Actvty_Prap_View_Ap_Status.equals("B")    //Natural: IF AP-STATUS EQ 'C' OR AP-STATUS EQ 'A' OR AP-STATUS EQ 'B' OR AP-LEGAL-ANN-OPTION = 'Y'
                || annty_Actvty_Prap_View_Ap_Legal_Ann_Option.equals("Y")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  NEEDED VALUES FOR AP-RELEASE-IND
            //*  ==========================================================
            //*  1 - NEW APPLICATION
            //*  3 - APPLICATION READY FOR WELCOME PRINTING
            //*  4 - APPLICATION READY FOR LEGAL PRINTING
            //*  7 - DEFAULT ENROLLMENT AWAITING APPLICATION - SHOULD NOT BE PRINTED!
            //*  ==========================================================
            //*  NEW PREMIUM
            //*  ANY OTHERS
            if (condition(annty_Actvty_Prap_View_Ap_Release_Ind.equals(2) || annty_Actvty_Prap_View_Ap_Release_Ind.greaterOrEqual(5)))                                    //Natural: IF AP-RELEASE-IND EQ 2 OR AP-RELEASE-IND GE 5
            {
                //*      OR AP-RELEASE-IND EQ 5                   /* ILLINOIS SMP
                //*      OR AP-RELEASE-IND EQ 6                   /* ALL PACKAGES PRINTED
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Miscellanous_Variables_Pnd_Read_Count.nadd(1);                                                                                                            //Natural: ADD 1 TO #READ-COUNT
            //*  WRITE '=' AP-TIAA-CNTRCT                    /* TEST ONLY
                                                                                                                                                                          //Natural: PERFORM CALL-PREM-INTERFACE
            sub_Call_Prem_Interface();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READUPD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READUPD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Miscellanous_Variables_Pnd_Prem_Found.getBoolean()))                                                                                        //Natural: IF #PREM-FOUND
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-RECORDS
                sub_Update_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READUPD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READUPD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-MATCH-REPORT
                sub_Write_Match_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READUPD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READUPD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT
        sub_Write_Control_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaScia8250.getPpt_File_Open_Sw().equals("Y")))                                                                                                     //Natural: IF PPT-FILE-OPEN-SW = 'Y'
        {
            pdaScia8250.getPpt_Part_Fund_Rec().reset();                                                                                                                   //Natural: RESET PPT-PART-FUND-REC
            pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Eof_Sw().setValue("Y");                                                                                                  //Natural: ASSIGN PPT-EOF-SW := 'Y'
                                                                                                                                                                          //Natural: PERFORM CALL-PREM-INTERFACE
            sub_Call_Prem_Interface();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-PREM-INTERFACE
        //* *-------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RECORDS
        //* *---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REPORT
        //* *---------------------------------------
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT
        //* *----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXCEPTION-REPORT-WRITE-HEADER
        //* *-----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MATCH-REPORT
        //* *----------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MATCH-REPORT-WRITE-HEADER
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-RTN
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Call_Prem_Interface() throws Exception                                                                                                               //Natural: CALL-PREM-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        getReports().write(0, "GOT TO CALL-PREM-INTERFACE");                                                                                                              //Natural: WRITE 'GOT TO CALL-PREM-INTERFACE'
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Prem_Found.reset();                                                                                                                //Natural: RESET #PREM-FOUND #EXCEPT-FOUND
        pnd_Miscellanous_Variables_Pnd_Except_Found.reset();
        pdaScia8250.getPpt_Part_Fund_Rec().reset();                                                                                                                       //Natural: RESET PPT-PART-FUND-REC
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Request_Type().setValue("F");                                                                                                //Natural: ASSIGN PPT-REQUEST-TYPE := 'F'
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Eof_Sw().setValue("N");                                                                                                      //Natural: ASSIGN PPT-EOF-SW := 'N'
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Plan_Num().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                 //Natural: ASSIGN PPT-PLAN-NUM := AP-SGRD-PLAN-NO
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Part_Sub_Plan().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No);                                                         //Natural: ASSIGN PPT-PART-SUB-PLAN := AP-SGRD-SUBPLAN-NO
        if (condition(annty_Actvty_Prap_View_Ap_Soc_Sec.greater(getZero())))                                                                                              //Natural: IF AP-SOC-SEC GT 0
        {
            pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Part_Number().setValueEdited(annty_Actvty_Prap_View_Ap_Soc_Sec,new ReportEditMask("999999999"));                         //Natural: MOVE EDITED AP-SOC-SEC ( EM = 999999999 ) TO PPT-PART-NUMBER
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Miscellanous_Variables_Pnd_Int_First_Time.equals("Y")))                                                                                         //Natural: IF #INT-FIRST-TIME = 'Y'
        {
            pdaScia8250.getPpt_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN PPT-FILE-OPEN-SW := 'N'
            pnd_Miscellanous_Variables_Pnd_Int_First_Time.setValue("N");                                                                                                  //Natural: ASSIGN #INT-FIRST-TIME := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia8250.getPpt_File_Open_Sw().setValue("Y");                                                                                                              //Natural: ASSIGN PPT-FILE-OPEN-SW := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Scin8250.class , getCurrentProcessState(), pdaScia8250.getPpt_File_Open_Sw(), pdaScia8250.getPpt_Part_Fund_Rec());                                //Natural: CALLNAT 'SCIN8250' PPT-FILE-OPEN-SW PPT-PART-FUND-REC
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Code().notEquals(getZero())))                                                                           //Natural: IF PPT-RETURN-CODE NE 0
        {
            pnd_Miscellanous_Variables_Pnd_Except_Found.setValue(true);                                                                                                   //Natural: ASSIGN #EXCEPT-FOUND = TRUE
            pnd_Miscellanous_Variables_Pnd_Exception_Cnt.nadd(1);                                                                                                         //Natural: ADD 1 TO #EXCEPTION-CNT
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT
            sub_Exception_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_First_Contrib_Date().greater(getZero())))                                                                  //Natural: IF PPT-FIRST-CONTRIB-DATE GT 0
            {
                pnd_Miscellanous_Variables_Pnd_Prem_Found.setValue(true);                                                                                                 //Natural: ASSIGN #PREM-FOUND = TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-PREM-INTERFACE
    }
    private void sub_Update_Records() throws Exception                                                                                                                    //Natural: UPDATE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        GET1:                                                                                                                                                             //Natural: GET ANNTY-ACTVTY-PRAP-VIEW *ISN ( READUPD. )
        vw_annty_Actvty_Prap_View.readByID(vw_annty_Actvty_Prap_View.getAstISN("READUPD"), "GET1");
        annty_Actvty_Prap_View_Ap_Status.setValue("C");                                                                                                                   //Natural: MOVE 'C' TO AP-STATUS
        getReports().write(0, "UPDATING CNTRCT:",annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,"STATUS:",annty_Actvty_Prap_View_Ap_Status);                                       //Natural: WRITE 'UPDATING CNTRCT:' AP-TIAA-CNTRCT 'STATUS:' AP-STATUS
        if (Global.isEscape()) return;
        //*  KG 5/9/2008
        pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_A.setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYYYY"));                                                   //Natural: MOVE EDITED *DATX ( EM = MMDDYYYY ) TO #DATE-MMDDYYYY-A
        //*  KG 5/9/2008
        annty_Actvty_Prap_View_Ap_Dt_Matched.setValue(pnd_Miscellanous_Variables_Pnd_Date_Mmddyyyy_N);                                                                    //Natural: MOVE #DATE-MMDDYYYY-N TO AP-DT-MATCHED
        vw_annty_Actvty_Prap_View.updateDBRow("GET1");                                                                                                                    //Natural: UPDATE ( GET1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Miscellanous_Variables_Pnd_Update_Count.nadd(1);                                                                                                              //Natural: ADD 1 TO #UPDATE-COUNT
    }
    //*  TITLE LEFT JUSTIFIED
    private void sub_Write_Control_Report() throws Exception                                                                                                              //Natural: WRITE-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. MATCH AND RELEASE CONTROL TOTALS    ",new ColumnSpacing(16),Global.getDATU());           //Natural: WRITE ( 1 ) *PROGRAM 5X 'A.C.I.S. MATCH AND RELEASE CONTROL TOTALS    ' 16X *DATU
        if (Global.isEscape()) return;
        getReports().write(1, Global.getTIMX(),new ColumnSpacing(58),NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);                                                            //Natural: WRITE ( 1 ) *TIMX 58X /////
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL PRAP RECORDS READ ",new ColumnSpacing(19),pnd_Miscellanous_Variables_Pnd_All_Read_Count,NEWLINE);              //Natural: WRITE ( 1 ) 14X 'TOTAL PRAP RECORDS READ ' 19X #ALL-READ-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL PRAP UNMATCHED/RELEASED RECORDS READ",pnd_Miscellanous_Variables_Pnd_Read_Count,NEWLINE);                      //Natural: WRITE ( 1 ) 14X 'TOTAL PRAP UNMATCHED/RELEASED RECORDS READ' #READ-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL APPLICATION RECORDS UPDATED TO PRAP ",pnd_Miscellanous_Variables_Pnd_Update_Count,NEWLINE);                    //Natural: WRITE ( 1 ) 14X 'TOTAL APPLICATION RECORDS UPDATED TO PRAP ' #UPDATE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(14),"TOTAL RECORDS IN ERROR ",new ColumnSpacing(23),pnd_Miscellanous_Variables_Pnd_Exception_Cnt,NEWLINE,                 //Natural: WRITE ( 1 ) 14X 'TOTAL RECORDS IN ERROR ' 23X #EXCEPTION-CNT ///////
            NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new ColumnSpacing(16),"**    END    OF   REPORT   **");                                                                                     //Natural: WRITE ( 1 ) 16X '**    END    OF   REPORT   **'
        if (Global.isEscape()) return;
    }
    private void sub_Exception_Report() throws Exception                                                                                                                  //Natural: EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        short decideConditionsMet385 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PPT-RETURN-MSG-MODULE EQ 'PTPTIOX'
        if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().equals("PTPTIOX")))
        {
            decideConditionsMet385++;
            pnd_Miscellanous_Variables_Pnd_Except_Reason.setValue("PARTICIPANT NOT FOUND IN OMNI");                                                                       //Natural: MOVE 'PARTICIPANT NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN PPT-RETURN-MSG-MODULE EQ 'PLPLIO'
        else if (condition(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().equals("PLPLIO")))
        {
            decideConditionsMet385++;
            pnd_Miscellanous_Variables_Pnd_Except_Reason.setValue("PLAN NOT FOUND IN OMNI");                                                                              //Natural: MOVE 'PLAN NOT FOUND IN OMNI' TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Miscellanous_Variables_Pnd_Except_Reason.setValue(pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Message());                                                 //Natural: MOVE PPT-RETURN-MESSAGE TO #EXCEPT-REASON
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().write(2, NEWLINE,new ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Soc_Sec, new ReportEditMask ("999-99-9999"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,  //Natural: WRITE ( 2 ) / 2X AP-SOC-SEC ( EM = 999-99-9999 ) 7X AP-TIAA-CNTRCT ( EM = XXXXXXX-X ) 7X AP-CREF-CERT ( EM = XXXXXXX-X ) 8X AP-SGRD-PLAN-NO 11X AP-SGRD-SUBPLAN-NO 7X #EXCEPT-REASON
            new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Cref_Cert, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(8),annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,new 
            ColumnSpacing(11),annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,new ColumnSpacing(7),pnd_Miscellanous_Variables_Pnd_Except_Reason);
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #EXC-LINE-CNT
        pdaScia8250.getPpt_Part_Fund_Rec_Ppt_Return_Msg_Module().reset();                                                                                                 //Natural: RESET PPT-RETURN-MSG-MODULE
        //*  EXCEPTION-REPORT
    }
    private void sub_Exception_Report_Write_Header() throws Exception                                                                                                     //Natural: EXCEPTION-REPORT-WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        //*  TITLE LEFT JUSTIFIED
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.reset();                                                                                                              //Natural: RESET #EXC-LINE-CNT
        getReports().write(2, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. OMNI MATCH/RELEASE DAILY EXCEPTION REPORT",new ColumnSpacing(51),Global.getDATU());      //Natural: WRITE ( 2 ) *PROGRAM 5X 'A.C.I.S. OMNI MATCH/RELEASE DAILY EXCEPTION REPORT' 51X *DATU
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(114),Global.getTIMX(),NEWLINE,NEWLINE);                                                                                   //Natural: WRITE ( 2 ) 114X *TIMX //
        if (Global.isEscape()) return;
        getReports().write(2, "SOCIAL SECURITY",new ColumnSpacing(3),"TIAA CONTRACT",new ColumnSpacing(3),"CREF CONTRACT",new ColumnSpacing(3),"SUNGARD PLAN",new         //Natural: WRITE ( 2 ) 'SOCIAL SECURITY' 3X 'TIAA CONTRACT' 3X 'CREF CONTRACT' 3X 'SUNGARD PLAN' 3X 'SUNGARD SUBPLAN' 3X 'EXCEPTION'/
            ColumnSpacing(3),"SUNGARD SUBPLAN",new ColumnSpacing(3),"EXCEPTION",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, new ColumnSpacing(4),"NUMBER",new ColumnSpacing(11),"NUMBER",new ColumnSpacing(10),"NUMBER",new ColumnSpacing(10),"NUMBER",new              //Natural: WRITE ( 2 ) 4X 'NUMBER' 11X 'NUMBER' 10X 'NUMBER' 10X 'NUMBER' 11X 'NUMBER' 7X 'REASON' /
            ColumnSpacing(11),"NUMBER",new ColumnSpacing(7),"REASON",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, "---------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"------------",new         //Natural: WRITE ( 2 ) '---------------' 3X '-------------' 3X '-------------' 3X '------------' 3X '---------------' 3X '------------------------------------------'/
            ColumnSpacing(3),"---------------",new ColumnSpacing(3),"------------------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Exc_Line_Cnt.nadd(5);                                                                                                              //Natural: ADD 5 TO #EXC-LINE-CNT
        //*  EXCEPTION-REPORT-WRITE-HEADER
    }
    private void sub_Write_Match_Report() throws Exception                                                                                                                //Natural: WRITE-MATCH-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
                                                                                                                                                                          //Natural: PERFORM NAME-RTN
        sub_Name_Rtn();
        if (condition(Global.isEscape())) {return;}
        getReports().write(3, NEWLINE,new ColumnSpacing(2),annty_Actvty_Prap_View_Ap_Soc_Sec, new ReportEditMask ("999-99-9999"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,  //Natural: WRITE ( 3 ) / 2X AP-SOC-SEC ( EM = 999-99-9999 ) 7X AP-TIAA-CNTRCT ( EM = XXXXXXX-X ) 7X AP-CREF-CERT ( EM = XXXXXXX-X ) 8X AP-SGRD-PLAN-NO 11X AP-SGRD-SUBPLAN-NO 8X #AP-NAME
            new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(7),annty_Actvty_Prap_View_Ap_Cref_Cert, new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(8),annty_Actvty_Prap_View_Ap_Sgrd_Plan_No,new 
            ColumnSpacing(11),annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No,new ColumnSpacing(8),pnd_Ap_Name);
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt.nadd(1);                                                                                                            //Natural: ADD 1 TO #MATCH-LINE-CNT
        pnd_Miscellanous_Variables_Pnd_Write_Count.nadd(1);                                                                                                               //Natural: ADD 1 TO #WRITE-COUNT
        //*  WRITE-MATCH-REPORT
    }
    private void sub_Match_Report_Write_Header() throws Exception                                                                                                         //Natural: MATCH-REPORT-WRITE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------
        //*  TITLE LEFT JUSTIFIED
        pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt.reset();                                                                                                            //Natural: RESET #MATCH-LINE-CNT
        getReports().write(3, Global.getPROGRAM(),new ColumnSpacing(5),"A.C.I.S. OMNI MATCH/RELEASE DAILY REPORT",new ColumnSpacing(52),Global.getDATU());                //Natural: WRITE ( 3 ) *PROGRAM 5X 'A.C.I.S. OMNI MATCH/RELEASE DAILY REPORT' 52X *DATU
        if (Global.isEscape()) return;
        getReports().write(3, new ColumnSpacing(105),Global.getTIMX(),NEWLINE,NEWLINE);                                                                                   //Natural: WRITE ( 3 ) 105X *TIMX //
        if (Global.isEscape()) return;
        getReports().write(3, "SOCIAL SECURITY",new ColumnSpacing(3),"TIAA CONTRACT",new ColumnSpacing(3),"CREF CONTRACT",new ColumnSpacing(3),"SUNGARD PLAN",new         //Natural: WRITE ( 3 ) 'SOCIAL SECURITY' 3X 'TIAA CONTRACT' 3X 'CREF CONTRACT' 3X 'SUNGARD PLAN' 3X 'SUNGARD SUBPLAN' 4X 'PARTICIPANT' /
            ColumnSpacing(3),"SUNGARD SUBPLAN",new ColumnSpacing(4),"PARTICIPANT",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(3, new ColumnSpacing(4),"NUMBER",new ColumnSpacing(11),"NUMBER",new ColumnSpacing(10),"NUMBER",new ColumnSpacing(10),"NUMBER",new              //Natural: WRITE ( 3 ) 4X 'NUMBER' 11X 'NUMBER' 10X 'NUMBER' 10X 'NUMBER' 11X 'NUMBER' 11X 'NAME' /
            ColumnSpacing(11),"NUMBER",new ColumnSpacing(11),"NAME",NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(3, "---------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"-------------",new ColumnSpacing(3),"------------",new         //Natural: WRITE ( 3 ) '---------------' 3X '-------------' 3X '-------------' 3X '------------' 3X '---------------' 3X '-------------------------------'/
            ColumnSpacing(3),"---------------",new ColumnSpacing(3),"-------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Miscellanous_Variables_Pnd_Match_Line_Cnt.nadd(6);                                                                                                            //Natural: ADD 6 TO #MATCH-LINE-CNT
        //*  DEFLT-REPORT-WRITE-HEADER
    }
    private void sub_Name_Rtn() throws Exception                                                                                                                          //Natural: NAME-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Ap_Name.reset();                                                                                                                                              //Natural: RESET #AP-NAME
        pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme.setValue(annty_Actvty_Prap_View_Ap_Cor_Last_Nme);                                                                               //Natural: MOVE AP-COR-LAST-NME TO #COR-LAST-NME
        pnd_Cor_Full_Nme_Pnd_Cor_First_Nme.setValue(annty_Actvty_Prap_View_Ap_Cor_First_Nme);                                                                             //Natural: MOVE AP-COR-FIRST-NME TO #COR-FIRST-NME
        pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme.setValue(annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme);                                                                             //Natural: MOVE AP-COR-MDDLE-NME TO #COR-MDDLE-NME
        if (condition(pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_10.equals(" ")))                                                                                                 //Natural: IF #COR-MDDLE-NME-10 = ' '
        {
            pnd_Ap_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16, pnd_Miscellanous_Variables_Pnd_Comma,              //Natural: COMPRESS #COR-LAST-NME-16 #COMMA #DEL #COR-FIRST-NME-10 INTO #AP-NAME LEAVING NO SPACE
                pnd_Miscellanous_Variables_Pnd_Del, pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ap_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Cor_Full_Nme_Pnd_Cor_Last_Nme_16, pnd_Miscellanous_Variables_Pnd_Comma,              //Natural: COMPRESS #COR-LAST-NME-16 #COMMA #DEL #COR-FIRST-NME-10 #COMMA #DEL #COR-MDDLE-NME-1 INTO #AP-NAME LEAVING NO SPACE
                pnd_Miscellanous_Variables_Pnd_Del, pnd_Cor_Full_Nme_Pnd_Cor_First_Nme_10, pnd_Miscellanous_Variables_Pnd_Comma, pnd_Miscellanous_Variables_Pnd_Del, 
                pnd_Cor_Full_Nme_Pnd_Cor_Mddle_Nme_1));
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Ap_Name,true), new ExamineSearch("+"), new ExamineReplace(" "));                                                            //Natural: EXAMINE FULL #AP-NAME '+' REPLACE WITH ' '
        //*  NAME-RTN
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM EXCEPTION-REPORT-WRITE-HEADER
                    sub_Exception_Report_Write_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM MATCH-REPORT-WRITE-HEADER
                    sub_Match_Report_Write_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE // 'CONTRACT:' 25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),annty_Actvty_Prap_View_Ap_Pin_Nbr);
        getReports().write(0, "PRAP RECORDS UPDATED:",pnd_Miscellanous_Variables_Pnd_Update_Count, new ReportEditMask ("ZZZ,ZZ9"));                                       //Natural: WRITE 'PRAP RECORDS UPDATED:' #UPDATE-COUNT ( EM = ZZZ,ZZ9 )
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52");
        Global.format(2, "LS=133 PS=52");
        Global.format(3, "LS=133 PS=52");
    }
}
