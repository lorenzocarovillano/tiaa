/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:48 PM
**        * FROM NATURAL PROGRAM : Appb2075
************************************************************
**        * FILE NAME            : Appb2075.java
**        * CLASS NAME           : Appb2075
**        * INSTANCE NAME        : Appb2075
************************************************************
************************************************************************
* APPP2075 - DETERMINES NEXT ALPHA VALUE                               *
* USED BY BATCH TO STORE NEXT CONTRACT RANGE                           *
* USED BY ONLINE TO DETERMINE NEXT ALPHA CONTRACT NUMBER               *
************************************************************************
*
************************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                     *
* ----------  ----------------  -------------------------------------- *
* 02/01/2007  JANET BERGHEISER  NEW                                    *
* 06/02/2007  JANET BERGHEISER  ADD UPDATE COUNT & END OF REPORT (JB1) *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb2075 extends BLNatBase
{
    // Data Areas
    private PdaAppa2075 pdaAppa2075;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_da_Ilog_Curr_Jumper;
    private DbsField da_Ilog_Curr_Jumper_Start_Dte;
    private DbsField da_Ilog_Curr_Jumper_Rcrd_Cde;
    private DbsField da_Ilog_Curr_Jumper_Rcrd_Nme;
    private DbsField da_Ilog_Curr_Jumper_Prdct_Cde;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Cref_Pref;
    private DbsField da_Ilog_Curr_Jumper_Rcrd_Revision_Nbr;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Range_Start;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Range_End;
    private DbsField da_Ilog_Curr_Jumper_Cntrct_Last_Used;
    private DbsField da_Ilog_Curr_Jumper_Previous_Rcrd_Dte;
    private DbsField da_Ilog_Curr_Jumper_Current_Rcrd_Dte;

    private DataAccessProgramView vw_da_Ilog_Next_Jumper;
    private DbsField da_Ilog_Next_Jumper_Start_Dte;
    private DbsField da_Ilog_Next_Jumper_Rcrd_Cde;
    private DbsField da_Ilog_Next_Jumper_Rcrd_Nme;
    private DbsField da_Ilog_Next_Jumper_Prdct_Cde;
    private DbsField da_Ilog_Next_Jumper_Cntrct_Cref_Pref;
    private DbsField da_Ilog_Next_Jumper_Rcrd_Revision_Nbr;
    private DbsField da_Ilog_Next_Jumper_Cntrct_Range_Start;
    private DbsField da_Ilog_Next_Jumper_Cntrct_Range_End;
    private DbsField da_Ilog_Next_Jumper_Cntrct_Last_Used;
    private DbsField da_Ilog_Next_Jumper_Previous_Rcrd_Dte;
    private DbsField da_Ilog_Next_Jumper_Current_Rcrd_Dte;

    private DataAccessProgramView vw_ia_Ilog_Curr_Jumper;
    private DbsField ia_Ilog_Curr_Jumper_Start_Dte;
    private DbsField ia_Ilog_Curr_Jumper_Rcrd_Cde;
    private DbsField ia_Ilog_Curr_Jumper_Rcrd_Nme;
    private DbsField ia_Ilog_Curr_Jumper_Prdct_Cde;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Cref_Pref;
    private DbsField ia_Ilog_Curr_Jumper_Rcrd_Revision_Nbr;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Range_Start;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Range_End;
    private DbsField ia_Ilog_Curr_Jumper_Cntrct_Last_Used;
    private DbsField ia_Ilog_Curr_Jumper_Previous_Rcrd_Dte;
    private DbsField ia_Ilog_Curr_Jumper_Current_Rcrd_Dte;

    private DataAccessProgramView vw_ia_Ilog_Next_Jumper;
    private DbsField ia_Ilog_Next_Jumper_Start_Dte;
    private DbsField ia_Ilog_Next_Jumper_Rcrd_Cde;
    private DbsField ia_Ilog_Next_Jumper_Rcrd_Nme;
    private DbsField ia_Ilog_Next_Jumper_Prdct_Cde;
    private DbsField ia_Ilog_Next_Jumper_Cntrct_Cref_Pref;
    private DbsField ia_Ilog_Next_Jumper_Rcrd_Revision_Nbr;
    private DbsField ia_Ilog_Next_Jumper_Cntrct_Range_Start;
    private DbsField ia_Ilog_Next_Jumper_Cntrct_Range_End;
    private DbsField ia_Ilog_Next_Jumper_Cntrct_Last_Used;
    private DbsField ia_Ilog_Next_Jumper_Previous_Rcrd_Dte;
    private DbsField ia_Ilog_Next_Jumper_Current_Rcrd_Dte;
    private DbsField pnd_Da_Jump_Key;

    private DbsGroup pnd_Da_Jump_Key__R_Field_1;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Start_Dte;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme;
    private DbsField pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde;
    private DbsField pnd_Ia_Jump_Key;

    private DbsGroup pnd_Ia_Jump_Key__R_Field_2;
    private DbsField pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme;
    private DbsField pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde;
    private DbsField pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde;
    private DbsField pnd_Da_Add_Jumper_Count;
    private DbsField pnd_Ia_Add_Jumper_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAppa2075 = new PdaAppa2075(localVariables);

        // Local Variables

        vw_da_Ilog_Curr_Jumper = new DataAccessProgramView(new NameInfo("vw_da_Ilog_Curr_Jumper", "DA-ILOG-CURR-JUMPER"), "APP_ILOG_J_RCRD", "ACIS_APP_ILOG");
        da_Ilog_Curr_Jumper_Start_Dte = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        da_Ilog_Curr_Jumper_Rcrd_Cde = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        da_Ilog_Curr_Jumper_Rcrd_Nme = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        da_Ilog_Curr_Jumper_Prdct_Cde = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PRDCT_CDE");
        da_Ilog_Curr_Jumper_Cntrct_Cref_Pref = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Cref_Pref", "CNTRCT-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CREF_PREF");
        da_Ilog_Curr_Jumper_Rcrd_Revision_Nbr = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Rcrd_Revision_Nbr", "RCRD-REVISION-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RCRD_REVISION_NBR");
        da_Ilog_Curr_Jumper_Cntrct_Range_Start = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Range_Start", "CNTRCT-RANGE-START", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_RANGE_START");
        da_Ilog_Curr_Jumper_Cntrct_Range_End = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Range_End", "CNTRCT-RANGE-END", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_RANGE_END");
        da_Ilog_Curr_Jumper_Cntrct_Last_Used = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Cntrct_Last_Used", "CNTRCT-LAST-USED", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_LAST_USED");
        da_Ilog_Curr_Jumper_Previous_Rcrd_Dte = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Previous_Rcrd_Dte", "PREVIOUS-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PREVIOUS_RCRD_DTE");
        da_Ilog_Curr_Jumper_Current_Rcrd_Dte = vw_da_Ilog_Curr_Jumper.getRecord().newFieldInGroup("da_Ilog_Curr_Jumper_Current_Rcrd_Dte", "CURRENT-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CURRENT_RCRD_DTE");
        registerRecord(vw_da_Ilog_Curr_Jumper);

        vw_da_Ilog_Next_Jumper = new DataAccessProgramView(new NameInfo("vw_da_Ilog_Next_Jumper", "DA-ILOG-NEXT-JUMPER"), "APP_ILOG_J_RCRD", "ACIS_APP_ILOG");
        da_Ilog_Next_Jumper_Start_Dte = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        da_Ilog_Next_Jumper_Rcrd_Cde = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        da_Ilog_Next_Jumper_Rcrd_Nme = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        da_Ilog_Next_Jumper_Prdct_Cde = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PRDCT_CDE");
        da_Ilog_Next_Jumper_Cntrct_Cref_Pref = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Cntrct_Cref_Pref", "CNTRCT-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CREF_PREF");
        da_Ilog_Next_Jumper_Rcrd_Revision_Nbr = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Rcrd_Revision_Nbr", "RCRD-REVISION-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RCRD_REVISION_NBR");
        da_Ilog_Next_Jumper_Cntrct_Range_Start = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Cntrct_Range_Start", "CNTRCT-RANGE-START", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_RANGE_START");
        da_Ilog_Next_Jumper_Cntrct_Range_End = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Cntrct_Range_End", "CNTRCT-RANGE-END", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_RANGE_END");
        da_Ilog_Next_Jumper_Cntrct_Last_Used = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Cntrct_Last_Used", "CNTRCT-LAST-USED", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "CNTRCT_LAST_USED");
        da_Ilog_Next_Jumper_Previous_Rcrd_Dte = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Previous_Rcrd_Dte", "PREVIOUS-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PREVIOUS_RCRD_DTE");
        da_Ilog_Next_Jumper_Current_Rcrd_Dte = vw_da_Ilog_Next_Jumper.getRecord().newFieldInGroup("da_Ilog_Next_Jumper_Current_Rcrd_Dte", "CURRENT-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CURRENT_RCRD_DTE");
        registerRecord(vw_da_Ilog_Next_Jumper);

        vw_ia_Ilog_Curr_Jumper = new DataAccessProgramView(new NameInfo("vw_ia_Ilog_Curr_Jumper", "IA-ILOG-CURR-JUMPER"), "CIS_CNTRCT_J_RCRD", "CIS_CNTRCT_FILE");
        ia_Ilog_Curr_Jumper_Start_Dte = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        ia_Ilog_Curr_Jumper_Rcrd_Cde = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        ia_Ilog_Curr_Jumper_Rcrd_Nme = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        ia_Ilog_Curr_Jumper_Prdct_Cde = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PRDCT_CDE");
        ia_Ilog_Curr_Jumper_Cntrct_Cref_Pref = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Cref_Pref", "CNTRCT-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CREF_PREF");
        ia_Ilog_Curr_Jumper_Rcrd_Revision_Nbr = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Rcrd_Revision_Nbr", "RCRD-REVISION-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RCRD_REVISION_NBR");
        ia_Ilog_Curr_Jumper_Cntrct_Range_Start = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Range_Start", "CNTRCT-RANGE-START", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_RANGE_START");
        ia_Ilog_Curr_Jumper_Cntrct_Range_End = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Range_End", "CNTRCT-RANGE-END", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_RANGE_END");
        ia_Ilog_Curr_Jumper_Cntrct_Last_Used = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Cntrct_Last_Used", "CNTRCT-LAST-USED", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_LAST_USED");
        ia_Ilog_Curr_Jumper_Previous_Rcrd_Dte = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Previous_Rcrd_Dte", "PREVIOUS-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PREVIOUS_RCRD_DTE");
        ia_Ilog_Curr_Jumper_Current_Rcrd_Dte = vw_ia_Ilog_Curr_Jumper.getRecord().newFieldInGroup("ia_Ilog_Curr_Jumper_Current_Rcrd_Dte", "CURRENT-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CURRENT_RCRD_DTE");
        registerRecord(vw_ia_Ilog_Curr_Jumper);

        vw_ia_Ilog_Next_Jumper = new DataAccessProgramView(new NameInfo("vw_ia_Ilog_Next_Jumper", "IA-ILOG-NEXT-JUMPER"), "CIS_CNTRCT_J_RCRD", "CIS_CNTRCT_FILE");
        ia_Ilog_Next_Jumper_Start_Dte = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Start_Dte", "START-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "START_DTE");
        ia_Ilog_Next_Jumper_Rcrd_Cde = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Rcrd_Cde", "RCRD-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_CDE");
        ia_Ilog_Next_Jumper_Rcrd_Nme = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Rcrd_Nme", "RCRD-NME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "RCRD_NME");
        ia_Ilog_Next_Jumper_Prdct_Cde = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Prdct_Cde", "PRDCT-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PRDCT_CDE");
        ia_Ilog_Next_Jumper_Cntrct_Cref_Pref = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Cntrct_Cref_Pref", "CNTRCT-CREF-PREF", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CREF_PREF");
        ia_Ilog_Next_Jumper_Rcrd_Revision_Nbr = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Rcrd_Revision_Nbr", "RCRD-REVISION-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RCRD_REVISION_NBR");
        ia_Ilog_Next_Jumper_Cntrct_Range_Start = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Cntrct_Range_Start", "CNTRCT-RANGE-START", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_RANGE_START");
        ia_Ilog_Next_Jumper_Cntrct_Range_End = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Cntrct_Range_End", "CNTRCT-RANGE-END", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_RANGE_END");
        ia_Ilog_Next_Jumper_Cntrct_Last_Used = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Cntrct_Last_Used", "CNTRCT-LAST-USED", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_LAST_USED");
        ia_Ilog_Next_Jumper_Previous_Rcrd_Dte = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Previous_Rcrd_Dte", "PREVIOUS-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PREVIOUS_RCRD_DTE");
        ia_Ilog_Next_Jumper_Current_Rcrd_Dte = vw_ia_Ilog_Next_Jumper.getRecord().newFieldInGroup("ia_Ilog_Next_Jumper_Current_Rcrd_Dte", "CURRENT-RCRD-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CURRENT_RCRD_DTE");
        registerRecord(vw_ia_Ilog_Next_Jumper);

        pnd_Da_Jump_Key = localVariables.newFieldInRecord("pnd_Da_Jump_Key", "#DA-JUMP-KEY", FieldType.STRING, 26);

        pnd_Da_Jump_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Da_Jump_Key__R_Field_1", "REDEFINE", pnd_Da_Jump_Key);
        pnd_Da_Jump_Key_Pnd_Da_Start_Dte = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Start_Dte", "#DA-START-DTE", FieldType.STRING, 
            8);
        pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde", "#DA-RCRD-CDE", FieldType.STRING, 
            2);
        pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme", "#DA-RCRD-NME", FieldType.STRING, 
            12);
        pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde = pnd_Da_Jump_Key__R_Field_1.newFieldInGroup("pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde", "#DA-PRDCT-CDE", FieldType.STRING, 
            4);
        pnd_Ia_Jump_Key = localVariables.newFieldInRecord("pnd_Ia_Jump_Key", "#IA-JUMP-KEY", FieldType.STRING, 24);

        pnd_Ia_Jump_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ia_Jump_Key__R_Field_2", "REDEFINE", pnd_Ia_Jump_Key);
        pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme = pnd_Ia_Jump_Key__R_Field_2.newFieldInGroup("pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme", "#IA-RCRD-NME", FieldType.STRING, 
            12);
        pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde = pnd_Ia_Jump_Key__R_Field_2.newFieldInGroup("pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde", "#IA-RCRD-CDE", FieldType.STRING, 
            2);
        pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde = pnd_Ia_Jump_Key__R_Field_2.newFieldInGroup("pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde", "#IA-PRDCT-CDE", FieldType.STRING, 
            10);
        pnd_Da_Add_Jumper_Count = localVariables.newFieldInRecord("pnd_Da_Add_Jumper_Count", "#DA-ADD-JUMPER-COUNT", FieldType.NUMERIC, 5);
        pnd_Ia_Add_Jumper_Count = localVariables.newFieldInRecord("pnd_Ia_Add_Jumper_Count", "#IA-ADD-JUMPER-COUNT", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_da_Ilog_Curr_Jumper.reset();
        vw_da_Ilog_Next_Jumper.reset();
        vw_ia_Ilog_Curr_Jumper.reset();
        vw_ia_Ilog_Next_Jumper.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb2075() throws Exception
    {
        super("Appb2075");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 80 PS = 55;//Natural: FORMAT ( 2 ) LS = 80 PS = 55;//Natural: FORMAT ( 3 ) LS = 80 PS = 55
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 17X 'NEXT ALPHA RANGE ERROR REPORT' 15X *DATX ( EM = MM/DD/YYYY ) / 'PRODUCT    RANGE    RANGE' / 'CODE       START    END      ERROR MESSAGE' / '-' ( 7 ) 4X '-' ( 8 ) '-' ( 8 ) '-' ( 50 ) /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *PROGRAM 19X 'NEXT DA RANGE ADDED REPORT' 16X *DATX ( EM = MM/DD/YYYY ) /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT *PROGRAM 19X 'NEXT IA RANGE ADDED REPORT' 16X *DATX ( EM = MM/DD/YYYY ) /
        //* *--------------------------------------------------------------------**
        //* * READ DA ILOG RECORD                                                **
        //* *--------------------------------------------------------------------**
        vw_da_Ilog_Curr_Jumper.startDatabaseFind                                                                                                                          //Natural: FIND DA-ILOG-CURR-JUMPER WITH RCRD-NME = 'CURRENT'
        (
        "FIND01",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", WcType.WITH) }
        );
        FIND01:
        while (condition(vw_da_Ilog_Curr_Jumper.readNextRow("FIND01")))
        {
            vw_da_Ilog_Curr_Jumper.setIfNotFoundControlFlag(false);
            if (condition(da_Ilog_Curr_Jumper_Rcrd_Cde.notEquals("JU")))                                                                                                  //Natural: REJECT IF RCRD-CDE NE 'JU'
            {
                continue;
            }
            //* *****************************************************
            //*    CHECK TO SEE IF THERE IS A NEXT AVAILABLE RANGE  *
            //* *****************************************************
            pnd_Da_Jump_Key_Pnd_Da_Start_Dte.setValue(da_Ilog_Curr_Jumper_Start_Dte);                                                                                     //Natural: ASSIGN #DA-START-DTE := START-DTE
            pnd_Da_Jump_Key_Pnd_Da_Rcrd_Cde.setValue(da_Ilog_Curr_Jumper_Rcrd_Cde);                                                                                       //Natural: ASSIGN #DA-RCRD-CDE := RCRD-CDE
            pnd_Da_Jump_Key_Pnd_Da_Rcrd_Nme.setValue("NEXT");                                                                                                             //Natural: ASSIGN #DA-RCRD-NME := 'NEXT'
            pnd_Da_Jump_Key_Pnd_Da_Prdct_Cde.setValue(da_Ilog_Curr_Jumper_Prdct_Cde);                                                                                     //Natural: ASSIGN #DA-PRDCT-CDE := PRDCT-CDE
            vw_da_Ilog_Next_Jumper.getTotalRowCount                                                                                                                       //Natural: FIND NUMBER DA-ILOG-NEXT-JUMPER WITH RCRD-KEY = #DA-JUMP-KEY
            (
            new Wc[] { new Wc("RCRD_KEY", "=", pnd_Da_Jump_Key, WcType.WITH) }
            );
            if (condition(vw_da_Ilog_Next_Jumper.getAstNUMBER().equals(getZero())))                                                                                       //Natural: IF *NUMBER ( ##L1200. ) = 0
            {
                pdaAppa2075.getPnd_Appa2075().reset();                                                                                                                    //Natural: RESET #APPA2075
                pdaAppa2075.getPnd_Appa2075_Pnd_P_Product_Code().setValue(da_Ilog_Curr_Jumper_Prdct_Cde);                                                                 //Natural: MOVE DA-ILOG-CURR-JUMPER.PRDCT-CDE TO #APPA2075.#P-PRODUCT-CODE
                pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract().setValue(da_Ilog_Curr_Jumper_Cntrct_Range_Start);                                                        //Natural: MOVE DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START TO #APPA2075.#P-BEG-CONTRACT
                pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract().setValue(da_Ilog_Curr_Jumper_Cntrct_Range_End);                                                          //Natural: MOVE DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END TO #APPA2075.#P-END-CONTRACT
                pdaAppa2075.getPnd_Appa2075_Pnd_P_Last_Used().setValue(da_Ilog_Curr_Jumper_Cntrct_Last_Used);                                                             //Natural: MOVE DA-ILOG-CURR-JUMPER.CNTRCT-LAST-USED TO #APPA2075.#P-LAST-USED
                //*     *****************************************************************
                //*     * FOR PURPOSES OF THE NEXT ALPHA RANGE SERIES                   *
                //*     * CHANGE BEG CONTRACT ALPHA VALUE TO SAME AS END CONTRACT ALPHA *
                //*     *****************************************************************
                short decideConditionsMet179 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #P-BEG-POS-5 = MASK ( A )
                if (condition(DbsUtil.maskMatches(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_5(),"A")))
                {
                    decideConditionsMet179++;
                    pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_5_7().setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Pos_5_7());                                            //Natural: ASSIGN #P-BEG-POS-5-7 := #P-END-POS-5-7
                }                                                                                                                                                         //Natural: WHEN #P-BEG-POS-6 = MASK ( A )
                else if (condition(DbsUtil.maskMatches(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_6(),"A")))
                {
                    decideConditionsMet179++;
                    pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_6_7().setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Pos_6_7());                                            //Natural: ASSIGN #P-BEG-POS-6-7 := #P-END-POS-6-7
                }                                                                                                                                                         //Natural: WHEN #P-BEG-POS-7 = MASK ( A )
                else if (condition(DbsUtil.maskMatches(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_7(),"A")))
                {
                    decideConditionsMet179++;
                    pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_7().setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Pos_7());                                                //Natural: ASSIGN #P-BEG-POS-7 := #P-END-POS-7
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                DbsUtil.callnat(Appn2075.class , getCurrentProcessState(), pdaAppa2075.getPnd_Appa2075());                                                                //Natural: CALLNAT 'APPN2075' #APPA2075
                if (condition(Global.isEscape())) return;
                if (condition(pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Code().equals(getZero())))                                                                         //Natural: IF #P-RETURN-CODE = 0
                {
                    vw_da_Ilog_Next_Jumper.setValuesByName(vw_da_Ilog_Curr_Jumper);                                                                                       //Natural: MOVE BY NAME DA-ILOG-CURR-JUMPER TO DA-ILOG-NEXT-JUMPER
                    da_Ilog_Next_Jumper_Rcrd_Nme.setValue("NEXT");                                                                                                        //Natural: ASSIGN DA-ILOG-NEXT-JUMPER.RCRD-NME := 'NEXT'
                    da_Ilog_Next_Jumper_Cntrct_Range_Start.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract());                                                    //Natural: MOVE #APPA2075.#P-BEG-CONTRACT TO DA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-START
                    da_Ilog_Next_Jumper_Cntrct_Range_End.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract());                                                      //Natural: MOVE #APPA2075.#P-END-CONTRACT TO DA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-END
                    da_Ilog_Next_Jumper_Cntrct_Last_Used.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_Last_Used());                                                         //Natural: MOVE #APPA2075.#P-LAST-USED TO DA-ILOG-NEXT-JUMPER.CNTRCT-LAST-USED
                    vw_da_Ilog_Next_Jumper.insertDBRow();                                                                                                                 //Natural: STORE DA-ILOG-NEXT-JUMPER
                    //*  JB1
                    pnd_Da_Add_Jumper_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO #DA-ADD-JUMPER-COUNT
                    getReports().display(2, "/PRODUCT/CODE",                                                                                                              //Natural: DISPLAY ( 2 ) '/PRODUCT/CODE' DA-ILOG-CURR-JUMPER.PRDCT-CDE 'CURRENT/CONTRACT/START' DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START 'CURRENT/CONTRACT/END' DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END 'CURRENT/LAST/USED' DA-ILOG-CURR-JUMPER.CNTRCT-LAST-USED 'NEXT/CONTRACT/START' DA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-START 'NEXT/CONTRACT/END' DA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-END 'NEXT/LAST/USED' DA-ILOG-NEXT-JUMPER.CNTRCT-LAST-USED
                    		da_Ilog_Curr_Jumper_Prdct_Cde,"CURRENT/CONTRACT/START",
                    		da_Ilog_Curr_Jumper_Cntrct_Range_Start,"CURRENT/CONTRACT/END",
                    		da_Ilog_Curr_Jumper_Cntrct_Range_End,"CURRENT/LAST/USED",
                    		da_Ilog_Curr_Jumper_Cntrct_Last_Used,"NEXT/CONTRACT/START",
                    		da_Ilog_Next_Jumper_Cntrct_Range_Start,"NEXT/CONTRACT/END",
                    		da_Ilog_Next_Jumper_Cntrct_Range_End,"NEXT/LAST/USED",
                    		da_Ilog_Next_Jumper_Cntrct_Last_Used);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, da_Ilog_Curr_Jumper_Prdct_Cde,new ColumnSpacing(7),da_Ilog_Curr_Jumper_Cntrct_Range_Start, new AlphanumericLength               //Natural: WRITE ( 1 ) DA-ILOG-CURR-JUMPER.PRDCT-CDE 7X DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START ( AL = 8 ) DA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END ( AL = 8 ) #P-RETURN-MESSAGE ( AL = 50 )
                        (8),da_Ilog_Curr_Jumper_Cntrct_Range_End, new AlphanumericLength (8),pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Message(), new AlphanumericLength 
                        (50));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  JB1
        if (condition(pnd_Da_Add_Jumper_Count.equals(getZero())))                                                                                                         //Natural: IF #DA-ADD-JUMPER-COUNT = 0
        {
            //*  JB1
            getReports().write(2, "NO MISSING DA NEXT JUMPER RECORDS");                                                                                                   //Natural: WRITE ( 2 ) 'NO MISSING DA NEXT JUMPER RECORDS'
            if (Global.isEscape()) return;
            //*  JB1
        }                                                                                                                                                                 //Natural: END-IF
        //*  JB1
        //*  JB1
        getReports().write(2, NEWLINE,new ColumnSpacing(29),"*** END OF REPORT ***",NEWLINE,NEWLINE,"DA NEXT JUMPERS ADDED:",pnd_Da_Add_Jumper_Count,                     //Natural: WRITE ( 2 ) / 29X '*** END OF REPORT ***' // 'DA NEXT JUMPERS ADDED:' #DA-ADD-JUMPER-COUNT ( EM = ZZZZ9 )
            new ReportEditMask ("ZZZZ9"));
        if (Global.isEscape()) return;
        //* *--------------------------------------------------------------------**
        //* * READ IA ILOG RECORD                                                **
        //* *--------------------------------------------------------------------**
        vw_ia_Ilog_Curr_Jumper.startDatabaseFind                                                                                                                          //Natural: FIND IA-ILOG-CURR-JUMPER WITH RCRD-NME = 'CURRENT'
        (
        "FIND02",
        new Wc[] { new Wc("RCRD_NME", "=", "CURRENT", WcType.WITH) }
        );
        FIND02:
        while (condition(vw_ia_Ilog_Curr_Jumper.readNextRow("FIND02")))
        {
            vw_ia_Ilog_Curr_Jumper.setIfNotFoundControlFlag(false);
            if (condition(ia_Ilog_Curr_Jumper_Rcrd_Cde.notEquals("JU")))                                                                                                  //Natural: REJECT IF RCRD-CDE NE 'JU'
            {
                continue;
            }
            //* *****************************************************
            //*    CHECK TO SEE IF THERE IS A NEXT AVAILABLE RANGE  *
            //* *****************************************************
            pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Nme.setValue("NEXT");                                                                                                             //Natural: ASSIGN #IA-RCRD-NME := 'NEXT'
            pnd_Ia_Jump_Key_Pnd_Ia_Rcrd_Cde.setValue(ia_Ilog_Curr_Jumper_Rcrd_Cde);                                                                                       //Natural: ASSIGN #IA-RCRD-CDE := RCRD-CDE
            pnd_Ia_Jump_Key_Pnd_Ia_Prdct_Cde.setValue(ia_Ilog_Curr_Jumper_Prdct_Cde);                                                                                     //Natural: ASSIGN #IA-PRDCT-CDE := PRDCT-CDE
            vw_ia_Ilog_Next_Jumper.getTotalRowCount                                                                                                                       //Natural: FIND NUMBER IA-ILOG-NEXT-JUMPER WITH JUMP-KEY = #IA-JUMP-KEY
            (
            new Wc[] { new Wc("JUMP_KEY", "=", pnd_Ia_Jump_Key, WcType.WITH) }
            );
            if (condition(vw_ia_Ilog_Next_Jumper.getAstNUMBER().equals(getZero())))                                                                                       //Natural: IF *NUMBER ( ##L1990. ) = 0
            {
                pdaAppa2075.getPnd_Appa2075().reset();                                                                                                                    //Natural: RESET #APPA2075
                pdaAppa2075.getPnd_Appa2075_Pnd_P_Product_Code().setValue(ia_Ilog_Curr_Jumper_Prdct_Cde);                                                                 //Natural: MOVE IA-ILOG-CURR-JUMPER.PRDCT-CDE TO #APPA2075.#P-PRODUCT-CODE
                pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract().setValue(ia_Ilog_Curr_Jumper_Cntrct_Range_Start);                                                        //Natural: MOVE IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START TO #APPA2075.#P-BEG-CONTRACT
                pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract().setValue(ia_Ilog_Curr_Jumper_Cntrct_Range_End);                                                          //Natural: MOVE IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END TO #APPA2075.#P-END-CONTRACT
                pdaAppa2075.getPnd_Appa2075_Pnd_P_Last_Used().setValue(ia_Ilog_Curr_Jumper_Cntrct_Last_Used);                                                             //Natural: MOVE IA-ILOG-CURR-JUMPER.CNTRCT-LAST-USED TO #APPA2075.#P-LAST-USED
                //*     *****************************************************************
                //*     * FOR PURPOSES OF THE NEXT ALPHA RANGE SERIES                   *
                //*     * CHANGE BEG CONTRACT ALPHA VALUE TO SAME AS END CONTRACT ALPHA *
                //*     *****************************************************************
                short decideConditionsMet246 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #P-BEG-POS-5 = MASK ( A )
                if (condition(DbsUtil.maskMatches(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_5(),"A")))
                {
                    decideConditionsMet246++;
                    pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_5_7().setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Pos_5_7());                                            //Natural: ASSIGN #P-BEG-POS-5-7 := #P-END-POS-5-7
                }                                                                                                                                                         //Natural: WHEN #P-BEG-POS-6 = MASK ( A )
                else if (condition(DbsUtil.maskMatches(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_6(),"A")))
                {
                    decideConditionsMet246++;
                    pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_6_7().setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Pos_6_7());                                            //Natural: ASSIGN #P-BEG-POS-6-7 := #P-END-POS-6-7
                }                                                                                                                                                         //Natural: WHEN #P-BEG-POS-7 = MASK ( A )
                else if (condition(DbsUtil.maskMatches(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_7(),"A")))
                {
                    decideConditionsMet246++;
                    pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Pos_7().setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Pos_7());                                                //Natural: ASSIGN #P-BEG-POS-7 := #P-END-POS-7
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                DbsUtil.callnat(Appn2075.class , getCurrentProcessState(), pdaAppa2075.getPnd_Appa2075());                                                                //Natural: CALLNAT 'APPN2075' #APPA2075
                if (condition(Global.isEscape())) return;
                if (condition(pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Code().equals(getZero())))                                                                         //Natural: IF #P-RETURN-CODE = 0
                {
                    vw_ia_Ilog_Next_Jumper.setValuesByName(vw_ia_Ilog_Curr_Jumper);                                                                                       //Natural: MOVE BY NAME IA-ILOG-CURR-JUMPER TO IA-ILOG-NEXT-JUMPER
                    ia_Ilog_Next_Jumper_Rcrd_Nme.setValue("NEXT");                                                                                                        //Natural: ASSIGN IA-ILOG-NEXT-JUMPER.RCRD-NME := 'NEXT'
                    ia_Ilog_Next_Jumper_Cntrct_Range_Start.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_Beg_Contract());                                                    //Natural: MOVE #APPA2075.#P-BEG-CONTRACT TO IA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-START
                    ia_Ilog_Next_Jumper_Cntrct_Range_End.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_End_Contract());                                                      //Natural: MOVE #APPA2075.#P-END-CONTRACT TO IA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-END
                    ia_Ilog_Next_Jumper_Cntrct_Last_Used.setValue(pdaAppa2075.getPnd_Appa2075_Pnd_P_Last_Used());                                                         //Natural: MOVE #APPA2075.#P-LAST-USED TO IA-ILOG-NEXT-JUMPER.CNTRCT-LAST-USED
                    vw_ia_Ilog_Next_Jumper.insertDBRow();                                                                                                                 //Natural: STORE IA-ILOG-NEXT-JUMPER
                    //*  JB1
                    pnd_Ia_Add_Jumper_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO #IA-ADD-JUMPER-COUNT
                    getReports().display(3, "/PRODUCT/CODE",                                                                                                              //Natural: DISPLAY ( 3 ) '/PRODUCT/CODE' IA-ILOG-CURR-JUMPER.PRDCT-CDE 'CURRENT/CONTRACT/START' IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START 'CURRENT/CONTRACT/END' IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END 'CURRENT/LAST/USED' IA-ILOG-CURR-JUMPER.CNTRCT-LAST-USED 'NEXT/CONTRACT/START' IA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-START 'NEXT/CONTRACT/END' IA-ILOG-NEXT-JUMPER.CNTRCT-RANGE-END 'NEXT/LAST/USED' IA-ILOG-NEXT-JUMPER.CNTRCT-LAST-USED
                    		ia_Ilog_Curr_Jumper_Prdct_Cde,"CURRENT/CONTRACT/START",
                    		ia_Ilog_Curr_Jumper_Cntrct_Range_Start,"CURRENT/CONTRACT/END",
                    		ia_Ilog_Curr_Jumper_Cntrct_Range_End,"CURRENT/LAST/USED",
                    		ia_Ilog_Curr_Jumper_Cntrct_Last_Used,"NEXT/CONTRACT/START",
                    		ia_Ilog_Next_Jumper_Cntrct_Range_Start,"NEXT/CONTRACT/END",
                    		ia_Ilog_Next_Jumper_Cntrct_Range_End,"NEXT/LAST/USED",
                    		ia_Ilog_Next_Jumper_Cntrct_Last_Used);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(1, ia_Ilog_Curr_Jumper_Prdct_Cde,ia_Ilog_Curr_Jumper_Cntrct_Range_Start, new AlphanumericLength (8),ia_Ilog_Curr_Jumper_Cntrct_Range_End,  //Natural: WRITE ( 1 ) IA-ILOG-CURR-JUMPER.PRDCT-CDE IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-START ( AL = 8 ) IA-ILOG-CURR-JUMPER.CNTRCT-RANGE-END ( AL = 8 ) #P-RETURN-MESSAGE ( AL = 50 )
                        new AlphanumericLength (8),pdaAppa2075.getPnd_Appa2075_Pnd_P_Return_Message(), new AlphanumericLength (50));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  JB1
        if (condition(pnd_Ia_Add_Jumper_Count.equals(getZero())))                                                                                                         //Natural: IF #IA-ADD-JUMPER-COUNT = 0
        {
            //*  JB1
            getReports().write(3, "NO MISSING IA NEXT JUMPER RECORDS");                                                                                                   //Natural: WRITE ( 3 ) 'NO MISSING IA NEXT JUMPER RECORDS'
            if (Global.isEscape()) return;
            //*  JB1
        }                                                                                                                                                                 //Natural: END-IF
        //*  JB1
        //*  JB1
        getReports().write(3, NEWLINE,new ColumnSpacing(29),"*** END OF REPORT ***",NEWLINE,NEWLINE,"IA NEXT JUMPERS ADDED:",pnd_Ia_Add_Jumper_Count,                     //Natural: WRITE ( 3 ) / 29X '*** END OF REPORT ***' // 'IA NEXT JUMPERS ADDED:' #IA-ADD-JUMPER-COUNT ( EM = ZZZZ9 )
            new ReportEditMask ("ZZZZ9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=55");
        Global.format(2, "LS=80 PS=55");
        Global.format(3, "LS=80 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(17),"NEXT ALPHA RANGE ERROR REPORT",new 
            ColumnSpacing(15),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"PRODUCT    RANGE    RANGE",NEWLINE,"CODE       START    END      ERROR MESSAGE",NEWLINE,"-",new 
            RepeatItem(7),new ColumnSpacing(4),"-",new RepeatItem(8),"-",new RepeatItem(8),"-",new RepeatItem(50),NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(19),"NEXT DA RANGE ADDED REPORT",new 
            ColumnSpacing(16),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(19),"NEXT IA RANGE ADDED REPORT",new 
            ColumnSpacing(16),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE);

        getReports().setDisplayColumns(2, "/PRODUCT/CODE",
        		da_Ilog_Curr_Jumper_Prdct_Cde,"CURRENT/CONTRACT/START",
        		da_Ilog_Curr_Jumper_Cntrct_Range_Start,"CURRENT/CONTRACT/END",
        		da_Ilog_Curr_Jumper_Cntrct_Range_End,"CURRENT/LAST/USED",
        		da_Ilog_Curr_Jumper_Cntrct_Last_Used,"NEXT/CONTRACT/START",
        		da_Ilog_Next_Jumper_Cntrct_Range_Start,"NEXT/CONTRACT/END",
        		da_Ilog_Next_Jumper_Cntrct_Range_End,"NEXT/LAST/USED",
        		da_Ilog_Next_Jumper_Cntrct_Last_Used);
        getReports().setDisplayColumns(3, "/PRODUCT/CODE",
        		ia_Ilog_Curr_Jumper_Prdct_Cde,"CURRENT/CONTRACT/START",
        		ia_Ilog_Curr_Jumper_Cntrct_Range_Start,"CURRENT/CONTRACT/END",
        		ia_Ilog_Curr_Jumper_Cntrct_Range_End,"CURRENT/LAST/USED",
        		ia_Ilog_Curr_Jumper_Cntrct_Last_Used,"NEXT/CONTRACT/START",
        		ia_Ilog_Next_Jumper_Cntrct_Range_Start,"NEXT/CONTRACT/END",
        		ia_Ilog_Next_Jumper_Cntrct_Range_End,"NEXT/LAST/USED",
        		ia_Ilog_Next_Jumper_Cntrct_Last_Used);
    }
}
