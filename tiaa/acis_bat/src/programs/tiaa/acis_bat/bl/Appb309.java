/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:21 PM
**        * FROM NATURAL PROGRAM : Appb309
************************************************************
**        * FILE NAME            : Appb309.java
**        * CLASS NAME           : Appb309
**        * INSTANCE NAME        : Appb309
************************************************************
************************************************************************
* PROGRAM  : APPB309 - PRAP OVERDUE LETTER FACILITY                    *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : JULY 28, 1999                                             *
* FUNCTION : READS WORK FILE CREATED BY APPB302/APPB303 AND FOR OVERDUE*
*            AND CREATES A REPORT FOR MONTHLY AND QUARTERLY RUNS.  IT  *
*            ALSO CREATES REPORTS FOR FOR THOSE APPS OVER 180 DAYS OR  *
*            PREMIUMS OVER 90 DAYS.                                    *
*
* REPORT 1 - REPORT BY INSTITUTION LINK CODE (WHEN CV-MAIL-IND EQ Y)
* REPORT 2 - REPORT BY PPG TEAM CODE
* REPORT 3 - REPORT BY INSTITUTION LINK CODE (WHEN CV-MAIL-IND EQ N)
*
*
* UPDATED  : 09/05/00 V.RAQUENO - USE SINGLE FUNCTION TO CALL IIS(VR1) *
*          : 08/01/01 K.GATES   - #DAYSAFTER FIELD NOW POPULATED BY    *
*                                 CV-DAY-DIFF INSTEAD OF CV-STATUS     *
*                                 FIELD.  CV-STATUS WAS REPLACED IN    *
*                                 SORT STATEMENT BY CV-DAY-DIFF.       *
*          : 11/08/01 K.GATES   - 457(B) PROJECT CHANGES SEE 457(B) KG *
*          : 04/25/04 K.GATES     SUNGARD RELEASE 2  SEE SGRD KG       *
*                               - EXCLUDED SUNGARD CONTRACTS FROM IIS  *
*                                 CALL. INSTEAD HARDCODED CONTACT INFO.*
*                               - ADDED PLAN NUMBER TO REPORT.         *
*          : 08/27/04 R.WILLIS  - CHANGES FOR SUNGARD REL 3.  ADDED
*                                 CALL TO OMNI CONTACT API AND ADAPTED
*                                 REPORT TO HANDLE SUNGARD CONTRACTS
*
*          : 10/05/04 R.WILLIS  - FIELDS WERE NOT DISPLAYING CORRECTLY
*                                 WHEN FIELDS WERE MOVED USING COMBO
*                                 OF SUBSTRING AND TEXT IN QUOTES. MOVED
*                                 TEXT TO FIELDS AND USED COMPRESS.
*          : 12/12/04 R.WILLIS  - TNT/REL4. CHANGES RW1
*          : 03/22/05 R.WILLIS  - ICAP/REL5. CHANGES  RW2
*          : 04/07/05 K.GATES   - ICAP/REL5. CONTACT CHANGES  SGRD5 KG
*          : 09/09/05 D.MARPURI - IRAS/REL6. INCLUDE IRA SEP/LOB/LOB TYP
*          : 09/27/06 K.GATES   - ADDED DIV/SUB CHANGES FOR DCA  DCA KG
*          : 08/28/08 N.CVETKOVIC-RHSP CHANGES NBC
*          : 03/10/10 C.AVE     - INCLUDED IRA INDEXED PRODUCTS - TIGR
*          : 05/10/17 MUKHR     - PIN EXPANSION CHANGES.
*                                 STOW ONLY (C425939)
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb309 extends BLNatBase
{
    // Data Areas
    private PdaIisa8000 pdaIisa8000;
    private LdaAppl302 ldaAppl302;
    private PdaScia8200 pdaScia8200;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Full_Name;

    private DbsGroup pnd_Report_Fields__R_Field_1;
    private DbsField pnd_Report_Fields_Pnd_F_M_L;
    private DbsField pnd_Report_Fields_Pnd_Overflow;
    private DbsField pnd_Report_Fields_Pnd_Contract;
    private DbsField pnd_Report_Fields_Pnd_Daysafter;
    private DbsField pnd_Report_Fields_Pnd_Cntrtype_Nbr;
    private DbsField pnd_Report_Fields_Pnd_Ppg_Plan;
    private DbsField pnd_Report_Fields_Pnd_Div_Sub;
    private DbsField pnd_Title;
    private DbsField pnd_Title2;
    private DbsField pnd_Title3;
    private DbsField pnd_Title4;
    private DbsField pnd_Comment_1;
    private DbsField pnd_Comment_2;
    private DbsField pnd_Be_Refunded;
    private DbsField pnd_Date_Will_Be;
    private DbsField pnd_Date;
    private DbsField pnd_First;
    private DbsField pnd_First2;
    private DbsField pnd_First3;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Withdraw;
    private DbsField pnd_Counters_Pnd_Total_Withdraw;
    private DbsField pnd_Sgrd;
    private DbsField pnd_Hold_Plan;
    private DbsField pnd_Sgrd_Call_Cnt;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_File_Open_Sw;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Cv_KeyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaIisa8000 = new PdaIisa8000(localVariables);
        ldaAppl302 = new LdaAppl302();
        registerRecord(ldaAppl302);
        pdaScia8200 = new PdaScia8200(localVariables);

        // Local Variables

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Full_Name = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 90);

        pnd_Report_Fields__R_Field_1 = pnd_Report_Fields.newGroupInGroup("pnd_Report_Fields__R_Field_1", "REDEFINE", pnd_Report_Fields_Pnd_Full_Name);
        pnd_Report_Fields_Pnd_F_M_L = pnd_Report_Fields__R_Field_1.newFieldInGroup("pnd_Report_Fields_Pnd_F_M_L", "#F-M-L", FieldType.STRING, 35);
        pnd_Report_Fields_Pnd_Overflow = pnd_Report_Fields__R_Field_1.newFieldInGroup("pnd_Report_Fields_Pnd_Overflow", "#OVERFLOW", FieldType.STRING, 
            55);
        pnd_Report_Fields_Pnd_Contract = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Contract", "#CONTRACT", FieldType.STRING, 8);
        pnd_Report_Fields_Pnd_Daysafter = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Daysafter", "#DAYSAFTER", FieldType.STRING, 3);
        pnd_Report_Fields_Pnd_Cntrtype_Nbr = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Cntrtype_Nbr", "#CNTRTYPE-NBR", FieldType.STRING, 
            15);
        pnd_Report_Fields_Pnd_Ppg_Plan = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Ppg_Plan", "#PPG-PLAN", FieldType.STRING, 6);
        pnd_Report_Fields_Pnd_Div_Sub = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Div_Sub", "#DIV-SUB", FieldType.STRING, 4);
        pnd_Title = localVariables.newFieldInRecord("pnd_Title", "#TITLE", FieldType.STRING, 30);
        pnd_Title2 = localVariables.newFieldInRecord("pnd_Title2", "#TITLE2", FieldType.STRING, 17);
        pnd_Title3 = localVariables.newFieldInRecord("pnd_Title3", "#TITLE3", FieldType.STRING, 47);
        pnd_Title4 = localVariables.newFieldInRecord("pnd_Title4", "#TITLE4", FieldType.STRING, 22);
        pnd_Comment_1 = localVariables.newFieldInRecord("pnd_Comment_1", "#COMMENT-1", FieldType.STRING, 80);
        pnd_Comment_2 = localVariables.newFieldInRecord("pnd_Comment_2", "#COMMENT-2", FieldType.STRING, 80);
        pnd_Be_Refunded = localVariables.newFieldInRecord("pnd_Be_Refunded", "#BE-REFUNDED", FieldType.STRING, 11);
        pnd_Date_Will_Be = localVariables.newFieldInRecord("pnd_Date_Will_Be", "#DATE-WILL-BE", FieldType.STRING, 12);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 10);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.STRING, 1);
        pnd_First2 = localVariables.newFieldInRecord("pnd_First2", "#FIRST2", FieldType.STRING, 1);
        pnd_First3 = localVariables.newFieldInRecord("pnd_First3", "#FIRST3", FieldType.STRING, 1);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Withdraw = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Withdraw", "#WITHDRAW", FieldType.NUMERIC, 7);
        pnd_Counters_Pnd_Total_Withdraw = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Withdraw", "#TOTAL-WITHDRAW", FieldType.NUMERIC, 7);
        pnd_Sgrd = localVariables.newFieldInRecord("pnd_Sgrd", "#SGRD", FieldType.BOOLEAN, 1);
        pnd_Hold_Plan = localVariables.newFieldInRecord("pnd_Hold_Plan", "#HOLD-PLAN", FieldType.STRING, 6);
        pnd_Sgrd_Call_Cnt = localVariables.newFieldInRecord("pnd_Sgrd_Call_Cnt", "#SGRD-CALL-CNT", FieldType.INTEGER, 4);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.INTEGER, 4);
        pnd_File_Open_Sw = localVariables.newFieldInRecord("pnd_File_Open_Sw", "#FILE-OPEN-SW", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Cv_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cv_Key_OLD", "Cv_Key_OLD", FieldType.STRING, 112);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaAppl302.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Appb309() throws Exception
    {
        super("Appb309");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB309", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 HW = OFF PS = 52 HC = L;//Natural: FORMAT ( 2 ) LS = 132 HW = OFF PS = 52 HC = L;//Natural: FORMAT ( 3 ) LS = 132 HW = OFF PS = 52 HC = L
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 CV-RULE-DATA-L CV-RULE-DATA-M
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M())))
        {
            CheckAtStartofData278();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  08/27/04
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*  SGRD KG
            //*  08/27/04
            //*  08/27/04
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals("SGRD")))                                                                                     //Natural: IF CV-COLL-CODE = 'SGRD'
            {
                //*    CV-INST-LINK-CDE := 11122
                pnd_Sgrd.setValue(true);                                                                                                                                  //Natural: ASSIGN #SGRD := TRUE
                pnd_Hold_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No());                                                                                    //Natural: ASSIGN #HOLD-PLAN := CV-SGRD-PLAN-NO
                //*  SGRD KG
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().equals("N")))                                                                                         //Natural: IF CV-MAIL-IND EQ 'N'
            {
                if (condition(pnd_First3.equals(" ")))                                                                                                                    //Natural: IF #FIRST3 EQ ' '
                {
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).reset();                                                                                          //Natural: RESET #ORG-NME ( 1 )
                                                                                                                                                                          //Natural: PERFORM SEPARATOR-PAGE3
                    sub_Separator_Page3();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM GET-INST-NAME
                    sub_Get_Inst_Name();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_First3.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #FIRST3
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT3
                sub_Write_Report3();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_First.equals(" ")))                                                                                                                     //Natural: IF #FIRST EQ ' '
                {
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).reset();                                                                                          //Natural: RESET #ORG-NME ( 1 )
                                                                                                                                                                          //Natural: PERFORM SEPARATOR-PAGE
                    sub_Separator_Page();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM GET-INST-NAME
                    sub_Get_Inst_Name();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_First.setValue("Y");                                                                                                                                  //Natural: MOVE 'Y' TO #FIRST
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  DCA KG
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF CV-SGRD-DIVSUB
            //*                                                                                                                                                           //Natural: AT BREAK OF CV-COLL-CODE
            //*                                                                                                                                                           //Natural: AT BREAK OF CV-SGRD-PLAN-NO
            //*                                                                                                                                                           //Natural: AT BREAK OF CV-KEY
            readWork01Cv_KeyOld.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Key());                                                                                           //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 1 CV-RULE-DATA-L CV-RULE-DATA-M
        while (condition(getWorkFiles().read(1, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M())))
        {
            getSort().writeSortInData(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type(), ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde(), ldaAppl302.getCv_Prap_Rule1_Cv_Key(),  //Natural: END-ALL
                ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub(), 
                ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(), ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), 
                ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  SGRD KG - ADDED PLAN NUMBER TO SORT
        //*  DCA KG
        getSort().sortData(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type(), ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde(), ldaAppl302.getCv_Prap_Rule1_Cv_Key(),             //Natural: SORT CV-RECORD-TYPE CV-PPG-TEAM-CDE CV-KEY CV-COLL-CODE CV-SGRD-PLAN-NO CV-SGRD-DIVSUB CV-DAY-DIFF DESC CV-COR-LAST-NME CV-SOC-SEC USING CV-RULE-DATA-L CV-RULE-DATA-M
            ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub(), ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(), 
            "DESCENDING", ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec());
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type(), ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde(), ldaAppl302.getCv_Prap_Rule1_Cv_Key(), 
            ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No(), ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub(), ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(), 
            ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(1) && ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().greater(180)))                              //Natural: IF CV-RECORD-TYPE EQ 1 AND CV-DAY-DIFF GT 180
            {
                if (condition(pnd_First2.equals(" ")))                                                                                                                    //Natural: IF #FIRST2 EQ ' '
                {
                                                                                                                                                                          //Natural: PERFORM SEPARATOR-PAGE2
                    sub_Separator_Page2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_First2.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #FIRST2
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT2
                sub_Write_Report2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(2) && ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().greater(90)))                               //Natural: IF CV-RECORD-TYPE EQ 2 AND CV-DAY-DIFF GT 90
            {
                if (condition(pnd_First2.equals(" ")))                                                                                                                    //Natural: IF #FIRST2 EQ ' '
                {
                                                                                                                                                                          //Natural: PERFORM SEPARATOR-PAGE2
                    sub_Separator_Page2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_First2.setValue("Y");                                                                                                                                 //Natural: MOVE 'Y' TO #FIRST2
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT2
                sub_Write_Report2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF CV-PPG-TEAM-CDE
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
            ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde().reset();                                                                                                        //Natural: RESET CV-PPG-TEAM-CDE
            if (condition(pnd_Counters_Pnd_Total_Withdraw.notEquals(getZero())))                                                                                          //Natural: IF #TOTAL-WITHDRAW NE 0
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,"Grand Total for all units:  ",pnd_Counters_Pnd_Total_Withdraw, new ReportEditMask ("Z,ZZZ,ZZ9"));                 //Natural: WRITE ( 2 ) 'Grand Total for all units:  ' #TOTAL-WITHDRAW ( EM = Z,ZZZ,ZZ9 )
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //*  CLOSE FILE - CALL TO OMNI FOR CONTACT INFORMATION
        //*  ----------
        if (condition(pnd_Sgrd_Call_Cnt.greater(getZero())))                                                                                                              //Natural: IF #SGRD-CALL-CNT > 0
        {
            pdaScia8200.getCon_Part_Rec().reset();                                                                                                                        //Natural: RESET CON-PART-REC
            pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("Y");                                                                                                       //Natural: ASSIGN CON-EOF-SW := 'Y'
            //*  SGRD 5 KG
            DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                 //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *==============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATOR-PAGE
        //* *===============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATOR-PAGE2
        //* *===============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATOR-PAGE3
        //* *=============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INST-NAME
        //*  CALL INTERFACE TO OMNI - RETRIEVE COMPANY NAME
        //*  ----------------------------------------------
        //*  #FUNCT-CDE-IN(1)   := '01'
        //*  #FUNCT-CDE-IN(2)   := '02'
        //*  #FUNCT-CDE-IN(3)   := '03'
        //*  #FUNCT-CDE-IN(4)   := '06'
        //*  #FUNCT-CDE-IN(5)   := '07'
        //* *=====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PREPARE-REPORT-FIELDS
        //*  TIAA ISSUE DATE IS NOT PRESENT(0)
        //*  ---------------------------------
        //*  CREF ISSUE DATE IS NOT PRESENT(0)
        //*  ---------------------------------
        //*  BOTH ISSUE DATES ARE PRESENT
        //*  ----------------------------
        //* *============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //* *=============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT2
        //*  'Overdue/Days'             #DAYSAFTER
        //* *=============================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT3
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 57T '_' ( 29 ) / 57T #TITLE // 57T #TITLE2 *DATX ( EM = MM/DD/YYYY ) / 57T '_' ( 29 ) // #ORG-NME ( 1 ) // #COMMENT-1 / #COMMENT-2 // '_' ( 85 )
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *PROGRAM 120T *DATU / 'PAGE' *PAGE-NUMBER ( 2 ) 120T *TIMX // 42T #TITLE3 / 42T #TITLE4 *DATX ( EM = MM/DD/YYYY ) // CV-PPG-TEAM-CDE '-' ( 131 ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT 57T '_' ( 29 ) / 57T #TITLE // 57T #TITLE2 *DATX ( EM = MM/DD/YYYY ) / 57T '_' ( 29 ) // #ORG-NME ( 1 ) // #COMMENT-1 / #COMMENT-2 // '_' ( 85 )
        //* *=====================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Separator_Page() throws Exception                                                                                                                    //Natural: SEPARATOR-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *==============================
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new                 //Natural: WRITE ( 1 ) NOTITLE NOHDR ////////// 15X 'INSTITUTION CODE:' CV-INST-LINK-CDE /
            ColumnSpacing(15),"INSTITUTION CODE:",ldaAppl302.getCv_Prap_Rule1_Cv_Inst_Link_Cde(),NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Separator_Page2() throws Exception                                                                                                                   //Natural: SEPARATOR-PAGE2
    {
        if (BLNatReinput.isReinput()) return;

        //* *===============================
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new                 //Natural: WRITE ( 2 ) NOTITLE NOHDR ////////// 15X 'PPG TEAM CODE :' CV-PPG-TEAM-CDE /
            ColumnSpacing(15),"PPG TEAM CODE :",ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde(),NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Separator_Page3() throws Exception                                                                                                                   //Natural: SEPARATOR-PAGE3
    {
        if (BLNatReinput.isReinput()) return;

        //* *===============================
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
        getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new                 //Natural: WRITE ( 3 ) NOTITLE NOHDR ////////// 15X 'INSTITUTION CODE:' CV-INST-LINK-CDE /
            ColumnSpacing(15),"INSTITUTION CODE:",ldaAppl302.getCv_Prap_Rule1_Cv_Inst_Link_Cde(),NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(3));                                                                                                                 //Natural: NEWPAGE ( 3 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Get_Inst_Name() throws Exception                                                                                                                     //Natural: GET-INST-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *=============================
        pdaIisa8000.getPnd_Ac_Pda().reset();                                                                                                                              //Natural: RESET #AC-PDA
        //* * SGRD KG - EXCLUDED SUNGARD CONTRACTS FROM IIS CALL
        //* *         - HARDCODED CONTACT INFO
        //*  IF CV-COLL-CODE = 'SGRD'                                /* SGRD KG
        //*  #FIRST-NME(1)            := 'ROBERTO '                  /* SGRD KG
        //*  #LAST-NME(1)             := 'TORRES '                   /* SGRD KG
        //*  #COMPRESSED-NME(1)       := 'ROBERTO TORRES'            /* SGRD KG
        //*  #PREFIX-NME(1)           := 'MR.'                       /* SGRD KG
        //*  #TITLE-NME(1)            := 'BENEFITS SPECIALIST'       /* SGRD KG
        //*  #ORG-NME(1)              := 'TIAA-CREF'                 /* SGRD KG
        //*  #ADDRESS-LINE-1(1)       := '730 3RD AVE'               /* SGRD KG
        //*  #ADDRESS-LINE-2(1)       := 'NEW YORK, NY 10017-3206'   /* SGRD KG
        //*  #POSTAL-CDE(1)           := '10017'                     /* SGRD KG
        //*  #ADDRESS-LINE-3(1)       := ' '                         /* SGRD KG
        //*  #ADDRESS-LINE-4(1)       := ' '                         /* SGRD KG
        //*  #ADDRESS-LINE-5(1)       := ' '                         /* SGRD KG
        //*  #ADDRESS-LINE-6(1)       := ' '                         /* SGRD KG
        //*  RELEASE 3 CHANGES                                   /* 08/27/04 >>>
        //*  -----------------
        if (condition(pnd_Sgrd.getBoolean()))                                                                                                                             //Natural: IF #SGRD
        {
            pnd_Sgrd_Call_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #SGRD-CALL-CNT
            //*  SET FILE OPEN SWITCH ON FIRST PASS
            //*  ----------------------------------
            //*  SGRD5 KG
            if (condition(pnd_Sgrd_Call_Cnt.equals(1)))                                                                                                                   //Natural: IF #SGRD-CALL-CNT = 1
            {
                pdaScia8200.getCon_File_Open_Sw().setValue("N");                                                                                                          //Natural: ASSIGN CON-FILE-OPEN-SW := 'N'
                //*  SGRD5 KG
                //*  DCA KG
            }                                                                                                                                                             //Natural: END-IF
            pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("N");                                                                                                       //Natural: ASSIGN CON-EOF-SW := 'N'
            pdaScia8200.getCon_Part_Rec_Con_Function_Code().getValue(1).setValue("08");                                                                                   //Natural: ASSIGN CON-FUNCTION-CODE ( 1 ) := '08'
            pdaScia8200.getCon_Part_Rec_Con_Access_Level().setValue(" ");                                                                                                 //Natural: ASSIGN CON-ACCESS-LEVEL := ' '
            pdaScia8200.getCon_Part_Rec_Con_Contact_Type().setValue("E");                                                                                                 //Natural: ASSIGN CON-CONTACT-TYPE := 'E'
            pdaScia8200.getCon_Part_Rec_Con_Plan_Num().setValue(pnd_Hold_Plan);                                                                                           //Natural: ASSIGN CON-PLAN-NUM := #HOLD-PLAN
            pdaScia8200.getCon_Part_Rec_Con_Part_Divsub().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub());                                                         //Natural: ASSIGN CON-PART-DIVSUB := CV-SGRD-DIVSUB
            //*  SGRD5 KG
            DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                 //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
            if (condition(Global.isEscape())) return;
            if (condition(pdaScia8200.getCon_Part_Rec_Con_Return_Code().notEquals(getZero()) || pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg().notEquals(" ")))        //Natural: IF CON-RETURN-CODE NE 0 OR CON-CONTACT-ERR-MSG NE ' '
            {
                getReports().write(0, "*CALL ERROR (SCIN8200):",pdaScia8200.getCon_Part_Rec_Con_Return_Code(),pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg());         //Natural: WRITE '*CALL ERROR (SCIN8200):' CON-RETURN-CODE CON-CONTACT-ERR-MSG
                if (Global.isEscape()) return;
                getReports().write(0, "PLAN:",pdaScia8200.getCon_Part_Rec_Con_Plan_Num());                                                                                //Natural: WRITE 'PLAN:' CON-PLAN-NUM
                if (Global.isEscape()) return;
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).setValue("**** NAME UNKNOWN ****");                                                                   //Natural: ASSIGN #ORG-NME ( 1 ) := '**** NAME UNKNOWN ****'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Company_Name());                                             //Natural: ASSIGN #ORG-NME ( 1 ) := CON-COMPANY-NAME
            }                                                                                                                                                             //Natural: END-IF
            //*  08/27/04 >>>
            pnd_Sgrd.reset();                                                                                                                                             //Natural: RESET #SGRD #HOLD-PLAN CON-PART-REC
            pnd_Hold_Plan.reset();
            pdaScia8200.getCon_Part_Rec().reset();
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Ppg_Cde_In().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code());                                                              //Natural: ASSIGN #PPG-CDE-IN := CV-COLL-CODE
            DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_Ppg_Cde_In()), new ExamineTranslate(TranslateOption.Upper));                                  //Natural: EXAMINE #PPG-CDE-IN TRANSLATE INTO UPPER CASE
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Type_In().setValue("01");                                                                                               //Natural: ASSIGN #ADDRESS-TYPE-IN := '01'
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Contact_Level().setValue(" ");                                                                                                  //Natural: ASSIGN #CONTACT-LEVEL := ' '
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Funct_Cde_In().getValue(1).setValue("01");                                                                                      //Natural: ASSIGN #FUNCT-CDE-IN ( 1 ) := '01'
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Ext_Contact_Count().setValue(1);                                                                                                //Natural: ASSIGN #EXT-CONTACT-COUNT := 1
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Role_Cde_In().setValue(" ");                                                                                                    //Natural: ASSIGN #ROLE-CDE-IN := ' '
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Int_Contact_Count().setValue(0);                                                                                                //Natural: ASSIGN #INT-CONTACT-COUNT := 0
            DbsUtil.callnat(Iisn8000.class , getCurrentProcessState(), pdaIisa8000.getPnd_Ac_Pda());                                                                      //Natural: CALLNAT 'IISN8000' #AC-PDA
            if (condition(Global.isEscape())) return;
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Prepare_Report_Fields() throws Exception                                                                                                             //Natural: PREPARE-REPORT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *=====================================
        //* * REMOVED CAP OF 91 DAYS ON INTERNAL REPORTS  KG 10/08/01
        //*  IF CV-DAY-DIFF > 91                              /* KG 8/01/01 START
        //*    MOVE 91          TO #DAYSAFTER
        //*  ELSE
        pnd_Report_Fields_Pnd_Daysafter.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff());                                                                              //Natural: MOVE CV-DAY-DIFF TO #DAYSAFTER
        //*  END-IF                                           /* KG 8/01/01 END
        short decideConditionsMet626 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CV-LOB;//Natural: VALUE 'D'
        if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob().equals("D"))))
        {
            decideConditionsMet626++;
            //*  RW1
            //*  RW2
            //*  NBC
            short decideConditionsMet631 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CV-LOB-TYPE;//Natural: VALUE '2'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("2"))))
            {
                decideConditionsMet631++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("RA");                                                                                                        //Natural: MOVE 'RA' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("5"))))
            {
                decideConditionsMet631++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("RS");                                                                                                        //Natural: MOVE 'RS' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("7"))))
            {
                decideConditionsMet631++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("GRA");                                                                                                       //Natural: MOVE 'GRA' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE 'A'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("A"))))
            {
                decideConditionsMet631++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("RC");                                                                                                        //Natural: MOVE 'RC' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("B"))))
            {
                decideConditionsMet631++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("RHSP");                                                                                                      //Natural: MOVE 'RHSP'TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.reset();                                                                                                               //Natural: RESET #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob().equals("I"))))
        {
            decideConditionsMet626++;
            //*  RL6
            //*  TIGR
            //*  TIGR
            //*  TIGR
            short decideConditionsMet650 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CV-LOB-TYPE;//Natural: VALUE '3'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("3"))))
            {
                decideConditionsMet650++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("IRAR");                                                                                                      //Natural: MOVE 'IRAR' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("4"))))
            {
                decideConditionsMet650++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("IRAC");                                                                                                      //Natural: MOVE 'IRAC' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("5"))))
            {
                decideConditionsMet650++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("KEOG");                                                                                                      //Natural: MOVE 'KEOG' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("6"))))
            {
                decideConditionsMet650++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("IRAS");                                                                                                      //Natural: MOVE 'IRAS' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("7"))))
            {
                decideConditionsMet650++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("IRIR");                                                                                                      //Natural: MOVE 'IRIR' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '8'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("8"))))
            {
                decideConditionsMet650++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("IRIC");                                                                                                      //Natural: MOVE 'IRIC' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '9'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("9"))))
            {
                decideConditionsMet650++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("IRIS");                                                                                                      //Natural: MOVE 'IRIS' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.reset();                                                                                                               //Natural: RESET #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob().equals("S"))))
        {
            decideConditionsMet626++;
            //*  457(B) KG
            //*  RW1
            //*  RW1
            //*  RW2
            short decideConditionsMet673 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CV-LOB-TYPE;//Natural: VALUE '2'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("2"))))
            {
                decideConditionsMet673++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("SRA");                                                                                                       //Natural: MOVE 'SRA' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("3"))))
            {
                decideConditionsMet673++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("GSRA");                                                                                                      //Natural: MOVE 'GSRA' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("4"))))
            {
                decideConditionsMet673++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("GA  ");                                                                                                      //Natural: MOVE 'GA  ' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("5"))))
            {
                decideConditionsMet673++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("RSP");                                                                                                       //Natural: MOVE 'RSP' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("6"))))
            {
                decideConditionsMet673++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("RSP2");                                                                                                      //Natural: MOVE 'RSP2' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("7"))))
            {
                decideConditionsMet673++;
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.setValue("RCP");                                                                                                       //Natural: MOVE 'RCP' TO #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Report_Fields_Pnd_Cntrtype_Nbr.reset();                                                                                                               //Natural: RESET #CNTRTYPE-NBR
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Cntrtype_Nbr.reset();                                                                                                                   //Natural: RESET #CNTRTYPE-NBR
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  IF CV-CONT NE ' ' /* EAC (RODGER)                             /* REL5.
        //*   MOVE CV-PREF   TO SUBSTR(#CNTRTYPE-NBR,7,1)                  /* REL5.
        //*   MOVE EDITED CV-CONT (EM=XXXXXX-X) /* EAC (RODGER)            /* REL5.
        //*     TO #CONTRACT /* EAC (RODGER EM FROM 999999-9 TO XXXXXX-X)  /* REL5.
        //*   MOVE #CONTRACT TO SUBSTR(#CNTRTYPE-NBR,8,8)                  /* REL5.
        //*  ELSE                                                          /* REL5.
        //*   MOVE CV-C-PREF TO SUBSTR(#CNTRTYPE-NBR,7,1)                  /* REL5.
        //*   MOVE EDITED CV-C-CONT (EM=XXXXXX-X) /* EAC (RODGER)          /* REL5.
        //*     TO #CONTRACT /* EAC (RODGER EM FROM 999999-9 TO XXXXXX-X)  /* REL5.
        //*   MOVE #CONTRACT TO SUBSTR(#CNTRTYPE-NBR,8,8)                  /* REL5.
        //*  END-IF                                                        /* REL5.
        //*  **********************************************************************
        //*  1. IF TIAA ISSUE DATE IS PRESENT   - USE TIAA CONTRACT#
        //*  2. IF CREF ISSUE DATE IS PRESENT   - USE CREF CONTRACT#
        //*  3. IF BOTH ISSUE DATES ARE PRESENT - USE TIAA CONTRACT#
        //*  **********************************************************************
        //*  REL5.>>
        short decideConditionsMet710 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CV-TIAA-DOI = 0
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Tiaa_Doi().equals(getZero())))
        {
            decideConditionsMet710++;
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_C_Pref(),pnd_Report_Fields_Pnd_Cntrtype_Nbr,7,1);                                                          //Natural: MOVE CV-C-PREF TO SUBSTR ( #CNTRTYPE-NBR,7,1 )
            pnd_Report_Fields_Pnd_Contract.setValueEdited(ldaAppl302.getCv_Prap_Rule1_Cv_C_Cont(),new ReportEditMask("XXXXXX-X"));                                        //Natural: MOVE EDITED CV-C-CONT ( EM = XXXXXX-X ) TO #CONTRACT
            setValueToSubstring(pnd_Report_Fields_Pnd_Contract,pnd_Report_Fields_Pnd_Cntrtype_Nbr,8,8);                                                                   //Natural: MOVE #CONTRACT TO SUBSTR ( #CNTRTYPE-NBR,8,8 )
        }                                                                                                                                                                 //Natural: WHEN CV-CREF-DOI = 0
        else if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Cref_Doi().equals(getZero())))
        {
            decideConditionsMet710++;
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_Pref(),pnd_Report_Fields_Pnd_Cntrtype_Nbr,7,1);                                                            //Natural: MOVE CV-PREF TO SUBSTR ( #CNTRTYPE-NBR,7,1 )
            pnd_Report_Fields_Pnd_Contract.setValueEdited(ldaAppl302.getCv_Prap_Rule1_Cv_Cont(),new ReportEditMask("XXXXXX-X"));                                          //Natural: MOVE EDITED CV-CONT ( EM = XXXXXX-X ) TO #CONTRACT
            setValueToSubstring(pnd_Report_Fields_Pnd_Contract,pnd_Report_Fields_Pnd_Cntrtype_Nbr,8,8);                                                                   //Natural: MOVE #CONTRACT TO SUBSTR ( #CNTRTYPE-NBR,8,8 )
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_Pref(),pnd_Report_Fields_Pnd_Cntrtype_Nbr,7,1);                                                            //Natural: MOVE CV-PREF TO SUBSTR ( #CNTRTYPE-NBR,7,1 )
            pnd_Report_Fields_Pnd_Contract.setValueEdited(ldaAppl302.getCv_Prap_Rule1_Cv_Cont(),new ReportEditMask("XXXXXX-X"));                                          //Natural: MOVE EDITED CV-CONT ( EM = XXXXXX-X ) TO #CONTRACT
            setValueToSubstring(pnd_Report_Fields_Pnd_Contract,pnd_Report_Fields_Pnd_Cntrtype_Nbr,8,8);                                                                   //Natural: MOVE #CONTRACT TO SUBSTR ( #CNTRTYPE-NBR,8,8 )
            //*  REL5. >>
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Report_Fields_Pnd_Full_Name.setValue(DbsUtil.compress(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_First_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Mddle_Nme(),         //Natural: COMPRESS CV-COR-FIRST-NME CV-COR-MDDLE-NME CV-COR-LAST-NME INTO #FULL-NAME
            ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme()));
        if (condition(pnd_Report_Fields_Pnd_Overflow.equals(" ")))                                                                                                        //Natural: IF #OVERFLOW EQ ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_F_M_L.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_First_Nme().getSubstring(1,1));                                                       //Natural: MOVE SUBSTR ( CV-COR-FIRST-NME,1,1 ) TO #F-M-L
            setValueToSubstring(".",pnd_Report_Fields_Pnd_F_M_L,2,1);                                                                                                     //Natural: MOVE '.' TO SUBSTR ( #F-M-L,2,1 )
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Mddle_Nme(),pnd_Report_Fields_Pnd_F_M_L,3,1);                                                          //Natural: MOVE CV-COR-MDDLE-NME TO SUBSTR ( #F-M-L,3,1 )
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(),pnd_Report_Fields_Pnd_F_M_L,5,30);                                                          //Natural: MOVE CV-COR-LAST-NME TO SUBSTR ( #F-M-L,5,30 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Report_Fields_Pnd_Div_Sub.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub());                                                                             //Natural: ASSIGN #DIV-SUB := CV-SGRD-DIVSUB
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *============================
                                                                                                                                                                          //Natural: PERFORM PREPARE-REPORT-FIELDS
        sub_Prepare_Report_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  SGRD KG - ADDED PLAN TO REPORT
        //*  '/PPG'                      CV-COLL-CODE               /* SGRD KG
        //*  SGRD KG
        //*  SGRD KG
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals("SGRD")))                                                                                         //Natural: IF CV-COLL-CODE EQ 'SGRD'
        {
            pnd_Report_Fields_Pnd_Ppg_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No());                                                                       //Natural: ASSIGN #PPG-PLAN := CV-SGRD-PLAN-NO
            //*  SGRD KG
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Ppg_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code());                                                                          //Natural: ASSIGN #PPG-PLAN := CV-COLL-CODE
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRD KG
        //*  DCA KG
        getReports().display(1, "Overdue/Days",                                                                                                                           //Natural: DISPLAY ( 1 ) 'Overdue/Days' #DAYSAFTER 'PPG or/Plan' #PPG-PLAN 'Div or/Sub' #DIV-SUB 'Contract Type/and Number' #CNTRTYPE-NBR 3X '/Employee Name' #F-M-L 'Social Security/Number' CV-SOC-SEC ( EM = 999-99-9999 )
        		pnd_Report_Fields_Pnd_Daysafter,"PPG or/Plan",
        		pnd_Report_Fields_Pnd_Ppg_Plan,"Div or/Sub",
        		pnd_Report_Fields_Pnd_Div_Sub,"Contract Type/and Number",
        		pnd_Report_Fields_Pnd_Cntrtype_Nbr,new ColumnSpacing(3),"/Employee Name",
        		pnd_Report_Fields_Pnd_F_M_L,"Social Security/Number",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
    }
    private void sub_Write_Report2() throws Exception                                                                                                                     //Natural: WRITE-REPORT2
    {
        if (BLNatReinput.isReinput()) return;

        //* *=============================
                                                                                                                                                                          //Natural: PERFORM PREPARE-REPORT-FIELDS
        sub_Prepare_Report_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  SGRD KG - ADDED PLAN TO REPORT
        //*  'PPG'                      CV-COLL-CODE
        //*  SGRD KG
        //*  SGRD KG
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals("SGRD")))                                                                                         //Natural: IF CV-COLL-CODE EQ 'SGRD'
        {
            pnd_Report_Fields_Pnd_Ppg_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No());                                                                       //Natural: ASSIGN #PPG-PLAN := CV-SGRD-PLAN-NO
            //*  SGRD KG
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Ppg_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code());                                                                          //Natural: ASSIGN #PPG-PLAN := CV-COLL-CODE
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  KG 10/04/01
        //*  SGRD KG
        //*  DCA KG
        getReports().display(2, "Days on/System",                                                                                                                         //Natural: DISPLAY ( 2 ) 'Days on/System' CV-DAY-DIFF 'PPG or/Plan' #PPG-PLAN 'Div or/Sub' #DIV-SUB 'Contract Type/and Number' #CNTRTYPE-NBR 'Employee/Name' #F-M-L 'Social/Security Number' CV-SOC-SEC ( EM = 999-99-9999 )
        		ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(),"PPG or/Plan",
        		pnd_Report_Fields_Pnd_Ppg_Plan,"Div or/Sub",
        		pnd_Report_Fields_Pnd_Div_Sub,"Contract Type/and Number",
        		pnd_Report_Fields_Pnd_Cntrtype_Nbr,"Employee/Name",
        		pnd_Report_Fields_Pnd_F_M_L,"Social/Security Number",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
        if (Global.isEscape()) return;
        pnd_Counters_Pnd_Withdraw.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WITHDRAW
    }
    private void sub_Write_Report3() throws Exception                                                                                                                     //Natural: WRITE-REPORT3
    {
        if (BLNatReinput.isReinput()) return;

        //* *=============================
                                                                                                                                                                          //Natural: PERFORM PREPARE-REPORT-FIELDS
        sub_Prepare_Report_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  SGRD KG - ADDED PLAN TO REPORT
        //*  '/PPG'                      CV-COLL-CODE
        //*  SGRD KG
        //*  SGRD KG
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals("SGRD")))                                                                                         //Natural: IF CV-COLL-CODE EQ 'SGRD'
        {
            pnd_Report_Fields_Pnd_Ppg_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No());                                                                       //Natural: ASSIGN #PPG-PLAN := CV-SGRD-PLAN-NO
            //*  SGRD KG
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Ppg_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code());                                                                          //Natural: ASSIGN #PPG-PLAN := CV-COLL-CODE
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRD KG
        //*  DCA KG
        getReports().display(3, "Overdue/Days",                                                                                                                           //Natural: DISPLAY ( 3 ) 'Overdue/Days' #DAYSAFTER 'PPG or/Plan' #PPG-PLAN 'Div or/Sub' #DIV-SUB 'Contract Type/and Number' #CNTRTYPE-NBR 3X '/Employee Name' #F-M-L 'Social Security/Number' CV-SOC-SEC ( EM = 999-99-9999 )
        		pnd_Report_Fields_Pnd_Daysafter,"PPG or/Plan",
        		pnd_Report_Fields_Pnd_Ppg_Plan,"Div or/Sub",
        		pnd_Report_Fields_Pnd_Div_Sub,"Contract Type/and Number",
        		pnd_Report_Fields_Pnd_Cntrtype_Nbr,new ColumnSpacing(3),"/Employee Name",
        		pnd_Report_Fields_Pnd_F_M_L,"Social Security/Number",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
        if (Global.isEscape()) return;
        getReports().skip(3, 1);                                                                                                                                          //Natural: SKIP ( 3 ) 1
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *=====================
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"Error:     ",Global.getERROR_NR());                 //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'Error:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_DivsubIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub().isBreak(endOfData);
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_Coll_CodeIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().isBreak(endOfData);
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No().isBreak(endOfData);
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Key().isBreak(endOfData);
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_DivsubIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_Coll_CodeIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak 
            || ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            //*  DCA KG
            if (condition(pnd_First.equals("Y")))                                                                                                                         //Natural: IF #FIRST EQ 'Y'
            {
                //*  DCA KG
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                //*  DCA KG
            }                                                                                                                                                             //Natural: END-IF
            //*  DCA KG
            if (condition(pnd_First3.equals("Y")))                                                                                                                        //Natural: IF #FIRST3 EQ 'Y'
            {
                //*  DCA KG
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape())){return;}
                //*  DCA KG
            }                                                                                                                                                             //Natural: END-IF
            //*  DCA KG
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_Coll_CodeIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            if (condition(pnd_First.equals("Y")))                                                                                                                         //Natural: IF #FIRST EQ 'Y'
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First3.equals("Y")))                                                                                                                        //Natural: IF #FIRST3 EQ 'Y'
            {
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  08/27/04
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            //*  08/27/04
            if (condition(pnd_First.equals("Y")))                                                                                                                         //Natural: IF #FIRST EQ 'Y'
            {
                //*  08/27/04
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                //*  08/27/04
            }                                                                                                                                                             //Natural: END-IF
            //*  08/27/04
            if (condition(pnd_First3.equals("Y")))                                                                                                                        //Natural: IF #FIRST3 EQ 'Y'
            {
                //*  08/27/04
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape())){return;}
                //*  08/27/04
            }                                                                                                                                                             //Natural: END-IF
            //*  08/27/04
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            getReports().getPageNumberDbs(1).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( 1 ) *PAGE-NUMBER ( 3 ) #ORG-NME ( 1 )
            getReports().getPageNumberDbs(3).reset();
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).reset();
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Key().notEquals(readWork01Cv_KeyOld)))                                                                           //Natural: IF CV-KEY NE OLD ( CV-KEY )
            {
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST #FIRST3
                pnd_First3.reset();
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-INST-NAME
            sub_Get_Inst_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_Ppg_Team_CdeIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde().isBreak(endOfData);
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_Ppg_Team_CdeIsBreak))
        {
            getReports().getPageNumberDbs(2).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( 2 ) #ORG-NME ( 1 ) #FIRST2
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).reset();
            pnd_First2.reset();
            if (condition(pnd_Counters_Pnd_Withdraw.notEquals(getZero())))                                                                                                //Natural: IF #WITHDRAW NE 0
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Total for Unit:  ",pnd_Counters_Pnd_Withdraw, new ReportEditMask ("Z,ZZZ,ZZ9"));              //Natural: WRITE ( 2 ) // 'Total for Unit:  ' #WITHDRAW ( EM = Z,ZZZ,ZZ9 )
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counters_Pnd_Total_Withdraw.nadd(pnd_Counters_Pnd_Withdraw);                                                                                              //Natural: ADD #WITHDRAW TO #TOTAL-WITHDRAW
            pnd_Counters_Pnd_Withdraw.reset();                                                                                                                            //Natural: RESET #WITHDRAW
                                                                                                                                                                          //Natural: PERFORM GET-INST-NAME
            sub_Get_Inst_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 HW=OFF PS=52 HC=L");
        Global.format(2, "LS=132 HW=OFF PS=52 HC=L");
        Global.format(3, "LS=132 HW=OFF PS=52 HC=L");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(57),"_",NEWLINE,new TabSetting(57),pnd_Title,NEWLINE,NEWLINE,new 
            TabSetting(57),pnd_Title2,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(57),"_",NEWLINE,NEWLINE,pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme(),
            NEWLINE,NEWLINE,pnd_Comment_1,NEWLINE,pnd_Comment_2,NEWLINE,NEWLINE,"_");
        getReports().write(2, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(120),Global.getDATU(),NEWLINE,"PAGE",getReports().getPageNumberDbs(2),new 
            TabSetting(120),Global.getTIMX(),NEWLINE,NEWLINE,new TabSetting(42),pnd_Title3,NEWLINE,new TabSetting(42),pnd_Title4,Global.getDATX(), new ReportEditMask 
            ("MM/DD/YYYY"),NEWLINE,NEWLINE,ldaAppl302.getCv_Prap_Rule1_Cv_Ppg_Team_Cde(),"-",new RepeatItem(131),NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(57),"_",NEWLINE,new TabSetting(57),pnd_Title,NEWLINE,NEWLINE,new 
            TabSetting(57),pnd_Title2,Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(57),"_",NEWLINE,NEWLINE,pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme(),
            NEWLINE,NEWLINE,pnd_Comment_1,NEWLINE,pnd_Comment_2,NEWLINE,NEWLINE,"_");

        getReports().setDisplayColumns(1, "Overdue/Days",
        		pnd_Report_Fields_Pnd_Daysafter,"PPG or/Plan",
        		pnd_Report_Fields_Pnd_Ppg_Plan,"Div or/Sub",
        		pnd_Report_Fields_Pnd_Div_Sub,"Contract Type/and Number",
        		pnd_Report_Fields_Pnd_Cntrtype_Nbr,new ColumnSpacing(3),"/Employee Name",
        		pnd_Report_Fields_Pnd_F_M_L,"Social Security/Number",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
        getReports().setDisplayColumns(2, "Days on/System",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff(),"PPG or/Plan",
        		pnd_Report_Fields_Pnd_Ppg_Plan,"Div or/Sub",
        		pnd_Report_Fields_Pnd_Div_Sub,"Contract Type/and Number",
        		pnd_Report_Fields_Pnd_Cntrtype_Nbr,"Employee/Name",
        		pnd_Report_Fields_Pnd_F_M_L,"Social/Security Number",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
        getReports().setDisplayColumns(3, "Overdue/Days",
        		pnd_Report_Fields_Pnd_Daysafter,"PPG or/Plan",
        		pnd_Report_Fields_Pnd_Ppg_Plan,"Div or/Sub",
        		pnd_Report_Fields_Pnd_Div_Sub,"Contract Type/and Number",
        		pnd_Report_Fields_Pnd_Cntrtype_Nbr,new ColumnSpacing(3),"/Employee Name",
        		pnd_Report_Fields_Pnd_F_M_L,"Social Security/Number",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
    }
    private void CheckAtStartofData278() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(1)))                                                                                        //Natural: IF CV-RECORD-TYPE EQ 1
            {
                pnd_Title.setValue("Overdue Premiums");                                                                                                                   //Natural: MOVE 'Overdue Premiums' TO #TITLE
                pnd_Title2.setValue("For Period Ending");                                                                                                                 //Natural: MOVE 'For Period Ending' TO #TITLE2
                pnd_Title3.setValue("PRAP EMPLOYEE APPLICATIONS READY FOR WITHDRAWAL");                                                                                   //Natural: MOVE 'PRAP EMPLOYEE APPLICATIONS READY FOR WITHDRAWAL' TO #TITLE3
                pnd_Title4.setValue("FOR PERIOD ENDING");                                                                                                                 //Natural: MOVE 'FOR PERIOD ENDING' TO #TITLE4
                pnd_Comment_1.setValue("Contracts without premiums received after 180 days from the issue");                                                              //Natural: MOVE 'Contracts without premiums received after 180 days from the issue' TO #COMMENT-1
                //*      MOVE 'date will be' TO SUBSTR(#COMMENT-1,67,12)      /* 10/05/04
                //*  10/05/04
                pnd_Date_Will_Be.setValue("date will be");                                                                                                                //Natural: MOVE 'date will be' TO #DATE-WILL-BE
                //*  10/05/04
                pnd_Comment_1.setValue(DbsUtil.compress(pnd_Comment_1, pnd_Date_Will_Be));                                                                                //Natural: COMPRESS #COMMENT-1 #DATE-WILL-BE INTO #COMMENT-1
                pnd_Comment_2.setValue("cancelled as of");                                                                                                                //Natural: MOVE 'cancelled as of' TO #COMMENT-2
                pnd_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                               //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE
                //*      MOVE #DATE         TO SUBSTR(#COMMENT-2,17,10)       /* 10/05/04
                //*  10/05/04
                pnd_Comment_2.setValue(DbsUtil.compress(pnd_Comment_2, pnd_Date));                                                                                        //Natural: COMPRESS #COMMENT-2 #DATE INTO #COMMENT-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Title.setValue("Overdue Applications");                                                                                                               //Natural: MOVE 'Overdue Applications' TO #TITLE
                pnd_Title2.setValue("For Period Ending");                                                                                                                 //Natural: MOVE 'For Period Ending' TO #TITLE2
                pnd_Title3.setValue("   PRAP EMPLOYEE PREMIUMS READY FOR REFUND");                                                                                        //Natural: MOVE '   PRAP EMPLOYEE PREMIUMS READY FOR REFUND' TO #TITLE3
                pnd_Title4.setValue("   FOR PERIOD ENDING");                                                                                                              //Natural: MOVE '   FOR PERIOD ENDING' TO #TITLE4
                pnd_Comment_1.setValue("Premiums received without a completed application after 90 days will");                                                           //Natural: MOVE 'Premiums received without a completed application after 90 days will' TO #COMMENT-1
                pnd_Be_Refunded.setValue("be refunded");                                                                                                                  //Natural: MOVE 'be refunded' TO #BE-REFUNDED
                //*      MOVE 'be refunded' TO SUBSTR(#COMMENT-1,70,11)       /* 10/05/04
                //*  10/05/04
                pnd_Comment_1.setValue(DbsUtil.compress(pnd_Comment_1, pnd_Be_Refunded));                                                                                 //Natural: COMPRESS #COMMENT-1 #BE-REFUNDED INTO #COMMENT-1
                pnd_Comment_2.setValue("as of");                                                                                                                          //Natural: MOVE 'as of' TO #COMMENT-2
                pnd_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                               //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #DATE
                //*      MOVE #DATE TO SUBSTR(#COMMENT-2,7,10)                /* 10/05/04
                //*  10/05/04
                pnd_Comment_2.setValue(DbsUtil.compress(pnd_Comment_2, pnd_Date));                                                                                        //Natural: COMPRESS #COMMENT-2 #DATE INTO #COMMENT-2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
