/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:19 PM
**        * FROM NATURAL PROGRAM : Appb308
************************************************************
**        * FILE NAME            : Appb308.java
**        * CLASS NAME           : Appb308
**        * INSTANCE NAME        : Appb308
************************************************************
************************************************************************
* PROGRAM  : APPB308                                                   *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : JULY 2, 1999                                              *
* FUNCTION : READS THE ACIS-LETTER-TX-FILE AND DELETES THE REQUESTS ON *
*            A DAILY BASIS AFTER APPB306 AND APPB307 PROCESS COMPLETE. *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb308 extends BLNatBase
{
    // Data Areas
    private LdaAppl306 ldaAppl306;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Et_Ctr;
    private DbsField pnd_Et_Limit;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl306 = new LdaAppl306();
        registerRecord(ldaAppl306);
        registerRecord(ldaAppl306.getVw_request_R());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Et_Ctr = localVariables.newFieldInRecord("pnd_Et_Ctr", "#ET-CTR", FieldType.NUMERIC, 3);
        pnd_Et_Limit = localVariables.newFieldInRecord("pnd_Et_Limit", "#ET-LIMIT", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl306.initializeValues();

        localVariables.reset();
        pnd_Et_Limit.setInitialValue(50);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb308() throws Exception
    {
        super("Appb308");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB308", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52
        ldaAppl306.getVw_request_R().startDatabaseRead                                                                                                                    //Natural: READ REQUEST-R BY REQUEST-ID-KEY
        (
        "PND_PND_L0180",
        new Oc[] { new Oc("REQUEST_ID_KEY", "ASC") }
        );
        PND_PND_L0180:
        while (condition(ldaAppl306.getVw_request_R().readNextRow("PND_PND_L0180")))
        {
            getReports().display(1, "REQUEST/BY",                                                                                                                         //Natural: DISPLAY ( 1 ) 'REQUEST/BY' UPDATE-BY 'RQST DATE/TIME' UPDATE-DATE ( EM = 9999/99/99 ) / '/' UPDATE-TIME ( EM = 99:99:99:9 ) 'PPG' PPG-CODE 'REQUEST/TEAM' REQUEST-TEAM 'REQ/TYP' REQUESTED-LETTER-TYPE 'PULL/CODE' PULLOUT-CODE 'TEAM/CODE' IIS-TEAM-CODE 'APP/PREM' APP-PREM-RQST 'INSTN/CODE' INSTN-LINK-CDE 'SYSTEM/CODE' SYSTEM-LINK-CDE 'PUBLIC/CODE' PUBLIC-LINK-CDE 'MIT/DTE-TME' MIT-RQST-LOG-DTE-TME ( 1:2 ) 'MIT/OP CODE' MIT-RQST-LOG-OPRTR-CDE ( 1:2 ) 'MIT/UNIT CDE' MIT-ORIGINAL-UNIT-CDE ( 1:2 ) 'MIT/STEP ID' MIT-STEP-ID ( 1:2 ) 'MIT/STATUS' MIT-STATUS-CDE ( 1:2 )
            		ldaAppl306.getRequest_R_Update_By(),"RQST DATE/TIME",
            		ldaAppl306.getRequest_R_Update_Date(), new ReportEditMask ("9999/99/99"),NEWLINE,"/",
            		ldaAppl306.getRequest_R_Update_Time(), new ReportEditMask ("99:99:99:9"),"PPG",
            		ldaAppl306.getRequest_R_Ppg_Code(),"REQUEST/TEAM",
            		ldaAppl306.getRequest_R_Request_Team(),"REQ/TYP",
            		ldaAppl306.getRequest_R_Requested_Letter_Type(),"PULL/CODE",
            		ldaAppl306.getRequest_R_Pullout_Code(),"TEAM/CODE",
            		ldaAppl306.getRequest_R_Iis_Team_Code(),"APP/PREM",
            		ldaAppl306.getRequest_R_App_Prem_Rqst(),"INSTN/CODE",
            		ldaAppl306.getRequest_R_Instn_Link_Cde(),"SYSTEM/CODE",
            		ldaAppl306.getRequest_R_System_Link_Cde(),"PUBLIC/CODE",
            		ldaAppl306.getRequest_R_Public_Link_Cde(),"MIT/DTE-TME",
            		ldaAppl306.getRequest_R_Mit_Rqst_Log_Dte_Tme().getValue(1,":",2),"MIT/OP CODE",
            		ldaAppl306.getRequest_R_Mit_Rqst_Log_Oprtr_Cde().getValue(1,":",2),"MIT/UNIT CDE",
            		ldaAppl306.getRequest_R_Mit_Original_Unit_Cde().getValue(1,":",2),"MIT/STEP ID",
            		ldaAppl306.getRequest_R_Mit_Step_Id().getValue(1,":",2),"MIT/STATUS",
            		ldaAppl306.getRequest_R_Mit_Status_Cde().getValue(1,":",2));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0180"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0180"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaAppl306.getVw_request_R().deleteDBRow("PND_PND_L0180");                                                                                                    //Natural: DELETE ( ##L0180. )
            pnd_Et_Ctr.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #ET-CTR
            if (condition(pnd_Et_Ctr.greaterOrEqual(pnd_Et_Limit)))                                                                                                       //Natural: IF #ET-CTR GE #ET-LIMIT
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Et_Ctr.reset();                                                                                                                                       //Natural: RESET #ET-CTR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 120T *DATU // 48T 'PRAP ON DEMAND LETTER REQUEST PURGE' /// '-' ( 132 ) //
        //* *======
    }                                                                                                                                                                     //Natural: ON ERROR

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"Error:     ",Global.getERROR_NR());                 //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'Error:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(120),Global.getDATU(),NEWLINE,NEWLINE,new 
            TabSetting(48),"PRAP ON DEMAND LETTER REQUEST PURGE",NEWLINE,NEWLINE,NEWLINE,"-",new RepeatItem(132),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "REQUEST/BY",
        		ldaAppl306.getRequest_R_Update_By(),"RQST DATE/TIME",
        		ldaAppl306.getRequest_R_Update_Date(), new ReportEditMask ("9999/99/99"),NEWLINE,"/",
        		ldaAppl306.getRequest_R_Update_Time(), new ReportEditMask ("99:99:99:9"),"PPG",
        		ldaAppl306.getRequest_R_Ppg_Code(),"REQUEST/TEAM",
        		ldaAppl306.getRequest_R_Request_Team(),"REQ/TYP",
        		ldaAppl306.getRequest_R_Requested_Letter_Type(),"PULL/CODE",
        		ldaAppl306.getRequest_R_Pullout_Code(),"TEAM/CODE",
        		ldaAppl306.getRequest_R_Iis_Team_Code(),"APP/PREM",
        		ldaAppl306.getRequest_R_App_Prem_Rqst(),"INSTN/CODE",
        		ldaAppl306.getRequest_R_Instn_Link_Cde(),"SYSTEM/CODE",
        		ldaAppl306.getRequest_R_System_Link_Cde(),"PUBLIC/CODE",
        		ldaAppl306.getRequest_R_Public_Link_Cde(),"MIT/DTE-TME",
        		ldaAppl306.getRequest_R_Mit_Rqst_Log_Dte_Tme(),"MIT/OP CODE",
        		ldaAppl306.getRequest_R_Mit_Rqst_Log_Oprtr_Cde(),"MIT/UNIT CDE",
        		ldaAppl306.getRequest_R_Mit_Original_Unit_Cde(),"MIT/STEP ID",
        		ldaAppl306.getRequest_R_Mit_Step_Id(),"MIT/STATUS",
        		ldaAppl306.getRequest_R_Mit_Status_Cde());
    }
}
