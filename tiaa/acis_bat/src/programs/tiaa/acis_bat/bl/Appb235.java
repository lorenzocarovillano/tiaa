/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:06 PM
**        * FROM NATURAL PROGRAM : Appb235
************************************************************
**        * FILE NAME            : Appb235.java
**        * CLASS NAME           : Appb235
**        * INSTANCE NAME        : Appb235
************************************************************
************************************************************************
* PROGRAM  : APPB235                                                   *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : APRIL 21, 2000                                            *
* FUNCTION : READS THE WORK FILE CREATED IN P1040NID AND PRODUCES      *
*   INSTITUTIONAL REPORTS FOR THE ARKANSAS SYSTEM.                     *
************************************************************************
* UPDATE:  : SUNGARD RELEASE 2   K.GATES                               *
*            RESTOWED FOR APPL230 AND APPL231                          *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb235 extends BLNatBase
{
    // Data Areas
    private LdaAppl230 ldaAppl230;
    private LdaAppl231 ldaAppl231;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Day;

    private DbsGroup pnd_Day__R_Field_1;
    private DbsField pnd_Day_Pnd_Day_Mm;
    private DbsField pnd_Day_Pnd_Day_Dd;
    private DbsField pnd_Day_Pnd_Day_Yyyy;
    private DbsField pnd_Display_Line;

    private DbsGroup pnd_Display_Line__R_Field_2;
    private DbsField pnd_Display_Line_Pnd_Filler;
    private DbsField pnd_Display_Line_Pnd_Display_Date;
    private DbsField pnd_Month;

    private DbsGroup pnd_Read_Ppg_Table;
    private DbsField pnd_Read_Ppg_Table_Pnd_Read_Ppg;
    private DbsField pnd_Read_Ppg_Table_Pnd_Read_Title;

    private DbsGroup pnd_Report_Fields;
    private DbsField pnd_Report_Fields_Pnd_Ppg_Title;
    private DbsField pnd_Report_Fields_Pnd_Ownership;
    private DbsField pnd_I;
    private DbsField pnd_Length;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl230 = new LdaAppl230();
        registerRecord(ldaAppl230);
        ldaAppl231 = new LdaAppl231();
        registerRecord(ldaAppl231);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 8);

        pnd_Day__R_Field_1 = localVariables.newGroupInRecord("pnd_Day__R_Field_1", "REDEFINE", pnd_Day);
        pnd_Day_Pnd_Day_Mm = pnd_Day__R_Field_1.newFieldInGroup("pnd_Day_Pnd_Day_Mm", "#DAY-MM", FieldType.NUMERIC, 2);
        pnd_Day_Pnd_Day_Dd = pnd_Day__R_Field_1.newFieldInGroup("pnd_Day_Pnd_Day_Dd", "#DAY-DD", FieldType.NUMERIC, 2);
        pnd_Day_Pnd_Day_Yyyy = pnd_Day__R_Field_1.newFieldInGroup("pnd_Day_Pnd_Day_Yyyy", "#DAY-YYYY", FieldType.NUMERIC, 4);
        pnd_Display_Line = localVariables.newFieldInRecord("pnd_Display_Line", "#DISPLAY-LINE", FieldType.STRING, 132);

        pnd_Display_Line__R_Field_2 = localVariables.newGroupInRecord("pnd_Display_Line__R_Field_2", "REDEFINE", pnd_Display_Line);
        pnd_Display_Line_Pnd_Filler = pnd_Display_Line__R_Field_2.newFieldInGroup("pnd_Display_Line_Pnd_Filler", "#FILLER", FieldType.STRING, 48);
        pnd_Display_Line_Pnd_Display_Date = pnd_Display_Line__R_Field_2.newFieldInGroup("pnd_Display_Line_Pnd_Display_Date", "#DISPLAY-DATE", FieldType.STRING, 
            36);
        pnd_Month = localVariables.newFieldInRecord("pnd_Month", "#MONTH", FieldType.STRING, 9);

        pnd_Read_Ppg_Table = localVariables.newGroupInRecord("pnd_Read_Ppg_Table", "#READ-PPG-TABLE");
        pnd_Read_Ppg_Table_Pnd_Read_Ppg = pnd_Read_Ppg_Table.newFieldArrayInGroup("pnd_Read_Ppg_Table_Pnd_Read_Ppg", "#READ-PPG", FieldType.STRING, 4, 
            new DbsArrayController(1, 10));
        pnd_Read_Ppg_Table_Pnd_Read_Title = pnd_Read_Ppg_Table.newFieldArrayInGroup("pnd_Read_Ppg_Table_Pnd_Read_Title", "#READ-TITLE", FieldType.STRING, 
            56, new DbsArrayController(1, 10));

        pnd_Report_Fields = localVariables.newGroupInRecord("pnd_Report_Fields", "#REPORT-FIELDS");
        pnd_Report_Fields_Pnd_Ppg_Title = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Ppg_Title", "#PPG-TITLE", FieldType.STRING, 56);
        pnd_Report_Fields_Pnd_Ownership = pnd_Report_Fields.newFieldInGroup("pnd_Report_Fields_Pnd_Ownership", "#OWNERSHIP", FieldType.STRING, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Length = localVariables.newFieldInRecord("pnd_Length", "#LENGTH", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl230.initializeValues();
        ldaAppl231.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Appb235() throws Exception
    {
        super("Appb235");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB235", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 HW = OFF PS = 52
        pnd_Day.setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYYYY"));                                                                                          //Natural: MOVE EDITED *DATX ( EM = MMDDYYYY ) TO #DAY
        if (condition(pnd_Day_Pnd_Day_Dd.lessOrEqual(10)))                                                                                                                //Natural: IF #DAY-DD LE 10
        {
            pnd_Day_Pnd_Day_Mm.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #DAY-MM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Day_Pnd_Day_Mm.equals(getZero())))                                                                                                              //Natural: IF #DAY-MM EQ 0
        {
            pnd_Day_Pnd_Day_Mm.setValue(12);                                                                                                                              //Natural: MOVE 12 TO #DAY-MM
            pnd_Day_Pnd_Day_Yyyy.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #DAY-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Month.setValue(ldaAppl231.getPnd_Month_Table().getValue(pnd_Day_Pnd_Day_Mm));                                                                                 //Natural: MOVE #MONTH-TABLE ( #DAY-MM ) TO #MONTH
        DbsUtil.examine(new ExamineSource(pnd_Month,2,8), new ExamineTranslate(TranslateOption.Lower));                                                                   //Natural: EXAMINE SUBSTR ( #MONTH,2,8 ) TRANSLATE INTO LOWER CASE
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        DbsUtil.examine(new ExamineSource(pnd_Month), new ExamineSearch(" "), new ExamineGivingLength(pnd_I));                                                            //Natural: EXAMINE #MONTH FOR ' ' GIVING LENGTH #I
        pnd_Length.compute(new ComputeParameters(false, pnd_Length), DbsField.subtract(19,((DbsField.add(22,pnd_I)).divide(2))));                                         //Natural: COMPUTE #LENGTH = 19 - ( ( 22 + #I ) / 2 )
        setValueToSubstring("For the month of ",pnd_Display_Line_Pnd_Display_Date,pnd_Length.getInt(),17);                                                                //Natural: MOVE 'For the month of ' TO SUBSTR ( #DISPLAY-DATE,#LENGTH,17 )
        pnd_Length.nadd(17);                                                                                                                                              //Natural: ADD 17 TO #LENGTH
        setValueToSubstring(pnd_Month,pnd_Display_Line_Pnd_Display_Date,pnd_Length.getInt(),pnd_I.getInt());                                                              //Natural: MOVE #MONTH TO SUBSTR ( #DISPLAY-DATE,#LENGTH,#I )
        pnd_Length.nadd(pnd_I);                                                                                                                                           //Natural: ADD #I TO #LENGTH
        pnd_Length.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #LENGTH
        setValueToSubstring(pnd_Day_Pnd_Day_Yyyy,pnd_Display_Line_Pnd_Display_Date,pnd_Length.getInt(),4);                                                                //Natural: MOVE #DAY-YYYY TO SUBSTR ( #DISPLAY-DATE,#LENGTH,4 )
        pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue(1).setValue("0018");                                                                                                     //Natural: MOVE '0018' TO #READ-PPG ( 1 )
        pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue(2).setValue("0491");                                                                                                     //Natural: MOVE '0491' TO #READ-PPG ( 2 )
        pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue(3).setValue("1060");                                                                                                     //Natural: MOVE '1060' TO #READ-PPG ( 3 )
        pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue(4).setValue("2448");                                                                                                     //Natural: MOVE '2448' TO #READ-PPG ( 4 )
        pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue(5).setValue("2492");                                                                                                     //Natural: MOVE '2492' TO #READ-PPG ( 5 )
        pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue(6).setValue("2643");                                                                                                     //Natural: MOVE '2643' TO #READ-PPG ( 6 )
        pnd_Read_Ppg_Table_Pnd_Read_Title.getValue(1).setValue("University of Arkansas at Fayetteville - PPG 0018");                                                      //Natural: MOVE 'University of Arkansas at Fayetteville - PPG 0018' TO #READ-TITLE ( 1 )
        pnd_Read_Ppg_Table_Pnd_Read_Title.getValue(2).setValue("University of Arkansas at Little Rock - PPG 0491");                                                       //Natural: MOVE 'University of Arkansas at Little Rock - PPG 0491' TO #READ-TITLE ( 2 )
        pnd_Read_Ppg_Table_Pnd_Read_Title.getValue(3).setValue("University of Arkansas Medical Sciences - PPG 1060");                                                     //Natural: MOVE 'University of Arkansas Medical Sciences - PPG 1060' TO #READ-TITLE ( 3 )
        pnd_Read_Ppg_Table_Pnd_Read_Title.getValue(4).setValue("University of Arkansas Coop Extension Service - PPG 2448");                                               //Natural: MOVE 'University of Arkansas Coop Extension Service - PPG 2448' TO #READ-TITLE ( 4 )
        pnd_Read_Ppg_Table_Pnd_Read_Title.getValue(5).setValue("University of Arkansas at Pine Bluff - PPG 2492");                                                        //Natural: MOVE 'University of Arkansas at Pine Bluff - PPG 2492' TO #READ-TITLE ( 5 )
        pnd_Read_Ppg_Table_Pnd_Read_Title.getValue(6).setValue("University of Arkansas at Monticello - PPG 2643");                                                        //Natural: MOVE 'University of Arkansas at Monticello - PPG 2643' TO #READ-TITLE ( 6 )
        //*  OIA (CRS) \/
        //*  OIA (CRS) /\
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 DA-SRA-1 DA-SRA-2 DA-SRA-3 DA-SRA-4 DA-SRA-5 DA-SRA-6
        while (condition(getWorkFiles().read(1, ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_1(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_2(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_3(), 
            ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_4(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_5(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_6())))
        {
            if (condition(!(ldaAppl230.getDa_Sra_Summary_Trans_Coll_Code().equals(pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue("*")))))                                       //Natural: ACCEPT IF COLL-CODE EQ #READ-PPG ( * )
            {
                continue;
            }
            getSort().writeSortInData(ldaAppl230.getDa_Sra_Summary_Trans_Coll_Code(), ldaAppl230.getDa_Sra_Summary_Trans_Name(), ldaAppl230.getDa_Sra_Summary_Trans_Soc_Sec_Num(),  //Natural: END-ALL
                ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_1(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_2(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_3(), 
                ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_4(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_5(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_6());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  OIA (CRS) \/
        //*  OIA (CRS) /\
        getSort().sortData(ldaAppl230.getDa_Sra_Summary_Trans_Coll_Code(), ldaAppl230.getDa_Sra_Summary_Trans_Name(), ldaAppl230.getDa_Sra_Summary_Trans_Soc_Sec_Num());  //Natural: SORT BY COLL-CODE NAME SOC-SEC-NUM USING DA-SRA-1 DA-SRA-2 DA-SRA-3 DA-SRA-4 DA-SRA-5 DA-SRA-6
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaAppl230.getDa_Sra_Summary_Trans_Coll_Code(), ldaAppl230.getDa_Sra_Summary_Trans_Name(), ldaAppl230.getDa_Sra_Summary_Trans_Soc_Sec_Num(), 
            ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_1(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_2(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_3(), 
            ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_4(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_5(), ldaAppl230.getDa_Sra_Summary_Trans_Da_Sra_6())))
        {
            CheckAtStartofData251();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
                                                                                                                                                                          //Natural: PERFORM BLD-REPORT
            sub_Bld_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF COLL-CODE
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 48X 'Arkansas System Monthly Issue Report' / #DISPLAY-LINE // #PPG-TITLE ///
        //* *-----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PPG-TITLE
        //* *--------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLD-REPORT
        //* *======
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Get_Ppg_Title() throws Exception                                                                                                                     //Natural: GET-PPG-TITLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        DbsUtil.examine(new ExamineSource(pnd_Read_Ppg_Table_Pnd_Read_Ppg.getValue("*")), new ExamineSearch(ldaAppl230.getDa_Sra_Summary_Trans_Coll_Code()),              //Natural: EXAMINE #READ-PPG ( * ) FOR COLL-CODE GIVING INDEX #I
            new ExamineGivingIndex(pnd_I));
        if (condition(pnd_I.notEquals(getZero())))                                                                                                                        //Natural: IF #I NE 0
        {
            pnd_Report_Fields_Pnd_Ppg_Title.setValue(pnd_Read_Ppg_Table_Pnd_Read_Title.getValue(pnd_I));                                                                  //Natural: MOVE #READ-TITLE ( #I ) TO #PPG-TITLE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Ppg_Title.reset();                                                                                                                      //Natural: RESET #PPG-TITLE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bld_Report() throws Exception                                                                                                                        //Natural: BLD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------
        short decideConditionsMet292 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OWNERSHIP;//Natural: VALUE '1'
        if (condition((ldaAppl230.getDa_Sra_Summary_Trans_Ownership().equals("1"))))
        {
            decideConditionsMet292++;
            pnd_Report_Fields_Pnd_Ownership.setValue("Immediate");                                                                                                        //Natural: MOVE 'Immediate' TO #OWNERSHIP
        }                                                                                                                                                                 //Natural: VALUE '2', '3'
        else if (condition((ldaAppl230.getDa_Sra_Summary_Trans_Ownership().equals("2") || ldaAppl230.getDa_Sra_Summary_Trans_Ownership().equals("3"))))
        {
            decideConditionsMet292++;
            pnd_Report_Fields_Pnd_Ownership.setValue("Delayed");                                                                                                          //Natural: MOVE 'Delayed' TO #OWNERSHIP
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Report_Fields_Pnd_Ownership.setValue(ldaAppl230.getDa_Sra_Summary_Trans_Ownership());                                                                     //Natural: MOVE OWNERSHIP TO #OWNERSHIP
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().display(1, "Participant/Name",                                                                                                                       //Natural: DISPLAY ( 1 ) 'Participant/Name' NAME 5X 'Social Security/Number' SOC-SEC-NUM ( EM = 999-99-9999 ) 5X 'TIAA Contract/Number' TIAA-CONTRACT ( EM = XXXXXXX-X ) 5X 'CREF Contract/Number' CREF-CONTRACT ( EM = XXXXXXX-X ) 5X 'Vesting/Status' #OWNERSHIP
        		ldaAppl230.getDa_Sra_Summary_Trans_Name(),new ColumnSpacing(5),"Social Security/Number",
        		ldaAppl230.getDa_Sra_Summary_Trans_Soc_Sec_Num(), new ReportEditMask ("999-99-9999"),new ColumnSpacing(5),"TIAA Contract/Number",
        		ldaAppl230.getDa_Sra_Summary_Trans_Tiaa_Contract(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(5),"CREF Contract/Number",
        		ldaAppl230.getDa_Sra_Summary_Trans_Cref_Contract(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(5),"Vesting/Status",
        		pnd_Report_Fields_Pnd_Ownership);
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getReports().write(0, "PROGRAM:   ",Global.getPROGRAM(),NEWLINE,"ERROR LINE:",Global.getERROR_LINE(),NEWLINE,"ERROR:     ",Global.getERROR_NR());                 //Natural: WRITE 'PROGRAM:   ' *PROGRAM / 'ERROR LINE:' *ERROR-LINE / 'ERROR:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaAppl230_getDa_Sra_Summary_Trans_Coll_CodeIsBreak = ldaAppl230.getDa_Sra_Summary_Trans_Coll_Code().isBreak(endOfData);
        if (condition(ldaAppl230_getDa_Sra_Summary_Trans_Coll_CodeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM GET-PPG-TITLE
            sub_Get_Ppg_Title();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 HW=OFF PS=52");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new ColumnSpacing(48),"Arkansas System Monthly Issue Report",NEWLINE,
            pnd_Display_Line,NEWLINE,NEWLINE,pnd_Report_Fields_Pnd_Ppg_Title,NEWLINE,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "Participant/Name",
        		ldaAppl230.getDa_Sra_Summary_Trans_Name(),new ColumnSpacing(5),"Social Security/Number",
        		ldaAppl230.getDa_Sra_Summary_Trans_Soc_Sec_Num(), new ReportEditMask ("999-99-9999"),new ColumnSpacing(5),"TIAA Contract/Number",
        		ldaAppl230.getDa_Sra_Summary_Trans_Tiaa_Contract(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(5),"CREF Contract/Number",
        		ldaAppl230.getDa_Sra_Summary_Trans_Cref_Contract(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(5),"Vesting/Status",
        		pnd_Report_Fields_Pnd_Ownership);
    }
    private void CheckAtStartofData251() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM GET-PPG-TITLE
            sub_Get_Ppg_Title();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
