/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:00 PM
**        * FROM NATURAL PROGRAM : Appb220
************************************************************
**        * FILE NAME            : Appb220.java
**        * CLASS NAME           : Appb220
**        * INSTANCE NAME        : Appb220
************************************************************
***********************************************************************
* PROGRAM ID :  APPB220
* FUNCTION   :  DAILY STATISTICAL REPORTS AND CREATE WORKFILES.
*
* MODIFY ON BY
* 03/09/99  L SHU    TO OUTPUT BUCKET, ACCUM, START DATE AND      (LS1)
*                     END DATE TO WORK FILE.
* 03/23/99  L SHU    TO CATER FOR KEOGH
* 08/12/99  L SHU    TO CHECK SPECIFIC REGION FOR 1&2 AND R&S     (LS2)
* 11/13/01  B.E.     457(B) B.E. APPM223 MAP CHANGED - RESTOWED MODULE
* 03/29/02  C.S.     RESTOW DUE TO CHANGES TO APPS221 AND APPS222
* 03/01/04  K.G.     RESTOW DUE TO CHANGES TO APPS221, APPS222, AND
*                    APPG225 FOR SGRD PLAN AND SUBPLAN
*                    CHECKING!!!!!!!!!!!!!!!!!!!!!!!!!!!
* 12/23/04  R.W.     TNT/REL4. CHANGES              (RW1.)
* 03/16/05  R.W.     TNT/REL5. CHANGES -RESTOW      (RW2.)
* 09/09/05  D.M.     TNT/REL6. CHANGES IN GLOBAL (APPG225)
*                    RESTOW ONLY                    (DTM.)
* 10/03/06  K.GATES  DCA CHANGES IN GLOBAL (APPG225) - RESTOW ONLY
* 09/19/08  N.CVETKOVIC RHSP CHANGES IN GLOBAL (APPG225) - RESTOW ONLY
* 03/05/10  C.AVE    RESTOW DUE TO CHANGES IN
*                    1) GDA APPG225 2) MAP APPM223
*                    MODIFIED CRITERIA FOR CAPTURING THE NUMBER WITHIN
*                    THE CONTRACT USED FOR CALCULATION - TIGR
* 06/22/17  BABRE    PIN EXPANSION CHANGES. CHG425939   STOW ONLY
***********************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb220 extends BLNatBase
{
    // Data Areas
    private GdaAppg225 gdaAppg225;
    private LdaAppl020 ldaAppl020;
    private PdaAppa010 pdaAppa010;
    private PdaAppa020 pdaAppa020;
    private LdaAcil7070 ldaAcil7070;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Table_Id_Entry_Cde;

    private DbsGroup pnd_Table_Id_Entry_Cde__R_Field_1;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id;
    private DbsField pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde;
    private DbsField pnd_L;
    private DbsField pnd_Premium_Team_Code;

    private DbsGroup pnd_Premium_Team_Code__R_Field_2;
    private DbsField pnd_Premium_Team_Code_Pnd_Prem_Team_Disc;
    private DbsField pnd_Premium_Team_Code_Pnd_Prem_Team_Cd;
    private DbsField pnd_Wk_Prem_Team_Code;

    private DbsGroup pnd_Wk_Prem_Team_Code__R_Field_3;
    private DbsField pnd_Wk_Prem_Team_Code_Pnd_Wk_Iis_Prem_Team_Disc;
    private DbsField pnd_Wk_Prem_Team_Code_Pnd_Wk_Prap_Prem_Team_Code;
    private DbsField pnd_Hold_Team_Disc;
    private DbsField pnd_Team_Disc;
    private DbsField display_Map_Date;
    private DbsField pnd_Wk_Start_Date_D;
    private DbsField pnd_Wk_Start_Date;
    private DbsField pnd_Entry_Dscrptn_Txt;

    private DbsGroup pnd_Entry_Dscrptn_Txt__R_Field_4;
    private DbsField pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket;
    private DbsField pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum;
    private DbsField pnd_Entry_Dscrptn_Txt_Pnd_Entry_Team_Cde;
    private DbsField pnd_I;
    private DbsField pnd_Wk_Cont;
    private DbsField pnd_Prev_Contr;
    private DbsField pnd_Curr_Contr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaAppg225 = GdaAppg225.getInstance(getCallnatLevel());
        registerRecord(gdaAppg225);
        if (gdaOnly) return;

        ldaAppl020 = new LdaAppl020();
        registerRecord(ldaAppl020);
        registerRecord(ldaAppl020.getVw_app_Ilog_B_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_J_Rcrd_View());
        registerRecord(ldaAppl020.getVw_app_Ilog_A_Rcrd_View());
        localVariables = new DbsRecord();
        pdaAppa010 = new PdaAppa010(localVariables);
        pdaAppa020 = new PdaAppa020(localVariables);
        ldaAcil7070 = new LdaAcil7070();
        registerRecord(ldaAcil7070);
        registerRecord(ldaAcil7070.getVw_app_Table_Entry_View());

        // Local Variables
        pnd_Table_Id_Entry_Cde = localVariables.newFieldInRecord("pnd_Table_Id_Entry_Cde", "#TABLE-ID-ENTRY-CDE", FieldType.STRING, 28);

        pnd_Table_Id_Entry_Cde__R_Field_1 = localVariables.newGroupInRecord("pnd_Table_Id_Entry_Cde__R_Field_1", "REDEFINE", pnd_Table_Id_Entry_Cde);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr", 
            "#ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id", 
            "#ENTRY-TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde = pnd_Table_Id_Entry_Cde__R_Field_1.newFieldInGroup("pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde", "#ENTRY-CDE", 
            FieldType.STRING, 8);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_Premium_Team_Code = localVariables.newFieldInRecord("pnd_Premium_Team_Code", "#PREMIUM-TEAM-CODE", FieldType.STRING, 10);

        pnd_Premium_Team_Code__R_Field_2 = localVariables.newGroupInRecord("pnd_Premium_Team_Code__R_Field_2", "REDEFINE", pnd_Premium_Team_Code);
        pnd_Premium_Team_Code_Pnd_Prem_Team_Disc = pnd_Premium_Team_Code__R_Field_2.newFieldInGroup("pnd_Premium_Team_Code_Pnd_Prem_Team_Disc", "#PREM-TEAM-DISC", 
            FieldType.STRING, 8);
        pnd_Premium_Team_Code_Pnd_Prem_Team_Cd = pnd_Premium_Team_Code__R_Field_2.newFieldInGroup("pnd_Premium_Team_Code_Pnd_Prem_Team_Cd", "#PREM-TEAM-CD", 
            FieldType.STRING, 2);
        pnd_Wk_Prem_Team_Code = localVariables.newFieldInRecord("pnd_Wk_Prem_Team_Code", "#WK-PREM-TEAM-CODE", FieldType.STRING, 10);

        pnd_Wk_Prem_Team_Code__R_Field_3 = localVariables.newGroupInRecord("pnd_Wk_Prem_Team_Code__R_Field_3", "REDEFINE", pnd_Wk_Prem_Team_Code);
        pnd_Wk_Prem_Team_Code_Pnd_Wk_Iis_Prem_Team_Disc = pnd_Wk_Prem_Team_Code__R_Field_3.newFieldInGroup("pnd_Wk_Prem_Team_Code_Pnd_Wk_Iis_Prem_Team_Disc", 
            "#WK-IIS-PREM-TEAM-DISC", FieldType.STRING, 8);
        pnd_Wk_Prem_Team_Code_Pnd_Wk_Prap_Prem_Team_Code = pnd_Wk_Prem_Team_Code__R_Field_3.newFieldInGroup("pnd_Wk_Prem_Team_Code_Pnd_Wk_Prap_Prem_Team_Code", 
            "#WK-PRAP-PREM-TEAM-CODE", FieldType.STRING, 2);
        pnd_Hold_Team_Disc = localVariables.newFieldInRecord("pnd_Hold_Team_Disc", "#HOLD-TEAM-DISC", FieldType.STRING, 8);
        pnd_Team_Disc = localVariables.newFieldInRecord("pnd_Team_Disc", "#TEAM-DISC", FieldType.STRING, 8);
        display_Map_Date = localVariables.newFieldInRecord("display_Map_Date", "DISPLAY-MAP-DATE", FieldType.DATE);
        pnd_Wk_Start_Date_D = localVariables.newFieldInRecord("pnd_Wk_Start_Date_D", "#WK-START-DATE-D", FieldType.DATE);
        pnd_Wk_Start_Date = localVariables.newFieldInRecord("pnd_Wk_Start_Date", "#WK-START-DATE", FieldType.STRING, 8);
        pnd_Entry_Dscrptn_Txt = localVariables.newFieldInRecord("pnd_Entry_Dscrptn_Txt", "#ENTRY-DSCRPTN-TXT", FieldType.STRING, 60);

        pnd_Entry_Dscrptn_Txt__R_Field_4 = localVariables.newGroupInRecord("pnd_Entry_Dscrptn_Txt__R_Field_4", "REDEFINE", pnd_Entry_Dscrptn_Txt);
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket = pnd_Entry_Dscrptn_Txt__R_Field_4.newFieldInGroup("pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket", "#ENTRY-BUCKET", 
            FieldType.STRING, 2);
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum = pnd_Entry_Dscrptn_Txt__R_Field_4.newFieldInGroup("pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum", "#ENTRY-ACCUM", 
            FieldType.STRING, 2);
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Team_Cde = pnd_Entry_Dscrptn_Txt__R_Field_4.newFieldInGroup("pnd_Entry_Dscrptn_Txt_Pnd_Entry_Team_Cde", "#ENTRY-TEAM-CDE", 
            FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 1);
        pnd_Wk_Cont = localVariables.newFieldInRecord("pnd_Wk_Cont", "#WK-CONT", FieldType.STRING, 6);
        pnd_Prev_Contr = localVariables.newFieldInRecord("pnd_Prev_Contr", "#PREV-CONTR", FieldType.STRING, 6);
        pnd_Curr_Contr = localVariables.newFieldInRecord("pnd_Curr_Contr", "#CURR-CONTR", FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl020.initializeValues();
        ldaAcil7070.initializeValues();

        localVariables.reset();
        pnd_L.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb220() throws Exception
    {
        super("Appb220");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
        sub_Init_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MAIN-RTN
        sub_Main_Rtn();
        if (condition(Global.isEscape())) {return;}
        FOR01:                                                                                                                                                            //Natural: FOR #PROD-SUB 1 30
        for (gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub().setValue(1); condition(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub().lessOrEqual(30)); 
            gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub().nadd(1))
        {
            pdaAppa020.getPnd_Parmdata_Pnd_Accum_Tally().getValue("*").reset();                                                                                           //Natural: RESET #ACCUM-TALLY ( * )
            //*  EAC (SINGLETON)
            pnd_Prev_Contr.reset();                                                                                                                                       //Natural: RESET #PREV-CONTR #CURR-CONTR #WK-CONT
            pnd_Curr_Contr.reset();
            pnd_Wk_Cont.reset();
            if (condition(gdaAppg225.getRoman_Global_Pnd_Cnt_Product_Code().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).notEquals(" ")))           //Natural: IF #CNT-PRODUCT-CODE ( #PROD-SUB ) <> ' '
            {
                pdaAppa020.getPnd_Parmdata_Pnd_Product_Code().setValue(gdaAppg225.getRoman_Global_Pnd_Cnt_Product_Code().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub())); //Natural: MOVE #CNT-PRODUCT-CODE ( #PROD-SUB ) TO #PRODUCT-CODE
                DbsUtil.callnat(Appp030.class , getCurrentProcessState(), pdaAppa020.getPnd_Parmdata());                                                                  //Natural: CALLNAT 'APPP030' #PARMDATA
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //*                                            /* CHECK FOR BLANK CONTRACT#
                //*  SINGLETON 5/29/01
                if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Last_Contract().greater(" ")))                                                                               //Natural: IF #LAST-CONTRACT GT ' '
                {
                    gdaAppg225.getRoman_Global_Display_Curr_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).setValue(pdaAppa020.getPnd_Parmdata_Pnd_Last_Contract()); //Natural: MOVE #LAST-CONTRACT TO DISPLAY-CURR-CONTRACT ( #PROD-SUB )
                    gdaAppg225.getPnd_Balancing_Report_Fields_Curr_Contract().setValue(pdaAppa020.getPnd_Parmdata_Pnd_Last_Contract());                                   //Natural: MOVE #LAST-CONTRACT TO CURR-CONTRACT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaAppg225.getRoman_Global_Display_Curr_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).setValue("0000000");            //Natural: MOVE '0000000' TO DISPLAY-CURR-CONTRACT ( #PROD-SUB )
                    gdaAppg225.getPnd_Balancing_Report_Fields_Curr_Contract().setValue("0000000");                                                                        //Natural: MOVE '0000000' TO CURR-CONTRACT
                    getReports().write(0, "APPB220: LAST CONTRACT NUMBER IS BLANK - USING ZEROES");                                                                       //Natural: WRITE 'APPB220: LAST CONTRACT NUMBER IS BLANK - USING ZEROES'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  CHECK FOR BLANK CONTRACT#
                //*  SINGLETON 5/29/01
                if (condition(pdaAppa020.getPnd_Parmdata_Pnd_Prev_Contract().greater(" ")))                                                                               //Natural: IF #PREV-CONTRACT GT ' '
                {
                    gdaAppg225.getRoman_Global_Display_Prev_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).setValue(pdaAppa020.getPnd_Parmdata_Pnd_Prev_Contract()); //Natural: MOVE #PREV-CONTRACT TO DISPLAY-PREV-CONTRACT ( #PROD-SUB )
                    gdaAppg225.getPnd_Balancing_Report_Fields_Prev_Contract().setValue(pdaAppa020.getPnd_Parmdata_Pnd_Prev_Contract());                                   //Natural: MOVE #PREV-CONTRACT TO PREV-CONTRACT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaAppg225.getRoman_Global_Display_Prev_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).setValue("0000000");            //Natural: MOVE '0000000' TO DISPLAY-PREV-CONTRACT ( #PROD-SUB )
                    gdaAppg225.getPnd_Balancing_Report_Fields_Prev_Contract().setValue("0000000");                                                                        //Natural: MOVE '0000000' TO PREV-CONTRACT
                    getReports().write(0, "APPB220: PREV CONTRACT NUMBER IS BLANK - USING ZEROES");                                                                       //Natural: WRITE 'APPB220: PREV CONTRACT NUMBER IS BLANK - USING ZEROES'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  EAC (SINGLETON)
                }                                                                                                                                                         //Natural: END-IF
                FOR02:                                                                                                                                                    //Natural: FOR #I = 1 TO 6
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
                {
                    //* *  TIGR-COMMENTED OUT BELOW
                    //* *    IF SUBSTR(PREV-CONTRACT-NUM,#I,1) NE MASK (N)   /* EAC (SINGLETON)
                    //* *      ESCAPE BOTTOM                                 /* EAC (SINGLETON)
                    //* *    END-IF                                          /* EAC (SINGLETON)
                    //* *  TIGR-ADDED THE IF CONDITION BELOW TO CONSIDER ONLY THE NUMBER
                    //* *  COZ INDEXED PROD. IS PREFIXED WITH 2 ALPHA.
                    //*  TIGR
                    //*  EAC (SINGLETON)
                    if (condition(DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Prev_Contract_Num().getSubstring(pnd_I.getInt(),1),"N")))                 //Natural: IF SUBSTR ( PREV-CONTRACT-NUM,#I,1 ) EQ MASK ( N )
                    {
                        pnd_Wk_Cont.setValue(gdaAppg225.getPnd_Balancing_Report_Fields_Prev_Contract_Num().getSubstring(pnd_I.getInt(),1));                               //Natural: ASSIGN #WK-CONT := SUBSTR ( PREV-CONTRACT-NUM,#I,1 )
                        //*  EAC (SINGLETON)
                        //*  EAC (SINGLETON)
                        pnd_Prev_Contr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Prev_Contr, pnd_Wk_Cont));                                            //Natural: COMPRESS #PREV-CONTR #WK-CONT INTO #PREV-CONTR LEAVING NO SPACE
                        //*  TIGR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EAC (SINGLETON)
                    //*  EAC (SINGLETON)
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR03:                                                                                                                                                    //Natural: FOR #I = 1 TO 6
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
                {
                    //* *  TIGR-COMMENTED OUT BELOW
                    //* *    IF SUBSTR(CURR-CONTRACT-NUM,#I,1) NE MASK (N)   /* EAC (SINGLETON)
                    //* *      ESCAPE BOTTOM                                 /* EAC (SINGLETON)
                    //* *    END-IF                                          /* EAC (SINGLETON)
                    //* *  TIGR-ADDED THE IF CONDITION BELOW TO CONSIDER ONLY THE NUMBER
                    //* *  COZ INDEXED PROD. IS PREFIXED WITH 2 ALPHA.
                    //*  TIGR
                    //*  EAC (SINGLETON)
                    if (condition(DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Curr_Contract_Num().getSubstring(pnd_I.getInt(),1),"N")))                 //Natural: IF SUBSTR ( CURR-CONTRACT-NUM,#I,1 ) EQ MASK ( N )
                    {
                        pnd_Wk_Cont.setValue(gdaAppg225.getPnd_Balancing_Report_Fields_Curr_Contract_Num().getSubstring(pnd_I.getInt(),1));                               //Natural: ASSIGN #WK-CONT := SUBSTR ( CURR-CONTRACT-NUM,#I,1 )
                        //*  EAC (SINGLETON)
                        //*  EAC (SINGLETON)
                        pnd_Curr_Contr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Curr_Contr, pnd_Wk_Cont));                                            //Natural: COMPRESS #CURR-CONTR #WK-CONT INTO #CURR-CONTR LEAVING NO SPACE
                        //*  TIGR
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EAC (SINGLETON)
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  EAC (SINGLETON)
                //*  EAC (SINGLETON)
                gdaAppg225.getRoman_Global_Display_Nums_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).compute(new ComputeParameters(false,  //Natural: COMPUTE DISPLAY-NUMS-CONTRACT ( #PROD-SUB ) = VAL ( #CURR-CONTR ) - VAL ( #PREV-CONTR )
                    gdaAppg225.getRoman_Global_Display_Nums_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub())), pnd_Curr_Contr.val().subtract(pnd_Prev_Contr.val()));
                gdaAppg225.getRoman_Global_Display_Contracts_Tot().nadd(gdaAppg225.getRoman_Global_Display_Nums_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub())); //Natural: ADD DISPLAY-NUMS-CONTRACT ( #PROD-SUB ) TO DISPLAY-CONTRACTS-TOT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaAppg225.getRoman_Global_Display_Curr_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).reset();                            //Natural: RESET DISPLAY-CURR-CONTRACT ( #PROD-SUB ) DISPLAY-PREV-CONTRACT ( #PROD-SUB ) DISPLAY-NUMS-CONTRACT ( #PROD-SUB )
                gdaAppg225.getRoman_Global_Display_Prev_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).reset();
                gdaAppg225.getRoman_Global_Display_Nums_Contract().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()).reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        gdaAppg225.getRoman_Global_Display_Out_Balance().compute(new ComputeParameters(false, gdaAppg225.getRoman_Global_Display_Out_Balance()), gdaAppg225.getRoman_Global_Display_Contracts_Tot().subtract(gdaAppg225.getPnd_Balancing_Report_Fields_Total_Issued_Applications()).subtract(gdaAppg225.getPnd_Balancing_Report_Fields_Total_Assigned_Premiums()).subtract(gdaAppg225.getPnd_Balancing_Report_Fields_Region_Wrong_Count())); //Natural: COMPUTE DISPLAY-OUT-BALANCE = DISPLAY-CONTRACTS-TOT - TOTAL-ISSUED-APPLICATIONS - TOTAL-ASSIGNED-PREMIUMS - REGION-WRONG-COUNT
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Appm223.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING MAP 'APPM223'
                                                                                                                                                                          //Natural: PERFORM PUT-CHECKPOINT-DATE
        sub_Put_Checkpoint_Date();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-RTN
        //*  ----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MAIN-RTN
        //*  ----------------MAIN LOOP READ PRAP---------------------------------
        //*  ----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECKPOINT-DATE
        //*  ----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PUT-CHECKPOINT-DATE
        //*  ----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PUT-DISPLAY-COUNTS
        //*  ----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROGRAM-PRODUCT-COUNTS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TEAM-DSCRPTN
    }
    private void sub_Init_Rtn() throws Exception                                                                                                                          //Natural: INIT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------INITIALIZE-----------------------------------------
        gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Ap_Prap_Key().reset();                                                                                              //Natural: RESET #AP-PRAP-KEY
        gdaAppg225.getRoman_Global_Display_Prev_Contract().getValue("*").setValue("99999999");                                                                            //Natural: MOVE '99999999' TO DISPLAY-PREV-CONTRACT ( * )
        gdaAppg225.getRoman_Global_Display_Curr_Contract().getValue("*").setValue("A0000000");                                                                            //Natural: MOVE 'A0000000' TO DISPLAY-CURR-CONTRACT ( * )
                                                                                                                                                                          //Natural: PERFORM GET-CHECKPOINT-DATE
        sub_Get_Checkpoint_Date();
        if (condition(Global.isEscape())) {return;}
        //*  --------------- PARMAREA ------------------------------------------
        pdaAppa020.getPnd_Parmdata_Pnd_End_Date().setValue(Global.getDATN());                                                                                             //Natural: MOVE *DATN TO #END-DATE
        //*                                                            /* START LS1
        pnd_Wk_Start_Date.setValueEdited(pnd_Wk_Start_Date_D,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #WK-START-DATE-D ( EM = YYYYMMDD ) TO #WK-START-DATE
        if (condition(pnd_Wk_Start_Date.greater(pdaAppa020.getPnd_Parmdata_Pnd_End_Date())))                                                                              //Natural: IF #WK-START-DATE > #END-DATE
        {
            pnd_Wk_Start_Date_D.nsubtract(1);                                                                                                                             //Natural: COMPUTE #WK-START-DATE-D = #WK-START-DATE-D - 1
            pnd_Wk_Start_Date.setValueEdited(pnd_Wk_Start_Date_D,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED #WK-START-DATE-D ( EM = YYYYMMDD ) TO #WK-START-DATE
            //*  END  LS1
        }                                                                                                                                                                 //Natural: END-IF
        pdaAppa020.getPnd_Parmdata_Pnd_Get_Cntrct_Flag().setValue("GET LAST CNTRCT");                                                                                     //Natural: ASSIGN #GET-CNTRCT-FLAG = 'GET LAST CNTRCT'
        pdaAppa020.getPnd_Parmdata_Pnd_Accum_Buckets_Flag().setValue("GET NAMED BUCKS");                                                                                  //Natural: ASSIGN #ACCUM-BUCKETS-FLAG = 'GET NAMED BUCKS'
        pdaAppa020.getPnd_Parmdata_Pnd_Use_Product_Code_Flag().setValue("GET NAMED PROD");                                                                                //Natural: ASSIGN #USE-PRODUCT-CODE-FLAG = 'GET NAMED PROD'
    }
    private void sub_Main_Rtn() throws Exception                                                                                                                          //Natural: MAIN-RTN
    {
        if (BLNatReinput.isReinput()) return;

        gdaAppg225.getVw_app_Prap_Rule_View().startDatabaseRead                                                                                                           //Natural: READ APP-PRAP-RULE-VIEW WITH AP-PRAP-KEY STARTING FROM #AP-PRAP-KEY
        (
        "READ2",
        new Wc[] { new Wc("AP_PRAP_KEY", ">=", gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Ap_Prap_Key().getBinary(), WcType.BY) },
        new Oc[] { new Oc("AP_PRAP_KEY", "ASC") }
        );
        READ2:
        while (condition(gdaAppg225.getVw_app_Prap_Rule_View().readNextRow("READ2")))
        {
            gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_Cnt().nadd(1);                                                                                             //Natural: ADD 1 TO #PRAP-CNT
            if (condition(gdaAppg225.getApp_Prap_Rule_View_Ap_Record_Type().equals(1)))                                                                                   //Natural: IF AP-RECORD-TYPE = 1
            {
                gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_Appl_Cnt().nadd(1);                                                                                    //Natural: ADD 1 TO #PRAP-APPL-CNT
                if (condition(gdaAppg225.getApp_Prap_Rule_View_Ap_Release_Ind().equals(1)))                                                                               //Natural: IF AP-RELEASE-IND = 1
                {
                    if (condition(gdaAppg225.getApp_Prap_Rule_View_Ap_Status().notEquals("A")))                                                                           //Natural: IF AP-STATUS <> 'A'
                    {
                        gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Cnt().nadd(1);                                                                        //Natural: ADD 1 TO #PRAP-NEW-APPL-CNT
                        gdaAppg225.getApp_Prap_Rule_View_Ap_Release_Ind().setValue(3);                                                                                    //Natural: MOVE 3 TO AP-RELEASE-IND
                        DbsUtil.terminateApplication("External Subroutine CREATE_T_12_APPS222_NEW is missing from the collection!");                                      //Natural: PERFORM CREATE-T-12-APPS222-NEW
                        DbsUtil.terminateApplication("External Subroutine CREATE_EXTR_BENE_APPS221_NEW is missing from the collection!");                                 //Natural: PERFORM CREATE-EXTR-BENE-APPS221-NEW
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_New_Appl_Del_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #PRAP-NEW-APPL-DEL-CNT
                        gdaAppg225.getApp_Prap_Rule_View_Ap_Release_Ind().setValue(4);                                                                                    //Natural: MOVE 4 TO AP-RELEASE-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_Old_Appl_Cnt().nadd(1);                                                                            //Natural: ADD 1 TO #PRAP-OLD-APPL-CNT
                    gdaAppg225.getApp_Prap_Rule_View_Ap_Release_Ind().setValue(4);                                                                                        //Natural: MOVE 4 TO AP-RELEASE-IND
                    DbsUtil.terminateApplication("External Subroutine CREATE_T_12_APPS222_NEW is missing from the collection!");                                          //Natural: PERFORM CREATE-T-12-APPS222-NEW
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(gdaAppg225.getApp_Prap_Rule_View_Ap_Record_Type().equals(2)))                                                                               //Natural: IF AP-RECORD-TYPE = 2
                {
                    gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_Prem_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #PRAP-PREM-CNT
                    if (condition(gdaAppg225.getApp_Prap_Rule_View_Ap_Release_Ind().equals(2)))                                                                           //Natural: IF AP-RELEASE-IND = 2
                    {
                        gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_New_Prem_Cnt().nadd(1);                                                                        //Natural: ADD 1 TO #PRAP-NEW-PREM-CNT
                        gdaAppg225.getApp_Prap_Rule_View_Ap_Release_Ind().setValue(4);                                                                                    //Natural: MOVE 4 TO AP-RELEASE-IND
                        DbsUtil.terminateApplication("External Subroutine CREATE_T_12_APPS222_NEW is missing from the collection!");                                      //Natural: PERFORM CREATE-T-12-APPS222-NEW
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_Old_Prem_Cnt().nadd(1);                                                                        //Natural: ADD 1 TO #PRAP-OLD-PREM-CNT
                        DbsUtil.terminateApplication("External Subroutine CREATE_T_12_APPS222_NEW is missing from the collection!");                                      //Natural: PERFORM CREATE-T-12-APPS222-NEW
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_Othr_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #PRAP-OTHR-CNT
                    DbsUtil.terminateApplication("External Subroutine CREATE_T_12_APPS222_NEW is missing from the collection!");                                          //Natural: PERFORM CREATE-T-12-APPS222-NEW
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROGRAM-PRODUCT-COUNTS
            sub_Program_Product_Counts();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prap_Updt_Cnt().nadd(1);                                                                                        //Natural: ADD 1 TO #PRAP-UPDT-CNT
            gdaAppg225.getVw_app_Prap_Rule_View().updateDBRow("READ2");                                                                                                   //Natural: UPDATE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  END TRANSACTION
    }
    private void sub_Get_Checkpoint_Date() throws Exception                                                                                                               //Natural: GET-CHECKPOINT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaAppa020.getPnd_Parmdata().reset();                                                                                                                             //Natural: RESET #PARMDATA
        pdaAppa020.getPnd_Parmdata_Pnd_Requestor().setValue("PDA1510");                                                                                                   //Natural: MOVE 'PDA1510' TO #PARMDATA.#REQUESTOR
        pdaAppa020.getPnd_Parmdata_Pnd_Get_Cntrct_Flag().setValue("GET LAST RUN DT");                                                                                     //Natural: MOVE 'GET LAST RUN DT' TO #PARMDATA.#GET-CNTRCT-FLAG
        DbsUtil.callnat(Appp030.class , getCurrentProcessState(), pdaAppa020.getPnd_Parmdata());                                                                          //Natural: CALLNAT 'APPP030' #PARMDATA
        if (condition(Global.isEscape())) return;
        //*  CHECK RETURN CODES HERE
        gdaAppg225.getPnd_Balancing_Report_Fields_D_Start_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pdaAppa020.getPnd_Parmdata_Pnd_Start_Date());              //Natural: MOVE EDITED #PARMDATA.#START-DATE TO D-START-DATE ( EM = YYYYMMDD )
        //*  LS1
        //*  LS1
        pnd_Wk_Start_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pdaAppa020.getPnd_Parmdata_Pnd_Start_Date());                                                   //Natural: MOVE EDITED #PARMDATA.#START-DATE TO #WK-START-DATE-D ( EM = YYYYMMDD )
        display_Map_Date.setValue(gdaAppg225.getPnd_Balancing_Report_Fields_D_Start_Date());                                                                              //Natural: MOVE D-START-DATE TO DISPLAY-MAP-DATE
        gdaAppg225.getPnd_Balancing_Report_Fields_D_Start_Date().nadd(1);                                                                                                 //Natural: ADD 1 TO D-START-DATE
        //*  LS1
        pnd_Wk_Start_Date_D.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #WK-START-DATE-D
        pdaAppa020.getPnd_Parmdata_Pnd_Start_Date().setValueEdited(gdaAppg225.getPnd_Balancing_Report_Fields_D_Start_Date(),new ReportEditMask("YYYYMMDD"));              //Natural: MOVE EDITED D-START-DATE ( EM = YYYYMMDD ) TO #PARMDATA.#START-DATE
        getReports().write(0, " LAST CHECKPOINT DATE = ",pdaAppa020.getPnd_Parmdata_Pnd_Start_Date());                                                                    //Natural: WRITE ' LAST CHECKPOINT DATE = ' #PARMDATA.#START-DATE
        if (Global.isEscape()) return;
    }
    private void sub_Put_Checkpoint_Date() throws Exception                                                                                                               //Natural: PUT-CHECKPOINT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaAppa010.getPnd_Parameter().reset();                                                                                                                            //Natural: RESET #PARAMETER
        pdaAppa010.getPnd_Parameter_Pnd_Requestor().setValue("PDA1510");                                                                                                  //Natural: MOVE 'PDA1510' TO #PARAMETER.#REQUESTOR
        pdaAppa010.getPnd_Parameter_Pnd_Get_Contract_Flag().setValue("PUT NEW RUN DT");                                                                                   //Natural: MOVE 'PUT NEW RUN DT' TO #PARAMETER.#GET-CONTRACT-FLAG
        pdaAppa010.getPnd_Parameter_Pnd_Start_Date().setValue(pdaAppa020.getPnd_Parmdata_Pnd_End_Date());                                                                 //Natural: MOVE #END-DATE TO #PARAMETER.#START-DATE
        DbsUtil.callnat(Appp020.class , getCurrentProcessState(), pdaAppa010.getPnd_Parameter());                                                                         //Natural: CALLNAT 'APPP020' #PARAMETER
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Appn300.class , getCurrentProcessState(), pdaAppa010.getPnd_Parameter());                                                                         //Natural: CALLNAT 'APPN300' #PARAMETER
        if (condition(Global.isEscape())) return;
        getReports().write(0, " NEW CHECKPOINT DATE = ",pdaAppa020.getPnd_Parmdata_Pnd_End_Date());                                                                       //Natural: WRITE ' NEW CHECKPOINT DATE = ' #END-DATE
        if (Global.isEscape()) return;
        getReports().write(0, " PROCESS REJECTED      ",gdaAppg225.getPnd_Balancing_Report_Fields_Process_Stat_Reject());                                                 //Natural: WRITE ' PROCESS REJECTED      ' PROCESS-STAT-REJECT
        if (Global.isEscape()) return;
    }
    private void sub_Put_Display_Counts() throws Exception                                                                                                                //Natural: PUT-DISPLAY-COUNTS
    {
        if (BLNatReinput.isReinput()) return;

        DbsUtil.examine(new ExamineSource(gdaAppg225.getRoman_Global_Display_Product().getValue("*"),true), new ExamineSearch(gdaAppg225.getRoman_Global_Pnd_Cnt_Product_Group().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub()),  //Natural: EXAMINE FULL DISPLAY-PRODUCT ( * ) FULL #CNT-PRODUCT-GROUP ( #PROD-SUB ) WITH DELIMITER ' ' GIVING INDEX #PROD-VAL
            true), new ExamineGivingIndex(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Val()));
        if (condition(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Val().equals(getZero())))                                                                        //Natural: IF #PROD-VAL = 0
        {
            gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Group_Val().nadd(1);                                                                                            //Natural: ADD 1 TO #GROUP-VAL
            gdaAppg225.getRoman_Global_Display_Product().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Group_Val()).setValue(gdaAppg225.getRoman_Global_Pnd_Cnt_Product_Group().getValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Sub())); //Natural: MOVE #CNT-PRODUCT-GROUP ( #PROD-SUB ) TO DISPLAY-PRODUCT ( #GROUP-VAL )
            gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Val().setValue(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Group_Val());                                 //Natural: MOVE #GROUP-VAL TO #PROD-VAL
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Program_Product_Counts() throws Exception                                                                                                            //Natural: PROGRAM-PRODUCT-COUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------DETERMINE COUNTERS ----------------------------------
        //*  DETERMINE BY EXAMINATION OF PRAP RECORD, WHAT THE PROCESS-STATUS IS,
        //*  WHAT THE PRODUCT-CODE IS.
        //*  EAC (SINGLETON)
        gdaAppg225.getPnd_Balancing_Report_Fields_Contract_Pfx().setValue(gdaAppg225.getApp_Prap_Rule_View_Ap_Pref_New());                                                //Natural: MOVE APP-PRAP-RULE-VIEW.AP-PREF-NEW TO CONTRACT-PFX
        //*  EAC (SINGLETON)
        gdaAppg225.getPnd_Balancing_Report_Fields_H_Contract_Pfx().setValue(gdaAppg225.getApp_Prap_Rule_View_Ap_Pref_New());                                              //Natural: MOVE APP-PRAP-RULE-VIEW.AP-PREF-NEW TO H-CONTRACT-PFX
        //*  EAC(SINGLETON)
        gdaAppg225.getPnd_Balancing_Report_Fields_H_Contract_Num7().setValue(gdaAppg225.getApp_Prap_Rule_View_Ap_Cont_New());                                             //Natural: MOVE APP-PRAP-RULE-VIEW.AP-CONT-NEW TO H-CONTRACT-NUM7
        gdaAppg225.getPnd_Balancing_Report_Fields_Contract_Num().setValue(gdaAppg225.getPnd_Balancing_Report_Fields_H_Contract_Num());                                    //Natural: MOVE H-CONTRACT-NUM TO CONTRACT-NUM
        DbsUtil.callnat(Appp925.class , getCurrentProcessState(), gdaAppg225.getPnd_Balancing_Report_Fields_Contract7(), gdaAppg225.getPnd_Balancing_Report_Fields_Contract_Info()); //Natural: CALLNAT 'APPP925' CONTRACT7 CONTRACT-INFO
        if (condition(Global.isEscape())) return;
        DbsUtil.examine(new ExamineSource(gdaAppg225.getRoman_Global_Pnd_Cnt_Product_Code().getValue("*"),true), new ExamineSearch(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Contract_Prod_Code(),  //Natural: EXAMINE FULL #CNT-PRODUCT-CODE ( * ) FULL #CONTRACT-PROD-CODE WITH DELIMITER ' ' GIVING INDEX #PROD-VAL
            true), new ExamineGivingIndex(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Val()));
        if (condition(gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Prod_Val().notEquals(getZero())))                                                                     //Natural: IF #PROD-VAL <> 0
        {
            if (condition(gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals(" ") || gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals("0")            //Natural: IF APP-PRAP-RULE-VIEW.AP-PROCESS-STATUS = ' ' OR = '0' OR = '1' OR = '2' OR = '3' OR = '4' OR = '5' OR = '6'
                || gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals("1") || gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals("2") 
                || gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals("3") || gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals("4") 
                || gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals("5") || gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status().equals("6")))
            {
                gdaAppg225.getPnd_Balancing_Report_Fields_Process_Stat_Reject().nadd(1);                                                                                  //Natural: ADD 1 TO PROCESS-STAT-REJECT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat().setValue(gdaAppg225.getApp_Prap_Rule_View_Ap_Process_Status());                                //Natural: MOVE APP-PRAP-RULE-VIEW.AP-PROCESS-STATUS TO HOLD-PROC-STAT
                if (condition(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need().equals("P")))                                                               //Natural: IF HOLD-PROC-STAT-NEED = 'P'
                {
                    gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need().setValue("R");                                                                        //Natural: MOVE 'R' TO HOLD-PROC-STAT-NEED
                }                                                                                                                                                         //Natural: END-IF
                //*  LS1
                if (condition((((DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg(),"1-2") && (DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need(),"'R'")  //Natural: IF ( HOLD-PROC-STAT-REG = MASK ( 1-2 ) AND ( HOLD-PROC-STAT-NEED = MASK ( 'R' ) OR HOLD-PROC-STAT-NEED = MASK ( 'S' ) ) AND HOLD-PROC-STAT-NUM = MASK ( '0'1-8 ) ) OR ( HOLD-PROC-STAT-REG = MASK ( A ) AND ( HOLD-PROC-STAT-NEED = MASK ( 1-9 ) OR HOLD-PROC-STAT-NEED = MASK ( A ) ) AND HOLD-PROC-STAT-NUM = MASK ( '0'1-8 ) )
                    || DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need(),"'S'"))) && DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num(),"'0'1-8")) 
                    || ((DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg(),"A") && (DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need(),"1-9") 
                    || DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need(),"A"))) && DbsUtil.maskMatches(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num(),
                    "'0'1-8")))))
                {
                    if (condition(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num().equals("03")))                                                           //Natural: IF HOLD-PROC-STAT-NUM = '03'
                    {
                        gdaAppg225.getPnd_Balancing_Report_Fields_Total_Assigned_Premiums().nadd(1);                                                                      //Natural: ADD 1 TO TOTAL-ASSIGNED-PREMIUMS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition((gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num().equals("04") || gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num().equals("07")  //Natural: IF ( HOLD-PROC-STAT-NUM = '04' OR HOLD-PROC-STAT-NUM = '07' OR HOLD-PROC-STAT-NUM = '08' )
                            || gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num().equals("08"))))
                        {
                            gdaAppg225.getPnd_Balancing_Report_Fields_Total_Issued_Applications().nadd(1);                                                                //Natural: ADD 1 TO TOTAL-ISSUED-APPLICATIONS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num().equals("05")))                                                   //Natural: IF HOLD-PROC-STAT-NUM = '05'
                            {
                                gdaAppg225.getPnd_Balancing_Report_Fields_Region_Wrong_Count().nadd(1);                                                                   //Natural: ADD 1 TO REGION-WRONG-COUNT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-TEAM-DSCRPTN
                    sub_Get_Team_Dscrptn();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  LS1
                //*  LS1
                getWorkFiles().write(5, false, gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg(), gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need(),  //Natural: WRITE WORK FILE 5 HOLD-PROC-STAT-REG HOLD-PROC-STAT-NEED HOLD-PROC-STAT-NUM #CONTRACT-PROD-CODE DISPLAY-MAP-DATE H-CONTRACT-NUM #TEAM-DISC #ENTRY-BUCKET #ENTRY-ACCUM #WK-START-DATE #PARMDATA.#END-DATE
                    gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Num(), gdaAppg225.getPnd_Balancing_Report_Fields_Pnd_Contract_Prod_Code(), 
                    display_Map_Date, gdaAppg225.getPnd_Balancing_Report_Fields_H_Contract_Num(), pnd_Team_Disc, pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket, 
                    pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum, pnd_Wk_Start_Date, pdaAppa020.getPnd_Parmdata_Pnd_End_Date());
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  LS1
        pnd_Team_Disc.reset();                                                                                                                                            //Natural: RESET #TEAM-DISC #ENTRY-DSCRPTN-TXT
        pnd_Entry_Dscrptn_Txt.reset();
    }
    private void sub_Get_Team_Dscrptn() throws Exception                                                                                                                  //Natural: GET-TEAM-DSCRPTN
    {
        if (BLNatReinput.isReinput()) return;

        //*  LS2
        //*  LS2
        if (condition(((gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg().equals("1") || gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg().equals("2"))  //Natural: IF ( HOLD-PROC-STAT-REG = '1' OR = '2' ) AND ( HOLD-PROC-STAT-NEED = 'R' OR = 'S' )
            && (gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need().equals("R") || gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need().equals("S")))))
        {
            pnd_Team_Disc.setValue("REGULAR ");                                                                                                                           //Natural: MOVE 'REGULAR ' TO #TEAM-DISC
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------CHANGED TO FORCE SORT -----------------------KH1 2-99
        //*   IF HOLD-PROC-STAT-NEED = 'S'
        //*    MOVE 'REGULAR ' TO #TEAM-DISC
        //*    MOVE 'SPECIAL ' TO #TEAM-DISC
        //*   END-IF
        //*  LS1
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Bucket.setValue("BU");                                                                                                            //Natural: ASSIGN #ENTRY-BUCKET = 'BU'
        //*  LS1
        pnd_Entry_Dscrptn_Txt_Pnd_Entry_Accum.setValue("AC");                                                                                                             //Natural: ASSIGN #ENTRY-ACCUM = 'AC'
        //*  IF HOLD-PROC-STAT-REG = 'T' AND                                /* LS1
        //*  LS1
        if (condition(pnd_Team_Disc.equals("  ")))                                                                                                                        //Natural: IF #TEAM-DISC = '  '
        {
            pnd_Premium_Team_Code_Pnd_Prem_Team_Cd.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Reg(),  //Natural: COMPRESS HOLD-PROC-STAT-REG HOLD-PROC-STAT-NEED INTO #PREM-TEAM-CD LEAVING NO SPACE
                gdaAppg225.getPnd_Balancing_Report_Fields_Hold_Proc_Stat_Need()));
            pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr.setValue(100);                                                                                                  //Natural: ASSIGN #ENTRY-TABLE-ID-NBR = 000100
            pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id.setValue("TM");                                                                                                 //Natural: ASSIGN #ENTRY-TABLE-SUB-ID = 'TM'
            pnd_Table_Id_Entry_Cde_Pnd_Entry_Cde.setValue("       ");                                                                                                     //Natural: ASSIGN #ENTRY-CDE = '       '
            ldaAcil7070.getVw_app_Table_Entry_View().startDatabaseRead                                                                                                    //Natural: READ APP-TABLE-ENTRY-VIEW BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-ID-ENTRY-CDE
            (
            "READ01",
            new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Id_Entry_Cde, WcType.BY) },
            new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
            );
            READ01:
            while (condition(ldaAcil7070.getVw_app_Table_Entry_View().readNextRow("READ01")))
            {
                if (condition(ldaAcil7070.getApp_Table_Entry_View_Entry_Table_Id_Nbr().notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Id_Nbr) || ldaAcil7070.getApp_Table_Entry_View_Entry_Table_Sub_Id().notEquals(pnd_Table_Id_Entry_Cde_Pnd_Entry_Table_Sub_Id))) //Natural: IF ENTRY-TABLE-ID-NBR NE #ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #ENTRY-TABLE-SUB-ID
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAcil7070.getApp_Table_Entry_View_Entry_Team_Cde().equals(pnd_Premium_Team_Code_Pnd_Prem_Team_Cd)))                                       //Natural: IF ENTRY-TEAM-CDE = #PREM-TEAM-CD
                {
                    pnd_Team_Disc.setValue(ldaAcil7070.getApp_Table_Entry_View_Entry_Team());                                                                             //Natural: ASSIGN #TEAM-DISC = ENTRY-TEAM
                    //*  LS1
                    pnd_Entry_Dscrptn_Txt.setValue(ldaAcil7070.getApp_Table_Entry_View_Entry_Dscrptn_Txt());                                                              //Natural: ASSIGN #ENTRY-DSCRPTN-TXT = ENTRY-DSCRPTN-TXT
                    //*  LS1
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*       ELSE                                                      /* LS1
                    //*          ESCAPE TOP                                             /* LS1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   IF HOLD-PROC-STAT-REG = 'T' AND                               /* LS1
        //*  LS1
        if (condition(pnd_Team_Disc.equals("  ")))                                                                                                                        //Natural: IF #TEAM-DISC = '  '
        {
            pnd_Team_Disc.setValue("UNKNOWN");                                                                                                                            //Natural: MOVE 'UNKNOWN' TO #TEAM-DISC
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
