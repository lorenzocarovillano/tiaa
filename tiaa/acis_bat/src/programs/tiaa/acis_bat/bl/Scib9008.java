/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:58 PM
**        * FROM NATURAL PROGRAM : Scib9008
************************************************************
**        * FILE NAME            : Scib9008.java
**        * CLASS NAME           : Scib9008
**        * INSTANCE NAME        : Scib9008
************************************************************
*********************************************************************
* PROGRAM      : SCIB9008                                           *
* DESCRIPTION  : GET GUARANTEE RATE FROM SYS105 FILE                *
* ----------------------------------------------------------------- *
* 03/16/2011  J BERGHEISER  FOR RATE LOOKUP FOR PRAP MOVE FORMATTED *
*                           TIAA DATE TO $SYS105-DATE (JRB1)        *
* 05/07/2011  J BERGHEISER  MOVE ET COUNT CLOSER TO UPDATE LOGIC    *
* 07/18/2013  L SHU         INCREASE COUNTER SIZE TO HANDLE > 300K  *
*                           FOR IRA SUBSTITUTION WEEKEND  - TNGSUB  *
* 05/23/2017  ELLO          PIN EXPANSION CHANGES                   *
*                           CHANGED TO POINT TO NEW PRAP VIEW (PINE)*
* 01/01/2021  JENABI        HARD CODE '0202' IN PLACE OF INCEPTION  *
*                           YEAR TO ALLOW TODAY'S RUN OF P1020NID   *
*                           SEE - INC5993528                        *
* 01/03/2021  RJ FRANKLIN   REMOVE TEMP FIX OF HARD CODED '0202' AND*
*                           USE NEWLY CREATED DDM FOR ACIS'S VIEW   *
*                           OF THE RATE FACTOR FILE.                *
*                           ADMINISTRATOR CREATED NEW FILE AND NEW  *
*                           DDM FOR THIS PROGRAM                    *
*                           SEE - INC5993528                        *
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9008 extends BLNatBase
{
    // Data Areas
    private LdaY2datebw ldaY2datebw;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_View;
    private DbsField annty_View_Ap_Lob;
    private DbsField annty_View_Ap_T_Doi;
    private DbsField annty_View_Ap_Lob_Type;
    private DbsField annty_View_Ap_Release_Ind;
    private DbsField annty_View_Ap_Cor_Last_Nme;
    private DbsField annty_View_Ap_Cor_First_Nme;
    private DbsField annty_View_Ap_Financial_1;
    private DbsField annty_View_Ap_Tiaa_Cntrct;
    private DbsField annty_View_Ap_Cref_Cert;

    private DataAccessProgramView vw_dfact_1;
    private DbsField dfact_1_Record_Key;

    private DbsGroup dfact_1_Fund_Control_Record;

    private DbsGroup dfact_1_Fund_Factor_Date;
    private DbsField dfact_1_Fund_Factor_Year;
    private DbsField dfact_1_Fund_Factor_Month;
    private DbsField dfact_1_Fund_Factor_Day;

    private DbsGroup dfact_1_Fund_Pre_Format_Date;
    private DbsField dfact_1_Fund_Pre_Format_Year;
    private DbsField dfact_1_Fund_Pre_Format_Month;
    private DbsField dfact_1_Fund_Pre_Format_Day;

    private DataAccessProgramView vw_dfact_2;
    private DbsField dfact_2_Record_Key;

    private DbsGroup dfact_2__R_Field_1;
    private DbsField dfact_2_Filler;
    private DbsField dfact_2_Rec_Date;
    private DbsField dfact_2_Pnd_Rate_Basis;
    private DbsField dfact_2_Pnd_Product;
    private DbsField dfact_2_Record_Type;
    private DbsField dfact_2_Filler_1;
    private DbsField dfact_2_Rate_Type;
    private DbsField dfact_2_Inception_Year;
    private DbsField dfact_2_Inception_Month;

    private DataAccessProgramView vw_dfact_3;
    private DbsField dfact_3_Record_Key;

    private DbsGroup dfact_3__R_Field_2;
    private DbsField dfact_3_Fill;
    private DbsField dfact_3_Pnd_Rate_B;
    private DbsField dfact_3_Pnd_Prod;
    private DbsField dfact_3_Record_Type;
    private DbsField dfact_3_Filler_1;
    private DbsField dfact_3_Annual_Interest_Rate;
    private DbsField dfact_3_Rb_Into_Which_Divs_Roll;
    private DbsField dfact_3_Conversion_Period;
    private DbsField dfact_3_Guar_Interest_Rate;
    private DbsField pnd_Sys105_Key;

    private DbsGroup pnd_Sys105_Key__R_Field_3;
    private DbsField pnd_Sys105_Key_Pnd_Sys105_Fund;
    private DbsField pnd_Sys105_Key_Pnd_Sys105_Date;
    private DbsField pnd_Sys105_Key_Pnd_Sys105_Rb;
    private DbsField pnd_Sys105_Key_Pnd_Sys105_Prod;
    private DbsField pnd_Guarantee_Rate_Rb_View;

    private DbsGroup pnd_Guarantee_Rate_Rb_View__R_Field_4;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Rec_Key;

    private DbsGroup pnd_Guarantee_Rate_Rb_View__R_Field_5;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Fund_Code;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Date;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Fil1;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Product;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Rate_Type;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Filler1;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Rate_Basic;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Year;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Month;
    private DbsField pnd_Guarantee_Rate_Rb_View_Gr_Filler2;
    private DbsField pnd_Date_Hold_Yyyymmdd;

    private DbsGroup pnd_Date_Hold_Yyyymmdd__R_Field_6;
    private DbsField pnd_Date_Hold_Yyyymmdd_Pnd_Date_Cc;
    private DbsField pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yymmdd;

    private DbsGroup pnd_Date_Hold_Yyyymmdd__R_Field_7;
    private DbsField pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Date_Hold_Yyyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Date_Hold_Yyyymmdd_Pnd_Date_Dd;

    private DbsGroup pnd_Date_Hold_Yyyymmdd__R_Field_8;
    private DbsField pnd_Date_Hold_Yyyymmdd_Pnd_Date_Ccyymm;
    private DbsField pnd_Date_Hold_Yyyymmdd_Pnd_Date_Filler;
    private DbsField pnd_Doi_Hold_Mmyy;

    private DbsGroup pnd_Doi_Hold_Mmyy__R_Field_9;
    private DbsField pnd_Doi_Hold_Mmyy_Pnd_Doi_Mm;
    private DbsField pnd_Doi_Hold_Mmyy_Pnd_Doi_Yy;

    private DbsGroup pnd_Work_Table;

    private DbsGroup pnd_Work_Table_Pnd_Work_Entry;

    private DbsGroup pnd_Work_Table_Pnd_Rate_Info;
    private DbsField pnd_Work_Table_Pnd_Tbl_Date;
    private DbsField pnd_Work_Table_Pnd_Tbl_Rate;
    private DbsField pnd_Current_Date;
    private DbsField pnd_Update_Isn;
    private DbsField pnd_Rate_In_Table;
    private DbsField pnd_Rate_On_File;
    private DbsField pnd_I;
    private DbsField pnd_Wk_Contract;
    private DbsField pnd_Print_Ssn;
    private DbsField pnd_Wk_Ssn;

    private DbsGroup pnd_Wk_Ssn__R_Field_10;
    private DbsField pnd_Wk_Ssn_Pnd_Wk_Ssn3;
    private DbsField pnd_Wk_Ssn_Pnd_Wk_Ssn2;
    private DbsField pnd_Wk_Ssn_Pnd_Wk_Ssn4;
    private DbsField pnd_Et_Read_Cnt;
    private DbsField pnd_Wk_Rate;
    private DbsField pnd_Table_Cnt;
    private DbsField pnd_Sys105_Cnt;
    private DbsField pnd_Contract_Cnt;
    private DbsField pnd_Contract_Read;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_For_Curr_Date;
    private DbsField pnd_Work_Date;

    private DbsGroup pnd_Work_Date__R_Field_11;
    private DbsField pnd_Work_Date_Pnd_Wk_Ccyy;
    private DbsField pnd_Work_Date_Pnd_Wk_Mm;
    private DbsField pnd_Work_Date_Pnd_Wk_Dd;
    private DbsField pnd_Tot_Yes;
    private DbsField pnd_Tot_No;
    private DbsField pnd_Tot_Cnt;
    private DbsField pnd_Zero;
    private DbsField pnd_Tiaa_Cntrct;
    private DbsField pnd_Cust_Name;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaY2datebw = new LdaY2datebw();
        registerRecord(ldaY2datebw);

        // Local Variables
        localVariables = new DbsRecord();

        vw_annty_View = new DataAccessProgramView(new NameInfo("vw_annty_View", "ANNTY-VIEW"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        annty_View_Ap_Lob = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB");
        annty_View_Ap_Lob.setDdmHeader("LOB");
        annty_View_Ap_T_Doi = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, 
            "AP_T_DOI");
        annty_View_Ap_Lob_Type = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB_TYPE");
        annty_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_View_Ap_Release_Ind = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "AP_RELEASE_IND");
        annty_View_Ap_Cor_Last_Nme = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_View_Ap_Cor_First_Nme = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", FieldType.STRING, 30, 
            RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_View_Ap_Financial_1 = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Financial_1", "AP-FINANCIAL-1", FieldType.NUMERIC, 10, 2, 
            RepeatingFieldStrategy.None, "AP_FINANCIAL_1");
        annty_View_Ap_Tiaa_Cntrct = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_TIAA_CNTRCT");
        annty_View_Ap_Cref_Cert = vw_annty_View.getRecord().newFieldInGroup("annty_View_Ap_Cref_Cert", "AP-CREF-CERT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "AP_CREF_CERT");
        registerRecord(vw_annty_View);

        vw_dfact_1 = new DataAccessProgramView(new NameInfo("vw_dfact_1", "DFACT-1"), "ACT_DAILY_FACTOR_FUND_CONTROL", "ACT_DAILY_FACTOR_FUND_CONTROL");
        dfact_1_Record_Key = vw_dfact_1.getRecord().newFieldInGroup("dfact_1_Record_Key", "RECORD-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "RECORD_KEY");

        dfact_1_Fund_Control_Record = vw_dfact_1.getRecord().newGroupInGroup("DFACT_1_FUND_CONTROL_RECORD", "FUND-CONTROL-RECORD");

        dfact_1_Fund_Factor_Date = dfact_1_Fund_Control_Record.newGroupInGroup("DFACT_1_FUND_FACTOR_DATE", "FUND-FACTOR-DATE");
        dfact_1_Fund_Factor_Year = dfact_1_Fund_Factor_Date.newFieldInGroup("dfact_1_Fund_Factor_Year", "FUND-FACTOR-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "FUND_FACTOR_YEAR");
        dfact_1_Fund_Factor_Month = dfact_1_Fund_Factor_Date.newFieldInGroup("dfact_1_Fund_Factor_Month", "FUND-FACTOR-MONTH", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "FUND_FACTOR_MONTH");
        dfact_1_Fund_Factor_Day = dfact_1_Fund_Factor_Date.newFieldInGroup("dfact_1_Fund_Factor_Day", "FUND-FACTOR-DAY", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "FUND_FACTOR_DAY");

        dfact_1_Fund_Pre_Format_Date = dfact_1_Fund_Control_Record.newGroupInGroup("DFACT_1_FUND_PRE_FORMAT_DATE", "FUND-PRE-FORMAT-DATE");
        dfact_1_Fund_Pre_Format_Year = dfact_1_Fund_Pre_Format_Date.newFieldInGroup("dfact_1_Fund_Pre_Format_Year", "FUND-PRE-FORMAT-YEAR", FieldType.NUMERIC, 
            4, RepeatingFieldStrategy.None, "FUND_PRE_FORMAT_YEAR");
        dfact_1_Fund_Pre_Format_Month = dfact_1_Fund_Pre_Format_Date.newFieldInGroup("dfact_1_Fund_Pre_Format_Month", "FUND-PRE-FORMAT-MONTH", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "FUND_PRE_FORMAT_MONTH");
        dfact_1_Fund_Pre_Format_Day = dfact_1_Fund_Pre_Format_Date.newFieldInGroup("dfact_1_Fund_Pre_Format_Day", "FUND-PRE-FORMAT-DAY", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "FUND_PRE_FORMAT_DAY");
        registerRecord(vw_dfact_1);

        vw_dfact_2 = new DataAccessProgramView(new NameInfo("vw_dfact_2", "DFACT-2"), "ACT_DAILY_FACTOR_TIAA_RATE_BASIZ", "ACT_DAILY_FACTOR_TIAA_RATE_BASIZ");
        dfact_2_Record_Key = vw_dfact_2.getRecord().newFieldInGroup("dfact_2_Record_Key", "RECORD-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "RECORD_KEY");

        dfact_2__R_Field_1 = vw_dfact_2.getRecord().newGroupInGroup("dfact_2__R_Field_1", "REDEFINE", dfact_2_Record_Key);
        dfact_2_Filler = dfact_2__R_Field_1.newFieldInGroup("dfact_2_Filler", "FILLER", FieldType.STRING, 1);
        dfact_2_Rec_Date = dfact_2__R_Field_1.newFieldInGroup("dfact_2_Rec_Date", "REC-DATE", FieldType.NUMERIC, 8);
        dfact_2_Pnd_Rate_Basis = dfact_2__R_Field_1.newFieldInGroup("dfact_2_Pnd_Rate_Basis", "#RATE-BASIS", FieldType.STRING, 2);
        dfact_2_Pnd_Product = dfact_2__R_Field_1.newFieldInGroup("dfact_2_Pnd_Product", "#PRODUCT", FieldType.STRING, 1);
        dfact_2_Record_Type = vw_dfact_2.getRecord().newFieldInGroup("dfact_2_Record_Type", "RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RECORD_TYPE");
        dfact_2_Filler_1 = vw_dfact_2.getRecord().newFieldInGroup("dfact_2_Filler_1", "FILLER-1", FieldType.STRING, 1, RepeatingFieldStrategy.None, "FILLER_1");
        dfact_2_Rate_Type = vw_dfact_2.getRecord().newFieldInGroup("dfact_2_Rate_Type", "RATE-TYPE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "RATE_TYPE");
        dfact_2_Inception_Year = vw_dfact_2.getRecord().newFieldInGroup("dfact_2_Inception_Year", "INCEPTION-YEAR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "INCEPTION_YEAR");
        dfact_2_Inception_Month = vw_dfact_2.getRecord().newFieldInGroup("dfact_2_Inception_Month", "INCEPTION-MONTH", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "INCEPTION_MONTH");
        registerRecord(vw_dfact_2);

        vw_dfact_3 = new DataAccessProgramView(new NameInfo("vw_dfact_3", "DFACT-3"), "ACT_DAILY_FACTOR_TIAA_BASIC_FCTR", "ACT_DAILY_FACTOR_TIAA_BASIC_FCTR");
        dfact_3_Record_Key = vw_dfact_3.getRecord().newFieldInGroup("dfact_3_Record_Key", "RECORD-KEY", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "RECORD_KEY");

        dfact_3__R_Field_2 = vw_dfact_3.getRecord().newGroupInGroup("dfact_3__R_Field_2", "REDEFINE", dfact_3_Record_Key);
        dfact_3_Fill = dfact_3__R_Field_2.newFieldInGroup("dfact_3_Fill", "FILL", FieldType.STRING, 9);
        dfact_3_Pnd_Rate_B = dfact_3__R_Field_2.newFieldInGroup("dfact_3_Pnd_Rate_B", "#RATE-B", FieldType.STRING, 2);
        dfact_3_Pnd_Prod = dfact_3__R_Field_2.newFieldInGroup("dfact_3_Pnd_Prod", "#PROD", FieldType.STRING, 1);
        dfact_3_Record_Type = vw_dfact_3.getRecord().newFieldInGroup("dfact_3_Record_Type", "RECORD-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RECORD_TYPE");
        dfact_3_Filler_1 = vw_dfact_3.getRecord().newFieldInGroup("dfact_3_Filler_1", "FILLER-1", FieldType.STRING, 1, RepeatingFieldStrategy.None, "FILLER_1");
        dfact_3_Annual_Interest_Rate = vw_dfact_3.getRecord().newFieldInGroup("dfact_3_Annual_Interest_Rate", "ANNUAL-INTEREST-RATE", FieldType.PACKED_DECIMAL, 
            5, 5, RepeatingFieldStrategy.None, "ANNUAL_INTEREST_RATE");
        dfact_3_Rb_Into_Which_Divs_Roll = vw_dfact_3.getRecord().newFieldInGroup("dfact_3_Rb_Into_Which_Divs_Roll", "RB-INTO-WHICH-DIVS-ROLL", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "RB_INTO_WHICH_DIVS_ROLL");
        dfact_3_Conversion_Period = vw_dfact_3.getRecord().newFieldInGroup("dfact_3_Conversion_Period", "CONVERSION-PERIOD", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "CONVERSION_PERIOD");
        dfact_3_Guar_Interest_Rate = vw_dfact_3.getRecord().newFieldInGroup("dfact_3_Guar_Interest_Rate", "GUAR-INTEREST-RATE", FieldType.PACKED_DECIMAL, 
            5, 5, RepeatingFieldStrategy.None, "GUAR_INTEREST_RATE");
        registerRecord(vw_dfact_3);

        pnd_Sys105_Key = localVariables.newFieldInRecord("pnd_Sys105_Key", "#SYS105-KEY", FieldType.STRING, 12);

        pnd_Sys105_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Sys105_Key__R_Field_3", "REDEFINE", pnd_Sys105_Key);
        pnd_Sys105_Key_Pnd_Sys105_Fund = pnd_Sys105_Key__R_Field_3.newFieldInGroup("pnd_Sys105_Key_Pnd_Sys105_Fund", "#SYS105-FUND", FieldType.STRING, 
            1);
        pnd_Sys105_Key_Pnd_Sys105_Date = pnd_Sys105_Key__R_Field_3.newFieldInGroup("pnd_Sys105_Key_Pnd_Sys105_Date", "#SYS105-DATE", FieldType.NUMERIC, 
            8);
        pnd_Sys105_Key_Pnd_Sys105_Rb = pnd_Sys105_Key__R_Field_3.newFieldInGroup("pnd_Sys105_Key_Pnd_Sys105_Rb", "#SYS105-RB", FieldType.STRING, 2);
        pnd_Sys105_Key_Pnd_Sys105_Prod = pnd_Sys105_Key__R_Field_3.newFieldInGroup("pnd_Sys105_Key_Pnd_Sys105_Prod", "#SYS105-PROD", FieldType.STRING, 
            1);
        pnd_Guarantee_Rate_Rb_View = localVariables.newFieldInRecord("pnd_Guarantee_Rate_Rb_View", "#GUARANTEE-RATE-RB-VIEW", FieldType.STRING, 30);

        pnd_Guarantee_Rate_Rb_View__R_Field_4 = localVariables.newGroupInRecord("pnd_Guarantee_Rate_Rb_View__R_Field_4", "REDEFINE", pnd_Guarantee_Rate_Rb_View);
        pnd_Guarantee_Rate_Rb_View_Gr_Rec_Key = pnd_Guarantee_Rate_Rb_View__R_Field_4.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Rec_Key", "GR-REC-KEY", 
            FieldType.STRING, 12);

        pnd_Guarantee_Rate_Rb_View__R_Field_5 = pnd_Guarantee_Rate_Rb_View__R_Field_4.newGroupInGroup("pnd_Guarantee_Rate_Rb_View__R_Field_5", "REDEFINE", 
            pnd_Guarantee_Rate_Rb_View_Gr_Rec_Key);
        pnd_Guarantee_Rate_Rb_View_Gr_Fund_Code = pnd_Guarantee_Rate_Rb_View__R_Field_5.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Fund_Code", "GR-FUND-CODE", 
            FieldType.STRING, 1);
        pnd_Guarantee_Rate_Rb_View_Gr_Date = pnd_Guarantee_Rate_Rb_View__R_Field_5.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Date", "GR-DATE", FieldType.NUMERIC, 
            8);
        pnd_Guarantee_Rate_Rb_View_Gr_Fil1 = pnd_Guarantee_Rate_Rb_View__R_Field_5.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Fil1", "GR-FIL1", FieldType.STRING, 
            2);
        pnd_Guarantee_Rate_Rb_View_Gr_Product = pnd_Guarantee_Rate_Rb_View__R_Field_5.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Product", "GR-PRODUCT", 
            FieldType.STRING, 1);
        pnd_Guarantee_Rate_Rb_View_Gr_Rate_Type = pnd_Guarantee_Rate_Rb_View__R_Field_4.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Rate_Type", "GR-RATE-TYPE", 
            FieldType.STRING, 1);
        pnd_Guarantee_Rate_Rb_View_Gr_Filler1 = pnd_Guarantee_Rate_Rb_View__R_Field_4.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Filler1", "GR-FILLER1", 
            FieldType.STRING, 1);
        pnd_Guarantee_Rate_Rb_View_Gr_Rate_Basic = pnd_Guarantee_Rate_Rb_View__R_Field_4.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Rate_Basic", "GR-RATE-BASIC", 
            FieldType.STRING, 2);
        pnd_Guarantee_Rate_Rb_View_Gr_Year = pnd_Guarantee_Rate_Rb_View__R_Field_4.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Year", "GR-YEAR", FieldType.STRING, 
            4);
        pnd_Guarantee_Rate_Rb_View_Gr_Month = pnd_Guarantee_Rate_Rb_View__R_Field_4.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Month", "GR-MONTH", 
            FieldType.STRING, 2);
        pnd_Guarantee_Rate_Rb_View_Gr_Filler2 = pnd_Guarantee_Rate_Rb_View__R_Field_4.newFieldInGroup("pnd_Guarantee_Rate_Rb_View_Gr_Filler2", "GR-FILLER2", 
            FieldType.STRING, 8);
        pnd_Date_Hold_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Hold_Yyyymmdd", "#DATE-HOLD-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Date_Hold_Yyyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Date_Hold_Yyyymmdd__R_Field_6", "REDEFINE", pnd_Date_Hold_Yyyymmdd);
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Cc = pnd_Date_Hold_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Date_Hold_Yyyymmdd_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 
            2);
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yymmdd = pnd_Date_Hold_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yymmdd", "#DATE-YYMMDD", 
            FieldType.NUMERIC, 6);

        pnd_Date_Hold_Yyyymmdd__R_Field_7 = pnd_Date_Hold_Yyyymmdd__R_Field_6.newGroupInGroup("pnd_Date_Hold_Yyyymmdd__R_Field_7", "REDEFINE", pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yymmdd);
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yy = pnd_Date_Hold_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 
            2);
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Mm = pnd_Date_Hold_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Date_Hold_Yyyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Dd = pnd_Date_Hold_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Date_Hold_Yyyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Date_Hold_Yyyymmdd__R_Field_8 = localVariables.newGroupInRecord("pnd_Date_Hold_Yyyymmdd__R_Field_8", "REDEFINE", pnd_Date_Hold_Yyyymmdd);
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Ccyymm = pnd_Date_Hold_Yyyymmdd__R_Field_8.newFieldInGroup("pnd_Date_Hold_Yyyymmdd_Pnd_Date_Ccyymm", "#DATE-CCYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Filler = pnd_Date_Hold_Yyyymmdd__R_Field_8.newFieldInGroup("pnd_Date_Hold_Yyyymmdd_Pnd_Date_Filler", "#DATE-FILLER", 
            FieldType.NUMERIC, 2);
        pnd_Doi_Hold_Mmyy = localVariables.newFieldInRecord("pnd_Doi_Hold_Mmyy", "#DOI-HOLD-MMYY", FieldType.NUMERIC, 4);

        pnd_Doi_Hold_Mmyy__R_Field_9 = localVariables.newGroupInRecord("pnd_Doi_Hold_Mmyy__R_Field_9", "REDEFINE", pnd_Doi_Hold_Mmyy);
        pnd_Doi_Hold_Mmyy_Pnd_Doi_Mm = pnd_Doi_Hold_Mmyy__R_Field_9.newFieldInGroup("pnd_Doi_Hold_Mmyy_Pnd_Doi_Mm", "#DOI-MM", FieldType.NUMERIC, 2);
        pnd_Doi_Hold_Mmyy_Pnd_Doi_Yy = pnd_Doi_Hold_Mmyy__R_Field_9.newFieldInGroup("pnd_Doi_Hold_Mmyy_Pnd_Doi_Yy", "#DOI-YY", FieldType.NUMERIC, 2);

        pnd_Work_Table = localVariables.newGroupInRecord("pnd_Work_Table", "#WORK-TABLE");

        pnd_Work_Table_Pnd_Work_Entry = pnd_Work_Table.newGroupArrayInGroup("pnd_Work_Table_Pnd_Work_Entry", "#WORK-ENTRY", new DbsArrayController(1, 
            100));

        pnd_Work_Table_Pnd_Rate_Info = pnd_Work_Table_Pnd_Work_Entry.newGroupInGroup("pnd_Work_Table_Pnd_Rate_Info", "#RATE-INFO");
        pnd_Work_Table_Pnd_Tbl_Date = pnd_Work_Table_Pnd_Rate_Info.newFieldInGroup("pnd_Work_Table_Pnd_Tbl_Date", "#TBL-DATE", FieldType.NUMERIC, 8);
        pnd_Work_Table_Pnd_Tbl_Rate = pnd_Work_Table_Pnd_Rate_Info.newFieldInGroup("pnd_Work_Table_Pnd_Tbl_Rate", "#TBL-RATE", FieldType.NUMERIC, 10, 
            2);
        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.NUMERIC, 8);
        pnd_Update_Isn = localVariables.newFieldInRecord("pnd_Update_Isn", "#UPDATE-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Rate_In_Table = localVariables.newFieldInRecord("pnd_Rate_In_Table", "#RATE-IN-TABLE", FieldType.BOOLEAN, 1);
        pnd_Rate_On_File = localVariables.newFieldInRecord("pnd_Rate_On_File", "#RATE-ON-FILE", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Wk_Contract = localVariables.newFieldInRecord("pnd_Wk_Contract", "#WK-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Print_Ssn = localVariables.newFieldInRecord("pnd_Print_Ssn", "#PRINT-SSN", FieldType.STRING, 11);
        pnd_Wk_Ssn = localVariables.newFieldInRecord("pnd_Wk_Ssn", "#WK-SSN", FieldType.NUMERIC, 9);

        pnd_Wk_Ssn__R_Field_10 = localVariables.newGroupInRecord("pnd_Wk_Ssn__R_Field_10", "REDEFINE", pnd_Wk_Ssn);
        pnd_Wk_Ssn_Pnd_Wk_Ssn3 = pnd_Wk_Ssn__R_Field_10.newFieldInGroup("pnd_Wk_Ssn_Pnd_Wk_Ssn3", "#WK-SSN3", FieldType.STRING, 3);
        pnd_Wk_Ssn_Pnd_Wk_Ssn2 = pnd_Wk_Ssn__R_Field_10.newFieldInGroup("pnd_Wk_Ssn_Pnd_Wk_Ssn2", "#WK-SSN2", FieldType.STRING, 2);
        pnd_Wk_Ssn_Pnd_Wk_Ssn4 = pnd_Wk_Ssn__R_Field_10.newFieldInGroup("pnd_Wk_Ssn_Pnd_Wk_Ssn4", "#WK-SSN4", FieldType.STRING, 4);
        pnd_Et_Read_Cnt = localVariables.newFieldInRecord("pnd_Et_Read_Cnt", "#ET-READ-CNT", FieldType.NUMERIC, 4);
        pnd_Wk_Rate = localVariables.newFieldInRecord("pnd_Wk_Rate", "#WK-RATE", FieldType.NUMERIC, 10, 2);
        pnd_Table_Cnt = localVariables.newFieldInRecord("pnd_Table_Cnt", "#TABLE-CNT", FieldType.NUMERIC, 7);
        pnd_Sys105_Cnt = localVariables.newFieldInRecord("pnd_Sys105_Cnt", "#SYS105-CNT", FieldType.NUMERIC, 7);
        pnd_Contract_Cnt = localVariables.newFieldInRecord("pnd_Contract_Cnt", "#CONTRACT-CNT", FieldType.NUMERIC, 6);
        pnd_Contract_Read = localVariables.newFieldInRecord("pnd_Contract_Read", "#CONTRACT-READ", FieldType.NUMERIC, 6);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.NUMERIC, 6);
        pnd_For_Curr_Date = localVariables.newFieldInRecord("pnd_For_Curr_Date", "#FOR-CURR-DATE", FieldType.STRING, 1);
        pnd_Work_Date = localVariables.newFieldInRecord("pnd_Work_Date", "#WORK-DATE", FieldType.NUMERIC, 8);

        pnd_Work_Date__R_Field_11 = localVariables.newGroupInRecord("pnd_Work_Date__R_Field_11", "REDEFINE", pnd_Work_Date);
        pnd_Work_Date_Pnd_Wk_Ccyy = pnd_Work_Date__R_Field_11.newFieldInGroup("pnd_Work_Date_Pnd_Wk_Ccyy", "#WK-CCYY", FieldType.STRING, 4);
        pnd_Work_Date_Pnd_Wk_Mm = pnd_Work_Date__R_Field_11.newFieldInGroup("pnd_Work_Date_Pnd_Wk_Mm", "#WK-MM", FieldType.STRING, 2);
        pnd_Work_Date_Pnd_Wk_Dd = pnd_Work_Date__R_Field_11.newFieldInGroup("pnd_Work_Date_Pnd_Wk_Dd", "#WK-DD", FieldType.STRING, 2);
        pnd_Tot_Yes = localVariables.newFieldInRecord("pnd_Tot_Yes", "#TOT-YES", FieldType.NUMERIC, 6);
        pnd_Tot_No = localVariables.newFieldInRecord("pnd_Tot_No", "#TOT-NO", FieldType.NUMERIC, 6);
        pnd_Tot_Cnt = localVariables.newFieldInRecord("pnd_Tot_Cnt", "#TOT-CNT", FieldType.NUMERIC, 6);
        pnd_Zero = localVariables.newFieldInRecord("pnd_Zero", "#ZERO", FieldType.PACKED_DECIMAL, 5);
        pnd_Tiaa_Cntrct = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Cust_Name = localVariables.newFieldInRecord("pnd_Cust_Name", "#CUST-NAME", FieldType.STRING, 40);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_View.reset();
        vw_dfact_1.reset();
        vw_dfact_2.reset();
        vw_dfact_3.reset();

        ldaY2datebw.initializeValues();

        localVariables.reset();
        pnd_Rate_In_Table.setInitialValue(false);
        pnd_Rate_On_File.setInitialValue(false);
        pnd_Table_Cnt.setInitialValue(0);
        pnd_Sys105_Cnt.setInitialValue(0);
        pnd_Update_Cnt.setInitialValue(0);
        pnd_Zero.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9008() throws Exception
    {
        super("Scib9008");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIB9008", onError);
        //* ******************************************************
        //*  MAIN PROCESS STARTS HERE
        //* ******************************************************
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TABLE
        sub_Initialize_Table();
        if (condition(Global.isEscape())) {return;}
        //*  GET RATE FOR CURRENT DATE AND SAVE IT IN A TABLE
        pnd_Date_Hold_Yyyymmdd.setValue(Global.getDATN());                                                                                                                //Natural: MOVE *DATN TO #DATE-HOLD-YYYYMMDD
        pnd_Date_Hold_Yyyymmdd_Pnd_Date_Dd.setValue(0);                                                                                                                   //Natural: MOVE 00 TO #DATE-DD
        pnd_Wk_Rate.reset();                                                                                                                                              //Natural: RESET #WK-RATE
                                                                                                                                                                          //Natural: PERFORM GET-RATE-BASIS
        sub_Get_Rate_Basis();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-RATE
        sub_Get_Rate();
        if (condition(Global.isEscape())) {return;}
        pnd_Sys105_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SYS105-CNT
                                                                                                                                                                          //Natural: PERFORM SAVE-RATE
        sub_Save_Rate();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "CURRENT RATE ",pnd_Sys105_Key_Pnd_Sys105_Rb,"=",pnd_Wk_Rate," ",pnd_Sys105_Key_Pnd_Sys105_Date);                                           //Natural: WRITE 'CURRENT RATE ' #SYS105-RB '=' #WK-RATE ' ' #SYS105-DATE
        if (Global.isEscape()) return;
        //* *********************************************************************
        //*  YR2000 COMPLIANT COPYCODE  ---> C.BROWN                            *
        //* *********************************************************************
        //*  Y2CHED SH CONVERTED FROM COBOL TO NATURAL --> E. DAVID 10/20/97
        //*                          Y 2 D A T E B P
        //* *******  BATCH DATE COMPARE ROUTINE FOR YR2000 PROCESSING  ******
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-3
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-4
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-DATE-COMPARE-5
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-3
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-4
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: YR2000-J-DATE-COMPARE-5
        //* ********  END Y2K BATCH DATE-COMPARE PROCEDURE COPYCODE  ********
        vw_annty_View.startDatabaseRead                                                                                                                                   //Natural: READ ANNTY-VIEW BY AP-RELEASE-IND STARTING FROM 1
        (
        "MAIN_RD",
        new Wc[] { new Wc("AP_RELEASE_IND", ">=", 1, WcType.BY) },
        new Oc[] { new Oc("AP_RELEASE_IND", "ASC") }
        );
        MAIN_RD:
        while (condition(vw_annty_View.readNextRow("MAIN_RD")))
        {
            if (condition(annty_View_Ap_Release_Ind.greater(1)))                                                                                                          //Natural: IF AP-RELEASE-IND > 1
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(annty_View_Ap_Financial_1.greater(getZero())))                                                                                                  //Natural: IF AP-FINANCIAL-1 > 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!((annty_View_Ap_Lob.equals("I") && ((annty_View_Ap_Lob_Type.equals("7") || annty_View_Ap_Lob_Type.equals("8")) || annty_View_Ap_Lob_Type.equals("9")))))) //Natural: ACCEPT IF AP-LOB = 'I' AND ( AP-LOB-TYPE = '7' OR = '8' OR = '9' )
            {
                continue;
            }
            pnd_Update_Isn.setValue(vw_annty_View.getAstISN("MAIN_RD"));                                                                                                  //Natural: ASSIGN #UPDATE-ISN := *ISN ( MAIN-RD. )
            //*  CONVERT ISSUE DATE TO FORMAT YYYYMMDD
            pnd_Doi_Hold_Mmyy.setValue(annty_View_Ap_T_Doi);                                                                                                              //Natural: MOVE AP-T-DOI TO #DOI-HOLD-MMYY
            pnd_Date_Hold_Yyyymmdd_Pnd_Date_Mm.setValue(pnd_Doi_Hold_Mmyy_Pnd_Doi_Mm);                                                                                    //Natural: MOVE #DOI-MM TO #DATE-MM
            pnd_Date_Hold_Yyyymmdd_Pnd_Date_Yy.setValue(pnd_Doi_Hold_Mmyy_Pnd_Doi_Yy);                                                                                    //Natural: MOVE #DOI-YY TO #DATE-YY
            pnd_Date_Hold_Yyyymmdd_Pnd_Date_Dd.setValue(0);                                                                                                               //Natural: MOVE 00 TO #DATE-DD
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().reset();                                                                                           //Natural: RESET #Y2-YYMMDD-AREA1-A
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().setValue(pnd_Doi_Hold_Mmyy_Pnd_Doi_Yy);                                                                  //Natural: MOVE #DOI-YY TO #Y2-YY-AREA1
                                                                                                                                                                          //Natural: PERFORM YR2000-DATE-COMPARE
            sub_Yr2000_Date_Compare();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Date_Hold_Yyyymmdd_Pnd_Date_Cc.setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1());                                                            //Natural: MOVE #Y2-CC-AREA1 TO #DATE-CC
            pnd_Contract_Read.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CONTRACT-READ
            //*  MOVE #DATE-HOLD-YYYYMMDD TO #WORK-DATE
            //*  JRB1
            pnd_Sys105_Key_Pnd_Sys105_Date.setValue(pnd_Date_Hold_Yyyymmdd);                                                                                              //Natural: MOVE #DATE-HOLD-YYYYMMDD TO #SYS105-DATE
            pnd_Tiaa_Cntrct.setValue(annty_View_Ap_Tiaa_Cntrct);                                                                                                          //Natural: MOVE AP-TIAA-CNTRCT TO #TIAA-CNTRCT
            pnd_Cust_Name.setValue(DbsUtil.compress(annty_View_Ap_Cor_First_Nme, " ", annty_View_Ap_Cor_Last_Nme));                                                       //Natural: COMPRESS AP-COR-FIRST-NME ' ' AP-COR-LAST-NME INTO #CUST-NAME
            pnd_Wk_Rate.reset();                                                                                                                                          //Natural: RESET #WK-RATE
            pnd_Rate_In_Table.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #RATE-IN-TABLE
            pnd_Rate_On_File.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #RATE-ON-FILE
                                                                                                                                                                          //Natural: PERFORM CHECK-SAVED-RATES
            sub_Check_Saved_Rates();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("MAIN_RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("MAIN_RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Rate_In_Table.getBoolean()))                                                                                                                //Natural: IF #RATE-IN-TABLE
            {
                pnd_Table_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TABLE-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GET-RATE-BASIS
                sub_Get_Rate_Basis();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-RATE
                sub_Get_Rate();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SAVE-RATE
                sub_Save_Rate();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Sys105_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #SYS105-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition((pnd_Rate_In_Table.getBoolean() || pnd_Rate_On_File.getBoolean())))                                                                             //Natural: IF ( #RATE-IN-TABLE OR #RATE-ON-FILE )
            {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CONTRACT
                sub_Update_Contract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MAIN_RD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "*** RUN TOTALS ***               ");                                                                                                       //Natural: WRITE '*** RUN TOTALS ***               '
        if (Global.isEscape()) return;
        getReports().write(0, "CALLS TO SYS105 FILE -------->>> ",pnd_Sys105_Cnt);                                                                                        //Natural: WRITE 'CALLS TO SYS105 FILE -------->>> ' #SYS105-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "RATES FOUND IN TABLE -------->>> ",pnd_Table_Cnt);                                                                                         //Natural: WRITE 'RATES FOUND IN TABLE -------->>> ' #TABLE-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "CONTRACTS UPDATED    -------->>> ",pnd_Update_Cnt);                                                                                        //Natural: WRITE 'CONTRACTS UPDATED    -------->>> ' #UPDATE-CNT
        if (Global.isEscape()) return;
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RATE-BASIS
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RATE
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-SAVED-RATES
        //* *******************************************************
        //* ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-TABLE
        //* ******************************************************
        //* ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SAVE-RATE
        //* ******************************************************
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CONTRACT
        //* ***************************************
        //* **************************************
        //*                                                                                                                                                               //Natural: ON ERROR
        //* **************************************
    }
    private void sub_Yr2000_Date_Compare() throws Exception                                                                                                               //Natural: YR2000-DATE-COMPARE
    {
        if (BLNatReinput.isReinput()) return;

        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Currccyymmdd().setValue(Global.getDATN());                                                                              //Natural: MOVE *DATN TO #Y2-CURRCCYYMMDD
        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9().setValue(9);                                                                                                    //Natural: MOVE 9 TO #Y2-DTE-9
        ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy().setValue(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Curryy());                                                  //Natural: MOVE #Y2-CURRYY TO #Y2-DTE-YY
        //*  60/40 RULE
        short decideConditionsMet531 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #Y2-DATE-RULE;//Natural: VALUE '1'
        if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("1"))))
        {
            decideConditionsMet531++;
            //*  80/20 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy60());                                              //Natural: SUBTRACT #Y2-YY60 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("2"))))
        {
            decideConditionsMet531++;
            //*  90/10 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy80());                                              //Natural: SUBTRACT #Y2-YY80 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Date_Rule().equals("3"))))
        {
            decideConditionsMet531++;
            //*  DEFAULT TO 70/30 RULE
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy90());                                              //Natural: SUBTRACT #Y2-YY90 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_9yy().nsubtract(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy70());                                              //Natural: SUBTRACT #Y2-YY70 FROM #Y2-DTE-9YY
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area1().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA1 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area1().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area2().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA2 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area2().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area2().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA2
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet562 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #Y2-YYMMDD-AREA1-A = '000000' OR = ' ' OR = '0000' OR = '0'
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals(" ") 
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area1_A().equals("0")))
        {
            decideConditionsMet562++;
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area1_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA1-9
        }                                                                                                                                                                 //Natural: WHEN #Y2-YYMMDD-AREA2-A = '000000' OR = ' ' OR = '0000' OR = '0'
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals(" ") 
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area2_A().equals("0")))
        {
            decideConditionsMet562++;
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area2_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA2-9
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet562 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Yr2000_Date_Compare_3() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-3
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area3().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA3 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area3().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area3().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA3
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA3-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area3_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area3_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA3-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_Date_Compare_4() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-4
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area4().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA4 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area4().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area4().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA4
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA4-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area4_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area4_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA4-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_Date_Compare_5() throws Exception                                                                                                             //Natural: YR2000-DATE-COMPARE-5
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yy_Area5().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                           //Natural: IF #Y2-YY-AREA5 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area5().setValue(19);                                                                                            //Natural: MOVE 19 TO #Y2-CC-AREA5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Cc_Area5().setValue(20);                                                                                            //Natural: MOVE 20 TO #Y2-CC-AREA5
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("000000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals(" ")  //Natural: IF #Y2-YYMMDD-AREA5-A = '000000' OR = ' ' OR = '0000' OR = '0'
            || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("0000") || ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Yymmdd_Area5_A().equals("0")))
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Ccyymmdd_Area5_9().setValue(0);                                                                                     //Natural: MOVE 0 TO #Y2-CCYYMMDD-AREA5-9
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare() throws Exception                                                                                                             //Natural: YR2000-J-DATE-COMPARE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area1().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA1 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area1().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area1().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area2().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA2 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area2().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area2().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA2
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_3() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-3
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area3().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA3 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area3().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area3().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA3
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_4() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-4
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area4().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA4 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area4().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area4().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA4
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Yr2000_J_Date_Compare_5() throws Exception                                                                                                           //Natural: YR2000-J-DATE-COMPARE-5
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Yy_Area5().greaterOrEqual(ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_Dte_Yy())))                         //Natural: IF #Y2-J-YY-AREA5 NOT < #Y2-DTE-YY
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area5().setValue(19);                                                                                          //Natural: MOVE 19 TO #Y2-J-CC-AREA5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaY2datebw.getPnd_Y2_Date_Wrkarea_Pnd_Y2_J_Cc_Area5().setValue(20);                                                                                          //Natural: MOVE 20 TO #Y2-J-CC-AREA5
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Rate_Basis() throws Exception                                                                                                                    //Natural: GET-RATE-BASIS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        pnd_Sys105_Key_Pnd_Sys105_Fund.setValue("9");                                                                                                                     //Natural: MOVE '9' TO #SYS105-FUND
        pnd_Sys105_Key_Pnd_Sys105_Date.setValue(pnd_Date_Hold_Yyyymmdd);                                                                                                  //Natural: MOVE #DATE-HOLD-YYYYMMDD TO #SYS105-DATE
        pnd_Sys105_Key_Pnd_Sys105_Rb.setValue("00");                                                                                                                      //Natural: MOVE '00' TO #SYS105-RB
        pnd_Sys105_Key_Pnd_Sys105_Prod.setValue("X");                                                                                                                     //Natural: MOVE 'X' TO #SYS105-PROD
        vw_dfact_2.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) DFACT-2 WITH RECORD-KEY = #SYS105-KEY
        (
        "FIND01",
        new Wc[] { new Wc("RECORD_KEY", "=", pnd_Sys105_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_dfact_2.readNextRow("FIND01", true)))
        {
            vw_dfact_2.setIfNotFoundControlFlag(false);
            if (condition(vw_dfact_2.getAstCOUNTER().equals(0)))                                                                                                          //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "********************************************************************");                                                            //Natural: WRITE '********************************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* SCIB9008 program is being controled terminated with TERMINATE(99)*");                                                            //Natural: WRITE '* SCIB9008 program is being controled terminated with TERMINATE(99)*'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* This is a result of SCIB9008 trying to read SYS105(factor file)  *");                                                            //Natural: WRITE '* This is a result of SCIB9008 trying to read SYS105(factor file)  *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* for the IRA Indexed rate and it NOT being found                  *");                                                            //Natural: WRITE '* for the IRA Indexed rate and it NOT being found                  *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Please refer to Doc-IT for direction on who to contact           *","********************************************************************"); //Natural: WRITE '* Please refer to Doc-IT for direction on who to contact           *' '********************************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "Contract number: ",pnd_Tiaa_Cntrct,"Customer Name: ",pnd_Cust_Name);                                                               //Natural: WRITE 'Contract number: ' #TIAA-CNTRCT 'Customer Name: ' #CUST-NAME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "Contract Issue date: ",pnd_Sys105_Key_Pnd_Sys105_Date);                                                                            //Natural: WRITE 'Contract Issue date: ' #SYS105-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-NOREC
            //*  /* START OF INC5993528 TAG
            getReports().write(0, " RECORD-KEY = ",dfact_2_Record_Key,"RECORD-TYPE = ",dfact_2_Record_Type,"FILLER-1 = ",dfact_2_Filler_1,"RATE-TYPE = ",                 //Natural: WRITE ' RECORD-KEY = ' RECORD-KEY 'RECORD-TYPE = ' RECORD-TYPE 'FILLER-1 = ' FILLER-1 'RATE-TYPE = ' RATE-TYPE / 'INCEPTION-YEAR = ' INCEPTION-YEAR 'INCEPTION-MONTH = ' INCEPTION-MONTH
                dfact_2_Rate_Type,NEWLINE,"INCEPTION-YEAR = ",dfact_2_Inception_Year,"INCEPTION-MONTH = ",dfact_2_Inception_Month);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  COMPRESS RECORD-KEY RECORD-TYPE FILLER-1 RATE-TYPE INCEPTION-YEAR
            //*    INCEPTION-MONTH INTO #GUARANTEE-RATE-RB-VIEW LEAVING NO
            pnd_Guarantee_Rate_Rb_View_Gr_Rec_Key.setValue(dfact_2_Record_Key);                                                                                           //Natural: MOVE RECORD-KEY TO GR-REC-KEY
            pnd_Guarantee_Rate_Rb_View_Gr_Rate_Type.setValue(dfact_2_Record_Type);                                                                                        //Natural: MOVE RECORD-TYPE TO GR-RATE-TYPE
            pnd_Guarantee_Rate_Rb_View_Gr_Filler1.setValue(dfact_2_Filler_1);                                                                                             //Natural: MOVE FILLER-1 TO GR-FILLER1
            pnd_Guarantee_Rate_Rb_View_Gr_Rate_Basic.setValue(dfact_2_Rate_Type);                                                                                         //Natural: MOVE RATE-TYPE TO GR-RATE-BASIC
            pnd_Guarantee_Rate_Rb_View_Gr_Year.setValue(dfact_2_Inception_Year);                                                                                          //Natural: MOVE INCEPTION-YEAR TO GR-YEAR
            pnd_Guarantee_Rate_Rb_View_Gr_Month.setValue(dfact_2_Inception_Month);                                                                                        //Natural: MOVE INCEPTION-MONTH TO GR-MONTH
            getReports().write(0, " #GUARANTEE-RATE-RB-VIEW  =  ",pnd_Guarantee_Rate_Rb_View);                                                                            //Natural: WRITE ' #GUARANTEE-RATE-RB-VIEW  =  ' #GUARANTEE-RATE-RB-VIEW
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  /* END OF INC5993528 TAG
            if (condition(pnd_Guarantee_Rate_Rb_View_Gr_Date.equals(pnd_Sys105_Key_Pnd_Sys105_Date) && pnd_Guarantee_Rate_Rb_View_Gr_Product.equals(pnd_Sys105_Key_Pnd_Sys105_Prod)  //Natural: IF GR-DATE = #SYS105-DATE AND GR-PRODUCT = #SYS105-PROD AND GR-RATE-BASIC NE '00'
                && pnd_Guarantee_Rate_Rb_View_Gr_Rate_Basic.notEquals("00")))
            {
                pnd_Sys105_Key_Pnd_Sys105_Rb.setValue(pnd_Guarantee_Rate_Rb_View_Gr_Rate_Basic);                                                                          //Natural: MOVE GR-RATE-BASIC TO #SYS105-RB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Rate() throws Exception                                                                                                                          //Natural: GET-RATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        vw_dfact_3.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) DFACT-3 WITH RECORD-KEY = #SYS105-KEY
        (
        "FIND02",
        new Wc[] { new Wc("RECORD_KEY", "=", pnd_Sys105_Key, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(vw_dfact_3.readNextRow("FIND02", true)))
        {
            vw_dfact_3.setIfNotFoundControlFlag(false);
            if (condition(vw_dfact_3.getAstCOUNTER().equals(0)))                                                                                                          //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "********************************************************************");                                                            //Natural: WRITE '********************************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* SCIB9008 program is being controled terminated with TERMINATE(99)*");                                                            //Natural: WRITE '* SCIB9008 program is being controled terminated with TERMINATE(99)*'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* This is a result of SCIB9008 trying to read SYS105(factor file)  *");                                                            //Natural: WRITE '* This is a result of SCIB9008 trying to read SYS105(factor file)  *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* for the IRA Indexed rate and it NOT being found                  *");                                                            //Natural: WRITE '* for the IRA Indexed rate and it NOT being found                  *'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "* Please refer to Doc-IT for direction on who to contact           *","********************************************************************"); //Natural: WRITE '* Please refer to Doc-IT for direction on who to contact           *' '********************************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "Contract number: ",pnd_Tiaa_Cntrct,"Customer Name: ",pnd_Cust_Name);                                                               //Natural: WRITE 'Contract number: ' #TIAA-CNTRCT 'Customer Name: ' #CUST-NAME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "Contract Issue date: ",pnd_Sys105_Key_Pnd_Sys105_Date);                                                                            //Natural: WRITE 'Contract Issue date: ' #SYS105-DATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pnd_Sys105_Key.equals(dfact_3_Record_Key)))                                                                                                     //Natural: IF #SYS105-KEY = DFACT-3.RECORD-KEY
            {
                pnd_Wk_Rate.compute(new ComputeParameters(false, pnd_Wk_Rate), dfact_3_Guar_Interest_Rate.multiply(100));                                                 //Natural: COMPUTE #WK-RATE = GUAR-INTEREST-RATE * 100
                pnd_Rate_On_File.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #RATE-ON-FILE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Check_Saved_Rates() throws Exception                                                                                                                 //Natural: CHECK-SAVED-RATES
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #I 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            //*  IF FOUND, MOVE TO WORK FIELD
            if (condition(pnd_Work_Table_Pnd_Tbl_Date.getValue(pnd_I).equals(pnd_Sys105_Key_Pnd_Sys105_Date)))                                                            //Natural: IF #TBL-DATE ( #I ) = #SYS105-DATE
            {
                pnd_Wk_Rate.setValue(pnd_Work_Table_Pnd_Tbl_Rate.getValue(pnd_I));                                                                                        //Natural: MOVE #TBL-RATE ( #I ) TO #WK-RATE
                pnd_Rate_In_Table.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #RATE-IN-TABLE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Table_Pnd_Tbl_Date.getValue(pnd_I).equals(getZero())))                                                                                 //Natural: IF #TBL-DATE ( #I ) = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Initialize_Table() throws Exception                                                                                                                  //Natural: INITIALIZE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            pnd_Work_Table_Pnd_Tbl_Date.getValue(pnd_I).reset();                                                                                                          //Natural: RESET #TBL-DATE ( #I ) #TBL-RATE ( #I )
            pnd_Work_Table_Pnd_Tbl_Rate.getValue(pnd_I).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Save_Rate() throws Exception                                                                                                                         //Natural: SAVE-RATE
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #I 1 100
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
        {
            //*  IF ALREADY IN TABLE GET OUT
            if (condition(pnd_Sys105_Key_Pnd_Sys105_Date.equals(pnd_Work_Table_Pnd_Tbl_Date.getValue(pnd_I))))                                                            //Natural: IF #SYS105-DATE = #TBL-DATE ( #I )
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  IF NOT IN TABLE - SAVE
            if (condition(pnd_Work_Table_Pnd_Tbl_Date.getValue(pnd_I).equals(getZero())))                                                                                 //Natural: IF #TBL-DATE ( #I ) = 0
            {
                pnd_Work_Table_Pnd_Tbl_Date.getValue(pnd_I).setValue(pnd_Sys105_Key_Pnd_Sys105_Date);                                                                     //Natural: MOVE #SYS105-DATE TO #TBL-DATE ( #I )
                pnd_Work_Table_Pnd_Tbl_Rate.getValue(pnd_I).setValue(pnd_Wk_Rate);                                                                                        //Natural: MOVE #WK-RATE TO #TBL-RATE ( #I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Update_Contract() throws Exception                                                                                                                   //Natural: UPDATE-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        PND_PND_L3510:                                                                                                                                                    //Natural: GET ANNTY-VIEW #UPDATE-ISN
        vw_annty_View.readByID(pnd_Update_Isn.getLong(), "PND_PND_L3510");
        annty_View_Ap_Financial_1.setValue(pnd_Wk_Rate);                                                                                                                  //Natural: ASSIGN AP-FINANCIAL-1 := #WK-RATE
        vw_annty_View.updateDBRow("PND_PND_L3510");                                                                                                                       //Natural: UPDATE ( ##L3510. )
        pnd_Et_Read_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #ET-READ-CNT
        if (condition(pnd_Et_Read_Cnt.greaterOrEqual(100)))                                                                                                               //Natural: IF #ET-READ-CNT GE 100
        {
            pnd_Et_Read_Cnt.reset();                                                                                                                                      //Natural: RESET #ET-READ-CNT
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE AP-TIAA-CNTRCT #WK-RATE #SYS105-DATE
        pnd_Update_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #UPDATE-CNT
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "Program:  ",Global.getPROGRAM(),NEWLINE,"Line:     ",Global.getERROR_LINE(),NEWLINE,"Error:    ",Global.getERROR_NR(),NEWLINE);            //Natural: WRITE 'Program:  ' *PROGRAM / 'Line:     ' *ERROR-LINE / 'Error:    ' *ERROR-NR /
    };                                                                                                                                                                    //Natural: END-ERROR
}
