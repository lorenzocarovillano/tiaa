/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:17 PM
**        * FROM NATURAL PROGRAM : Appb160
************************************************************
**        * FILE NAME            : Appb160.java
**        * CLASS NAME           : Appb160
**        * INSTANCE NAME        : Appb160
************************************************************
**----------------------------------------------------------------------
* PROGRAM-ID    :  APPB160
* DESCRIPTION   :  REGISTRY IDENTIFICATION
*
*
*
*
* DATE WRITTEN  :  07/04/2006
* AUTHOR        :  D. MARPURI
*
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES
**--------   ----------  -----------------------------------------------
**9/29/2006   K.GATES    CHANGES FOR DCA AND NEW GA PRODUCT  SEE DCA KG
**2/21/2007   K.GATES    CHANGES FOR ATRA/KEOGH CONV TO OMNI SEE ATRA KG
**5/07/2007   K.GATES    CHANGES FOR ARR2 PROJECT            SEE ARR2 KG
**9/18/2009   A.RONDEAU  CHANGES FOR RHSP PROJECT - SCAN 'AER'
**3/10/2010   C.AVE      INCLUDED IRA INDEXED PRODUCTS - TIGR
**9/15/2010   L.SHU      UNBLOCK CHANGES DONE BY AER AND ARR2
**7/18/2013   B.NEWSOM   NEW CONTRACTS ISSUED AS PART OF IRA
**                       SUBSTITUTION WILL BE EXCLUDED IN THE ACIS TO
**                       SIEBEL INTERFACE FOR REPLACEMENT REGISTER
**                       INFORMATION.                         SEE TNGSUB
**9/1/2015    B.ELLO     NEW PSEUDO CONTRACTS/ACCOUNTS AS PART OF THE
**                       BENE IN THE PLAN WILL BE EXCLUDED IN THE ACIS
**                       TO SIEBEL INTERFACE FOR REPLACEMENT REGISTER
**                       INFORMATION.                         SEE: BIP
**10/10/2015  GHOSHSI    DELIMITER CHANGED FROM ',' TO '|' TO AVOID THE
**                       FAILURE ON SIEBEL SIDE --INC2856964
**06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
**----------------------------------------------------------------------

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb160 extends BLNatBase
{
    // Data Areas
    private LdaScil1080 ldaScil1080;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap;
    private DbsField annty_Actvty_Prap_Ap_Pin_Nbr;
    private DbsField annty_Actvty_Prap_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_Ap_Dob;
    private DbsField annty_Actvty_Prap_Ap_Lob;
    private DbsField annty_Actvty_Prap_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_Ap_Ownership;
    private DbsField annty_Actvty_Prap_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_Ap_Register_Id;
    private DbsField annty_Actvty_Prap_Ap_Replacement_Ind;
    private DbsField annty_Actvty_Prap_Ap_Exempt_Ind;
    private DbsField annty_Actvty_Prap_Ap_Agent_Or_Racf_Id;
    private DbsField annty_Actvty_Prap_Ap_Racf_Id;
    private DbsField annty_Actvty_Prap_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_Ap_Irc_Sectn_Cde;
    private DbsField annty_Actvty_Prap_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_Ap_Coll_St_Cd;
    private DbsField annty_Actvty_Prap_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_Ap_Arr_Insurer_1;
    private DbsField annty_Actvty_Prap_Ap_Arr_Insurer_2;
    private DbsField annty_Actvty_Prap_Ap_Arr_Insurer_3;
    private DbsField annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_1;
    private DbsField annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_2;
    private DbsField annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_3;
    private DbsField annty_Actvty_Prap_Ap_Substitution_Contract_Ind;
    private DbsField annty_Actvty_Prap_Ap_Decedent_Contract;

    private DbsGroup pnd_Header_Info;
    private DbsField pnd_Header_Info_Pnd_Pin_H;
    private DbsField pnd_Header_Info_Pnd_Comma_1h;
    private DbsField pnd_Header_Info_Pnd_Last_Name_H;
    private DbsField pnd_Header_Info_Pnd_Comma_2h;
    private DbsField pnd_Header_Info_Pnd_First_Name_H;
    private DbsField pnd_Header_Info_Pnd_Comma_3h;
    private DbsField pnd_Header_Info_Pnd_Dob_H;
    private DbsField pnd_Header_Info_Pnd_Comma_4h;
    private DbsField pnd_Header_Info_Pnd_Contract_Number_H;
    private DbsField pnd_Header_Info_Pnd_Comma_5h;
    private DbsField pnd_Header_Info_Pnd_Date_Of_Issuance_H;
    private DbsField pnd_Header_Info_Pnd_Comma_6h;
    private DbsField pnd_Header_Info_Pnd_Product_Name_H;
    private DbsField pnd_Header_Info_Pnd_Comma_7h;
    private DbsField pnd_Header_Info_Pnd_Register_Id_H;
    private DbsField pnd_Header_Info_Pnd_Comma_8h;
    private DbsField pnd_Header_Info_Pnd_Sale_Type_H;
    private DbsField pnd_Header_Info_Pnd_Comma_9h;
    private DbsField pnd_Header_Info_Pnd_Exmpt_Flag_H;
    private DbsField pnd_Header_Info_Pnd_Comma_10h;
    private DbsField pnd_Header_Info_Pnd_Replacement_Flag_H;
    private DbsField pnd_Header_Info_Pnd_Comma_11h;
    private DbsField pnd_Header_Info_Pnd_Exist_Insurer_Name_1_H;
    private DbsField pnd_Header_Info_Pnd_Comma_12h;
    private DbsField pnd_Header_Info_Pnd_Exchange_1035_Flag_1_H;
    private DbsField pnd_Header_Info_Pnd_Comma_13h;
    private DbsField pnd_Header_Info_Pnd_Financed_Purchase_Fl_1_H;
    private DbsField pnd_Header_Info_Pnd_Comma_14h;
    private DbsField pnd_Header_Info_Pnd_Exist_Insurer_Name_2_H;
    private DbsField pnd_Header_Info_Pnd_Comma_15h;
    private DbsField pnd_Header_Info_Pnd_Exchange_1035_Flag_2_H;
    private DbsField pnd_Header_Info_Pnd_Comma_16h;
    private DbsField pnd_Header_Info_Pnd_Financed_Purchase_Fl_2_H;
    private DbsField pnd_Header_Info_Pnd_Comma_17h;
    private DbsField pnd_Header_Info_Pnd_Exist_Insurer_Name_3_H;
    private DbsField pnd_Header_Info_Pnd_Comma_18h;
    private DbsField pnd_Header_Info_Pnd_Exchange_1035_Flag_3_H;
    private DbsField pnd_Header_Info_Pnd_Comma_19h;
    private DbsField pnd_Header_Info_Pnd_Financed_Purchase_Fl_3_H;
    private DbsField pnd_Header_Info_Pnd_Comma_20h;
    private DbsField pnd_Header_Info_Pnd_Agent_Name_H;
    private DbsField pnd_Header_Info_Pnd_Comma_21h;
    private DbsField pnd_Header_Info_Pnd_Racf_Id_H;
    private DbsField pnd_Header_Info_Pnd_Comma_22h;
    private DbsField pnd_Header_Info_Pnd_State_Of_Issuance_H;
    private DbsField pnd_Header_Info_Pnd_Filler1;
    private DbsField pnd_Header_Info_Pnd_Filler2;

    private DbsGroup pnd_Header_Info__R_Field_1;
    private DbsField pnd_Header_Info_Pnd_Header_1;
    private DbsField pnd_Header_Info_Pnd_Header_2;
    private DbsField pnd_Header_Info_Pnd_Header_3;
    private DbsField pnd_Header_Info_Pnd_Header_4;
    private DbsField pnd_Header_Info_Pnd_Header_5;

    private DbsGroup pnd_Scil8600;
    private DbsField pnd_Scil8600_Pnd_Pin;
    private DbsField pnd_Scil8600_Pnd_Comma_1;
    private DbsField pnd_Scil8600_Pnd_Last_Name;
    private DbsField pnd_Scil8600_Pnd_Comma_2;
    private DbsField pnd_Scil8600_Pnd_First_Name;
    private DbsField pnd_Scil8600_Pnd_Comma_3;
    private DbsField pnd_Scil8600_Pnd_Dob;
    private DbsField pnd_Scil8600_Pnd_Comma_4;
    private DbsField pnd_Scil8600_Pnd_Contract_Number;
    private DbsField pnd_Scil8600_Pnd_Comma_5;
    private DbsField pnd_Scil8600_Pnd_Date_Of_Issuance;
    private DbsField pnd_Scil8600_Pnd_Comma_6;
    private DbsField pnd_Scil8600_Pnd_Product_Name;
    private DbsField pnd_Scil8600_Pnd_Comma_7;
    private DbsField pnd_Scil8600_Pnd_Register_Id;
    private DbsField pnd_Scil8600_Pnd_Comma_8;
    private DbsField pnd_Scil8600_Pnd_Sale_Type;
    private DbsField pnd_Scil8600_Pnd_Comma_9;
    private DbsField pnd_Scil8600_Pnd_Exmpt_Flag;
    private DbsField pnd_Scil8600_Pnd_Comma_10;
    private DbsField pnd_Scil8600_Pnd_Replacement_Flag;
    private DbsField pnd_Scil8600_Pnd_Comma_11;
    private DbsField pnd_Scil8600_Pnd_Existing_Insurer_Name_1;
    private DbsField pnd_Scil8600_Pnd_Comma_12;
    private DbsField pnd_Scil8600_Pnd_Exchange_1035_Flag_1;
    private DbsField pnd_Scil8600_Pnd_Comma_13;
    private DbsField pnd_Scil8600_Pnd_Financed_Purchase_Flag_1;
    private DbsField pnd_Scil8600_Pnd_Comma_14;
    private DbsField pnd_Scil8600_Pnd_Existing_Insurer_Name_2;
    private DbsField pnd_Scil8600_Pnd_Comma_15;
    private DbsField pnd_Scil8600_Pnd_Exchange_1035_Flag_2;
    private DbsField pnd_Scil8600_Pnd_Comma_16;
    private DbsField pnd_Scil8600_Pnd_Financed_Purchase_Flag_2;
    private DbsField pnd_Scil8600_Pnd_Comma_17;
    private DbsField pnd_Scil8600_Pnd_Existing_Insurer_Name_3;
    private DbsField pnd_Scil8600_Pnd_Comma_18;
    private DbsField pnd_Scil8600_Pnd_Exchange_1035_Flag_3;
    private DbsField pnd_Scil8600_Pnd_Comma_19;
    private DbsField pnd_Scil8600_Pnd_Financed_Purchase_Flag_3;
    private DbsField pnd_Scil8600_Pnd_Comma_20;
    private DbsField pnd_Scil8600_Pnd_Agent_Name;
    private DbsField pnd_Scil8600_Pnd_Comma_21;
    private DbsField pnd_Scil8600_Pnd_Racf_Id;
    private DbsField pnd_Scil8600_Pnd_Comma_22;
    private DbsField pnd_Scil8600_Pnd_State_Of_Issuance;

    private DbsGroup pnd_Scil8600__R_Field_2;
    private DbsField pnd_Scil8600_Pnd_Scil8600_1;
    private DbsField pnd_Scil8600_Pnd_Scil8600_2;
    private DbsField pnd_Scil8600_Pnd_Scil8600_3;
    private DbsField pnd_Scil8600_Pnd_Scil8600_4;
    private DbsField pnd_Scil8600_Pnd_Scil8600_5;
    private DbsField pnd_Control_Rec;

    private DbsGroup pnd_Control_Rec__R_Field_3;
    private DbsField pnd_Control_Rec_Pnd_Datn;
    private DbsField pnd_Control_Rec_Pnd_Datn_F;
    private DbsField pnd_Control_Rec_Pnd_Total_Count;
    private DbsField pnd_Control_Rec_Pnd_Filler;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_4;
    private DbsField pnd_Date_Pnd_Date_Ccyy;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_K;
    private DbsField pnd_Full_Name;
    private DbsField pnd_St_Of_Issuance;
    private DbsField pnd_Table_Sub_Id;
    private DbsField pnd_Product_Code;

    private DbsGroup pnd_Product_Code__R_Field_5;
    private DbsField pnd_Product_Code_Pnd_Lob;
    private DbsField pnd_Product_Code_Pnd_Lob_Type;
    private DbsField pnd_U_Doi;

    private DbsGroup pnd_U_Doi__R_Field_6;
    private DbsField pnd_U_Doi_Pnd_U_Doi_Mm;
    private DbsField pnd_U_Doi_Pnd_U_Doi_Yy;

    private DbsGroup pnd_Doi;
    private DbsField pnd_Doi_Pnd_Doi_Mm;
    private DbsField pnd_Doi_Pnd_Doi_Dd;
    private DbsField pnd_Doi_Pnd_Doi_Cc;
    private DbsField pnd_Doi_Pnd_Doi_Yy;
    private DbsField pnd_Dob_Num;

    private DbsGroup pnd_Dob_Num__R_Field_7;
    private DbsField pnd_Dob_Num_Pnd_Dob_Mm;
    private DbsField pnd_Dob_Num_Pnd_Dob_Dd;
    private DbsField pnd_Dob_Num_Pnd_Dob_Ccyy;

    private DbsGroup pnd_Summary_Report_Rec;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Write_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaScil1080 = new LdaScil1080();
        registerRecord(ldaScil1080);

        // Local Variables
        localVariables = new DbsRecord();

        vw_annty_Actvty_Prap = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap", "ANNTY-ACTVTY-PRAP"), "ANNTY_ACTVTY_PRAP_12", "ACIS_ANNTY_PRAP");
        annty_Actvty_Prap_Ap_Pin_Nbr = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        annty_Actvty_Prap_Ap_Release_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Release_Ind", "AP-RELEASE-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_Ap_Cor_First_Nme = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_Ap_Dob = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 8, 
            RepeatingFieldStrategy.None, "AP_DOB");
        annty_Actvty_Prap_Ap_Lob = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Lob", "AP-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AP_LOB");
        annty_Actvty_Prap_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_Ap_Lob_Type = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Lob_Type", "AP-LOB-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_Ap_Ownership = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Ownership", "AP-OWNERSHIP", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_OWNERSHIP");
        annty_Actvty_Prap_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_Ap_Register_Id = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Register_Id", "AP-REGISTER-ID", FieldType.STRING, 
            11, RepeatingFieldStrategy.None, "AP_REGISTER_ID");
        annty_Actvty_Prap_Ap_Replacement_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Replacement_Ind", "AP-REPLACEMENT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_REPLACEMENT_IND");
        annty_Actvty_Prap_Ap_Exempt_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Exempt_Ind", "AP-EXEMPT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_EXEMPT_IND");
        annty_Actvty_Prap_Ap_Agent_Or_Racf_Id = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Agent_Or_Racf_Id", "AP-AGENT-OR-RACF-ID", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "AP_AGENT_OR_RACF_ID");
        annty_Actvty_Prap_Ap_Racf_Id = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Racf_Id", "AP-RACF-ID", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "AP_RACF_ID");
        annty_Actvty_Prap_Ap_Orig_Issue_State = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Orig_Issue_State", "AP-ORIG-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_Ap_T_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_Ap_C_Doi = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_Ap_Irc_Sectn_Cde = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Irc_Sectn_Cde", "AP-IRC-SECTN-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "AP_IRC_SECTN_CDE");
        annty_Actvty_Prap_Ap_Coll_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Coll_Code", "AP-COLL-CODE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_Ap_Current_State_Code = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Current_State_Code", "AP-CURRENT-STATE-CODE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_Ap_Coll_St_Cd = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Coll_St_Cd", "AP-COLL-ST-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "AP_COLL_ST_CD");
        annty_Actvty_Prap_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Sgrd_Subplan_No", "AP-SGRD-SUBPLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_Ap_Arr_Insurer_1 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Arr_Insurer_1", "AP-ARR-INSURER-1", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_ARR_INSURER_1");
        annty_Actvty_Prap_Ap_Arr_Insurer_2 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Arr_Insurer_2", "AP-ARR-INSURER-2", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_ARR_INSURER_2");
        annty_Actvty_Prap_Ap_Arr_Insurer_3 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Arr_Insurer_3", "AP-ARR-INSURER-3", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_ARR_INSURER_3");
        annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_1 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_1", "AP-ARR-1035-EXCH-IND-1", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_1");
        annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_2 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_2", "AP-ARR-1035-EXCH-IND-2", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_2");
        annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_3 = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_3", "AP-ARR-1035-EXCH-IND-3", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_ARR_1035_EXCH_IND_3");
        annty_Actvty_Prap_Ap_Substitution_Contract_Ind = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Substitution_Contract_Ind", 
            "AP-SUBSTITUTION-CONTRACT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_SUBSTITUTION_CONTRACT_IND");
        annty_Actvty_Prap_Ap_Decedent_Contract = vw_annty_Actvty_Prap.getRecord().newFieldInGroup("annty_Actvty_Prap_Ap_Decedent_Contract", "AP-DECEDENT-CONTRACT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_DECEDENT_CONTRACT");
        registerRecord(vw_annty_Actvty_Prap);

        pnd_Header_Info = localVariables.newGroupInRecord("pnd_Header_Info", "#HEADER-INFO");
        pnd_Header_Info_Pnd_Pin_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Pin_H", "#PIN-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_1h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_1h", "#COMMA-1H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Last_Name_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Last_Name_H", "#LAST-NAME-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_2h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_2h", "#COMMA-2H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_First_Name_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_First_Name_H", "#FIRST-NAME-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_3h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_3h", "#COMMA-3H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Dob_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Dob_H", "#DOB-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_4h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_4h", "#COMMA-4H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Contract_Number_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Contract_Number_H", "#CONTRACT-NUMBER-H", FieldType.STRING, 
            26);
        pnd_Header_Info_Pnd_Comma_5h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_5h", "#COMMA-5H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Date_Of_Issuance_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Date_Of_Issuance_H", "#DATE-OF-ISSUANCE-H", FieldType.STRING, 
            26);
        pnd_Header_Info_Pnd_Comma_6h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_6h", "#COMMA-6H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Product_Name_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Product_Name_H", "#PRODUCT-NAME-H", FieldType.STRING, 
            26);
        pnd_Header_Info_Pnd_Comma_7h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_7h", "#COMMA-7H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Register_Id_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Register_Id_H", "#REGISTER-ID-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_8h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_8h", "#COMMA-8H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Sale_Type_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Sale_Type_H", "#SALE-TYPE-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_9h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_9h", "#COMMA-9H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Exmpt_Flag_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Exmpt_Flag_H", "#EXMPT-FLAG-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_10h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_10h", "#COMMA-10H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Replacement_Flag_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Replacement_Flag_H", "#REPLACEMENT-FLAG-H", FieldType.STRING, 
            26);
        pnd_Header_Info_Pnd_Comma_11h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_11h", "#COMMA-11H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Exist_Insurer_Name_1_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Exist_Insurer_Name_1_H", "#EXIST-INSURER-NAME-1-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_12h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_12h", "#COMMA-12H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Exchange_1035_Flag_1_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Exchange_1035_Flag_1_H", "#EXCHANGE-1035-FLAG-1-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_13h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_13h", "#COMMA-13H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Financed_Purchase_Fl_1_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Financed_Purchase_Fl_1_H", "#FINANCED-PURCHASE-FL-1-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_14h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_14h", "#COMMA-14H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Exist_Insurer_Name_2_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Exist_Insurer_Name_2_H", "#EXIST-INSURER-NAME-2-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_15h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_15h", "#COMMA-15H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Exchange_1035_Flag_2_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Exchange_1035_Flag_2_H", "#EXCHANGE-1035-FLAG-2-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_16h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_16h", "#COMMA-16H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Financed_Purchase_Fl_2_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Financed_Purchase_Fl_2_H", "#FINANCED-PURCHASE-FL-2-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_17h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_17h", "#COMMA-17H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Exist_Insurer_Name_3_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Exist_Insurer_Name_3_H", "#EXIST-INSURER-NAME-3-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_18h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_18h", "#COMMA-18H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Exchange_1035_Flag_3_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Exchange_1035_Flag_3_H", "#EXCHANGE-1035-FLAG-3-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_19h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_19h", "#COMMA-19H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Financed_Purchase_Fl_3_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Financed_Purchase_Fl_3_H", "#FINANCED-PURCHASE-FL-3-H", 
            FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_20h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_20h", "#COMMA-20H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Agent_Name_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Agent_Name_H", "#AGENT-NAME-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_21h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_21h", "#COMMA-21H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_Racf_Id_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Racf_Id_H", "#RACF-ID-H", FieldType.STRING, 26);
        pnd_Header_Info_Pnd_Comma_22h = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Comma_22h", "#COMMA-22H", FieldType.STRING, 1);
        pnd_Header_Info_Pnd_State_Of_Issuance_H = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_State_Of_Issuance_H", "#STATE-OF-ISSUANCE-H", FieldType.STRING, 
            26);
        pnd_Header_Info_Pnd_Filler1 = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Filler1", "#FILLER1", FieldType.STRING, 250);
        pnd_Header_Info_Pnd_Filler2 = pnd_Header_Info.newFieldInGroup("pnd_Header_Info_Pnd_Filler2", "#FILLER2", FieldType.STRING, 176);

        pnd_Header_Info__R_Field_1 = localVariables.newGroupInRecord("pnd_Header_Info__R_Field_1", "REDEFINE", pnd_Header_Info);
        pnd_Header_Info_Pnd_Header_1 = pnd_Header_Info__R_Field_1.newFieldInGroup("pnd_Header_Info_Pnd_Header_1", "#HEADER-1", FieldType.STRING, 250);
        pnd_Header_Info_Pnd_Header_2 = pnd_Header_Info__R_Field_1.newFieldInGroup("pnd_Header_Info_Pnd_Header_2", "#HEADER-2", FieldType.STRING, 250);
        pnd_Header_Info_Pnd_Header_3 = pnd_Header_Info__R_Field_1.newFieldInGroup("pnd_Header_Info_Pnd_Header_3", "#HEADER-3", FieldType.STRING, 250);
        pnd_Header_Info_Pnd_Header_4 = pnd_Header_Info__R_Field_1.newFieldInGroup("pnd_Header_Info_Pnd_Header_4", "#HEADER-4", FieldType.STRING, 250);
        pnd_Header_Info_Pnd_Header_5 = pnd_Header_Info__R_Field_1.newFieldInGroup("pnd_Header_Info_Pnd_Header_5", "#HEADER-5", FieldType.STRING, 46);

        pnd_Scil8600 = localVariables.newGroupInRecord("pnd_Scil8600", "#SCIL8600");
        pnd_Scil8600_Pnd_Pin = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Pin", "#PIN", FieldType.STRING, 30);
        pnd_Scil8600_Pnd_Comma_1 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_1", "#COMMA-1", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Last_Name = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 50);
        pnd_Scil8600_Pnd_Comma_2 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_2", "#COMMA-2", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_First_Name = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 50);
        pnd_Scil8600_Pnd_Comma_3 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_3", "#COMMA-3", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Dob = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Dob", "#DOB", FieldType.STRING, 10);
        pnd_Scil8600_Pnd_Comma_4 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_4", "#COMMA-4", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Contract_Number = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Contract_Number", "#CONTRACT-NUMBER", FieldType.STRING, 100);
        pnd_Scil8600_Pnd_Comma_5 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_5", "#COMMA-5", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Date_Of_Issuance = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Date_Of_Issuance", "#DATE-OF-ISSUANCE", FieldType.STRING, 10);
        pnd_Scil8600_Pnd_Comma_6 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_6", "#COMMA-6", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Product_Name = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Product_Name", "#PRODUCT-NAME", FieldType.STRING, 100);
        pnd_Scil8600_Pnd_Comma_7 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_7", "#COMMA-7", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Register_Id = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Register_Id", "#REGISTER-ID", FieldType.STRING, 30);
        pnd_Scil8600_Pnd_Comma_8 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_8", "#COMMA-8", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Sale_Type = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Sale_Type", "#SALE-TYPE", FieldType.STRING, 30);
        pnd_Scil8600_Pnd_Comma_9 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_9", "#COMMA-9", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Exmpt_Flag = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Exmpt_Flag", "#EXMPT-FLAG", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Comma_10 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_10", "#COMMA-10", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Replacement_Flag = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Replacement_Flag", "#REPLACEMENT-FLAG", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Comma_11 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_11", "#COMMA-11", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Existing_Insurer_Name_1 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Existing_Insurer_Name_1", "#EXISTING-INSURER-NAME-1", 
            FieldType.STRING, 100);
        pnd_Scil8600_Pnd_Comma_12 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_12", "#COMMA-12", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Exchange_1035_Flag_1 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Exchange_1035_Flag_1", "#EXCHANGE-1035-FLAG-1", FieldType.STRING, 
            1);
        pnd_Scil8600_Pnd_Comma_13 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_13", "#COMMA-13", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Financed_Purchase_Flag_1 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Financed_Purchase_Flag_1", "#FINANCED-PURCHASE-FLAG-1", 
            FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Comma_14 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_14", "#COMMA-14", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Existing_Insurer_Name_2 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Existing_Insurer_Name_2", "#EXISTING-INSURER-NAME-2", 
            FieldType.STRING, 100);
        pnd_Scil8600_Pnd_Comma_15 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_15", "#COMMA-15", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Exchange_1035_Flag_2 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Exchange_1035_Flag_2", "#EXCHANGE-1035-FLAG-2", FieldType.STRING, 
            30);
        pnd_Scil8600_Pnd_Comma_16 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_16", "#COMMA-16", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Financed_Purchase_Flag_2 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Financed_Purchase_Flag_2", "#FINANCED-PURCHASE-FLAG-2", 
            FieldType.STRING, 50);
        pnd_Scil8600_Pnd_Comma_17 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_17", "#COMMA-17", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Existing_Insurer_Name_3 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Existing_Insurer_Name_3", "#EXISTING-INSURER-NAME-3", 
            FieldType.STRING, 100);
        pnd_Scil8600_Pnd_Comma_18 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_18", "#COMMA-18", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Exchange_1035_Flag_3 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Exchange_1035_Flag_3", "#EXCHANGE-1035-FLAG-3", FieldType.STRING, 
            50);
        pnd_Scil8600_Pnd_Comma_19 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_19", "#COMMA-19", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Financed_Purchase_Flag_3 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Financed_Purchase_Flag_3", "#FINANCED-PURCHASE-FLAG-3", 
            FieldType.STRING, 50);
        pnd_Scil8600_Pnd_Comma_20 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_20", "#COMMA-20", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Agent_Name = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Agent_Name", "#AGENT-NAME", FieldType.STRING, 50);
        pnd_Scil8600_Pnd_Comma_21 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_21", "#COMMA-21", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_Racf_Id = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Racf_Id", "#RACF-ID", FieldType.STRING, 50);
        pnd_Scil8600_Pnd_Comma_22 = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_Comma_22", "#COMMA-22", FieldType.STRING, 1);
        pnd_Scil8600_Pnd_State_Of_Issuance = pnd_Scil8600.newFieldInGroup("pnd_Scil8600_Pnd_State_Of_Issuance", "#STATE-OF-ISSUANCE", FieldType.STRING, 
            30);

        pnd_Scil8600__R_Field_2 = localVariables.newGroupInRecord("pnd_Scil8600__R_Field_2", "REDEFINE", pnd_Scil8600);
        pnd_Scil8600_Pnd_Scil8600_1 = pnd_Scil8600__R_Field_2.newFieldInGroup("pnd_Scil8600_Pnd_Scil8600_1", "#SCIL8600-1", FieldType.STRING, 250);
        pnd_Scil8600_Pnd_Scil8600_2 = pnd_Scil8600__R_Field_2.newFieldInGroup("pnd_Scil8600_Pnd_Scil8600_2", "#SCIL8600-2", FieldType.STRING, 250);
        pnd_Scil8600_Pnd_Scil8600_3 = pnd_Scil8600__R_Field_2.newFieldInGroup("pnd_Scil8600_Pnd_Scil8600_3", "#SCIL8600-3", FieldType.STRING, 250);
        pnd_Scil8600_Pnd_Scil8600_4 = pnd_Scil8600__R_Field_2.newFieldInGroup("pnd_Scil8600_Pnd_Scil8600_4", "#SCIL8600-4", FieldType.STRING, 250);
        pnd_Scil8600_Pnd_Scil8600_5 = pnd_Scil8600__R_Field_2.newFieldInGroup("pnd_Scil8600_Pnd_Scil8600_5", "#SCIL8600-5", FieldType.STRING, 46);
        pnd_Control_Rec = localVariables.newFieldInRecord("pnd_Control_Rec", "#CONTROL-REC", FieldType.STRING, 30);

        pnd_Control_Rec__R_Field_3 = localVariables.newGroupInRecord("pnd_Control_Rec__R_Field_3", "REDEFINE", pnd_Control_Rec);
        pnd_Control_Rec_Pnd_Datn = pnd_Control_Rec__R_Field_3.newFieldInGroup("pnd_Control_Rec_Pnd_Datn", "#DATN", FieldType.STRING, 10);
        pnd_Control_Rec_Pnd_Datn_F = pnd_Control_Rec__R_Field_3.newFieldInGroup("pnd_Control_Rec_Pnd_Datn_F", "#DATN-F", FieldType.STRING, 1);
        pnd_Control_Rec_Pnd_Total_Count = pnd_Control_Rec__R_Field_3.newFieldInGroup("pnd_Control_Rec_Pnd_Total_Count", "#TOTAL-COUNT", FieldType.NUMERIC, 
            9);
        pnd_Control_Rec_Pnd_Filler = pnd_Control_Rec__R_Field_3.newFieldInGroup("pnd_Control_Rec_Pnd_Filler", "#FILLER", FieldType.STRING, 10);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Date__R_Field_4", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Ccyy = pnd_Date__R_Field_4.newFieldInGroup("pnd_Date_Pnd_Date_Ccyy", "#DATE-CCYY", FieldType.STRING, 4);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_4.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.STRING, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_4.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.STRING, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Full_Name = localVariables.newFieldInRecord("pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 40);
        pnd_St_Of_Issuance = localVariables.newFieldInRecord("pnd_St_Of_Issuance", "#ST-OF-ISSUANCE", FieldType.STRING, 2);
        pnd_Table_Sub_Id = localVariables.newFieldInRecord("pnd_Table_Sub_Id", "#TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Product_Code = localVariables.newFieldInRecord("pnd_Product_Code", "#PRODUCT-CODE", FieldType.STRING, 2);

        pnd_Product_Code__R_Field_5 = localVariables.newGroupInRecord("pnd_Product_Code__R_Field_5", "REDEFINE", pnd_Product_Code);
        pnd_Product_Code_Pnd_Lob = pnd_Product_Code__R_Field_5.newFieldInGroup("pnd_Product_Code_Pnd_Lob", "#LOB", FieldType.STRING, 1);
        pnd_Product_Code_Pnd_Lob_Type = pnd_Product_Code__R_Field_5.newFieldInGroup("pnd_Product_Code_Pnd_Lob_Type", "#LOB-TYPE", FieldType.STRING, 1);
        pnd_U_Doi = localVariables.newFieldInRecord("pnd_U_Doi", "#U-DOI", FieldType.NUMERIC, 4);

        pnd_U_Doi__R_Field_6 = localVariables.newGroupInRecord("pnd_U_Doi__R_Field_6", "REDEFINE", pnd_U_Doi);
        pnd_U_Doi_Pnd_U_Doi_Mm = pnd_U_Doi__R_Field_6.newFieldInGroup("pnd_U_Doi_Pnd_U_Doi_Mm", "#U-DOI-MM", FieldType.STRING, 2);
        pnd_U_Doi_Pnd_U_Doi_Yy = pnd_U_Doi__R_Field_6.newFieldInGroup("pnd_U_Doi_Pnd_U_Doi_Yy", "#U-DOI-YY", FieldType.STRING, 2);

        pnd_Doi = localVariables.newGroupInRecord("pnd_Doi", "#DOI");
        pnd_Doi_Pnd_Doi_Mm = pnd_Doi.newFieldInGroup("pnd_Doi_Pnd_Doi_Mm", "#DOI-MM", FieldType.STRING, 2);
        pnd_Doi_Pnd_Doi_Dd = pnd_Doi.newFieldInGroup("pnd_Doi_Pnd_Doi_Dd", "#DOI-DD", FieldType.STRING, 2);
        pnd_Doi_Pnd_Doi_Cc = pnd_Doi.newFieldInGroup("pnd_Doi_Pnd_Doi_Cc", "#DOI-CC", FieldType.STRING, 2);
        pnd_Doi_Pnd_Doi_Yy = pnd_Doi.newFieldInGroup("pnd_Doi_Pnd_Doi_Yy", "#DOI-YY", FieldType.STRING, 2);
        pnd_Dob_Num = localVariables.newFieldInRecord("pnd_Dob_Num", "#DOB-NUM", FieldType.NUMERIC, 8);

        pnd_Dob_Num__R_Field_7 = localVariables.newGroupInRecord("pnd_Dob_Num__R_Field_7", "REDEFINE", pnd_Dob_Num);
        pnd_Dob_Num_Pnd_Dob_Mm = pnd_Dob_Num__R_Field_7.newFieldInGroup("pnd_Dob_Num_Pnd_Dob_Mm", "#DOB-MM", FieldType.STRING, 2);
        pnd_Dob_Num_Pnd_Dob_Dd = pnd_Dob_Num__R_Field_7.newFieldInGroup("pnd_Dob_Num_Pnd_Dob_Dd", "#DOB-DD", FieldType.STRING, 2);
        pnd_Dob_Num_Pnd_Dob_Ccyy = pnd_Dob_Num__R_Field_7.newFieldInGroup("pnd_Dob_Num_Pnd_Dob_Ccyy", "#DOB-CCYY", FieldType.STRING, 4);

        pnd_Summary_Report_Rec = localVariables.newGroupInRecord("pnd_Summary_Report_Rec", "#SUMMARY-REPORT-REC");
        pnd_Summary_Report_Rec_Pnd_Tot_Write_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Write_Cnt", "#TOT-WRITE-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap.reset();

        ldaScil1080.initializeValues();

        localVariables.reset();
        pnd_Header_Info_Pnd_Pin_H.setInitialValue("OWNER PIN");
        pnd_Header_Info_Pnd_Comma_1h.setInitialValue("|");
        pnd_Header_Info_Pnd_Last_Name_H.setInitialValue("OWNER LAST NAME");
        pnd_Header_Info_Pnd_Comma_2h.setInitialValue("|");
        pnd_Header_Info_Pnd_First_Name_H.setInitialValue("OWNER FIRST NAME");
        pnd_Header_Info_Pnd_Comma_3h.setInitialValue("|");
        pnd_Header_Info_Pnd_Dob_H.setInitialValue("OWNER DOB");
        pnd_Header_Info_Pnd_Comma_4h.setInitialValue("|");
        pnd_Header_Info_Pnd_Contract_Number_H.setInitialValue("CONTRACT NUMBER");
        pnd_Header_Info_Pnd_Comma_5h.setInitialValue("|");
        pnd_Header_Info_Pnd_Date_Of_Issuance_H.setInitialValue("DATE OF ISSUANCE");
        pnd_Header_Info_Pnd_Comma_6h.setInitialValue("|");
        pnd_Header_Info_Pnd_Product_Name_H.setInitialValue("PRODUCT NAME");
        pnd_Header_Info_Pnd_Comma_7h.setInitialValue("|");
        pnd_Header_Info_Pnd_Register_Id_H.setInitialValue("REGISTER ID");
        pnd_Header_Info_Pnd_Comma_8h.setInitialValue("|");
        pnd_Header_Info_Pnd_Sale_Type_H.setInitialValue("SALE TYPE");
        pnd_Header_Info_Pnd_Comma_9h.setInitialValue("|");
        pnd_Header_Info_Pnd_Exmpt_Flag_H.setInitialValue("EXEMPT FLAG");
        pnd_Header_Info_Pnd_Comma_10h.setInitialValue("|");
        pnd_Header_Info_Pnd_Replacement_Flag_H.setInitialValue("REPLACEMENT FLAG");
        pnd_Header_Info_Pnd_Comma_11h.setInitialValue("|");
        pnd_Header_Info_Pnd_Exist_Insurer_Name_1_H.setInitialValue("EXISTING INSURER NAME #1");
        pnd_Header_Info_Pnd_Comma_12h.setInitialValue("|");
        pnd_Header_Info_Pnd_Exchange_1035_Flag_1_H.setInitialValue("EXCHANGE 1035 FLAG #1");
        pnd_Header_Info_Pnd_Comma_13h.setInitialValue("|");
        pnd_Header_Info_Pnd_Financed_Purchase_Fl_1_H.setInitialValue("FINANCED PURCHASES FLAG #1");
        pnd_Header_Info_Pnd_Comma_14h.setInitialValue("|");
        pnd_Header_Info_Pnd_Exist_Insurer_Name_2_H.setInitialValue("EXISTING INSURER NAME #2");
        pnd_Header_Info_Pnd_Comma_15h.setInitialValue("|");
        pnd_Header_Info_Pnd_Exchange_1035_Flag_2_H.setInitialValue("EXCHANGE 1035 FLAG #2");
        pnd_Header_Info_Pnd_Comma_16h.setInitialValue("|");
        pnd_Header_Info_Pnd_Financed_Purchase_Fl_2_H.setInitialValue("FINANCED PURCHASE FLAG #2");
        pnd_Header_Info_Pnd_Comma_17h.setInitialValue("|");
        pnd_Header_Info_Pnd_Exist_Insurer_Name_3_H.setInitialValue("EXISTING INSURER NAME #3");
        pnd_Header_Info_Pnd_Comma_18h.setInitialValue("|");
        pnd_Header_Info_Pnd_Exchange_1035_Flag_3_H.setInitialValue("EXCHANGE 1035 FLAG #3");
        pnd_Header_Info_Pnd_Comma_19h.setInitialValue("|");
        pnd_Header_Info_Pnd_Financed_Purchase_Fl_3_H.setInitialValue("FINANCED PURCHASE FLAG #3");
        pnd_Header_Info_Pnd_Comma_20h.setInitialValue("|");
        pnd_Header_Info_Pnd_Agent_Name_H.setInitialValue("AGENT NAME");
        pnd_Header_Info_Pnd_Comma_21h.setInitialValue("|");
        pnd_Header_Info_Pnd_Racf_Id_H.setInitialValue("RACF ID");
        pnd_Header_Info_Pnd_Comma_22h.setInitialValue("|");
        pnd_Header_Info_Pnd_State_Of_Issuance_H.setInitialValue("STATE OF ISSUANCE");
        pnd_Scil8600_Pnd_Comma_1.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_2.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_3.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_4.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_5.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_6.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_7.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_8.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_9.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_10.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_11.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_12.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_13.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_14.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_15.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_16.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_17.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_18.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_19.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_20.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_21.setInitialValue("|");
        pnd_Scil8600_Pnd_Comma_22.setInitialValue("|");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb160() throws Exception
    {
        super("Appb160");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *===========================================================
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60
        pnd_Summary_Report_Rec_Pnd_Tot_Write_Cnt.reset();                                                                                                                 //Natural: RESET #TOT-WRITE-CNT
        //* ************************************************************
        //* *        MAIN PROCESS                                     **
        //* ************************************************************
        vw_annty_Actvty_Prap.startDatabaseRead                                                                                                                            //Natural: READ ANNTY-ACTVTY-PRAP BY AP-RELEASE-IND
        (
        "READ01",
        new Oc[] { new Oc("AP_RELEASE_IND", "ASC") }
        );
        READ01:
        while (condition(vw_annty_Actvty_Prap.readNextRow("READ01")))
        {
            CheckAtStartofData340();

            //*  TNGSUB
            //*  TNGSUB
            //*  BIP
            if (condition(!(annty_Actvty_Prap_Ap_Release_Ind.equals(1) && annty_Actvty_Prap_Ap_Register_Id.notEquals(" ") && (annty_Actvty_Prap_Ap_Substitution_Contract_Ind.notEquals("W")  //Natural: ACCEPT IF AP-RELEASE-IND = 1 AND AP-REGISTER-ID NE ' ' AND ( AP-SUBSTITUTION-CONTRACT-IND NE 'W' AND AP-SUBSTITUTION-CONTRACT-IND NE 'O' ) AND AP-DECEDENT-CONTRACT = ' '
                && annty_Actvty_Prap_Ap_Substitution_Contract_Ind.notEquals("O")) && annty_Actvty_Prap_Ap_Decedent_Contract.equals(" "))))
            {
                continue;
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*  ADD 1 TO #TOT-WRITE-CNT
            pnd_Product_Code_Pnd_Lob.setValue(annty_Actvty_Prap_Ap_Lob);                                                                                                  //Natural: ASSIGN #LOB := AP-LOB
            pnd_Product_Code_Pnd_Lob_Type.setValue(annty_Actvty_Prap_Ap_Lob_Type);                                                                                        //Natural: ASSIGN #LOB-TYPE := AP-LOB-TYPE
            //* ********************************************************************
            //*  ARR2 KG START - BLOCK INSTITUTIONAL CONTROLLED PRODUCTS    *LS START
            //*  BLOCKING RETIREMENT CHOICE, RETIREMENT CHOICE PLUS, GROUP ANNUITIES,
            //*  AND RETIREMENT LOANS FROM EXTRACTING TO THE REGISTER FILE
            //*  IF #PRODUCT-CODE EQ 'DA' OR EQ 'D6' OR EQ 'S4' OR EQ 'S7' OR EQ 'S9'
            //*    ESCAPE TOP
            //*  END-IF
            //*  BLOCKING 457(F) PUBLIC AND PRIVATE GSRA CONTRACTS FROM EXTRACTING
            //*  IF #PRODUCT-CODE EQ 'S3' AND AP-IRC-SECTN-CDE EQ 07
            //*    ESCAPE TOP
            //*  END-IF
            //*  ARR2 KG END - BLOCK INSTITUTIONAL CONTROLLED PRODUCTS
            //* *********************************************************************
            //*  IF #PRODUCT-CODE = 'DB'                                       /* AER
            //*      ESCAPE TOP                                                /* AER
            //*  END-IF                                                        /* AER
            //*                                                             /* LS END
            pnd_Summary_Report_Rec_Pnd_Tot_Write_Cnt.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-WRITE-CNT
                                                                                                                                                                          //Natural: PERFORM GET-PRODUCT-NAME
            sub_Get_Product_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-PRAP-TO-SCIL8600
            sub_Move_Prap_To_Scil8600();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-INFO
            sub_Write_Detail_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Control_Rec_Pnd_Total_Count.setValue(pnd_Summary_Report_Rec_Pnd_Tot_Write_Cnt);                                                                               //Natural: ASSIGN #TOTAL-COUNT := #TOT-WRITE-CNT
        pnd_Date.setValue(Global.getDATN());                                                                                                                              //Natural: ASSIGN #DATE := *DATN
        pnd_Scil8600.reset();                                                                                                                                             //Natural: RESET #SCIL8600
        pnd_Control_Rec_Pnd_Datn.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Date_Pnd_Date_Mm, "/", pnd_Date_Pnd_Date_Dd, "/", pnd_Date_Pnd_Date_Ccyy)); //Natural: COMPRESS #DATE-MM '/' #DATE-DD '/' #DATE-CCYY INTO #DATN LEAVING NO
        pnd_Control_Rec_Pnd_Datn_F.setValue(",");                                                                                                                         //Natural: ASSIGN #DATN-F := ','
        pnd_Scil8600_Pnd_Pin.setValue(pnd_Control_Rec);                                                                                                                   //Natural: ASSIGN #PIN := #CONTROL-REC
        getWorkFiles().write(1, false, pnd_Scil8600_Pnd_Scil8600_1, pnd_Scil8600_Pnd_Scil8600_2, pnd_Scil8600_Pnd_Scil8600_3, pnd_Scil8600_Pnd_Scil8600_4,                //Natural: WRITE WORK FILE 1 #SCIL8600-1 #SCIL8600-2 #SCIL8600-3 #SCIL8600-4 #SCIL8600-5
            pnd_Scil8600_Pnd_Scil8600_5);
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Number of New Register Id Count: ",pnd_Summary_Report_Rec_Pnd_Tot_Write_Cnt);                                 //Natural: WRITE ( 01 ) / 'Number of New Register Id Count: ' #TOT-WRITE-CNT
        if (Global.isEscape()) return;
        //*  ------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PRODUCT-NAME
        //*    IF AP-COLL-CODE = '9004' OR = '9006' OR = '9007' OR = '9008'
        //*        OR = '9010' OR = '9011' OR = '9012' OR = '9013'
        //*        OR = '9014' OR = '9015' OR = '9016' OR = '9017'
        //*        OR = '9018' OR = '9019' OR = '9020'
        //*    #ST-OF-ISSUANCE    := AP-CURRENT-STATE-CODE
        //* ************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-PRAP-TO-SCIL8600
        //* ************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADER-INFO
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-INFO
    }
    private void sub_Get_Product_Name() throws Exception                                                                                                                  //Natural: GET-PRODUCT-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------------------------------- *
        //*  RA
        short decideConditionsMet426 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PRODUCT-CODE = 'D2'
        if (condition(pnd_Product_Code.equals("D2")))
        {
            decideConditionsMet426++;
            //*  ATRA KG
            if (condition(((annty_Actvty_Prap_Ap_Sgrd_Subplan_No.getSubstring(1,2).equals("AA") || (annty_Actvty_Prap_Ap_Coll_Code.equals("9000") || annty_Actvty_Prap_Ap_Coll_Code.equals("9004")))  //Natural: IF SUBSTRING ( AP-SGRD-SUBPLAN-NO,1,2 ) = 'AA' OR AP-COLL-CODE = '9000' OR = '9004' OR AP-COLL-CODE = '9006' THRU '9020' BUT NOT '9009'
                || ((annty_Actvty_Prap_Ap_Coll_Code.greaterOrEqual("9006") && annty_Actvty_Prap_Ap_Coll_Code.lessOrEqual("9020")) && annty_Actvty_Prap_Ap_Coll_Code.notEquals("9009")))))
            {
                pnd_Scil8600_Pnd_Product_Name.setValue("AFTER TAX RETIREMENT ANNUITY");                                                                                   //Natural: ASSIGN #PRODUCT-NAME := 'AFTER TAX RETIREMENT ANNUITY'
                pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                     //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Scil8600_Pnd_Product_Name.setValue("RETIREMENT ANNUITY");                                                                                             //Natural: ASSIGN #PRODUCT-NAME := 'RETIREMENT ANNUITY'
                pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                     //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
                //*  GRA
                //*  SRA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'D7' OR = 'D9'
        if (condition(pnd_Product_Code.equals("D7") || pnd_Product_Code.equals("D9")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("GROUP RETIREMENT ANNUITY");                                                                                           //Natural: ASSIGN #PRODUCT-NAME := 'GROUP RETIREMENT ANNUITY'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'S2'
        if (condition(pnd_Product_Code.equals("S2")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("SUPPLEMENTAL RETIREMENT ANNUITY");                                                                                    //Natural: ASSIGN #PRODUCT-NAME := 'SUPPLEMENTAL RETIREMENT ANNUITY'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'S3'
        if (condition(pnd_Product_Code.equals("S3")))
        {
            decideConditionsMet426++;
            //*  GSRA
            if (condition(annty_Actvty_Prap_Ap_Irc_Sectn_Cde.equals(10)))                                                                                                 //Natural: IF AP-IRC-SECTN-CDE = 10
            {
                pnd_Scil8600_Pnd_Product_Name.setValue("457(B) DEFERRED COMPENSATION PLAN (PUBLIC)");                                                                     //Natural: ASSIGN #PRODUCT-NAME := '457(B) DEFERRED COMPENSATION PLAN (PUBLIC)'
                pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                             //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  GSRA
                if (condition(annty_Actvty_Prap_Ap_Irc_Sectn_Cde.equals(7)))                                                                                              //Natural: IF AP-IRC-SECTN-CDE = 07
                {
                    pnd_Scil8600_Pnd_Product_Name.setValue("457(F) NON-TRUSTEE CUSTODIAL AGREEMENT");                                                                     //Natural: ASSIGN #PRODUCT-NAME := '457(F) NON-TRUSTEE CUSTODIAL AGREEMENT'
                    pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Scil8600_Pnd_Product_Name.setValue("GROUP SUPPLEMENTAL RETIREMENT ANNUITY");                                                                      //Natural: ASSIGN #PRODUCT-NAME := 'GROUP SUPPLEMENTAL RETIREMENT ANNUITY'
                    pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
                }                                                                                                                                                         //Natural: END-IF
                //*  SRA
                //*  SRA QVEC
                //*  GA AND GA5 - DCA KG
                //*  RL
                //*  RA QVEC
                //*  IRA ORIGINAL PROD
                //*  IRA TRADITIONAL
                //*  IRA ROTH
                //*  IRA SEP
                //*  IRA SEP
                //*  ATRA KG
                //*  RC
                //*  RCP
                //*  TIGR-START
                //*  TIGR-END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'S4'
        if (condition(pnd_Product_Code.equals("S4")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("457(B) DEFERRED COMPENSATION PLAN (PRIVATE)");                                                                        //Natural: ASSIGN #PRODUCT-NAME := '457(B) DEFERRED COMPENSATION PLAN (PRIVATE)'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'S8'
        if (condition(pnd_Product_Code.equals("S8")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("SUPPLEMENTAL RETIREMENT ANNUITY");                                                                                    //Natural: ASSIGN #PRODUCT-NAME := 'SUPPLEMENTAL RETIREMENT ANNUITY'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'S9'
        if (condition(pnd_Product_Code.equals("S9")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("GROUP ANNUITY");                                                                                                      //Natural: ASSIGN #PRODUCT-NAME := 'GROUP ANNUITY'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'D6'
        if (condition(pnd_Product_Code.equals("D6")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("RETIREMENT LOAN");                                                                                                    //Natural: ASSIGN #PRODUCT-NAME := 'RETIREMENT LOAN'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'D8'
        if (condition(pnd_Product_Code.equals("D8")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("RETIREMENT ANNUITY");                                                                                                 //Natural: ASSIGN #PRODUCT-NAME := 'RETIREMENT ANNUITY'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I1' OR = 'I2'
        if (condition(pnd_Product_Code.equals("I1") || pnd_Product_Code.equals("I2")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("ORIGINAL IRA PRODUCT");                                                                                               //Natural: ASSIGN #PRODUCT-NAME := 'ORIGINAL IRA PRODUCT'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I4'
        if (condition(pnd_Product_Code.equals("I4")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("TRADITIONAL IRA");                                                                                                    //Natural: ASSIGN #PRODUCT-NAME := 'TRADITIONAL IRA'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I3'
        if (condition(pnd_Product_Code.equals("I3")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("ROTH IRA");                                                                                                           //Natural: ASSIGN #PRODUCT-NAME := 'ROTH IRA'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I6'
        if (condition(pnd_Product_Code.equals("I6")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("SIMPLIFIED EMPLOYEE PENSION (SEP) IRA");                                                                              //Natural: ASSIGN #PRODUCT-NAME := 'SIMPLIFIED EMPLOYEE PENSION (SEP) IRA'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I5'
        if (condition(pnd_Product_Code.equals("I5")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("KEOGH");                                                                                                              //Natural: ASSIGN #PRODUCT-NAME := 'KEOGH'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'DA'
        if (condition(pnd_Product_Code.equals("DA")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("RETIREMENT CHOICE");                                                                                                  //Natural: ASSIGN #PRODUCT-NAME := 'RETIREMENT CHOICE'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'S7'
        if (condition(pnd_Product_Code.equals("S7")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("RETIREMENT CHOICE PLUS");                                                                                             //Natural: ASSIGN #PRODUCT-NAME := 'RETIREMENT CHOICE PLUS'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Coll_St_Cd);                                                                                                 //Natural: ASSIGN #ST-OF-ISSUANCE := AP-COLL-ST-CD
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I8'
        if (condition(pnd_Product_Code.equals("I8")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("TRADITIONAL IRA (INDEXED GUARANTEED)");                                                                               //Natural: ASSIGN #PRODUCT-NAME := 'TRADITIONAL IRA (INDEXED GUARANTEED)'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I7'
        if (condition(pnd_Product_Code.equals("I7")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("ROTH IRA (INDEXED GUARANTEED)");                                                                                      //Natural: ASSIGN #PRODUCT-NAME := 'ROTH IRA (INDEXED GUARANTEED)'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN #PRODUCT-CODE = 'I9'
        if (condition(pnd_Product_Code.equals("I9")))
        {
            decideConditionsMet426++;
            pnd_Scil8600_Pnd_Product_Name.setValue("SIMPLIFIED EMPLOYEE PENSION IRA (INDEXED GUARANTEED)");                                                               //Natural: ASSIGN #PRODUCT-NAME := 'SIMPLIFIED EMPLOYEE PENSION IRA (INDEXED GUARANTEED)'
            pnd_St_Of_Issuance.setValue(annty_Actvty_Prap_Ap_Current_State_Code);                                                                                         //Natural: ASSIGN #ST-OF-ISSUANCE := AP-CURRENT-STATE-CODE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet426 == 0))
        {
            pnd_Scil8600_Pnd_Product_Name.setValue(" ");                                                                                                                  //Natural: ASSIGN #PRODUCT-NAME := ' '
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Move_Prap_To_Scil8600() throws Exception                                                                                                             //Natural: MOVE-PRAP-TO-SCIL8600
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Scil8600_Pnd_Pin.setValue(annty_Actvty_Prap_Ap_Pin_Nbr);                                                                                                      //Natural: ASSIGN #PIN := AP-PIN-NBR
        pnd_Scil8600_Pnd_Last_Name.setValue(annty_Actvty_Prap_Ap_Cor_Last_Nme);                                                                                           //Natural: ASSIGN #LAST-NAME := AP-COR-LAST-NME
        pnd_Scil8600_Pnd_First_Name.setValue(annty_Actvty_Prap_Ap_Cor_First_Nme);                                                                                         //Natural: ASSIGN #FIRST-NAME := AP-COR-FIRST-NME
        pnd_Dob_Num.setValue(annty_Actvty_Prap_Ap_Dob);                                                                                                                   //Natural: ASSIGN #DOB-NUM := AP-DOB
        pnd_Scil8600_Pnd_Dob.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dob_Num_Pnd_Dob_Mm, "/", pnd_Dob_Num_Pnd_Dob_Dd, "/", pnd_Dob_Num_Pnd_Dob_Ccyy)); //Natural: COMPRESS #DOB-MM '/' #DOB-DD '/' #DOB-CCYY INTO #DOB LEAVING NO
        pnd_Scil8600_Pnd_Contract_Number.setValue(annty_Actvty_Prap_Ap_Tiaa_Cntrct);                                                                                      //Natural: ASSIGN #CONTRACT-NUMBER := AP-TIAA-CNTRCT
        if (condition(annty_Actvty_Prap_Ap_T_Doi.greater(getZero())))                                                                                                     //Natural: IF AP-T-DOI GT 0
        {
            pnd_U_Doi.setValue(annty_Actvty_Prap_Ap_T_Doi);                                                                                                               //Natural: ASSIGN #U-DOI := AP-T-DOI
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(annty_Actvty_Prap_Ap_C_Doi.greater(getZero())))                                                                                                 //Natural: IF AP-C-DOI GT 0
            {
                pnd_U_Doi.setValue(annty_Actvty_Prap_Ap_C_Doi);                                                                                                           //Natural: ASSIGN #U-DOI := AP-C-DOI
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Doi_Pnd_Doi_Mm.setValue(pnd_U_Doi_Pnd_U_Doi_Mm);                                                                                                              //Natural: ASSIGN #DOI-MM := #U-DOI-MM
        pnd_Doi_Pnd_Doi_Yy.setValue(pnd_U_Doi_Pnd_U_Doi_Yy);                                                                                                              //Natural: ASSIGN #DOI-YY := #U-DOI-YY
        short decideConditionsMet542 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #U-DOI-YY;//Natural: VALUE '50' : '99'
        if (condition((pnd_U_Doi_Pnd_U_Doi_Yy.equals("50' : '99"))))
        {
            decideConditionsMet542++;
            pnd_Doi_Pnd_Doi_Cc.setValue("19");                                                                                                                            //Natural: ASSIGN #DOI-CC := '19'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Doi_Pnd_Doi_Cc.setValue("20");                                                                                                                            //Natural: ASSIGN #DOI-CC := '20'
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ARR2 KG
        //*  ARR2 KG
        //*  ARR2 KG
        //*  ARR2 KG
        //*  ARR2 KG
        //*  ARR2 KG
        pnd_Scil8600_Pnd_Date_Of_Issuance.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Doi_Pnd_Doi_Mm, "/01/", pnd_Doi_Pnd_Doi_Cc, pnd_Doi_Pnd_Doi_Yy));  //Natural: COMPRESS #DOI-MM '/01/' #DOI-CC #DOI-YY INTO #DATE-OF-ISSUANCE LEAVING NO
        pnd_Scil8600_Pnd_Register_Id.setValue(annty_Actvty_Prap_Ap_Register_Id);                                                                                          //Natural: ASSIGN #REGISTER-ID := AP-REGISTER-ID
        pnd_Scil8600_Pnd_Sale_Type.setValue(" ");                                                                                                                         //Natural: ASSIGN #SALE-TYPE := ' '
        pnd_Scil8600_Pnd_Exmpt_Flag.setValue(annty_Actvty_Prap_Ap_Exempt_Ind);                                                                                            //Natural: ASSIGN #EXMPT-FLAG := AP-EXEMPT-IND
        pnd_Scil8600_Pnd_Replacement_Flag.setValue(annty_Actvty_Prap_Ap_Replacement_Ind);                                                                                 //Natural: ASSIGN #REPLACEMENT-FLAG := AP-REPLACEMENT-IND
        pnd_Scil8600_Pnd_Existing_Insurer_Name_1.setValue(annty_Actvty_Prap_Ap_Arr_Insurer_1);                                                                            //Natural: ASSIGN #EXISTING-INSURER-NAME-1 := AP-ARR-INSURER-1
        pnd_Scil8600_Pnd_Exchange_1035_Flag_1.setValue(annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_1);                                                                         //Natural: ASSIGN #EXCHANGE-1035-FLAG-1 := AP-ARR-1035-EXCH-IND-1
        pnd_Scil8600_Pnd_Financed_Purchase_Flag_1.setValue(" ");                                                                                                          //Natural: ASSIGN #FINANCED-PURCHASE-FLAG-1 := ' '
        pnd_Scil8600_Pnd_Existing_Insurer_Name_2.setValue(annty_Actvty_Prap_Ap_Arr_Insurer_2);                                                                            //Natural: ASSIGN #EXISTING-INSURER-NAME-2 := AP-ARR-INSURER-2
        pnd_Scil8600_Pnd_Exchange_1035_Flag_2.setValue(annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_2);                                                                         //Natural: ASSIGN #EXCHANGE-1035-FLAG-2 := AP-ARR-1035-EXCH-IND-2
        pnd_Scil8600_Pnd_Financed_Purchase_Flag_2.setValue(" ");                                                                                                          //Natural: ASSIGN #FINANCED-PURCHASE-FLAG-2 := ' '
        pnd_Scil8600_Pnd_Existing_Insurer_Name_3.setValue(annty_Actvty_Prap_Ap_Arr_Insurer_3);                                                                            //Natural: ASSIGN #EXISTING-INSURER-NAME-3 := AP-ARR-INSURER-3
        pnd_Scil8600_Pnd_Exchange_1035_Flag_3.setValue(annty_Actvty_Prap_Ap_Arr_1035_Exch_Ind_3);                                                                         //Natural: ASSIGN #EXCHANGE-1035-FLAG-3 := AP-ARR-1035-EXCH-IND-3
        pnd_Scil8600_Pnd_Financed_Purchase_Flag_3.setValue(" ");                                                                                                          //Natural: ASSIGN #FINANCED-PURCHASE-FLAG-3 := ' '
        pnd_Scil8600_Pnd_Agent_Name.setValue(" ");                                                                                                                        //Natural: ASSIGN #AGENT-NAME := ' '
        pnd_Scil8600_Pnd_Racf_Id.setValue(annty_Actvty_Prap_Ap_Agent_Or_Racf_Id);                                                                                         //Natural: ASSIGN #RACF-ID := AP-AGENT-OR-RACF-ID
        pnd_K.reset();                                                                                                                                                    //Natural: RESET #K #STATE-OF-ISSUANCE
        pnd_Scil8600_Pnd_State_Of_Issuance.reset();
        DbsUtil.examine(new ExamineSource(ldaScil1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue("*"),3,2), new ExamineSearch(pnd_St_Of_Issuance),           //Natural: EXAMINE SUBSTR ( #STATE-ZIP-IDX ( * ) ,3,2 ) FOR #ST-OF-ISSUANCE GIVING INDEX #K
            new ExamineGivingIndex(pnd_K));
        if (condition(pnd_K.notEquals(getZero())))                                                                                                                        //Natural: IF #K NE 0
        {
            pnd_Scil8600_Pnd_State_Of_Issuance.setValue(ldaScil1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue(pnd_K).getSubstring(1,2));                    //Natural: MOVE SUBSTR ( #STATE-ZIP-IDX ( #K ) ,1,2 ) TO #STATE-OF-ISSUANCE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Header_Info() throws Exception                                                                                                                 //Natural: WRITE-HEADER-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        getWorkFiles().write(1, false, pnd_Header_Info_Pnd_Header_1, pnd_Header_Info_Pnd_Header_2, pnd_Header_Info_Pnd_Header_3, pnd_Header_Info_Pnd_Header_4,            //Natural: WRITE WORK FILE 1 #HEADER-1 #HEADER-2 #HEADER-3 #HEADER-4 #HEADER-5
            pnd_Header_Info_Pnd_Header_5);
    }
    private void sub_Write_Detail_Info() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        getWorkFiles().write(1, false, pnd_Scil8600_Pnd_Scil8600_1, pnd_Scil8600_Pnd_Scil8600_2, pnd_Scil8600_Pnd_Scil8600_3, pnd_Scil8600_Pnd_Scil8600_4,                //Natural: WRITE WORK FILE 1 #SCIL8600-1 #SCIL8600-2 #SCIL8600-3 #SCIL8600-4 #SCIL8600-5
            pnd_Scil8600_Pnd_Scil8600_5);
        pnd_Scil8600_Pnd_Last_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Scil8600_Pnd_Last_Name, ","));                                            //Natural: COMPRESS #LAST-NAME ',' INTO #LAST-NAME LEAVING NO
        pnd_Full_Name.setValue(DbsUtil.compress(pnd_Scil8600_Pnd_Last_Name, pnd_Scil8600_Pnd_First_Name));                                                                //Natural: COMPRESS #LAST-NAME #FIRST-NAME INTO #FULL-NAME
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(2),pnd_Full_Name, new AlphanumericLength (30),new ColumnSpacing(7),annty_Actvty_Prap_Ap_Replacement_Ind,  //Natural: WRITE ( 01 ) / 2X #FULL-NAME ( AL = 30 ) 7X AP-REPLACEMENT-IND ( AL = 1 ) 7X AP-REGISTER-ID ( AL = 11 ) 6X AP-AGENT-OR-RACF-ID ( AL = 15 ) 8X AP-EXEMPT-IND ( AL = 1 )
            new AlphanumericLength (1),new ColumnSpacing(7),annty_Actvty_Prap_Ap_Register_Id, new AlphanumericLength (11),new ColumnSpacing(6),annty_Actvty_Prap_Ap_Agent_Or_Racf_Id, 
            new AlphanumericLength (15),new ColumnSpacing(8),annty_Actvty_Prap_Ap_Exempt_Ind, new AlphanumericLength (1));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"Automated Replacement Register",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 25T 'Automated Replacement Register' 63T 'Run Date:' *DATX / 25T '      Summary Report          ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"      Summary Report          ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 1 ) 2
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(10),"Full Name",new TabSetting(37),"Rep Ind",new TabSetting(48),"Register Id",new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 10T 'Full Name' 37T 'Rep Ind' 48T 'Register Id' 65T 'Agent/RACF-ID' 85T 'Exempt Ind' /
                        TabSetting(65),"Agent/RACF-ID",new TabSetting(85),"Exempt Ind",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
    }
    private void CheckAtStartofData340() throws Exception
    {
        if (condition(vw_annty_Actvty_Prap.getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-INFO
            sub_Write_Header_Info();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
