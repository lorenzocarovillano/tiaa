/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:46 PM
**        * FROM NATURAL PROGRAM : Appb4101
************************************************************
**        * FILE NAME            : Appb4101.java
**        * CLASS NAME           : Appb4101
**        * INSTANCE NAME        : Appb4101
************************************************************
************************************************************************
*                                                                      *
* PROGRAM  : APPB4101 - READ WORK FILE AND PRODUCE CONTROL REPORT      *
* FUNCTION : READS WORK FILE CREATED BY APPB4100 AND PRODUCES A        *
*            CONTROL REPORT.                                           *
* DATE     : 08/18/03                                                  *
*                                                                      *
* UPDATED  : 03-23-2006 J CAMPBELL - ATRA ACTIVITY FOR COMPLIANCE      *
*              MANAGER, PROJECT REQUEST # 513: SEPARATE ATRA CONTRACTS *
*              FROM OTHER RA CONTRACTS.  ADD COUNTER FOR IRASEP.       *
*                                                        (JC2)         *
*          : 08-28-2008 N CVETKOVIC - RHSP CHANGES NBC
*          : 03-10-2010 C.AVE - INCLUDED IRA INDEXED PRODUCTS (TIGR)   *
* 06/20/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.  *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb4101 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_1;
    private DbsField pnd_Work_File_Pnd_Ap_Pin_Nbr;
    private DbsField pnd_Work_File_Pnd_Contract;
    private DbsField pnd_Work_File_Pnd_Prt_Date;
    private DbsField pnd_Work_File_Pnd_Ap_Lob;
    private DbsField pnd_Work_File_Pnd_Work_Name;
    private DbsField pnd_Lob;
    private DbsField pnd_Grand_Total;
    private DbsField pnd_Lob_Tot1;
    private DbsField pnd_Lob_Tot2;
    private DbsField pnd_Lob_Tot3;
    private DbsField pnd_Lob_Tot4;
    private DbsField pnd_Lob_Tot5;
    private DbsField pnd_Lob_Tot6;
    private DbsField pnd_Lob_Tot7;
    private DbsField pnd_Lob_Tot8;
    private DbsField pnd_Lob_Tot9;
    private DbsField pnd_Lob_Tot10;
    private DbsField pnd_Lob_Tot11;
    private DbsField pnd_Lob_Tot12;
    private DbsField pnd_Lob_Tot13;
    private DbsField pnd_Lob_Tot14;
    private DbsField pnd_Lob_Tot15;
    private DbsField pnd_Lob_Tot16;
    private DbsField pnd_Atra;

    private DbsGroup pnd_Atra__R_Field_2;
    private DbsField pnd_Atra_Pnd_Atra_A2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 71);

        pnd_Work_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_1", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Ap_Pin_Nbr = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Ap_Pin_Nbr", "#AP-PIN-NBR", FieldType.STRING, 12);
        pnd_Work_File_Pnd_Contract = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Work_File_Pnd_Prt_Date = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Prt_Date", "#PRT-DATE", FieldType.STRING, 8);
        pnd_Work_File_Pnd_Ap_Lob = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Ap_Lob", "#AP-LOB", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Work_Name = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Work_Name", "#WORK-NAME", FieldType.STRING, 39);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 6);
        pnd_Grand_Total = localVariables.newFieldInRecord("pnd_Grand_Total", "#GRAND-TOTAL", FieldType.NUMERIC, 9);
        pnd_Lob_Tot1 = localVariables.newFieldInRecord("pnd_Lob_Tot1", "#LOB-TOT1", FieldType.NUMERIC, 9);
        pnd_Lob_Tot2 = localVariables.newFieldInRecord("pnd_Lob_Tot2", "#LOB-TOT2", FieldType.NUMERIC, 9);
        pnd_Lob_Tot3 = localVariables.newFieldInRecord("pnd_Lob_Tot3", "#LOB-TOT3", FieldType.NUMERIC, 9);
        pnd_Lob_Tot4 = localVariables.newFieldInRecord("pnd_Lob_Tot4", "#LOB-TOT4", FieldType.NUMERIC, 9);
        pnd_Lob_Tot5 = localVariables.newFieldInRecord("pnd_Lob_Tot5", "#LOB-TOT5", FieldType.NUMERIC, 9);
        pnd_Lob_Tot6 = localVariables.newFieldInRecord("pnd_Lob_Tot6", "#LOB-TOT6", FieldType.NUMERIC, 9);
        pnd_Lob_Tot7 = localVariables.newFieldInRecord("pnd_Lob_Tot7", "#LOB-TOT7", FieldType.NUMERIC, 9);
        pnd_Lob_Tot8 = localVariables.newFieldInRecord("pnd_Lob_Tot8", "#LOB-TOT8", FieldType.NUMERIC, 9);
        pnd_Lob_Tot9 = localVariables.newFieldInRecord("pnd_Lob_Tot9", "#LOB-TOT9", FieldType.NUMERIC, 9);
        pnd_Lob_Tot10 = localVariables.newFieldInRecord("pnd_Lob_Tot10", "#LOB-TOT10", FieldType.NUMERIC, 9);
        pnd_Lob_Tot11 = localVariables.newFieldInRecord("pnd_Lob_Tot11", "#LOB-TOT11", FieldType.NUMERIC, 9);
        pnd_Lob_Tot12 = localVariables.newFieldInRecord("pnd_Lob_Tot12", "#LOB-TOT12", FieldType.NUMERIC, 9);
        pnd_Lob_Tot13 = localVariables.newFieldInRecord("pnd_Lob_Tot13", "#LOB-TOT13", FieldType.NUMERIC, 9);
        pnd_Lob_Tot14 = localVariables.newFieldInRecord("pnd_Lob_Tot14", "#LOB-TOT14", FieldType.NUMERIC, 9);
        pnd_Lob_Tot15 = localVariables.newFieldInRecord("pnd_Lob_Tot15", "#LOB-TOT15", FieldType.NUMERIC, 9);
        pnd_Lob_Tot16 = localVariables.newFieldInRecord("pnd_Lob_Tot16", "#LOB-TOT16", FieldType.NUMERIC, 9);
        pnd_Atra = localVariables.newFieldInRecord("pnd_Atra", "#ATRA", FieldType.STRING, 4);

        pnd_Atra__R_Field_2 = localVariables.newGroupInRecord("pnd_Atra__R_Field_2", "REDEFINE", pnd_Atra);
        pnd_Atra_Pnd_Atra_A2 = pnd_Atra__R_Field_2.newFieldInGroup("pnd_Atra_Pnd_Atra_A2", "#ATRA-A2", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Atra.setInitialValue("ATRA");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Appb4101() throws Exception
    {
        super("Appb4101");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) LS = 132 PS = 60;//Natural: FORMAT ( 01 ) LS = 132 PS = 60
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-FILE
        while (condition(getWorkFiles().read(1, pnd_Work_File)))
        {
            pnd_Grand_Total.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #GRAND-TOTAL
            getSort().writeSortInData(pnd_Work_File_Pnd_Ap_Lob, pnd_Work_File_Pnd_Ap_Pin_Nbr, pnd_Work_File_Pnd_Contract, pnd_Work_File_Pnd_Prt_Date,                     //Natural: END-ALL
                pnd_Work_File_Pnd_Work_Name);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Work_File_Pnd_Ap_Lob);                                                                                                                     //Natural: SORT #AP-LOB USING #AP-PIN-NBR #CONTRACT #PRT-DATE #WORK-NAME
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Work_File_Pnd_Ap_Lob, pnd_Work_File_Pnd_Ap_Pin_Nbr, pnd_Work_File_Pnd_Contract, pnd_Work_File_Pnd_Prt_Date, 
            pnd_Work_File_Pnd_Work_Name)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            short decideConditionsMet77 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF #AP-LOB;//Natural: VALUE 'D2'
            if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("D2"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("RA");                                                                                                                                   //Natural: MOVE 'RA' TO #LOB
                pnd_Lob_Tot1.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT1
            }                                                                                                                                                             //Natural: VALUE 'D6'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("D6"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("RL");                                                                                                                                   //Natural: MOVE 'RL' TO #LOB
                pnd_Lob_Tot2.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT2
            }                                                                                                                                                             //Natural: VALUE 'D7'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("D7"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("GRA");                                                                                                                                  //Natural: MOVE 'GRA' TO #LOB
                pnd_Lob_Tot3.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT3
            }                                                                                                                                                             //Natural: VALUE 'D9'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("D9"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("GRA-II");                                                                                                                               //Natural: MOVE 'GRA-II' TO #LOB
                pnd_Lob_Tot4.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT4
            }                                                                                                                                                             //Natural: VALUE 'I3'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("I3"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("IRAR");                                                                                                                                 //Natural: MOVE 'IRAR' TO #LOB
                pnd_Lob_Tot5.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT5
            }                                                                                                                                                             //Natural: VALUE 'I4'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("I4"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("IRAC");                                                                                                                                 //Natural: MOVE 'IRAC' TO #LOB
                pnd_Lob_Tot6.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT6
            }                                                                                                                                                             //Natural: VALUE 'I5'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("I5"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("KEOG");                                                                                                                                 //Natural: MOVE 'KEOG' TO #LOB
                pnd_Lob_Tot7.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT7
            }                                                                                                                                                             //Natural: VALUE 'S2'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("S2"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("SRA");                                                                                                                                  //Natural: MOVE 'SRA' TO #LOB
                pnd_Lob_Tot8.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT8
            }                                                                                                                                                             //Natural: VALUE 'S3'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("S3"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("GSRA");                                                                                                                                 //Natural: MOVE 'GSRA' TO #LOB
                pnd_Lob_Tot9.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LOB-TOT9
            }                                                                                                                                                             //Natural: VALUE 'S4'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("S4"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("GA");                                                                                                                                   //Natural: MOVE 'GA' TO #LOB
                pnd_Lob_Tot10.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #LOB-TOT10
                //*                                                         (JC2) BEGIN
            }                                                                                                                                                             //Natural: VALUE #ATRA-A2
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals(pnd_Atra_Pnd_Atra_A2))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue(pnd_Atra);                                                                                                                               //Natural: MOVE #ATRA TO #LOB
                pnd_Lob_Tot11.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #LOB-TOT11
            }                                                                                                                                                             //Natural: VALUE 'I6'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("I6"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("IRASEP");                                                                                                                               //Natural: MOVE 'IRASEP' TO #LOB
                pnd_Lob_Tot12.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #LOB-TOT12
                //*                                                         (JC2) END
            }                                                                                                                                                             //Natural: VALUE 'DB'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("DB"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("RHSP");                                                                                                                                 //Natural: MOVE 'RHSP' TO #LOB
                //*  TIGR START
                pnd_Lob_Tot13.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #LOB-TOT13
                //*                                                         (NBC) END
            }                                                                                                                                                             //Natural: VALUE 'I7'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("I7"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("IRIR");                                                                                                                                 //Natural: MOVE 'IRIR' TO #LOB
                pnd_Lob_Tot14.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #LOB-TOT14
            }                                                                                                                                                             //Natural: VALUE 'I8'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("I8"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("IRIC");                                                                                                                                 //Natural: MOVE 'IRIC' TO #LOB
                pnd_Lob_Tot15.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #LOB-TOT15
            }                                                                                                                                                             //Natural: VALUE 'I9'
            else if (condition((pnd_Work_File_Pnd_Ap_Lob.equals("I9"))))
            {
                decideConditionsMet77++;
                pnd_Lob.setValue("IRIS");                                                                                                                                 //Natural: MOVE 'IRIS' TO #LOB
                //*  TIGR END
                pnd_Lob_Tot16.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #LOB-TOT16
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Lob.reset();                                                                                                                                          //Natural: RESET #LOB
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  PINE
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 01 ) TITLE LEFT 1T 'PROGRAM APPB4101' 47T 'CIP INTERFACE CONTROL REPORT       ' 119T 'PAGE' *PAGE-NUMBER ( 01 ) ( EM = ZZ9 ) / 50T 'CUSTOMER/ACCOUNT IMPORT' 110T 'RUN DATE' *DATU / 1T '                TIAA                                      ' / 1T '  PIN         CONTRACT      DOB   LOB TYPE  NAME ' /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    1T '           TIAA                                           ' /
            //*    1T '  PIN    CONTRACT      DOB   LOB TYPE  NAME ' /
            //*                                                              /* PINE <<
            getReports().write(1, pnd_Work_File_Pnd_Ap_Pin_Nbr,new ColumnSpacing(2),pnd_Work_File_Pnd_Contract,pnd_Work_File_Pnd_Prt_Date,new ColumnSpacing(2),pnd_Lob,new  //Natural: WRITE ( 01 ) #AP-PIN-NBR 2X #CONTRACT #PRT-DATE 2X #LOB 3X #WORK-NAME
                ColumnSpacing(3),pnd_Work_File_Pnd_Work_Name);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #AP-LOB
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        getReports().write(1, NEWLINE,"GRAND TOTAL     ",pnd_Grand_Total);                                                                                                //Natural: WRITE ( 01 ) / 'GRAND TOTAL     ' #GRAND-TOTAL
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"END OF REPORT");                                                                                                                   //Natural: WRITE ( 01 ) / 'END OF REPORT'
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Work_File_Pnd_Ap_LobIsBreak = pnd_Work_File_Pnd_Ap_Lob.isBreak(endOfData);
        if (condition(pnd_Work_File_Pnd_Ap_LobIsBreak))
        {
            if (condition(pnd_Lob.equals("RA")))                                                                                                                          //Natural: IF #LOB EQ 'RA'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot1);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT1
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("RL")))                                                                                                                          //Natural: IF #LOB EQ 'RL'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot2);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT2
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("GRA")))                                                                                                                         //Natural: IF #LOB EQ 'GRA'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot3);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT3
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("GRA-II")))                                                                                                                      //Natural: IF #LOB EQ 'GRA-II'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot4);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT4
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("IRAR")))                                                                                                                        //Natural: IF #LOB EQ 'IRAR'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot5);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT5
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("IRAC")))                                                                                                                        //Natural: IF #LOB EQ 'IRAC'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot6);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT6
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("KEOG")))                                                                                                                        //Natural: IF #LOB EQ 'KEOG'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot7);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT7
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("SRA")))                                                                                                                         //Natural: IF #LOB EQ 'SRA'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot8);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT8
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("GSRA")))                                                                                                                        //Natural: IF #LOB EQ 'GSRA'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot9);                                                                                          //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT9
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("GA")))                                                                                                                          //Natural: IF #LOB EQ 'GA'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot10);                                                                                         //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT10
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //*                                                         (JC2) BEGIN
            if (condition(pnd_Lob.equals(pnd_Atra)))                                                                                                                      //Natural: IF #LOB EQ #ATRA
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot11);                                                                                         //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT11
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("IRASEP")))                                                                                                                      //Natural: IF #LOB EQ 'IRASEP'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot12);                                                                                         //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT12
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //*                                                         (JC2) END
            if (condition(pnd_Lob.equals("RHSP")))                                                                                                                        //Natural: IF #LOB EQ 'RHSP'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot13);                                                                                         //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT13
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //*                                                         (NBC) END
            //*  TIGR-START
            if (condition(pnd_Lob.equals("IRIR")))                                                                                                                        //Natural: IF #LOB EQ 'IRIR'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot14);                                                                                         //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT14
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("IRIC")))                                                                                                                        //Natural: IF #LOB EQ 'IRIC'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot15);                                                                                         //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT15
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lob.equals("IRIS")))                                                                                                                        //Natural: IF #LOB EQ 'IRIS'
            {
                getReports().write(1, NEWLINE,"TOTAL FOR",pnd_Lob,pnd_Lob_Tot16);                                                                                         //Natural: WRITE ( 01 ) / 'TOTAL FOR' #LOB #LOB-TOT16
                if (condition(Global.isEscape())) return;
                //*  TIGR-END
            }                                                                                                                                                             //Natural: END-IF
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(1, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),"PROGRAM APPB4101",new TabSetting(47),"CIP INTERFACE CONTROL REPORT       ",new 
            TabSetting(119),"PAGE",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,new TabSetting(50),"CUSTOMER/ACCOUNT IMPORT",new 
            TabSetting(110),"RUN DATE",Global.getDATU(),NEWLINE,new TabSetting(1),"                TIAA                                      ",NEWLINE,new 
            TabSetting(1),"  PIN         CONTRACT      DOB   LOB TYPE  NAME ",NEWLINE);
    }
}
