/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:16 PM
**        * FROM NATURAL PROGRAM : Appb304
************************************************************
**        * FILE NAME            : Appb304.java
**        * CLASS NAME           : Appb304
**        * INSTANCE NAME        : Appb304
************************************************************
************************************************************************
* PROGRAM  : APPB304 - PRAP OVERDUE LETTER FACILITY                    *
* AUTHOR   : MARIE ARACENA                                             *
* DATE     : JUNE 24, 1999                                             *
* FUNCTION : READS WORK FILE CREATED BY APPB302/APPB303 AND FEEDS INTO *
*            THE POST SYSTEM TO CREATE THE OVERDUE LETTERS FOR MONTHLY *
*            AND QUARTERLY RUNS.                                       *
* UPDATED  : 09/05/00 V.RAQUENO - USE SINGLE FUNCTION TO CALL IIS(VR1) *
*          : 08/01/01 K.GATES   - PRAP-LETTER-DETAIL-DAYSAFTER  NOW    *
*                                 POPULATED BY CV-DAY-DIFF INSTEAD OF  *
*                                 CV-STATUS FIELD.                     *
*          : 08/06/01 K.GATES   - CHANGED SIGNATURES FOR OVERDUE LETTER*
*          : 11/08/01 K.GATES   - 457(B) CHANGES SEE 457(B) KG         *
*          : 11/19/01 K.GATES   - PRODUCTION FIX FOR FIRST RECORD READ *
*          :                      WITH NAMES LONGER THAN 35 CHARACTES  *
*          :                      SEE PROD. FIX KG 11/19/01            *
*   *DATU  : 04/25/04 K.GATES     SUNGARD RELEASE 2  SEE SGRD KG       *
*          :                    - EXCLUDED SUNGARD CONTRACTS FROM CALL *
*          :                      TO IIS INSTEAD HARDCODED CONTACT     *
*          :                      INFO FOR SUNGARD.                    *
*          :                    - HARDCODED INST-LINK-CDE FOR SUNGARD. *
*          : 08/23/04 R.WILLIS  - REL 3 CHANGES                        *
*          : 12/27/04 R.WILLIS  - TNT/REL4. CHANGES  RW1               *
*          : 03/22/05 R.WILLIS  - ICAP/REL5. CHANGES  RW2              *
*          : 04/07/05 K.GATES   - ICAP/REL5. CONTACT CHANGES  SGRD5 KG *
*          : 09/09/05 D.MARPURI - IRAS/REL6. INCLUDE IRA SEP LOB/LOB   *
*          : 03/09/05 JANET B.  - PRINT ON FILE FOR SSN ON LETTER JRB1 *
*          : 09/26/06 K.GATES   - ADD DIV/SUB FOR DCA  SEE DCA KG      *
*          : 01/08/07 K.GATES   - ADD DIV/SUB FOR SUNY CCR-SUNY CCR KG *
*          : 08/28/07 N.CVETKOVIC- RHSP CHANGES NBC
*          : 09/25/09 DEVELBISS - CHANGED SIGNATURE AND TITLE- BJD SIG *
*          : 03/10/10 C.AVE     - INCLUDED IRA INDEXED PRODUCTS - TIGR *
*          : 02/08/12 D.WOLF    - REDUCE #ET-LIMIT TO PREVENT TIMEOUT. *
*          :                    THE ET LIMIT CONTROLS HOW OFTEN AN END *
*          :                    TRANSACTION IS DONE. ONE ET PER PLAN   *
*          :                    BREAK.                            (DW) *
* 06/20/2017  MITRAPU           PIN EXPANSION CHANGES.(C425939) PINE.  *
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb304 extends BLNatBase
{
    // Data Areas
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaIisa8000 pdaIisa8000;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private LdaAppl302 ldaAppl302;
    private LdaPstl6335 ldaPstl6335;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCdaobj pdaCdaobj;
    private PdaScia8200 pdaScia8200;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Full_Name;

    private DbsGroup pnd_Full_Name__R_Field_1;
    private DbsField pnd_Full_Name_Pnd_F_M_L;
    private DbsField pnd_Full_Name_Pnd_Overflow;
    private DbsField pnd_Contract;
    private DbsField pnd_Title;
    private DbsField pnd_I;
    private DbsField pnd_J;

    private DbsGroup pnd_Control_Counters;
    private DbsField pnd_Control_Counters_Pnd_Rec_In;
    private DbsField pnd_Control_Counters_Pnd_Rec_Out;
    private DbsField pnd_Control_Counters_Pnd_Et_Ctr;
    private DbsField pnd_Control_Counters_Pnd_Status_A;
    private DbsField pnd_Control_Counters_Pnd_Status_P;
    private DbsField pnd_Control_Counters_Pnd_Stat_Desc;
    private DbsField pnd_Control_Counters_Pnd_Sub_Total;
    private DbsField pnd_Control_Counters_Pnd_Grand;
    private DbsField pnd_Control_Counters_Pnd_Packages;

    private DbsGroup pnd_Indicators;
    private DbsField pnd_Indicators_Pnd_Newpage_Ind;
    private DbsField pnd_Indicators_Pnd_Write;
    private DbsField pnd_Indicators_Pnd_Store_Post;
    private DbsField pnd_Indicators_Pnd_Post_Open;

    private DbsGroup pnd_Work_Restart_Area;

    private DbsGroup pnd_Work_Restart_Area_Pnd_Restart_Fields;
    private DbsField pnd_Work_Restart_Area_Pnd_Restart_Text;
    private DbsField pnd_Work_Restart_Area_Pnd_Restart_Rec;
    private DbsField pnd_Work_Restart_Area_Pnd_Restart_Pin;
    private DbsField pnd_Work_Restart_Area_Pnd_Restart_Contract;
    private DbsField pnd_Work_Restart_Area_Pnd_Reset;
    private DbsField pnd_Work_Restart_Area_Pnd_Rec_Read;
    private DbsField pnd_Work_Restart_Area_Pnd_Et_Cnt;
    private DbsField pnd_Work_Restart_Area_Pnd_Et_Limit;
    private DbsField pnd_Work_Restart_Area_Pnd_Restart_Ind;
    private DbsField pnd_Work_Restart_Area_Pnd_File_Type;
    private DbsField pnd_Branch;
    private DbsField pnd_Signature;
    private DbsField pnd_Univ_Id;

    private DbsGroup pnd_Univ_Id__R_Field_2;
    private DbsField pnd_Univ_Id_Pnd_Univ_Inst;
    private DbsField pnd_Univ_Id_Pnd_Univ_Ppg;
    private DbsField pnd_Addr;
    private DbsField pnd_Test_Name;
    private DbsField pnd_Where;
    private DbsField pnd_Sgrd;
    private DbsField pnd_Sgrd_Call_Cnt;
    private DbsField pnd_File_Open_Sw;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Pnd_Wk_Compressed_Nme;
    private DbsField pnd_Work_Pnd_Wk_F_Name;
    private DbsField pnd_Work_Pnd_Wk_L_Name;
    private DbsField pnd_Work_Pnd_Wk_M_Name;
    private DbsField pnd_Work_Pnd_Wk_Zip;

    private DbsGroup pnd_Work__R_Field_3;
    private DbsField pnd_Work_Pnd_Wk_Zip5;
    private DbsField pnd_Work_Pnd_Wk_Zip4;
    private DbsField pnd_Work_Pnd_Wk_Zip9;
    private DbsField pnd_Work_Pnd_Wk_Cntr;
    private DbsField pnd_Work_Pnd_Wk_City;
    private DbsField pnd_Work_Pnd_Wk_City_State_Zip9;
    private DbsField pnd_Work_Pnd_Addr2_Fnd;
    private DbsField pnd_Work_Pnd_Addr3_Fnd;
    private DbsField pnd_Ab_Plan;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaIisa8000 = new PdaIisa8000(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        ldaAppl302 = new LdaAppl302();
        registerRecord(ldaAppl302);
        ldaPstl6335 = new LdaPstl6335();
        registerRecord(ldaPstl6335);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaScia8200 = new PdaScia8200(localVariables);

        // Local Variables
        pnd_Full_Name = localVariables.newFieldInRecord("pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 90);

        pnd_Full_Name__R_Field_1 = localVariables.newGroupInRecord("pnd_Full_Name__R_Field_1", "REDEFINE", pnd_Full_Name);
        pnd_Full_Name_Pnd_F_M_L = pnd_Full_Name__R_Field_1.newFieldInGroup("pnd_Full_Name_Pnd_F_M_L", "#F-M-L", FieldType.STRING, 35);
        pnd_Full_Name_Pnd_Overflow = pnd_Full_Name__R_Field_1.newFieldInGroup("pnd_Full_Name_Pnd_Overflow", "#OVERFLOW", FieldType.STRING, 55);
        pnd_Contract = localVariables.newFieldInRecord("pnd_Contract", "#CONTRACT", FieldType.STRING, 8);
        pnd_Title = localVariables.newFieldInRecord("pnd_Title", "#TITLE", FieldType.STRING, 35);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);

        pnd_Control_Counters = localVariables.newGroupInRecord("pnd_Control_Counters", "#CONTROL-COUNTERS");
        pnd_Control_Counters_Pnd_Rec_In = pnd_Control_Counters.newFieldInGroup("pnd_Control_Counters_Pnd_Rec_In", "#REC-IN", FieldType.NUMERIC, 7);
        pnd_Control_Counters_Pnd_Rec_Out = pnd_Control_Counters.newFieldInGroup("pnd_Control_Counters_Pnd_Rec_Out", "#REC-OUT", FieldType.NUMERIC, 7);
        pnd_Control_Counters_Pnd_Et_Ctr = pnd_Control_Counters.newFieldInGroup("pnd_Control_Counters_Pnd_Et_Ctr", "#ET-CTR", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Control_Counters_Pnd_Status_A = pnd_Control_Counters.newFieldArrayInGroup("pnd_Control_Counters_Pnd_Status_A", "#STATUS-A", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 10));
        pnd_Control_Counters_Pnd_Status_P = pnd_Control_Counters.newFieldArrayInGroup("pnd_Control_Counters_Pnd_Status_P", "#STATUS-P", FieldType.NUMERIC, 
            7, new DbsArrayController(1, 10));
        pnd_Control_Counters_Pnd_Stat_Desc = pnd_Control_Counters.newFieldArrayInGroup("pnd_Control_Counters_Pnd_Stat_Desc", "#STAT-DESC", FieldType.STRING, 
            16, new DbsArrayController(1, 10));
        pnd_Control_Counters_Pnd_Sub_Total = pnd_Control_Counters.newFieldInGroup("pnd_Control_Counters_Pnd_Sub_Total", "#SUB-TOTAL", FieldType.NUMERIC, 
            7);
        pnd_Control_Counters_Pnd_Grand = pnd_Control_Counters.newFieldInGroup("pnd_Control_Counters_Pnd_Grand", "#GRAND", FieldType.NUMERIC, 7);
        pnd_Control_Counters_Pnd_Packages = pnd_Control_Counters.newFieldInGroup("pnd_Control_Counters_Pnd_Packages", "#PACKAGES", FieldType.NUMERIC, 
            7);

        pnd_Indicators = localVariables.newGroupInRecord("pnd_Indicators", "#INDICATORS");
        pnd_Indicators_Pnd_Newpage_Ind = pnd_Indicators.newFieldInGroup("pnd_Indicators_Pnd_Newpage_Ind", "#NEWPAGE-IND", FieldType.STRING, 1);
        pnd_Indicators_Pnd_Write = pnd_Indicators.newFieldInGroup("pnd_Indicators_Pnd_Write", "#WRITE", FieldType.STRING, 1);
        pnd_Indicators_Pnd_Store_Post = pnd_Indicators.newFieldInGroup("pnd_Indicators_Pnd_Store_Post", "#STORE-POST", FieldType.STRING, 1);
        pnd_Indicators_Pnd_Post_Open = pnd_Indicators.newFieldInGroup("pnd_Indicators_Pnd_Post_Open", "#POST-OPEN", FieldType.STRING, 1);

        pnd_Work_Restart_Area = localVariables.newGroupInRecord("pnd_Work_Restart_Area", "#WORK-RESTART-AREA");

        pnd_Work_Restart_Area_Pnd_Restart_Fields = pnd_Work_Restart_Area.newGroupInGroup("pnd_Work_Restart_Area_Pnd_Restart_Fields", "#RESTART-FIELDS");
        pnd_Work_Restart_Area_Pnd_Restart_Text = pnd_Work_Restart_Area_Pnd_Restart_Fields.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Restart_Text", "#RESTART-TEXT", 
            FieldType.STRING, 1);
        pnd_Work_Restart_Area_Pnd_Restart_Rec = pnd_Work_Restart_Area_Pnd_Restart_Fields.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Restart_Rec", "#RESTART-REC", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Work_Restart_Area_Pnd_Restart_Pin = pnd_Work_Restart_Area_Pnd_Restart_Fields.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Restart_Pin", "#RESTART-PIN", 
            FieldType.NUMERIC, 12);
        pnd_Work_Restart_Area_Pnd_Restart_Contract = pnd_Work_Restart_Area_Pnd_Restart_Fields.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Restart_Contract", 
            "#RESTART-CONTRACT", FieldType.STRING, 10);
        pnd_Work_Restart_Area_Pnd_Reset = pnd_Work_Restart_Area.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Reset", "#RESET", FieldType.STRING, 5);
        pnd_Work_Restart_Area_Pnd_Rec_Read = pnd_Work_Restart_Area.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Work_Restart_Area_Pnd_Et_Cnt = pnd_Work_Restart_Area.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Work_Restart_Area_Pnd_Et_Limit = pnd_Work_Restart_Area.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Et_Limit", "#ET-LIMIT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Work_Restart_Area_Pnd_Restart_Ind = pnd_Work_Restart_Area.newFieldInGroup("pnd_Work_Restart_Area_Pnd_Restart_Ind", "#RESTART-IND", FieldType.BOOLEAN, 
            1);
        pnd_Work_Restart_Area_Pnd_File_Type = pnd_Work_Restart_Area.newFieldInGroup("pnd_Work_Restart_Area_Pnd_File_Type", "#FILE-TYPE", FieldType.STRING, 
            8);
        pnd_Branch = localVariables.newFieldArrayInRecord("pnd_Branch", "#BRANCH", FieldType.STRING, 1, new DbsArrayController(1, 11));
        pnd_Signature = localVariables.newFieldArrayInRecord("pnd_Signature", "#SIGNATURE", FieldType.STRING, 1, new DbsArrayController(1, 11));
        pnd_Univ_Id = localVariables.newFieldInRecord("pnd_Univ_Id", "#UNIV-ID", FieldType.STRING, 13);

        pnd_Univ_Id__R_Field_2 = localVariables.newGroupInRecord("pnd_Univ_Id__R_Field_2", "REDEFINE", pnd_Univ_Id);
        pnd_Univ_Id_Pnd_Univ_Inst = pnd_Univ_Id__R_Field_2.newFieldInGroup("pnd_Univ_Id_Pnd_Univ_Inst", "#UNIV-INST", FieldType.NUMERIC, 6);
        pnd_Univ_Id_Pnd_Univ_Ppg = pnd_Univ_Id__R_Field_2.newFieldInGroup("pnd_Univ_Id_Pnd_Univ_Ppg", "#UNIV-PPG", FieldType.STRING, 7);
        pnd_Addr = localVariables.newFieldArrayInRecord("pnd_Addr", "#ADDR", FieldType.STRING, 35, new DbsArrayController(1, 6));
        pnd_Test_Name = localVariables.newFieldInRecord("pnd_Test_Name", "#TEST-NAME", FieldType.STRING, 35);
        pnd_Where = localVariables.newFieldInRecord("pnd_Where", "#WHERE", FieldType.NUMERIC, 2);
        pnd_Sgrd = localVariables.newFieldInRecord("pnd_Sgrd", "#SGRD", FieldType.BOOLEAN, 1);
        pnd_Sgrd_Call_Cnt = localVariables.newFieldInRecord("pnd_Sgrd_Call_Cnt", "#SGRD-CALL-CNT", FieldType.INTEGER, 4);
        pnd_File_Open_Sw = localVariables.newFieldInRecord("pnd_File_Open_Sw", "#FILE-OPEN-SW", FieldType.STRING, 1);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Pnd_Wk_Compressed_Nme = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Compressed_Nme", "#WK-COMPRESSED-NME", FieldType.STRING, 40);
        pnd_Work_Pnd_Wk_F_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_F_Name", "#WK-F-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_L_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_L_Name", "#WK-L-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_M_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_M_Name", "#WK-M-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_Zip = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip", "#WK-ZIP", FieldType.STRING, 9);

        pnd_Work__R_Field_3 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_3", "REDEFINE", pnd_Work_Pnd_Wk_Zip);
        pnd_Work_Pnd_Wk_Zip5 = pnd_Work__R_Field_3.newFieldInGroup("pnd_Work_Pnd_Wk_Zip5", "#WK-ZIP5", FieldType.STRING, 5);
        pnd_Work_Pnd_Wk_Zip4 = pnd_Work__R_Field_3.newFieldInGroup("pnd_Work_Pnd_Wk_Zip4", "#WK-ZIP4", FieldType.STRING, 4);
        pnd_Work_Pnd_Wk_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip9", "#WK-ZIP9", FieldType.STRING, 10);
        pnd_Work_Pnd_Wk_Cntr = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Cntr", "#WK-CNTR", FieldType.INTEGER, 2);
        pnd_Work_Pnd_Wk_City = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City", "#WK-CITY", FieldType.STRING, 29);
        pnd_Work_Pnd_Wk_City_State_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City_State_Zip9", "#WK-CITY-STATE-ZIP9", FieldType.STRING, 35);
        pnd_Work_Pnd_Addr2_Fnd = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Addr2_Fnd", "#ADDR2-FND", FieldType.BOOLEAN, 1);
        pnd_Work_Pnd_Addr3_Fnd = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Addr3_Fnd", "#ADDR3-FND", FieldType.BOOLEAN, 1);
        pnd_Ab_Plan = localVariables.newFieldInRecord("pnd_Ab_Plan", "#AB-PLAN", FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl302.initializeValues();
        ldaPstl6335.initializeValues();

        localVariables.reset();
        pnd_Work_Restart_Area_Pnd_Et_Limit.setInitialValue(10);
        pnd_Work_Restart_Area_Pnd_Restart_Ind.setInitialValue(false);
        pnd_Work_Restart_Area_Pnd_File_Type.setInitialValue("ARACIS1");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb304() throws Exception
    {
        super("Appb304");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Appb304|Main");
        OnErrorManager.pushEvent("APPB304", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 132 HW = OFF PS = 52;//Natural: FORMAT ( 2 ) LS = 132 HW = OFF PS = 52;//Natural: FORMAT ( 3 ) LS = 132 HW = OFF PS = 52
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: MOVE 'INFP9000' TO *ERROR-TA
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(1).setValue("Deleted");                                                                                       //Natural: MOVE 'Deleted' TO #STAT-DESC ( 1 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(2).setValue("Assigned");                                                                                      //Natural: MOVE 'Assigned' TO #STAT-DESC ( 2 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(3).setValue("Match & Release");                                                                               //Natural: MOVE 'Match & Release' TO #STAT-DESC ( 3 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(4).setValue("Released");                                                                                      //Natural: MOVE 'Released' TO #STAT-DESC ( 4 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(5).setValue("Unassigned");                                                                                    //Natural: MOVE 'Unassigned' TO #STAT-DESC ( 5 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(6).setValue("Suspense");                                                                                      //Natural: MOVE 'Suspense' TO #STAT-DESC ( 6 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(7).setValue("Overdue 30 Days");                                                                               //Natural: MOVE 'Overdue 30 Days' TO #STAT-DESC ( 7 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(8).setValue("Overdue 60 Days");                                                                               //Natural: MOVE 'Overdue 60 Days' TO #STAT-DESC ( 8 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(9).setValue("Overdue 90 Days");                                                                               //Natural: MOVE 'Overdue 90 Days' TO #STAT-DESC ( 9 )
                pnd_Control_Counters_Pnd_Stat_Desc.getValue(10).setValue("Unknown");                                                                                      //Natural: MOVE 'Unknown' TO #STAT-DESC ( 10 )
                //*  THIS TELLS YOU IF SOMETHING IS ON THE STACK
                //*  IN THIS CASE IT WOULD BE 'RESET' IN THE JCL
                if (condition(Global.getSTACK().getDatacount().equals(1), INPUT_1))                                                                                       //Natural: IF *DATA = 1
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Work_Restart_Area_Pnd_Reset);                                                                  //Natural: INPUT #RESET
                    //*  NEXT TO THE PROGRAM NAME.
                    if (condition(pnd_Work_Restart_Area_Pnd_Reset.equals("RESET")))                                                                                       //Natural: IF #RESET = 'RESET'
                    {
                        //*  THE ' ' AFTER ET DELETES THE RECORD HELD BY
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION ' '
                        //*  ADABAS (WORK AREA).
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  READS DATA SAVED WITH PREVIOUS ET
                //*  THESE RESTART FIELDS WILL BE SAVED WHEN
                //*  AN ET IS HIT
                //*                                                                                                                                                       //Natural: GET TRANSACTION DATA #RESTART-TEXT #RESTART-REC #RESTART-PIN #RESTART-CONTRACT
                if (condition(pnd_Work_Restart_Area_Pnd_Restart_Text.notEquals(" ")))                                                                                     //Natural: IF #RESTART-TEXT NE ' '
                {
                    pnd_Work_Restart_Area_Pnd_Restart_Ind.setValue(true);                                                                                                 //Natural: MOVE TRUE TO #RESTART-IND
                    getReports().write(1, new TabSetting(7),"RESTART DATA: ",NEWLINE,new TabSetting(22),"RECORDS PROCESSED...:",pnd_Work_Restart_Area_Pnd_Restart_Rec,    //Natural: WRITE ( 1 ) 7T 'RESTART DATA: ' / 22T 'RECORDS PROCESSED...:' #RESTART-REC / 22T 'PIN ................:' #RESTART-PIN / 22T 'CONTRACT............:' #RESTART-CONTRACT
                        new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(22),"PIN ................:",pnd_Work_Restart_Area_Pnd_Restart_Pin,NEWLINE,new 
                        TabSetting(22),"CONTRACT............:",pnd_Work_Restart_Area_Pnd_Restart_Contract);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Work_Restart_Area_Pnd_Restart_Rec.reset();                                                                                                        //Natural: RESET #RESTART-REC
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOAD-SIGNATURE-TABLE
                sub_Load_Signature_Table();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK FILE 1 CV-RULE-DATA-L CV-RULE-DATA-M
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_L(), ldaAppl302.getCv_Prap_Rule1_Cv_Rule_Data_M())))
                {
                    CheckAtStartofData855();

                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_Work_Restart_Area_Pnd_Rec_Read.nadd(1);                                                                                                           //Natural: ADD 1 TO #REC-READ
                    //*  08/23/04
                    if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals("SGRD")))                                                                             //Natural: IF CV-COLL-CODE = 'SGRD'
                    {
                        //*  08/23/04
                        ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No());                                               //Natural: MOVE CV-SGRD-PLAN-NO TO CV-COLL-CODE
                        //*  08/23/04
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    //*  IS THIS A RESTART ?
                    if (condition(pnd_Work_Restart_Area_Pnd_Restart_Ind.getBoolean()))                                                                                    //Natural: IF #RESTART-IND
                    {
                        //*  IF YES, WE DO THIS.
                        if (condition(pnd_Work_Restart_Area_Pnd_Rec_Read.less(pnd_Work_Restart_Area_Pnd_Restart_Rec)))                                                    //Natural: IF #REC-READ LT #RESTART-REC
                        {
                            //*  IF NOT WE BYPASS THESE
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                            //*  'IF' STATEMENTS.
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Work_Restart_Area_Pnd_Restart_Ind.setValue(false);                                                                                            //Natural: MOVE FALSE TO #RESTART-IND
                        //*  IF IT's equal
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals(" ")))                                                                                //Natural: IF CV-COLL-CODE EQ ' '
                    {
                        getReports().display(2, "PPG",                                                                                                                    //Natural: DISPLAY ( 2 ) 'PPG' CV-COLL-CODE 'LAST/NAME' CV-COR-LAST-NME 'FIRST/NAME' CV-COR-FIRST-NME 'SOCIAL/SECURITY NUMBER' CV-SOC-SEC ( EM = 999-99-9999 )
                        		ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(),"LAST/NAME",
                        		ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(),"FIRST/NAME",
                        		ldaAppl302.getCv_Prap_Rule1_Cv_Cor_First_Nme(),"SOCIAL/SECURITY NUMBER",
                        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Control_Counters_Pnd_Rec_In.nadd(1);                                                                                                              //Natural: ADD 1 TO #REC-IN
                    if (condition(pnd_Control_Counters_Pnd_Rec_In.equals(1) && pnd_Indicators_Pnd_Post_Open.equals(" ")))                                                 //Natural: IF #REC-IN EQ 1 AND #POST-OPEN EQ ' '
                    {
                                                                                                                                                                          //Natural: PERFORM GET-INST-ADDR
                        sub_Get_Inst_Addr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*    WRITE 'GOING TO ASSIGN-POST-DATA 1ST' CV-COLL-CODE
                        //*  PROD. FIX KG 11/19/01
                        pnd_I.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #I
                                                                                                                                                                          //Natural: PERFORM ASSIGN-POST-DATA
                        sub_Assign_Post_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  PROD. FIX KG 11/19/01
                        pnd_I.reset();                                                                                                                                    //Natural: RESET #I
                        getReports().write(0, "GOING TO READ POST-OPEN");                                                                                                 //Natural: WRITE 'GOING TO READ POST-OPEN'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM POST-OPEN
                        sub_Post_Open();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(0, "GOING OUT FROM POST-OPEN");                                                                                                //Natural: WRITE 'GOING OUT FROM POST-OPEN'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pdaPsta9500.getPsta9500().reset();                                                                                                                //Natural: RESET PSTA9500
                                                                                                                                                                          //Natural: PERFORM LOAD-LETTER-DATA
                        sub_Load_Letter_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-POST
                    sub_Write_Post();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Control_Counters_Pnd_Rec_Out.nadd(1);                                                                                                             //Natural: ADD 1 TO #REC-OUT
                    short decideConditionsMet910 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE CV-STATUS;//Natural: VALUE 'A'
                    if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("A"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(1);                                                                                                                                //Natural: MOVE 1 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'B'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("B"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(2);                                                                                                                                //Natural: MOVE 2 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("C"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(3);                                                                                                                                //Natural: MOVE 3 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'D'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("D"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(4);                                                                                                                                //Natural: MOVE 4 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'E'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("E"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(5);                                                                                                                                //Natural: MOVE 5 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'F'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("F"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(6);                                                                                                                                //Natural: MOVE 6 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'G'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("G"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(7);                                                                                                                                //Natural: MOVE 7 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'H'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("H"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(8);                                                                                                                                //Natural: MOVE 8 TO #J
                    }                                                                                                                                                     //Natural: VALUE 'I'
                    else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Status().equals("I"))))
                    {
                        decideConditionsMet910++;
                        pnd_J.setValue(9);                                                                                                                                //Natural: MOVE 9 TO #J
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_J.setValue(10);                                                                                                                               //Natural: MOVE 10 TO #J
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(1)))                                                                                //Natural: IF CV-RECORD-TYPE EQ 1
                    {
                        pnd_Control_Counters_Pnd_Status_A.getValue(pnd_J).nadd(1);                                                                                        //Natural: ADD 1 TO #STATUS-A ( #J )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Control_Counters_Pnd_Status_P.getValue(pnd_J).nadd(1);                                                                                        //Natural: ADD 1 TO #STATUS-P ( #J )
                        //*  DCA KG
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT BREAK OF CV-SGRD-DIVSUB
                    //*                                                                                                                                                   //Natural: AT BREAK OF CV-COLL-CODE
                    //*                                                                                                                                                   //Natural: AT BREAK OF CV-SGRD-PLAN-NO
                    //*                                                                                                                                                   //Natural: AT BREAK OF CV-KEY
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION ' '
                getReports().write(1, "*",new RepeatItem(79),NEWLINE,NEWLINE,"Program",Global.getPROGRAM(),new ColumnSpacing(50),"Date",Global.getDATU(),NEWLINE,new      //Natural: WRITE ( 1 ) '*' ( 79 ) // 'Program' *PROGRAM 50X 'Date' *DATU / 22X #TITLE // '     Total records read from extract ' #REC-READ ( EM = Z,ZZZ,ZZ9 ) / '     Total records accepted          ' #REC-IN ( EM = Z,ZZZ,ZZ9 ) / '     Total records written to POST   ' #REC-OUT ( EM = Z,ZZZ,ZZ9 ) / '     Total packages written to POST  ' #PACKAGES ( EM = Z,ZZZ,ZZ9 ) //
                    ColumnSpacing(22),pnd_Title,NEWLINE,NEWLINE,"     Total records read from extract ",pnd_Work_Restart_Area_Pnd_Rec_Read, new ReportEditMask 
                    ("Z,ZZZ,ZZ9"),NEWLINE,"     Total records accepted          ",pnd_Control_Counters_Pnd_Rec_In, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"     Total records written to POST   ",pnd_Control_Counters_Pnd_Rec_Out, 
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"     Total packages written to POST  ",pnd_Control_Counters_Pnd_Packages, new ReportEditMask 
                    ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
                pnd_Indicators_Pnd_Write.reset();                                                                                                                         //Natural: RESET #WRITE
                FOR01:                                                                                                                                                    //Natural: FOR #J 1 10
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(10)); pnd_J.nadd(1))
                {
                    if (condition(pnd_Control_Counters_Pnd_Status_A.getValue(pnd_J).equals(getZero())))                                                                   //Natural: IF #STATUS-A ( #J ) EQ 0
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Indicators_Pnd_Write.equals(" ")))                                                                                                  //Natural: IF #WRITE EQ ' '
                    {
                        getReports().write(1, "    APPLICATIONS",NEWLINE);                                                                                                //Natural: WRITE ( 1 ) '    APPLICATIONS' /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Indicators_Pnd_Write.setValue("Y");                                                                                                           //Natural: MOVE 'Y' TO #WRITE
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, new ColumnSpacing(6),pnd_Control_Counters_Pnd_Stat_Desc.getValue(pnd_J),new ColumnSpacing(6),pnd_Control_Counters_Pnd_Status_A.getValue(pnd_J),  //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #J ) 6X #STATUS-A ( #J ) ( EM = Z,ZZZ,ZZZ )
                        new ReportEditMask ("Z,ZZZ,ZZZ"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Control_Counters_Pnd_Sub_Total.nadd(pnd_Control_Counters_Pnd_Status_A.getValue(pnd_J));                                                           //Natural: ADD #STATUS-A ( #J ) TO #SUB-TOTAL
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (condition(pnd_Control_Counters_Pnd_Sub_Total.notEquals(getZero())))                                                                                   //Natural: IF #SUB-TOTAL NE 0
                {
                    getReports().write(1, NEWLINE,"    Total Apps ******",new ColumnSpacing(17),pnd_Control_Counters_Pnd_Sub_Total, new ReportEditMask                    //Natural: WRITE ( 1 ) / '    Total Apps ******' 17X #SUB-TOTAL ( EM = Z,ZZZ,ZZZ )
                        ("Z,ZZZ,ZZZ"));
                    if (Global.isEscape()) return;
                    pnd_Control_Counters_Pnd_Grand.nadd(pnd_Control_Counters_Pnd_Sub_Total);                                                                              //Natural: ADD #SUB-TOTAL TO #GRAND
                    pnd_Control_Counters_Pnd_Sub_Total.reset();                                                                                                           //Natural: RESET #SUB-TOTAL
                }                                                                                                                                                         //Natural: END-IF
                pnd_Indicators_Pnd_Write.reset();                                                                                                                         //Natural: RESET #WRITE
                FOR02:                                                                                                                                                    //Natural: FOR #J 1 10
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(10)); pnd_J.nadd(1))
                {
                    if (condition(pnd_Control_Counters_Pnd_Status_P.getValue(pnd_J).equals(getZero())))                                                                   //Natural: IF #STATUS-P ( #J ) EQ 0
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Indicators_Pnd_Write.equals(" ")))                                                                                                  //Natural: IF #WRITE EQ ' '
                    {
                        getReports().write(1, NEWLINE,NEWLINE,"    PREMIUMS",NEWLINE);                                                                                    //Natural: WRITE ( 1 ) // '    PREMIUMS' /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Indicators_Pnd_Write.setValue("Y");                                                                                                           //Natural: MOVE 'Y' TO #WRITE
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, new ColumnSpacing(6),pnd_Control_Counters_Pnd_Stat_Desc.getValue(pnd_J),new ColumnSpacing(6),pnd_Control_Counters_Pnd_Status_P.getValue(pnd_J),  //Natural: WRITE ( 1 ) 6X #STAT-DESC ( #J ) 6X #STATUS-P ( #J ) ( EM = Z,ZZZ,ZZZ )
                        new ReportEditMask ("Z,ZZZ,ZZZ"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Control_Counters_Pnd_Sub_Total.nadd(pnd_Control_Counters_Pnd_Status_P.getValue(pnd_J));                                                           //Natural: ADD #STATUS-P ( #J ) TO #SUB-TOTAL
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (condition(pnd_Control_Counters_Pnd_Sub_Total.notEquals(getZero())))                                                                                   //Natural: IF #SUB-TOTAL NE 0
                {
                    getReports().write(1, NEWLINE,"    Total premiums **",new ColumnSpacing(17),pnd_Control_Counters_Pnd_Sub_Total, new ReportEditMask                    //Natural: WRITE ( 1 ) / '    Total premiums **' 17X #SUB-TOTAL ( EM = Z,ZZZ,ZZZ )
                        ("Z,ZZZ,ZZZ"));
                    if (Global.isEscape()) return;
                    pnd_Control_Counters_Pnd_Grand.nadd(pnd_Control_Counters_Pnd_Sub_Total);                                                                              //Natural: ADD #SUB-TOTAL TO #GRAND
                    pnd_Control_Counters_Pnd_Sub_Total.reset();                                                                                                           //Natural: RESET #SUB-TOTAL
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, NEWLINE,NEWLINE,"    Grand Total *****",new ColumnSpacing(17),pnd_Control_Counters_Pnd_Grand, new ReportEditMask                    //Natural: WRITE ( 1 ) //'    Grand Total *****' 17X #GRAND ( EM = Z,ZZZ,ZZZ ) / '*' ( 79 )
                    ("Z,ZZZ,ZZZ"),NEWLINE,"*",new RepeatItem(79));
                if (Global.isEscape()) return;
                //*  08/23/04
                getReports().write(1, "NUMBER OF CALLS TO OMNI:",pnd_Sgrd_Call_Cnt);                                                                                      //Natural: WRITE ( 1 ) 'NUMBER OF CALLS TO OMNI:' #SGRD-CALL-CNT
                if (Global.isEscape()) return;
                //*  CLOSE FILE - CALL TO OMNI FOR CONTACT INFORMATION        /* 08/23/04
                //*  ----------                                               /* 08/23/04
                //*  08/23/04
                if (condition(pnd_Sgrd_Call_Cnt.greater(getZero())))                                                                                                      //Natural: IF #SGRD-CALL-CNT > 0
                {
                    //*  08/23/04
                    //*  08/23/04
                    pdaScia8200.getCon_Part_Rec().reset();                                                                                                                //Natural: RESET CON-PART-REC
                    pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("Y");                                                                                               //Natural: ASSIGN CON-EOF-SW := 'Y'
                    //*  08/23/04 SGRD5
                    DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                         //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
                    if (condition(Global.isEscape())) return;
                    //*  08/23/04
                }                                                                                                                                                         //Natural: END-IF
                //* *====================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-SIGNATURE-TABLE
                //* *================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-POST-DATA
                //*    VALUE '1'     MOVE 'Stephanie Morano' TO PSTA9612.EMPL-SGNTRY-NME
                //*    VALUE '2'     MOVE 'Bradley Finley'   TO PSTA9612.EMPL-SGNTRY-NME
                //*    VALUE '3'     MOVE 'Dorie Driscoll'   TO PSTA9612.EMPL-SGNTRY-NME
                //*    VALUE '2'     MOVE 'Bradley Finley'   TO PSTA9612.EMPL-SGNTRY-NME
                //*    NONE          MOVE 'Stephanie Morano' TO PSTA9612.EMPL-SGNTRY-NME
                //* *=============================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INST-ADDR
                //*    BOTH ADDRESS LINES BLANK
                //*    ------------------------
                //*    LINE 2 HAS DATA, LINE 3 DOES NOT
                //*    --------------------------------
                //*    LINE 2 & 3 ARE BOTH BLANK
                //*    -------------------------
                //*    LINE 2 BLANK BUT LINE 3 HAS DATA
                //*    --------------------------------
                //*  #FUNCT-CDE-IN(1)   := '01'
                //*  #FUNCT-CDE-IN(2)   := '02'
                //*  #FUNCT-CDE-IN(3)   := '03'
                //*  #FUNCT-CDE-IN(4)   := '06'
                //*  #FUNCT-CDE-IN(5)   := '07'
                //* *==========================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-POST
                //* *================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-LETTER-DATA
                //* *==========================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-POST
                //* *=========================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OPEN
                //* *===========================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-CLOSE
                //* *==========================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-TX-RTN
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 2 ) TITLE LEFT *PROGRAM 120T *DATU / 'PAGE' *PAGE-NUMBER ( 2 ) 120T *TIMX // 49X #TITLE // 58X 'Exception report' // '-' ( 131 ) //
                //* *==========================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAME-CHECK
                //* *=====================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR
                //* *======
            }                                                                                                                                                             //Natural: ON ERROR
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Load_Signature_Table() throws Exception                                                                                                              //Natural: LOAD-SIGNATURE-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *====================================
        pnd_Branch.getValue(1).setValue("J");                                                                                                                             //Natural: MOVE 'J' TO #BRANCH ( 1 )
        pnd_Branch.getValue(2).setValue("A");                                                                                                                             //Natural: MOVE 'A' TO #BRANCH ( 2 )
        pnd_Branch.getValue(3).setValue("B");                                                                                                                             //Natural: MOVE 'B' TO #BRANCH ( 3 )
        pnd_Branch.getValue(4).setValue("C");                                                                                                                             //Natural: MOVE 'C' TO #BRANCH ( 4 )
        pnd_Branch.getValue(5).setValue("T");                                                                                                                             //Natural: MOVE 'T' TO #BRANCH ( 5 )
        pnd_Branch.getValue(6).setValue("D");                                                                                                                             //Natural: MOVE 'D' TO #BRANCH ( 6 )
        pnd_Branch.getValue(7).setValue("M");                                                                                                                             //Natural: MOVE 'M' TO #BRANCH ( 7 )
        pnd_Branch.getValue(8).setValue("N");                                                                                                                             //Natural: MOVE 'N' TO #BRANCH ( 8 )
        pnd_Branch.getValue(9).setValue("P");                                                                                                                             //Natural: MOVE 'P' TO #BRANCH ( 9 )
        pnd_Branch.getValue(10).setValue("S");                                                                                                                            //Natural: MOVE 'S' TO #BRANCH ( 10 )
        pnd_Branch.getValue(11).setValue("W");                                                                                                                            //Natural: MOVE 'W' TO #BRANCH ( 11 )
        pnd_Signature.getValue(1).setValue("1");                                                                                                                          //Natural: MOVE '1' TO #SIGNATURE ( 1 )
        pnd_Signature.getValue(2).setValue("2");                                                                                                                          //Natural: MOVE '2' TO #SIGNATURE ( 2 )
        pnd_Signature.getValue(3).setValue("1");                                                                                                                          //Natural: MOVE '1' TO #SIGNATURE ( 3 )
        pnd_Signature.getValue(4).setValue("2");                                                                                                                          //Natural: MOVE '2' TO #SIGNATURE ( 4 )
        pnd_Signature.getValue(5).setValue("2");                                                                                                                          //Natural: MOVE '2' TO #SIGNATURE ( 5 )
        pnd_Signature.getValue(6).setValue("2");                                                                                                                          //Natural: MOVE '2' TO #SIGNATURE ( 6 )
        pnd_Signature.getValue(7).setValue("2");                                                                                                                          //Natural: MOVE '2' TO #SIGNATURE ( 7 )
        pnd_Signature.getValue(8).setValue("1");                                                                                                                          //Natural: MOVE '1' TO #SIGNATURE ( 8 )
        pnd_Signature.getValue(9).setValue("3");                                                                                                                          //Natural: MOVE '3' TO #SIGNATURE ( 9 )
        pnd_Signature.getValue(10).setValue("2");                                                                                                                         //Natural: MOVE '2' TO #SIGNATURE ( 10 )
        pnd_Signature.getValue(11).setValue("3");                                                                                                                         //Natural: MOVE '3' TO #SIGNATURE ( 11 )
    }
    private void sub_Assign_Post_Data() throws Exception                                                                                                                  //Natural: ASSIGN-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *================================
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("BATCHACIS");                                                                                                     //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE = 'BATCHACIS'
        pdaPsta9612.getPsta9612_Addrss_Line_1().reset();                                                                                                                  //Natural: RESET PSTA9612.ADDRSS-LINE-1 PSTA9612.ADDRSS-LINE-2 PSTA9612.ADDRSS-LINE-3 PSTA9612.ADDRSS-LINE-4 PSTA9612.ADDRSS-LINE-5 PSTA9612.ADDRSS-LINE-6 PSTA9612.FULL-NME PSTA9612.LTTR-SLTTN-TXT PSTA9612.ADDRSS-PSTL-DTA #FULL-NAME
        pdaPsta9612.getPsta9612_Addrss_Line_2().reset();
        pdaPsta9612.getPsta9612_Addrss_Line_3().reset();
        pdaPsta9612.getPsta9612_Addrss_Line_4().reset();
        pdaPsta9612.getPsta9612_Addrss_Line_5().reset();
        pdaPsta9612.getPsta9612_Addrss_Line_6().reset();
        pdaPsta9612.getPsta9612_Full_Nme().reset();
        pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().reset();
        pdaPsta9612.getPsta9612_Addrss_Pstl_Dta().reset();
        pnd_Full_Name.reset();
        //*  IF CV-COLL-CODE = 'SGRD'           /* SGRD KG     08/23/04 RMW
        //*   MOVE 11122 TO CV-INST-LINK-CDE    /* SGRD KG     08/23/04 RMW
        //*  END-IF                             /* SGRD KG     08/23/04 RMW
        pnd_Univ_Id_Pnd_Univ_Inst.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Inst_Link_Cde());                                                                               //Natural: MOVE CV-INST-LINK-CDE TO #UNIV-INST
        pnd_Univ_Id_Pnd_Univ_Ppg.setValue("0000000");                                                                                                                     //Natural: MOVE '0000000' TO #UNIV-PPG
        pdaPsta9612.getPsta9612_Univ_Id().setValue(pnd_Univ_Id);                                                                                                          //Natural: MOVE #UNIV-ID TO PSTA9612.UNIV-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("I");                                                                                                              //Natural: MOVE 'I' TO PSTA9612.UNIV-ID-TYP
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index().notEquals(getZero())))                                                                                  //Natural: IF CV-ADDR-INDEX NE 0
        {
            if (condition(pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).notEquals(" ") || pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_1().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).notEquals(" "))) //Natural: IF #LAST-NME ( CV-ADDR-INDEX ) NE ' ' OR #ADDRESS-LINE-1 ( CV-ADDR-INDEX ) NE ' '
            {
                pnd_Test_Name.setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_First_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));                                  //Natural: MOVE #FIRST-NME ( CV-ADDR-INDEX ) TO #TEST-NAME
                                                                                                                                                                          //Natural: PERFORM NAME-CHECK
                sub_Name_Check();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_First_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),2,34),                  //Natural: EXAMINE SUBSTR ( #FIRST-NME ( CV-ADDR-INDEX ) ,2,34 ) TRANSLATE INTO LOWER CASE
                    new ExamineTranslate(TranslateOption.Lower));
                if (condition(pnd_Where.notEquals(getZero()) && pnd_Where.lessOrEqual(34)))                                                                               //Natural: IF #WHERE NE 0 AND #WHERE LE 34
                {
                    pnd_Where.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WHERE
                    DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_First_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),pnd_Where.getInt(),1),  //Natural: EXAMINE SUBSTR ( #FIRST-NME ( CV-ADDR-INDEX ) ,#WHERE,1 ) TRANSLATE INTO UPPER CASE
                        new ExamineTranslate(TranslateOption.Upper));
                }                                                                                                                                                         //Natural: END-IF
                pnd_Test_Name.setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Middle_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));                                 //Natural: MOVE #MIDDLE-NME ( CV-ADDR-INDEX ) TO #TEST-NAME
                                                                                                                                                                          //Natural: PERFORM NAME-CHECK
                sub_Name_Check();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_Middle_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),2,34),                 //Natural: EXAMINE SUBSTR ( #MIDDLE-NME ( CV-ADDR-INDEX ) ,2,34 ) TRANSLATE INTO LOWER CASE
                    new ExamineTranslate(TranslateOption.Lower));
                if (condition(pnd_Where.notEquals(getZero()) && pnd_Where.lessOrEqual(34)))                                                                               //Natural: IF #WHERE NE 0 AND #WHERE LE 34
                {
                    pnd_Where.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WHERE
                    DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_Middle_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),pnd_Where.getInt(),1),  //Natural: EXAMINE SUBSTR ( #MIDDLE-NME ( CV-ADDR-INDEX ) ,#WHERE,1 ) TRANSLATE INTO UPPER CASE
                        new ExamineTranslate(TranslateOption.Upper));
                }                                                                                                                                                         //Natural: END-IF
                pnd_Test_Name.setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));                                   //Natural: MOVE #LAST-NME ( CV-ADDR-INDEX ) TO #TEST-NAME
                                                                                                                                                                          //Natural: PERFORM NAME-CHECK
                sub_Name_Check();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),2,34),                   //Natural: EXAMINE SUBSTR ( #LAST-NME ( CV-ADDR-INDEX ) ,2,34 ) TRANSLATE INTO LOWER CASE
                    new ExamineTranslate(TranslateOption.Lower));
                if (condition(pnd_Where.notEquals(getZero()) && pnd_Where.lessOrEqual(34)))                                                                               //Natural: IF #WHERE NE 0 AND #WHERE LE 34
                {
                    pnd_Where.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #WHERE
                    DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),pnd_Where.getInt(),1),  //Natural: EXAMINE SUBSTR ( #LAST-NME ( CV-ADDR-INDEX ) ,#WHERE,1 ) TRANSLATE INTO UPPER CASE
                        new ExamineTranslate(TranslateOption.Upper));
                }                                                                                                                                                         //Natural: END-IF
                pnd_Full_Name.setValue(DbsUtil.compress(pdaIisa8000.getPnd_Ac_Pda_Pnd_Prefix_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),                 //Natural: COMPRESS #PREFIX-NME ( CV-ADDR-INDEX ) #FIRST-NME ( CV-ADDR-INDEX ) #MIDDLE-NME ( CV-ADDR-INDEX ) #LAST-NME ( CV-ADDR-INDEX ) INTO #FULL-NAME
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_First_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()), pdaIisa8000.getPnd_Ac_Pda_Pnd_Middle_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()), 
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index())));
                if (condition(pnd_Full_Name_Pnd_Overflow.equals(" ")))                                                                                                    //Natural: IF #OVERFLOW EQ ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Full_Name_Pnd_F_M_L.setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_First_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).getSubstring(1,      //Natural: MOVE SUBSTR ( #FIRST-NME ( CV-ADDR-INDEX ) ,1,1 ) TO #F-M-L
                        1));
                    setValueToSubstring(".",pnd_Full_Name_Pnd_F_M_L,2,1);                                                                                                 //Natural: MOVE '.' TO SUBSTR ( #F-M-L,2,1 )
                    setValueToSubstring(pdaIisa8000.getPnd_Ac_Pda_Pnd_Middle_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),pnd_Full_Name_Pnd_F_M_L,         //Natural: MOVE #MIDDLE-NME ( CV-ADDR-INDEX ) TO SUBSTR ( #F-M-L,3,1 )
                        3,1);
                    setValueToSubstring(pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()),pnd_Full_Name_Pnd_F_M_L,           //Natural: MOVE #LAST-NME ( CV-ADDR-INDEX ) TO SUBSTR ( #F-M-L,5,30 )
                        5,30);
                    pnd_Full_Name_Pnd_F_M_L.setValue(DbsUtil.compress(pdaIisa8000.getPnd_Ac_Pda_Pnd_Prefix_Nme().getValue(pnd_I), pnd_Full_Name_Pnd_F_M_L));              //Natural: COMPRESS #PREFIX-NME ( #I ) #F-M-L INTO #F-M-L
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9612.getPsta9612_Full_Nme().setValue(pnd_Full_Name_Pnd_F_M_L);                                                                                     //Natural: MOVE #F-M-L TO PSTA9612.FULL-NME PSTA9612.LTTR-SLTTN-TXT
                pdaPsta9612.getPsta9612_Lttr_Slttn_Txt().setValue(pnd_Full_Name_Pnd_F_M_L);
                pnd_Addr.getValue("*").reset();                                                                                                                           //Natural: RESET #ADDR ( * ) #J
                pnd_J.reset();
                if (condition((pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_5().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).notEquals(" ") &&                     //Natural: IF ( #ADDRESS-LINE-5 ( CV-ADDR-INDEX ) NE ' ' AND #ADDRESS-LINE-6 ( CV-ADDR-INDEX ) NE ' ' ) OR ( #ORG-NME ( 1 ) EQ ' ' AND #TITLE-NME ( CV-ADDR-INDEX ) EQ ' ' )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_6().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).notEquals(" ")) || (pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).equals(" ") 
                    && pdaIisa8000.getPnd_Ac_Pda_Pnd_Title_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).equals(" "))))
                {
                    pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_1().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index())); //Natural: MOVE #ADDRESS-LINE-1 ( CV-ADDR-INDEX ) TO PSTA9612.ADDRSS-LINE-1
                    pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_2().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index())); //Natural: MOVE #ADDRESS-LINE-2 ( CV-ADDR-INDEX ) TO PSTA9612.ADDRSS-LINE-2
                    pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_3().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index())); //Natural: MOVE #ADDRESS-LINE-3 ( CV-ADDR-INDEX ) TO PSTA9612.ADDRSS-LINE-3
                    pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_4().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index())); //Natural: MOVE #ADDRESS-LINE-4 ( CV-ADDR-INDEX ) TO PSTA9612.ADDRSS-LINE-4
                    pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_5().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index())); //Natural: MOVE #ADDRESS-LINE-5 ( CV-ADDR-INDEX ) TO PSTA9612.ADDRSS-LINE-5
                    pdaPsta9612.getPsta9612_Addrss_Line_6().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_6().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index())); //Natural: MOVE #ADDRESS-LINE-6 ( CV-ADDR-INDEX ) TO PSTA9612.ADDRSS-LINE-6
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaIisa8000.getPnd_Ac_Pda_Pnd_Title_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).notEquals(" ") && pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_5().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()).equals(" "))) //Natural: IF #TITLE-NME ( CV-ADDR-INDEX ) NE ' ' AND #ADDRESS-LINE-5 ( CV-ADDR-INDEX ) EQ ' '
                    {
                        pnd_J.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #J
                        pnd_Addr.getValue(pnd_J).setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Title_Nme().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));               //Natural: MOVE #TITLE-NME ( CV-ADDR-INDEX ) TO #ADDR ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).notEquals(" ")))                                                                    //Natural: IF #ORG-NME ( 1 ) NE ' '
                    {
                        pnd_J.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #J
                        pnd_Addr.getValue(pnd_J).setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1));                                                           //Natural: MOVE #ORG-NME ( 1 ) TO #ADDR ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    pnd_Addr.getValue(pnd_J).setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_1().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));              //Natural: MOVE #ADDRESS-LINE-1 ( CV-ADDR-INDEX ) TO #ADDR ( #J )
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    pnd_Addr.getValue(pnd_J).setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_2().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));              //Natural: MOVE #ADDRESS-LINE-2 ( CV-ADDR-INDEX ) TO #ADDR ( #J )
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    pnd_Addr.getValue(pnd_J).setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_3().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));              //Natural: MOVE #ADDRESS-LINE-3 ( CV-ADDR-INDEX ) TO #ADDR ( #J )
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    pnd_Addr.getValue(pnd_J).setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_4().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));              //Natural: MOVE #ADDRESS-LINE-4 ( CV-ADDR-INDEX ) TO #ADDR ( #J )
                    if (condition(pnd_J.less(6)))                                                                                                                         //Natural: IF #J LT 6
                    {
                        pnd_J.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #J
                        pnd_Addr.getValue(pnd_J).setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_5().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));          //Natural: MOVE #ADDRESS-LINE-5 ( CV-ADDR-INDEX ) TO #ADDR ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                    pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(pnd_Addr.getValue(1));                                                                               //Natural: MOVE #ADDR ( 1 ) TO PSTA9612.ADDRSS-LINE-1
                    pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(pnd_Addr.getValue(2));                                                                               //Natural: MOVE #ADDR ( 2 ) TO PSTA9612.ADDRSS-LINE-2
                    pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(pnd_Addr.getValue(3));                                                                               //Natural: MOVE #ADDR ( 3 ) TO PSTA9612.ADDRSS-LINE-3
                    pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(pnd_Addr.getValue(4));                                                                               //Natural: MOVE #ADDR ( 4 ) TO PSTA9612.ADDRSS-LINE-4
                    pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(pnd_Addr.getValue(5));                                                                               //Natural: MOVE #ADDR ( 5 ) TO PSTA9612.ADDRSS-LINE-5
                    pdaPsta9612.getPsta9612_Addrss_Line_6().setValue(pnd_Addr.getValue(6));                                                                               //Natural: MOVE #ADDR ( 6 ) TO PSTA9612.ADDRSS-LINE-6
                }                                                                                                                                                         //Natural: END-IF
                pdaPsta9612.getPsta9612_Addrss_Pstl_Dta().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Postal_Cde().getValue(ldaAppl302.getCv_Prap_Rule1_Cv_Addr_Index()));     //Natural: MOVE #POSTAL-CDE ( CV-ADDR-INDEX ) TO PSTA9612.ADDRSS-PSTL-DTA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTPRAPNY");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE = 'PTPRAPNY'
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Mail_Ind().equals("N")))                                                                                             //Natural: IF CV-MAIL-IND EQ 'N'
        {
            pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("RTU");                                                                                                    //Natural: MOVE 'RTU' TO PSTA9612.PCKGE-DLVRY-TYP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().reset();                                                                                                            //Natural: RESET PSTA9612.PCKGE-DLVRY-TYP
        }                                                                                                                                                                 //Natural: END-IF
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        DbsUtil.examine(new ExamineSource(pnd_Branch.getValue("*")), new ExamineSearch(ldaAppl302.getCv_Prap_Rule1_Cv_Branch()), new ExamineGivingIndex(pnd_J));          //Natural: EXAMINE #BRANCH ( * ) FOR CV-BRANCH GIVING INDEX #J
        if (condition(pnd_J.notEquals(getZero())))                                                                                                                        //Natural: IF #J NE 0
        {
            short decideConditionsMet1255 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #SIGNATURE ( #J );//Natural: VALUE '1'
            if (condition((pnd_Signature.getValue(pnd_J).equals("1"))))
            {
                decideConditionsMet1255++;
                pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("Jeff Wirth");                                                                                         //Natural: MOVE 'Jeff Wirth' TO PSTA9612.EMPL-SGNTRY-NME
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Signature.getValue(pnd_J).equals("2"))))
            {
                decideConditionsMet1255++;
                pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("Jeff Wirth");                                                                                         //Natural: MOVE 'Jeff Wirth' TO PSTA9612.EMPL-SGNTRY-NME
                if (condition(pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().equals("RTU")))                                                                                   //Natural: IF PSTA9612.PCKGE-DLVRY-TYP EQ 'RTU'
                {
                    pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTPRAPDN");                                                                                             //Natural: ASSIGN PSTA9612.PCKGE-CDE = 'PTPRAPDN'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pnd_Signature.getValue(pnd_J).equals("3"))))
            {
                decideConditionsMet1255++;
                pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("Jeff Wirth");                                                                                         //Natural: MOVE 'Jeff Wirth' TO PSTA9612.EMPL-SGNTRY-NME
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().reset();                                                                                                        //Natural: RESET PSTA9612.EMPL-SGNTRY-NME
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet1269 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CV-REGION;//Natural: VALUE '2'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Region().equals("2"))))
            {
                decideConditionsMet1269++;
                pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("Jeff Wirth");                                                                                         //Natural: MOVE 'Jeff Wirth' TO PSTA9612.EMPL-SGNTRY-NME
                if (condition(pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().equals("RTU")))                                                                                   //Natural: IF PSTA9612.PCKGE-DLVRY-TYP EQ 'RTU'
                {
                    pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTPRAPDN");                                                                                             //Natural: ASSIGN PSTA9612.PCKGE-CDE = 'PTPRAPDN'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("Jeff Wirth");                                                                                         //Natural: MOVE 'Jeff Wirth' TO PSTA9612.EMPL-SGNTRY-NME
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE 'Institutional Services' TO PSTA9612.EMPL-UNIT-WORK-NME
        pdaPsta9612.getPsta9612_Empl_Unit_Work_Nme().setValue("Director I & I Operations");                                                                               //Natural: MOVE 'Director I & I Operations' TO PSTA9612.EMPL-UNIT-WORK-NME
        //*  BJD SIG END
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Log().notEquals(" ")))                                                                                          //Natural: IF CV-IMIT-LOG NE ' '
        {
            pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Log());                                                   //Natural: MOVE CV-IMIT-LOG TO PSTA9612.RQST-LOG-DTE-TME ( 1 )
            pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Wpid());                                                     //Natural: MOVE CV-IMIT-WPID TO PSTA9612.WORK-PRCSS-ID ( 1 )
            pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue("8043");                                                                                            //Natural: MOVE '8043' TO PSTA9612.STATUS-CDE ( 1 )
            pdaPsta9612.getPsta9612_Rqst_Origin_Unit_Cde().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Unit());                                                          //Natural: MOVE CV-IMIT-UNIT TO PSTA9612.RQST-ORIGIN-UNIT-CDE PSTA9612.UNIT-CDE ( 1 )
            pdaPsta9612.getPsta9612_Unit_Cde().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Unit());
            pdaPsta9612.getPsta9612_Rqst_Log_Oprtr_Cde().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Oprtr());                                                           //Natural: MOVE CV-IMIT-OPRTR TO PSTA9612.RQST-LOG-OPRTR-CDE PSTA9612.EMPL-RACF-ID ( 1 )
            pdaPsta9612.getPsta9612_Empl_Racf_Id().getValue(1).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Imit_Oprtr());
            pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(false);                                                                                                //Natural: MOVE FALSE TO PSTA9612.NO-UPDTE-TO-MIT-IND
            pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(false);                                                                                                //Natural: MOVE FALSE TO PSTA9612.NO-UPDTE-TO-EFM-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).reset();                                                                                               //Natural: RESET PSTA9612.RQST-LOG-DTE-TME ( 1 ) PSTA9612.WORK-PRCSS-ID ( 1 ) PSTA9612.STATUS-CDE ( 1 ) PSTA9612.RQST-ORIGIN-UNIT-CDE PSTA9612.UNIT-CDE ( 1 ) PSTA9612.RQST-LOG-OPRTR-CDE PSTA9612.EMPL-RACF-ID ( 1 )
            pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(1).reset();
            pdaPsta9612.getPsta9612_Status_Cde().getValue(1).reset();
            pdaPsta9612.getPsta9612_Rqst_Origin_Unit_Cde().reset();
            pdaPsta9612.getPsta9612_Unit_Cde().getValue(1).reset();
            pdaPsta9612.getPsta9612_Rqst_Log_Oprtr_Cde().reset();
            pdaPsta9612.getPsta9612_Empl_Racf_Id().getValue(1).reset();
            pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                 //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-MIT-IND := TRUE
            pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                 //Natural: ASSIGN PSTA9612.NO-UPDTE-TO-EFM-IND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Inst_Addr() throws Exception                                                                                                                     //Natural: GET-INST-ADDR
    {
        if (BLNatReinput.isReinput()) return;

        //* *=============================
        pdaIisa8000.getPnd_Ac_Pda().reset();                                                                                                                              //Natural: RESET #AC-PDA CON-PART-REC
        pdaScia8200.getCon_Part_Rec().reset();
        //* * SGRD KG - EXCLUDED SUNGARD CONTRACTS FROM IIS CALL
        //* *         - HARDCODED CONTACT INFO
        //*  08/23/04 >>
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals("SGRD") || pnd_Sgrd.getBoolean()))                                                                //Natural: IF CV-COLL-CODE = 'SGRD' OR #SGRD
        {
            pnd_Sgrd_Call_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #SGRD-CALL-CNT
            //*  SET FILE OPEN SWITCH ON FIRST PASS
            //*  ----------------------------------
            //*  SGRD5 KG
            if (condition(pnd_Sgrd_Call_Cnt.equals(1)))                                                                                                                   //Natural: IF #SGRD-CALL-CNT = 1
            {
                pdaScia8200.getCon_File_Open_Sw().setValue("N");                                                                                                          //Natural: ASSIGN CON-FILE-OPEN-SW := 'N'
                //*  SGRD5 KG
                //*  DCA KG
            }                                                                                                                                                             //Natural: END-IF
            pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("N");                                                                                                       //Natural: ASSIGN CON-EOF-SW := 'N'
            pdaScia8200.getCon_Part_Rec_Con_Function_Code().getValue(1).setValue("08");                                                                                   //Natural: ASSIGN CON-FUNCTION-CODE ( 1 ) := '08'
            pdaScia8200.getCon_Part_Rec_Con_Access_Level().setValue(" ");                                                                                                 //Natural: ASSIGN CON-ACCESS-LEVEL := ' '
            pdaScia8200.getCon_Part_Rec_Con_Contact_Type().setValue("E");                                                                                                 //Natural: ASSIGN CON-CONTACT-TYPE := 'E'
            pdaScia8200.getCon_Part_Rec_Con_Plan_Num().setValue(pnd_Ab_Plan);                                                                                             //Natural: ASSIGN CON-PLAN-NUM := #AB-PLAN
            pdaScia8200.getCon_Part_Rec_Con_Part_Divsub().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub());                                                         //Natural: ASSIGN CON-PART-DIVSUB := CV-SGRD-DIVSUB
            //*  SGRD5 KG
            DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                 //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
            if (condition(Global.isEscape())) return;
            if (condition(pdaScia8200.getCon_Part_Rec_Con_Return_Code().notEquals(getZero()) || pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg().notEquals(" ")))        //Natural: IF CON-RETURN-CODE NE 0 OR CON-CONTACT-ERR-MSG NE ' '
            {
                getReports().write(0, "*CALL ERROR (PSG9061):",pdaScia8200.getCon_Part_Rec_Con_Return_Code(),pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg());          //Natural: WRITE '*CALL ERROR (PSG9061):' CON-RETURN-CODE CON-CONTACT-ERR-MSG
                if (Global.isEscape()) return;
                getReports().write(0, "PLAN:",pdaScia8200.getCon_Part_Rec_Con_Plan_Num());                                                                                //Natural: WRITE 'PLAN:' CON-PLAN-NUM
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  CREATE NAME VALUES
                //*  ------------------
                pdaScia8200.getCon_Part_Rec_Con_Contact_Name().separate(SeparateOption.WithAnyDelimiters, ",", pnd_Work_Pnd_Wk_L_Name, pnd_Work_Pnd_Wk_F_Name);           //Natural: SEPARATE CON-CONTACT-NAME INTO #WK-L-NAME #WK-F-NAME WITH DELIMITERS ','
                pnd_Work_Pnd_Wk_L_Name.setValue(pnd_Work_Pnd_Wk_L_Name, MoveOption.LeftJustified);                                                                        //Natural: MOVE LEFT JUSTIFIED #WK-L-NAME TO #WK-L-NAME
                pnd_Work_Pnd_Wk_F_Name.setValue(pnd_Work_Pnd_Wk_F_Name, MoveOption.LeftJustified);                                                                        //Natural: MOVE LEFT JUSTIFIED #WK-F-NAME TO #WK-F-NAME
                pnd_Work_Pnd_Wk_Compressed_Nme.setValue(DbsUtil.compress(pnd_Work_Pnd_Wk_F_Name, pnd_Work_Pnd_Wk_L_Name));                                                //Natural: COMPRESS #WK-F-NAME #WK-L-NAME INTO #WK-COMPRESSED-NME
                //*  CREATE ADDRESS VALUES
                //*  ---------------------
                pnd_Work_Pnd_Wk_Zip.setValue(pdaScia8200.getCon_Part_Rec_Con_Zip());                                                                                      //Natural: MOVE CON-ZIP TO #WK-ZIP
                if (condition(pnd_Work_Pnd_Wk_Zip4.equals(" ")))                                                                                                          //Natural: IF #WK-ZIP4 = ' '
                {
                    pnd_Work_Pnd_Wk_Zip4.setValue("0000");                                                                                                                //Natural: MOVE '0000' TO #WK-ZIP4
                }                                                                                                                                                         //Natural: END-IF
                pnd_Work_Pnd_Wk_Zip9.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Pnd_Wk_Zip5, "-", pnd_Work_Pnd_Wk_Zip4));                          //Natural: COMPRESS #WK-ZIP5 '-' #WK-ZIP4 INTO #WK-ZIP9 LEAVING NO
                //*  PLACE COMMA AFTER CITY
                //*  ----------------------
                pnd_Work_Pnd_Wk_City.setValue(pdaScia8200.getCon_Part_Rec_Con_City());                                                                                    //Natural: MOVE CON-CITY TO #WK-CITY
                FOR03:                                                                                                                                                    //Natural: FOR #WK-CNTR 28 TO 1 STEP -1
                for (pnd_Work_Pnd_Wk_Cntr.setValue(28); condition(pnd_Work_Pnd_Wk_Cntr.greaterOrEqual(1)); pnd_Work_Pnd_Wk_Cntr.nsubtract(1))
                {
                    if (condition(!pnd_Work_Pnd_Wk_City.getSubstring(pnd_Work_Pnd_Wk_Cntr.getInt()).equals(" ")))                                                         //Natural: IF SUBSTRING ( #WK-CITY,#WK-CNTR ) NE ' '
                    {
                        pnd_Work_Pnd_Wk_Cntr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WK-CNTR
                        setValueToSubstring(",",pnd_Work_Pnd_Wk_City,pnd_Work_Pnd_Wk_Cntr.getInt());                                                                      //Natural: MOVE ',' TO SUBSTRING ( #WK-CITY,#WK-CNTR )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  BUILD THE CITY, STATE ZIP9
                //*  --------------------------
                pnd_Work_Pnd_Wk_City_State_Zip9.setValue(DbsUtil.compress(pnd_Work_Pnd_Wk_City, pdaScia8200.getCon_Part_Rec_Con_State(), pnd_Work_Pnd_Wk_Zip9));          //Natural: COMPRESS #WK-CITY CON-STATE #WK-ZIP9 INTO #WK-CITY-STATE-ZIP9
                //*  WHICH ADDRESS LINES THAT WERE RETURNED ARE POPULATED?
                //*  -----------------------------------------------------
                short decideConditionsMet1366 = 0;                                                                                                                        //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CON-ADDRESS-LINE2 > ' '
                if (condition(pdaScia8200.getCon_Part_Rec_Con_Address_Line2().greater(" ")))
                {
                    decideConditionsMet1366++;
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_2().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line2());                                 //Natural: MOVE CON-ADDRESS-LINE2 TO #ADDRESS-LINE-2 ( 1 )
                    pnd_Work_Pnd_Addr2_Fnd.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #ADDR2-FND
                }                                                                                                                                                         //Natural: WHEN CON-ADDRESS-LINE3 > ' '
                if (condition(pdaScia8200.getCon_Part_Rec_Con_Address_Line3().greater(" ")))
                {
                    decideConditionsMet1366++;
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_3().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line3());                                 //Natural: MOVE CON-ADDRESS-LINE3 TO #ADDRESS-LINE-3 ( 1 )
                    pnd_Work_Pnd_Addr3_Fnd.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #ADDR3-FND
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet1366 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  DETERMINE WHERE TO PLACE ADDRESS LINES
                //*  --------------------------------------
                short decideConditionsMet1379 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #ADDR2-FND AND NOT #ADDR3-FND
                if (condition(! ((pnd_Work_Pnd_Addr2_Fnd.getBoolean()) && ! (pnd_Work_Pnd_Addr3_Fnd.getBoolean()))))
                {
                    decideConditionsMet1379++;
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_2().getValue(1).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO #ADDRESS-LINE-2 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_3().getValue(1).setValue(" ");                                                                             //Natural: MOVE ' ' TO #ADDRESS-LINE-3 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_4().getValue(1).setValue(" ");                                                                             //Natural: MOVE ' ' TO #ADDRESS-LINE-4 ( 1 )
                }                                                                                                                                                         //Natural: WHEN #ADDR2-FND AND NOT #ADDR3-FND
                else if (condition(pnd_Work_Pnd_Addr2_Fnd.getBoolean() && ! (pnd_Work_Pnd_Addr3_Fnd.getBoolean())))
                {
                    decideConditionsMet1379++;
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_3().getValue(1).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO #ADDRESS-LINE-3 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_2().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line2());                                 //Natural: MOVE CON-ADDRESS-LINE2 TO #ADDRESS-LINE-2 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_4().getValue(1).setValue(" ");                                                                             //Natural: MOVE ' ' TO #ADDRESS-LINE-4 ( 1 )
                }                                                                                                                                                         //Natural: WHEN #ADDR2-FND AND #ADDR3-FND
                else if (condition(pnd_Work_Pnd_Addr2_Fnd.getBoolean() && pnd_Work_Pnd_Addr3_Fnd.getBoolean()))
                {
                    decideConditionsMet1379++;
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_4().getValue(1).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO #ADDRESS-LINE-4 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_2().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line2());                                 //Natural: MOVE CON-ADDRESS-LINE2 TO #ADDRESS-LINE-2 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_3().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line3());                                 //Natural: MOVE CON-ADDRESS-LINE3 TO #ADDRESS-LINE-3 ( 1 )
                }                                                                                                                                                         //Natural: WHEN NOT #ADDR2-FND AND #ADDR3-FND
                else if (condition(! ((pnd_Work_Pnd_Addr2_Fnd.getBoolean()) && pnd_Work_Pnd_Addr3_Fnd.getBoolean())))
                {
                    decideConditionsMet1379++;
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_2().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line3());                                 //Natural: MOVE CON-ADDRESS-LINE3 TO #ADDRESS-LINE-2 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_3().getValue(1).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO #ADDRESS-LINE-3 ( 1 )
                    pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_4().getValue(1).setValue(" ");                                                                             //Natural: MOVE ' ' TO #ADDRESS-LINE-4 ( 1 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    getReports().write(0, "COULD NOT DETERMINE WHERE TO PUT ADDRESS LINE",pnd_Ab_Plan);                                                                   //Natural: WRITE 'COULD NOT DETERMINE WHERE TO PUT ADDRESS LINE' #AB-PLAN
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-DECIDE
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_1().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line1());                                     //Natural: MOVE CON-ADDRESS-LINE1 TO #ADDRESS-LINE-1 ( 1 )
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_5().getValue(1).setValue(" ");                                                                                 //Natural: MOVE ' ' TO #ADDRESS-LINE-5 ( 1 )
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_6().getValue(1).setValue(" ");                                                                                 //Natural: MOVE ' ' TO #ADDRESS-LINE-6 ( 1 )
                pdaIisa8000.getPnd_Ac_Pda_Pnd_First_Nme().getValue(1).setValue(pnd_Work_Pnd_Wk_F_Name);                                                                   //Natural: ASSIGN #FIRST-NME ( 1 ) := #WK-F-NAME
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Last_Nme().getValue(1).setValue(pnd_Work_Pnd_Wk_L_Name);                                                                    //Natural: ASSIGN #LAST-NME ( 1 ) := #WK-L-NAME
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Compressed_Nme().getValue(1).setValue(pnd_Work_Pnd_Wk_Compressed_Nme);                                                      //Natural: ASSIGN #COMPRESSED-NME ( 1 ) := #WK-COMPRESSED-NME
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Prefix_Nme().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Title());                                                 //Natural: ASSIGN #PREFIX-NME ( 1 ) := CON-TITLE
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Title_Nme().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Job_Title());                                              //Natural: ASSIGN #TITLE-NME ( 1 ) := CON-JOB-TITLE
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Company_Name());                                             //Natural: ASSIGN #ORG-NME ( 1 ) := CON-COMPANY-NAME
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Line_1().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line1());                                     //Natural: ASSIGN #ADDRESS-LINE-1 ( 1 ) := CON-ADDRESS-LINE1
                pdaIisa8000.getPnd_Ac_Pda_Pnd_Postal_Cde().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Zip());                                                   //Natural: ASSIGN #POSTAL-CDE ( 1 ) := CON-ZIP
                //*    WRITE 'SGRD ACCESS' #REC-READ CV-COLL-CODE #AB-PLAN
                //*    WRITE
                //*      '='   #FIRST-NME(1)       /
                //*      '='   #LAST-NME(1)        /
                //*      '='   #COMPRESSED-NME(1)  /
                //*      '='   #PREFIX-NME(1)      /
                //*      '='   #TITLE-NME(1)       /
                //*      '='   #ORG-NME(1)         /
                //*      '='   #ADDRESS-LINE-1(1)  /
                //*      '='   #ADDRESS-LINE-2(1)  /
                //*      '='   #ADDRESS-LINE-3(1)  /
                //*      '='   #ADDRESS-LINE-4(1)  /
                //*      '='   #ADDRESS-LINE-5(1)  /
                //*      '='   #ADDRESS-LINE-6(1)  /
                //*      '='   #POSTAL-CDE(1)      /
            }                                                                                                                                                             //Natural: END-IF
            pdaScia8200.getCon_Part_Rec().reset();                                                                                                                        //Natural: RESET CON-PART-REC #WORK #SGRD
            pnd_Work.reset();
            pnd_Sgrd.reset();
            //*  IF CV-COLL-CODE = 'SGRD'                                 /* SGRD KG
            //*   #FIRST-NME(1)            := 'ROBERTO '                  /* SGRD KG
            //*   #LAST-NME(1)             := 'TORRES '                   /* SGRD KG
            //*   #COMPRESSED-NME(1)       := 'ROBERTO TORRES'            /* SGRD KG
            //*   #PREFIX-NME(1)           := 'MR.'                       /* SGRD KG
            //*   #TITLE-NME(1)            := 'BENEFITS SPECIALIST'       /* SGRD KG
            //*   #ORG-NME(1)              := 'TIAA-CREF'                 /* SGRD KG
            //*   #ADDRESS-LINE-1(1)       := '730 3RD AVE'               /* SGRD KG
            //*   #ADDRESS-LINE-2(1)       := 'NEW YORK, NY 10017-3206'   /* SGRD KG
            //*   #POSTAL-CDE(1)           := '10017'                     /* SGRD KG
            //*   #ADDRESS-LINE-3(1)       := ' '                         /* SGRD KG
            //*   #ADDRESS-LINE-4(1)       := ' '                         /* SGRD KG
            //*   #ADDRESS-LINE-5(1)       := ' '                         /* SGRD KG
            //*   #ADDRESS-LINE-6(1)       := ' '                         /* SGRD KG
            //*                                                          /* 08/23/04 >>
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Ppg_Cde_In().setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code());                                                              //Natural: ASSIGN #PPG-CDE-IN := CV-COLL-CODE
            DbsUtil.examine(new ExamineSource(pdaIisa8000.getPnd_Ac_Pda_Pnd_Ppg_Cde_In()), new ExamineTranslate(TranslateOption.Upper));                                  //Natural: EXAMINE #PPG-CDE-IN TRANSLATE INTO UPPER CASE
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Address_Type_In().setValue("01");                                                                                               //Natural: ASSIGN #ADDRESS-TYPE-IN := '01'
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Contact_Level().setValue(" ");                                                                                                  //Natural: ASSIGN #CONTACT-LEVEL := ' '
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Funct_Cde_In().getValue(1).setValue("01");                                                                                      //Natural: ASSIGN #FUNCT-CDE-IN ( 1 ) := '01'
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Ext_Contact_Count().setValue(1);                                                                                                //Natural: ASSIGN #EXT-CONTACT-COUNT := 1
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Role_Cde_In().setValue(" ");                                                                                                    //Natural: ASSIGN #ROLE-CDE-IN := ' '
            pdaIisa8000.getPnd_Ac_Pda_Pnd_Int_Contact_Count().setValue(0);                                                                                                //Natural: ASSIGN #INT-CONTACT-COUNT := 0
            DbsUtil.callnat(Iisn8000.class , getCurrentProcessState(), pdaIisa8000.getPnd_Ac_Pda());                                                                      //Natural: CALLNAT 'IISN8000' #AC-PDA
            if (condition(Global.isEscape())) return;
            //*  SGRD KG
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Post() throws Exception                                                                                                                        //Natural: WRITE-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* *==========================
        //*  CHANGING THE MAX OCCURRENCE FOR THIS ARRAY
        //*  TO MAKE ROOM FOR THE EXPANDED PPG REQUIRED
        //*  FOR RELEASE 3
        //*  ------------------------------------------
        //*  IF #I + 1 GT 63                           /* 08/23/04
        //*  IF #I + 1 GT 62                             /* 08/23/04 /* SUNY CCR KG
        //*  SUNY CCR KG - POST CHG
        if (condition(pnd_I.add(1).greater(56)))                                                                                                                          //Natural: IF #I + 1 GT 56
        {
                                                                                                                                                                          //Natural: PERFORM STORE-POST
            sub_Store_Post();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM LOAD-LETTER-DATA
            sub_Load_Letter_Data();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Report_Detail().getValue("*").reset();                                                                               //Natural: RESET PRAP-LETTER-REPORT-DETAIL ( * )
            pnd_I.reset();                                                                                                                                                //Natural: RESET #I
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #I
        if (condition(pnd_Indicators_Pnd_Newpage_Ind.equals("Y")))                                                                                                        //Natural: IF #NEWPAGE-IND EQ 'Y'
        {
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Newpage_Ind().getValue(pnd_I).setValue("Y");                                                                  //Natural: MOVE 'Y' TO PRAP-LETTER-DETAIL-NEWPAGE-IND ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Newpage_Ind().getValue(pnd_I).reset();                                                                        //Natural: RESET PRAP-LETTER-DETAIL-NEWPAGE-IND ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Indicators_Pnd_Newpage_Ind.reset();                                                                                                                           //Natural: RESET #NEWPAGE-IND
        //*  KG 8/01/01 START
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff().greater(91)))                                                                                             //Natural: IF CV-DAY-DIFF > 91
        {
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Daysafter().getValue(pnd_I).setValue(91);                                                                     //Natural: MOVE 91 TO PRAP-LETTER-DETAIL-DAYSAFTER ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Daysafter().getValue(pnd_I).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff());                              //Natural: MOVE CV-DAY-DIFF TO PRAP-LETTER-DETAIL-DAYSAFTER ( #I )
            //*  KG 8/01/01 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUNYCCR
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No().equals("151169") || ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No().equals("151166")                    //Natural: IF CV-SGRD-PLAN-NO EQ '151169' OR EQ '151166' OR EQ '151212'
            || ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No().equals("151212")))
        {
            //*  SUNY CCR KG
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Daysafter().getValue(pnd_I).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Day_Diff());                              //Natural: MOVE CV-DAY-DIFF TO PRAP-LETTER-DETAIL-DAYSAFTER ( #I )
            //*  SUNY CCR KG
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Ppg().getValue(pnd_I).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code());                                       //Natural: MOVE CV-COLL-CODE TO PRAP-LETTER-DETAIL-PPG ( #I )
        //*  SUNY CCR KG
        ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Div_Sub().getValue(pnd_I).setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub());                                 //Natural: MOVE CV-SGRD-DIVSUB TO PRAP-LETTER-DETAIL-DIV-SUB ( #I )
        short decideConditionsMet1509 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF CV-LOB;//Natural: VALUE 'D'
        if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob().equals("D"))))
        {
            decideConditionsMet1509++;
            //*  RW1
            //*  RW2
            //*  NBC
            short decideConditionsMet1514 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CV-LOB-TYPE;//Natural: VALUE '2'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("2"))))
            {
                decideConditionsMet1514++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("RA");                                                                //Natural: MOVE 'RA' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("5"))))
            {
                decideConditionsMet1514++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("RS");                                                                //Natural: MOVE 'RS' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("7"))))
            {
                decideConditionsMet1514++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("GRA");                                                               //Natural: MOVE 'GRA' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE 'A'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("A"))))
            {
                decideConditionsMet1514++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("RC");                                                                //Natural: MOVE 'RC' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("B"))))
            {
                decideConditionsMet1514++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("RHSP");                                                              //Natural: MOVE 'RHSP' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).reset();                                                                       //Natural: RESET PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob().equals("I"))))
        {
            decideConditionsMet1509++;
            //*  RL6
            //*  TIGR
            //*  TIGR
            //*  TIGR
            short decideConditionsMet1533 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CV-LOB-TYPE;//Natural: VALUE '3'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("3"))))
            {
                decideConditionsMet1533++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("IRAR");                                                              //Natural: MOVE 'IRAR' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("4"))))
            {
                decideConditionsMet1533++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("IRAC");                                                              //Natural: MOVE 'IRAC' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("5"))))
            {
                decideConditionsMet1533++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("KEOG");                                                              //Natural: MOVE 'KEOG' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("6"))))
            {
                decideConditionsMet1533++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("IRAS");                                                              //Natural: MOVE 'IRAS' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("7"))))
            {
                decideConditionsMet1533++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("IRIR");                                                              //Natural: MOVE 'IRIR' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '8'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("8"))))
            {
                decideConditionsMet1533++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("IRIC");                                                              //Natural: MOVE 'IRIC' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '9'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("9"))))
            {
                decideConditionsMet1533++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("IRIS");                                                              //Natural: MOVE 'IRIS' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).reset();                                                                       //Natural: RESET PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob().equals("S"))))
        {
            decideConditionsMet1509++;
            //*  RW1
            //*  RW1
            //*  RW2
            //*  457(B)
            short decideConditionsMet1556 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CV-LOB-TYPE;//Natural: VALUE '2'
            if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("2"))))
            {
                decideConditionsMet1556++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("SRA");                                                               //Natural: MOVE 'SRA' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("3"))))
            {
                decideConditionsMet1556++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("GSRA");                                                              //Natural: MOVE 'GSRA' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("5"))))
            {
                decideConditionsMet1556++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("RSP");                                                               //Natural: MOVE 'RSP' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("6"))))
            {
                decideConditionsMet1556++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("RSP2");                                                              //Natural: MOVE 'RSP2' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("7"))))
            {
                decideConditionsMet1556++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("RCP");                                                               //Natural: MOVE 'RCP' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Lob_Type().equals("4"))))
            {
                decideConditionsMet1556++;
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).setValue("GA");                                                                //Natural: MOVE 'GA' TO PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).reset();                                                                       //Natural: RESET PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrtype().getValue(pnd_I).reset();                                                                           //Natural: RESET PRAP-LETTER-DETAIL-CNTRTYPE ( #I )
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  EAC (RODGER)
        if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Cont().notEquals(" ")))                                                                                              //Natural: IF CV-CONT NE ' '
        {
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_Pref(),ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrnbr().getValue(pnd_I),1,1);                     //Natural: MOVE CV-PREF TO SUBSTR ( PRAP-LETTER-DETAIL-CNTRNBR ( #I ) ,1,1 )
            //*  EAC (RODGER)
            //*  EAC (RODGER EM CHANGED FROM 999999-9 TO XXXXXX-X)
            pnd_Contract.setValueEdited(ldaAppl302.getCv_Prap_Rule1_Cv_Cont(),new ReportEditMask("XXXXXX-X"));                                                            //Natural: MOVE EDITED CV-CONT ( EM = XXXXXX-X ) TO #CONTRACT
            setValueToSubstring(pnd_Contract,ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrnbr().getValue(pnd_I),2,8);                                              //Natural: MOVE #CONTRACT TO SUBSTR ( PRAP-LETTER-DETAIL-CNTRNBR ( #I ) ,2,8 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_C_Pref(),ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrnbr().getValue(pnd_I),1,1);                   //Natural: MOVE CV-C-PREF TO SUBSTR ( PRAP-LETTER-DETAIL-CNTRNBR ( #I ) ,1,1 )
            //*  EAC (RODGER EM CHANGED FROM 999999-9 TO XXXXXX-X)
            pnd_Contract.setValueEdited(ldaAppl302.getCv_Prap_Rule1_Cv_C_Cont(),new ReportEditMask("XXXXXX-X"));                                                          //Natural: MOVE EDITED CV-C-CONT ( EM = XXXXXX-X ) TO #CONTRACT
            setValueToSubstring(pnd_Contract,ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Cntrnbr().getValue(pnd_I),2,8);                                              //Natural: MOVE #CONTRACT TO SUBSTR ( PRAP-LETTER-DETAIL-CNTRNBR ( #I ) ,2,8 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Full_Name.setValue(DbsUtil.compress(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_First_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Mddle_Nme(), ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme())); //Natural: COMPRESS CV-COR-FIRST-NME CV-COR-MDDLE-NME CV-COR-LAST-NME INTO #FULL-NAME
        if (condition(pnd_Full_Name_Pnd_Overflow.equals(" ")))                                                                                                            //Natural: IF #OVERFLOW EQ ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Full_Name_Pnd_F_M_L.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_First_Nme().getSubstring(1,1));                                                           //Natural: MOVE SUBSTR ( CV-COR-FIRST-NME,1,1 ) TO #F-M-L
            setValueToSubstring(".",pnd_Full_Name_Pnd_F_M_L,2,1);                                                                                                         //Natural: MOVE '.' TO SUBSTR ( #F-M-L,2,1 )
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Mddle_Nme(),pnd_Full_Name_Pnd_F_M_L,3,1);                                                              //Natural: MOVE CV-COR-MDDLE-NME TO SUBSTR ( #F-M-L,3,1 )
            setValueToSubstring(ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(),pnd_Full_Name_Pnd_F_M_L,5,30);                                                              //Natural: MOVE CV-COR-LAST-NME TO SUBSTR ( #F-M-L,5,30 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Empl_Name().getValue(pnd_I).setValue(pnd_Full_Name_Pnd_F_M_L);                                                    //Natural: MOVE #F-M-L TO PRAP-LETTER-DETAIL-EMPL-NAME ( #I )
        //* *MOVE EDITED CV-SOC-SEC (EM=999-99-9999) TO                     /* JRB1
        //* *  PRAP-LETTER-DETAIL-SOCSEC-NBR (#I)                           /* JRB1
        //*  JRB1
        ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Socsec_Nbr().getValue(pnd_I).setValue("ON FILE");                                                                 //Natural: MOVE 'ON FILE' TO PRAP-LETTER-DETAIL-SOCSEC-NBR ( #I )
        getReports().write(0, "=",ldaPstl6335.getPstl6335_Data_Prap_Letter_Detail_Socsec_Nbr().getValue(pnd_I));                                                          //Natural: WRITE '=' PRAP-LETTER-DETAIL-SOCSEC-NBR ( #I )
        if (Global.isEscape()) return;
    }
    private void sub_Load_Letter_Data() throws Exception                                                                                                                  //Natural: LOAD-LETTER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *================================
        short decideConditionsMet1611 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE CV-RECORD-TYPE;//Natural: VALUE 1
        if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(1))))
        {
            decideConditionsMet1611++;
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Type_Ind().setValue("A");                                                                                            //Natural: MOVE 'A' TO PRAP-LETTER-TYPE-IND
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(2))))
        {
            decideConditionsMet1611++;
            ldaPstl6335.getPstl6335_Data_Prap_Letter_Type_Ind().setValue("P");                                                                                            //Natural: MOVE 'P' TO PRAP-LETTER-TYPE-IND
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaPstl6335.getPstl6335_Data_Prap_Letter_End_Date().setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                            //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO PRAP-LETTER-END-DATE
        ldaPstl6335.getPstl6335_Data_Prap_Letter_Cutoff_Date().setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                         //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO PRAP-LETTER-CUTOFF-DATE
        //*  IF CV-SGRD-DIVSUB NE ' '                    /* DCA KG /* SUNY CCR KG
        //*    COMPRESS #ORG-NME (1) '-' CV-SGRD-DIVSUB  /* DCA KG /* SUNY CCR KG
        //*      INTO PRAP-LETTER-INST-NAME              /* DCA KG /* SUNY CCR KG
        //*  ELSE                                        /* DCA KG /* SUNY CCR KG
        //*  DCA KG
        ldaPstl6335.getPstl6335_Data_Prap_Letter_Inst_Name().setValue(pdaIisa8000.getPnd_Ac_Pda_Pnd_Org_Nme().getValue(1));                                               //Natural: MOVE #ORG-NME ( 1 ) TO PRAP-LETTER-INST-NAME
        //*  END-IF                                      /* DCA KG /* SUNY CCR KG
    }
    private void sub_Store_Post() throws Exception                                                                                                                        //Natural: STORE-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* *==========================
        ldaPstl6335.getPstl6335_Data_Prap_Letter_Report_Count().setValue(pnd_I);                                                                                          //Natural: MOVE #I TO PRAP-LETTER-REPORT-COUNT
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        //*   <===  SORT KEY
        //*   <===  NAME OF RECORD TO BE WRITTEN
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL6335-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6335.getSort_Key(), ldaPstl6335.getPstl6335_Data());
        if (condition(Global.isEscape())) return;
        pnd_Indicators_Pnd_Store_Post.setValue("Y");                                                                                                                      //Natural: MOVE 'Y' TO #STORE-POST
    }
    private void sub_Post_Open() throws Exception                                                                                                                         //Natural: POST-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* *=========================
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Indicators_Pnd_Post_Open.setValue("Y");                                                                                                                       //Natural: MOVE 'Y' TO #POST-OPEN
    }
    private void sub_Post_Close() throws Exception                                                                                                                        //Natural: POST-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* *===========================
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Control_Counters_Pnd_Et_Ctr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #ET-CTR
                                                                                                                                                                          //Natural: PERFORM END-TX-RTN
        sub_End_Tx_Rtn();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_End_Tx_Rtn() throws Exception                                                                                                                        //Natural: END-TX-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *==========================
        //*  THIS RTN WILL DO AN ET AND WILL
        if (condition(pnd_Control_Counters_Pnd_Et_Ctr.greaterOrEqual(pnd_Work_Restart_Area_Pnd_Et_Limit)))                                                                //Natural: IF #ET-CTR GE #ET-LIMIT
        {
            //*  SET THE RECORD ON ADABAS IN CASE
            //*  WE NEED TO DO A RESTART.
            //*  'X' IS USED AS A RESTART IND
            //*  IF #RESTART-TEXT NE ' '
            //*  IT WILL EQ 'X'. THIS FIELD IS SET
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION 'X' #REC-READ CV-PIN-NBR CV-CONT
            //*  HERE AND LOOKED FOR IN THE
            //*  GET TRANSACTION DATA
            pnd_Control_Counters_Pnd_Et_Ctr.reset();                                                                                                                      //Natural: RESET #ET-CTR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Name_Check() throws Exception                                                                                                                        //Natural: NAME-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *==========================
        pnd_Where.reset();                                                                                                                                                //Natural: RESET #WHERE
        DbsUtil.examine(new ExamineSource(pnd_Test_Name), new ExamineSearch("-"), new ExamineGivingPosition(pnd_Where));                                                  //Natural: EXAMINE #TEST-NAME FOR '-' GIVING POSITION #WHERE
    }
    private void sub_Error() throws Exception                                                                                                                             //Natural: ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* *=====================
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* *======
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        getReports().write(0, "Program:   ",Global.getPROGRAM(),NEWLINE,"Error Line:",Global.getERROR_LINE(),NEWLINE,"Error:     ",Global.getERROR_NR());                 //Natural: WRITE 'Program:   ' *PROGRAM / 'Error Line:' *ERROR-LINE / 'Error:     ' *ERROR-NR
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_DivsubIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Divsub().isBreak(endOfData);
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_Coll_CodeIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().isBreak(endOfData);
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No().isBreak(endOfData);
        boolean ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak = ldaAppl302.getCv_Prap_Rule1_Cv_Key().isBreak(endOfData);
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_DivsubIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_Coll_CodeIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak 
            || ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            //*  DCA KG
            pnd_Indicators_Pnd_Newpage_Ind.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #NEWPAGE-IND
            //*  DCA KG
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_Coll_CodeIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            pnd_Indicators_Pnd_Newpage_Ind.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #NEWPAGE-IND
            //*  08/23/04
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_Sgrd_Plan_NoIsBreak || ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            //*  08/23/04
            //*  08/23/04
            pnd_Indicators_Pnd_Newpage_Ind.setValue("Y");                                                                                                                 //Natural: MOVE 'Y' TO #NEWPAGE-IND
            pnd_Ab_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No());                                                                                          //Natural: ASSIGN #AB-PLAN := CV-SGRD-PLAN-NO
            //*  08/23/04
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaAppl302_getCv_Prap_Rule1_Cv_KeyIsBreak))
        {
            if (condition(pnd_Work_Restart_Area_Pnd_Rec_Read.less(pnd_Work_Restart_Area_Pnd_Restart_Rec) && pnd_Work_Restart_Area_Pnd_Restart_Ind.getBoolean()))          //Natural: IF #REC-READ LT #RESTART-REC AND #RESTART-IND
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_I.notEquals(getZero())))                                                                                                                //Natural: IF #I NE 0
                {
                                                                                                                                                                          //Natural: PERFORM STORE-POST
                    sub_Store_Post();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                pnd_Indicators_Pnd_Newpage_Ind.reset();                                                                                                                   //Natural: RESET #NEWPAGE-IND
                if (condition(pnd_Indicators_Pnd_Store_Post.equals("Y")))                                                                                                 //Natural: IF #STORE-POST EQ 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM POST-CLOSE
                    sub_Post_Close();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Control_Counters_Pnd_Packages.nadd(1);                                                                                                            //Natural: ADD 1 TO #PACKAGES
                }                                                                                                                                                         //Natural: END-IF
                pnd_Indicators_Pnd_Store_Post.reset();                                                                                                                    //Natural: RESET #STORE-POST
                                                                                                                                                                          //Natural: PERFORM GET-INST-ADDR
                sub_Get_Inst_Addr();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ASSIGN-POST-DATA
                sub_Assign_Post_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM POST-OPEN
                sub_Post_Open();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pdaPsta9500.getPsta9500().reset();                                                                                                                        //Natural: RESET PSTA9500
                                                                                                                                                                          //Natural: PERFORM LOAD-LETTER-DATA
                sub_Load_Letter_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                ldaPstl6335.getPstl6335_Data_Prap_Letter_Report_Detail().getValue("*").reset();                                                                           //Natural: RESET PRAP-LETTER-REPORT-DETAIL ( * ) #I
                pnd_I.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 HW=OFF PS=52");
        Global.format(2, "LS=132 HW=OFF PS=52");
        Global.format(3, "LS=132 HW=OFF PS=52");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(120),Global.getDATU(),NEWLINE,"PAGE",getReports().getPageNumberDbs(2),new 
            TabSetting(120),Global.getTIMX(),NEWLINE,NEWLINE,new ColumnSpacing(49),pnd_Title,NEWLINE,NEWLINE,new ColumnSpacing(58),"Exception report",NEWLINE,NEWLINE,"-",new 
            RepeatItem(131),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, "PPG",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code(),"LAST/NAME",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Cor_Last_Nme(),"FIRST/NAME",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Cor_First_Nme(),"SOCIAL/SECURITY NUMBER",
        		ldaAppl302.getCv_Prap_Rule1_Cv_Soc_Sec(), new ReportEditMask ("999-99-9999"));
    }
    private void CheckAtStartofData855() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Record_Type().equals(1)))                                                                                        //Natural: IF CV-RECORD-TYPE EQ 1
            {
                pnd_Title.setValue("PRAP Application Overdue Letters");                                                                                                   //Natural: MOVE 'PRAP Application Overdue Letters' TO #TITLE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Title.setValue("  PRAP Premium Overdue Letters  ");                                                                                                   //Natural: MOVE '  PRAP Premium Overdue Letters  ' TO #TITLE
            }                                                                                                                                                             //Natural: END-IF
            //*  08/23/04
            if (condition(ldaAppl302.getCv_Prap_Rule1_Cv_Coll_Code().equals("SGRD")))                                                                                     //Natural: IF CV-COLL-CODE = 'SGRD'
            {
                //*  08/23/04
                //*  08/23/04
                pnd_Sgrd.setValue(true);                                                                                                                                  //Natural: MOVE TRUE TO #SGRD
                pnd_Ab_Plan.setValue(ldaAppl302.getCv_Prap_Rule1_Cv_Sgrd_Plan_No());                                                                                      //Natural: ASSIGN #AB-PLAN := CV-SGRD-PLAN-NO
                //*  08/23/04
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
