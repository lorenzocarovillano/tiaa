/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:09 PM
**        * FROM NATURAL PROGRAM : Scib9600
************************************************************
**        * FILE NAME            : Scib9600.java
**        * CLASS NAME           : Scib9600
**        * INSTANCE NAME        : Scib9600
************************************************************
************************************************************************
* PROGRAM  : SCIB9600 - OMNI/ACIS T051, T850, T851, T813 (PH665) AND
*            T801 CHG (PH665) DRIVER
* FUNCTION   NEW NATURAL PROGRAM TO READ THE INTERFACE FILE OF
*            ALL THE T051, T850, T851, T813 (PH665) AND T801 CHG (PH665)
*            TRANSACTIONS DONE IN OMNIPLUS.
*            THIS PROGRAM WILL CALL SCIN9600 TO PERFORM THE UPDATES TO
*            THE CONTRACT IN ANNTY-ACTVTY-PRAP AND ACIS-REPRINT-FL.  IT
*            WILL ALSO CREATE THE INTERFACE FILE TO MDM.
* -------------------------------------------------------------------- *
*   DATE   DEVELOPER CHANGE DESCRIPTION                                *
* -------- --------- ------------------------------------------------- *
* 12/07/09 B.ELLO    DO NOT CALL SCIN9600 FOR THE UPDATE ON PH665- OMNI-
*                    ACCOUNT ISSUANCE INDICATOR.  THE CRITERIA NOT TO
*                    CALL SCIN9600 IS BASED ON REC-TYPE = A801 OR A813.
* 06/11/10 H.KAKADIA ADDED CODE TO BYPASS B813 REC-TYPE
*                    B813 IS BEING HANDLED BY PSG1250 COBOL MODULE
* 01/18/12 B.ELLO    ADDED THE SSN IN THE MDM INTERFACE FILE. SEE SRK
* 07/15/14 B.NEWSOM  THE OUTPUT FILE IS MODIFIED TO INCLUDE THE PLAN
*                    AND SUBPLAN FROM THE BASEXT FILE TO THE OUTPUT
*                    FOR MDM.                                   (MDMSOR)
* 09/01/15 B.ELLO    ADDED THE BENE IN THE PLAN DATA ELEMENTS   (BIP)
*                    CHANGED LRECL FOR MDM FILE FROM 120 TO 160 (BIP)
* 09/01/15 B.NEWSOM  COR/NAAD RETIREMENT                        (CNR)
*                    REPLACED COR READ WITH A READ TO THE REPRINT
* 05/23/17 B.ELLO    PIN EXPANSION CHANGES
*                    CHANGED PRAP AND REPRINT VIEWS TO USE MODIFIED
*                    VIEWS WITH EXPANDED PINS                   (PINE)
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9600 extends BLNatBase
{
    // Data Areas
    private PdaScia9600 pdaScia9600;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Omni_Interface_File;
    private DbsField pnd_Omni_Interface_File_Pnd_Rec_Type;
    private DbsField pnd_Omni_Interface_File_Pnd_Plan;
    private DbsField pnd_Omni_Interface_File_Pnd_Subplan;
    private DbsField pnd_Omni_Interface_File_Pnd_Ssn;
    private DbsField pnd_Omni_Interface_File_Pnd_Pin;
    private DbsField pnd_Omni_Interface_File_Pnd_Tiaa_Contract;
    private DbsField pnd_Omni_Interface_File_Pnd_Other_Parms;

    private DbsGroup pnd_Omni_Interface_File__R_Field_1;
    private DbsField pnd_Omni_Interface_File_Pnd_New_Ssn;
    private DbsField pnd_Omni_Interface_File_Pnd_New_Subplan;

    private DbsGroup pnd_Omni_Interface_File__R_Field_2;
    private DbsField pnd_Omni_Interface_File_Pnd_Old_Def_Enrl_Ind;
    private DbsField pnd_Omni_Interface_File_Pnd_New_Def_Enrl_Ind;

    private DbsGroup pnd_Mdm_Interface_File;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Rec_Type;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Pin;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Tiaa_Contract;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Payee_Code;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_From_Val;

    private DbsGroup pnd_Mdm_Interface_File__R_Field_3;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_From_Ssn;

    private DbsGroup pnd_Mdm_Interface_File__R_Field_4;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_From_Pin;

    private DbsGroup pnd_Mdm_Interface_File__R_Field_5;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_From_Dflt_Enrl_Ind;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_To_Val;

    private DbsGroup pnd_Mdm_Interface_File__R_Field_6;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_To_Ssn;

    private DbsGroup pnd_Mdm_Interface_File__R_Field_7;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_To_Pin;

    private DbsGroup pnd_Mdm_Interface_File__R_Field_8;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_To_Dflt_Enrl_Ind;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Filler;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Ssn;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Plan;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Subplan;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Acceptance_Flag;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Relation_To_Decedent;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Decedent_Contract;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Ssn_Tin_Ind;
    private DbsField pnd_Mdm_Interface_File_Pnd_Mdm_Filler2;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_Coll_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_T_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_C_Doi;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Cref_Cert;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext;
    private DbsField annty_Actvty_Prap_View_Ap_Status;

    private DbsGroup annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm;
    private DbsField annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time;

    private DataAccessProgramView vw_reprint;
    private DbsField reprint_Rp_Soc_Sec;
    private DbsField reprint_Rp_Tiaa_Contr;
    private DbsField reprint_Rp_Pin_Nbr;
    private DbsField pnd_Ssn_A9;

    private DbsGroup pnd_Ssn_A9__R_Field_9;
    private DbsField pnd_Ssn_A9_Pnd_Ssn_N9;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Mdm_Report_Count;
    private DbsField pnd_Acis_Update_Count;
    private DbsField pnd_Acis_Reject_Count;
    private DbsField pnd_Rec_Type_Desc;
    private DbsField pnd_Update_Reason;
    private DbsField pnd_D_New_Ssn;
    private DbsField pnd_D_New_Subplan;
    private DbsField pnd_D_Old_Def_Enrl_Ind;
    private DbsField pnd_D_New_Def_Enrl_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaScia9600 = new PdaScia9600(localVariables);

        // Local Variables

        pnd_Omni_Interface_File = localVariables.newGroupInRecord("pnd_Omni_Interface_File", "#OMNI-INTERFACE-FILE");
        pnd_Omni_Interface_File_Pnd_Rec_Type = pnd_Omni_Interface_File.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 
            4);
        pnd_Omni_Interface_File_Pnd_Plan = pnd_Omni_Interface_File.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Plan", "#PLAN", FieldType.STRING, 6);
        pnd_Omni_Interface_File_Pnd_Subplan = pnd_Omni_Interface_File.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Subplan", "#SUBPLAN", FieldType.STRING, 
            6);
        pnd_Omni_Interface_File_Pnd_Ssn = pnd_Omni_Interface_File.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Ssn", "#SSN", FieldType.STRING, 9);
        pnd_Omni_Interface_File_Pnd_Pin = pnd_Omni_Interface_File.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Pin", "#PIN", FieldType.STRING, 12);
        pnd_Omni_Interface_File_Pnd_Tiaa_Contract = pnd_Omni_Interface_File.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Tiaa_Contract", "#TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Omni_Interface_File_Pnd_Other_Parms = pnd_Omni_Interface_File.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Other_Parms", "#OTHER-PARMS", FieldType.STRING, 
            15);

        pnd_Omni_Interface_File__R_Field_1 = pnd_Omni_Interface_File.newGroupInGroup("pnd_Omni_Interface_File__R_Field_1", "REDEFINE", pnd_Omni_Interface_File_Pnd_Other_Parms);
        pnd_Omni_Interface_File_Pnd_New_Ssn = pnd_Omni_Interface_File__R_Field_1.newFieldInGroup("pnd_Omni_Interface_File_Pnd_New_Ssn", "#NEW-SSN", FieldType.STRING, 
            9);
        pnd_Omni_Interface_File_Pnd_New_Subplan = pnd_Omni_Interface_File__R_Field_1.newFieldInGroup("pnd_Omni_Interface_File_Pnd_New_Subplan", "#NEW-SUBPLAN", 
            FieldType.STRING, 6);

        pnd_Omni_Interface_File__R_Field_2 = pnd_Omni_Interface_File.newGroupInGroup("pnd_Omni_Interface_File__R_Field_2", "REDEFINE", pnd_Omni_Interface_File_Pnd_Other_Parms);
        pnd_Omni_Interface_File_Pnd_Old_Def_Enrl_Ind = pnd_Omni_Interface_File__R_Field_2.newFieldInGroup("pnd_Omni_Interface_File_Pnd_Old_Def_Enrl_Ind", 
            "#OLD-DEF-ENRL-IND", FieldType.STRING, 1);
        pnd_Omni_Interface_File_Pnd_New_Def_Enrl_Ind = pnd_Omni_Interface_File__R_Field_2.newFieldInGroup("pnd_Omni_Interface_File_Pnd_New_Def_Enrl_Ind", 
            "#NEW-DEF-ENRL-IND", FieldType.STRING, 1);

        pnd_Mdm_Interface_File = localVariables.newGroupInRecord("pnd_Mdm_Interface_File", "#MDM-INTERFACE-FILE");
        pnd_Mdm_Interface_File_Pnd_Mdm_Rec_Type = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Rec_Type", "#MDM-REC-TYPE", FieldType.STRING, 
            3);
        pnd_Mdm_Interface_File_Pnd_Mdm_Pin = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Pin", "#MDM-PIN", FieldType.STRING, 
            12);
        pnd_Mdm_Interface_File_Pnd_Mdm_Tiaa_Contract = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Tiaa_Contract", "#MDM-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Mdm_Interface_File_Pnd_Mdm_Payee_Code = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Payee_Code", "#MDM-PAYEE-CODE", 
            FieldType.STRING, 2);
        pnd_Mdm_Interface_File_Pnd_Mdm_From_Val = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_From_Val", "#MDM-FROM-VAL", FieldType.STRING, 
            29);

        pnd_Mdm_Interface_File__R_Field_3 = pnd_Mdm_Interface_File.newGroupInGroup("pnd_Mdm_Interface_File__R_Field_3", "REDEFINE", pnd_Mdm_Interface_File_Pnd_Mdm_From_Val);
        pnd_Mdm_Interface_File_Pnd_Mdm_From_Ssn = pnd_Mdm_Interface_File__R_Field_3.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_From_Ssn", "#MDM-FROM-SSN", 
            FieldType.STRING, 9);

        pnd_Mdm_Interface_File__R_Field_4 = pnd_Mdm_Interface_File.newGroupInGroup("pnd_Mdm_Interface_File__R_Field_4", "REDEFINE", pnd_Mdm_Interface_File_Pnd_Mdm_From_Val);
        pnd_Mdm_Interface_File_Pnd_Mdm_From_Pin = pnd_Mdm_Interface_File__R_Field_4.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_From_Pin", "#MDM-FROM-PIN", 
            FieldType.STRING, 12);

        pnd_Mdm_Interface_File__R_Field_5 = pnd_Mdm_Interface_File.newGroupInGroup("pnd_Mdm_Interface_File__R_Field_5", "REDEFINE", pnd_Mdm_Interface_File_Pnd_Mdm_From_Val);
        pnd_Mdm_Interface_File_Pnd_Mdm_From_Dflt_Enrl_Ind = pnd_Mdm_Interface_File__R_Field_5.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_From_Dflt_Enrl_Ind", 
            "#MDM-FROM-DFLT-ENRL-IND", FieldType.STRING, 1);
        pnd_Mdm_Interface_File_Pnd_Mdm_To_Val = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_To_Val", "#MDM-TO-VAL", FieldType.STRING, 
            29);

        pnd_Mdm_Interface_File__R_Field_6 = pnd_Mdm_Interface_File.newGroupInGroup("pnd_Mdm_Interface_File__R_Field_6", "REDEFINE", pnd_Mdm_Interface_File_Pnd_Mdm_To_Val);
        pnd_Mdm_Interface_File_Pnd_Mdm_To_Ssn = pnd_Mdm_Interface_File__R_Field_6.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_To_Ssn", "#MDM-TO-SSN", 
            FieldType.STRING, 9);

        pnd_Mdm_Interface_File__R_Field_7 = pnd_Mdm_Interface_File.newGroupInGroup("pnd_Mdm_Interface_File__R_Field_7", "REDEFINE", pnd_Mdm_Interface_File_Pnd_Mdm_To_Val);
        pnd_Mdm_Interface_File_Pnd_Mdm_To_Pin = pnd_Mdm_Interface_File__R_Field_7.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_To_Pin", "#MDM-TO-PIN", 
            FieldType.STRING, 12);

        pnd_Mdm_Interface_File__R_Field_8 = pnd_Mdm_Interface_File.newGroupInGroup("pnd_Mdm_Interface_File__R_Field_8", "REDEFINE", pnd_Mdm_Interface_File_Pnd_Mdm_To_Val);
        pnd_Mdm_Interface_File_Pnd_Mdm_To_Dflt_Enrl_Ind = pnd_Mdm_Interface_File__R_Field_8.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_To_Dflt_Enrl_Ind", 
            "#MDM-TO-DFLT-ENRL-IND", FieldType.STRING, 1);
        pnd_Mdm_Interface_File_Pnd_Mdm_Filler = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Filler", "#MDM-FILLER", FieldType.STRING, 
            14);
        pnd_Mdm_Interface_File_Pnd_Mdm_Ssn = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Ssn", "#MDM-SSN", FieldType.NUMERIC, 
            9);
        pnd_Mdm_Interface_File_Pnd_Mdm_Plan = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Plan", "#MDM-PLAN", FieldType.STRING, 
            6);
        pnd_Mdm_Interface_File_Pnd_Mdm_Subplan = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Subplan", "#MDM-SUBPLAN", FieldType.STRING, 
            6);
        pnd_Mdm_Interface_File_Pnd_Mdm_Acceptance_Flag = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Acceptance_Flag", "#MDM-ACCEPTANCE-FLAG", 
            FieldType.STRING, 1);
        pnd_Mdm_Interface_File_Pnd_Mdm_Relation_To_Decedent = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Relation_To_Decedent", 
            "#MDM-RELATION-TO-DECEDENT", FieldType.STRING, 1);
        pnd_Mdm_Interface_File_Pnd_Mdm_Decedent_Contract = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Decedent_Contract", 
            "#MDM-DECEDENT-CONTRACT", FieldType.STRING, 10);
        pnd_Mdm_Interface_File_Pnd_Mdm_Ssn_Tin_Ind = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Ssn_Tin_Ind", "#MDM-SSN-TIN-IND", 
            FieldType.STRING, 1);
        pnd_Mdm_Interface_File_Pnd_Mdm_Filler2 = pnd_Mdm_Interface_File.newFieldInGroup("pnd_Mdm_Interface_File_Pnd_Mdm_Filler2", "#MDM-FILLER2", FieldType.STRING, 
            27);

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_Coll_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Coll_Code", "AP-COLL-CODE", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_COLL_CODE");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_T_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_T_Doi", "AP-T-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_T_DOI");
        annty_Actvty_Prap_View_Ap_C_Doi = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_C_Doi", "AP-C-DOI", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_C_DOI");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Bank_Pymnt_Acct_Nmbr", 
            "AP-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "AP_BANK_PYMNT_ACCT_NMBR");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Cref_Cert = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cref_Cert", "AP-CREF-CERT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_CREF_CERT");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Subplan_No", 
            "AP-SGRD-SUBPLAN-NO", FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_SUBPLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Part_Ext", "AP-SGRD-PART-EXT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_SGRD_PART_EXT");
        annty_Actvty_Prap_View_Ap_Status = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Status", "AP-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_STATUS");

        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm", 
            "AP-RQST-LOG-DTE-TM", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time = annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Tm.newFieldArrayInGroup("annty_Actvty_Prap_View_Ap_Rqst_Log_Dte_Time", 
            "AP-RQST-LOG-DTE-TIME", FieldType.STRING, 15, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_RQST_LOG_DTE_TIME", 
            "ACIS_ANNTY_PRAP_AP_RQST_LOG_DTE_TM");
        registerRecord(vw_annty_Actvty_Prap_View);

        vw_reprint = new DataAccessProgramView(new NameInfo("vw_reprint", "REPRINT"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE");
        reprint_Rp_Soc_Sec = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Soc_Sec", "RP-SOC-SEC", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "RP_SOC_SEC");
        reprint_Rp_Soc_Sec.setDdmHeader("SOC/SEC");
        reprint_Rp_Tiaa_Contr = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RP_TIAA_CONTR");
        reprint_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        reprint_Rp_Pin_Nbr = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Pin_Nbr", "RP-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "RP_PIN_NBR");
        reprint_Rp_Pin_Nbr.setDdmHeader("PIN/NBR");
        registerRecord(vw_reprint);

        pnd_Ssn_A9 = localVariables.newFieldInRecord("pnd_Ssn_A9", "#SSN-A9", FieldType.STRING, 9);

        pnd_Ssn_A9__R_Field_9 = localVariables.newGroupInRecord("pnd_Ssn_A9__R_Field_9", "REDEFINE", pnd_Ssn_A9);
        pnd_Ssn_A9_Pnd_Ssn_N9 = pnd_Ssn_A9__R_Field_9.newFieldInGroup("pnd_Ssn_A9_Pnd_Ssn_N9", "#SSN-N9", FieldType.NUMERIC, 9);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 7);
        pnd_Mdm_Report_Count = localVariables.newFieldInRecord("pnd_Mdm_Report_Count", "#MDM-REPORT-COUNT", FieldType.NUMERIC, 7);
        pnd_Acis_Update_Count = localVariables.newFieldInRecord("pnd_Acis_Update_Count", "#ACIS-UPDATE-COUNT", FieldType.NUMERIC, 7);
        pnd_Acis_Reject_Count = localVariables.newFieldInRecord("pnd_Acis_Reject_Count", "#ACIS-REJECT-COUNT", FieldType.NUMERIC, 7);
        pnd_Rec_Type_Desc = localVariables.newFieldInRecord("pnd_Rec_Type_Desc", "#REC-TYPE-DESC", FieldType.STRING, 10);
        pnd_Update_Reason = localVariables.newFieldInRecord("pnd_Update_Reason", "#UPDATE-REASON", FieldType.STRING, 15);
        pnd_D_New_Ssn = localVariables.newFieldInRecord("pnd_D_New_Ssn", "#D-NEW-SSN", FieldType.STRING, 9);
        pnd_D_New_Subplan = localVariables.newFieldInRecord("pnd_D_New_Subplan", "#D-NEW-SUBPLAN", FieldType.STRING, 6);
        pnd_D_Old_Def_Enrl_Ind = localVariables.newFieldInRecord("pnd_D_Old_Def_Enrl_Ind", "#D-OLD-DEF-ENRL-IND", FieldType.STRING, 1);
        pnd_D_New_Def_Enrl_Ind = localVariables.newFieldInRecord("pnd_D_New_Def_Enrl_Ind", "#D-NEW-DEF-ENRL-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();
        vw_reprint.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9600() throws Exception
    {
        super("Scib9600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIB9600", onError);
        setupReports();
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 52
        getReports().definePrinter(3, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 2 )
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) LS = 133 PS = 52
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED 001T 'REPORT DATE:' 1X *DATU '-' *TIMX ( EM = HH:IIAP ) 050T 'OMNI TO MDM INTERFACE REPORT' 103T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZZZ9 ) / 001T 'NAT PROGRAM:' *PROGRAM 057T 'AUDIT CONTROL RPT' 103T *INIT-USER / 001T '-' ( 113 )
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT JUSTIFIED 001T 'REPORT DATE:' 1X *DATU '-' *TIMX ( EM = HH:IIAP ) 050T 'OMNI TO ACIS UPDATE REPORT' 103T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZZZ9 ) / 001T 'NAT PROGRAM:' *PROGRAM 057T 'AUDIT CONTROL RPT' 103T *INIT-USER / 001T '-' ( 113 )
        L_READ:                                                                                                                                                           //Natural: READ WORK FILE 1 #OMNI-INTERFACE-FILE
        while (condition(getWorkFiles().read(1, pnd_Omni_Interface_File)))
        {
            pnd_Record_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORD-COUNT
            if (condition(pnd_Omni_Interface_File_Pnd_Rec_Type.equals("B813")))                                                                                           //Natural: IF #REC-TYPE = 'B813'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE INTERFACE RECORD TO MDM
                                                                                                                                                                          //Natural: PERFORM WRITE-MDM-INTERFACE
            sub_Write_Mdm_Interface();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("L_READ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("L_READ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WRITE MDM CONTROL REPORT
                                                                                                                                                                          //Natural: PERFORM WRITE-MDM-CONTROL-REPORT
            sub_Write_Mdm_Control_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("L_READ"))) break;
                else if (condition(Global.isEscapeBottomImmediate("L_READ"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A801") || pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A813"))))                                //Natural: IF NOT ( #REC-TYPE = 'A801' OR = 'A813' )
            {
                //*  UPDATE ACIS
                                                                                                                                                                          //Natural: PERFORM CALL-ACIS-INTERFACE
                sub_Call_Acis_Interface();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  WRITE ACIS CONTROL REPORT
                                                                                                                                                                          //Natural: PERFORM WRITE-ACIS-CONTROL-REPORT
                sub_Write_Acis_Control_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_READ"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_READ"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        L_READ_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL MDM INTERFACE-RECORDS:  ",pnd_Mdm_Report_Count);                                                                //Natural: WRITE ( 1 ) NOTITLE 'TOTAL MDM INTERFACE-RECORDS:  ' #MDM-REPORT-COUNT
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TOTAL SUCCESSFUL ACIS UPDATE  : ",pnd_Acis_Update_Count,NEWLINE,"TOTAL UNSUCCESSFUL ACIS UPDATE: ",                   //Natural: WRITE ( 2 ) NOTITLE 'TOTAL SUCCESSFUL ACIS UPDATE  : ' #ACIS-UPDATE-COUNT / 'TOTAL UNSUCCESSFUL ACIS UPDATE: ' #ACIS-REJECT-COUNT
            pnd_Acis_Reject_Count);
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-ACIS-INTERFACE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MDM-INTERFACE
        //*  #MDM-PIN := VAL(#PIN)
        //*    IF #MDM-FROM-DFLT-ENRL-IND = '0'
        //*      RESET #MDM-FROM-DFLT-ENRL-IND
        //*    END-IF
        //*    IF #MDM-TO-DFLT-ENRL-IND = '0'
        //*      RESET #MDM-TO-DFLT-ENRL-IND
        //*    END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MDM-CONTROL-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ACIS-CONTROL-REPORT
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Call_Acis_Interface() throws Exception                                                                                                               //Natural: CALL-ACIS-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaScia9600.getScia9600().reset();                                                                                                                                //Natural: RESET SCIA9600
        pdaScia9600.getScia9600_Pnd_P_Rec_Type().setValue(pnd_Omni_Interface_File_Pnd_Rec_Type);                                                                          //Natural: ASSIGN #P-REC-TYPE := #REC-TYPE
        pdaScia9600.getScia9600_Pnd_P_Plan().setValue(pnd_Omni_Interface_File_Pnd_Plan);                                                                                  //Natural: ASSIGN #P-PLAN := #PLAN
        pdaScia9600.getScia9600_Pnd_P_Subplan().setValue(pnd_Omni_Interface_File_Pnd_Subplan);                                                                            //Natural: ASSIGN #P-SUBPLAN := #SUBPLAN
        pdaScia9600.getScia9600_Pnd_P_Ssn().setValue(pnd_Omni_Interface_File_Pnd_Ssn);                                                                                    //Natural: ASSIGN #P-SSN := #SSN
        if (condition(pnd_Omni_Interface_File_Pnd_Pin.greater(" ")))                                                                                                      //Natural: IF #PIN GT ' '
        {
            pnd_Omni_Interface_File_Pnd_Pin.setValue(pnd_Omni_Interface_File_Pnd_Pin, MoveOption.RightJustified);                                                         //Natural: MOVE RIGHT #PIN TO #PIN
            DbsUtil.examine(new ExamineSource(pnd_Omni_Interface_File_Pnd_Pin,true), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE FULL #PIN FOR ' ' REPLACE WITH '0'
            pdaScia9600.getScia9600_Pnd_P_Pin().compute(new ComputeParameters(false, pdaScia9600.getScia9600_Pnd_P_Pin()), pnd_Omni_Interface_File_Pnd_Pin.val());        //Natural: ASSIGN #P-PIN := VAL ( #PIN )
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia9600.getScia9600_Pnd_P_Tiaa_Contract().setValue(pnd_Omni_Interface_File_Pnd_Tiaa_Contract);                                                                //Natural: ASSIGN #P-TIAA-CONTRACT := #TIAA-CONTRACT
        pdaScia9600.getScia9600_Pnd_P_New_Ssn().setValue(pnd_Omni_Interface_File_Pnd_New_Ssn);                                                                            //Natural: ASSIGN #P-NEW-SSN := #NEW-SSN
        pdaScia9600.getScia9600_Pnd_P_New_Subplan().setValue(pnd_Omni_Interface_File_Pnd_New_Subplan);                                                                    //Natural: ASSIGN #P-NEW-SUBPLAN := #NEW-SUBPLAN
        pdaScia9600.getScia9600_Pnd_P_New_Def_Enrl_Ind().setValue(pnd_Omni_Interface_File_Pnd_New_Def_Enrl_Ind);                                                          //Natural: ASSIGN #P-NEW-DEF-ENRL-IND := #NEW-DEF-ENRL-IND
        DbsUtil.callnat(Scin9600.class , getCurrentProcessState(), pdaScia9600.getScia9600());                                                                            //Natural: CALLNAT 'SCIN9600' SCIA9600
        if (condition(Global.isEscape())) return;
        //*  CALL-ACIS-INTERFACE
    }
    private void sub_Write_Mdm_Interface() throws Exception                                                                                                               //Natural: WRITE-MDM-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Mdm_Interface_File.reset();                                                                                                                                   //Natural: RESET #MDM-INTERFACE-FILE
        if (condition(pnd_Omni_Interface_File_Pnd_Pin.greater(" ")))                                                                                                      //Natural: IF #PIN GT ' '
        {
            pnd_Omni_Interface_File_Pnd_Pin.setValue(pnd_Omni_Interface_File_Pnd_Pin, MoveOption.RightJustified);                                                         //Natural: MOVE RIGHT #PIN TO #PIN
            //*  BIP
            DbsUtil.examine(new ExamineSource(pnd_Omni_Interface_File_Pnd_Pin,true), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE FULL #PIN FOR ' ' REPLACE WITH '0'
            pnd_Mdm_Interface_File_Pnd_Mdm_Pin.setValue(pnd_Omni_Interface_File_Pnd_Pin);                                                                                 //Natural: ASSIGN #MDM-PIN := #PIN
        }                                                                                                                                                                 //Natural: END-IF
        //*  SRK
        if (condition(pnd_Omni_Interface_File_Pnd_Ssn.greater(" ")))                                                                                                      //Natural: IF #SSN GT ' '
        {
            //*  SRK
            pnd_Omni_Interface_File_Pnd_Ssn.setValue(pnd_Omni_Interface_File_Pnd_Ssn, MoveOption.RightJustified);                                                         //Natural: MOVE RIGHT #SSN TO #SSN
            //*  SRK
            //*  SRK
            DbsUtil.examine(new ExamineSource(pnd_Omni_Interface_File_Pnd_Ssn,true), new ExamineSearch(" "), new ExamineReplace("0"));                                    //Natural: EXAMINE FULL #SSN FOR ' ' REPLACE WITH '0'
            pnd_Mdm_Interface_File_Pnd_Mdm_Ssn.compute(new ComputeParameters(false, pnd_Mdm_Interface_File_Pnd_Mdm_Ssn), pnd_Omni_Interface_File_Pnd_Ssn.val());          //Natural: ASSIGN #MDM-SSN := VAL ( #SSN )
            //*  SRK
            //*  MDMSOR
            //*  MDMSOR
            //*  HARDCODED TO '01' PAYEE CODE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mdm_Interface_File_Pnd_Mdm_Plan.setValue(pnd_Omni_Interface_File_Pnd_Plan);                                                                                   //Natural: ASSIGN #MDM-PLAN := #PLAN
        pnd_Mdm_Interface_File_Pnd_Mdm_Subplan.setValue(pnd_Omni_Interface_File_Pnd_Subplan);                                                                             //Natural: ASSIGN #MDM-SUBPLAN := #SUBPLAN
        pnd_Mdm_Interface_File_Pnd_Mdm_Tiaa_Contract.setValue(pnd_Omni_Interface_File_Pnd_Tiaa_Contract);                                                                 //Natural: ASSIGN #MDM-TIAA-CONTRACT := #TIAA-CONTRACT
        pnd_Mdm_Interface_File_Pnd_Mdm_Payee_Code.setValue("01");                                                                                                         //Natural: ASSIGN #MDM-PAYEE-CODE := '01'
        //*  CONTRACT DELETE
        //*  ENROLLMENT PROD. DEFAULT IND. UPDATE
        //*  SSN CHANGE
        //*  PIN MERGE
        short decideConditionsMet280 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #REC-TYPE;//Natural: VALUE 'A051'
        if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A051"))))
        {
            decideConditionsMet280++;
            pnd_Mdm_Interface_File_Pnd_Mdm_Rec_Type.setValue("1");                                                                                                        //Natural: ASSIGN #MDM-REC-TYPE := '1'
        }                                                                                                                                                                 //Natural: VALUE 'A801', 'A813'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A801") || pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A813"))))
        {
            decideConditionsMet280++;
            pnd_Mdm_Interface_File_Pnd_Mdm_Rec_Type.setValue("2");                                                                                                        //Natural: ASSIGN #MDM-REC-TYPE := '2'
            pnd_Mdm_Interface_File_Pnd_Mdm_From_Dflt_Enrl_Ind.setValue(pnd_Omni_Interface_File_Pnd_Old_Def_Enrl_Ind);                                                     //Natural: ASSIGN #MDM-FROM-DFLT-ENRL-IND := #OLD-DEF-ENRL-IND
            pnd_Mdm_Interface_File_Pnd_Mdm_To_Dflt_Enrl_Ind.setValue(pnd_Omni_Interface_File_Pnd_New_Def_Enrl_Ind);                                                       //Natural: ASSIGN #MDM-TO-DFLT-ENRL-IND := #NEW-DEF-ENRL-IND
        }                                                                                                                                                                 //Natural: VALUE 'A850'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A850"))))
        {
            decideConditionsMet280++;
            pnd_Mdm_Interface_File_Pnd_Mdm_Rec_Type.setValue("3");                                                                                                        //Natural: ASSIGN #MDM-REC-TYPE := '3'
            pnd_Mdm_Interface_File_Pnd_Mdm_From_Ssn.setValue(pnd_Omni_Interface_File_Pnd_Ssn);                                                                            //Natural: ASSIGN #MDM-FROM-SSN := #SSN
            pnd_Mdm_Interface_File_Pnd_Mdm_To_Ssn.setValue(pnd_Omni_Interface_File_Pnd_New_Ssn);                                                                          //Natural: ASSIGN #MDM-TO-SSN := #NEW-SSN
        }                                                                                                                                                                 //Natural: VALUE 'A851'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A851"))))
        {
            decideConditionsMet280++;
            pnd_Mdm_Interface_File_Pnd_Mdm_Rec_Type.setValue("4");                                                                                                        //Natural: ASSIGN #MDM-REC-TYPE := '4'
            pnd_Mdm_Interface_File_Pnd_Mdm_From_Pin.setValue(pnd_Omni_Interface_File_Pnd_Pin);                                                                            //Natural: ASSIGN #MDM-FROM-PIN := #PIN
            pnd_Mdm_Interface_File_Pnd_Mdm_To_Pin.reset();                                                                                                                //Natural: RESET #MDM-TO-PIN
            pnd_Ssn_A9.setValue(pnd_Omni_Interface_File_Pnd_New_Ssn);                                                                                                     //Natural: ASSIGN #SSN-A9 := #NEW-SSN
            //*    READ COR BY COR-SUPER-SSN-RCDTYPE-PIN STARTING FROM #SSN-A9   /* CNR
            //*      IF COR.PH-SOCIAL-SECURITY-NO NE #SSN-N9                     /* CNR
            //*        ESCAPE BOTTOM                                             /* CNR
            //*      END-IF                                                      /* CNR
            //*      #MDM-TO-PIN := COR.PH-UNIQUE-ID-NBR                         /* CNR
            //*      ESCAPE BOTTOM                                               /* CNR
            //*    END-READ
            //*  CNR
            vw_reprint.startDatabaseRead                                                                                                                                  //Natural: READ REPRINT BY RP-SSN-TIAA-NO STARTING FROM #SSN-A9
            (
            "READ01",
            new Wc[] { new Wc("RP_SSN_TIAA_NO", ">=", pnd_Ssn_A9, WcType.BY) },
            new Oc[] { new Oc("RP_SSN_TIAA_NO", "ASC") }
            );
            READ01:
            while (condition(vw_reprint.readNextRow("READ01")))
            {
                //*  CNR
                if (condition(reprint_Rp_Soc_Sec.notEquals(pnd_Ssn_A9_Pnd_Ssn_N9)))                                                                                       //Natural: IF REPRINT.RP-SOC-SEC NE #SSN-N9
                {
                    //*  CNR
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  CNR
                    //*  CNR
                }                                                                                                                                                         //Natural: END-IF
                pnd_Mdm_Interface_File_Pnd_Mdm_To_Pin.setValue(reprint_Rp_Pin_Nbr);                                                                                       //Natural: ASSIGN #MDM-TO-PIN := REPRINT.RP-PIN-NBR
                //*  CNR
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  CNR
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getWorkFiles().write(2, false, pnd_Mdm_Interface_File);                                                                                                           //Natural: WRITE WORK FILE 2 #MDM-INTERFACE-FILE
    }
    private void sub_Write_Mdm_Control_Report() throws Exception                                                                                                          //Natural: WRITE-MDM-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_D_New_Ssn.reset();                                                                                                                                            //Natural: RESET #D-NEW-SSN #D-NEW-SUBPLAN #D-OLD-DEF-ENRL-IND #D-NEW-DEF-ENRL-IND
        pnd_D_New_Subplan.reset();
        pnd_D_Old_Def_Enrl_Ind.reset();
        pnd_D_New_Def_Enrl_Ind.reset();
        short decideConditionsMet329 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #REC-TYPE;//Natural: VALUE 'A051'
        if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A051"))))
        {
            decideConditionsMet329++;
            pnd_Rec_Type_Desc.setValue("CONT. DLTE");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'CONT. DLTE'
        }                                                                                                                                                                 //Natural: VALUE 'A801', 'A813'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A801") || pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A813"))))
        {
            decideConditionsMet329++;
            pnd_Rec_Type_Desc.setValue("UPDT PH665");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'UPDT PH665'
            pnd_D_Old_Def_Enrl_Ind.setValue(pnd_Omni_Interface_File_Pnd_Old_Def_Enrl_Ind);                                                                                //Natural: ASSIGN #D-OLD-DEF-ENRL-IND := #OLD-DEF-ENRL-IND
            pnd_D_New_Def_Enrl_Ind.setValue(pnd_Omni_Interface_File_Pnd_New_Def_Enrl_Ind);                                                                                //Natural: ASSIGN #D-NEW-DEF-ENRL-IND := #NEW-DEF-ENRL-IND
        }                                                                                                                                                                 //Natural: VALUE 'A850'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A850"))))
        {
            decideConditionsMet329++;
            pnd_Rec_Type_Desc.setValue("SSN CHANGE");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'SSN CHANGE'
            pnd_D_New_Ssn.setValue(pnd_Omni_Interface_File_Pnd_New_Ssn);                                                                                                  //Natural: ASSIGN #D-NEW-SSN := #NEW-SSN
            pnd_D_New_Subplan.setValue(pnd_Omni_Interface_File_Pnd_New_Subplan);                                                                                          //Natural: ASSIGN #D-NEW-SUBPLAN := #NEW-SUBPLAN
        }                                                                                                                                                                 //Natural: VALUE 'A851'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A851"))))
        {
            decideConditionsMet329++;
            pnd_Rec_Type_Desc.setValue("PIN MERGE ");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'PIN MERGE '
            pnd_D_New_Ssn.setValue(pnd_Omni_Interface_File_Pnd_New_Ssn);                                                                                                  //Natural: ASSIGN #D-NEW-SSN := #NEW-SSN
            pnd_D_New_Subplan.setValue(pnd_Omni_Interface_File_Pnd_New_Subplan);                                                                                          //Natural: ASSIGN #D-NEW-SUBPLAN := #NEW-SUBPLAN
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().display(1, "ACTION  ",                                                                                                                               //Natural: DISPLAY ( 1 ) 'ACTION  ' #REC-TYPE-DESC 'PLAN  ' #PLAN 'SUBPLAN' #SUBPLAN 'SSN      ' #SSN 'PIN   ' #PIN 'TIAA CONT.' #TIAA-CONTRACT 'NEW SSN  ' #D-NEW-SSN 'NEW SUBPLAN' #D-NEW-SUBPLAN 'OLD PH665' #D-OLD-DEF-ENRL-IND 'NEW PH665' #D-NEW-DEF-ENRL-IND
        		pnd_Rec_Type_Desc,"PLAN  ",
        		pnd_Omni_Interface_File_Pnd_Plan,"SUBPLAN",
        		pnd_Omni_Interface_File_Pnd_Subplan,"SSN      ",
        		pnd_Omni_Interface_File_Pnd_Ssn,"PIN   ",
        		pnd_Omni_Interface_File_Pnd_Pin,"TIAA CONT.",
        		pnd_Omni_Interface_File_Pnd_Tiaa_Contract,"NEW SSN  ",
        		pnd_D_New_Ssn,"NEW SUBPLAN",
        		pnd_D_New_Subplan,"OLD PH665",
        		pnd_D_Old_Def_Enrl_Ind,"NEW PH665",
        		pnd_D_New_Def_Enrl_Ind);
        if (Global.isEscape()) return;
        pnd_Mdm_Report_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #MDM-REPORT-COUNT
    }
    private void sub_Write_Acis_Control_Report() throws Exception                                                                                                         //Natural: WRITE-ACIS-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_D_New_Ssn.reset();                                                                                                                                            //Natural: RESET #D-NEW-SSN #D-NEW-SUBPLAN #D-OLD-DEF-ENRL-IND #D-NEW-DEF-ENRL-IND
        pnd_D_New_Subplan.reset();
        pnd_D_Old_Def_Enrl_Ind.reset();
        pnd_D_New_Def_Enrl_Ind.reset();
        short decideConditionsMet355 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #REC-TYPE;//Natural: VALUE 'A051'
        if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A051"))))
        {
            decideConditionsMet355++;
            pnd_Rec_Type_Desc.setValue("CONT. DLTE");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'CONT. DLTE'
        }                                                                                                                                                                 //Natural: VALUE 'A801', 'A813'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A801") || pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A813"))))
        {
            decideConditionsMet355++;
            pnd_Rec_Type_Desc.setValue("UPDT PH665");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'UPDT PH665'
            pnd_D_Old_Def_Enrl_Ind.setValue(pnd_Omni_Interface_File_Pnd_Old_Def_Enrl_Ind);                                                                                //Natural: ASSIGN #D-OLD-DEF-ENRL-IND := #OLD-DEF-ENRL-IND
            pnd_D_New_Def_Enrl_Ind.setValue(pnd_Omni_Interface_File_Pnd_New_Def_Enrl_Ind);                                                                                //Natural: ASSIGN #D-NEW-DEF-ENRL-IND := #NEW-DEF-ENRL-IND
        }                                                                                                                                                                 //Natural: VALUE 'A850'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A850"))))
        {
            decideConditionsMet355++;
            pnd_Rec_Type_Desc.setValue("SSN CHANGE");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'SSN CHANGE'
            pnd_D_New_Ssn.setValue(pnd_Omni_Interface_File_Pnd_New_Ssn);                                                                                                  //Natural: ASSIGN #D-NEW-SSN := #NEW-SSN
            pnd_D_New_Subplan.setValue(pnd_Omni_Interface_File_Pnd_New_Subplan);                                                                                          //Natural: ASSIGN #D-NEW-SUBPLAN := #NEW-SUBPLAN
        }                                                                                                                                                                 //Natural: VALUE 'A851'
        else if (condition((pnd_Omni_Interface_File_Pnd_Rec_Type.equals("A851"))))
        {
            decideConditionsMet355++;
            pnd_Rec_Type_Desc.setValue("PIN MERGE ");                                                                                                                     //Natural: ASSIGN #REC-TYPE-DESC := 'PIN MERGE '
            pnd_D_New_Ssn.setValue(pnd_Omni_Interface_File_Pnd_New_Ssn);                                                                                                  //Natural: ASSIGN #D-NEW-SSN := #NEW-SSN
            pnd_D_New_Subplan.setValue(pnd_Omni_Interface_File_Pnd_New_Subplan);                                                                                          //Natural: ASSIGN #D-NEW-SUBPLAN := #NEW-SUBPLAN
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Update_Reason.reset();                                                                                                                                        //Natural: RESET #UPDATE-REASON
        short decideConditionsMet375 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #P-RETURN-CODE;//Natural: VALUE '0000'
        if (condition((pdaScia9600.getScia9600_Pnd_P_Return_Code().equals("0000"))))
        {
            decideConditionsMet375++;
            pnd_Update_Reason.setValue("SUCCESSFUL UPDT");                                                                                                                //Natural: ASSIGN #UPDATE-REASON := 'SUCCESSFUL UPDT'
            pnd_Acis_Update_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ACIS-UPDATE-COUNT
        }                                                                                                                                                                 //Natural: VALUE '1111'
        else if (condition((pdaScia9600.getScia9600_Pnd_P_Return_Code().equals("1111"))))
        {
            decideConditionsMet375++;
            pnd_Update_Reason.setValue("NO RECS TO UPDT");                                                                                                                //Natural: ASSIGN #UPDATE-REASON := 'NO RECS TO UPDT'
            pnd_Acis_Reject_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ACIS-REJECT-COUNT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().display(2, "ACTION  ",                                                                                                                               //Natural: DISPLAY ( 2 ) 'ACTION  ' #REC-TYPE-DESC 'PLAN  ' #PLAN 'SUBPLAN' #SUBPLAN 'SSN      ' #SSN 'PIN   ' #PIN 'TIAA CONT.' #TIAA-CONTRACT 'NEW SSN  ' #D-NEW-SSN 'NEW SUBPLAN' #D-NEW-SUBPLAN 'OLD PH665' #D-OLD-DEF-ENRL-IND 'NEW PH665' #D-NEW-DEF-ENRL-IND 'UPDATE REASON' #UPDATE-REASON
        		pnd_Rec_Type_Desc,"PLAN  ",
        		pnd_Omni_Interface_File_Pnd_Plan,"SUBPLAN",
        		pnd_Omni_Interface_File_Pnd_Subplan,"SSN      ",
        		pnd_Omni_Interface_File_Pnd_Ssn,"PIN   ",
        		pnd_Omni_Interface_File_Pnd_Pin,"TIAA CONT.",
        		pnd_Omni_Interface_File_Pnd_Tiaa_Contract,"NEW SSN  ",
        		pnd_D_New_Ssn,"NEW SUBPLAN",
        		pnd_D_New_Subplan,"OLD PH665",
        		pnd_D_Old_Def_Enrl_Ind,"NEW PH665",
        		pnd_D_New_Def_Enrl_Ind,"UPDATE REASON",
        		pnd_Update_Reason);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE // 'CONTRACT:' 25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),annty_Actvty_Prap_View_Ap_Pin_Nbr);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=52");
        Global.format(2, "LS=133 PS=52");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),"REPORT DATE:",new ColumnSpacing(1),Global.getDATU(),"-",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),new TabSetting(50),"OMNI TO MDM INTERFACE REPORT",new TabSetting(103),"PAGE:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZZZ9"),NEWLINE,new TabSetting(1),"NAT PROGRAM:",Global.getPROGRAM(),new TabSetting(57),"AUDIT CONTROL RPT",new TabSetting(103),Global.getINIT_USER(),NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(113));
        getReports().write(2, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),"REPORT DATE:",new ColumnSpacing(1),Global.getDATU(),"-",Global.getTIMX(), 
            new ReportEditMask ("HH:IIAP"),new TabSetting(50),"OMNI TO ACIS UPDATE REPORT",new TabSetting(103),"PAGE:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZZZ9"),NEWLINE,new TabSetting(1),"NAT PROGRAM:",Global.getPROGRAM(),new TabSetting(57),"AUDIT CONTROL RPT",new TabSetting(103),Global.getINIT_USER(),NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(113));

        getReports().setDisplayColumns(1, "ACTION  ",
        		pnd_Rec_Type_Desc,"PLAN  ",
        		pnd_Omni_Interface_File_Pnd_Plan,"SUBPLAN",
        		pnd_Omni_Interface_File_Pnd_Subplan,"SSN      ",
        		pnd_Omni_Interface_File_Pnd_Ssn,"PIN   ",
        		pnd_Omni_Interface_File_Pnd_Pin,"TIAA CONT.",
        		pnd_Omni_Interface_File_Pnd_Tiaa_Contract,"NEW SSN  ",
        		pnd_D_New_Ssn,"NEW SUBPLAN",
        		pnd_D_New_Subplan,"OLD PH665",
        		pnd_D_Old_Def_Enrl_Ind,"NEW PH665",
        		pnd_D_New_Def_Enrl_Ind);
        getReports().setDisplayColumns(2, "ACTION  ",
        		pnd_Rec_Type_Desc,"PLAN  ",
        		pnd_Omni_Interface_File_Pnd_Plan,"SUBPLAN",
        		pnd_Omni_Interface_File_Pnd_Subplan,"SSN      ",
        		pnd_Omni_Interface_File_Pnd_Ssn,"PIN   ",
        		pnd_Omni_Interface_File_Pnd_Pin,"TIAA CONT.",
        		pnd_Omni_Interface_File_Pnd_Tiaa_Contract,"NEW SSN  ",
        		pnd_D_New_Ssn,"NEW SUBPLAN",
        		pnd_D_New_Subplan,"OLD PH665",
        		pnd_D_Old_Def_Enrl_Ind,"NEW PH665",
        		pnd_D_New_Def_Enrl_Ind,"UPDATE REASON",
        		pnd_Update_Reason);
    }
}
