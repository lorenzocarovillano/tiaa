/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:00 PM
**        * FROM NATURAL PROGRAM : Appb1152
************************************************************
**        * FILE NAME            : Appb1152.java
**        * CLASS NAME           : Appb1152
**        * INSTANCE NAME        : Appb1152
************************************************************
************************************************************************
* PROGRAM  : APPB1152                                                  *
* SYSTEM   : ACIS                                                      *
* TITLE    : DIALOGUE FORMAT PROGRAM.                                  *
* FUNCTION : BUILDS DRIVER RECORD, BENE RECORDS, ALLOCATION RECORDS,   *
*            AND FUND RECORDS FOR DIALOG.                              *
*                                                                      *
* ----------  ---------  --------------------------------------------  *
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES                        *
* ----------  ---------  --------------------------------------------  *
* 07/25/2008  DEVELBISS  INITIAL WRITE                                 *
* 04/24/2009  DEVELBISS  ADD SEQ NUMBER TO HANDLE DUPLICATE CONTRACTS  *
*                        - ACIS_EOP_GUID        SEE BJD1               *
* 03/02/2010  DEVELBISS  SEND LEGAL SPLIT ISSUE DATE IN NEW FIELDS.    *
*                        - SEE BJD2                                    *
* 03/15/2010  B.HOLLOWAY TIAA STABLE VALUE PROJECT - SEE DBH1          *
* 03/16/2010  C. AVE     INCLUDED IRA INDEXED PRODUCTS - TIGR          *
* 12/27/2011  BERGHEISER WRITE NEW RECORD TYPES FOR SPECIAL BENE TEXT  *
* 12/29/2011  FRANKLIN   CORRECT CALL TO APPN1154 FROM PREVIOUS RELEASE*
* 03/18/2011  B.ELLO     PASS AGENT INFO. TO DIALOGUE AS PART OF THE
* 05/05/2011  C.SCHNEIDER MOVE THE E-DELIVERY AND ANNUITY PRINT OPTION
*                        INDICATORS FROM THE ACIS PACKAGE EXTRACT FILE
*                        TO THE FILES GOING TO DIALOG            (JHU)
* 07/05/2011  C SCHNEIDER TRANSFER-IN CREDIT (TIC) CHANGES
* 09/20/2011  C SCHNEIDER MOBIUS KEY (CS MOBKEY) SRK CHANGES
* 10/06/2011  DWOLF DEW1 FIX BENE-SPCL-TXT-IND CONDITION FOR 2A & 3A TO
*                        TO ACCOUNT FOR THE POSSIBILTY OF SPACES.
* 06/2012     L SHU      DO NOT PASS CANAD AND FORGN ZIP TO DOCUMERGE
*                                                            - SRK LS
* 10/2012     P GOLDEN   UPDATED #FUND-WRITTEN FROM (N6) TO (N8)
*                        DUE TO AN ABEND - ALSO UPDATED OTHER COUNTERS
* 07/18/13    L SHU      CHANGES TO CATER FOR IRA SUBSTITUTION (TNGSUB)
*                      - ADDED NEW LDA APPA1154 FOR BENE DATA
*                      - POPULATING BUNDLING DATE
*                      - REMOVING / IN BETWEEN ACIS-EOP-US-SOC-SEC
*                      - EXPAND GUID FIELD TO 9 BYTES
*                      - WHEN NO DATA IN ORIG STATE CODE FOR IRA SUBS
*                        WEEKEND, POPULATE IT WITH CURRENT STATE CODE
*                        FOR DCS                               (TNGSUB3)
*                      - SET ACIS-EOP-IRA-INSERT-IND TO Y FOR WEEKEND
*                        IRA SUBSTITUTION ONLY AND N FOR OTHER (TNGSUB4)
* 07/18/13    B NEWSOM   ADDED THE FOLLOWING FIELDS:    (TNGSUB-BWN)
*                      - ACIS-EOP-CNTR-FORM-NUMB
*                      - ACIS-EOP-CERT-FORM-NUMB
*                      - ACIS-EOP-ED-NUMB-T
*                      - ACIS-EOP-ED-NUMB-C
* 08/23/2013  RJF        APPN1154 CHANGED TO PROVIDE TWO ARRAY'S OF
*                        BENEFICIARIES, PRIMARY AND CONTINGENT.
*                        THIS PROGRAM WILL SEND THE PRIMARY TO THE
*                        OUTPUT FIRST THEN THE CONTINGENT.
*                        SEE TAG C291829
* 09/13/2013  L SHU      CHANGES TO CATER FOR MT SINAI TO INCLUDE
*                        NON PROPRIETARY PACKAGE IND            (MTSIN)
* 12/08/2013  B NEWSOM   SEQUENCE NUMBER IS USED BY ETL FOR PROPERLY
*                        PROCESSING AND SPLITING THE FILE BEEN SENT.
*                        THE CHANGE IN THIS PROGRAM IN TO TAKE THE
*                        SEQUENCE FOR A FLOOTING POSITION TO A FIXED
*                        POSITION. (CUSPORT)
* 08/01/2014  B NEWSOM   INVESTMENT MENU BY SUBPLAN (IMBS)
*                        REMOVE 05 RECORDS FROM THE ETL FEED.
* 09/12/2014  B. NEWSOM  ACIS/CIS CREF REDESIGN COMMUNICATIONS   (ACCRC)
*                        IF WELCOME PACKAGE THE MULTIPLAN INDICATOR WILL
*                        BE AN 'N' AND NO PLANS WILL BE SENT IN THE PLAN
*                        ARRAY.  IF LEGAL MULTIPLAN INDICATOR WILL BE
*                        SET BASED ON THE PLAN ARRAY.
* 06/15/17    BARUA      PIN EXPANSION CHANGES. (CHG425939) STOW ONLY
* 01/13/18    L. SHU     ADD TIAA/CREF INITIAL PREMIUM         (ONEIRA2)
* 03/10/18    L. SHU     ADDING BENE INFO TO ONEIRA LEGAL PACKAGE FOR
*                        STATE OF FL                           (1IRAFL)
* 11/17/19    L. SHU     INCLUDE ADDITIONAL FIELDS TPA/IPRO FOR
*                        CONTRACT STRATEGY AND 2ND ALLOCATION FOR IISG
*                        ADDING A NEW REC TYPE 4A FOR IISG
*                                                      (CNTRSTRG & IISG)
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb1152 extends BLNatBase
{
    // Data Areas
    private LdaAppl1152 ldaAppl1152;
    private LdaAppl1153 ldaAppl1153;
    private LdaAppl1154 ldaAppl1154;
    private LdaAppl1155 ldaAppl1155;
    private PdaAppa1154 pdaAppa1154;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Alloc_Category;
    private DbsField pnd_Alloc_Name;
    private DbsField pnd_Alloc_Cnt;
    private DbsField pnd_Alloc_Return_Code;
    private DbsField pnd_Alloc_Return_Msg;
    private DbsField pnd_Alloc_Category_2;
    private DbsField pnd_Alloc_Name_2;
    private DbsField pnd_Alloc_Cnt_2;
    private DbsField pnd_Extr_Read;
    private DbsField pnd_Driver_Written;
    private DbsField pnd_Pbene_Written;
    private DbsField pnd_Cbene_Written;
    private DbsField pnd_Alloc_Written;
    private DbsField pnd_Fund_Written;
    private DbsField pnd_Tot_Written;
    private DbsField pnd_Guid;
    private DbsField pnd_Contract_Type;
    private DbsField pnd_Sub_Na;
    private DbsField pnd_Sub_Addr;
    private DbsField pnd_Sub_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Pos;
    private DbsField pnd_Lz_Cnt;
    private DbsField pnd_Rt_Tic_Limit;
    private DbsField rt_Tic_Limit_A;

    private DbsGroup rt_Tic_Limit_A__R_Field_1;
    private DbsField rt_Tic_Limit_A_Rt_Tic_Limit_N;

    private DbsGroup rt_Tic_Limit_A__R_Field_2;
    private DbsField rt_Tic_Limit_A_Rt_Tic_Limit_T;
    private DbsField pnd_Hold;

    private DbsGroup pnd_Hold__R_Field_3;
    private DbsField pnd_Hold_Pnd_Hold_T;
    private DbsField pnd_Acis_Eop_Tic_Percentage;

    private DbsGroup pnd_Acis_Eop_Tic_Percentage__R_Field_4;
    private DbsField pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop;
    private DbsField pnd_Rt_Tic_Percentage;
    private DbsField rt_Tic_Percentage_A;

    private DbsGroup rt_Tic_Percentage_A__R_Field_5;
    private DbsField rt_Tic_Percentage_A_Rt_Tic_Percentage_N;
    private DbsField pnd_Rt_Tic_Windowdays_A;

    private DbsGroup pnd_Rt_Tic_Windowdays_A__R_Field_6;
    private DbsField pnd_Rt_Tic_Windowdays_A_Pnd_Rt_Tic_Windowdays_N;
    private DbsField pnd_Rt_Tic_Reqdlywindow_A;

    private DbsGroup pnd_Rt_Tic_Reqdlywindow_A__R_Field_7;
    private DbsField pnd_Rt_Tic_Reqdlywindow_A_Pnd_Rt_Tic_Reqdlywindow_N;
    private DbsField pnd_Rt_Tic_Recap_Prov_A;

    private DbsGroup pnd_Rt_Tic_Recap_Prov_A__R_Field_8;
    private DbsField pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N;
    private DbsField pnd_Window;
    private DbsField pnd_Hold_Wd;
    private DbsField pnd_Ppg;
    private DbsField pnd_Print_Type;
    private DbsField pnd_Product;

    private DbsGroup pnd_Product__R_Field_9;
    private DbsField pnd_Product_Pnd_Product_N;
    private DbsField pnd_Work_Addr_Line;

    private DbsGroup pnd_Work_Addr_Line__R_Field_10;
    private DbsField pnd_Work_Addr_Line_Pnd_Work_Addr_Digit;
    private DbsField pnd_Work_Zip;

    private DbsGroup pnd_Work_Zip__R_Field_11;
    private DbsField pnd_Work_Zip_Pnd_Work_Zip_Digit;
    private DbsField pnd_Work_Zip_N;

    private DbsGroup pnd_Work_Zip_N__R_Field_12;
    private DbsField pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N;
    private DbsField pnd_Work_Zip_Check_Digit;

    private DbsGroup pnd_Work_Zip_Check_Digit__R_Field_13;
    private DbsField pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_10;
    private DbsField pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1;

    private DbsGroup pnd_Work_Zip_Check_Digit__R_Field_14;
    private DbsField pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A;
    private DbsField pnd_Date_Format_Field;
    private DbsField pnd_Numeric_Format_Field;

    private DbsGroup pnd_Numeric_Format_Field__R_Field_15;
    private DbsField pnd_Numeric_Format_Field_Pnd_Alpha_Format_Field;
    private DbsField pnd_Hold_Ext_Data_14;

    private DbsGroup pnd_Hold_Ext_Data_14__R_Field_16;
    private DbsField pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_1;
    private DbsField pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_51;
    private DbsField pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_101;
    private DbsField pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_151;
    private DbsField pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_251;
    private DbsField pnd_Seq_No;
    private DbsField pnd_Max_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl1152 = new LdaAppl1152();
        registerRecord(ldaAppl1152);
        ldaAppl1153 = new LdaAppl1153();
        registerRecord(ldaAppl1153);
        ldaAppl1154 = new LdaAppl1154();
        registerRecord(ldaAppl1154);
        ldaAppl1155 = new LdaAppl1155();
        registerRecord(ldaAppl1155);
        localVariables = new DbsRecord();
        pdaAppa1154 = new PdaAppa1154(localVariables);

        // Local Variables
        pnd_Alloc_Category = localVariables.newFieldArrayInRecord("pnd_Alloc_Category", "#ALLOC-CATEGORY", FieldType.STRING, 60, new DbsArrayController(1, 
            100));
        pnd_Alloc_Name = localVariables.newFieldArrayInRecord("pnd_Alloc_Name", "#ALLOC-NAME", FieldType.STRING, 60, new DbsArrayController(1, 100));
        pnd_Alloc_Cnt = localVariables.newFieldInRecord("pnd_Alloc_Cnt", "#ALLOC-CNT", FieldType.NUMERIC, 6);
        pnd_Alloc_Return_Code = localVariables.newFieldInRecord("pnd_Alloc_Return_Code", "#ALLOC-RETURN-CODE", FieldType.NUMERIC, 2);
        pnd_Alloc_Return_Msg = localVariables.newFieldInRecord("pnd_Alloc_Return_Msg", "#ALLOC-RETURN-MSG", FieldType.STRING, 79);
        pnd_Alloc_Category_2 = localVariables.newFieldArrayInRecord("pnd_Alloc_Category_2", "#ALLOC-CATEGORY-2", FieldType.STRING, 60, new DbsArrayController(1, 
            100));
        pnd_Alloc_Name_2 = localVariables.newFieldArrayInRecord("pnd_Alloc_Name_2", "#ALLOC-NAME-2", FieldType.STRING, 60, new DbsArrayController(1, 100));
        pnd_Alloc_Cnt_2 = localVariables.newFieldInRecord("pnd_Alloc_Cnt_2", "#ALLOC-CNT-2", FieldType.NUMERIC, 6);
        pnd_Extr_Read = localVariables.newFieldInRecord("pnd_Extr_Read", "#EXTR-READ", FieldType.NUMERIC, 6);
        pnd_Driver_Written = localVariables.newFieldInRecord("pnd_Driver_Written", "#DRIVER-WRITTEN", FieldType.NUMERIC, 8);
        pnd_Pbene_Written = localVariables.newFieldInRecord("pnd_Pbene_Written", "#PBENE-WRITTEN", FieldType.NUMERIC, 8);
        pnd_Cbene_Written = localVariables.newFieldInRecord("pnd_Cbene_Written", "#CBENE-WRITTEN", FieldType.NUMERIC, 8);
        pnd_Alloc_Written = localVariables.newFieldInRecord("pnd_Alloc_Written", "#ALLOC-WRITTEN", FieldType.NUMERIC, 8);
        pnd_Fund_Written = localVariables.newFieldInRecord("pnd_Fund_Written", "#FUND-WRITTEN", FieldType.NUMERIC, 8);
        pnd_Tot_Written = localVariables.newFieldInRecord("pnd_Tot_Written", "#TOT-WRITTEN", FieldType.NUMERIC, 8);
        pnd_Guid = localVariables.newFieldInRecord("pnd_Guid", "#GUID", FieldType.NUMERIC, 9);
        pnd_Contract_Type = localVariables.newFieldInRecord("pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 6);
        pnd_Sub_Na = localVariables.newFieldInRecord("pnd_Sub_Na", "#SUB-NA", FieldType.NUMERIC, 1);
        pnd_Sub_Addr = localVariables.newFieldInRecord("pnd_Sub_Addr", "#SUB-ADDR", FieldType.NUMERIC, 2);
        pnd_Sub_Cnt = localVariables.newFieldInRecord("pnd_Sub_Cnt", "#SUB-CNT", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 10);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 10);
        pnd_Pos = localVariables.newFieldInRecord("pnd_Pos", "#POS", FieldType.NUMERIC, 3);
        pnd_Lz_Cnt = localVariables.newFieldInRecord("pnd_Lz_Cnt", "#LZ-CNT", FieldType.NUMERIC, 6);
        pnd_Rt_Tic_Limit = localVariables.newFieldInRecord("pnd_Rt_Tic_Limit", "#RT-TIC-LIMIT", FieldType.STRING, 14);
        rt_Tic_Limit_A = localVariables.newFieldInRecord("rt_Tic_Limit_A", "RT-TIC-LIMIT-A", FieldType.STRING, 11);

        rt_Tic_Limit_A__R_Field_1 = localVariables.newGroupInRecord("rt_Tic_Limit_A__R_Field_1", "REDEFINE", rt_Tic_Limit_A);
        rt_Tic_Limit_A_Rt_Tic_Limit_N = rt_Tic_Limit_A__R_Field_1.newFieldInGroup("rt_Tic_Limit_A_Rt_Tic_Limit_N", "RT-TIC-LIMIT-N", FieldType.NUMERIC, 
            11, 2);

        rt_Tic_Limit_A__R_Field_2 = localVariables.newGroupInRecord("rt_Tic_Limit_A__R_Field_2", "REDEFINE", rt_Tic_Limit_A);
        rt_Tic_Limit_A_Rt_Tic_Limit_T = rt_Tic_Limit_A__R_Field_2.newFieldArrayInGroup("rt_Tic_Limit_A_Rt_Tic_Limit_T", "RT-TIC-LIMIT-T", FieldType.STRING, 
            1, new DbsArrayController(1, 11));
        pnd_Hold = localVariables.newFieldInRecord("pnd_Hold", "#HOLD", FieldType.STRING, 11);

        pnd_Hold__R_Field_3 = localVariables.newGroupInRecord("pnd_Hold__R_Field_3", "REDEFINE", pnd_Hold);
        pnd_Hold_Pnd_Hold_T = pnd_Hold__R_Field_3.newFieldArrayInGroup("pnd_Hold_Pnd_Hold_T", "#HOLD-T", FieldType.STRING, 1, new DbsArrayController(1, 
            11));
        pnd_Acis_Eop_Tic_Percentage = localVariables.newFieldInRecord("pnd_Acis_Eop_Tic_Percentage", "#ACIS-EOP-TIC-PERCENTAGE", FieldType.STRING, 6);

        pnd_Acis_Eop_Tic_Percentage__R_Field_4 = localVariables.newGroupInRecord("pnd_Acis_Eop_Tic_Percentage__R_Field_4", "REDEFINE", pnd_Acis_Eop_Tic_Percentage);
        pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop = pnd_Acis_Eop_Tic_Percentage__R_Field_4.newFieldArrayInGroup("pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop", 
            "#ACIS-EOP", FieldType.STRING, 1, new DbsArrayController(1, 6));
        pnd_Rt_Tic_Percentage = localVariables.newFieldInRecord("pnd_Rt_Tic_Percentage", "#RT-TIC-PERCENTAGE", FieldType.STRING, 10);
        rt_Tic_Percentage_A = localVariables.newFieldInRecord("rt_Tic_Percentage_A", "RT-TIC-PERCENTAGE-A", FieldType.STRING, 9);

        rt_Tic_Percentage_A__R_Field_5 = localVariables.newGroupInRecord("rt_Tic_Percentage_A__R_Field_5", "REDEFINE", rt_Tic_Percentage_A);
        rt_Tic_Percentage_A_Rt_Tic_Percentage_N = rt_Tic_Percentage_A__R_Field_5.newFieldInGroup("rt_Tic_Percentage_A_Rt_Tic_Percentage_N", "RT-TIC-PERCENTAGE-N", 
            FieldType.NUMERIC, 9, 6);
        pnd_Rt_Tic_Windowdays_A = localVariables.newFieldInRecord("pnd_Rt_Tic_Windowdays_A", "#RT-TIC-WINDOWDAYS-A", FieldType.STRING, 3);

        pnd_Rt_Tic_Windowdays_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Rt_Tic_Windowdays_A__R_Field_6", "REDEFINE", pnd_Rt_Tic_Windowdays_A);
        pnd_Rt_Tic_Windowdays_A_Pnd_Rt_Tic_Windowdays_N = pnd_Rt_Tic_Windowdays_A__R_Field_6.newFieldInGroup("pnd_Rt_Tic_Windowdays_A_Pnd_Rt_Tic_Windowdays_N", 
            "#RT-TIC-WINDOWDAYS-N", FieldType.NUMERIC, 3);
        pnd_Rt_Tic_Reqdlywindow_A = localVariables.newFieldInRecord("pnd_Rt_Tic_Reqdlywindow_A", "#RT-TIC-REQDLYWINDOW-A", FieldType.STRING, 3);

        pnd_Rt_Tic_Reqdlywindow_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Rt_Tic_Reqdlywindow_A__R_Field_7", "REDEFINE", pnd_Rt_Tic_Reqdlywindow_A);
        pnd_Rt_Tic_Reqdlywindow_A_Pnd_Rt_Tic_Reqdlywindow_N = pnd_Rt_Tic_Reqdlywindow_A__R_Field_7.newFieldInGroup("pnd_Rt_Tic_Reqdlywindow_A_Pnd_Rt_Tic_Reqdlywindow_N", 
            "#RT-TIC-REQDLYWINDOW-N", FieldType.NUMERIC, 3);
        pnd_Rt_Tic_Recap_Prov_A = localVariables.newFieldInRecord("pnd_Rt_Tic_Recap_Prov_A", "#RT-TIC-RECAP-PROV-A", FieldType.STRING, 3);

        pnd_Rt_Tic_Recap_Prov_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Rt_Tic_Recap_Prov_A__R_Field_8", "REDEFINE", pnd_Rt_Tic_Recap_Prov_A);
        pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N = pnd_Rt_Tic_Recap_Prov_A__R_Field_8.newFieldInGroup("pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N", 
            "#RT-TIC-RECAP-PROV-N", FieldType.NUMERIC, 3);
        pnd_Window = localVariables.newFieldInRecord("pnd_Window", "#WINDOW", FieldType.NUMERIC, 9);
        pnd_Hold_Wd = localVariables.newFieldInRecord("pnd_Hold_Wd", "#HOLD-WD", FieldType.STRING, 3);
        pnd_Ppg = localVariables.newFieldInRecord("pnd_Ppg", "#PPG", FieldType.STRING, 6);
        pnd_Print_Type = localVariables.newFieldInRecord("pnd_Print_Type", "#PRINT-TYPE", FieldType.STRING, 1);
        pnd_Product = localVariables.newFieldInRecord("pnd_Product", "#PRODUCT", FieldType.STRING, 3);

        pnd_Product__R_Field_9 = localVariables.newGroupInRecord("pnd_Product__R_Field_9", "REDEFINE", pnd_Product);
        pnd_Product_Pnd_Product_N = pnd_Product__R_Field_9.newFieldInGroup("pnd_Product_Pnd_Product_N", "#PRODUCT-N", FieldType.NUMERIC, 3);
        pnd_Work_Addr_Line = localVariables.newFieldInRecord("pnd_Work_Addr_Line", "#WORK-ADDR-LINE", FieldType.STRING, 35);

        pnd_Work_Addr_Line__R_Field_10 = localVariables.newGroupInRecord("pnd_Work_Addr_Line__R_Field_10", "REDEFINE", pnd_Work_Addr_Line);
        pnd_Work_Addr_Line_Pnd_Work_Addr_Digit = pnd_Work_Addr_Line__R_Field_10.newFieldArrayInGroup("pnd_Work_Addr_Line_Pnd_Work_Addr_Digit", "#WORK-ADDR-DIGIT", 
            FieldType.STRING, 1, new DbsArrayController(1, 35));
        pnd_Work_Zip = localVariables.newFieldInRecord("pnd_Work_Zip", "#WORK-ZIP", FieldType.STRING, 5);

        pnd_Work_Zip__R_Field_11 = localVariables.newGroupInRecord("pnd_Work_Zip__R_Field_11", "REDEFINE", pnd_Work_Zip);
        pnd_Work_Zip_Pnd_Work_Zip_Digit = pnd_Work_Zip__R_Field_11.newFieldArrayInGroup("pnd_Work_Zip_Pnd_Work_Zip_Digit", "#WORK-ZIP-DIGIT", FieldType.STRING, 
            1, new DbsArrayController(1, 5));
        pnd_Work_Zip_N = localVariables.newFieldInRecord("pnd_Work_Zip_N", "#WORK-ZIP-N", FieldType.NUMERIC, 5);

        pnd_Work_Zip_N__R_Field_12 = localVariables.newGroupInRecord("pnd_Work_Zip_N__R_Field_12", "REDEFINE", pnd_Work_Zip_N);
        pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N = pnd_Work_Zip_N__R_Field_12.newFieldArrayInGroup("pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N", "#WORK-ZIP-DIGIT-N", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Work_Zip_Check_Digit = localVariables.newFieldInRecord("pnd_Work_Zip_Check_Digit", "#WORK-ZIP-CHECK-DIGIT", FieldType.NUMERIC, 2);

        pnd_Work_Zip_Check_Digit__R_Field_13 = localVariables.newGroupInRecord("pnd_Work_Zip_Check_Digit__R_Field_13", "REDEFINE", pnd_Work_Zip_Check_Digit);
        pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_10 = pnd_Work_Zip_Check_Digit__R_Field_13.newFieldInGroup("pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_10", 
            "#WORK-ZIP-CHECK-DIGIT-10", FieldType.NUMERIC, 1);
        pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1 = pnd_Work_Zip_Check_Digit__R_Field_13.newFieldInGroup("pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1", 
            "#WORK-ZIP-CHECK-DIGIT-1", FieldType.NUMERIC, 1);

        pnd_Work_Zip_Check_Digit__R_Field_14 = pnd_Work_Zip_Check_Digit__R_Field_13.newGroupInGroup("pnd_Work_Zip_Check_Digit__R_Field_14", "REDEFINE", 
            pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1);
        pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A = pnd_Work_Zip_Check_Digit__R_Field_14.newFieldInGroup("pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A", 
            "#WORK-ZIP-CHECK-DIGIT-A", FieldType.STRING, 1);
        pnd_Date_Format_Field = localVariables.newFieldInRecord("pnd_Date_Format_Field", "#DATE-FORMAT-FIELD", FieldType.DATE);
        pnd_Numeric_Format_Field = localVariables.newFieldInRecord("pnd_Numeric_Format_Field", "#NUMERIC-FORMAT-FIELD", FieldType.NUMERIC, 8);

        pnd_Numeric_Format_Field__R_Field_15 = localVariables.newGroupInRecord("pnd_Numeric_Format_Field__R_Field_15", "REDEFINE", pnd_Numeric_Format_Field);
        pnd_Numeric_Format_Field_Pnd_Alpha_Format_Field = pnd_Numeric_Format_Field__R_Field_15.newFieldInGroup("pnd_Numeric_Format_Field_Pnd_Alpha_Format_Field", 
            "#ALPHA-FORMAT-FIELD", FieldType.STRING, 8);
        pnd_Hold_Ext_Data_14 = localVariables.newFieldInRecord("pnd_Hold_Ext_Data_14", "#HOLD-EXT-DATA-14", FieldType.STRING, 250);

        pnd_Hold_Ext_Data_14__R_Field_16 = localVariables.newGroupInRecord("pnd_Hold_Ext_Data_14__R_Field_16", "REDEFINE", pnd_Hold_Ext_Data_14);
        pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_1 = pnd_Hold_Ext_Data_14__R_Field_16.newFieldInGroup("pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_1", "#HOLD-EXT-1", FieldType.STRING, 
            50);
        pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_51 = pnd_Hold_Ext_Data_14__R_Field_16.newFieldInGroup("pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_51", "#HOLD-EXT-51", 
            FieldType.STRING, 50);
        pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_101 = pnd_Hold_Ext_Data_14__R_Field_16.newFieldInGroup("pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_101", "#HOLD-EXT-101", 
            FieldType.STRING, 50);
        pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_151 = pnd_Hold_Ext_Data_14__R_Field_16.newFieldInGroup("pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_151", "#HOLD-EXT-151", 
            FieldType.STRING, 50);
        pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_251 = pnd_Hold_Ext_Data_14__R_Field_16.newFieldInGroup("pnd_Hold_Ext_Data_14_Pnd_Hold_Ext_251", "#HOLD-EXT-251", 
            FieldType.STRING, 50);
        pnd_Seq_No = localVariables.newFieldInRecord("pnd_Seq_No", "#SEQ-NO", FieldType.NUMERIC, 15);
        pnd_Max_Count = localVariables.newFieldInRecord("pnd_Max_Count", "#MAX-COUNT", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl1152.initializeValues();
        ldaAppl1153.initializeValues();
        ldaAppl1154.initializeValues();
        ldaAppl1155.initializeValues();

        localVariables.reset();
        pnd_Y.setInitialValue(12);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb1152() throws Exception
    {
        super("Appb1152");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* **********************************************************************
        //*  MAIN LOGIC SECTION                                                  *
        //* **********************************************************************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD RECORD-ID-EX-FILE
        while (condition(getWorkFiles().read(1, ldaAppl1152.getRecord_Id_Ex_File())))
        {
            pnd_Extr_Read.nadd(1);                                                                                                                                        //Natural: ASSIGN #EXTR-READ := #EXTR-READ + 1
            ldaAppl1153.getAcis_Driver_Record().reset();                                                                                                                  //Natural: RESET ACIS-DRIVER-RECORD
                                                                                                                                                                          //Natural: PERFORM MOVE-DRIVER-DATA
            sub_Move_Driver_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type().equals("W")))                                                                          //Natural: IF RT-PRINT-PACKAGE-TYPE = 'W'
            {
                                                                                                                                                                          //Natural: PERFORM GET-BENE-DATA
                sub_Get_Bene_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Estate_As_Bene().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Estate_As_Bene());                              //Natural: ASSIGN ACIS-EOP-ESTATE-AS-BENE := #BENE-ESTATE-AS-BENE
                ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Trust_As_Bene().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Trust_As_Bene());                                //Natural: ASSIGN ACIS-EOP-TRUST-AS-BENE := #BENE-TRUST-AS-BENE
                ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Beneficiary_Category().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Category());                              //Natural: ASSIGN ACIS-EOP-BENEFICIARY-CATEGORY := #BENE-CATEGORY
                //*  IMBS
                                                                                                                                                                          //Natural: PERFORM GET-ALLOC-DATA
                sub_Get_Alloc_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  IISG
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Fund_Source_2().notEquals(" ")))                                                                        //Natural: IF RT-FUND-SOURCE-2 NOT = ' '
                {
                    //*  IISG
                                                                                                                                                                          //Natural: PERFORM GET-ALLOC-DATA-2
                    sub_Get_Alloc_Data_2();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  IISG
                }                                                                                                                                                         //Natural: END-IF
                //*    PERFORM GET-FUND-DATA
                //*  1IRAFL >>>
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  LEGAL AND FL
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type().equals("L")))                                                                      //Natural: IF RT-PRINT-PACKAGE-TYPE = 'L'
                {
                    if (condition(((((((ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA001") || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA002"))  //Natural: IF ( RT-SG-PLAN-NO = 'IRA001' OR = 'IRA002' OR = 'IRA008' OR = 'IRA111' OR = 'IRA222' OR = 'IRA888' ) AND ( RT-CURRENT-ISS-STATE = 'FL' )
                        || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA008")) || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA111")) 
                        || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA222")) || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA888")) 
                        && ldaAppl1152.getRecord_Id_Ex_File_Rt_Current_Iss_State().equals("FL"))))
                    {
                                                                                                                                                                          //Natural: PERFORM GET-BENE-DATA
                        sub_Get_Bene_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Estate_As_Bene().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Estate_As_Bene());                      //Natural: ASSIGN ACIS-EOP-ESTATE-AS-BENE := #BENE-ESTATE-AS-BENE
                        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Trust_As_Bene().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Trust_As_Bene());                        //Natural: ASSIGN ACIS-EOP-TRUST-AS-BENE := #BENE-TRUST-AS-BENE
                        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Beneficiary_Category().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Category());                      //Natural: ASSIGN ACIS-EOP-BENEFICIARY-CATEGORY := #BENE-CATEGORY
                        //*  IF ONEIRA AND FL
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF LEGAL                           /* 1IRAFL <<<
                }                                                                                                                                                         //Natural: END-IF
                //*   IF WELCOME
            }                                                                                                                                                             //Natural: END-IF
            //*  BJD1
            //*  BJD1
            pnd_Guid.nadd(+1);                                                                                                                                            //Natural: ADD +1 TO #GUID
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Guid().setValue(pnd_Guid);                                                                                         //Natural: ASSIGN ACIS_EOP_GUID := #GUID
            //* CUSPORT
            pnd_Seq_No.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SEQ-NO
            ldaAppl1153.getAcis_Driver_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                                                //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
            getWorkFiles().write(2, true, ldaAppl1153.getAcis_Driver_Record());                                                                                           //Natural: WRITE WORK FILE 2 VARIABLE ACIS-DRIVER-RECORD
            pnd_Driver_Written.nadd(+1);                                                                                                                                  //Natural: ADD +1 TO #DRIVER-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
            //*  TNGSUB START
            if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type().equals("W")))                                                                          //Natural: IF RT-PRINT-PACKAGE-TYPE = 'W'
            {
                if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().greater(getZero())))                                                                       //Natural: IF #BENE-PRMRY-CNT > 0
                {
                    FOR01:                                                                                                                                                //Natural: FOR #I = 1 TO #BENE-PRMRY-CNT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt())); pnd_I.nadd(1))
                    {
                        ldaAppl1154.getAcis_Bene_Record_Acis_Bene_Record_Id().setValue(ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Record_Id());                           //Natural: ASSIGN ACIS-BENE-RECORD-ID := ACIS-EOP-RECORD-ID
                        if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Mos_Ind().equals("Y")))                                                                        //Natural: IF #BENE-MOS-IND = 'Y'
                        {
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Record_Type().setValue("2B");                                                                        //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-RECORD-TYPE := '2B'
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Spcl_Dsgn_Txt().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt().getValue(pnd_I)); //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-BENE-SPCL-DSGN-TXT := #BENE-SPCL-DSGN-TXT ( #I )
                            //*  CUSPORT
                            pnd_Seq_No.nadd(1);                                                                                                                           //Natural: ADD 1 TO #SEQ-NO
                            ldaAppl1154.getAcis_Bene_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                                  //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                            getWorkFiles().write(2, true, ldaAppl1154.getAcis_Bene_Record());                                                                             //Natural: WRITE WORK FILE 2 VARIABLE ACIS-BENE-RECORD
                            pnd_Pbene_Written.nadd(+1);                                                                                                                   //Natural: ADD +1 TO #PBENE-WRITTEN
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  C291829
                            //*  C291829
                            //*  C291829
                            //*  C291829
                            //*  C291829
                            //*  C291829
                            if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind().getValue(pnd_I).notEquals("Y")))                                       //Natural: IF #BENE-PRMRY-STD-TXT-IND ( #I ) NOT = 'Y'
                            {
                                ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Record_Type().setValue("02");                                                                    //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-RECORD-TYPE := '02'
                                ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Name().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(pnd_I));         //Natural: ASSIGN ACIS-EOP-BENE-NAME := #BENE-PRMRY-NAME ( #I )
                                ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Ssn().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Ssn().getValue(pnd_I));           //Natural: ASSIGN ACIS-EOP-BENE-SSN := #BENE-PRMRY-SSN ( #I )
                                ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Dob().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Dob().getValue(pnd_I));           //Natural: ASSIGN ACIS-EOP-BENE-DOB := #BENE-PRMRY-DOB ( #I )
                                ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Relationship().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Relationship().getValue(pnd_I)); //Natural: ASSIGN ACIS-EOP-BENE-RELATIONSHIP := #BENE-PRMRY-RELATIONSHIP ( #I )
                                //*  CUSPORT
                                pnd_Seq_No.nadd(1);                                                                                                                       //Natural: ADD 1 TO #SEQ-NO
                                ldaAppl1154.getAcis_Bene_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                              //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                                getWorkFiles().write(2, true, ldaAppl1154.getAcis_Bene_Record());                                                                         //Natural: WRITE WORK FILE 2 VARIABLE ACIS-BENE-RECORD
                                pnd_Pbene_Written.nadd(+1);                                                                                                               //Natural: ADD +1 TO #PBENE-WRITTEN
                                //*  (2450)
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Record_Type().setValue("2A");                                                                    //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-RECORD-TYPE := '2A'
                                FOR02:                                                                                                                                    //Natural: FOR #J = 1 TO 3
                                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(3)); pnd_J.nadd(1))
                                {
                                    //*  C291829
                                    //*  C291829
                                    if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt().getValue(pnd_I,pnd_J).greater(" ")))                              //Natural: IF #BENE-PRMRY-SPCL-TXT ( #I,#J ) > ' '
                                    {
                                        ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Spcl_Txt().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt().getValue(pnd_I, //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-BENE-SPCL-TXT := #BENE-PRMRY-SPCL-TXT ( #I,#J )
                                            pnd_J));
                                        //*  CUSPORT
                                        pnd_Seq_No.nadd(1);                                                                                                               //Natural: ADD 1 TO #SEQ-NO
                                        ldaAppl1154.getAcis_Bene_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                      //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                                        getWorkFiles().write(2, true, ldaAppl1154.getAcis_Bene_Record());                                                                 //Natural: WRITE WORK FILE 2 VARIABLE ACIS-BENE-RECORD
                                        pnd_Pbene_Written.nadd(+1);                                                                                                       //Natural: ADD +1 TO #PBENE-WRITTEN
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*  (2450) IF #BENE-PRMRY-STD-TXT-IND    /* C291829
                            }                                                                                                                                             //Natural: END-IF
                            //*  (2360) IF #BENE-MOS-IND
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (2340) FOR #BENE-PRMRY-CNT
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  (2330) IF #BENE-PRMRY-CNT > 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt().greater(getZero())))                                                                      //Natural: IF #BENE-CNTGNT-CNT > 0
                {
                    FOR03:                                                                                                                                                //Natural: FOR #I = 1 TO #BENE-CNTGNT-CNT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt())); pnd_I.nadd(1))
                    {
                        ldaAppl1154.getAcis_Bene_Record_Acis_Bene_Record_Id().setValue(ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Record_Id());                           //Natural: ASSIGN ACIS-BENE-RECORD-ID := ACIS-EOP-RECORD-ID
                        //*  C291829
                        //*  C291829
                        //*  C291829
                        //*  C291829
                        //*  C291829
                        //*  C291829
                        if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind().getValue(pnd_I).notEquals("Y")))                                          //Natural: IF #BENE-CNTGNT-STD-TXT-IND ( #I ) NOT = 'Y'
                        {
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Record_Type().setValue("03");                                                                        //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-RECORD-TYPE := '03'
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Name().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Name().getValue(pnd_I));            //Natural: ASSIGN ACIS-EOP-BENE-NAME := #BENE-CNTGNT-NAME ( #I )
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Ssn().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Ssn().getValue(pnd_I));              //Natural: ASSIGN ACIS-EOP-BENE-SSN := #BENE-CNTGNT-SSN ( #I )
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Dob().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Dob().getValue(pnd_I));              //Natural: ASSIGN ACIS-EOP-BENE-DOB := #BENE-CNTGNT-DOB ( #I )
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Relationship().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Relationship().getValue(pnd_I)); //Natural: ASSIGN ACIS-EOP-BENE-RELATIONSHIP := #BENE-CNTGNT-RELATIONSHIP ( #I )
                            //*  CUSPORT
                            pnd_Seq_No.nadd(1);                                                                                                                           //Natural: ADD 1 TO #SEQ-NO
                            ldaAppl1154.getAcis_Bene_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                                  //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                            getWorkFiles().write(2, true, ldaAppl1154.getAcis_Bene_Record());                                                                             //Natural: WRITE WORK FILE 2 VARIABLE ACIS-BENE-RECORD
                            pnd_Cbene_Written.nadd(+1);                                                                                                                   //Natural: ADD +1 TO #CBENE-WRITTEN
                            //*  (2760)
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Record_Type().setValue("3A");                                                                        //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-RECORD-TYPE := '3A'
                            FOR04:                                                                                                                                        //Natural: FOR #J = 1 TO 3
                            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(3)); pnd_J.nadd(1))
                            {
                                //*  C291829
                                //*  C291829
                                if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt().getValue(pnd_I,pnd_J).greater(" ")))                                 //Natural: IF #BENE-CNTGNT-SPCL-TXT ( #I,#J ) > ' '
                                {
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Spcl_Txt().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt().getValue(pnd_I, //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-BENE-SPCL-TXT := #BENE-CNTGNT-SPCL-TXT ( #I,#J )
                                        pnd_J));
                                    //* CUSPORT
                                    pnd_Seq_No.nadd(1);                                                                                                                   //Natural: ADD 1 TO #SEQ-NO
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                          //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                                    getWorkFiles().write(2, true, ldaAppl1154.getAcis_Bene_Record());                                                                     //Natural: WRITE WORK FILE 2 VARIABLE ACIS-BENE-RECORD
                                    pnd_Cbene_Written.nadd(+1);                                                                                                           //Natural: ADD +1 TO #CBENE-WRITTEN
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  (2760) IF #BENE-CNTGNT-STD-TXT-IND   /* C291829
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  TNGSUB
                }                                                                                                                                                         //Natural: END-IF
                FOR05:                                                                                                                                                    //Natural: FOR #I = 1 TO 100
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
                {
                    //*  TNGSUB
                    if (condition(pnd_Alloc_Name.getValue(pnd_I).notEquals(" ")))                                                                                         //Natural: IF #ALLOC-NAME ( #I ) NE ' '
                    {
                        ldaAppl1155.getAcis_Alloc_Record_Acis_Alloc_Record_Id().setValue(ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Record_Id());                         //Natural: ASSIGN ACIS-ALLOC-RECORD-ID := ACIS-EOP-RECORD-ID
                        ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Record_Type().setValue("04");                                                                           //Natural: ASSIGN ACIS-ALLOC-RECORD.ACIS-EOP-RECORD-TYPE := '04'
                        ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Asset_Cl_Name().setValue(pnd_Alloc_Category.getValue(pnd_I));                                           //Natural: ASSIGN ACIS-EOP-ASSET-CL-NAME := #ALLOC-CATEGORY ( #I )
                        ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Fund_Name().setValue(pnd_Alloc_Name.getValue(pnd_I));                                                   //Natural: ASSIGN ACIS-EOP-FUND-NAME := #ALLOC-NAME ( #I )
                        ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Alloc_Pct().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Alloc_Pct().getValue(pnd_I));                  //Natural: ASSIGN ACIS-EOP-ALLOC-PCT := RT-ALLOC-PCT ( #I )
                        //*  CUSPORT
                        pnd_Seq_No.nadd(1);                                                                                                                               //Natural: ADD 1 TO #SEQ-NO
                        ldaAppl1155.getAcis_Alloc_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                                     //Natural: ASSIGN ACIS-ALLOC-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                        getWorkFiles().write(2, true, ldaAppl1155.getAcis_Alloc_Record());                                                                                //Natural: WRITE WORK FILE 2 VARIABLE ACIS-ALLOC-RECORD
                        pnd_Alloc_Written.nadd(+1);                                                                                                                       //Natural: ADD +1 TO #ALLOC-WRITTEN
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  IISG START >>>
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Fund_Source_2().notEquals(" ")))                                                                        //Natural: IF RT-FUND-SOURCE-2 NOT = ' '
                {
                    FOR06:                                                                                                                                                //Natural: FOR #I = 1 TO 100
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_Alloc_Name_2.getValue(pnd_I).notEquals(" ")))                                                                                   //Natural: IF #ALLOC-NAME-2 ( #I ) NE ' '
                        {
                            ldaAppl1155.getAcis_Alloc_Record_Acis_Alloc_Record_Id().setValue(ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Record_Id());                     //Natural: ASSIGN ACIS-ALLOC-RECORD-ID := ACIS-EOP-RECORD-ID
                            ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Record_Type().setValue("4A");                                                                       //Natural: ASSIGN ACIS-ALLOC-RECORD.ACIS-EOP-RECORD-TYPE := '4A'
                            ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Asset_Cl_Name().setValue(pnd_Alloc_Category_2.getValue(pnd_I));                                     //Natural: ASSIGN ACIS-EOP-ASSET-CL-NAME := #ALLOC-CATEGORY-2 ( #I )
                            ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Fund_Name().setValue(pnd_Alloc_Name_2.getValue(pnd_I));                                             //Natural: ASSIGN ACIS-EOP-FUND-NAME := #ALLOC-NAME-2 ( #I )
                            ldaAppl1155.getAcis_Alloc_Record_Acis_Eop_Alloc_Pct().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Alloc_Pct_2().getValue(pnd_I));            //Natural: ASSIGN ACIS-EOP-ALLOC-PCT := RT-ALLOC-PCT-2 ( #I )
                            pnd_Seq_No.nadd(1);                                                                                                                           //Natural: ADD 1 TO #SEQ-NO
                            ldaAppl1155.getAcis_Alloc_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                                 //Natural: ASSIGN ACIS-ALLOC-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                            getWorkFiles().write(2, true, ldaAppl1155.getAcis_Alloc_Record());                                                                            //Natural: WRITE WORK FILE 2 VARIABLE ACIS-ALLOC-RECORD
                            pnd_Alloc_Written.nadd(+1);                                                                                                                   //Natural: ADD +1 TO #ALLOC-WRITTEN
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  IISG   END <<<
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #FUND-CNT > 0                                           /* IMBS
                //*      FOR #I = 1 TO #FUND-CNT                                  /* IMBS
                //*        ACIS-FUND-RECORD-ID   := ACIS-EOP-RECORD-ID            /* IMBS
                //*        ACIS-FUND-RECORD.ACIS-EOP-RECORD-TYPE := '05'          /* IMBS
                //*        ACIS-EOP-PPG-ASSET-CL-NME := #FUND-CATEGORY(#I)        /* IMBS
                //*        ACIS-EOP-PPG-FUND-NUM   := #FUND-FUND-NUM-CDE(#I)      /* IMBS
                //*        ACIS-EOP-PPG-FUND-NAME  := #FUND-FUND-NAME(#I)         /* IMBS
                //*        ADD 1 TO SEQ-NO                                        /* IMBS
                //*        ACIS-FUND-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO       /* IMBS
                //*        WRITE WORK FILE 2 VARIABLE ACIS-FUND-RECORD            /* IMBS
                //*        ADD +1 TO #FUND-WRITTEN                                /* IMBS
                //*      END-FOR                                                  /* IMBS
                //*    END-IF                                                     /* IMBS
                //*  PROCESS FOR LEGAL AND FL                 /* 1IRAFL >>>
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  LEGAL AND FL
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type().equals("L")))                                                                      //Natural: IF RT-PRINT-PACKAGE-TYPE = 'L'
                {
                    if (condition(((((((ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA001") || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA002"))  //Natural: IF ( RT-SG-PLAN-NO = 'IRA001' OR = 'IRA002' OR = 'IRA008' OR = 'IRA111' OR = 'IRA222' OR = 'IRA888' ) AND ( RT-CURRENT-ISS-STATE = 'FL' )
                        || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA008")) || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA111")) 
                        || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA222")) || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA888")) 
                        && ldaAppl1152.getRecord_Id_Ex_File_Rt_Current_Iss_State().equals("FL"))))
                    {
                        if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt().greater(getZero())))                                                               //Natural: IF #BENE-PRMRY-CNT > 0
                        {
                            FOR07:                                                                                                                                        //Natural: FOR #I = 1 TO #BENE-PRMRY-CNT
                            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt())); pnd_I.nadd(1))
                            {
                                ldaAppl1154.getAcis_Bene_Record_Acis_Bene_Record_Id().setValue(ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Record_Id());                   //Natural: ASSIGN ACIS-BENE-RECORD-ID := ACIS-EOP-RECORD-ID
                                if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Mos_Ind().notEquals("Y") && pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind().getValue(pnd_I).notEquals("Y"))) //Natural: IF #BENE-MOS-IND NOT = 'Y' AND #BENE-PRMRY-STD-TXT-IND ( #I ) NOT = 'Y'
                                {
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Record_Type().setValue("02");                                                                //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-EOP-RECORD-TYPE := '02'
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Name().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(pnd_I));     //Natural: ASSIGN ACIS-EOP-BENE-NAME := #BENE-PRMRY-NAME ( #I )
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Ssn().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Ssn().getValue(pnd_I));       //Natural: ASSIGN ACIS-EOP-BENE-SSN := #BENE-PRMRY-SSN ( #I )
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Dob().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Dob().getValue(pnd_I));       //Natural: ASSIGN ACIS-EOP-BENE-DOB := #BENE-PRMRY-DOB ( #I )
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Eop_Bene_Relationship().setValue(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Relationship().getValue(pnd_I)); //Natural: ASSIGN ACIS-EOP-BENE-RELATIONSHIP := #BENE-PRMRY-RELATIONSHIP ( #I )
                                    pnd_Seq_No.nadd(1);                                                                                                                   //Natural: ADD 1 TO #SEQ-NO
                                    ldaAppl1154.getAcis_Bene_Record_Acis_Dialog_Sequence().setValue(pnd_Seq_No);                                                          //Natural: ASSIGN ACIS-BENE-RECORD.ACIS-DIALOG-SEQUENCE := #SEQ-NO
                                    getWorkFiles().write(2, true, ldaAppl1154.getAcis_Bene_Record());                                                                     //Natural: WRITE WORK FILE 2 VARIABLE ACIS-BENE-RECORD
                                    pnd_Pbene_Written.nadd(+1);                                                                                                           //Natural: ADD +1 TO #PBENE-WRITTEN
                                    //*  NOT MOS AND NOT STD TEXT  FOR FL LEGAL
                                }                                                                                                                                         //Natural: END-IF
                                //*  (2340) FOR #BENE-PRMRY-CNT     FOR FL LEGAL
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  (2330) IF #BENE-PRMRY-CNT > 0   FOR FL LEGAL
                        }                                                                                                                                                 //Natural: END-IF
                        //*   1IRA PLAN AND FLORIDA
                    }                                                                                                                                                     //Natural: END-IF
                    //*   IF LEGAL                          /* 1IRAFL <<<
                }                                                                                                                                                         //Natural: END-IF
                //*   IF WELCOME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Extr_Read.equals(getZero())))                                                                                                                   //Natural: IF #EXTR-READ = 0
        {
            getReports().display(0, "********  EXTRACT FILE IS EMPTY ********");                                                                                          //Natural: DISPLAY '********  EXTRACT FILE IS EMPTY ********'
            if (Global.isEscape()) return;
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "EXTRACT RECORDS READ:            ",pnd_Extr_Read,NEWLINE,"DRIVER RECORDS WRITTEN:          ",pnd_Tot_Written,NEWLINE,                  //Natural: WRITE 'EXTRACT RECORDS READ:            ' #EXTR-READ / 'DRIVER RECORDS WRITTEN:          ' #TOT-WRITTEN / 'PRIMARY BENE RECORDS WRITTEN:    ' #PBENE-WRITTEN / 'CONTINGENT BENE RECORDS WRITTEN: ' #CBENE-WRITTEN / 'ALLOCATION RECORDS WRITTEN:      ' #ALLOC-WRITTEN / 'FUND RECORDS WRITTEN:            ' #FUND-WRITTEN /
                "PRIMARY BENE RECORDS WRITTEN:    ",pnd_Pbene_Written,NEWLINE,"CONTINGENT BENE RECORDS WRITTEN: ",pnd_Cbene_Written,NEWLINE,"ALLOCATION RECORDS WRITTEN:      ",
                pnd_Alloc_Written,NEWLINE,"FUND RECORDS WRITTEN:            ",pnd_Fund_Written,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DRIVER-DATA
        //* *--------------------------------------------------------------------**
        //*                                                         TNGSUB START
        //*  START TNGSUB-BWN
        //*  END TNGSUB-BWN
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-ZIP-POSITION
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-ZIP
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BENE-DATA
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALLOC-DATA
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ALLOC-DATA-2
        //* *--------------------------------------------------------------------**
        //*  DEFINE SUBROUTINE GET-FUND-DATA
        //* *--------------------------------------------------------------------**
        //*  IF RT-INST-CODE = 'SGRD'
        //*    PERFORM GET-OMNI-FUND-DATA
        //*  ELSE
        //*    PERFORM GET-LEGACY-FUND-DATA
        //*  END-IF
        //*  END-SUBROUTINE   /* GET-FUND-DATA     /* IMBS
        //* *--------------------------------------------------------------------**
        //*  DEFINE SUBROUTINE GET-LEGACY-FUND-DATA                 /* IMBS
        //* *--------------------------------------------------------------------**
        //*  #PPG := RT-INST-CODE
        //*  #PRODUCT := RT-PRODUCT-CDE
        //*  RESET #FUND-CATEGORY(*)
        //*    #FUND-FUND-NUM-CDE(*)
        //*    #FUND-FUND-NAME(*)
        //*    #FUND-CNT
        //*    #FUND-RETURN-CODE
        //*    #FUND-RETURN-MSG
        //*  CALLNAT 'APPN1156'
        //*    #PPG
        //*    #PRODUCT-N
        //*    #FUND-CATEGORY(*)
        //*    #FUND-FUND-NUM-CDE(*)
        //*    #FUND-FUND-NAME(*)
        //*    #FUND-CNT
        //*    #FUND-RETURN-CODE
        //*    #FUND-RETURN-MSG
        //*  IF #FUND-RETURN-CODE > 0
        //*    WRITE 'ERROR FROM FUND - APPN1156' #FUND-RETURN-CODE
        //*    WRITE #FUND-RETURN-MSG
        //*    TERMINATE #FUND-RETURN-CODE
        //*  END-IF
        //*  END-SUBROUTINE   /* GET-LEGACY-FUND-DATA  IMBS
        //* *--------------------------------------------------------------------**
        //*  DEFINE SUBROUTINE GET-OMNI-FUND-DATA  /* IMBS
        //* *--------------------------------------------------------------------**
        //*  RESET #FUND-CATEGORY(*)
        //*    #FUND-FUND-NUM-CDE(*)
        //*    #FUND-FUND-NAME(*)
        //*    #FUND-CNT
        //*    #FUND-RETURN-CODE
        //*    #FUND-RETURN-MSG
        //*  IF RT-REPRINT-REQUEST-TYPE = ' '
        //*    #PRINT-TYPE := 'O'
        //*  ELSE
        //*    #PRINT-TYPE := 'R'
        //*  END-IF
        //*  CALLNAT 'APPN1157'
        //*    #PRINT-TYPE
        //*    RT-SG-PLAN-NO
        //*    #FUND-CATEGORY(*)
        //*    #FUND-FUND-NUM-CDE(*)
        //*    #FUND-FUND-NAME(*)
        //*    #FUND-CNT
        //*    #FUND-RETURN-CODE
        //*    #FUND-RETURN-MSG
        //*  IF #FUND-RETURN-CODE > 0
        //*    WRITE 'ERROR FROM FUND - APPN1157' #FUND-RETURN-CODE
        //*    WRITE #FUND-RETURN-MSG
        //*    TERMINATE #FUND-RETURN-CODE
        //*  END-IF
        //*  END-SUBROUTINE   /* GET-OMNI-FUND-DATA     IMBS
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TIC-FIELDS
        //* *--------------------------------------------------------------------**
        //* *--------------------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-TIC-LIMIT
    }
    private void sub_Move_Driver_Data() throws Exception                                                                                                                  //Natural: MOVE-DRIVER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Record_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Hd_Record_Type());                                             //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-RECORD-TYPE := HD-RECORD-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Output_Profile().setValue(ldaAppl1152.getRecord_Id_Ex_File_Hd_Output_Profile());                                       //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-OUTPUT-PROFILE := HD-OUTPUT-PROFILE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Bundle_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Bundle_Type());                                             //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-BUNDLE-TYPE := RT-BUNDLE-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Us_Soc_Sec().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Soc_Sec());                                                  //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-US-SOC-SEC := RT-SOC-SEC
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Print_Package_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type());                               //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-PRINT-PACKAGE-TYPE := RT-PRINT-PACKAGE-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Product_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Hd_Product_Type());                                           //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-PRODUCT-TYPE := HD-PRODUCT-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Reprint_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Reprint_Request_Type());                                   //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-REPRINT-TYPE := RT-REPRINT-REQUEST-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Cref_Both().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Type_Cd());                                              //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-TIAA-CREF-BOTH := RT-TYPE-CD
        //*                                                      /* TNGSUB END
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_No_1().equals(" ")))                                                                                       //Natural: IF RT-TIAA-NO-1 = ' '
        {
            setValueToSubstring("NONE",ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_1(),2,4);                                                                //Natural: MOVE 'NONE' TO SUBSTR ( ACIS-DRIVER-RECORD.ACIS-EOP-TIAA-CNTR-NUMB-1,2,4 )
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  TNGSUB
            //*  TNGSUB
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_No_1(),  //Natural: COMPRESS RT-TIAA-NO-1 RT-TIAA-FILL-1 INTO ACIS-DRIVER-RECORD.ACIS-EOP-TIAA-CNTR-NUMB-1 LEAVING NO
                ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_Fill_1()));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_No_1().equals(" ")))                                                                                       //Natural: IF RT-CREF-NO-1 = ' '
        {
            setValueToSubstring("NONE",ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Cert_Numb_1(),2,4);                                                                //Natural: MOVE 'NONE' TO SUBSTR ( ACIS-DRIVER-RECORD.ACIS-EOP-CREF-CERT-NUMB-1,2,4 )
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  TNGSUB
            //*  TNGSUB
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Cert_Numb_1().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_No_1(),  //Natural: COMPRESS RT-CREF-NO-1 RT-CREF-FILL-1 INTO ACIS-DRIVER-RECORD.ACIS-EOP-CREF-CERT-NUMB-1 LEAVING NO
                ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Fill_1()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Cntr_Numb_2().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_Cntr_2());                                        //Natural: ASSIGN ACIS-EOP-TIAA-CNTR-NUMB-2 := RT-TIAA-CNTR-2
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Cert_Numb_2().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Cert_2());                                        //Natural: ASSIGN ACIS-EOP-CREF-CERT-NUMB-2 := RT-CREF-CERT-2
        //*  TNG SUB
        //*  FOR RL CREF #
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Cert_4().greater(" ")))                                                                                    //Natural: IF RT-CREF-CERT-4 > ' '
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Cert_Numb_4().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Cert_4());                                    //Natural: ASSIGN ACIS-EOP-CREF-CERT-NUMB-4 := RT-CREF-CERT-4
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Bundle_Number().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Bundle_Number());                                         //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-BUNDLE-NUMBER := RT-BUNDLE-NUMBER
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_1().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cl_Tiaa_Contract_1());                               //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-CL-TIAA-CONTRACT-1 := RT-CL-TIAA-CONTRACT-1
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_2().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cl_Tiaa_Contract_2());                               //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-CL-TIAA-CONTRACT-2 := RT-CL-TIAA-CONTRACT-2
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_3().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cl_Tiaa_Contract_3());                               //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-CL-TIAA-CONTRACT-3 := RT-CL-TIAA-CONTRACT-3
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cl_Tiaa_Contract_4().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cl_Tiaa_Contract_4());                               //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-CL-TIAA-CONTRACT-4 := RT-CL-TIAA-CONTRACT-4
        short decideConditionsMet1302 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF RT-ZIP;//Natural: VALUE 'CANAD'
        if (condition((ldaAppl1152.getRecord_Id_Ex_File_Rt_Zip().equals("CANAD"))))
        {
            decideConditionsMet1302++;
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Address_Type_Code().setValue("C");                                                                                 //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-ADDRESS-TYPE-CODE := 'C'
        }                                                                                                                                                                 //Natural: VALUE 'FORGN'
        else if (condition((ldaAppl1152.getRecord_Id_Ex_File_Rt_Zip().equals("FORGN"))))
        {
            decideConditionsMet1302++;
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Address_Type_Code().setValue("F");                                                                                 //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-ADDRESS-TYPE-CODE := 'F'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Address_Type_Code().setValue("U");                                                                                 //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-ADDRESS-TYPE-CODE := 'U'
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Substitution_Contract_Ind().equals(" ")))                                                                       //Natural: IF RT-SUBSTITUTION-CONTRACT-IND = ' '
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Substitution_Ind().setValue("N");                                                                                  //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-SUBSTITUTION-IND := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Substitution_Ind().setValue("Y");                                                                                  //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-SUBSTITUTION-IND := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  TNGSUB2 LS
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Bundle_Record_Type().equals("M")))                                                                              //Natural: IF RT-BUNDLE-RECORD-TYPE = 'M'
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Bundle_Record_Type().setValue("Y");                                                                                //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-BUNDLE-RECORD-TYPE := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Bundle_Record_Type().setValue("N");                                                                                //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-BUNDLE-RECORD-TYPE := 'N'
            //*  TNGSUB2 LS
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                          TNGSUB END
        //*  BJD2
        //*  BJD2
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Appl_Source().equals("L")))                                                                                     //Natural: IF RT-APPL-SOURCE = 'L'
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Issue_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Effective_Dt());                                    //Natural: ASSIGN ACIS-EOP-TIAA-ISSUE-DATE := RT-EFFECTIVE-DT
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Issue_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Effective_Dt());                                    //Natural: ASSIGN ACIS-EOP-CREF-ISSUE-DATE := RT-EFFECTIVE-DT
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Effective_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Effective_Dt());                                     //Natural: ASSIGN ACIS-EOP-EFFECTIVE-DATE := RT-EFFECTIVE-DT
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Issue_Date_Ls().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(1));                       //Natural: ASSIGN ACIS-EOP-TIAA-ISSUE-DATE-LS := RT-DATE-TABLE ( 1 )
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Issue_Date_Ls().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(2));                       //Natural: ASSIGN ACIS-EOP-CREF-ISSUE-DATE-LS := RT-DATE-TABLE ( 2 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Issue_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(1));                          //Natural: ASSIGN ACIS-EOP-TIAA-ISSUE-DATE := RT-DATE-TABLE ( 1 )
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Issue_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(2));                          //Natural: ASSIGN ACIS-EOP-CREF-ISSUE-DATE := RT-DATE-TABLE ( 2 )
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Effective_Date().setValue(" ");                                                                                    //Natural: ASSIGN ACIS-EOP-EFFECTIVE-DATE := ' '
            //*  TNGSUB2
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Annuity_Start_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(3));                      //Natural: ASSIGN ACIS-EOP-TIAA-ANNUITY-START-DATE := RT-DATE-TABLE ( 3 )
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Annuity_Start_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(4));                      //Natural: ASSIGN ACIS-EOP-CREF-ANNUITY-START-DATE := RT-DATE-TABLE ( 4 )
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Dob().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(5));                                          //Natural: ASSIGN ACIS-EOP-DOB := RT-DATE-TABLE ( 5 )
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Dt_App_Recvd().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Date_Table().getValue(6));                                 //Natural: ASSIGN ACIS-EOP-DT-APP-RECVD := RT-DATE-TABLE ( 6 )
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Request().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Addl_Cref_Req());                                      //Natural: ASSIGN ACIS-EOP-CREF-REQUEST := RT-EOP-ADDL-CREF-REQ
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Us_Soc_Sec().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Soc_Sec());                                                  //Natural: ASSIGN ACIS-DRIVER-RECORD.ACIS-EOP-US-SOC-SEC := RT-SOC-SEC
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Pin_Numb().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Pin_No());                                                     //Natural: ASSIGN ACIS-EOP-PIN-NUMB := RT-PIN-NO
        FOR08:                                                                                                                                                            //Natural: FOR #SUB-NA = 6 TO 1 STEP -1
        for (pnd_Sub_Na.setValue(6); condition(pnd_Sub_Na.greaterOrEqual(1)); pnd_Sub_Na.nsubtract(1))
        {
            if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Address_Table().getValue(pnd_Sub_Na).notEquals(" ")))                                                       //Natural: IF RT-ADDRESS-TABLE ( #SUB-NA ) NE ' '
            {
                pnd_Work_Addr_Line.setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Address_Table().getValue(pnd_Sub_Na));                                                    //Natural: ASSIGN #WORK-ADDR-LINE := RT-ADDRESS-TABLE ( #SUB-NA )
                //*  SRK LS
                //*  SRK LS
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode().equals("CANAD") || ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode().equals("FORGN")))    //Natural: IF RT-EOP-ZIPCODE = 'CANAD' OR = 'FORGN'
                {
                    ignore();
                    //*  SRK LS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM FIND-ZIP-POSITION
                    sub_Find_Zip_Position();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  SRK LS
                }                                                                                                                                                         //Natural: END-IF
                pnd_Sub_Na.setValue(0);                                                                                                                                   //Natural: MOVE 0 TO #SUB-NA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Each_Name_Line().getValue("*").setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Address_Table().getValue("*"));            //Natural: ASSIGN ACIS-EOP-EACH-NAME-LINE ( * ) := RT-ADDRESS-TABLE ( * )
        if (condition(DbsUtil.maskMatches(ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode(),"NNNNN") && ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode().notEquals("00000"))) //Natural: IF RT-EOP-ZIPCODE = MASK ( NNNNN ) AND RT-EOP-ZIPCODE NE '00000'
        {
            pnd_Work_Zip_Check_Digit.setValue(0);                                                                                                                         //Natural: ASSIGN #WORK-ZIP-CHECK-DIGIT := 0
            pnd_Work_Zip_N.compute(new ComputeParameters(false, pnd_Work_Zip_N), ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode().val());                                //Natural: ASSIGN #WORK-ZIP-N := VAL ( RT-EOP-ZIPCODE )
            FOR09:                                                                                                                                                        //Natural: FOR #I = 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                pnd_Work_Zip_Check_Digit.nadd(pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N.getValue(pnd_I));                                                                       //Natural: ADD #WORK-ZIP-DIGIT-N ( #I ) TO #WORK-ZIP-CHECK-DIGIT
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1.notEquals(getZero())))                                                                      //Natural: IF #WORK-ZIP-CHECK-DIGIT-1 NE 0
            {
                pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1.compute(new ComputeParameters(false, pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1),            //Natural: ASSIGN #WORK-ZIP-CHECK-DIGIT-1 := 10 - #WORK-ZIP-CHECK-DIGIT-1
                    DbsField.subtract(10,pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1));
            }                                                                                                                                                             //Natural: END-IF
            //*  TNGSUB2
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Post_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode(),  //Natural: COMPRESS RT-EOP-ZIPCODE '0000' #WORK-ZIP-CHECK-DIGIT-A INTO ACIS-EOP-POST-ZIP LEAVING NO SPACE
                "0000", pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Code().equals("SGRD") && ldaAppl1152.getRecord_Id_Ex_File_Hd_Product_Type().getSubstring(1,                //Natural: IF RT-INST-CODE = 'SGRD' AND SUBSTR ( HD-PRODUCT-TYPE,1,3 ) = 'IRA'
            3).equals("IRA")))
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Inst_Name().setValue(" ");                                                                                         //Natural: ASSIGN ACIS-EOP-INST-NAME := ' '
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Employer_Name().setValue(" ");                                                                                     //Natural: ASSIGN ACIS-EOP-EMPLOYER-NAME := ' '
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Inst_Name().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Name());                                             //Natural: ASSIGN ACIS-EOP-INST-NAME := RT-INST-NAME
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Employer_Name().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Employer_Name());                                     //Natural: ASSIGN ACIS-EOP-EMPLOYER-NAME := RT-EMPLOYER-NAME
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Each_Ia_Line().getValue("*").setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Address_Table().getValue("*"));         //Natural: ASSIGN ACIS-EOP-EACH-IA-LINE ( * ) := RT-INST-ADDRESS-TABLE ( * )
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Inst_Zip().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Zip_Cd());                                                //Natural: ASSIGN ACIS-EOP-INST-ZIP := RT-INST-ZIP-CD
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Current_Issue_State().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Current_Iss_State());                               //Natural: ASSIGN ACIS-EOP-CURRENT-ISSUE-STATE := RT-CURRENT-ISS-STATE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Lob().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Lob());                                                             //Natural: ASSIGN ACIS-EOP-LOB := RT-LOB
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Lob_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Lob_Type());                                                   //Natural: ASSIGN ACIS-EOP-LOB-TYPE := RT-LOB-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Name_State().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_State_Name());                                           //Natural: ASSIGN ACIS-EOP-NAME-STATE := RT-EOP-STATE-NAME
        DbsUtil.examine(new ExamineSource(ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Name_State(),true), new ExamineSearch("."), new ExamineReplace(" "));                //Natural: EXAMINE FULL ACIS-EOP-NAME-STATE '.' REPLACE WITH ' '
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Institution_Code().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Code());                                          //Natural: ASSIGN ACIS-EOP-INSTITUTION-CODE := RT-INST-CODE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Rollover_Amt().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Rollover_Amt());                                           //Natural: ASSIGN ACIS-EOP-ROLLOVER-AMT := RT-ROLLOVER-AMT
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Mail_Date().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Mail_Date());                                                 //Natural: ASSIGN ACIS-EOP-MAIL-DATE := RT-MAIL-DATE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Unit_Name().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Unit_Name());                                                 //Natural: ASSIGN ACIS-EOP-UNIT-NAME := RT-UNIT-NAME
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Email_Address().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Email_Address());                                         //Natural: ASSIGN ACIS-EOP-EMAIL-ADDRESS := RT-EMAIL-ADDRESS
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Acct_Sum_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Acct_Sum_Sheet_Type());                                   //Natural: ASSIGN ACIS-EOP-ACCT-SUM-TYPE := RT-ACCT-SUM-SHEET-TYPE
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Code().equals("SGRD") || (ldaAppl1152.getRecord_Id_Ex_File_Rt_Appl_Source().equals("L")                    //Natural: IF RT-INST-CODE = 'SGRD' OR ( RT-APPL-SOURCE = 'L' AND HD-PRODUCT-TYPE = 'RL' )
            && ldaAppl1152.getRecord_Id_Ex_File_Hd_Product_Type().equals("RL"))))
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Group_Contract_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No(),  //Natural: COMPRESS RT-SG-PLAN-NO RT-SG-SUBPLAN-NO INTO ACIS-EOP-GROUP-CONTRACT-NAME LEAVING NO SPACE
                ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Subplan_No()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Group_Contract_Name().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Code());                                   //Natural: ASSIGN ACIS-EOP-GROUP-CONTRACT-NAME := RT-INST-CODE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Access_Code().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Dflt_Access_Code());                                        //Natural: ASSIGN ACIS-EOP-ACCESS-CODE := RT-DFLT-ACCESS-CODE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Ownership_Code().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Ownership());                                            //Natural: ASSIGN ACIS-EOP-OWNERSHIP-CODE := RT-OWNERSHIP
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Alloc_Descrepancy().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Alloc_Disc());                                        //Natural: ASSIGN ACIS-EOP-ALLOC-DESCREPANCY := RT-ALLOC-DISC
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Participant_Status().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Participant_Status());                               //Natural: ASSIGN ACIS-EOP-PARTICIPANT-STATUS := RT-PARTICIPANT-STATUS
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Portfolio_Selected().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Portfolio_Selected());                               //Natural: ASSIGN ACIS-EOP-PORTFOLIO-SELECTED := RT-PORTFOLIO-SELECTED
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Divorce_Contract().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Divorce_Contract());                                   //Natural: ASSIGN ACIS-EOP-DIVORCE-CONTRACT := RT-DIVORCE-CONTRACT
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Access_Cd_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Access_Cd_Ind());                                         //Natural: ASSIGN ACIS-EOP-ACCESS-CD-IND := RT-ACCESS-CD-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Mailing_Instructions().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Mail_Instructions());                              //Natural: ASSIGN ACIS-EOP-MAILING-INSTRUCTIONS := RT-MAIL-INSTRUCTIONS
        //*  TNGSUB3 LS
        //*  TNGSUB3 LS
        //*  TNGSUB3 LS
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Substitution_Contract_Ind().equals("W") && ldaAppl1152.getRecord_Id_Ex_File_Rt_Orig_Iss_State().equals(" ")))   //Natural: IF RT-SUBSTITUTION-CONTRACT-IND = 'W' AND RT-ORIG-ISS-STATE = ' '
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Original_Issue_State().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Current_Iss_State());                          //Natural: ASSIGN ACIS-EOP-ORIGINAL-ISSUE-STATE := RT-CURRENT-ISS-STATE
            //*  TNGSUB3 LS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Original_Issue_State().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Orig_Iss_State());                             //Natural: ASSIGN ACIS-EOP-ORIGINAL-ISSUE-STATE := RT-ORIG-ISS-STATE
            //*  TNGSUB3 LS
        }                                                                                                                                                                 //Natural: END-IF
        //*  TNGSUB4 LS
        //*  TNGSUB4 LS
        //*  TNGSUB4 LS
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Substitution_Contract_Ind().equals("W") && ldaAppl1152.getRecord_Id_Ex_File_Rt_Reprint_Request_Type().equals(" "))) //Natural: IF RT-SUBSTITUTION-CONTRACT-IND = 'W' AND RT-REPRINT-REQUEST-TYPE = ' '
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Ira_Insert_Ind().setValue("Y");                                                                                    //Natural: ASSIGN ACIS-EOP-IRA-INSERT-IND := 'Y'
            //*  TNGSUB4 LS
            //*  TNGSUB4 LS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Ira_Insert_Ind().setValue("N");                                                                                    //Natural: ASSIGN ACIS-EOP-IRA-INSERT-IND := 'N'
            //*  TNGSUB4 LS
            //*  FROM EXTR
            //*  FROM EXTR
            //*  FROM EXTR
            //*  FROM EXTR
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Application_Source().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Appl_Source());                                      //Natural: ASSIGN ACIS-EOP-APPLICATION-SOURCE := RT-APPL-SOURCE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Gsra_Loan_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Gsra_Loan_Ind());                                         //Natural: ASSIGN ACIS-EOP-GSRA-LOAN-IND := RT-GSRA-LOAN-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Spec_Ppg_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Spec_Ppg_Ind());                                           //Natural: ASSIGN ACIS-EOP-SPEC-PPG-IND := RT-SPEC-PPG-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Mult_App_Version().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Mult_App_Version());                                   //Natural: ASSIGN ACIS-EOP-MULT-APP-VERSION := RT-MULT-APP-VERSION
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Mult_App_Status().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Mult_App_Status());                                     //Natural: ASSIGN ACIS-EOP-MULT-APP-STATUS := RT-MULT-APP-STATUS
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Mult_App_Lob().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Mult_App_Lob());                                           //Natural: ASSIGN ACIS-EOP-MULT-APP-LOB := RT-MULT-APP-LOB
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Mult_App_Lob_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Mult_App_Lob_Type());                                 //Natural: ASSIGN ACIS-EOP-MULT-APP-LOB-TYPE := RT-MULT-APP-LOB-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Ira_Record_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Ira_Record_Type());                                     //Natural: ASSIGN ACIS-EOP-IRA-RECORD-TYPE := RT-IRA-RECORD-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Enroll_Req_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Enroll_Request_Type());                                 //Natural: ASSIGN ACIS-EOP-ENROLL-REQ-TYPE := RT-ENROLL-REQUEST-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Erisa_Inst().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Erisa_Inst());                                               //Natural: ASSIGN ACIS-EOP-ERISA-INST := RT-ERISA-INST
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Each_Correction_Type().getValue("*").setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Correction_Type().getValue("*"));    //Natural: ASSIGN ACIS-EOP-EACH-CORRECTION-TYPE ( * ) := RT-CORRECTION-TYPE ( * )
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Oia_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Oia_Ind());                                                     //Natural: ASSIGN ACIS-EOP-OIA-IND := RT-OIA-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Sg_Plan_No().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No());                                               //Natural: ASSIGN ACIS-EOP-SG-PLAN-NO := RT-SG-PLAN-NO
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Sg_Subplan_No().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Subplan_No());                                         //Natural: ASSIGN ACIS-EOP-SG-SUBPLAN-NO := RT-SG-SUBPLAN-NO
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Sg_Subplan_Type().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Subplan_Type());                                     //Natural: ASSIGN ACIS-EOP-SG-SUBPLAN-TYPE := RT-SG-SUBPLAN-TYPE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Omni_Issue().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Omni_Issue_Ind());                                           //Natural: ASSIGN ACIS-EOP-OMNI-ISSUE := RT-OMNI-ISSUE-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Single_Issue_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Single_Issue_Ind());                                   //Natural: ASSIGN ACIS-EOP-SINGLE-ISSUE-IND := RT-SINGLE-ISSUE-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Spec_Fund_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Spec_Fund_Ind());                                         //Natural: ASSIGN ACIS-EOP-SPEC-FUND-IND := RT-SPEC-FUND-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Product_Price_Level().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Product_Price_Level());                             //Natural: ASSIGN ACIS-EOP-PRODUCT-PRICE-LEVEL := RT-PRODUCT-PRICE-LEVEL
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Roth_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Roth_Ind());                                                   //Natural: ASSIGN ACIS-EOP-ROTH-IND := RT-ROTH-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Plan_Issue_State().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Plan_Issue_State());                                   //Natural: ASSIGN ACIS-EOP-PLAN-ISSUE-STATE := RT-PLAN-ISSUE-STATE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Irc_Sectn_Cde().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Irc_Sectn_Cde());                                         //Natural: ASSIGN ACIS-EOP-IRC-SECTN-CDE := RT-IRC-SECTN-CDE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Client_Id().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Client_Id());                                                 //Natural: ASSIGN ACIS-EOP-CLIENT-ID := RT-CLIENT-ID
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Portfolio_Model().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Portfolio_Model());                                     //Natural: ASSIGN ACIS-EOP-PORTFOLIO-MODEL := RT-PORTFOLIO-MODEL
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_As_Cur_Dflt_Amt().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_As_Cur_Dflt_Amt());                                     //Natural: ASSIGN ACIS-EOP-AS-CUR-DFLT-AMT := RT-AS-CUR-DFLT-AMT
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_As_Incr_Amt().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_As_Incr_Amt());                                             //Natural: ASSIGN ACIS-EOP-AS-INCR-AMT := RT-AS-INCR-AMT
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_As_Max_Pct().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_As_Max_Pct());                                               //Natural: ASSIGN ACIS-EOP-AS-MAX-PCT := RT-AS-MAX-PCT
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Ae_Opt_Out_Days().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Ae_Opt_Out_Days());                                     //Natural: ASSIGN ACIS-EOP-AE-OPT-OUT-DAYS := RT-AE-OPT-OUT-DAYS
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cntr_Form_Numb().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_Extr_Form());                                       //Natural: ASSIGN ACIS-EOP-CNTR-FORM-NUMB := RT-TIAA-EXTR-FORM
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cert_Form_Numb().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Extr_Form());                                       //Natural: ASSIGN ACIS-EOP-CERT-FORM-NUMB := RT-CREF-EXTR-FORM
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Ed_Numb_T().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_Extr_Ed());                                              //Natural: ASSIGN ACIS-EOP-ED-NUMB-T := RT-TIAA-EXTR-ED
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Ed_Numb_C().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Extr_Ed());                                              //Natural: ASSIGN ACIS-EOP-ED-NUMB-C := RT-CREF-EXTR-ED
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Plan_Name().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Name_Table().getValue(1));                               //Natural: ASSIGN ACIS-EOP-PLAN-NAME := RT-INST-NAME-TABLE ( 1 )
        setValueToSubstring(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Name_Table().getValue(2),ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Plan_Name(),31,                  //Natural: MOVE RT-INST-NAME-TABLE ( 2 ) TO SUBSTR ( ACIS-EOP-PLAN-NAME,31,30 )
            30);
        //*  DBH1
        //*  TIGR
        //*  B.E. FLORIDA
        //*  B.E. FLORIDA
        //*  B.E. FLORIDA
        //*  B.E. FLORIDA
        //*  B.E. FLORIDA
        //*  B.E. FLORIDA
        //*  B.E. FLORIDA
        //*  B.E. FLORIDA
        //*  CS JHU
        //*  CS MOBKEY
        //*  CS MOBKEY
        //*  CS MOBKEY
        //*  CS MOBKEY
        //*  CS MOBKEY
        setValueToSubstring(ldaAppl1152.getRecord_Id_Ex_File_Rt_Inst_Name_Table().getValue(3).getSubstring(1,4),ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Plan_Name(),   //Natural: MOVE SUBSTR ( RT-INST-NAME-TABLE ( 3 ) ,1,4 ) TO SUBSTR ( ACIS-EOP-PLAN-NAME,61,4 )
            61,4);
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Auto_Enroll_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Auto_Enroll_Ind());                                     //Natural: ASSIGN ACIS-EOP-AUTO-ENROLL-IND := RT-AUTO-ENROLL-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Auto_Save_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Auto_Save_Ind());                                         //Natural: ASSIGN ACIS-EOP-AUTO-SAVE-IND := RT-AUTO-SAVE-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Tsv_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tsv_Ind());                                                         //Natural: ASSIGN ACIS-TSV-IND := RT-TSV-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Indx_Guarntd_Rte().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_Indx_Guarntd_Rte());                         //Natural: ASSIGN ACIS-EOP-TIAA-INDX-GUARNTD-RTE := RT-TIAA-INDX-GUARNTD-RTE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Crd_Pull_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Crd_Pull_Ind());                                           //Natural: ASSIGN ACIS-EOP-CRD-PULL-IND := RT-CRD-PULL-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Agent_Name().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Agent_Name());                                               //Natural: ASSIGN ACIS-EOP-AGENT-NAME := RT-AGENT-NAME
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Agent_Work_Addr1().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Agent_Work_Addr1());                                   //Natural: ASSIGN ACIS-EOP-AGENT-WORK-ADDR1 := RT-AGENT-WORK-ADDR1
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Agent_Work_Addr2().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Agent_Work_Addr2());                                   //Natural: ASSIGN ACIS-EOP-AGENT-WORK-ADDR2 := RT-AGENT-WORK-ADDR2
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Agent_Work_City().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Agent_Work_City());                                     //Natural: ASSIGN ACIS-EOP-AGENT-WORK-CITY := RT-AGENT-WORK-CITY
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Agent_Work_State().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Agent_Work_State());                                   //Natural: ASSIGN ACIS-EOP-AGENT-WORK-STATE := RT-AGENT-WORK-STATE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Agent_Work_Zip().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Agent_Work_Zip());                                       //Natural: ASSIGN ACIS-EOP-AGENT-WORK-ZIP := RT-AGENT-WORK-ZIP
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Agent_Work_Phone().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Agent_Work_Phone());                                   //Natural: ASSIGN ACIS-EOP-AGENT-WORK-PHONE := RT-AGENT-WORK-PHONE
        ldaAppl1153.getAcis_Driver_Record_Acis_Ecs_Dcs_Annty_Optn_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Ecs_Dcs_Annty_Optn_Ind());                           //Natural: ASSIGN ACIS-ECS-DCS-ANNTY-OPTN-IND := RT-ECS-DCS-ANNTY-OPTN-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Name_Prefix_Lg().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Addr_Pref());                                            //Natural: ASSIGN ACIS-EOP-NAME-PREFIX-LG := RT-ADDR-PREF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Name_Last_Lg().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Addr_Last());                                              //Natural: ASSIGN ACIS-EOP-NAME-LAST-LG := RT-ADDR-LAST
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Name_First_Lg().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Addr_Frst());                                             //Natural: ASSIGN ACIS-EOP-NAME-FIRST-LG := RT-ADDR-FRST
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Name_Middle_Lg().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Full_Middle_Name());                                     //Natural: ASSIGN ACIS-EOP-NAME-MIDDLE-LG := RT-FULL-MIDDLE-NAME
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Name_Suffix_Lg().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Addr_Sffx());                                            //Natural: ASSIGN ACIS-EOP-NAME-SUFFIX-LG := RT-ADDR-SFFX
        //*   TIC
        if (condition((ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type().equals("L") && (ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Ind().equals("Y")                 //Natural: IF RT-PRINT-PACKAGE-TYPE = 'L' AND ( RT-TIC-IND = 'Y' OR = 'y' )
            || ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Ind().equals("y")))))
        {
            //*   TIC
                                                                                                                                                                          //Natural: PERFORM MOVE-TIC-FIELDS
            sub_Move_Tic_Fields();
            if (condition(Global.isEscape())) {return;}
            //*   TIC
            //*  MTSIN
            //*  ACCRC
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Non_Proprietary_Pkg_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Non_Proprietary_Pkg_Ind());                     //Natural: ASSIGN ACIS-EOP-NON-PROPRIETARY-PKG-IND := RT-NON-PROPRIETARY-PKG-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Multi_Plan_Ind().setValue("N");                                                                                            //Natural: ASSIGN ACIS-MULTI-PLAN-IND := 'N'
        //*  ACCRC
        if (condition(ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Print_Package_Type().notEquals("W")))                                                                    //Natural: IF ACIS-DRIVER-RECORD.ACIS-EOP-PRINT-PACKAGE-TYPE NOT = 'W'
        {
            //*  ACCRC
            //*  ACCRC
            if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Multi_Plan_No().getValue(2).greater(" ")))                                                                  //Natural: IF RT-MULTI-PLAN-NO ( 2 ) > ' '
            {
                ldaAppl1153.getAcis_Driver_Record_Acis_Multi_Plan_Ind().setValue("Y");                                                                                    //Natural: ASSIGN ACIS-MULTI-PLAN-IND := 'Y'
                //*  ACCRC
                //*  ACCRC
                //*  ACCRC
            }                                                                                                                                                             //Natural: END-IF
            ldaAppl1153.getAcis_Driver_Record_Acis_Multi_Plan_No().getValue("*").setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Multi_Plan_No().getValue("*"));             //Natural: ASSIGN ACIS-MULTI-PLAN-NO ( * ) := RT-MULTI-PLAN-NO ( * )
            ldaAppl1153.getAcis_Driver_Record_Acis_Multi_Sub_Plan().getValue("*").setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Multi_Sub_Plan().getValue("*"));           //Natural: ASSIGN ACIS-MULTI-SUB-PLAN ( * ) := RT-MULTI-SUB-PLAN ( * )
            //*  ACCRC
        }                                                                                                                                                                 //Natural: END-IF
        //*  >>>  ONEIRA2
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type().equals("L")))                                                                              //Natural: IF RT-PRINT-PACKAGE-TYPE = 'L'
        {
            if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA001") || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA002")          //Natural: IF ( RT-SG-PLAN-NO = 'IRA001' OR = 'IRA002' OR = 'IRA008' OR = 'IRA111' OR = 'IRA222' OR = 'IRA888' )
                || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA008") || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA111") 
                || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA222") || ldaAppl1152.getRecord_Id_Ex_File_Rt_Sg_Plan_No().equals("IRA888")))
            {
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_Init_Prem().greater(getZero())))                                                                   //Natural: IF RT-TIAA-INIT-PREM > 0
                {
                    ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Init_Prem().setValueEdited(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_Init_Prem(),new                   //Natural: MOVE EDITED RT-TIAA-INIT-PREM ( EM = ZZZZZZZ9.99 ) TO ACIS-EOP-TIAA-INIT-PREM
                        ReportEditMask("ZZZZZZZ9.99"));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tiaa_Init_Prem().setValue("0.00");                                                                         //Natural: MOVE '0.00' TO ACIS-EOP-TIAA-INIT-PREM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Init_Prem().greater(getZero())))                                                                   //Natural: IF RT-CREF-INIT-PREM > 0
                {
                    ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Init_Prem().setValueEdited(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_Init_Prem(),new                   //Natural: MOVE EDITED RT-CREF-INIT-PREM ( EM = ZZZZZZZ9.99 ) TO ACIS-EOP-CREF-INIT-PREM
                        ReportEditMask("ZZZZZZZ9.99"));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Cref_Init_Prem().setValue("0.00");                                                                         //Natural: MOVE '0.00' TO ACIS-EOP-CREF-INIT-PREM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  <<<  ONEIRA2
        }                                                                                                                                                                 //Natural: END-IF
        //*                                               /* CNTRSTRG START >>>
        //*  MOVE TPA/IPRO FOR LEGAL REPRINT RT-REPRINT-REQUEST-TYPE  = 'R'
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Reprint_Request_Type().equals("R") && ldaAppl1152.getRecord_Id_Ex_File_Rt_Print_Package_Type().equals("L")))    //Natural: IF RT-REPRINT-REQUEST-TYPE = 'R' AND RT-PRINT-PACKAGE-TYPE = 'L'
        {
            FOR10:                                                                                                                                                        //Natural: FOR #I = 1 TO 30
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(30)); pnd_I.nadd(1))
            {
                if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Related_Contract_Type().getValue(pnd_I).notEquals(" ")))                                                //Natural: IF RT-RELATED-CONTRACT-TYPE ( #I ) NE ' '
                {
                    ldaAppl1153.getAcis_Driver_Record_Cis_Eop_Related_Contract_Info().getValue(pnd_I).setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Related_Contract_Info().getValue(pnd_I)); //Natural: ASSIGN CIS-EOP-RELATED-CONTRACT-INFO ( #I ) := RT-RELATED-CONTRACT-INFO ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  CNTRSTRG END <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE-DRIVER-DATA
    }
    private void sub_Find_Zip_Position() throws Exception                                                                                                                 //Natural: FIND-ZIP-POSITION
    {
        if (BLNatReinput.isReinput()) return;

        FOR11:                                                                                                                                                            //Natural: FOR #SUB-ADDR = 35 TO 1 STEP -1
        for (pnd_Sub_Addr.setValue(35); condition(pnd_Sub_Addr.greaterOrEqual(1)); pnd_Sub_Addr.nsubtract(1))
        {
            if (condition(pnd_Work_Addr_Line_Pnd_Work_Addr_Digit.getValue(pnd_Sub_Addr).notEquals(" ")))                                                                  //Natural: IF #WORK-ADDR-DIGIT ( #SUB-ADDR ) NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-ZIP
                sub_Move_Zip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Sub_Addr.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #SUB-ADDR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FIND-ZIP-POSITION
    }
    private void sub_Move_Zip() throws Exception                                                                                                                          //Natural: MOVE-ZIP
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        if (condition(pnd_Sub_Addr.greater(29)))                                                                                                                          //Natural: IF #SUB-ADDR > 29
        {
            pnd_Sub_Na.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SUB-NA
            if (condition(pnd_Sub_Na.lessOrEqual(6)))                                                                                                                     //Natural: IF #SUB-NA LE 6
            {
                ldaAppl1152.getRecord_Id_Ex_File_Rt_Address_Table().getValue(pnd_Sub_Na).setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode());                     //Natural: MOVE RT-EOP-ZIPCODE TO RT-ADDRESS-TABLE ( #SUB-NA )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Sub_Addr.nadd(+1);                                                                                                                                        //Natural: ADD +1 TO #SUB-ADDR
            pnd_Work_Zip.setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Eop_Zipcode());                                                                                     //Natural: ASSIGN #WORK-ZIP := RT-EOP-ZIPCODE
            FOR12:                                                                                                                                                        //Natural: FOR #SUB-CNT = 1 TO 5
            for (pnd_Sub_Cnt.setValue(1); condition(pnd_Sub_Cnt.lessOrEqual(5)); pnd_Sub_Cnt.nadd(1))
            {
                pnd_Sub_Addr.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #SUB-ADDR
                pnd_Work_Addr_Line_Pnd_Work_Addr_Digit.getValue(pnd_Sub_Addr).setValue(pnd_Work_Zip_Pnd_Work_Zip_Digit.getValue(pnd_Sub_Cnt));                            //Natural: MOVE #WORK-ZIP-DIGIT ( #SUB-CNT ) TO #WORK-ADDR-DIGIT ( #SUB-ADDR )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            ldaAppl1152.getRecord_Id_Ex_File_Rt_Address_Table().getValue(pnd_Sub_Na).setValue(pnd_Work_Addr_Line);                                                        //Natural: MOVE #WORK-ADDR-LINE TO RT-ADDRESS-TABLE ( #SUB-NA )
        }                                                                                                                                                                 //Natural: END-IF
        //*   MOVE-ZIP
    }
    private void sub_Get_Bene_Data() throws Exception                                                                                                                     //Natural: GET-BENE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //*  TNGSUB START
        //*  TNGSUB END
        pdaAppa1154.getPnd_Appa1154().reset();                                                                                                                            //Natural: RESET #APPA1154
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tiaa_No_1().getSubstring(1,8));                                      //Natural: ASSIGN #BENE-TIAA-NBR := SUBSTR ( RT-TIAA-NO-1,1,8 )
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cref_Nbr().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Cref_No_1().getSubstring(1,8));                                      //Natural: ASSIGN #BENE-CREF-NBR := SUBSTR ( RT-CREF-NO-1,1,8 )
        pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Resid_State().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Current_Iss_State());                                             //Natural: ASSIGN #BENE-RESID-STATE := RT-CURRENT-ISS-STATE
        //*  TNGSUB
        DbsUtil.callnat(Appn1154.class , getCurrentProcessState(), pdaAppa1154.getPnd_Appa1154());                                                                        //Natural: CALLNAT 'APPN1154' #APPA1154
        if (condition(Global.isEscape())) return;
        if (condition(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Return_Code().greater(getZero()) && pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Return_Code().less(99)))              //Natural: IF #BENE-RETURN-CODE > 0 AND #BENE-RETURN-CODE < 99
        {
            getReports().write(0, "ERROR FROM BENE - APPN1154",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Return_Code());                                                       //Natural: WRITE 'ERROR FROM BENE - APPN1154' #BENE-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Msg());                                                                                            //Natural: WRITE #BENE-MSG
            if (Global.isEscape()) return;
            DbsUtil.terminate(pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Return_Code());  if (true) return;                                                                     //Natural: TERMINATE #BENE-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  START DISPLAY AFTER APPN1154
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        //*  C291829
        getReports().write(0, " *** AFTER APPN1154  ***",NEWLINE,"=",ldaAppl1152.getRecord_Id_Ex_File_Rt_Soc_Sec(),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Tiaa_Nbr(),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Estate_As_Bene(),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Trust_As_Bene(),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Category(),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Mos_Ind(),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Cnt(),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Type().getValue(1,":",4),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Std_Txt_Ind().getValue(1,":",3),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Name().getValue(1,":",3),  //Natural: WRITE ' *** AFTER APPN1154  ***' / '=' RT-SOC-SEC '=' #BENE-TIAA-NBR / '=' #BENE-ESTATE-AS-BENE '=' #BENE-TRUST-AS-BENE '=' #BENE-CATEGORY '=' #BENE-MOS-IND / '=' #BENE-PRMRY-CNT '=' #BENE-PRMRY-TYPE ( 1:4 ) '=' #BENE-PRMRY-STD-TXT-IND ( 1:3 ) / '=' #BENE-PRMRY-NAME ( 1:3 ) ( AL = 20 ) / '=' #BENE-PRMRY-SSN ( 1:4 ) / '=' #BENE-PRMRY-RELATIONSHIP ( 1:4 ) / '=' #BENE-PRMRY-SPCL-TXT ( 1,1 ) / '=' #BENE-CNTGNT-CNT '=' #BENE-CNTGNT-TYPE ( 1:4 ) '=' #BENE-CNTGNT-STD-TXT-IND ( 1:3 ) / '=' #BENE-CNTGNT-NAME ( 1:3 ) ( AL = 20 ) / '=' #BENE-CNTGNT-SSN ( 1:4 ) / '=' #BENE-CNTGNT-RELATIONSHIP ( 1:4 ) / '=' #BENE-CNTGNT-SPCL-TXT ( 1,1 ) / '=' #BENE-SPCL-DSGN-TXT ( 1:1 )
            new AlphanumericLength (20),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Ssn().getValue(1,":",4),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Relationship().getValue(1,":",4),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Prmry_Spcl_Txt().getValue(1,1),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Cnt(),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Type().getValue(1,":",4),"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Std_Txt_Ind().getValue(1,":",3),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Name().getValue(1,":",3), 
            new AlphanumericLength (20),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Ssn().getValue(1,":",4),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Relationship().getValue(1,
            ":",4),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Cntgnt_Spcl_Txt().getValue(1,1),NEWLINE,"=",pdaAppa1154.getPnd_Appa1154_Pnd_Bene_Spcl_Dsgn_Txt().getValue(1,
            ":",1));
        if (Global.isEscape()) return;
        //*  GET-BENE-DATA
    }
    private void sub_Get_Alloc_Data() throws Exception                                                                                                                    //Natural: GET-ALLOC-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Alloc_Category.getValue("*").reset();                                                                                                                         //Natural: RESET #ALLOC-CATEGORY ( * ) #ALLOC-NAME ( * ) #ALLOC-CNT #ALLOC-RETURN-CODE #ALLOC-RETURN-MSG
        pnd_Alloc_Name.getValue("*").reset();
        pnd_Alloc_Cnt.reset();
        pnd_Alloc_Return_Code.reset();
        pnd_Alloc_Return_Msg.reset();
        DbsUtil.callnat(Appn1155.class , getCurrentProcessState(), ldaAppl1152.getRecord_Id_Ex_File_Rt_Fund_Cde().getValue("*"), ldaAppl1152.getRecord_Id_Ex_File_Rt_Alloc_Pct().getValue("*"),  //Natural: CALLNAT 'APPN1155' RT-FUND-CDE ( * ) RT-ALLOC-PCT ( * ) #ALLOC-CATEGORY ( * ) #ALLOC-NAME ( * ) #ALLOC-CNT #ALLOC-RETURN-CODE #ALLOC-RETURN-MSG
            pnd_Alloc_Category.getValue("*"), pnd_Alloc_Name.getValue("*"), pnd_Alloc_Cnt, pnd_Alloc_Return_Code, pnd_Alloc_Return_Msg);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Alloc_Return_Code.greater(getZero())))                                                                                                          //Natural: IF #ALLOC-RETURN-CODE > 0
        {
            getReports().write(0, "ERROR FROM ALLOC - APPN1155",pnd_Alloc_Return_Code);                                                                                   //Natural: WRITE 'ERROR FROM ALLOC - APPN1155' #ALLOC-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Alloc_Return_Msg);                                                                                                                  //Natural: WRITE #ALLOC-RETURN-MSG
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Alloc_Return_Code);  if (true) return;                                                                                                  //Natural: TERMINATE #ALLOC-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-ALLOC-DATA
    }
    //*  IISG  START >>>
    private void sub_Get_Alloc_Data_2() throws Exception                                                                                                                  //Natural: GET-ALLOC-DATA-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Alloc_Category_2.getValue("*").reset();                                                                                                                       //Natural: RESET #ALLOC-CATEGORY-2 ( * ) #ALLOC-NAME-2 ( * ) #ALLOC-CNT-2 #ALLOC-RETURN-CODE #ALLOC-RETURN-MSG
        pnd_Alloc_Name_2.getValue("*").reset();
        pnd_Alloc_Cnt_2.reset();
        pnd_Alloc_Return_Code.reset();
        pnd_Alloc_Return_Msg.reset();
        DbsUtil.callnat(Appn1155.class , getCurrentProcessState(), ldaAppl1152.getRecord_Id_Ex_File_Rt_Fund_Cde_2().getValue("*"), ldaAppl1152.getRecord_Id_Ex_File_Rt_Alloc_Pct_2().getValue("*"),  //Natural: CALLNAT 'APPN1155' RT-FUND-CDE-2 ( * ) RT-ALLOC-PCT-2 ( * ) #ALLOC-CATEGORY-2 ( * ) #ALLOC-NAME-2 ( * ) #ALLOC-CNT-2 #ALLOC-RETURN-CODE #ALLOC-RETURN-MSG
            pnd_Alloc_Category_2.getValue("*"), pnd_Alloc_Name_2.getValue("*"), pnd_Alloc_Cnt_2, pnd_Alloc_Return_Code, pnd_Alloc_Return_Msg);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Alloc_Return_Code.greater(getZero())))                                                                                                          //Natural: IF #ALLOC-RETURN-CODE > 0
        {
            getReports().write(0, "ERROR FROM 2ND ALLOC - APPN1155",pnd_Alloc_Return_Code);                                                                               //Natural: WRITE 'ERROR FROM 2ND ALLOC - APPN1155' #ALLOC-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Alloc_Return_Msg);                                                                                                                  //Natural: WRITE #ALLOC-RETURN-MSG
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Alloc_Return_Code);  if (true) return;                                                                                                  //Natural: TERMINATE #ALLOC-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************
        getReports().write(0, "********************************");                                                                                                        //Natural: WRITE '********************************'
        if (Global.isEscape()) return;
        getReports().write(0, "ALLOC NAME 2 ",pnd_Alloc_Name_2.getValue(1,":",6),NEWLINE,"      %    2 ",ldaAppl1152.getRecord_Id_Ex_File_Rt_Alloc_Pct_2().getValue(1,    //Natural: WRITE 'ALLOC NAME 2 ' #ALLOC-NAME-2 ( 1:6 ) / '      %    2 ' RT-ALLOC-PCT-2 ( 1:6 )
            ":",6));
        if (Global.isEscape()) return;
        //* *******************
        //*  GET-ALLOC-DATA-2              /* IISG END <<<
    }
    //*       TIC
    //*       TIC
    //*       TIC
    //*       TIC
    //*       TIC
    private void sub_Move_Tic_Fields() throws Exception                                                                                                                   //Natural: MOVE-TIC-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Ind().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Ind());                                                     //Natural: ASSIGN ACIS-EOP-TIC-IND := RT-TIC-IND
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Postdays().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Postdays());                                           //Natural: ASSIGN ACIS-EOP-TIC-POSTDAYS := RT-TIC-POSTDAYS
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Postfreq().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Postfreq());                                           //Natural: ASSIGN ACIS-EOP-TIC-POSTFREQ := RT-TIC-POSTFREQ
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Pl_Level().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Pl_Level());                                           //Natural: ASSIGN ACIS-EOP-TIC-PL-LEVEL := RT-TIC-PL-LEVEL
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Startdate().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Startdate());                                         //Natural: ASSIGN ACIS-EOP-TIC-STARTDATE := RT-TIC-STARTDATE
        rt_Tic_Percentage_A.setValue(" ");                                                                                                                                //Natural: ASSIGN RT-TIC-PERCENTAGE-A := ' '
        pnd_Hold_Ext_Data_14.setValue(ldaAppl1152.getRecord_Id_Ex_File_Header_Ext_Data_14());                                                                             //Natural: MOVE HEADER-EXT-DATA-14 TO #HOLD-EXT-DATA-14
        pnd_Rt_Tic_Windowdays_A.setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Windowdays());                                                                           //Natural: MOVE RT-TIC-WINDOWDAYS TO #RT-TIC-WINDOWDAYS-A
        if (condition(pnd_Rt_Tic_Windowdays_A_Pnd_Rt_Tic_Windowdays_N.lessOrEqual(180)))                                                                                  //Natural: IF #RT-TIC-WINDOWDAYS-N LE 180
        {
            //* *  MOVE EDITED #RT-TIC-WINDOWDAYS-N (EM=Z99) TO #HOLD-WD
            //* *  COMPRESS #HOLD-WD          'day'  INTO ACIS-EOP-TIC-WINDOWDAYS
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Windowdays().setValue(DbsUtil.compress(pnd_Rt_Tic_Windowdays_A_Pnd_Rt_Tic_Windowdays_N, "day"));               //Natural: COMPRESS #RT-TIC-WINDOWDAYS-N 'day' INTO ACIS-EOP-TIC-WINDOWDAYS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Window.compute(new ComputeParameters(true, pnd_Window), pnd_Rt_Tic_Windowdays_A_Pnd_Rt_Tic_Windowdays_N.divide(30));                                      //Natural: COMPUTE ROUNDED #WINDOW = #RT-TIC-WINDOWDAYS-N / 30
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Windowdays().setValue(DbsUtil.compress(pnd_Window, "month"));                                                  //Natural: COMPRESS #WINDOW 'month' INTO ACIS-EOP-TIC-WINDOWDAYS
        }                                                                                                                                                                 //Natural: END-IF
        //*  LS
        pnd_Window.reset();                                                                                                                                               //Natural: RESET #WINDOW
        pnd_Rt_Tic_Reqdlywindow_A.setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Reqdlywindow());                                                                       //Natural: MOVE RT-TIC-REQDLYWINDOW TO #RT-TIC-REQDLYWINDOW-A
        if (condition(pnd_Rt_Tic_Reqdlywindow_A_Pnd_Rt_Tic_Reqdlywindow_N.lessOrEqual(180)))                                                                              //Natural: IF #RT-TIC-REQDLYWINDOW-N LE 180
        {
            //* *  MOVE EDITED #RT-TIC-REQDLYWINDOW-N   (EM=Z99) TO #HOLD-WD
            //* *  COMPRESS #HOLD-WD          'day'  INTO ACIS-EOP-TIC-REQDLYWINDOW
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Reqdlywindow().setValue(DbsUtil.compress(pnd_Rt_Tic_Reqdlywindow_A_Pnd_Rt_Tic_Reqdlywindow_N,                  //Natural: COMPRESS #RT-TIC-REQDLYWINDOW-N 'day' INTO ACIS-EOP-TIC-REQDLYWINDOW
                "day"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Window.compute(new ComputeParameters(true, pnd_Window), pnd_Rt_Tic_Reqdlywindow_A_Pnd_Rt_Tic_Reqdlywindow_N.divide(30));                                  //Natural: COMPUTE ROUNDED #WINDOW = #RT-TIC-REQDLYWINDOW-N / 30
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Reqdlywindow().setValue(DbsUtil.compress(pnd_Window, "month"));                                                //Natural: COMPRESS #WINDOW 'month' INTO ACIS-EOP-TIC-REQDLYWINDOW
        }                                                                                                                                                                 //Natural: END-IF
        //*   LS
        pnd_Window.reset();                                                                                                                                               //Natural: RESET #WINDOW
        pnd_Rt_Tic_Recap_Prov_A.setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Recap_Prov());                                                                           //Natural: MOVE RT-TIC-RECAP-PROV TO #RT-TIC-RECAP-PROV-A
        if (condition(pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N.equals(0)))                                                                                         //Natural: IF #RT-TIC-RECAP-PROV-N = 000
        {
            //*   LS
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Recap_Prov().setValue("NOT APPLICABLE");                                                                       //Natural: MOVE 'NOT APPLICABLE' TO ACIS-EOP-TIC-RECAP-PROV
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N.lessOrEqual(180)))                                                                              //Natural: IF #RT-TIC-RECAP-PROV-N LE 180
            {
                //* *  MOVE EDITED #RT-TIC-RECAP-PROV-N   (EM=Z99) TO #HOLD-WD
                //* *  COMPRESS #HOLD-WD          'days' INTO ACIS-EOP-TIC-RECAP-PROV
                ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Recap_Prov().setValue(DbsUtil.compress(pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N,                    //Natural: COMPRESS #RT-TIC-RECAP-PROV-N 'days' INTO ACIS-EOP-TIC-RECAP-PROV
                    "days"));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N.greater(180)))                                                                              //Natural: IF #RT-TIC-RECAP-PROV-N GT 180
                {
                    pnd_Window.compute(new ComputeParameters(true, pnd_Window), pnd_Rt_Tic_Recap_Prov_A_Pnd_Rt_Tic_Recap_Prov_N.divide(30));                              //Natural: COMPUTE ROUNDED #WINDOW = #RT-TIC-RECAP-PROV-N / 30
                    ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Recap_Prov().setValue(DbsUtil.compress(pnd_Window, "months"));                                         //Natural: COMPRESS #WINDOW 'months' INTO ACIS-EOP-TIC-RECAP-PROV
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Percentage().equals(" ")))                                                                                  //Natural: IF RT-TIC-PERCENTAGE = ' '
        {
            rt_Tic_Percentage_A.setValue("000000000");                                                                                                                    //Natural: MOVE '000000000' TO RT-TIC-PERCENTAGE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            rt_Tic_Percentage_A.setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Percentage());                                                                           //Natural: MOVE RT-TIC-PERCENTAGE TO RT-TIC-PERCENTAGE-A
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rt_Tic_Percentage.setValueEdited(rt_Tic_Percentage_A_Rt_Tic_Percentage_N,new ReportEditMask("9.999"));                                                        //Natural: MOVE EDITED RT-TIC-PERCENTAGE-N ( EM = 9.999 ) TO #RT-TIC-PERCENTAGE
        pnd_Acis_Eop_Tic_Percentage.setValue(pnd_Rt_Tic_Percentage);                                                                                                      //Natural: MOVE #RT-TIC-PERCENTAGE TO #ACIS-EOP-TIC-PERCENTAGE
        FOR13:                                                                                                                                                            //Natural: FOR #X 6 1 -1
        for (pnd_X.setValue(6); condition(pnd_X.greaterOrEqual(1)); pnd_X.nsubtract(1))
        {
            if (condition(pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop.getValue(pnd_X).equals(" ")))                                                                          //Natural: IF #ACIS-EOP ( #X ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop.getValue(pnd_X).equals(".")))                                                                          //Natural: IF #ACIS-EOP ( #X ) = '.'
            {
                pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop.getValue(pnd_X).setValue(" ");                                                                                   //Natural: MOVE ' ' TO #ACIS-EOP ( #X )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop.getValue(pnd_X).equals("0")))                                                                          //Natural: IF #ACIS-EOP ( #X ) = '0'
            {
                pnd_Acis_Eop_Tic_Percentage_Pnd_Acis_Eop.getValue(pnd_X).setValue(" ");                                                                                   //Natural: MOVE ' ' TO #ACIS-EOP ( #X )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Percentage().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Acis_Eop_Tic_Percentage,                 //Natural: COMPRESS #ACIS-EOP-TIC-PERCENTAGE '%' INTO ACIS-EOP-TIC-PERCENTAGE LEAVING NO SPACE
            "%"));
        //*       TIC
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Enddate().setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Enddate());                                             //Natural: MOVE RT-TIC-ENDDATE TO ACIS-EOP-TIC-ENDDATE
        ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Limit().setValue(" ");                                                                                             //Natural: ASSIGN ACIS-EOP-TIC-LIMIT := ' '
        pnd_Lz_Cnt.setValue(0);                                                                                                                                           //Natural: ASSIGN #LZ-CNT := 0
        //*  FIND NUMBER OF LEADING ZEROS           /*      TIC
        rt_Tic_Limit_A.setValue(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Limit());                                                                                         //Natural: MOVE RT-TIC-LIMIT TO RT-TIC-LIMIT-A
        FOR14:                                                                                                                                                            //Natural: FOR #X 1 11
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(11)); pnd_X.nadd(1))
        {
            //*       TIC
            if (condition(rt_Tic_Limit_A_Rt_Tic_Limit_T.getValue(pnd_X).equals("0")))                                                                                     //Natural: IF RT-TIC-LIMIT-T ( #X ) = '0'
            {
                //*       TIC
                pnd_Lz_Cnt.setValue(pnd_X);                                                                                                                               //Natural: MOVE #X TO #LZ-CNT
                //*       TIC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*       TIC
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*       TIC
            }                                                                                                                                                             //Natural: END-IF
            //*       TIC
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Lz_Cnt.equals(11)))                                                                                                                             //Natural: IF #LZ-CNT = 11
        {
            ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Limit().setValue("none");                                                                                      //Natural: MOVE 'none' TO ACIS-EOP-TIC-LIMIT
            //*  DO REST OF CODE
            //*       TIC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hold.setValue(" ");                                                                                                                                       //Natural: ASSIGN #HOLD := ' '
            //*       TIC
            //*  BEGIN MOVING AFTER LAST LEADING  /*      TIC
            pnd_Lz_Cnt.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #LZ-CNT
            FOR15:                                                                                                                                                        //Natural: FOR #X #LZ-CNT 11
            for (pnd_X.setValue(pnd_Lz_Cnt); condition(pnd_X.lessOrEqual(11)); pnd_X.nadd(1))
            {
                //*  ZERO TO RIGHT JUSTIFY THE RESULT   /*      TIC
                //*       TIC
                pnd_Hold_Pnd_Hold_T.getValue(pnd_X).setValue(rt_Tic_Limit_A_Rt_Tic_Limit_T.getValue(pnd_Lz_Cnt));                                                         //Natural: MOVE RT-TIC-LIMIT-T ( #LZ-CNT ) TO #HOLD-T ( #X )
                //*       TIC
                pnd_Lz_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #LZ-CNT
                //*       TIC
                //*  DETERMINE POSITIONALITY FOR FORMAT MASK   /*      TIC
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR16:                                                                                                                                                        //Natural: FOR #X 1 11
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(11)); pnd_X.nadd(1))
            {
                //*      TIC
                if (condition(pnd_Hold_Pnd_Hold_T.getValue(pnd_X).notEquals(" ")))                                                                                        //Natural: IF #HOLD-T ( #X ) NE ' '
                {
                    //*       TIC
                    pnd_Pos.setValue(pnd_X);                                                                                                                              //Natural: MOVE #X TO #POS
                    //*       TIC
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*       TIC
                }                                                                                                                                                         //Natural: END-IF
                //*       TIC
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*       TIC
            if (condition(ldaAppl1152.getRecord_Id_Ex_File_Rt_Tic_Limit().equals(" ") || pnd_Pos.equals(getZero())))                                                      //Natural: IF RT-TIC-LIMIT = ' ' OR #POS = 0
            {
                //*       TIC
                ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Limit().setValue("$0");                                                                                    //Natural: MOVE '$0' TO ACIS-EOP-TIC-LIMIT
                //*       TIC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  MOVE RT-TIC-LIMIT TO RT-TIC-LIMIT-A
                //*       TIC
                                                                                                                                                                          //Natural: PERFORM FORMAT-TIC-LIMIT
                sub_Format_Tic_Limit();
                if (condition(Global.isEscape())) {return;}
                //*       TIC
                //*       TIC
                ldaAppl1153.getAcis_Driver_Record_Acis_Eop_Tic_Limit().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "$", pnd_Rt_Tic_Limit));                  //Natural: COMPRESS '$' #RT-TIC-LIMIT INTO ACIS-EOP-TIC-LIMIT LEAVING NO SPACE
                //*       TIC
            }                                                                                                                                                             //Natural: END-IF
            //*       TIC
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE-TIC-FIELDS
    }
    private void sub_Format_Tic_Limit() throws Exception                                                                                                                  //Natural: FORMAT-TIC-LIMIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        short decideConditionsMet1806 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #POS;//Natural: VALUE 1
        if (condition((pnd_Pos.equals(1))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("ZZZ,ZZZ,ZZ9"));                                                             //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = ZZZ,ZZZ,ZZ9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Pos.equals(2))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("ZZ,ZZZ,ZZ9"));                                                              //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = ZZ,ZZZ,ZZ9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Pos.equals(3))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("Z,ZZZ,ZZ9"));                                                               //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = Z,ZZZ,ZZ9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Pos.equals(4))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("ZZZ,ZZ9"));                                                                 //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = ZZZ,ZZ9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_Pos.equals(5))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("ZZ,ZZ9"));                                                                  //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = ZZ,ZZ9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pnd_Pos.equals(6))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("Z,ZZ9"));                                                                   //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = Z,ZZ9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((pnd_Pos.equals(7))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("ZZ9"));                                                                     //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = ZZ9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pnd_Pos.equals(8))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("Z9"));                                                                      //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = Z9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 9
        else if (condition((pnd_Pos.equals(9))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("9"));                                                                       //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = 9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 10
        else if (condition((pnd_Pos.equals(10))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("9"));                                                                       //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = 9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: VALUE 11
        else if (condition((pnd_Pos.equals(11))))
        {
            decideConditionsMet1806++;
            pnd_Rt_Tic_Limit.setValueEdited(rt_Tic_Limit_A_Rt_Tic_Limit_N,new ReportEditMask("9"));                                                                       //Natural: MOVE EDITED RT-TIC-LIMIT-N ( EM = 9 ) TO #RT-TIC-LIMIT
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  FORMAT-TIC-LIMIT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, "********  EXTRACT FILE IS EMPTY ********");
    }
}
