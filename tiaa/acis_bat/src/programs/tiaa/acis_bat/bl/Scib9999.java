/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:21 PM
**        * FROM NATURAL PROGRAM : Scib9999
************************************************************
**        * FILE NAME            : Scib9999.java
**        * CLASS NAME           : Scib9999
**        * INSTANCE NAME        : Scib9999
************************************************************
************************************************************************
* PROGRAM  : SCIB9999
* SYSTEM   : NEWRIVER PROCESSING
* AUTHOR   : CHARLES SINGLETON
* GENERATED: JULY 2005
* FUNCTION : THIS PROGRAM READS A SEQUENTIAL DATASET OF ORG CODES FOR
*          : INSTITUTIONS SET FOR CONVERSION TO THE SUNGARD PLATFORM.
*          : THEN CREATES APP-TABLE-ENTRY RECORDS THAT WILL BE PROCESSED
*          : TO CREATE A BASELINE FILE THAT WILL BE SENT TO NEWRIVER
*          : FOR PROSPECTUS MAILING.
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
*
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9999 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_table_Entry;
    private DbsField table_Entry_Entry_Actve_Ind;
    private DbsField table_Entry_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Entry_Table_Sub_Id;
    private DbsField table_Entry_Entry_Cde;
    private DbsField table_Entry_Entry_Dscrptn_Txt;
    private DbsField pnd_Entry_Code_N;

    private DbsGroup pnd_Entry_Code_N__R_Field_1;
    private DbsField pnd_Entry_Code_N_Pnd_Entry_Code_A;

    private DataAccessProgramView vw_add_View;
    private DbsField add_View_Entry_Actve_Ind;
    private DbsField add_View_Entry_Table_Id_Nbr;
    private DbsField add_View_Entry_Table_Sub_Id;
    private DbsField add_View_Entry_Cde;
    private DbsGroup add_View_Addtnl_Dscrptn_TxtMuGroup;
    private DbsField add_View_Addtnl_Dscrptn_Txt;
    private DbsField add_View_Entry_Dscrptn_Txt;
    private DbsField add_View_Entry_User_Id;
    private DbsField add_View_Update_By;
    private DbsField add_View_Update_Date;
    private DbsField entry_Dscrptn_Txt;

    private DbsGroup entry_Dscrptn_Txt__R_Field_2;
    private DbsField entry_Dscrptn_Txt_Entry_Orgcode_Num;
    private DbsField entry_Dscrptn_Txt_Filler;
    private DbsField entry_Dscrptn_Txt_Entry_Status;

    private DbsGroup pnd_Ppg_Orgcode_Rec;
    private DbsField pnd_Ppg_Orgcode_Rec_Pnd_Ppg;
    private DbsField pnd_Ppg_Orgcode_Rec_Pnd_Org_Code;
    private DbsField pnd_Search_Key;

    private DbsGroup pnd_Search_Key__R_Field_3;
    private DbsField pnd_Search_Key_Pnd_S_Entry_Actve_Ind;
    private DbsField pnd_Search_Key_Pnd_S_Entry_Table_Id_Nbr;
    private DbsField pnd_Search_Key_Pnd_S_Entry_Table_Sub_Id;
    private DbsField pnd_Search_Key_Pnd_S_Entry_Cde;
    private DbsField pnd_Last_Entry_Code;

    private DbsGroup pnd_Last_Entry_Code__R_Field_4;
    private DbsField pnd_Last_Entry_Code_Pnd_Last_Entry_Code_N;
    private DbsField update_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_table_Entry = new DataAccessProgramView(new NameInfo("vw_table_Entry", "TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        table_Entry_Entry_Actve_Ind = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        table_Entry_Entry_Table_Id_Nbr = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        table_Entry_Entry_Table_Sub_Id = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        table_Entry_Entry_Cde = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        table_Entry_Entry_Dscrptn_Txt = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        registerRecord(vw_table_Entry);

        pnd_Entry_Code_N = localVariables.newFieldInRecord("pnd_Entry_Code_N", "#ENTRY-CODE-N", FieldType.NUMERIC, 7);

        pnd_Entry_Code_N__R_Field_1 = localVariables.newGroupInRecord("pnd_Entry_Code_N__R_Field_1", "REDEFINE", pnd_Entry_Code_N);
        pnd_Entry_Code_N_Pnd_Entry_Code_A = pnd_Entry_Code_N__R_Field_1.newFieldInGroup("pnd_Entry_Code_N_Pnd_Entry_Code_A", "#ENTRY-CODE-A", FieldType.STRING, 
            7);

        vw_add_View = new DataAccessProgramView(new NameInfo("vw_add_View", "ADD-VIEW"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        add_View_Entry_Actve_Ind = vw_add_View.getRecord().newFieldInGroup("add_View_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ENTRY_ACTVE_IND");
        add_View_Entry_Table_Id_Nbr = vw_add_View.getRecord().newFieldInGroup("add_View_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        add_View_Entry_Table_Sub_Id = vw_add_View.getRecord().newFieldInGroup("add_View_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        add_View_Entry_Cde = vw_add_View.getRecord().newFieldInGroup("add_View_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        add_View_Addtnl_Dscrptn_TxtMuGroup = vw_add_View.getRecord().newGroupInGroup("ADD_VIEW_ADDTNL_DSCRPTN_TXTMuGroup", "ADDTNL_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ADDTNL_DSCRPTN_TXT");
        add_View_Addtnl_Dscrptn_Txt = add_View_Addtnl_Dscrptn_TxtMuGroup.newFieldArrayInGroup("add_View_Addtnl_Dscrptn_Txt", "ADDTNL-DSCRPTN-TXT", FieldType.STRING, 
            60, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDTNL_DSCRPTN_TXT");
        add_View_Entry_Dscrptn_Txt = vw_add_View.getRecord().newFieldInGroup("add_View_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 60, 
            RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        add_View_Entry_User_Id = vw_add_View.getRecord().newFieldInGroup("add_View_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENTRY_USER_ID");
        add_View_Update_By = vw_add_View.getRecord().newFieldInGroup("add_View_Update_By", "UPDATE-BY", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UPDATE_BY");
        add_View_Update_Date = vw_add_View.getRecord().newFieldInGroup("add_View_Update_Date", "UPDATE-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "UPDATE_DATE");
        registerRecord(vw_add_View);

        entry_Dscrptn_Txt = localVariables.newFieldInRecord("entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 60);

        entry_Dscrptn_Txt__R_Field_2 = localVariables.newGroupInRecord("entry_Dscrptn_Txt__R_Field_2", "REDEFINE", entry_Dscrptn_Txt);
        entry_Dscrptn_Txt_Entry_Orgcode_Num = entry_Dscrptn_Txt__R_Field_2.newFieldInGroup("entry_Dscrptn_Txt_Entry_Orgcode_Num", "ENTRY-ORGCODE-NUM", 
            FieldType.STRING, 6);
        entry_Dscrptn_Txt_Filler = entry_Dscrptn_Txt__R_Field_2.newFieldInGroup("entry_Dscrptn_Txt_Filler", "FILLER", FieldType.STRING, 2);
        entry_Dscrptn_Txt_Entry_Status = entry_Dscrptn_Txt__R_Field_2.newFieldInGroup("entry_Dscrptn_Txt_Entry_Status", "ENTRY-STATUS", FieldType.STRING, 
            1);

        pnd_Ppg_Orgcode_Rec = localVariables.newGroupInRecord("pnd_Ppg_Orgcode_Rec", "#PPG-ORGCODE-REC");
        pnd_Ppg_Orgcode_Rec_Pnd_Ppg = pnd_Ppg_Orgcode_Rec.newFieldInGroup("pnd_Ppg_Orgcode_Rec_Pnd_Ppg", "#PPG", FieldType.STRING, 6);
        pnd_Ppg_Orgcode_Rec_Pnd_Org_Code = pnd_Ppg_Orgcode_Rec.newFieldInGroup("pnd_Ppg_Orgcode_Rec_Pnd_Org_Code", "#ORG-CODE", FieldType.STRING, 6);
        pnd_Search_Key = localVariables.newFieldInRecord("pnd_Search_Key", "#SEARCH-KEY", FieldType.STRING, 29);

        pnd_Search_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Search_Key__R_Field_3", "REDEFINE", pnd_Search_Key);
        pnd_Search_Key_Pnd_S_Entry_Actve_Ind = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_S_Entry_Actve_Ind", "#S-ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Search_Key_Pnd_S_Entry_Table_Id_Nbr = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_S_Entry_Table_Id_Nbr", "#S-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Search_Key_Pnd_S_Entry_Table_Sub_Id = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_S_Entry_Table_Sub_Id", "#S-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Search_Key_Pnd_S_Entry_Cde = pnd_Search_Key__R_Field_3.newFieldInGroup("pnd_Search_Key_Pnd_S_Entry_Cde", "#S-ENTRY-CDE", FieldType.STRING, 
            20);
        pnd_Last_Entry_Code = localVariables.newFieldInRecord("pnd_Last_Entry_Code", "#LAST-ENTRY-CODE", FieldType.STRING, 7);

        pnd_Last_Entry_Code__R_Field_4 = localVariables.newGroupInRecord("pnd_Last_Entry_Code__R_Field_4", "REDEFINE", pnd_Last_Entry_Code);
        pnd_Last_Entry_Code_Pnd_Last_Entry_Code_N = pnd_Last_Entry_Code__R_Field_4.newFieldInGroup("pnd_Last_Entry_Code_Pnd_Last_Entry_Code_N", "#LAST-ENTRY-CODE-N", 
            FieldType.NUMERIC, 7);
        update_Cnt = localVariables.newFieldInRecord("update_Cnt", "UPDATE-CNT", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_table_Entry.reset();
        vw_add_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9999() throws Exception
    {
        super("Scib9999");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        update_Cnt.reset();                                                                                                                                               //Natural: RESET UPDATE-CNT
                                                                                                                                                                          //Natural: PERFORM FIND-BP-RECORDS
        sub_Find_Bp_Records();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 RECORD #PPG-ORGCODE-REC
        while (condition(getWorkFiles().read(1, pnd_Ppg_Orgcode_Rec)))
        {
                                                                                                                                                                          //Natural: PERFORM ADD-TABLE-ENTRY
            sub_Add_Table_Entry();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-BP-RECORDS
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-TABLE-ENTRY
        //*  ADD-VIEW.ENTRY-STATUS        := 1
        getReports().write(0, "======================================",NEWLINE,"TABLE RECORDS ADDED :",update_Cnt,NEWLINE,"======================================",       //Natural: WRITE '======================================' / 'TABLE RECORDS ADDED :' UPDATE-CNT / '======================================' /
            NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Find_Bp_Records() throws Exception                                                                                                                   //Natural: FIND-BP-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_Search_Key.reset();                                                                                                                                           //Natural: RESET #SEARCH-KEY
        pnd_Search_Key_Pnd_S_Entry_Actve_Ind.setValue("Y");                                                                                                               //Natural: ASSIGN #S-ENTRY-ACTVE-IND := 'Y'
        pnd_Search_Key_Pnd_S_Entry_Table_Id_Nbr.setValue(100);                                                                                                            //Natural: ASSIGN #S-ENTRY-TABLE-ID-NBR := 100
        pnd_Search_Key_Pnd_S_Entry_Table_Sub_Id.setValue("BP");                                                                                                           //Natural: ASSIGN #S-ENTRY-TABLE-SUB-ID := 'BP'
        pnd_Search_Key_Pnd_S_Entry_Cde.setValue("0000001");                                                                                                               //Natural: ASSIGN #S-ENTRY-CDE := '0000001'
        vw_table_Entry.startDatabaseRead                                                                                                                                  //Natural: READ TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE FROM #SEARCH-KEY
        (
        "READ02",
        new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Search_Key, WcType.BY) },
        new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ02:
        while (condition(vw_table_Entry.readNextRow("READ02")))
        {
            if (condition(table_Entry_Entry_Actve_Ind.notEquals(pnd_Search_Key_Pnd_S_Entry_Actve_Ind) || table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Search_Key_Pnd_S_Entry_Table_Id_Nbr)  //Natural: IF ENTRY-ACTVE-IND NE #S-ENTRY-ACTVE-IND OR ENTRY-TABLE-ID-NBR NE #S-ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #S-ENTRY-TABLE-SUB-ID
                || table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Search_Key_Pnd_S_Entry_Table_Sub_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Entry_Code.setValue(table_Entry_Entry_Cde);                                                                                                          //Natural: ASSIGN #LAST-ENTRY-CODE := TABLE-ENTRY.ENTRY-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  FIND-BP-RECORDS
    }
    private void sub_Add_Table_Entry() throws Exception                                                                                                                   //Natural: ADD-TABLE-ENTRY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        vw_add_View.reset();                                                                                                                                              //Natural: RESET ADD-VIEW
        add_View_Entry_Actve_Ind.setValue("Y");                                                                                                                           //Natural: ASSIGN ADD-VIEW.ENTRY-ACTVE-IND := 'Y'
        add_View_Entry_Table_Id_Nbr.setValue(100);                                                                                                                        //Natural: ASSIGN ADD-VIEW.ENTRY-TABLE-ID-NBR := 100
        add_View_Entry_Table_Sub_Id.setValue("BP");                                                                                                                       //Natural: ASSIGN ADD-VIEW.ENTRY-TABLE-SUB-ID := 'BP'
        add_View_Entry_User_Id.setValue(Global.getUSER());                                                                                                                //Natural: ASSIGN ADD-VIEW.ENTRY-USER-ID := *USER
        add_View_Update_Date.setValue(Global.getDATN());                                                                                                                  //Natural: ASSIGN ADD-VIEW.UPDATE-DATE := *DATN
        pnd_Last_Entry_Code_Pnd_Last_Entry_Code_N.nadd(1);                                                                                                                //Natural: ASSIGN #LAST-ENTRY-CODE-N := #LAST-ENTRY-CODE-N + 1
        add_View_Entry_Cde.setValue(pnd_Last_Entry_Code);                                                                                                                 //Natural: ASSIGN ADD-VIEW.ENTRY-CDE := #LAST-ENTRY-CODE
        pnd_Last_Entry_Code.setValue(add_View_Entry_Cde);                                                                                                                 //Natural: ASSIGN #LAST-ENTRY-CODE := ADD-VIEW.ENTRY-CDE
        entry_Dscrptn_Txt_Entry_Orgcode_Num.setValue(pnd_Ppg_Orgcode_Rec_Pnd_Org_Code);                                                                                   //Natural: MOVE #ORG-CODE TO ENTRY-ORGCODE-NUM
        entry_Dscrptn_Txt_Entry_Status.setValue("1");                                                                                                                     //Natural: MOVE '1' TO ENTRY-STATUS
        add_View_Entry_Dscrptn_Txt.setValue(entry_Dscrptn_Txt);                                                                                                           //Natural: MOVE ENTRY-DSCRPTN-TXT TO ADD-VIEW.ENTRY-DSCRPTN-TXT
        vw_add_View.insertDBRow();                                                                                                                                        //Natural: STORE ADD-VIEW
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        update_Cnt.nadd(1);                                                                                                                                               //Natural: ADD 1 TO UPDATE-CNT
        //*  ADD-TABLE-ENTRY
    }

    //
}
