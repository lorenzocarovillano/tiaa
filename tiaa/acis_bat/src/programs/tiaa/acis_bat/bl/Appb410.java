/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:09:37 PM
**        * FROM NATURAL PROGRAM : Appb410
************************************************************
**        * FILE NAME            : Appb410.java
**        * CLASS NAME           : Appb410
**        * INSTANCE NAME        : Appb410
************************************************************
************************************************************************
*  PROGRAM: APPB410
*   AUTHOR: RON WILLIS
* FUNCTION: READ CONVERSION FILE AND UPDATE ACIS-REPRINT-FL WITH
*           PLAN AND SUBPLAN IF THE PRODUCTS MATCH. ALSO REPORT ON
*           STATS.
*
*           -PROCESS A FILE BASED ON OUR CONTROL RECORD
*
* DATE     TIME    DESC
* -------- ------- -----------------------------------------------------
* 11/16/04 WILLISR FIX COUNTER TO INCREMENT MISMATCH WHEN CONTRACT IS
*                  PRESENT BUT A MATCH COULD NOT BE MADE ON PRODUCT
* 01/03/05 K GATES RESTOW FOR RELEASE 4 FOR APPL180
* 12/15/05 D MARPUI RESTOW FOR RELEASE 8 FOR APPL180 - DIVSUB FIELD
* 04/11/06 K GATES RESTOW FOR APPL180 FOR SUNGARD STABLE RETURN FUND
* 07/20/06 K GATES RESTOW FOR APPL180 FOR ARR PROJECT
* 07/26/07 B DEVELBISS RESTOW FOR APPL180 FOR STABLE RETURN SV
* 04/07/08 K GATES RESTOW FOR APPL180 FOR CALSTRS PROJECT
* 09/23/08 B DEVELBISS RESTOW FOR APPL180 FOR AUTO ENROLL
* 08/12/09 C. AVE - RESTOW - NEW LAYOUT OF APPL180 FOR ACIS PERF.
* 03/15/10 B.HOLLOWAY - TIAA STABLE VALUE - RESTOW FOR APPL160
* 03/04/11 B. ELLO   - FLORIDA PROJECT - RESTOW FOR APPL160
* 05/04/2011  C. SCHNEIDER RESTOW FOR NEW VERSION OF APPL180 FOR JHU
* 07/01/11 L. SHU    - TRANSFER IN CREDIT PROJECT - RESTOW FOR APPL180
* 08/04/11 G. SUDSATAYA - RESTOW FOR CAMPUS - USE APPL180
* 03/23/12 K.GATES   - RE-STOW FOR APPL180 FOR ROCH2
* 11/17/12 L. SHU    - RE-STOW FOR APPL180 FOR LEGAL PKG ANNUITY OPTION
* 07/18/13 L. SHU    - RE-STOW FOR APPL180 FOR IRA SUBSTITUTION
* 09/13/13 L. SHU    - RE-STOW FOR APPL180 FOR MT. SINAI       MTSIN
* 08/1/14  B.NEWSOM  - INVESTMENT MENU BY SUBPLAN                 (IMBS)
*                      RESTOW FOR APPL180 CHANGES
* 09/15/14 B.NEWSOM  - ACIS/CIS CREF REDESIGN COMMUNICATIONS     (ACCRC)
*                      RESTOW FOR APPL180 CHANGES
* 09/01/15 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180
*                      FOR BENE THE PLAN.                        (BIP)
* 09/01/16 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180
*                      FOR ONEIRA                               ONEIRA
* 06/20/2017  MITRAPU           PIN EXPANSION CHANGES.(C425939) PINE.  *
* 01/20/2018  ELLO   - RESTOW FOR APPL180 WITH NEW FIELDS FOR ONEIRA   *
*                      RP-FINANCIAL-2 = TIAA INIT. PREMIUM
*                      RP-FINANCIAL-3 = CREF INIT. PREMIUM
* 11/16/19 L. SHU    - RESTOW FOR NEW FIELDS IN APPL180  CNTRSTRG & IISG
************************************************************************

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb410 extends BLNatBase
{
    // Data Areas
    private LdaAppl180 ldaAppl180;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Conversion_File;
    private DbsField pnd_Conversion_File_Pnd_T_Contract_Nbr;
    private DbsField pnd_Conversion_File_Pnd_C_Contract_Nbr;
    private DbsField pnd_Conversion_File_Pnd_Tiaa_Related_Ppg_Code;

    private DbsGroup pnd_Conversion_File__R_Field_1;
    private DbsField pnd_Conversion_File_Pnd_Ppg;
    private DbsField pnd_Conversion_File_Pnd_Ppg_Fill;
    private DbsField pnd_Conversion_File_Pnd_Omni_Plan_Number;
    private DbsField pnd_Conversion_File_Pnd_Omni_Sub_Plan;

    private DbsGroup pnd_Conversion_File__R_Field_2;
    private DbsField pnd_Conversion_File_Pnd_Omni_Lob_Sublob;
    private DbsField pnd_Conversion_File_Pnd_Decentralized_Payroll;
    private DbsField pnd_Conversion_File_Pnd_Subplan_Sequence_Id;
    private DbsField pnd_Conversion_File_Pnd_Filler1;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Fnd_Cnt;
    private DbsField pnd_Upd_Cnt;
    private DbsField pnd_Not_Fnd_Cnt;
    private DbsField pnd_Unique_Contracts_Cnt;
    private DbsField pnd_Wrong_Product_Cnt;
    private DbsField pnd_Et_Cnt;
    private DbsField pnd_Fnd_Y;
    private DbsField pnd_Present;

    private DbsGroup pnd_Old_Info;
    private DbsField pnd_Old_Info_Pnd_Old_Contract_Nbr;
    private DbsField pnd_Wk_Product;

    private DbsGroup pnd_Wk_Product__R_Field_3;
    private DbsField pnd_Wk_Product_Pnd_Wk_Lob;
    private DbsField pnd_Wk_Product_Pnd_Wk_Lob_Type;
    private DbsField pnd_Today;
    private DbsField pnd_Last_Run;
    private DbsField pnd_Process;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl180 = new LdaAppl180();
        registerRecord(ldaAppl180);
        registerRecord(ldaAppl180.getVw_acis_Reprint_Fl_View());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Conversion_File = localVariables.newGroupInRecord("pnd_Conversion_File", "#CONVERSION-FILE");
        pnd_Conversion_File_Pnd_T_Contract_Nbr = pnd_Conversion_File.newFieldInGroup("pnd_Conversion_File_Pnd_T_Contract_Nbr", "#T-CONTRACT-NBR", FieldType.STRING, 
            10);
        pnd_Conversion_File_Pnd_C_Contract_Nbr = pnd_Conversion_File.newFieldInGroup("pnd_Conversion_File_Pnd_C_Contract_Nbr", "#C-CONTRACT-NBR", FieldType.STRING, 
            10);
        pnd_Conversion_File_Pnd_Tiaa_Related_Ppg_Code = pnd_Conversion_File.newFieldInGroup("pnd_Conversion_File_Pnd_Tiaa_Related_Ppg_Code", "#TIAA-RELATED-PPG-CODE", 
            FieldType.STRING, 6);

        pnd_Conversion_File__R_Field_1 = pnd_Conversion_File.newGroupInGroup("pnd_Conversion_File__R_Field_1", "REDEFINE", pnd_Conversion_File_Pnd_Tiaa_Related_Ppg_Code);
        pnd_Conversion_File_Pnd_Ppg = pnd_Conversion_File__R_Field_1.newFieldInGroup("pnd_Conversion_File_Pnd_Ppg", "#PPG", FieldType.STRING, 4);
        pnd_Conversion_File_Pnd_Ppg_Fill = pnd_Conversion_File__R_Field_1.newFieldInGroup("pnd_Conversion_File_Pnd_Ppg_Fill", "#PPG-FILL", FieldType.STRING, 
            2);
        pnd_Conversion_File_Pnd_Omni_Plan_Number = pnd_Conversion_File.newFieldInGroup("pnd_Conversion_File_Pnd_Omni_Plan_Number", "#OMNI-PLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Conversion_File_Pnd_Omni_Sub_Plan = pnd_Conversion_File.newFieldInGroup("pnd_Conversion_File_Pnd_Omni_Sub_Plan", "#OMNI-SUB-PLAN", FieldType.STRING, 
            6);

        pnd_Conversion_File__R_Field_2 = pnd_Conversion_File.newGroupInGroup("pnd_Conversion_File__R_Field_2", "REDEFINE", pnd_Conversion_File_Pnd_Omni_Sub_Plan);
        pnd_Conversion_File_Pnd_Omni_Lob_Sublob = pnd_Conversion_File__R_Field_2.newFieldInGroup("pnd_Conversion_File_Pnd_Omni_Lob_Sublob", "#OMNI-LOB-SUBLOB", 
            FieldType.STRING, 3);
        pnd_Conversion_File_Pnd_Decentralized_Payroll = pnd_Conversion_File__R_Field_2.newFieldInGroup("pnd_Conversion_File_Pnd_Decentralized_Payroll", 
            "#DECENTRALIZED-PAYROLL", FieldType.STRING, 2);
        pnd_Conversion_File_Pnd_Subplan_Sequence_Id = pnd_Conversion_File__R_Field_2.newFieldInGroup("pnd_Conversion_File_Pnd_Subplan_Sequence_Id", "#SUBPLAN-SEQUENCE-ID", 
            FieldType.STRING, 1);
        pnd_Conversion_File_Pnd_Filler1 = pnd_Conversion_File.newFieldInGroup("pnd_Conversion_File_Pnd_Filler1", "#FILLER1", FieldType.STRING, 42);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.INTEGER, 4);
        pnd_Fnd_Cnt = localVariables.newFieldInRecord("pnd_Fnd_Cnt", "#FND-CNT", FieldType.INTEGER, 4);
        pnd_Upd_Cnt = localVariables.newFieldInRecord("pnd_Upd_Cnt", "#UPD-CNT", FieldType.INTEGER, 4);
        pnd_Not_Fnd_Cnt = localVariables.newFieldInRecord("pnd_Not_Fnd_Cnt", "#NOT-FND-CNT", FieldType.INTEGER, 4);
        pnd_Unique_Contracts_Cnt = localVariables.newFieldInRecord("pnd_Unique_Contracts_Cnt", "#UNIQUE-CONTRACTS-CNT", FieldType.INTEGER, 4);
        pnd_Wrong_Product_Cnt = localVariables.newFieldInRecord("pnd_Wrong_Product_Cnt", "#WRONG-PRODUCT-CNT", FieldType.INTEGER, 4);
        pnd_Et_Cnt = localVariables.newFieldInRecord("pnd_Et_Cnt", "#ET-CNT", FieldType.INTEGER, 4);
        pnd_Fnd_Y = localVariables.newFieldInRecord("pnd_Fnd_Y", "#FND-Y", FieldType.BOOLEAN, 1);
        pnd_Present = localVariables.newFieldInRecord("pnd_Present", "#PRESENT", FieldType.BOOLEAN, 1);

        pnd_Old_Info = localVariables.newGroupInRecord("pnd_Old_Info", "#OLD-INFO");
        pnd_Old_Info_Pnd_Old_Contract_Nbr = pnd_Old_Info.newFieldInGroup("pnd_Old_Info_Pnd_Old_Contract_Nbr", "#OLD-CONTRACT-NBR", FieldType.STRING, 10);
        pnd_Wk_Product = localVariables.newFieldInRecord("pnd_Wk_Product", "#WK-PRODUCT", FieldType.STRING, 2);

        pnd_Wk_Product__R_Field_3 = localVariables.newGroupInRecord("pnd_Wk_Product__R_Field_3", "REDEFINE", pnd_Wk_Product);
        pnd_Wk_Product_Pnd_Wk_Lob = pnd_Wk_Product__R_Field_3.newFieldInGroup("pnd_Wk_Product_Pnd_Wk_Lob", "#WK-LOB", FieldType.STRING, 1);
        pnd_Wk_Product_Pnd_Wk_Lob_Type = pnd_Wk_Product__R_Field_3.newFieldInGroup("pnd_Wk_Product_Pnd_Wk_Lob_Type", "#WK-LOB-TYPE", FieldType.STRING, 
            1);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.DATE);
        pnd_Last_Run = localVariables.newFieldInRecord("pnd_Last_Run", "#LAST-RUN", FieldType.DATE);
        pnd_Process = localVariables.newFieldInRecord("pnd_Process", "#PROCESS", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAppl180.initializeValues();

        localVariables.reset();
        pnd_Process.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb410() throws Exception
    {
        super("Appb410");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB410", onError);
        getReports().atTopOfPage(atTopEventRpt0, 0);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 80 PS = 52 ZP = ON;//Natural: FORMAT ( 1 ) LS = 80 PS = 52 ZP = ON;//Natural: FORMAT ( 2 ) LS = 80 PS = 52 ZP = ON
        //*  READ CONVERSION FILE - DUPLICATE RECORDS WERE REMOVED IN PRIOR STEP
        //*  -------------------------------------------------------------------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CONVERSION-FILE
        while (condition(getWorkFiles().read(1, pnd_Conversion_File)))
        {
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            //*  DETERMINE IF FURTHER PROCESSING IS REQUIRED
            //*  BASED ON THE DATE OF THE LAST RUN
            //*  -------------------------------------------
            if (condition(pnd_Process.equals(false)))                                                                                                                     //Natural: IF #PROCESS = FALSE
            {
                if (condition(pnd_Conversion_File_Pnd_T_Contract_Nbr.equals("     AAA")))                                                                                 //Natural: IF #T-CONTRACT-NBR = '     AAA'
                {
                    pnd_Last_Run.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Conversion_File_Pnd_C_Contract_Nbr);                                                   //Natural: MOVE EDITED #C-CONTRACT-NBR TO #LAST-RUN ( EM = YYYYMMDD )
                    pnd_Today.setValue(Global.getDATX());                                                                                                                 //Natural: MOVE *DATX TO #TODAY
                    getReports().write(0, "Today:",pnd_Today,NEWLINE,"Prior run date:",pnd_Last_Run,NEWLINE,NEWLINE);                                                     //Natural: WRITE 'Today:' #TODAY / 'Prior run date:' #LAST-RUN //
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Today.greater(pnd_Last_Run)))                                                                                                       //Natural: IF #TODAY > #LAST-RUN
                    {
                        getReports().write(0, "Data in file was created prior to today.",NEWLINE,"Processing file...");                                                   //Natural: WRITE 'Data in file was created prior to today.' / 'Processing file...'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Process.setValue(true);                                                                                                                       //Natural: ASSIGN #PROCESS := TRUE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "Data in file was not created prior to today.",NEWLINE,"Processing will not continue.");                                    //Natural: WRITE 'Data in file was not created prior to today.' / 'Processing will not continue.'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*        PERFORM WRITE-TO-SHARE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "Could not determine last run date. Terminating!!!");                                                                           //Natural: WRITE 'Could not determine last run date. Terminating!!!'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(16);  if (true) return;                                                                                                             //Natural: TERMINATE 16
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Conversion_File_Pnd_T_Contract_Nbr.notEquals(pnd_Old_Info_Pnd_Old_Contract_Nbr)))                                                           //Natural: IF #T-CONTRACT-NBR NE #OLD-CONTRACT-NBR
            {
                pnd_Unique_Contracts_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #UNIQUE-CONTRACTS-CNT
                if (condition(pnd_Present.equals(true) && pnd_Fnd_Y.equals(false)))                                                                                       //Natural: IF #PRESENT = TRUE AND #FND-Y = FALSE
                {
                    //*  11/16/04
                    pnd_Wrong_Product_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #WRONG-PRODUCT-CNT
                    getReports().write(1, "CONTRACT PRESENT-NO PRODUCT MATCH:",pnd_Old_Info_Pnd_Old_Contract_Nbr);                                                        //Natural: WRITE ( 1 ) 'CONTRACT PRESENT-NO PRODUCT MATCH:' #OLD-CONTRACT-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Fnd_Y.reset();                                                                                                                                        //Natural: RESET #FND-Y #PRESENT
                pnd_Present.reset();
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Fnd_Y.equals(false)))                                                                                                                       //Natural: IF #FND-Y = FALSE
            {
                //*  FIND CONTRACT NUMBER ON THE REPRINT FILE
                //*  ----------------------------------------
                ldaAppl180.getVw_acis_Reprint_Fl_View().startDatabaseFind                                                                                                 //Natural: FIND ACIS-REPRINT-FL-VIEW WITH RP-TIAA-CONTR = #T-CONTRACT-NBR
                (
                "F1",
                new Wc[] { new Wc("RP_TIAA_CONTR", "=", pnd_Conversion_File_Pnd_T_Contract_Nbr, WcType.WITH) }
                );
                F1:
                while (condition(ldaAppl180.getVw_acis_Reprint_Fl_View().readNextRow("F1", true)))
                {
                    ldaAppl180.getVw_acis_Reprint_Fl_View().setIfNotFoundControlFlag(false);
                    if (condition(ldaAppl180.getVw_acis_Reprint_Fl_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORD FOUND
                    {
                        pnd_Not_Fnd_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #NOT-FND-CNT
                        getReports().write(1, "     CONTRACT NOT ON REPRINT FILE:",pnd_Conversion_File_Pnd_T_Contract_Nbr);                                               //Natural: WRITE ( 1 ) '     CONTRACT NOT ON REPRINT FILE:' #T-CONTRACT-NBR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("F1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break F1;                                                                                                                               //Natural: ESCAPE BOTTOM ( F1. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Fnd_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #FND-CNT
                    pnd_Present.setValue(true);                                                                                                                           //Natural: ASSIGN #PRESENT := TRUE
                    pnd_Wk_Product_Pnd_Wk_Lob.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob());                                                                      //Natural: MOVE RP-LOB TO #WK-LOB
                    pnd_Wk_Product_Pnd_Wk_Lob_Type.setValue(ldaAppl180.getAcis_Reprint_Fl_View_Rp_Lob_Type());                                                            //Natural: MOVE RP-LOB-TYPE TO #WK-LOB-TYPE
                    //*  MATCH PRODUCTS NOW THAT CONTRACT IS FOUND
                    //*  -----------------------------------------
                    short decideConditionsMet619 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE #OMNI-LOB-SUBLOB;//Natural: VALUE 'RA6'
                    if (condition((pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("RA6"))))
                    {
                        decideConditionsMet619++;
                        if (condition(pnd_Wk_Product.equals("D2")))                                                                                                       //Natural: IF #WK-PRODUCT = 'D2'
                        {
                            pnd_Fnd_Y.setValue(true);                                                                                                                     //Natural: ASSIGN #FND-Y := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'GR1'
                    else if (condition((pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("GR1"))))
                    {
                        decideConditionsMet619++;
                        if (condition(pnd_Wk_Product.equals("D7")))                                                                                                       //Natural: IF #WK-PRODUCT = 'D7'
                        {
                            pnd_Fnd_Y.setValue(true);                                                                                                                     //Natural: ASSIGN #FND-Y := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'SR1'
                    else if (condition((pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("SR1"))))
                    {
                        decideConditionsMet619++;
                        if (condition(pnd_Wk_Product.equals("S2")))                                                                                                       //Natural: IF #WK-PRODUCT = 'S2'
                        {
                            pnd_Fnd_Y.setValue(true);                                                                                                                     //Natural: ASSIGN #FND-Y := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'GS1', 'GA1'
                    else if (condition((pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("GS1") || pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("GA1"))))
                    {
                        decideConditionsMet619++;
                        if (condition(pnd_Wk_Product.equals("S3")))                                                                                                       //Natural: IF #WK-PRODUCT = 'S3'
                        {
                            pnd_Fnd_Y.setValue(true);                                                                                                                     //Natural: ASSIGN #FND-Y := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'RL1'
                    else if (condition((pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("RL1"))))
                    {
                        decideConditionsMet619++;
                        if (condition(pnd_Wk_Product.equals("D6")))                                                                                                       //Natural: IF #WK-PRODUCT = 'D6'
                        {
                            pnd_Fnd_Y.setValue(true);                                                                                                                     //Natural: ASSIGN #FND-Y := TRUE
                            //*  ADDED IRA - CALSTRS KG
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'IR1'
                    else if (condition((pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("IR1"))))
                    {
                        decideConditionsMet619++;
                        if (condition(pnd_Wk_Product.equals("I4")))                                                                                                       //Natural: IF #WK-PRODUCT = 'I4'
                        {
                            pnd_Fnd_Y.setValue(true);                                                                                                                     //Natural: ASSIGN #FND-Y := TRUE
                            //*  ADDED IRA -CALSTRS KG
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'IR2'
                    else if (condition((pnd_Conversion_File_Pnd_Omni_Lob_Sublob.equals("IR2"))))
                    {
                        decideConditionsMet619++;
                        if (condition(pnd_Wk_Product.equals("I3")))                                                                                                       //Natural: IF #WK-PRODUCT = 'I3'
                        {
                            pnd_Fnd_Y.setValue(true);                                                                                                                     //Natural: ASSIGN #FND-Y := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Wrong_Product_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #WRONG-PRODUCT-CNT
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  F1.
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CONTRACTS FOUND AND PRODUCTS MATCH - UPDATE THESE RECS
                //*  ------------------------------------------------------
                if (condition(pnd_Fnd_Y.equals(true)))                                                                                                                    //Natural: IF #FND-Y = TRUE
                {
                    pnd_Upd_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #UPD-CNT
                    pnd_Et_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #ET-CNT
                    G1:                                                                                                                                                   //Natural: GET ACIS-REPRINT-FL-VIEW *ISN ( F1. )
                    ldaAppl180.getVw_acis_Reprint_Fl_View().readByID(ldaAppl180.getVw_acis_Reprint_Fl_View().getAstISN("ReadWork01"), "G1");
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Plan_No().setValue(pnd_Conversion_File_Pnd_Omni_Plan_Number);                                              //Natural: MOVE #OMNI-PLAN-NUMBER TO RP-SGRD-PLAN-NO
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Sgrd_Subplan_No().setValue(pnd_Conversion_File_Pnd_Omni_Sub_Plan);                                              //Natural: MOVE #OMNI-SUB-PLAN TO RP-SGRD-SUBPLAN-NO
                    ldaAppl180.getAcis_Reprint_Fl_View_Rp_Coll_Code().setValue("SGRD");                                                                                   //Natural: MOVE 'SGRD' TO RP-COLL-CODE
                    ldaAppl180.getVw_acis_Reprint_Fl_View().updateDBRow("G1");                                                                                            //Natural: UPDATE ( G1. )
                    if (condition(pnd_Et_Cnt.greater(75)))                                                                                                                //Natural: IF #ET-CNT > 75
                    {
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Et_Cnt.reset();                                                                                                                               //Natural: RESET #ET-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Old_Info_Pnd_Old_Contract_Nbr.setValue(pnd_Conversion_File_Pnd_T_Contract_Nbr);                                                                           //Natural: MOVE #T-CONTRACT-NBR TO #OLD-CONTRACT-NBR
            pnd_Wk_Product.reset();                                                                                                                                       //Natural: RESET #WK-PRODUCT
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-SHARE
        sub_Write_To_Share();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "NUMBER OF CONTRACTS READ:             ",pnd_Read_Cnt,NEWLINE,"(includes control record)             ",NEWLINE,"NUMBER OF UNIQUE CONTRACTS ON FILE:   ", //Natural: WRITE 'NUMBER OF CONTRACTS READ:             ' #READ-CNT / '(includes control record)             ' / 'NUMBER OF UNIQUE CONTRACTS ON FILE:   ' #UNIQUE-CONTRACTS-CNT / 'CONTRACTS NOT ON REPRINT FILE:        ' #NOT-FND-CNT / '*'/ 'NUMBER OF CONTRACTS FOUND:            ' #FND-CNT / 'CONTRACTS FOR UPDATE:                 ' #UPD-CNT / '(UPDATED WITH PLAN/SUBPLAN)           ' / 'CONTRACT PRESENT BUT PRODUCT MISMATCH:' #WRONG-PRODUCT-CNT / '(NOT UPDATED WITH PLAN/SUBPLAN)       ' / '*'/
            pnd_Unique_Contracts_Cnt,NEWLINE,"CONTRACTS NOT ON REPRINT FILE:        ",pnd_Not_Fnd_Cnt,NEWLINE,"*",NEWLINE,"NUMBER OF CONTRACTS FOUND:            ",
            pnd_Fnd_Cnt,NEWLINE,"CONTRACTS FOR UPDATE:                 ",pnd_Upd_Cnt,NEWLINE,"(UPDATED WITH PLAN/SUBPLAN)           ",NEWLINE,"CONTRACT PRESENT BUT PRODUCT MISMATCH:",
            pnd_Wrong_Product_Cnt,NEWLINE,"(NOT UPDATED WITH PLAN/SUBPLAN)       ",NEWLINE,"*",NEWLINE);
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-SHARE
    }
    private void sub_Write_To_Share() throws Exception                                                                                                                    //Natural: WRITE-TO-SHARE
    {
        if (BLNatReinput.isReinput()) return;

        //*  **********************************************************************
        //*  MOVE PARAMETERS TO NEW SHARE FILE TO
        //*  CONTROL PROCESSING FOR NEXT RUN
        //*  ------------------------------------
        pnd_Conversion_File.reset();                                                                                                                                      //Natural: RESET #CONVERSION-FILE
        pnd_Conversion_File_Pnd_T_Contract_Nbr.setValue("     AAA");                                                                                                      //Natural: MOVE '     AAA' TO #T-CONTRACT-NBR
        pnd_Conversion_File_Pnd_C_Contract_Nbr.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #C-CONTRACT-NBR
        getWorkFiles().write(2, false, pnd_Conversion_File);                                                                                                              //Natural: WRITE WORK FILE 2 #CONVERSION-FILE
        //*  (WRITE-TO-SHARE)
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(63),Global.getDATU(),NEWLINE,new ColumnSpacing(35),"EXCEPTION REPORT",NEWLINE,new         //Natural: WRITE ( 1 ) *PROGRAM 63X *DATU / 35X'EXCEPTION REPORT'/ 27X'MISSING OR UNMATCHED CONTRACTS'/
                        ColumnSpacing(27),"MISSING OR UNMATCHED CONTRACTS",NEWLINE);
                    getReports().write(1, NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE ( 1 ) //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, Global.getPROGRAM(),new ColumnSpacing(63),Global.getDATU(),NEWLINE);                                                            //Natural: WRITE *PROGRAM 63X *DATU /
                    getReports().write(0, NEWLINE,new ColumnSpacing(27),"UPDATE STATISTICS REPORT",NEWLINE);                                                              //Natural: WRITE / 27X'UPDATE STATISTICS REPORT' /
                    getReports().write(0, NEWLINE);                                                                                                                       //Natural: WRITE /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "PROGRAM:  ",Global.getPROGRAM(),NEWLINE,"LINE:     ",Global.getERROR_LINE(),NEWLINE,"ERROR:    ",Global.getERROR_NR(),NEWLINE,             //Natural: WRITE 'PROGRAM:  ' *PROGRAM / 'LINE:     ' *ERROR-LINE / 'ERROR:    ' *ERROR-NR / 'RECORD:   ' #READ-CNT / 'CONTRACT: ' #T-CONTRACT-NBR /
            "RECORD:   ",pnd_Read_Cnt,NEWLINE,"CONTRACT: ",pnd_Conversion_File_Pnd_T_Contract_Nbr,NEWLINE);
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=52 ZP=ON");
        Global.format(1, "LS=80 PS=52 ZP=ON");
        Global.format(2, "LS=80 PS=52 ZP=ON");
    }
}
