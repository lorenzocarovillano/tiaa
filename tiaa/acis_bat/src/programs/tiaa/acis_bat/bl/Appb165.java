/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:08:20 PM
**        * FROM NATURAL PROGRAM : Appb165
************************************************************
**        * FILE NAME            : Appb165.java
**        * CLASS NAME           : Appb165
**        * INSTANCE NAME        : Appb165
************************************************************
************************************************************************
* PROGRAM  : APPB165 - ACIS/POST RHSP LETTER PROCESS                   *
* FUNCTION : READS RHSP CONTRACTS FROM PRAP FILE AND EXTRACTS THEM TO  *
*            FILE TO GENERATE WELCOME AND ENROLLMENT LETTERS       *
*            UPDATE RELEASE INDICATOR ON EXTRACTED RECORDS
* CREATED  : 09/20/08 N. CVETKOVIC (CLONED/EXTRACTED FROM APPPB100)
*
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES
**--------   ----------  -----------------------------------------------
**06/14/2017  MITRAPU   - PIN EXPANSION CHANGES.(C425939)       PINE.***
**--------   ----------  -----------------------------------------------
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Appb165 extends BLNatBase
{
    // Data Areas
    private LdaAppl165 ldaAppl165;
    private LdaScil1080 ldaScil1080;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_annty_Actvty_Prap_View;
    private DbsField annty_Actvty_Prap_View_Ap_G_Key;
    private DbsField annty_Actvty_Prap_View_Ap_G_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Soc_Sec;
    private DbsField annty_Actvty_Prap_View_Ap_Dob;

    private DbsGroup annty_Actvty_Prap_View_Ap_Tiaa_Contr;
    private DbsField annty_Actvty_Prap_View_App_Pref;
    private DbsField annty_Actvty_Prap_View_App_Cont;
    private DbsField annty_Actvty_Prap_View_Ap_Mail_Zip;
    private DbsField annty_Actvty_Prap_View_Ap_Record_Type;
    private DbsField annty_Actvty_Prap_View_Ap_Release_Ind;
    private DbsField annty_Actvty_Prap_View_Ap_Orig_Issue_State;
    private DbsField annty_Actvty_Prap_View_Ap_Lob;
    private DbsField annty_Actvty_Prap_View_Ap_Lob_Type;

    private DbsGroup annty_Actvty_Prap_View_Ap_Address_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Line;
    private DbsField annty_Actvty_Prap_View_Ap_City;
    private DbsField annty_Actvty_Prap_View_Ap_Current_State_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Annuity_Start_Date;

    private DbsGroup annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Last_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_First_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme;
    private DbsField annty_Actvty_Prap_View_Ap_Pin_Nbr;
    private DbsField annty_Actvty_Prap_View_Ap_Cor_Ind;

    private DbsGroup annty_Actvty_Prap_View_Ap_Addr_Info;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Txt;
    private DbsField annty_Actvty_Prap_View_Ap_Address_Dest_Name;
    private DbsField annty_Actvty_Prap_View_Ap_Zip_Code;
    private DbsField annty_Actvty_Prap_View_Ap_Tiaa_Cntrct;
    private DbsField annty_Actvty_Prap_View_Ap_Email_Address;
    private DbsField annty_Actvty_Prap_View_Ap_Phone_No;

    private DbsGroup annty_Actvty_Prap_View_Ap_Fund_Identifier;
    private DbsField annty_Actvty_Prap_View_Ap_Fund_Cde;
    private DbsField annty_Actvty_Prap_View_Ap_Allocation_Pct;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Plan_No;
    private DbsField annty_Actvty_Prap_View_Ap_Sgrd_Divsub;
    private DbsField pnd_Restart_Data;

    private DbsGroup pnd_Restart_Data__R_Field_1;
    private DbsField pnd_Restart_Data_Pnd_Restart_Program;
    private DbsField pnd_Restart_Data_Pnd_Restart_Ppg;
    private DbsField pnd_Restart_Data_Pnd_Restart_Plan;
    private DbsField pnd_Restart_Data_Pnd_Restart_Name_Key;
    private DbsField pnd_Restart_Data_Pnd_Restart_Tiaa_Contract;
    private DbsField pnd_Restart_Data_Pnd_Restart_Record_Nbr;

    private DbsGroup pnd_Restart_Data__R_Field_2;
    private DbsField pnd_Restart_Data_Pnd_Restart_Record_Nbr_A;

    private DbsGroup pnd_Restart_Data_Pnd_Restart_Date_Time;
    private DbsField pnd_Restart_Data_Pnd_Restart_Date;
    private DbsField pnd_Restart_Data_Pnd_Restart_Time;
    private DbsField pnd_Restart_Data_Pnd_Restart_Filler;
    private DbsField pnd_Restart;
    private DbsField pnd_Write_Count;
    private DbsField pnd_K;
    private DbsField pnd_Debug;
    private DbsField pnd_No_Update;
    private DbsField pnd_Print;
    private DbsField pnd_Have_Correct_File;
    private DbsField pnd_Max_Test_Records_To_Update;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAppl165 = new LdaAppl165();
        registerRecord(ldaAppl165);
        ldaScil1080 = new LdaScil1080();
        registerRecord(ldaScil1080);

        // Local Variables
        localVariables = new DbsRecord();

        vw_annty_Actvty_Prap_View = new DataAccessProgramView(new NameInfo("vw_annty_Actvty_Prap_View", "ANNTY-ACTVTY-PRAP-VIEW"), "ANNTY_ACTVTY_PRAP_12", 
            "ACIS_ANNTY_PRAP", DdmPeriodicGroups.getInstance().getGroups("ANNTY_ACTVTY_PRAP_12"));
        annty_Actvty_Prap_View_Ap_G_Key = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Key", "AP-G-KEY", FieldType.PACKED_DECIMAL, 
            4, RepeatingFieldStrategy.None, "AP_G_KEY");
        annty_Actvty_Prap_View_Ap_G_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_G_Ind", "AP-G-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "AP_G_IND");
        annty_Actvty_Prap_View_Ap_Soc_Sec = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Soc_Sec", "AP-SOC-SEC", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "AP_SOC_SEC");
        annty_Actvty_Prap_View_Ap_Dob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Dob", "AP-DOB", FieldType.PACKED_DECIMAL, 
            8, RepeatingFieldStrategy.None, "AP_DOB");

        annty_Actvty_Prap_View_Ap_Tiaa_Contr = vw_annty_Actvty_Prap_View.getRecord().newGroupInGroup("ANNTY_ACTVTY_PRAP_VIEW_AP_TIAA_CONTR", "AP-TIAA-CONTR");
        annty_Actvty_Prap_View_App_Pref = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Pref", "APP-PREF", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "APP_PREF");
        annty_Actvty_Prap_View_App_Cont = annty_Actvty_Prap_View_Ap_Tiaa_Contr.newFieldInGroup("annty_Actvty_Prap_View_App_Cont", "APP-CONT", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "APP_CONT");
        annty_Actvty_Prap_View_Ap_Mail_Zip = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Mail_Zip", "AP-MAIL-ZIP", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "AP_MAIL_ZIP");
        annty_Actvty_Prap_View_Ap_Record_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Record_Type", "AP-RECORD-TYPE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RECORD_TYPE");
        annty_Actvty_Prap_View_Ap_Release_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Release_Ind", "AP-RELEASE-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "AP_RELEASE_IND");
        annty_Actvty_Prap_View_Ap_Orig_Issue_State = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Orig_Issue_State", 
            "AP-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_ORIG_ISSUE_STATE");
        annty_Actvty_Prap_View_Ap_Lob = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob", "AP-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_LOB");
        annty_Actvty_Prap_View_Ap_Lob.setDdmHeader("LOB");
        annty_Actvty_Prap_View_Ap_Lob_Type = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Lob_Type", "AP-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AP_LOB_TYPE");
        annty_Actvty_Prap_View_Ap_Lob_Type.setDdmHeader("LOB/TYPE");

        annty_Actvty_Prap_View_Ap_Address_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Address_Info", 
            "AP-ADDRESS-INFO", new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_Address_Line = annty_Actvty_Prap_View_Ap_Address_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Line", "AP-ADDRESS-LINE", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_LINE", "ACIS_ANNTY_PRAP_AP_ADDRESS_INFO");
        annty_Actvty_Prap_View_Ap_City = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_City", "AP-CITY", FieldType.STRING, 
            27, RepeatingFieldStrategy.None, "AP_CITY");
        annty_Actvty_Prap_View_Ap_Current_State_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Current_State_Code", 
            "AP-CURRENT-STATE-CODE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "AP_CURRENT_STATE_CODE");
        annty_Actvty_Prap_View_Ap_Annuity_Start_Date = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Annuity_Start_Date", 
            "AP-ANNUITY-START-DATE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_ANNUITY_START_DATE");

        annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct", 
            "AP-MAXIMUM-ALLOC-PCT", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct", 
            "AP-MAX-ALLOC-PCT", FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign = annty_Actvty_Prap_View_Ap_Maximum_Alloc_Pct.newFieldInGroup("annty_Actvty_Prap_View_Ap_Max_Alloc_Pct_Sign", 
            "AP-MAX-ALLOC-PCT-SIGN", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_MAX_ALLOC_PCT_SIGN", "ACIS_ANNTY_PRAP_AP_MAXIMUM_ALLOC_PCT");
        annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Prfx_Nme", "AP-COR-PRFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_PRFX_NME");
        annty_Actvty_Prap_View_Ap_Cor_Last_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Last_Nme", "AP-COR-LAST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_LAST_NME");
        annty_Actvty_Prap_View_Ap_Cor_First_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_First_Nme", "AP-COR-FIRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_FIRST_NME");
        annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Mddle_Nme", "AP-COR-MDDLE-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "AP_COR_MDDLE_NME");
        annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Sffx_Nme", "AP-COR-SFFX-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "AP_COR_SFFX_NME");
        annty_Actvty_Prap_View_Ap_Pin_Nbr = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Pin_Nbr", "AP-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "AP_PIN_NBR");
        annty_Actvty_Prap_View_Ap_Cor_Ind = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Cor_Ind", "AP-COR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AP_COR_IND");

        annty_Actvty_Prap_View_Ap_Addr_Info = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Addr_Info", "AP-ADDR-INFO", 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Txt = annty_Actvty_Prap_View_Ap_Addr_Info.newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Txt", "AP-ADDRESS-TXT", 
            FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ADDRESS_TXT", "ACIS_ANNTY_PRAP_AP_ADDR_INFO");
        annty_Actvty_Prap_View_Ap_Address_Dest_Name = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Address_Dest_Name", 
            "AP-ADDRESS-DEST-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, "AP_ADDRESS_DEST_NAME");
        annty_Actvty_Prap_View_Ap_Zip_Code = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Zip_Code", "AP-ZIP-CODE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "AP_ZIP_CODE");
        annty_Actvty_Prap_View_Ap_Tiaa_Cntrct = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Tiaa_Cntrct", "AP-TIAA-CNTRCT", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "AP_TIAA_CNTRCT");
        annty_Actvty_Prap_View_Ap_Email_Address = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Email_Address", "AP-EMAIL-ADDRESS", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "AP_EMAIL_ADDRESS");
        annty_Actvty_Prap_View_Ap_Phone_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Phone_No", "AP-PHONE-NO", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "AP_PHONE_NO");

        annty_Actvty_Prap_View_Ap_Fund_Identifier = vw_annty_Actvty_Prap_View.getRecord().newGroupArrayInGroup("annty_Actvty_Prap_View_Ap_Fund_Identifier", 
            "AP-FUND-IDENTIFIER", new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Fund_Cde = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Fund_Cde", "AP-FUND-CDE", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_FUND_CDE", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Allocation_Pct = annty_Actvty_Prap_View_Ap_Fund_Identifier.newFieldInGroup("annty_Actvty_Prap_View_Ap_Allocation_Pct", 
            "AP-ALLOCATION-PCT", FieldType.NUMERIC, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "AP_ALLOCATION_PCT", "ACIS_ANNTY_PRAP_AP_FUND_IDENTIFIER");
        annty_Actvty_Prap_View_Ap_Sgrd_Plan_No = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Plan_No", "AP-SGRD-PLAN-NO", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "AP_SGRD_PLAN_NO");
        annty_Actvty_Prap_View_Ap_Sgrd_Divsub = vw_annty_Actvty_Prap_View.getRecord().newFieldInGroup("annty_Actvty_Prap_View_Ap_Sgrd_Divsub", "AP-SGRD-DIVSUB", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "AP_SGRD_DIVSUB");
        registerRecord(vw_annty_Actvty_Prap_View);

        pnd_Restart_Data = localVariables.newFieldInRecord("pnd_Restart_Data", "#RESTART-DATA", FieldType.STRING, 79);

        pnd_Restart_Data__R_Field_1 = localVariables.newGroupInRecord("pnd_Restart_Data__R_Field_1", "REDEFINE", pnd_Restart_Data);
        pnd_Restart_Data_Pnd_Restart_Program = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Program", "#RESTART-PROGRAM", 
            FieldType.STRING, 8);
        pnd_Restart_Data_Pnd_Restart_Ppg = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Ppg", "#RESTART-PPG", FieldType.STRING, 
            4);
        pnd_Restart_Data_Pnd_Restart_Plan = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Plan", "#RESTART-PLAN", FieldType.STRING, 
            6);
        pnd_Restart_Data_Pnd_Restart_Name_Key = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Name_Key", "#RESTART-NAME-KEY", 
            FieldType.STRING, 7);
        pnd_Restart_Data_Pnd_Restart_Tiaa_Contract = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Tiaa_Contract", "#RESTART-TIAA-CONTRACT", 
            FieldType.STRING, 8);
        pnd_Restart_Data_Pnd_Restart_Record_Nbr = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Record_Nbr", "#RESTART-RECORD-NBR", 
            FieldType.NUMERIC, 9);

        pnd_Restart_Data__R_Field_2 = pnd_Restart_Data__R_Field_1.newGroupInGroup("pnd_Restart_Data__R_Field_2", "REDEFINE", pnd_Restart_Data_Pnd_Restart_Record_Nbr);
        pnd_Restart_Data_Pnd_Restart_Record_Nbr_A = pnd_Restart_Data__R_Field_2.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Record_Nbr_A", "#RESTART-RECORD-NBR-A", 
            FieldType.STRING, 9);

        pnd_Restart_Data_Pnd_Restart_Date_Time = pnd_Restart_Data__R_Field_1.newGroupInGroup("pnd_Restart_Data_Pnd_Restart_Date_Time", "#RESTART-DATE-TIME");
        pnd_Restart_Data_Pnd_Restart_Date = pnd_Restart_Data_Pnd_Restart_Date_Time.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Date", "#RESTART-DATE", 
            FieldType.STRING, 10);
        pnd_Restart_Data_Pnd_Restart_Time = pnd_Restart_Data_Pnd_Restart_Date_Time.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Time", "#RESTART-TIME", 
            FieldType.STRING, 5);
        pnd_Restart_Data_Pnd_Restart_Filler = pnd_Restart_Data__R_Field_1.newFieldInGroup("pnd_Restart_Data_Pnd_Restart_Filler", "#RESTART-FILLER", FieldType.STRING, 
            4);
        pnd_Restart = localVariables.newFieldInRecord("pnd_Restart", "#RESTART", FieldType.BOOLEAN, 1);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_No_Update = localVariables.newFieldInRecord("pnd_No_Update", "#NO-UPDATE", FieldType.BOOLEAN, 1);
        pnd_Print = localVariables.newFieldInRecord("pnd_Print", "#PRINT", FieldType.BOOLEAN, 1);
        pnd_Have_Correct_File = localVariables.newFieldInRecord("pnd_Have_Correct_File", "#HAVE-CORRECT-FILE", FieldType.BOOLEAN, 1);
        pnd_Max_Test_Records_To_Update = localVariables.newFieldInRecord("pnd_Max_Test_Records_To_Update", "#MAX-TEST-RECORDS-TO-UPDATE", FieldType.PACKED_DECIMAL, 
            3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_annty_Actvty_Prap_View.reset();

        ldaAppl165.initializeValues();
        ldaScil1080.initializeValues();

        localVariables.reset();
        pnd_Have_Correct_File.setInitialValue(false);
        pnd_Max_Test_Records_To_Update.setInitialValue(100);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Appb165() throws Exception
    {
        super("Appb165");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("APPB165", onError);
        setupReports();
        //*  #DEBUG :=  TRUE
        //*   #NO-UPDATE := TRUE
        pnd_Print.setValue(true);                                                                                                                                         //Natural: ASSIGN #PRINT := TRUE
        //*  PROGRAM WILL 'auto restart                                                                                                                                   //Natural: FORMAT ( 1 ) LS = 80 PS = 52
        //*                                                                                                                                                               //Natural: GET TRANSACTION DATA #RESTART-DATA
        if (condition(pnd_Restart_Data.notEquals(" ") && ! (pnd_Debug.getBoolean())))                                                                                     //Natural: IF #RESTART-DATA NE ' ' AND NOT #DEBUG
        {
            pnd_Restart.setValue(true);                                                                                                                                   //Natural: ASSIGN #RESTART := TRUE
            getReports().write(0, "RESTART DATA - LAST RECORD STORED",NEWLINE,NEWLINE,"          PROGRAM:",pnd_Restart_Data_Pnd_Restart_Program,NEWLINE,"LAST ET DATE/TIME:",pnd_Restart_Data_Pnd_Restart_Date,pnd_Restart_Data_Pnd_Restart_Time,NEWLINE,"   LAST ET RECORD:",pnd_Restart_Data_Pnd_Restart_Record_Nbr,  //Natural: WRITE 'RESTART DATA - LAST RECORD STORED' // '          PROGRAM:' #RESTART-PROGRAM / 'LAST ET DATE/TIME:' #RESTART-DATE #RESTART-TIME / '   LAST ET RECORD:' #RESTART-RECORD-NBR ( EM = ZZZZZZZZ9 ) / '             PLAN:' #RESTART-PLAN / '    TIIA CONTRACT:' #RESTART-TIAA-CONTRACT
                new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,"             PLAN:",pnd_Restart_Data_Pnd_Restart_Plan,NEWLINE,"    TIIA CONTRACT:",pnd_Restart_Data_Pnd_Restart_Tiaa_Contract);
            if (Global.isEscape()) return;
            //*   / '         NAME KEY:' #RESTART-NAME-KEY
            pnd_Restart.setValue(true);                                                                                                                                   //Natural: ASSIGN #RESTART := TRUE
                                                                                                                                                                          //Natural: PERFORM COPY-EXTRACTED-RECORDS
            sub_Copy_Extracted_Records();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Restart_Data_Pnd_Restart_Program.setValue(Global.getPROGRAM());                                                                                               //Natural: ASSIGN #RESTART-PROGRAM := *PROGRAM
        vw_annty_Actvty_Prap_View.startDatabaseFind                                                                                                                       //Natural: FIND ANNTY-ACTVTY-PRAP-VIEW WITH AP-RELEASE-IND EQ 3 WHERE AP-RECORD-TYPE EQ 1 AND AP-LOB = 'D' AND AP-LOB-TYPE = 'B'
        (
        "FINDUPD",
        new Wc[] { new Wc("AP_RELEASE_IND", "=", 3, WcType.WITH) ,
        new Wc("AP_RECORD_TYPE", "=", 1, WCAndOr.And, WcType.WHERE) ,
        new Wc("AP_LOB", "=", "D", WCAndOr.And, WcType.WHERE) ,
        new Wc("AP_LOB_TYPE", "=", "B", WcType.WHERE) }
        );
        FINDUPD:
        while (condition(vw_annty_Actvty_Prap_View.readNextRow("FINDUPD", true)))
        {
            vw_annty_Actvty_Prap_View.setIfNotFoundControlFlag(false);
            if (condition(vw_annty_Actvty_Prap_View.getAstCOUNTER().equals(0)))                                                                                           //Natural: IF NO RECORDS FOUND
            {
                getReports().display(0, "********  NO PRAP RECORDS FOUND  ********");                                                                                     //Natural: DISPLAY '********  NO PRAP RECORDS FOUND  ********'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM WRITE-EXTRACT-RECORD
            sub_Write_Extract_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-PRAP-RECORD
            sub_Update_Prap_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Write_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #WRITE-COUNT
            if (condition(pnd_Debug.getBoolean() && pnd_Write_Count.greater(pnd_Max_Test_Records_To_Update)))                                                             //Natural: IF #DEBUG AND #WRITE-COUNT > #MAX-TEST-RECORDS-TO-UPDATE
            {
                getReports().write(0, "Max records updated","=",pnd_Max_Test_Records_To_Update);                                                                          //Natural: WRITE 'Max records updated' '=' #MAX-TEST-RECORDS-TO-UPDATE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, " terminating run in debug mode");                                                                                                  //Natural: WRITE ' terminating run in debug mode'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FINDUPD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FINDUPD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE 'end of run' STATISTICS'
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *PROGRAM 9X 'ACIS/POST RHSP Extract Process' 8X *DATX ( EM = MM/DD/YYYY ) *TIMX ( EM = HH:II ) / 25X 'OMNI STATISTICS REPORT' /
        getReports().write(1, "Extract Records Written:",pnd_Write_Count, new ReportEditMask ("ZZZ,ZZ9"));                                                                //Natural: WRITE ( 1 ) 'Extract Records Written:' #WRITE-COUNT ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXTRACT-RECORD
        //*  APPL165
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COPY-EXTRACTED-RECORDS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PRAP-RECORD
        //*  JRB2
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Write_Extract_Record() throws Exception                                                                                                              //Natural: WRITE-EXTRACT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAppl165.getAppl165_Tiaa_Contract().setValue(annty_Actvty_Prap_View_Ap_Tiaa_Cntrct);                                                                            //Natural: MOVE AP-TIAA-CNTRCT TO TIAA-CONTRACT
        ldaAppl165.getAppl165_Pin().setValue(annty_Actvty_Prap_View_Ap_Pin_Nbr);                                                                                          //Natural: MOVE AP-PIN-NBR TO PIN
        ldaAppl165.getAppl165_Ssn().setValue(annty_Actvty_Prap_View_Ap_Soc_Sec);                                                                                          //Natural: MOVE AP-SOC-SEC TO SSN
        ldaAppl165.getAppl165_Birthdate_Mmddyyyy_Num().setValue(annty_Actvty_Prap_View_Ap_Dob);                                                                           //Natural: MOVE AP-DOB TO BIRTHDATE-MMDDYYYY-NUM
        ldaAppl165.getAppl165_Alloc_Pct().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Allocation_Pct.getValue(1,":",100));                                     //Natural: MOVE AP-ALLOCATION-PCT ( 1:100 ) TO ALLOC-PCT ( 1:100 )
        ldaAppl165.getAppl165_Alloc_Fund_Cde().getValue(1,":",100).setValue(annty_Actvty_Prap_View_Ap_Fund_Cde.getValue(1,":",100));                                      //Natural: MOVE AP-FUND-CDE ( 1:100 ) TO ALLOC-FUND-CDE ( 1:100 )
        ldaAppl165.getAppl165_Plan().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Plan_No);                                                                                    //Natural: MOVE AP-SGRD-PLAN-NO TO PLAN
        ldaAppl165.getAppl165_Divsub().setValue(annty_Actvty_Prap_View_Ap_Sgrd_Divsub);                                                                                   //Natural: MOVE AP-SGRD-DIVSUB TO DIVSUB
        ldaAppl165.getAppl165_Email_Address().setValue(annty_Actvty_Prap_View_Ap_Email_Address);                                                                          //Natural: MOVE AP-EMAIL-ADDRESS TO EMAIL-ADDRESS
        ldaAppl165.getAppl165_Address().getValue(1).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(1));                                                         //Natural: MOVE AP-ADDRESS-LINE ( 1 ) TO ADDRESS ( 1 )
        ldaAppl165.getAppl165_Address().getValue(2).setValue(annty_Actvty_Prap_View_Ap_Address_Line.getValue(2));                                                         //Natural: MOVE AP-ADDRESS-LINE ( 2 ) TO ADDRESS ( 2 )
        ldaAppl165.getAppl165_Address().getValue(3).setValue(annty_Actvty_Prap_View_Ap_City);                                                                             //Natural: MOVE AP-CITY TO ADDRESS ( 3 )
        pnd_K.reset();                                                                                                                                                    //Natural: RESET #K
        DbsUtil.examine(new ExamineSource(ldaScil1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue("*"),3,2), new ExamineSearch(annty_Actvty_Prap_View_Ap_Orig_Issue_State),  //Natural: EXAMINE SUBSTR ( #STATE-ZIP-IDX ( * ) ,3,2 ) FOR AP-ORIG-ISSUE-STATE GIVING INDEX #K
            new ExamineGivingIndex(pnd_K));
        if (condition(pnd_K.notEquals(getZero())))                                                                                                                        //Natural: IF #K NE 0
        {
            ldaAppl165.getAppl165_Address().getValue(4).setValue(ldaScil1080.getPnd_State_Zipcode_Array_Pnd_State_Zip_Idx().getValue(pnd_K).getSubstring(1,               //Natural: MOVE SUBSTR ( #STATE-ZIP-IDX ( #K ) ,1,2 ) TO ADDRESS ( 4 )
                2));
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE AP-ORIG-ISSUE-STATE TO ADDRESS(4)
        ldaAppl165.getAppl165_Zip().setValue(annty_Actvty_Prap_View_Ap_Mail_Zip);                                                                                         //Natural: MOVE AP-MAIL-ZIP TO ZIP
        ldaAppl165.getAppl165_Restart_Record_Number().setValue(pnd_Write_Count);                                                                                          //Natural: MOVE #WRITE-COUNT TO RESTART-RECORD-NUMBER
        pnd_Restart_Data_Pnd_Restart_Plan.setValue(ldaAppl165.getAppl165_Plan());                                                                                         //Natural: ASSIGN #RESTART-PLAN := APPL165.PLAN
        pnd_Restart_Data_Pnd_Restart_Tiaa_Contract.setValue(ldaAppl165.getAppl165_Tiaa_Contract());                                                                       //Natural: ASSIGN #RESTART-TIAA-CONTRACT := APPL165.TIAA-CONTRACT
        pnd_Restart_Data_Pnd_Restart_Record_Nbr.setValue(pnd_Write_Count);                                                                                                //Natural: ASSIGN #RESTART-RECORD-NBR := #WRITE-COUNT
        pnd_Restart_Data_Pnd_Restart_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                              //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #RESTART-DATE
        ldaAppl165.getAppl165_Restart_Date().setValue(pnd_Restart_Data_Pnd_Restart_Date);                                                                                 //Natural: MOVE #RESTART-DATE TO RESTART-DATE
        pnd_Restart_Data_Pnd_Restart_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II"));                                                                   //Natural: MOVE EDITED *TIMX ( EM = HH:II ) TO #RESTART-TIME
        ldaAppl165.getAppl165_Restart_Time().setValue(pnd_Restart_Data_Pnd_Restart_Time);                                                                                 //Natural: MOVE #RESTART-TIME TO RESTART-TIME
        //*  COMPRESS AP-COR-LAST-NME AP-COR-FIRST-NME INTO FULL-NAME
        ldaAppl165.getAppl165_Full_Name().setValue(DbsUtil.compress(annty_Actvty_Prap_View_Ap_Cor_First_Nme, annty_Actvty_Prap_View_Ap_Cor_Last_Nme));                    //Natural: COMPRESS AP-COR-FIRST-NME AP-COR-LAST-NME INTO FULL-NAME
        getWorkFiles().write(1, false, ldaAppl165.getAppl165());                                                                                                          //Natural: WRITE WORK FILE 1 APPL165
        if (condition(! (pnd_Print.getBoolean())))                                                                                                                        //Natural: IF NOT #PRINT
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"=",ldaAppl165.getAppl165_Tiaa_Contract(),NEWLINE,"=",ldaAppl165.getAppl165_Pin(),NEWLINE,"=",ldaAppl165.getAppl165_Ssn(),          //Natural: WRITE / '=' TIAA-CONTRACT / '=' PIN / '=' SSN / '=' FULL-NAME / '=' BIRTHDATE-MMDDYYYY / '=' ALLOC-FUND-CDE ( 1:10 ) / '=' ALLOC-PCT ( 1:10 ) / '=' PLAN / '=' DIVSUB / '=' EMAIL-ADDRESS / '=' ADDRESS ( 1:5 ) / '=' ZIP / '=' RESTART-DATE / '=' RESTART-TIME / '=' RESTART-RECORD-NUMBER
            NEWLINE,"=",ldaAppl165.getAppl165_Full_Name(),NEWLINE,"=",ldaAppl165.getAppl165_Birthdate_Mmddyyyy(),NEWLINE,"=",ldaAppl165.getAppl165_Alloc_Fund_Cde().getValue(1,
            ":",10),NEWLINE,"=",ldaAppl165.getAppl165_Alloc_Pct().getValue(1,":",10),NEWLINE,"=",ldaAppl165.getAppl165_Plan(),NEWLINE,"=",ldaAppl165.getAppl165_Divsub(),
            NEWLINE,"=",ldaAppl165.getAppl165_Email_Address(),NEWLINE,"=",ldaAppl165.getAppl165_Address().getValue(1,":",5),NEWLINE,"=",ldaAppl165.getAppl165_Zip(),
            NEWLINE,"=",ldaAppl165.getAppl165_Restart_Date(),NEWLINE,"=",ldaAppl165.getAppl165_Restart_Time(),NEWLINE,"=",ldaAppl165.getAppl165_Restart_Record_Number());
        if (Global.isEscape()) return;
    }
    private void sub_Copy_Extracted_Records() throws Exception                                                                                                            //Natural: COPY-EXTRACTED-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ON A RESTART, SINCE THE OUTPUT FILE WAS PARTIALLY WRITTEN
        //*  YOU NEED TO 'copy' THE SUCCESSFULLY EXTRACTED RECORDS FROM THE
        //*  PREVIOUS GENERATION
        //*  CHECK TO MAKE SURE INPUT FILE IS FROM LAST RUN, ELSE ABORT
        READWORK01:                                                                                                                                                       //Natural: READ WORK 2 APPL165
        while (condition(getWorkFiles().read(2, ldaAppl165.getAppl165())))
        {
            if (condition(ldaAppl165.getAppl165_Restart_Date().equals(pnd_Restart_Data_Pnd_Restart_Date) && ldaAppl165.getAppl165_Restart_Time().equals(pnd_Restart_Data_Pnd_Restart_Time)  //Natural: IF RESTART-DATE EQ #RESTART-DATE AND RESTART-TIME EQ #RESTART-TIME AND RESTART-RECORD-NUMBER EQ #RESTART-RECORD-NBR
                && ldaAppl165.getAppl165_Restart_Record_Number().equals(pnd_Restart_Data_Pnd_Restart_Record_Nbr)))
            {
                pnd_Have_Correct_File.setValue(true);                                                                                                                     //Natural: ASSIGN #HAVE-CORRECT-FILE := TRUE
                //*  FOUND A MATCH, FILE IS OK
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, ldaAppl165.getAppl165());                                                                                                      //Natural: WRITE WORK FILE 1 APPL165
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(! (pnd_Have_Correct_File.getBoolean())))                                                                                                            //Natural: IF NOT #HAVE-CORRECT-FILE
        {
            getReports().write(0, "Incorrect input file; correct and re-run");                                                                                            //Natural: WRITE 'Incorrect input file; correct and re-run'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Prap_Record() throws Exception                                                                                                                //Natural: UPDATE-PRAP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_No_Update.getBoolean()))                                                                                                                        //Natural: IF #NO-UPDATE
        {
            if (condition(pnd_Print.getBoolean()))                                                                                                                        //Natural: IF #PRINT
            {
                getReports().write(0, "updating record routine but no update performed");                                                                                 //Natural: WRITE 'updating record routine but no update performed'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        GET1:                                                                                                                                                             //Natural: GET ANNTY-ACTVTY-PRAP-VIEW *ISN ( FINDUPD. )
        vw_annty_Actvty_Prap_View.readByID(vw_annty_Actvty_Prap_View.getAstISN("FINDUPD"), "GET1");
        annty_Actvty_Prap_View_Ap_Release_Ind.setValue(6);                                                                                                                //Natural: MOVE 6 TO AP-RELEASE-IND
        vw_annty_Actvty_Prap_View.updateDBRow("GET1");                                                                                                                    //Natural: UPDATE ( GET1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  JRB2
        //*  JRB2
        //*  JRB2
        getReports().write(0, NEWLINE,NEWLINE,"Program:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"Error:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"Line:",new  //Natural: WRITE // 'Program:' 25T *PROGRAM / 'Error:' 25T *ERROR-NR / 'Line:' 25T *ERROR-LINE // 'Contract:' 25T AP-TIAA-CNTRCT / 'PIN:' 25T AP-PIN-NBR
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"Contract:",new TabSetting(25),annty_Actvty_Prap_View_Ap_Tiaa_Cntrct,NEWLINE,"PIN:",new 
            TabSetting(25),annty_Actvty_Prap_View_Ap_Pin_Nbr);
        //*  JRB2
        getReports().write(0, "Extract Records Written:",pnd_Write_Count, new ReportEditMask ("ZZZ,ZZ9"));                                                                //Natural: WRITE 'Extract Records Written:' #WRITE-COUNT ( EM = ZZZ,ZZ9 )
        //*  JRB2
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=52");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new ColumnSpacing(9),"ACIS/POST RHSP Extract Process",new 
            ColumnSpacing(8),Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),Global.getTIMX(), new ReportEditMask ("HH:II"),NEWLINE,new ColumnSpacing(25),
            "OMNI STATISTICS REPORT",NEWLINE);

        getReports().setDisplayColumns(0, "********  NO PRAP RECORDS FOUND  ********");
    }
}
