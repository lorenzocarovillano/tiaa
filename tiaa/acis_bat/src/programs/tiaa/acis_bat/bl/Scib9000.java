/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:33 PM
**        * FROM NATURAL PROGRAM : Scib9000
************************************************************
**        * FILE NAME            : Scib9000.java
**        * CLASS NAME           : Scib9000
**        * INSTANCE NAME        : Scib9000
************************************************************
************************************************************************
* PROGRAM : SCIB9000
* SYSTEM  : ENROLLMENT DRIVER PROGRAM - SUNGARD
* AUTHOR  : CHARLES SINGLETON
* DATE    : MAR. 2004
* PURPOSE : READ AND PROCESS SUNGARD ENROLLMENTS. THIS PROCESS
*         : WILL ALSO PREPARE A FILE TO UPDATE SUNGARD DATABASE.
* HISTORY :
* 08/06/04 B.E.  MOVE #PLAN-TYPE TO #IRC-SECTION CODE. THE PROGRAM
*                PSG9000 WAS CHANGED TO PASS THE VALUE OF PL660 INSTEAD
*                OF PL089.  PL660 CONTAINS THE ACTUAL VALUE OF THE IRC
*                SECTION CODE.
* 08/23/04 C.S.  ADD CONTRACT PICKUP LOGIC TO CHECK TO ACIS PRAP FILE
*                TO SEE IF EXISTING CONTRACT CAN BE PICKED-UP. ADDED
*                CALL TO SUBPROGRAM SCIN9002 TO READ PRAP FOR POSSIBLE
*                PICK-UPS.
* 08/30/04 B.E.  ELIMINATE THE HARD-CODING OF CMMA# AS THE DEFAULT.
*                THE DEFAULT FUND WILL ALWAYS BE PASSED BY OMNI VBA
*                PSG9000.
* 09/21/04 C.S.  ELIMINATE CHECK OF VESTING-CODE-OVERRIDE TO DETERMINE
*                VESTING-CODE.VESTING-CODE-OVERRIDE WILL BE PASSED
*                SCIN1200 FOR OTHER PROCESSING.
* 11/22/04 C.S.  ADD LOGIC TO HANDLE CONTRACT ISSUANCE FOR TNT
*                PRODUCTS (RS AND RSP).
* 12/02/04 C.S.  COMMENT OUT PICKUP PROCESSING LOGIC SINCE IT
*                IS NOW OUT OF SCOPE.
* 12/04/04 C.S.  ADD LOGIC TO DETERMINE WHETHER A CONTRACT IS
*                TNT AND SET AN INDICATOR TO PASS TO SCIN1200
*                AND EVENTUALLY UPDATE COR. ALSO, ADDED CODE
*                TO CAPTURE BENE ALLOCATION PERCENT.
* 12/08/04 C.S.  LOGIC TO CHECK DEFAULTED DATA ELEMENT VALUES
*                WHEN INCOMPLETE BIO DATA HAS BEEN SENT. THIS
*                ROUTINE WILL SET DEFAULT VALUES TO BE UPDATED
*                IN ACIS AND INTERFACE THE DEFAULTED VALUES BACK
*                TO OMNI USING T813 TRANSACTIONS CREATED IN SCIB9001.
* 03/05/05 C.S.  LOGIC TO PROCESS NEGATIVE ENROLLMENTS. SCIL9000
*                HAS BEEN MODIFIED TO ACCEPT ORIGINATING TIAA AND
*                CREF CONTRACT NUMBERS FROM OMNI FOR THESE ENROLLMENTS.
* 03/11/05 C.S.  ADD LOGIC TO HANDLE CONTRACT ISSUANCE FOR IRA
*                PRODUCTS.
* 03/12/05 C.S.  ADD LOGIC TO HANDLE CONTRACT ISSUANCE FOR ICAP
*                PRODUCTS.
* 04/18/05 C.S.  CALL SCIN9003 WHEN #MONEY-INVESTED-IND IS SET.SCIN9003
*                WILL MATCH AND RELEASE CONTRACT ON PRAP.
* 04/22/05 B.E.  CALL SCIN2200 TO CHECK FOR ICAP APPROVED STATES.
*                SCIN2200 READS APP-TABLE-ENTRY FOR APPROVED STATE.
*                IF STATE IS NOT APPROVED FOR ICAP THE ENROLLMENT WILL
*                WILL BE REJECTED,REPORTED ON AS AN ERROR THEN SENT
*                TO POWERIMAGE TO LOG AS AN OMNI REJECT.
* 05/26/05 D.M.  COMMENT 'IR5' & 'IR6' NOT NEEDED FOR RELEASE 6.
* 08/23/05 MARPURI   - CHANGE OLD TNT SUBPLANS TO NEW TNT SUBPLAS
* 12/08/05 C.S.  PASS DIV-SUB TO SCIN1200 FOR ACIS
* 12/29/05 C.S.  SET MIT-UNIT TO 'IRATRS' FOR IRA PRODUCTS
* 02/18/06 C.S.  SETUP BENE DATA FOR ILLINOIS SMP ENROLLMENTS
* 06/10/06 C.S.  PASS REPLACEMENT REGISTER FIELDS TO SCIN1200 FOR
*                AUTOMATED REPLACEMENT REGISTER.
* 09/10/06 C.S.  ADD LOGIC TO HANDLE CONTRACT ISSUANCE FOR 'TGA'
*                PRODUCTS. THESE ARE 'GA5' PRODUCTS IN OMNIPLUS.
* 11/27/06 C.S.  SET DEFAULT VALUES FOR DIVORCE-IND AND ADDITIONAL-CREF
*                RQST WHEN BLANK VALUES ARE SENT FROM OMNI.
* 12/07/06 C.S.  OVERRIDE VESTING TO PARTICIPANT OWNED FOR PLANS
*                102975 AND 150381 FOR GSRA CONTRACTS.
* 01/09/07 BJD   OVERRIDE VESTING TO PARTICIPANT OWNED FOR PLAN
*                102148 FOR GSRA CONTRACTS.
* 02/21/07 KG    ADDED ATRA SUBPLAN FOR CIP INDICATOR POPULATION
* 03/23/07 BJD   OVERRIDE VESTING TO PARTICIPANT OWNED FOR PLAN
*                151109 FOR GSRA CONTRACTS.
* 03/14/07 O S   PUT EXTERNALIZED CHECK FOR VESTING EXCEPTION.
*                SC 031407.
* 05/08/2007 AB  ADDED NEW FIELDS TO SCIA1200, SCIL9000: INSURER-1,
*                INSURER-2, INSURER-3, 1035-EXCH-IND-1,
*                1035-EXCH-IND-2, 1035-EXCH-IND-3.
*                EDITS FOR 1035-EXCH-IND WERE ALSO ADDED.
* 07/02/2007 BJD MOVE #ISSUE-STATE TO POS 1-2 OF #SG-TEXT-UDF-1
*                FOR STABLE RETURN STATE VARIATIONS PROJECT 769.
* 07/19/2007 AB  BENEFICIARY SSN FIELD IS CHANGED TO CHECK FOR ZEROS
*                BESIDES SPACES IN ORDER TO PREVENT BLANK LINES FROM
*                BEING WRITTEN TO THE BENEFICIARY FILE.
* 08/28/2007 AB  WHEN THERE IS AN ERROR ISSUING A CONTRACT NUMBER, A   *
*                FILE IS BEING ADDED TO BE SENT TO POWERIMAGE.         *
*                ADDED NEW REPORT 'PowerImage Interface Report - New   *
*                TASKS FOR ACIS REJECTS'                               *
* 01/07/2008 KG  ADDED CHANGES FOR ACIS REJECTS CCR - SEE REJECT KG    *
*                - HARDCODING SRA CONTRACTS AS PARTICIPANT OWNED.      *
*                - ADDING ENROLL METHOD AND VESTING TO T813 FILE.      *
*                - ADDING RES VS MAIL ISSUE STATE FIX - SEE MAIL-RES   *
*                  FIX KG                                              *
* 03/17/2008 AB  ADDED PL-CLIENT-ID AND MODEL-ID FIELDS TO INPUT FILE- *
*                NEW FIELDS FOR CALSTRS IN TEXT-UDF-2.                 *
* 06/13/2008 JB  MODIFY CODE FOR CHECKING EMPLOYER TYPE TO MATCH OMNI  *
*                AND DO NOT SET CIP INDICATOR FOR CHURCH - JRB1        *
* 09/02/2008 RM  AUTO ENROLL / AUTO SAVE PROJECT                       *
*                ADD NEW FIELDS AND PASS NEW DATA TO PRAP FILE.        *
*                (LDA - SCIL9000 AND SCIA1200 UPDATED)                 *
* 09/11/2008 BJD RHSP - DON't set CIP indicator to Y - add RHSP        *
*                PRODUCT CODE AND SET ISSUE-STATE TO RES STATE.        *
*                   SEE BJD RHSP                                       *
* 02/2009    JJG INC578075 - WHEN ISSUING A CREF COMPANION CERTIFICATE *
*                TO A PREVIOUSLY ISSUED TIAA CONTRACT, THE SYSTEM IS   *
*                OVERLAYING THE ORIGINAL TIAA CONTRACT ISSUE DATE WITH *
*                THE NEWLY ISSUED CREF COMPANION CERTIFICATE DATE.     *
* 07/07/2009 B.E 1. ADDED WORKFILE 6 TO WRITE THE ERRORED RECORDS FROM *
*                   CONTRACT ISSUANCE.                                 *
* 09/9/2009  BJD CREATE A POWER IMAGE ENRACISRJCT RECORD FOR ALL ACIS  *
*                REJECTS, NOT JUST THOSE WITHOUT AN EXISTING TASK ID.  *
*                ALSO INLCUDE ALL REJECTS ON THE PI REJECT REPORT.     *
*                SEE BJD ACISRJCT                                      *
* 10/3/2009  NBC MODIFY FOR MDM PERFORMANCE (SEE NBC)                  *
* 02/8/2010  DBH ADD TSV INDICATOR                                     *
* 03/5/2010  RS0 PERFORM STATE PRODUCT APPROVAL CHECK FOR INDX CNTRCTS *
* 1/20/2011  RS1 CHANGES FOR 'STAR 2A Eligibility' PROJECT             *
*                HANDLE PROCESSING FOR INELIGIBLE(STATUS '03') AND     *
*                ELIGIBLE NOT PARTICIPATING(STATUS '04') CONTRACTS     *
* 7/13/2011  GGG MAKING CHANGES TO THE RETURN ERROR MESSAGE FROM THE   *
*                CALL TO SCIN2200. SEE - 'GGG-0711'                    *
* 1/11/2012  PG  REMOVED THE "IF" STATEMENT THAT SETS THE              *
*                #T813-UPDATE-ADDRESS TO 'Y'. IT WILL NOW              *
*                UNCONDITIONALLY HAVE A VALUE OF "Y". THIS IS IN       *
*                SUBROUTINE FORMAT-N-WRITE-INTERFACE-RECORDS.          *
* 03/01/2012 BE1 RESIDENCE ADDRESS CAPTURE PROJECT.  THE CHANGE IS TO  *
*                ALLOW MILITARY ADDRESS TO BE ACCEPTED AS THE PARTICI- *
*                PANT's residence address.  This requires changing     *
*                THIS PROGRAM TO TRANSLATE AA, AE, AP STATE CODE IN    *
*                THE RESIDENCE ADDRESS TO THE EQUIVALENT STATE CODE    *
*                FOR CONTRACT ISSUANCE PURPOSES.                       *
*                TRANSLATE: AE TO NY                                   *
*                         : AP TO CA                                   *
*                         : AA TO FL                                   *
* 03/16/2012 DBH ADD EDIT CHECK/REJECT LOGIC FOR ISSUE STATE - SEE DBH2
***   BELOW CHANGES IS FOR SRK JUN 2012  *******************************
* 10/11/2011 SCHNEIDER- ADD ORCHESTRATION ID  (SRK)                    *
* 12/01/2011 LS1 STAR 2D TO INCLUDE ADDITIONAL DEFAULT ENROLLMENTS     *
*                AND NOT FROM AWC FOR PLANS SUBJECT TO ELIGIBILITY     *
* 01/04/2012 K GATES - ADDED TRANSLATION OF BENE RELATIONSHIP CODE     *
*                      FROM OPE - SEE SRK KG                           *
* 07/19/2012 BNEWSOM - CHANGES MADE TO HANDLE A NEW BENEFICIARY DEFAULT*
*                      OF 'PLAN/PRODUCT PROVISIONS' INSTEAD OF ESTATE. *
*                      THE PROGRAM WILL QUERY THE 'PL' TABLE IN ACIS   *
*                      FOR PLANS THAT ARE SUBJECT TO THE DEFAULT.      *
*                      SEE CHANGE TAG:   CHG256421                     *
* 11/17/2012 L SHU   - MODIFY PGM TO READ THE LPAO IND FROM INTERFACE  *
*                      FILE AND PASS IT TO SCIA1200 TO UPDATE ANNUITY  *
*                      OPTION INDICATOR.                               *
*                      RESET MONEY INVEST INDICATOR FOR A PLAN SUBJECT *
*                      TO LEGAL PACKAGE ANNUITY FUND.          -  LPAO *
* 08/10/2012 BNEWSOM - REMOVE IF STATEMENT AROUND THE MOVE OF THE      *
*                      ORCHESTRATION ID. INC1793837                    *
* 10/12/2012 PGOLDEN - FIXED HANDLING OF STATE CODE ERRORS BY CHANGES  *
*                      UNDER DBH2 SO THAT REJECTS ARE WRITEN TO THE    *
*                      RETRY FILE AND TO THE POWERIMAGE TASK FILE.     *
*                      ADDED FIRST-NAME, LAST-NAME, SSN TO SCIA1200    *
*                      DATA AREA IF ISSUE-STATE IS MISSING.  PG1       *
* 10/12/2012 RJFRANK - FIX BPEL OVERRIDE ERRORS SO THEY ARE COUNTED    *
*                      IN THE TOTAL ERRORS                             *
*                      AND RELOCATE RESTART LOGIC TO BEFORE ANY        *
*                      PROCESSING, AND THEREBY CORRECT THE TOTAL       *
*                      RECORDS COUNT VALUE ON THE REPORTS              *
*                      AND FIX THE NAME APPEARING ON THE REJECT REPORT *
*                      FOR BPEL MANAGED TRANSACTIONS THAT ARE REJCTED  *
*                      SCAN FOR CHANGE TAG   RJF1
* 07/18/2013 L SHU   - WRITE A NEW WORK FILE 7 FOR BENE INTERFACE AND  *
*                      WILL BE USED BY THE BENE TO COPY BENE ELECTIONS *
*                      FROM THE OLD CONTRACT INTO NEW CNTRCT  (TNGSUB) *
*                    - ADDED TO EXCLUDE IRA SUBSTITUTED CONTRACT FROM  *
*                      THE CIP VERIFICATION PROCESS           (TNGSUB) *
*                    - ADD A CHECK FOR IRA SUBS NOT TO OVERLAID/HARD   *
*                      CODE THE DAY OF THE ISSUE DATE AND TO USE       *
*                      EXISTING CREF ISSUE DATE PASS FROM OMNI FOR THE *
*                      CREF NUMBER                            (TNGSUB) *
*                    - ADDED ORIGINAL TIAA CONTRACT NUMBER FOR IRA     *
*                      SUBSTITUTION IN ACCEPTED REPORT        (TNGSUB) *
* 07/18/2013 C LAEVEY- ADD #T813-SUBSTITUTION-CONTRACT-IND AND         *
*                      #T813-DECEASED-IND TO #T813-HEADER              *
*                      POPULATE FIELDS FROM SCIL9000 BEFORE WRITING    *
*                      INTERFACE
*                      USE STANDARD STATE TRANSLATION ON THE CONVERTED
*                      ISSUE STATE.                             (TNGSUB)
* 07/18/2013 C LAEVEY- CONTRACT ISSUANCE FOR QDRO PARTICIPANTS (TNGPROC)
*                      CHECK IRA SUB START DATE TO ALLOW IRA SUBSTITUTX
*                      ISSUE BOTH IRA SUB AND 3% CONTRACTS IF CURRENT
*                      DATE GREATER THAN IRA SUB START DATE.
*                      SET HOLD DATE BASED ON SUB CONTRACT IND
*                      REJECT AND REPORT WHEN BENE COPY FAILS BUT ALLOW
*                      CONTRACT ISSUANCE.
*                      CHANGE PI-LOG-TASK-TYPE TO ENRACISREJ
* 07/18/2013 B NEWSOM- MADE THE FOLLOWING CHANGES FOR NY200:
*                      CHANGE ALL REFERENCES FROM SCIA1280 TO SCIA1290
*                      FOR DOMESTIC ADDRESS, DO NOT USE THE BPEL DATA,
*                      INSTEAD MOVE THE DOMESTIC ADDRESS TO SCIA1290.
*                      FOR FOREIGN ADDRESS, USE THE BPEL DATA. (NY200)
* 09/12/2013 B ELLO  - MADE THE FOLLOWING CHANGES FOR MT. SINAI.
*                      ADD LOGIC TO MOVE A NEW INDICATOR(NON-PROPRIETA-
*                      RY-PKG-IND) FROM OMNI TO ACIS.  THE INDICATOR
*                      WILL HAVE A VALUE OF OF 'PRUG' FOR MT. SINAI
*                      PLANS TO TELL ACIS/DCS TO GENERATE A PRUDENTIAL
*                      GUARANTEED INTEREST ANNUITY (GIA) CERTIFICATE
*                      ALONG WITH THE TIAA-LEGAL PACKAGE. (MTSIN)
*                    - FIXED TRANSLATION OF BENEFICIARY RELATIONSHIP
*                    - CODES                              (MTSIN)
* 11/22/2014 W BAUER - ADD TSR-IND FOR STABLE RETURN FUND. (IMBS)
*                      VALUES WILL BE "SRF" IF PLAN OFFERS THE STABLE
*                      RETURN FUND. IT WILL BE BLANK IF IT DOES NOT.
* 03/31/2015 B NEWSOM - PH CAN'T CHG BENE ONLINE DUE TO IRREVOCABLE
*                       DESIGNATION.                         (CHG346370)
* 09/01/2015 B ELLO   - ADD ASSIGNMENT OF BENE IN THE PLAN FIELDS FROM
*                       OMNI TO ACIS.                        (BIP)
*          B.NEWSOM     COR/NAAD RETIREMENT                        (CNR)
* 09/18/2015 B.NEWSOM - METRO WATER BENE SUPPRESSION FROM THE BENE
*                       ONLINE.                                   (MWBS)
* 12/01/2015 B.ELLO   - BYPASS PRODUCT STATE APPROVAL EDIT FOR BENE IN
*                       THE PLAN ACCOUNTS.                        (BIP2)
* 02/01/2016 L.SHU    - ADD TO INCLUDE BENE EXTENDED NAME         (BNL2)
*                     - CHANGE LOGIC TO ALWAYS RETRIEVE BENE INFO.(BNL2)
*                       FROM BENE LEGACY FOR NEGATIVE ENROLLMENT. (BNL2)
* 03/18/2016 B.NEWSOM - ADD LOGIC TO COUNT THE INDEXED SUBSTITUTION
*                       CONTRACTS.                                (BDP)
* 03/22/2016 B.NEWSOM - A OUT OF BALANCE CONDITION WAS FOUND IN THE
*                       POWER IMAGE PASS FILE COMMING FROM EXTR030.
*                       THE OOB WAS CAUSE WHEN TWO CONTRACTS ARE ISSUED
*                       WITH A TASK ID.  THIS CAUSES THE TASK ID TO BE
*                       WRITTEN TWICE TO THE PASS FILE.     (INC3169566)
* 05/15/2016 B.NEWSOM - ADD TO INCLUDE BENE EXTENDED NAME       (BNL2F1)
* 03/17/2017 B.NEWSOM - ONEIRA PROJECT                          (ONEIRA)
*    1. ADD A NEW PARAMETER SCIA1295 REPLACE SCIA1290 AND INCLUDED BENE
*       FRACTION NUMERATOR AND DENOMINATOR TO ALLOW BENE FRACTION INFO
*       TO ACIS BENE FILE.
*    2. NOT TO PASS REJECT CONTRACT DATA TO PI EXPAG
*    3. ONEIRA FIELD EDITING IS DONE IN UD BEFORE ACIS.
*    4. FOR ONEIRA PROCESSING, CALLING A NEW MODULE SCIN9000 WITH NEW
*       PARM SCIA9000 TO STORE INFORMATION IN ACIS
* 03/17/2017 B NEWSOM - EASE ENROLLMENT PROJECT.
*                       SET THE MATCH AND RELEASE              (EASEENR)
* 05/23/17 B.ELLO      PIN EXPANSION CHANGES
*                      RESTOWED FOR UPDATED PDAS AND LDAS
*                      CHANGED FIELD FORMAT FROM A7 TO A12  (PINE)
* 01/19/18 L. SHU      PI EXPAG                             (PIEXPAG)
*                        ADDING 4 MORE FIELDS PASS TO PI EXPAG
*                        DIVORCE IND / BENE IN THE PLAN IND /
*                        ENROLLMENT METHOD / OMNI FOLDER
* 01/19/18 B. ELLO     PRODUCTION FIXES                     (PFIX1)
*                      1. MOVE THE CALL TO UPDATE-BATCH-RESTART-RECORD
*                         BEFORE ANY PROCESSING IS DONE TO ENSURE
*                         RESTART RECORD IS ALWAYS UPDATED.
*                      2. DO NOT CALL SCIN9913 TO RETRIEVE ACIS BENE
*                         RECORD FOR BENE IN THE PLAN SINCE BENE WILL
*                         NOT EXISTS IN ACIS.  BENE IS INTERFACED FROM
*                         OMNI DIRECTLY TO BENE LEGACY.
* 02/01/18 L. SHUL     PRODUCTION FIXES                     (PFIX-PI)
* 04/04/19 B.NEWSOM    BREAKING ACIS DEPENDENCIES OF THE BENE LEGACY
*                      SYSTEM                                (BADOTBLS)
* 11/09/19 L. SHUL     IISG PROJECT                         (IISG)
*                      ADD ADDITIONAL SOURCE AND ALLOCATION
* 11/09/19 B.NEWSOM    CHANGE ASLAC BENEFICIARY TO HAVE THE SAME WORDING
*                      AS METRO-WATER.  ASLAC PLANS ARE 406711, 406712,
*                      407221, 407222, 407223.               (ASLAC)
*                     - ADDITIONAL MODIFICATIONS FOR BENE SUNSET
************************************************************************
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9000 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaScia1200 pdaScia1200;
    private PdaScia1295 pdaScia1295;
    private PdaScia9000 pdaScia9000;
    private LdaScil9000 ldaScil9000;
    private PdaScia9002 pdaScia9002;
    private PdaScia9003 pdaScia9003;
    private PdaScia2200 pdaScia2200;
    private PdaScia2100 pdaScia2100;
    private LdaScil9020 ldaScil9020;
    private PdaScia1250 pdaScia1250;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pls_Error_Occurence;
    private DbsField pls_Dar_Program;

    private DataAccessProgramView vw_batch_Restart;
    private DbsField batch_Restart_Rst_Actve_Ind;
    private DbsField batch_Restart_Rst_Job_Nme;
    private DbsField batch_Restart_Rst_Jobstep_Nme;
    private DbsField batch_Restart_Rst_Pgm_Nme;
    private DbsField batch_Restart_Rst_Curr_Dte;
    private DbsField batch_Restart_Rst_Curr_Tme;
    private DbsField batch_Restart_Rst_Start_Dte;
    private DbsField batch_Restart_Rst_Start_Tme;
    private DbsField batch_Restart_Rst_End_Dte;
    private DbsField batch_Restart_Rst_End_Tme;
    private DbsField batch_Restart_Rst_Key;
    private DbsField batch_Restart_Rst_Cnt;
    private DbsField batch_Restart_Rst_Isn_Nbr;
    private DbsField batch_Restart_Rst_Rstrt_Data_Ind;

    private DbsGroup batch_Restart_Rst_Data_Grp;
    private DbsField batch_Restart_Rst_Data_Txt;
    private DbsField batch_Restart_Rst_Frst_Run_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Tme;
    private DbsField batch_Restart_Rst_Updt_Dte;
    private DbsField batch_Restart_Rst_Updt_Tme;

    private DbsGroup bene_File;

    private DbsGroup bene_File_Bene_File_Data;
    private DbsField bene_File_Bene_Intrfcng_Systm;
    private DbsField bene_File_Bene_Rqstng_System;
    private DbsField bene_File_Bene_Intrfce_Bsnss_Dte;
    private DbsField bene_File_Bene_Intrfce_Mgrtn_Ind;
    private DbsField bene_File_Bene_Nmbr_Of_Benes;
    private DbsField bene_File_Bene_Tiaa_Nbr;
    private DbsField bene_File_Bene_Cref_Nbr;
    private DbsField bene_File_Bene_Pin_Nbr;
    private DbsField bene_File_Bene_Lob;
    private DbsField bene_File_Bene_Lob_Type;
    private DbsField bene_File_Bene_Part_Prfx;
    private DbsField bene_File_Bene_Part_Sffx;
    private DbsField bene_File_Bene_Part_First_Nme;
    private DbsField bene_File_Bene_Part_Middle_Nme;
    private DbsField bene_File_Bene_Part_Last_Nme;
    private DbsField bene_File_Bene_Part_Ssn;
    private DbsField bene_File_Bene_Part_Dob;
    private DbsField bene_File_Bene_Estate;
    private DbsField bene_File_Bene_Trust;
    private DbsField bene_File_Bene_Category;
    private DbsField bene_File_Bene_Effective_Dt;
    private DbsField bene_File_Bene_Mos_Ind;
    private DbsField bene_File_Bene_Mos_Irrvcble_Ind;
    private DbsField bene_File_Bene_Pymnt_Chld_Dcsd_Ind;
    private DbsField bene_File_Bene_Update_Dt;
    private DbsField bene_File_Bene_Update_Time;
    private DbsField bene_File_Bene_Update_By;
    private DbsField bene_File_Bene_Record_Status;
    private DbsField bene_File_Bene_Same_As_Ind;
    private DbsField bene_File_Bene_New_Issuefslash_Chng_Ind;
    private DbsField bene_File_Bene_Contract_Type;
    private DbsField bene_File_Bene_Tiaa_Cref_Ind;
    private DbsField bene_File_Bene_Stat;
    private DbsField bene_File_Bene_Dflt_To_Estate;
    private DbsField bene_File_Bene_More_Than_Five_Benes_Ind;
    private DbsField bene_File_Bene_Illgble_Ind;
    private DbsField bene_File_Bene_Exempt_Spouse_Rights;
    private DbsField bene_File_Bene_Spouse_Waived_Bnfts;
    private DbsField bene_File_Bene_Trust_Data_Fldr;
    private DbsField bene_File_Bene_Addr_Fldr;
    private DbsField bene_File_Bene_Co_Owner_Data_Fldr;
    private DbsField bene_File_Bene_Fldr_Log_Dte_Tme;
    private DbsField bene_File_Bene_Fldr_Min;
    private DbsField bene_File_Bene_Fldr_Srce_Id;
    private DbsField bene_File_Bene_Rqst_Timestamp;

    private DbsGroup bene_File__R_Field_1;
    private DbsField bene_File_Bene_Rqst_Date;
    private DbsField bene_File_Bene_Rqst_Time;
    private DbsField bene_File_Bene_Last_Vrfy_Dte;
    private DbsField bene_File_Bene_Last_Vrfy_Tme;
    private DbsField bene_File_Bene_Last_Vrfy_Userid;
    private DbsField bene_File_Bene_Last_Dsgntn_Srce;
    private DbsField bene_File_Bene_Last_Dsgntn_System;

    private DbsGroup bene_File_Bene_Data;
    private DbsField bene_File_Bene_Type;
    private DbsField bene_File_Bene_Name;
    private DbsField bene_File_Bene_Extended_Name;
    private DbsField bene_File_Bene_Ssn_Cd;
    private DbsField bene_File_Bene_Ssn_Nbr;
    private DbsField bene_File_Bene_Dob;
    private DbsField bene_File_Bene_Dte_Birth_Trust;
    private DbsField bene_File_Bene_Relationship_Free_Txt;
    private DbsField bene_File_Bene_Relationship_Cde;
    private DbsField bene_File_Bene_Prctge_Frctn_Ind;
    private DbsField bene_File_Bene_Irrvcbl_Ind;
    private DbsField bene_File_Bene_Alloc_Pct;
    private DbsField bene_File_Bene_Nmrtr_Nbr;
    private DbsField bene_File_Bene_Dnmntr_Nbr;
    private DbsField bene_File_Bene_Std_Txt_Ind;
    private DbsField bene_File_Bene_Sttlmnt_Rstrctn;

    private DbsGroup bene_File_Bene_Addtl_Info;
    private DbsField bene_File_Bene_Addr1;
    private DbsField bene_File_Bene_Addr2;
    private DbsField bene_File_Bene_Addr3_City;
    private DbsField bene_File_Bene_State;
    private DbsField bene_File_Bene_Zip;
    private DbsField bene_File_Bene_Country;
    private DbsField bene_File_Bene_Phone;
    private DbsField bene_File_Bene_Gender;
    private DbsField bene_File_Bene_Spcl_Txt;
    private DbsField bene_File_Bene_Mdo_Calc_Bene;
    private DbsField bene_File_Bene_Spcl_Dsgn_Txt;
    private DbsField bene_File_Bene_Hold_Cde;
    private DbsField bene_File_Pnd_Table_Rltn;

    private DbsGroup bene_File__R_Field_2;
    private DbsField bene_File_Pnd_Rltn_Cd;
    private DbsField bene_File_Pnd_Rltn_Text;
    private DbsField bene_File_Bene_Total_Contract_Written;
    private DbsField bene_File_Bene_Total_Mos_Written;
    private DbsField bene_File_Bene_Total_Dest_Written;
    private DbsField bene_File_Bene_Return_Code;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;

    private DbsGroup app_Table_Entry__R_Field_3;
    private DbsField app_Table_Entry_Entry_Rule_Category;
    private DbsField app_Table_Entry_Entry_Rule_Plan;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;
    private DbsField app_Table_Entry_Entry_User_Id;
    private DbsField pnd_Restart_Key;

    private DbsGroup pnd_Restart_Key__R_Field_4;

    private DbsGroup pnd_Restart_Key_Pnd_Restart_Structure;
    private DbsField pnd_Restart_Key_Pnd_Restart_Plan_No;
    private DbsField pnd_Restart_Key_Pnd_Restart_Subplan_No;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ssn;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ext;
    private DbsField pnd_Sp_Actve_Job_Step;

    private DbsGroup pnd_Sp_Actve_Job_Step__R_Field_5;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme;
    private DbsField pnd_Restart_Isn;
    private DbsField pnd_Last_Contract_Processed;

    private DbsGroup pnd_T813_Record;
    private DbsField pnd_T813_Record_Pnd_T813_Plan_No;
    private DbsField pnd_T813_Record_Pnd_T813_Part_Id;

    private DbsGroup pnd_T813_Record__R_Field_6;
    private DbsField pnd_T813_Record_Pnd_T813_Ssn;
    private DbsField pnd_T813_Record_Pnd_T813_Subplan_No;
    private DbsField pnd_T813_Record_Pnd_T813_Ext;
    private DbsField pnd_T813_Record_Pnd_T813_Tiaa_Contract;
    private DbsField pnd_T813_Record_Pnd_T813_Cref_Contract;
    private DbsField pnd_T813_Record_Pnd_T813_Pin_No;
    private DbsField pnd_T813_Record_Pnd_T813_Trade_Date;
    private DbsField pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date;

    private DbsGroup pnd_T813_Record__R_Field_7;
    private DbsField pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Ccyy;
    private DbsField pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Mm;
    private DbsField pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Dd;
    private DbsField pnd_T813_Record_Pnd_T813_Cref_Issue_Date;

    private DbsGroup pnd_T813_Record__R_Field_8;
    private DbsField pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Ccyy;
    private DbsField pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Mm;
    private DbsField pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Dd;
    private DbsField pnd_T813_Record_Pnd_T813_Annty_Start_Date;
    private DbsField pnd_T813_Record_Pnd_T813_App_Received_Date;
    private DbsField pnd_T813_Record_Pnd_T813_Issue_State;
    private DbsField pnd_T813_Record_Pnd_T813_Mail_Address_Line;
    private DbsField pnd_T813_Record_Pnd_T813_Mail_City;
    private DbsField pnd_T813_Record_Pnd_T813_Mail_State;
    private DbsField pnd_T813_Record_Pnd_T813_Mail_Zip;
    private DbsField pnd_T813_Record_Pnd_T813_Register_Id;
    private DbsField pnd_T813_Record_Pnd_T813_Divsub;
    private DbsField pnd_T813_Record_Pnd_T813_Enrollment_Type;
    private DbsField pnd_T813_Record_Pnd_T813_Vesting;

    private DbsGroup pnd_T813_Record_Pnd_T813_Flags;
    private DbsField pnd_T813_Record_Pnd_T813_Update_Address;
    private DbsField pnd_T813_Record_Pnd_T813_Update_Divorce;
    private DbsField pnd_T813_Record_Pnd_T813_Update_Add_Cref;
    private DbsField pnd_T813_Record_Pnd_T813_Update_Pull_Code;
    private DbsField pnd_T813_Record_Pnd_T813_Update_Incompl_Data_Ind;
    private DbsField pnd_T813_Record_Pnd_T813_Update_Alloc_Model_Ind;
    private DbsField pnd_T813_Record_Pnd_T813_Update_Cai_Ind;
    private DbsField pnd_T813_Record_Pnd_T813_Update_App_Rcvd;
    private DbsField pnd_T813_Record_Pnd_T813_Eir_Ind;
    private DbsField pnd_T813_Record_Pnd_Orchestration_Id;
    private DbsField pnd_T813_Record_Pnd_T813_Substitution_Contract_Ind;
    private DbsField pnd_T813_Record_Pnd_T813_Deceased_Ind;
    private DbsField pnd_T813_Record_Pnd_T813_Oneira_Account;
    private DbsField pnd_Pi_Interface_Rec;

    private DbsGroup pnd_Pi_Interface_Rec__R_Field_9;
    private DbsField pnd_Pi_Interface_Rec_Pnd_Task_Id;
    private DbsField pnd_Pi_Interface_Rec_Pnd_Pass_Fail_Ind;

    private DbsGroup pnd_Accepted_Report_Rec;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Name;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Divsub;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Ssn;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Return_Msg;

    private DbsGroup pnd_Rejected_Report_Rec;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Name;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Subplan;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Div_Sub;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Ssn;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Return_Code;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg;

    private DbsGroup pnd_Summary_Report_Rec;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Eir_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Expected_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Processed_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed_2;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Records_Reprocessed;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Mntben_Cnt;

    private DbsGroup pnd_Pi_Summary_Report_Rec;
    private DbsField pnd_Pi_Summary_Report_Rec_Pnd_Pi_Accept_Cnt;
    private DbsField pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt;

    private DbsGroup pnd_Pi_Report_Rec;
    private DbsField pnd_Pi_Report_Rec_Pnd_Pi_Rept_Plan_No;
    private DbsField pnd_Pi_Report_Rec_Pnd_Pi_Rept_Subplan_No;
    private DbsField pnd_Pi_Report_Rec_Pnd_Pi_Rept_Ssn;
    private DbsField pnd_Pi_Report_Rec_Pnd_Pi_Rept_Task_Id;

    private DbsGroup pnd_Pi_Error_Report_Rec;
    private DbsField pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Plan_No;
    private DbsField pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Subplan_No;
    private DbsField pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Ssn;
    private DbsField pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Task_Id;
    private DbsField pnd_Record_Cnt;
    private DbsField pnd_Pi_Cnt;

    private DbsGroup break_Counters;
    private DbsField break_Counters_Pnd_Plan_Cnt;
    private DbsField break_Counters_Pnd_Divsub_Cnt;
    private DbsField break_Counters_Pnd_Err_Plan_Cnt;
    private DbsField break_Counters_Pnd_Err_Divsub_Cnt;
    private DbsField pnd_Hold_Plan_No;
    private DbsField pnd_Hold_Err_Plan_No;
    private DbsField pnd_Hold_Divsub;
    private DbsField pnd_Hold_Err_Divsub;
    private DbsField pnd_Hold_Product;
    private DbsField pnd_Hold_Issue_Date;

    private DbsGroup pnd_Hold_Issue_Date__R_Field_10;
    private DbsField pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Ccyy;
    private DbsField pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Mm;
    private DbsField pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Dd;
    private DbsField pnd_Restarted;
    private DbsField pnd_Skip_Restart_Record;
    private DbsField pnd_Pickup_Omni_Contracts;
    private DbsField pnd_Pi_Reject_Flag;
    private DbsField pnd_Pi_Type;
    private DbsField pnd_Acis_Reject_Flag;
    private DbsField pnd_Scia1200_Field_3;

    private DbsGroup pnd_Scia1200_Field_3__R_Field_11;
    private DbsField pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Tiaa_Contract;
    private DbsField pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Cref_Contract;
    private DbsField pnd_Current_Business_Date;

    private DbsGroup pnd_Current_Business_Date__R_Field_12;
    private DbsField pnd_Current_Business_Date_Pnd_Current_Business_Date_A;
    private DbsField pnd_Ira_Sub_Global_Start_Date;
    private DbsField pnd_Continue;
    private DbsField pnd_Retry_Cnt;
    private DbsField intvl;
    private DbsField reqid;
    private DbsField pnd_Jobname;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_A;
    private DbsField i;
    private DbsField pnd_Num_Contracts;
    private DbsField pnd_Scin9913_Return_Code;
    private DbsField pnd_Scin9913_Return_Msg;
    private DbsField pnd_Calc_Issue_State;
    private DbsField pnd_Debug;
    private DbsField pnd_Icap_Product;
    private DbsField pnd_Indx_Product;
    private DbsField pnd_Exception_Found;
    private DbsField pnd_Contract_Exists;
    private DbsField pnd_Wk_Guar_Rate_Ind;
    private DbsField pnd_Plan_No_A6;

    private DbsGroup pnd_Plan_No_A6__R_Field_13;
    private DbsField pnd_Plan_No_A6_Pnd_Plan_No_A3;
    private DbsField pnd_Wk_Text_Udf_2;

    private DbsGroup pnd_Wk_Text_Udf_2__R_Field_14;
    private DbsField pnd_Wk_Text_Udf_2_Pnd_Wk_Pl_Client_Id;
    private DbsField pnd_Wk_Text_Udf_2_Pnd_Wk_Model_Id;
    private DbsField pnd_Wk_Text_Udf_3;

    private DbsGroup pnd_Wk_Text_Udf_3__R_Field_15;
    private DbsField pnd_Wk_Text_Udf_3_Pnd_Wk_Addr_Chg_Ind;
    private DbsField pnd_Wk_Text_Udf_3_Pnd_Wk_Name_Chg_Ind;
    private DbsField pnd_Wk_Text_Udf_3_Pnd_Wk_Rea_Threshold_Amt;
    private DbsField pnd_Wk_Text_Udf_3_Pnd_Filler_1;
    private DbsField pnd_Rc;
    private DbsField pnd_Ws_Issue_Date;

    private DbsGroup pnd_Ws_Issue_Date__R_Field_16;
    private DbsField pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Mm;
    private DbsField pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler1;
    private DbsField pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Dd;
    private DbsField pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler2;
    private DbsField pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Ccyy;
    private DbsField pnd_Reject_Sw;
    private DbsField pnd_Check_Issue_St;
    private DbsField pnd_Bene_Dflt_Plans;
    private DbsField pnd_Plan_Key;

    private DbsGroup pnd_Plan_Key__R_Field_17;

    private DbsGroup pnd_Plan_Key_Pnd_Plan_Key_Structure;
    private DbsField pnd_Plan_Key_Entry_Table_Id_Nbr;
    private DbsField pnd_Plan_Key_Entry_Table_Sub_Id;
    private DbsField pnd_Plan_Key_Entry_Cde;
    private DbsField pnd_Number_Of_Bene_Dflt_Plans;

    private DataAccessProgramView vw_reprint;
    private DbsField reprint_Rp_Tiaa_Contr;
    private DbsField reprint_Rp_Annuity_Start_Date;
    private DbsField pnd_Use_Acis_Annuity_Start_Date;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaScia1200 = new PdaScia1200(localVariables);
        pdaScia1295 = new PdaScia1295(localVariables);
        pdaScia9000 = new PdaScia9000(localVariables);
        ldaScil9000 = new LdaScil9000();
        registerRecord(ldaScil9000);
        pdaScia9002 = new PdaScia9002(localVariables);
        pdaScia9003 = new PdaScia9003(localVariables);
        pdaScia2200 = new PdaScia2200(localVariables);
        pdaScia2100 = new PdaScia2100(localVariables);
        ldaScil9020 = new LdaScil9020();
        registerRecord(ldaScil9020);
        pdaScia1250 = new PdaScia1250(localVariables);

        // Local Variables
        pls_Error_Occurence = WsIndependent.getInstance().newFieldInRecord("pls_Error_Occurence", "+ERROR-OCCURENCE", FieldType.BOOLEAN, 1);
        pls_Dar_Program = WsIndependent.getInstance().newFieldInRecord("pls_Dar_Program", "+DAR-PROGRAM", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        // Local Variables

        vw_batch_Restart = new DataAccessProgramView(new NameInfo("vw_batch_Restart", "BATCH-RESTART"), "BATCH_RESTART", "BATCH_RESTART", DdmPeriodicGroups.getInstance().getGroups("BATCH_RESTART"));
        batch_Restart_Rst_Actve_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Actve_Ind", "RST-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RST_ACTVE_IND");
        batch_Restart_Rst_Actve_Ind.setDdmHeader("ACTIVE IND");
        batch_Restart_Rst_Job_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Job_Nme", "RST-JOB-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_JOB_NME");
        batch_Restart_Rst_Job_Nme.setDdmHeader("JOB NAME");
        batch_Restart_Rst_Jobstep_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Jobstep_Nme", "RST-JOBSTEP-NME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_JOBSTEP_NME");
        batch_Restart_Rst_Jobstep_Nme.setDdmHeader("STEP NAME");
        batch_Restart_Rst_Pgm_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Pgm_Nme", "RST-PGM-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_PGM_NME");
        batch_Restart_Rst_Pgm_Nme.setDdmHeader("PROGRAM NAME");
        batch_Restart_Rst_Curr_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Dte", "RST-CURR-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_DTE");
        batch_Restart_Rst_Curr_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Tme", "RST-CURR-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_TME");
        batch_Restart_Rst_Start_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Dte", "RST-START-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_DTE");
        batch_Restart_Rst_Start_Dte.setDdmHeader("START/DATE");
        batch_Restart_Rst_Start_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Tme", "RST-START-TME", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_TME");
        batch_Restart_Rst_Start_Tme.setDdmHeader("START/TIME");
        batch_Restart_Rst_End_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Dte", "RST-END-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_DTE");
        batch_Restart_Rst_End_Dte.setDdmHeader("END/DATE");
        batch_Restart_Rst_End_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Tme", "RST-END-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_TME");
        batch_Restart_Rst_End_Tme.setDdmHeader("END/TIME");
        batch_Restart_Rst_Key = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Key", "RST-KEY", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "RST_KEY");
        batch_Restart_Rst_Cnt = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Cnt", "RST-CNT", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "RST_CNT");
        batch_Restart_Rst_Isn_Nbr = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Isn_Nbr", "RST-ISN-NBR", FieldType.PACKED_DECIMAL, 
            10, RepeatingFieldStrategy.None, "RST_ISN_NBR");
        batch_Restart_Rst_Rstrt_Data_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Rstrt_Data_Ind", "RST-RSTRT-DATA-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RST_RSTRT_DATA_IND");
        batch_Restart_Rst_Rstrt_Data_Ind.setDdmHeader("FAILED");

        batch_Restart_Rst_Data_Grp = vw_batch_Restart.getRecord().newGroupInGroup("batch_Restart_Rst_Data_Grp", "RST-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Data_Txt = batch_Restart_Rst_Data_Grp.newFieldArrayInGroup("batch_Restart_Rst_Data_Txt", "RST-DATA-TXT", FieldType.STRING, 250, 
            new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RST_DATA_TXT", "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Frst_Run_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Frst_Run_Dte", "RST-FRST-RUN-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_FRST_RUN_DTE");
        batch_Restart_Rst_Frst_Run_Dte.setDdmHeader("FIRST/RUN DTE");
        batch_Restart_Rst_Last_Rstrt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Dte", "RST-LAST-RSTRT-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_DTE");
        batch_Restart_Rst_Last_Rstrt_Dte.setDdmHeader("LAST RSTRT/DATE");
        batch_Restart_Rst_Last_Rstrt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Tme", "RST-LAST-RSTRT-TME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_TME");
        batch_Restart_Rst_Last_Rstrt_Tme.setDdmHeader("LAST RSTRT/TIME");
        batch_Restart_Rst_Updt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Dte", "RST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_DTE");
        batch_Restart_Rst_Updt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Tme", "RST-UPDT-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_TME");
        registerRecord(vw_batch_Restart);

        bene_File = localVariables.newGroupInRecord("bene_File", "BENE-FILE");

        bene_File_Bene_File_Data = bene_File.newGroupInGroup("bene_File_Bene_File_Data", "BENE-FILE-DATA");
        bene_File_Bene_Intrfcng_Systm = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfcng_Systm", "BENE-INTRFCNG-SYSTM", FieldType.STRING, 
            8);
        bene_File_Bene_Rqstng_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqstng_System", "BENE-RQSTNG-SYSTEM", FieldType.STRING, 
            10);
        bene_File_Bene_Intrfce_Bsnss_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Bsnss_Dte", "BENE-INTRFCE-BSNSS-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Intrfce_Mgrtn_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Mgrtn_Ind", "BENE-INTRFCE-MGRTN-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Nmbr_Of_Benes = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Nmbr_Of_Benes", "BENE-NMBR-OF-BENES", FieldType.NUMERIC, 
            3);
        bene_File_Bene_Tiaa_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Nbr", "BENE-TIAA-NBR", FieldType.STRING, 10);
        bene_File_Bene_Cref_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Cref_Nbr", "BENE-CREF-NBR", FieldType.STRING, 10);
        bene_File_Bene_Pin_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pin_Nbr", "BENE-PIN-NBR", FieldType.NUMERIC, 12);
        bene_File_Bene_Lob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob", "BENE-LOB", FieldType.STRING, 1);
        bene_File_Bene_Lob_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob_Type", "BENE-LOB-TYPE", FieldType.STRING, 1);
        bene_File_Bene_Part_Prfx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Prfx", "BENE-PART-PRFX", FieldType.STRING, 10);
        bene_File_Bene_Part_Sffx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Sffx", "BENE-PART-SFFX", FieldType.STRING, 10);
        bene_File_Bene_Part_First_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_First_Nme", "BENE-PART-FIRST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Middle_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Middle_Nme", "BENE-PART-MIDDLE-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Last_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Last_Nme", "BENE-PART-LAST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Ssn = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Ssn", "BENE-PART-SSN", FieldType.NUMERIC, 9);
        bene_File_Bene_Part_Dob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Dob", "BENE-PART-DOB", FieldType.NUMERIC, 8);
        bene_File_Bene_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Estate", "BENE-ESTATE", FieldType.STRING, 1);
        bene_File_Bene_Trust = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust", "BENE-TRUST", FieldType.STRING, 1);
        bene_File_Bene_Category = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Category", "BENE-CATEGORY", FieldType.STRING, 1);
        bene_File_Bene_Effective_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Effective_Dt", "BENE-EFFECTIVE-DT", FieldType.STRING, 8);
        bene_File_Bene_Mos_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Ind", "BENE-MOS-IND", FieldType.STRING, 1);
        bene_File_Bene_Mos_Irrvcble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Irrvcble_Ind", "BENE-MOS-IRRVCBLE-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Pymnt_Chld_Dcsd_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pymnt_Chld_Dcsd_Ind", "BENE-PYMNT-CHLD-DCSD-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Update_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Dt", "BENE-UPDATE-DT", FieldType.STRING, 8);
        bene_File_Bene_Update_Time = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Time", "BENE-UPDATE-TIME", FieldType.STRING, 7);
        bene_File_Bene_Update_By = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_By", "BENE-UPDATE-BY", FieldType.STRING, 8);
        bene_File_Bene_Record_Status = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Record_Status", "BENE-RECORD-STATUS", FieldType.STRING, 
            1);
        bene_File_Bene_Same_As_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Same_As_Ind", "BENE-SAME-AS-IND", FieldType.STRING, 1);
        bene_File_Bene_New_Issuefslash_Chng_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_New_Issuefslash_Chng_Ind", "BENE-NEW-ISSUE/CHNG-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Contract_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Contract_Type", "BENE-CONTRACT-TYPE", FieldType.STRING, 
            1);
        bene_File_Bene_Tiaa_Cref_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Cref_Ind", "BENE-TIAA-CREF-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Stat = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Stat", "BENE-STAT", FieldType.STRING, 1);
        bene_File_Bene_Dflt_To_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Dflt_To_Estate", "BENE-DFLT-TO-ESTATE", FieldType.STRING, 
            1);
        bene_File_Bene_More_Than_Five_Benes_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_More_Than_Five_Benes_Ind", "BENE-MORE-THAN-FIVE-BENES-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Illgble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Illgble_Ind", "BENE-ILLGBLE-IND", FieldType.STRING, 1);
        bene_File_Bene_Exempt_Spouse_Rights = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Exempt_Spouse_Rights", "BENE-EXEMPT-SPOUSE-RIGHTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Spouse_Waived_Bnfts = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Spouse_Waived_Bnfts", "BENE-SPOUSE-WAIVED-BNFTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Trust_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust_Data_Fldr", "BENE-TRUST-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Addr_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Addr_Fldr", "BENE-ADDR-FLDR", FieldType.STRING, 1);
        bene_File_Bene_Co_Owner_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Co_Owner_Data_Fldr", "BENE-CO-OWNER-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Fldr_Log_Dte_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Log_Dte_Tme", "BENE-FLDR-LOG-DTE-TME", FieldType.STRING, 
            15);
        bene_File_Bene_Fldr_Min = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Min", "BENE-FLDR-MIN", FieldType.STRING, 11);
        bene_File_Bene_Fldr_Srce_Id = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Srce_Id", "BENE-FLDR-SRCE-ID", FieldType.STRING, 6);
        bene_File_Bene_Rqst_Timestamp = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqst_Timestamp", "BENE-RQST-TIMESTAMP", FieldType.STRING, 
            15);

        bene_File__R_Field_1 = bene_File_Bene_File_Data.newGroupInGroup("bene_File__R_Field_1", "REDEFINE", bene_File_Bene_Rqst_Timestamp);
        bene_File_Bene_Rqst_Date = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Date", "BENE-RQST-DATE", FieldType.STRING, 8);
        bene_File_Bene_Rqst_Time = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Time", "BENE-RQST-TIME", FieldType.STRING, 7);
        bene_File_Bene_Last_Vrfy_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Dte", "BENE-LAST-VRFY-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Vrfy_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Tme", "BENE-LAST-VRFY-TME", FieldType.STRING, 
            7);
        bene_File_Bene_Last_Vrfy_Userid = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Userid", "BENE-LAST-VRFY-USERID", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Dsgntn_Srce = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_Srce", "BENE-LAST-DSGNTN-SRCE", FieldType.STRING, 
            1);
        bene_File_Bene_Last_Dsgntn_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_System", "BENE-LAST-DSGNTN-SYSTEM", FieldType.STRING, 
            1);

        bene_File_Bene_Data = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Data", "BENE-DATA");
        bene_File_Bene_Type = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Name", "BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Extended_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Extended_Name", "BENE-EXTENDED-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 40));
        bene_File_Bene_Ssn_Cd = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Cd", "BENE-SSN-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Ssn_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Nbr", "BENE-SSN-NBR", FieldType.STRING, 9, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dob = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dob", "BENE-DOB", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dte_Birth_Trust = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dte_Birth_Trust", "BENE-DTE-BIRTH-TRUST", FieldType.STRING, 
            8, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Free_Txt = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Free_Txt", "BENE-RELATIONSHIP-FREE-TXT", 
            FieldType.STRING, 15, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Cde = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Cde", "BENE-RELATIONSHIP-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 40));
        bene_File_Bene_Prctge_Frctn_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Prctge_Frctn_Ind", "BENE-PRCTGE-FRCTN-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Irrvcbl_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Irrvcbl_Ind", "BENE-IRRVCBL-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Alloc_Pct = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Alloc_Pct", "BENE-ALLOC-PCT", FieldType.NUMERIC, 5, 2, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Nmrtr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Nmrtr_Nbr", "BENE-NMRTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dnmntr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dnmntr_Nbr", "BENE-DNMNTR-NBR", FieldType.NUMERIC, 3, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Std_Txt_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Std_Txt_Ind", "BENE-STD-TXT-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Sttlmnt_Rstrctn = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Sttlmnt_Rstrctn", "BENE-STTLMNT-RSTRCTN", FieldType.STRING, 
            1, new DbsArrayController(1, 40));

        bene_File_Bene_Addtl_Info = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Addtl_Info", "BENE-ADDTL-INFO");
        bene_File_Bene_Addr1 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr2 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr3_City = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, 
            new DbsArrayController(1, 40));
        bene_File_Bene_State = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_State", "BENE-STATE", FieldType.STRING, 2, new DbsArrayController(1, 
            40));
        bene_File_Bene_Zip = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, new DbsArrayController(1, 
            40));
        bene_File_Bene_Country = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Phone = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, new DbsArrayController(1, 
            40));
        bene_File_Bene_Gender = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Spcl_Txt = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Spcl_Txt", "BENE-SPCL-TXT", FieldType.STRING, 72, new 
            DbsArrayController(1, 40, 1, 3));
        bene_File_Bene_Mdo_Calc_Bene = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Mdo_Calc_Bene", "BENE-MDO-CALC-BENE", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Spcl_Dsgn_Txt = bene_File_Bene_File_Data.newFieldArrayInGroup("bene_File_Bene_Spcl_Dsgn_Txt", "BENE-SPCL-DSGN-TXT", FieldType.STRING, 
            72, new DbsArrayController(1, 60));
        bene_File_Bene_Hold_Cde = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Hold_Cde", "BENE-HOLD-CDE", FieldType.STRING, 8);
        bene_File_Pnd_Table_Rltn = bene_File.newFieldInGroup("bene_File_Pnd_Table_Rltn", "#TABLE-RLTN", FieldType.STRING, 1683);

        bene_File__R_Field_2 = bene_File.newGroupInGroup("bene_File__R_Field_2", "REDEFINE", bene_File_Pnd_Table_Rltn);
        bene_File_Pnd_Rltn_Cd = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Cd", "#RLTN-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            99));
        bene_File_Pnd_Rltn_Text = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Text", "#RLTN-TEXT", FieldType.STRING, 15, new DbsArrayController(1, 
            99));
        bene_File_Bene_Total_Contract_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Contract_Written", "BENE-TOTAL-CONTRACT-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Mos_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Mos_Written", "BENE-TOTAL-MOS-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Dest_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Dest_Written", "BENE-TOTAL-DEST-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Return_Code = bene_File.newFieldInGroup("bene_File_Bene_Return_Code", "BENE-RETURN-CODE", FieldType.STRING, 1);

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        app_Table_Entry__R_Field_3 = vw_app_Table_Entry.getRecord().newGroupInGroup("app_Table_Entry__R_Field_3", "REDEFINE", app_Table_Entry_Entry_Cde);
        app_Table_Entry_Entry_Rule_Category = app_Table_Entry__R_Field_3.newFieldInGroup("app_Table_Entry_Entry_Rule_Category", "ENTRY-RULE-CATEGORY", 
            FieldType.STRING, 14);
        app_Table_Entry_Entry_Rule_Plan = app_Table_Entry__R_Field_3.newFieldInGroup("app_Table_Entry_Entry_Rule_Plan", "ENTRY-RULE-PLAN", FieldType.STRING, 
            6);
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        app_Table_Entry_Entry_User_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        registerRecord(vw_app_Table_Entry);

        pnd_Restart_Key = localVariables.newFieldInRecord("pnd_Restart_Key", "#RESTART-KEY", FieldType.STRING, 50);

        pnd_Restart_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Restart_Key__R_Field_4", "REDEFINE", pnd_Restart_Key);

        pnd_Restart_Key_Pnd_Restart_Structure = pnd_Restart_Key__R_Field_4.newGroupInGroup("pnd_Restart_Key_Pnd_Restart_Structure", "#RESTART-STRUCTURE");
        pnd_Restart_Key_Pnd_Restart_Plan_No = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Plan_No", "#RESTART-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Restart_Key_Pnd_Restart_Subplan_No = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Subplan_No", "#RESTART-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Restart_Key_Pnd_Restart_Ssn = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ssn", "#RESTART-SSN", FieldType.STRING, 
            9);
        pnd_Restart_Key_Pnd_Restart_Ext = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ext", "#RESTART-EXT", FieldType.STRING, 
            2);
        pnd_Sp_Actve_Job_Step = localVariables.newFieldInRecord("pnd_Sp_Actve_Job_Step", "#SP-ACTVE-JOB-STEP", FieldType.STRING, 17);

        pnd_Sp_Actve_Job_Step__R_Field_5 = localVariables.newGroupInRecord("pnd_Sp_Actve_Job_Step__R_Field_5", "REDEFINE", pnd_Sp_Actve_Job_Step);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind = pnd_Sp_Actve_Job_Step__R_Field_5.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind", "#RST-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme = pnd_Sp_Actve_Job_Step__R_Field_5.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme", "#RST-JOB-NME", 
            FieldType.STRING, 8);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme = pnd_Sp_Actve_Job_Step__R_Field_5.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme", "#RST-STEP-NME", 
            FieldType.STRING, 8);
        pnd_Restart_Isn = localVariables.newFieldInRecord("pnd_Restart_Isn", "#RESTART-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Last_Contract_Processed = localVariables.newFieldInRecord("pnd_Last_Contract_Processed", "#LAST-CONTRACT-PROCESSED", FieldType.STRING, 8);

        pnd_T813_Record = localVariables.newGroupInRecord("pnd_T813_Record", "#T813-RECORD");
        pnd_T813_Record_Pnd_T813_Plan_No = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Plan_No", "#T813-PLAN-NO", FieldType.STRING, 6);
        pnd_T813_Record_Pnd_T813_Part_Id = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Part_Id", "#T813-PART-ID", FieldType.STRING, 17);

        pnd_T813_Record__R_Field_6 = pnd_T813_Record.newGroupInGroup("pnd_T813_Record__R_Field_6", "REDEFINE", pnd_T813_Record_Pnd_T813_Part_Id);
        pnd_T813_Record_Pnd_T813_Ssn = pnd_T813_Record__R_Field_6.newFieldInGroup("pnd_T813_Record_Pnd_T813_Ssn", "#T813-SSN", FieldType.STRING, 9);
        pnd_T813_Record_Pnd_T813_Subplan_No = pnd_T813_Record__R_Field_6.newFieldInGroup("pnd_T813_Record_Pnd_T813_Subplan_No", "#T813-SUBPLAN-NO", FieldType.STRING, 
            6);
        pnd_T813_Record_Pnd_T813_Ext = pnd_T813_Record__R_Field_6.newFieldInGroup("pnd_T813_Record_Pnd_T813_Ext", "#T813-EXT", FieldType.STRING, 2);
        pnd_T813_Record_Pnd_T813_Tiaa_Contract = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Tiaa_Contract", "#T813-TIAA-CONTRACT", FieldType.STRING, 
            10);
        pnd_T813_Record_Pnd_T813_Cref_Contract = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Cref_Contract", "#T813-CREF-CONTRACT", FieldType.STRING, 
            10);
        pnd_T813_Record_Pnd_T813_Pin_No = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Pin_No", "#T813-PIN-NO", FieldType.STRING, 12);
        pnd_T813_Record_Pnd_T813_Trade_Date = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Trade_Date", "#T813-TRADE-DATE", FieldType.STRING, 
            8);
        pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date", "#T813-TIAA-ISSUE-DATE", 
            FieldType.STRING, 8);

        pnd_T813_Record__R_Field_7 = pnd_T813_Record.newGroupInGroup("pnd_T813_Record__R_Field_7", "REDEFINE", pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date);
        pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Ccyy = pnd_T813_Record__R_Field_7.newFieldInGroup("pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Ccyy", "#T813-TIAA-ISSUE-DATE-CCYY", 
            FieldType.STRING, 4);
        pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Mm = pnd_T813_Record__R_Field_7.newFieldInGroup("pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Mm", "#T813-TIAA-ISSUE-DATE-MM", 
            FieldType.STRING, 2);
        pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Dd = pnd_T813_Record__R_Field_7.newFieldInGroup("pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date_Dd", "#T813-TIAA-ISSUE-DATE-DD", 
            FieldType.STRING, 2);
        pnd_T813_Record_Pnd_T813_Cref_Issue_Date = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Cref_Issue_Date", "#T813-CREF-ISSUE-DATE", 
            FieldType.STRING, 8);

        pnd_T813_Record__R_Field_8 = pnd_T813_Record.newGroupInGroup("pnd_T813_Record__R_Field_8", "REDEFINE", pnd_T813_Record_Pnd_T813_Cref_Issue_Date);
        pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Ccyy = pnd_T813_Record__R_Field_8.newFieldInGroup("pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Ccyy", "#T813-CREF-ISSUE-DATE-CCYY", 
            FieldType.STRING, 4);
        pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Mm = pnd_T813_Record__R_Field_8.newFieldInGroup("pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Mm", "#T813-CREF-ISSUE-DATE-MM", 
            FieldType.STRING, 2);
        pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Dd = pnd_T813_Record__R_Field_8.newFieldInGroup("pnd_T813_Record_Pnd_T813_Cref_Issue_Date_Dd", "#T813-CREF-ISSUE-DATE-DD", 
            FieldType.STRING, 2);
        pnd_T813_Record_Pnd_T813_Annty_Start_Date = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Annty_Start_Date", "#T813-ANNTY-START-DATE", 
            FieldType.STRING, 8);
        pnd_T813_Record_Pnd_T813_App_Received_Date = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_App_Received_Date", "#T813-APP-RECEIVED-DATE", 
            FieldType.STRING, 8);
        pnd_T813_Record_Pnd_T813_Issue_State = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Issue_State", "#T813-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_T813_Record_Pnd_T813_Mail_Address_Line = pnd_T813_Record.newFieldArrayInGroup("pnd_T813_Record_Pnd_T813_Mail_Address_Line", "#T813-MAIL-ADDRESS-LINE", 
            FieldType.STRING, 35, new DbsArrayController(1, 3));
        pnd_T813_Record_Pnd_T813_Mail_City = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Mail_City", "#T813-MAIL-CITY", FieldType.STRING, 
            27);
        pnd_T813_Record_Pnd_T813_Mail_State = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Mail_State", "#T813-MAIL-STATE", FieldType.STRING, 
            2);
        pnd_T813_Record_Pnd_T813_Mail_Zip = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Mail_Zip", "#T813-MAIL-ZIP", FieldType.STRING, 9);
        pnd_T813_Record_Pnd_T813_Register_Id = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Register_Id", "#T813-REGISTER-ID", FieldType.STRING, 
            11);
        pnd_T813_Record_Pnd_T813_Divsub = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Divsub", "#T813-DIVSUB", FieldType.STRING, 4);
        pnd_T813_Record_Pnd_T813_Enrollment_Type = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Enrollment_Type", "#T813-ENROLLMENT-TYPE", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Vesting = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Vesting", "#T813-VESTING", FieldType.STRING, 1);

        pnd_T813_Record_Pnd_T813_Flags = pnd_T813_Record.newGroupInGroup("pnd_T813_Record_Pnd_T813_Flags", "#T813-FLAGS");
        pnd_T813_Record_Pnd_T813_Update_Address = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_Address", "#T813-UPDATE-ADDRESS", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Update_Divorce = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_Divorce", "#T813-UPDATE-DIVORCE", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Update_Add_Cref = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_Add_Cref", "#T813-UPDATE-ADD-CREF", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Update_Pull_Code = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_Pull_Code", "#T813-UPDATE-PULL-CODE", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Update_Incompl_Data_Ind = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_Incompl_Data_Ind", 
            "#T813-UPDATE-INCOMPL-DATA-IND", FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Update_Alloc_Model_Ind = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_Alloc_Model_Ind", 
            "#T813-UPDATE-ALLOC-MODEL-IND", FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Update_Cai_Ind = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_Cai_Ind", "#T813-UPDATE-CAI-IND", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Update_App_Rcvd = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Update_App_Rcvd", "#T813-UPDATE-APP-RCVD", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Eir_Ind = pnd_T813_Record_Pnd_T813_Flags.newFieldInGroup("pnd_T813_Record_Pnd_T813_Eir_Ind", "#T813-EIR-IND", FieldType.STRING, 
            1);
        pnd_T813_Record_Pnd_Orchestration_Id = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_Orchestration_Id", "#ORCHESTRATION-ID", FieldType.STRING, 
            15);
        pnd_T813_Record_Pnd_T813_Substitution_Contract_Ind = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Substitution_Contract_Ind", "#T813-SUBSTITUTION-CONTRACT-IND", 
            FieldType.STRING, 1);
        pnd_T813_Record_Pnd_T813_Deceased_Ind = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Deceased_Ind", "#T813-DECEASED-IND", FieldType.STRING, 
            1);
        pnd_T813_Record_Pnd_T813_Oneira_Account = pnd_T813_Record.newFieldInGroup("pnd_T813_Record_Pnd_T813_Oneira_Account", "#T813-ONEIRA-ACCOUNT", FieldType.STRING, 
            10);
        pnd_Pi_Interface_Rec = localVariables.newFieldInRecord("pnd_Pi_Interface_Rec", "#PI-INTERFACE-REC", FieldType.STRING, 16);

        pnd_Pi_Interface_Rec__R_Field_9 = localVariables.newGroupInRecord("pnd_Pi_Interface_Rec__R_Field_9", "REDEFINE", pnd_Pi_Interface_Rec);
        pnd_Pi_Interface_Rec_Pnd_Task_Id = pnd_Pi_Interface_Rec__R_Field_9.newFieldInGroup("pnd_Pi_Interface_Rec_Pnd_Task_Id", "#TASK-ID", FieldType.STRING, 
            15);
        pnd_Pi_Interface_Rec_Pnd_Pass_Fail_Ind = pnd_Pi_Interface_Rec__R_Field_9.newFieldInGroup("pnd_Pi_Interface_Rec_Pnd_Pass_Fail_Ind", "#PASS-FAIL-IND", 
            FieldType.STRING, 1);

        pnd_Accepted_Report_Rec = localVariables.newGroupInRecord("pnd_Accepted_Report_Rec", "#ACCEPTED-REPORT-REC");
        pnd_Accepted_Report_Rec_Pnd_Rept_Name = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Name", "#REPT-NAME", FieldType.STRING, 
            60);
        pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No", "#REPT-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Accepted_Report_Rec_Pnd_Rept_Divsub = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Divsub", "#REPT-DIVSUB", FieldType.STRING, 
            4);
        pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract", "#REPT-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract", "#REPT-CREF-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Accepted_Report_Rec_Pnd_Rept_Ssn = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Ssn", "#REPT-SSN", FieldType.STRING, 
            9);
        pnd_Accepted_Report_Rec_Pnd_Return_Msg = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Return_Msg", "#RETURN-MSG", FieldType.STRING, 
            50);

        pnd_Rejected_Report_Rec = localVariables.newGroupInRecord("pnd_Rejected_Report_Rec", "#REJECTED-REPORT-REC");
        pnd_Rejected_Report_Rec_Pnd_Error_Name = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Name", "#ERROR-NAME", FieldType.STRING, 
            60);
        pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number", "#ERROR-PLAN-NUMBER", 
            FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Subplan = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Subplan", "#ERROR-SUBPLAN", 
            FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Div_Sub = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Div_Sub", "#ERROR-DIV-SUB", 
            FieldType.STRING, 4);
        pnd_Rejected_Report_Rec_Pnd_Error_Ssn = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Ssn", "#ERROR-SSN", FieldType.STRING, 
            9);
        pnd_Rejected_Report_Rec_Pnd_Error_Return_Code = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Return_Code", "#ERROR-RETURN-CODE", 
            FieldType.STRING, 4);
        pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg", "#ERROR-RETURN-MSG", 
            FieldType.STRING, 50);

        pnd_Summary_Report_Rec = localVariables.newGroupInRecord("pnd_Summary_Report_Rec", "#SUMMARY-REPORT-REC");
        pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt", "#TOT-TRANS-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt", "#TOT-ACCEPT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt", "#TOT-REJECT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Eir_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Eir_Cnt", "#TOT-EIR-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Expected_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Expected_Cnt", 
            "#TOT-INDEXED-EXPECTED-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Processed_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Processed_Cnt", 
            "#TOT-INDEXED-PROCESSED-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed", 
            "#TOT-INDEXED-NOT-PROCESSED", FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed_2 = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed_2", 
            "#TOT-INDEXED-NOT-PROCESSED-2", FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Records_Reprocessed = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Records_Reprocessed", 
            "#TOT-RECORDS-REPROCESSED", FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Mntben_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Mntben_Cnt", "#TOT-MNTBEN-CNT", 
            FieldType.PACKED_DECIMAL, 9);

        pnd_Pi_Summary_Report_Rec = localVariables.newGroupInRecord("pnd_Pi_Summary_Report_Rec", "#PI-SUMMARY-REPORT-REC");
        pnd_Pi_Summary_Report_Rec_Pnd_Pi_Accept_Cnt = pnd_Pi_Summary_Report_Rec.newFieldInGroup("pnd_Pi_Summary_Report_Rec_Pnd_Pi_Accept_Cnt", "#PI-ACCEPT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt = pnd_Pi_Summary_Report_Rec.newFieldInGroup("pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt", "#PI-REJECT-CNT", 
            FieldType.PACKED_DECIMAL, 9);

        pnd_Pi_Report_Rec = localVariables.newGroupInRecord("pnd_Pi_Report_Rec", "#PI-REPORT-REC");
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Plan_No = pnd_Pi_Report_Rec.newFieldInGroup("pnd_Pi_Report_Rec_Pnd_Pi_Rept_Plan_No", "#PI-REPT-PLAN-NO", FieldType.STRING, 
            6);
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Subplan_No = pnd_Pi_Report_Rec.newFieldInGroup("pnd_Pi_Report_Rec_Pnd_Pi_Rept_Subplan_No", "#PI-REPT-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Ssn = pnd_Pi_Report_Rec.newFieldInGroup("pnd_Pi_Report_Rec_Pnd_Pi_Rept_Ssn", "#PI-REPT-SSN", FieldType.STRING, 9);
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Task_Id = pnd_Pi_Report_Rec.newFieldInGroup("pnd_Pi_Report_Rec_Pnd_Pi_Rept_Task_Id", "#PI-REPT-TASK-ID", FieldType.STRING, 
            15);

        pnd_Pi_Error_Report_Rec = localVariables.newGroupInRecord("pnd_Pi_Error_Report_Rec", "#PI-ERROR-REPORT-REC");
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Plan_No = pnd_Pi_Error_Report_Rec.newFieldInGroup("pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Plan_No", "#PI-ERR-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Subplan_No = pnd_Pi_Error_Report_Rec.newFieldInGroup("pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Subplan_No", "#PI-ERR-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Ssn = pnd_Pi_Error_Report_Rec.newFieldInGroup("pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Ssn", "#PI-ERR-SSN", FieldType.STRING, 
            9);
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Task_Id = pnd_Pi_Error_Report_Rec.newFieldInGroup("pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Task_Id", "#PI-ERR-TASK-ID", 
            FieldType.STRING, 15);
        pnd_Record_Cnt = localVariables.newFieldInRecord("pnd_Record_Cnt", "#RECORD-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Pi_Cnt = localVariables.newFieldInRecord("pnd_Pi_Cnt", "#PI-CNT", FieldType.PACKED_DECIMAL, 9);

        break_Counters = localVariables.newGroupInRecord("break_Counters", "BREAK-COUNTERS");
        break_Counters_Pnd_Plan_Cnt = break_Counters.newFieldInGroup("break_Counters_Pnd_Plan_Cnt", "#PLAN-CNT", FieldType.PACKED_DECIMAL, 6);
        break_Counters_Pnd_Divsub_Cnt = break_Counters.newFieldInGroup("break_Counters_Pnd_Divsub_Cnt", "#DIVSUB-CNT", FieldType.PACKED_DECIMAL, 6);
        break_Counters_Pnd_Err_Plan_Cnt = break_Counters.newFieldInGroup("break_Counters_Pnd_Err_Plan_Cnt", "#ERR-PLAN-CNT", FieldType.PACKED_DECIMAL, 
            6);
        break_Counters_Pnd_Err_Divsub_Cnt = break_Counters.newFieldInGroup("break_Counters_Pnd_Err_Divsub_Cnt", "#ERR-DIVSUB-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Hold_Plan_No = localVariables.newFieldInRecord("pnd_Hold_Plan_No", "#HOLD-PLAN-NO", FieldType.STRING, 6);
        pnd_Hold_Err_Plan_No = localVariables.newFieldInRecord("pnd_Hold_Err_Plan_No", "#HOLD-ERR-PLAN-NO", FieldType.STRING, 6);
        pnd_Hold_Divsub = localVariables.newFieldInRecord("pnd_Hold_Divsub", "#HOLD-DIVSUB", FieldType.STRING, 4);
        pnd_Hold_Err_Divsub = localVariables.newFieldInRecord("pnd_Hold_Err_Divsub", "#HOLD-ERR-DIVSUB", FieldType.STRING, 4);
        pnd_Hold_Product = localVariables.newFieldInRecord("pnd_Hold_Product", "#HOLD-PRODUCT", FieldType.STRING, 3);
        pnd_Hold_Issue_Date = localVariables.newFieldInRecord("pnd_Hold_Issue_Date", "#HOLD-ISSUE-DATE", FieldType.STRING, 8);

        pnd_Hold_Issue_Date__R_Field_10 = localVariables.newGroupInRecord("pnd_Hold_Issue_Date__R_Field_10", "REDEFINE", pnd_Hold_Issue_Date);
        pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Ccyy = pnd_Hold_Issue_Date__R_Field_10.newFieldInGroup("pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Ccyy", 
            "#HOLD-ISSUE-DATE-CCYY", FieldType.STRING, 4);
        pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Mm = pnd_Hold_Issue_Date__R_Field_10.newFieldInGroup("pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Mm", "#HOLD-ISSUE-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Dd = pnd_Hold_Issue_Date__R_Field_10.newFieldInGroup("pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Dd", "#HOLD-ISSUE-DATE-DD", 
            FieldType.STRING, 2);
        pnd_Restarted = localVariables.newFieldInRecord("pnd_Restarted", "#RESTARTED", FieldType.BOOLEAN, 1);
        pnd_Skip_Restart_Record = localVariables.newFieldInRecord("pnd_Skip_Restart_Record", "#SKIP-RESTART-RECORD", FieldType.BOOLEAN, 1);
        pnd_Pickup_Omni_Contracts = localVariables.newFieldInRecord("pnd_Pickup_Omni_Contracts", "#PICKUP-OMNI-CONTRACTS", FieldType.BOOLEAN, 1);
        pnd_Pi_Reject_Flag = localVariables.newFieldInRecord("pnd_Pi_Reject_Flag", "#PI-REJECT-FLAG", FieldType.STRING, 1);
        pnd_Pi_Type = localVariables.newFieldInRecord("pnd_Pi_Type", "#PI-TYPE", FieldType.STRING, 15);
        pnd_Acis_Reject_Flag = localVariables.newFieldInRecord("pnd_Acis_Reject_Flag", "#ACIS-REJECT-FLAG", FieldType.STRING, 1);
        pnd_Scia1200_Field_3 = localVariables.newFieldInRecord("pnd_Scia1200_Field_3", "#SCIA1200-FIELD-3", FieldType.STRING, 16);

        pnd_Scia1200_Field_3__R_Field_11 = localVariables.newGroupInRecord("pnd_Scia1200_Field_3__R_Field_11", "REDEFINE", pnd_Scia1200_Field_3);
        pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Tiaa_Contract = pnd_Scia1200_Field_3__R_Field_11.newFieldInGroup("pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Tiaa_Contract", 
            "#SCIA1200-ORIG-TIAA-CONTRACT", FieldType.STRING, 8);
        pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Cref_Contract = pnd_Scia1200_Field_3__R_Field_11.newFieldInGroup("pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Cref_Contract", 
            "#SCIA1200-ORIG-CREF-CONTRACT", FieldType.STRING, 8);
        pnd_Current_Business_Date = localVariables.newFieldInRecord("pnd_Current_Business_Date", "#CURRENT-BUSINESS-DATE", FieldType.NUMERIC, 8);

        pnd_Current_Business_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_Current_Business_Date__R_Field_12", "REDEFINE", pnd_Current_Business_Date);
        pnd_Current_Business_Date_Pnd_Current_Business_Date_A = pnd_Current_Business_Date__R_Field_12.newFieldInGroup("pnd_Current_Business_Date_Pnd_Current_Business_Date_A", 
            "#CURRENT-BUSINESS-DATE-A", FieldType.STRING, 8);
        pnd_Ira_Sub_Global_Start_Date = localVariables.newFieldInRecord("pnd_Ira_Sub_Global_Start_Date", "#IRA-SUB-GLOBAL-START-DATE", FieldType.NUMERIC, 
            8);
        pnd_Continue = localVariables.newFieldInRecord("pnd_Continue", "#CONTINUE", FieldType.BOOLEAN, 1);
        pnd_Retry_Cnt = localVariables.newFieldInRecord("pnd_Retry_Cnt", "#RETRY-CNT", FieldType.NUMERIC, 3);
        intvl = localVariables.newFieldInRecord("intvl", "INTVL", FieldType.INTEGER, 1);
        reqid = localVariables.newFieldInRecord("reqid", "REQID", FieldType.STRING, 8);
        pnd_Jobname = localVariables.newFieldInRecord("pnd_Jobname", "#JOBNAME", FieldType.STRING, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        i = localVariables.newFieldInRecord("i", "I", FieldType.NUMERIC, 1);
        pnd_Num_Contracts = localVariables.newFieldInRecord("pnd_Num_Contracts", "#NUM-CONTRACTS", FieldType.NUMERIC, 1);
        pnd_Scin9913_Return_Code = localVariables.newFieldInRecord("pnd_Scin9913_Return_Code", "#SCIN9913-RETURN-CODE", FieldType.STRING, 4);
        pnd_Scin9913_Return_Msg = localVariables.newFieldInRecord("pnd_Scin9913_Return_Msg", "#SCIN9913-RETURN-MSG", FieldType.STRING, 80);
        pnd_Calc_Issue_State = localVariables.newFieldInRecord("pnd_Calc_Issue_State", "#CALC-ISSUE-STATE", FieldType.STRING, 2);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Icap_Product = localVariables.newFieldInRecord("pnd_Icap_Product", "#ICAP-PRODUCT", FieldType.BOOLEAN, 1);
        pnd_Indx_Product = localVariables.newFieldInRecord("pnd_Indx_Product", "#INDX-PRODUCT", FieldType.BOOLEAN, 1);
        pnd_Exception_Found = localVariables.newFieldInRecord("pnd_Exception_Found", "#EXCEPTION-FOUND", FieldType.BOOLEAN, 1);
        pnd_Contract_Exists = localVariables.newFieldInRecord("pnd_Contract_Exists", "#CONTRACT-EXISTS", FieldType.STRING, 1);
        pnd_Wk_Guar_Rate_Ind = localVariables.newFieldInRecord("pnd_Wk_Guar_Rate_Ind", "#WK-GUAR-RATE-IND", FieldType.STRING, 1);
        pnd_Plan_No_A6 = localVariables.newFieldInRecord("pnd_Plan_No_A6", "#PLAN-NO-A6", FieldType.STRING, 6);

        pnd_Plan_No_A6__R_Field_13 = localVariables.newGroupInRecord("pnd_Plan_No_A6__R_Field_13", "REDEFINE", pnd_Plan_No_A6);
        pnd_Plan_No_A6_Pnd_Plan_No_A3 = pnd_Plan_No_A6__R_Field_13.newFieldInGroup("pnd_Plan_No_A6_Pnd_Plan_No_A3", "#PLAN-NO-A3", FieldType.STRING, 3);
        pnd_Wk_Text_Udf_2 = localVariables.newFieldInRecord("pnd_Wk_Text_Udf_2", "#WK-TEXT-UDF-2", FieldType.STRING, 10);

        pnd_Wk_Text_Udf_2__R_Field_14 = localVariables.newGroupInRecord("pnd_Wk_Text_Udf_2__R_Field_14", "REDEFINE", pnd_Wk_Text_Udf_2);
        pnd_Wk_Text_Udf_2_Pnd_Wk_Pl_Client_Id = pnd_Wk_Text_Udf_2__R_Field_14.newFieldInGroup("pnd_Wk_Text_Udf_2_Pnd_Wk_Pl_Client_Id", "#WK-PL-CLIENT-ID", 
            FieldType.STRING, 6);
        pnd_Wk_Text_Udf_2_Pnd_Wk_Model_Id = pnd_Wk_Text_Udf_2__R_Field_14.newFieldInGroup("pnd_Wk_Text_Udf_2_Pnd_Wk_Model_Id", "#WK-MODEL-ID", FieldType.STRING, 
            4);
        pnd_Wk_Text_Udf_3 = localVariables.newFieldInRecord("pnd_Wk_Text_Udf_3", "#WK-TEXT-UDF-3", FieldType.STRING, 10);

        pnd_Wk_Text_Udf_3__R_Field_15 = localVariables.newGroupInRecord("pnd_Wk_Text_Udf_3__R_Field_15", "REDEFINE", pnd_Wk_Text_Udf_3);
        pnd_Wk_Text_Udf_3_Pnd_Wk_Addr_Chg_Ind = pnd_Wk_Text_Udf_3__R_Field_15.newFieldInGroup("pnd_Wk_Text_Udf_3_Pnd_Wk_Addr_Chg_Ind", "#WK-ADDR-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Text_Udf_3_Pnd_Wk_Name_Chg_Ind = pnd_Wk_Text_Udf_3__R_Field_15.newFieldInGroup("pnd_Wk_Text_Udf_3_Pnd_Wk_Name_Chg_Ind", "#WK-NAME-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Wk_Text_Udf_3_Pnd_Wk_Rea_Threshold_Amt = pnd_Wk_Text_Udf_3__R_Field_15.newFieldInGroup("pnd_Wk_Text_Udf_3_Pnd_Wk_Rea_Threshold_Amt", "#WK-REA-THRESHOLD-AMT", 
            FieldType.STRING, 7);
        pnd_Wk_Text_Udf_3_Pnd_Filler_1 = pnd_Wk_Text_Udf_3__R_Field_15.newFieldInGroup("pnd_Wk_Text_Udf_3_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 
            1);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_Ws_Issue_Date = localVariables.newFieldInRecord("pnd_Ws_Issue_Date", "#WS-ISSUE-DATE", FieldType.STRING, 10);

        pnd_Ws_Issue_Date__R_Field_16 = localVariables.newGroupInRecord("pnd_Ws_Issue_Date__R_Field_16", "REDEFINE", pnd_Ws_Issue_Date);
        pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Mm = pnd_Ws_Issue_Date__R_Field_16.newFieldInGroup("pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Mm", "#WS-ISSUE-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler1 = pnd_Ws_Issue_Date__R_Field_16.newFieldInGroup("pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler1", "#WS-ISSUE-DATE-FILLER1", 
            FieldType.STRING, 1);
        pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Dd = pnd_Ws_Issue_Date__R_Field_16.newFieldInGroup("pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Dd", "#WS-ISSUE-DATE-DD", 
            FieldType.STRING, 2);
        pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler2 = pnd_Ws_Issue_Date__R_Field_16.newFieldInGroup("pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler2", "#WS-ISSUE-DATE-FILLER2", 
            FieldType.STRING, 1);
        pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Ccyy = pnd_Ws_Issue_Date__R_Field_16.newFieldInGroup("pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Ccyy", "#WS-ISSUE-DATE-CCYY", 
            FieldType.STRING, 4);
        pnd_Reject_Sw = localVariables.newFieldInRecord("pnd_Reject_Sw", "#REJECT-SW", FieldType.BOOLEAN, 1);
        pnd_Check_Issue_St = localVariables.newFieldInRecord("pnd_Check_Issue_St", "#CHECK-ISSUE-ST", FieldType.BOOLEAN, 1);
        pnd_Bene_Dflt_Plans = localVariables.newFieldArrayInRecord("pnd_Bene_Dflt_Plans", "#BENE-DFLT-PLANS", FieldType.STRING, 6, new DbsArrayController(1, 
            1000));
        pnd_Plan_Key = localVariables.newFieldInRecord("pnd_Plan_Key", "#PLAN-KEY", FieldType.STRING, 28);

        pnd_Plan_Key__R_Field_17 = localVariables.newGroupInRecord("pnd_Plan_Key__R_Field_17", "REDEFINE", pnd_Plan_Key);

        pnd_Plan_Key_Pnd_Plan_Key_Structure = pnd_Plan_Key__R_Field_17.newGroupInGroup("pnd_Plan_Key_Pnd_Plan_Key_Structure", "#PLAN-KEY-STRUCTURE");
        pnd_Plan_Key_Entry_Table_Id_Nbr = pnd_Plan_Key_Pnd_Plan_Key_Structure.newFieldInGroup("pnd_Plan_Key_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Plan_Key_Entry_Table_Sub_Id = pnd_Plan_Key_Pnd_Plan_Key_Structure.newFieldInGroup("pnd_Plan_Key_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Plan_Key_Entry_Cde = pnd_Plan_Key_Pnd_Plan_Key_Structure.newFieldInGroup("pnd_Plan_Key_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20);
        pnd_Number_Of_Bene_Dflt_Plans = localVariables.newFieldInRecord("pnd_Number_Of_Bene_Dflt_Plans", "#NUMBER-OF-BENE-DFLT-PLANS", FieldType.PACKED_DECIMAL, 
            10);

        vw_reprint = new DataAccessProgramView(new NameInfo("vw_reprint", "REPRINT"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE");
        reprint_Rp_Tiaa_Contr = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "RP_TIAA_CONTR");
        reprint_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        reprint_Rp_Annuity_Start_Date = vw_reprint.getRecord().newFieldInGroup("reprint_Rp_Annuity_Start_Date", "RP-ANNUITY-START-DATE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "RP_ANNUITY_START_DATE");
        reprint_Rp_Annuity_Start_Date.setDdmHeader("ANNUITY/START DTE");
        registerRecord(vw_reprint);

        pnd_Use_Acis_Annuity_Start_Date = localVariables.newFieldInRecord("pnd_Use_Acis_Annuity_Start_Date", "#USE-ACIS-ANNUITY-START-DATE", FieldType.BOOLEAN, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_batch_Restart.reset();
        vw_app_Table_Entry.reset();
        vw_reprint.reset();

        ldaScil9000.initializeValues();
        ldaScil9020.initializeValues();

        localVariables.reset();
        pnd_Last_Contract_Processed.setInitialValue(" ");
        pnd_Record_Cnt.setInitialValue(0);
        pnd_Pi_Cnt.setInitialValue(0);
        pnd_Restarted.setInitialValue(false);
        pnd_Skip_Restart_Record.setInitialValue(false);
        intvl.setInitialValue(1);
        reqid.setInitialValue("MYREQID");
        pnd_Use_Acis_Annuity_Start_Date.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9000() throws Exception
    {
        super("Scib9000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIB9000", onError);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        setupReports();
        //*  ONEIRA
        //*                                                                                                                                                               //Natural: FORMAT LS = 132;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60;//Natural: FORMAT ( 3 ) LS = 132 PS = 60;//Natural: FORMAT ( 4 ) LS = 132 PS = 60;//Natural: FORMAT ( 5 ) LS = 132 PS = 60;//Natural: FORMAT ( 6 ) LS = 132 PS = 60;//Natural: FORMAT ( 7 ) LS = 132 PS = 60
        getReports().write(0, "Entered SCIB9000 :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                  //Natural: WRITE 'Entered SCIB9000 :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        //*  OVERRIDES NATURAL ERROR ROUTINE
        //*  THIS REPLACES *STARTUP LOGIC.
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        pnd_Current_Business_Date.setValue(Global.getDATN());                                                                                                             //Natural: ASSIGN #CURRENT-BUSINESS-DATE := *DATN
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pls_Dar_Program.setValue(Global.getPROGRAM());                                                                                                                    //Natural: ASSIGN +DAR-PROGRAM := *PROGRAM
        //*  CAUSES A TERMINATION IN THE EVENT OF
        if (condition(pls_Error_Occurence.getBoolean()))                                                                                                                  //Natural: IF +ERROR-OCCURENCE
        {
            //*  AN ERROR. NEEDED TO FORCE OPERATOR RESTART.
            DbsUtil.terminate(5);  if (true) return;                                                                                                                      //Natural: TERMINATE 5
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-RESTART
        sub_Check_For_Restart();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TITLE-LINES1
        sub_Write_Title_Lines1();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TITLE-LINES2
        sub_Write_Title_Lines2();
        if (condition(Global.isEscape())) {return;}
        Global.getERROR_TA().setValue(" ");                                                                                                                               //Natural: ASSIGN *ERROR-TA := ' '
        //*  BENE DFLT PRODUCT PLAN PROVISIONS
                                                                                                                                                                          //Natural: PERFORM LOAD-PLANS-BENE-DFLT
        sub_Load_Plans_Bene_Dflt();
        if (condition(Global.isEscape())) {return;}
        //*                              /* ENHANCEMENT/NWI
        //*  (TNGPROC)
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-IRASUB-DATE
        sub_Retrieve_Irasub_Date();
        if (condition(Global.isEscape())) {return;}
        //*  MAIN LOGIC
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  IISG
        //*  IISG
        //*  BDP
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 8 #OMNI-DATA-1 #OMNI-DATA-2 #OMNI-DATA-3 #OMNI-DATA-4 #OMNI-DATA-5 #OMNI-DATA-6 #OMNI-DATA-7 #OMNI-DATA-8 #OMNI-DATA-9 #OMNI-DATA-10 #OMNI-DATA-11 #OMNI-DATA-12 #OMNI-DATA-13 #OMNI-DATA-14 #OMNI-DATA-15 #OMNI-DATA-16 #OMNI-DATA-17 #OMNI-DATA-18 #OMNI-DATA-19 #OMNI-DATA-20 #OMNI-DATA-21 #OMNI-DATA-22 #OMNI-DATA-23 #OMNI-DATA-24 #OMNI-DATA-25 #OMNI-DATA-26 #OMNI-DATA-27 #OMNI-DATA-28 #OMNI-DATA-29 #OMNI-DATA-30 #OMNI-DATA-31 #OMNI-DATA-32 #OMNI-DATA-33 #OMNI-DATA-34 #OMNI-DATA-35 #OMNI-DATA-36 #OMNI-DATA-37 #OMNI-DATA-38 #OMNI-DATA-39 #OMNI-DATA-40
        while (condition(getWorkFiles().read(8, ldaScil9000.getScil9000_Pnd_Omni_Data_1(), ldaScil9000.getScil9000_Pnd_Omni_Data_2(), ldaScil9000.getScil9000_Pnd_Omni_Data_3(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_4(), ldaScil9000.getScil9000_Pnd_Omni_Data_5(), ldaScil9000.getScil9000_Pnd_Omni_Data_6(), ldaScil9000.getScil9000_Pnd_Omni_Data_7(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_8(), ldaScil9000.getScil9000_Pnd_Omni_Data_9(), ldaScil9000.getScil9000_Pnd_Omni_Data_10(), ldaScil9000.getScil9000_Pnd_Omni_Data_11(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_12(), ldaScil9000.getScil9000_Pnd_Omni_Data_13(), ldaScil9000.getScil9000_Pnd_Omni_Data_14(), ldaScil9000.getScil9000_Pnd_Omni_Data_15(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_16(), ldaScil9000.getScil9000_Pnd_Omni_Data_17(), ldaScil9000.getScil9000_Pnd_Omni_Data_18(), ldaScil9000.getScil9000_Pnd_Omni_Data_19(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_20(), ldaScil9000.getScil9000_Pnd_Omni_Data_21(), ldaScil9000.getScil9000_Pnd_Omni_Data_22(), ldaScil9000.getScil9000_Pnd_Omni_Data_23(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_24(), ldaScil9000.getScil9000_Pnd_Omni_Data_25(), ldaScil9000.getScil9000_Pnd_Omni_Data_26(), ldaScil9000.getScil9000_Pnd_Omni_Data_27(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_28(), ldaScil9000.getScil9000_Pnd_Omni_Data_29(), ldaScil9000.getScil9000_Pnd_Omni_Data_30(), ldaScil9000.getScil9000_Pnd_Omni_Data_31(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_32(), ldaScil9000.getScil9000_Pnd_Omni_Data_33(), ldaScil9000.getScil9000_Pnd_Omni_Data_34(), ldaScil9000.getScil9000_Pnd_Omni_Data_35(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_36(), ldaScil9000.getScil9000_Pnd_Omni_Data_37(), ldaScil9000.getScil9000_Pnd_Omni_Data_38(), ldaScil9000.getScil9000_Pnd_Omni_Data_39(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_40())))
        {
            pnd_Summary_Report_Rec_Pnd_Tot_Records_Reprocessed.nadd(1);                                                                                                   //Natural: ASSIGN #TOT-RECORDS-REPROCESSED := #TOT-RECORDS-REPROCESSED + 1
            //*  BDP
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  SCIL9000
            //*  IISG
            //*  IISG
            getWorkFiles().read(1, ldaScil9000.getScil9000_Pnd_Omni_Data_1(), ldaScil9000.getScil9000_Pnd_Omni_Data_2(), ldaScil9000.getScil9000_Pnd_Omni_Data_3(),       //Natural: READ WORK FILE 1 ONCE #OMNI-DATA-1 #OMNI-DATA-2 #OMNI-DATA-3 #OMNI-DATA-4 #OMNI-DATA-5 #OMNI-DATA-6 #OMNI-DATA-7 #OMNI-DATA-8 #OMNI-DATA-9 #OMNI-DATA-10 #OMNI-DATA-11 #OMNI-DATA-12 #OMNI-DATA-13 #OMNI-DATA-14 #OMNI-DATA-15 #OMNI-DATA-16 #OMNI-DATA-17 #OMNI-DATA-18 #OMNI-DATA-19 #OMNI-DATA-20 #OMNI-DATA-21 #OMNI-DATA-22 #OMNI-DATA-23 #OMNI-DATA-24 #OMNI-DATA-25 #OMNI-DATA-26 #OMNI-DATA-27 #OMNI-DATA-28 #OMNI-DATA-29 #OMNI-DATA-30 #OMNI-DATA-31 #OMNI-DATA-32 #OMNI-DATA-33 #OMNI-DATA-34 #OMNI-DATA-35 #OMNI-DATA-36 #OMNI-DATA-37 #OMNI-DATA-38 #OMNI-DATA-39 #OMNI-DATA-40
                ldaScil9000.getScil9000_Pnd_Omni_Data_4(), ldaScil9000.getScil9000_Pnd_Omni_Data_5(), ldaScil9000.getScil9000_Pnd_Omni_Data_6(), ldaScil9000.getScil9000_Pnd_Omni_Data_7(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_8(), ldaScil9000.getScil9000_Pnd_Omni_Data_9(), ldaScil9000.getScil9000_Pnd_Omni_Data_10(), ldaScil9000.getScil9000_Pnd_Omni_Data_11(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_12(), ldaScil9000.getScil9000_Pnd_Omni_Data_13(), ldaScil9000.getScil9000_Pnd_Omni_Data_14(), ldaScil9000.getScil9000_Pnd_Omni_Data_15(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_16(), ldaScil9000.getScil9000_Pnd_Omni_Data_17(), ldaScil9000.getScil9000_Pnd_Omni_Data_18(), ldaScil9000.getScil9000_Pnd_Omni_Data_19(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_20(), ldaScil9000.getScil9000_Pnd_Omni_Data_21(), ldaScil9000.getScil9000_Pnd_Omni_Data_22(), ldaScil9000.getScil9000_Pnd_Omni_Data_23(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_24(), ldaScil9000.getScil9000_Pnd_Omni_Data_25(), ldaScil9000.getScil9000_Pnd_Omni_Data_26(), ldaScil9000.getScil9000_Pnd_Omni_Data_27(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_28(), ldaScil9000.getScil9000_Pnd_Omni_Data_29(), ldaScil9000.getScil9000_Pnd_Omni_Data_30(), ldaScil9000.getScil9000_Pnd_Omni_Data_31(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_32(), ldaScil9000.getScil9000_Pnd_Omni_Data_33(), ldaScil9000.getScil9000_Pnd_Omni_Data_34(), ldaScil9000.getScil9000_Pnd_Omni_Data_35(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_36(), ldaScil9000.getScil9000_Pnd_Omni_Data_37(), ldaScil9000.getScil9000_Pnd_Omni_Data_38(), ldaScil9000.getScil9000_Pnd_Omni_Data_39(), 
                ldaScil9000.getScil9000_Pnd_Omni_Data_40());
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
                sub_Write_Summary_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-SUMMARY-REPORT
                sub_Write_Pi_Summary_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pnd_Restarted.getBoolean())))                                                                                                            //Natural: IF NOT #RESTARTED
                {
                                                                                                                                                                          //Natural: PERFORM DELETE-RESTART-RECORD
                    sub_Delete_Restart_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            if (condition(pnd_Restarted.getBoolean()))                                                                                                                    //Natural: IF #RESTARTED
            {
                if (condition(ldaScil9000.getScil9000_Pnd_Plan_No().equals(pnd_Restart_Key_Pnd_Restart_Plan_No) && ldaScil9000.getScil9000_Pnd_Sub_Plan_No().equals(pnd_Restart_Key_Pnd_Restart_Subplan_No)  //Natural: IF SCIL9000.#PLAN-NO = #RESTART-PLAN-NO AND SCIL9000.#SUB-PLAN-NO = #RESTART-SUBPLAN-NO AND SCIL9000.#SSN = #RESTART-SSN AND SCIL9000.#SG-PART-EXT = #RESTART-EXT
                    && ldaScil9000.getScil9000_Pnd_Ssn().equals(pnd_Restart_Key_Pnd_Restart_Ssn) && ldaScil9000.getScil9000_Pnd_Sg_Part_Ext().equals(pnd_Restart_Key_Pnd_Restart_Ext)))
                {
                    pnd_Skip_Restart_Record.setValue(true);                                                                                                               //Natural: ASSIGN #SKIP-RESTART-RECORD := TRUE
                    //*  REPORT AS AN ERROR RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                    sub_Write_Error_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Restarted.reset();                                                                                                                                //Natural: RESET #RESTARTED
                    pnd_Skip_Restart_Record.reset();                                                                                                                      //Natural: RESET #SKIP-RESTART-RECORD
                    //*  READ THE NEXT RECORD AND PROCESS
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  READ THE NEXT RECORD
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOT-TRANS-CNT
            //*  PFIX1
                                                                                                                                                                          //Natural: PERFORM UPDATE-BATCH-RESTART-RECORD
            sub_Update_Batch_Restart_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition((ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y") && ((ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA101")                //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y' AND ( SCIL9000.#PLAN-NO = 'IRA101' OR = 'IRA201' OR = 'IRA801' )
                || ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA201")) || ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA801")))))
            {
                ldaScil9000.getScil9000_Pnd_Date_App_Recvd().setValue(Global.getDATN());                                                                                  //Natural: ASSIGN SCIL9000.#DATE-APP-RECVD := SCIA1200.#DATE-APP-RECVD := *DATN
                pdaScia1200.getScia1200_Pnd_Date_App_Recvd().setValue(Global.getDATN());
                ldaScil9000.getScil9000_Pnd_Enrollment_Method().setValue("N");                                                                                            //Natural: ASSIGN SCIL9000.#ENROLLMENT-METHOD := 'N'
                pnd_Use_Acis_Annuity_Start_Date.resetInitial();                                                                                                           //Natural: RESET INITIAL #USE-ACIS-ANNUITY-START-DATE
                if (condition(! (DbsUtil.maskMatches(ldaScil9000.getScil9000_Pnd_Annuity_Start_Date(),"YYYYMMDD"))))                                                      //Natural: IF SCIL9000.#ANNUITY-START-DATE NE MASK ( YYYYMMDD )
                {
                    vw_reprint.startDatabaseFind                                                                                                                          //Natural: FIND ( 1 ) REPRINT WITH RP-TIAA-CONTR = SCIL9000.#ORIG-TIAA-CNTRCT
                    (
                    "FIND01",
                    new Wc[] { new Wc("RP_TIAA_CONTR", "=", ldaScil9000.getScil9000_Pnd_Orig_Tiaa_Cntrct(), WcType.WITH) },
                    1
                    );
                    FIND01:
                    while (condition(vw_reprint.readNextRow("FIND01")))
                    {
                        vw_reprint.setIfNotFoundControlFlag(false);
                        ldaScil9000.getScil9000_Pnd_Annuity_Start_Date().setValue(reprint_Rp_Annuity_Start_Date);                                                         //Natural: ASSIGN SCIL9000.#ANNUITY-START-DATE := RP-ANNUITY-START-DATE
                        pnd_Use_Acis_Annuity_Start_Date.setValue(true);                                                                                                   //Natural: ASSIGN #USE-ACIS-ANNUITY-START-DATE := TRUE
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  TNG CHANGES - ( ) AND OR
            if (condition((ldaScil9000.getScil9000_Pnd_Orchestration_Id().greater(" ") && ldaScil9000.getScil9000_Pnd_Use_Bpel_Data().equals("Y"))))                      //Natural: IF ( SCIL9000.#ORCHESTRATION-ID GT ' ' AND SCIL9000.#USE-BPEL-DATA = 'Y' )
            {
                getReports().write(0, "DOING BPEL");                                                                                                                      //Natural: WRITE 'DOING BPEL'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  NY200
                //*  NY200
                //* NY200
                pdaScia1295.getPnd_Scia1295_In().getValue("*").reset();                                                                                                   //Natural: RESET #SCIA1295-IN ( * )
                pdaScia1295.getPnd_Scia1295_In_Pnd_Ssn().setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                     //Natural: ASSIGN #SCIA1295-IN.#SSN := SCIL9000.#SSN
                pdaScia1295.getPnd_Scia1295_In_Pnd_Orchestration_Id().setValue(ldaScil9000.getScil9000_Pnd_Orchestration_Id());                                           //Natural: ASSIGN #SCIA1295-IN.#ORCHESTRATION-ID := SCIL9000.#ORCHESTRATION-ID
                //*  NY200 ONEIRA
                //*  NY200
                DbsUtil.callnat(Scin1282.class , getCurrentProcessState(), pdaScia1295.getPnd_Scia1295_In().getValue(1,":",10911), pdaScia1295.getPnd_Scia1295_Out());    //Natural: CALLNAT 'SCIN1282' #SCIA1295-IN ( 0001:10911 ) #SCIA1295-OUT
                if (condition(Global.isEscape())) return;
                getReports().write(0, "=",pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Code());                                                                           //Natural: WRITE '=' #RESPONSE-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Code().notEquals("0000")))                                                                     //Natural: IF #RESPONSE-CODE <> '0000'
                {
                    pdaScia1200.getScia1200_Pnd_Sg_Plan_No().setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                             //Natural: ASSIGN SCIA1200.#SG-PLAN-NO := SCIL9000.#PLAN-NO
                    pdaScia1200.getScia1200_Pnd_Sg_Subplan_No().setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No());                                                      //Natural: ASSIGN SCIA1200.#SG-SUBPLAN-NO := SCIL9000.#SUB-PLAN-NO
                    pdaScia1200.getScia1200_Pnd_Ssn().setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                        //Natural: ASSIGN SCIA1200.#SSN := SCIL9000.#SSN
                    pdaScia1200.getScia1200_Pnd_First_Name().setValue(ldaScil9000.getScil9000_Pnd_First_Name());                                                          //Natural: ASSIGN SCIA1200.#FIRST-NAME := SCIL9000.#FIRST-NAME
                    pdaScia1200.getScia1200_Pnd_Last_Name().setValue(ldaScil9000.getScil9000_Pnd_Last_Name());                                                            //Natural: ASSIGN SCIA1200.#LAST-NAME := SCIL9000.#LAST-NAME
                    pdaScia1200.getScia1200_Pnd_Return_Code().setValue(pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Code());                                              //Natural: ASSIGN SCIA1200.#RETURN-CODE := #RESPONSE-CODE
                    pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(pdaScia1295.getPnd_Scia1295_Out_Pnd_Response_Text());                                               //Natural: ASSIGN SCIA1200.#RETURN-MSG := #RESPONSE-TEXT
                    pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                    //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                    sub_Write_Error_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPROCESS-FILE
                    sub_Write_Reprocess_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                                   //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
                    sub_Write_Reject_To_Pi();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                                 //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
                    {
                        pnd_Pi_Reject_Flag.setValue("Y");                                                                                                                 //Natural: ASSIGN #PI-REJECT-FLAG := 'Y'
                        pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                              //Natural: ADD 1 TO #PI-REJECT-CNT
                        //*  PI TASK STATUS = 'F' (FAIL)
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                        sub_Write_Pi_Interface_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ERROR-REC
                        sub_Write_Pi_Error_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  ONEIRA2
                                                                                                                                                                          //Natural: PERFORM CHECK-BREAK-ON-PLAN-DIVSUB
                sub_Check_Break_On_Plan_Divsub();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  NY200
                                                                                                                                                                          //Natural: PERFORM MOVE-DATA-SCIA1295-TO-SCIL9000
                sub_Move_Data_Scia1295_To_Scil9000();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ONEIRA
                if (condition(ldaScil9000.getScil9000_Pnd_Oneira_Acct_No().notEquals(" ")))                                                                               //Natural: IF SCIL9000.#ONEIRA-ACCT-NO <> ' '
                {
                    //*  ONEIRA
                    pnd_Reject_Sw.reset();                                                                                                                                //Natural: RESET #REJECT-SW
                    //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM MOVE-FORMAT-DATA-TO-SCIA1200
                    sub_Move_Format_Data_To_Scia1200();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No().setValue(ldaScil9000.getScil9000_Pnd_Tiaa_Contract_No());                                              //Natural: ASSIGN SCIA1200.#TIAA-CONTRACT-NO := SCIL9000.#TIAA-CONTRACT-NO
                    pdaScia1200.getScia1200_Pnd_Cref_Certificate_No().setValue(ldaScil9000.getScil9000_Pnd_Cref_Certificate_No());                                        //Natural: ASSIGN SCIA1200.#CREF-CERTIFICATE-NO := SCIL9000.#CREF-CERTIFICATE-NO
                    //*  ONEIRA
                    pdaScia9000.getScia9000().setValuesByName(pdaScia1200.getScia1200());                                                                                 //Natural: MOVE BY NAME SCIA1200 TO SCIA9000
                    //*  ONEIRA
                    //*  ONEIRA
                    pdaScia9000.getScia9000_Pnd_Oneira_Acct_No().setValue(ldaScil9000.getScil9000_Pnd_Oneira_Acct_No());                                                  //Natural: MOVE SCIL9000.#ONEIRA-ACCT-NO TO SCIA9000.#ONEIRA-ACCT-NO
                    //*  ONEIRA
                    DbsUtil.callnat(Scin9000.class , getCurrentProcessState(), pdaScia9000.getScia9000());                                                                //Natural: CALLNAT 'SCIN9000' SCIA9000
                    if (condition(Global.isEscape())) return;
                    //*  ONEIRA
                    if (condition(pdaScia9000.getScia9000_Pnd_Return_Code().notEquals("0000")))                                                                           //Natural: IF SCIA9000.#RETURN-CODE <> '0000'
                    {
                        //*  ONEIRA
                        pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #TOT-REJECT-CNT
                        //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                        sub_Write_Error_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-REPROCESS-FILE
                        sub_Write_Reprocess_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ONEIRA
                        pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                               //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                        //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
                        sub_Write_Reject_To_Pi();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ONEIRA
                        //*  ONEIRA
                        if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                             //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
                        {
                            pnd_Pi_Reject_Flag.setValue("Y");                                                                                                             //Natural: ASSIGN #PI-REJECT-FLAG := 'Y'
                            //*  ONEIRA
                            pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                          //Natural: ADD 1 TO #PI-REJECT-CNT
                            //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                            sub_Write_Pi_Interface_Rec();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  ONEIRA
                            pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                           //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                            //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ERROR-REC
                            sub_Write_Pi_Error_Rec();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  ONEIRA
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ONEIRA
                        pdaScia1200.getScia1200().setValuesByName(pdaScia9000.getScia9000());                                                                             //Natural: MOVE BY NAME SCIA9000 TO SCIA1200
                        //*  ONEIRA
                        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #TOT-ACCEPT-CNT
                        //*  ONEIRA
                        //*  ONEIRA
                        if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                             //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
                        {
                            pnd_Pi_Reject_Flag.setValue("N");                                                                                                             //Natural: ASSIGN #PI-REJECT-FLAG := 'N'
                            //*  ONEIRA
                            pnd_Pi_Summary_Report_Rec_Pnd_Pi_Accept_Cnt.nadd(1);                                                                                          //Natural: ADD 1 TO #PI-ACCEPT-CNT
                            //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                            sub_Write_Pi_Interface_Rec();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-REPORT-REC
                            sub_Write_Pi_Report_Rec();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  ONEIRA
                        }                                                                                                                                                 //Natural: END-IF
                        //*  ONEIRA2
                        break_Counters_Pnd_Plan_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #PLAN-CNT
                        //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM FORMAT-N-WRITE-INTERFACE-RECORDS
                        sub_Format_N_Write_Interface_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-REC
                        sub_Write_Report_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  ONEIRA
                    }                                                                                                                                                     //Natural: END-IF
                    //*  READ THE NEXT RECORD AND PROCESS
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  ONEIRA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 3 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 4 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 5 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 6 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 7 )
            //*  ONEIRA
                                                                                                                                                                          //Natural: PERFORM CHECK-BREAK-ON-PLAN-DIVSUB
            sub_Check_Break_On_Plan_Divsub();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PERFORM UPDATE-BATCH-RESTART-RECORD  /* PFIX1
            //*  DBH2
            pnd_Reject_Sw.reset();                                                                                                                                        //Natural: RESET #REJECT-SW
            //*  NEW ENROLLMENT OR CONTRACT
                                                                                                                                                                          //Natural: PERFORM MOVE-FORMAT-DATA-TO-SCIA1200
            sub_Move_Format_Data_To_Scia1200();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                       /* NOT FOUND ON PRAP FOR PICKUP
            //*  DBH2 START NEW IF
            if (condition(pnd_Reject_Sw.getBoolean()))                                                                                                                    //Natural: IF #REJECT-SW
            {
                pdaScia1200.getScia1200_Pnd_Return_Code().setValue(9000);                                                                                                 //Natural: ASSIGN SCIA1200.#RETURN-CODE := 9000
                pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-REJECT-CNT
                //*  REPORT AS AN ERROR RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                sub_Write_Error_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PG1
                                                                                                                                                                          //Natural: PERFORM WRITE-REPROCESS-FILE
                sub_Write_Reprocess_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TNGSUB
                pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                                       //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                //*  PG1 ACISRJCT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
                sub_Write_Reject_To_Pi();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  DBH2 END NEW IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Icap_Product.getBoolean()))                                                                                                                 //Natural: IF #ICAP-PRODUCT
            {
                //*  BIP2
                if (condition(! (pdaScia2200.getScia2200_Pnd_Product_Approved().getBoolean()) && pdaScia1200.getScia1200_Pnd_Decedent_Contract().equals(" ")))            //Natural: IF NOT #PRODUCT-APPROVED AND SCIA1200.#DECEDENT-CONTRACT = ' '
                {
                    pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                    //Natural: ADD 1 TO #TOT-REJECT-CNT
                    pdaScia1200.getScia1200_Pnd_Return_Code().setValue(8888);                                                                                             //Natural: ASSIGN SCIA1200.#RETURN-CODE := 8888
                    //*  GGG-0711
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Ccyy.setValue(pdaScia2200.getScia2200_Pnd_Applctn_Rcvd_Date().getSubstring(1,4));                                 //Natural: MOVE SUBSTRING ( SCIA2200.#APPLCTN-RCVD-DATE,1,4 ) TO #WS-ISSUE-DATE-CCYY
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Mm.setValue(pdaScia2200.getScia2200_Pnd_Applctn_Rcvd_Date().getSubstring(5,2));                                   //Natural: MOVE SUBSTRING ( SCIA2200.#APPLCTN-RCVD-DATE,5,2 ) TO #WS-ISSUE-DATE-MM
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Dd.setValue(pdaScia2200.getScia2200_Pnd_Applctn_Rcvd_Date().getSubstring(7,2));                                   //Natural: MOVE SUBSTRING ( SCIA2200.#APPLCTN-RCVD-DATE,7,2 ) TO #WS-ISSUE-DATE-DD
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler1.setValue("/");                                                                                            //Natural: MOVE '/' TO #WS-ISSUE-DATE-FILLER1 #WS-ISSUE-DATE-FILLER2
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler2.setValue("/");
                    //*  GGG-0711
                    pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(DbsUtil.compress("PRODUCT NOT APPROVED IN", pdaScia2200.getScia2200_Pnd_S_State_Cde(),              //Natural: COMPRESS 'PRODUCT NOT APPROVED IN' #S-STATE-CDE 'FOR DATE -' #WS-ISSUE-DATE INTO SCIA1200.#RETURN-MSG
                        "FOR DATE -", pnd_Ws_Issue_Date));
                    //*  WRITE ACIS REJECT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                    sub_Write_Error_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  TNGSUB
                    pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                                   //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                    //*  BJD ACISRJCT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
                    sub_Write_Reject_To_Pi();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                                 //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
                    {
                        pnd_Pi_Reject_Flag.setValue("Y");                                                                                                                 //Natural: ASSIGN #PI-REJECT-FLAG := 'Y'
                        pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                              //Natural: ADD 1 TO #PI-REJECT-CNT
                        //*  PI TASK STATUS = 'F' (FAIL)
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                        sub_Write_Pi_Interface_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  TNGSUB
                        pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                               //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ERROR-REC
                        sub_Write_Pi_Error_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BNL2 RECON
                                                                                                                                                                          //Natural: PERFORM WRITE-REPROCESS-FILE
                    sub_Write_Reprocess_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  RS0 FOR IRA INDEXED RATE PRODUCTS
            if (condition(pnd_Indx_Product.getBoolean()))                                                                                                                 //Natural: IF #INDX-PRODUCT
            {
                //*  BIP2
                if (condition(! (pdaScia2200.getScia2200_Pnd_Product_Approved().getBoolean()) && pdaScia1200.getScia1200_Pnd_Decedent_Contract().equals(" ")))            //Natural: IF NOT #PRODUCT-APPROVED AND SCIA1200.#DECEDENT-CONTRACT = ' '
                {
                    pdaScia1200.getScia1200_Pnd_Return_Code().setValue(8888);                                                                                             //Natural: ASSIGN SCIA1200.#RETURN-CODE := 8888
                    //*  GGG-0711
                    //*     �
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Ccyy.setValue(pdaScia2200.getScia2200_Pnd_Applctn_Rcvd_Date().getSubstring(1,4));                                 //Natural: MOVE SUBSTRING ( SCIA2200.#APPLCTN-RCVD-DATE,1,4 ) TO #WS-ISSUE-DATE-CCYY
                    //*     �
                    //*     �
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Mm.setValue(pdaScia2200.getScia2200_Pnd_Applctn_Rcvd_Date().getSubstring(5,2));                                   //Natural: MOVE SUBSTRING ( SCIA2200.#APPLCTN-RCVD-DATE,5,2 ) TO #WS-ISSUE-DATE-MM
                    //*     �
                    //*     �
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Dd.setValue(pdaScia2200.getScia2200_Pnd_Applctn_Rcvd_Date().getSubstring(7,2));                                   //Natural: MOVE SUBSTRING ( SCIA2200.#APPLCTN-RCVD-DATE,7,2 ) TO #WS-ISSUE-DATE-DD
                    //*     �
                    //*     �
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler1.setValue("/");                                                                                            //Natural: MOVE '/' TO #WS-ISSUE-DATE-FILLER1 #WS-ISSUE-DATE-FILLER2
                    pnd_Ws_Issue_Date_Pnd_Ws_Issue_Date_Filler2.setValue("/");
                    //*     �
                    //*     �
                    //*  GGG-0711
                    pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(DbsUtil.compress("PRODUCT NOT APPROVED IN", pdaScia2200.getScia2200_Pnd_S_State_Cde(),              //Natural: COMPRESS 'PRODUCT NOT APPROVED IN' #S-STATE-CDE 'FOR DATE -' #WS-ISSUE-DATE INTO SCIA1200.#RETURN-MSG
                        "FOR DATE -", pnd_Ws_Issue_Date));
                    //*  WRITE ACIS REJECT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                    sub_Write_Error_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  TNGSUB
                    pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                                   //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                    //*  BJD ACISRJCT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
                    sub_Write_Reject_To_Pi();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                                 //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
                    {
                        pnd_Pi_Reject_Flag.setValue("Y");                                                                                                                 //Natural: ASSIGN #PI-REJECT-FLAG := 'Y'
                        pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                              //Natural: ADD 1 TO #PI-REJECT-CNT
                        //*  PI TASK STATUS = 'F' (FAIL)
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                        sub_Write_Pi_Interface_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ERROR-REC
                        sub_Write_Pi_Error_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS0
                                                                                                                                                                          //Natural: PERFORM WRITE-REPROCESS-FILE
                    sub_Write_Reprocess_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                    //Natural: ADD 1 TO #TOT-REJECT-CNT
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  PROCESSING.
                                                                                                                                                                          //Natural: PERFORM CREATE-NEW-CONTRACT
            sub_Create_New_Contract();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(ldaScil9000.getScil9000_Pnd_Div_Sub().greater(" ") && break_Counters_Pnd_Divsub_Cnt.greater(getZero())))                                            //Natural: IF SCIL9000.#DIV-SUB GT ' ' AND #DIVSUB-CNT GT 0
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-DIVISION-TOTALS
            sub_Write_Division_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil9000.getScil9000_Pnd_Plan_No().greater(" ") && break_Counters_Pnd_Plan_Cnt.greater(getZero())))                                              //Natural: IF SCIL9000.#PLAN-NO GT ' ' AND #PLAN-CNT GT 0
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-PLAN-TOTALS
            sub_Write_Plan_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"     ");                                                                                            //Natural: WRITE ( 01 ) NOTITLE 1T '     '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"TOTAL PARTICIPANTS ACCEPTED ==> ",pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt);                       //Natural: WRITE ( 01 ) NOTITLE 1T 'TOTAL PARTICIPANTS ACCEPTED ==> ' #TOT-ACCEPT-CNT
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"     ");                                                                                            //Natural: WRITE ( 02 ) NOTITLE 1T '     '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"TOTAL PARTICIPANTS REJECTED ==> ",pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt);                       //Natural: WRITE ( 02 ) NOTITLE 1T 'TOTAL PARTICIPANTS REJECTED ==> ' #TOT-REJECT-CNT
        if (Global.isEscape()) return;
        getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),"     ");                                                                                            //Natural: WRITE ( 07 ) NOTITLE 1T '     '
        if (Global.isEscape()) return;
        getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),"TOTAL PARTICIPANTS REJECTED BY ACIS ==> ",pnd_Pi_Cnt);                                              //Natural: WRITE ( 07 ) NOTITLE 1T 'TOTAL PARTICIPANTS REJECTED BY ACIS ==> ' #PI-CNT
        if (Global.isEscape()) return;
        //*  NBC
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "Leaving SCIB9000 :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                  //Natural: WRITE 'Leaving SCIB9000 :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        //*  BADOTBLS
        if (condition(bene_File_Bene_Total_Contract_Written.equals(getZero())))                                                                                           //Natural: IF BENE-TOTAL-CONTRACT-WRITTEN = 0
        {
            getWorkFiles().write(10, false, "                                        ");                                                                                  //Natural: WRITE WORK FILE 10 '                                        '
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(bene_File_Bene_Total_Mos_Written.equals(getZero())))                                                                                                //Natural: IF BENE-TOTAL-MOS-WRITTEN = 0
        {
            getWorkFiles().write(11, false, "                                        ");                                                                                  //Natural: WRITE WORK FILE 11 '                                        '
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(bene_File_Bene_Total_Dest_Written.equals(getZero())))                                                                                               //Natural: IF BENE-TOTAL-DEST-WRITTEN = 0
        {
            getWorkFiles().write(12, false, "                                        ");                                                                                  //Natural: WRITE WORK FILE 12 '                                        '
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO INPUT RECORDS TO PROCESS
        if (condition(pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.equals(getZero())))                                                                                        //Natural: IF #TOT-TRANS-CNT = 0
        {
            getReports().display(0, "********  OMNI INTERFACE FILE IS EMPTY ********");                                                                                   //Natural: DISPLAY '********  OMNI INTERFACE FILE IS EMPTY ********'
            if (Global.isEscape()) return;
            DbsUtil.terminate(1);  if (true) return;                                                                                                                      //Natural: TERMINATE 1
        }                                                                                                                                                                 //Natural: END-IF
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-FORMAT-DATA-TO-SCIA1200
        //*  SCIA1200.#ISSUE-STATE         := SCIL9000.#ISSUE-STATE
        //* * NEGATIVE ENROLLMENT
        //* *----------------------------------------------------------------------
        //*  PLAN TYPE INFORMATION
        //* *----------------------------------------------------------------------
        //* *****************************************************
        //* * DELETE NON-NUMERIC CHARACTERS FROM PHONE NUMBERS **
        //* * BEFORE PASSING PHONE NUMBERS TO ACIS             **
        //* *****************************************************
        //*  TNGSUB - B.E.
        //*  THE AUTO ENROLL WAS MOVED HERE FROM AFTER BENE STATEMENTS    TNGSUB
        //* *----------------------------------------------------------------------
        //*  AUTO ENROLL                                       09/02/08      RM
        //* *----------------------------------------------------------------------
        //*  PASS AE DATA TO PRAP
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-BENE-DATA-IN-SCIA1200
        //*    IF SCIL9000.#BENE-NAME (1) = ' '
        //*    DEFAULT BENE INFO BEFORE CALLING SCIN9911
        //*    END-IF
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-BENE-RELATION-CODE
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-DEFAULTS
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-NEW-CONTRACT
        //* *======================================================================
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPROCESS-FILE
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-CONTRACT-EXISTS
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-N-WRITE-INTERFACE-RECORDS
        //* *======================================================================
        //*  #T813-TRADE-DATE        := SCIA1200.#ENTRY-DATE
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PI-INTERFACE-REC
        //* *****  START OF CHANGE BJD ACISRJCT     *******
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REJECT-TO-PI
        //* *****   END OF CHANGE BJD ACISRJCT      *******
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TITLE-LINES1
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TITLE-LINES2
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-COLUMN-HEADER1
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-COLUMN-HEADER2
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DIVISION-TOTALS
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PLAN-TOTALS
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-REC
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REC
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PI-REPORT-REC
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PI-ERROR-REC
        //* ***************** BEGIN CHANGE 08/31/2007 AB **************************
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PI-ACIS-REJECTS-REPORT
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PI-SUMMARY-REPORT
        //* ***************** BEGIN CHANGE 08/31/2007 AB **************************
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-RESTART
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-BATCH-RESTART-RECORD
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-RESTART-RECORD
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-APPROVAL-CHECK
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-VESTING-OVERRIDE
        //* *======================================================================
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PLANS-BENE-DFLT
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-IRASUB-DATE
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DATA-SCIA1295-TO-SCIL9000
        //* *======================================================================
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IRA-SUB-BENEFICIARY-COPY
        //*  ====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-BREAK-ON-PLAN-DIVSUB
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //* *======================================================================
        //*  RETRY ERROR LOGIC
        //* *======================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Move_Format_Data_To_Scia1200() throws Exception                                                                                                      //Natural: MOVE-FORMAT-DATA-TO-SCIA1200
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pdaScia1200.getScia1200().reset();                                                                                                                                //Natural: RESET SCIA1200 #T813-FLAGS
        pnd_T813_Record_Pnd_T813_Flags.reset();
        pnd_Icap_Product.setValue(false);                                                                                                                                 //Natural: ASSIGN #ICAP-PRODUCT := FALSE
        pnd_Indx_Product.setValue(false);                                                                                                                                 //Natural: ASSIGN #INDX-PRODUCT := FALSE
        //*  L. SHU 11/17/12 FOR LPAO STARTS HERE ...
        //*  GRA
        //*  RC
        //*  RCP
        if (condition(ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("GS1") || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("GR1")  //Natural: IF SUBSTRING ( SCIL9000.#SUB-PLAN-NO,1,3 ) = 'GS1' OR = 'GR1' OR = 'RS1' OR = 'RS2' OR = 'RS3' OR = 'RP1' OR = 'RP2' OR = 'RP3'
            || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("RS1") || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("RS2") 
            || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("RS3") || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("RP1") 
            || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("RP2") || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("RP3")))
        {
            if (condition(ldaScil9000.getScil9000_Pnd_Legal_Ann_Option().equals("Y")))                                                                                    //Natural: IF SCIL9000.#LEGAL-ANN-OPTION = 'Y'
            {
                pdaScia1200.getScia1200_Pnd_Legal_Ann_Option().setValue(ldaScil9000.getScil9000_Pnd_Legal_Ann_Option());                                                  //Natural: ASSIGN SCIA1200.#LEGAL-ANN-OPTION := SCIL9000.#LEGAL-ANN-OPTION
                ldaScil9000.getScil9000_Pnd_Money_Invested_Ind().setValue("N");                                                                                           //Natural: ASSIGN SCIL9000.#MONEY-INVESTED-IND := 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Legal_Ann_Option().setValue(" ");                                                                                                 //Natural: ASSIGN SCIA1200.#LEGAL-ANN-OPTION := ' '
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Orchestration_Id().setValue(ldaScil9000.getScil9000_Pnd_Orchestration_Id());                                                          //Natural: ASSIGN SCIA1200.#ORCHESTRATION-ID := SCIL9000.#ORCHESTRATION-ID
        pdaScia1200.getScia1200_Pnd_System_Requestor().setValue("ACIBATSG");                                                                                              //Natural: ASSIGN SCIA1200.#SYSTEM-REQUESTOR := 'ACIBATSG'
        pdaScia1200.getScia1200_Pnd_Racf_Id().setValue("BATCHACI");                                                                                                       //Natural: ASSIGN SCIA1200.#RACF-ID := 'BATCHACI'
        pdaScia1200.getScia1200_Pnd_More_Data().setValue("N");                                                                                                            //Natural: ASSIGN SCIA1200.#MORE-DATA := 'N'
        pdaScia1200.getScia1200_Pnd_Retry().setValue("N");                                                                                                                //Natural: ASSIGN SCIA1200.#RETRY := 'N'
        pdaScia1200.getScia1200_Pnd_Processing_Location().setValue(" ");                                                                                                  //Natural: ASSIGN SCIA1200.#PROCESSING-LOCATION := ' '
        pdaScia1200.getScia1200_Pnd_Print_Location().setValue(" ");                                                                                                       //Natural: ASSIGN SCIA1200.#PRINT-LOCATION := ' '
        pdaScia1200.getScia1200_Pnd_Processing_Type().setValue("AP");                                                                                                     //Natural: ASSIGN SCIA1200.#PROCESSING-TYPE := 'AP'
        pdaScia1200.getScia1200_Pnd_Bypass_Rules().setValue("Y");                                                                                                         //Natural: ASSIGN SCIA1200.#BYPASS-RULES := 'Y'
        pdaScia1200.getScia1200_Pnd_Enrollment_Type().setValue(ldaScil9000.getScil9000_Pnd_Enrollment_Method());                                                          //Natural: ASSIGN SCIA1200.#ENROLLMENT-TYPE := SCIL9000.#ENROLLMENT-METHOD
        pdaScia1200.getScia1200_Pnd_Loan_Ind().setValue(" ");                                                                                                             //Natural: ASSIGN SCIA1200.#LOAN-IND := ' '
        pdaScia1200.getScia1200_Pnd_Mit_Create_Folder().setValue("N");                                                                                                    //Natural: ASSIGN SCIA1200.#MIT-CREATE-FOLDER := 'N'
        pdaScia1200.getScia1200_Pnd_Entry_Date().setValue(ldaScil9000.getScil9000_Pnd_Entry_Date());                                                                      //Natural: ASSIGN SCIA1200.#ENTRY-DATE := SCIL9000.#ENTRY-DATE
        pdaScia1200.getScia1200_Pnd_Annuity_Start_Date().setValue(ldaScil9000.getScil9000_Pnd_Annuity_Start_Date());                                                      //Natural: ASSIGN SCIA1200.#ANNUITY-START-DATE := SCIL9000.#ANNUITY-START-DATE
        pdaScia1200.getScia1200_Pnd_Tiaa_Date_Of_Issue().setValue(ldaScil9000.getScil9000_Pnd_Tiaa_Date_Of_Issue());                                                      //Natural: ASSIGN SCIA1200.#TIAA-DATE-OF-ISSUE := SCIL9000.#TIAA-DATE-OF-ISSUE
        pdaScia1200.getScia1200_Pnd_Cref_Date_Of_Issue().setValue(ldaScil9000.getScil9000_Pnd_Cref_Date_Of_Issue());                                                      //Natural: ASSIGN SCIA1200.#CREF-DATE-OF-ISSUE := SCIL9000.#CREF-DATE-OF-ISSUE
        pdaScia1200.getScia1200_Pnd_Sg_Plan_Id().setValue(ldaScil9000.getScil9000_Pnd_Plan_Id());                                                                         //Natural: ASSIGN SCIA1200.#SG-PLAN-ID := SCIL9000.#PLAN-ID
        pdaScia1200.getScia1200_Pnd_Sg_Plan_No().setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                                         //Natural: ASSIGN SCIA1200.#SG-PLAN-NO := SCIL9000.#PLAN-NO
        pdaScia1200.getScia1200_Pnd_Sg_Subplan_No().setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No());                                                                  //Natural: ASSIGN SCIA1200.#SG-SUBPLAN-NO := SCIL9000.#SUB-PLAN-NO
        pdaScia1200.getScia1200_Pnd_Sg_Part_Ext().setValue(ldaScil9000.getScil9000_Pnd_Sg_Part_Ext());                                                                    //Natural: ASSIGN SCIA1200.#SG-PART-EXT := SCIL9000.#SG-PART-EXT
        pdaScia1200.getScia1200_Pnd_Erisa_Ind().setValue(ldaScil9000.getScil9000_Pnd_Erisa_Ind());                                                                        //Natural: ASSIGN SCIA1200.#ERISA-IND := SCIL9000.#ERISA-IND
        setValueToSubstring(ldaScil9000.getScil9000_Pnd_Tsv_Ind(),pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_1(),3,1);                                                       //Natural: MOVE SCIL9000.#TSV-IND TO SUBSTR ( SCIA1200.#SG-TEXT-UDF-1,3,1 )
        //*  IMBS
        setValueToSubstring(ldaScil9000.getScil9000_Pnd_Tsr_Ind(),pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_1(),8,3);                                                       //Natural: MOVE SCIL9000.#TSR-IND TO SUBSTR ( SCIA1200.#SG-TEXT-UDF-1,8,3 )
        if (condition(ldaScil9000.getScil9000_Pnd_Tsv_Ind().equals("1")))                                                                                                 //Natural: IF SCIL9000.#TSV-IND = '1'
        {
            ldaScil9000.getScil9000_Pnd_Oia_Ind().setValue("10");                                                                                                         //Natural: ASSIGN SCIL9000.#OIA-IND := '10'
            //*  CRS 12/8/05
            //*  CRS 6/19/06
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Oia_Ind().setValue(ldaScil9000.getScil9000_Pnd_Oia_Ind());                                                                            //Natural: ASSIGN SCIA1200.#OIA-IND := SCIL9000.#OIA-IND
        pdaScia1200.getScia1200_Pnd_Sg_Divsub().setValue(ldaScil9000.getScil9000_Pnd_Div_Sub());                                                                          //Natural: ASSIGN SCIA1200.#SG-DIVSUB := SCIL9000.#DIV-SUB
        pdaScia1200.getScia1200_Pnd_Sg_Register_Id().setValue(ldaScil9000.getScil9000_Pnd_Sg_Register_Id());                                                              //Natural: ASSIGN SCIA1200.#SG-REGISTER-ID := SCIL9000.#SG-REGISTER-ID
        pdaScia1200.getScia1200_Pnd_Sg_Agent_Racf_Id().setValue(ldaScil9000.getScil9000_Pnd_Sg_Agent_Racf_Id());                                                          //Natural: ASSIGN SCIA1200.#SG-AGENT-RACF-ID := SCIL9000.#SG-AGENT-RACF-ID
        //*  RS1
        setValueToSubstring(ldaScil9000.getScil9000_Pnd_Eir_Status(),pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_1(),5,2);                                                    //Natural: MOVE SCIL9000.#EIR-STATUS TO SUBSTR ( SCIA1200.#SG-TEXT-UDF-1,5,2 )
        pdaScia1200.getScia1200_Pnd_Sg_Insurer_1().setValue(ldaScil9000.getScil9000_Pnd_Sg_Insurer_1());                                                                  //Natural: ASSIGN SCIA1200.#SG-INSURER-1 := SCIL9000.#SG-INSURER-1
        pdaScia1200.getScia1200_Pnd_Sg_Insurer_2().setValue(ldaScil9000.getScil9000_Pnd_Sg_Insurer_2());                                                                  //Natural: ASSIGN SCIA1200.#SG-INSURER-2 := SCIL9000.#SG-INSURER-2
        pdaScia1200.getScia1200_Pnd_Sg_Insurer_3().setValue(ldaScil9000.getScil9000_Pnd_Sg_Insurer_3());                                                                  //Natural: ASSIGN SCIA1200.#SG-INSURER-3 := SCIL9000.#SG-INSURER-3
        pdaScia1200.getScia1200_Pnd_Sg_1035_Exch_Ind_1().setValue(ldaScil9000.getScil9000_Pnd_Sg_1035_Exch_Ind_1());                                                      //Natural: ASSIGN SCIA1200.#SG-1035-EXCH-IND-1 := SCIL9000.#SG-1035-EXCH-IND-1
        pdaScia1200.getScia1200_Pnd_Sg_1035_Exch_Ind_2().setValue(ldaScil9000.getScil9000_Pnd_Sg_1035_Exch_Ind_2());                                                      //Natural: ASSIGN SCIA1200.#SG-1035-EXCH-IND-2 := SCIL9000.#SG-1035-EXCH-IND-2
        pdaScia1200.getScia1200_Pnd_Sg_1035_Exch_Ind_3().setValue(ldaScil9000.getScil9000_Pnd_Sg_1035_Exch_Ind_3());                                                      //Natural: ASSIGN SCIA1200.#SG-1035-EXCH-IND-3 := SCIL9000.#SG-1035-EXCH-IND-3
        setValueToSubstring(ldaScil9000.getScil9000_Pnd_Issue_State(),pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_1(),1,2);                                                   //Natural: MOVE SCIL9000.#ISSUE-STATE TO SUBSTR ( SCIA1200.#SG-TEXT-UDF-1,1,2 )
        //*  AB 3-17-08
        pnd_Wk_Text_Udf_2_Pnd_Wk_Pl_Client_Id.setValue(ldaScil9000.getScil9000_Pnd_Pl_Client_Id());                                                                       //Natural: MOVE SCIL9000.#PL-CLIENT-ID TO #WK-PL-CLIENT-ID
        //*  AB 3-17-08
        pnd_Wk_Text_Udf_2_Pnd_Wk_Model_Id.setValue(ldaScil9000.getScil9000_Pnd_Model_Id());                                                                               //Natural: MOVE SCIL9000.#MODEL-ID TO #WK-MODEL-ID
        //*  AB 3-17-08
        pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_2().setValue(pnd_Wk_Text_Udf_2);                                                                                          //Natural: MOVE #WK-TEXT-UDF-2 TO SCIA1200.#SG-TEXT-UDF-2
        //*  CRS 6/19/06 \/
        if (condition(! (ldaScil9000.getScil9000_Pnd_Sg_Replacement_Ind().equals("Y") || ldaScil9000.getScil9000_Pnd_Sg_Replacement_Ind().equals("N"))))                  //Natural: IF NOT SCIL9000.#SG-REPLACEMENT-IND = 'Y' OR = 'N'
        {
            pdaScia1200.getScia1200_Pnd_Sg_Replacement_Ind().setValue("N");                                                                                               //Natural: ASSIGN SCIA1200.#SG-REPLACEMENT-IND := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Sg_Replacement_Ind().setValue(ldaScil9000.getScil9000_Pnd_Sg_Replacement_Ind());                                                  //Natural: ASSIGN SCIA1200.#SG-REPLACEMENT-IND := SCIL9000.#SG-REPLACEMENT-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (ldaScil9000.getScil9000_Pnd_Sg_Exempt_Ind().equals("Y") || ldaScil9000.getScil9000_Pnd_Sg_Exempt_Ind().equals("N"))))                            //Natural: IF NOT SCIL9000.#SG-EXEMPT-IND = 'Y' OR = 'N'
        {
            pdaScia1200.getScia1200_Pnd_Sg_Exempt_Ind().setValue("N");                                                                                                    //Natural: ASSIGN SCIA1200.#SG-EXEMPT-IND := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Sg_Exempt_Ind().setValue(ldaScil9000.getScil9000_Pnd_Sg_Exempt_Ind());                                                            //Natural: ASSIGN SCIA1200.#SG-EXEMPT-IND := SCIL9000.#SG-EXEMPT-IND
            //*  CRS 6/19/06 /\
        }                                                                                                                                                                 //Natural: END-IF
        //*  CS 3/29/05
        if (condition(! (ldaScil9000.getScil9000_Pnd_Companion_Ind().equals("Y") || ldaScil9000.getScil9000_Pnd_Companion_Ind().equals("N"))))                            //Natural: IF NOT SCIL9000.#COMPANION-IND = 'Y' OR = 'N'
        {
            pdaScia1200.getScia1200_Pnd_Companion_Ind().setValue("N");                                                                                                    //Natural: ASSIGN SCIA1200.#COMPANION-IND := 'N'
            //*  CS 3/29/05
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Companion_Ind().setValue(ldaScil9000.getScil9000_Pnd_Companion_Ind());                                                            //Natural: ASSIGN SCIA1200.#COMPANION-IND := SCIL9000.#COMPANION-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET CONTRACT NOS. FOR COMPANION PROCESSING
        if (condition(ldaScil9000.getScil9000_Pnd_Companion_Ind().equals("Y")))                                                                                           //Natural: IF SCIL9000.#COMPANION-IND = 'Y'
        {
            pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No().setValue(ldaScil9000.getScil9000_Pnd_Tiaa_Contract_No());                                                      //Natural: ASSIGN SCIA1200.#TIAA-CONTRACT-NO := SCIL9000.#TIAA-CONTRACT-NO
            pdaScia1200.getScia1200_Pnd_Cref_Certificate_No().setValue(ldaScil9000.getScil9000_Pnd_Cref_Certificate_No());                                                //Natural: ASSIGN SCIA1200.#CREF-CERTIFICATE-NO := SCIL9000.#CREF-CERTIFICATE-NO
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************************************************************
        //* * NEGATIVE ENROLLMENT         /* R5 (CRS) 3/5/05 START   \/
        //* *********************************************************************
        //*  EASEENR
        if (condition(ldaScil9000.getScil9000_Pnd_Enrollment_Method().equals("N") || ldaScil9000.getScil9000_Pnd_Enrollment_Method().equals("M")))                        //Natural: IF SCIL9000.#ENROLLMENT-METHOD = 'N' OR = 'M'
        {
            pdaScia1200.getScia1200_Pnd_System_Requestor().setValue("ACIBATNE");                                                                                          //Natural: ASSIGN SCIA1200.#SYSTEM-REQUESTOR := 'ACIBATNE'
            pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Tiaa_Contract.setValue(ldaScil9000.getScil9000_Pnd_Orig_Tiaa_Cntrct());                                                //Natural: ASSIGN #SCIA1200-ORIG-TIAA-CONTRACT := SCIL9000.#ORIG-TIAA-CNTRCT
            pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Cref_Contract.setValue(ldaScil9000.getScil9000_Pnd_Orig_Cref_Cntrct());                                                //Natural: ASSIGN #SCIA1200-ORIG-CREF-CONTRACT := SCIL9000.#ORIG-CREF-CNTRCT
            pdaScia1200.getScia1200_Pnd_Field3().setValue(pnd_Scia1200_Field_3);                                                                                          //Natural: ASSIGN SCIA1200.#FIELD3 = #SCIA1200-FIELD-3
            //*  POWERIMAGE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Omni_Account_Issuance().setValue(ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance());                                                //Natural: ASSIGN SCIA1200.#OMNI-ACCOUNT-ISSUANCE := SCIL9000.#OMNI-ACCOUNT-ISSUANCE
        pdaScia1200.getScia1200_Pnd_Mit_Log_Date_Time_Parent().setValue(ldaScil9000.getScil9000_Pnd_Pi_Task_Id());                                                        //Natural: ASSIGN SCIA1200.#MIT-LOG-DATE-TIME-PARENT := SCIL9000.#PI-TASK-ID
        //*                                                           /* TASK ID
        //*  CRS 1/7/06 BEGIN \/
        //*  TEAM CODE FOR IRA PLANS
        if (condition(ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,2).equals("IR")))                                                                          //Natural: IF SUBSTRING ( SCIL9000.#SUB-PLAN-NO,1,2 ) = 'IR'
        {
            pdaScia1200.getScia1200_Pnd_Mit_Unit().setValue("IRATRS");                                                                                                    //Natural: ASSIGN SCIA1200.#MIT-UNIT := 'IRATRS'
            //*  TEAM CODE FOR ALL OTHER PLANS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Mit_Unit().setValue("TMOMN");                                                                                                     //Natural: ASSIGN SCIA1200.#MIT-UNIT := 'TMOMN'
            //*  CRS 1/7/06 END   /\
        }                                                                                                                                                                 //Natural: END-IF
        //*  R4 (CRS) INCOMPLETE BIO DATA 12/08/04
                                                                                                                                                                          //Natural: PERFORM CHECK-DEFAULTS
        sub_Check_Defaults();
        if (condition(Global.isEscape())) {return;}
        //*  CRS 1/13/05 \/
        if (condition(ldaScil9000.getScil9000_Pnd_Date_App_Recvd().equals("00000000") || ldaScil9000.getScil9000_Pnd_Date_App_Recvd().equals("        ")))                //Natural: IF SCIL9000.#DATE-APP-RECVD = '00000000' OR SCIL9000.#DATE-APP-RECVD = '        '
        {
            ldaScil9000.getScil9000_Pnd_Date_App_Recvd().setValue(Global.getDATN());                                                                                      //Natural: ASSIGN SCIL9000.#DATE-APP-RECVD := *DATN
            //*  CRS 1/13/05 /\
        }                                                                                                                                                                 //Natural: END-IF
        //*  CRS 10/11/05 BEGIN \/
        if (condition(ldaScil9000.getScil9000_Pnd_Companion_Ind().equals("Y")))                                                                                           //Natural: IF SCIL9000.#COMPANION-IND = 'Y'
        {
            pdaScia1200.getScia1200_Pnd_Date_App_Recvd().setValue(ldaScil9000.getScil9000_Pnd_Entry_Date());                                                              //Natural: ASSIGN SCIA1200.#DATE-APP-RECVD := SCIL9000.#ENTRY-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Date_App_Recvd().setValue(ldaScil9000.getScil9000_Pnd_Date_App_Recvd());                                                          //Natural: ASSIGN SCIA1200.#DATE-APP-RECVD := SCIL9000.#DATE-APP-RECVD
            //*  CRS 10/11/05 END   /\
        }                                                                                                                                                                 //Natural: END-IF
        //*  CRS 11/20/06 BEGIN \/
        if (condition(ldaScil9000.getScil9000_Pnd_Divorce_Ind().equals(" ")))                                                                                             //Natural: IF SCIL9000.#DIVORCE-IND = ' '
        {
            ldaScil9000.getScil9000_Pnd_Divorce_Ind().setValue("N");                                                                                                      //Natural: ASSIGN SCIL9000.#DIVORCE-IND := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil9000.getScil9000_Pnd_Additional_Cref_Rqst().equals(" ")))                                                                                    //Natural: IF SCIL9000.#ADDITIONAL-CREF-RQST = ' '
        {
            ldaScil9000.getScil9000_Pnd_Additional_Cref_Rqst().setValue("N");                                                                                             //Natural: ASSIGN SCIL9000.#ADDITIONAL-CREF-RQST := 'N'
            //*  CRS 11/20/06 END  /\
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Divorce_Ind().setValue(ldaScil9000.getScil9000_Pnd_Divorce_Ind());                                                                    //Natural: ASSIGN SCIA1200.#DIVORCE-IND := SCIL9000.#DIVORCE-IND
        pdaScia1200.getScia1200_Pnd_Additional_Cref_Rqst().setValue(ldaScil9000.getScil9000_Pnd_Additional_Cref_Rqst());                                                  //Natural: ASSIGN SCIA1200.#ADDITIONAL-CREF-RQST := SCIL9000.#ADDITIONAL-CREF-RQST
        pdaScia1200.getScia1200_Pnd_Ica_Ind().setValue(ldaScil9000.getScil9000_Pnd_Ica_Rcvd_Ind());                                                                       //Natural: ASSIGN SCIA1200.#ICA-IND := SCIL9000.#ICA-RCVD-IND
        if (condition(ldaScil9000.getScil9000_Pnd_Pull_Code().equals("Y")))                                                                                               //Natural: IF SCIL9000.#PULL-CODE = 'Y'
        {
            pdaScia1200.getScia1200_Pnd_Pull_Code().setValue("A");                                                                                                        //Natural: ASSIGN SCIA1200.#PULL-CODE = 'A'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Pull_Code().setValue(" ");                                                                                                        //Natural: ASSIGN SCIA1200.#PULL-CODE = ' '
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Vesting_Code_Override().setValue(ldaScil9000.getScil9000_Pnd_Vesting_Code_Override());                                                //Natural: ASSIGN SCIA1200.#VESTING-CODE-OVERRIDE = SCIL9000.#VESTING-CODE-OVERRIDE
        short decideConditionsMet3414 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE SCIL9000.#VESTING-CODE;//Natural: VALUE '0'
        if (condition((ldaScil9000.getScil9000_Pnd_Vesting_Code().equals("0"))))
        {
            decideConditionsMet3414++;
            //*  PARTICIPANT OWNED
            pdaScia1200.getScia1200_Pnd_Vesting_Code().setValue("1");                                                                                                     //Natural: ASSIGN SCIA1200.#VESTING-CODE = '1'
        }                                                                                                                                                                 //Natural: VALUE '1'
        else if (condition((ldaScil9000.getScil9000_Pnd_Vesting_Code().equals("1"))))
        {
            decideConditionsMet3414++;
            //*  INSTITUTION OWNED
            pdaScia1200.getScia1200_Pnd_Vesting_Code().setValue("3");                                                                                                     //Natural: ASSIGN SCIA1200.#VESTING-CODE = '3'
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaScil9000.getScil9000_Pnd_Vesting_Code().equals("2"))))
        {
            decideConditionsMet3414++;
            //*  DELAYED VESTING
            pdaScia1200.getScia1200_Pnd_Vesting_Code().setValue("2");                                                                                                     //Natural: ASSIGN SCIA1200.#VESTING-CODE = '2'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("RL1")))                                                                         //Natural: IF SUBSTRING ( SCIL9000.#SUB-PLAN-NO,1,3 ) = 'RL1'
        {
            pdaScia1200.getScia1200_Pnd_Vesting_Code().setValue("1");                                                                                                     //Natural: ASSIGN SCIA1200.#VESTING-CODE = '1'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3).equals("SR1")))                                                                         //Natural: IF SUBSTRING ( SCIL9000.#SUB-PLAN-NO,1,3 ) = 'SR1'
        {
            pdaScia1200.getScia1200_Pnd_Vesting_Code().setValue("1");                                                                                                     //Natural: ASSIGN SCIA1200.#VESTING-CODE = '1'
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Irc_Section_Code().setValue(ldaScil9000.getScil9000_Pnd_Plan_Type());                                                                 //Natural: ASSIGN SCIA1200.#IRC-SECTION-CODE := SCIL9000.#PLAN-TYPE
        //* *----------------------------------------------------------------------
        //*  CIP VERIFICATION
        //* *----------------------------------------------------------------------
        //*  JRB1 - 6/13/06
        //*  BIP - DO NOT CIP BENE
        //*  BIP - IN THE PLAN ACCTS.
        //*  TNGSUB
        //*  TNGSUB
        //*  CHURCH PLAN
        //*  BHD RHSP
        short decideConditionsMet3446 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIL9000.#DECEDENT-CONTRACT GT ' '
        if (condition(ldaScil9000.getScil9000_Pnd_Decedent_Contract().greater(" ")))
        {
            decideConditionsMet3446++;
            pdaScia1200.getScia1200_Pnd_Cip_Ind().setValue("N");                                                                                                          //Natural: ASSIGN SCIA1200.#CIP-IND := 'N'
        }                                                                                                                                                                 //Natural: WHEN SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y'
        else if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y")))
        {
            decideConditionsMet3446++;
            pdaScia1200.getScia1200_Pnd_Cip_Ind().setValue("N");                                                                                                          //Natural: ASSIGN SCIA1200.#CIP-IND := 'N'
        }                                                                                                                                                                 //Natural: WHEN SCIL9000.#EMPLOYER-TYPE = '03'
        else if (condition(ldaScil9000.getScil9000_Pnd_Employer_Type().equals("03")))
        {
            decideConditionsMet3446++;
            pdaScia1200.getScia1200_Pnd_Cip_Ind().setValue("N");                                                                                                          //Natural: ASSIGN SCIA1200.#CIP-IND := 'N'
        }                                                                                                                                                                 //Natural: WHEN ( ( SCIL9000.#EMPLOYER-TYPE = '02' AND SCIL9000.#ERISA-IND = 'N' ) AND NOT ( SCIA1200.#IRC-SECTION-CODE = '7' OR = '10' OR = '17' ) AND NOT ( SUBSTRING ( SCIL9000.#SUB-PLAN-NO,1,3 ) = 'RH1' ) )
        else if (condition((((ldaScil9000.getScil9000_Pnd_Employer_Type().equals("02") && ldaScil9000.getScil9000_Pnd_Erisa_Ind().equals("N")) && ! (((pdaScia1200.getScia1200_Pnd_Irc_Section_Code().equals("7") 
            || pdaScia1200.getScia1200_Pnd_Irc_Section_Code().equals("10")) || pdaScia1200.getScia1200_Pnd_Irc_Section_Code().equals("17")))) && ! (ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,
            3).equals("RH1")))))
        {
            decideConditionsMet3446++;
            //*  PRIVATE,NON ERISA,NON 457
            pdaScia1200.getScia1200_Pnd_Cip_Ind().setValue("Y");                                                                                                          //Natural: ASSIGN SCIA1200.#CIP-IND = 'Y'
        }                                                                                                                                                                 //Natural: WHEN SUBSTRING ( SCIL9000.#SUB-PLAN-NO,1,2 ) = 'IR' OR = 'KE' OR = 'AA'
        else if (condition(ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,2).equals("IR") || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,2).equals("KE") 
            || ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,2).equals("AA")))
        {
            decideConditionsMet3446++;
            //*  IRAS & KEOGHS & ATRAS
            pdaScia1200.getScia1200_Pnd_Cip_Ind().setValue("Y");                                                                                                          //Natural: ASSIGN SCIA1200.#CIP-IND = 'Y'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Cip_Ind().setValue("N");                                                                                                          //Natural: ASSIGN SCIA1200.#CIP-IND := 'N'
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaScia1200.getScia1200_Pnd_Email_Address().setValue(ldaScil9000.getScil9000_Pnd_Email_Address());                                                                //Natural: ASSIGN SCIA1200.#EMAIL-ADDRESS = SCIL9000.#EMAIL-ADDRESS
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(! (DbsUtil.maskMatches(ldaScil9000.getScil9000_Pnd_Daytime_Phone().getSubstring(pnd_I.getInt(),1),"N"))))                                       //Natural: IF SUBSTR ( SCIL9000.#DAYTIME-PHONE,#I,1 ) NE MASK ( N )
            {
                setValueToSubstring(" ",ldaScil9000.getScil9000_Pnd_Daytime_Phone(),pnd_I.getInt(),1);                                                                    //Natural: MOVE ' ' TO SUBSTR ( SCIL9000.#DAYTIME-PHONE,#I,1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Daytime_Phone(),true), new ExamineSearch(" "), new ExamineDelete());                                //Natural: EXAMINE FULL SCIL9000.#DAYTIME-PHONE FOR ' ' DELETE
        pdaScia1200.getScia1200_Pnd_Phone().setValue(ldaScil9000.getScil9000_Pnd_Daytime_Phone());                                                                        //Natural: ASSIGN SCIA1200.#PHONE = SCIL9000.#DAYTIME-PHONE
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(! (DbsUtil.maskMatches(ldaScil9000.getScil9000_Pnd_Evening_Phone().getSubstring(pnd_I.getInt(),1),"N"))))                                       //Natural: IF SUBSTR ( SCIL9000.#EVENING-PHONE,#I,1 ) NE MASK ( N )
            {
                setValueToSubstring(" ",ldaScil9000.getScil9000_Pnd_Evening_Phone(),pnd_I.getInt(),1);                                                                    //Natural: MOVE ' ' TO SUBSTR ( SCIL9000.#EVENING-PHONE,#I,1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Evening_Phone(),true), new ExamineSearch(" "), new ExamineDelete());                                //Natural: EXAMINE FULL SCIL9000.#EVENING-PHONE FOR ' ' DELETE
        pdaScia1200.getScia1200_Pnd_Res_Phone().setValue(ldaScil9000.getScil9000_Pnd_Evening_Phone());                                                                    //Natural: ASSIGN SCIA1200.#RES-PHONE = SCIL9000.#EVENING-PHONE
        //* *----------------------------------------------------------------------
        //*  ADD INDEXED GUARANTEE RATE INDICATOR FROM ONMI (RS0)
        //* *----------------------------------------------------------------------
        //*  ONEIRA - START HERE... FORCE INDEX AS ALL ONEIRA ACCOUNTS ARE INDEXED
        //*  ONEIRA
        if (condition(ldaScil9000.getScil9000_Pnd_Plan_No().getSubstring(1,3).equals("IRA")))                                                                             //Natural: IF SUBSTR ( SCIL9000.#PLAN-NO,1,3 ) = 'IRA'
        {
            if (condition(! (ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA101") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA201") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA801")))) //Natural: IF NOT ( SCIL9000.#PLAN-NO = 'IRA101' OR = 'IRA201' OR = 'IRA801' )
            {
                //*  THEN ONEIRA PLAN
                ldaScil9000.getScil9000_Pnd_Indx_Guar_Rate_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO SCIL9000.#INDX-GUAR-RATE-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ONEIRA - ENDS HERE... FORCE INDEX AS ALL ONEIRA ACCOUNTS ARE INDEXED
        //*  RS0
        pnd_Wk_Guar_Rate_Ind.setValue(ldaScil9000.getScil9000_Pnd_Indx_Guar_Rate_Ind());                                                                                  //Natural: MOVE SCIL9000.#INDX-GUAR-RATE-IND TO #WK-GUAR-RATE-IND
        setValueToSubstring(pnd_Wk_Guar_Rate_Ind,pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_1(),4,1);                                                                        //Natural: MOVE #WK-GUAR-RATE-IND TO SUBSTR ( SCIA1200.#SG-TEXT-UDF-1,4,1 )
        //* *----------------------------------------------------------------------
        //*  NEW CONTRACT INFO
        //* *----------------------------------------------------------------------
        pdaScia1200.getScia1200_Pnd_Ppg_Code().setValue("SGRD");                                                                                                          //Natural: ASSIGN SCIA1200.#PPG-CODE = 'SGRD'
        //*  PARTICIPANT
        //*  INSTITUTION
        //*  SPECIAL POLICY TEXT - PULL PACKAGE
        short decideConditionsMet3507 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE SCIL9000.#MAILING-INSTRUCTIONS;//Natural: VALUE 'E'
        if (condition((ldaScil9000.getScil9000_Pnd_Mailing_Instructions().equals("E"))))
        {
            decideConditionsMet3507++;
            pdaScia1200.getScia1200_Pnd_Mailing_Instr().setValue("1");                                                                                                    //Natural: ASSIGN SCIA1200.#MAILING-INSTR := '1'
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaScil9000.getScil9000_Pnd_Mailing_Instructions().equals("R"))))
        {
            decideConditionsMet3507++;
            pdaScia1200.getScia1200_Pnd_Mailing_Instr().setValue("2");                                                                                                    //Natural: ASSIGN SCIA1200.#MAILING-INSTR := '2'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaScil9000.getScil9000_Pnd_Mailing_Instructions().equals("S"))))
        {
            decideConditionsMet3507++;
            pdaScia1200.getScia1200_Pnd_Mailing_Instr().setValue("3");                                                                                                    //Natural: ASSIGN SCIA1200.#MAILING-INSTR := '3'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *----------------------------------------------------------------------
        //* * CONVERT PRODUCT CODE FROM OMNI TO IIS PRODUCT CODE FOR SCIA1200
        //* *----------------------------------------------------------------------
        pnd_Hold_Product.setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No().getSubstring(1,3));                                                                           //Natural: MOVE SUBSTRING ( SCIL9000.#SUB-PLAN-NO,1,3 ) TO #HOLD-PRODUCT
        //*  031407 START
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-VESTING-OVERRIDE
        sub_Check_For_Vesting_Override();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Exception_Found.getBoolean()))                                                                                                                  //Natural: IF #EXCEPTION-FOUND
        {
            pdaScia1200.getScia1200_Pnd_Vesting_Code().setValue(pdaScia2100.getScia2100_Pnd_Entry_Dscrptn_Txt().getSubstring(1,1));                                       //Natural: ASSIGN SCIA1200.#VESTING-CODE := SUBSTR ( SCIA2100.#ENTRY-DSCRPTN-TXT,1,1 )
            //*  031407 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  BE1. 11/16/2011 STARTS HERE...
        short decideConditionsMet3530 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF SCIL9000.#RESIDENCE-STATE;//Natural: VALUE 'AE'
        if (condition((ldaScil9000.getScil9000_Pnd_Residence_State().equals("AE"))))
        {
            decideConditionsMet3530++;
            ldaScil9000.getScil9000_Pnd_Residence_State().setValue("NY");                                                                                                 //Natural: MOVE 'NY' TO SCIL9000.#RESIDENCE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'AP'
        else if (condition((ldaScil9000.getScil9000_Pnd_Residence_State().equals("AP"))))
        {
            decideConditionsMet3530++;
            ldaScil9000.getScil9000_Pnd_Residence_State().setValue("CA");                                                                                                 //Natural: MOVE 'CA' TO SCIL9000.#RESIDENCE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'AA'
        else if (condition((ldaScil9000.getScil9000_Pnd_Residence_State().equals("AA"))))
        {
            decideConditionsMet3530++;
            ldaScil9000.getScil9000_Pnd_Residence_State().setValue("FL");                                                                                                 //Natural: MOVE 'FL' TO SCIL9000.#RESIDENCE-STATE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  BE1. 11/16/2011 ENDS HERE.
        //*   TNGSUB
        short decideConditionsMet3542 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF SCIL9000.#CONVERSION-ISSUE-STATE;//Natural: VALUE 'AE'
        if (condition((ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().equals("AE"))))
        {
            decideConditionsMet3542++;
            ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().setValue("NY");                                                                                          //Natural: MOVE 'NY' TO SCIL9000.#CONVERSION-ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'AP'
        else if (condition((ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().equals("AP"))))
        {
            decideConditionsMet3542++;
            ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().setValue("CA");                                                                                          //Natural: MOVE 'CA' TO SCIL9000.#CONVERSION-ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'AA'
        else if (condition((ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().equals("AA"))))
        {
            decideConditionsMet3542++;
            ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().setValue("FL");                                                                                          //Natural: MOVE 'FL' TO SCIL9000.#CONVERSION-ISSUE-STATE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y")))                                                                               //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y'
        {
            if (condition(ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().greater(" ")))                                                                             //Natural: IF SCIL9000.#CONVERSION-ISSUE-STATE GT ' '
            {
                pdaScia1200.getScia1200_Pnd_Conversion_Issue_State().setValue(ldaScil9000.getScil9000_Pnd_Conversion_Issue_State());                                      //Natural: ASSIGN SCIA1200.#CONVERSION-ISSUE-STATE = SCIL9000.#CONVERSION-ISSUE-STATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia1200.getScia1200_Pnd_Conversion_Issue_State().setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                             //Natural: ASSIGN SCIA1200.#CONVERSION-ISSUE-STATE = SCIL9000.#RESIDENCE-STATE
                ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                             //Natural: ASSIGN SCIL9000.#CONVERSION-ISSUE-STATE = SCIL9000.#RESIDENCE-STATE
            }                                                                                                                                                             //Natural: END-IF
            //*   TNGSUB
            //*  DBH2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Issue_St.setValue(true);                                                                                                                                //Natural: ASSIGN #CHECK-ISSUE-ST := TRUE
        //*  SRA
        short decideConditionsMet3564 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #HOLD-PRODUCT;//Natural: VALUE 'SR1'
        if (condition((pnd_Hold_Product.equals("SR1"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("3");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '3'
            //*  MAIL-RES FIX KG
            if (condition(ldaScil9000.getScil9000_Pnd_Residence_State().equals(" ")))                                                                                     //Natural: IF SCIL9000.#RESIDENCE-STATE = ' '
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Mailing_State());                                                                               //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#MAILING-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                             //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#RESIDENCE-STATE
                //*  MAIL-RES FIX KG
                //*  GSRA
                //*  PL919
                //*  GA FOR RFSUNY - MAKE IT RA DV
                //*  PL919
                //*  GA FOR CUNY D.F. CRS 9/10/06
                //*  PL919
                //*  FOR MAYO/FIDELITY 1/17/07
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'GS1','GA1'
        else if (condition((pnd_Hold_Product.equals("GS1") || pnd_Hold_Product.equals("GA1"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("4");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '4'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'GA3'
        else if (condition((pnd_Hold_Product.equals("GA3"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("1");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '1'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'GA5'
        else if (condition((pnd_Hold_Product.equals("GA5"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("25");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '25'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'GA6'
        else if (condition((pnd_Hold_Product.equals("GA6"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("4");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '4'
            //*  COLLEGE OWNED
            pdaScia1200.getScia1200_Pnd_Vesting_Code().setValue("3");                                                                                                     //Natural: ASSIGN SCIA1200.#VESTING-CODE = '3'
            //*  DBH2
            //*  RA - ATRA - DTM 02/16/07
            pnd_Check_Issue_St.reset();                                                                                                                                   //Natural: RESET #CHECK-ISSUE-ST
        }                                                                                                                                                                 //Natural: VALUE 'RA6' , 'AA1'
        else if (condition((pnd_Hold_Product.equals("RA6") || pnd_Hold_Product.equals("AA1"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("1");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '1'
            //*  MAIL-RES FIX KG
            if (condition(ldaScil9000.getScil9000_Pnd_Residence_State().equals(" ")))                                                                                     //Natural: IF SCIL9000.#RESIDENCE-STATE = ' '
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Mailing_State());                                                                               //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#MAILING-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                             //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#RESIDENCE-STATE
                //*  MAIL-RES FIX KG
                //*  GRA
                //*  PL919
                //*  RL
                //*  R5 (CRS)
                //*  PL919
                //*  CRS 8/5/05
                //* * TNT RS PRODUCT
                //*  PL919
                //* * TNT RS PLUS
                //*  PL919
                //* * TNT RS PLUS II
                //*  PL919
                //* * ICAP RETIREMENT CHOICE
                //*  PL919
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'GR1'
        else if (condition((pnd_Hold_Product.equals("GR1"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("2");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '2'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'RL1'
        else if (condition((pnd_Hold_Product.equals("RL1"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("1");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '1'
            pdaScia1200.getScia1200_Pnd_Rl_Contract_No().setValue(ldaScil9000.getScil9000_Pnd_Rl_Contract_No());                                                          //Natural: ASSIGN SCIA1200.#RL-CONTRACT-NO := SCIL9000.#RL-CONTRACT-NO
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
            pdaScia1200.getScia1200_Pnd_Loan_Ind().setValue("Y");                                                                                                         //Natural: ASSIGN SCIA1200.#LOAN-IND := 'Y'
        }                                                                                                                                                                 //Natural: VALUE 'SA1','SA2','SA3'
        else if (condition((pnd_Hold_Product.equals("SA1") || pnd_Hold_Product.equals("SA2") || pnd_Hold_Product.equals("SA3"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("20");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '20'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'SP1','SP2','SP3'
        else if (condition((pnd_Hold_Product.equals("SP1") || pnd_Hold_Product.equals("SP2") || pnd_Hold_Product.equals("SP3"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("21");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '21'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'S21','S22','S23'
        else if (condition((pnd_Hold_Product.equals("S21") || pnd_Hold_Product.equals("S22") || pnd_Hold_Product.equals("S23"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("22");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '22'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'RS1','RS2','RS3'
        else if (condition((pnd_Hold_Product.equals("RS1") || pnd_Hold_Product.equals("RS2") || pnd_Hold_Product.equals("RS3"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("23");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '23'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
            //* * ICAP RETIREMENT CHOICE PLUS
            //*  PL919
                                                                                                                                                                          //Natural: PERFORM STATE-APPROVAL-CHECK
            sub_State_Approval_Check();
            if (condition(Global.isEscape())) {return;}
            pnd_Icap_Product.setValue(true);                                                                                                                              //Natural: ASSIGN #ICAP-PRODUCT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 'RP1','RP2','RP3'
        else if (condition((pnd_Hold_Product.equals("RP1") || pnd_Hold_Product.equals("RP2") || pnd_Hold_Product.equals("RP3"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("24");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '24'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
            //* * IRA TRADITIONAL
                                                                                                                                                                          //Natural: PERFORM STATE-APPROVAL-CHECK
            sub_State_Approval_Check();
            if (condition(Global.isEscape())) {return;}
            pnd_Icap_Product.setValue(true);                                                                                                                              //Natural: ASSIGN #ICAP-PRODUCT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 'IR1'
        else if (condition((pnd_Hold_Product.equals("IR1"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("3");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '3'
            //*  MAIL-RES FIX KG
            if (condition(ldaScil9000.getScil9000_Pnd_Residence_State().equals(" ")))                                                                                     //Natural: IF SCIL9000.#RESIDENCE-STATE = ' '
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Mailing_State());                                                                               //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#MAILING-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                             //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#RESIDENCE-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: END-IF
            //*  RS0
            if (condition(pnd_Wk_Guar_Rate_Ind.equals("Y")))                                                                                                              //Natural: IF #WK-GUAR-RATE-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM STATE-APPROVAL-CHECK
                sub_State_Approval_Check();
                if (condition(Global.isEscape())) {return;}
                pnd_Indx_Product.setValue(true);                                                                                                                          //Natural: ASSIGN #INDX-PRODUCT := TRUE
                //* * IRA ROTH
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IR2'
        else if (condition((pnd_Hold_Product.equals("IR2"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("3");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '3'
            //*  MAIL-RES FIX KG
            if (condition(ldaScil9000.getScil9000_Pnd_Residence_State().equals(" ")))                                                                                     //Natural: IF SCIL9000.#RESIDENCE-STATE = ' '
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Mailing_State());                                                                               //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#MAILING-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                             //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#RESIDENCE-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: END-IF
            //*  RS0
            if (condition(pnd_Wk_Guar_Rate_Ind.equals("Y")))                                                                                                              //Natural: IF #WK-GUAR-RATE-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM STATE-APPROVAL-CHECK
                sub_State_Approval_Check();
                if (condition(Global.isEscape())) {return;}
                pnd_Indx_Product.setValue(true);                                                                                                                          //Natural: ASSIGN #INDX-PRODUCT := TRUE
                //* * IRA ROLLOVER
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IR3'
        else if (condition((pnd_Hold_Product.equals("IR3"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("3");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '3'
            //*  MAIL-RES FIX KG
            if (condition(ldaScil9000.getScil9000_Pnd_Residence_State().equals(" ")))                                                                                     //Natural: IF SCIL9000.#RESIDENCE-STATE = ' '
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Mailing_State());                                                                               //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#MAILING-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                             //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#RESIDENCE-STATE
                //*  MAIL-RES FIX KG
                //* * IRA SEP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IR8'
        else if (condition((pnd_Hold_Product.equals("IR8"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("3");                                                                                                     //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '3'
            //*  MAIL-RES FIX KG
            if (condition(ldaScil9000.getScil9000_Pnd_Residence_State().equals(" ")))                                                                                     //Natural: IF SCIL9000.#RESIDENCE-STATE = ' '
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Mailing_State());                                                                               //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#MAILING-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                             //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#RESIDENCE-STATE
                //*  MAIL-RES FIX KG
            }                                                                                                                                                             //Natural: END-IF
            //*  RS0
            if (condition(pnd_Wk_Guar_Rate_Ind.equals("Y")))                                                                                                              //Natural: IF #WK-GUAR-RATE-IND = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM STATE-APPROVAL-CHECK
                sub_State_Approval_Check();
                if (condition(Global.isEscape())) {return;}
                pnd_Indx_Product.setValue(true);                                                                                                                          //Natural: ASSIGN #INDX-PRODUCT := TRUE
                //* * CRS 04/10/07
                //*  PL919
                //*  BJD RHSP
                //*  BJD RHSP
                //*  BJD RHSP
                //*  PL919
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'KE1' : 'KE9'
        else if (condition((pnd_Hold_Product.equals("KE1' : 'KE9"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("18");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '18'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: VALUE 'RH1'
        else if (condition((pnd_Hold_Product.equals("RH1"))))
        {
            decideConditionsMet3564++;
            pdaScia1200.getScia1200_Pnd_Product_Code().setValue("26");                                                                                                    //Natural: ASSIGN SCIA1200.#PRODUCT-CODE := '26'
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                                 //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#RESIDENCE-STATE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Calc_Issue_State.setValue(ldaScil9000.getScil9000_Pnd_Issue_State());                                                                                     //Natural: ASSIGN #CALC-ISSUE-STATE := SCIL9000.#ISSUE-STATE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DBH2 START NEW IF...EDIT ISSUE STATE
        if (condition(pnd_Check_Issue_St.getBoolean()))                                                                                                                   //Natural: IF #CHECK-ISSUE-ST
        {
            //*   TNGSUB
            //*  ONEIRA
            if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals(" ") && ldaScil9000.getScil9000_Pnd_Oneira_Acct_No().equals(" ")))               //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = ' ' AND SCIL9000.#ONEIRA-ACCT-NO = ' '
            {
                if (condition(pnd_Calc_Issue_State.equals(" ")))                                                                                                          //Natural: IF #CALC-ISSUE-STATE = ' '
                {
                    pnd_Reject_Sw.setValue(true);                                                                                                                         //Natural: ASSIGN #REJECT-SW := TRUE
                    //*  PG1 - START
                    //*  PG1 - END
                    pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(2360) ISSUE STATE IS MISSING"));                            //Natural: COMPRESS *PROGRAM '(2360) ISSUE STATE IS MISSING' INTO SCIA1200.#RETURN-MSG
                    pdaScia1200.getScia1200_Pnd_Last_Name().setValue(ldaScil9000.getScil9000_Pnd_Last_Name());                                                            //Natural: ASSIGN SCIA1200.#LAST-NAME := SCIL9000.#LAST-NAME
                    pdaScia1200.getScia1200_Pnd_First_Name().setValue(ldaScil9000.getScil9000_Pnd_First_Name());                                                          //Natural: ASSIGN SCIA1200.#FIRST-NAME := SCIL9000.#FIRST-NAME
                    pdaScia1200.getScia1200_Pnd_Ssn().setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                        //Natural: ASSIGN SCIA1200.#SSN := SCIL9000.#SSN
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                //*  VERIFY STATE CODE
                DbsUtil.callnat(Appn1158.class , getCurrentProcessState(), pnd_Calc_Issue_State, pnd_Reject_Sw);                                                          //Natural: CALLNAT 'APPN1158' #CALC-ISSUE-STATE #REJECT-SW
                if (condition(Global.isEscape())) return;
                if (condition(pnd_Reject_Sw.getBoolean()))                                                                                                                //Natural: IF #REJECT-SW
                {
                    pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(2378) INVALID ISSUE STATE -", pnd_Calc_Issue_State));       //Natural: COMPRESS *PROGRAM '(2378) INVALID ISSUE STATE -' #CALC-ISSUE-STATE INTO SCIA1200.#RETURN-MSG
                    pdaScia1200.getScia1200_Pnd_Last_Name().setValue(ldaScil9000.getScil9000_Pnd_Last_Name());                                                            //Natural: ASSIGN SCIA1200.#LAST-NAME := SCIL9000.#LAST-NAME
                    pdaScia1200.getScia1200_Pnd_First_Name().setValue(ldaScil9000.getScil9000_Pnd_First_Name());                                                          //Natural: ASSIGN SCIA1200.#FIRST-NAME := SCIL9000.#FIRST-NAME
                    pdaScia1200.getScia1200_Pnd_Ssn().setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                        //Natural: ASSIGN SCIA1200.#SSN := SCIL9000.#SSN
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ONEIRA
                if (condition(ldaScil9000.getScil9000_Pnd_Oneira_Acct_No().equals(" ")))                                                                                  //Natural: IF SCIL9000.#ONEIRA-ACCT-NO = ' '
                {
                    //*   TNGSUB
                    if (condition(ldaScil9000.getScil9000_Pnd_Conversion_Issue_State().equals(" ")))                                                                      //Natural: IF SCIL9000.#CONVERSION-ISSUE-STATE = ' '
                    {
                        pnd_Reject_Sw.setValue(true);                                                                                                                     //Natural: ASSIGN #REJECT-SW := TRUE
                        //*  PG1 - START
                        //*  PG1 - END
                        pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(2360) ISSUE STATE IS MISSING"));                        //Natural: COMPRESS *PROGRAM '(2360) ISSUE STATE IS MISSING' INTO SCIA1200.#RETURN-MSG
                        pdaScia1200.getScia1200_Pnd_Last_Name().setValue(ldaScil9000.getScil9000_Pnd_Last_Name());                                                        //Natural: ASSIGN SCIA1200.#LAST-NAME := SCIL9000.#LAST-NAME
                        pdaScia1200.getScia1200_Pnd_First_Name().setValue(ldaScil9000.getScil9000_Pnd_First_Name());                                                      //Natural: ASSIGN SCIA1200.#FIRST-NAME := SCIL9000.#FIRST-NAME
                        pdaScia1200.getScia1200_Pnd_Ssn().setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                    //Natural: ASSIGN SCIA1200.#SSN := SCIL9000.#SSN
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  VERIFY STATE CODE
                    DbsUtil.callnat(Appn1158.class , getCurrentProcessState(), ldaScil9000.getScil9000_Pnd_Conversion_Issue_State(), pnd_Reject_Sw);                      //Natural: CALLNAT 'APPN1158' SCIL9000.#CONVERSION-ISSUE-STATE #REJECT-SW
                    if (condition(Global.isEscape())) return;
                    if (condition(pnd_Reject_Sw.getBoolean()))                                                                                                            //Natural: IF #REJECT-SW
                    {
                        pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(2378) INVALID ISSUE STATE -", ldaScil9000.getScil9000_Pnd_Conversion_Issue_State())); //Natural: COMPRESS *PROGRAM '(2378) INVALID ISSUE STATE -' SCIL9000.#CONVERSION-ISSUE-STATE INTO SCIA1200.#RETURN-MSG
                        pdaScia1200.getScia1200_Pnd_Last_Name().setValue(ldaScil9000.getScil9000_Pnd_Last_Name());                                                        //Natural: ASSIGN SCIA1200.#LAST-NAME := SCIL9000.#LAST-NAME
                        pdaScia1200.getScia1200_Pnd_First_Name().setValue(ldaScil9000.getScil9000_Pnd_First_Name());                                                      //Natural: ASSIGN SCIA1200.#FIRST-NAME := SCIL9000.#FIRST-NAME
                        pdaScia1200.getScia1200_Pnd_Ssn().setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                    //Natural: ASSIGN SCIA1200.#SSN := SCIL9000.#SSN
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*   TNGSUB
            }                                                                                                                                                             //Natural: END-IF
            //*  DBH2 END NEW IF
            //*  BE 08/29/04
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Issue_State().setValue(pnd_Calc_Issue_State);                                                                                         //Natural: ASSIGN SCIA1200.#ISSUE-STATE := #CALC-ISSUE-STATE
        //* *----------------------------------------------------------------------
        //*  COR
        //* *----------------------------------------------------------------------
        pdaScia1200.getScia1200_Pnd_Pin_Nbr().setValue(ldaScil9000.getScil9000_Pnd_Pin_Nbr());                                                                            //Natural: ASSIGN SCIA1200.#PIN-NBR = SCIL9000.#PIN-NBR
        pdaScia1200.getScia1200_Pnd_Ssn().setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                                    //Natural: ASSIGN SCIA1200.#SSN = SCIL9000.#SSN
        pdaScia1200.getScia1200_Pnd_Last_Name().setValue(ldaScil9000.getScil9000_Pnd_Last_Name());                                                                        //Natural: ASSIGN SCIA1200.#LAST-NAME = SCIL9000.#LAST-NAME
        pdaScia1200.getScia1200_Pnd_First_Name().setValue(ldaScil9000.getScil9000_Pnd_First_Name());                                                                      //Natural: ASSIGN SCIA1200.#FIRST-NAME = SCIL9000.#FIRST-NAME
        pdaScia1200.getScia1200_Pnd_Middle_Name().setValue(ldaScil9000.getScil9000_Pnd_Middle_Name());                                                                    //Natural: ASSIGN SCIA1200.#MIDDLE-NAME = SCIL9000.#MIDDLE-NAME
        pdaScia1200.getScia1200_Pnd_Suffix().setValue(ldaScil9000.getScil9000_Pnd_Suffix());                                                                              //Natural: ASSIGN SCIA1200.#SUFFIX = SCIL9000.#SUFFIX
        pdaScia1200.getScia1200_Pnd_Prefix().setValue(ldaScil9000.getScil9000_Pnd_Prefix());                                                                              //Natural: ASSIGN SCIA1200.#PREFIX = SCIL9000.#PREFIX
        pdaScia1200.getScia1200_Pnd_Date_Of_Birth().setValue(ldaScil9000.getScil9000_Pnd_Date_Of_Birth());                                                                //Natural: ASSIGN SCIA1200.#DATE-OF-BIRTH = SCIL9000.#DATE-OF-BIRTH
        if (condition(! (ldaScil9000.getScil9000_Pnd_Sex().equals("U") || ldaScil9000.getScil9000_Pnd_Sex().equals("M") || ldaScil9000.getScil9000_Pnd_Sex().equals("F")))) //Natural: IF NOT ( SCIL9000.#SEX = 'U' OR = 'M' OR = 'F' )
        {
            pdaScia1200.getScia1200_Pnd_Sex().setValue("M");                                                                                                              //Natural: ASSIGN SCIA1200.#SEX = 'M'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Sex().setValue(ldaScil9000.getScil9000_Pnd_Sex());                                                                                //Natural: ASSIGN SCIA1200.#SEX = SCIL9000.#SEX
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------------------
        //*  MAILING ADDRESS
        //* *----------------------------------------------------------------------
        pdaScia1200.getScia1200_Pnd_Address_Line().getValue(1).setValue(ldaScil9000.getScil9000_Pnd_Mailing_Address_Line().getValue(1));                                  //Natural: ASSIGN SCIA1200.#ADDRESS-LINE ( 1 ) = SCIL9000.#MAILING-ADDRESS-LINE ( 1 )
        pdaScia1200.getScia1200_Pnd_Address_Line().getValue(2).setValue(ldaScil9000.getScil9000_Pnd_Mailing_Address_Line().getValue(2));                                  //Natural: ASSIGN SCIA1200.#ADDRESS-LINE ( 2 ) = SCIL9000.#MAILING-ADDRESS-LINE ( 2 )
        pdaScia1200.getScia1200_Pnd_Address_Line().getValue(3).setValue(ldaScil9000.getScil9000_Pnd_Mailing_Address_Line().getValue(3));                                  //Natural: ASSIGN SCIA1200.#ADDRESS-LINE ( 3 ) = SCIL9000.#MAILING-ADDRESS-LINE ( 3 )
        pdaScia1200.getScia1200_Pnd_Address_Line().getValue(4).setValue(" ");                                                                                             //Natural: ASSIGN SCIA1200.#ADDRESS-LINE ( 4 ) = ' '
        pdaScia1200.getScia1200_Pnd_Address_Line().getValue(5).setValue(" ");                                                                                             //Natural: ASSIGN SCIA1200.#ADDRESS-LINE ( 5 ) = ' '
        pdaScia1200.getScia1200_Pnd_City().setValue(ldaScil9000.getScil9000_Pnd_Mailing_City());                                                                          //Natural: ASSIGN SCIA1200.#CITY = SCIL9000.#MAILING-CITY
        pdaScia1200.getScia1200_Pnd_State().setValue(ldaScil9000.getScil9000_Pnd_Mailing_State());                                                                        //Natural: ASSIGN SCIA1200.#STATE = SCIL9000.#MAILING-STATE
        pdaScia1200.getScia1200_Pnd_State_Nbr().setValue(" ");                                                                                                            //Natural: ASSIGN SCIA1200.#STATE-NBR = ' '
        pdaScia1200.getScia1200_Pnd_Zip_Code().setValue(ldaScil9000.getScil9000_Pnd_Mailing_Zip_Code());                                                                  //Natural: ASSIGN SCIA1200.#ZIP-CODE = SCIL9000.#MAILING-ZIP-CODE
        //* *----------------------------------------------------------------------
        //*  RESIDENCE ADDRESS
        //* *----------------------------------------------------------------------
        pdaScia1200.getScia1200_Pnd_Res_Address_Line().getValue(1).setValue(ldaScil9000.getScil9000_Pnd_Residence_Address_Line().getValue(1));                            //Natural: ASSIGN SCIA1200.#RES-ADDRESS-LINE ( 1 ) = SCIL9000.#RESIDENCE-ADDRESS-LINE ( 1 )
        pdaScia1200.getScia1200_Pnd_Res_Address_Line().getValue(2).setValue(ldaScil9000.getScil9000_Pnd_Residence_Address_Line().getValue(2));                            //Natural: ASSIGN SCIA1200.#RES-ADDRESS-LINE ( 2 ) = SCIL9000.#RESIDENCE-ADDRESS-LINE ( 2 )
        pdaScia1200.getScia1200_Pnd_Res_Address_Line().getValue(3).setValue(ldaScil9000.getScil9000_Pnd_Residence_Address_Line().getValue(3));                            //Natural: ASSIGN SCIA1200.#RES-ADDRESS-LINE ( 3 ) = SCIL9000.#RESIDENCE-ADDRESS-LINE ( 3 )
        pdaScia1200.getScia1200_Pnd_Res_City().setValue(ldaScil9000.getScil9000_Pnd_Residence_City());                                                                    //Natural: ASSIGN SCIA1200.#RES-CITY = SCIL9000.#RESIDENCE-CITY
        pdaScia1200.getScia1200_Pnd_Res_State().setValue(ldaScil9000.getScil9000_Pnd_Residence_State());                                                                  //Natural: ASSIGN SCIA1200.#RES-STATE = SCIL9000.#RESIDENCE-STATE
        pdaScia1200.getScia1200_Pnd_Res_Zip_Code().setValue(ldaScil9000.getScil9000_Pnd_Residence_Zip_Code());                                                            //Natural: ASSIGN SCIA1200.#RES-ZIP-CODE = SCIL9000.#RESIDENCE-ZIP-CODE
        pdaScia1200.getScia1200_Pnd_Res_Country_Code().setValue(ldaScil9000.getScil9000_Pnd_Country_Code());                                                              //Natural: ASSIGN SCIA1200.#RES-COUNTRY-CODE = SCIL9000.#COUNTRY-CODE
        if (condition(ldaScil9000.getScil9000_Pnd_Orchestration_Id().greater(" ") && ldaScil9000.getScil9000_Pnd_Use_Bpel_Data().equals("Y")))                            //Natural: IF SCIL9000.#ORCHESTRATION-ID GT ' ' AND SCIL9000.#USE-BPEL-DATA = 'Y'
        {
            pdaScia1200.getScia1200_Pnd_Field2().setValue(ldaScil9000.getScil9000_Pnd_Use_Bpel_Data());                                                                   //Natural: ASSIGN SCIA1200.#FIELD2 := SCIL9000.#USE-BPEL-DATA
            //*  NY200
            //*  NY200
            if (condition(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Iso_Country_Code().greater(" ")))                                                                       //Natural: IF #SCIA1295-IN.#MAIL-ISO-COUNTRY-CODE GT ' '
            {
                pdaScia1200.getScia1200_Pnd_Mail_Addr_Country_Cd().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Iso_Country_Code());                                  //Natural: ASSIGN SCIA1200.#MAIL-ADDR-COUNTRY-CD := #SCIA1295-IN.#MAIL-ISO-COUNTRY-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaScil9000.getScil9000_Pnd_Mailing_State().equals("CN")))                                                                                  //Natural: IF SCIL9000.#MAILING-STATE = 'CN'
                {
                    pdaScia1200.getScia1200_Pnd_Mail_Addr_Country_Cd().setValue("CA");                                                                                    //Natural: ASSIGN SCIA1200.#MAIL-ADDR-COUNTRY-CD := 'CA'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------------------------------------------------------------------
        //*  ALLOCATIONS
        //* *----------------------------------------------------------------------
        if (condition(ldaScil9000.getScil9000_Pnd_Alloc_Default_Ind().equals("Y") || ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("1") ||                   //Natural: IF SCIL9000.#ALLOC-DEFAULT-IND = 'Y' OR ( SCIL9000.#OMNI-ACCOUNT-ISSUANCE = '1' OR = '2' OR = '3' )
            ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("2") || ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("3")))
        {
            pdaScia1200.getScia1200_Pnd_Alloc_Discrepancy_Ind().setValue("6");                                                                                            //Natural: ASSIGN SCIA1200.#ALLOC-DISCREPANCY-IND := '6'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Alloc_Discrepancy_Ind().setValue("0");                                                                                            //Natural: ASSIGN SCIA1200.#ALLOC-DISCREPANCY-IND := '0'
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Allocation_Model_Type().setValue(ldaScil9000.getScil9000_Pnd_Alloc_Model_Ind());                                                      //Natural: ASSIGN SCIA1200.#ALLOCATION-MODEL-TYPE := SCIL9000.#ALLOC-MODEL-IND
        //*  THIS CODE IS TO MAKE SURE WE DEFAULT TO MMA WHEN SUNGARD DOES
        //*  NOT PASS ALLOCATIONS.
        //*  IISG ADDING A CHECK
        if (condition(ldaScil9000.getScil9000_Pnd_Allocation_Source_1().notEquals(" ")))                                                                                  //Natural: IF SCIL9000.#ALLOCATION-SOURCE-1 NE ' '
        {
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 TO 100
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
            {
                pdaScia1200.getScia1200_Pnd_Allocation().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Allocation().getValue(pnd_I));                              //Natural: ASSIGN SCIA1200.#ALLOCATION ( #I ) = SCIL9000.#ALLOCATION ( #I )
                if (condition(ldaScil9000.getScil9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).getSubstring(1,4).equals("TIAA")))                                     //Natural: IF SUBSTRING ( SCIL9000.#ALLOCATION-FUND-TICKER ( #I ) ,1,4 ) = 'TIAA'
                {
                    pdaScia1200.getScia1200_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).setValue("TIAA#");                                                               //Natural: ASSIGN SCIA1200.#ALLOCATION-FUND-TICKER ( #I ) = 'TIAA#'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia1200.getScia1200_Pnd_Allocation_Fund_Ticker().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Allocation_Fund_Ticker().getValue(pnd_I));  //Natural: ASSIGN SCIA1200.#ALLOCATION-FUND-TICKER ( #I ) = SCIL9000.#ALLOCATION-FUND-TICKER ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  IISG
        }                                                                                                                                                                 //Natural: END-IF
        //*                                     /*   IISG  ADD 2ND ALLOC >>>
        pdaScia1200.getScia1200_Pnd_Fund_Source_1().setValue(ldaScil9000.getScil9000_Pnd_Allocation_Source_1());                                                          //Natural: ASSIGN SCIA1200.#FUND-SOURCE-1 = SCIL9000.#ALLOCATION-SOURCE-1
        pdaScia1200.getScia1200_Pnd_Fund_Source_2().setValue(ldaScil9000.getScil9000_Pnd_Allocation_Source_2());                                                          //Natural: ASSIGN SCIA1200.#FUND-SOURCE-2 = SCIL9000.#ALLOCATION-SOURCE-2
        if (condition(ldaScil9000.getScil9000_Pnd_Allocation_Source_2().notEquals(" ")))                                                                                  //Natural: IF SCIL9000.#ALLOCATION-SOURCE-2 NE ' '
        {
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO 100
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(100)); pnd_I.nadd(1))
            {
                pdaScia1200.getScia1200_Pnd_Allocation_2().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Allocation_Pct_2().getValue(pnd_I));                      //Natural: ASSIGN SCIA1200.#ALLOCATION-2 ( #I ) = SCIL9000.#ALLOCATION-PCT-2 ( #I )
                if (condition(ldaScil9000.getScil9000_Pnd_Allocation_Ticker_2().getValue(pnd_I).getSubstring(1,4).equals("TIAA")))                                        //Natural: IF SUBSTRING ( SCIL9000.#ALLOCATION-TICKER-2 ( #I ) ,1,4 ) = 'TIAA'
                {
                    pdaScia1200.getScia1200_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I).setValue("TIAA#");                                                             //Natural: ASSIGN SCIA1200.#ALLOCATION-FUND-TICKER-2 ( #I ) = 'TIAA#'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia1200.getScia1200_Pnd_Allocation_Fund_Ticker_2().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Allocation_Ticker_2().getValue(pnd_I));   //Natural: ASSIGN SCIA1200.#ALLOCATION-FUND-TICKER-2 ( #I ) = SCIL9000.#ALLOCATION-TICKER-2 ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  IISG    <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  TNGSUB - B.E.
        if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y")))                                                                               //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y'
        {
            //*  PNI0800R - PNI9800R
            //*  SUBSTITUTION WEEKEND
            if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"....'800R'")))                                                                                       //Natural: IF *INIT-USER = MASK ( ....'800R' )
            {
                pdaScia1200.getScia1200_Pnd_Substitution_Contract_Ind().setValue("W");                                                                                    //Natural: ASSIGN SCIA1200.#SUBSTITUTION-CONTRACT-IND := 'W'
                //*  ONE-OFF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia1200.getScia1200_Pnd_Substitution_Contract_Ind().setValue("O");                                                                                    //Natural: ASSIGN SCIA1200.#SUBSTITUTION-CONTRACT-IND := 'O'
            }                                                                                                                                                             //Natural: END-IF
            pdaScia1200.getScia1200_Pnd_Cref_Certificate_No().setValue(ldaScil9000.getScil9000_Pnd_Cref_Certificate_No());                                                //Natural: ASSIGN SCIA1200.#CREF-CERTIFICATE-NO := SCIL9000.#CREF-CERTIFICATE-NO
            pdaScia1200.getScia1200_Pnd_Cref_Date_Of_Issue().setValue(ldaScil9000.getScil9000_Pnd_Cref_Date_Of_Issue());                                                  //Natural: ASSIGN SCIA1200.#CREF-DATE-OF-ISSUE := SCIL9000.#CREF-DATE-OF-ISSUE
            //*   TNGSUB
            //*   MTSIN
            //*   MTSIN
            //*  BIP
            //*  BIP
            //*  BIP
            //*  CNR
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Deceased_Ind().setValue(ldaScil9000.getScil9000_Pnd_Deceased_Ind());                                                                  //Natural: ASSIGN SCIA1200.#DECEASED-IND := SCIL9000.#DECEASED-IND
        pdaScia1200.getScia1200_Pnd_Non_Proprietary_Pkg_Ind().setValue(ldaScil9000.getScil9000_Pnd_Non_Proprietary_Pkg_Ind());                                            //Natural: ASSIGN SCIA1200.#NON-PROPRIETARY-PKG-IND := SCIL9000.#NON-PROPRIETARY-PKG-IND
        pdaScia1200.getScia1200_Pnd_Incmpl_Acct_Ind().setValue(ldaScil9000.getScil9000_Pnd_Incompl_Data_Ind());                                                           //Natural: ASSIGN SCIA1200.#INCMPL-ACCT-IND := SCIL9000.#INCOMPL-DATA-IND
        pdaScia1200.getScia1200_Pnd_As_Ind().setValue(ldaScil9000.getScil9000_Pnd_As_Ind());                                                                              //Natural: ASSIGN SCIA1200.#AS-IND := SCIL9000.#AS-IND
        pdaScia1200.getScia1200_Pnd_Curr_Def_Option().setValue(ldaScil9000.getScil9000_Pnd_Curr_Def_Option());                                                            //Natural: ASSIGN SCIA1200.#CURR-DEF-OPTION := SCIL9000.#CURR-DEF-OPTION
        pdaScia1200.getScia1200_Pnd_Curr_Def_Amt().setValue(ldaScil9000.getScil9000_Pnd_Curr_Def_Amt_A());                                                                //Natural: ASSIGN SCIA1200.#CURR-DEF-AMT := SCIL9000.#CURR-DEF-AMT-A
        pdaScia1200.getScia1200_Pnd_Incr_Option().setValue(ldaScil9000.getScil9000_Pnd_Incr_Option());                                                                    //Natural: ASSIGN SCIA1200.#INCR-OPTION := SCIL9000.#INCR-OPTION
        pdaScia1200.getScia1200_Pnd_Incr_Amt().setValue(ldaScil9000.getScil9000_Pnd_Incr_Amt_A());                                                                        //Natural: ASSIGN SCIA1200.#INCR-AMT := SCIL9000.#INCR-AMT-A
        pdaScia1200.getScia1200_Pnd_Max_Pct().setValue(ldaScil9000.getScil9000_Pnd_Max_Pct_A());                                                                          //Natural: ASSIGN SCIA1200.#MAX-PCT := SCIL9000.#MAX-PCT-A
        pdaScia1200.getScia1200_Pnd_Optout_Days().setValue(ldaScil9000.getScil9000_Pnd_Optout_Days_A());                                                                  //Natural: ASSIGN SCIA1200.#OPTOUT-DAYS := SCIL9000.#OPTOUT-DAYS-A
        pdaScia1200.getScia1200_Pnd_Decedent_Contract().setValue(ldaScil9000.getScil9000_Pnd_Decedent_Contract());                                                        //Natural: ASSIGN SCIA1200.#DECEDENT-CONTRACT := SCIL9000.#DECEDENT-CONTRACT
        pdaScia1200.getScia1200_Pnd_Relation_To_Decedent().setValue(ldaScil9000.getScil9000_Pnd_Relation_To_Decedent());                                                  //Natural: ASSIGN SCIA1200.#RELATION-TO-DECEDENT := SCIL9000.#RELATION-TO-DECEDENT
        pdaScia1200.getScia1200_Pnd_Ssn_Tin_Ind().setValue(ldaScil9000.getScil9000_Pnd_Ssn_Tin_Ind());                                                                    //Natural: ASSIGN SCIA1200.#SSN-TIN-IND := SCIL9000.#SSN-TIN-IND
        pdaScia1200.getScia1200_Pnd_Participant_File().setValue(ldaScil9000.getScil9000_Pnd_Participant_File());                                                          //Natural: ASSIGN SCIA1200.#PARTICIPANT-FILE := SCIL9000.#PARTICIPANT-FILE
        //* BNL2
                                                                                                                                                                          //Natural: PERFORM SET-BENE-DATA-IN-SCIA1200
        sub_Set_Bene_Data_In_Scia1200();
        if (condition(Global.isEscape())) {return;}
    }
    //* BNL2
    private void sub_Set_Bene_Data_In_Scia1200() throws Exception                                                                                                         //Natural: SET-BENE-DATA-IN-SCIA1200
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //*  MWBS
        //*  ASLAC
        //*  ASLAC
        short decideConditionsMet3925 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SCIL9000.#PLAN-NO = '100825' OR = '406772' OR = '406773' OR = '406711' OR = '406712' OR = '407221' OR = '407222' OR = '407223'
        if (condition(ldaScil9000.getScil9000_Pnd_Plan_No().equals("100825") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("406772") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("406773") 
            || ldaScil9000.getScil9000_Pnd_Plan_No().equals("406711") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("406712") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("407221") 
            || ldaScil9000.getScil9000_Pnd_Plan_No().equals("407222") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("407223")))
        {
            decideConditionsMet3925++;
            //*   ILL SURS(100825) AND METRO WATER(406772,406773) BENE HANDLING -MWBS
            //*  MWBS
            //*  MWBS
            //*  MWBS
            getReports().write(0, "DOING LOGIC FOR METROWATER");                                                                                                          //Natural: WRITE 'DOING LOGIC FOR METROWATER'
            if (Global.isEscape()) return;
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(1,1).setValue("BENEFICIARY RECORDS ARE MAINTAINED BY YOUR EMPLOYER.");                               //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( 1,1 ) := 'BENEFICIARY RECORDS ARE MAINTAINED BY YOUR EMPLOYER.'
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(1,2).setValue("PLEASE CONTACT YOUR EMPLOYER TO UPDATE YOUR BENEFICIARY INFORMATION.");               //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( 1,2 ) := 'PLEASE CONTACT YOUR EMPLOYER TO UPDATE YOUR BENEFICIARY INFORMATION.'
            pdaScia1200.getScia1200_Pnd_Bene_Type().getValue(1).setValue("P");                                                                                            //Natural: ASSIGN SCIA1200.#BENE-TYPE ( 1 ) := 'P'
            pdaScia1200.getScia1200_Pnd_Bene_Estate().setValue("N");                                                                                                      //Natural: ASSIGN SCIA1200.#BENE-ESTATE := 'N'
            pdaScia1200.getScia1200_Pnd_Bene_Trust().setValue("N");                                                                                                       //Natural: ASSIGN SCIA1200.#BENE-TRUST := 'N'
            pdaScia1200.getScia1200_Pnd_Bene_Mos_Ind().setValue("N");                                                                                                     //Natural: ASSIGN SCIA1200.#BENE-MOS-IND := 'N'
            pdaScia1200.getScia1200_Pnd_Bene_Category().setValue("4");                                                                                                    //Natural: ASSIGN SCIA1200.#BENE-CATEGORY := '4'
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text_Ind().getValue(1).setValue("T");                                                                                //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT-IND ( 1 ) := 'T'
            //*  EASEENR
            //*  TNGSUB
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN SCIL9000.#ENROLLMENT-METHOD = 'N' OR = 'M' AND SCIL9000.#SUBSTITUTION-CONTRACT-IND = ' '
        else if (condition(((ldaScil9000.getScil9000_Pnd_Enrollment_Method().equals("N") || ldaScil9000.getScil9000_Pnd_Enrollment_Method().equals("M")) 
            && ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals(" "))))
        {
            decideConditionsMet3925++;
            pdaScia1200.getScia1200_Pnd_Bene_Mos_Ind().setValue(ldaScil9000.getScil9000_Pnd_Bene_Mos_Ind());                                                              //Natural: ASSIGN SCIA1200.#BENE-MOS-IND := SCIL9000.#BENE-MOS-IND
            pdaScia1200.getScia1200_Pnd_Bene_Estate().setValue("N");                                                                                                      //Natural: ASSIGN SCIA1200.#BENE-ESTATE := 'N'
            pdaScia1200.getScia1200_Pnd_Bene_Trust().setValue("N");                                                                                                       //Natural: ASSIGN SCIA1200.#BENE-TRUST := 'N'
            pdaScia1200.getScia1200_Pnd_Bene_Category().setValue("1");                                                                                                    //Natural: ASSIGN SCIA1200.#BENE-CATEGORY := '1'
            pdaScia1200.getScia1200_Pnd_Bene_Type().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Type().getValue("*"));                                        //Natural: ASSIGN SCIA1200.#BENE-TYPE ( * ) := SCIL9000.#BENE-TYPE ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Ssn().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue("*"));                                          //Natural: ASSIGN SCIA1200.#BENE-SSN ( * ) := SCIL9000.#BENE-SSN ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Name().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Name().getValue("*"));                                        //Natural: ASSIGN SCIA1200.#BENE-NAME ( * ) := SCIL9000.#BENE-NAME ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Relationship().getValue("*"));                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( * ) := SCIL9000.#BENE-RELATIONSHIP ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue("*"));              //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( * ) := SCIL9000.#BENE-RELATIONSHIP-CODE ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Date_Of_Birth().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue("*"));                      //Natural: ASSIGN SCIA1200.#BENE-DATE-OF-BIRTH ( * ) := SCIL9000.#BENE-DATE-OF-BIRTH ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Alloc_Pct().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue("*"));                              //Natural: ASSIGN SCIA1200.#BENE-ALLOC-PCT ( * ) := SCIL9000.#BENE-ALLOC-PCT ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Numerator().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue("*"));                              //Natural: ASSIGN SCIA1200.#BENE-NUMERATOR ( * ) := SCIL9000.#BENE-NUMERATOR ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Denominator().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue("*"));                          //Natural: ASSIGN SCIA1200.#BENE-DENOMINATOR ( * ) := SCIL9000.#BENE-DENOMINATOR ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text_Ind().getValue("*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Special_Text_Ind().getValue("*"));                //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT-IND ( * ) := SCIL9000.#BENE-SPECIAL-TEXT-IND ( * )
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue("*","*").setValue(ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue("*","*"));                //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( *,* ) := SCIL9000.#BENE-SPECIAL-TEXT ( *,* )
            DbsUtil.callnat(Scin9911.class , getCurrentProcessState(), ldaScil9000.getScil9000_Pnd_Orig_Tiaa_Cntrct(), ldaScil9000.getScil9000_Pnd_Orig_Cref_Cntrct(),    //Natural: CALLNAT 'SCIN9911' SCIL9000.#ORIG-TIAA-CNTRCT SCIL9000.#ORIG-CREF-CNTRCT SCIA1200
                pdaScia1200.getScia1200());
            if (condition(Global.isEscape())) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaScil9000.getScil9000_Pnd_Bene_Estate().equals(" ")))                                                                                             //Natural: IF SCIL9000.#BENE-ESTATE = ' '
        {
            pdaScia1200.getScia1200_Pnd_Bene_Estate().setValue("N");                                                                                                      //Natural: ASSIGN SCIA1200.#BENE-ESTATE = 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Bene_Estate().setValue(ldaScil9000.getScil9000_Pnd_Bene_Estate());                                                                //Natural: ASSIGN SCIA1200.#BENE-ESTATE = SCIL9000.#BENE-ESTATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil9000.getScil9000_Pnd_Bene_Trust().equals(" ")))                                                                                              //Natural: IF SCIL9000.#BENE-TRUST = ' '
        {
            pdaScia1200.getScia1200_Pnd_Bene_Trust().setValue("N");                                                                                                       //Natural: ASSIGN SCIA1200.#BENE-TRUST = 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia1200.getScia1200_Pnd_Bene_Trust().setValue(ldaScil9000.getScil9000_Pnd_Bene_Trust());                                                                  //Natural: ASSIGN SCIA1200.#BENE-TRUST = SCIL9000.#BENE-TRUST
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO BENE DATA TO PROCESS
        if (condition((ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(1).equals(" ") && (ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(1).equals(" ")                 //Natural: IF SCIL9000.#BENE-NAME ( 1 ) = ' ' AND ( SCIL9000.#BENE-SSN ( 1 ) = ' ' OR = '000000000' )
            || ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(1).equals("000000000")))))
        {
            pdaScia1200.getScia1200_Pnd_Bene_Category().setValue("1");                                                                                                    //Natural: ASSIGN SCIA1200.#BENE-CATEGORY = '1'
            //*  DEFAULT TO ESTATE
            pdaScia1200.getScia1200_Pnd_Bene_Estate().setValue("Y");                                                                                                      //Natural: ASSIGN SCIA1200.#BENE-ESTATE = 'Y'
            //*  LOGIC TO HANDLE PROD.PLAN PROVISION DEFAULT.   /* CHG256421   START
            //*  PAPER OR REMITTANCE
            //*  CHG346370
            //*  OTHER
            //*  THIS IS 100%
            if (condition((ldaScil9000.getScil9000_Pnd_Plan_No().equals(pnd_Bene_Dflt_Plans.getValue("*")) && (ldaScil9000.getScil9000_Pnd_Enrollment_Method().equals("P")  //Natural: IF SCIL9000.#PLAN-NO = #BENE-DFLT-PLANS ( * ) AND ( SCIL9000.#ENROLLMENT-METHOD = 'P' OR = 'X' )
                || ldaScil9000.getScil9000_Pnd_Enrollment_Method().equals("X")))))
            {
                pdaScia1200.getScia1200_Pnd_Bene_Name().getValue(1).setValue("DEFAULT TO PLAN / PRODUCT PROVISION");                                                      //Natural: ASSIGN SCIA1200.#BENE-NAME ( 1 ) := 'DEFAULT TO PLAN / PRODUCT PROVISION'
                pdaScia1200.getScia1200_Pnd_Bene_Type().getValue(1).setValue("P");                                                                                        //Natural: ASSIGN SCIA1200.#BENE-TYPE ( 1 ) := 'P'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(1).setValue("40");                                                                          //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( 1 ) := '40'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(1).setValue("OTHER");                                                                            //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( 1 ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Alloc_Pct().getValue(1).setValue(10000);                                                                                 //Natural: ASSIGN SCIA1200.#BENE-ALLOC-PCT ( 1 ) := 10000
                pdaScia1200.getScia1200_Pnd_Bene_Estate().setValue("N");                                                                                                  //Natural: ASSIGN SCIA1200.#BENE-ESTATE := 'N'
                pdaScia1200.getScia1200_Pnd_Bene_Trust().setValue("N");                                                                                                   //Natural: ASSIGN SCIA1200.#BENE-TRUST := 'N'
                pdaScia1200.getScia1200_Pnd_Bene_Mos_Ind().setValue("N");                                                                                                 //Natural: ASSIGN SCIA1200.#BENE-MOS-IND := 'N'
                pdaScia1200.getScia1200_Pnd_Bene_Category().setValue("4");                                                                                                //Natural: ASSIGN SCIA1200.#BENE-CATEGORY := '4'
                pdaScia1200.getScia1200_Pnd_Bene_Special_Text_Ind().getValue(1).setValue("T");                                                                            //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT-IND ( 1 ) := 'T'
                pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(1,1).setValue("You currently have not designated a beneficiary; we will default");               //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( 1,1 ) := 'You currently have not designated a beneficiary; we will default'
                pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(1,2).setValue("to the provisions of the plan and/or product for this account.");                 //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( 1,2 ) := 'to the provisions of the plan and/or product for this account.'
                pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(1,3).setValue("Please add a beneficiary.");                                                      //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( 1,3 ) := 'Please add a beneficiary.'
            }                                                                                                                                                             //Natural: END-IF
            //*  LOGIC TO HANDLE PROD.PLAN PROVISION DEFAULT.  CHG256421   END.
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MORE THAN 10 BENE's
            if (condition(ldaScil9000.getScil9000_Pnd_Bene_Mos_Ind().equals("Y")))                                                                                        //Natural: IF SCIL9000.#BENE-MOS-IND = 'Y'
            {
                pdaScia1200.getScia1200_Pnd_Bene_Category().setValue("2");                                                                                                //Natural: ASSIGN SCIA1200.#BENE-CATEGORY = '2'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  BENE DATA OK
                pdaScia1200.getScia1200_Pnd_Bene_Category().setValue("4");                                                                                                //Natural: ASSIGN SCIA1200.#BENE-CATEGORY = '4'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia1200.getScia1200_Pnd_Bene_Mos_Ind().setValue(ldaScil9000.getScil9000_Pnd_Bene_Mos_Ind());                                                                  //Natural: ASSIGN SCIA1200.#BENE-MOS-IND = SCIL9000.#BENE-MOS-IND
        FOR1:                                                                                                                                                             //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  NO BENE DATA
            //*  AB 07/19/2007
            if (condition((ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(pnd_I).equals(" ") && (ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(pnd_I).equals(" ")     //Natural: IF SCIL9000.#BENE-NAME ( #I ) = ' ' AND ( SCIL9000.#BENE-SSN ( #I ) = ' ' OR = '000000000' )
                || ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(pnd_I).equals("000000000")))))
            {
                //*  AB 07/19/2007
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-BENE-RELATION-CODE
            sub_Translate_Bene_Relation_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FOR1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FOR1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaScia1200.getScia1200_Pnd_Bene_Name().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(pnd_I));                                    //Natural: ASSIGN SCIA1200.#BENE-NAME ( #I ) = SCIL9000.#BENE-NAME ( #I )
            pdaScia1200.getScia1200_Pnd_Bene_Type().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(pnd_I));                                    //Natural: ASSIGN SCIA1200.#BENE-TYPE ( #I ) = SCIL9000.#BENE-TYPE ( #I )
            pdaScia1200.getScia1200_Pnd_Bene_Date_Of_Birth().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(pnd_I));                  //Natural: ASSIGN SCIA1200.#BENE-DATE-OF-BIRTH ( #I ) = SCIL9000.#BENE-DATE-OF-BIRTH ( #I )
            pdaScia1200.getScia1200_Pnd_Bene_Ssn().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(pnd_I));                                      //Natural: ASSIGN SCIA1200.#BENE-SSN ( #I ) = SCIL9000.#BENE-SSN ( #I )
            //*  R4 (CRS) 12/03/04
            pdaScia1200.getScia1200_Pnd_Bene_Alloc_Pct().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(pnd_I));                          //Natural: ASSIGN SCIA1200.#BENE-ALLOC-PCT ( #I ) = SCIL9000.#BENE-ALLOC-PCT ( #I )
            pdaScia1200.getScia1200_Pnd_Bene_Denominator().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(pnd_I));                      //Natural: ASSIGN SCIA1200.#BENE-DENOMINATOR ( #I ) = SCIL9000.#BENE-DENOMINATOR ( #I )
            pdaScia1200.getScia1200_Pnd_Bene_Numerator().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(pnd_I));                          //Natural: ASSIGN SCIA1200.#BENE-NUMERATOR ( #I ) = SCIL9000.#BENE-NUMERATOR ( #I )
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(pnd_I,1).setValue(ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(pnd_I,                    //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( #I,1 ) = SCIL9000.#BENE-SPECIAL-TEXT ( #I,1 )
                1));
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(pnd_I,2).setValue(ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(pnd_I,                    //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( #I,2 ) = SCIL9000.#BENE-SPECIAL-TEXT ( #I,2 )
                2));
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue(pnd_I,3).setValue(ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(pnd_I,                    //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT ( #I,3 ) = SCIL9000.#BENE-SPECIAL-TEXT ( #I,3 )
                3));
            pdaScia1200.getScia1200_Pnd_Bene_Special_Text_Ind().getValue(pnd_I).setValue(ldaScil9000.getScil9000_Pnd_Bene_Special_Text_Ind().getValue(pnd_I));            //Natural: ASSIGN SCIA1200.#BENE-SPECIAL-TEXT-IND ( #I ) = SCIL9000.#BENE-SPECIAL-TEXT-IND ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * NY200 START - MOVE THE BENE FIELDS NOT AVAILABLE IN SCIL9000 FROM
        //* * SCIA1295 TO SCIA1200.
        //* BNL2F1
        //* BNL2F1
        //* BNL2F1
        if (condition((ldaScil9000.getScil9000_Pnd_Orchestration_Id().greater(" ") && ldaScil9000.getScil9000_Pnd_Use_Bpel_Data().equals("Y"))))                          //Natural: IF ( SCIL9000.#ORCHESTRATION-ID GT ' ' AND SCIL9000.#USE-BPEL-DATA = 'Y' )
        {
            pdaScia1200.getScia1200_Pnd_Orchestration_Id().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Orchestration_Id());                                               //Natural: ASSIGN SCIA1200.#ORCHESTRATION-ID := #SCIA1295-IN.#ORCHESTRATION-ID
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 1 ) := #SCIA1295-IN.#BENE1-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 1 ) := #SCIA1295-IN.#BENE1-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 1 ) := #SCIA1295-IN.#BENE1-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 1 ) := #SCIA1295-IN.#BENE1-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 1 ) := #SCIA1295-IN.#BENE1-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 1 ) := #SCIA1295-IN.#BENE1-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 1 ) := #SCIA1295-IN.#BENE1-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 1 ) := #SCIA1295-IN.#BENE1-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 1 ) := #SCIA1295-IN.#BENE1-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 2 ) := #SCIA1295-IN.#BENE2-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 2 ) := #SCIA1295-IN.#BENE2-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 2 ) := #SCIA1295-IN.#BENE2-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 2 ) := #SCIA1295-IN.#BENE2-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 2 ) := #SCIA1295-IN.#BENE2-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 2 ) := #SCIA1295-IN.#BENE2-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 2 ) := #SCIA1295-IN.#BENE2-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 2 ) := #SCIA1295-IN.#BENE2-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 2 ) := #SCIA1295-IN.#BENE2-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 3 ) := #SCIA1295-IN.#BENE3-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 3 ) := #SCIA1295-IN.#BENE3-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 3 ) := #SCIA1295-IN.#BENE3-ADDR-LINE-3-CITY
            //* TESTRA
            getReports().write(0, "#BENE-ADDR3-CITY(3): ",pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(3));                                                     //Natural: WRITE '#BENE-ADDR3-CITY(3): ' SCIA1200.#BENE-ADDR3-CITY ( 3 )
            if (Global.isEscape()) return;
            //* TESTRA
            getReports().write(0, "#BENE3-STATE : ",pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_State());                                                                    //Natural: WRITE '#BENE3-STATE : ' #SCIA1295-IN.#BENE3-STATE
            if (Global.isEscape()) return;
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 3 ) := #SCIA1295-IN.#BENE3-STATE
            //* TESTRA
            getReports().write(0, "#BENE3-STATE(3) : ",pdaScia1200.getScia1200_Pnd_Bene_State().getValue(3));                                                             //Natural: WRITE '#BENE3-STATE(3) : ' SCIA1200.#BENE-STATE ( 3 )
            if (Global.isEscape()) return;
            //* TESTRA
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            getReports().write(0, "#BENE3-ZIP      : ",pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Zip());                                                                   //Natural: WRITE '#BENE3-ZIP      : ' #SCIA1295-IN.#BENE3-ZIP
            if (Global.isEscape()) return;
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 3 ) := #SCIA1295-IN.#BENE3-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 3 ) := #SCIA1295-IN.#BENE3-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 3 ) := #SCIA1295-IN.#BENE3-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 3 ) := #SCIA1295-IN.#BENE3-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 3 ) := #SCIA1295-IN.#BENE3-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 4 ) := #SCIA1295-IN.#BENE4-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 4 ) := #SCIA1295-IN.#BENE4-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 4 ) := #SCIA1295-IN.#BENE4-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 4 ) := #SCIA1295-IN.#BENE4-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 4 ) := #SCIA1295-IN.#BENE4-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 4 ) := #SCIA1295-IN.#BENE4-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 4 ) := #SCIA1295-IN.#BENE4-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 4 ) := #SCIA1295-IN.#BENE4-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 4 ) := #SCIA1295-IN.#BENE4-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 5 ) := #SCIA1295-IN.#BENE5-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 5 ) := #SCIA1295-IN.#BENE5-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 5 ) := #SCIA1295-IN.#BENE5-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 5 ) := #SCIA1295-IN.#BENE5-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 5 ) := #SCIA1295-IN.#BENE5-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 5 ) := #SCIA1295-IN.#BENE5-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 5 ) := #SCIA1295-IN.#BENE5-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 5 ) := #SCIA1295-IN.#BENE5-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 5 ) := #SCIA1295-IN.#BENE5-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 6 ) := #SCIA1295-IN.#BENE6-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 6 ) := #SCIA1295-IN.#BENE6-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 6 ) := #SCIA1295-IN.#BENE6-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 6 ) := #SCIA1295-IN.#BENE6-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 6 ) := #SCIA1295-IN.#BENE6-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 6 ) := #SCIA1295-IN.#BENE6-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 6 ) := #SCIA1295-IN.#BENE6-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 6 ) := #SCIA1295-IN.#BENE6-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 6 ) := #SCIA1295-IN.#BENE6-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 7 ) := #SCIA1295-IN.#BENE7-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 7 ) := #SCIA1295-IN.#BENE7-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 7 ) := #SCIA1295-IN.#BENE7-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 7 ) := #SCIA1295-IN.#BENE7-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 7 ) := #SCIA1295-IN.#BENE7-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 7 ) := #SCIA1295-IN.#BENE7-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 7 ) := #SCIA1295-IN.#BENE7-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 7 ) := #SCIA1295-IN.#BENE7-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 7 ) := #SCIA1295-IN.#BENE7-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 8 ) := #SCIA1295-IN.#BENE8-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 8 ) := #SCIA1295-IN.#BENE8-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 8 ) := #SCIA1295-IN.#BENE8-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 8 ) := #SCIA1295-IN.#BENE8-STATE
            //* TESTRA
            getReports().write(0, "#BENE8-STATE:-",pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_State());                                                                     //Natural: WRITE '#BENE8-STATE:-' #SCIA1295-IN.#BENE8-STATE
            if (Global.isEscape()) return;
            //* TESTRA
            getReports().write(0, "#BENE8-ZIP            : ",pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Zip());                                                             //Natural: WRITE '#BENE8-ZIP            : ' #SCIA1295-IN.#BENE8-ZIP
            if (Global.isEscape()) return;
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 8 ) := #SCIA1295-IN.#BENE8-ZIP
            //* TESTRA
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            //* BNL2F1
            getReports().write(0, "#BENE-ZIP(8)       : ",pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(8));                                                            //Natural: WRITE '#BENE-ZIP(8)       : ' SCIA1200.#BENE-ZIP ( 8 )
            if (Global.isEscape()) return;
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 8 ) := #SCIA1295-IN.#BENE8-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 8 ) := #SCIA1295-IN.#BENE8-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 8 ) := #SCIA1295-IN.#BENE8-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 8 ) := #SCIA1295-IN.#BENE8-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Addr_Line_1());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 9 ) := #SCIA1295-IN.#BENE9-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Addr_Line_2());                                        //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 9 ) := #SCIA1295-IN.#BENE9-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Addr_Line_3_City());                              //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 9 ) := #SCIA1295-IN.#BENE9-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_State());                                              //Natural: ASSIGN SCIA1200.#BENE-STATE ( 9 ) := #SCIA1295-IN.#BENE9-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Zip());                                                  //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 9 ) := #SCIA1295-IN.#BENE9-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Phone());                                              //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 9 ) := #SCIA1295-IN.#BENE9-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Gender());                                            //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 9 ) := #SCIA1295-IN.#BENE9-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Country());                                          //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 9 ) := #SCIA1295-IN.#BENE9-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Name2());                                      //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 9 ) := #SCIA1295-IN.#BENE9-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 10 ) := #SCIA1295-IN.#BENE10-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 10 ) := #SCIA1295-IN.#BENE10-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 10 ) := #SCIA1295-IN.#BENE10-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 10 ) := #SCIA1295-IN.#BENE10-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 10 ) := #SCIA1295-IN.#BENE10-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 10 ) := #SCIA1295-IN.#BENE10-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 10 ) := #SCIA1295-IN.#BENE10-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 10 ) := #SCIA1295-IN.#BENE10-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 10 ) := #SCIA1295-IN.#BENE10-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 11 ) := #SCIA1295-IN.#BENE11-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 11 ) := #SCIA1295-IN.#BENE11-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 11 ) := #SCIA1295-IN.#BENE11-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 11 ) := #SCIA1295-IN.#BENE11-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 11 ) := #SCIA1295-IN.#BENE11-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 11 ) := #SCIA1295-IN.#BENE11-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 11 ) := #SCIA1295-IN.#BENE11-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 11 ) := #SCIA1295-IN.#BENE11-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 11 ) := #SCIA1295-IN.#BENE11-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 12 ) := #SCIA1295-IN.#BENE12-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 12 ) := #SCIA1295-IN.#BENE12-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 12 ) := #SCIA1295-IN.#BENE12-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 12 ) := #SCIA1295-IN.#BENE12-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 12 ) := #SCIA1295-IN.#BENE12-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 12 ) := #SCIA1295-IN.#BENE12-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 12 ) := #SCIA1295-IN.#BENE12-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 12 ) := #SCIA1295-IN.#BENE12-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 12 ) := #SCIA1295-IN.#BENE12-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 13 ) := #SCIA1295-IN.#BENE13-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 13 ) := #SCIA1295-IN.#BENE13-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 13 ) := #SCIA1295-IN.#BENE13-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 13 ) := #SCIA1295-IN.#BENE13-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 13 ) := #SCIA1295-IN.#BENE13-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 13 ) := #SCIA1295-IN.#BENE13-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 13 ) := #SCIA1295-IN.#BENE13-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 13 ) := #SCIA1295-IN.#BENE13-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 13 ) := #SCIA1295-IN.#BENE13-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 14 ) := #SCIA1295-IN.#BENE14-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 14 ) := #SCIA1295-IN.#BENE14-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 14 ) := #SCIA1295-IN.#BENE14-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 14 ) := #SCIA1295-IN.#BENE14-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 14 ) := #SCIA1295-IN.#BENE14-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 14 ) := #SCIA1295-IN.#BENE14-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 14 ) := #SCIA1295-IN.#BENE14-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 14 ) := #SCIA1295-IN.#BENE14-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 14 ) := #SCIA1295-IN.#BENE14-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 15 ) := #SCIA1295-IN.#BENE15-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 15 ) := #SCIA1295-IN.#BENE15-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 15 ) := #SCIA1295-IN.#BENE15-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 15 ) := #SCIA1295-IN.#BENE15-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 15 ) := #SCIA1295-IN.#BENE15-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 15 ) := #SCIA1295-IN.#BENE15-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 15 ) := #SCIA1295-IN.#BENE15-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 15 ) := #SCIA1295-IN.#BENE15-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 15 ) := #SCIA1295-IN.#BENE15-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 16 ) := #SCIA1295-IN.#BENE16-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 16 ) := #SCIA1295-IN.#BENE16-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 16 ) := #SCIA1295-IN.#BENE16-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 16 ) := #SCIA1295-IN.#BENE16-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 16 ) := #SCIA1295-IN.#BENE16-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 16 ) := #SCIA1295-IN.#BENE16-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 16 ) := #SCIA1295-IN.#BENE16-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 16 ) := #SCIA1295-IN.#BENE16-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 16 ) := #SCIA1295-IN.#BENE16-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 17 ) := #SCIA1295-IN.#BENE17-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 17 ) := #SCIA1295-IN.#BENE17-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 17 ) := #SCIA1295-IN.#BENE17-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 17 ) := #SCIA1295-IN.#BENE17-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 17 ) := #SCIA1295-IN.#BENE17-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 17 ) := #SCIA1295-IN.#BENE17-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 17 ) := #SCIA1295-IN.#BENE17-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 17 ) := #SCIA1295-IN.#BENE17-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 17 ) := #SCIA1295-IN.#BENE17-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 18 ) := #SCIA1295-IN.#BENE18-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 18 ) := #SCIA1295-IN.#BENE18-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 18 ) := #SCIA1295-IN.#BENE18-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 18 ) := #SCIA1295-IN.#BENE18-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 18 ) := #SCIA1295-IN.#BENE18-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 18 ) := #SCIA1295-IN.#BENE18-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 18 ) := #SCIA1295-IN.#BENE18-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 18 ) := #SCIA1295-IN.#BENE18-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 18 ) := #SCIA1295-IN.#BENE18-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 19 ) := #SCIA1295-IN.#BENE19-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 19 ) := #SCIA1295-IN.#BENE19-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 19 ) := #SCIA1295-IN.#BENE19-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 19 ) := #SCIA1295-IN.#BENE19-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 19 ) := #SCIA1295-IN.#BENE19-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 19 ) := #SCIA1295-IN.#BENE19-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 19 ) := #SCIA1295-IN.#BENE19-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 19 ) := #SCIA1295-IN.#BENE19-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 19 ) := #SCIA1295-IN.#BENE19-NAME2
            pdaScia1200.getScia1200_Pnd_Bene_Addr1().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Addr_Line_1());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR1 ( 20 ) := #SCIA1295-IN.#BENE20-ADDR-LINE-1
            pdaScia1200.getScia1200_Pnd_Bene_Addr2().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Addr_Line_2());                                      //Natural: ASSIGN SCIA1200.#BENE-ADDR2 ( 20 ) := #SCIA1295-IN.#BENE20-ADDR-LINE-2
            pdaScia1200.getScia1200_Pnd_Bene_Addr3_City().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Addr_Line_3_City());                            //Natural: ASSIGN SCIA1200.#BENE-ADDR3-CITY ( 20 ) := #SCIA1295-IN.#BENE20-ADDR-LINE-3-CITY
            pdaScia1200.getScia1200_Pnd_Bene_State().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_State());                                            //Natural: ASSIGN SCIA1200.#BENE-STATE ( 20 ) := #SCIA1295-IN.#BENE20-STATE
            pdaScia1200.getScia1200_Pnd_Bene_Zip().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Zip());                                                //Natural: ASSIGN SCIA1200.#BENE-ZIP ( 20 ) := #SCIA1295-IN.#BENE20-ZIP
            pdaScia1200.getScia1200_Pnd_Bene_Phone().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Phone());                                            //Natural: ASSIGN SCIA1200.#BENE-PHONE ( 20 ) := #SCIA1295-IN.#BENE20-PHONE
            pdaScia1200.getScia1200_Pnd_Bene_Gender().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Gender());                                          //Natural: ASSIGN SCIA1200.#BENE-GENDER ( 20 ) := #SCIA1295-IN.#BENE20-GENDER
            pdaScia1200.getScia1200_Pnd_Bene_Country().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Country());                                        //Natural: ASSIGN SCIA1200.#BENE-COUNTRY ( 20 ) := #SCIA1295-IN.#BENE20-COUNTRY
            pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Name2());                                    //Natural: ASSIGN SCIA1200.#BENE-EXTENDED-NAME ( 20 ) := #SCIA1295-IN.#BENE20-NAME2
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Translate_Bene_Relation_Code() throws Exception                                                                                                      //Natural: TRANSLATE-BENE-RELATION-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //* * SRK KG - TRANSLATED BENE RELATIONSHIP CODE FROM OPE - START
        if (condition(ldaScil9000.getScil9000_Pnd_Orchestration_Id().greater(" ") && ldaScil9000.getScil9000_Pnd_Use_Bpel_Data().equals("Y")))                            //Natural: IF SCIL9000.#ORCHESTRATION-ID GT ' ' AND SCIL9000.#USE-BPEL-DATA = 'Y'
        {
            //*  HUSBAND
            //*  WIFE
            //*  DAUGHTER
            //*  SON
            //*  ESTATE
            //*  GRANDDAUGHTER
            //* MTSIN
            //*  GRANDSON
            //*  TRUST
            //*  FRIEND
            //*  PARTNER
            //*  AUNT
            //*  BOYFRIEND
            //*  BROTHER
            //*  BROTHER IN LAW
            //*  MTSIN
            //*  CHILD
            //*  CHILDREN
            //*  COMMON LAW HUSBAND
            //* MTSIN
            //*  COMMON LAW WIFE
            //* MTSIN
            //*  COUSIN
            //*  DAUGHTER IN LAW
            //* MTSIN
            //*  FATHER
            //*  FATHER-IN-LAW
            //* MTSIN
            //*  FIANCE
            //*  GIRLFRIEND
            //*  GRANDCHILD
            //*  GRANDFATHER
            //* MTSIN
            //*  GRANDMOTHER
            //* MTSIN
            //*  GRANDPARENTS
            //* MTSIN
            //*  INSTITUTION
            //*  LIVING TRUST NON-SPOUSE
            //* MTSIN
            //*  LIVING TRUST SPOUSE
            //* MTSIN
            //*  MOTHER
            //*  MOTHER-IN-LAW
            //* MTSIN
            //*  NEPHEW
            //*  NIECE
            //*  NON-SPOUSE
            //* MTSIN
            //*  ORGANIZATION
            //* MTSIN
            //*  OTHER
            //*  SIGNIFICANT OTHER
            //* MTSIN
            //*  SISTER
            //*  SISTER IN LAW
            //* MTSIN
            //*  SON IN LAW
            //* MTSIN
            //*  SPOUSE
            //*  STEP-DAUGHTER
            //* MTSIN
            //*  STEP-FATHER
            //* MTSIN
            //*  STEP-MOTHER
            //* MTSIN
            //*  STEP-SON
            //* MTSIN
            //*  UNCLE
            //*  DOMESTIC PARTNER
            //* MTSIN
            //*  EX-SPOUSE
            //* MTSIN
            //*  GODCHILD
            //* MTSIN
            //*  WILL
            //* MTSIN
            //*  PARENT
            //*  SIBLING
            //*  GUARDIAN
            //*  COMMON LAW SPOUSE
            //*  MTSIN
            //*  STEP PARENT
            //*  STEP-CHILD
            //*  BUSINESS PARTNER
            //* MTSIN
            short decideConditionsMet4366 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF SCIL9000.#BENE-RELATIONSHIP-CODE ( #I );//Natural: VALUE '01', '1'
            if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("01") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("1"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("HUSBAND");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'HUSBAND'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("05");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '05'
            }                                                                                                                                                             //Natural: VALUE '02', '2'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("02") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("2"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("WIFE");                                                                         //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'WIFE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("06");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '06'
            }                                                                                                                                                             //Natural: VALUE '03', '3'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("03") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("3"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("DAUGHTER");                                                                     //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'DAUGHTER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("10");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '10'
            }                                                                                                                                                             //Natural: VALUE '04', '4'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("04") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("4"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SON");                                                                          //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SON'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("09");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '09'
            }                                                                                                                                                             //Natural: VALUE '05', '5'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("05") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("5"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("ESTATE");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'ESTATE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("35");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '35'
            }                                                                                                                                                             //Natural: VALUE '06', '6'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("06") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("6"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDDAUGHTER");                                                                //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDDAUGHTER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("33");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '33'
            }                                                                                                                                                             //Natural: VALUE '07', '7'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("07") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("7"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDSON");                                                                     //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDSON'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("32");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '32'
            }                                                                                                                                                             //Natural: VALUE '08', '8'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("08") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("8"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("TRUST");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'TRUST'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("36");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '36'
            }                                                                                                                                                             //Natural: VALUE '09', '9'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("09") || ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("9"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("FRIEND");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'FRIEND'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("12");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '12'
            }                                                                                                                                                             //Natural: VALUE '10'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("10"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("PARTNER");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'PARTNER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("30");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '30'
            }                                                                                                                                                             //Natural: VALUE '11'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("11"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("AUNT");                                                                         //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'AUNT'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("13");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '13'
            }                                                                                                                                                             //Natural: VALUE '12'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("12"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("BOYFRIEND");                                                                    //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'BOYFRIEND'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("17");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '17'
            }                                                                                                                                                             //Natural: VALUE '13'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("13"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("BROTHER");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'BROTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("01");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '01'
            }                                                                                                                                                             //Natural: VALUE '14'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("14"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("BROTHER-IN-LAW");                                                               //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'BROTHER-IN-LAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("41");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '41'
            }                                                                                                                                                             //Natural: VALUE '15'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("15"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("CHILD");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'CHILD'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("07");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '07'
            }                                                                                                                                                             //Natural: VALUE '16'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("16"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("CHILDREN");                                                                     //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'CHILDREN'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("08");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '08'
            }                                                                                                                                                             //Natural: VALUE '17'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("17"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("COM-LAW HUSBAND");                                                              //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'COM-LAW HUSBAND'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("19");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '19'
            }                                                                                                                                                             //Natural: VALUE '18'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("18"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("COM-LAW WIFE");                                                                 //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'COM-LAW WIFE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("20");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '20'
            }                                                                                                                                                             //Natural: VALUE '19'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("19"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("COUSIN");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'COUSIN'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("11");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '11'
            }                                                                                                                                                             //Natural: VALUE '20'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("20"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("DAUGHTER-IN-LAW");                                                              //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'DAUGHTER-IN-LAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("44");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '44'
            }                                                                                                                                                             //Natural: VALUE '21'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("21"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("FATHER");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'FATHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("03");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '03'
            }                                                                                                                                                             //Natural: VALUE '22'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("22"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("FATHER-IN-LAW");                                                                //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'FATHER-IN-LAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("25");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '25'
            }                                                                                                                                                             //Natural: VALUE '23'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("23"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("FIANCE");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'FIANCE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("16");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '16'
            }                                                                                                                                                             //Natural: VALUE '24'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("24"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GIRLFRIEND");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GIRLFRIEND'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("18");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '18'
            }                                                                                                                                                             //Natural: VALUE '25'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("25"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDCHILD");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDCHILD'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("34");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '34'
            }                                                                                                                                                             //Natural: VALUE '26'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("26"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDFATHER");                                                                  //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDFATHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("22");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '22'
            }                                                                                                                                                             //Natural: VALUE '27'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("27"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDMOTHER");                                                                  //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDMOTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("21");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '21'
            }                                                                                                                                                             //Natural: VALUE '28'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("28"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDPARENT");                                                                  //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDPARENT'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("23");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '23'
            }                                                                                                                                                             //Natural: VALUE '29'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("29"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("INSTITUTION");                                                                  //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'INSTITUTION'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("45");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '45'
            }                                                                                                                                                             //Natural: VALUE '30'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("30"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("LT TRUST-NSPSE");                                                               //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'LT TRUST-NSPSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("46");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '46'
            }                                                                                                                                                             //Natural: VALUE '31'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("31"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("LT TRUST-SPSE");                                                                //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'LT TRUST-SPSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("47");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '47'
            }                                                                                                                                                             //Natural: VALUE '32'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("32"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("MOTHER");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'MOTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("02");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '02'
            }                                                                                                                                                             //Natural: VALUE '33'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("33"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("MOTHER-IN-LAW");                                                                //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'MOTHER-IN-LAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("24");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '24'
            }                                                                                                                                                             //Natural: VALUE '34'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("34"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("NEPHEW");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'NEPHEW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("39");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '39'
            }                                                                                                                                                             //Natural: VALUE '35'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("35"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("NIECE");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'NIECE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("38");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '38'
            }                                                                                                                                                             //Natural: VALUE '36'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("36"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("NON-SPOUSE");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'NON-SPOUSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("48");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '48'
            }                                                                                                                                                             //Natural: VALUE '37'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("37"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("ORGANIZATION");                                                                 //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'ORGANIZATION'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("37");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '37'
            }                                                                                                                                                             //Natural: VALUE '38'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("38"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE '39'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("39"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SIGNIFICANT OTHER");                                                            //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SIGNIFICANT OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("15");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '15'
            }                                                                                                                                                             //Natural: VALUE '40'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("40"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SISTER");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SISTER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("04");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '04'
            }                                                                                                                                                             //Natural: VALUE '41'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("41"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SISTER-IN-LAW");                                                                //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SISTER-IN-LAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("42");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '42'
            }                                                                                                                                                             //Natural: VALUE '42'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("42"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SON-IN-LAW");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SON-IN-LAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("43");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '43'
            }                                                                                                                                                             //Natural: VALUE '43'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("43"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SPOUSE");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SPOUSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("31");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '31'
            }                                                                                                                                                             //Natural: VALUE '44'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("44"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEP-DAUGHTER");                                                                //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEP-DAUGHTER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("29");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '29'
            }                                                                                                                                                             //Natural: VALUE '45'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("45"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEP-FATHER");                                                                  //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEP-FATHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("26");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '26'
            }                                                                                                                                                             //Natural: VALUE '46'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("46"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEP-MOTHER");                                                                  //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEP-MOTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("27");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '27'
            }                                                                                                                                                             //Natural: VALUE '47'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("47"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEP-SON");                                                                     //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEP-SON'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("28");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '28'
            }                                                                                                                                                             //Natural: VALUE '48'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("48"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("UNCLE");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'UNCLE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("14");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '14'
            }                                                                                                                                                             //Natural: VALUE '49'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("49"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("DOMESTIC PARTNER");                                                             //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'DOMESTIC PARTNER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("49");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '49'
            }                                                                                                                                                             //Natural: VALUE '50'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("50"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("EX-SPOUSE");                                                                    //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'EX-SPOUSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("50");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '50'
            }                                                                                                                                                             //Natural: VALUE '51'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("51"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GOD-CHILD");                                                                    //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GOD-CHILD'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("51");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '51'
            }                                                                                                                                                             //Natural: VALUE '52'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("52"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("TESTAMENTARY TR");                                                              //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'TESTAMENTARY TR'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("52");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '52'
            }                                                                                                                                                             //Natural: VALUE '53'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("53"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("PARENT");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'PARENT'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("53");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '53'
            }                                                                                                                                                             //Natural: VALUE '54'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("54"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SIBLING");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SIBLING'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("54");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '54'
            }                                                                                                                                                             //Natural: VALUE '55'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("55"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GUARDIAN");                                                                     //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GUARDIAN'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("55");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '55'
            }                                                                                                                                                             //Natural: VALUE '56'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("56"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("COMMON LAW SPOUSE");                                                            //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'COMMON LAW SPOUSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("56");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '56'
            }                                                                                                                                                             //Natural: VALUE '57'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("57"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEP PARENT");                                                                  //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEP PARENT'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("57");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '57'
            }                                                                                                                                                             //Natural: VALUE '58'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("58"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEP-CHILD");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEP-CHILD'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("58");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '58'
            }                                                                                                                                                             //Natural: VALUE '59'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("59"))))
            {
                decideConditionsMet4366++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("BUSINESS PARTNER");                                                             //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'BUSINESS PARTNER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("59");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '59'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: END-DECIDE
            //* * SRK KG - TRANSLATED BENE RELATIONSHIP CODE FROM OPE - END
            //*  SRK KG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  SPOUSE (DEFAULT)
            //*  AUNT
            //*  BUSINESS
            //*  DAUGHTER
            //*  FATHER
            //*  FRIEND
            //*  FATHER-IN-LAW
            //*  GRANDCHILD
            //*  GRANDFATHER
            //*  GRANDMOTHER
            //*  MOTHER
            //*  MOTHER-IN-LAW
            //*  NIECE-NEPHEW
            //*  STEP-DAUGHTER
            //*  STEP-FATHER
            //*  STEP-MOTHER
            //*  STEP-SON
            //*  SISTER-BROTHER
            //*  SON
            //*  UNCLE
            //*  CHARITY
            //*  CHURCH
            //*  CREDITOR
            //*  EMPLOYER
            //*  ESTATE
            //*  SCHOOL
            //*  TRUST
            //*  OTHER
            //*  ORIGINATOR
            //*  SPOUSE (DEFAULT)
            short decideConditionsMet4581 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF SCIL9000.#BENE-RELATIONSHIP-CODE ( #I );//Natural: VALUE '0'
            if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("0"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SPOUSE");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SPOUSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("31");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '31'
            }                                                                                                                                                             //Natural: VALUE '1'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("1"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("AUNT");                                                                         //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'AUNT'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("13");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '13'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("2"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("3"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("DAUGHTER");                                                                     //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'DAUGHTER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("10");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '10'
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("4"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("FATHER");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'FATHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("03");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '03'
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("5"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("FRIEND");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'FRIEND'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("12");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '12'
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("6"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("FATHRINLAW");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'FATHRINLAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("25");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '25'
            }                                                                                                                                                             //Natural: VALUE '7'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("7"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDCHILD");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDCHILD'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("34");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '34'
            }                                                                                                                                                             //Natural: VALUE '8'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("8"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDFATHR");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDFATHR'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("22");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '22'
            }                                                                                                                                                             //Natural: VALUE '9'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("9"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("GRANDMOTHR");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'GRANDMOTHR'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("21");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '21'
            }                                                                                                                                                             //Natural: VALUE 'A'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("A"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("MOTHER");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'MOTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("02");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '02'
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("B"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("MOTHRINLAW");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'MOTHRINLAW'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("24");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '24'
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("C"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("D"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STPDAUGHTR");                                                                   //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STPDAUGHTR'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("29");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '29'
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("E"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEPFATHR");                                                                    //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEPFATHR'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("26");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '26'
            }                                                                                                                                                             //Natural: VALUE 'F'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("F"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEPMOTHR");                                                                    //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEPMOTHR'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("27");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '27'
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("G"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("STEPSON");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'STEPSON'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("28");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '28'
            }                                                                                                                                                             //Natural: VALUE 'H'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("H"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("I"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SON");                                                                          //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SON'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("09");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '09'
            }                                                                                                                                                             //Natural: VALUE 'J'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("J"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("UNCLE");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'UNCLE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("14");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '14'
            }                                                                                                                                                             //Natural: VALUE 'K'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("K"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("L"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("M"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("N"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'O'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("O"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("ESTATE");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'ESTATE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("35");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '35'
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("P"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'Q'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("Q"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("TRUST");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'TRUST'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("36");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '36'
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("R"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals("S"))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: VALUE ' '
            else if (condition((ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(pnd_I).equals(" "))))
            {
                decideConditionsMet4581++;
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("SPOUSE");                                                                       //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'SPOUSE'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("31");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '31'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue(pnd_I).setValue("OTHER");                                                                        //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP ( #I ) := 'OTHER'
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue(pnd_I).setValue("40");                                                                      //Natural: ASSIGN SCIA1200.#BENE-RELATIONSHIP-CODE ( #I ) := '40'
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  SRK KG
        }                                                                                                                                                                 //Natural: END-IF
        //*  SCIA1200.#MIT-UNIT := 'TMOMN'  /* TEAM CODE FOR SUNGARD
    }
    //*  R4 CRS 12/08/04
    private void sub_Check_Defaults() throws Exception                                                                                                                    //Natural: CHECK-DEFAULTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //* *  SET DEFAULTS WHEN INCOMPLETE DATA OR COMPANION REQUEST WERE SENT  **
        if (condition(ldaScil9000.getScil9000_Pnd_Incompl_Data_Ind().equals("I") || ldaScil9000.getScil9000_Pnd_Companion_Ind().equals("Y")))                             //Natural: IF SCIL9000.#INCOMPL-DATA-IND = 'I' OR SCIL9000.#COMPANION-IND = 'Y'
        {
            //*  PH627
            if (condition(ldaScil9000.getScil9000_Pnd_Incompl_Data_Ind().equals("I")))                                                                                    //Natural: IF SCIL9000.#INCOMPL-DATA-IND = 'I'
            {
                pnd_T813_Record_Pnd_T813_Update_Incompl_Data_Ind.setValue("Y");                                                                                           //Natural: ASSIGN #T813-UPDATE-INCOMPL-DATA-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  PH625
            if (condition(ldaScil9000.getScil9000_Pnd_Divorce_Ind().equals(" ")))                                                                                         //Natural: IF SCIL9000.#DIVORCE-IND = ' '
            {
                ldaScil9000.getScil9000_Pnd_Divorce_Ind().setValue("N");                                                                                                  //Natural: ASSIGN SCIL9000.#DIVORCE-IND := 'N'
                pnd_T813_Record_Pnd_T813_Update_Divorce.setValue("Y");                                                                                                    //Natural: ASSIGN #T813-UPDATE-DIVORCE := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  PH629
            if (condition(ldaScil9000.getScil9000_Pnd_Additional_Cref_Rqst().equals(" ")))                                                                                //Natural: IF SCIL9000.#ADDITIONAL-CREF-RQST = ' '
            {
                ldaScil9000.getScil9000_Pnd_Additional_Cref_Rqst().setValue("N");                                                                                         //Natural: ASSIGN SCIL9000.#ADDITIONAL-CREF-RQST := 'N'
                pnd_T813_Record_Pnd_T813_Update_Add_Cref.setValue("Y");                                                                                                   //Natural: ASSIGN #T813-UPDATE-ADD-CREF := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  PH636
            if (condition(ldaScil9000.getScil9000_Pnd_Pull_Code().equals(" ")))                                                                                           //Natural: IF SCIL9000.#PULL-CODE = ' '
            {
                ldaScil9000.getScil9000_Pnd_Pull_Code().setValue("N");                                                                                                    //Natural: ASSIGN SCIL9000.#PULL-CODE := 'N'
                pnd_T813_Record_Pnd_T813_Update_Pull_Code.setValue("Y");                                                                                                  //Natural: ASSIGN #T813-UPDATE-PULL-CODE := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  PH640
            if (condition(ldaScil9000.getScil9000_Pnd_Alloc_Model_Ind().equals(" ")))                                                                                     //Natural: IF SCIL9000.#ALLOC-MODEL-IND = ' '
            {
                ldaScil9000.getScil9000_Pnd_Alloc_Model_Ind().setValue("N");                                                                                              //Natural: ASSIGN SCIL9000.#ALLOC-MODEL-IND := 'N'
                pnd_T813_Record_Pnd_T813_Update_Alloc_Model_Ind.setValue("Y");                                                                                            //Natural: ASSIGN #T813-UPDATE-ALLOC-MODEL-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  PH628
            if (condition(ldaScil9000.getScil9000_Pnd_Ica_Rcvd_Ind().equals(" ")))                                                                                        //Natural: IF SCIL9000.#ICA-RCVD-IND = ' '
            {
                ldaScil9000.getScil9000_Pnd_Ica_Rcvd_Ind().setValue("N");                                                                                                 //Natural: ASSIGN SCIL9000.#ICA-RCVD-IND := 'N'
                pnd_T813_Record_Pnd_T813_Update_Cai_Ind.setValue("Y");                                                                                                    //Natural: ASSIGN #T813-UPDATE-CAI-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            //*  PH773
            if (condition(ldaScil9000.getScil9000_Pnd_Date_App_Recvd().equals("00000000") || ldaScil9000.getScil9000_Pnd_Date_App_Recvd().equals("        ")))            //Natural: IF SCIL9000.#DATE-APP-RECVD = '00000000' OR SCIL9000.#DATE-APP-RECVD = '        '
            {
                pnd_T813_Record_Pnd_T813_Update_App_Rcvd.setValue("Y");                                                                                                   //Natural: ASSIGN #T813-UPDATE-APP-RCVD := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  RS1
        //*  PH773
        if (condition(ldaScil9000.getScil9000_Pnd_Eir_From_T114_Ind().equals("Y")))                                                                                       //Natural: IF SCIL9000.#EIR-FROM-T114-IND = 'Y'
        {
            pnd_T813_Record_Pnd_T813_Update_App_Rcvd.setValue("Y");                                                                                                       //Natural: ASSIGN #T813-UPDATE-APP-RCVD := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        //*  LS1
        //*  LS1
        //*  RS1
        if (condition((((ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("1") || ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("2"))              //Natural: IF ( SCIL9000.#OMNI-ACCOUNT-ISSUANCE = '1' OR = '2' ) AND ( SCIL9000.#ENROLLMENT-METHOD NOT = 'A' ) AND ( SCIL9000.#EIR-STATUS = '03' OR SCIL9000.#EIR-STATUS = '04' )
            && ldaScil9000.getScil9000_Pnd_Enrollment_Method().notEquals("A")) && (ldaScil9000.getScil9000_Pnd_Eir_Status().equals("03") || ldaScil9000.getScil9000_Pnd_Eir_Status().equals("04")))))
        {
            pnd_T813_Record_Pnd_T813_Eir_Ind.setValue("Y");                                                                                                               //Natural: ASSIGN #T813-EIR-IND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_T813_Record_Pnd_T813_Eir_Ind.setValue(" ");                                                                                                               //Natural: ASSIGN #T813-EIR-IND := ' '
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_New_Contract() throws Exception                                                                                                               //Natural: CREATE-NEW-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pi_Reject_Flag.setValue("N");                                                                                                                                 //Natural: ASSIGN #PI-REJECT-FLAG := 'N'
        //*   /* START TNG
        //*  CHECK IF CONTRACT ALREADY EXIST
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-CONTRACT-EXISTS
        sub_Check_If_Contract_Exists();
        if (condition(Global.isEscape())) {return;}
        //*  ISSUED CONTRACT ALREADY EXISTS.
        if (condition(pnd_Contract_Exists.equals("Y") && pdaScia1200.getScia1200_Pnd_Companion_Ind().notEquals("Y")))                                                     //Natural: IF #CONTRACT-EXISTS = 'Y' AND SCIA1200.#COMPANION-IND NE 'Y'
        {
            pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                            //Natural: ADD 1 TO #TOT-REJECT-CNT
            pdaScia1200.getScia1200_Pnd_Return_Code().setValue(9005);                                                                                                     //Natural: ASSIGN SCIA1200.#RETURN-CODE := 9005
            pdaScia1200.getScia1200_Pnd_Return_Msg().setValue("ISSUED CONTRACT ALREADY EXISTS.");                                                                         //Natural: ASSIGN SCIA1200.#RETURN-MSG := 'ISSUED CONTRACT ALREADY EXISTS.'
            //*  WRITE ACIS REJECT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
            sub_Write_Error_Rec();
            if (condition(Global.isEscape())) {return;}
            //*  TNGSUB
            pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                                           //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
            //*  BJD ACISRJCT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
            sub_Write_Reject_To_Pi();
            if (condition(Global.isEscape())) {return;}
            if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                                         //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
            {
                pnd_Pi_Reject_Flag.setValue("Y");                                                                                                                         //Natural: ASSIGN #PI-REJECT-FLAG := 'Y'
                pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                                      //Natural: ADD 1 TO #PI-REJECT-CNT
                //*  PI TASK STATUS = 'F'
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                sub_Write_Pi_Interface_Rec();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ERROR-REC
                sub_Write_Pi_Error_Rec();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*      /* END TNG
        //*   PH809 - INDX-GUAR-RATE-IND IS POPULATED ONLY FOR ISIRA  /*     ONEIRA
        //* * (SUBSTR(SCIL9000.#SUB-PLAN-NO,1,3) = 'IR1' OR= 'IR2' OR= 'IR8') AND
        pnd_Num_Contracts.setValue(1);                                                                                                                                    //Natural: ASSIGN #NUM-CONTRACTS = 1
        //*  ONEIRA
        if (condition(ldaScil9000.getScil9000_Pnd_Plan_No().getSubstring(1,3).equals("IRA")))                                                                             //Natural: IF SUBSTR ( SCIL9000.#PLAN-NO,1,3 ) = 'IRA'
        {
            //* ONEIRA
            if (condition((((((ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA101") || ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA201")) || ldaScil9000.getScil9000_Pnd_Plan_No().equals("IRA801"))  //Natural: IF ( SCIL9000.#PLAN-NO = 'IRA101' OR = 'IRA201' OR = 'IRA801' ) AND SCIL9000.#INDX-GUAR-RATE-IND NE 'Y' AND #CURRENT-BUSINESS-DATE GT #IRA-SUB-GLOBAL-START-DATE AND #IRA-SUB-GLOBAL-START-DATE GT 0
                && ldaScil9000.getScil9000_Pnd_Indx_Guar_Rate_Ind().notEquals("Y")) && pnd_Current_Business_Date.greater(pnd_Ira_Sub_Global_Start_Date)) 
                && pnd_Ira_Sub_Global_Start_Date.greater(getZero()))))
            {
                //*  (BDP)
                pnd_Num_Contracts.setValue(2);                                                                                                                            //Natural: ASSIGN #NUM-CONTRACTS = 2
                pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Expected_Cnt.nadd(1);                                                                                              //Natural: ASSIGN #TOT-INDEXED-EXPECTED-CNT := #TOT-INDEXED-EXPECTED-CNT + 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ONEIRA FORCE IDX
                setValueToSubstring("Y",pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_1(),4,1);                                                                                 //Natural: MOVE 'Y' TO SUBSTR ( SCIA1200.#SG-TEXT-UDF-1,4,1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Scin9913_Return_Code.reset();                                                                                                                                 //Natural: RESET #SCIN9913-RETURN-CODE #SCIN9913-RETURN-MSG
        pnd_Scin9913_Return_Msg.reset();
        FOR05:                                                                                                                                                            //Natural: FOR I = 1 TO #NUM-CONTRACTS
        for (i.setValue(1); condition(i.lessOrEqual(pnd_Num_Contracts)); i.nadd(1))
        {
            if (condition(i.equals(2)))                                                                                                                                   //Natural: IF I = 2
            {
                ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().setValue("O");                                                                                    //Natural: ASSIGN SCIL9000.#SUBSTITUTION-CONTRACT-IND := 'O'
                //*  FORCE IRA INDEX
                setValueToSubstring("Y",pdaScia1200.getScia1200_Pnd_Sg_Text_Udf_1(),4,1);                                                                                 //Natural: MOVE 'Y' TO SUBSTR ( SCIA1200.#SG-TEXT-UDF-1,4,1 )
                pdaScia1200.getScia1200_Pnd_Contract_Type().reset();                                                                                                      //Natural: RESET SCIA1200.#CONTRACT-TYPE SCIA1200.#LOB SCIA1200.#LOB-TYPE
                pdaScia1200.getScia1200_Pnd_Lob().reset();
                pdaScia1200.getScia1200_Pnd_Lob_Type().reset();
                pdaScia1200.getScia1200_Pnd_System_Requestor().setValue("ACIBATNE");                                                                                      //Natural: ASSIGN SCIA1200.#SYSTEM-REQUESTOR := 'ACIBATNE'
                pdaScia1200.getScia1200_Pnd_Enrollment_Type().setValue("N");                                                                                              //Natural: ASSIGN SCIA1200.#ENROLLMENT-TYPE := 'N'
                pdaScia1200.getScia1200_Pnd_Substitution_Contract_Ind().setValue("O");                                                                                    //Natural: ASSIGN SCIA1200.#SUBSTITUTION-CONTRACT-IND := 'O'
                pdaScia1200.getScia1200_Pnd_Conversion_Issue_State().setValue(pdaScia1200.getScia1200_Pnd_State());                                                       //Natural: ASSIGN SCIA1200.#CONVERSION-ISSUE-STATE := SCIA1200.#STATE
                pdaScia1200.getScia1200_Pnd_Deceased_Ind().setValue(" ");                                                                                                 //Natural: ASSIGN SCIA1200.#DECEASED-IND := ' '
                pdaScia1200.getScia1200_Pnd_Cip_Ind().setValue("N");                                                                                                      //Natural: ASSIGN SCIA1200.#CIP-IND := 'N'
                ldaScil9000.getScil9000_Pnd_Orig_Tiaa_Cntrct().setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                                  //Natural: ASSIGN SCIL9000.#ORIG-TIAA-CNTRCT := SCIA1200.#TIAA-CONTRACT-NO
                ldaScil9000.getScil9000_Pnd_Orig_Cref_Cntrct().setValue(pdaScia1200.getScia1200_Pnd_Cref_Certificate_No());                                               //Natural: ASSIGN SCIL9000.#ORIG-CREF-CNTRCT := SCIA1200.#CREF-CERTIFICATE-NO
                pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Tiaa_Contract.setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                            //Natural: ASSIGN #SCIA1200-ORIG-TIAA-CONTRACT := SCIA1200.#TIAA-CONTRACT-NO
                pnd_Scia1200_Field_3_Pnd_Scia1200_Orig_Cref_Contract.setValue(pdaScia1200.getScia1200_Pnd_Cref_Certificate_No());                                         //Natural: ASSIGN #SCIA1200-ORIG-CREF-CONTRACT := SCIA1200.#CREF-CERTIFICATE-NO
                pdaScia1200.getScia1200_Pnd_Field3().setValue(pnd_Scia1200_Field_3);                                                                                      //Natural: ASSIGN SCIA1200.#FIELD3 := #SCIA1200-FIELD-3
                //*  BNL2
                pdaScia1200.getScia1200_Pnd_Bene_Type().getValue("*").reset();                                                                                            //Natural: RESET SCIA1200.#BENE-TYPE ( * ) SCIA1200.#BENE-SSN ( * ) SCIA1200.#BENE-NAME ( * ) SCIA1200.#BENE-RELATIONSHIP ( * ) SCIA1200.#BENE-EXTENDED-NAME ( * ) SCIA1200.#BENE-RELATIONSHIP-CODE ( * ) SCIA1200.#BENE-DATE-OF-BIRTH ( * ) SCIA1200.#BENE-ALLOC-PCT ( * ) SCIA1200.#BENE-NUMERATOR ( * ) SCIA1200.#BENE-DENOMINATOR ( * ) SCIA1200.#BENE-SPECIAL-TEXT-IND ( * ) SCIA1200.#BENE-SPECIAL-TEXT ( *,* ) SCIA1200.#BENE-MOS-IND SCIA1200.#BENE-ESTATE SCIA1200.#BENE-TRUST SCIA1200.#BENE-CATEGORY SCIA1200.#TIAA-AGE-1ST-PYMNT SCIA1200.#CREF-AGE-1ST-PYMNT
                pdaScia1200.getScia1200_Pnd_Bene_Ssn().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Name().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Relationship().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Extended_Name().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Relationship_Code().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Date_Of_Birth().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Alloc_Pct().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Numerator().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Denominator().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Special_Text_Ind().getValue("*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Special_Text().getValue("*","*").reset();
                pdaScia1200.getScia1200_Pnd_Bene_Mos_Ind().reset();
                pdaScia1200.getScia1200_Pnd_Bene_Estate().reset();
                pdaScia1200.getScia1200_Pnd_Bene_Trust().reset();
                pdaScia1200.getScia1200_Pnd_Bene_Category().reset();
                pdaScia1200.getScia1200_Pnd_Tiaa_Age_1st_Pymnt().reset();
                pdaScia1200.getScia1200_Pnd_Cref_Age_1st_Pymnt().reset();
                //*  (TNGPROC)
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Scin1200.class , getCurrentProcessState(), pdaScia1200.getScia1200());                                                                        //Natural: CALLNAT 'SCIN1200' SCIA1200
            if (condition(Global.isEscape())) return;
            if (condition(pdaScia1200.getScia1200_Pnd_Return_Code().notEquals("0000")))                                                                                   //Natural: IF SCIA1200.#RETURN-CODE NE '0000'
            {
                //*  (BDP)
                if (condition(pnd_Num_Contracts.equals(2)))                                                                                                               //Natural: IF #NUM-CONTRACTS = 2
                {
                    if (condition(i.equals(1)))                                                                                                                           //Natural: IF I = 1
                    {
                        pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed.nadd(1);                                                                                     //Natural: ASSIGN #TOT-INDEXED-NOT-PROCESSED := #TOT-INDEXED-NOT-PROCESSED + 1
                        //*  (BDP)
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Not_Processed_2.nadd(1);                                                                                   //Natural: ASSIGN #TOT-INDEXED-NOT-PROCESSED-2 := #TOT-INDEXED-NOT-PROCESSED-2 + 1
                        //*  (BDP)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (BDP)
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals(" ")))                                                                       //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = ' '
                {
                    //*  RS0
                                                                                                                                                                          //Natural: PERFORM WRITE-REPROCESS-FILE
                    sub_Write_Reprocess_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-REJECT-CNT
                //*  WRITE ACIS REJECT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                sub_Write_Error_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  TNGSUB
                pnd_Pi_Type.setValue("ENRACISREJ");                                                                                                                       //Natural: ASSIGN #PI-TYPE = 'ENRACISREJ'
                //*  BJD ACISRJCT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
                sub_Write_Reject_To_Pi();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                                     //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
                {
                    pnd_Pi_Reject_Flag.setValue("Y");                                                                                                                     //Natural: ASSIGN #PI-REJECT-FLAG := 'Y'
                    pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                                  //Natural: ADD 1 TO #PI-REJECT-CNT
                    //*  PI TASK STATUS = 'F'
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                    sub_Write_Pi_Interface_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ERROR-REC
                    sub_Write_Pi_Error_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *****  START OF CHANGE BJD ACISRJCT     *******
                }                                                                                                                                                         //Natural: END-IF
                //* *****  START OF CHANGE 08-28-2007 AB    *******
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  (BDP)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  (BDP)
                if (condition(pnd_Num_Contracts.equals(2)))                                                                                                               //Natural: IF #NUM-CONTRACTS = 2
                {
                    //*  (BDP)
                    if (condition(i.equals(2)))                                                                                                                           //Natural: IF I = 2
                    {
                        pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Processed_Cnt.nadd(1);                                                                                     //Natural: ASSIGN #TOT-INDEXED-PROCESSED-CNT := #TOT-INDEXED-PROCESSED-CNT + 1
                        //*  (BDP)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (BDP)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  LS1
            //*  LS1
            //*  RS1,BYPASS INVESTED IND
            if (condition((((ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("1") || ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("2"))          //Natural: IF ( SCIL9000.#OMNI-ACCOUNT-ISSUANCE = '1' OR = '2' ) AND ( SCIL9000.#ENROLLMENT-METHOD NOT = 'A' ) AND ( SCIL9000.#EIR-STATUS = '03' OR SCIL9000.#EIR-STATUS = '04' ) THEN
                && ldaScil9000.getScil9000_Pnd_Enrollment_Method().notEquals("A")) && (ldaScil9000.getScil9000_Pnd_Eir_Status().equals("03") || ldaScil9000.getScil9000_Pnd_Eir_Status().equals("04")))))
            {
                ignore();
                //*  RS1 END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  EASEENR
                //*  EASEENR
                if (condition(ldaScil9000.getScil9000_Pnd_Money_Invested_Ind().equals("Y") || ldaScil9000.getScil9000_Pnd_Enrollment_Method().equals("M")))               //Natural: IF SCIL9000.#MONEY-INVESTED-IND = 'Y' OR SCIL9000.#ENROLLMENT-METHOD = 'M'
                {
                    //*     IF SCIL9000.#PLAN-NO = 'IRA101' OR= 'IRA201' OR= 'IRA801' /* ONEIRA
                    //*  ONEIRA
                    //*  TNG
                    if (condition(ldaScil9000.getScil9000_Pnd_Plan_No().getSubstring(1,3).equals("IRA")))                                                                 //Natural: IF SUBSTR ( SCIL9000.#PLAN-NO,1,3 ) = 'IRA'
                    {
                        ignore();
                        //*  TNG
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaScia9003.getScia9003().reset();                                                                                                                //Natural: RESET SCIA9003
                        pdaScia9003.getScia9003_Pnd_Plan_No().setValue(pdaScia1200.getScia1200_Pnd_Sg_Plan_No());                                                         //Natural: ASSIGN SCIA9003.#PLAN-NO := SCIA1200.#SG-PLAN-NO
                        pdaScia9003.getScia9003_Pnd_Sub_Plan_No().setValue(pdaScia1200.getScia1200_Pnd_Sg_Subplan_No());                                                  //Natural: ASSIGN SCIA9003.#SUB-PLAN-NO := SCIA1200.#SG-SUBPLAN-NO
                        pdaScia9003.getScia9003_Pnd_Tiaa_Cntrct().setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                               //Natural: ASSIGN SCIA9003.#TIAA-CNTRCT := SCIA1200.#TIAA-CONTRACT-NO
                        pdaScia9003.getScia9003_Pnd_Ssn().setValue(pdaScia1200.getScia1200_Pnd_Ssn());                                                                    //Natural: ASSIGN SCIA9003.#SSN := SCIA1200.#SSN
                        //*  FIND PRAP RECORD AND M&R
                        DbsUtil.callnat(Scin9003.class , getCurrentProcessState(), pdaScia9003.getScia9003());                                                            //Natural: CALLNAT 'SCIN9003' SCIA9003
                        if (condition(Global.isEscape())) return;
                        //*  IF SCIA9003.#RETURN-CODE = '    '
                        //*  CHANGE PER CHARLES REQUEST
                        if (condition(pdaScia9003.getScia9003_Pnd_Return_Msg().notEquals("    ")))                                                                        //Natural: IF SCIA9003.#RETURN-MSG NE '    '
                        {
                            getReports().write(0, "ERROR IN SCIN9003 - CONTRACT:",pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                        //Natural: WRITE 'ERROR IN SCIN9003 - CONTRACT:'SCIA1200.#TIAA-CONTRACT-NO
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "RETURN CODE:",pdaScia9003.getScia9003_Pnd_Return_Code());                                                              //Natural: WRITE 'RETURN CODE:' SCIA9003.#RETURN-CODE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "RETURN MSG :",pdaScia9003.getScia9003_Pnd_Return_Msg());                                                               //Natural: WRITE 'RETURN MSG :' SCIA9003.#RETURN-MSG
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  TNG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  LS1
            //*  LS1
            //*  RS1
            if (condition((((ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("1") || ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("2"))          //Natural: IF ( SCIL9000.#OMNI-ACCOUNT-ISSUANCE = '1' OR = '2' ) AND ( SCIL9000.#ENROLLMENT-METHOD NOT = 'A' ) AND ( SCIL9000.#EIR-STATUS = '03' OR SCIL9000.#EIR-STATUS = '04' )
                && ldaScil9000.getScil9000_Pnd_Enrollment_Method().notEquals("A")) && (ldaScil9000.getScil9000_Pnd_Eir_Status().equals("03") || ldaScil9000.getScil9000_Pnd_Eir_Status().equals("04")))))
            {
                pnd_Summary_Report_Rec_Pnd_Tot_Eir_Cnt.nadd(1);                                                                                                           //Natural: ADD 1 TO #TOT-EIR-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-ACCEPT-CNT
            }                                                                                                                                                             //Natural: END-IF
            break_Counters_Pnd_Plan_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #PLAN-CNT
            if (condition(ldaScil9000.getScil9000_Pnd_Div_Sub().greater(" ")))                                                                                            //Natural: IF SCIL9000.#DIV-SUB GT ' '
            {
                break_Counters_Pnd_Divsub_Cnt.nadd(1);                                                                                                                    //Natural: ADD 1 TO #DIVSUB-CNT
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FORMAT-N-WRITE-INTERFACE-RECORDS
            sub_Format_N_Write_Interface_Records();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  START OF TNGSUB
            if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y")))                                                                           //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y'
            {
                getWorkFiles().write(7, false, pdaScia1200.getScia1200_Pnd_Pin_Nbr(), ldaScil9000.getScil9000_Pnd_Orig_Tiaa_Cntrct(), ldaScil9000.getScil9000_Pnd_Orig_Cref_Cntrct(),  //Natural: WRITE WORK FILE 7 SCIA1200.#PIN-NBR SCIL9000.#ORIG-TIAA-CNTRCT SCIL9000.#ORIG-CREF-CNTRCT SCIA1200.#TIAA-CONTRACT-NO SCIA1200.#CREF-CERTIFICATE-NO SCIA1200.#DATE-APP-RECVD
                    pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No(), pdaScia1200.getScia1200_Pnd_Cref_Certificate_No(), pdaScia1200.getScia1200_Pnd_Date_App_Recvd());
                //*  END OF TNGSUB
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-REC
            sub_Write_Report_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  INC3169566
            if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ") && i.equals(1)))                                                                          //Natural: IF SCIL9000.#PI-TASK-ID GT ' ' AND I = 1
            {
                pnd_Pi_Summary_Report_Rec_Pnd_Pi_Accept_Cnt.nadd(1);                                                                                                      //Natural: ADD 1 TO #PI-ACCEPT-CNT
                //*  PI TASK STATUS = 'P'
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                sub_Write_Pi_Interface_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-REPORT-REC
                sub_Write_Pi_Report_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(i.equals(1) && pnd_Num_Contracts.equals(2)))                                                                                                    //Natural: IF I = 1 AND #NUM-CONTRACTS = 2
            {
                //*  ESCAPE TOP/NO-BENE  /*PFIX1
                if (condition(ldaScil9000.getScil9000_Pnd_Decedent_Contract().greater(" ")))                                                                              //Natural: IF SCIL9000.#DECEDENT-CONTRACT GT ' '
                {
                    //* PFIX1
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //* PFIX1
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Scin9913.class , getCurrentProcessState(), pdaScia1200.getScia1200(), pnd_Scin9913_Return_Code, pnd_Scin9913_Return_Msg);                 //Natural: CALLNAT 'SCIN9913' SCIA1200 #SCIN9913-RETURN-CODE #SCIN9913-RETURN-MSG
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  (TNGPROC)
            if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("O")))                                                                           //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'O'
            {
                if (condition(pnd_Scin9913_Return_Code.equals(" ")))                                                                                                      //Natural: IF #SCIN9913-RETURN-CODE = ' '
                {
                                                                                                                                                                          //Natural: PERFORM IRA-SUB-BENEFICIARY-COPY
                    sub_Ira_Sub_Beneficiary_Copy();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  BADOTBLS
                    getReports().write(0, "=",bene_File_Bene_Return_Code);                                                                                                //Natural: WRITE '=' BENE-RETURN-CODE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      IF #O-RTRN-CD NE '0000'                                /* BADOTBLS
                    //*        ASSIGN SCIA1200.#RETURN-CODE = #O-RTRN-CD            /* BADOTBLS
                    //*        ASSIGN SCIA1200.#RETURN-MSG  = #O-RTRN-MSG           /* BADOTBLS
                    //*        ASSIGN #PI-TYPE = 'MNTBEN'                           /* BADOTBLS
                    //*        #PI-REJECT-FLAG  := 'Y'                              /* BADOTBLS
                    //*        ADD 1 TO #PI-REJECT-CNT                              /* BADOTBLS
                    //*        ADD 1 TO #TOT-REJECT-CNT                             /* BADOTBLS
                    //*        ADD 1 TO #TOT-MNTBEN-CNT                             /* BADOTBLS
                    //*        PERFORM WRITE-REJECT-TO-PI                           /* BADOTBLS
                    //*        PERFORM WRITE-ERROR-REC                              /* BADOTBLS
                    //*        IF SCIL9000.#PI-TASK-ID GT ' '                       /* BADOTBLS
                    //*          #PI-REJECT-FLAG  := 'Y'                            /* BADOTBLS
                    //*          ADD 1 TO #PI-REJECT-CNT                            /* BADOTBLS
                    //*          PERFORM WRITE-PI-INTERFACE-REC  /* PI TASK STATUS = 'F' (FAIL)
                    //*          PERFORM WRITE-PI-ERROR-REC                         /* BADOTBLS
                    //*        END-IF                                               /* BADOTBLS
                    //*        PERFORM WRITE-PI-INTERFACE-REC                            /* BDP
                    //*        PERFORM WRITE-PI-ERROR-REC                                /* BDP
                    //*      END-IF                                                 /* BADOTBLS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaScia1200.getScia1200_Pnd_Return_Code().setValue(pnd_Scin9913_Return_Code);                                                                         //Natural: ASSIGN SCIA1200.#RETURN-CODE = #SCIN9913-RETURN-CODE
                    pdaScia1200.getScia1200_Pnd_Return_Msg().setValue(pnd_Scin9913_Return_Msg);                                                                           //Natural: ASSIGN SCIA1200.#RETURN-MSG = #SCIN9913-RETURN-MSG
                    pnd_Pi_Type.setValue("MNTBEN");                                                                                                                       //Natural: ASSIGN #PI-TYPE = 'MNTBEN'
                    //*      #PI-REJECT-FLAG  := 'Y'                                     /* BDP
                    pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                                  //Natural: ADD 1 TO #PI-REJECT-CNT
                    pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                    //Natural: ADD 1 TO #TOT-REJECT-CNT
                    //*  BDP
                    pnd_Summary_Report_Rec_Pnd_Tot_Mntben_Cnt.nadd(1);                                                                                                    //Natural: ADD 1 TO #TOT-MNTBEN-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJECT-TO-PI
                    sub_Write_Reject_To_Pi();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                    sub_Write_Error_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  BDP >>>
                    if (condition(ldaScil9000.getScil9000_Pnd_Pi_Task_Id().greater(" ")))                                                                                 //Natural: IF SCIL9000.#PI-TASK-ID GT ' '
                    {
                        pnd_Pi_Reject_Flag.setValue("Y");                                                                                                                 //Natural: ASSIGN #PI-REJECT-FLAG := 'Y'
                        pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt.nadd(1);                                                                                              //Natural: ADD 1 TO #PI-REJECT-CNT
                        //*  PI TASK STATUS = 'F' (FAIL)
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-INTERFACE-REC
                        sub_Write_Pi_Interface_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ERROR-REC
                        sub_Write_Pi_Error_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  BDP <<<
                    }                                                                                                                                                     //Natural: END-IF
                    //*      PERFORM WRITE-PI-INTERFACE-REC                              /* BDP
                    //*      PERFORM WRITE-PI-ERROR-REC                                  /* BDP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  RS0
    private void sub_Write_Reprocess_File() throws Exception                                                                                                              //Natural: WRITE-REPROCESS-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //*  ADDED BY ELLO 7/7/2009 STARTS HERE...
        //*  IISG
        //*  IISG
        getWorkFiles().write(6, false, ldaScil9000.getScil9000_Pnd_Omni_Data_1(), ldaScil9000.getScil9000_Pnd_Omni_Data_2(), ldaScil9000.getScil9000_Pnd_Omni_Data_3(),   //Natural: WRITE WORK FILE 6 #OMNI-DATA-1 #OMNI-DATA-2 #OMNI-DATA-3 #OMNI-DATA-4 #OMNI-DATA-5 #OMNI-DATA-6 #OMNI-DATA-7 #OMNI-DATA-8 #OMNI-DATA-9 #OMNI-DATA-10 #OMNI-DATA-11 #OMNI-DATA-12 #OMNI-DATA-13 #OMNI-DATA-14 #OMNI-DATA-15 #OMNI-DATA-16 #OMNI-DATA-17 #OMNI-DATA-18 #OMNI-DATA-19 #OMNI-DATA-20 #OMNI-DATA-21 #OMNI-DATA-22 #OMNI-DATA-23 #OMNI-DATA-24 #OMNI-DATA-25 #OMNI-DATA-26 #OMNI-DATA-27 #OMNI-DATA-28 #OMNI-DATA-29 #OMNI-DATA-30 #OMNI-DATA-31 #OMNI-DATA-32 #OMNI-DATA-33 #OMNI-DATA-34 #OMNI-DATA-35 #OMNI-DATA-36 #OMNI-DATA-37 #OMNI-DATA-38 #OMNI-DATA-39 #OMNI-DATA-40
            ldaScil9000.getScil9000_Pnd_Omni_Data_4(), ldaScil9000.getScil9000_Pnd_Omni_Data_5(), ldaScil9000.getScil9000_Pnd_Omni_Data_6(), ldaScil9000.getScil9000_Pnd_Omni_Data_7(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_8(), ldaScil9000.getScil9000_Pnd_Omni_Data_9(), ldaScil9000.getScil9000_Pnd_Omni_Data_10(), ldaScil9000.getScil9000_Pnd_Omni_Data_11(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_12(), ldaScil9000.getScil9000_Pnd_Omni_Data_13(), ldaScil9000.getScil9000_Pnd_Omni_Data_14(), ldaScil9000.getScil9000_Pnd_Omni_Data_15(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_16(), ldaScil9000.getScil9000_Pnd_Omni_Data_17(), ldaScil9000.getScil9000_Pnd_Omni_Data_18(), ldaScil9000.getScil9000_Pnd_Omni_Data_19(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_20(), ldaScil9000.getScil9000_Pnd_Omni_Data_21(), ldaScil9000.getScil9000_Pnd_Omni_Data_22(), ldaScil9000.getScil9000_Pnd_Omni_Data_23(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_24(), ldaScil9000.getScil9000_Pnd_Omni_Data_25(), ldaScil9000.getScil9000_Pnd_Omni_Data_26(), ldaScil9000.getScil9000_Pnd_Omni_Data_27(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_28(), ldaScil9000.getScil9000_Pnd_Omni_Data_29(), ldaScil9000.getScil9000_Pnd_Omni_Data_30(), ldaScil9000.getScil9000_Pnd_Omni_Data_31(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_32(), ldaScil9000.getScil9000_Pnd_Omni_Data_33(), ldaScil9000.getScil9000_Pnd_Omni_Data_34(), ldaScil9000.getScil9000_Pnd_Omni_Data_35(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_36(), ldaScil9000.getScil9000_Pnd_Omni_Data_37(), ldaScil9000.getScil9000_Pnd_Omni_Data_38(), ldaScil9000.getScil9000_Pnd_Omni_Data_39(), 
            ldaScil9000.getScil9000_Pnd_Omni_Data_40());
        //*  ADDED BY ELLO 7/7/2009 ENDS HERE...
    }
    //*  SGRD (BE) 9/6/05
    private void sub_Check_If_Contract_Exists() throws Exception                                                                                                          //Natural: CHECK-IF-CONTRACT-EXISTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        DbsUtil.callnat(Scin9005.class , getCurrentProcessState(), pdaScia1200.getScia1200_Pnd_Sg_Plan_No(), pdaScia1200.getScia1200_Pnd_Sg_Subplan_No(),                 //Natural: CALLNAT 'SCIN9005' SCIA1200.#SG-PLAN-NO SCIA1200.#SG-SUBPLAN-NO SCIA1200.#SG-PART-EXT SCIA1200.#SSN SCIA1200.#SUBSTITUTION-CONTRACT-IND #CONTRACT-EXISTS
            pdaScia1200.getScia1200_Pnd_Sg_Part_Ext(), pdaScia1200.getScia1200_Pnd_Ssn(), pdaScia1200.getScia1200_Pnd_Substitution_Contract_Ind(), pnd_Contract_Exists);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Format_N_Write_Interface_Records() throws Exception                                                                                                  //Natural: FORMAT-N-WRITE-INTERFACE-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_T813_Record_Pnd_T813_Plan_No.setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                                                 //Natural: ASSIGN #T813-PLAN-NO := SCIL9000.#PLAN-NO
        //*  #T813-PIN-NO               := SCIA1200.#PIN-NBR          /* PINE >>
        if (condition(pdaScia1200.getScia1200_Pnd_Pin_Nbr().getSubstring(1,5).equals("00000")))                                                                           //Natural: IF SUBSTR ( SCIA1200.#PIN-NBR,1,5 ) EQ '00000'
        {
            pnd_T813_Record_Pnd_T813_Pin_No.setValue(pdaScia1200.getScia1200_Pnd_Pin_Nbr().getSubstring(6,7));                                                            //Natural: MOVE SUBSTR ( SCIA1200.#PIN-NBR,6,7 ) TO #T813-PIN-NO
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_T813_Record_Pnd_T813_Pin_No.setValue(pdaScia1200.getScia1200_Pnd_Pin_Nbr());                                                                              //Natural: ASSIGN #T813-PIN-NO := SCIA1200.#PIN-NBR
            //*  PINE <<
            //*  RS1
            //*  RS1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T813_Record_Pnd_T813_Ssn.setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                                         //Natural: ASSIGN #T813-SSN := SCIL9000.#SSN
        pnd_T813_Record_Pnd_T813_Subplan_No.setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No());                                                                          //Natural: ASSIGN #T813-SUBPLAN-NO := SCIL9000.#SUB-PLAN-NO
        pnd_T813_Record_Pnd_Orchestration_Id.setValue(ldaScil9000.getScil9000_Pnd_Orchestration_Id());                                                                    //Natural: MOVE SCIL9000.#ORCHESTRATION-ID TO #T813-RECORD.#ORCHESTRATION-ID
        //*  ONEIRA
        pnd_T813_Record_Pnd_T813_Oneira_Account.setValue(ldaScil9000.getScil9000_Pnd_Oneira_Acct_No());                                                                   //Natural: MOVE SCIL9000.#ONEIRA-ACCT-NO TO #T813-ONEIRA-ACCOUNT
        //*  LS1
        //*  LS1
        //*  RS1
        if (condition((((ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("1") || ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("2"))              //Natural: IF ( SCIL9000.#OMNI-ACCOUNT-ISSUANCE = '1' OR = '2' ) AND ( SCIL9000.#ENROLLMENT-METHOD NOT = 'A' ) AND ( SCIL9000.#EIR-STATUS = '03' OR SCIL9000.#EIR-STATUS = '04' )
            && ldaScil9000.getScil9000_Pnd_Enrollment_Method().notEquals("A")) && (ldaScil9000.getScil9000_Pnd_Eir_Status().equals("03") || ldaScil9000.getScil9000_Pnd_Eir_Status().equals("04")))))
        {
            //*  WORK FILE TO BE PROCESSED BY SCIP9001
            getWorkFiles().write(2, false, pnd_T813_Record);                                                                                                              //Natural: WRITE WORK FILE 2 #T813-RECORD
            //*                                /* TO CREATE T813 CARDS FOR OMNI
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T813_Record_Pnd_T813_Divsub.setValue(ldaScil9000.getScil9000_Pnd_Div_Sub());                                                                                  //Natural: ASSIGN #T813-DIVSUB := SCIL9000.#DIV-SUB
        pnd_T813_Record_Pnd_T813_Tiaa_Contract.setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                                                  //Natural: ASSIGN #T813-TIAA-CONTRACT := SCIA1200.#TIAA-CONTRACT-NO
        pnd_T813_Record_Pnd_T813_Cref_Contract.setValue(pdaScia1200.getScia1200_Pnd_Cref_Certificate_No());                                                               //Natural: ASSIGN #T813-CREF-CONTRACT := SCIA1200.#CREF-CERTIFICATE-NO
        pnd_T813_Record_Pnd_T813_Register_Id.setValue(pdaScia1200.getScia1200_Pnd_Sg_Register_Id());                                                                      //Natural: ASSIGN #T813-REGISTER-ID := SCIA1200.#SG-REGISTER-ID
        pnd_T813_Record_Pnd_T813_Cref_Issue_Date.reset();                                                                                                                 //Natural: RESET #T813-CREF-ISSUE-DATE #HOLD-ISSUE-DATE
        pnd_Hold_Issue_Date.reset();
        if (condition(ldaScil9000.getScil9000_Pnd_Oia_Ind().equals("10") && ! (ldaScil9000.getScil9000_Pnd_Companion_Ind().equals("Y"))))                                 //Natural: IF SCIL9000.#OIA-IND = '10' AND NOT SCIL9000.#COMPANION-IND = 'Y'
        {
            pnd_T813_Record_Pnd_T813_Cref_Issue_Date.reset();                                                                                                             //Natural: RESET #T813-CREF-ISSUE-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  TNGSUB
            if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals(" ")))                                                                           //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = ' '
            {
                //*  CRS 10/05
                if (condition(ldaScil9000.getScil9000_Pnd_Cref_Date_Of_Issue().equals(" ") || ldaScil9000.getScil9000_Pnd_Cref_Date_Of_Issue().equals("00000000")))       //Natural: IF SCIL9000.#CREF-DATE-OF-ISSUE = ' ' OR = '00000000'
                {
                    pnd_Hold_Issue_Date.reset();                                                                                                                          //Natural: RESET #HOLD-ISSUE-DATE
                    pnd_Hold_Issue_Date.setValue(pdaScia1200.getScia1200_Pnd_Cref_Date_Of_Issue());                                                                       //Natural: ASSIGN #HOLD-ISSUE-DATE := SCIA1200.#CREF-DATE-OF-ISSUE
                    pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Dd.setValue("01");                                                                                            //Natural: ASSIGN #HOLD-ISSUE-DATE-DD := '01'
                    pnd_T813_Record_Pnd_T813_Cref_Issue_Date.setValue(pnd_Hold_Issue_Date);                                                                               //Natural: ASSIGN #T813-CREF-ISSUE-DATE := #HOLD-ISSUE-DATE
                }                                                                                                                                                         //Natural: END-IF
                //*  TNGSUB
                //*  TNGSUB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Issue_Date.setValue(pdaScia1200.getScia1200_Pnd_Cref_Date_Of_Issue());                                                                           //Natural: ASSIGN #HOLD-ISSUE-DATE := SCIA1200.#CREF-DATE-OF-ISSUE
                //*  TNGSUB
                if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("O")))                                                                       //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'O'
                {
                    pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Dd.setValue("01");                                                                                            //Natural: ASSIGN #HOLD-ISSUE-DATE-DD := '01'
                    //*  TNGSUB
                }                                                                                                                                                         //Natural: END-IF
                pnd_T813_Record_Pnd_T813_Cref_Issue_Date.setValue(pnd_Hold_Issue_Date);                                                                                   //Natural: ASSIGN #T813-CREF-ISSUE-DATE := #HOLD-ISSUE-DATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date.reset();                                                                                                                 //Natural: RESET #T813-TIAA-ISSUE-DATE
        if (condition(ldaScil9000.getScil9000_Pnd_Oia_Ind().equals("11") && ! (ldaScil9000.getScil9000_Pnd_Companion_Ind().equals("Y"))))                                 //Natural: IF SCIL9000.#OIA-IND = '11' AND NOT SCIL9000.#COMPANION-IND = 'Y'
        {
            pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date.reset();                                                                                                             //Natural: RESET #T813-TIAA-ISSUE-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CRS 10/11/05
            if (condition(ldaScil9000.getScil9000_Pnd_Tiaa_Date_Of_Issue().equals(" ") || ldaScil9000.getScil9000_Pnd_Tiaa_Date_Of_Issue().equals("00000000")))           //Natural: IF SCIL9000.#TIAA-DATE-OF-ISSUE = ' ' OR = '00000000'
            {
                pnd_Hold_Issue_Date.reset();                                                                                                                              //Natural: RESET #HOLD-ISSUE-DATE
                pnd_Hold_Issue_Date.setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Date_Of_Issue());                                                                           //Natural: ASSIGN #HOLD-ISSUE-DATE := SCIA1200.#TIAA-DATE-OF-ISSUE
                //*  TNGSUB
                if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals(" ")))                                                                       //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = ' '
                {
                    pnd_Hold_Issue_Date_Pnd_Hold_Issue_Date_Dd.setValue("01");                                                                                            //Natural: ASSIGN #HOLD-ISSUE-DATE-DD := '01'
                    //*  TNGSUB
                }                                                                                                                                                         //Natural: END-IF
                pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date.setValue(pnd_Hold_Issue_Date);                                                                                   //Natural: ASSIGN #T813-TIAA-ISSUE-DATE := #HOLD-ISSUE-DATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  TNGSUB
        //*  TNGSUB
        if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y")))                                                                               //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y'
        {
            pnd_T813_Record_Pnd_T813_Tiaa_Issue_Date.setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Date_Of_Issue());                                                          //Natural: ASSIGN #T813-TIAA-ISSUE-DATE := SCIA1200.#TIAA-DATE-OF-ISSUE
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil9000.getScil9000_Pnd_Use_Bpel_Data().equals("Y")))                                                                                           //Natural: IF SCIL9000.#USE-BPEL-DATA = 'Y'
        {
            pnd_T813_Record_Pnd_T813_Update_Address.setValue("N");                                                                                                        //Natural: ASSIGN #T813-UPDATE-ADDRESS := 'N'
            //*  LS1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_T813_Record_Pnd_T813_Update_Address.setValue("Y");                                                                                                        //Natural: ASSIGN #T813-UPDATE-ADDRESS := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T813_Record_Pnd_T813_Issue_State.setValue(pnd_Calc_Issue_State);                                                                                              //Natural: ASSIGN #T813-ISSUE-STATE := #CALC-ISSUE-STATE
        pnd_T813_Record_Pnd_T813_Annty_Start_Date.setValue(pdaScia1200.getScia1200_Pnd_Annuity_Start_Date());                                                             //Natural: ASSIGN #T813-ANNTY-START-DATE := SCIA1200.#ANNUITY-START-DATE
        //*  TNGSUB
        //*  TNGSUB
        //*  TNGSUB
        if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().greater(" ") && DbsUtil.maskMatches(ldaScil9000.getScil9000_Pnd_Annuity_Start_Date(),"YYYYMMDD")  //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND GT ' ' AND SCIL9000.#ANNUITY-START-DATE = MASK ( YYYYMMDD ) AND NOT #USE-ACIS-ANNUITY-START-DATE
            && ! (pnd_Use_Acis_Annuity_Start_Date.getBoolean())))
        {
            //*  TNGSUB
            pnd_T813_Record_Pnd_T813_Annty_Start_Date.reset();                                                                                                            //Natural: RESET #T813-ANNTY-START-DATE
            //*  TNGSUB
            //*  CRS 6/22/05
            //*  REJECT KG
            //*  REJECT KG
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T813_Record_Pnd_T813_App_Received_Date.setValue(pdaScia1200.getScia1200_Pnd_Date_App_Recvd());                                                                //Natural: ASSIGN #T813-APP-RECEIVED-DATE := SCIA1200.#DATE-APP-RECVD
        pnd_T813_Record_Pnd_T813_Mail_Address_Line.getValue(1).setValue(pdaScia1200.getScia1200_Pnd_Address_Line().getValue(1));                                          //Natural: ASSIGN #T813-MAIL-ADDRESS-LINE ( 1 ) := SCIA1200.#ADDRESS-LINE ( 1 )
        pnd_T813_Record_Pnd_T813_Mail_Address_Line.getValue(2).setValue(pdaScia1200.getScia1200_Pnd_Address_Line().getValue(2));                                          //Natural: ASSIGN #T813-MAIL-ADDRESS-LINE ( 2 ) := SCIA1200.#ADDRESS-LINE ( 2 )
        pnd_T813_Record_Pnd_T813_Mail_Address_Line.getValue(3).setValue(pdaScia1200.getScia1200_Pnd_Address_Line().getValue(3));                                          //Natural: ASSIGN #T813-MAIL-ADDRESS-LINE ( 3 ) := SCIA1200.#ADDRESS-LINE ( 3 )
        pnd_T813_Record_Pnd_T813_Mail_City.setValue(pdaScia1200.getScia1200_Pnd_City());                                                                                  //Natural: ASSIGN #T813-MAIL-CITY := SCIA1200.#CITY
        pnd_T813_Record_Pnd_T813_Mail_State.setValue(pdaScia1200.getScia1200_Pnd_State());                                                                                //Natural: ASSIGN #T813-MAIL-STATE := SCIA1200.#STATE
        pnd_T813_Record_Pnd_T813_Mail_Zip.setValue(pdaScia1200.getScia1200_Pnd_Zip_Code());                                                                               //Natural: ASSIGN #T813-MAIL-ZIP := SCIA1200.#ZIP-CODE
        pnd_T813_Record_Pnd_T813_Trade_Date.setValue(Global.getDATN());                                                                                                   //Natural: ASSIGN #T813-TRADE-DATE := *DATN
        pnd_T813_Record_Pnd_T813_Enrollment_Type.setValue(pdaScia1200.getScia1200_Pnd_Enrollment_Type());                                                                 //Natural: ASSIGN #T813-ENROLLMENT-TYPE := SCIA1200.#ENROLLMENT-TYPE
        pnd_T813_Record_Pnd_T813_Vesting.setValue(pdaScia1200.getScia1200_Pnd_Vesting_Code());                                                                            //Natural: ASSIGN #T813-VESTING := SCIA1200.#VESTING-CODE
        pnd_T813_Record_Pnd_Orchestration_Id.setValue(ldaScil9000.getScil9000_Pnd_Orchestration_Id());                                                                    //Natural: MOVE SCIL9000.#ORCHESTRATION-ID TO #T813-RECORD.#ORCHESTRATION-ID
        //*   TNGSUB
        pnd_T813_Record_Pnd_T813_Substitution_Contract_Ind.setValue(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind());                                             //Natural: MOVE SCIL9000.#SUBSTITUTION-CONTRACT-IND TO #T813-SUBSTITUTION-CONTRACT-IND
        //*   TNGSUB
        pnd_T813_Record_Pnd_T813_Deceased_Ind.setValue(ldaScil9000.getScil9000_Pnd_Deceased_Ind());                                                                       //Natural: MOVE SCIL9000.#DECEASED-IND TO #T813-DECEASED-IND
        //*  WORK FILE TO BE PROCESSED BY SCIP9001
        getWorkFiles().write(2, false, pnd_T813_Record);                                                                                                                  //Natural: WRITE WORK FILE 2 #T813-RECORD
        //*                            /* TO CREATE T813 CARDS FOR OMNI
    }
    private void sub_Write_Pi_Interface_Rec() throws Exception                                                                                                            //Natural: WRITE-PI-INTERFACE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Pi_Interface_Rec.reset();                                                                                                                                     //Natural: RESET #PI-INTERFACE-REC
        pnd_Pi_Interface_Rec_Pnd_Task_Id.setValue(ldaScil9000.getScil9000_Pnd_Pi_Task_Id());                                                                              //Natural: ASSIGN #TASK-ID := SCIL9000.#PI-TASK-ID
        if (condition(pnd_Pi_Reject_Flag.equals("N")))                                                                                                                    //Natural: IF #PI-REJECT-FLAG = 'N'
        {
            pnd_Pi_Interface_Rec_Pnd_Pass_Fail_Ind.setValue("P");                                                                                                         //Natural: ASSIGN #PASS-FAIL-IND := 'P'
            getWorkFiles().write(3, false, pnd_Pi_Interface_Rec);                                                                                                         //Natural: WRITE WORK FILE 3 #PI-INTERFACE-REC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pi_Interface_Rec_Pnd_Pass_Fail_Ind.setValue("F");                                                                                                         //Natural: ASSIGN #PASS-FAIL-IND := 'F'
            getWorkFiles().write(4, false, pnd_Pi_Interface_Rec);                                                                                                         //Natural: WRITE WORK FILE 4 #PI-INTERFACE-REC
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Reject_To_Pi() throws Exception                                                                                                                //Natural: WRITE-REJECT-TO-PI
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //*  WRITE ALL NON ONEIRA REJECT TO WORKFILE 5 FOR PI-EXPAG   /* PFIX-PI
        ldaScil9020.getPnd_Powerimage_File().reset();                                                                                                                     //Natural: RESET #POWERIMAGE-FILE
        //*  NON ONEIRA
        if (condition(ldaScil9000.getScil9000_Pnd_Oneira_Acct_No().equals(" ")))                                                                                          //Natural: IF SCIL9000.#ONEIRA-ACCT-NO = ' '
        {
            //*  PIEXPAG >>>
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Log_Task_Type_2().setValue(pnd_Pi_Type);                                                                            //Natural: ASSIGN #PI-LOG-TASK-TYPE-2 = #PI-TYPE
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No_2().setValue(pdaScia1200.getScia1200_Pnd_Sg_Plan_No());                                                //Natural: ASSIGN #PI-SGRD-PLAN-NO-2 = SCIA1200.#SG-PLAN-NO
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No_2().setValue(pdaScia1200.getScia1200_Pnd_Sg_Subplan_No());                                          //Natural: ASSIGN #PI-SGRD-SUBPLAN-NO-2 = SCIA1200.#SG-SUBPLAN-NO
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Ssn_2().setValue(pdaScia1200.getScia1200_Pnd_Ssn());                                                                //Natural: ASSIGN #PI-SSN-2 = SCIA1200.#SSN
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Omni_Folder_2().setValue(ldaScil9000.getScil9000_Pnd_Omni_Folder_Name());                                           //Natural: ASSIGN #PI-OMNI-FOLDER-2 = SCIL9000.#OMNI-FOLDER-NAME
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Divorce_Ind_2().setValue(ldaScil9000.getScil9000_Pnd_Divorce_Ind());                                                //Natural: ASSIGN #PI-DIVORCE-IND-2 = SCIL9000.#DIVORCE-IND
            if (condition(ldaScil9000.getScil9000_Pnd_Decedent_Contract().greater(" ")))                                                                                  //Natural: IF SCIL9000.#DECEDENT-CONTRACT > ' '
            {
                ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Beneinplan_Ind_2().setValue("Y");                                                                               //Natural: ASSIGN #PI-BENEINPLAN-IND-2 = 'Y'
            }                                                                                                                                                             //Natural: END-IF
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Enroll_Method_2().setValue(ldaScil9000.getScil9000_Pnd_Enrollment_Method());                                        //Natural: ASSIGN #PI-ENROLL-METHOD-2 = SCIL9000.#ENROLLMENT-METHOD
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_No_2().setValue(pdaScia1200.getScia1200_Pnd_Return_Code());                                              //Natural: ASSIGN #PI-ACIS-ERROR-NO-2 = SCIA1200.#RETURN-CODE
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2().setValue(pdaScia1200.getScia1200_Pnd_Return_Msg());                                              //Natural: ASSIGN #PI-ACIS-ERROR-MSG-2 = SCIA1200.#RETURN-MSG
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Orchestration_Id_2().setValue(ldaScil9000.getScil9000_Pnd_Orchestration_Id());                                      //Natural: ASSIGN #PI-ORCHESTRATION-ID-2 = SCIL9000.#ORCHESTRATION-ID
            if (condition(ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Orchestration_Id_2().greater(" ") && ldaScil9000.getScil9000_Pnd_Use_Bpel_Data().equals("Y")))        //Natural: IF #PI-ORCHESTRATION-ID-2 GT ' ' AND SCIL9000.#USE-BPEL-DATA = 'Y'
            {
                ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2().setValue(DbsUtil.compress("BPEL ERROR-", ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg_2())); //Natural: COMPRESS 'BPEL ERROR-' #PI-ACIS-ERROR-MSG-2 INTO #PI-ACIS-ERROR-MSG-2
                //*  END PIEXPAG
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(5, false, ldaScil9020.getPnd_Powerimage_File());                                                                                         //Natural: WRITE WORK FILE 5 #POWERIMAGE-FILE
            //*  PIEXPAG  <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE ALL REJECT TO WORKFILE 9 FOR BPEL                  /* PFIX-PI
        ldaScil9020.getPnd_Powerimage_File().reset();                                                                                                                     //Natural: RESET #POWERIMAGE-FILE
        //*  TNGSUB
        ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Log_Task_Type().setValue(pnd_Pi_Type);                                                                                  //Natural: ASSIGN #PI-LOG-TASK-TYPE = #PI-TYPE
        ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No().setValue(pdaScia1200.getScia1200_Pnd_Sg_Plan_No());                                                      //Natural: ASSIGN #PI-SGRD-PLAN-NO = SCIA1200.#SG-PLAN-NO
        ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No().setValue(pdaScia1200.getScia1200_Pnd_Sg_Subplan_No());                                                //Natural: ASSIGN #PI-SGRD-SUBPLAN-NO = SCIA1200.#SG-SUBPLAN-NO
        ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Ssn().setValue(pdaScia1200.getScia1200_Pnd_Ssn());                                                                      //Natural: ASSIGN #PI-SSN = SCIA1200.#SSN
        ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_No().setValue(pdaScia1200.getScia1200_Pnd_Return_Code());                                                    //Natural: ASSIGN #PI-ACIS-ERROR-NO = SCIA1200.#RETURN-CODE
        ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg().setValue(pdaScia1200.getScia1200_Pnd_Return_Msg());                                                    //Natural: ASSIGN #PI-ACIS-ERROR-MSG = SCIA1200.#RETURN-MSG
        //*  (SRK) CS
        ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Orchestration_Id().setValue(ldaScil9000.getScil9000_Pnd_Orchestration_Id());                                            //Natural: ASSIGN #PI-ORCHESTRATION-ID = SCIL9000.#ORCHESTRATION-ID
        if (condition(ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Orchestration_Id().greater(" ") && ldaScil9000.getScil9000_Pnd_Use_Bpel_Data().equals("Y")))              //Natural: IF #PI-ORCHESTRATION-ID GT ' ' AND SCIL9000.#USE-BPEL-DATA = 'Y'
        {
            ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg().setValue(DbsUtil.compress("BPEL ERROR-", ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg())); //Natural: COMPRESS 'BPEL ERROR-' #PI-ACIS-ERROR-MSG INTO #PI-ACIS-ERROR-MSG
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(9, false, ldaScil9020.getPnd_Powerimage_File());                                                                                             //Natural: WRITE WORK FILE 9 #POWERIMAGE-FILE
        //*  IF SCIL9000.#ONEIRA-ACCT-NO = ' '                    /* ONEIRA
        //*   WRITE WORK 5 #POWERIMAGE-FILE
        //*  ELSE
        //*   WRITE WORK 9 #POWERIMAGE-FILE
        //*  END-IF
        pnd_Pi_Cnt.nadd(+1);                                                                                                                                              //Natural: ADD +1 TO #PI-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-PI-ACIS-REJECTS-REPORT
        sub_Write_Pi_Acis_Rejects_Report();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Title_Lines1() throws Exception                                                                                                                //Natural: WRITE-TITLE-LINES1
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"OMNI Interface Accepted Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 25T 'OMNI Interface Accepted Report' 63T 'Run Date:' *DATX / 25T '      Accepted Records        ' 63T 'Run Time:' *TIMX
            TabSetting(25),"      Accepted Records        ",new TabSetting(63),"Run Time:",Global.getTIMX());
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2
                                                                                                                                                                          //Natural: PERFORM WRITE-COLUMN-HEADER1
        sub_Write_Column_Header1();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Title_Lines2() throws Exception                                                                                                                //Natural: WRITE-TITLE-LINES2
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"OMNI Interface Rejected Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 02 ) NOTITLE NOHDR 25T 'OMNI Interface Rejected Report' 63T 'Run Date:' *DATX / 25T '      Rejected Records        ' 63T 'Run Time:' *TIMX
            TabSetting(25),"      Rejected Records        ",new TabSetting(63),"Run Time:",Global.getTIMX());
        if (Global.isEscape()) return;
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
                                                                                                                                                                          //Natural: PERFORM WRITE-COLUMN-HEADER2
        sub_Write_Column_Header2();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Column_Header1() throws Exception                                                                                                              //Natural: WRITE-COLUMN-HEADER1
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"PLAN",new TabSetting(9),"DIV",new TabSetting(15),"PARTICIPANT NAME",new TabSetting(61),"SSN",new    //Natural: WRITE ( 1 ) NOTITLE 1T 'PLAN' 9T 'DIV' 15T 'PARTICIPANT NAME' 61T 'SSN' 70T 'TIAA CNTRCT' 82T 'CREF CNTRCT'
            TabSetting(70),"TIAA CNTRCT",new TabSetting(82),"CREF CNTRCT");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"------",new TabSetting(9),"----",new TabSetting(15),"----------------",new TabSetting(58),"-----------",new  //Natural: WRITE ( 1 ) NOTITLE 1T '------' 9T '----' 15T '----------------' 58T '-----------' 70T '-----------' 82T '-----------'
            TabSetting(70),"-----------",new TabSetting(82),"-----------");
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
    }
    private void sub_Write_Column_Header2() throws Exception                                                                                                              //Natural: WRITE-COLUMN-HEADER2
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"PLAN",new TabSetting(9),"DIV",new TabSetting(15),"SUB PLAN",new TabSetting(25),"PARTICIPANT NAME",new  //Natural: WRITE ( 2 ) NOTITLE 1T 'PLAN' 9T 'DIV' 15T 'SUB PLAN' 25T 'PARTICIPANT NAME' 74T 'SSN'
            TabSetting(74),"SSN");
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"------",new TabSetting(9),"---",new TabSetting(15),"--------",new TabSetting(25),"----------------",new  //Natural: WRITE ( 2 ) NOTITLE 1T '------' 9T '---' 15T '--------' 25T '----------------' 70T '-----------'
            TabSetting(70),"-----------");
        if (Global.isEscape()) return;
        getReports().skip(2, 1);                                                                                                                                          //Natural: SKIP ( 2 ) 1
    }
    private void sub_Write_Division_Totals() throws Exception                                                                                                             //Natural: WRITE-DIVISION-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"                                  ",NEWLINE);                                                       //Natural: WRITE ( 1 ) NOTITLE 1T '                                  ' /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),"TOTAL FOR DIV: ",pnd_Hold_Divsub," ",break_Counters_Pnd_Divsub_Cnt);                                //Natural: WRITE ( 1 ) NOTITLE 3T 'TOTAL FOR DIV: ' #HOLD-DIVSUB ' ' #DIVSUB-CNT
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
    }
    private void sub_Write_Plan_Totals() throws Exception                                                                                                                 //Natural: WRITE-PLAN-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),"TOTAL FOR PLAN:",pnd_Hold_Plan_No,break_Counters_Pnd_Plan_Cnt,NEWLINE);                             //Natural: WRITE ( 1 ) NOTITLE 3T 'TOTAL FOR PLAN:' #HOLD-PLAN-NO #PLAN-CNT /
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"-------------------------------");                                                                  //Natural: WRITE ( 1 ) NOTITLE 1T '-------------------------------'
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"                                                ");                                                 //Natural: WRITE ( 1 ) NOTITLE 1T '                                                '
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
    }
    private void sub_Write_Report_Rec() throws Exception                                                                                                                  //Natural: WRITE-REPORT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Accepted_Report_Rec.reset();                                                                                                                                  //Natural: RESET #ACCEPTED-REPORT-REC
        //*  LS1
        //*  LS1
        //*  RS1
        if (condition((((ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("1") || ldaScil9000.getScil9000_Pnd_Omni_Account_Issuance().equals("2"))              //Natural: IF ( SCIL9000.#OMNI-ACCOUNT-ISSUANCE = '1' OR = '2' ) AND ( SCIL9000.#ENROLLMENT-METHOD NOT = 'A' ) AND ( SCIL9000.#EIR-STATUS = '03' OR SCIL9000.#EIR-STATUS = '04' )
            && ldaScil9000.getScil9000_Pnd_Enrollment_Method().notEquals("A")) && (ldaScil9000.getScil9000_Pnd_Eir_Status().equals("03") || ldaScil9000.getScil9000_Pnd_Eir_Status().equals("04")))))
        {
            pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract.reset();                                                                                                       //Natural: RESET #REPT-TIAA-CONTRACT #REPT-CREF-CONTRACT
            pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract.reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract.setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                                      //Natural: ASSIGN #REPT-TIAA-CONTRACT := SCIA1200.#TIAA-CONTRACT-NO
            pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract.setValue(pdaScia1200.getScia1200_Pnd_Cref_Certificate_No());                                                   //Natural: ASSIGN #REPT-CREF-CONTRACT := SCIA1200.#CREF-CERTIFICATE-NO
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No.setValue(pdaScia1200.getScia1200_Pnd_Sg_Plan_No());                                                                      //Natural: ASSIGN #REPT-PLAN-NO := SCIA1200.#SG-PLAN-NO
        pnd_Accepted_Report_Rec_Pnd_Rept_Divsub.setValue(pdaScia1200.getScia1200_Pnd_Sg_Divsub());                                                                        //Natural: ASSIGN #REPT-DIVSUB := SCIA1200.#SG-DIVSUB
        pnd_Accepted_Report_Rec_Pnd_Rept_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia1200.getScia1200_Pnd_Last_Name(), ", ",                     //Natural: COMPRESS SCIA1200.#LAST-NAME ', ' SCIA1200.#FIRST-NAME INTO #REPT-NAME LEAVING NO
            pdaScia1200.getScia1200_Pnd_First_Name()));
        pnd_Accepted_Report_Rec_Pnd_Rept_Ssn.setValue(pdaScia1200.getScia1200_Pnd_Ssn());                                                                                 //Natural: ASSIGN #REPT-SSN := SCIA1200.#SSN
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No,new TabSetting(9),pnd_Accepted_Report_Rec_Pnd_Rept_Divsub,new  //Natural: WRITE ( 1 ) NOTITLE 1T #REPT-PLAN-NO 9T #REPT-DIVSUB 15T #REPT-NAME ( AL = 40 ) 58T #REPT-SSN ( EM = XXX-XX-XXXX ) 72T #REPT-TIAA-CONTRACT 83T #REPT-CREF-CONTRACT
            TabSetting(15),pnd_Accepted_Report_Rec_Pnd_Rept_Name, new AlphanumericLength (40),new TabSetting(58),pnd_Accepted_Report_Rec_Pnd_Rept_Ssn, new 
            ReportEditMask ("XXX-XX-XXXX"),new TabSetting(72),pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract,new TabSetting(83),pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract);
        if (Global.isEscape()) return;
        //*  TNGSUB
        if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y") || ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("O")))        //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y' OR = 'O'
        {
            //*  TNGSUB
            //*  TNGSUB
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(72),ldaScil9000.getScil9000_Pnd_Orig_Tiaa_Cntrct(),NEWLINE);                                        //Natural: WRITE ( 1 ) NOTITLE 72T SCIL9000.#ORIG-TIAA-CNTRCT /
            if (Global.isEscape()) return;
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  TNGSUB
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            //*  TNGSUB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLinesLeft(1).less(10)))                                                                                                          //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
    }
    private void sub_Write_Error_Rec() throws Exception                                                                                                                   //Natural: WRITE-ERROR-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Rejected_Report_Rec.reset();                                                                                                                                  //Natural: RESET #REJECTED-REPORT-REC
        //*  REPORT RESTART RECORD NOT PROCESSED
        if (condition(pnd_Skip_Restart_Record.getBoolean()))                                                                                                              //Natural: IF #SKIP-RESTART-RECORD
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaScil9000.getScil9000_Pnd_Last_Name(), ", ",                //Natural: COMPRESS SCIL9000.#LAST-NAME ', ' SCIL9000.#FIRST-NAME INTO #ERROR-NAME LEAVING NO
                ldaScil9000.getScil9000_Pnd_First_Name()));
            pnd_Rejected_Report_Rec_Pnd_Error_Ssn.setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                            //Natural: ASSIGN #ERROR-SSN := SCIL9000.#SSN
            pnd_Rejected_Report_Rec_Pnd_Error_Return_Code.setValue(pdaScia1200.getScia1200_Pnd_Return_Code());                                                            //Natural: ASSIGN #ERROR-RETURN-CODE := SCIA1200.#RETURN-CODE
            pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg.setValue("ERROR OCCURRED DURING PROCESSING - REASON UNKNOWN");                                                   //Natural: MOVE 'ERROR OCCURRED DURING PROCESSING - REASON UNKNOWN' TO #ERROR-RETURN-MSG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number.setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                                //Natural: ASSIGN #ERROR-PLAN-NUMBER := SCIL9000.#PLAN-NO
            pnd_Rejected_Report_Rec_Pnd_Error_Subplan.setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No());                                                                //Natural: ASSIGN #ERROR-SUBPLAN := SCIL9000.#SUB-PLAN-NO
            pnd_Rejected_Report_Rec_Pnd_Error_Div_Sub.setValue(ldaScil9000.getScil9000_Pnd_Div_Sub());                                                                    //Natural: ASSIGN #ERROR-DIV-SUB := SCIL9000.#DIV-SUB
            pnd_Rejected_Report_Rec_Pnd_Error_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaScia1200.getScia1200_Pnd_Last_Name(), ", ",                //Natural: COMPRESS SCIA1200.#LAST-NAME ', ' SCIA1200.#FIRST-NAME INTO #ERROR-NAME LEAVING NO
                pdaScia1200.getScia1200_Pnd_First_Name()));
            pnd_Rejected_Report_Rec_Pnd_Error_Ssn.setValue(pdaScia1200.getScia1200_Pnd_Ssn());                                                                            //Natural: ASSIGN #ERROR-SSN := SCIA1200.#SSN
            pnd_Rejected_Report_Rec_Pnd_Error_Return_Code.setValue(pdaScia1200.getScia1200_Pnd_Return_Code());                                                            //Natural: ASSIGN #ERROR-RETURN-CODE := SCIA1200.#RETURN-CODE
            pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg.setValue(pdaScia1200.getScia1200_Pnd_Return_Msg());                                                              //Natural: ASSIGN #ERROR-RETURN-MSG := SCIA1200.#RETURN-MSG
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Rejected_Report_Rec_Pnd_Error_Plan_Number,new TabSetting(9),pnd_Rejected_Report_Rec_Pnd_Error_Div_Sub,new  //Natural: WRITE ( 2 ) NOTITLE 1T #ERROR-PLAN-NUMBER 9T #ERROR-DIV-SUB 15T #ERROR-SUBPLAN 25T #ERROR-NAME ( AL = 40 ) 70T #ERROR-SSN ( EM = XXX-XX-XXXX ) / 1T 'Error Code: ' #ERROR-RETURN-CODE 20T 'Msg: ' #ERROR-RETURN-MSG ( AL = 50 ) / 1T '----------------------------------------'
            TabSetting(15),pnd_Rejected_Report_Rec_Pnd_Error_Subplan,new TabSetting(25),pnd_Rejected_Report_Rec_Pnd_Error_Name, new AlphanumericLength (40),new 
            TabSetting(70),pnd_Rejected_Report_Rec_Pnd_Error_Ssn, new ReportEditMask ("XXX-XX-XXXX"),NEWLINE,new TabSetting(1),"Error Code: ",pnd_Rejected_Report_Rec_Pnd_Error_Return_Code,new 
            TabSetting(20),"Msg: ",pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg, new AlphanumericLength (50),NEWLINE,new TabSetting(1),"----------------------------------------");
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(2).less(10)))                                                                                                          //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  BDP
        //*  RS1
        getReports().write(3, ReportOption.NOTITLE,"Total records read (new + reprocess)            :",pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt, new ReportEditMask       //Natural: WRITE ( 3 ) NOTITLE 'Total records read (new + reprocess)            :' #TOT-TRANS-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total records accepted                       (+):' #TOT-ACCEPT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total records rejected                       (+):' #TOT-REJECT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Index-Sub contracts generated          (-):' #TOT-INDEXED-PROCESSED-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Index-Sub contracts with Bene error    (-):' #TOT-MNTBEN-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / '----------- SUPPLEMENTAL INFORMATION ------------' / 'Total records reprocessed in previous run       :' #TOT-RECORDS-REPROCESSED ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Ineligible and Eligible not participation :' #TOT-EIR-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total ACIS Rejected POWER IMAGE records written :' #PI-CNT ( EM = ZZZ,ZZZ,ZZ9 ) /
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total records accepted                       (+):",pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total records rejected                       (+):",pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Index-Sub contracts generated          (-):",pnd_Summary_Report_Rec_Pnd_Tot_Indexed_Processed_Cnt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Index-Sub contracts with Bene error    (-):",pnd_Summary_Report_Rec_Pnd_Tot_Mntben_Cnt, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"----------- SUPPLEMENTAL INFORMATION ------------",NEWLINE,"Total records reprocessed in previous run       :",pnd_Summary_Report_Rec_Pnd_Tot_Records_Reprocessed, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Ineligible and Eligible not participation :",pnd_Summary_Report_Rec_Pnd_Tot_Eir_Cnt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total ACIS Rejected POWER IMAGE records written :",pnd_Pi_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Pi_Report_Rec() throws Exception                                                                                                               //Natural: WRITE-PI-REPORT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Pi_Report_Rec.reset();                                                                                                                                        //Natural: RESET #PI-REPORT-REC
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Plan_No.setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                                            //Natural: ASSIGN #PI-REPT-PLAN-NO := SCIL9000.#PLAN-NO
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Subplan_No.setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No());                                                                     //Natural: ASSIGN #PI-REPT-SUBPLAN-NO := SCIL9000.#SUB-PLAN-NO
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Ssn.setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                                    //Natural: ASSIGN #PI-REPT-SSN := SCIL9000.#SSN
        pnd_Pi_Report_Rec_Pnd_Pi_Rept_Task_Id.setValue(ldaScil9000.getScil9000_Pnd_Pi_Task_Id());                                                                         //Natural: ASSIGN #PI-REPT-TASK-ID := SCIL9000.#PI-TASK-ID
        getReports().write(4, ReportOption.NOTITLE,"PLAN NUMBER:",pnd_Pi_Report_Rec_Pnd_Pi_Rept_Plan_No,"SUBPLAN    :",pnd_Pi_Report_Rec_Pnd_Pi_Rept_Subplan_No,          //Natural: WRITE ( 4 ) NOTITLE 'PLAN NUMBER:' #PI-REPT-PLAN-NO 'SUBPLAN    :' #PI-REPT-SUBPLAN-NO / 'PART. SSN  :' #PI-REPT-SSN / 'TASK ID    :' #PI-REPT-TASK-ID //
            NEWLINE,"PART. SSN  :",pnd_Pi_Report_Rec_Pnd_Pi_Rept_Ssn,NEWLINE,"TASK ID    :",pnd_Pi_Report_Rec_Pnd_Pi_Rept_Task_Id,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(4).less(10)))                                                                                                          //Natural: NEWPAGE ( 4 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(4);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-PI-REPORT-REC
    }
    private void sub_Write_Pi_Error_Rec() throws Exception                                                                                                                //Natural: WRITE-PI-ERROR-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Pi_Error_Report_Rec.reset();                                                                                                                                  //Natural: RESET #PI-ERROR-REPORT-REC
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Plan_No.setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                                       //Natural: ASSIGN #PI-ERR-PLAN-NO := SCIL9000.#PLAN-NO
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Subplan_No.setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No());                                                                //Natural: ASSIGN #PI-ERR-SUBPLAN-NO := SCIL9000.#SUB-PLAN-NO
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Ssn.setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                               //Natural: ASSIGN #PI-ERR-SSN := SCIL9000.#SSN
        pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Task_Id.setValue(ldaScil9000.getScil9000_Pnd_Pi_Task_Id());                                                                    //Natural: ASSIGN #PI-ERR-TASK-ID := SCIL9000.#PI-TASK-ID
        getReports().write(5, ReportOption.NOTITLE,"PLAN NUMBER:",pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Plan_No,"SUBPLAN    :",pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Subplan_No, //Natural: WRITE ( 5 ) NOTITLE 'PLAN NUMBER:' #PI-ERR-PLAN-NO 'SUBPLAN    :' #PI-ERR-SUBPLAN-NO / 'PART. SSN  :' #PI-ERR-SSN / 'TASK ID    :' #PI-ERR-TASK-ID //
            NEWLINE,"PART. SSN  :",pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Ssn,NEWLINE,"TASK ID    :",pnd_Pi_Error_Report_Rec_Pnd_Pi_Err_Task_Id,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(5).less(10)))                                                                                                          //Natural: NEWPAGE ( 5 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(5);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-PI-ERROR-REPORT
    }
    private void sub_Write_Pi_Acis_Rejects_Report() throws Exception                                                                                                      //Natural: WRITE-PI-ACIS-REJECTS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Pi_Error_Report_Rec.reset();                                                                                                                                  //Natural: RESET #PI-ERROR-REPORT-REC
        getReports().write(7, ReportOption.NOTITLE,new TabSetting(1),"*                                           ",NEWLINE,new TabSetting(1),"PLAN NUMBER  :",ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Sgrd_Plan_No(),new  //Natural: WRITE ( 7 ) NOTITLE 1T '*                                           ' / 1T 'PLAN NUMBER  :' #PI-SGRD-PLAN-NO 45T 'TASK TYPE    :' #PI-LOG-TASK-TYPE / 1T 'SUBPLAN      :' #PI-SGRD-SUBPLAN-NO 45T 'PART. SSN    :' #PI-SSN / 1T 'ERROR NBR/MSG:' #PI-ACIS-ERROR-NO '/' #PI-ACIS-ERROR-MSG
            TabSetting(45),"TASK TYPE    :",ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Log_Task_Type(),NEWLINE,new TabSetting(1),"SUBPLAN      :",ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Sgrd_Subplan_No(),new 
            TabSetting(45),"PART. SSN    :",ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Ssn(),NEWLINE,new TabSetting(1),"ERROR NBR/MSG:",ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_No(),
            "/",ldaScil9020.getPnd_Powerimage_File_Pnd_Pi_Acis_Error_Msg());
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(7).less(10)))                                                                                                          //Natural: NEWPAGE ( 7 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(7);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-PI-ERROR-REPORT
    }
    private void sub_Write_Pi_Summary_Report() throws Exception                                                                                                           //Natural: WRITE-PI-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(6, ReportOption.NOTITLE,"TOTAL RECORDS PROCESSED  :",pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL RECORDS PASSED     :",pnd_Pi_Summary_Report_Rec_Pnd_Pi_Accept_Cnt,  //Natural: WRITE ( 6 ) NOTITLE 'TOTAL RECORDS PROCESSED  :' #TOT-TRANS-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'TOTAL RECORDS PASSED     :' #PI-ACCEPT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'TOTAL RECORDS FAILED     :' #PI-REJECT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'TOTAL ACIS REJECTED POWER IMAGE RECORDS:' #PI-CNT ( EM = ZZZ,ZZZ,ZZ9 ) /
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL RECORDS FAILED     :",pnd_Pi_Summary_Report_Rec_Pnd_Pi_Reject_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL ACIS REJECTED POWER IMAGE RECORDS:",pnd_Pi_Cnt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
        //* ******************* END CHANGE 08/31/2007 AB **************************
    }
    private void sub_Check_For_Restart() throws Exception                                                                                                                 //Natural: CHECK-FOR-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind.setValue("A");                                                                                                            //Natural: ASSIGN #RST-ACTVE-IND = 'A'
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                            //Natural: ASSIGN #RST-JOB-NME = *INIT-USER
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme.setValue(Global.getINIT_ID());                                                                                             //Natural: ASSIGN #RST-STEP-NME = *INIT-ID
        vw_batch_Restart.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) BATCH-RESTART WITH SP-ACTVE-JOB-STEP = #SP-ACTVE-JOB-STEP
        (
        "FIND_R",
        new Wc[] { new Wc("SP_ACTVE_JOB_STEP", "=", pnd_Sp_Actve_Job_Step, WcType.WITH) },
        1
        );
        FIND_R:
        while (condition(vw_batch_Restart.readNextRow("FIND_R", true)))
        {
            vw_batch_Restart.setIfNotFoundControlFlag(false);
            //*  CREATE A RECORD IF NONE EXISTS.
            if (condition(vw_batch_Restart.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                batch_Restart_Rst_Actve_Ind.setValue("A");                                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-ACTVE-IND := 'A'
                batch_Restart_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-JOB-NME := *INIT-USER
                batch_Restart_Rst_Jobstep_Nme.setValue(Global.getINIT_ID());                                                                                              //Natural: ASSIGN BATCH-RESTART.RST-JOBSTEP-NME := *INIT-ID
                batch_Restart_Rst_Pgm_Nme.setValue(Global.getPROGRAM());                                                                                                  //Natural: ASSIGN BATCH-RESTART.RST-PGM-NME := *PROGRAM
                batch_Restart_Rst_Curr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Curr_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Start_Dte.setValue(batch_Restart_Rst_Curr_Dte);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-DTE := BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Start_Tme.setValue(batch_Restart_Rst_Curr_Tme);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-TME := BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Frst_Run_Dte.setValue(batch_Restart_Rst_Start_Dte);                                                                                     //Natural: ASSIGN BATCH-RESTART.RST-FRST-RUN-DTE := BATCH-RESTART.RST-START-DTE
                pnd_Restart_Key_Pnd_Restart_Plan_No.setValue(" ");                                                                                                        //Natural: ASSIGN #RESTART-KEY.#RESTART-PLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Subplan_No.setValue(" ");                                                                                                     //Natural: ASSIGN #RESTART-KEY.#RESTART-SUBPLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Ssn.setValue(" ");                                                                                                            //Natural: ASSIGN #RESTART-KEY.#RESTART-SSN := ' '
                pnd_Restart_Key_Pnd_Restart_Ext.setValue(" ");                                                                                                            //Natural: ASSIGN #RESTART-KEY.#RESTART-EXT := ' '
                batch_Restart_Rst_Key.setValue(pnd_Restart_Key);                                                                                                          //Natural: ASSIGN BATCH-RESTART.RST-KEY := #RESTART-KEY
                STORE_REST:                                                                                                                                               //Natural: STORE BATCH-RESTART
                vw_batch_Restart.insertDBRow("STORE_REST");
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                           //Natural: ASSIGN #RESTART-ISN := *ISN ( STORE-REST. )
                getReports().write(0, "***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***");                                                                        //Natural: WRITE '***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*********************************************************");                                                                       //Natural: WRITE '*********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, NEWLINE,NEWLINE);                                                                                                                   //Natural: WRITE //
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break FIND_R;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FIND-R. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                               //Natural: ASSIGN #RESTART-ISN := *ISN ( FIND-R. )
            pnd_Restart_Key.setValue(batch_Restart_Rst_Key);                                                                                                              //Natural: ASSIGN #RESTART-KEY := BATCH-RESTART.RST-KEY
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Key,"***");                                                                                                 //Natural: WRITE '***' '=' BATCH-RESTART.RST-KEY '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Dte,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-DTE '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Tme,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Last_Rstrt_Tme,"***");                                                                                      //Natural: WRITE '***' '=' BATCH-RESTART.RST-LAST-RSTRT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",pnd_Restart_Isn,"***");                                                                                                       //Natural: WRITE '***' '=' #RESTART-ISN '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Restarted.setValue(true);                                                                                                                                 //Natural: ASSIGN #RESTARTED := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Update_Batch_Restart_Record() throws Exception                                                                                                       //Natural: UPDATE-BATCH-RESTART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        GET_U:                                                                                                                                                            //Natural: GET BATCH-RESTART #RESTART-ISN
        vw_batch_Restart.readByID(pnd_Restart_Isn.getLong(), "GET_U");
        pnd_Restart_Key_Pnd_Restart_Plan_No.setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                                              //Natural: ASSIGN #RESTART-KEY.#RESTART-PLAN-NO = SCIL9000.#PLAN-NO
        pnd_Restart_Key_Pnd_Restart_Subplan_No.setValue(ldaScil9000.getScil9000_Pnd_Sub_Plan_No());                                                                       //Natural: ASSIGN #RESTART-KEY.#RESTART-SUBPLAN-NO = SCIL9000.#SUB-PLAN-NO
        pnd_Restart_Key_Pnd_Restart_Ssn.setValue(ldaScil9000.getScil9000_Pnd_Ssn());                                                                                      //Natural: ASSIGN #RESTART-KEY.#RESTART-SSN = SCIL9000.#SSN
        pnd_Restart_Key_Pnd_Restart_Ext.setValue(ldaScil9000.getScil9000_Pnd_Sg_Part_Ext());                                                                              //Natural: ASSIGN #RESTART-KEY.#RESTART-EXT = SCIL9000.#SG-PART-EXT
        batch_Restart_Rst_Curr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-CURR-DTE
        batch_Restart_Rst_Curr_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                                       //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-CURR-TME
        batch_Restart_Rst_Start_Dte.setValue(batch_Restart_Rst_Curr_Dte);                                                                                                 //Natural: ASSIGN BATCH-RESTART.RST-START-DTE = BATCH-RESTART.RST-CURR-DTE
        batch_Restart_Rst_Start_Tme.setValue(batch_Restart_Rst_Curr_Tme);                                                                                                 //Natural: ASSIGN BATCH-RESTART.RST-START-TME = BATCH-RESTART.RST-CURR-TME
        batch_Restart_Rst_End_Dte.reset();                                                                                                                                //Natural: RESET BATCH-RESTART.RST-END-DTE BATCH-RESTART.RST-END-TME
        batch_Restart_Rst_End_Tme.reset();
        batch_Restart_Rst_Key.setValue(pnd_Restart_Key);                                                                                                                  //Natural: ASSIGN BATCH-RESTART.RST-KEY := #RESTART-KEY
        batch_Restart_Rst_Rstrt_Data_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN BATCH-RESTART.RST-RSTRT-DATA-IND := 'Y'
        vw_batch_Restart.updateDBRow("GET_U");                                                                                                                            //Natural: UPDATE ( GET-U. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Delete_Restart_Record() throws Exception                                                                                                             //Natural: DELETE-RESTART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //* ****************** UPDATE BATCH RESTART FOR NORMAL EOJ **************
        //*  THIS LOGIC DELETES THE BATCH RESTART RECORD, INDICATING THAT THE
        //*  JOB HAS BEEN RUN SUCCESSFULLY.
        //* *********************************************************************
        GET_R:                                                                                                                                                            //Natural: GET BATCH-RESTART #RESTART-ISN
        vw_batch_Restart.readByID(pnd_Restart_Isn.getLong(), "GET_R");
        vw_batch_Restart.deleteDBRow("GET_R");                                                                                                                            //Natural: DELETE ( GET-R. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_State_Approval_Check() throws Exception                                                                                                              //Natural: STATE-APPROVAL-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pdaScia2200.getScia2200_Pnd_S_Entry_Actve_Ind().setValue("Y");                                                                                                    //Natural: MOVE 'Y' TO SCIA2200.#S-ENTRY-ACTVE-IND
        pdaScia2200.getScia2200_Pnd_S_Entry_Table_Id_Nbr().setValue(100);                                                                                                 //Natural: MOVE 000100 TO SCIA2200.#S-ENTRY-TABLE-ID-NBR
        pdaScia2200.getScia2200_Pnd_S_Entry_Table_Sub_Id().setValue("PA");                                                                                                //Natural: MOVE 'PA' TO SCIA2200.#S-ENTRY-TABLE-SUB-ID
        //*   TNGSUB
        if (condition(ldaScil9000.getScil9000_Pnd_Substitution_Contract_Ind().equals("Y")))                                                                               //Natural: IF SCIL9000.#SUBSTITUTION-CONTRACT-IND = 'Y'
        {
            pdaScia2200.getScia2200_Pnd_S_State_Cde().setValue(ldaScil9000.getScil9000_Pnd_Conversion_Issue_State());                                                     //Natural: MOVE SCIL9000.#CONVERSION-ISSUE-STATE TO SCIA2200.#S-STATE-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia2200.getScia2200_Pnd_S_State_Cde().setValue(pnd_Calc_Issue_State);                                                                                     //Natural: MOVE #CALC-ISSUE-STATE TO SCIA2200.#S-STATE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia2200.getScia2200_Pnd_S_Product_Cde().compute(new ComputeParameters(false, pdaScia2200.getScia2200_Pnd_S_Product_Cde()), pdaScia1200.getScia1200_Pnd_Product_Code().val()); //Natural: ASSIGN SCIA2200.#S-PRODUCT-CDE = VAL ( SCIA1200.#PRODUCT-CODE )
        pdaScia2200.getScia2200_Pnd_Applctn_Rcvd_Date().setValue(pdaScia1200.getScia1200_Pnd_Date_App_Recvd());                                                           //Natural: MOVE SCIA1200.#DATE-APP-RECVD TO SCIA2200.#APPLCTN-RCVD-DATE
        DbsUtil.callnat(Scin2200.class , getCurrentProcessState(), pdaScia2200.getScia2200(), pnd_Debug);                                                                 //Natural: CALLNAT 'SCIN2200' SCIA2200 #DEBUG
        if (condition(Global.isEscape())) return;
    }
    //*  SEARCH BY TABLE-ID
    private void sub_Check_For_Vesting_Override() throws Exception                                                                                                        //Natural: CHECK-FOR-VESTING-OVERRIDE
    {
        if (BLNatReinput.isReinput()) return;

        pdaScia2100.getScia2100_Pnd_Search_Mode().setValue("AV");                                                                                                         //Natural: ASSIGN SCIA2100.#SEARCH-MODE := 'AV'
        pdaScia2100.getScia2100_Pnd_S_Av_Entry_Table_Id_Nbr().setValue(400);                                                                                              //Natural: ASSIGN SCIA2100.#S-AV-ENTRY-TABLE-ID-NBR := 000400
        pdaScia2100.getScia2100_Pnd_S_Av_Entry_Table_Sub_Id().setValue("PE");                                                                                             //Natural: ASSIGN SCIA2100.#S-AV-ENTRY-TABLE-SUB-ID := 'PE'
        pdaScia2100.getScia2100_Pnd_S_Av_Entry_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaScil9000.getScil9000_Pnd_Plan_No(), pnd_Hold_Product));  //Natural: COMPRESS SCIL9000.#PLAN-NO #HOLD-PRODUCT INTO SCIA2100.#S-AV-ENTRY-CDE LEAVING NO
        DbsUtil.callnat(Scin2100.class , getCurrentProcessState(), pdaScia2100.getScia2100(), pnd_Debug);                                                                 //Natural: CALLNAT 'SCIN2100' SCIA2100 #DEBUG
        if (condition(Global.isEscape())) return;
        if (condition(pdaScia2100.getScia2100_Pnd_Return_Code().equals(" ")))                                                                                             //Natural: IF SCIA2100.#RETURN-CODE = ' '
        {
            pnd_Exception_Found.setValue(true);                                                                                                                           //Natural: ASSIGN #EXCEPTION-FOUND := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Exception_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #EXCEPTION-FOUND := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  CHG256421
    private void sub_Load_Plans_Bene_Dflt() throws Exception                                                                                                              //Natural: LOAD-PLANS-BENE-DFLT
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Plan_Key_Entry_Table_Id_Nbr.setValue(310);                                                                                                                    //Natural: ASSIGN #PLAN-KEY.ENTRY-TABLE-ID-NBR = 000310
        pnd_Plan_Key_Entry_Table_Sub_Id.setValue("PL");                                                                                                                   //Natural: ASSIGN #PLAN-KEY.ENTRY-TABLE-SUB-ID = 'PL'
        //*  TNG
        pnd_Plan_Key_Entry_Cde.setValue("BENE-DEFAULT-PLAN");                                                                                                             //Natural: ASSIGN #PLAN-KEY.ENTRY-CDE = 'BENE-DEFAULT-PLAN'
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #PLAN-KEY
        (
        "PND_PND_L6152",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Plan_Key, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        PND_PND_L6152:
        while (condition(vw_app_Table_Entry.readNextRow("PND_PND_L6152")))
        {
            getReports().write(0, "SCIB9000 - INSIDE THE TABLE",vw_app_Table_Entry.getAstCOUNTER(),pnd_Plan_Key);                                                         //Natural: WRITE 'SCIB9000 - INSIDE THE TABLE' *COUNTER ( ##L6152. ) #PLAN-KEY
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L6152"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L6152"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  TNG  AND OR
            //*  TNG
            if (condition(app_Table_Entry_Entry_Table_Sub_Id.greater("PL") || app_Table_Entry_Entry_Rule_Category.notEquals("BENE-DFLT-PLAN")))                           //Natural: IF APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID > 'PL' OR ENTRY-RULE-CATEGORY NE 'BENE-DFLT-PLAN'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(app_Table_Entry_Entry_Table_Sub_Id.notEquals("PL")))                                                                                            //Natural: REJECT IF APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID <> 'PL'
            {
                continue;
            }
            pnd_Number_Of_Bene_Dflt_Plans.nadd(1);                                                                                                                        //Natural: ASSIGN #NUMBER-OF-BENE-DFLT-PLANS := #NUMBER-OF-BENE-DFLT-PLANS + 1
            pnd_Bene_Dflt_Plans.getValue(pnd_Number_Of_Bene_Dflt_Plans).setValue(app_Table_Entry_Entry_Rule_Plan);                                                        //Natural: MOVE APP-TABLE-ENTRY.ENTRY-RULE-PLAN TO #BENE-DFLT-PLANS ( #NUMBER-OF-BENE-DFLT-PLANS )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  (TNGPROC)
    private void sub_Retrieve_Irasub_Date() throws Exception                                                                                                              //Natural: RETRIEVE-IRASUB-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Plan_Key_Entry_Table_Id_Nbr.setValue(100);                                                                                                                    //Natural: ASSIGN #PLAN-KEY.ENTRY-TABLE-ID-NBR = 000100
        pnd_Plan_Key_Entry_Table_Sub_Id.setValue("IS");                                                                                                                   //Natural: ASSIGN #PLAN-KEY.ENTRY-TABLE-SUB-ID = 'IS'
        pnd_Plan_Key_Entry_Cde.setValue(" ");                                                                                                                             //Natural: ASSIGN #PLAN-KEY.ENTRY-CDE = ' '
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ ( 1 ) APP-TABLE-ENTRY WITH TABLE-ID-ENTRY-CDE = #PLAN-KEY
        (
        "READ02",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Plan_Key, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") },
        1
        );
        READ02:
        while (condition(vw_app_Table_Entry.readNextRow("READ02")))
        {
            //*  TNG  AND OR
            //*  TNG
            if (condition(app_Table_Entry_Entry_Table_Sub_Id.greater("IS") || app_Table_Entry_Entry_Table_Id_Nbr.notEquals(100)))                                         //Natural: IF APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID > 'IS' OR APP-TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE 00100
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ira_Sub_Global_Start_Date.compute(new ComputeParameters(false, pnd_Ira_Sub_Global_Start_Date), app_Table_Entry_Entry_Cde.val());                          //Natural: ASSIGN #IRA-SUB-GLOBAL-START-DATE = VAL ( APP-TABLE-ENTRY.ENTRY-CDE )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  START NY200
    private void sub_Move_Data_Scia1295_To_Scil9000() throws Exception                                                                                                    //Natural: MOVE-DATA-SCIA1295-TO-SCIL9000
    {
        if (BLNatReinput.isReinput()) return;

        ldaScil9000.getScil9000_Pnd_Plan_No().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Plan_Number());                                                                 //Natural: ASSIGN SCIL9000.#PLAN-NO := #SCIA1295-IN.#PLAN-NUMBER
        ldaScil9000.getScil9000_Pnd_Sub_Plan_No().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Subplan_Number());                                                          //Natural: ASSIGN SCIL9000.#SUB-PLAN-NO := #SCIA1295-IN.#SUBPLAN-NUMBER
        ldaScil9000.getScil9000_Pnd_Div_Sub().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Location_Code());                                                               //Natural: ASSIGN SCIL9000.#DIV-SUB := #SCIA1295-IN.#LOCATION-CODE
        ldaScil9000.getScil9000_Pnd_Prefix().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Name_Prefix());                                                                  //Natural: ASSIGN SCIL9000.#PREFIX := #SCIA1295-IN.#NAME-PREFIX
        ldaScil9000.getScil9000_Pnd_Last_Name().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Last_Name());                                                                 //Natural: ASSIGN SCIL9000.#LAST-NAME := #SCIA1295-IN.#LAST-NAME
        ldaScil9000.getScil9000_Pnd_First_Name().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_First_Name());                                                               //Natural: ASSIGN SCIL9000.#FIRST-NAME := #SCIA1295-IN.#FIRST-NAME
        ldaScil9000.getScil9000_Pnd_Middle_Name().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Middle_Name());                                                             //Natural: ASSIGN SCIL9000.#MIDDLE-NAME := #SCIA1295-IN.#MIDDLE-NAME
        ldaScil9000.getScil9000_Pnd_Ssn().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Ssn());                                                                             //Natural: ASSIGN SCIL9000.#SSN := #SCIA1295-IN.#SSN
        ldaScil9000.getScil9000_Pnd_Date_Of_Birth().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Birth_Date());                                                            //Natural: ASSIGN SCIL9000.#DATE-OF-BIRTH := #SCIA1295-IN.#BIRTH-DATE
        ldaScil9000.getScil9000_Pnd_Pin_Nbr().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Pin());                                                                         //Natural: ASSIGN SCIL9000.#PIN-NBR := #SCIA1295-IN.#PIN
        ldaScil9000.getScil9000_Pnd_Email_Address().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Email());                                                                 //Natural: ASSIGN SCIL9000.#EMAIL-ADDRESS := #SCIA1295-IN.#EMAIL
        ldaScil9000.getScil9000_Pnd_Daytime_Phone().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Daytime_Phone());                                                         //Natural: ASSIGN SCIL9000.#DAYTIME-PHONE := #SCIA1295-IN.#DAYTIME-PHONE
        ldaScil9000.getScil9000_Pnd_Evening_Phone().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Evening_Phone());                                                         //Natural: ASSIGN SCIL9000.#EVENING-PHONE := #SCIA1295-IN.#EVENING-PHONE
        if (condition(ldaScil9000.getScil9000_Pnd_Mailing_Zip_Code().equals("FORGN") || ldaScil9000.getScil9000_Pnd_Mailing_Zip_Code().equals("CANAD")))                  //Natural: IF SCIL9000.#MAILING-ZIP-CODE = 'FORGN' OR = 'CANAD'
        {
            ldaScil9000.getScil9000_Pnd_Mailing_Address_Line().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line1());                             //Natural: ASSIGN SCIL9000.#MAILING-ADDRESS-LINE ( 1 ) := #SCIA1295-IN.#MAIL-ADDRESS-LINE1
            ldaScil9000.getScil9000_Pnd_Mailing_Address_Line().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line2());                             //Natural: ASSIGN SCIL9000.#MAILING-ADDRESS-LINE ( 2 ) := #SCIA1295-IN.#MAIL-ADDRESS-LINE2
            ldaScil9000.getScil9000_Pnd_Mailing_Address_Line().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Address_Line3());                             //Natural: ASSIGN SCIL9000.#MAILING-ADDRESS-LINE ( 3 ) := #SCIA1295-IN.#MAIL-ADDRESS-LINE3
            ldaScil9000.getScil9000_Pnd_Mailing_City().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_City());                                                          //Natural: ASSIGN SCIL9000.#MAILING-CITY := #SCIA1295-IN.#MAIL-CITY
            ldaScil9000.getScil9000_Pnd_Mailing_State().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_State());                                                        //Natural: ASSIGN SCIL9000.#MAILING-STATE := #SCIA1295-IN.#MAIL-STATE
            ldaScil9000.getScil9000_Pnd_Mailing_Zip_Code().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Mail_Zip());                                                       //Natural: ASSIGN SCIL9000.#MAILING-ZIP-CODE := #SCIA1295-IN.#MAIL-ZIP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil9000.getScil9000_Pnd_Residence_Zip_Code().equals("FORGN") || ldaScil9000.getScil9000_Pnd_Residence_Zip_Code().equals("CANAD")))              //Natural: IF SCIL9000.#RESIDENCE-ZIP-CODE = 'FORGN' OR = 'CANAD'
        {
            ldaScil9000.getScil9000_Pnd_Residence_Address_Line().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Address_Line1());                            //Natural: ASSIGN SCIL9000.#RESIDENCE-ADDRESS-LINE ( 1 ) := #SCIA1295-IN.#RES-ADDRESS-LINE1
            ldaScil9000.getScil9000_Pnd_Residence_Address_Line().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Address_Line2());                            //Natural: ASSIGN SCIL9000.#RESIDENCE-ADDRESS-LINE ( 2 ) := #SCIA1295-IN.#RES-ADDRESS-LINE2
            ldaScil9000.getScil9000_Pnd_Residence_Address_Line().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Address_Line3());                            //Natural: ASSIGN SCIL9000.#RESIDENCE-ADDRESS-LINE ( 3 ) := #SCIA1295-IN.#RES-ADDRESS-LINE3
            ldaScil9000.getScil9000_Pnd_Residence_City().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Res_City());                                                         //Natural: ASSIGN SCIL9000.#RESIDENCE-CITY := #SCIA1295-IN.#RES-CITY
            ldaScil9000.getScil9000_Pnd_Residence_State().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Res_State());                                                       //Natural: ASSIGN SCIL9000.#RESIDENCE-STATE := #SCIA1295-IN.#RES-STATE
            ldaScil9000.getScil9000_Pnd_Residence_Zip_Code().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Zip());                                                      //Natural: ASSIGN SCIL9000.#RESIDENCE-ZIP-CODE := #SCIA1295-IN.#RES-ZIP
            ldaScil9000.getScil9000_Pnd_Country_Code().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Res_Iso_Country_Code());                                               //Natural: ASSIGN SCIL9000.#COUNTRY-CODE := #SCIA1295-IN.#RES-ISO-COUNTRY-CODE
        }                                                                                                                                                                 //Natural: END-IF
        ldaScil9000.getScil9000_Pnd_Bene_Estate().setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Default_To_Estate());                                                       //Natural: ASSIGN SCIL9000.#BENE-ESTATE := #SCIA1295-IN.#DEFAULT-TO-ESTATE
        ldaScil9000.getScil9000_Pnd_Bene_Mos_Ind().setValue("N");                                                                                                         //Natural: ASSIGN SCIL9000.#BENE-MOS-IND := 'N'
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 1 ) := #SCIA1295-IN.#BENE1-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE1-SSN TO SCIL9000.#BENE-SSN ( 1 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(1),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 1 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 1 ) := #SCIA1295-IN.#BENE1-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 1 ) := #SCIA1295-IN.#BENE1-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 1 ) := #SCIA1295-IN.#BENE1-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(1,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 1,1 ) := #SCIA1295-IN.#BENE1-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(1,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 1,2 ) := #SCIA1295-IN.#BENE1-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(1,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 1,3 ) := #SCIA1295-IN.#BENE1-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 1 ) := #SCIA1295-IN.#BENE1-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 1 ) := #SCIA1295-IN.#BENE1-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene1_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 1 ) := #SCIA1295-IN.#BENE1-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 2 ) := #SCIA1295-IN.#BENE2-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE2-SSN TO SCIL9000.#BENE-SSN ( 2 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(2),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 2 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 2 ) := #SCIA1295-IN.#BENE2-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 2 ) := #SCIA1295-IN.#BENE2-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 2 ) := #SCIA1295-IN.#BENE2-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(2,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 2,1 ) := #SCIA1295-IN.#BENE2-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(2,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 2,2 ) := #SCIA1295-IN.#BENE2-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(2,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 2,3 ) := #SCIA1295-IN.#BENE2-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 2 ) := #SCIA1295-IN.#BENE2-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 2 ) := #SCIA1295-IN.#BENE2-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene2_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 2 ) := #SCIA1295-IN.#BENE2-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 3 ) := #SCIA1295-IN.#BENE3-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE3-SSN TO SCIL9000.#BENE-SSN ( 3 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(3),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 3 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 3 ) := #SCIA1295-IN.#BENE3-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 3 ) := #SCIA1295-IN.#BENE3-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 3 ) := #SCIA1295-IN.#BENE3-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(3,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 3,1 ) := #SCIA1295-IN.#BENE3-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(3,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 3,2 ) := #SCIA1295-IN.#BENE3-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(3,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 3,3 ) := #SCIA1295-IN.#BENE3-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 3 ) := #SCIA1295-IN.#BENE3-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 3 ) := #SCIA1295-IN.#BENE3-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene3_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 3 ) := #SCIA1295-IN.#BENE3-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 4 ) := #SCIA1295-IN.#BENE4-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE4-SSN TO SCIL9000.#BENE-SSN ( 4 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(4),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 4 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 4 ) := #SCIA1295-IN.#BENE4-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 4 ) := #SCIA1295-IN.#BENE4-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 4 ) := #SCIA1295-IN.#BENE4-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(4,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 4,1 ) := #SCIA1295-IN.#BENE4-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(4,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 4,2 ) := #SCIA1295-IN.#BENE4-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(4,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 4,3 ) := #SCIA1295-IN.#BENE4-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 4 ) := #SCIA1295-IN.#BENE4-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 4 ) := #SCIA1295-IN.#BENE4-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(4).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene4_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 4 ) := #SCIA1295-IN.#BENE4-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 5 ) := #SCIA1295-IN.#BENE5-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE5-SSN TO SCIL9000.#BENE-SSN ( 5 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(5),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 5 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 5 ) := #SCIA1295-IN.#BENE5-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 5 ) := #SCIA1295-IN.#BENE5-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 5 ) := #SCIA1295-IN.#BENE5-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(5,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 5,1 ) := #SCIA1295-IN.#BENE5-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(5,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 5,2 ) := #SCIA1295-IN.#BENE5-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(5,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 5,3 ) := #SCIA1295-IN.#BENE5-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 5 ) := #SCIA1295-IN.#BENE5-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 5 ) := #SCIA1295-IN.#BENE5-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(5).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene5_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 5 ) := #SCIA1295-IN.#BENE5-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 6 ) := #SCIA1295-IN.#BENE6-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE6-SSN TO SCIL9000.#BENE-SSN ( 6 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(6),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 6 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 6 ) := #SCIA1295-IN.#BENE6-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 6 ) := #SCIA1295-IN.#BENE6-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 6 ) := #SCIA1295-IN.#BENE6-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(6,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 6,1 ) := #SCIA1295-IN.#BENE6-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(6,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 6,2 ) := #SCIA1295-IN.#BENE6-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(6,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 6,3 ) := #SCIA1295-IN.#BENE6-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 6 ) := #SCIA1295-IN.#BENE6-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 6 ) := #SCIA1295-IN.#BENE6-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(6).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene6_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 6 ) := #SCIA1295-IN.#BENE6-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 7 ) := #SCIA1295-IN.#BENE7-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE7-SSN TO SCIL9000.#BENE-SSN ( 7 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(7),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 7 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 7 ) := #SCIA1295-IN.#BENE7-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 7 ) := #SCIA1295-IN.#BENE7-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 7 ) := #SCIA1295-IN.#BENE7-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(7,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 7,1 ) := #SCIA1295-IN.#BENE7-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(7,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 7,2 ) := #SCIA1295-IN.#BENE7-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(7,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 7,3 ) := #SCIA1295-IN.#BENE7-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 7 ) := #SCIA1295-IN.#BENE7-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 7 ) := #SCIA1295-IN.#BENE7-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(7).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene7_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 7 ) := #SCIA1295-IN.#BENE7-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 8 ) := #SCIA1295-IN.#BENE8-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE8-SSN TO SCIL9000.#BENE-SSN ( 8 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(8),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 8 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 8 ) := #SCIA1295-IN.#BENE8-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 8 ) := #SCIA1295-IN.#BENE8-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 8 ) := #SCIA1295-IN.#BENE8-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(8,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 8,1 ) := #SCIA1295-IN.#BENE8-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(8,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 8,2 ) := #SCIA1295-IN.#BENE8-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(8,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 8,3 ) := #SCIA1295-IN.#BENE8-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 8 ) := #SCIA1295-IN.#BENE8-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 8 ) := #SCIA1295-IN.#BENE8-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(8).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene8_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 8 ) := #SCIA1295-IN.#BENE8-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Type());                                                    //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 9 ) := #SCIA1295-IN.#BENE9-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Ssn(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #SCIA1295-IN.#BENE9-SSN TO SCIL9000.#BENE-SSN ( 9 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(9),true), new ExamineSearch(" "), new ExamineReplace("0"));                     //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 9 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Name());                                                    //Natural: ASSIGN SCIL9000.#BENE-NAME ( 9 ) := #SCIA1295-IN.#BENE9-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Rel_Code());                                   //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 9 ) := #SCIA1295-IN.#BENE9-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Birthdate());                                      //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 9 ) := #SCIA1295-IN.#BENE9-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(9,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 9,1 ) := #SCIA1295-IN.#BENE9-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(9,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Text_Line1());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 9,2 ) := #SCIA1295-IN.#BENE9-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(9,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Text_Line3());                                    //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 9,3 ) := #SCIA1295-IN.#BENE9-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Alloc_Pct());                                          //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 9 ) := #SCIA1295-IN.#BENE9-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Alloc_Num());                                          //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 9 ) := #SCIA1295-IN.#BENE9-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(9).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene9_Alloc_Denum());                                      //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 9 ) := #SCIA1295-IN.#BENE9-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 10 ) := #SCIA1295-IN.#BENE10-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE10-SSN TO SCIL9000.#BENE-SSN ( 10 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(10),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 10 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 10 ) := #SCIA1295-IN.#BENE10-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 10 ) := #SCIA1295-IN.#BENE10-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 10 ) := #SCIA1295-IN.#BENE10-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(10,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 10,1 ) := #SCIA1295-IN.#BENE10-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(10,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 10,2 ) := #SCIA1295-IN.#BENE10-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(10,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 10,3 ) := #SCIA1295-IN.#BENE10-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 10 ) := #SCIA1295-IN.#BENE10-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 10 ) := #SCIA1295-IN.#BENE10-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(10).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene10_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 10 ) := #SCIA1295-IN.#BENE10-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 11 ) := #SCIA1295-IN.#BENE11-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE11-SSN TO SCIL9000.#BENE-SSN ( 11 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(11),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 11 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 11 ) := #SCIA1295-IN.#BENE11-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 11 ) := #SCIA1295-IN.#BENE11-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 11 ) := #SCIA1295-IN.#BENE11-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(11,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 11,1 ) := #SCIA1295-IN.#BENE11-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(11,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 11,2 ) := #SCIA1295-IN.#BENE11-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(11,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 11,3 ) := #SCIA1295-IN.#BENE11-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 11 ) := #SCIA1295-IN.#BENE11-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 11 ) := #SCIA1295-IN.#BENE11-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(11).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene11_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 11 ) := #SCIA1295-IN.#BENE11-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 12 ) := #SCIA1295-IN.#BENE12-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE12-SSN TO SCIL9000.#BENE-SSN ( 12 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(12),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 12 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 12 ) := #SCIA1295-IN.#BENE12-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 12 ) := #SCIA1295-IN.#BENE12-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 12 ) := #SCIA1295-IN.#BENE12-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(12,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 12,1 ) := #SCIA1295-IN.#BENE12-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(12,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 12,2 ) := #SCIA1295-IN.#BENE12-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(12,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 12,3 ) := #SCIA1295-IN.#BENE12-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 12 ) := #SCIA1295-IN.#BENE12-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 12 ) := #SCIA1295-IN.#BENE12-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(12).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene12_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 12 ) := #SCIA1295-IN.#BENE12-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 13 ) := #SCIA1295-IN.#BENE13-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE13-SSN TO SCIL9000.#BENE-SSN ( 13 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(13),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 13 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 13 ) := #SCIA1295-IN.#BENE13-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 13 ) := #SCIA1295-IN.#BENE13-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 13 ) := #SCIA1295-IN.#BENE13-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(13,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 13,1 ) := #SCIA1295-IN.#BENE13-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(13,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 13,2 ) := #SCIA1295-IN.#BENE13-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(13,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 13,3 ) := #SCIA1295-IN.#BENE13-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 13 ) := #SCIA1295-IN.#BENE13-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 13 ) := #SCIA1295-IN.#BENE13-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(13).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene13_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 13 ) := #SCIA1295-IN.#BENE13-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 14 ) := #SCIA1295-IN.#BENE14-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE14-SSN TO SCIL9000.#BENE-SSN ( 14 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(14),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 14 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 14 ) := #SCIA1295-IN.#BENE14-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 14 ) := #SCIA1295-IN.#BENE14-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 14 ) := #SCIA1295-IN.#BENE14-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(14,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 14,1 ) := #SCIA1295-IN.#BENE14-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(14,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 14,2 ) := #SCIA1295-IN.#BENE14-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(14,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 14,3 ) := #SCIA1295-IN.#BENE14-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 14 ) := #SCIA1295-IN.#BENE14-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 14 ) := #SCIA1295-IN.#BENE14-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(14).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene14_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 14 ) := #SCIA1295-IN.#BENE14-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 15 ) := #SCIA1295-IN.#BENE15-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE15-SSN TO SCIL9000.#BENE-SSN ( 15 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(15),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 15 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 15 ) := #SCIA1295-IN.#BENE15-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 15 ) := #SCIA1295-IN.#BENE15-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 15 ) := #SCIA1295-IN.#BENE15-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(15,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 15,1 ) := #SCIA1295-IN.#BENE15-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(15,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 15,2 ) := #SCIA1295-IN.#BENE15-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(15,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 15,3 ) := #SCIA1295-IN.#BENE15-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 15 ) := #SCIA1295-IN.#BENE15-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 15 ) := #SCIA1295-IN.#BENE15-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(15).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene15_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 15 ) := #SCIA1295-IN.#BENE15-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 16 ) := #SCIA1295-IN.#BENE16-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE16-SSN TO SCIL9000.#BENE-SSN ( 16 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(16),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 16 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 16 ) := #SCIA1295-IN.#BENE16-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 16 ) := #SCIA1295-IN.#BENE16-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 16 ) := #SCIA1295-IN.#BENE16-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(16,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 16,1 ) := #SCIA1295-IN.#BENE16-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(16,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 16,2 ) := #SCIA1295-IN.#BENE16-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(16,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 16,3 ) := #SCIA1295-IN.#BENE16-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 16 ) := #SCIA1295-IN.#BENE16-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 16 ) := #SCIA1295-IN.#BENE16-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(16).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene16_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 16 ) := #SCIA1295-IN.#BENE16-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 17 ) := #SCIA1295-IN.#BENE17-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE17-SSN TO SCIL9000.#BENE-SSN ( 17 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(17),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 17 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 17 ) := #SCIA1295-IN.#BENE17-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 17 ) := #SCIA1295-IN.#BENE17-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 17 ) := #SCIA1295-IN.#BENE17-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(17,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 17,1 ) := #SCIA1295-IN.#BENE17-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(17,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 17,2 ) := #SCIA1295-IN.#BENE17-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(17,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 17,3 ) := #SCIA1295-IN.#BENE17-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 17 ) := #SCIA1295-IN.#BENE17-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 17 ) := #SCIA1295-IN.#BENE17-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(17).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene17_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 17 ) := #SCIA1295-IN.#BENE17-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 18 ) := #SCIA1295-IN.#BENE18-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE18-SSN TO SCIL9000.#BENE-SSN ( 18 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(18),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 18 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 18 ) := #SCIA1295-IN.#BENE18-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 18 ) := #SCIA1295-IN.#BENE18-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 18 ) := #SCIA1295-IN.#BENE18-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(18,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 18,1 ) := #SCIA1295-IN.#BENE18-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(18,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 18,2 ) := #SCIA1295-IN.#BENE18-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(18,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 18,3 ) := #SCIA1295-IN.#BENE18-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 18 ) := #SCIA1295-IN.#BENE18-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 18 ) := #SCIA1295-IN.#BENE18-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(18).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene18_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 18 ) := #SCIA1295-IN.#BENE18-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 19 ) := #SCIA1295-IN.#BENE19-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE19-SSN TO SCIL9000.#BENE-SSN ( 19 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(19),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 19 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 19 ) := #SCIA1295-IN.#BENE19-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 19 ) := #SCIA1295-IN.#BENE19-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 19 ) := #SCIA1295-IN.#BENE19-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(19,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 19,1 ) := #SCIA1295-IN.#BENE19-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(19,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 19,2 ) := #SCIA1295-IN.#BENE19-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(19,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 19,3 ) := #SCIA1295-IN.#BENE19-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 19 ) := #SCIA1295-IN.#BENE19-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 19 ) := #SCIA1295-IN.#BENE19-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(19).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene19_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 19 ) := #SCIA1295-IN.#BENE19-ALLOC-DENUM
        ldaScil9000.getScil9000_Pnd_Bene_Type().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Type());                                                  //Natural: ASSIGN SCIL9000.#BENE-TYPE ( 20 ) := #SCIA1295-IN.#BENE20-TYPE
        ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Ssn(), MoveOption.RightJustified);                         //Natural: MOVE RIGHT #SCIA1295-IN.#BENE20-SSN TO SCIL9000.#BENE-SSN ( 20 )
        DbsUtil.examine(new ExamineSource(ldaScil9000.getScil9000_Pnd_Bene_Ssn().getValue(20),true), new ExamineSearch(" "), new ExamineReplace("0"));                    //Natural: EXAMINE FULL SCIL9000.#BENE-SSN ( 20 ) FOR ' ' REPLACE WITH '0'
        ldaScil9000.getScil9000_Pnd_Bene_Name().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Name());                                                  //Natural: ASSIGN SCIL9000.#BENE-NAME ( 20 ) := #SCIA1295-IN.#BENE20-NAME
        ldaScil9000.getScil9000_Pnd_Bene_Relationship_Code().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Rel_Code());                                 //Natural: ASSIGN SCIL9000.#BENE-RELATIONSHIP-CODE ( 20 ) := #SCIA1295-IN.#BENE20-REL-CODE
        ldaScil9000.getScil9000_Pnd_Bene_Date_Of_Birth().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Birthdate());                                    //Natural: ASSIGN SCIL9000.#BENE-DATE-OF-BIRTH ( 20 ) := #SCIA1295-IN.#BENE20-BIRTHDATE
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(20,1).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 20,1 ) := #SCIA1295-IN.#BENE20-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(20,2).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Text_Line1());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 20,2 ) := #SCIA1295-IN.#BENE20-TEXT-LINE1
        ldaScil9000.getScil9000_Pnd_Bene_Special_Text().getValue(20,3).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Text_Line3());                                  //Natural: ASSIGN SCIL9000.#BENE-SPECIAL-TEXT ( 20,3 ) := #SCIA1295-IN.#BENE20-TEXT-LINE3
        ldaScil9000.getScil9000_Pnd_Bene_Alloc_Pct().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Alloc_Pct());                                        //Natural: ASSIGN SCIL9000.#BENE-ALLOC-PCT ( 20 ) := #SCIA1295-IN.#BENE20-ALLOC-PCT
        ldaScil9000.getScil9000_Pnd_Bene_Numerator().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Alloc_Num());                                        //Natural: ASSIGN SCIL9000.#BENE-NUMERATOR ( 20 ) := #SCIA1295-IN.#BENE20-ALLOC-NUM
        ldaScil9000.getScil9000_Pnd_Bene_Denominator().getValue(20).setValue(pdaScia1295.getPnd_Scia1295_In_Pnd_Bene20_Alloc_Denum());                                    //Natural: ASSIGN SCIL9000.#BENE-DENOMINATOR ( 20 ) := #SCIA1295-IN.#BENE20-ALLOC-DENUM
    }
    //*   (TNGPROC)
    private void sub_Ira_Sub_Beneficiary_Copy() throws Exception                                                                                                          //Natural: IRA-SUB-BENEFICIARY-COPY
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        bene_File_Bene_File_Data.reset();                                                                                                                                 //Natural: RESET BENE-FILE-DATA
        bene_File_Bene_Intrfce_Bsnss_Dte.setValue(pnd_Current_Business_Date_Pnd_Current_Business_Date_A);                                                                 //Natural: ASSIGN BENE-INTRFCE-BSNSS-DTE := #CURRENT-BUSINESS-DATE-A
        bene_File_Bene_Intrfcng_Systm.setValue("ACIS");                                                                                                                   //Natural: ASSIGN BENE-INTRFCNG-SYSTM := 'ACIS'
        bene_File_Bene_Rqstng_System.setValue("IRA SUB");                                                                                                                 //Natural: ASSIGN BENE-RQSTNG-SYSTEM := 'IRA SUB'
        bene_File_Bene_New_Issuefslash_Chng_Ind.setValue("N");                                                                                                            //Natural: ASSIGN BENE-NEW-ISSUE/CHNG-IND := 'N'
        bene_File_Bene_Nmbr_Of_Benes.setValue(1);                                                                                                                         //Natural: ASSIGN BENE-NMBR-OF-BENES := 1
        bene_File_Bene_Pin_Nbr.compute(new ComputeParameters(false, bene_File_Bene_Pin_Nbr), pdaScia1200.getScia1200_Pnd_Pin_Nbr().val());                                //Natural: ASSIGN BENE-PIN-NBR := VAL ( SCIA1200.#PIN-NBR )
        bene_File_Bene_Tiaa_Nbr.setValue(pdaScia1200.getScia1200_Pnd_Tiaa_Contract_No());                                                                                 //Natural: ASSIGN BENE-TIAA-NBR := SCIA1200.#TIAA-CONTRACT-NO
        bene_File_Bene_Cref_Nbr.setValue(pdaScia1200.getScia1200_Pnd_Cref_Certificate_No());                                                                              //Natural: ASSIGN BENE-CREF-NBR := SCIA1200.#CREF-CERTIFICATE-NO
        bene_File_Bene_Contract_Type.setValue("D");                                                                                                                       //Natural: ASSIGN BENE-CONTRACT-TYPE := 'D'
        bene_File_Bene_Effective_Dt.setValue(pnd_Current_Business_Date_Pnd_Current_Business_Date_A);                                                                      //Natural: ASSIGN BENE-EFFECTIVE-DT := #CURRENT-BUSINESS-DATE-A
        bene_File_Bene_Tiaa_Cref_Ind.setValue(" ");                                                                                                                       //Natural: ASSIGN BENE-TIAA-CREF-IND := ' '
        bene_File_Bene_Last_Dsgntn_Srce.setValue("I");                                                                                                                    //Natural: ASSIGN BENE-LAST-DSGNTN-SRCE := 'I'
        bene_File_Bene_Last_Dsgntn_System.setValue("A");                                                                                                                  //Natural: ASSIGN BENE-LAST-DSGNTN-SYSTEM := 'A'
        bene_File_Bene_Same_As_Ind.setValue("Y");                                                                                                                         //Natural: ASSIGN BENE-SAME-AS-IND := 'Y'
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        bene_File_Bene_Name.getValue(1).setValue(DbsUtil.compress("SAME AS CONTRACT ", ldaScil9000.getScil9000_Pnd_Orig_Tiaa_Cntrct()));                                  //Natural: COMPRESS 'SAME AS CONTRACT ' SCIL9000.#ORIG-TIAA-CNTRCT INTO BENE-NAME ( 1 )
        bene_File_Bene_Type.getValue(1).setValue("P");                                                                                                                    //Natural: ASSIGN BENE-TYPE ( 1 ) := 'P'
        bene_File_Bene_Prctge_Frctn_Ind.getValue(1).setValue("P");                                                                                                        //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( 1 ) := 'P'
        bene_File_Bene_Alloc_Pct.getValue(1).setValue(100);                                                                                                               //Natural: ASSIGN BENE-ALLOC-PCT ( 1 ) := 100.0
        //*  BADOTBLS
        DbsUtil.callnat(Filtomdm.class , getCurrentProcessState(), bene_File);                                                                                            //Natural: CALLNAT 'FILTOMDM' BENE-FILE
        if (condition(Global.isEscape())) return;
    }
    //*  ONEIRA
    private void sub_Check_Break_On_Plan_Divsub() throws Exception                                                                                                        //Natural: CHECK-BREAK-ON-PLAN-DIVSUB
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================================================
        if (condition(ldaScil9000.getScil9000_Pnd_Div_Sub().notEquals(pnd_Hold_Divsub)))                                                                                  //Natural: IF SCIL9000.#DIV-SUB NE #HOLD-DIVSUB
        {
            if (condition(pnd_Hold_Divsub.greater(" ") && break_Counters_Pnd_Divsub_Cnt.notEquals(getZero())))                                                            //Natural: IF #HOLD-DIVSUB GT ' ' AND #DIVSUB-CNT NE 0
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-DIVISION-TOTALS
                sub_Write_Division_Totals();
                if (condition(Global.isEscape())) {return;}
                break_Counters_Pnd_Divsub_Cnt.reset();                                                                                                                    //Natural: RESET #DIVSUB-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(break_Counters_Pnd_Plan_Cnt.notEquals(getZero())))                                                                                              //Natural: IF #PLAN-CNT NE 0
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"               ");                                                                          //Natural: WRITE ( 01 ) NOTITLE 1T '               '
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaScil9000.getScil9000_Pnd_Plan_No().notEquals(pnd_Hold_Plan_No)))                                                                                 //Natural: IF SCIL9000.#PLAN-NO NE #HOLD-PLAN-NO
        {
            if (condition(pnd_Hold_Plan_No.greater(" ") && break_Counters_Pnd_Plan_Cnt.notEquals(getZero())))                                                             //Natural: IF #HOLD-PLAN-NO GT ' ' AND #PLAN-CNT NE 0
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-PLAN-TOTALS
                sub_Write_Plan_Totals();
                if (condition(Global.isEscape())) {return;}
                break_Counters_Pnd_Plan_Cnt.reset();                                                                                                                      //Natural: RESET #PLAN-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hold_Plan_No.setValue(ldaScil9000.getScil9000_Pnd_Plan_No());                                                                                                 //Natural: ASSIGN #HOLD-PLAN-NO := SCIL9000.#PLAN-NO
        pnd_Hold_Divsub.setValue(ldaScil9000.getScil9000_Pnd_Div_Sub());                                                                                                  //Natural: ASSIGN #HOLD-DIVSUB := SCIL9000.#DIV-SUB
    }
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        getReports().write(0, "Invoking OPEN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                     //Natural: WRITE 'Invoking OPEN :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "Finished OPEN:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                      //Natural: WRITE 'Finished OPEN:' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"OMNI Interface Summary Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 25T 'OMNI Interface Summary Report' 63T 'Run Date:' *DATX / 25T '      Process Totals         ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"      Process Totals         ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(3, 2);                                                                                                                              //Natural: SKIP ( 3 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"PowerImage Interface Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 04 ) NOTITLE NOHDR 25T 'PowerImage Interface Report' 63T 'Run Date:' *DATX / 25T '     Accepted Records      ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"     Accepted Records      ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(4, 2);                                                                                                                              //Natural: SKIP ( 4 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"PowerImage Interface Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 05 ) NOTITLE NOHDR 25T 'PowerImage Interface Report' 63T 'Run Date:' *DATX / 25T '    Rejected Records       ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"    Rejected Records       ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(5, 2);                                                                                                                              //Natural: SKIP ( 5 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"PowerImage Interface Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 06 ) NOTITLE NOHDR 25T 'PowerImage Interface Report' 63T 'Run Date:' *DATX / 25T '      Process Totals       ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"      Process Totals       ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(6, 2);                                                                                                                              //Natural: SKIP ( 6 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(7, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"PowerImage Interface Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 7 ) NOTITLE NOHDR 25T 'PowerImage Interface Report' 63T 'Run Date:' *DATX / 25T 'New Tasks for ACIS Rejects ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"New Tasks for ACIS Rejects ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(7, 2);                                                                                                                              //Natural: SKIP ( 7 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        short decideConditionsMet3214 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet3214++;
            if (condition(pnd_Retry_Cnt.less(25)))                                                                                                                        //Natural: IF #RETRY-CNT < 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.equals(25)))                                                                                                                      //Natural: IF #RETRY-CNT = 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",intvl,reqid);                                                                                     //Natural: CALL 'CMROLL' INTVL REQID
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(25) && pnd_Retry_Cnt.lessOrEqual(50)))                                                                                    //Natural: IF #RETRY-CNT > 25 AND #RETRY-CNT <= 50
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(50)))                                                                                                                     //Natural: IF #RETRY-CNT > 50
            {
                getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                        //Natural: WRITE NOHDR '*****************************' /
                getReports().write(0, ReportOption.NOHDR,"* ADABAS RECORD ON HOLD     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* ADABAS RECORD ON HOLD     *' /
                getReports().write(0, ReportOption.NOHDR,"* RETRY COUNT EXCEEDED.     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* RETRY COUNT EXCEEDED.     *' /
                getReports().write(0, ReportOption.NOHDR,"* BACKOUT UPDATES.          *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* BACKOUT UPDATES.          *' /
                getReports().write(0, ReportOption.NOHDR,"*****************************");                                                                                //Natural: WRITE NOHDR '*****************************'
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 3148
        else if (condition((Global.getERROR_NR().equals(3148))))
        {
            decideConditionsMet3214++;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                            //Natural: WRITE NOHDR '*****************************' /
            getReports().write(0, ReportOption.NOHDR,"* ADABAS FILE NOT AVAILABLE *",NEWLINE);                                                                            //Natural: WRITE NOHDR '* ADABAS FILE NOT AVAILABLE *' /
            getReports().write(0, ReportOption.NOHDR,"* ANY UPDATES BACKED OUT    *",NEWLINE);                                                                            //Natural: WRITE NOHDR '* ANY UPDATES BACKED OUT    *' /
            getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                            //Natural: WRITE NOHDR '*****************************' /
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",                  //Natural: WRITE NOHDR '****************************' / 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE / '****************************' /
                Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE,"****************************",NEWLINE);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
        Global.format(3, "LS=132 PS=60");
        Global.format(4, "LS=132 PS=60");
        Global.format(5, "LS=132 PS=60");
        Global.format(6, "LS=132 PS=60");
        Global.format(7, "LS=132 PS=60");

        getReports().setDisplayColumns(0, "********  OMNI INTERFACE FILE IS EMPTY ********");
    }
}
