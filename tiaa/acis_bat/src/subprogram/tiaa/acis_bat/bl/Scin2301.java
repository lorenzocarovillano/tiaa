/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:08:46 AM
**        * FROM NATURAL SUBPROGRAM : Scin2301
************************************************************
**        * FILE NAME            : Scin2301.java
**        * CLASS NAME           : Scin2301
**        * INSTANCE NAME        : Scin2301
************************************************************
**--------------------------------------------------------------------**
*  PROGRAM-ID    :  SCIN2301                                           *
*  DESCRIPTION   :  RETRIEVE PRODUCT CODE BASED ON RL CONTRACT         *
**--------------------------------------------------------------------**
*   DATE    DEVELOPER CHANGE DESCRIPTION                               *
* --------  --------- ------------------------------------------------ *
* 05/11/12  B.NEWSOM  RESTOWED WITH DIV-SUB NEW FIELDS IN SCIA1200     *
**--------------------------------------------------------------------**
* NOTE:                                                                *
**--------------------------------------------------------------------**
*

************************************************************ */

package tiaa.acis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scin2301 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup scia2301;

    private DbsGroup scia2301_Pnd_Input;
    private DbsField scia2301_Pnd_Rl_Contract_No;

    private DbsGroup scia2301_Pnd_Output;
    private DbsField scia2301_Pnd_Product_Cde;
    private DbsField scia2301_Pnd_Return_Code;
    private DbsField scia2301_Pnd_Return_Txt;
    private DbsField pnd_Found_Contract;

    private DataAccessProgramView vw_fund_Desc;
    private DbsField fund_Desc_Nec_Table_Cde;
    private DbsField fund_Desc_Nec_Product_Cde;
    private DbsField fund_Desc_Nec_Sub_Product_Cde;
    private DbsField fund_Desc_Nec_Acct_From;
    private DbsField fund_Desc_Nec_Acct_To;
    private DbsField fund_Desc_Nec_Cref_Acct_From;
    private DbsField fund_Desc_Nec_Cref_Acct_To;
    private DbsField pnd_Nec_Prd_Super2;

    private DbsGroup pnd_Nec_Prd_Super2__R_Field_1;
    private DbsField pnd_Nec_Prd_Super2_Pnd_Nec_Table_Cde;
    private DbsField pnd_Nec_Prd_Super2_Pnd_Nec_Product_Cde;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();

        scia2301 = parameters.newGroupInRecord("scia2301", "SCIA2301");
        scia2301.setParameterOption(ParameterOption.ByReference);

        scia2301_Pnd_Input = scia2301.newGroupInGroup("scia2301_Pnd_Input", "#INPUT");
        scia2301_Pnd_Rl_Contract_No = scia2301_Pnd_Input.newFieldInGroup("scia2301_Pnd_Rl_Contract_No", "#RL-CONTRACT-NO", FieldType.STRING, 10);

        scia2301_Pnd_Output = scia2301.newGroupInGroup("scia2301_Pnd_Output", "#OUTPUT");
        scia2301_Pnd_Product_Cde = scia2301_Pnd_Output.newFieldInGroup("scia2301_Pnd_Product_Cde", "#PRODUCT-CDE", FieldType.STRING, 10);
        scia2301_Pnd_Return_Code = scia2301_Pnd_Output.newFieldInGroup("scia2301_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 4);
        scia2301_Pnd_Return_Txt = scia2301_Pnd_Output.newFieldInGroup("scia2301_Pnd_Return_Txt", "#RETURN-TXT", FieldType.STRING, 70);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Found_Contract = localVariables.newFieldInRecord("pnd_Found_Contract", "#FOUND-CONTRACT", FieldType.BOOLEAN, 1);

        vw_fund_Desc = new DataAccessProgramView(new NameInfo("vw_fund_Desc", "FUND-DESC"), "NEW_EXT_CNTRL_PRD", "NEW_EXT_CNTRL");
        fund_Desc_Nec_Table_Cde = vw_fund_Desc.getRecord().newFieldInGroup("fund_Desc_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NEC_TABLE_CDE");
        fund_Desc_Nec_Product_Cde = vw_fund_Desc.getRecord().newFieldInGroup("fund_Desc_Nec_Product_Cde", "NEC-PRODUCT-CDE", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "NEC_PRODUCT_CDE");
        fund_Desc_Nec_Product_Cde.setDdmHeader("PRODUCT CODE");
        fund_Desc_Nec_Sub_Product_Cde = vw_fund_Desc.getRecord().newFieldInGroup("fund_Desc_Nec_Sub_Product_Cde", "NEC-SUB-PRODUCT-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "NEC_SUB_PRODUCT_CDE");
        fund_Desc_Nec_Acct_From = vw_fund_Desc.getRecord().newFieldInGroup("fund_Desc_Nec_Acct_From", "NEC-ACCT-FROM", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "NEC_ACCT_FROM");
        fund_Desc_Nec_Acct_To = vw_fund_Desc.getRecord().newFieldInGroup("fund_Desc_Nec_Acct_To", "NEC-ACCT-TO", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "NEC_ACCT_TO");
        fund_Desc_Nec_Cref_Acct_From = vw_fund_Desc.getRecord().newFieldInGroup("fund_Desc_Nec_Cref_Acct_From", "NEC-CREF-ACCT-FROM", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_CREF_ACCT_FROM");
        fund_Desc_Nec_Cref_Acct_To = vw_fund_Desc.getRecord().newFieldInGroup("fund_Desc_Nec_Cref_Acct_To", "NEC-CREF-ACCT-TO", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "NEC_CREF_ACCT_TO");
        registerRecord(vw_fund_Desc);

        pnd_Nec_Prd_Super2 = localVariables.newFieldInRecord("pnd_Nec_Prd_Super2", "#NEC-PRD-SUPER2", FieldType.STRING, 13);

        pnd_Nec_Prd_Super2__R_Field_1 = localVariables.newGroupInRecord("pnd_Nec_Prd_Super2__R_Field_1", "REDEFINE", pnd_Nec_Prd_Super2);
        pnd_Nec_Prd_Super2_Pnd_Nec_Table_Cde = pnd_Nec_Prd_Super2__R_Field_1.newFieldInGroup("pnd_Nec_Prd_Super2_Pnd_Nec_Table_Cde", "#NEC-TABLE-CDE", 
            FieldType.STRING, 3);
        pnd_Nec_Prd_Super2_Pnd_Nec_Product_Cde = pnd_Nec_Prd_Super2__R_Field_1.newFieldInGroup("pnd_Nec_Prd_Super2_Pnd_Nec_Product_Cde", "#NEC-PRODUCT-CDE", 
            FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fund_Desc.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Scin2301() throws Exception
    {
        super("Scin2301");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        pnd_Nec_Prd_Super2_Pnd_Nec_Table_Cde.setValue("PRD");                                                                                                             //Natural: ASSIGN #NEC-TABLE-CDE := 'PRD'
        pnd_Nec_Prd_Super2_Pnd_Nec_Product_Cde.reset();                                                                                                                   //Natural: RESET #NEC-PRODUCT-CDE #PRODUCT-CDE #RETURN-CODE #RETURN-TXT
        scia2301_Pnd_Product_Cde.reset();
        scia2301_Pnd_Return_Code.reset();
        scia2301_Pnd_Return_Txt.reset();
        vw_fund_Desc.startDatabaseRead                                                                                                                                    //Natural: READ FUND-DESC BY NEC-PRD-SUPER2 EQ #NEC-PRD-SUPER2
        (
        "READ01",
        new Wc[] { new Wc("NEC_PRD_SUPER2", ">=", pnd_Nec_Prd_Super2, WcType.BY) },
        new Oc[] { new Oc("NEC_PRD_SUPER2", "ASC") }
        );
        READ01:
        while (condition(vw_fund_Desc.readNextRow("READ01")))
        {
            if (condition(fund_Desc_Nec_Table_Cde.notEquals("PRD")))                                                                                                      //Natural: IF NEC-TABLE-CDE NOT EQ 'PRD'
            {
                if (condition(scia2301_Pnd_Product_Cde.equals(" ")))                                                                                                      //Natural: IF #PRODUCT-CDE = ' '
                {
                    scia2301_Pnd_Return_Code.setValue(9999);                                                                                                              //Natural: ASSIGN #RETURN-CODE := 9999
                    scia2301_Pnd_Return_Txt.setValue("RL CONTRACT NOT FOUND IN EXTERNALIZATION");                                                                         //Natural: ASSIGN #RETURN-TXT := 'RL CONTRACT NOT FOUND IN EXTERNALIZATION'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(scia2301_Pnd_Rl_Contract_No.greaterOrEqual(fund_Desc_Nec_Cref_Acct_From) && scia2301_Pnd_Rl_Contract_No.lessOrEqual(fund_Desc_Nec_Cref_Acct_To))) //Natural: IF #RL-CONTRACT-NO GE NEC-CREF-ACCT-FROM AND #RL-CONTRACT-NO LE NEC-CREF-ACCT-TO
            {
                scia2301_Pnd_Product_Cde.setValue(fund_Desc_Nec_Product_Cde);                                                                                             //Natural: ASSIGN #PRODUCT-CDE := NEC-PRODUCT-CDE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
